﻿namespace AtomEHR.公共卫生.Module
{
    partial class frm重叠数据向导
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm重叠数据向导));
            DevExpress.XtraCharts.SimpleDiagram simpleDiagram2 = new DevExpress.XtraCharts.SimpleDiagram();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel3 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions5 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PiePointOptions piePointOptions6 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.SeriesPoint seriesPoint5 = new DevExpress.XtraCharts.SeriesPoint("AA", new object[] {
            ((object)(12D))}, 0);
            DevExpress.XtraCharts.SeriesPoint seriesPoint6 = new DevExpress.XtraCharts.SeriesPoint("BB", new object[] {
            ((object)(13D))}, 1);
            DevExpress.XtraCharts.SeriesPoint seriesPoint7 = new DevExpress.XtraCharts.SeriesPoint("CC", new object[] {
            ((object)(21D))}, 2);
            DevExpress.XtraCharts.SeriesPoint seriesPoint8 = new DevExpress.XtraCharts.SeriesPoint("DD", new object[] {
            ((object)(31D))}, 3);
            DevExpress.XtraCharts.PieSeriesView pieSeriesView3 = new DevExpress.XtraCharts.PieSeriesView(new int[] {
            3,
            2,
            1,
            0});
            DevExpress.XtraCharts.SeriesTitle seriesTitle2 = new DevExpress.XtraCharts.SeriesTitle();
            DevExpress.XtraCharts.PieSeriesLabel pieSeriesLabel4 = new DevExpress.XtraCharts.PieSeriesLabel();
            DevExpress.XtraCharts.PiePointOptions piePointOptions7 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PiePointOptions piePointOptions8 = new DevExpress.XtraCharts.PiePointOptions();
            DevExpress.XtraCharts.PieSeriesView pieSeriesView4 = new DevExpress.XtraCharts.PieSeriesView();
            this.wizardControl1 = new DevExpress.XtraWizard.WizardControl();
            this.welcomeWizardPage1 = new DevExpress.XtraWizard.WelcomeWizardPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btn分母 = new DevExpress.XtraEditors.SimpleButton();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio重点人群 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.chk2型糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肿瘤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk慢阻肺 = new DevExpress.XtraEditors.CheckEdit();
            this.chk重症精神病 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wizardPage1 = new DevExpress.XtraWizard.WizardPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn添加 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn确定 = new DevExpress.XtraEditors.SimpleButton();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk高血压1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk2型糖尿病1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk冠心病1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑卒中1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肿瘤1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk慢阻肺1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk重症精神病1 = new DevExpress.XtraEditors.CheckEdit();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.completionWizardPage1 = new DevExpress.XtraWizard.CompletionWizardPage();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            ((System.ComponentModel.ISupportInitialize)(this.wizardControl1)).BeginInit();
            this.wizardControl1.SuspendLayout();
            this.welcomeWizardPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio重点人群.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2型糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肿瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk慢阻肺.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重症精神病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            this.wizardPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2型糖尿病1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk冠心病1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑卒中1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肿瘤1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk慢阻肺1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重症精神病1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            this.completionWizardPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView4)).BeginInit();
            this.SuspendLayout();
            // 
            // wizardControl1
            // 
            this.wizardControl1.CancelText = "返回";
            this.wizardControl1.Controls.Add(this.welcomeWizardPage1);
            this.wizardControl1.Controls.Add(this.wizardPage1);
            this.wizardControl1.Controls.Add(this.completionWizardPage1);
            this.wizardControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizardControl1.FinishText = "&关闭";
            this.wizardControl1.Location = new System.Drawing.Point(0, 0);
            this.wizardControl1.Name = "wizardControl1";
            this.wizardControl1.NextText = "&下一步 >";
            this.wizardControl1.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.welcomeWizardPage1,
            this.wizardPage1,
            this.completionWizardPage1});
            this.wizardControl1.PreviousText = "< &上一步";
            this.wizardControl1.Size = new System.Drawing.Size(963, 593);
            this.wizardControl1.Text = "";
            // 
            // welcomeWizardPage1
            // 
            this.welcomeWizardPage1.Controls.Add(this.layoutControl1);
            this.welcomeWizardPage1.IntroductionText = "";
            this.welcomeWizardPage1.Name = "welcomeWizardPage1";
            this.welcomeWizardPage1.ProceedText = "";
            this.welcomeWizardPage1.Size = new System.Drawing.Size(746, 460);
            this.welcomeWizardPage1.Text = "选择重叠数据底数";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.btn分母);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(866, 169, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(746, 460);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(613, 257);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 14);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "分母:";
            // 
            // btn分母
            // 
            this.btn分母.Image = ((System.Drawing.Image)(resources.GetObject("btn分母.Image")));
            this.btn分母.Location = new System.Drawing.Point(645, 257);
            this.btn分母.Name = "btn分母";
            this.btn分母.Size = new System.Drawing.Size(89, 22);
            this.btn分母.StyleController = this.layoutControl1;
            this.btn分母.TabIndex = 7;
            this.btn分母.Text = "确定";
            this.btn分母.Click += new System.EventHandler(this.btn分母_Click);
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.radio重点人群);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(43, 12);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(691, 99);
            this.flowLayoutPanel5.TabIndex = 5;
            // 
            // radio重点人群
            // 
            this.radio重点人群.Location = new System.Drawing.Point(0, 0);
            this.radio重点人群.Margin = new System.Windows.Forms.Padding(0);
            this.radio重点人群.Name = "radio重点人群";
            this.radio重点人群.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("", "不限年龄"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "0~3岁幼童"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "0~6岁幼童"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("15", "育龄妇女"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("23", "孕产妇"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("65", "65岁及以上老年人")});
            this.radio重点人群.Size = new System.Drawing.Size(691, 82);
            this.radio重点人群.TabIndex = 0;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.chk高血压);
            this.flowLayoutPanel6.Controls.Add(this.chk2型糖尿病);
            this.flowLayoutPanel6.Controls.Add(this.chk冠心病);
            this.flowLayoutPanel6.Controls.Add(this.chk脑卒中);
            this.flowLayoutPanel6.Controls.Add(this.chk肿瘤);
            this.flowLayoutPanel6.Controls.Add(this.chk慢阻肺);
            this.flowLayoutPanel6.Controls.Add(this.chk重症精神病);
            this.flowLayoutPanel6.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(43, 115);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(691, 138);
            this.flowLayoutPanel6.TabIndex = 6;
            this.flowLayoutPanel6.WrapContents = false;
            // 
            // chk高血压
            // 
            this.chk高血压.Location = new System.Drawing.Point(0, 0);
            this.chk高血压.Margin = new System.Windows.Forms.Padding(0);
            this.chk高血压.Name = "chk高血压";
            this.chk高血压.Properties.Caption = "高血压";
            this.chk高血压.Size = new System.Drawing.Size(75, 19);
            this.chk高血压.TabIndex = 0;
            // 
            // chk2型糖尿病
            // 
            this.chk2型糖尿病.Location = new System.Drawing.Point(0, 19);
            this.chk2型糖尿病.Margin = new System.Windows.Forms.Padding(0);
            this.chk2型糖尿病.Name = "chk2型糖尿病";
            this.chk2型糖尿病.Properties.Caption = "糖尿病";
            this.chk2型糖尿病.Size = new System.Drawing.Size(86, 19);
            this.chk2型糖尿病.TabIndex = 1;
            // 
            // chk冠心病
            // 
            this.chk冠心病.Location = new System.Drawing.Point(0, 38);
            this.chk冠心病.Margin = new System.Windows.Forms.Padding(0);
            this.chk冠心病.Name = "chk冠心病";
            this.chk冠心病.Properties.Caption = "冠心病";
            this.chk冠心病.Size = new System.Drawing.Size(75, 19);
            this.chk冠心病.TabIndex = 2;
            // 
            // chk脑卒中
            // 
            this.chk脑卒中.Location = new System.Drawing.Point(0, 57);
            this.chk脑卒中.Margin = new System.Windows.Forms.Padding(0);
            this.chk脑卒中.Name = "chk脑卒中";
            this.chk脑卒中.Properties.Caption = "脑卒中";
            this.chk脑卒中.Size = new System.Drawing.Size(75, 19);
            this.chk脑卒中.TabIndex = 3;
            // 
            // chk肿瘤
            // 
            this.chk肿瘤.Location = new System.Drawing.Point(0, 76);
            this.chk肿瘤.Margin = new System.Windows.Forms.Padding(0);
            this.chk肿瘤.Name = "chk肿瘤";
            this.chk肿瘤.Properties.Caption = "肿瘤";
            this.chk肿瘤.Size = new System.Drawing.Size(64, 19);
            this.chk肿瘤.TabIndex = 4;
            // 
            // chk慢阻肺
            // 
            this.chk慢阻肺.Location = new System.Drawing.Point(0, 95);
            this.chk慢阻肺.Margin = new System.Windows.Forms.Padding(0);
            this.chk慢阻肺.Name = "chk慢阻肺";
            this.chk慢阻肺.Properties.Caption = "慢阻肺";
            this.chk慢阻肺.Size = new System.Drawing.Size(63, 19);
            this.chk慢阻肺.TabIndex = 5;
            // 
            // chk重症精神病
            // 
            this.chk重症精神病.Location = new System.Drawing.Point(0, 114);
            this.chk重症精神病.Margin = new System.Windows.Forms.Padding(0);
            this.chk重症精神病.Name = "chk重症精神病";
            this.chk重症精神病.Properties.Caption = "重症精神病";
            this.chk重症精神病.Size = new System.Drawing.Size(88, 19);
            this.chk重症精神病.TabIndex = 6;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem3,
            this.emptySpaceItem2,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(746, 460);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.flowLayoutPanel5;
            this.layoutControlItem2.CustomizationFormText = "年龄:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(726, 103);
            this.layoutControlItem2.Text = "年龄:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(28, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.flowLayoutPanel6;
            this.layoutControlItem1.CustomizationFormText = "患者:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 103);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(726, 142);
            this.layoutControlItem1.Text = "患者:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(28, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 271);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(726, 169);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btn分母;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(633, 245);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(93, 26);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 245);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(601, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.labelControl1;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(601, 245);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(32, 26);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // wizardPage1
            // 
            this.wizardPage1.Controls.Add(this.layoutControl2);
            this.wizardPage1.DescriptionText = "";
            this.wizardPage1.Name = "wizardPage1";
            this.wizardPage1.Size = new System.Drawing.Size(931, 448);
            this.wizardPage1.Text = "选择重叠数据因数";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.btn删除);
            this.layoutControl2.Controls.Add(this.btn添加);
            this.layoutControl2.Controls.Add(this.gridControl1);
            this.layoutControl2.Controls.Add(this.btn确定);
            this.layoutControl2.Controls.Add(this.flowLayoutPanel1);
            this.layoutControl2.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7});
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(866, 169, 250, 350);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(931, 448);
            this.layoutControl2.TabIndex = 1;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(765, 81);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(76, 22);
            this.btn删除.StyleController = this.layoutControl2;
            this.btn删除.TabIndex = 10;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn添加
            // 
            this.btn添加.Image = ((System.Drawing.Image)(resources.GetObject("btn添加.Image")));
            this.btn添加.Location = new System.Drawing.Point(647, 81);
            this.btn添加.Name = "btn添加";
            this.btn添加.Size = new System.Drawing.Size(114, 22);
            this.btn添加.StyleController = this.layoutControl2;
            this.btn添加.TabIndex = 9;
            this.btn添加.Text = "添加分子数据";
            this.btn添加.Click += new System.EventHandler(this.btn添加_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.Location = new System.Drawing.Point(12, 107);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(907, 329);
            this.gridControl1.TabIndex = 8;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "类型";
            this.gridColumn1.FieldName = "类型";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "人数";
            this.gridColumn2.FieldName = "人数";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // btn确定
            // 
            this.btn确定.Image = ((System.Drawing.Image)(resources.GetObject("btn确定.Image")));
            this.btn确定.Location = new System.Drawing.Point(845, 81);
            this.btn确定.Name = "btn确定";
            this.btn确定.Size = new System.Drawing.Size(74, 22);
            this.btn确定.StyleController = this.layoutControl2;
            this.btn确定.TabIndex = 7;
            this.btn确定.Text = "确定";
            this.btn确定.Click += new System.EventHandler(this.btn确定_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.radioGroup1);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(43, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(876, 35);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(0, 0);
            this.radioGroup1.Margin = new System.Windows.Forms.Padding(0);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("", "不限年龄"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "0~3岁幼童"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "0~6岁幼童"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("15", "育龄妇女"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("23", "孕产妇"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("65", "65岁及以上老年人")});
            this.radioGroup1.Size = new System.Drawing.Size(800, 32);
            this.radioGroup1.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.chk高血压1);
            this.flowLayoutPanel2.Controls.Add(this.chk2型糖尿病1);
            this.flowLayoutPanel2.Controls.Add(this.chk冠心病1);
            this.flowLayoutPanel2.Controls.Add(this.chk脑卒中1);
            this.flowLayoutPanel2.Controls.Add(this.chk肿瘤1);
            this.flowLayoutPanel2.Controls.Add(this.chk慢阻肺1);
            this.flowLayoutPanel2.Controls.Add(this.chk重症精神病1);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(43, 51);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(876, 26);
            this.flowLayoutPanel2.TabIndex = 6;
            // 
            // chk高血压1
            // 
            this.chk高血压1.Location = new System.Drawing.Point(0, 0);
            this.chk高血压1.Margin = new System.Windows.Forms.Padding(0);
            this.chk高血压1.Name = "chk高血压1";
            this.chk高血压1.Properties.Caption = "高血压";
            this.chk高血压1.Size = new System.Drawing.Size(75, 19);
            this.chk高血压1.TabIndex = 0;
            // 
            // chk2型糖尿病1
            // 
            this.chk2型糖尿病1.Location = new System.Drawing.Point(75, 0);
            this.chk2型糖尿病1.Margin = new System.Windows.Forms.Padding(0);
            this.chk2型糖尿病1.Name = "chk2型糖尿病1";
            this.chk2型糖尿病1.Properties.Caption = "糖尿病";
            this.chk2型糖尿病1.Size = new System.Drawing.Size(86, 19);
            this.chk2型糖尿病1.TabIndex = 1;
            // 
            // chk冠心病1
            // 
            this.chk冠心病1.Location = new System.Drawing.Point(161, 0);
            this.chk冠心病1.Margin = new System.Windows.Forms.Padding(0);
            this.chk冠心病1.Name = "chk冠心病1";
            this.chk冠心病1.Properties.Caption = "冠心病";
            this.chk冠心病1.Size = new System.Drawing.Size(75, 19);
            this.chk冠心病1.TabIndex = 2;
            // 
            // chk脑卒中1
            // 
            this.chk脑卒中1.Location = new System.Drawing.Point(236, 0);
            this.chk脑卒中1.Margin = new System.Windows.Forms.Padding(0);
            this.chk脑卒中1.Name = "chk脑卒中1";
            this.chk脑卒中1.Properties.Caption = "脑卒中";
            this.chk脑卒中1.Size = new System.Drawing.Size(75, 19);
            this.chk脑卒中1.TabIndex = 3;
            // 
            // chk肿瘤1
            // 
            this.chk肿瘤1.Location = new System.Drawing.Point(311, 0);
            this.chk肿瘤1.Margin = new System.Windows.Forms.Padding(0);
            this.chk肿瘤1.Name = "chk肿瘤1";
            this.chk肿瘤1.Properties.Caption = "肿瘤";
            this.chk肿瘤1.Size = new System.Drawing.Size(64, 19);
            this.chk肿瘤1.TabIndex = 4;
            // 
            // chk慢阻肺1
            // 
            this.chk慢阻肺1.Location = new System.Drawing.Point(375, 0);
            this.chk慢阻肺1.Margin = new System.Windows.Forms.Padding(0);
            this.chk慢阻肺1.Name = "chk慢阻肺1";
            this.chk慢阻肺1.Properties.Caption = "慢阻肺";
            this.chk慢阻肺1.Size = new System.Drawing.Size(63, 19);
            this.chk慢阻肺1.TabIndex = 5;
            // 
            // chk重症精神病1
            // 
            this.chk重症精神病1.Location = new System.Drawing.Point(438, 0);
            this.chk重症精神病1.Margin = new System.Windows.Forms.Padding(0);
            this.chk重症精神病1.Name = "chk重症精神病1";
            this.chk重症精神病1.Properties.Caption = "重症精神病";
            this.chk重症精神病1.Size = new System.Drawing.Size(88, 19);
            this.chk重症精神病1.TabIndex = 6;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 116);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(702, 143);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Root";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem4,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem11});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(931, 448);
            this.layoutControlGroup2.Text = "Root";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.flowLayoutPanel1;
            this.layoutControlItem4.CustomizationFormText = "年龄:";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem2";
            this.layoutControlItem4.Size = new System.Drawing.Size(911, 39);
            this.layoutControlItem4.Text = "年龄:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(28, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.flowLayoutPanel2;
            this.layoutControlItem5.CustomizationFormText = "患者:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 39);
            this.layoutControlItem5.Name = "layoutControlItem1";
            this.layoutControlItem5.Size = new System.Drawing.Size(911, 30);
            this.layoutControlItem5.Text = "患者:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(28, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btn确定;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem6.Location = new System.Drawing.Point(833, 69);
            this.layoutControlItem6.Name = "layoutControlItem3";
            this.layoutControlItem6.Size = new System.Drawing.Size(78, 26);
            this.layoutControlItem6.Text = "layoutControlItem3";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 69);
            this.emptySpaceItem4.Name = "emptySpaceItem2";
            this.emptySpaceItem4.Size = new System.Drawing.Size(635, 26);
            this.emptySpaceItem4.Text = "emptySpaceItem2";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.gridControl1;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 95);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(911, 333);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btn添加;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(635, 69);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(118, 26);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btn删除;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(753, 69);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // completionWizardPage1
            // 
            this.completionWizardPage1.Controls.Add(this.chartControl1);
            this.completionWizardPage1.FinishText = "";
            this.completionWizardPage1.Name = "completionWizardPage1";
            this.completionWizardPage1.ProceedText = "";
            this.completionWizardPage1.Size = new System.Drawing.Size(746, 483);
            this.completionWizardPage1.Text = "";
            // 
            // chartControl1
            // 
            simpleDiagram2.EqualPieSize = false;
            this.chartControl1.Diagram = simpleDiagram2;
            this.chartControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartControl1.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Left;
            this.chartControl1.Legend.AlignmentVertical = DevExpress.XtraCharts.LegendAlignmentVertical.BottomOutside;
            this.chartControl1.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartControl1.Location = new System.Drawing.Point(0, 0);
            this.chartControl1.Name = "chartControl1";
            pieSeriesLabel3.Antialiasing = true;
            piePointOptions5.PointView = DevExpress.XtraCharts.PointView.Argument;
            piePointOptions5.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            pieSeriesLabel3.PointOptions = piePointOptions5;
            pieSeriesLabel3.Position = DevExpress.XtraCharts.PieSeriesLabelPosition.TwoColumns;
            series2.Label = pieSeriesLabel3;
            piePointOptions6.ArgumentNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            piePointOptions6.PercentOptions.ValueAsPercent = false;
            piePointOptions6.PointView = DevExpress.XtraCharts.PointView.ArgumentAndValues;
            piePointOptions6.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            piePointOptions6.ValueNumericOptions.Precision = 0;
            series2.LegendPointOptions = piePointOptions6;
            series2.Name = "Series 1";
            series2.Points.AddRange(new DevExpress.XtraCharts.SeriesPoint[] {
            seriesPoint5,
            seriesPoint6,
            seriesPoint7,
            seriesPoint8});
            series2.SynchronizePointOptions = false;
            pieSeriesView3.ExplodeMode = DevExpress.XtraCharts.PieExplodeMode.UsePoints;
            pieSeriesView3.RuntimeExploding = false;
            seriesTitle2.Text = "标题";
            pieSeriesView3.Titles.AddRange(new DevExpress.XtraCharts.SeriesTitle[] {
            seriesTitle2});
            series2.View = pieSeriesView3;
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series2};
            piePointOptions7.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.General;
            pieSeriesLabel4.PointOptions = piePointOptions7;
            this.chartControl1.SeriesTemplate.Label = pieSeriesLabel4;
            piePointOptions8.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            this.chartControl1.SeriesTemplate.LegendPointOptions = piePointOptions8;
            this.chartControl1.SeriesTemplate.SynchronizePointOptions = false;
            pieSeriesView4.RuntimeExploding = false;
            this.chartControl1.SeriesTemplate.View = pieSeriesView4;
            this.chartControl1.Size = new System.Drawing.Size(746, 483);
            this.chartControl1.TabIndex = 1;
            // 
            // frm重叠数据向导
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 593);
            this.Controls.Add(this.wizardControl1);
            this.Name = "frm重叠数据向导";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frm重叠数据向导_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wizardControl1)).EndInit();
            this.wizardControl1.ResumeLayout(false);
            this.welcomeWizardPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio重点人群.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2型糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肿瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk慢阻肺.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重症精神病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            this.wizardPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk高血压1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk2型糖尿病1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk冠心病1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑卒中1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肿瘤1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk慢阻肺1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk重症精神病1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            this.completionWizardPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(simpleDiagram2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pieSeriesView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWizard.WizardControl wizardControl1;
        private DevExpress.XtraWizard.WelcomeWizardPage welcomeWizardPage1;
        private DevExpress.XtraWizard.WizardPage wizardPage1;
        private DevExpress.XtraWizard.CompletionWizardPage completionWizardPage1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.RadioGroup radio重点人群;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.CheckEdit chk高血压;
        private DevExpress.XtraEditors.CheckEdit chk2型糖尿病;
        private DevExpress.XtraEditors.CheckEdit chk冠心病;
        private DevExpress.XtraEditors.CheckEdit chk脑卒中;
        private DevExpress.XtraEditors.CheckEdit chk肿瘤;
        private DevExpress.XtraEditors.CheckEdit chk慢阻肺;
        private DevExpress.XtraEditors.CheckEdit chk重症精神病;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton btn分母;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.SimpleButton btn确定;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.CheckEdit chk高血压1;
        private DevExpress.XtraEditors.CheckEdit chk2型糖尿病1;
        private DevExpress.XtraEditors.CheckEdit chk冠心病1;
        private DevExpress.XtraEditors.CheckEdit chk脑卒中1;
        private DevExpress.XtraEditors.CheckEdit chk肿瘤1;
        private DevExpress.XtraEditors.CheckEdit chk慢阻肺1;
        private DevExpress.XtraEditors.CheckEdit chk重症精神病1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton btn添加;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
    }
}