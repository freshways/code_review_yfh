﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using DevExpress.XtraCharts;

namespace AtomEHR.公共卫生.Module
{
    public partial class frm重叠数据向导 : Form
    {
        public frm重叠数据向导()
        {
            InitializeComponent();
        }

        bll慢性病统计分析 bll = new bll慢性病统计分析();
        DataTable dtMain = new DataTable();
        private void frm重叠数据向导_Load(object sender, EventArgs e)
        {
            DataColumn dc1 = new DataColumn();

            dc1.DataType = System.Type.GetType("System.String");
            dc1.AllowDBNull = false;
            dc1.Caption = "类型";
            dc1.ColumnName = "类型";
            dtMain.Columns.Add(dc1);

            DataColumn dc2 = new DataColumn();

            dc2.DataType = System.Type.GetType("System.String");
            dc2.AllowDBNull = false;
            dc2.Caption = "人数";
            dc2.ColumnName = "人数";
            dtMain.Columns.Add(dc2);

            //设置图表标题  
            ChartTitle ct = new ChartTitle();
            ct.Text = "慢性病人群统计分析图";
            ct.TextColor = Color.Black;//颜色  
            ct.Font = new Font("Tahoma", 12);//字体  
            ct.Dock = ChartTitleDockStyle.Top;//停靠在上方  
            ct.Alignment = StringAlignment.Center;//居中显示  
            this.chartControl1.Titles.Add(ct);
        }

        double d分母 = 0;
        private void btn分母_Click(object sender, EventArgs e)
        {
            string sql = "select count(1) 人数 from View_个人信息表 where 档案状态='1' ";
            string where  = SetWhere();
            if (!string.IsNullOrEmpty(where))
                sql += where;
            DataTable dt =  bll.GetDataTable(sql);
            if (dt != null && dt.Rows.Count > 0)
                d分母 = Convert.ToDouble(dt.Rows[0]["人数"].ToString());
            this.labelControl1.Text = "分母:" + d分母.ToString();

            string title = radio重点人群.Properties.Items[radio重点人群.SelectedIndex].Description + "中" + GetFlowLayoutResult(flowLayoutPanel6) + "人群数据分析";
            this.chartControl1.Titles[0].Text = title;
        }

        private void btn添加_Click(object sender, EventArgs e)
        {
            string sql = "select count(1) 人数 from View_个人信息表 where 档案状态='1' ";
            string where = SetWhere_Sub();
            if (!string.IsNullOrEmpty(where))
                sql += where;
            DataTable dt = bll.GetDataTable(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                d分母 -= Convert.ToDouble(dt.Rows[0]["人数"].ToString());
                DataRow dr = dtMain.NewRow();
                dr["类型"] = GetFlowLayoutResult(flowLayoutPanel2);
                dr["人数"] = Convert.ToDouble(dt.Rows[0]["人数"].ToString());
                dtMain.Rows.Add(dr);
            }
            this.gridControl1.DataSource = dtMain;
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            DataRow dr =  gridView1.GetFocusedDataRow();
            if (dr == null) return;
            dtMain.Rows.Remove(dr);
        }

        string SetWhere()
        {
            string _strWhere = string.Empty;
            //重点人群
            if (this.radio重点人群.EditValue != null && this.radio重点人群.EditValue.ToString() != "")
            {
                string age = this.radio重点人群.EditValue.ToString();
                if (age == "3")//儿童0-三岁
                {
                    string date = DateTime.Now.AddYears(-4).ToString("yyyy-MM-dd");
                    _strWhere += "  and  出生日期 >= '" + date + "' ";
                }
                else if (age == "6")//儿童0-6岁
                {
                    string date = DateTime.Now.AddYears(-7).ToString("yyyy-MM-dd");
                    _strWhere += "  and  出生日期 >= '" + date + "' ";
                }
                else if (age == "15")//育龄妇女
                {
                    string date1 = DateTime.Now.AddYears(-15).ToString("yyyy-MM-dd");
                    string date2 = DateTime.Now.AddYears(-49).ToString("yyyy-MM-dd");
                    _strWhere += "  and 出生日期 <= '" + date1 + "'  and 出生日期 >= '" + date2 + "' and 性别  = '女' ";
                }
                else if (age == "23")//孕产妇
                {
                    _strWhere += "  and 孕产妇基本信息表 is not null  and 孕产妇基本信息表<>''  ";
                }
                else if (age == "65")//65岁及以上老年人 
                {
                    string date = DateTime.Now.AddYears(-65).ToString("yyyy-MM-dd");
                    _strWhere += "  and  出生日期 <= '" + date + "' ";
                }
            }
            if (chk高血压.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB高血压管理卡 where isnull(tb_MXB高血压管理卡.终止管理,'')<>'1') ";
            }
            if (chk2型糖尿病.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB糖尿病管理卡 where isnull(tb_MXB糖尿病管理卡.终止管理,'')<>'1') ";
            }
            if (chk冠心病.Checked)
            {
                _strWhere += " and  个人档案编号 in  ( select 个人档案编号 from tb_MXB冠心病管理卡 where isnull(tb_MXB冠心病管理卡.终止管理,'')<>'1') ";
            }
            if (chk脑卒中.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB脑卒中管理卡 where isnull(tb_MXB脑卒中管理卡.终止管理,'')<>'1')  ";
            }
            if (chk肿瘤.Checked)
            {
                _strWhere += " and 是否肿瘤 =  '1' ";
            }
            if (chk慢阻肺.Checked)
            {
                _strWhere += " and 是否慢阻肺 =  '1' ";
            }
            if (chk重症精神病.Checked)
            {
                _strWhere += " and 精神疾病信息补充表 <>  '' and 精神疾病信息补充表 is not null ";
            }

            return _strWhere;
        }

        string SetWhere_Sub()
        {
            string _strWhere = string.Empty;
            //重点人群
            if (this.radio重点人群.EditValue != null && this.radio重点人群.EditValue.ToString() != "")
            {
                string age = this.radio重点人群.EditValue.ToString();
                if (age == "3")//儿童0-三岁
                {
                    string date = DateTime.Now.AddYears(-4).ToString("yyyy-MM-dd");
                    _strWhere += "  and  出生日期 >= '" + date + "' ";
                }
                else if (age == "6")//儿童0-6岁
                {
                    string date = DateTime.Now.AddYears(-7).ToString("yyyy-MM-dd");
                    _strWhere += "  and  出生日期 >= '" + date + "' ";
                }
                else if (age == "15")//育龄妇女
                {
                    string date1 = DateTime.Now.AddYears(-15).ToString("yyyy-MM-dd");
                    string date2 = DateTime.Now.AddYears(-49).ToString("yyyy-MM-dd");
                    _strWhere += "  and 出生日期 <= '" + date1 + "'  and 出生日期 >= '" + date2 + "' and 性别  = '女' ";
                }
                else if (age == "23")//孕产妇
                {
                    _strWhere += "  and 孕产妇基本信息表 is not null  and 孕产妇基本信息表<>''  ";
                }
                else if (age == "65")//65岁及以上老年人 
                {
                    string date = DateTime.Now.AddYears(-65).ToString("yyyy-MM-dd");
                    _strWhere += "  and  出生日期 <= '" + date + "' ";
                }
            }
            if (chk高血压1.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB高血压管理卡 where isnull(tb_MXB高血压管理卡.终止管理,'')<>'1') ";
            }
            if (chk2型糖尿病1.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB糖尿病管理卡 where isnull(tb_MXB糖尿病管理卡.终止管理,'')<>'1') ";
            }
            if (chk冠心病1.Checked)
            {
                _strWhere += " and  个人档案编号 in  ( select 个人档案编号 from tb_MXB冠心病管理卡 where isnull(tb_MXB冠心病管理卡.终止管理,'')<>'1') ";
            }
            if (chk脑卒中1.Checked)
            {
                _strWhere += " and 个人档案编号 in  ( select 个人档案编号 from tb_MXB脑卒中管理卡 where isnull(tb_MXB脑卒中管理卡.终止管理,'')<>'1')  ";
            }
            if (chk肿瘤1.Checked)
            {
                _strWhere += " and 是否肿瘤 =  '1' ";
            }
            if (chk慢阻肺1.Checked)
            {
                _strWhere += " and 是否慢阻肺 =  '1' ";
            }
            if (chk重症精神病1.Checked)
            {
                _strWhere += " and 精神疾病信息补充表 <>  '' and 精神疾病信息补充表 is not null ";
            }

            return _strWhere;
        }

        private  string GetFlowLayoutResult(FlowLayoutPanel flow)
        {
            if (flow == null || flow.Controls.Count == 0) return "";
            string result = "";
            for (int i = 0; i < flow.Controls.Count; i++)
            {
                if (flow.Controls[i].GetType() == typeof(DevExpress.XtraEditors.CheckEdit))
                {
                    DevExpress.XtraEditors.CheckEdit chk = (DevExpress.XtraEditors.CheckEdit)flow.Controls[i];
                    if (chk == null) continue;
                    if (chk.Checked)
                    {
                        result += chk.Text + ",";
                    }
                }
            }
            result = string.IsNullOrEmpty(result) ? result : result.Remove(result.Length - 1);
            return result;
        }

        private void btn确定_Click(object sender, EventArgs e)
        {
            if (dtMain != null && dtMain.Rows.Count>0)
            LoadChartInfo();
        }

        /// <summary>
        /// 统计图
        /// </summary>
        private Series mySeries;
        /// <summary>
        /// 手动加载统计图信息
        /// </summary>
        private void LoadChartInfo()
        {
            this.chartControl1.Series.Clear();

            //Legend显示样式
            chartControl1.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Left; //居左显示
            chartControl1.Legend.AlignmentVertical = LegendAlignmentVertical.BottomOutside; //显示在下方

            mySeries = new Series("圆饼图", ViewType.Pie);
            ((PiePointOptions)mySeries.LegendPointOptions).PointView = PointView.ArgumentAndValues;
            ((PieSeriesLabel)mySeries.Label).Position = PieSeriesLabelPosition.TwoColumns;

            //设置爆炸方式
            ((PieSeriesView)mySeries.View).ExplodeMode = PieExplodeMode.UsePoints;

            //设置Series样式  
            mySeries.ArgumentScaleType = ScaleType.Auto;//.Qualitative;//定性的
            mySeries.ValueScaleType = ScaleType.Numerical;//数字类型  
            mySeries.Label.PointOptions.PointView = PointView.ArgumentAndValues;//图表显示标题和值

            mySeries.LegendPointOptions.PointView = PointView.ArgumentAndValues;//显示表示的信息和数据  
            mySeries.LegendPointOptions.ValueNumericOptions.Format = NumericFormat.Number;//NumericFormat.Percent;//用百分比表示


            // 获取到的数据
            DataTable datTable = new DataTable();
            datTable = dtMain;

            this.chartControl1.Legend.Visible = false;
            //设置显示方式右上角
            ((PiePointOptions)mySeries.LegendPointOptions).PercentOptions.ValueAsPercent = false;
            ((PiePointOptions)mySeries.LegendPointOptions).ValueNumericOptions.Format = NumericFormat.Number;
            ((PiePointOptions)mySeries.LegendPointOptions).ValueNumericOptions.Precision = 0;

            string titles = "";
            for (int i = 0; i < datTable.Rows.Count; i++)
            {
                string strType = datTable.Rows[i]["类型"].ToString();
                double dtCount = Convert.ToDouble(datTable.Rows[i]["人数"].ToString());

                mySeries.Points.Add(new SeriesPoint(strType + "(" + dtCount.ToString() + ")", new double[] { dtCount }));
                titles += strType + ";";
            }
            mySeries.Points.Add(new SeriesPoint("单纯"+GetFlowLayoutResult(flowLayoutPanel6) + "(" + d分母.ToString() + ")", new double[] { d分母 }));

            this.completionWizardPage1.Text = titles.Substring(0,titles.Length-1)+"各人群占比";
            //这个是爆炸扩展
            chartControl1.SetPieExplode(mySeries, PieExplodeMode.MinValue, 5, true);



            //添加到统计图上
            this.chartControl1.Series.Clear();
            this.chartControl1.Series.Add(mySeries);


            //图例设置
            SimpleDiagram3D diagram = new SimpleDiagram3D();
            diagram.RuntimeRotation = true;
            diagram.RuntimeScrolling = true;
            diagram.RuntimeZooming = true;
        }


    }
}
