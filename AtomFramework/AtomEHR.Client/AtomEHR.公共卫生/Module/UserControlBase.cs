﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using DevExpress.XtraNavBar;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraLayout;
using AtomEHR.Business;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Common;
using AtomEHR.Business.Security;



namespace AtomEHR.公共卫生.Module
{
    public partial class UserControlBase : UserControl
    {

        #region 常用变量
        /// <summary>
        /// 设置页面的考核项目
        /// </summary>
        private DataRow[] _base考核项 = null;

        protected int _base缺项 = 0;

        protected int _base完整度 = 0;

        private int _base总项 = 0;

        protected bll机构信息 _bll机构信息 = new bll机构信息();
        protected bllUser _bllUser = new bllUser();
        protected bll地区档案 _bll地区 = new bll地区档案();
        /// <summary>
        /// 业务单据窗体的业务逻辑层
        /// </summary>
        protected bllBaseBusiness _BLL = new bllUnknow();
        public bllBaseBusiness BLL { get { return _BLL; } }

        /// <summary>
        /// 数据操作状态
        /// </summary>
        protected UpdateType _UpdateType = UpdateType.None;

        /// <summary>
        /// 当前正在处理的业务数据
        /// </summary>
        protected DataTable _dt缓存数据 = null;

        /// <summary>
        /// 当前正在处理的业务数据
        /// </summary>        
        public DataTable dt缓存数据 { get { return _dt缓存数据; } }
        #endregion

        public UserControlBase()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 在保存时有此情况发生: 不会更新最后一个编辑框里的数据!
        /// 当移除焦点后会更新输入框的数据.
        /// </summary>
        protected void UpdateLastControl()
        {
            try
            {
                if (ActiveControl == null) return;
                Control ctl = ActiveControl;
                txtFocusForSave.Focus();
                ActiveControl = ctl;
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }

        #region NavBarControl
        /// <summary>
        /// 创建导航组件的组按钮(NavBarControl.NavBarGroup)
        /// </summary>
        /// <param name="navBar">NavBarControl对象</param>
        /// <param name="moduleMenu">要处理的数据</param>
        /// <param name="moduleDisplayName">年</param>
        /// <param name="s划分时间字段"></param>
        //public virtual void CreateNavBarButton(NavBarControl navBar, DataTable moduleMenu, string moduleDisplayName, string s划分时间字段)
        //{
        //    navBar.Groups.Clear();
        //    //绑定事件,当组按钮的Index改变时触发
        //    navBar.MouseClick += new MouseEventHandler(this.OnNavBar_MouseClick);

        //    NavBarGroup group = navBar.Groups.Add();//增加一个Button组.
        //    group.Caption = moduleDisplayName; //模块的显示名称
        //    group.LargeImageIndex = 0;

        //    for (int i = 0; i < moduleMenu.Rows.Count; i++)
        //    {
        //        string yaer = moduleMenu.Rows[i][s划分时间字段].ToString();
        //        if (Convert.ToDateTime(yaer).Year.ToString() == moduleDisplayName)
        //        {
        //            NavBarItem newItem = new NavBarItem();
        //            newItem.Caption = Convert.ToDateTime(yaer).ToShortDateString();
        //            newItem.Tag = moduleMenu.Rows[i];
        //            navBar.Items.Add(newItem);
        //            group.ItemLinks.Add(newItem);
        //        }
        //    }

        //}

        ///// <summary>
        ///// 点击导航分组按钮时触发该事件
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected virtual void OnNavBar_MouseClick(object sender, MouseEventArgs e)
        //{
        //    NavBarControl nav = (sender as NavBarControl);//取到NavBarControl对象引用
        //    NavBarHitInfo hit = nav.CalcHitInfo(e.Location);//计算点击区域的对象

        //    if (hit.InGroup && hit.InGroupCaption)//点击导航分组按钮
        //    {
        //        try
        //        {
        //            nav.ActiveGroup = hit.Group; //立即设置为激活的组
        //            hit.Group.Expanded = true;
        //        }
        //        catch (Exception ex)
        //        { Msg.ShowException(ex); }
        //    }
        //    else if (hit.Link != null && hit.Link.Item.Tag != null)
        //    {
        //        try
        //        {
        //            DoBindingSummaryEditor(hit.Link.Item.Tag);
        //        }
        //        catch (Exception ex)
        //        { Msg.ShowException(ex); }
        //    }
        //}

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected virtual void DoBindingSummaryEditor(DataTable dataSource)
        {
            //需要重写,此方法适用于修改保存页面
        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected virtual void DoBindingSummaryEditor(object dataSource)
        {
            if (dataSource is DataRow)
            {
                DataRow dr = dataSource as DataRow;

                _dt缓存数据 = _BLL.CurrentBusiness.Tables[0].Clone();

                _dt缓存数据.Rows.Add(dr.ItemArray);

                DoBindingSummaryEditor(_dt缓存数据);
            }
            //需要重写,此方法仅限于显示页面
        }
        #endregion

        #region flow绑定处理

        /// <summary>
        /// 获取flow控件的值
        /// </summary>
        /// <param name="flow"></param>
        /// <returns></returns>
        protected virtual object GetFlowLayoutResult(FlowLayoutPanel flow)
        {
            if (flow == null || flow.Controls.Count == 0) return "";
            string result = "";
            for (int i = 0; i < flow.Controls.Count; i++)
            {
                if (flow.Controls[i].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flow.Controls[i];
                    if (chk == null) continue;
                    if (chk.Checked)
                    {
                        result += chk.Tag + ",";
                    }
                }
            }
            result = string.IsNullOrEmpty(result) ? result : result.Remove(result.Length - 1);
            return result;
        }

        /// <summary>
        /// 绑定flow控件的值-全部绑定
        /// </summary>
        /// <param name="valueList"></param>
        /// <param name="flow"></param>
        protected virtual void SetFlowLayoutResult(FlowLayoutPanel flow,bool check)
        {
            for (int j = 0; j < flow.Controls.Count; j++)
            {
                if (flow.Controls[j].GetType() != typeof(CheckEdit)) break;
                if (check )
                {
                    CheckEdit chk = (CheckEdit)flow.Controls[j];
                    chk.Checked = check;
                }
                else
                {
                    CheckEdit chk = (CheckEdit)flow.Controls[j];
                    chk.Checked = false;
                }
            }
            
        }

        /// <summary>
        /// 绑定flow控件的值
        /// </summary>
        /// <param name="valueList"></param>
        /// <param name="flow"></param>
        protected virtual void SetFlowLayoutResult(string valueList, FlowLayoutPanel flow)
        {
            if (string.IsNullOrEmpty(valueList)) return;
            if (flow == null) return;
            for (int j = 0; j < flow.Controls.Count; j++)
            {
                if (flow.Controls[j].GetType() == typeof(CheckEdit))
                {
                    CheckEdit chk = (CheckEdit)flow.Controls[j];
                    chk.Checked = false;
                }
            }
            string[] list = valueList.Split(',');
            for (int i = 0; i < list.Length; i++)
            {
                string item = list[i];
                if (string.IsNullOrEmpty(item)) continue;

                for (int j = 0; j < flow.Controls.Count; j++)
                {
                    if (flow.Controls[j].GetType() == typeof(CheckEdit))
                    {
                        CheckEdit chk = (CheckEdit)flow.Controls[j];
                        string tag = chk.Tag == null ? "" : Convert.ToString(chk.Tag);
                        if (string.IsNullOrEmpty(tag)) continue;
                        if (tag == item)
                        {
                            chk.Checked = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 显示页面绑定flow控件的值
        /// </summary>
        /// <param name="type"></param>
        /// <param name="ValueCodes"></param>
        /// <returns></returns>
        protected virtual string SetFlowLayoutValues(string type, string ValueCodes)
        {
            try
            {
                string[] scode = ValueCodes.Split(new char[] { ',' });
                string rtString = "";
                foreach (string s in scode)
                    rtString += BLL.ReturnDis字典显示(type, s) + ",";
                rtString = rtString.Substring(0, rtString.Length - 1);
                return rtString;
            }
            catch
            {
                return ValueCodes;
            }
        }
        #endregion

        #region 表格明细行添加/删除

        /// <summary>
        /// 明细表格按钮的事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnEmbeddedNavigatorButtonClick(object sender, GridControl gc)
        {
            try
            {
                int e = Convert.ToInt32(sender);
                //GridControl gc = (GridControl)((GridControlNavigator)sender).Parent;
                GridView gridView = (GridView)gc.Views[0]; //每个GridControl只有一个GridView.
                if (e == DetailButtons.Add || e == DetailButtons.Insert)
                    CreateOneDetail(gridView);
                else if (e == DetailButtons.Delete)
                {
                    if (Msg.AskQuestion("真的要删除这条记录?"))
                        gridView.DeleteRow(gridView.FocusedRowHandle);
                }
                //e.Handled = true;
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }

        /// <summary>
        /// 创建一条明细数据
        /// </summary>
        /// <param name="gridView">明细表格</param>
        protected virtual void CreateOneDetail(GridView gridView) { }
        #endregion

        #region Set Editors Accessable

        /// <summary>
        /// 控制明细页面上的控件可被编辑.
        /// </summary>
        protected virtual void SetDetailEditorsAccessable(Control panel, bool value)
        {
            if (panel == null) return;

            for (int i = 0; i < panel.Controls.Count; i++)
            {
                SetControlAccessable(panel.Controls[i], value);
            }
        }

        /// <summary>
        /// 设置控件状态.ReadOnly or Enable = false/true
        /// </summary>
        protected void SetControlAccessable(Control control, bool value)
        {
            try
            {
                if (control is Label) return;
                if (control is ControlNavigator) return;
                if (control is UserControl) return;

                if (control.Controls.Count > 0)
                {
                    foreach (Control c in control.Controls)
                        SetControlAccessable(c, value);
                }

                System.Type type = control.GetType();
                PropertyInfo[] infos = type.GetProperties();
                foreach (PropertyInfo info in infos)
                {
                    if (info.Name == "ReadOnly")//ReadOnly
                    {
                        info.SetValue(control, !value, null);
                        return;
                    }
                    if (info.Name == "Properties")//Properties.ReadOnly
                    {
                        object o = info.GetValue(control, null);
                        if (o is RepositoryItem)
                        {
                            ((RepositoryItem)o).ReadOnly = !value;
                        }
                        //解决日期控件和ButtonEdit在浏览状态下也能按button的问题
                        if ((o is RepositoryItemButtonEdit) && (((RepositoryItemButtonEdit)o).Buttons.Count > 0))
                            ((RepositoryItemButtonEdit)o).Buttons[0].Enabled = value;
                        if ((o is RepositoryItemDateEdit) && (((RepositoryItemDateEdit)o).Buttons.Count > 0))
                            ((RepositoryItemDateEdit)o).Buttons[0].Enabled = value;
                        return;
                    }
                    if (info.Name == "Views")//OptionsBehavior.Editable
                    {
                        object o = info.GetValue(control, null);
                        if (null == o) return;
                        foreach (object view in (GridControlViewCollection)o)
                        {
                            if (view is ColumnView)
                                ((ColumnView)view).OptionsBehavior.Editable = value;
                        }
                        return;
                    }

                }
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
        }

        /// <summary>
        /// 设置某个编辑控件状态.ReadOnly or Enable . (递归)循环控制
        /// </summary>
        protected void SetControlAccessableCycle(Control control, bool value)
        {
            if (control.HasChildren)
            {
                foreach (Control ctrl in control.Controls)
                {
                    //DevExpress的内部(Inner)控件
                    if (ctrl.Name == string.Empty)
                        SetControlAccessable(control, value);
                    else
                        SetControlAccessableCycle(ctrl, value);
                }
            }
            else SetControlAccessable(control, value);
        }

        #endregion

        #region 显示页面

        /// <summary>
        /// 显示页面
        /// </summary>
        /// <param name="control"></param>
        protected void ShowControl(Control control, DockStyle style = DockStyle.Fill)
        {
            //DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(AtomEHR.公共卫生.Module.个人健康.WaitForm1));
            Control parent = this.Parent;
            if (parent == null) return;
            Size size = control.Size;
            parent.Controls.Clear();
            try
            {
                //add by wjz 20151109 作用：通过“添加”、“删除”进入新画面后，允许滚动页面 ▽
                if ((parent.Parent != null) && (parent.Parent.Name == "frm个人健康"))
                {
                    个人健康.frm个人健康 grandParent = parent.Parent as 个人健康.frm个人健康;
                    grandParent.SetLayoutControlMouseWheel(control);
                }
                //add by wjz 20151109 作用：通过“添加”、“删除”进入新画面后，允许滚动页面 △

                parent.Controls.Add(control);
                if (style == DockStyle.None)
                {
                    control.Size = size;
                    int maxSize = parent.Width;
                    int minSize = control.Width;

                    if (maxSize >= minSize)
                    {
                        control.Left = (maxSize - minSize) / 2;
                    }
                }
                else
                {
                    control.Dock = style;
                }
                //DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }
            catch (Exception ex)
            {
                // DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
            }

        }
        #endregion

        #region 考核项目变色处理


        /// <summary>
        /// 仅限于LayoutControl使用的方法，此方法还能返回缺项和完整度，在保存时也可调用此方法
        /// </summary>
        /// <param name="panel">Layout</param>
        /// <param name="lab">label</param>
        protected void Set考核项颜色_(Control panel, Control lab)
        {
            if (DataDictCache.Cache.t考核项 != null)
            {
                _base考核项 = DataDictCache.Cache.t考核项.Select("新表名='" + _BLL.SummaryTableName + "'");
            }

            if (_base考核项 == null) return;
            if (panel == null) return;

            _base缺项 = 0; //初始

            if (panel is LayoutControl)
            {
                LayoutControl lac = panel as LayoutControl;
                for (int i = 0; i < lac.Items.Count; i++)
                {
                    if (lac.Items[i] is LayoutControlItem)
                        _base缺项 += SetContrlsColor(lac.Items[i] as LayoutControlItem, _base考核项);
                }

            }

            //设置考核项
            if (lab is LabelControl)
            {
                int i总项 = _base考核项.Length;//考核项.Split(new char[] { '|' }).Length;
                _base完整度 = (100 - _base缺项 * 100 / i总项);
                lab.Text = string.Format("考核项：{0}    缺项：{1}     完整度：{2} % ", i总项, _base缺项, _base完整度);
            }
        }

        protected int SetContrlsColor(LayoutControlItem control, DataRow[] 考核项)
        {
            try
            {
                //System.Type types = control.GetType();
                //if (types.Name == "LayoutControlItem")  //LayoutControlGroup
                //{
                //    //这一块暂时没用
                //}
                //string[] 验证项目 = 考核项.Split(new char[] { '|' });
                bool setRed = true;
                if (MatchCharCount(考核项, control.Text) == 1)
                {
                    //TextEdit
                    if (control.Control is TextEdit || control.Control is DateEdit)
                    {
                        if (control.Control.Text.Trim() == "") setRed = true;
                        else setRed = false;
                    }
                    //RadioGroup
                    if (control.Control is RadioGroup)
                    {
                        if (((RadioGroup)control.Control).EditValue == null || ((RadioGroup)control.Control).EditValue.ToString() == "") setRed = true;
                        else setRed = false;
                    }
                    //FlowLayoutPanel
                    if (control.Control is FlowLayoutPanel)
                    {
                        if (((FlowLayoutPanel)control.Control).Controls.Count > 0)
                        {
                            foreach (Control flow in ((FlowLayoutPanel)control.Control).Controls)
                            {
                                if (flow is CheckEdit)
                                {
                                    if (!((CheckEdit)flow).Checked) { setRed = true; }
                                    else { setRed = false; break; }
                                }
                                if (flow is TextEdit)
                                {
                                    if (((TextEdit)flow).Text.ToString() == "") { setRed = true; }
                                    else { setRed = false; break; }
                                }
                            }
                        }
                        else setRed = false;
                    }
                    //由于用户自定义控件属性无法正确判断，所以在自定义控件中添加textchanged时间给tag赋值，
                    //只要tag不是null就是已经有值所以不变红
                    if (control.Control is UserControl)
                    {
                        if (((UserControl)control.Control).Tag == null) setRed = true;
                        else setRed = false;

                        //if (((UserControl)control.Control) is Library.UserControls.UCTxtLblTxtLbl)
                        //{
                        //    if (((Library.UserControls.UCTxtLblTxtLbl)((UserControl)control.Control)).Lbl1Text == "") setRed = true;
                        //    if (((Library.UserControls.UCTxtLblTxtLbl)((UserControl)control.Control)).Lbl2Text == "") setRed = true;
                        //}
                        ////setRed = false;
                    }
                    //设置颜色
                    if (setRed)
                    {
                        control.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        return 1;
                    }
                    else
                    {
                        control.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                    }
                }
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }

            return 0;
        }

        /// <summary>
        /// 匹配字符串，用于比较
        /// </summary>
        /// <param name="data">考核项</param>
        /// <param name="match">验证项</param>
        /// <returns>1=存在，0=不存在</returns>
        private int MatchCharCount(DataRow[] data, string match)
        {
            try
            {
                foreach (DataRow dr in data)
                {
                    if (dr["考核项名称"].ToString() == match.Replace(":", "") || dr["考核项名称"].ToString() == match.Replace("：", "")) { return 1; }
                }
                //foreach (string s in data)
                //{
                //    if (s == match.Replace(":", "") || s == match.Replace("：", "")) { return 1; }
                //}              
            }
            catch { return 0; }
            return 0;
        }

        #endregion

        #region 考核项目变色处理new

        /// <summary>
        /// 仅限于LayoutControl使用的方法，此方法还能返回缺项和完整度，在保存时也可调用此方法
        /// 需要指定LayoutControlItem的tag为check，手动写入
        /// </summary>
        /// <param name="panel">Layout</param>
        /// <param name="lab">label</param>
        protected void Set考核项颜色_new(Object panel, Control lab)
        {
            if (panel == null) return;

            _base缺项 = 0; //初始
            _base总项 = 0;

            if (panel is LayoutControl)
            {
                foreach (var it in ((LayoutControl)panel).Items)
                    setLayoutControl(it);
            }

            //上移
            int i总项 = _base总项 == 0 ? 1 : _base总项;//_base考核项.Length;
            _base完整度 = (100 - _base缺项 * 100 / i总项);

            //设置显示考核项
            if (lab is LabelControl)
            {
                //int i总项 = _base总项 == 0 ? 1 : _base总项;//_base考核项.Length;
                //_base完整度 = (100 - _base缺项 * 100 / i总项);
                lab.Text = string.Format("考核项：{0}    缺项：{1}     完整度：{2} % ", i总项, _base缺项, _base完整度);
            }
        }

        private void setLayoutControl(Object obj)
        {
            if (obj is LayoutControlGroup)
            {
                foreach (var it in ((LayoutControlGroup)obj).Items)
                {
                    if (it is LayoutControlItem)
                        _base缺项 += Set指定项颜色((LayoutControlItem)it);
                    //else if (it is LayoutControlGroup) //2015-11-24 14:59:29 yufh 注释掉，不用递归，解决一个group下有多个group的问题
                    //    setLayoutControl(it);
                }
            }
        }

        private int Set指定项颜色(LayoutControlItem control)
        {
            try
            {
                if (!(control.Tag != null && control.Tag.ToString() == "check")) { return 0; }
                else { _base总项 += 1; }
                bool setRed = true;
                if (control.Control is TextEdit || control.Control is DateEdit || control.Control is LookUpEdit)
                {
                    if (control.Control.Text.Trim() == "" || control.Control.Text.Trim() == "请选择") setRed = true;
                    else setRed = false;
                }
                //RadioGroup
                else if (control.Control is RadioGroup)
                {
                    if (((RadioGroup)control.Control).EditValue == null || ((RadioGroup)control.Control).EditValue.ToString() == "") setRed = true;
                    else setRed = false;
                }
                //FlowLayoutPanel
                else if (control.Control is FlowLayoutPanel)
                {
                    if (((FlowLayoutPanel)control.Control).Controls.Count > 0)
                    {
                        foreach (Control flow in ((FlowLayoutPanel)control.Control).Controls)
                        {
                            if (flow is CheckEdit)
                            {
                                if (!((CheckEdit)flow).Checked) { setRed = true; }
                                else { setRed = false; break; }
                            }
                            else if (flow is TextEdit)
                            {
                                if (((TextEdit)flow).Text.ToString() == "") { setRed = true; }
                                else { setRed = false; break; }
                            }
                            else if (flow is ComboBoxEdit)
                            {
                                if (((ComboBoxEdit)flow).Text.ToString() == "") { setRed = true; }
                                else { setRed = false; break; }
                            }
                        }
                    }
                    else setRed = false;
                }
                //由于用户自定义控件属性无法正确判断，所以在自定义控件中添加textchanged时间给tag赋值，
                //只要tag不是null就是已经有值所以不变红
                else if (control.Control is UserControl)
                {
                    if (((UserControl)control.Control).Tag == null) setRed = true;
                    else setRed = false;
                }

                //add by wjz 2015-10-09
                else if (control.Control is Panel)
                {
                    if (((Panel)control.Control).Controls.Count > 0)
                    {
                        foreach (Control flow in ((Panel)control.Control).Controls)
                        {
                            if (flow is ComboBoxEdit)
                            {
                                if (((ComboBoxEdit)flow).Text.ToString() == "") { setRed = true; break; }
                                else { setRed = false; }
                            }
                            else if (flow is UserControl)
                            {
                                if (((UserControl)flow).Tag == null) { setRed = true; break; }
                                else { setRed = false; }
                            }
                        }
                    }
                    else setRed = false;
                }

                //设置颜色
                if (setRed)
                {
                    control.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                    return 1;
                }
                else
                {
                    control.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                }
            }
            catch (Exception ex)
            { Msg.ShowException(ex); }
            return 0;
        }

        #endregion

        #region 判断当前登陆人和创建人是否一样
        protected bool canModifyBy同一机构(string 创建人机构)
        {
            if (Loginer.CurrentUser.IsAdmin()) return true; //如果是管理员的话允许点击修改
            if (Loginer.CurrentUser.IsSubAdmin()) return true; //如果是有需要跨机构修改权限的话允许点击修改
            if (string.IsNullOrEmpty(创建人机构)) return true;
            string loginerCode = Loginer.CurrentUser.所属机构;
            if (loginerCode == 创建人机构) return true;
            else return false;
        }

        // add 删除孕产妇基本信息，由个人基本信息表代替，把个人基本信息表的编辑权限放给妇幼人员，不再是使用孕产妇所在卫生室的账号编辑。 ▽
        protected bool Check是否下属机构(string str创建人机构)
        {
            string str上级机构 = _bll机构信息.Get上级机构(str创建人机构);
            if(Loginer.CurrentUser.所属机构==str上级机构)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // add 删除孕产妇基本信息，由个人基本信息表代替，把个人基本信息表的编辑权限放给妇幼人员，不再是使用孕产妇所在卫生室的账号编辑。 △
        #endregion

        #region BMI
        /// <summary>
        /// 计算体重指数BMI
        /// </summary>
        /// <param name="s身高"></param>
        /// <param name="s体重"></param>
        /// <param name="Tex"></param>
        protected void ComputeBMI(string s身高, string s体重, TextEdit Tex)
        {
            try
            {
                Decimal 身高 = 0;
                Decimal 体重 = 0;
                if (string.IsNullOrEmpty(s身高) || string.IsNullOrEmpty(s体重)) return;
                if (Decimal.TryParse(s身高, out 身高) && Decimal.TryParse(s体重, out 体重))
                {
                    if (身高 == 0) return;
                    Tex.Text = (Math.Round(体重 / ((身高 / 100) * (身高 / 100)), 2)).ToString();
                }
            }
            catch
            {
                return;
            }
        }
        #endregion

        protected virtual void SetRadioGroupDoubleClick(RadioGroup radio)
        {
                radio.Properties.AllowMouseWheel = false;
                radio.MouseDown += radio_MouseDown;
                //radio.DoubleClick += radio_DoubleClick;
        }

        private void UserControlBase_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < this.Controls.Count; i++)
            {
                if (Controls[i] is LayoutControl)
                {
                    LayoutControl layoutControl = (LayoutControl)Controls[i];
                    for (int j = 0; j < layoutControl.Controls.Count; j++)
                    {
                        if (layoutControl.Controls[j] is ComboBoxEdit)
                        {
                            ComboBoxEdit cbo = (ComboBoxEdit)layoutControl.Controls[j];
                            cbo.Properties.AllowMouseWheel = false;
                        }
                        if (layoutControl.Controls[j] is RadioGroup)
                        {
                            RadioGroup radio = (RadioGroup)layoutControl.Controls[j];
                            //radio.Properties.AllowMouseWheel = false;
                            //radio.MouseDown += radio_MouseDown;
                            ////radio.DoubleClick += radio_DoubleClick;

                            SetRadioGroupDoubleClick(radio);
                        }
                        if (layoutControl.Controls[j] is DateEdit)
                        {
                            DateEdit edit = (DateEdit)layoutControl.Controls[j];
                            edit.Properties.AllowMouseWheel = false;
                            //edit.Properties.Mask.EditMask = "yyyy-MM-dd";
                            edit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
                            edit.Properties.MinValue = Convert.ToDateTime("1900-01-01");
                            edit.Properties.MaxValue = Convert.ToDateTime("2049-12-31");
                            edit.Properties.Mask.UseMaskAsDisplayFormat = true;
                        }
                        if (layoutControl.Controls[j] is LookUpEdit)
                        {
                            LookUpEdit edit = (LookUpEdit)layoutControl.Controls[j];
                            edit.Properties.AllowMouseWheel = false;
                        }
                        if (layoutControl.Controls[j] is FlowLayoutPanel)
                        {
                            FlowLayoutPanel flow = (FlowLayoutPanel)layoutControl.Controls[j];
                            for (int k = 0; k < flow.Controls.Count; k++)
                            {
                                if (flow.Controls[k] is ComboBoxEdit)
                                {
                                    ComboBoxEdit cbo = (ComboBoxEdit)flow.Controls[k];
                                    cbo.Properties.AllowMouseWheel = false;
                                }
                                if (flow.Controls[k] is DateEdit)
                                {
                                    DateEdit cbo = (DateEdit)flow.Controls[k];
                                    cbo.Properties.AllowMouseWheel = false;
                                }
                                if (flow.Controls[k] is RadioGroup)
                                {
                                    RadioGroup cbo = (RadioGroup)flow.Controls[k];

                                    SetRadioGroupDoubleClick(cbo);
                                    //cbo.Properties.AllowMouseWheel = false;
                                    //cbo.MouseDown += radio_MouseDown;
                                    //cbo.DoubleClick += radio_DoubleClick;
                                }
                            }
                        }
                    }
                }
                if (Controls[i] is FlowLayoutPanel)
                {
                    FlowLayoutPanel flow = (FlowLayoutPanel)Controls[i];
                    for (int j = 0; j < flow.Controls.Count; j++)
                    {
                        if (flow.Controls[j] is ComboBoxEdit)
                        {
                            ComboBoxEdit cbo = (ComboBoxEdit)flow.Controls[j];
                            cbo.Properties.AllowMouseWheel = false;
                        }
                        if (flow.Controls[j] is RadioGroup)
                        {
                            RadioGroup cbo = (RadioGroup)flow.Controls[j];
                            SetRadioGroupDoubleClick(cbo);
                            //cbo.Properties.AllowMouseWheel = false;
                            //cbo.MouseDown += radio_MouseDown;
                            
                            // cbo.DoubleClick += radio_DoubleClick;
                        }
                        if (flow.Controls[j] is DateEdit)
                        {
                            DateEdit cbo = (DateEdit)flow.Controls[j];
                            cbo.Properties.AllowMouseWheel = false;
                        }
                    }
                }

                if (Controls[i] is ComboBoxEdit)
                {
                    ComboBoxEdit cbo = (ComboBoxEdit)Controls[i];
                    cbo.Properties.AllowMouseWheel = false;
                }
                if (Controls[i] is RadioGroup)
                {
                    RadioGroup cbo = (RadioGroup)Controls[i];
                    SetRadioGroupDoubleClick(cbo);
                    //cbo.Properties.AllowMouseWheel = false;
                    //cbo.MouseDown += radio_MouseDown;
                    
                    //cbo.DoubleClick += radio_DoubleClick;
                }
                if (Controls[i] is DateEdit)
                {
                    DateEdit cbo = (DateEdit)Controls[i];
                    cbo.Properties.AllowMouseWheel = false;
                }
            }
        }

        void radio_DoubleClick(object sender, EventArgs e)
        {
            RadioGroup radio = (RadioGroup)sender;
            if (radio == null) return;
            radio.SelectedIndex = -1;
        }
        /// <summary>
        /// 双击取消选定
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void radio_MouseDown(object sender, MouseEventArgs e)
        {
            //if (e.Clicks == 2)
            //{
            //RadioGroup radio = (RadioGroup)sender;
            ////return;
            //object oldValue = radio.OldEditValue;
            //object newValue = radio.EditValue;
            //if (oldValue != null && newValue != null && !string.IsNullOrEmpty(oldValue.ToString()) && !string.IsNullOrEmpty(newValue.ToString()) && oldValue.ToString() == newValue.ToString())
            //{
            //    radio.SelectedIndex = -1;
            //}
            if (e.Clicks == 2)
            {
                RadioGroup radio = (RadioGroup)sender;
                radio.SelectedIndex = -1;
                //radio.DeselectAll();
                
            }
            //}
        }

        /// <summary>
        /// 快捷键 设置
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="keyData"></param>
        /// <returns></returns>
        protected override bool ProcessCmdKey(ref   System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
                return true;
            }
            return base.ProcessCmdKey(ref   msg, keyData);
        }

        protected static void SetRadiogroupDateeditTexteditLCI(LayoutControlItem item, Control control)
        {
            object editvalue = null;
            if (control is RadioGroup)
            {
                editvalue = ((RadioGroup)control).EditValue;
            }
            else if (control is DateEdit)
            {
                editvalue = ((DateEdit)control).EditValue;
            }
            else if (control is TextEdit)
            {
                editvalue = ((TextEdit)control).EditValue;
            }
            else
            { }

            if (editvalue == null || string.IsNullOrWhiteSpace(editvalue.ToString()))
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
        }

        //panel中全部是checkbox时有效
        protected static void SetCheckPanelLayoutControlItem(LayoutControlItem item, Panel panel)
        {
            bool SetBlue = false;
            for(int index = 0; index < panel.Controls.Count; index++)
            {
                if (panel.Controls[index] is CheckEdit)
                {
                    CheckEdit checkbox = panel.Controls[index] as CheckEdit;
                    if(checkbox.Checked)
                    {
                        SetBlue = true;
                        break;
                    }
                }
            }
            if(SetBlue)
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// 要求为文本控件，日期控件等，要求全部文本控件都填写内容
        /// </summary>
        /// <param name="item"></param>
        /// <param name="panel"></param>

        protected static void SetEditPanelControLayoutControlItem(LayoutControlItem item, ControlCollection controls)
        {
            bool setred = false;
            for (int index = 0; index < controls.Count; index++)
            {
                if (controls[index] is TextEdit)
                {
                    object obj = ((TextEdit)controls[index]).EditValue;
                    if(obj==null || string.IsNullOrWhiteSpace(obj.ToString()))
                    {
                        setred = true;
                        break;
                    }
                }
                else if (controls[index] is DateEdit)
                {
                    object obj = ((DateEdit)controls[index]).EditValue;
                    if(obj==null || string.IsNullOrWhiteSpace(obj.ToString()))
                    {
                        setred = true;
                        break;
                    }
                }
                else if (controls[index] is RadioGroup)
                {
                    object obj = ((RadioGroup)controls[index]).EditValue;
                    if (obj == null || string.IsNullOrWhiteSpace(obj.ToString()))
                    {
                        setred = true;
                        break;
                    }
                }
            }
            if(setred)
            {
                item.AppearanceItemCaption.ForeColor = Color.Red;
            }
            else
            {
                item.AppearanceItemCaption.ForeColor = Color.Blue;
            }
        }

        /// <summary>
        /// 显示手签照
        /// </summary>
        protected static void ShowImage(LayoutControlItem item, PictureEdit pe, string base64)
        {
            bool setRed = true;
            try
            {
                if (!string.IsNullOrEmpty(base64) && base64.Length > 64)
                {
                    Byte[] bitmapData = new Byte[base64.Length];
                    bitmapData = Convert.FromBase64String(base64);
                    System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);
                    pe.Image = Image.FromStream(streamBitmap);
                    setRed = false;
                    
                }
                else
                {
                    if (string.IsNullOrEmpty(base64))
                        base64 = "暂无签名";
                    Graphics g = Graphics.FromImage(new Bitmap(1, 1));
                    Font font = new Font("宋体", 9);
                    SizeF sizeF = g.MeasureString(base64, font); //测量出字体的高度和宽度  
                    Brush brush; //笔刷，颜色  
                    brush = Brushes.Black;
                    PointF pf = new PointF(0, 0);
                    Bitmap img = new Bitmap(Convert.ToInt32(sizeF.Width), Convert.ToInt32(sizeF.Height));
                    g = Graphics.FromImage(img);
                    g.DrawString(base64, font, brush, pf);
                    pe.Image = img;
                    setRed = true;
                }
            }
            catch
            {
                setRed = true;
            }

            if (item != null)
            {
                if (setRed)
                {
                    item.AppearanceItemCaption.ForeColor = Color.Red;
                }
                else
                {
                    item.AppearanceItemCaption.ForeColor = Color.Blue;
                }
            }
        }
    }
}
