﻿namespace AtomEHR.公共卫生.Module
{
    partial class UserControlBase
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFocusForSave = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFocusForSave.Location = new System.Drawing.Point(57, 51);
            this.txtFocusForSave.Name = "txtFocusForSave";
            this.txtFocusForSave.Size = new System.Drawing.Size(0, 14);
            this.txtFocusForSave.TabIndex = 123;
            // 
            // UserControlBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtFocusForSave);
            this.Name = "UserControlBase";
            this.Size = new System.Drawing.Size(115, 117);
            this.Load += new System.EventHandler(this.UserControlBase_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtFocusForSave;





    }
}
