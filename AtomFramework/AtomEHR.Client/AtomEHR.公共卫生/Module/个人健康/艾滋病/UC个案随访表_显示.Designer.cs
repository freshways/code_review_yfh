﻿namespace AtomEHR.公共卫生.Module.个人健康.艾滋病
{
    partial class UC个案随访表_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC个案随访表_显示));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn添加 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt随访人员 = new DevExpress.XtraEditors.TextEdit();
            this.txt随访实施单位 = new DevExpress.XtraEditors.TextEdit();
            this.date随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt现住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt修改人 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建人 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt修改时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt备注 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.txtCD4检测次数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.txtCD4检测结果个数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.dateCD4检测日期 = new DevExpress.XtraEditors.DateEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.txtCD4检测单位 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.txt抗病毒治疗编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio结核病检查结果 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否接受过结核病检查 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否接受免费治疗 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否淋巴结肿大 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否体重异常下降 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否容易疲劳 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否发热 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel22 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否出汗 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel21 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否咳血 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否咳嗽 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.radio是否每次与固定性伴性行为时都用安全套 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.radio是否每次性行为时都用安全套 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.txt3个月性行为人数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txt同性固定性伴人数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txt同性固定性伴已检测人数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txt异性固定性伴人数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txt异性固定性伴已检测人数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否有固定性伴侣 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txt阳性配偶卡片编号 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio配偶感染状况 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio配偶变更及性行为情况 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否有配偶 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否为母婴传播病例 = new DevExpress.XtraEditors.RadioGroup();
            this.txt阳性母亲卡片编号 = new DevExpress.XtraEditors.TextEdit();
            this.flow主要死因 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk主要死因1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk主要死因2 = new DevExpress.XtraEditors.CheckEdit();
            this.chk主要死因3 = new DevExpress.XtraEditors.CheckEdit();
            this.chk主要死因4 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否死亡 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk病毒感染者 = new DevExpress.XtraEditors.CheckEdit();
            this.chk艾滋病人 = new DevExpress.XtraEditors.CheckEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt患儿家长姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt患者姓名 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.chk以后无需随访 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio失访原因 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio是否羁押 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt随访次数 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.userControlBase1 = new AtomEHR.公共卫生.Module.UserControlBase();
            this.txt个人档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt卡片编号 = new DevExpress.XtraEditors.TextEdit();
            this.date艾滋病确诊日期 = new DevExpress.XtraEditors.DateEdit();
            this.date死亡日期 = new DevExpress.XtraEditors.DateEdit();
            this.date检测日期 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访人员.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访实施单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt现住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt修改时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).BeginInit();
            this.flowLayoutPanel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCD4检测次数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCD4检测结果个数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCD4检测日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCD4检测日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCD4检测单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt抗病毒治疗编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt所属机构.Properties)).BeginInit();
            this.flowLayoutPanel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio结核病检查结果.Properties)).BeginInit();
            this.flowLayoutPanel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否接受过结核病检查.Properties)).BeginInit();
            this.flowLayoutPanel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否接受免费治疗.Properties)).BeginInit();
            this.flowLayoutPanel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否淋巴结肿大.Properties)).BeginInit();
            this.flowLayoutPanel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否体重异常下降.Properties)).BeginInit();
            this.flowLayoutPanel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否容易疲劳.Properties)).BeginInit();
            this.flowLayoutPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否发热.Properties)).BeginInit();
            this.flowLayoutPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否出汗.Properties)).BeginInit();
            this.flowLayoutPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否咳血.Properties)).BeginInit();
            this.flowLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否咳嗽.Properties)).BeginInit();
            this.flowLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否每次与固定性伴性行为时都用安全套.Properties)).BeginInit();
            this.flowLayoutPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否每次性行为时都用安全套.Properties)).BeginInit();
            this.flowLayoutPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt3个月性行为人数.Properties)).BeginInit();
            this.flowLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt同性固定性伴人数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt同性固定性伴已检测人数.Properties)).BeginInit();
            this.flowLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt异性固定性伴人数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt异性固定性伴已检测人数.Properties)).BeginInit();
            this.flowLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否有固定性伴侣.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阳性配偶卡片编号.Properties)).BeginInit();
            this.flowLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio配偶感染状况.Properties)).BeginInit();
            this.flowLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio配偶变更及性行为情况.Properties)).BeginInit();
            this.flowLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否有配偶.Properties)).BeginInit();
            this.flowLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否为母婴传播病例.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阳性母亲卡片编号.Properties)).BeginInit();
            this.flow主要死因.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk主要死因1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk主要死因2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk主要死因3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk主要死因4.Properties)).BeginInit();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否死亡.Properties)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk病毒感染者.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk艾滋病人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt患儿家长姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt患者姓名.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk以后无需随访.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio失访原因.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否羁押.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访次数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt卡片编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date艾滋病确诊日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date艾滋病确诊日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date死亡日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date死亡日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date检测日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date检测日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Size = new System.Drawing.Size(100, 700);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn添加);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(100, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(650, 29);
            this.flowLayoutPanel1.TabIndex = 126;
            // 
            // btn添加
            // 
            this.btn添加.Image = ((System.Drawing.Image)(resources.GetObject("btn添加.Image")));
            this.btn添加.Location = new System.Drawing.Point(3, 3);
            this.btn添加.Name = "btn添加";
            this.btn添加.Size = new System.Drawing.Size(75, 23);
            this.btn添加.TabIndex = 0;
            this.btn添加.Text = "添加";
            this.btn添加.Click += new System.EventHandler(this.btn添加_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(84, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 1;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(165, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 2;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(246, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 3;
            this.btn导出.Text = "导出";
            this.btn导出.Visible = false;
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt随访人员);
            this.layoutControl1.Controls.Add(this.txt随访实施单位);
            this.layoutControl1.Controls.Add(this.date随访日期);
            this.layoutControl1.Controls.Add(this.txt现住地址);
            this.layoutControl1.Controls.Add(this.txt修改人);
            this.layoutControl1.Controls.Add(this.txt创建人);
            this.layoutControl1.Controls.Add(this.txt创建机构);
            this.layoutControl1.Controls.Add(this.txt创建时间);
            this.layoutControl1.Controls.Add(this.txt修改时间);
            this.layoutControl1.Controls.Add(this.txt备注);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel30);
            this.layoutControl1.Controls.Add(this.txt抗病毒治疗编号);
            this.layoutControl1.Controls.Add(this.txt所属机构);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel29);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel28);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel27);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel26);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel25);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel24);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel23);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel22);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel21);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel20);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel19);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel18);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel17);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel16);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel15);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel14);
            this.layoutControl1.Controls.Add(this.txt阳性配偶卡片编号);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel13);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel12);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel11);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel10);
            this.layoutControl1.Controls.Add(this.txt阳性母亲卡片编号);
            this.layoutControl1.Controls.Add(this.flow主要死因);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel8);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel7);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt患儿家长姓名);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt患者姓名);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.txt个人档案编号);
            this.layoutControl1.Controls.Add(this.txt卡片编号);
            this.layoutControl1.Controls.Add(this.date艾滋病确诊日期);
            this.layoutControl1.Controls.Add(this.date死亡日期);
            this.layoutControl1.Controls.Add(this.date检测日期);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(100, 29);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(2);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(521, 226, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(650, 671);
            this.layoutControl1.TabIndex = 127;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(118, 80);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(177, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 7;
            // 
            // txt随访人员
            // 
            this.txt随访人员.Location = new System.Drawing.Point(371, 980);
            this.txt随访人员.Name = "txt随访人员";
            this.txt随访人员.Properties.ReadOnly = true;
            this.txt随访人员.Size = new System.Drawing.Size(63, 20);
            this.txt随访人员.StyleController = this.layoutControl1;
            this.txt随访人员.TabIndex = 169;
            // 
            // txt随访实施单位
            // 
            this.txt随访实施单位.Location = new System.Drawing.Point(118, 980);
            this.txt随访实施单位.Name = "txt随访实施单位";
            this.txt随访实施单位.Properties.ReadOnly = true;
            this.txt随访实施单位.Size = new System.Drawing.Size(134, 20);
            this.txt随访实施单位.StyleController = this.layoutControl1;
            this.txt随访实施单位.TabIndex = 168;
            // 
            // date随访日期
            // 
            this.date随访日期.EditValue = null;
            this.date随访日期.Location = new System.Drawing.Point(114, 152);
            this.date随访日期.MaximumSize = new System.Drawing.Size(0, 24);
            this.date随访日期.MinimumSize = new System.Drawing.Size(0, 22);
            this.date随访日期.Name = "date随访日期";
            this.date随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date随访日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.date随访日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date随访日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.date随访日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date随访日期.Properties.ReadOnly = true;
            this.date随访日期.Size = new System.Drawing.Size(120, 20);
            this.date随访日期.StyleController = this.layoutControl1;
            this.date随访日期.TabIndex = 12;
            // 
            // txt现住地址
            // 
            this.txt现住地址.Location = new System.Drawing.Point(118, 128);
            this.txt现住地址.Name = "txt现住地址";
            this.txt现住地址.Properties.ReadOnly = true;
            this.txt现住地址.Size = new System.Drawing.Size(512, 20);
            this.txt现住地址.StyleController = this.layoutControl1;
            this.txt现住地址.TabIndex = 11;
            // 
            // txt修改人
            // 
            this.txt修改人.Location = new System.Drawing.Point(495, 1052);
            this.txt修改人.Margin = new System.Windows.Forms.Padding(2);
            this.txt修改人.Name = "txt修改人";
            this.txt修改人.Properties.ReadOnly = true;
            this.txt修改人.Size = new System.Drawing.Size(135, 20);
            this.txt修改人.StyleController = this.layoutControl1;
            this.txt修改人.TabIndex = 164;
            // 
            // txt创建人
            // 
            this.txt创建人.Location = new System.Drawing.Point(301, 1052);
            this.txt创建人.Margin = new System.Windows.Forms.Padding(2);
            this.txt创建人.Name = "txt创建人";
            this.txt创建人.Properties.ReadOnly = true;
            this.txt创建人.Size = new System.Drawing.Size(95, 20);
            this.txt创建人.StyleController = this.layoutControl1;
            this.txt创建人.TabIndex = 163;
            // 
            // txt创建机构
            // 
            this.txt创建机构.Location = new System.Drawing.Point(98, 1052);
            this.txt创建机构.Margin = new System.Windows.Forms.Padding(2);
            this.txt创建机构.Name = "txt创建机构";
            this.txt创建机构.Properties.ReadOnly = true;
            this.txt创建机构.Size = new System.Drawing.Size(104, 20);
            this.txt创建机构.StyleController = this.layoutControl1;
            this.txt创建机构.TabIndex = 162;
            // 
            // txt创建时间
            // 
            this.txt创建时间.Location = new System.Drawing.Point(98, 1028);
            this.txt创建时间.Margin = new System.Windows.Forms.Padding(2);
            this.txt创建时间.Name = "txt创建时间";
            this.txt创建时间.Properties.ReadOnly = true;
            this.txt创建时间.Size = new System.Drawing.Size(104, 20);
            this.txt创建时间.StyleController = this.layoutControl1;
            this.txt创建时间.TabIndex = 161;
            // 
            // txt修改时间
            // 
            this.txt修改时间.Location = new System.Drawing.Point(301, 1028);
            this.txt修改时间.Margin = new System.Windows.Forms.Padding(2);
            this.txt修改时间.Name = "txt修改时间";
            this.txt修改时间.Properties.ReadOnly = true;
            this.txt修改时间.Size = new System.Drawing.Size(95, 20);
            this.txt修改时间.StyleController = this.layoutControl1;
            this.txt修改时间.TabIndex = 160;
            // 
            // txt备注
            // 
            this.txt备注.Location = new System.Drawing.Point(118, 1004);
            this.txt备注.Margin = new System.Windows.Forms.Padding(2);
            this.txt备注.Name = "txt备注";
            this.txt备注.Properties.ReadOnly = true;
            this.txt备注.Size = new System.Drawing.Size(512, 20);
            this.txt备注.StyleController = this.layoutControl1;
            this.txt备注.TabIndex = 158;
            // 
            // flowLayoutPanel30
            // 
            this.flowLayoutPanel30.Controls.Add(this.labelControl20);
            this.flowLayoutPanel30.Controls.Add(this.txtCD4检测次数);
            this.flowLayoutPanel30.Controls.Add(this.labelControl21);
            this.flowLayoutPanel30.Controls.Add(this.txtCD4检测结果个数);
            this.flowLayoutPanel30.Controls.Add(this.labelControl22);
            this.flowLayoutPanel30.Controls.Add(this.labelControl23);
            this.flowLayoutPanel30.Controls.Add(this.dateCD4检测日期);
            this.flowLayoutPanel30.Controls.Add(this.labelControl24);
            this.flowLayoutPanel30.Controls.Add(this.txtCD4检测单位);
            this.flowLayoutPanel30.Controls.Add(this.labelControl25);
            this.flowLayoutPanel30.Location = new System.Drawing.Point(118, 928);
            this.flowLayoutPanel30.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel30.Name = "flowLayoutPanel30";
            this.flowLayoutPanel30.Size = new System.Drawing.Size(512, 50);
            this.flowLayoutPanel30.TabIndex = 157;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(2, 2);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(166, 14);
            this.labelControl20.TabIndex = 0;
            this.labelControl20.Text = "自上次随访以来,做过CD4+检测";
            // 
            // txtCD4检测次数
            // 
            this.txtCD4检测次数.Location = new System.Drawing.Point(172, 2);
            this.txtCD4检测次数.Margin = new System.Windows.Forms.Padding(2);
            this.txtCD4检测次数.Name = "txtCD4检测次数";
            this.txtCD4检测次数.Properties.ReadOnly = true;
            this.txtCD4检测次数.Size = new System.Drawing.Size(75, 20);
            this.txtCD4检测次数.TabIndex = 1;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(251, 2);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(155, 14);
            this.labelControl21.TabIndex = 2;
            this.labelControl21.Text = "次  (最近一次CD4+检测结果:";
            // 
            // txtCD4检测结果个数
            // 
            this.txtCD4检测结果个数.Location = new System.Drawing.Point(410, 2);
            this.txtCD4检测结果个数.Margin = new System.Windows.Forms.Padding(2);
            this.txtCD4检测结果个数.Name = "txtCD4检测结果个数";
            this.txtCD4检测结果个数.Properties.ReadOnly = true;
            this.txtCD4检测结果个数.Size = new System.Drawing.Size(75, 20);
            this.txtCD4检测结果个数.TabIndex = 3;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(2, 26);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(34, 14);
            this.labelControl22.TabIndex = 5;
            this.labelControl22.Text = "个/μl; ";
            // 
            // labelControl23
            // 
            this.labelControl23.Location = new System.Drawing.Point(40, 26);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(52, 14);
            this.labelControl23.TabIndex = 6;
            this.labelControl23.Text = "检测日期:";
            // 
            // dateCD4检测日期
            // 
            this.dateCD4检测日期.EditValue = null;
            this.dateCD4检测日期.Location = new System.Drawing.Point(96, 24);
            this.dateCD4检测日期.Margin = new System.Windows.Forms.Padding(2, 0, 2, 2);
            this.dateCD4检测日期.Name = "dateCD4检测日期";
            this.dateCD4检测日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateCD4检测日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateCD4检测日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.dateCD4检测日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateCD4检测日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateCD4检测日期.Properties.ReadOnly = true;
            this.dateCD4检测日期.Size = new System.Drawing.Size(117, 20);
            this.dateCD4检测日期.TabIndex = 4;
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(217, 26);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(60, 14);
            this.labelControl24.TabIndex = 7;
            this.labelControl24.Text = ";  检测单位";
            // 
            // txtCD4检测单位
            // 
            this.txtCD4检测单位.Location = new System.Drawing.Point(281, 24);
            this.txtCD4检测单位.Margin = new System.Windows.Forms.Padding(2, 0, 2, 2);
            this.txtCD4检测单位.Name = "txtCD4检测单位";
            this.txtCD4检测单位.Properties.ReadOnly = true;
            this.txtCD4检测单位.Size = new System.Drawing.Size(115, 20);
            this.txtCD4检测单位.TabIndex = 8;
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(400, 26);
            this.labelControl25.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(5, 14);
            this.labelControl25.TabIndex = 9;
            this.labelControl25.Text = ")";
            // 
            // txt抗病毒治疗编号
            // 
            this.txt抗病毒治疗编号.Location = new System.Drawing.Point(238, 905);
            this.txt抗病毒治疗编号.Margin = new System.Windows.Forms.Padding(2);
            this.txt抗病毒治疗编号.Name = "txt抗病毒治疗编号";
            this.txt抗病毒治疗编号.Properties.ReadOnly = true;
            this.txt抗病毒治疗编号.Size = new System.Drawing.Size(125, 20);
            this.txt抗病毒治疗编号.StyleController = this.layoutControl1;
            this.txt抗病毒治疗编号.TabIndex = 156;
            // 
            // txt所属机构
            // 
            this.txt所属机构.Location = new System.Drawing.Point(495, 1028);
            this.txt所属机构.Margin = new System.Windows.Forms.Padding(2);
            this.txt所属机构.Name = "txt所属机构";
            this.txt所属机构.Properties.ReadOnly = true;
            this.txt所属机构.Size = new System.Drawing.Size(135, 20);
            this.txt所属机构.StyleController = this.layoutControl1;
            this.txt所属机构.TabIndex = 127;
            // 
            // flowLayoutPanel29
            // 
            this.flowLayoutPanel29.Controls.Add(this.radio结核病检查结果);
            this.flowLayoutPanel29.Location = new System.Drawing.Point(113, 849);
            this.flowLayoutPanel29.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel29.MaximumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel29.MinimumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel29.Name = "flowLayoutPanel29";
            this.flowLayoutPanel29.Size = new System.Drawing.Size(517, 22);
            this.flowLayoutPanel29.TabIndex = 155;
            // 
            // radio结核病检查结果
            // 
            this.radio结核病检查结果.Location = new System.Drawing.Point(0, 0);
            this.radio结核病检查结果.Margin = new System.Windows.Forms.Padding(0);
            this.radio结核病检查结果.Name = "radio结核病检查结果";
            this.radio结核病检查结果.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "肺结核"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "肺外结核"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "未患结核"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "结果不清楚")});
            this.radio结核病检查结果.Properties.ReadOnly = true;
            this.radio结核病检查结果.Size = new System.Drawing.Size(517, 23);
            this.radio结核病检查结果.TabIndex = 0;
            // 
            // flowLayoutPanel28
            // 
            this.flowLayoutPanel28.Controls.Add(this.radio是否接受过结核病检查);
            this.flowLayoutPanel28.Controls.Add(this.labelControl18);
            this.flowLayoutPanel28.Location = new System.Drawing.Point(113, 821);
            this.flowLayoutPanel28.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel28.MaximumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel28.MinimumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel28.Name = "flowLayoutPanel28";
            this.flowLayoutPanel28.Size = new System.Drawing.Size(517, 22);
            this.flowLayoutPanel28.TabIndex = 154;
            // 
            // radio是否接受过结核病检查
            // 
            this.radio是否接受过结核病检查.EditValue = "2";
            this.radio是否接受过结核病检查.Location = new System.Drawing.Point(0, 0);
            this.radio是否接受过结核病检查.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否接受过结核病检查.Name = "radio是否接受过结核病检查";
            this.radio是否接受过结核病检查.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否接受过结核病检查.Properties.ReadOnly = true;
            this.radio是否接受过结核病检查.Size = new System.Drawing.Size(124, 22);
            this.radio是否接受过结核病检查.TabIndex = 0;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(126, 2);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(88, 14);
            this.labelControl18.TabIndex = 2;
            this.labelControl18.Text = "          如果‘‘是’’";
            // 
            // flowLayoutPanel27
            // 
            this.flowLayoutPanel27.Controls.Add(this.radio是否接受免费治疗);
            this.flowLayoutPanel27.Controls.Add(this.labelControl19);
            this.flowLayoutPanel27.Location = new System.Drawing.Point(113, 877);
            this.flowLayoutPanel27.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel27.MaximumSize = new System.Drawing.Size(0, 24);
            this.flowLayoutPanel27.MinimumSize = new System.Drawing.Size(0, 24);
            this.flowLayoutPanel27.Name = "flowLayoutPanel27";
            this.flowLayoutPanel27.Size = new System.Drawing.Size(517, 24);
            this.flowLayoutPanel27.TabIndex = 153;
            // 
            // radio是否接受免费治疗
            // 
            this.radio是否接受免费治疗.EditValue = "2";
            this.radio是否接受免费治疗.Location = new System.Drawing.Point(0, 0);
            this.radio是否接受免费治疗.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否接受免费治疗.Name = "radio是否接受免费治疗";
            this.radio是否接受免费治疗.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否接受免费治疗.Properties.ReadOnly = true;
            this.radio是否接受免费治疗.Size = new System.Drawing.Size(124, 22);
            this.radio是否接受免费治疗.TabIndex = 1;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(126, 2);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(88, 14);
            this.labelControl19.TabIndex = 3;
            this.labelControl19.Text = "          如果‘‘是’’";
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.Controls.Add(this.radio是否淋巴结肿大);
            this.flowLayoutPanel26.Location = new System.Drawing.Point(278, 793);
            this.flowLayoutPanel26.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(90, 24);
            this.flowLayoutPanel26.TabIndex = 152;
            // 
            // radio是否淋巴结肿大
            // 
            this.radio是否淋巴结肿大.EditValue = "2";
            this.radio是否淋巴结肿大.Location = new System.Drawing.Point(0, 0);
            this.radio是否淋巴结肿大.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否淋巴结肿大.Name = "radio是否淋巴结肿大";
            this.radio是否淋巴结肿大.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否淋巴结肿大.Properties.ReadOnly = true;
            this.radio是否淋巴结肿大.Size = new System.Drawing.Size(75, 20);
            this.radio是否淋巴结肿大.TabIndex = 1;
            // 
            // flowLayoutPanel25
            // 
            this.flowLayoutPanel25.Controls.Add(this.radio是否体重异常下降);
            this.flowLayoutPanel25.Location = new System.Drawing.Point(278, 765);
            this.flowLayoutPanel25.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            this.flowLayoutPanel25.Size = new System.Drawing.Size(90, 24);
            this.flowLayoutPanel25.TabIndex = 151;
            // 
            // radio是否体重异常下降
            // 
            this.radio是否体重异常下降.EditValue = "2";
            this.radio是否体重异常下降.Location = new System.Drawing.Point(0, 0);
            this.radio是否体重异常下降.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否体重异常下降.Name = "radio是否体重异常下降";
            this.radio是否体重异常下降.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否体重异常下降.Properties.ReadOnly = true;
            this.radio是否体重异常下降.Size = new System.Drawing.Size(75, 20);
            this.radio是否体重异常下降.TabIndex = 1;
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.Controls.Add(this.radio是否容易疲劳);
            this.flowLayoutPanel24.Location = new System.Drawing.Point(537, 765);
            this.flowLayoutPanel24.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(93, 24);
            this.flowLayoutPanel24.TabIndex = 150;
            // 
            // radio是否容易疲劳
            // 
            this.radio是否容易疲劳.EditValue = "2";
            this.radio是否容易疲劳.Location = new System.Drawing.Point(0, 0);
            this.radio是否容易疲劳.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否容易疲劳.Name = "radio是否容易疲劳";
            this.radio是否容易疲劳.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否容易疲劳.Properties.ReadOnly = true;
            this.radio是否容易疲劳.Size = new System.Drawing.Size(75, 20);
            this.radio是否容易疲劳.TabIndex = 1;
            // 
            // flowLayoutPanel23
            // 
            this.flowLayoutPanel23.Controls.Add(this.radio是否发热);
            this.flowLayoutPanel23.Location = new System.Drawing.Point(278, 737);
            this.flowLayoutPanel23.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel23.Name = "flowLayoutPanel23";
            this.flowLayoutPanel23.Size = new System.Drawing.Size(90, 24);
            this.flowLayoutPanel23.TabIndex = 149;
            // 
            // radio是否发热
            // 
            this.radio是否发热.EditValue = "2";
            this.radio是否发热.Location = new System.Drawing.Point(0, 0);
            this.radio是否发热.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否发热.Name = "radio是否发热";
            this.radio是否发热.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否发热.Properties.ReadOnly = true;
            this.radio是否发热.Size = new System.Drawing.Size(75, 20);
            this.radio是否发热.TabIndex = 1;
            // 
            // flowLayoutPanel22
            // 
            this.flowLayoutPanel22.Controls.Add(this.radio是否出汗);
            this.flowLayoutPanel22.Location = new System.Drawing.Point(537, 737);
            this.flowLayoutPanel22.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel22.Name = "flowLayoutPanel22";
            this.flowLayoutPanel22.Size = new System.Drawing.Size(93, 24);
            this.flowLayoutPanel22.TabIndex = 148;
            // 
            // radio是否出汗
            // 
            this.radio是否出汗.EditValue = "2";
            this.radio是否出汗.Location = new System.Drawing.Point(0, 0);
            this.radio是否出汗.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否出汗.Name = "radio是否出汗";
            this.radio是否出汗.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否出汗.Properties.ReadOnly = true;
            this.radio是否出汗.Size = new System.Drawing.Size(75, 20);
            this.radio是否出汗.TabIndex = 1;
            // 
            // flowLayoutPanel21
            // 
            this.flowLayoutPanel21.Controls.Add(this.radio是否咳血);
            this.flowLayoutPanel21.Location = new System.Drawing.Point(537, 709);
            this.flowLayoutPanel21.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel21.Name = "flowLayoutPanel21";
            this.flowLayoutPanel21.Size = new System.Drawing.Size(93, 24);
            this.flowLayoutPanel21.TabIndex = 147;
            // 
            // radio是否咳血
            // 
            this.radio是否咳血.EditValue = "2";
            this.radio是否咳血.Location = new System.Drawing.Point(0, 0);
            this.radio是否咳血.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否咳血.Name = "radio是否咳血";
            this.radio是否咳血.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否咳血.Properties.ReadOnly = true;
            this.radio是否咳血.Size = new System.Drawing.Size(75, 20);
            this.radio是否咳血.TabIndex = 1;
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.Controls.Add(this.radio是否咳嗽);
            this.flowLayoutPanel20.Location = new System.Drawing.Point(278, 709);
            this.flowLayoutPanel20.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(90, 24);
            this.flowLayoutPanel20.TabIndex = 146;
            // 
            // radio是否咳嗽
            // 
            this.radio是否咳嗽.EditValue = "2";
            this.radio是否咳嗽.Location = new System.Drawing.Point(0, 0);
            this.radio是否咳嗽.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否咳嗽.Name = "radio是否咳嗽";
            this.radio是否咳嗽.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否咳嗽.Properties.ReadOnly = true;
            this.radio是否咳嗽.Size = new System.Drawing.Size(75, 20);
            this.radio是否咳嗽.TabIndex = 0;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.Controls.Add(this.labelControl17);
            this.flowLayoutPanel19.Controls.Add(this.radio是否每次与固定性伴性行为时都用安全套);
            this.flowLayoutPanel19.Location = new System.Drawing.Point(83, 658);
            this.flowLayoutPanel19.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel19.MaximumSize = new System.Drawing.Size(0, 45);
            this.flowLayoutPanel19.MinimumSize = new System.Drawing.Size(0, 45);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(547, 45);
            this.flowLayoutPanel19.TabIndex = 145;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(2, 2);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(332, 14);
            this.labelControl17.TabIndex = 0;
            this.labelControl17.Text = "过去3个月,是否每次与配偶/固定性伴发生性行为时都用安全套:";
            // 
            // radio是否每次与固定性伴性行为时都用安全套
            // 
            this.radio是否每次与固定性伴性行为时都用安全套.Location = new System.Drawing.Point(0, 18);
            this.radio是否每次与固定性伴性行为时都用安全套.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否每次与固定性伴性行为时都用安全套.Name = "radio是否每次与固定性伴性行为时都用安全套";
            this.radio是否每次与固定性伴性行为时都用安全套.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "未与配偶/固定性伴发生性行为")});
            this.radio是否每次与固定性伴性行为时都用安全套.Properties.ReadOnly = true;
            this.radio是否每次与固定性伴性行为时都用安全套.Size = new System.Drawing.Size(547, 23);
            this.radio是否每次与固定性伴性行为时都用安全套.TabIndex = 1;
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.Controls.Add(this.labelControl16);
            this.flowLayoutPanel18.Controls.Add(this.radio是否每次性行为时都用安全套);
            this.flowLayoutPanel18.Location = new System.Drawing.Point(83, 632);
            this.flowLayoutPanel18.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel18.MaximumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel18.MinimumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(547, 22);
            this.flowLayoutPanel18.TabIndex = 144;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(2, 2);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(243, 14);
            this.labelControl16.TabIndex = 0;
            this.labelControl16.Text = "过去3个月,是否每次发生性行为时都用安全套:";
            // 
            // radio是否每次性行为时都用安全套
            // 
            this.radio是否每次性行为时都用安全套.Location = new System.Drawing.Point(247, 0);
            this.radio是否每次性行为时都用安全套.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否每次性行为时都用安全套.Name = "radio是否每次性行为时都用安全套";
            this.radio是否每次性行为时都用安全套.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否每次性行为时都用安全套.Properties.ReadOnly = true;
            this.radio是否每次性行为时都用安全套.Size = new System.Drawing.Size(113, 22);
            this.radio是否每次性行为时都用安全套.TabIndex = 1;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.Controls.Add(this.labelControl15);
            this.flowLayoutPanel17.Controls.Add(this.txt3个月性行为人数);
            this.flowLayoutPanel17.Controls.Add(this.labelControl14);
            this.flowLayoutPanel17.Location = new System.Drawing.Point(83, 606);
            this.flowLayoutPanel17.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel17.MaximumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel17.MinimumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(547, 22);
            this.flowLayoutPanel17.TabIndex = 143;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(2, 2);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(71, 14);
            this.labelControl15.TabIndex = 1;
            this.labelControl15.Text = "过去3个月,有";
            // 
            // txt3个月性行为人数
            // 
            this.txt3个月性行为人数.Enabled = false;
            this.txt3个月性行为人数.Location = new System.Drawing.Point(77, 2);
            this.txt3个月性行为人数.Margin = new System.Windows.Forms.Padding(2);
            this.txt3个月性行为人数.Name = "txt3个月性行为人数";
            this.txt3个月性行为人数.Properties.ReadOnly = true;
            this.txt3个月性行为人数.Size = new System.Drawing.Size(75, 20);
            this.txt3个月性行为人数.TabIndex = 3;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(156, 2);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(102, 14);
            this.labelControl14.TabIndex = 2;
            this.labelControl14.Text = "人与您有过性行为?";
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.Controls.Add(this.labelControl8);
            this.flowLayoutPanel16.Controls.Add(this.txt同性固定性伴人数);
            this.flowLayoutPanel16.Controls.Add(this.labelControl9);
            this.flowLayoutPanel16.Controls.Add(this.txt同性固定性伴已检测人数);
            this.flowLayoutPanel16.Controls.Add(this.labelControl10);
            this.flowLayoutPanel16.Location = new System.Drawing.Point(83, 580);
            this.flowLayoutPanel16.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel16.MaximumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel16.MinimumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(547, 22);
            this.flowLayoutPanel16.TabIndex = 142;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(2, 2);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(88, 14);
            this.labelControl8.TabIndex = 0;
            this.labelControl8.Text = "同性固定性伴,有";
            // 
            // txt同性固定性伴人数
            // 
            this.txt同性固定性伴人数.Enabled = false;
            this.txt同性固定性伴人数.Location = new System.Drawing.Point(94, 2);
            this.txt同性固定性伴人数.Margin = new System.Windows.Forms.Padding(2);
            this.txt同性固定性伴人数.Name = "txt同性固定性伴人数";
            this.txt同性固定性伴人数.Properties.ReadOnly = true;
            this.txt同性固定性伴人数.Size = new System.Drawing.Size(75, 20);
            this.txt同性固定性伴人数.TabIndex = 3;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(173, 2);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(52, 14);
            this.labelControl9.TabIndex = 1;
            this.labelControl9.Text = "个,本年内";
            // 
            // txt同性固定性伴已检测人数
            // 
            this.txt同性固定性伴已检测人数.Enabled = false;
            this.txt同性固定性伴已检测人数.Location = new System.Drawing.Point(229, 2);
            this.txt同性固定性伴已检测人数.Margin = new System.Windows.Forms.Padding(2);
            this.txt同性固定性伴已检测人数.Name = "txt同性固定性伴已检测人数";
            this.txt同性固定性伴已检测人数.Properties.ReadOnly = true;
            this.txt同性固定性伴已检测人数.Size = new System.Drawing.Size(75, 20);
            this.txt同性固定性伴已检测人数.TabIndex = 4;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(308, 2);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(52, 14);
            this.labelControl10.TabIndex = 2;
            this.labelControl10.Text = "人已检测.";
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.Controls.Add(this.labelControl6);
            this.flowLayoutPanel15.Controls.Add(this.txt异性固定性伴人数);
            this.flowLayoutPanel15.Controls.Add(this.labelControl11);
            this.flowLayoutPanel15.Controls.Add(this.txt异性固定性伴已检测人数);
            this.flowLayoutPanel15.Controls.Add(this.labelControl12);
            this.flowLayoutPanel15.Location = new System.Drawing.Point(83, 552);
            this.flowLayoutPanel15.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel15.MaximumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel15.MinimumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(547, 22);
            this.flowLayoutPanel15.TabIndex = 141;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(2, 2);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(88, 14);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "异性固定性伴,有";
            // 
            // txt异性固定性伴人数
            // 
            this.txt异性固定性伴人数.Enabled = false;
            this.txt异性固定性伴人数.Location = new System.Drawing.Point(94, 2);
            this.txt异性固定性伴人数.Margin = new System.Windows.Forms.Padding(2);
            this.txt异性固定性伴人数.Name = "txt异性固定性伴人数";
            this.txt异性固定性伴人数.Properties.ReadOnly = true;
            this.txt异性固定性伴人数.Size = new System.Drawing.Size(75, 20);
            this.txt异性固定性伴人数.TabIndex = 3;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(173, 2);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(52, 14);
            this.labelControl11.TabIndex = 1;
            this.labelControl11.Text = "个,本年内";
            // 
            // txt异性固定性伴已检测人数
            // 
            this.txt异性固定性伴已检测人数.Enabled = false;
            this.txt异性固定性伴已检测人数.Location = new System.Drawing.Point(229, 2);
            this.txt异性固定性伴已检测人数.Margin = new System.Windows.Forms.Padding(2);
            this.txt异性固定性伴已检测人数.Name = "txt异性固定性伴已检测人数";
            this.txt异性固定性伴已检测人数.Properties.ReadOnly = true;
            this.txt异性固定性伴已检测人数.Size = new System.Drawing.Size(75, 20);
            this.txt异性固定性伴已检测人数.TabIndex = 4;
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(308, 2);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(52, 14);
            this.labelControl12.TabIndex = 2;
            this.labelControl12.Text = "人已检测;";
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.Controls.Add(this.radio是否有固定性伴侣);
            this.flowLayoutPanel14.Controls.Add(this.labelControl7);
            this.flowLayoutPanel14.Location = new System.Drawing.Point(83, 524);
            this.flowLayoutPanel14.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel14.MaximumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel14.MinimumSize = new System.Drawing.Size(0, 22);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(547, 22);
            this.flowLayoutPanel14.TabIndex = 140;
            // 
            // radio是否有固定性伴侣
            // 
            this.radio是否有固定性伴侣.EditValue = "2";
            this.radio是否有固定性伴侣.Location = new System.Drawing.Point(0, 0);
            this.radio是否有固定性伴侣.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否有固定性伴侣.Name = "radio是否有固定性伴侣";
            this.radio是否有固定性伴侣.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否有固定性伴侣.Properties.ReadOnly = true;
            this.radio是否有固定性伴侣.Size = new System.Drawing.Size(124, 22);
            this.radio是否有固定性伴侣.TabIndex = 2;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(126, 2);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(112, 14);
            this.labelControl7.TabIndex = 1;
            this.labelControl7.Text = "          如果回答‘‘是’’";
            // 
            // txt阳性配偶卡片编号
            // 
            this.txt阳性配偶卡片编号.Location = new System.Drawing.Point(512, 500);
            this.txt阳性配偶卡片编号.Margin = new System.Windows.Forms.Padding(2);
            this.txt阳性配偶卡片编号.Name = "txt阳性配偶卡片编号";
            this.txt阳性配偶卡片编号.Properties.ReadOnly = true;
            this.txt阳性配偶卡片编号.Size = new System.Drawing.Size(118, 20);
            this.txt阳性配偶卡片编号.StyleController = this.layoutControl1;
            this.txt阳性配偶卡片编号.TabIndex = 139;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.Controls.Add(this.radio配偶感染状况);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(213, 472);
            this.flowLayoutPanel13.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(417, 24);
            this.flowLayoutPanel13.TabIndex = 138;
            // 
            // radio配偶感染状况
            // 
            this.radio配偶感染状况.Location = new System.Drawing.Point(0, 0);
            this.radio配偶感染状况.Margin = new System.Windows.Forms.Padding(0);
            this.radio配偶感染状况.Name = "radio配偶感染状况";
            this.radio配偶感染状况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "未查/不祥"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "阴性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "阳性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "检测结果不确定")});
            this.radio配偶感染状况.Properties.ReadOnly = true;
            this.radio配偶感染状况.Size = new System.Drawing.Size(417, 26);
            this.radio配偶感染状况.TabIndex = 0;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.Controls.Add(this.radio配偶变更及性行为情况);
            this.flowLayoutPanel12.Location = new System.Drawing.Point(238, 422);
            this.flowLayoutPanel12.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(392, 46);
            this.flowLayoutPanel12.TabIndex = 137;
            // 
            // radio配偶变更及性行为情况
            // 
            this.radio配偶变更及性行为情况.Location = new System.Drawing.Point(0, 0);
            this.radio配偶变更及性行为情况.Margin = new System.Windows.Forms.Padding(0);
            this.radio配偶变更及性行为情况.Name = "radio配偶变更及性行为情况";
            this.radio配偶变更及性行为情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "配偶未变更,有性行为"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "配偶未变更,无性行为"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "配偶发生变更,有性行为"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "配偶发生变更,无性行为")});
            this.radio配偶变更及性行为情况.Properties.ReadOnly = true;
            this.radio配偶变更及性行为情况.Size = new System.Drawing.Size(392, 42);
            this.radio配偶变更及性行为情况.TabIndex = 0;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.radio是否有配偶);
            this.flowLayoutPanel11.Controls.Add(this.labelControl13);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(113, 394);
            this.flowLayoutPanel11.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(517, 24);
            this.flowLayoutPanel11.TabIndex = 136;
            // 
            // radio是否有配偶
            // 
            this.radio是否有配偶.EditValue = "2";
            this.radio是否有配偶.Location = new System.Drawing.Point(5, 0);
            this.radio是否有配偶.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.radio是否有配偶.Name = "radio是否有配偶";
            this.radio是否有配偶.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否有配偶.Properties.ReadOnly = true;
            this.radio是否有配偶.Size = new System.Drawing.Size(119, 22);
            this.radio是否有配偶.TabIndex = 3;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(126, 5);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(112, 14);
            this.labelControl13.TabIndex = 2;
            this.labelControl13.Text = "          如果回答‘‘是’’";
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.radio是否为母婴传播病例);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(118, 370);
            this.flowLayoutPanel10.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(117, 20);
            this.flowLayoutPanel10.TabIndex = 135;
            // 
            // radio是否为母婴传播病例
            // 
            this.radio是否为母婴传播病例.EditValue = "2";
            this.radio是否为母婴传播病例.Location = new System.Drawing.Point(0, 0);
            this.radio是否为母婴传播病例.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否为母婴传播病例.Name = "radio是否为母婴传播病例";
            this.radio是否为母婴传播病例.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否为母婴传播病例.Properties.ReadOnly = true;
            this.radio是否为母婴传播病例.Size = new System.Drawing.Size(119, 19);
            this.radio是否为母婴传播病例.TabIndex = 0;
            // 
            // txt阳性母亲卡片编号
            // 
            this.txt阳性母亲卡片编号.Location = new System.Drawing.Point(350, 370);
            this.txt阳性母亲卡片编号.Margin = new System.Windows.Forms.Padding(2);
            this.txt阳性母亲卡片编号.MaximumSize = new System.Drawing.Size(200, 0);
            this.txt阳性母亲卡片编号.MinimumSize = new System.Drawing.Size(200, 0);
            this.txt阳性母亲卡片编号.Name = "txt阳性母亲卡片编号";
            this.txt阳性母亲卡片编号.Properties.ReadOnly = true;
            this.txt阳性母亲卡片编号.Size = new System.Drawing.Size(200, 20);
            this.txt阳性母亲卡片编号.StyleController = this.layoutControl1;
            this.txt阳性母亲卡片编号.TabIndex = 133;
            // 
            // flow主要死因
            // 
            this.flow主要死因.Controls.Add(this.chk主要死因1);
            this.flow主要死因.Controls.Add(this.chk主要死因2);
            this.flow主要死因.Controls.Add(this.chk主要死因3);
            this.flow主要死因.Controls.Add(this.chk主要死因4);
            this.flow主要死因.Controls.Add(this.labelControl5);
            this.flow主要死因.Location = new System.Drawing.Point(118, 310);
            this.flow主要死因.Margin = new System.Windows.Forms.Padding(2);
            this.flow主要死因.Name = "flow主要死因";
            this.flow主要死因.Size = new System.Drawing.Size(512, 56);
            this.flow主要死因.TabIndex = 132;
            // 
            // chk主要死因1
            // 
            this.chk主要死因1.Location = new System.Drawing.Point(2, 2);
            this.chk主要死因1.Margin = new System.Windows.Forms.Padding(2);
            this.chk主要死因1.Name = "chk主要死因1";
            this.chk主要死因1.Properties.Caption = "艾滋病相关疾病死亡";
            this.chk主要死因1.Properties.ReadOnly = true;
            this.chk主要死因1.Size = new System.Drawing.Size(129, 19);
            this.chk主要死因1.TabIndex = 0;
            // 
            // chk主要死因2
            // 
            this.chk主要死因2.Location = new System.Drawing.Point(135, 2);
            this.chk主要死因2.Margin = new System.Windows.Forms.Padding(2);
            this.chk主要死因2.Name = "chk主要死因2";
            this.chk主要死因2.Properties.Caption = "艾滋病无关疾病死亡";
            this.chk主要死因2.Properties.ReadOnly = true;
            this.chk主要死因2.Size = new System.Drawing.Size(131, 19);
            this.chk主要死因2.TabIndex = 1;
            // 
            // chk主要死因3
            // 
            this.chk主要死因3.Location = new System.Drawing.Point(2, 25);
            this.chk主要死因3.Margin = new System.Windows.Forms.Padding(2);
            this.chk主要死因3.Name = "chk主要死因3";
            this.chk主要死因3.Properties.Caption = "非疾病外因死亡(自杀、吸毒过量、损伤等)";
            this.chk主要死因3.Properties.ReadOnly = true;
            this.chk主要死因3.Size = new System.Drawing.Size(261, 19);
            this.chk主要死因3.TabIndex = 2;
            // 
            // chk主要死因4
            // 
            this.chk主要死因4.Location = new System.Drawing.Point(267, 25);
            this.chk主要死因4.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.chk主要死因4.Name = "chk主要死因4";
            this.chk主要死因4.Properties.Caption = "无法判定";
            this.chk主要死因4.Properties.ReadOnly = true;
            this.chk主要死因4.Size = new System.Drawing.Size(76, 19);
            this.chk主要死因4.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(345, 29);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(2, 6, 2, 2);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(144, 14);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "（死亡个案随访到此结束）";
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.radio是否死亡);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(118, 286);
            this.flowLayoutPanel8.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(236, 20);
            this.flowLayoutPanel8.TabIndex = 131;
            // 
            // radio是否死亡
            // 
            this.radio是否死亡.EditValue = "2";
            this.radio是否死亡.Location = new System.Drawing.Point(0, 0);
            this.radio是否死亡.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否死亡.Name = "radio是否死亡";
            this.radio是否死亡.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否死亡.Properties.ReadOnly = true;
            this.radio是否死亡.Size = new System.Drawing.Size(119, 20);
            this.radio是否死亡.TabIndex = 0;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.chk病毒感染者);
            this.flowLayoutPanel7.Controls.Add(this.chk艾滋病人);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(118, 262);
            this.flowLayoutPanel7.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(236, 20);
            this.flowLayoutPanel7.TabIndex = 130;
            // 
            // chk病毒感染者
            // 
            this.chk病毒感染者.Location = new System.Drawing.Point(2, 2);
            this.chk病毒感染者.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.chk病毒感染者.Name = "chk病毒感染者";
            this.chk病毒感染者.Properties.Caption = "艾滋病病毒感染者";
            this.chk病毒感染者.Properties.ReadOnly = true;
            this.chk病毒感染者.Size = new System.Drawing.Size(129, 19);
            this.chk病毒感染者.TabIndex = 0;
            // 
            // chk艾滋病人
            // 
            this.chk艾滋病人.Location = new System.Drawing.Point(133, 2);
            this.chk艾滋病人.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.chk艾滋病人.Name = "chk艾滋病人";
            this.chk艾滋病人.Properties.Caption = "艾滋病病人";
            this.chk艾滋病人.Properties.ReadOnly = true;
            this.chk艾滋病人.Size = new System.Drawing.Size(85, 19);
            this.chk艾滋病人.TabIndex = 1;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(118, 104);
            this.txt联系电话.Margin = new System.Windows.Forms.Padding(2);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(177, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 9;
            // 
            // txt患儿家长姓名
            // 
            this.txt患儿家长姓名.Location = new System.Drawing.Point(414, 104);
            this.txt患儿家长姓名.Margin = new System.Windows.Forms.Padding(2);
            this.txt患儿家长姓名.Name = "txt患儿家长姓名";
            this.txt患儿家长姓名.Properties.ReadOnly = true;
            this.txt患儿家长姓名.Size = new System.Drawing.Size(216, 20);
            this.txt患儿家长姓名.StyleController = this.layoutControl1;
            this.txt患儿家长姓名.TabIndex = 10;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(414, 80);
            this.txt身份证号.Margin = new System.Windows.Forms.Padding(2);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(216, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 8;
            // 
            // txt患者姓名
            // 
            this.txt患者姓名.Location = new System.Drawing.Point(414, 56);
            this.txt患者姓名.Margin = new System.Windows.Forms.Padding(2);
            this.txt患者姓名.Name = "txt患者姓名";
            this.txt患者姓名.Properties.ReadOnly = true;
            this.txt患者姓名.Size = new System.Drawing.Size(216, 20);
            this.txt患者姓名.StyleController = this.layoutControl1;
            this.txt患者姓名.TabIndex = 6;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.labelControl3);
            this.flowLayoutPanel5.Controls.Add(this.chk以后无需随访);
            this.flowLayoutPanel5.Controls.Add(this.labelControl4);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(184, 234);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Padding = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel5.Size = new System.Drawing.Size(446, 24);
            this.flowLayoutPanel5.TabIndex = 12;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(3, 3);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(5, 14);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "(";
            // 
            // chk以后无需随访
            // 
            this.chk以后无需随访.Location = new System.Drawing.Point(10, 1);
            this.chk以后无需随访.Margin = new System.Windows.Forms.Padding(0);
            this.chk以后无需随访.Name = "chk以后无需随访";
            this.chk以后无需随访.Properties.Caption = "以后无需随访";
            this.chk以后无需随访.Properties.ReadOnly = true;
            this.chk以后无需随访.Size = new System.Drawing.Size(105, 19);
            this.chk以后无需随访.TabIndex = 0;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(117, 3);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(2);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(5, 14);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = ")";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.radio失访原因);
            this.flowLayoutPanel4.Controls.Add(this.labelControl2);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(184, 206);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(446, 24);
            this.flowLayoutPanel4.TabIndex = 11;
            // 
            // radio失访原因
            // 
            this.radio失访原因.Location = new System.Drawing.Point(0, 0);
            this.radio失访原因.Margin = new System.Windows.Forms.Padding(0);
            this.radio失访原因.Name = "radio失访原因";
            this.radio失访原因.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "外出"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "拒绝随访"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "羁押"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "转入时地址不祥")});
            this.radio失访原因.Properties.ReadOnly = true;
            this.radio失访原因.Size = new System.Drawing.Size(446, 24);
            this.radio失访原因.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(2, 28);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(2, 4, 2, 2);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(77, 14);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "此次随访结束)";
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.radio是否羁押);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(350, 178);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(280, 24);
            this.flowLayoutPanel3.TabIndex = 10;
            // 
            // radio是否羁押
            // 
            this.radio是否羁押.EditValue = "2";
            this.radio是否羁押.Location = new System.Drawing.Point(0, 0);
            this.radio是否羁押.Margin = new System.Windows.Forms.Padding(0);
            this.radio是否羁押.Name = "radio是否羁押";
            this.radio是否羁押.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio是否羁押.Properties.ReadOnly = true;
            this.radio是否羁押.Size = new System.Drawing.Size(93, 26);
            this.radio是否羁押.TabIndex = 14;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.txt随访次数);
            this.flowLayoutPanel2.Controls.Add(this.labelControl1);
            this.flowLayoutPanel2.Controls.Add(this.userControlBase1);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(184, 178);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(85, 24);
            this.flowLayoutPanel2.TabIndex = 9;
            // 
            // txt随访次数
            // 
            this.txt随访次数.Location = new System.Drawing.Point(0, 3);
            this.txt随访次数.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.txt随访次数.Name = "txt随访次数";
            this.txt随访次数.Properties.ReadOnly = true;
            this.txt随访次数.Size = new System.Drawing.Size(53, 20);
            this.txt随访次数.TabIndex = 13;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.labelControl1.Location = new System.Drawing.Point(55, 5);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(17, 14);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "次)";
            // 
            // userControlBase1
            // 
            this.userControlBase1.Location = new System.Drawing.Point(3, 26);
            this.userControlBase1.MaximumSize = new System.Drawing.Size(68, 72);
            this.userControlBase1.MinimumSize = new System.Drawing.Size(68, 72);
            this.userControlBase1.Name = "userControlBase1";
            this.userControlBase1.Size = new System.Drawing.Size(68, 72);
            this.userControlBase1.TabIndex = 2;
            // 
            // txt个人档案编号
            // 
            this.txt个人档案编号.Location = new System.Drawing.Point(118, 56);
            this.txt个人档案编号.Margin = new System.Windows.Forms.Padding(2);
            this.txt个人档案编号.Name = "txt个人档案编号";
            this.txt个人档案编号.Properties.ReadOnly = true;
            this.txt个人档案编号.Size = new System.Drawing.Size(177, 20);
            this.txt个人档案编号.StyleController = this.layoutControl1;
            this.txt个人档案编号.TabIndex = 5;
            // 
            // txt卡片编号
            // 
            this.txt卡片编号.Location = new System.Drawing.Point(118, 30);
            this.txt卡片编号.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txt卡片编号.MaximumSize = new System.Drawing.Size(170, 26);
            this.txt卡片编号.MinimumSize = new System.Drawing.Size(0, 20);
            this.txt卡片编号.Name = "txt卡片编号";
            this.txt卡片编号.Properties.ReadOnly = true;
            this.txt卡片编号.Size = new System.Drawing.Size(170, 20);
            this.txt卡片编号.StyleController = this.layoutControl1;
            this.txt卡片编号.TabIndex = 4;
            // 
            // date艾滋病确诊日期
            // 
            this.date艾滋病确诊日期.EditValue = null;
            this.date艾滋病确诊日期.Location = new System.Drawing.Point(469, 262);
            this.date艾滋病确诊日期.Margin = new System.Windows.Forms.Padding(2);
            this.date艾滋病确诊日期.Name = "date艾滋病确诊日期";
            this.date艾滋病确诊日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date艾滋病确诊日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date艾滋病确诊日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.date艾滋病确诊日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date艾滋病确诊日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.date艾滋病确诊日期.Properties.ReadOnly = true;
            this.date艾滋病确诊日期.Size = new System.Drawing.Size(103, 20);
            this.date艾滋病确诊日期.StyleController = this.layoutControl1;
            this.date艾滋病确诊日期.TabIndex = 127;
            // 
            // date死亡日期
            // 
            this.date死亡日期.EditValue = null;
            this.date死亡日期.Location = new System.Drawing.Point(469, 286);
            this.date死亡日期.Margin = new System.Windows.Forms.Padding(2);
            this.date死亡日期.Name = "date死亡日期";
            this.date死亡日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date死亡日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date死亡日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.date死亡日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date死亡日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.date死亡日期.Properties.ReadOnly = true;
            this.date死亡日期.Size = new System.Drawing.Size(104, 20);
            this.date死亡日期.StyleController = this.layoutControl1;
            this.date死亡日期.TabIndex = 129;
            // 
            // date检测日期
            // 
            this.date检测日期.EditValue = null;
            this.date检测日期.Location = new System.Drawing.Point(218, 500);
            this.date检测日期.Margin = new System.Windows.Forms.Padding(2);
            this.date检测日期.Name = "date检测日期";
            this.date检测日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date检测日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date检测日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.date检测日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.date检测日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.date检测日期.Properties.ReadOnly = true;
            this.date检测日期.Size = new System.Drawing.Size(81, 20);
            this.date检测日期.StyleController = this.layoutControl1;
            this.date检测日期.TabIndex = 127;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem20,
            this.layoutControlItem23,
            this.layoutControlItem27,
            this.emptySpaceItem7,
            this.layoutControlItem29,
            this.layoutControlItem28,
            this.emptySpaceItem8,
            this.layoutControlItem24,
            this.layoutControlItem21,
            this.layoutControlItem25,
            this.layoutControlItem19,
            this.layoutControlItem26,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.emptySpaceItem6,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem22,
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem40,
            this.emptySpaceItem9,
            this.layoutControlItem41,
            this.emptySpaceItem10,
            this.layoutControlItem42,
            this.emptySpaceItem11,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem47,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.layoutControlItem46,
            this.layoutControlItem9,
            this.layoutControlItem49,
            this.layoutControlItem48,
            this.layoutControlItem50,
            this.layoutControlItem45,
            this.layoutControlItem51,
            this.layoutControlItem52,
            this.layoutControlItem1,
            this.layoutControlItem12,
            this.layoutControlItem10,
            this.layoutControlItem13,
            this.emptySpaceItem5,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem11,
            this.layoutControlItem8});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(633, 1075);
            this.layoutControlGroup1.Text = "个案随访表";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(235, 122);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(396, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emptySpaceItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 148);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(110, 84);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(110, 84);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(110, 84);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "随访状态";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(110, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem6.Control = this.flowLayoutPanel2;
            this.layoutControlItem6.CustomizationFormText = "随访(第";
            this.layoutControlItem6.Location = new System.Drawing.Point(110, 148);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(160, 28);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(160, 28);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "随访(第";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(66, 18);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.flowLayoutPanel3;
            this.layoutControlItem7.CustomizationFormText = "当前是否羁押";
            this.layoutControlItem7.Location = new System.Drawing.Point(270, 148);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(199, 28);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(361, 28);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "当前是否羁押";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(72, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem2.Control = this.flowLayoutPanel4;
            this.layoutControlItem2.CustomizationFormText = "失访(原因";
            this.layoutControlItem2.Location = new System.Drawing.Point(110, 176);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(231, 28);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(521, 28);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "失访(原因";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(66, 18);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem4.Control = this.flowLayoutPanel5;
            this.layoutControlItem4.CustomizationFormText = "查无此人";
            this.layoutControlItem4.Location = new System.Drawing.Point(110, 204);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(231, 28);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(521, 28);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "查无此人";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(66, 18);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.date艾滋病确诊日期;
            this.layoutControlItem14.CustomizationFormText = "艾滋病确诊日期";
            this.layoutControlItem14.Location = new System.Drawing.Point(355, 232);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(218, 24);
            this.layoutControlItem14.Text = "艾滋病确诊日期";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.date死亡日期;
            this.layoutControlItem15.CustomizationFormText = "死亡日期";
            this.layoutControlItem15.Location = new System.Drawing.Point(355, 256);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem15.Text = "死亡日期";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.flowLayoutPanel7;
            this.layoutControlItem16.CustomizationFormText = "病程阶段";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 232);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem16.Text = "病程阶段";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(110, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.flowLayoutPanel8;
            this.layoutControlItem17.CustomizationFormText = "是否已死亡";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 256);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(355, 24);
            this.layoutControlItem17.Text = "是否已死亡";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.flow主要死因;
            this.layoutControlItem18.CustomizationFormText = "主要死亡原因";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 280);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(0, 60);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(239, 60);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(631, 60);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "主要死因";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.txt阳性母亲卡片编号;
            this.layoutControlItem20.CustomizationFormText = "其阳性母亲卡片编号";
            this.layoutControlItem20.Location = new System.Drawing.Point(236, 340);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(415, 24);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(1, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(395, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "其阳性母亲卡片编号";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.flowLayoutPanel10;
            this.layoutControlItem23.CustomizationFormText = "是否为母婴传播病例";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 340);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(219, 24);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "是否为母婴传播病例";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem27.Control = this.flowLayoutPanel14;
            this.layoutControlItem27.CustomizationFormText = "目前是否有固定性伴侣";
            this.layoutControlItem27.Location = new System.Drawing.Point(80, 494);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(551, 28);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "目前是否有固定性伴侣";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextToControlDistance = 0;
            this.layoutControlItem27.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem7.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem7.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem7.CustomizationFormText = "目前是否有固定性伴侣";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 494);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(80, 185);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(80, 185);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(80, 185);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.Text = "目前是否有固定性伴侣";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(108, 0);
            this.emptySpaceItem7.TextVisible = true;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.flowLayoutPanel16;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(80, 550);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(551, 26);
            this.layoutControlItem29.Text = "layoutControlItem29";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.flowLayoutPanel15;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(80, 522);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(551, 28);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "layoutControlItem28";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextToControlDistance = 0;
            this.layoutControlItem28.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem8.CustomizationFormText = "目前是否有配偶";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 364);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(110, 140);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(110, 130);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(110, 130);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.Text = "目前是否有配偶";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(108, 0);
            this.emptySpaceItem8.TextVisible = true;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.flowLayoutPanel11;
            this.layoutControlItem24.CustomizationFormText = "目前是否有配偶";
            this.layoutControlItem24.Location = new System.Drawing.Point(110, 364);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(521, 28);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "目前是否有配偶";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextToControlDistance = 0;
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem21.Control = this.flowLayoutPanel12;
            this.layoutControlItem21.CustomizationFormText = "自上次随访以来配偶变更及配偶间性行为情况";
            this.layoutControlItem21.Location = new System.Drawing.Point(110, 392);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(0, 50);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(1, 50);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(521, 50);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "自上次随访以来配偶变更及配偶间性行为情况";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(120, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.flowLayoutPanel13;
            this.layoutControlItem25.CustomizationFormText = "当前配偶感染状况";
            this.layoutControlItem25.Location = new System.Drawing.Point(110, 442);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(521, 28);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "当前配偶感染状况";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(95, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.date检测日期;
            this.layoutControlItem19.CustomizationFormText = "若已检测,检测日期";
            this.layoutControlItem19.Location = new System.Drawing.Point(110, 470);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(165, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(190, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "若已检测,检测日期";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(100, 18);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.txt阳性配偶卡片编号;
            this.layoutControlItem26.CustomizationFormText = "若当前配偶感染状况为阳性,其卡片编号为";
            this.layoutControlItem26.Location = new System.Drawing.Point(300, 470);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(263, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(331, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "若当前配偶感染状况为阳性其卡片编号";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(204, 14);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.flowLayoutPanel17;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(80, 576);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(551, 26);
            this.layoutControlItem30.Text = "layoutControlItem30";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextToControlDistance = 0;
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.flowLayoutPanel18;
            this.layoutControlItem31.CustomizationFormText = "layoutControlItem31";
            this.layoutControlItem31.Location = new System.Drawing.Point(80, 602);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(551, 26);
            this.layoutControlItem31.Text = "layoutControlItem31";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextToControlDistance = 0;
            this.layoutControlItem31.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.flowLayoutPanel19;
            this.layoutControlItem32.CustomizationFormText = "layoutControlItem32";
            this.layoutControlItem32.Location = new System.Drawing.Point(80, 628);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(551, 51);
            this.layoutControlItem32.Text = "layoutControlItem32";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextToControlDistance = 0;
            this.layoutControlItem32.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 679);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(110, 112);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(110, 112);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(110, 112);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "本次随访是否出现以下结核病可疑筛查症状";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(108, 0);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.flowLayoutPanel20;
            this.layoutControlItem33.CustomizationFormText = "layoutControlItem33";
            this.layoutControlItem33.Location = new System.Drawing.Point(110, 679);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(300, 28);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(200, 28);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(259, 28);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "咳嗽、咳痰持续2周以上";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(160, 18);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.flowLayoutPanel21;
            this.layoutControlItem34.CustomizationFormText = "layoutControlItem34";
            this.layoutControlItem34.Location = new System.Drawing.Point(369, 679);
            this.layoutControlItem34.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(262, 28);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "反复咳出的痰中带血";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(160, 18);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.flowLayoutPanel22;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(369, 707);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(262, 28);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "夜间经常出汗";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(160, 18);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.flowLayoutPanel23;
            this.layoutControlItem35.CustomizationFormText = "反复发热持续2周以上";
            this.layoutControlItem35.Location = new System.Drawing.Point(110, 707);
            this.layoutControlItem35.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(259, 28);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "反复发热持续2周以上";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(160, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.flowLayoutPanel24;
            this.layoutControlItem36.CustomizationFormText = "经常容易疲劳或呼吸短促";
            this.layoutControlItem36.Location = new System.Drawing.Point(369, 735);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(262, 56);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "经常容易疲劳或呼吸短促";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(160, 20);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.flowLayoutPanel25;
            this.layoutControlItem37.CustomizationFormText = "是否无法解释的体重明显下降";
            this.layoutControlItem37.Location = new System.Drawing.Point(110, 735);
            this.layoutControlItem37.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(259, 28);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Text = "是否无法解释的体重明显下降";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(160, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.flowLayoutPanel26;
            this.layoutControlItem38.CustomizationFormText = "淋巴结肿大";
            this.layoutControlItem38.Location = new System.Drawing.Point(110, 763);
            this.layoutControlItem38.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(259, 28);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "淋巴结肿大";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(160, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.flowLayoutPanel27;
            this.layoutControlItem39.CustomizationFormText = "layoutControlItem39";
            this.layoutControlItem39.Location = new System.Drawing.Point(110, 847);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(521, 28);
            this.layoutControlItem39.Text = "layoutControlItem39";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem39.TextToControlDistance = 0;
            this.layoutControlItem39.TextVisible = false;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.flowLayoutPanel28;
            this.layoutControlItem40.CustomizationFormText = "layoutControlItem40";
            this.layoutControlItem40.Location = new System.Drawing.Point(110, 791);
            this.layoutControlItem40.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(242, 28);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(521, 28);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "layoutControlItem40";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem40.TextToControlDistance = 0;
            this.layoutControlItem40.TextVisible = false;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem9.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem9.CustomizationFormText = "过去6个月是否接受过结核病检查";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 791);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(110, 56);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(110, 56);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(110, 56);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.Text = "过去6个月是否接受过结核病检查";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(108, 0);
            this.emptySpaceItem9.TextVisible = true;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.flowLayoutPanel29;
            this.layoutControlItem41.CustomizationFormText = "layoutControlItem41";
            this.layoutControlItem41.Location = new System.Drawing.Point(110, 819);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(521, 28);
            this.layoutControlItem41.Text = "layoutControlItem41";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem41.TextToControlDistance = 0;
            this.layoutControlItem41.TextVisible = false;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem10.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem10.CustomizationFormText = "目前是否接受国家免费艾滋病抗病毒治疗";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 847);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(110, 60);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(110, 53);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(110, 53);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.Text = "目前是否接受国家免费艾滋病抗病毒治疗";
            this.emptySpaceItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(108, 20);
            this.emptySpaceItem10.TextVisible = true;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.txt抗病毒治疗编号;
            this.layoutControlItem42.CustomizationFormText = "抗病毒治疗编号";
            this.layoutControlItem42.Location = new System.Drawing.Point(110, 875);
            this.layoutControlItem42.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(165, 24);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(254, 25);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Text = "抗病毒治疗编号";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(120, 18);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(364, 875);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(267, 25);
            this.emptySpaceItem11.Text = "emptySpaceItem11";
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.flowLayoutPanel30;
            this.layoutControlItem43.CustomizationFormText = "CD4+检测";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 900);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 56);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(242, 50);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 0, 0);
            this.layoutControlItem43.Size = new System.Drawing.Size(631, 50);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "CD4+检测";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.txt备注;
            this.layoutControlItem44.CustomizationFormText = "备注";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 974);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(631, 24);
            this.layoutControlItem44.Text = "备注";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.txt创建时间;
            this.layoutControlItem47.CustomizationFormText = "创建时间";
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 998);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(203, 24);
            this.layoutControlItem47.Text = "创建时间";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(90, 18);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(573, 232);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(58, 24);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(574, 256);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(57, 24);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.txt修改时间;
            this.layoutControlItem46.CustomizationFormText = "最近修改时间";
            this.layoutControlItem46.Location = new System.Drawing.Point(203, 998);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(194, 24);
            this.layoutControlItem46.Text = "最近修改时间";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(90, 18);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt所属机构;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(397, 998);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem9.Text = "当前所属机构";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 18);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem49.Control = this.txt创建人;
            this.layoutControlItem49.CustomizationFormText = "创建人";
            this.layoutControlItem49.Location = new System.Drawing.Point(203, 1022);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(194, 24);
            this.layoutControlItem49.Text = "创建人";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(90, 18);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.txt创建机构;
            this.layoutControlItem48.CustomizationFormText = "创建机构";
            this.layoutControlItem48.Location = new System.Drawing.Point(0, 1022);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(203, 24);
            this.layoutControlItem48.Text = "创建机构";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.Control = this.txt修改人;
            this.layoutControlItem50.CustomizationFormText = "最近修改人";
            this.layoutControlItem50.Location = new System.Drawing.Point(397, 1022);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem50.Text = "最近修改人";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(90, 18);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.date随访日期;
            this.layoutControlItem45.CustomizationFormText = "随访日期";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(169, 26);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(235, 26);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "随访日期";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem51.Control = this.txt随访实施单位;
            this.layoutControlItem51.CustomizationFormText = "随访实施单位";
            this.layoutControlItem51.Location = new System.Drawing.Point(0, 950);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(253, 24);
            this.layoutControlItem51.Text = "随访实施单位";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem52.Control = this.txt随访人员;
            this.layoutControlItem52.CustomizationFormText = "随访人员";
            this.layoutControlItem52.Location = new System.Drawing.Point(253, 950);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(182, 24);
            this.layoutControlItem52.Text = "随访人员";
            this.layoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem52.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem52.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt卡片编号;
            this.layoutControlItem1.CustomizationFormText = "卡片编号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(1, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(631, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "卡片编号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt联系电话;
            this.layoutControlItem12.CustomizationFormText = "联系电话";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(296, 24);
            this.layoutControlItem12.Text = "联系电话";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt患儿家长姓名;
            this.layoutControlItem10.CustomizationFormText = "患儿家长姓名";
            this.layoutControlItem10.Location = new System.Drawing.Point(296, 74);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(335, 24);
            this.layoutControlItem10.Text = "患儿家长姓名";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt现住地址;
            this.layoutControlItem13.CustomizationFormText = "现住地址（详填）";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(631, 24);
            this.layoutControlItem13.Text = "现住地址（详填）";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(435, 950);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(196, 24);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem3.Control = this.txt个人档案编号;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(296, 24);
            this.layoutControlItem3.Text = "个人档案号";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt患者姓名;
            this.layoutControlItem5.CustomizationFormText = "患者姓名";
            this.layoutControlItem5.Location = new System.Drawing.Point(296, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(335, 24);
            this.layoutControlItem5.Text = "患者姓名";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt性别;
            this.layoutControlItem11.CustomizationFormText = "性别";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(1, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(296, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "性别";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt身份证号;
            this.layoutControlItem8.CustomizationFormText = "身份证号";
            this.layoutControlItem8.Location = new System.Drawing.Point(296, 50);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(169, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(335, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "身份证号";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(110, 18);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // UC个案随访表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UC个案随访表_显示";
            this.Size = new System.Drawing.Size(750, 700);
            this.Load += new System.EventHandler(this.UC个案随访表_显示_Load);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访人员.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访实施单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt现住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt修改时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).EndInit();
            this.flowLayoutPanel30.ResumeLayout(false);
            this.flowLayoutPanel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCD4检测次数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCD4检测结果个数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCD4检测日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateCD4检测日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCD4检测单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt抗病毒治疗编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt所属机构.Properties)).EndInit();
            this.flowLayoutPanel29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio结核病检查结果.Properties)).EndInit();
            this.flowLayoutPanel28.ResumeLayout(false);
            this.flowLayoutPanel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否接受过结核病检查.Properties)).EndInit();
            this.flowLayoutPanel27.ResumeLayout(false);
            this.flowLayoutPanel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否接受免费治疗.Properties)).EndInit();
            this.flowLayoutPanel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio是否淋巴结肿大.Properties)).EndInit();
            this.flowLayoutPanel25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio是否体重异常下降.Properties)).EndInit();
            this.flowLayoutPanel24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio是否容易疲劳.Properties)).EndInit();
            this.flowLayoutPanel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio是否发热.Properties)).EndInit();
            this.flowLayoutPanel22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio是否出汗.Properties)).EndInit();
            this.flowLayoutPanel21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio是否咳血.Properties)).EndInit();
            this.flowLayoutPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio是否咳嗽.Properties)).EndInit();
            this.flowLayoutPanel19.ResumeLayout(false);
            this.flowLayoutPanel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否每次与固定性伴性行为时都用安全套.Properties)).EndInit();
            this.flowLayoutPanel18.ResumeLayout(false);
            this.flowLayoutPanel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否每次性行为时都用安全套.Properties)).EndInit();
            this.flowLayoutPanel17.ResumeLayout(false);
            this.flowLayoutPanel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt3个月性行为人数.Properties)).EndInit();
            this.flowLayoutPanel16.ResumeLayout(false);
            this.flowLayoutPanel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt同性固定性伴人数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt同性固定性伴已检测人数.Properties)).EndInit();
            this.flowLayoutPanel15.ResumeLayout(false);
            this.flowLayoutPanel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt异性固定性伴人数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt异性固定性伴已检测人数.Properties)).EndInit();
            this.flowLayoutPanel14.ResumeLayout(false);
            this.flowLayoutPanel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否有固定性伴侣.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阳性配偶卡片编号.Properties)).EndInit();
            this.flowLayoutPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio配偶感染状况.Properties)).EndInit();
            this.flowLayoutPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio配偶变更及性行为情况.Properties)).EndInit();
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否有配偶.Properties)).EndInit();
            this.flowLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio是否为母婴传播病例.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阳性母亲卡片编号.Properties)).EndInit();
            this.flow主要死因.ResumeLayout(false);
            this.flow主要死因.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk主要死因1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk主要死因2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk主要死因3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk主要死因4.Properties)).EndInit();
            this.flowLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio是否死亡.Properties)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk病毒感染者.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk艾滋病人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt患儿家长姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt患者姓名.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk以后无需随访.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio失访原因.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio是否羁押.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访次数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt卡片编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date艾滋病确诊日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date艾滋病确诊日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date死亡日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date死亡日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date检测日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date检测日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt随访人员;
        private DevExpress.XtraEditors.TextEdit txt随访实施单位;
        private DevExpress.XtraEditors.DateEdit date随访日期;
        private DevExpress.XtraEditors.TextEdit txt现住地址;
        private DevExpress.XtraEditors.TextEdit txt修改人;
        private DevExpress.XtraEditors.TextEdit txt创建人;
        private DevExpress.XtraEditors.TextEdit txt创建机构;
        private DevExpress.XtraEditors.TextEdit txt创建时间;
        private DevExpress.XtraEditors.TextEdit txt修改时间;
        private DevExpress.XtraEditors.TextEdit txt备注;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.TextEdit txtCD4检测次数;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.TextEdit txtCD4检测结果个数;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.DateEdit dateCD4检测日期;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.TextEdit txtCD4检测单位;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.TextEdit txt抗病毒治疗编号;
        private DevExpress.XtraEditors.TextEdit txt所属机构;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
        private DevExpress.XtraEditors.RadioGroup radio结核病检查结果;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
        private DevExpress.XtraEditors.RadioGroup radio是否接受过结核病检查;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
        private DevExpress.XtraEditors.RadioGroup radio是否接受免费治疗;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private DevExpress.XtraEditors.RadioGroup radio是否淋巴结肿大;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private DevExpress.XtraEditors.RadioGroup radio是否体重异常下降;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private DevExpress.XtraEditors.RadioGroup radio是否容易疲劳;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
        private DevExpress.XtraEditors.RadioGroup radio是否发热;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel22;
        private DevExpress.XtraEditors.RadioGroup radio是否出汗;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel21;
        private DevExpress.XtraEditors.RadioGroup radio是否咳血;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private DevExpress.XtraEditors.RadioGroup radio是否咳嗽;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.RadioGroup radio是否每次与固定性伴性行为时都用安全套;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.RadioGroup radio是否每次性行为时都用安全套;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.TextEdit txt3个月性行为人数;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txt同性固定性伴人数;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txt同性固定性伴已检测人数;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txt异性固定性伴人数;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txt异性固定性伴已检测人数;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private DevExpress.XtraEditors.RadioGroup radio是否有固定性伴侣;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txt阳性配偶卡片编号;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private DevExpress.XtraEditors.RadioGroup radio配偶感染状况;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private DevExpress.XtraEditors.RadioGroup radio配偶变更及性行为情况;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private DevExpress.XtraEditors.RadioGroup radio是否有配偶;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private DevExpress.XtraEditors.RadioGroup radio是否为母婴传播病例;
        private DevExpress.XtraEditors.TextEdit txt阳性母亲卡片编号;
        private System.Windows.Forms.FlowLayoutPanel flow主要死因;
        private DevExpress.XtraEditors.CheckEdit chk主要死因1;
        private DevExpress.XtraEditors.CheckEdit chk主要死因2;
        private DevExpress.XtraEditors.CheckEdit chk主要死因3;
        private DevExpress.XtraEditors.CheckEdit chk主要死因4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private DevExpress.XtraEditors.RadioGroup radio是否死亡;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private DevExpress.XtraEditors.CheckEdit chk病毒感染者;
        private DevExpress.XtraEditors.CheckEdit chk艾滋病人;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt患儿家长姓名;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt患者姓名;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.CheckEdit chk以后无需随访;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.RadioGroup radio失访原因;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.RadioGroup radio是否羁押;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.TextEdit txt随访次数;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private UserControlBase userControlBase1;
        private DevExpress.XtraEditors.TextEdit txt个人档案编号;
        private DevExpress.XtraEditors.TextEdit txt卡片编号;
        private DevExpress.XtraEditors.DateEdit date艾滋病确诊日期;
        private DevExpress.XtraEditors.DateEdit date死亡日期;
        private DevExpress.XtraEditors.DateEdit date检测日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.SimpleButton btn添加;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;

    }
}
