﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraNavBar;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康.艾滋病;

namespace AtomEHR.公共卫生.Module.个人健康.艾滋病
{
    public partial class UC个案随访表_显示 : UserControlBaseNavBar
    {
        DataRow[] _dr个人档案信息 = null;
        string _创建日期 = "";
        string _ID = "";
        public UC个案随访表_显示()
        {
            InitializeComponent();
        }

        public UC个案随访表_显示(DataRow[] dr, object date)
        {
            InitializeComponent();

            _dr个人档案信息 = dr;
            _创建日期 = date == null ? "" : date.ToString();
            _BLL = new bll个案随访表();

            //默认绑定
            this.txt个人档案编号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt患者姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt现住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
               
        }

        private void UC个案随访表_显示_Load(object sender, EventArgs e)
        {
            _dt缓存数据 = _BLL.GetBusinessByKey(this.txt个人档案编号.Text, true).Tables[tb_个案随访表.__TableName];

            CreateNavBarButton_new(dt缓存数据, tb_个案随访表.随访日期);

            if (dt缓存数据 != null && dt缓存数据.Rows.Count > 0)
            {
                if (_创建日期 != null && _创建日期 != "")
                {
                    DataRow[] drss = dt缓存数据.Select("创建时间='" + _创建日期 + "'");
                    DoBindingSummaryEditor(drss[0]);
                    //DoBindingSummaryEditor(dt缓存数据.Select("创建时间='" + _创建日期 + "'")[0]);
                }
                else
                {
                    DoBindingSummaryEditor(dt缓存数据.Rows[0]);
                }
                SetItemColorToRed(_ID);//设置左侧当前随访日期颜色

                //SetLclColor();//设置考核项的颜色
            }
            else
            {//打开新增页面
                btn添加.PerformClick();
            }
        }

        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null)
            {
                return;
            }

            dataSource.Rows[0][tb_个案随访表.个人档案编号] = txt个人档案编号.Text;
            _ID = dataSource.Rows[0][tb_个案随访表.ID].ToString();

            DataBinder.BindingTextEdit(txt卡片编号, dataSource, tb_个案随访表.卡片编号);
            DataBinder.BindingTextEditDateTime(date随访日期, dataSource, tb_个案随访表.随访日期);
            DataBinder.BindingTextEdit(txt随访次数, dataSource, tb_个案随访表.随访次数);
            DataBinder.BindingRadioEdit(radio是否羁押, dataSource, tb_个案随访表.是否羁押);
            DataBinder.BindingRadioEdit(radio失访原因, dataSource, tb_个案随访表.失访原因);
            string str查无此人 = dataSource.Rows[0][tb_个案随访表.查无此人].ToString();
            if (!string.IsNullOrEmpty(str查无此人) && str查无此人=="1") 
            {
                chk以后无需随访.Checked = true;
            }
            else
            {
                chk以后无需随访.Checked = false;
            }
            DataBinder.BindingTextEdit(txt患儿家长姓名, dataSource, tb_个案随访表.患儿家长姓名);
            string str病程阶段 = dataSource.Rows[0][tb_个案随访表.病程阶段].ToString();
            string[] arr病程阶段 = str病程阶段.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int index = 0; index < arr病程阶段.Length; index++)
            {
                switch (arr病程阶段[index])
                {
                    case "1":
                        chk病毒感染者.Checked = true;
                        this.date艾滋病确诊日期.EditValue = "";
                        break;
                    case "2":
                        chk艾滋病人.Checked = true;
                        DataBinder.BindingTextEditDateTime(date艾滋病确诊日期, dataSource, tb_个案随访表.艾滋病确诊日期);
                        break;
                    default: break;
                }
            }
            DataBinder.BindingRadioEdit(radio是否死亡, dataSource, tb_个案随访表.是否已死亡);
            if (radio是否死亡.EditValue != null && radio是否死亡.EditValue.ToString() == "2")
            {
                this.date死亡日期.EditValue = "";
            }
            else
            {
                DataBinder.BindingTextEditDateTime(date死亡日期, dataSource, tb_个案随访表.死亡日期);
            }
            string str主要死因 = dataSource.Rows[0][tb_个案随访表.主要死因].ToString();
            string[] arr主要死因 = str主要死因.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int index = 0; index < arr主要死因.Length; index++)
            {
                switch (arr主要死因[index])
                {
                    case "1":
                        chk主要死因1.Checked = true;
                        break;
                    case "2":
                        chk主要死因2.Checked = true;
                        break;
                    case "3":
                        chk主要死因3.Checked = true;
                        break;
                    case "4":
                        chk主要死因4.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            DataBinder.BindingRadioEdit(radio是否为母婴传播病例, dataSource, tb_个案随访表.是否为母婴传播病例);
            DataBinder.BindingTextEdit(txt阳性母亲卡片编号, dataSource, tb_个案随访表.阳性母亲卡片编号);
            DataBinder.BindingRadioEdit(radio是否有配偶, dataSource, tb_个案随访表.是否有配偶);
            DataBinder.BindingRadioEdit(radio配偶变更及性行为情况, dataSource, tb_个案随访表.配偶变更及性行为情况);
            DataBinder.BindingRadioEdit(radio配偶感染状况, dataSource, tb_个案随访表.配偶感染状况);
            if (radio配偶感染状况.EditValue.ToString() =="" && radio配偶感染状况.EditValue.ToString() == "1")//未查/不详
            {
                this.date检测日期.EditValue = "";
                this.txt阳性配偶卡片编号.Text = "";
            }
            else
            {
                DataBinder.BindingTextEditDateTime(date检测日期, dataSource, tb_个案随访表.检测日期);
                DataBinder.BindingTextEdit(txt阳性配偶卡片编号, dataSource, tb_个案随访表.配偶感染状况为阳性卡片编号);
            }
            DataBinder.BindingRadioEdit(radio是否有固定性伴侣, dataSource, tb_个案随访表.是否有固定性伴);
            DataBinder.BindingTextEdit(txt异性固定性伴人数, dataSource, tb_个案随访表.异性固定性伴个数);
            DataBinder.BindingTextEdit(txt异性固定性伴已检测人数, dataSource, tb_个案随访表.异性固定性伴检测人数);
            DataBinder.BindingTextEdit(txt同性固定性伴人数, dataSource, tb_个案随访表.同性固定性伴个数);
            DataBinder.BindingTextEdit(txt同性固定性伴已检测人数, dataSource, tb_个案随访表.同性固定性伴检测人数);
            DataBinder.BindingTextEdit(txt3个月性行为人数, dataSource, tb_个案随访表.过去3个月性行为人数);
            DataBinder.BindingRadioEdit(radio是否每次性行为时都用安全套, dataSource, tb_个案随访表.是否每次性行为时都用安全套);
            DataBinder.BindingRadioEdit(radio是否每次与固定性伴性行为时都用安全套, dataSource, tb_个案随访表.是否每次与固定性伴性行为时都用安全套);
            DataBinder.BindingRadioEdit(radio是否咳嗽, dataSource, tb_个案随访表.是否咳嗽咳痰);
            DataBinder.BindingRadioEdit(radio是否咳血, dataSource, tb_个案随访表.是否咳痰带血);
            DataBinder.BindingRadioEdit(radio是否发热, dataSource, tb_个案随访表.是否反复发热);
            DataBinder.BindingRadioEdit(radio是否出汗, dataSource, tb_个案随访表.是否夜间出汗);
            DataBinder.BindingRadioEdit(radio是否体重异常下降, dataSource, tb_个案随访表.是否异常体重下降);
            DataBinder.BindingRadioEdit(radio是否容易疲劳, dataSource, tb_个案随访表.是否容易疲劳);
            DataBinder.BindingRadioEdit(radio是否淋巴结肿大, dataSource, tb_个案随访表.是否淋巴结肿大);
            DataBinder.BindingRadioEdit(radio是否接受过结核病检查, dataSource, tb_个案随访表.是否接受过结核病检查);
            DataBinder.BindingRadioEdit(radio结核病检查结果, dataSource, tb_个案随访表.结核病检查结果);
            DataBinder.BindingRadioEdit(radio是否接受免费治疗, dataSource, tb_个案随访表.是否接受免费艾滋病抗病毒治疗);
            DataBinder.BindingTextEdit(txt抗病毒治疗编号, dataSource, tb_个案随访表.抗病毒治疗编号);
            DataBinder.BindingTextEdit(txtCD4检测次数, dataSource, tb_个案随访表.做过CD4检测次数);
            DataBinder.BindingTextEdit(txtCD4检测结果个数, dataSource, tb_个案随访表.CD4检测结果个数);
            DataBinder.BindingTextEditDateTime(dateCD4检测日期, dataSource, tb_个案随访表.CD4检测日期);
            DataBinder.BindingTextEdit(txtCD4检测单位, dataSource, tb_个案随访表.CD4检测单位);
            DataBinder.BindingTextEdit(txt随访实施单位, dataSource, tb_个案随访表.随访实施单位);
            DataBinder.BindingTextEdit(txt随访人员, dataSource, tb_个案随访表.随访人员);
            DataBinder.BindingTextEdit(txt备注, dataSource, tb_个案随访表.备注);

            //非编辑项
            this.txt所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_个案随访表.所属机构].ToString());
            this.txt创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_个案随访表.创建机构].ToString());
            this.txt创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_个案随访表.创建人].ToString());
            this.txt创建时间.Text = dataSource.Rows[0][tb_个案随访表.创建时间].ToString();
            this.txt修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_个案随访表.修改人].ToString());
            this.txt修改时间.Text = dataSource.Rows[0][tb_个案随访表.修改时间].ToString();
        }

        private void btn添加_Click(object sender, EventArgs e)
        {
            _UpdateType = UpdateType.Add;
            UC个案随访表 control = new UC个案随访表(_dr个人档案信息, _UpdateType, "");
            ShowControl(control);
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                _UpdateType = UpdateType.Modify;
                UC个案随访表 control = new UC个案随访表(_dr个人档案信息, _UpdateType, _ID);
                ShowControl(control);
            }
            else { Msg.ShowInformation("只能修改属于本机构的记录！"); }
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("确认要删除？")) return;
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                if (BLL.Delete(_ID))
                {
                    _创建日期 = "";
                    this.OnLoad(e);
                }
            }
            else { Msg.ShowInformation("只能操作属于本机构的记录！"); }
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            Msg.ShowInformation("导出");
        }
        
    }
}
