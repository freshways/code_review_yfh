﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Library;

namespace AtomEHR.公共卫生.Module.个人健康.艾滋病
{
    public partial class UC个案随访表 : UserControlBase
    {
        DataRow[] _dr个人档案信息 = null;
        string _ID = "";
        public UC个案随访表()
        {
            InitializeComponent();
        }
        public UC个案随访表(DataRow[] dr, UpdateType updateType, object ID)
        {
            InitializeComponent();

            base._UpdateType = updateType;
            _dr个人档案信息 = dr;
            _ID = ID == null ? "" : ID.ToString();
            _BLL = new bll个案随访表();

            //默认绑定
            this.txt个人档案编号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt患者姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt现住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();

        }

        private void UC个案随访表_Load(object sender, EventArgs e)
        {
            if (_UpdateType == UpdateType.Add)
            {
                _BLL.GetBusinessByKey("-", true);
                _BLL.NewBusiness();
                _BLL.DataBinder.Rows[0][tb_个案随访表.个人档案编号] = _dr个人档案信息[0][tb_个案随访表.个人档案编号].ToString();
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                date随访日期.Properties.ReadOnly = true;
                date随访日期.Properties.Buttons[0].Enabled = false;

                if (_ID != null && _ID != "")
                {
                    ((bll个案随访表)_BLL).GetBusinessByKeyEdit(_ID, true);
                }
                else
                {
                    return;
                }
            }

            DataTable dataSource = _BLL.CurrentBusiness.Tables[tb_个案随访表.__TableName];
            DoBindingSummaryEditor(dataSource);

        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null)
            {
                return;
            }

            dataSource.Rows[0][tb_个案随访表.个人档案编号] = txt个人档案编号.Text;

            DataBinder.BindingTextEdit(txt卡片编号, dataSource, tb_个案随访表.卡片编号);
            DataBinder.BindingTextEditDateTime(date随访日期, dataSource, tb_个案随访表.随访日期);
            DataBinder.BindingTextEdit(txt随访次数, dataSource, tb_个案随访表.随访次数);
            DataBinder.BindingRadioEdit(radio是否羁押, dataSource, tb_个案随访表.是否羁押);
            DataBinder.BindingRadioEdit(radio失访原因, dataSource, tb_个案随访表.失访原因);
            string str查无此人 = dataSource.Rows[0][tb_个案随访表.查无此人].ToString();
            if (!string.IsNullOrEmpty(str查无此人) && str查无此人 == "1")
            {
                chk以后无需随访.Checked = true;
            }
            else
            {
                chk以后无需随访.Checked = false;
            }
            DataBinder.BindingTextEdit(txt患儿家长姓名, dataSource, tb_个案随访表.患儿家长姓名);
            string str病程阶段 = dataSource.Rows[0][tb_个案随访表.病程阶段].ToString();
            string[] arr病程阶段 = str病程阶段.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int index = 0; index < arr病程阶段.Length; index++)
            {
                switch (arr病程阶段[index])
                {
                    case "1":
                        chk病毒感染者.Checked = true;
                        this.date艾滋病确诊日期.EditValue = "";
                        dataSource.Rows[0][tb_个案随访表.艾滋病确诊日期] = this.date艾滋病确诊日期.EditValue;
                        break;
                    case "2": 
                        chk艾滋病人.Checked = true;
                        DataBinder.BindingTextEditDateTime(date艾滋病确诊日期, dataSource, tb_个案随访表.艾滋病确诊日期);
                        break;
                    default: break;
                }
            }
            DataBinder.BindingRadioEdit(radio是否死亡, dataSource, tb_个案随访表.是否已死亡);
            if (radio是否死亡.EditValue != null && radio是否死亡.EditValue.ToString() == "2")
            {
                this.date死亡日期.EditValue = "";
                dataSource.Rows[0][tb_个案随访表.死亡日期] = this.date死亡日期.EditValue;
            }
            else
            {
                DataBinder.BindingTextEditDateTime(date死亡日期, dataSource, tb_个案随访表.死亡日期);
            }
            string str主要死因 = dataSource.Rows[0][tb_个案随访表.主要死因].ToString();
            string[] arr主要死因 = str主要死因.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries);
            for (int index = 0; index < arr主要死因.Length; index++)
            {
                switch (arr主要死因[index])
                {
                    case "1":
                        chk主要死因1.Checked = true;
                        break;
                    case "2":
                        chk主要死因2.Checked = true;
                        break;
                    case "3":
                        chk主要死因3.Checked = true;
                        break;
                    case "4":
                        chk主要死因4.Checked = true;
                        break;
                    default: 
                        break;
                }
            }
            DataBinder.BindingRadioEdit(radio是否为母婴传播病例, dataSource, tb_个案随访表.是否为母婴传播病例);
            DataBinder.BindingTextEdit(txt阳性母亲卡片编号, dataSource, tb_个案随访表.阳性母亲卡片编号);
            DataBinder.BindingRadioEdit(radio是否有配偶, dataSource, tb_个案随访表.是否有配偶);
            DataBinder.BindingRadioEdit(radio配偶变更及性行为情况, dataSource, tb_个案随访表.配偶变更及性行为情况);
            DataBinder.BindingRadioEdit(radio配偶感染状况, dataSource, tb_个案随访表.配偶感染状况);
            if (radio配偶感染状况.EditValue.ToString() =="" || radio配偶感染状况.EditValue.ToString() == "1")//未查/不详
            {
                this.date检测日期.EditValue= "";
                this.txt阳性配偶卡片编号.Text = "";
                dataSource.Rows[0][tb_个案随访表.检测日期] = this.date检测日期.EditValue;
                dataSource.Rows[0][tb_个案随访表.配偶感染状况为阳性卡片编号] = this.txt阳性配偶卡片编号.Text;
            }
            else
            {
                DataBinder.BindingTextEditDateTime(date检测日期, dataSource, tb_个案随访表.检测日期);
                DataBinder.BindingTextEdit(txt阳性配偶卡片编号, dataSource, tb_个案随访表.配偶感染状况为阳性卡片编号);
            }
            DataBinder.BindingRadioEdit(radio是否有固定性伴侣, dataSource, tb_个案随访表.是否有固定性伴);
            DataBinder.BindingTextEdit(txt异性固定性伴人数, dataSource, tb_个案随访表.异性固定性伴个数);
            DataBinder.BindingTextEdit(txt异性固定性伴已检测人数, dataSource, tb_个案随访表.异性固定性伴检测人数);
            DataBinder.BindingTextEdit(txt同性固定性伴人数, dataSource, tb_个案随访表.同性固定性伴个数);
            DataBinder.BindingTextEdit(txt同性固定性伴已检测人数, dataSource, tb_个案随访表.同性固定性伴检测人数);
            DataBinder.BindingTextEdit(txt3个月性行为人数, dataSource, tb_个案随访表.过去3个月性行为人数);
            DataBinder.BindingRadioEdit(radio是否每次性行为时都用安全套, dataSource, tb_个案随访表.是否每次性行为时都用安全套);
            DataBinder.BindingRadioEdit(radio是否每次与固定性伴性行为时都用安全套, dataSource, tb_个案随访表.是否每次与固定性伴性行为时都用安全套);
            DataBinder.BindingRadioEdit(radio是否咳嗽, dataSource, tb_个案随访表.是否咳嗽咳痰);
            DataBinder.BindingRadioEdit(radio是否咳血, dataSource, tb_个案随访表.是否咳痰带血);
            DataBinder.BindingRadioEdit(radio是否发热, dataSource, tb_个案随访表.是否反复发热);
            DataBinder.BindingRadioEdit(radio是否出汗, dataSource, tb_个案随访表.是否夜间出汗);
            DataBinder.BindingRadioEdit(radio是否体重异常下降, dataSource, tb_个案随访表.是否异常体重下降);
            DataBinder.BindingRadioEdit(radio是否容易疲劳, dataSource, tb_个案随访表.是否容易疲劳);
            DataBinder.BindingRadioEdit(radio是否淋巴结肿大, dataSource, tb_个案随访表.是否淋巴结肿大);
            DataBinder.BindingRadioEdit(radio是否接受过结核病检查, dataSource, tb_个案随访表.是否接受过结核病检查);
            DataBinder.BindingRadioEdit(radio结核病检查结果, dataSource, tb_个案随访表.结核病检查结果);
            DataBinder.BindingRadioEdit(radio是否接受免费治疗, dataSource, tb_个案随访表.是否接受免费艾滋病抗病毒治疗);
            DataBinder.BindingTextEdit(txt抗病毒治疗编号, dataSource, tb_个案随访表.抗病毒治疗编号);
            DataBinder.BindingTextEdit(txtCD4检测次数, dataSource, tb_个案随访表.做过CD4检测次数);
            DataBinder.BindingTextEdit(txtCD4检测结果个数, dataSource, tb_个案随访表.CD4检测结果个数);
            DataBinder.BindingTextEditDateTime(dateCD4检测日期, dataSource, tb_个案随访表.CD4检测日期);
            DataBinder.BindingTextEdit(txtCD4检测单位, dataSource, tb_个案随访表.CD4检测单位);
            DataBinder.BindingTextEdit(txt随访实施单位, dataSource, tb_个案随访表.随访实施单位);
            DataBinder.BindingTextEdit(txt随访人员, dataSource, tb_个案随访表.随访人员);
            DataBinder.BindingTextEdit(txt备注, dataSource, tb_个案随访表.备注);

            //非编辑项
            this.txt所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_个案随访表.所属机构].ToString());
            this.txt创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_个案随访表.创建机构].ToString());
            this.txt创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_个案随访表.创建人].ToString());
            this.txt创建时间.Text = dataSource.Rows[0][tb_个案随访表.创建时间].ToString();
            this.txt修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_个案随访表.修改人].ToString());
            this.txt修改时间.Text = dataSource.Rows[0][tb_个案随访表.修改时间].ToString();
        }

        private void chk艾滋病人_CheckedChanged(object sender, EventArgs e)
        {
            if (chk艾滋病人.Checked)
            {
                this.date艾滋病确诊日期.Enabled = true;
            }
            else
            {
                this.date艾滋病确诊日期.Enabled = false;
                this.date艾滋病确诊日期.EditValue = "";
            }
        }

        private void radio是否死亡_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio是否死亡.EditValue != null && radio是否死亡.EditValue.ToString() == "1")//是
            {
                this.date死亡日期.Enabled = true;
                this.flow主要死因.Enabled = true;
            }
            else
            {
                this.date死亡日期.Enabled = false;
                this.flow主要死因.Enabled = false;
                this.date死亡日期.EditValue = "";
                this.chk主要死因1.Checked = false;
                this.chk主要死因2.Checked = false;
                this.chk主要死因3.Checked = false;
                this.chk主要死因4.Checked = false;

            }
        }

        private void radio是否为母婴传播病例_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio是否为母婴传播病例.EditValue != null && radio是否为母婴传播病例.EditValue.ToString() == "1")//是
            {
                this.txt阳性母亲卡片编号.Enabled = true;
            }
            else
            {
                this.txt阳性母亲卡片编号.Enabled = false;
                this.txt阳性母亲卡片编号.Text = "";
            }
        }

        private void radio是否有配偶_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio是否有配偶.EditValue != null && radio是否有配偶.EditValue.ToString() == "1")//是
            {
              this.radio配偶变更及性行为情况.Enabled=true;
              this.radio配偶感染状况.Enabled=true;
              this.date检测日期.Enabled=true;
              this.txt阳性配偶卡片编号.Enabled = true;
            }
            else
            {
                this.radio配偶变更及性行为情况.Enabled = false;
                this.radio配偶感染状况.Enabled = false;
                this.date检测日期.Enabled = false;
                this.txt阳性配偶卡片编号.Enabled = false;
                this.radio配偶变更及性行为情况.EditValue=null;
                this.radio配偶感染状况.EditValue=null;
                this.date检测日期.EditValue = "";
                this.txt阳性配偶卡片编号.Text="";
            }
        }
        private void radio配偶感染状况_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio配偶感染状况.EditValue== null || radio配偶感染状况.EditValue.ToString() == "1")//未查/不详
            {
                this.date检测日期.Enabled = false;
                this.txt阳性配偶卡片编号.Enabled = false;
                this.date检测日期.EditValue = "";
                this.txt阳性配偶卡片编号.Text = "";
            }
            else
            {
                this.date检测日期.Enabled = true;
                this.txt阳性配偶卡片编号.Enabled = true;
            }
        }

        private void radio是否有固定性伴侣_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio是否有固定性伴侣.EditValue != null && radio是否有固定性伴侣.EditValue.ToString() == "1")//是
            { 
                this.txt异性固定性伴人数.Enabled=true;
                this.txt异性固定性伴已检测人数.Enabled=true;
                this.txt同性固定性伴人数.Enabled=true;
                this.txt同性固定性伴已检测人数.Enabled=true;
                this.txt3个月性行为人数.Enabled=true;
                this.radio是否每次性行为时都用安全套.Enabled=true;
                this.radio是否每次与固定性伴性行为时都用安全套.Enabled = true;
            }
            else
            {
                this.txt异性固定性伴人数.Enabled = false;
                this.txt异性固定性伴已检测人数.Enabled = false;
                this.txt同性固定性伴人数.Enabled = false;
                this.txt同性固定性伴已检测人数.Enabled = false;
                this.txt3个月性行为人数.Enabled = false;
                this.radio是否每次性行为时都用安全套.Enabled = false;
                this.radio是否每次与固定性伴性行为时都用安全套.Enabled = false;
                this.txt异性固定性伴人数.Text = "";
                this.txt异性固定性伴已检测人数.Text = "";
                this.txt同性固定性伴人数.Text = "";
                this.txt同性固定性伴已检测人数.Text = "";
                this.txt3个月性行为人数.Text = "";
                this.radio是否每次性行为时都用安全套.EditValue = null;
                this.radio是否每次与固定性伴性行为时都用安全套.EditValue = null;
            }
        }
        private void radio是否接受过结核病检查_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio是否接受过结核病检查.EditValue != null && radio是否接受过结核病检查.EditValue.ToString() == "1")
            {
                this.radio结核病检查结果.Enabled = true;
            }
            else
            {
                this.radio结核病检查结果.Enabled = false;
                this.radio结核病检查结果.EditValue = null;
            }
        }
        private void radio是否接受免费治疗_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radio是否接受免费治疗.EditValue != null && radio是否接受免费治疗.EditValue.ToString() == "1")
            {
                this.txt抗病毒治疗编号.Enabled = true;
            }  
            else
            {
                this.txt抗病毒治疗编号.Enabled = false;
                this.txt抗病毒治疗编号.Text = "";
            }
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            UpdateLastControl();

            if (!ValidatingSummaryData())
            {
                return; //检查主表数据合法性
            }
            if (!Msg.AskQuestion("信息保存后，‘随访日期’将不允许修改，确认保存信息？"))
            {
                return;
            }
            if (_UpdateType == UpdateType.None)
            {
                return;
            }

            string str查无此人 = "";
            if (chk以后无需随访.Checked) { str查无此人 += "1"; }
            _BLL.DataBinder.Rows[0][tb_个案随访表.查无此人] = str查无此人;

            #region 病程阶段
            string str病程阶段 = "";
            if (chk病毒感染者.Checked) { str病程阶段 += "1,"; }
            if (chk艾滋病人.Checked) { str病程阶段 += "2,"; }
            if (str病程阶段.Length >= 1)
            {
                str病程阶段 = str病程阶段.Remove(str病程阶段.Length - 1);
            }
            _BLL.DataBinder.Rows[0][tb_个案随访表.病程阶段] = str病程阶段;
            #endregion

            #region    主要死因
            string str主要死因 = "";
            if (chk主要死因1.Checked) { str主要死因 += "1,"; }
            if (chk主要死因2.Checked) { str主要死因 += "2,"; }
            if (chk主要死因3.Checked) { str主要死因 += "3,"; }
            if (chk主要死因4.Checked) { str主要死因 += "4,"; }
            if (str主要死因.Length >= 1)
            {
                str主要死因 = str主要死因.Remove(str主要死因.Length - 1);
            }
            _BLL.DataBinder.Rows[0][tb_个案随访表.主要死因] = str主要死因;
            #endregion


            if (_UpdateType == UpdateType.Modify)
            {

                _BLL.WriteLog(); //注意:只有修改状态下保存修改日志
            }

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据
            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法
            if (result.Success)
            {
                Msg.ShowInformation("保存成功!");
                this._UpdateType = UpdateType.None; // 最后情况操作状态
                //保存后跳转到显示页面
                UC个案随访表_显示 control = new UC个案随访表_显示(_dr个人档案信息, _BLL.DataBinder.Rows[0][tb_个案随访表.创建时间]);
                ShowControl(control, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("保存失败!" + result.Description);
            }

        }

        private bool ValidatingSummaryData()
        {
            if (string.IsNullOrEmpty(ConvertEx.ToString(date随访日期.Text)))
            {
                Msg.Warning("随访日期不能为空!");
                date随访日期.Focus();
                return false;
            }

            if (this.date随访日期.DateTime > Convert.ToDateTime(txt创建时间.Text))
            {
                Msg.Warning("随访日期不能大于填写日期!");
                date随访日期.Focus();
                return false;
            }
            return true;
        }





    }
}
