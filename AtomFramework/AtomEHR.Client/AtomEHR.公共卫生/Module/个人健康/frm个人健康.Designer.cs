﻿namespace AtomEHR.公共卫生.Module.个人健康
{
    partial class frm个人健康
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.nav家庭健康信息 = new DevExpress.XtraNavBar.NavBarGroup();
            this.nav家庭成员信息 = new DevExpress.XtraNavBar.NavBarGroup();
            this.nav体检就诊信息 = new DevExpress.XtraNavBar.NavBarGroup();
            this.barItem健康体检表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem接诊记录表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem会诊记录表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem食盐摄入情况 = new DevExpress.XtraNavBar.NavBarItem();
            this.nav儿童健康信息 = new DevExpress.XtraNavBar.NavBarGroup();
            this.barItem儿童基本信息 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem新生儿家庭访视记录表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_满月 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_3月 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_6月 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_8月 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_12月 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_18月 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_24月 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_30月 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_3岁 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_4岁 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_5岁 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童健康检查记录表_6岁 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem儿童入托信息表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem生长发育监测图 = new DevExpress.XtraNavBar.NavBarItem();
            this.nav妇女健康管理 = new DevExpress.XtraNavBar.NavBarGroup();
            this.barItem妇女保健检查表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem更年期保健检查表 = new DevExpress.XtraNavBar.NavBarItem();
            this.nav孕产妇健康管理 = new DevExpress.XtraNavBar.NavBarGroup();
            this.barItem孕妇基本信息 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem产前一次随访 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem产前二次随访 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem产前三次随访 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem产前四次随访 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem产前五次随访 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem产后访视记录表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem产后42天健康检查记录表 = new DevExpress.XtraNavBar.NavBarItem();
            this.nav老年人健康管理 = new DevExpress.XtraNavBar.NavBarGroup();
            this.barItem老年人生活自理能力评估表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem老年人中医药健康管理 = new DevExpress.XtraNavBar.NavBarItem();
            this.nav预防接种信息 = new DevExpress.XtraNavBar.NavBarGroup();
            this.barItem预防接种记录 = new DevExpress.XtraNavBar.NavBarItem();
            this.nav慢性病患者健康信息 = new DevExpress.XtraNavBar.NavBarGroup();
            this.barItem高血压患者管理 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem高血压患者随访服务记录表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem高血压患者随访服务记录表2 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem糖尿病患者管理卡 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem糖尿病患者随访服务记录表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem脑卒中患者管理卡 = new DevExpress.XtraNavBar.NavBarItem();
            this.BarItem脑卒中患者随访服务记录表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem冠心病患者管理卡 = new DevExpress.XtraNavBar.NavBarItem();
            this.BarItem冠心病患者随访服务记录表 = new DevExpress.XtraNavBar.NavBarItem();
            this.BarItem高血压高危人群干预随访表 = new DevExpress.XtraNavBar.NavBarItem();
            this.nav重性精神疾病患者健康信息 = new DevExpress.XtraNavBar.NavBarGroup();
            this.barItem精神疾病患者补充表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem精神疾病患者随访表 = new DevExpress.XtraNavBar.NavBarItem();
            this.nav残疾人健康信息 = new DevExpress.XtraNavBar.NavBarGroup();
            this.barItem听力语言残疾随访表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem肢体残疾随访表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem智力残疾随访表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem视力残疾随访表 = new DevExpress.XtraNavBar.NavBarItem();
            this.barItem残疾人康复服务随访记录表 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem肺结核第一次 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem肺结核后续随访 = new DevExpress.XtraNavBar.NavBarItem();
            this.nav艾滋病患者健康信息 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem个案随访表 = new DevExpress.XtraNavBar.NavBarItem();
            this.splitterControl1 = new DevExpress.XtraEditors.SplitterControl();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::AtomEHR.公共卫生.Module.个人健康.WaitForm1), true, true);
            this.panelControl2 = new System.Windows.Forms.Panel();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.gc干预提醒 = new DevExpress.XtraGrid.GridControl();
            this.gv干预提醒 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col业务表单 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.col缺项 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.col完整度 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc干预提醒)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv干预提醒)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            this.SuspendLayout();
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.nav家庭健康信息;
            this.navBarControl1.Appearance.GroupHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.navBarControl1.Appearance.GroupHeader.Options.UseFont = true;
            this.navBarControl1.Appearance.GroupHeaderActive.ForeColor = System.Drawing.Color.DarkRed;
            this.navBarControl1.Appearance.GroupHeaderActive.Options.UseForeColor = true;
            this.navBarControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.nav家庭健康信息,
            this.nav家庭成员信息,
            this.nav体检就诊信息,
            this.nav儿童健康信息,
            this.nav妇女健康管理,
            this.nav孕产妇健康管理,
            this.nav老年人健康管理,
            this.nav预防接种信息,
            this.nav慢性病患者健康信息,
            this.nav重性精神疾病患者健康信息,
            this.nav残疾人健康信息,
            this.navBarGroup1,
            this.nav艾滋病患者健康信息});
            this.navBarControl1.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.barItem健康体检表,
            this.barItem接诊记录表,
            this.barItem会诊记录表,
            this.barItem儿童基本信息,
            this.barItem新生儿家庭访视记录表,
            this.barItem高血压患者管理,
            this.barItem高血压患者随访服务记录表,
            this.barItem高血压患者随访服务记录表2,
            this.barItem糖尿病患者管理卡,
            this.barItem糖尿病患者随访服务记录表,
            this.barItem脑卒中患者管理卡,
            this.BarItem脑卒中患者随访服务记录表,
            this.barItem冠心病患者管理卡,
            this.BarItem冠心病患者随访服务记录表,
            this.barItem儿童健康检查记录表_满月,
            this.barItem儿童健康检查记录表_3月,
            this.barItem儿童健康检查记录表_6月,
            this.barItem儿童健康检查记录表_8月,
            this.barItem儿童健康检查记录表_12月,
            this.barItem儿童健康检查记录表_18月,
            this.barItem儿童健康检查记录表_24月,
            this.barItem儿童健康检查记录表_30月,
            this.barItem儿童健康检查记录表_3岁,
            this.barItem儿童健康检查记录表_4岁,
            this.barItem儿童健康检查记录表_5岁,
            this.barItem儿童健康检查记录表_6岁,
            this.barItem儿童入托信息表,
            this.barItem妇女保健检查表,
            this.barItem更年期保健检查表,
            this.barItem老年人生活自理能力评估表,
            this.barItem老年人中医药健康管理,
            this.barItem预防接种记录,
            this.barItem听力语言残疾随访表,
            this.barItem肢体残疾随访表,
            this.barItem智力残疾随访表,
            this.barItem视力残疾随访表,
            this.barItem精神疾病患者补充表,
            this.barItem精神疾病患者随访表,
            this.barItem孕妇基本信息,
            this.barItem产前一次随访,
            this.barItem产前二次随访,
            this.barItem产前三次随访,
            this.barItem产前四次随访,
            this.barItem产前五次随访,
            this.barItem产后访视记录表,
            this.barItem产后42天健康检查记录表,
            this.barItem残疾人康复服务随访记录表,
            this.navBarItem肺结核第一次,
            this.navBarItem肺结核后续随访,
            this.navBarItem个案随访表,
            this.BarItem高血压高危人群干预随访表,
            this.barItem食盐摄入情况,
            this.barItem生长发育监测图});
            this.navBarControl1.Location = new System.Drawing.Point(0, 0);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 199;
            this.navBarControl1.Size = new System.Drawing.Size(199, 509);
            this.navBarControl1.TabIndex = 1;
            this.navBarControl1.Text = "navBarControl1";
            this.navBarControl1.Click += new System.EventHandler(this.navBarControl1_Click);
            this.navBarControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.navBarControl1_MouseClick);
            // 
            // nav家庭健康信息
            // 
            this.nav家庭健康信息.Caption = "家庭健康信息";
            this.nav家庭健康信息.Name = "nav家庭健康信息";
            // 
            // nav家庭成员信息
            // 
            this.nav家庭成员信息.Caption = "家庭成员基本信息";
            this.nav家庭成员信息.Expanded = true;
            this.nav家庭成员信息.Name = "nav家庭成员信息";
            // 
            // nav体检就诊信息
            // 
            this.nav体检就诊信息.Caption = "体检就诊信息";
            this.nav体检就诊信息.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem健康体检表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem接诊记录表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem会诊记录表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem食盐摄入情况)});
            this.nav体检就诊信息.Name = "nav体检就诊信息";
            // 
            // barItem健康体检表
            // 
            this.barItem健康体检表.Appearance.BackColor = System.Drawing.Color.Blue;
            this.barItem健康体检表.Appearance.Options.UseBackColor = true;
            this.barItem健康体检表.Caption = "健康体检表";
            this.barItem健康体检表.Name = "barItem健康体检表";
            this.barItem健康体检表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem接诊记录表
            // 
            this.barItem接诊记录表.Caption = "接诊记录表";
            this.barItem接诊记录表.Name = "barItem接诊记录表";
            this.barItem接诊记录表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem会诊记录表
            // 
            this.barItem会诊记录表.Caption = "会诊记录表";
            this.barItem会诊记录表.Name = "barItem会诊记录表";
            this.barItem会诊记录表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem食盐摄入情况
            // 
            this.barItem食盐摄入情况.Caption = "食盐摄入情况";
            this.barItem食盐摄入情况.Name = "barItem食盐摄入情况";
            this.barItem食盐摄入情况.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // nav儿童健康信息
            // 
            this.nav儿童健康信息.Caption = "儿童健康信息";
            this.nav儿童健康信息.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童基本信息),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem新生儿家庭访视记录表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_满月),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_3月),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_6月),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_8月),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_12月),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_18月),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_24月),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_30月),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_3岁),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_4岁),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_5岁),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童健康检查记录表_6岁),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem儿童入托信息表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem生长发育监测图)});
            this.nav儿童健康信息.Name = "nav儿童健康信息";
            // 
            // barItem儿童基本信息
            // 
            this.barItem儿童基本信息.Caption = "儿童基本信息";
            this.barItem儿童基本信息.Name = "barItem儿童基本信息";
            this.barItem儿童基本信息.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem6_LinkClicked);
            // 
            // barItem新生儿家庭访视记录表
            // 
            this.barItem新生儿家庭访视记录表.Caption = "新生儿家庭访视记录表";
            this.barItem新生儿家庭访视记录表.Name = "barItem新生儿家庭访视记录表";
            this.barItem新生儿家庭访视记录表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBar新生儿访视_LinkClicked);
            // 
            // barItem儿童健康检查记录表_满月
            // 
            this.barItem儿童健康检查记录表_满月.Caption = "儿童健康检查记录表(满月)";
            this.barItem儿童健康检查记录表_满月.Name = "barItem儿童健康检查记录表_满月";
            this.barItem儿童健康检查记录表_满月.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_3月
            // 
            this.barItem儿童健康检查记录表_3月.Caption = "儿童健康检查记录表(3月)";
            this.barItem儿童健康检查记录表_3月.Name = "barItem儿童健康检查记录表_3月";
            this.barItem儿童健康检查记录表_3月.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_6月
            // 
            this.barItem儿童健康检查记录表_6月.Caption = "儿童健康检查记录表(6月)";
            this.barItem儿童健康检查记录表_6月.Name = "barItem儿童健康检查记录表_6月";
            this.barItem儿童健康检查记录表_6月.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_8月
            // 
            this.barItem儿童健康检查记录表_8月.Caption = "儿童健康检查记录表(8月)";
            this.barItem儿童健康检查记录表_8月.Name = "barItem儿童健康检查记录表_8月";
            this.barItem儿童健康检查记录表_8月.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_12月
            // 
            this.barItem儿童健康检查记录表_12月.Caption = "儿童健康检查记录表(12月)";
            this.barItem儿童健康检查记录表_12月.Name = "barItem儿童健康检查记录表_12月";
            this.barItem儿童健康检查记录表_12月.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_18月
            // 
            this.barItem儿童健康检查记录表_18月.Caption = "儿童健康检查记录表(18月)";
            this.barItem儿童健康检查记录表_18月.Name = "barItem儿童健康检查记录表_18月";
            this.barItem儿童健康检查记录表_18月.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_24月
            // 
            this.barItem儿童健康检查记录表_24月.Caption = "儿童健康检查记录表(24月)";
            this.barItem儿童健康检查记录表_24月.Name = "barItem儿童健康检查记录表_24月";
            this.barItem儿童健康检查记录表_24月.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_30月
            // 
            this.barItem儿童健康检查记录表_30月.Caption = "儿童健康检查记录表(30月)";
            this.barItem儿童健康检查记录表_30月.Name = "barItem儿童健康检查记录表_30月";
            this.barItem儿童健康检查记录表_30月.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_3岁
            // 
            this.barItem儿童健康检查记录表_3岁.Caption = "儿童健康检查记录表(3岁)";
            this.barItem儿童健康检查记录表_3岁.Name = "barItem儿童健康检查记录表_3岁";
            this.barItem儿童健康检查记录表_3岁.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_4岁
            // 
            this.barItem儿童健康检查记录表_4岁.Caption = "儿童健康检查记录表(4岁)";
            this.barItem儿童健康检查记录表_4岁.Name = "barItem儿童健康检查记录表_4岁";
            this.barItem儿童健康检查记录表_4岁.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_5岁
            // 
            this.barItem儿童健康检查记录表_5岁.Caption = "儿童健康检查记录表(5岁)";
            this.barItem儿童健康检查记录表_5岁.Name = "barItem儿童健康检查记录表_5岁";
            this.barItem儿童健康检查记录表_5岁.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童健康检查记录表_6岁
            // 
            this.barItem儿童健康检查记录表_6岁.Caption = "儿童健康检查记录表(6岁)";
            this.barItem儿童健康检查记录表_6岁.Name = "barItem儿童健康检查记录表_6岁";
            this.barItem儿童健康检查记录表_6岁.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem儿童入托信息表
            // 
            this.barItem儿童入托信息表.Caption = "儿童入托信息表";
            this.barItem儿童入托信息表.Name = "barItem儿童入托信息表";
            // 
            // barItem生长发育监测图
            // 
            this.barItem生长发育监测图.Caption = "生长发育监测图";
            this.barItem生长发育监测图.Name = "barItem生长发育监测图";
            this.barItem生长发育监测图.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // nav妇女健康管理
            // 
            this.nav妇女健康管理.Caption = "妇女健康管理";
            this.nav妇女健康管理.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem妇女保健检查表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem更年期保健检查表)});
            this.nav妇女健康管理.Name = "nav妇女健康管理";
            // 
            // barItem妇女保健检查表
            // 
            this.barItem妇女保健检查表.Caption = "妇女保健检查表";
            this.barItem妇女保健检查表.Name = "barItem妇女保健检查表";
            this.barItem妇女保健检查表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem更年期保健检查表
            // 
            this.barItem更年期保健检查表.Caption = "更年期保健检查表";
            this.barItem更年期保健检查表.Name = "barItem更年期保健检查表";
            this.barItem更年期保健检查表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // nav孕产妇健康管理
            // 
            this.nav孕产妇健康管理.Caption = "孕产妇健康管理";
            this.nav孕产妇健康管理.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem孕妇基本信息),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem产前一次随访),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem产前二次随访),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem产前三次随访),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem产前四次随访),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem产前五次随访),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem产后访视记录表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem产后42天健康检查记录表)});
            this.nav孕产妇健康管理.Name = "nav孕产妇健康管理";
            // 
            // barItem孕妇基本信息
            // 
            this.barItem孕妇基本信息.Caption = "孕产妇基本信息";
            this.barItem孕妇基本信息.Name = "barItem孕妇基本信息";
            this.barItem孕妇基本信息.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem产前一次随访
            // 
            this.barItem产前一次随访.Caption = "第1次产前随访服务记录表";
            this.barItem产前一次随访.Name = "barItem产前一次随访";
            this.barItem产前一次随访.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem产前二次随访
            // 
            this.barItem产前二次随访.Caption = "第2次产前随访服务记录表";
            this.barItem产前二次随访.Name = "barItem产前二次随访";
            this.barItem产前二次随访.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem产前三次随访
            // 
            this.barItem产前三次随访.Caption = "第3次产前随访服务记录表";
            this.barItem产前三次随访.Name = "barItem产前三次随访";
            this.barItem产前三次随访.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem产前四次随访
            // 
            this.barItem产前四次随访.Caption = "第4次产前随访服务记录表";
            this.barItem产前四次随访.Name = "barItem产前四次随访";
            this.barItem产前四次随访.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem产前五次随访
            // 
            this.barItem产前五次随访.Caption = "第5次产前随访服务记录表";
            this.barItem产前五次随访.Name = "barItem产前五次随访";
            this.barItem产前五次随访.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem产后访视记录表
            // 
            this.barItem产后访视记录表.Caption = "产后访视记录表 ";
            this.barItem产后访视记录表.Name = "barItem产后访视记录表";
            this.barItem产后访视记录表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem产后42天健康检查记录表
            // 
            this.barItem产后42天健康检查记录表.Caption = "产后42天健康检查记录表";
            this.barItem产后42天健康检查记录表.Name = "barItem产后42天健康检查记录表";
            this.barItem产后42天健康检查记录表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // nav老年人健康管理
            // 
            this.nav老年人健康管理.Caption = "老年人健康管理";
            this.nav老年人健康管理.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem老年人生活自理能力评估表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem老年人中医药健康管理)});
            this.nav老年人健康管理.Name = "nav老年人健康管理";
            // 
            // barItem老年人生活自理能力评估表
            // 
            this.barItem老年人生活自理能力评估表.Caption = "老年人生活自理能力评估表*";
            this.barItem老年人生活自理能力评估表.Name = "barItem老年人生活自理能力评估表";
            this.barItem老年人生活自理能力评估表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem老年人中医药健康管理
            // 
            this.barItem老年人中医药健康管理.Caption = "老年人中医药健康管理";
            this.barItem老年人中医药健康管理.Name = "barItem老年人中医药健康管理";
            this.barItem老年人中医药健康管理.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // nav预防接种信息
            // 
            this.nav预防接种信息.Caption = "预防接种信息";
            this.nav预防接种信息.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem预防接种记录)});
            this.nav预防接种信息.Name = "nav预防接种信息";
            // 
            // barItem预防接种记录
            // 
            this.barItem预防接种记录.Caption = "预防接种记录";
            this.barItem预防接种记录.Name = "barItem预防接种记录";
            this.barItem预防接种记录.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // nav慢性病患者健康信息
            // 
            this.nav慢性病患者健康信息.Caption = "慢性病患者健康信息";
            this.nav慢性病患者健康信息.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem高血压患者管理),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem高血压患者随访服务记录表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem高血压患者随访服务记录表2),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem糖尿病患者管理卡),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem糖尿病患者随访服务记录表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem脑卒中患者管理卡),
            new DevExpress.XtraNavBar.NavBarItemLink(this.BarItem脑卒中患者随访服务记录表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem冠心病患者管理卡),
            new DevExpress.XtraNavBar.NavBarItemLink(this.BarItem冠心病患者随访服务记录表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.BarItem高血压高危人群干预随访表)});
            this.nav慢性病患者健康信息.Name = "nav慢性病患者健康信息";
            // 
            // barItem高血压患者管理
            // 
            this.barItem高血压患者管理.Caption = "高血压患者管理卡*";
            this.barItem高血压患者管理.Name = "barItem高血压患者管理";
            this.barItem高血压患者管理.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem高血压患者随访服务记录表
            // 
            this.barItem高血压患者随访服务记录表.Caption = "高血压患者随访服务记录表";
            this.barItem高血压患者随访服务记录表.Name = "barItem高血压患者随访服务记录表";
            this.barItem高血压患者随访服务记录表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem高血压患者随访服务记录表2
            // 
            this.barItem高血压患者随访服务记录表2.Caption = "高血压患者随访服务记录表(二)";
            this.barItem高血压患者随访服务记录表2.Name = "barItem高血压患者随访服务记录表2";
            this.barItem高血压患者随访服务记录表2.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem糖尿病患者管理卡
            // 
            this.barItem糖尿病患者管理卡.Caption = "糖尿病患者管理卡*";
            this.barItem糖尿病患者管理卡.Name = "barItem糖尿病患者管理卡";
            this.barItem糖尿病患者管理卡.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem糖尿病患者随访服务记录表
            // 
            this.barItem糖尿病患者随访服务记录表.Caption = "糖尿病患者随访服务记录表";
            this.barItem糖尿病患者随访服务记录表.Name = "barItem糖尿病患者随访服务记录表";
            this.barItem糖尿病患者随访服务记录表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem脑卒中患者管理卡
            // 
            this.barItem脑卒中患者管理卡.Caption = "脑卒中患者管理卡*";
            this.barItem脑卒中患者管理卡.Name = "barItem脑卒中患者管理卡";
            this.barItem脑卒中患者管理卡.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // BarItem脑卒中患者随访服务记录表
            // 
            this.BarItem脑卒中患者随访服务记录表.Caption = "脑卒中患者随访服务记录表";
            this.BarItem脑卒中患者随访服务记录表.Name = "BarItem脑卒中患者随访服务记录表";
            this.BarItem脑卒中患者随访服务记录表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem冠心病患者管理卡
            // 
            this.barItem冠心病患者管理卡.Caption = "冠心病患者管理卡*";
            this.barItem冠心病患者管理卡.Name = "barItem冠心病患者管理卡";
            this.barItem冠心病患者管理卡.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // BarItem冠心病患者随访服务记录表
            // 
            this.BarItem冠心病患者随访服务记录表.Caption = "冠心病患者随访服务记录表";
            this.BarItem冠心病患者随访服务记录表.Name = "BarItem冠心病患者随访服务记录表";
            this.BarItem冠心病患者随访服务记录表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // BarItem高血压高危人群干预随访表
            // 
            this.BarItem高血压高危人群干预随访表.Caption = "高血压高危人群干预随访表";
            this.BarItem高血压高危人群干预随访表.Name = "BarItem高血压高危人群干预随访表";
            this.BarItem高血压高危人群干预随访表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // nav重性精神疾病患者健康信息
            // 
            this.nav重性精神疾病患者健康信息.Caption = "重性精神疾病患者健康信息";
            this.nav重性精神疾病患者健康信息.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem精神疾病患者补充表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem精神疾病患者随访表)});
            this.nav重性精神疾病患者健康信息.Name = "nav重性精神疾病患者健康信息";
            // 
            // barItem精神疾病患者补充表
            // 
            this.barItem精神疾病患者补充表.Caption = "精神疾病患者补充表";
            this.barItem精神疾病患者补充表.Name = "barItem精神疾病患者补充表";
            this.barItem精神疾病患者补充表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem精神疾病患者随访表
            // 
            this.barItem精神疾病患者随访表.Caption = "精神疾病患者随访表";
            this.barItem精神疾病患者随访表.Name = "barItem精神疾病患者随访表";
            this.barItem精神疾病患者随访表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // nav残疾人健康信息
            // 
            this.nav残疾人健康信息.Caption = "残疾人健康信息";
            this.nav残疾人健康信息.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem听力语言残疾随访表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem肢体残疾随访表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem智力残疾随访表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem视力残疾随访表),
            new DevExpress.XtraNavBar.NavBarItemLink(this.barItem残疾人康复服务随访记录表)});
            this.nav残疾人健康信息.Name = "nav残疾人健康信息";
            // 
            // barItem听力语言残疾随访表
            // 
            this.barItem听力语言残疾随访表.Caption = "听力语言残疾随访表";
            this.barItem听力语言残疾随访表.Name = "barItem听力语言残疾随访表";
            this.barItem听力语言残疾随访表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem肢体残疾随访表
            // 
            this.barItem肢体残疾随访表.Caption = "肢体残疾随访表";
            this.barItem肢体残疾随访表.Name = "barItem肢体残疾随访表";
            this.barItem肢体残疾随访表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem智力残疾随访表
            // 
            this.barItem智力残疾随访表.Caption = "智力残疾随访表";
            this.barItem智力残疾随访表.Name = "barItem智力残疾随访表";
            this.barItem智力残疾随访表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem视力残疾随访表
            // 
            this.barItem视力残疾随访表.Caption = "视力残疾随访表";
            this.barItem视力残疾随访表.Name = "barItem视力残疾随访表";
            this.barItem视力残疾随访表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // barItem残疾人康复服务随访记录表
            // 
            this.barItem残疾人康复服务随访记录表.Caption = "残疾人康复服务随访记录表";
            this.barItem残疾人康复服务随访记录表.Name = "barItem残疾人康复服务随访记录表";
            this.barItem残疾人康复服务随访记录表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "肺结核患者健康信息";
            this.navBarGroup1.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem肺结核第一次),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem肺结核后续随访)});
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // navBarItem肺结核第一次
            // 
            this.navBarItem肺结核第一次.Caption = "第一次随访记录表";
            this.navBarItem肺结核第一次.Name = "navBarItem肺结核第一次";
            this.navBarItem肺结核第一次.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // navBarItem肺结核后续随访
            // 
            this.navBarItem肺结核后续随访.Caption = "后续随访记录表";
            this.navBarItem肺结核后续随访.Name = "navBarItem肺结核后续随访";
            this.navBarItem肺结核后续随访.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // nav艾滋病患者健康信息
            // 
            this.nav艾滋病患者健康信息.Caption = "艾滋病患者健康信息";
            this.nav艾滋病患者健康信息.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem个案随访表)});
            this.nav艾滋病患者健康信息.Name = "nav艾滋病患者健康信息";
            // 
            // navBarItem个案随访表
            // 
            this.navBarItem个案随访表.Caption = "个案随访表";
            this.navBarItem个案随访表.Name = "navBarItem个案随访表";
            this.navBarItem个案随访表.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.barItem_LinkClicked);
            // 
            // splitterControl1
            // 
            this.splitterControl1.Location = new System.Drawing.Point(207, 0);
            this.splitterControl1.Name = "splitterControl1";
            this.splitterControl1.Size = new System.Drawing.Size(5, 536);
            this.splitterControl1.TabIndex = 2;
            this.splitterControl1.TabStop = false;
            // 
            // panelControl2
            // 
            this.panelControl2.BackColor = System.Drawing.Color.White;
            this.panelControl2.Location = new System.Drawing.Point(218, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(574, 536);
            this.panelControl2.TabIndex = 3;
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.pictureEdit1);
            this.panelControl1.Location = new System.Drawing.Point(949, 59);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(33, 155);
            this.panelControl1.TabIndex = 6;
            this.panelControl1.MouseEnter += new System.EventHandler(this.panelControl1_MouseEnter);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureEdit1.EditValue = global::AtomEHR.公共卫生.Properties.Resources.jkgytx;
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Size = new System.Drawing.Size(33, 155);
            this.pictureEdit1.TabIndex = 0;
            this.pictureEdit1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureEdit1_MouseMove);
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("fe881cad-ef19-433e-81a4-1c7965883690");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.Options.ShowCloseButton = false;
            this.dockPanel1.OriginalSize = new System.Drawing.Size(207, 200);
            this.dockPanel1.Size = new System.Drawing.Size(207, 536);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.navBarControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(199, 509);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // panelControl3
            // 
            this.panelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl3.Controls.Add(this.gc干预提醒);
            this.panelControl3.Location = new System.Drawing.Point(690, 59);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(292, 241);
            this.panelControl3.TabIndex = 8;
            this.panelControl3.Visible = false;
            this.panelControl3.MouseLeave += new System.EventHandler(this.panelControl3_MouseLeave);
            // 
            // gc干预提醒
            // 
            this.gc干预提醒.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc干预提醒.Location = new System.Drawing.Point(2, 2);
            this.gc干预提醒.MainView = this.gv干预提醒;
            this.gc干预提醒.Name = "gc干预提醒";
            this.gc干预提醒.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemHyperLinkEdit3});
            this.gc干预提醒.Size = new System.Drawing.Size(288, 237);
            this.gc干预提醒.TabIndex = 0;
            this.gc干预提醒.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv干预提醒});
            this.gc干预提醒.Leave += new System.EventHandler(this.gc干预提醒_Leave);
            this.gc干预提醒.MouseLeave += new System.EventHandler(this.gc干预提醒_MouseLeave);
            // 
            // gv干预提醒
            // 
            this.gv干预提醒.Appearance.EvenRow.BackColor = System.Drawing.Color.Transparent;
            this.gv干预提醒.Appearance.EvenRow.Options.UseBackColor = true;
            this.gv干预提醒.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.gv干预提醒.Appearance.OddRow.Options.UseBackColor = true;
            this.gv干预提醒.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col业务表单,
            this.col缺项,
            this.col完整度});
            this.gv干预提醒.GridControl = this.gc干预提醒;
            this.gv干预提醒.Name = "gv干预提醒";
            this.gv干预提醒.OptionsView.ColumnAutoWidth = false;
            this.gv干预提醒.OptionsView.EnableAppearanceEvenRow = true;
            this.gv干预提醒.OptionsView.EnableAppearanceOddRow = true;
            this.gv干预提醒.OptionsView.ShowGroupExpandCollapseButtons = false;
            this.gv干预提醒.OptionsView.ShowGroupPanel = false;
            this.gv干预提醒.OptionsView.ShowIndicator = false;
            this.gv干预提醒.MouseLeave += new System.EventHandler(this.gv干预提醒_MouseLeave);
            // 
            // col业务表单
            // 
            this.col业务表单.AppearanceCell.Options.UseTextOptions = true;
            this.col业务表单.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col业务表单.AppearanceHeader.Options.UseTextOptions = true;
            this.col业务表单.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col业务表单.Caption = "业务表单";
            this.col业务表单.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.col业务表单.FieldName = "业务表单";
            this.col业务表单.Name = "col业务表单";
            this.col业务表单.OptionsColumn.ReadOnly = true;
            this.col业务表单.Visible = true;
            this.col业务表单.VisibleIndex = 0;
            this.col业务表单.Width = 129;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit1_Click);
            // 
            // col缺项
            // 
            this.col缺项.AppearanceCell.Options.UseTextOptions = true;
            this.col缺项.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col缺项.AppearanceHeader.Options.UseTextOptions = true;
            this.col缺项.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col缺项.Caption = "缺项";
            this.col缺项.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.col缺项.FieldName = "缺项";
            this.col缺项.Name = "col缺项";
            this.col缺项.OptionsColumn.ReadOnly = true;
            this.col缺项.Visible = true;
            this.col缺项.VisibleIndex = 1;
            this.col缺项.Width = 67;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit2_Click);
            // 
            // col完整度
            // 
            this.col完整度.AppearanceCell.Options.UseTextOptions = true;
            this.col完整度.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col完整度.AppearanceHeader.Options.UseTextOptions = true;
            this.col完整度.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col完整度.Caption = "完整度";
            this.col完整度.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.col完整度.FieldName = "完整度";
            this.col完整度.Name = "col完整度";
            this.col完整度.OptionsColumn.ReadOnly = true;
            this.col完整度.Visible = true;
            this.col完整度.VisibleIndex = 2;
            this.col完整度.Width = 64;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.Click += new System.EventHandler(this.repositoryItemHyperLinkEdit3_Click);
            // 
            // frm个人健康
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 536);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.splitterControl1);
            this.Controls.Add(this.dockPanel1);
            this.Controls.Add(this.panelControl3);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frm个人健康";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "个人健康";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm个人健康_FormClosed);
            this.Load += new System.EventHandler(this.frm个人健康_Load);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc干预提醒)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv干预提醒)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraEditors.SplitterControl splitterControl1;
        private DevExpress.XtraNavBar.NavBarGroup nav儿童健康信息;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童基本信息;
        private DevExpress.XtraNavBar.NavBarGroup nav家庭成员信息;
        private DevExpress.XtraNavBar.NavBarGroup nav体检就诊信息;
        private DevExpress.XtraNavBar.NavBarItem barItem健康体检表;
        private DevExpress.XtraNavBar.NavBarItem barItem接诊记录表;
        private DevExpress.XtraNavBar.NavBarItem barItem会诊记录表;
        private DevExpress.XtraNavBar.NavBarGroup nav预防接种信息;
        private DevExpress.XtraNavBar.NavBarGroup nav慢性病患者健康信息;
        private DevExpress.XtraNavBar.NavBarGroup nav重性精神疾病患者健康信息;
        private DevExpress.XtraNavBar.NavBarGroup nav残疾人健康信息;
        private DevExpress.XtraNavBar.NavBarGroup nav家庭健康信息;
        private DevExpress.XtraNavBar.NavBarItem barItem新生儿家庭访视记录表;
        private DevExpress.XtraNavBar.NavBarGroup nav妇女健康管理;
        private DevExpress.XtraNavBar.NavBarGroup nav孕产妇健康管理;
        private DevExpress.XtraNavBar.NavBarGroup nav老年人健康管理;
        private System.Windows.Forms.Panel panelControl2;
        private DevExpress.XtraNavBar.NavBarItem barItem高血压患者管理;
        private DevExpress.XtraNavBar.NavBarItem barItem高血压患者随访服务记录表;
        private DevExpress.XtraNavBar.NavBarItem barItem糖尿病患者管理卡;
        private DevExpress.XtraNavBar.NavBarItem barItem糖尿病患者随访服务记录表;
        private DevExpress.XtraNavBar.NavBarItem barItem脑卒中患者管理卡;
        private DevExpress.XtraNavBar.NavBarItem BarItem脑卒中患者随访服务记录表;
        private DevExpress.XtraNavBar.NavBarItem barItem冠心病患者管理卡;
        private DevExpress.XtraNavBar.NavBarItem BarItem冠心病患者随访服务记录表;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_满月;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_3月;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_6月;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_8月;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_12月;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_18月;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_24月;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_30月;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_3岁;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_4岁;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_5岁;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童健康检查记录表_6岁;
        private DevExpress.XtraNavBar.NavBarItem barItem儿童入托信息表;
        private DevExpress.XtraNavBar.NavBarItem barItem妇女保健检查表;
        private DevExpress.XtraNavBar.NavBarItem barItem更年期保健检查表;
        private DevExpress.XtraNavBar.NavBarItem barItem老年人生活自理能力评估表;
        private DevExpress.XtraNavBar.NavBarItem barItem老年人中医药健康管理;
        private DevExpress.XtraNavBar.NavBarItem barItem预防接种记录;
        private DevExpress.XtraNavBar.NavBarItem barItem听力语言残疾随访表;
        private DevExpress.XtraNavBar.NavBarItem barItem肢体残疾随访表;
        private DevExpress.XtraNavBar.NavBarItem barItem智力残疾随访表;
        private DevExpress.XtraNavBar.NavBarItem barItem视力残疾随访表;
        private DevExpress.XtraNavBar.NavBarItem barItem精神疾病患者补充表;
        private DevExpress.XtraNavBar.NavBarItem barItem精神疾病患者随访表;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraNavBar.NavBarItem barItem孕妇基本信息;
        private DevExpress.XtraNavBar.NavBarItem barItem产前一次随访;
        private DevExpress.XtraNavBar.NavBarItem barItem产前二次随访;
        private DevExpress.XtraNavBar.NavBarItem barItem产前三次随访;
        private DevExpress.XtraNavBar.NavBarItem barItem产前四次随访;
        private DevExpress.XtraNavBar.NavBarItem barItem产前五次随访;
        private DevExpress.XtraNavBar.NavBarItem barItem产后访视记录表;
        private DevExpress.XtraNavBar.NavBarItem barItem产后42天健康检查记录表;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraGrid.GridControl gc干预提醒;
        private DevExpress.XtraGrid.Views.Grid.GridView gv干预提醒;
        private DevExpress.XtraGrid.Columns.GridColumn col业务表单;
        private DevExpress.XtraGrid.Columns.GridColumn col缺项;
        private DevExpress.XtraGrid.Columns.GridColumn col完整度;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraNavBar.NavBarItem barItem残疾人康复服务随访记录表;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem肺结核第一次;
        private DevExpress.XtraNavBar.NavBarItem navBarItem肺结核后续随访;
        private DevExpress.XtraNavBar.NavBarGroup nav艾滋病患者健康信息;
        private DevExpress.XtraNavBar.NavBarItem navBarItem个案随访表;
        private DevExpress.XtraNavBar.NavBarItem BarItem高血压高危人群干预随访表;
        private DevExpress.XtraNavBar.NavBarItem barItem高血压患者随访服务记录表2;
        private DevExpress.XtraNavBar.NavBarItem barItem食盐摄入情况;
        private DevExpress.XtraNavBar.NavBarItem barItem生长发育监测图;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
    }
}