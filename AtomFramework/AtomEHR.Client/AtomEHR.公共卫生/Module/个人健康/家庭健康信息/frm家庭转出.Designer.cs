﻿namespace AtomEHR.公共卫生.Module.个人健康.家庭健康信息
{
    partial class frm家庭转出
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo条件 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt条件 = new DevExpress.XtraEditors.TextEdit();
            this.btn查询 = new DevExpress.XtraEditors.SimpleButton();
            this.gc = new DevExpress.XtraGrid.GridControl();
            this.gv = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col操作 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn加入 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.col新家庭与户主关系 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lkp与户主关系 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.col档案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col户主姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col出生日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col居村委会 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col详细地址 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.link加入 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo条件.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt条件.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn加入)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp与户主关系)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.link加入)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cbo条件);
            this.flowLayoutPanel1.Controls.Add(this.txt条件);
            this.flowLayoutPanel1.Controls.Add(this.btn查询);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(715, 30);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // cbo条件
            // 
            this.cbo条件.EditValue = "请选择";
            this.cbo条件.Location = new System.Drawing.Point(3, 3);
            this.cbo条件.Name = "cbo条件";
            this.cbo条件.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo条件.Properties.Items.AddRange(new object[] {
            "请选择",
            "姓名",
            "档案号",
            "身份证号"});
            this.cbo条件.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo条件.Size = new System.Drawing.Size(117, 20);
            this.cbo条件.TabIndex = 0;
            // 
            // txt条件
            // 
            this.txt条件.Location = new System.Drawing.Point(126, 3);
            this.txt条件.Name = "txt条件";
            this.txt条件.Size = new System.Drawing.Size(196, 20);
            this.txt条件.TabIndex = 1;
            // 
            // btn查询
            // 
            this.btn查询.Location = new System.Drawing.Point(328, 3);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(87, 21);
            this.btn查询.TabIndex = 2;
            this.btn查询.Text = "查询";
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // gc
            // 
            this.gc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc.Location = new System.Drawing.Point(0, 30);
            this.gc.MainView = this.gv;
            this.gc.Name = "gc";
            this.gc.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.link加入,
            this.lkp与户主关系,
            this.repositoryItemDateEdit1,
            this.btn加入});
            this.gc.Size = new System.Drawing.Size(715, 297);
            this.gc.TabIndex = 1;
            this.gc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv});
            // 
            // gv
            // 
            this.gv.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col操作,
            this.col新家庭与户主关系,
            this.col档案号,
            this.col户主姓名,
            this.col性别,
            this.col身份证号,
            this.col出生日期,
            this.col居村委会,
            this.col详细地址});
            this.gv.GridControl = this.gc;
            this.gv.Name = "gv";
            this.gv.OptionsView.ColumnAutoWidth = false;
            this.gv.OptionsView.EnableAppearanceEvenRow = true;
            this.gv.OptionsView.EnableAppearanceOddRow = true;
            this.gv.OptionsView.ShowGroupPanel = false;
            // 
            // col操作
            // 
            this.col操作.AppearanceCell.Options.UseTextOptions = true;
            this.col操作.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col操作.AppearanceHeader.Options.UseTextOptions = true;
            this.col操作.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col操作.Caption = "操作";
            this.col操作.ColumnEdit = this.btn加入;
            this.col操作.Name = "col操作";
            this.col操作.Visible = true;
            this.col操作.VisibleIndex = 0;
            // 
            // btn加入
            // 
            this.btn加入.Appearance.BackColor = System.Drawing.SystemColors.Highlight;
            this.btn加入.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btn加入.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.btn加入.Appearance.Options.UseBackColor = true;
            this.btn加入.Appearance.Options.UseFont = true;
            this.btn加入.Appearance.Options.UseForeColor = true;
            this.btn加入.AutoHeight = false;
            this.btn加入.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btn加入.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "加入", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.btn加入.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btn加入.Name = "btn加入";
            this.btn加入.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn加入.Click += new System.EventHandler(this.btn加入_Click);
            // 
            // col新家庭与户主关系
            // 
            this.col新家庭与户主关系.AppearanceCell.Options.UseTextOptions = true;
            this.col新家庭与户主关系.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col新家庭与户主关系.AppearanceHeader.Options.UseTextOptions = true;
            this.col新家庭与户主关系.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col新家庭与户主关系.Caption = "新家庭与户主关系";
            this.col新家庭与户主关系.ColumnEdit = this.lkp与户主关系;
            this.col新家庭与户主关系.FieldName = "与户主关系";
            this.col新家庭与户主关系.Name = "col新家庭与户主关系";
            this.col新家庭与户主关系.Visible = true;
            this.col新家庭与户主关系.VisibleIndex = 1;
            this.col新家庭与户主关系.Width = 130;
            // 
            // lkp与户主关系
            // 
            this.lkp与户主关系.AutoHeight = false;
            this.lkp与户主关系.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp与户主关系.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "与户主关系")});
            this.lkp与户主关系.Name = "lkp与户主关系";
            this.lkp与户主关系.NullText = "";
            // 
            // col档案号
            // 
            this.col档案号.AppearanceHeader.Options.UseTextOptions = true;
            this.col档案号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col档案号.Caption = "档案号";
            this.col档案号.FieldName = "个人档案编号";
            this.col档案号.Name = "col档案号";
            this.col档案号.OptionsColumn.AllowEdit = false;
            this.col档案号.OptionsColumn.ReadOnly = true;
            this.col档案号.Visible = true;
            this.col档案号.VisibleIndex = 2;
            // 
            // col户主姓名
            // 
            this.col户主姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col户主姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col户主姓名.Caption = "户主姓名";
            this.col户主姓名.FieldName = "姓名";
            this.col户主姓名.Name = "col户主姓名";
            this.col户主姓名.OptionsColumn.AllowEdit = false;
            this.col户主姓名.OptionsColumn.ReadOnly = true;
            this.col户主姓名.Visible = true;
            this.col户主姓名.VisibleIndex = 3;
            // 
            // col性别
            // 
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.AllowEdit = false;
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 4;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.AllowEdit = false;
            this.col身份证号.OptionsColumn.ReadOnly = true;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 5;
            // 
            // col出生日期
            // 
            this.col出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.Caption = "出生日期";
            this.col出生日期.ColumnEdit = this.repositoryItemDateEdit1;
            this.col出生日期.FieldName = "出生日期";
            this.col出生日期.Name = "col出生日期";
            this.col出生日期.OptionsColumn.AllowEdit = false;
            this.col出生日期.OptionsColumn.ReadOnly = true;
            this.col出生日期.Visible = true;
            this.col出生日期.VisibleIndex = 6;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // col居村委会
            // 
            this.col居村委会.AppearanceHeader.Options.UseTextOptions = true;
            this.col居村委会.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col居村委会.Caption = "居/村委会";
            this.col居村委会.FieldName = "居委会";
            this.col居村委会.Name = "col居村委会";
            this.col居村委会.OptionsColumn.AllowEdit = false;
            this.col居村委会.OptionsColumn.ReadOnly = true;
            this.col居村委会.Visible = true;
            this.col居村委会.VisibleIndex = 7;
            // 
            // col详细地址
            // 
            this.col详细地址.AppearanceHeader.Options.UseTextOptions = true;
            this.col详细地址.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col详细地址.Caption = "详细地址";
            this.col详细地址.FieldName = "居住地址";
            this.col详细地址.Name = "col详细地址";
            this.col详细地址.OptionsColumn.AllowEdit = false;
            this.col详细地址.OptionsColumn.ReadOnly = true;
            this.col详细地址.Visible = true;
            this.col详细地址.VisibleIndex = 8;
            // 
            // link加入
            // 
            this.link加入.AutoHeight = false;
            this.link加入.Caption = "加入";
            this.link加入.Name = "link加入";
            this.link加入.Click += new System.EventHandler(this.link加入_Click);
            // 
            // frm家庭转出
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 327);
            this.Controls.Add(this.gc);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "frm家庭转出";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "家庭转出";
            this.Load += new System.EventHandler(this.frm家庭转出_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo条件.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt条件.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn加入)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp与户主关系)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.link加入)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraGrid.GridControl gc;
        private DevExpress.XtraGrid.Views.Grid.GridView gv;
        private DevExpress.XtraEditors.ComboBoxEdit cbo条件;
        private DevExpress.XtraEditors.TextEdit txt条件;
        private DevExpress.XtraEditors.SimpleButton btn查询;
        private DevExpress.XtraGrid.Columns.GridColumn col操作;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit link加入;
        private DevExpress.XtraGrid.Columns.GridColumn col新家庭与户主关系;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lkp与户主关系;
        private DevExpress.XtraGrid.Columns.GridColumn col档案号;
        private DevExpress.XtraGrid.Columns.GridColumn col户主姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col出生日期;
        private DevExpress.XtraGrid.Columns.GridColumn col居村委会;
        private DevExpress.XtraGrid.Columns.GridColumn col详细地址;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn加入;
    }
}