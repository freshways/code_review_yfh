﻿using AtomEHR.Common;
using AtomEHR.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.个人健康.家庭健康信息
{
    public partial class frm家庭转出 : frmBase
    {
        public string _yjtdabh;//原家庭档案编号
        public string _grdabh;
        public string _jtdabh;//转入后的家庭档案编号
        AtomEHR.Business.bll家庭档案 _bll = new Business.bll家庭档案();
        public frm家庭转出()
        {
            InitializeComponent();
        }
        public frm家庭转出(string jtdabh, string grdabh)
        {
            InitializeComponent();
            _yjtdabh = jtdabh;
            _grdabh = grdabh;
        }
        private void link加入_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv.GetFocusedDataRow();
            if (row == null) return;
            _jtdabh = row["家庭档案编号"].ToString();
            string yhzgx = row["与户主关系"].ToString();

            if (_bll.fun转出(yhzgx, _jtdabh, _yjtdabh, _grdabh))
            {
                Msg.ShowInformation("转出成功！");
                this.Close();
                //成功后跳转到相应的页面
                DialogResult = System.Windows.Forms.DialogResult.OK;

            }
            else
            {
                Msg.ShowInformation("转出！");
                this.Close();
                DialogResult = System.Windows.Forms.DialogResult.No;
            }
        }

        private void btn查询_Click(object sender, EventArgs e)
        {
            if (this.cbo条件.Text.Trim() == "请选择")
            {
                Msg.ShowInformation("查询的方式必选！");
                return;
            }
            if (this.txt条件.Text.Trim() == "")
            {
                Msg.ShowInformation("查询的值必填！");
                return;
            }
            string flag = this.cbo条件.SelectedIndex.ToString();
            string txt = this.txt条件.Text.Trim();
            string query = "";
            if (flag == "1")//姓名
            {
                query += " AND (a.姓名 like'" + txt + "%') ";
            }
            if (flag == "2")//档案号
            {
                query += " AND A.个人档案编号 = '" + txt + "'";
            }
            if (flag == "3")//身份证号
            {
                query += " AND A.身份证号 = '" + txt + "'";
            }
            if (!string.IsNullOrEmpty(Loginer.CurrentUser.所属机构))
            {
                string pgrid = Loginer.CurrentUser.所属机构;
                if (pgrid.Length == 12)
                {
                    query += " AND (A.所属机构=  '" + pgrid + "' or substring(A.所属机构,1,7)+'1'+substring(A.所属机构,9,7) like '" + pgrid + "%')";
                }
                else
                {
                    query += " AND A.所属机构 like '" + pgrid + "%'";
                }
            }
            if (!string.IsNullOrEmpty(_jtdabh))
            {
                query += " AND A.家庭档案编号 <> '" + _jtdabh + "'  and 与户主关系='1'";
            }
            DataTable dt = _bll.selectRkxzlByHzxx(query, _yjtdabh);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
            }
            this.gc.DataSource = dt;
            this.gv.BestFitColumns();
        }

        private void frm家庭转出_Load(object sender, EventArgs e)
        {
            DataBinder.BindingLookupEditDataSource(lkp与户主关系, AtomEHR.Business.DataDictCache.Cache.t与户主关系_非户主, "P_DESC", "P_CODE");
            
        }

        private void btn加入_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv.GetFocusedDataRow();
            if (row == null) return;
            _jtdabh = row["家庭档案编号"].ToString();
            string yhzgx = row["与户主关系"].ToString();

            if (_bll.fun转出(yhzgx, _jtdabh, _yjtdabh, _grdabh))
            {
                Msg.ShowInformation("转出成功！");
                this.Close();
                //成功后跳转到相应的页面
                DialogResult = System.Windows.Forms.DialogResult.OK;

            }
            else
            {
                Msg.ShowInformation("转出！");
                this.Close();
                DialogResult = System.Windows.Forms.DialogResult.No;
            }
        }
    }
}
