﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.家庭健康信息
{
    public partial class UC家庭基本信息 : UserControlBase
    {

        #region Fields

        frm个人健康 _frm;
        bll健康档案 _bll健康档案;
        bll家庭档案 _bll家庭档案;
        DataSet _ds家庭成员信息;
        string _familyNo;
        UpdateType Updatestate = UpdateType.Modify;
        #endregion

        #region Constructor
        public UC家庭基本信息()
        {
            InitializeComponent();
        }
        public UC家庭基本信息(Form frm)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _familyNo = _frm._familyDocNo;
            _UpdateType = UpdateType.Modify;

            _bll健康档案 = new bll健康档案();
            _bll家庭档案 = new bll家庭档案();
            _ds家庭成员信息 = _bll家庭档案.GetBusinessByFamilNo(_familyNo, true);

        }
        #endregion

        #region Private Methods

        private void DoBindingSummaryEditor(DataSet dataSource)
        {
            //DataBinder.BindingLookupEditDataSource(cbo与户主关系, DataDictCache.Cache.t与户主关系, "");
            //throw new NotImplementedException();
            DataTable dt家庭档案 = dataSource.Tables[tb_家庭档案.__TableName];
            DataTable dt家庭成员信息 = dataSource.Tables[tb_健康档案.__TableName];

            DataBinder.BindingLookupEditDataSource(lkp与户主关系, DataDictCache.Cache.t与户主关系, "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(lkp性别, DataDictCache.Cache.t性别, "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(lkp居住状况, DataDictCache.Cache.t常住类型, "P_DESC", "P_CODE");
            //DataBinder.BindingComboEdit(cbo住房类型, dt家庭档案, tb_家庭档案.住房类型);
            if ((dt家庭档案 == null || dt家庭档案.Rows.Count <= 0) && !string.IsNullOrEmpty(_familyNo))
            {
                DataRow row家庭档案 = dt家庭档案.Rows.Add();
                row家庭档案[tb_家庭档案.家庭档案编号] = _familyNo;

                row家庭档案[tb_家庭档案.建档时间] = dt家庭成员信息.Rows[0][tb_健康档案.创建时间];
                row家庭档案[tb_家庭档案.创建人] = dt家庭成员信息.Rows[0][tb_健康档案.创建人];
                row家庭档案[tb_家庭档案.创建时间] = dt家庭成员信息.Rows[0][tb_健康档案.创建时间];
                row家庭档案[tb_家庭档案.修改人] = dt家庭成员信息.Rows[0][tb_健康档案.修改人];
                row家庭档案[tb_家庭档案.修改时间] = dt家庭成员信息.Rows[0][tb_健康档案.修改时间];
                row家庭档案[tb_家庭档案.创建机构] = dt家庭成员信息.Rows[0][tb_健康档案.创建机构];
                row家庭档案[tb_家庭档案.所属机构] = dt家庭成员信息.Rows[0][tb_健康档案.所属机构];

                Updatestate = UpdateType.Add;
                //dt家庭档案.AcceptChanges();
                //dt家庭档案.Rows[0].SetAdded();
            }
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData(dt家庭档案.Rows[0][tb_家庭档案.住房类型].ToString(), cbo住房类型);
            if (string.IsNullOrEmpty(dt家庭档案.Rows[0][tb_家庭档案.住房类型].ToString()))
            {
                this.lbl住房类型.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lbl住房类型.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack; ;
            }
            DataBinder.BindingTextEdit(txt住房面积.Txt1, dt家庭档案, tb_家庭档案.住房面积);
            if (string.IsNullOrEmpty(dt家庭档案.Rows[0][tb_家庭档案.住房面积].ToString()))
            {
                this.lbl住房面积.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lbl住房面积.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack; ;
            }
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData(dt家庭档案.Rows[0][tb_家庭档案.厕所类型].ToString(), cbo厕所类型);
            if (string.IsNullOrEmpty(dt家庭档案.Rows[0][tb_家庭档案.厕所类型].ToString()))
            {
                this.lbl厕所类型.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lbl厕所类型.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack; ;
            }
            //DataBinder.BindingComboEdit(cbo厕所类型, dt家庭档案, tb_家庭档案.厕所类型);
            DataBinder.BindingTextEdit(txt家庭人均月收入.Txt1, dt家庭档案, tb_家庭档案.人均收入);
            if (string.IsNullOrEmpty(dt家庭档案.Rows[0][tb_家庭档案.人均收入].ToString()))
            {
                this.lbl家庭人均月收入.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lbl家庭人均月收入.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack; ;
            }
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData(dt家庭档案.Rows[0][tb_家庭档案.是否低保户].ToString(), cbo是否低保户);
            if (string.IsNullOrEmpty(dt家庭档案.Rows[0][tb_家庭档案.是否低保户].ToString()))
            {
                this.lbl是否低保户.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lbl是否低保户.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack; ;
            }
            DataBinder.BindingTextEdit(txt家庭通常每个月吃油盐量.Txt1, dt家庭档案, tb_家庭档案.吃油量);
            DataBinder.BindingTextEdit(txt家庭通常每个月吃油盐量.Txt2, dt家庭档案, tb_家庭档案.吃盐量);
            if (string.IsNullOrEmpty(dt家庭档案.Rows[0][tb_家庭档案.吃油量].ToString()) && string.IsNullOrEmpty(dt家庭档案.Rows[0][tb_家庭档案.吃盐量].ToString()))
            {
                this.lbl家庭通常每个月吃油盐量.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lbl家庭通常每个月吃油盐量.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack; ;
            }
            DataBinder.BindingTextEdit(txt创建时间, dt家庭档案, tb_家庭档案.创建时间);
            DataBinder.BindingTextEdit(txt录入时间, dt家庭档案, tb_家庭档案.建档时间);
            DataBinder.BindingTextEdit(txt最近更新时间, dt家庭档案, tb_家庭档案.修改时间);

            this.txt当前所属机构.Text = _BLL.Return机构名称(dt家庭档案.Rows[0][tb_家庭档案.所属机构].ToString());
            this.txt创建机构.Text = _BLL.Return机构名称(dt家庭档案.Rows[0][tb_家庭档案.创建机构].ToString());
            this.txt录入人.Text = _BLL.Return用户名称(dt家庭档案.Rows[0][tb_家庭档案.创建人].ToString());
            this.txt最近修改人.Text = _BLL.Return用户名称(dt家庭档案.Rows[0][tb_家庭档案.修改人].ToString());

            this.lbl考核项.Text = "6";
            this.lbl未填.Text = dt家庭档案.Rows[0][tb_家庭档案.缺少项].ToString();
            this.lbl完整度.Text = dt家庭档案.Rows[0][tb_家庭档案.完整度] + "%";
            for (int i = 0; i < dt家庭成员信息.Rows.Count; i++)
            {
                dt家庭成员信息.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt家庭成员信息.Rows[i]["姓名"].ToString());
            }
            this.gc家庭成员信息.DataSource = dt家庭成员信息;
            //this.gv家庭成员信息.BestFitColumns();


        }

        private bool f保存检查()
        {

            DataTable dt = (DataTable)this.gc家庭成员信息.DataSource;
            int i = 0;
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                DataRow row = dt.Rows[j];
                string 与户主关系 = row[tb_健康档案.与户主关系].ToString();
                if (与户主关系 == "1") i++;
            }

            if (i == 0)
            {
                Msg.ShowInformation("必须选择一个用户为户主！");
                return false;
            }
            if (i > 1)
            {
                Msg.ShowInformation("只能选择一个用户为户主！");
                return false;
            }
            return true;
        }
        private void InitView()
        {
            //绑定combox数据
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t厕所类型, cbo厕所类型);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t住房类型, cbo住房类型);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t是否, cbo是否低保户);
            //AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t与户主关系, cbo与户主关系);

            this.txt家庭人均月收入.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt家庭人均月收入.Txt1.Properties.Mask.EditMask = "n";
            this.txt家庭人均月收入.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt住房面积.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt住房面积.Txt1.Properties.Mask.EditMask = "n";
            this.txt住房面积.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt家庭通常每个月吃油盐量.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt家庭通常每个月吃油盐量.Txt1.Properties.Mask.EditMask = "n";
            this.txt家庭通常每个月吃油盐量.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt家庭通常每个月吃油盐量.Txt2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt家庭通常每个月吃油盐量.Txt2.Properties.Mask.EditMask = "n";
            this.txt家庭通常每个月吃油盐量.Txt2.Properties.Mask.UseMaskAsDisplayFormat = true;
        }
        /// <summary>
        /// 保存成功后，重新刷新页面
        /// </summary>
        private void RefreshPage()
        {
            _ds家庭成员信息 = _bll健康档案.GetBusinessByFamilNo(_familyNo, true);
            DoBindingSummaryEditor(_ds家庭成员信息);
        }
        #endregion

        #region Handle Events
        private void UC家庭基本信息2_Load(object sender, EventArgs e)
        {
            InitView();
            DoBindingSummaryEditor(_ds家庭成员信息);
        }
        private void btn保存修改_Click(object sender, EventArgs e)
        {
            if (f保存检查())
            {
                _ds家庭成员信息.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.厕所类型] = AtomEHR.公共卫生.util.ControlsHelper.GetComboxKey(cbo厕所类型);
                _ds家庭成员信息.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.住房类型] = AtomEHR.公共卫生.util.ControlsHelper.GetComboxKey(cbo住房类型);
                _ds家庭成员信息.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.是否低保户] = AtomEHR.公共卫生.util.ControlsHelper.GetComboxKey(cbo是否低保户);
                int i = 0;
                if (string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(this.cbo住房类型))) i++;
                if (this.txt住房面积.Txt1.Text.Trim() == "") i++;
                if (string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(this.cbo厕所类型))) i++;
                if (this.txt家庭人均月收入.Txt1.Text.Trim() == "") i++;
                if (string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(this.cbo是否低保户))) i++;
                if (this.txt家庭通常每个月吃油盐量.Txt1.Text.Trim() == null && this.txt家庭通常每个月吃油盐量.Txt2.Text.Trim() == null) i++;

                _ds家庭成员信息.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.缺少项] = i;
                int 完整度 = Convert.ToInt32((6 - i) * 100 / 6.0);
                _ds家庭成员信息.Tables[tb_家庭档案.__TableName].Rows[0][tb_家庭档案.完整度] = 完整度;
                for (int x = 0; x < _ds家庭成员信息.Tables[tb_健康档案.__TableName].Rows.Count; x++)
                {
                    _ds家庭成员信息.Tables[tb_健康档案.__TableName].Rows[x]["姓名"] = util.DESEncrypt.DES加密(_ds家庭成员信息.Tables[tb_健康档案.__TableName].Rows[x]["姓名"].ToString(),false);
                }
                for (int j = 0; j < _ds家庭成员信息.Tables[tb_健康档案_个人健康特征.__TableName].Rows.Count; j++)
                {
                    _ds家庭成员信息.Tables[tb_健康档案_个人健康特征.__TableName].Rows[j][tb_健康档案_个人健康特征.家庭档案] = i + "," + 完整度;
                }
                //_bll健康档案.WriteLog();
                DataSet ds1 = _bll家庭档案.CreateSaveData(_ds家庭成员信息, Updatestate);
                SaveResult result = _bll家庭档案.Save(ds1);
                if (result.Success)
                {
                    Msg.ShowInformation("保存成功！");
                    RefreshPage();
                }
            }
        }
        private void btn转入家庭关系_Click(object sender, EventArgs e)
        {
            frm转入家庭成员 frm = new frm转入家庭成员(_familyNo);
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                _ds家庭成员信息 = _bll健康档案.GetBusinessByFamilNo(_familyNo, true);
                this.OnLoad(e);//重新加载页面
                _frm.LoadFamilyTree(_familyNo);
                //_frm.ReLoad(this.Name, _docNo);
            }
            //else if (frm.DialogResult == DialogResult.Cancel)//点击页面中的 新建用户档案页面
            //{
            //UC个人基本信息表 uc = new UC个人基本信息表(_frm, UpdateType.AddPeople);
            //ShowControl(uc);
            //}
        }
        private void btn注销_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv家庭成员信息.GetFocusedDataRow();
            if (row == null) return;
            string _docNo = row[tb_健康档案.个人档案编号].ToString();
            string _familyNo = row[tb_健康档案.家庭档案编号].ToString();
            string 与户主关系 = row[tb_健康档案.与户主关系].ToString();
            int 家庭成员数量 = this.gv家庭成员信息.RowCount;
            if (与户主关系 == "1")//户主
            {
                if (家庭成员数量 > 1)
                {
                    Msg.Warning("该人是家庭户主，您必须指定该家庭中另一个人为户主，才能注销此人！");
                    return;
                }
                else
                {
                    if (Msg.AskQuestion("该人是家庭户主，您注销该户主将连同家庭档案一同删除，您是否继续？"))
                    {
                        if (_bll家庭档案.fun注销(_docNo, _familyNo, 与户主关系, 家庭成员数量))
                        {
                            _frm._familyDocNo = "";
                            UC个人基本信息表_显示 uc = new UC个人基本信息表_显示(_frm);
                            ShowControl(uc, DockStyle.Fill);
                        }
                    }
                }
            }
            else
            {
                if (_bll家庭档案.fun注销(_docNo, _familyNo, 与户主关系, 家庭成员数量))//注销成功
                {
                    _ds家庭成员信息 = _bll健康档案.GetBusinessByFamilNo(_familyNo, true);
                    this.OnLoad(e);//重新加载页面
                    _frm.LoadFamilyTree(_familyNo);
                }
            }
        }
        private void btn转出_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv家庭成员信息.GetFocusedDataRow();
            if (row == null) return;
            string _docNo = row[tb_健康档案.个人档案编号].ToString();
            string _familyNo = row[tb_健康档案.家庭档案编号].ToString();
            string 与户主关系 = row[tb_健康档案.与户主关系].ToString();
            int 家庭成员数量 = this.gv家庭成员信息.RowCount;
            if (与户主关系 == "1" && 家庭成员数量 != 1)//户主
            {
                Msg.ShowInformation("此人是户主，需要修改户主关系，指定一成员为户主");
                return;
            }
            else
            {
                frm家庭转出 frm = new frm家庭转出(_familyNo, _docNo);
                frm.ShowDialog();
                if (frm.DialogResult == DialogResult.OK)//表示转出成功
                {
                    if (家庭成员数量 == 1)
                    {
                        _frm._familyDocNo = frm._jtdabh;
                        _frm.ReLoad(this.Name, _docNo);
                    }
                    else
                    {
                        _ds家庭成员信息 = _bll健康档案.GetBusinessByFamilNo(_familyNo, true);
                        this.OnLoad(e);
                        _frm.ReLoad(this.Name, _docNo);
                    }

                }
            }
        }
        #endregion

        

        private void sbtnExport1_Click(object sender, EventArgs e)
        {
            string _docNo = _familyNo;
            if (!string.IsNullOrEmpty(_docNo))
            {
                report居民健康档案 report = new report居民健康档案(_docNo);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在家庭档案编号，请确认！");
            }
        }

        private void sbtnExport2_Click(object sender, EventArgs e)
        {
            string _docNo = _familyNo;
            if (!string.IsNullOrEmpty(_docNo))
            {
                report家庭基本信息表 report = new report家庭基本信息表(_docNo);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在家庭档案编号，请确认！");
            }
        }
    }
}
