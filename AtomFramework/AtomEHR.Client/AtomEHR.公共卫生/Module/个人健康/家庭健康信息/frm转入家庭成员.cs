﻿using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.个人健康.家庭健康信息
{
    public partial class frm转入家庭成员 : frmBase
    {
        AtomEHR.Business.bll家庭档案 _bll = new Business.bll家庭档案();
        public string _jtdabh;
        public frm转入家庭成员()
        {
            InitializeComponent();
        }
        public frm转入家庭成员(string jtdabh)
        {
            InitializeComponent();
            _jtdabh = jtdabh;
        }

        private void frm转入家庭成员_Load(object sender, EventArgs e)
        {
            util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t与户主关系_非户主, cbo与户主关系, "P_CODE", "P_DESC");
        }

        private void btn查询_Click(object sender, EventArgs e)
        {
            //if (this.cbo条件.Text.Trim() == "请选择")
            //{
            //    Msg.Warning("");
            //}
            string strWhere = string.Empty;
            string index = this.cbo条件.SelectedIndex.ToString();
            if (index == "1")//姓名
            {
                strWhere = " AND (A.姓名 LIKE '%" + util.DESEncrypt.DES加密(this.txt条件.Text.Trim()) + "%' or A.姓名 like '" + this.txt条件.Text.Trim() + "%') ";
            }
            if (index == "2")//档案号
            {
                strWhere = " AND A.个人档案编号 LIKE '%" + this.txt条件.Text.Trim() + "%'";
            }
            if (index == "3")//身份证号
            {
                strWhere = " AND A.身份证号 LIKE '%" + this.txt条件.Text.Trim() + "%'";
            }
            string pgrid = Loginer.CurrentUser.所属机构;
            if (pgrid.Length == 12)
            {
                strWhere += " and ([所属机构]=  '" + pgrid + "' or substring([所属机构],1,7)+'1'+substring([所属机构],9,7) like '" + pgrid + "%')";
            }
            else
            {
                strWhere += " and [所属机构] like '" + pgrid + "%'";
            }

            DataTable dt = _bll.selectFreeJtcy(strWhere);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i]["姓名"] = util.DESEncrypt.DES解密(dt.Rows[i]["姓名"].ToString());
            }
            this.gc个人健康档案.DataSource = dt;
            this.gv个人健康档案.BestFitColumns();
        }

        private void btn加入家庭_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(util.ControlsHelper.GetComboxKey(cbo与户主关系)))
            {
                Msg.Warning("请选择与户主关系！"); return;
            }
            if (this.gv个人健康档案.SelectedRowsCount > 1 || this.gv个人健康档案.SelectedRowsCount == 0)
            {
                Msg.Warning("每次至少且只能选择一个居民进行操作！");
                return;
            }
            int[] rows = this.gv个人健康档案.GetSelectedRows();
            string yhzgx = util.ControlsHelper.GetComboxKey(this.cbo与户主关系);
            string grdabh = this.gv个人健康档案.GetRowCellValue(rows[0], "个人档案编号").ToString();
            if (_bll.addJtcy(grdabh, _jtdabh, yhzgx))
            {
                Msg.ShowInformation("添加家庭成员成功！");
                this.Close();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                Msg.ShowInformation("添加家庭成员失败，请联系管理员！");
            }

        }

        private void btn新建_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
