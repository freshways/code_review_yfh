﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.家庭健康信息
{
    public partial class report家庭基本信息表 : DevExpress.XtraReports.UI.XtraReport
    {
        string docNo;
        DataSet _ds家庭;
        bll家庭档案 _bll = new bll家庭档案();

        public report家庭基本信息表()
        {
            InitializeComponent();
        }

        public report家庭基本信息表(string _docNo)
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds家庭 = _bll.GetInfoByFamily(_docNo, true);
            DoBindingDataSource(_ds家庭);
        }

        private void DoBindingDataSource(DataSet _ds家庭)
        {
            DataTable dt家庭 = _ds家庭.Tables[Models.tb_家庭档案.__TableName];
            if (dt家庭 == null || dt家庭.Rows.Count == 0) return;

            //DataTable dt个人健康 = _ds家庭.Tables[Models.tb_健康档案_个人健康特征.__TableName];
            //if (dt个人健康 == null || dt个人健康.Rows.Count == 0) return;

            DataTable dt健康档案 = _ds家庭.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;


            //string 姓名= util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());

            //string 性别= dt家庭.Rows[0][tb_健康档案.性别].ToString();
            //string 出生日期 = Convert.ToDateTime(dt家庭.Rows[0][tb_健康档案.出生日期]).ToLongDateString().ToString();//将时间格式转换为年月日
            //string 居住状况 = dt家庭.Rows[0][tb_健康档案.居住地址].ToString();
            for (int i = 0; i < dt健康档案.Rows.Count; i++)
            {
                string yhzgx = "txt与户主" + (i + 1);
                string name = "txt姓名" + (i + 1);
                string gender = "txt姓别" + (i + 1);
                string birth = "txt出生日期" + (i + 1);
                string statu = "txt居住状况" + (i + 1);


                XRTableCell cellyhzgx = (XRTableCell)FindControl(yhzgx, false);
                XRTableCell cellname = (XRTableCell)FindControl(name, false);
                XRTableCell cellgender = (XRTableCell)FindControl(gender, false);
                XRTableCell cellbirth = (XRTableCell)FindControl(birth, false);
                XRTableCell cellstatu = (XRTableCell)FindControl(statu, false);

                cellyhzgx.Text = _bll.ReturnDis字典显示("yhzgx", dt健康档案.Rows[i][tb_健康档案.与户主关系].ToString());
                cellname.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[i][tb_健康档案.姓名].ToString());
                cellgender.Text = _bll.ReturnDis字典显示("xb_xingbie", dt健康档案.Rows[i][tb_健康档案.性别].ToString());
                cellbirth.Text = dt健康档案.Rows[i][tb_健康档案.出生日期].ToString();
                cellstatu.Text = _bll.ReturnDis字典显示("jzzk", dt健康档案.Rows[i][tb_健康档案.常住类型].ToString());


                //string 与户主关系 = dt健康档案.Rows[i][tb_健康档案.与户主关系].ToString();
                //switch (与户主关系)
                //{
                //    case "1":
                //        this.txt与户主1.Text = dt健康档案.Rows[0][tb_健康档案.与户主关系].ToString();
                //        this.txt姓名1.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[i]["姓名"].ToString());
                //        this.txt姓别1.Text = dt健康档案.Rows[0][tb_健康档案.性别].ToString();
                //        this.txt出生日期1.Text = Convert.ToDateTime(dt健康档案.Rows[0][tb_健康档案.出生日期]).ToLongDateString().ToString();

                //        break;
                //    case "2":
                //        this.txt与户主2.Text = dt健康档案.Rows[0][tb_健康档案.与户主关系].ToString();
                //        this.txt姓名2.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[i]["姓名"].ToString());
                //        this.txt姓别2.Text = dt健康档案.Rows[0][tb_健康档案.性别].ToString();
                //        this.txt出生日期2.Text = Convert.ToDateTime(dt健康档案.Rows[0][tb_健康档案.出生日期]).ToLongDateString().ToString();

                //        break;
                //    case "3":
                //        this.txt与户主3.Text = dt健康档案.Rows[0][tb_健康档案.与户主关系].ToString();
                //        this.txt姓名3.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[i]["姓名"].ToString());
                //        this.txt姓别3.Text = dt健康档案.Rows[0][tb_健康档案.性别].ToString();
                //        this.txt出生日期3.Text = Convert.ToDateTime(dt健康档案.Rows[0][tb_健康档案.出生日期]).ToLongDateString().ToString();

                //        break;
                //    case "4":
                //        this.txt与户主4.Text = dt健康档案.Rows[0][tb_健康档案.与户主关系].ToString();
                //        this.txt姓名4.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[i]["姓名"].ToString());
                //        this.txt姓别4.Text = dt健康档案.Rows[0][tb_健康档案.性别].ToString();
                //        this.txt出生日期4.Text = Convert.ToDateTime(dt健康档案.Rows[0][tb_健康档案.出生日期]).ToLongDateString().ToString();

                //        break;
                //    case "5":
                //        this.txt与户主5.Text = dt健康档案.Rows[0][tb_健康档案.与户主关系].ToString();
                //        this.txt姓名5.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[i]["姓名"].ToString());
                //        this.txt姓别5.Text = dt健康档案.Rows[0][tb_健康档案.性别].ToString();
                //        this.txt出生日期5.Text = Convert.ToDateTime(dt健康档案.Rows[0][tb_健康档案.出生日期]).ToLongDateString().ToString();

                //        break;
                //    case "6":
                //        this.txt与户主6.Text = dt健康档案.Rows[0][tb_健康档案.与户主关系].ToString();
                //        this.txt姓名6.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[i]["姓名"].ToString());
                //        this.txt姓别6.Text = dt健康档案.Rows[0][tb_健康档案.性别].ToString();
                //        this.txt出生日期6.Text = Convert.ToDateTime(dt健康档案.Rows[0][tb_健康档案.出生日期]).ToLongDateString().ToString();

                //        break;
                //    case "7":
                //        this.txt与户主7.Text = dt健康档案.Rows[0][tb_健康档案.与户主关系].ToString();
                //        this.txt姓名7.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[i]["姓名"].ToString());
                //        this.txt姓别7.Text = dt健康档案.Rows[0][tb_健康档案.性别].ToString();
                //        this.txt出生日期7.Text = Convert.ToDateTime(dt健康档案.Rows[0][tb_健康档案.出生日期]).ToLongDateString().ToString();

                //        break;
                //}
            }
            //this.gc家庭成员信息.DataSource = dt家庭成员信息;
        }
    }
}
