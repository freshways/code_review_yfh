﻿namespace AtomEHR.公共卫生.Module.个人健康.家庭健康信息
{
    partial class UC家庭基本信息
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC家庭基本信息));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.Layout = new DevExpress.XtraLayout.LayoutControl();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lbl考核项 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lbl未填 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.lbl完整度 = new DevExpress.XtraEditors.LabelControl();
            this.gc家庭成员信息 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv家庭成员信息 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gb与户主关系 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col与户主关系 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.lkp与户主关系 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gb姓名 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col姓名 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gb性别 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col性别 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.lkp性别 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gb出生日期 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col出生日期 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gb居住情况 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col居住状况 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.lkp居住状况 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gb操作 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.col注销 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.btn注销 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.col转出 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.btn转出 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.cbo与户主关系 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbo住房类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo厕所类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt家庭人均月收入 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.cbo是否低保户 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt家庭通常每个月吃油盐量 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt录入时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.txt录入人 = new DevExpress.XtraEditors.TextEdit();
            this.txt当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt住房面积 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl住房类型 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl是否低保户 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl家庭通常每个月吃油盐量 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl住房面积 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl厕所类型 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl家庭人均月收入 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn转入家庭关系 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnExport1 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtnExport2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Layout)).BeginInit();
            this.Layout.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc家庭成员信息)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv家庭成员信息)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp与户主关系)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp性别)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp居住状况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn注销)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn转出)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo与户主关系)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo住房类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo厕所类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo是否低保户.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl住房类型)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl是否低保户)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家庭通常每个月吃油盐量)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl住房面积)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl厕所类型)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家庭人均月收入)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.groupControl1);
            this.panelControl1.Controls.Add(this.panelControl5);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(629, 445);
            this.panelControl1.TabIndex = 12;
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.AppearanceCaption.Options.UseTextOptions = true;
            this.groupControl1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.groupControl1.Controls.Add(this.Layout);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(2, 35);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(625, 408);
            this.groupControl1.TabIndex = 14;
            this.groupControl1.Text = "家庭基本信息";
            // 
            // Layout
            // 
            this.Layout.Controls.Add(this.flowLayoutPanel2);
            this.Layout.Controls.Add(this.gc家庭成员信息);
            this.Layout.Controls.Add(this.cbo住房类型);
            this.Layout.Controls.Add(this.cbo厕所类型);
            this.Layout.Controls.Add(this.txt家庭人均月收入);
            this.Layout.Controls.Add(this.cbo是否低保户);
            this.Layout.Controls.Add(this.txt家庭通常每个月吃油盐量);
            this.Layout.Controls.Add(this.txt创建时间);
            this.Layout.Controls.Add(this.txt录入时间);
            this.Layout.Controls.Add(this.txt最近更新时间);
            this.Layout.Controls.Add(this.txt创建机构);
            this.Layout.Controls.Add(this.txt最近修改人);
            this.Layout.Controls.Add(this.txt录入人);
            this.Layout.Controls.Add(this.txt当前所属机构);
            this.Layout.Controls.Add(this.txt住房面积);
            this.Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Layout.Location = new System.Drawing.Point(2, 26);
            this.Layout.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.Layout.Name = "Layout";
            this.Layout.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(423, 262, 250, 350);
            this.Layout.OptionsView.DrawItemBorders = true;
            this.Layout.Root = this.layoutControlGroup1;
            this.Layout.Size = new System.Drawing.Size(621, 380);
            this.Layout.TabIndex = 18;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.labelControl1);
            this.flowLayoutPanel2.Controls.Add(this.lbl考核项);
            this.flowLayoutPanel2.Controls.Add(this.labelControl3);
            this.flowLayoutPanel2.Controls.Add(this.lbl未填);
            this.flowLayoutPanel2.Controls.Add(this.labelControl5);
            this.flowLayoutPanel2.Controls.Add(this.lbl完整度);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(617, 20);
            this.flowLayoutPanel2.TabIndex = 26;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "考核项：";
            // 
            // lbl考核项
            // 
            this.lbl考核项.Location = new System.Drawing.Point(57, 3);
            this.lbl考核项.Name = "lbl考核项";
            this.lbl考核项.Size = new System.Drawing.Size(0, 14);
            this.lbl考核项.TabIndex = 4;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(63, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(36, 14);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "未填：";
            // 
            // lbl未填
            // 
            this.lbl未填.Location = new System.Drawing.Point(105, 3);
            this.lbl未填.Name = "lbl未填";
            this.lbl未填.Size = new System.Drawing.Size(0, 14);
            this.lbl未填.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(111, 3);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(48, 14);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "完整度：";
            // 
            // lbl完整度
            // 
            this.lbl完整度.Location = new System.Drawing.Point(165, 3);
            this.lbl完整度.Name = "lbl完整度";
            this.lbl完整度.Size = new System.Drawing.Size(0, 14);
            this.lbl完整度.TabIndex = 8;
            // 
            // gc家庭成员信息
            // 
            this.gc家庭成员信息.AllowBandedGridColumnSort = false;
            this.gc家庭成员信息.IsBestFitColumns = true;
            this.gc家庭成员信息.Location = new System.Drawing.Point(2, 26);
            this.gc家庭成员信息.MainView = this.gv家庭成员信息;
            this.gc家庭成员信息.Name = "gc家庭成员信息";
            this.gc家庭成员信息.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.cbo与户主关系,
            this.btn注销,
            this.btn转出,
            this.lkp性别,
            this.lkp与户主关系,
            this.lkp居住状况});
            this.gc家庭成员信息.ShowContextMenu = false;
            this.gc家庭成员信息.Size = new System.Drawing.Size(617, 160);
            this.gc家庭成员信息.StrWhere = "";
            this.gc家庭成员信息.TabIndex = 2;
            this.gc家庭成员信息.UseCheckBox = false;
            this.gc家庭成员信息.View = "";
            this.gc家庭成员信息.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv家庭成员信息});
            // 
            // gv家庭成员信息
            // 
            this.gv家庭成员信息.Appearance.BandPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.BandPanel.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.BandPanel.Options.UseTextOptions = true;
            this.gv家庭成员信息.Appearance.BandPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gv家庭成员信息.Appearance.BandPanelBackground.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.BandPanelBackground.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.DetailTip.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.Empty.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.EvenRow.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.FilterPanel.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.FixedLine.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.FocusedCell.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.FocusedRow.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv家庭成员信息.Appearance.FooterPanel.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.GroupButton.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.GroupFooter.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.GroupPanel.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.GroupRow.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.HeaderPanelBackground.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.HeaderPanelBackground.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.HorzLine.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.OddRow.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.Preview.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.Row.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.RowSeparator.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.SelectedRow.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.TopNewRow.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.VertLine.Options.UseFont = true;
            this.gv家庭成员信息.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv家庭成员信息.Appearance.ViewCaption.Options.UseFont = true;
            this.gv家庭成员信息.BandPanelRowHeight = 30;
            this.gv家庭成员信息.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gb与户主关系,
            this.gb姓名,
            this.gb性别,
            this.gb出生日期,
            this.gb居住情况,
            this.gb操作});
            this.gv家庭成员信息.ColumnPanelRowHeight = 30;
            this.gv家庭成员信息.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.col与户主关系,
            this.col姓名,
            this.col性别,
            this.col出生日期,
            this.col居住状况,
            this.col注销,
            this.col转出});
            this.gv家庭成员信息.GridControl = this.gc家庭成员信息;
            this.gv家庭成员信息.GroupPanelText = "DragColumn";
            this.gv家庭成员信息.Name = "gv家庭成员信息";
            this.gv家庭成员信息.OptionsView.EnableAppearanceEvenRow = true;
            this.gv家庭成员信息.OptionsView.EnableAppearanceOddRow = true;
            this.gv家庭成员信息.OptionsView.ShowColumnHeaders = false;
            this.gv家庭成员信息.OptionsView.ShowGroupPanel = false;
            // 
            // gb与户主关系
            // 
            this.gb与户主关系.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gb与户主关系.AppearanceHeader.Options.UseFont = true;
            this.gb与户主关系.Caption = "与户主关系";
            this.gb与户主关系.Columns.Add(this.col与户主关系);
            this.gb与户主关系.Name = "gb与户主关系";
            this.gb与户主关系.VisibleIndex = 0;
            this.gb与户主关系.Width = 99;
            // 
            // col与户主关系
            // 
            this.col与户主关系.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col与户主关系.AppearanceCell.Options.UseBackColor = true;
            this.col与户主关系.AppearanceCell.Options.UseTextOptions = true;
            this.col与户主关系.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col与户主关系.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col与户主关系.AppearanceHeader.Options.UseFont = true;
            this.col与户主关系.AppearanceHeader.Options.UseTextOptions = true;
            this.col与户主关系.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col与户主关系.Caption = "与户主关系";
            this.col与户主关系.ColumnEdit = this.lkp与户主关系;
            this.col与户主关系.FieldName = "与户主关系";
            this.col与户主关系.Name = "col与户主关系";
            this.col与户主关系.Visible = true;
            this.col与户主关系.Width = 99;
            // 
            // lkp与户主关系
            // 
            this.lkp与户主关系.AutoHeight = false;
            this.lkp与户主关系.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp与户主关系.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "与户主关系")});
            this.lkp与户主关系.Name = "lkp与户主关系";
            // 
            // gb姓名
            // 
            this.gb姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gb姓名.AppearanceHeader.Options.UseFont = true;
            this.gb姓名.Caption = "姓名";
            this.gb姓名.Columns.Add(this.col姓名);
            this.gb姓名.Name = "gb姓名";
            this.gb姓名.VisibleIndex = 1;
            this.gb姓名.Width = 99;
            // 
            // col姓名
            // 
            this.col姓名.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col姓名.AppearanceCell.Options.UseBackColor = true;
            this.col姓名.AppearanceCell.Options.UseTextOptions = true;
            this.col姓名.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col姓名.AppearanceHeader.Options.UseFont = true;
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.ReadOnly = true;
            this.col姓名.Visible = true;
            this.col姓名.Width = 99;
            // 
            // gb性别
            // 
            this.gb性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gb性别.AppearanceHeader.Options.UseFont = true;
            this.gb性别.Caption = "性别";
            this.gb性别.Columns.Add(this.col性别);
            this.gb性别.Name = "gb性别";
            this.gb性别.VisibleIndex = 2;
            this.gb性别.Width = 68;
            // 
            // col性别
            // 
            this.col性别.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col性别.AppearanceCell.Options.UseBackColor = true;
            this.col性别.AppearanceCell.Options.UseTextOptions = true;
            this.col性别.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col性别.AppearanceHeader.Options.UseFont = true;
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.ColumnEdit = this.lkp性别;
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.Width = 68;
            // 
            // lkp性别
            // 
            this.lkp性别.AutoHeight = false;
            this.lkp性别.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp性别.Name = "lkp性别";
            // 
            // gb出生日期
            // 
            this.gb出生日期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gb出生日期.AppearanceHeader.Options.UseFont = true;
            this.gb出生日期.Caption = "出生日期";
            this.gb出生日期.Columns.Add(this.col出生日期);
            this.gb出生日期.Name = "gb出生日期";
            this.gb出生日期.VisibleIndex = 3;
            this.gb出生日期.Width = 106;
            // 
            // col出生日期
            // 
            this.col出生日期.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col出生日期.AppearanceCell.Options.UseBackColor = true;
            this.col出生日期.AppearanceCell.Options.UseTextOptions = true;
            this.col出生日期.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col出生日期.AppearanceHeader.Options.UseFont = true;
            this.col出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.Caption = "出生日期";
            this.col出生日期.FieldName = "出生日期";
            this.col出生日期.Name = "col出生日期";
            this.col出生日期.OptionsColumn.ReadOnly = true;
            this.col出生日期.Visible = true;
            this.col出生日期.Width = 106;
            // 
            // gb居住情况
            // 
            this.gb居住情况.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gb居住情况.AppearanceHeader.Options.UseFont = true;
            this.gb居住情况.Caption = "居住情况";
            this.gb居住情况.Columns.Add(this.col居住状况);
            this.gb居住情况.Name = "gb居住情况";
            this.gb居住情况.VisibleIndex = 4;
            this.gb居住情况.Width = 178;
            // 
            // col居住状况
            // 
            this.col居住状况.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col居住状况.AppearanceCell.Options.UseBackColor = true;
            this.col居住状况.AppearanceCell.Options.UseTextOptions = true;
            this.col居住状况.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col居住状况.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col居住状况.AppearanceHeader.Options.UseFont = true;
            this.col居住状况.AppearanceHeader.Options.UseTextOptions = true;
            this.col居住状况.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col居住状况.Caption = "居住状况";
            this.col居住状况.ColumnEdit = this.lkp居住状况;
            this.col居住状况.FieldName = "常住类型";
            this.col居住状况.Name = "col居住状况";
            this.col居住状况.OptionsColumn.ReadOnly = true;
            this.col居住状况.Visible = true;
            this.col居住状况.Width = 178;
            // 
            // lkp居住状况
            // 
            this.lkp居住状况.AutoHeight = false;
            this.lkp居住状况.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp居住状况.Name = "lkp居住状况";
            // 
            // gb操作
            // 
            this.gb操作.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gb操作.AppearanceHeader.Options.UseFont = true;
            this.gb操作.Caption = "操作";
            this.gb操作.Columns.Add(this.col注销);
            this.gb操作.Columns.Add(this.col转出);
            this.gb操作.Name = "gb操作";
            this.gb操作.VisibleIndex = 5;
            this.gb操作.Width = 146;
            // 
            // col注销
            // 
            this.col注销.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col注销.AppearanceCell.Options.UseBackColor = true;
            this.col注销.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col注销.AppearanceHeader.Options.UseFont = true;
            this.col注销.AppearanceHeader.Options.UseTextOptions = true;
            this.col注销.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col注销.Caption = "注销";
            this.col注销.ColumnEdit = this.btn注销;
            this.col注销.Name = "col注销";
            this.col注销.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.col注销.Visible = true;
            this.col注销.Width = 71;
            // 
            // btn注销
            // 
            this.btn注销.AutoHeight = false;
            this.btn注销.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "注销", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, "", null, null, true)});
            this.btn注销.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btn注销.Name = "btn注销";
            this.btn注销.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn注销.Click += new System.EventHandler(this.btn注销_Click);
            // 
            // col转出
            // 
            this.col转出.AppearanceCell.BackColor = System.Drawing.Color.White;
            this.col转出.AppearanceCell.Options.UseBackColor = true;
            this.col转出.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col转出.AppearanceHeader.Options.UseFont = true;
            this.col转出.AppearanceHeader.Options.UseTextOptions = true;
            this.col转出.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col转出.Caption = "转出";
            this.col转出.ColumnEdit = this.btn转出;
            this.col转出.Name = "col转出";
            this.col转出.Visible = true;
            // 
            // btn转出
            // 
            this.btn转出.AutoHeight = false;
            this.btn转出.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "转出", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject10, "", null, null, true)});
            this.btn转出.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.btn转出.Name = "btn转出";
            this.btn转出.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn转出.Click += new System.EventHandler(this.btn转出_Click);
            // 
            // cbo与户主关系
            // 
            this.cbo与户主关系.AutoHeight = false;
            this.cbo与户主关系.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo与户主关系.Name = "cbo与户主关系";
            this.cbo与户主关系.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cbo住房类型
            // 
            this.cbo住房类型.Location = new System.Drawing.Point(148, 190);
            this.cbo住房类型.Name = "cbo住房类型";
            this.cbo住房类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo住房类型.Properties.Items.AddRange(new object[] {
            "平房",
            "楼房"});
            this.cbo住房类型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo住房类型.Size = new System.Drawing.Size(169, 20);
            this.cbo住房类型.StyleController = this.Layout;
            this.cbo住房类型.TabIndex = 1;
            // 
            // cbo厕所类型
            // 
            this.cbo厕所类型.Location = new System.Drawing.Point(148, 214);
            this.cbo厕所类型.Name = "cbo厕所类型";
            this.cbo厕所类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo厕所类型.Properties.Items.AddRange(new object[] {
            "坐式抽水式马桶",
            "蹲式冲水式厕所",
            "公共厕所",
            "其他"});
            this.cbo厕所类型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo厕所类型.Size = new System.Drawing.Size(150, 20);
            this.cbo厕所类型.StyleController = this.Layout;
            this.cbo厕所类型.TabIndex = 5;
            // 
            // txt家庭人均月收入
            // 
            this.txt家庭人均月收入.Lbl1Size = new System.Drawing.Size(30, 18);
            this.txt家庭人均月收入.Lbl1Text = "元";
            this.txt家庭人均月收入.Location = new System.Drawing.Point(148, 238);
            this.txt家庭人均月收入.Margin = new System.Windows.Forms.Padding(3, 2, 0, 0);
            this.txt家庭人均月收入.Name = "txt家庭人均月收入";
            this.txt家庭人均月收入.Size = new System.Drawing.Size(168, 20);
            this.txt家庭人均月收入.TabIndex = 7;
            this.txt家庭人均月收入.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // cbo是否低保户
            // 
            this.cbo是否低保户.Location = new System.Drawing.Point(425, 238);
            this.cbo是否低保户.Name = "cbo是否低保户";
            this.cbo是否低保户.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo是否低保户.Properties.Items.AddRange(new object[] {
            "是",
            "否"});
            this.cbo是否低保户.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo是否低保户.Size = new System.Drawing.Size(194, 20);
            this.cbo是否低保户.StyleController = this.Layout;
            this.cbo是否低保户.TabIndex = 9;
            // 
            // txt家庭通常每个月吃油盐量
            // 
            this.txt家庭通常每个月吃油盐量.Lbl1Size = new System.Drawing.Size(70, 14);
            this.txt家庭通常每个月吃油盐量.Lbl1Text = "斤植物油/月";
            this.txt家庭通常每个月吃油盐量.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt家庭通常每个月吃油盐量.Lbl2Text = "克盐/月";
            this.txt家庭通常每个月吃油盐量.Location = new System.Drawing.Point(148, 262);
            this.txt家庭通常每个月吃油盐量.Name = "txt家庭通常每个月吃油盐量";
            this.txt家庭通常每个月吃油盐量.Size = new System.Drawing.Size(471, 20);
            this.txt家庭通常每个月吃油盐量.TabIndex = 11;
            this.txt家庭通常每个月吃油盐量.Txt1EditValue = null;
            this.txt家庭通常每个月吃油盐量.Txt1Size = new System.Drawing.Size(100, 20);
            this.txt家庭通常每个月吃油盐量.Txt2EditValue = null;
            this.txt家庭通常每个月吃油盐量.Txt2Size = new System.Drawing.Size(100, 20);
            // 
            // txt创建时间
            // 
            this.txt创建时间.Location = new System.Drawing.Point(148, 286);
            this.txt创建时间.Name = "txt创建时间";
            this.txt创建时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt创建时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txt创建时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt创建时间.Properties.ReadOnly = true;
            this.txt创建时间.Size = new System.Drawing.Size(171, 20);
            this.txt创建时间.StyleController = this.Layout;
            this.txt创建时间.TabIndex = 19;
            // 
            // txt录入时间
            // 
            this.txt录入时间.Location = new System.Drawing.Point(428, 286);
            this.txt录入时间.Name = "txt录入时间";
            this.txt录入时间.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.txt录入时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txt录入时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt录入时间.Properties.ReadOnly = true;
            this.txt录入时间.Size = new System.Drawing.Size(191, 20);
            this.txt录入时间.StyleController = this.Layout;
            this.txt录入时间.TabIndex = 20;
            // 
            // txt最近更新时间
            // 
            this.txt最近更新时间.EditValue = "";
            this.txt最近更新时间.Location = new System.Drawing.Point(148, 310);
            this.txt最近更新时间.Name = "txt最近更新时间";
            this.txt最近更新时间.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.txt最近更新时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.txt最近更新时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt最近更新时间.Properties.ReadOnly = true;
            this.txt最近更新时间.Size = new System.Drawing.Size(171, 20);
            this.txt最近更新时间.StyleController = this.Layout;
            this.txt最近更新时间.TabIndex = 21;
            // 
            // txt创建机构
            // 
            this.txt创建机构.Location = new System.Drawing.Point(428, 310);
            this.txt创建机构.Name = "txt创建机构";
            this.txt创建机构.Properties.ReadOnly = true;
            this.txt创建机构.Size = new System.Drawing.Size(191, 20);
            this.txt创建机构.StyleController = this.Layout;
            this.txt创建机构.TabIndex = 22;
            // 
            // txt最近修改人
            // 
            this.txt最近修改人.Location = new System.Drawing.Point(428, 334);
            this.txt最近修改人.Name = "txt最近修改人";
            this.txt最近修改人.Properties.ReadOnly = true;
            this.txt最近修改人.Size = new System.Drawing.Size(191, 20);
            this.txt最近修改人.StyleController = this.Layout;
            this.txt最近修改人.TabIndex = 23;
            // 
            // txt录入人
            // 
            this.txt录入人.Location = new System.Drawing.Point(148, 334);
            this.txt录入人.Name = "txt录入人";
            this.txt录入人.Properties.ReadOnly = true;
            this.txt录入人.Size = new System.Drawing.Size(171, 20);
            this.txt录入人.StyleController = this.Layout;
            this.txt录入人.TabIndex = 24;
            // 
            // txt当前所属机构
            // 
            this.txt当前所属机构.Location = new System.Drawing.Point(148, 358);
            this.txt当前所属机构.Name = "txt当前所属机构";
            this.txt当前所属机构.Properties.ReadOnly = true;
            this.txt当前所属机构.Size = new System.Drawing.Size(471, 20);
            this.txt当前所属机构.StyleController = this.Layout;
            this.txt当前所属机构.TabIndex = 25;
            // 
            // txt住房面积
            // 
            this.txt住房面积.Lbl1Size = new System.Drawing.Size(30, 18);
            this.txt住房面积.Lbl1Text = "㎡";
            this.txt住房面积.Location = new System.Drawing.Point(426, 190);
            this.txt住房面积.Margin = new System.Windows.Forms.Padding(3, 2, 0, 0);
            this.txt住房面积.Name = "txt住房面积";
            this.txt住房面积.Size = new System.Drawing.Size(193, 20);
            this.txt住房面积.TabIndex = 3;
            this.txt住房面积.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl住房类型,
            this.lbl是否低保户,
            this.lbl家庭通常每个月吃油盐量,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.lbl住房面积,
            this.lbl厕所类型,
            this.lbl家庭人均月收入,
            this.emptySpaceItem1,
            this.layoutControlItem15,
            this.layoutControlItem14});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(621, 380);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // lbl住房类型
            // 
            this.lbl住房类型.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl住房类型.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl住房类型.AppearanceItemCaption.Options.UseFont = true;
            this.lbl住房类型.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl住房类型.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl住房类型.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl住房类型.Control = this.cbo住房类型;
            this.lbl住房类型.CustomizationFormText = "住房类型";
            this.lbl住房类型.Location = new System.Drawing.Point(0, 188);
            this.lbl住房类型.Name = "lbl住房类型";
            this.lbl住房类型.Size = new System.Drawing.Size(319, 24);
            this.lbl住房类型.Text = "住房类型";
            this.lbl住房类型.TextSize = new System.Drawing.Size(143, 14);
            // 
            // lbl是否低保户
            // 
            this.lbl是否低保户.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl是否低保户.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl是否低保户.AppearanceItemCaption.Options.UseFont = true;
            this.lbl是否低保户.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl是否低保户.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl是否低保户.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl是否低保户.Control = this.cbo是否低保户;
            this.lbl是否低保户.CustomizationFormText = "是否低保户";
            this.lbl是否低保户.Location = new System.Drawing.Point(318, 236);
            this.lbl是否低保户.Name = "lbl是否低保户";
            this.lbl是否低保户.Size = new System.Drawing.Size(303, 24);
            this.lbl是否低保户.Text = "是否低保户";
            this.lbl是否低保户.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl是否低保户.TextSize = new System.Drawing.Size(100, 14);
            this.lbl是否低保户.TextToControlDistance = 5;
            // 
            // lbl家庭通常每个月吃油盐量
            // 
            this.lbl家庭通常每个月吃油盐量.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl家庭通常每个月吃油盐量.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl家庭通常每个月吃油盐量.AppearanceItemCaption.Options.UseFont = true;
            this.lbl家庭通常每个月吃油盐量.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl家庭通常每个月吃油盐量.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl家庭通常每个月吃油盐量.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl家庭通常每个月吃油盐量.Control = this.txt家庭通常每个月吃油盐量;
            this.lbl家庭通常每个月吃油盐量.CustomizationFormText = "家庭通常每个月吃油盐量";
            this.lbl家庭通常每个月吃油盐量.Location = new System.Drawing.Point(0, 260);
            this.lbl家庭通常每个月吃油盐量.MinSize = new System.Drawing.Size(239, 24);
            this.lbl家庭通常每个月吃油盐量.Name = "lbl家庭通常每个月吃油盐量";
            this.lbl家庭通常每个月吃油盐量.Size = new System.Drawing.Size(621, 24);
            this.lbl家庭通常每个月吃油盐量.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl家庭通常每个月吃油盐量.Text = "家庭通常每个月吃油盐量";
            this.lbl家庭通常每个月吃油盐量.TextSize = new System.Drawing.Size(143, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem6.Control = this.txt创建时间;
            this.layoutControlItem6.CustomizationFormText = "创建时间: ";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 284);
            this.layoutControlItem6.Name = "txt创建时间item";
            this.layoutControlItem6.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem6.Text = "创建时间: ";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(143, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem7.Control = this.txt录入时间;
            this.layoutControlItem7.CustomizationFormText = "录入时间：";
            this.layoutControlItem7.Location = new System.Drawing.Point(321, 284);
            this.layoutControlItem7.Name = "txt录入时间item";
            this.layoutControlItem7.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem7.Text = "录入时间：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem8.Control = this.txt最近更新时间;
            this.layoutControlItem8.CustomizationFormText = "最近更新时间: ";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 308);
            this.layoutControlItem8.Name = "txt最近更新时间item";
            this.layoutControlItem8.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem8.Text = "最近更新时间: ";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(143, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem9.Control = this.txt创建机构;
            this.layoutControlItem9.CustomizationFormText = "创建机构: ";
            this.layoutControlItem9.Location = new System.Drawing.Point(321, 308);
            this.layoutControlItem9.Name = "txt创建机构item";
            this.layoutControlItem9.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem9.Text = "创建机构: ";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem10.Control = this.txt最近修改人;
            this.layoutControlItem10.CustomizationFormText = "最近修改人: ";
            this.layoutControlItem10.Location = new System.Drawing.Point(321, 332);
            this.layoutControlItem10.Name = "txt最近修改人item";
            this.layoutControlItem10.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem10.Text = "最近修改人: ";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem11.Control = this.txt录入人;
            this.layoutControlItem11.CustomizationFormText = "录入人:";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 332);
            this.layoutControlItem11.Name = "txt录入人item";
            this.layoutControlItem11.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem11.Text = "录入人:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(143, 14);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem12.Control = this.txt当前所属机构;
            this.layoutControlItem12.CustomizationFormText = "当前所属机构:";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 356);
            this.layoutControlItem12.Name = "txt当前所属机构item";
            this.layoutControlItem12.Size = new System.Drawing.Size(621, 24);
            this.layoutControlItem12.Text = "当前所属机构:";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(143, 14);
            // 
            // lbl住房面积
            // 
            this.lbl住房面积.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl住房面积.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl住房面积.AppearanceItemCaption.Options.UseFont = true;
            this.lbl住房面积.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl住房面积.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl住房面积.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl住房面积.Control = this.txt住房面积;
            this.lbl住房面积.CustomizationFormText = "住房面积";
            this.lbl住房面积.Location = new System.Drawing.Point(319, 188);
            this.lbl住房面积.Name = "lbl住房面积";
            this.lbl住房面积.Size = new System.Drawing.Size(302, 24);
            this.lbl住房面积.Text = "住房面积";
            this.lbl住房面积.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl住房面积.TextSize = new System.Drawing.Size(100, 14);
            this.lbl住房面积.TextToControlDistance = 5;
            // 
            // lbl厕所类型
            // 
            this.lbl厕所类型.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl厕所类型.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl厕所类型.AppearanceItemCaption.Options.UseFont = true;
            this.lbl厕所类型.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl厕所类型.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl厕所类型.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl厕所类型.Control = this.cbo厕所类型;
            this.lbl厕所类型.CustomizationFormText = "厕所类型";
            this.lbl厕所类型.Location = new System.Drawing.Point(0, 212);
            this.lbl厕所类型.Name = "lbl厕所类型";
            this.lbl厕所类型.Size = new System.Drawing.Size(300, 24);
            this.lbl厕所类型.Text = "厕所类型";
            this.lbl厕所类型.TextSize = new System.Drawing.Size(143, 14);
            // 
            // lbl家庭人均月收入
            // 
            this.lbl家庭人均月收入.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl家庭人均月收入.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl家庭人均月收入.AppearanceItemCaption.Options.UseFont = true;
            this.lbl家庭人均月收入.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl家庭人均月收入.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl家庭人均月收入.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl家庭人均月收入.Control = this.txt家庭人均月收入;
            this.lbl家庭人均月收入.CustomizationFormText = "家庭人均月收入";
            this.lbl家庭人均月收入.Location = new System.Drawing.Point(0, 236);
            this.lbl家庭人均月收入.Name = "lbl家庭人均月收入";
            this.lbl家庭人均月收入.Size = new System.Drawing.Size(318, 24);
            this.lbl家庭人均月收入.Text = "家庭人均月收入";
            this.lbl家庭人均月收入.TextSize = new System.Drawing.Size(143, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(300, 212);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(321, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.gc家庭成员信息;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(621, 164);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.flowLayoutPanel2;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(621, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // panelControl5
            // 
            this.panelControl5.Controls.Add(this.flowLayoutPanel1);
            this.panelControl5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl5.Location = new System.Drawing.Point(2, 2);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(625, 33);
            this.panelControl5.TabIndex = 11;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存修改);
            this.flowLayoutPanel1.Controls.Add(this.btn转入家庭关系);
            this.flowLayoutPanel1.Controls.Add(this.sbtnExport1);
            this.flowLayoutPanel1.Controls.Add(this.sbtnExport2);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(621, 29);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // btn保存修改
            // 
            this.btn保存修改.Image = ((System.Drawing.Image)(resources.GetObject("btn保存修改.Image")));
            this.btn保存修改.Location = new System.Drawing.Point(3, 3);
            this.btn保存修改.Name = "btn保存修改";
            this.btn保存修改.Size = new System.Drawing.Size(94, 23);
            this.btn保存修改.TabIndex = 1;
            this.btn保存修改.Text = "保存修改";
            this.btn保存修改.Click += new System.EventHandler(this.btn保存修改_Click);
            // 
            // btn转入家庭关系
            // 
            this.btn转入家庭关系.Image = ((System.Drawing.Image)(resources.GetObject("btn转入家庭关系.Image")));
            this.btn转入家庭关系.Location = new System.Drawing.Point(103, 3);
            this.btn转入家庭关系.Name = "btn转入家庭关系";
            this.btn转入家庭关系.Size = new System.Drawing.Size(124, 23);
            this.btn转入家庭关系.TabIndex = 2;
            this.btn转入家庭关系.Text = "转入家庭成员";
            this.btn转入家庭关系.Click += new System.EventHandler(this.btn转入家庭关系_Click);
            // 
            // sbtnExport1
            // 
            this.sbtnExport1.Location = new System.Drawing.Point(233, 3);
            this.sbtnExport1.Name = "sbtnExport1";
            this.sbtnExport1.Size = new System.Drawing.Size(75, 23);
            this.sbtnExport1.TabIndex = 5;
            this.sbtnExport1.Text = "导出1";
            this.sbtnExport1.Click += new System.EventHandler(this.sbtnExport1_Click);
            // 
            // sbtnExport2
            // 
            this.sbtnExport2.Location = new System.Drawing.Point(314, 3);
            this.sbtnExport2.Name = "sbtnExport2";
            this.sbtnExport2.Size = new System.Drawing.Size(75, 23);
            this.sbtnExport2.TabIndex = 6;
            this.sbtnExport2.Text = "导出2";
            this.sbtnExport2.Click += new System.EventHandler(this.sbtnExport2_Click);
            // 
            // UC家庭基本信息
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.Name = "UC家庭基本信息";
            this.Size = new System.Drawing.Size(629, 445);
            this.Load += new System.EventHandler(this.UC家庭基本信息2_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Layout)).EndInit();
            this.Layout.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc家庭成员信息)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv家庭成员信息)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp与户主关系)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp性别)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp居住状况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn注销)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn转出)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo与户主关系)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo住房类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo厕所类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo是否低保户.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl住房类型)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl是否低保户)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家庭通常每个月吃油盐量)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl住房面积)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl厕所类型)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家庭人均月收入)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.SimpleButton btn转入家庭关系;
        private DevExpress.XtraEditors.SimpleButton btn保存修改;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraLayout.LayoutControl Layout;
        private DevExpress.XtraEditors.ComboBoxEdit cbo住房类型;
        private DevExpress.XtraEditors.ComboBoxEdit cbo厕所类型;
        private Library.UserControls.UCTxtLbl txt家庭人均月收入;
        private DevExpress.XtraEditors.ComboBoxEdit cbo是否低保户;
        private Library.UserControls.UCTxtLblTxtLbl txt家庭通常每个月吃油盐量;
        private DevExpress.XtraEditors.TextEdit txt创建时间;
        private DevExpress.XtraEditors.TextEdit txt录入时间;
        private DevExpress.XtraEditors.TextEdit txt最近更新时间;
        private DevExpress.XtraEditors.TextEdit txt创建机构;
        private DevExpress.XtraEditors.TextEdit txt最近修改人;
        private DevExpress.XtraEditors.TextEdit txt录入人;
        private DevExpress.XtraEditors.TextEdit txt当前所属机构;
        private Library.UserControls.UCTxtLbl txt住房面积;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem lbl住房类型;
        private DevExpress.XtraLayout.LayoutControlItem lbl厕所类型;
        private DevExpress.XtraLayout.LayoutControlItem lbl家庭人均月收入;
        private DevExpress.XtraLayout.LayoutControlItem lbl是否低保户;
        private DevExpress.XtraLayout.LayoutControlItem lbl家庭通常每个月吃油盐量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem lbl住房面积;
        private Library.UserControls.DataGridControl gc家庭成员信息;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gv家庭成员信息;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gb与户主关系;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col与户主关系;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbo与户主关系;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gb姓名;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col姓名;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gb性别;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col性别;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gb出生日期;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col出生日期;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gb居住情况;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col居住状况;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gb操作;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col注销;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn注销;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn col转出;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn转出;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lkp性别;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lkp与户主关系;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lkp居住状况;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lbl考核项;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl lbl未填;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl lbl完整度;
        private DevExpress.XtraEditors.SimpleButton sbtnExport1;
        private DevExpress.XtraEditors.SimpleButton sbtnExport2;
    }
}
