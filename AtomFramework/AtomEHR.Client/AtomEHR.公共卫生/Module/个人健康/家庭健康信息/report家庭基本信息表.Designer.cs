﻿namespace AtomEHR.公共卫生.Module.个人健康.家庭健康信息
{
    partial class report家庭基本信息表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt与户主1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓名1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓别1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt居住状况1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt与户主2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓名2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓别2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt居住状况2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt与户主3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓名3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓别3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生日期3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt居住状况3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt与户主4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓名4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓别4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生日期4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt居住状况4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt与户主5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓名5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓别5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生日期5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt居住状况5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt与户主6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓名6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓别6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生日期6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt居住状况6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt与户主7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓名7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt姓别7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生日期7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt居住状况7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.xrLabel1,
            this.xrTable1});
            this.Detail.HeightF = 701.0417F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 156.4583F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(203.1233F, 30F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UsePadding = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "1.家庭成员一览表：";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("仿宋", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(201.0417F, 95.79169F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(301.04F, 30F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UsePadding = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "家庭基本信息表";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 186.4583F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8});
            this.xrTable1.SizeF = new System.Drawing.SizeF(702.0833F, 240F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell1,
            this.xrTableCell4,
            this.xrTableCell2,
            this.xrTableCell5,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1.2D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "编号";
            this.xrTableCell6.Weight = 0.3753708945362022D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "与户主关系";
            this.xrTableCell1.Weight = 0.47002968721107907D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "姓名";
            this.xrTableCell4.Weight = 0.47002968721107924D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "性别";
            this.xrTableCell2.Weight = 0.29910980095250489D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "出生日期";
            this.xrTableCell5.Weight = 0.55548963034036625D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "居住状况";
            this.xrTableCell3.Weight = 0.82997029974876835D;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.txt与户主1,
            this.txt姓名1,
            this.txt姓别1,
            this.txt出生日期1,
            this.txt居住状况1});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.2D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "1";
            this.xrTableCell7.Weight = 0.3753708945362022D;
            // 
            // txt与户主1
            // 
            this.txt与户主1.Name = "txt与户主1";
            this.txt与户主1.Weight = 0.47002968721107907D;
            // 
            // txt姓名1
            // 
            this.txt姓名1.Name = "txt姓名1";
            this.txt姓名1.Weight = 0.47002968721107924D;
            // 
            // txt姓别1
            // 
            this.txt姓别1.Name = "txt姓别1";
            this.txt姓别1.Weight = 0.29910980095250489D;
            // 
            // txt出生日期1
            // 
            this.txt出生日期1.Name = "txt出生日期1";
            this.txt出生日期1.Weight = 0.55548963034036625D;
            // 
            // txt居住状况1
            // 
            this.txt居住状况1.Name = "txt居住状况1";
            this.txt居住状况1.Weight = 0.82997029974876835D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.txt与户主2,
            this.txt姓名2,
            this.txt姓别2,
            this.txt出生日期2,
            this.txt居住状况2});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1.2D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "2";
            this.xrTableCell13.Weight = 0.3753708945362022D;
            // 
            // txt与户主2
            // 
            this.txt与户主2.Name = "txt与户主2";
            this.txt与户主2.Weight = 0.47002968721107907D;
            // 
            // txt姓名2
            // 
            this.txt姓名2.Name = "txt姓名2";
            this.txt姓名2.Weight = 0.47002968721107924D;
            // 
            // txt姓别2
            // 
            this.txt姓别2.Name = "txt姓别2";
            this.txt姓别2.Weight = 0.29910980095250489D;
            // 
            // txt出生日期2
            // 
            this.txt出生日期2.Name = "txt出生日期2";
            this.txt出生日期2.Weight = 0.55548963034036625D;
            // 
            // txt居住状况2
            // 
            this.txt居住状况2.Name = "txt居住状况2";
            this.txt居住状况2.Weight = 0.82997029974876835D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.txt与户主3,
            this.txt姓名3,
            this.txt姓别3,
            this.txt出生日期3,
            this.txt居住状况3});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1.2D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Text = "3";
            this.xrTableCell19.Weight = 0.3753708945362022D;
            // 
            // txt与户主3
            // 
            this.txt与户主3.Name = "txt与户主3";
            this.txt与户主3.Weight = 0.47002968721107907D;
            // 
            // txt姓名3
            // 
            this.txt姓名3.Name = "txt姓名3";
            this.txt姓名3.Weight = 0.47002968721107924D;
            // 
            // txt姓别3
            // 
            this.txt姓别3.Name = "txt姓别3";
            this.txt姓别3.Weight = 0.29910980095250489D;
            // 
            // txt出生日期3
            // 
            this.txt出生日期3.Name = "txt出生日期3";
            this.txt出生日期3.Weight = 0.55548963034036625D;
            // 
            // txt居住状况3
            // 
            this.txt居住状况3.Name = "txt居住状况3";
            this.txt居住状况3.Weight = 0.82997029974876835D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.txt与户主4,
            this.txt姓名4,
            this.txt姓别4,
            this.txt出生日期4,
            this.txt居住状况4});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1.2D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Text = "4";
            this.xrTableCell25.Weight = 0.3753708945362022D;
            // 
            // txt与户主4
            // 
            this.txt与户主4.Name = "txt与户主4";
            this.txt与户主4.Weight = 0.47002968721107907D;
            // 
            // txt姓名4
            // 
            this.txt姓名4.Name = "txt姓名4";
            this.txt姓名4.Weight = 0.47002968721107924D;
            // 
            // txt姓别4
            // 
            this.txt姓别4.Name = "txt姓别4";
            this.txt姓别4.Weight = 0.29910980095250489D;
            // 
            // txt出生日期4
            // 
            this.txt出生日期4.Name = "txt出生日期4";
            this.txt出生日期4.Weight = 0.55548963034036625D;
            // 
            // txt居住状况4
            // 
            this.txt居住状况4.Name = "txt居住状况4";
            this.txt居住状况4.Weight = 0.82997029974876835D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.txt与户主5,
            this.txt姓名5,
            this.txt姓别5,
            this.txt出生日期5,
            this.txt居住状况5});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1.2D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "5";
            this.xrTableCell31.Weight = 0.3753708945362022D;
            // 
            // txt与户主5
            // 
            this.txt与户主5.Name = "txt与户主5";
            this.txt与户主5.Weight = 0.47002968721107907D;
            // 
            // txt姓名5
            // 
            this.txt姓名5.Name = "txt姓名5";
            this.txt姓名5.Weight = 0.47002968721107924D;
            // 
            // txt姓别5
            // 
            this.txt姓别5.Name = "txt姓别5";
            this.txt姓别5.Weight = 0.29910980095250489D;
            // 
            // txt出生日期5
            // 
            this.txt出生日期5.Name = "txt出生日期5";
            this.txt出生日期5.Weight = 0.55548963034036625D;
            // 
            // txt居住状况5
            // 
            this.txt居住状况5.Name = "txt居住状况5";
            this.txt居住状况5.Weight = 0.82997029974876835D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.txt与户主6,
            this.txt姓名6,
            this.txt姓别6,
            this.txt出生日期6,
            this.txt居住状况6});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1.2D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Text = "6";
            this.xrTableCell37.Weight = 0.3753708945362022D;
            // 
            // txt与户主6
            // 
            this.txt与户主6.Name = "txt与户主6";
            this.txt与户主6.Weight = 0.47002968721107907D;
            // 
            // txt姓名6
            // 
            this.txt姓名6.Name = "txt姓名6";
            this.txt姓名6.Weight = 0.47002968721107924D;
            // 
            // txt姓别6
            // 
            this.txt姓别6.Name = "txt姓别6";
            this.txt姓别6.Weight = 0.29910980095250489D;
            // 
            // txt出生日期6
            // 
            this.txt出生日期6.Name = "txt出生日期6";
            this.txt出生日期6.Weight = 0.55548963034036625D;
            // 
            // txt居住状况6
            // 
            this.txt居住状况6.Name = "txt居住状况6";
            this.txt居住状况6.Weight = 0.82997029974876835D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.txt与户主7,
            this.txt姓名7,
            this.txt姓别7,
            this.txt出生日期7,
            this.txt居住状况7});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1.2D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Text = "7";
            this.xrTableCell43.Weight = 0.3753708945362022D;
            // 
            // txt与户主7
            // 
            this.txt与户主7.Name = "txt与户主7";
            this.txt与户主7.Weight = 0.47002968721107907D;
            // 
            // txt姓名7
            // 
            this.txt姓名7.Name = "txt姓名7";
            this.txt姓名7.Weight = 0.47002968721107924D;
            // 
            // txt姓别7
            // 
            this.txt姓别7.Name = "txt姓别7";
            this.txt姓别7.Weight = 0.29910980095250489D;
            // 
            // txt出生日期7
            // 
            this.txt出生日期7.Name = "txt出生日期7";
            this.txt出生日期7.Weight = 0.55548963034036625D;
            // 
            // txt居住状况7
            // 
            this.txt居住状况7.Name = "txt居住状况7";
            this.txt居住状况7.Weight = 0.82997029974876835D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 18F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 20F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report家庭基本信息表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(51, 53, 18, 20);
            this.PageHeight = 1169;
            this.PageWidth = 1654;
            this.PaperKind = System.Drawing.Printing.PaperKind.A3;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell txt与户主1;
        private DevExpress.XtraReports.UI.XRTableCell txt姓名1;
        private DevExpress.XtraReports.UI.XRTableCell txt姓别1;
        private DevExpress.XtraReports.UI.XRTableCell txt出生日期1;
        private DevExpress.XtraReports.UI.XRTableCell txt居住状况1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell txt与户主2;
        private DevExpress.XtraReports.UI.XRTableCell txt姓名2;
        private DevExpress.XtraReports.UI.XRTableCell txt姓别2;
        private DevExpress.XtraReports.UI.XRTableCell txt出生日期2;
        private DevExpress.XtraReports.UI.XRTableCell txt居住状况2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell txt与户主3;
        private DevExpress.XtraReports.UI.XRTableCell txt姓名3;
        private DevExpress.XtraReports.UI.XRTableCell txt姓别3;
        private DevExpress.XtraReports.UI.XRTableCell txt出生日期3;
        private DevExpress.XtraReports.UI.XRTableCell txt居住状况3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell txt与户主4;
        private DevExpress.XtraReports.UI.XRTableCell txt姓名4;
        private DevExpress.XtraReports.UI.XRTableCell txt姓别4;
        private DevExpress.XtraReports.UI.XRTableCell txt出生日期4;
        private DevExpress.XtraReports.UI.XRTableCell txt居住状况4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell txt与户主5;
        private DevExpress.XtraReports.UI.XRTableCell txt姓名5;
        private DevExpress.XtraReports.UI.XRTableCell txt姓别5;
        private DevExpress.XtraReports.UI.XRTableCell txt出生日期5;
        private DevExpress.XtraReports.UI.XRTableCell txt居住状况5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell txt与户主6;
        private DevExpress.XtraReports.UI.XRTableCell txt姓名6;
        private DevExpress.XtraReports.UI.XRTableCell txt姓别6;
        private DevExpress.XtraReports.UI.XRTableCell txt出生日期6;
        private DevExpress.XtraReports.UI.XRTableCell txt居住状况6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell txt与户主7;
        private DevExpress.XtraReports.UI.XRTableCell txt姓名7;
        private DevExpress.XtraReports.UI.XRTableCell txt姓别7;
        private DevExpress.XtraReports.UI.XRTableCell txt出生日期7;
        private DevExpress.XtraReports.UI.XRTableCell txt居住状况7;
    }
}
