﻿namespace AtomEHR.公共卫生.Module.个人健康.家庭健康信息
{
    partial class frm转入家庭成员
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm转入家庭成员));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo条件 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt条件 = new DevExpress.XtraEditors.TextEdit();
            this.btn查询 = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btn新建 = new DevExpress.XtraEditors.SimpleButton();
            this.btn加入家庭 = new DevExpress.XtraEditors.SimpleButton();
            this.cbo与户主关系 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gc个人健康档案 = new AtomEHR.Library.UserControls.DataGridControl();
            this.gv个人健康档案 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col个人档案号码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col姓名 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col性别 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col出生日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col身份证号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col街道乡镇 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col居村委会 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col居住地址 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col本人电话 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col联系人电话 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col所属机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建人 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col档案状态 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo条件.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt条件.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo与户主关系.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv个人健康档案)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.groupControl1.AppearanceCaption.Options.UseFont = true;
            this.groupControl1.Controls.Add(this.flowLayoutPanel1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(959, 55);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "查询条件";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cbo条件);
            this.flowLayoutPanel1.Controls.Add(this.txt条件);
            this.flowLayoutPanel1.Controls.Add(this.btn查询);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 24);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(955, 29);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // cbo条件
            // 
            this.cbo条件.EditValue = "请选择";
            this.cbo条件.Location = new System.Drawing.Point(3, 3);
            this.cbo条件.Name = "cbo条件";
            this.cbo条件.Properties.AutoHeight = false;
            this.cbo条件.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo条件.Properties.Items.AddRange(new object[] {
            "请选择",
            "姓名",
            "档案号",
            "身份证号"});
            this.cbo条件.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo条件.Size = new System.Drawing.Size(124, 24);
            this.cbo条件.TabIndex = 0;
            // 
            // txt条件
            // 
            this.txt条件.Location = new System.Drawing.Point(133, 3);
            this.txt条件.Name = "txt条件";
            this.txt条件.Properties.AutoHeight = false;
            this.txt条件.Size = new System.Drawing.Size(238, 24);
            this.txt条件.TabIndex = 4;
            // 
            // btn查询
            // 
            this.btn查询.Image = ((System.Drawing.Image)(resources.GetObject("btn查询.Image")));
            this.btn查询.Location = new System.Drawing.Point(377, 3);
            this.btn查询.Name = "btn查询";
            this.btn查询.Size = new System.Drawing.Size(87, 24);
            this.btn查询.TabIndex = 2;
            this.btn查询.Text = "查询";
            this.btn查询.Click += new System.EventHandler(this.btn查询_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.groupControl2.AppearanceCaption.Options.UseFont = true;
            this.groupControl2.Controls.Add(this.btn新建);
            this.groupControl2.Controls.Add(this.btn加入家庭);
            this.groupControl2.Controls.Add(this.cbo与户主关系);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.gc个人健康档案);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.FireScrollEventOnMouseWheel = true;
            this.groupControl2.Location = new System.Drawing.Point(0, 55);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(959, 365);
            this.groupControl2.TabIndex = 6;
            this.groupControl2.Text = "查询结果";
            // 
            // btn新建
            // 
            this.btn新建.Image = ((System.Drawing.Image)(resources.GetObject("btn新建.Image")));
            this.btn新建.Location = new System.Drawing.Point(613, 0);
            this.btn新建.Name = "btn新建";
            this.btn新建.Size = new System.Drawing.Size(150, 21);
            this.btn新建.TabIndex = 9;
            this.btn新建.Text = "新建家庭成员档案";
            this.btn新建.Click += new System.EventHandler(this.btn新建_Click);
            // 
            // btn加入家庭
            // 
            this.btn加入家庭.Image = ((System.Drawing.Image)(resources.GetObject("btn加入家庭.Image")));
            this.btn加入家庭.Location = new System.Drawing.Point(506, 0);
            this.btn加入家庭.Name = "btn加入家庭";
            this.btn加入家庭.Size = new System.Drawing.Size(100, 21);
            this.btn加入家庭.TabIndex = 8;
            this.btn加入家庭.Text = "加入家庭";
            this.btn加入家庭.Click += new System.EventHandler(this.btn加入家庭_Click);
            // 
            // cbo与户主关系
            // 
            this.cbo与户主关系.Location = new System.Drawing.Point(322, 1);
            this.cbo与户主关系.Name = "cbo与户主关系";
            this.cbo与户主关系.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo与户主关系.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo与户主关系.Size = new System.Drawing.Size(177, 20);
            this.cbo与户主关系.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(239, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(72, 14);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "与户主关系：";
            // 
            // gc个人健康档案
            // 
            this.gc个人健康档案.AllowBandedGridColumnSort = false;
            this.gc个人健康档案.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc个人健康档案.IsBestFitColumns = true;
            this.gc个人健康档案.Location = new System.Drawing.Point(2, 24);
            this.gc个人健康档案.MainView = this.gv个人健康档案;
            this.gc个人健康档案.Name = "gc个人健康档案";
            this.gc个人健康档案.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEdit1});
            this.gc个人健康档案.ShowContextMenu = false;
            this.gc个人健康档案.Size = new System.Drawing.Size(955, 339);
            this.gc个人健康档案.StrWhere = "";
            this.gc个人健康档案.TabIndex = 5;
            this.gc个人健康档案.UseCheckBox = true;
            this.gc个人健康档案.View = "";
            this.gc个人健康档案.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv个人健康档案});
            // 
            // gv个人健康档案
            // 
            this.gv个人健康档案.Appearance.ColumnFilterButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ColumnFilterButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.ColumnFilterButtonActive.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ColumnFilterButtonActive.Options.UseFont = true;
            this.gv个人健康档案.Appearance.CustomizationFormHint.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.CustomizationFormHint.Options.UseFont = true;
            this.gv个人健康档案.Appearance.DetailTip.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.DetailTip.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Empty.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Empty.Options.UseFont = true;
            this.gv个人健康档案.Appearance.EvenRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.EvenRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FilterCloseButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FilterCloseButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FilterPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FilterPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FixedLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FixedLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FocusedCell.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FocusedCell.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FocusedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.FocusedRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.FooterPanel.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold);
            this.gv个人健康档案.Appearance.FooterPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupButton.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupButton.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupFooter.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupFooter.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.GroupRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.GroupRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HeaderPanel.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HeaderPanel.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HideSelectionRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.HorzLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.HorzLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.OddRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.OddRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Preview.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Preview.Options.UseFont = true;
            this.gv个人健康档案.Appearance.Row.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.Row.Options.UseFont = true;
            this.gv个人健康档案.Appearance.RowSeparator.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.RowSeparator.Options.UseFont = true;
            this.gv个人健康档案.Appearance.SelectedRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.SelectedRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.TopNewRow.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.TopNewRow.Options.UseFont = true;
            this.gv个人健康档案.Appearance.VertLine.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.VertLine.Options.UseFont = true;
            this.gv个人健康档案.Appearance.ViewCaption.Font = new System.Drawing.Font("宋体", 9F);
            this.gv个人健康档案.Appearance.ViewCaption.Options.UseFont = true;
            this.gv个人健康档案.ColumnPanelRowHeight = 30;
            this.gv个人健康档案.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col个人档案号码,
            this.col姓名,
            this.col性别,
            this.col出生日期,
            this.col身份证号,
            this.col街道乡镇,
            this.col居村委会,
            this.col居住地址,
            this.col本人电话,
            this.col联系人电话,
            this.col所属机构,
            this.col创建人,
            this.col创建时间,
            this.col档案状态});
            this.gv个人健康档案.GridControl = this.gc个人健康档案;
            this.gv个人健康档案.GroupPanelText = "DragColumn";
            this.gv个人健康档案.Name = "gv个人健康档案";
            this.gv个人健康档案.OptionsView.ColumnAutoWidth = false;
            this.gv个人健康档案.OptionsView.EnableAppearanceEvenRow = true;
            this.gv个人健康档案.OptionsView.EnableAppearanceOddRow = true;
            this.gv个人健康档案.OptionsView.ShowGroupPanel = false;
            // 
            // col个人档案号码
            // 
            this.col个人档案号码.AppearanceCell.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col个人档案号码.AppearanceHeader.Options.UseFont = true;
            this.col个人档案号码.AppearanceHeader.Options.UseTextOptions = true;
            this.col个人档案号码.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案号码.Caption = "档案号";
            this.col个人档案号码.FieldName = "个人档案编号";
            this.col个人档案号码.Name = "col个人档案号码";
            this.col个人档案号码.OptionsColumn.AllowEdit = false;
            this.col个人档案号码.OptionsColumn.ReadOnly = true;
            this.col个人档案号码.Visible = true;
            this.col个人档案号码.VisibleIndex = 0;
            this.col个人档案号码.Width = 127;
            // 
            // col姓名
            // 
            this.col姓名.AppearanceCell.Options.UseTextOptions = true;
            this.col姓名.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col姓名.AppearanceHeader.Options.UseFont = true;
            this.col姓名.AppearanceHeader.Options.UseTextOptions = true;
            this.col姓名.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col姓名.Caption = "姓名";
            this.col姓名.FieldName = "姓名";
            this.col姓名.Name = "col姓名";
            this.col姓名.OptionsColumn.AllowEdit = false;
            this.col姓名.Visible = true;
            this.col姓名.VisibleIndex = 1;
            // 
            // col性别
            // 
            this.col性别.AppearanceCell.Options.UseTextOptions = true;
            this.col性别.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col性别.AppearanceHeader.Options.UseFont = true;
            this.col性别.AppearanceHeader.Options.UseTextOptions = true;
            this.col性别.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col性别.Caption = "性别";
            this.col性别.FieldName = "性别";
            this.col性别.Name = "col性别";
            this.col性别.OptionsColumn.AllowEdit = false;
            this.col性别.OptionsColumn.ReadOnly = true;
            this.col性别.Visible = true;
            this.col性别.VisibleIndex = 2;
            // 
            // col出生日期
            // 
            this.col出生日期.AppearanceCell.Options.UseTextOptions = true;
            this.col出生日期.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col出生日期.AppearanceHeader.Options.UseFont = true;
            this.col出生日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出生日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出生日期.Caption = "出生日期";
            this.col出生日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.col出生日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col出生日期.FieldName = "出生日期";
            this.col出生日期.Name = "col出生日期";
            this.col出生日期.OptionsColumn.AllowEdit = false;
            this.col出生日期.Visible = true;
            this.col出生日期.VisibleIndex = 3;
            // 
            // col身份证号
            // 
            this.col身份证号.AppearanceCell.Options.UseTextOptions = true;
            this.col身份证号.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col身份证号.AppearanceHeader.Options.UseFont = true;
            this.col身份证号.AppearanceHeader.Options.UseTextOptions = true;
            this.col身份证号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col身份证号.Caption = "身份证号";
            this.col身份证号.FieldName = "身份证号";
            this.col身份证号.Name = "col身份证号";
            this.col身份证号.OptionsColumn.AllowEdit = false;
            this.col身份证号.Visible = true;
            this.col身份证号.VisibleIndex = 4;
            // 
            // col街道乡镇
            // 
            this.col街道乡镇.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col街道乡镇.AppearanceHeader.Options.UseFont = true;
            this.col街道乡镇.AppearanceHeader.Options.UseTextOptions = true;
            this.col街道乡镇.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col街道乡镇.Caption = "街道/乡镇";
            this.col街道乡镇.Name = "col街道乡镇";
            this.col街道乡镇.OptionsColumn.AllowEdit = false;
            this.col街道乡镇.Visible = true;
            this.col街道乡镇.VisibleIndex = 5;
            // 
            // col居村委会
            // 
            this.col居村委会.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col居村委会.AppearanceHeader.Options.UseFont = true;
            this.col居村委会.AppearanceHeader.Options.UseTextOptions = true;
            this.col居村委会.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col居村委会.Caption = "居/村委会";
            this.col居村委会.Name = "col居村委会";
            this.col居村委会.OptionsColumn.AllowEdit = false;
            this.col居村委会.Visible = true;
            this.col居村委会.VisibleIndex = 6;
            // 
            // col居住地址
            // 
            this.col居住地址.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col居住地址.AppearanceHeader.Options.UseFont = true;
            this.col居住地址.AppearanceHeader.Options.UseTextOptions = true;
            this.col居住地址.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col居住地址.Caption = "居住地址";
            this.col居住地址.FieldName = "居住地址";
            this.col居住地址.Name = "col居住地址";
            this.col居住地址.OptionsColumn.AllowEdit = false;
            this.col居住地址.OptionsColumn.ReadOnly = true;
            this.col居住地址.Visible = true;
            this.col居住地址.VisibleIndex = 7;
            this.col居住地址.Width = 104;
            // 
            // col本人电话
            // 
            this.col本人电话.AppearanceCell.Options.UseTextOptions = true;
            this.col本人电话.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本人电话.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col本人电话.AppearanceHeader.Options.UseFont = true;
            this.col本人电话.AppearanceHeader.Options.UseTextOptions = true;
            this.col本人电话.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col本人电话.Caption = "本人电话";
            this.col本人电话.FieldName = "本人电话";
            this.col本人电话.Name = "col本人电话";
            this.col本人电话.OptionsColumn.AllowEdit = false;
            this.col本人电话.OptionsColumn.ReadOnly = true;
            this.col本人电话.Visible = true;
            this.col本人电话.VisibleIndex = 8;
            // 
            // col联系人电话
            // 
            this.col联系人电话.AppearanceCell.Options.UseTextOptions = true;
            this.col联系人电话.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col联系人电话.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col联系人电话.AppearanceHeader.Options.UseFont = true;
            this.col联系人电话.AppearanceHeader.Options.UseTextOptions = true;
            this.col联系人电话.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col联系人电话.Caption = "联系人电话";
            this.col联系人电话.FieldName = "联系人电话";
            this.col联系人电话.Name = "col联系人电话";
            this.col联系人电话.OptionsColumn.AllowEdit = false;
            this.col联系人电话.OptionsColumn.ReadOnly = true;
            this.col联系人电话.Visible = true;
            this.col联系人电话.VisibleIndex = 9;
            // 
            // col所属机构
            // 
            this.col所属机构.AppearanceCell.Options.UseTextOptions = true;
            this.col所属机构.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col所属机构.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col所属机构.AppearanceHeader.Options.UseFont = true;
            this.col所属机构.AppearanceHeader.Options.UseTextOptions = true;
            this.col所属机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col所属机构.Caption = "当前所属机构";
            this.col所属机构.FieldName = "所属机构";
            this.col所属机构.Name = "col所属机构";
            this.col所属机构.OptionsColumn.AllowEdit = false;
            this.col所属机构.OptionsColumn.ReadOnly = true;
            this.col所属机构.Visible = true;
            this.col所属机构.VisibleIndex = 10;
            this.col所属机构.Width = 93;
            // 
            // col创建人
            // 
            this.col创建人.AppearanceCell.Options.UseTextOptions = true;
            this.col创建人.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建人.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col创建人.AppearanceHeader.Options.UseFont = true;
            this.col创建人.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建人.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建人.Caption = "录入人";
            this.col创建人.FieldName = "创建人";
            this.col创建人.Name = "col创建人";
            this.col创建人.OptionsColumn.AllowEdit = false;
            this.col创建人.OptionsColumn.ReadOnly = true;
            this.col创建人.Visible = true;
            this.col创建人.VisibleIndex = 11;
            // 
            // col创建时间
            // 
            this.col创建时间.AppearanceCell.Options.UseTextOptions = true;
            this.col创建时间.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col创建时间.AppearanceHeader.Options.UseFont = true;
            this.col创建时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col创建时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col创建时间.Caption = "录入时间";
            this.col创建时间.DisplayFormat.FormatString = "d";
            this.col创建时间.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col创建时间.FieldName = "创建时间";
            this.col创建时间.Name = "col创建时间";
            this.col创建时间.OptionsColumn.AllowEdit = false;
            this.col创建时间.OptionsColumn.ReadOnly = true;
            this.col创建时间.Visible = true;
            this.col创建时间.VisibleIndex = 12;
            // 
            // col档案状态
            // 
            this.col档案状态.AppearanceCell.Options.UseTextOptions = true;
            this.col档案状态.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col档案状态.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.col档案状态.AppearanceHeader.Options.UseFont = true;
            this.col档案状态.AppearanceHeader.Options.UseTextOptions = true;
            this.col档案状态.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col档案状态.Caption = "档案状态";
            this.col档案状态.FieldName = "档案状态";
            this.col档案状态.Name = "col档案状态";
            this.col档案状态.OptionsColumn.AllowEdit = false;
            this.col档案状态.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            // 
            // frm转入家庭成员
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 420);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Name = "frm转入家庭成员";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "转入家庭成员";
            this.Load += new System.EventHandler(this.frm转入家庭成员_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo条件.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt条件.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo与户主关系.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv个人健康档案)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txt条件;
        private DevExpress.XtraEditors.SimpleButton btn查询;
        private DevExpress.XtraEditors.ComboBoxEdit cbo条件;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private Library.UserControls.DataGridControl gc个人健康档案;
        private DevExpress.XtraGrid.Views.Grid.GridView gv个人健康档案;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案号码;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn col姓名;
        private DevExpress.XtraGrid.Columns.GridColumn col性别;
        private DevExpress.XtraGrid.Columns.GridColumn col出生日期;
        private DevExpress.XtraGrid.Columns.GridColumn col身份证号;
        private DevExpress.XtraGrid.Columns.GridColumn col居住地址;
        private DevExpress.XtraGrid.Columns.GridColumn col本人电话;
        private DevExpress.XtraGrid.Columns.GridColumn col联系人电话;
        private DevExpress.XtraGrid.Columns.GridColumn col所属机构;
        private DevExpress.XtraGrid.Columns.GridColumn col创建人;
        private DevExpress.XtraGrid.Columns.GridColumn col创建时间;
        private DevExpress.XtraGrid.Columns.GridColumn col档案状态;
        private DevExpress.XtraGrid.Columns.GridColumn col街道乡镇;
        private DevExpress.XtraGrid.Columns.GridColumn col居村委会;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn新建;
        private DevExpress.XtraEditors.SimpleButton btn加入家庭;
        private DevExpress.XtraEditors.ComboBoxEdit cbo与户主关系;
        private DevExpress.XtraEditors.LabelControl labelControl1;
    }
}