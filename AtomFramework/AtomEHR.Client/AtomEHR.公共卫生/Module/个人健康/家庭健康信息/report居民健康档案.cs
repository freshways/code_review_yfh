﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.家庭健康信息
{
    public partial class report居民健康档案 : DevExpress.XtraReports.UI.XtraReport
    {
        string docNo;
        DataSet _ds家庭;
        bll家庭档案 _bll = new bll家庭档案();

        public report居民健康档案()
        {
            InitializeComponent();
        }

        public report居民健康档案(string _docNo)
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds家庭 = _bll.GetInfoByFamily(_docNo, true);
            DoBindingDataSource(_ds家庭);
        }

        private void DoBindingDataSource(DataSet _ds家庭)
        {
            DataTable dt家庭 = _ds家庭.Tables[Models.tb_家庭档案.__TableName];
            //DataTable dt健康档案 = _ds家庭.Tables[Models.tb_健康档案.__TableName];

            if (dt家庭 == null || dt家庭.Rows.Count == 0) return;

            DataTable dt健康档案 = _ds家庭.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());

            this.txt现住址.Text = dt健康档案.Select("与户主关系='1'")[0][tb_健康档案.居住地址].ToString();
            string 省 = _bll.Return地区名称(dt健康档案.Select("与户主关系='1'")[0][tb_健康档案.省].ToString());
            string 市 = _bll.Return地区名称(dt健康档案.Select("与户主关系='1'")[0][tb_健康档案.市].ToString());
            string 区 = _bll.Return地区名称(dt健康档案.Select("与户主关系='1'")[0][tb_健康档案.区].ToString());
            string 街道=_bll.Return地区名称(dt健康档案.Select("与户主关系='1'")[0][tb_健康档案.街道].ToString());
            string 居委会=_bll.Return地区名称(dt健康档案.Select("与户主关系='1'")[0][tb_健康档案.居委会].ToString());
            this.txt户籍.Text = 省 + 市 + 区 + 街道 + 居委会;
            this.txt电话.Text = dt健康档案.Select("与户主关系='1'")[0][tb_健康档案.本人电话].ToString();
            this.txt乡镇.Text =_bll.Return地区名称( dt健康档案.Select("与户主关系='1'")[0][tb_健康档案.街道].ToString());
            this.txt村委会.Text = _bll.Return地区名称(dt健康档案.Select("与户主关系='1'")[0][tb_健康档案.居委会].ToString());

            this.txt建档单位.Text = _bll.Return机构名称(dt家庭.Rows[0][tb_家庭档案.创建机构].ToString());
            this.txt建档人.Text = _bll.Return用户名称(dt家庭.Rows[0][tb_家庭档案.创建人].ToString());
            //this.txt责任医生.Text = dt家庭.Rows[0][tb_家庭档案].ToString();
            this.txt建档日期.Text = Convert.ToDateTime(dt家庭.Rows[0][tb_家庭档案.创建时间]).ToLongDateString().ToString();//将时间格式转换为年月日
        }
    }
}
