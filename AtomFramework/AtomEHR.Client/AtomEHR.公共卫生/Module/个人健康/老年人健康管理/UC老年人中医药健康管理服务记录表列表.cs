﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business.Security;
using AtomEHR.Models;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    public partial class UC老年人中医药健康管理服务记录表列表 : UserControlBase
    {

        #region Fields
        private DataSet ds;
        private frm个人健康 _frm个人健康;
        bll老年人中医药特征管理 _Bll = new Business.bll老年人中医药特征管理();
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        #endregion

        public UC老年人中医药健康管理服务记录表列表()
        {
            InitializeComponent();
        }

        public UC老年人中医药健康管理服务记录表列表(Form frm个人健康)
        {
            InitializeComponent();
            this._frm个人健康 = (frm个人健康)frm个人健康;
            _docNo = this._frm个人健康._docNo;

            ds = _Bll.GetAllDataValueByKey(_docNo);
            BindDataSource(ds);

        }

        private void BindDataSource(DataSet ds)
        {
            DataTable dt老年人信息 = ds.Tables[tb_老年人基本信息.__TableName];
            DataTable dt中医药 = ds.Tables[tb_老年人中医药特征管理.__TableName];
            if (dt老年人信息.Rows.Count == 1)
            {
                DataRow dr = dt老年人信息.Rows[0];
                this.txt姓名.Text = util.DESEncrypt.DES解密(dr[tb_老年人基本信息.姓名].ToString());
                this.txt性别.Text = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_老年人基本信息.性别].ToString());
                this.txt档案编号.Text = dr[tb_老年人基本信息.个人档案编号].ToString();
                this.txt身份证号.Text = dr[tb_老年人基本信息.身份证号].ToString();
                this.txt出生日期.Text = dr[tb_老年人基本信息.出生日期].ToString();
                this.txt联系电话.Text = dr[tb_老年人基本信息.联系电话].ToString();
                this.txt居住状态.Text = _BLL.ReturnDis字典显示("jzzk", dr[tb_老年人基本信息.常住类型].ToString());
                this.txt居住地址.Text = _BLL.Return地区名称(dr[tb_老年人基本信息.省].ToString()) + _BLL.Return地区名称(dr[tb_老年人基本信息.市].ToString()) + _BLL.Return地区名称(dr[tb_老年人基本信息.区].ToString()) + _BLL.Return地区名称(dr[tb_老年人基本信息.街道].ToString()) + _BLL.Return地区名称(dr[tb_老年人基本信息.居委会].ToString()) + dr[tb_老年人基本信息.居住地址].ToString();

            }
            //if (dt中医药.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dt中医药.Rows.Count; i++)
            //    {

            //    }
            //}
            this.gc老年人中医药健康管理记录.DataSource = dt中医药;
            this.gv老年人中医药健康管理记录.BestFitColumns();

        }

        private void btn添加_Click(object sender, EventArgs e)
        {
            UC老年人中医药健康管理服务记录表 uc = new UC老年人中医药健康管理服务记录表(_frm个人健康, UpdateType.Add);
            ShowControl(uc, DockStyle.Fill);
        }

        private void link随访_Click(object sender, EventArgs e)
        {
            DataRow row = gv老年人中医药健康管理记录.GetFocusedDataRow();
            string id = row["ID"].ToString();
            _frm个人健康._param = id;
            UC老年人中医药健康管理服务记录表_显示 uc = new UC老年人中医药健康管理服务记录表_显示(_frm个人健康);
            ShowControl(uc, DockStyle.Fill);
            _frm个人健康._param = null;
        }
    }
}
