﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business.Security;
using AtomEHR.Common;
using DevExpress.XtraEditors;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    public partial class UC老年人中医药健康管理服务记录表 : UserControlBase
    {
        #region Fields
        AtomEHR.Business.bll老年人中医药特征管理 _Bll = new Business.bll老年人中医药特征管理();
        DataSet _ds老年人中医药;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据

        int t1 = 0, t2 = 0, t3 = 0, t4 = 0, t5 = 0, t6 = 0, t7 = 0, t8 = 0, t9 = 0, t10 = 0, t11 = 0, t12 = 0, t13 = 0, t14 = 0, t15 = 0, t16 = 0, t17 = 0, t18 = 0, t19 = 0, t20 = 0, t21 = 0, t22 = 0, t23 = 0, t24 = 0, t25 = 0, t26 = 0, t27 = 0, t28 = 0, t29 = 0, t30 = 0, t31 = 0, t32 = 0, t33 = 0;
        string is气虚质, is阳虚质, is阴虚质, is痰湿质, is湿热质, is血瘀质, is气郁质, is特禀质, is平和质;
        int jf1 = 0, jf2 = 0, jf3 = 0, jf4 = 0, jf5 = 0, jf6 = 0, jf7 = 0, jf8 = 0, jf9 = 0;
        #endregion

        public UC老年人中医药健康管理服务记录表()
        {
            InitializeComponent();
        }
        public UC老年人中医药健康管理服务记录表(Form frm, UpdateType _updateType)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _serverDateTime = _Bll.ServiceDateTime;
            _UpdateType = _updateType;
            _id = _frm._param as string;
            if (_UpdateType == UpdateType.Add)//添加
            {
                _Bll.GetBusinessByKey(_docNo, true);
                //_Bll.NewBusiness();
                _ds老年人中医药 = _Bll.CurrentBusiness;
            }
            else if (_UpdateType == UpdateType.Modify)//修改
            {
                _ds老年人中医药 = _Bll.GetOneDataByKey(_docNo, _id, true);
                // this.dte随访日期.Properties.ReadOnly = true;
                //this.dte下次随访日期.Properties.ReadOnly = true;
            }
            DoBindingSummaryEditor(_ds老年人中医药);//绑定数据
        }
        private void DoBindingSummaryEditor(DataSet _ds老年人中医药)
        {
            if (_ds老年人中医药 == null) return;
            if (_ds老年人中医药.Tables.Count == 0) return;
            ////if (_ds老年人随访.Tables.Contains(tb_老年人随访.__TableName))
            ////{
            DataTable dt老年人信息 = _ds老年人中医药.Tables[tb_老年人基本信息.__TableName];
            DataTable dt中医药 = _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName];
            if (dt老年人信息.Rows.Count == 1)
            {
                DataRow dr = dt老年人信息.Rows[0];
                this.txt姓名.Text = util.DESEncrypt.DES解密(dr[tb_老年人基本信息.姓名].ToString());
                this.txt性别.Text = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_老年人基本信息.性别].ToString());
                this.txt个人档案号.Text = dr[tb_老年人基本信息.个人档案编号].ToString();
                this.txt身份证号.Text = dr[tb_老年人基本信息.身份证号].ToString();
                this.txt出生日期.Text = dr[tb_老年人基本信息.出生日期].ToString();
                this.txt联系电话.Text = dr[tb_老年人基本信息.联系电话].ToString();
                this.txt居住状态.Text = _BLL.ReturnDis字典显示("jzzk", dr[tb_老年人基本信息.常住类型].ToString());
                this.txt居住地址.Text = _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.省].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.市].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.区].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.街道].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.居委会].ToString()) + dr[tb_老年人基本信息.居住地址].ToString();
            }
            if (dt中医药.Rows.Count == 1)
            {
                BindData(dt中医药.Rows[0]);
            }
            if (_UpdateType == UpdateType.Add)
            {
                this.txt创建人.Text = Loginer.CurrentUser.AccountName;
                this.txt创建时间.Text = _serverDateTime;
                this.txt最近更新时间.Text = _serverDateTime;
                this.txt当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
                this.txt创建机构.Text = Loginer.CurrentUser.所属机构名称;
                this.txt最近修改人.Text = Loginer.CurrentUser.AccountName;
                this.dte填表日期.DateTime = DateTime.Now;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                this.txt创建人.Text = _Bll.Return用户名称(dt中医药.Rows[0][tb_老年人中医药特征管理.创建人].ToString());
                this.txt创建时间.Text = _serverDateTime;
                this.txt最近更新时间.Text = _serverDateTime;
                this.txt当前所属机构.Text = _Bll.Return机构名称(dt中医药.Rows[0][tb_老年人中医药特征管理.所属机构].ToString());
                this.txt创建机构.Text = _Bll.Return机构名称(dt中医药.Rows[0][tb_老年人中医药特征管理.创建机构].ToString());// Loginer.CurrentUser.所属机构名称;
                this.txt最近修改人.Text = Loginer.CurrentUser.AccountName; //Loginer.CurrentUser.AccountName;
            }
        }

        private void BindData(DataRow dataRow)
        {
            if (dataRow == null) return;
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征1].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征1]);
                SetRadioValue(1, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征2].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征2]);
                SetRadioValue(2, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征3].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征3]);
                SetRadioValue(3, score);
            }

            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征4].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征4]);
                SetRadioValue(4, score);
            }

            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征5].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征5]);
                SetRadioValue(5, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征6].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征6]);
                SetRadioValue(6, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征7].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征7]);
                SetRadioValue(7, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征8].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征8]);
                SetRadioValue(8, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征9].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征9]);
                SetRadioValue(9, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征10].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征10]);
                SetRadioValue(10, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征11].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征11]);
                SetRadioValue(11, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征12].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征12]);
                SetRadioValue(12, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征13].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征13]);
                SetRadioValue(13, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征14].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征14]);
                SetRadioValue(14, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征15].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征15]);
                SetRadioValue(15, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征16].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征16]);
                SetRadioValue(16, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征17].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征17]);
                SetRadioValue(17, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征18].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征18]);
                SetRadioValue(18, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征19].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征19]);
                SetRadioValue(19, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征20].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征20]);
                SetRadioValue(20, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征21].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征21]);
                SetRadioValue(21, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征22].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征22]);
                SetRadioValue(22, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征23].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征23]);
                SetRadioValue(23, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征24].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征24]);
                SetRadioValue(24, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征25].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征25]);
                SetRadioValue(25, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征26].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征26]);
                SetRadioValue(26, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征27].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征27]);
                SetRadioValue(27, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征28].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征28]);
                SetRadioValue(28, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征29].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征29]);
                SetRadioValue(29, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征30].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征30]);
                SetRadioValue(30, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征31].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征31]);
                SetRadioValue(31, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征32].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征32]);
                SetRadioValue(32, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征33].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征33]);
                SetRadioValue(33, score);
            }

            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.气虚质指导].ToString()))
            {
                SetFlowLayoutResult(dataRow[tb_老年人中医药特征管理.气虚质指导].ToString(), flow1);
                this.txt1.Text = dataRow[tb_老年人中医药特征管理.气郁质其他].ToString();
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.阳虚质指导].ToString()))
            {
                SetFlowLayoutResult(dataRow[tb_老年人中医药特征管理.阳虚质指导].ToString(), flow2);
                this.txt2.Text = dataRow[tb_老年人中医药特征管理.阳虚质其他].ToString();
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.阴虚质指导].ToString()))
            {
                SetFlowLayoutResult(dataRow[tb_老年人中医药特征管理.阴虚质指导].ToString(), flow3);
                this.txt3.Text = dataRow[tb_老年人中医药特征管理.阴虚质其他].ToString();
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.痰湿质指导].ToString()))
            {
                SetFlowLayoutResult(dataRow[tb_老年人中医药特征管理.痰湿质指导].ToString(), flow4);
                this.txt4.Text = dataRow[tb_老年人中医药特征管理.痰湿质其他].ToString();
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.湿热质指导].ToString()))
            {
                SetFlowLayoutResult(dataRow[tb_老年人中医药特征管理.湿热质指导].ToString(), flow5);
                this.txt5.Text = dataRow[tb_老年人中医药特征管理.湿热质其他].ToString();
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.血瘀质指导].ToString()))
            {
                SetFlowLayoutResult(dataRow[tb_老年人中医药特征管理.血瘀质指导].ToString(), flow6);
                this.txt6.Text = dataRow[tb_老年人中医药特征管理.血瘀质其他].ToString();
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.气郁质指导].ToString()))
            {
                SetFlowLayoutResult(dataRow[tb_老年人中医药特征管理.气郁质指导].ToString(), flow7);
                this.txt7.Text = dataRow[tb_老年人中医药特征管理.气郁质其他].ToString();
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特禀质指导].ToString()))
            {
                SetFlowLayoutResult(dataRow[tb_老年人中医药特征管理.特禀质指导].ToString(), flow8);
                this.txt8.Text = dataRow[tb_老年人中医药特征管理.特禀质其他].ToString();
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.平和质指导].ToString()))
            {
                SetFlowLayoutResult(dataRow[tb_老年人中医药特征管理.平和质指导].ToString(), flow9);
                this.txt9.Text = dataRow[tb_老年人中医药特征管理.平和质其他].ToString();
            }

            this.dte填表日期.Text = dataRow[tb_老年人中医药特征管理.发生时间].ToString();
            this.txt医生签名.Text = dataRow[tb_老年人中医药特征管理.医生签名].ToString();
        }
        /// <summary>
        /// 根据index 和 tag 来选中对应的checkedit
        /// </summary>
        /// <param name="groupIndex"></param>
        /// <param name="score"></param>
        private void SetRadioValue(int groupIndex, int score)
        {
            for (int i = 0; i < this.layoutControl1.Controls.Count; i++)
            {
                if (layoutControl1.Controls[i] is CheckEdit)
                {
                    CheckEdit chk = (CheckEdit)layoutControl1.Controls[i];
                    if (chk.Properties.RadioGroupIndex == groupIndex)//同一个组下的
                    {
                        int tag = Convert.ToInt32(chk.Tag);
                        if (tag == score)
                        {
                            chk.Checked = true;
                            break;
                        }
                    }
                }
            }
        }

        private void 计算体质()
        {
            if (t1 != 0 && t2 != 0 && t3 != 0 && t4 != 0 && t5 != 0 && t6 != 0 && t7 != 0 && t8 != 0 && t9 != 0 && t10 != 0 && t11 != 0 && t12 != 0 && t13 != 0 && t14 != 0 && t15 != 0 && t16 != 0 && t17 != 0 && t18 != 0 && t19 != 0 && t20 != 0 && t21 != 0 && t22 != 0 && t23 != 0 && t24 != 0 && t25 != 0 && t26 != 0 && t27 != 0 && t28 != 0 && t29 != 0 && t30 != 0 && t31 != 0 && t32 != 0 && t33 != 0)
            {
                //算每种体质的积分
                jf1 = t2 + t3 + t4 + t14;
                jf2 = t11 + t12 + t13 + t29;
                jf3 = t10 + t21 + t26 + t31;
                jf4 = t9 + t16 + t28 + t32;
                jf5 = t23 + t25 + t27 + t30;
                jf6 = t19 + t22 + t24 + t33;
                jf7 = t5 + t6 + t7 + t8;
                jf8 = t15 + t17 + t18 + t20;
                jf9 = t1 + (6 - t2) + (6 - t4) + (6 - t5) + (6 - t13);
                //赋积分
                this.ucqxdf.Txt1.Text = jf1.ToString();
                this.ucyangdf.Txt1.Text = jf2.ToString();
                this.ucyindf.Txt1.Text = jf3.ToString();
                this.uctansdf.Txt1.Text = jf4.ToString();
                this.ucshirdf.Txt1.Text = jf5.ToString();
                this.ucxueydf.Txt1.Text = jf6.ToString();
                this.ucqiydf.Txt1.Text = jf7.ToString();
                this.uctebdf.Txt1.Text = jf8.ToString();
                this.ucpinghdf.Txt1.Text = jf9.ToString();

                //根据积分赋体质辨识
                if (jf1 >= 9 && jf1 <= 10)
                {
                    is气虚质 = "2";
                    this.labelControl1.Text = "倾向是";
                    this.sp1.ForeColor = System.Drawing.Color.Blue;
                    SetFlowLayoutResult("2,4",flow1);
                }
                else if (jf1 >= 11)
                {
                    is气虚质 = "1";
                    this.labelControl1.Text = "是";
                    this.sp1.ForeColor = System.Drawing.Color.Red;
                    SetFlowLayoutResult("2,4", flow1);
                }
                else
                {
                    is气虚质 = "";
                    this.labelControl1.Text = "";
                    this.sp1.ForeColor = System.Drawing.Color.Black;
                    SetFlowLayoutResult(flow1, false);
                }
                if (jf2 >= 9 && jf2 <= 10)
                {
                    is阳虚质 = "2";
                    this.labelControl2.Text = "倾向是";
                    this.sp2.ForeColor = System.Drawing.Color.Blue;
                    SetFlowLayoutResult("2,4", flow2);
                }
                else if (jf2 >= 11)
                {
                    is阳虚质 = "1";
                    this.labelControl2.Text = "是";
                    this.sp2.ForeColor = System.Drawing.Color.Red;
                    SetFlowLayoutResult("2,4", flow2);
                }
                else
                {
                    is阳虚质 = "";
                    this.labelControl2.Text = "";
                    this.sp2.ForeColor = System.Drawing.Color.Black;
                    SetFlowLayoutResult(flow2, false);
                }
                if (jf3 >= 9 && jf3 <= 10)
                {
                    is阴虚质 = "2";
                    this.labelControl3.Text = "倾向是";
                    this.sp3.ForeColor = System.Drawing.Color.Blue;
                    SetFlowLayoutResult("2,4", flow3);
                }
                else if (jf3 >= 11)
                {
                    is阴虚质 = "1";
                    this.labelControl3.Text = "是";
                    this.sp3.ForeColor = System.Drawing.Color.Red;
                    SetFlowLayoutResult("2,4", flow3);
                }
                else
                {
                    is阴虚质 = "";
                    this.labelControl3.Text = "";
                    this.sp3.ForeColor = System.Drawing.Color.Black;
                    SetFlowLayoutResult(flow3, false);
                }
                if (jf4 >= 9 && jf4 <= 10)
                {
                    is痰湿质 = "2";
                    this.labelControl4.Text = "倾向是";
                    this.sp4.ForeColor = System.Drawing.Color.Blue;
                    SetFlowLayoutResult("2,3", flow4);
                }
                else if (jf4 >= 11)
                {
                    is痰湿质 = "1";
                    this.labelControl4.Text = "是";
                    this.sp4.ForeColor = System.Drawing.Color.Red;
                    SetFlowLayoutResult("2,3", flow4);
                }
                else
                {
                    is痰湿质 = "";
                    this.labelControl4.Text = "";
                    this.sp4.ForeColor = System.Drawing.Color.Black;
                    SetFlowLayoutResult(flow4, false);
                }
                if (jf5 >= 9 && jf5 <= 10)
                {
                    is湿热质 = "2";
                    this.labelControl5.Text = "倾向是";
                    this.sp5.ForeColor = System.Drawing.Color.Blue;
                    SetFlowLayoutResult("2,3", flow5);
                }
                else if (jf5 >= 11)
                {
                    is湿热质 = "1";
                    this.labelControl5.Text = "是";
                    this.sp5.ForeColor = System.Drawing.Color.Red;
                    SetFlowLayoutResult("2,3", flow5);
                }
                else
                {
                    is湿热质 = "";
                    this.labelControl5.Text = "";
                    this.sp5.ForeColor = System.Drawing.Color.Black;
                    SetFlowLayoutResult(flow5, false);
                }
                if (jf6 >= 9 && jf6 <= 10)
                {
                    is血瘀质 = "2";
                    this.labelControl6.Text = "倾向是";
                    this.sp6.ForeColor = System.Drawing.Color.Blue;
                    SetFlowLayoutResult("1,4", flow6);
                }
                else if (jf6 >= 11)
                {
                    is血瘀质 = "1";
                    this.labelControl6.Text = "是";
                    this.sp6.ForeColor = System.Drawing.Color.Red;
                    SetFlowLayoutResult("1,4", flow6);
                }
                else
                {
                    is血瘀质 = "";
                    this.labelControl6.Text = "";
                    this.sp6.ForeColor = System.Drawing.Color.Black;
                    SetFlowLayoutResult(flow6, false);
                }
                if (jf7 >= 9 && jf7 <= 10)
                {
                    is气郁质 = "2";
                    this.labelControl7.Text = "倾向是";
                    this.sp7.ForeColor = System.Drawing.Color.Blue;
                    SetFlowLayoutResult("1,4", flow7);
                }
                else if (jf7 >= 11)
                {
                    is气郁质 = "1";
                    this.labelControl7.Text = "是";
                    this.sp7.ForeColor = System.Drawing.Color.Red;
                    SetFlowLayoutResult("1,4", flow7);
                }
                else
                {
                    is气郁质 = "";
                    this.labelControl7.Text = "";
                    this.sp7.ForeColor = System.Drawing.Color.Black;
                    SetFlowLayoutResult(flow7, false);
                }
                if (jf8 >= 9 && jf8 <= 10)
                {
                    is特禀质 = "2";
                    this.labelControl8.Text = "倾向是";
                    this.sp8.ForeColor = System.Drawing.Color.Blue;
                    SetFlowLayoutResult("2,3,4", flow8);
                }
                else if (jf8 >= 11)
                {
                    is特禀质 = "1";
                    this.labelControl8.Text = "是";
                    this.sp8.ForeColor = System.Drawing.Color.Red;
                    SetFlowLayoutResult("2,3,4", flow8);
                }
                else
                {
                    is特禀质 = "";
                    this.labelControl8.Text = "";
                    this.sp8.ForeColor = System.Drawing.Color.Black;
                    SetFlowLayoutResult(flow8, false);
                }
                if (jf9 >= 17)
                {
                    if (jf1 < 8 && jf2 < 8 && jf3 < 8 && jf4 < 8 && jf5 < 8 && jf6 < 8 && jf7 < 8 && jf8 < 8)
                    {
                        is平和质 = "1";
                        this.labelControl9.Text = "是";
                        this.sp9.ForeColor = System.Drawing.Color.Red;
                        SetFlowLayoutResult("4", flow9);
                    }
                    else if (jf1 < 10 && jf2 < 10 && jf3 < 10 && jf4 < 10 && jf5 < 10 && jf6 < 10 && jf7 < 10 && jf8 < 10)
                    {
                        is平和质 = "2";
                        this.labelControl9.Text = "基本是";
                        this.sp9.ForeColor = System.Drawing.Color.Blue;
                        SetFlowLayoutResult("4", flow9);
                    }
                    else
                    {
                        is平和质 = "";
                        this.labelControl9.Text = "";
                        this.sp9.ForeColor = System.Drawing.Color.Black;
                        SetFlowLayoutResult(flow9, false);
                    }
                }
                else
                {
                    is平和质 = "";
                    this.labelControl9.Text = "";
                    this.sp9.ForeColor = System.Drawing.Color.Black;
                }
            }
            else
            {
                Clear();
            }
        }
        /// <summary>
        /// 清空
        /// </summary>
        private void Clear()
        {
            this.ucpinghdf.Txt1.Text = "";
            this.ucqiydf.Txt1.Text = "";
            this.ucqxdf.Txt1.Text = "";
            this.ucshirdf.Txt1.Text = "";
            this.uctansdf.Txt1.Text = "";
            this.uctebdf.Txt1.Text = "";
            this.ucxueydf.Txt1.Text = "";
            this.ucyangdf.Txt1.Text = "";
            this.ucyindf.Txt1.Text = "";

            this.labelControl1.Text = "";
            this.labelControl2.Text = "";
            this.labelControl3.Text = "";
            this.labelControl4.Text = "";
            this.labelControl5.Text = "";
            this.labelControl6.Text = "";
            this.labelControl7.Text = "";
            this.labelControl8.Text = "";
            this.labelControl9.Text = "";

            this.sp1.ForeColor = System.Drawing.Color.Black;
            this.sp2.ForeColor = System.Drawing.Color.Black;
            this.sp3.ForeColor = System.Drawing.Color.Black;
            this.sp4.ForeColor = System.Drawing.Color.Black;
            this.sp5.ForeColor = System.Drawing.Color.Black;
            this.sp6.ForeColor = System.Drawing.Color.Black;
            this.sp7.ForeColor = System.Drawing.Color.Black;
            this.sp8.ForeColor = System.Drawing.Color.Black;
            this.sp9.ForeColor = System.Drawing.Color.Black;
        }

        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit chk = (CheckEdit)sender;
            if (chk == null) return;
            if (chk.Checked)
            {
                int index = chk.Properties.RadioGroupIndex;
                int tag = Convert.ToInt32(chk.Tag);
                #region 变量赋值
                switch (index)
                {
                    case 1:
                        this.t1 = tag;
                        break;
                    case 2:
                        this.t2 = tag;
                        break;
                    case 3:
                        this.t3 = tag;
                        break;
                    case 4:
                        this.t4 = tag;
                        break;
                    case 5:
                        this.t5 = tag;
                        break;
                    case 6:
                        this.t6 = tag;
                        break;
                    case 7:
                        this.t7 = tag; break;
                    case 8:
                        this.t8 = tag; break;
                    case 9:
                        this.t9 = tag; break;
                    case 10:
                        this.t10 = tag; break;
                    case 11:
                        this.t11 = tag; break;
                    case 12:
                        this.t12 = tag; break;
                    case 13:
                        this.t13 = tag; break;
                    case 14:
                        this.t14 = tag; break;
                    case 15:
                        this.t15 = tag; break;
                    case 16:
                        this.t16 = tag; break;
                    case 17:
                        this.t17 = tag; break;
                    case 18:
                        this.t18 = tag; break;
                    case 19:
                        this.t19 = tag; break;
                    case 20:
                        this.t20 = tag; break;
                    case 21:
                        this.t21 = tag; break;
                    case 22:
                        this.t22 = tag; break;
                    case 23:
                        this.t23 = tag; break;
                    case 24:
                        this.t24 = tag; break;
                    case 25:
                        this.t25 = tag; break;
                    case 26:
                        this.t26 = tag; break;
                    case 27:
                        this.t27 = tag; break;
                    case 28:
                        this.t28 = tag; break;
                    case 29:
                        this.t29 = tag; break;
                    case 30:
                        this.t30 = tag; break;
                    case 31:
                        this.t31 = tag; break;
                    case 32:
                        this.t32 = tag; break;
                    case 33:
                        this.t33 = tag; break;
                    default:
                        break;
                }

                #endregion

                计算体质();
            }
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            if (fun必填项检查())
            {
                if (_UpdateType == UpdateType.Add)//添加
                {
                    #region tb_老年人中医药特征管理
                    DataRow row = _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows.Add();
                    row[tb_老年人中医药特征管理.个人档案编号] = _docNo;

                    row[tb_老年人中医药特征管理.气虚质辨识] = is气虚质;
                    row[tb_老年人中医药特征管理.气虚质得分] = jf1;
                    row[tb_老年人中医药特征管理.气虚质其他] = this.txt1.Text.Trim();
                    row[tb_老年人中医药特征管理.气虚质指导] = GetFlowLayoutResult(flow1);

                    row[tb_老年人中医药特征管理.阳虚质辨识] = is阳虚质;
                    row[tb_老年人中医药特征管理.阳虚质得分] = jf2;
                    row[tb_老年人中医药特征管理.阳虚质其他] = this.txt2.Text.Trim();
                    row[tb_老年人中医药特征管理.阳虚质指导] = GetFlowLayoutResult(flow2);

                    row[tb_老年人中医药特征管理.阴虚质辨识] = is阴虚质;
                    row[tb_老年人中医药特征管理.阴虚质得分] = jf3;
                    row[tb_老年人中医药特征管理.阴虚质其他] = this.txt3.Text.Trim();
                    row[tb_老年人中医药特征管理.阴虚质指导] = GetFlowLayoutResult(flow3);

                    row[tb_老年人中医药特征管理.痰湿质辨识] = is痰湿质;
                    row[tb_老年人中医药特征管理.痰湿质得分] = jf4;
                    row[tb_老年人中医药特征管理.痰湿质其他] = this.txt4.Text.Trim();
                    row[tb_老年人中医药特征管理.痰湿质指导] = GetFlowLayoutResult(flow4);

                    row[tb_老年人中医药特征管理.湿热质辨识] = is湿热质;
                    row[tb_老年人中医药特征管理.湿热质得分] = jf5;
                    row[tb_老年人中医药特征管理.湿热质其他] = this.txt5.Text.Trim();
                    row[tb_老年人中医药特征管理.湿热质指导] = GetFlowLayoutResult(flow5);

                    row[tb_老年人中医药特征管理.血瘀质辨识] = is血瘀质;
                    row[tb_老年人中医药特征管理.血瘀质得分] = jf6;
                    row[tb_老年人中医药特征管理.血瘀质其他] = this.txt6.Text.Trim();
                    row[tb_老年人中医药特征管理.血瘀质指导] = GetFlowLayoutResult(flow6);

                    row[tb_老年人中医药特征管理.气郁质辨识] = is气郁质;
                    row[tb_老年人中医药特征管理.气郁质得分] = jf7;
                    row[tb_老年人中医药特征管理.气郁质其他] = this.txt7.Text.Trim();
                    row[tb_老年人中医药特征管理.气郁质指导] = GetFlowLayoutResult(flow7);

                    row[tb_老年人中医药特征管理.特禀质辨识] = is特禀质;
                    row[tb_老年人中医药特征管理.特禀质得分] = jf8;
                    row[tb_老年人中医药特征管理.特禀质其他] = this.txt8.Text.Trim();
                    row[tb_老年人中医药特征管理.特禀质指导] = GetFlowLayoutResult(flow8);

                    row[tb_老年人中医药特征管理.平和质辨识] = is平和质;
                    row[tb_老年人中医药特征管理.平和质得分] = jf9;
                    row[tb_老年人中医药特征管理.平和质其他] = this.txt9.Text.Trim();
                    row[tb_老年人中医药特征管理.平和质指导] = GetFlowLayoutResult(flow9);

                    row[tb_老年人中医药特征管理.特征1] = t1;
                    row[tb_老年人中医药特征管理.特征2] = t2;
                    row[tb_老年人中医药特征管理.特征3] = t3;
                    row[tb_老年人中医药特征管理.特征4] = t4;
                    row[tb_老年人中医药特征管理.特征5] = t5;
                    row[tb_老年人中医药特征管理.特征6] = t6;
                    row[tb_老年人中医药特征管理.特征7] = t7;
                    row[tb_老年人中医药特征管理.特征8] = t8;
                    row[tb_老年人中医药特征管理.特征9] = t9;
                    row[tb_老年人中医药特征管理.特征10] = t10;
                    row[tb_老年人中医药特征管理.特征11] = t11;
                    row[tb_老年人中医药特征管理.特征12] = t12;
                    row[tb_老年人中医药特征管理.特征13] = t13;
                    row[tb_老年人中医药特征管理.特征14] = t14;
                    row[tb_老年人中医药特征管理.特征15] = t15;
                    row[tb_老年人中医药特征管理.特征16] = t16;
                    row[tb_老年人中医药特征管理.特征17] = t17;
                    row[tb_老年人中医药特征管理.特征18] = t18;
                    row[tb_老年人中医药特征管理.特征19] = t19;
                    row[tb_老年人中医药特征管理.特征20] = t20;
                    row[tb_老年人中医药特征管理.特征21] = t21;
                    row[tb_老年人中医药特征管理.特征22] = t22;
                    row[tb_老年人中医药特征管理.特征23] = t23;
                    row[tb_老年人中医药特征管理.特征24] = t24;
                    row[tb_老年人中医药特征管理.特征25] = t25;
                    row[tb_老年人中医药特征管理.特征26] = t26;
                    row[tb_老年人中医药特征管理.特征27] = t27;
                    row[tb_老年人中医药特征管理.特征28] = t28;
                    row[tb_老年人中医药特征管理.特征29] = t29;
                    row[tb_老年人中医药特征管理.特征30] = t30;
                    row[tb_老年人中医药特征管理.特征31] = t31;
                    row[tb_老年人中医药特征管理.特征32] = t32;
                    row[tb_老年人中医药特征管理.特征33] = t33;

                    row[tb_老年人中医药特征管理.创建机构] = Loginer.CurrentUser.所属机构;
                    row[tb_老年人中医药特征管理.创建人] = Loginer.CurrentUser.用户编码;
                    row[tb_老年人中医药特征管理.修改人] = Loginer.CurrentUser.用户编码;
                    row[tb_老年人中医药特征管理.所属机构] = Loginer.CurrentUser.所属机构;
                    row[tb_老年人中医药特征管理.创建时间] = _serverDateTime;
                    row[tb_老年人中医药特征管理.修改时间] = _serverDateTime;
                    row[tb_老年人中医药特征管理.发生时间] = this.dte填表日期.Text.Trim();
                    row[tb_老年人中医药特征管理.医生签名] = this.txt医生签名.Text.Trim();

                    #endregion

                    #region tb_健康体检

                    //if (_ds老年人中医药.Tables[tb_健康体检.__TableName].Rows.Count == 1)
                    //{
                    //    _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.平和质] = is平和质;
                    //    _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.气虚质] = is气虚质;
                    //    _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.阳虚质] = is阳虚质;
                    //    _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.阴虚质] = is阴虚质;
                    //    _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.痰湿质] = is痰湿质;
                    //    _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.湿热质] = is湿热质;
                    //    _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血瘀质] = is血瘀质;
                    //    _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.气郁质] = is气郁质;
                    //    _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.特禀质] = is特禀质;
                    //}
                    #endregion
                }
                else if (_UpdateType == UpdateType.Modify)
                {
                    #region tb_老年人中医药特征管理

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.气虚质辨识] = is气虚质;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.气虚质得分] = jf1;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.气虚质其他] = this.txt1.Text.Trim();
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.气虚质指导] = GetFlowLayoutResult(flow1);

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.阳虚质辨识] = is阳虚质;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.阳虚质得分] = jf2;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.阳虚质其他] = this.txt2.Text.Trim();
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.阳虚质指导] = GetFlowLayoutResult(flow2);

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.阴虚质辨识] = is阴虚质;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.阴虚质得分] = jf3;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.阴虚质其他] = this.txt3.Text.Trim();
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.阴虚质指导] = GetFlowLayoutResult(flow3);

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.痰湿质辨识] = is痰湿质;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.痰湿质得分] = jf4;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.痰湿质其他] = this.txt4.Text.Trim();
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.痰湿质指导] = GetFlowLayoutResult(flow4);

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.湿热质辨识] = is湿热质;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.湿热质得分] = jf5;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.湿热质其他] = this.txt5.Text.Trim();
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.湿热质指导] = GetFlowLayoutResult(flow5);

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.血瘀质辨识] = is血瘀质;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.血瘀质得分] = jf6;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.血瘀质其他] = this.txt6.Text.Trim();
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.血瘀质指导] = GetFlowLayoutResult(flow6);

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.气郁质辨识] = is气郁质;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.气郁质得分] = jf7;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.气郁质其他] = this.txt7.Text.Trim();
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.气郁质指导] = GetFlowLayoutResult(flow7);

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特禀质辨识] = is特禀质;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特禀质得分] = jf8;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特禀质其他] = this.txt8.Text.Trim();
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特禀质指导] = GetFlowLayoutResult(flow8);

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.平和质辨识] = is平和质;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.平和质得分] = jf9;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.平和质其他] = this.txt9.Text.Trim();
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.平和质指导] = GetFlowLayoutResult(flow9);

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征1] = t1;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征2] = t2;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征3] = t3;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征4] = t4;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征5] = t5;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征6] = t6;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征7] = t7;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征8] = t8;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征9] = t9;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征10] = t10;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征11] = t11;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征12] = t12;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征13] = t13;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征14] = t14;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征15] = t15;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征16] = t16;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征17] = t17;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征18] = t18;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征19] = t19;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征20] = t20;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征21] = t21;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征22] = t22;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征23] = t23;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征24] = t24;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征25] = t25;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征26] = t26;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征27] = t27;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征28] = t28;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征29] = t29;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征30] = t30;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征31] = t31;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征32] = t32;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.特征33] = t33;

                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.修改人] = Loginer.CurrentUser.用户编码;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.修改时间] = _serverDateTime;
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.发生时间] = this.dte填表日期.Text.Trim();
                    _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.医生签名] = this.txt医生签名.Text.Trim();

                    #endregion

                    #region tb_健康体检
                    if (_ds老年人中医药.Tables[tb_健康体检.__TableName].Rows.Count == 1)
                    {
                        _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.平和质] = is平和质;
                        _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.气虚质] = is气虚质;
                        _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.阳虚质] = is阳虚质;
                        _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.阴虚质] = is阴虚质;
                        _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.痰湿质] = is痰湿质;
                        _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.湿热质] = is湿热质;
                        _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血瘀质] = is血瘀质;
                        _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.气郁质] = is气郁质;
                        _ds老年人中医药.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.特禀质] = is特禀质;
                    }
                    #endregion
                }
                //TODO: 日志
                //_Bll.WriteLog();
                SaveResult result = _Bll.Save(_ds老年人中医药);
                if (result.Success)
                {
                    if (_UpdateType == UpdateType.Add)
                    {
                        string year = this.dte填表日期.Text.Substring(0, 4);
                        string sql = "  update tb_健康体检 set 平和质 = '" + is平和质 + "' ,气虚质 = '" + is气虚质 + "',阳虚质 = '" + is阳虚质 + "', 阴虚质 = '" + is阴虚质 + "',痰湿质 = '" + is痰湿质 + "', 湿热质 = '" + is湿热质 + "',血瘀质 = '" + is血瘀质 + "',气郁质 = '" + is气郁质 + "',特禀质 = '" + is特禀质 + "' where 体检日期 like '" + year + "%' and 个人档案编号 = '" + _docNo+"'";
                        _Bll.update体检信息(year, sql);
                    }
                    Msg.ShowInformation("保存成功！");
                    UC老年人中医药健康管理服务记录表列表 uc = new UC老年人中医药健康管理服务记录表列表(_frm);
                    ShowControl(uc);
                }
                else
                {
                    _frm._param = "";
                    UC老年人中医药健康管理服务记录表列表 uc = new UC老年人中医药健康管理服务记录表列表(_frm);
                    ShowControl(uc);
                }
            }
        }

        private bool fun必填项检查()
        {


            if (!(t1 != 0 && t2 != 0 && t3 != 0 && t4 != 0 && t5 != 0 && t6 != 0 && t7 != 0 && t8 != 0 && t9 != 0 && t10 != 0 && t11 != 0 && t12 != 0 && t13 != 0 && t14 != 0 && t15 != 0 && t16 != 0 && t17 != 0 && t18 != 0 && t19 != 0 && t20 != 0 && t21 != 0 && t22 != 0 && t23 != 0 && t24 != 0 && t25 != 0 && t26 != 0 && t27 != 0 && t28 != 0 && t29 != 0 && t30 != 0 && t31 != 0 && t32 != 0 && t33 != 0))
            {
                if (t1 == 0)
                {
                    this.labelControl11.BackColor = System.Drawing.Color.Red;
                    this.labelControl11.Focus();
                    Msg.Warning("请回答第   1   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl11.BackColor = System.Drawing.Color.Empty;

                if (t2 == 0)
                {
                    this.labelControl12.BackColor = System.Drawing.Color.Red;
                    this.labelControl12.Focus();
                    Msg.Warning("请回答第   2   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl12.BackColor = System.Drawing.Color.Empty;

                if (t3 == 0)
                {
                    this.labelControl13.BackColor = System.Drawing.Color.Red;
                    this.labelControl13.Focus();
                    Msg.Warning("请回答第   3   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl13.BackColor = System.Drawing.Color.Empty;

                if (t4 == 0)
                {
                    this.labelControl14.BackColor = System.Drawing.Color.Red;
                    this.labelControl14.Focus();
                    Msg.Warning("请回答第  4   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl14.BackColor = System.Drawing.Color.Empty;

                if (t5 == 0)
                {
                    this.labelControl15.BackColor = System.Drawing.Color.Red;
                    labelControl15.Focus();
                    Msg.Warning("请回答第   5   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl15.BackColor = System.Drawing.Color.Empty;

                if (t6 == 0)
                {
                    this.labelControl16.BackColor = System.Drawing.Color.Red;
                    this.labelControl16.Focus();
                    Msg.Warning("请回答第   6   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl16.BackColor = System.Drawing.Color.Empty;

                if (t7 == 0)
                {
                    this.labelControl17.BackColor = System.Drawing.Color.Red;
                    this.labelControl17.Focus();
                    Msg.Warning("请回答第   7   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl17.BackColor = System.Drawing.Color.Empty;

                if (t8 == 0)
                {
                    this.labelControl18.BackColor = System.Drawing.Color.Red;
                    this.labelControl18.Focus();
                    Msg.Warning("请回答第   8   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl18.BackColor = System.Drawing.Color.Empty;

                if (t9 == 0)
                {
                    this.labelControl19.BackColor = System.Drawing.Color.Red;
                    this.labelControl19.Focus();
                    Msg.Warning("请回答第   9   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl19.BackColor = System.Drawing.Color.Empty;

                if (t10 == 0)
                {
                    this.labelControl20.BackColor = System.Drawing.Color.Red;
                    this.labelControl20.Focus();
                    Msg.Warning("请回答第   10   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl20.BackColor = System.Drawing.Color.Empty;

                if (t11 == 0)
                {
                    this.labelControl21.BackColor = System.Drawing.Color.Red; this.labelControl21.Focus();

                    Msg.Warning("请回答第   11   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl21.BackColor = System.Drawing.Color.Empty;

                if (t12 == 0)
                {
                    this.labelControl22.BackColor = System.Drawing.Color.Red; this.labelControl22.Focus();

                    Msg.Warning("请回答第   12   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl22.BackColor = System.Drawing.Color.Empty;

                if (t13 == 0)
                {
                    this.labelControl23.BackColor = System.Drawing.Color.Red; this.labelControl23.Focus();

                    Msg.Warning("请回答第   13   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl23.BackColor = System.Drawing.Color.Empty;

                if (t14 == 0)
                {
                    this.labelControl24.BackColor = System.Drawing.Color.Red; this.labelControl24.Focus();

                    Msg.Warning("请回答第   14   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl24.BackColor = System.Drawing.Color.Empty;

                if (t15 == 0)
                {
                    this.labelControl25.BackColor = System.Drawing.Color.Red; this.labelControl25.Focus();

                    Msg.Warning("请回答第   15   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl25.BackColor = System.Drawing.Color.Empty;

                if (t16 == 0)
                {
                    this.labelControl26.BackColor = System.Drawing.Color.Red; this.labelControl26.Focus();

                    Msg.Warning("请回答第   16   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl26.BackColor = System.Drawing.Color.Empty;

                if (t17 == 0)
                {
                    this.labelControl27.BackColor = System.Drawing.Color.Red; this.labelControl27.Focus();

                    Msg.Warning("请回答第   17   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl27.BackColor = System.Drawing.Color.Empty;

                if (t18 == 0)
                {
                    this.labelControl28.BackColor = System.Drawing.Color.Red; this.labelControl28.Focus();

                    Msg.Warning("请回答第   18   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl28.BackColor = System.Drawing.Color.Empty;

                if (t19 == 0)
                {
                    this.labelControl29.BackColor = System.Drawing.Color.Red; this.labelControl29.Focus();

                    Msg.Warning("请回答第   19   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl29.BackColor = System.Drawing.Color.Empty;

                if (t20 == 0)
                {
                    this.labelControl30.BackColor = System.Drawing.Color.Red; this.labelControl30.Focus();

                    Msg.Warning("请回答第   20   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl30.BackColor = System.Drawing.Color.Empty;

                if (t21 == 0)
                {
                    this.labelControl31.BackColor = System.Drawing.Color.Red; this.labelControl31.Focus();

                    Msg.Warning("请回答第   21   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl31.BackColor = System.Drawing.Color.Empty;

                if (t22 == 0)
                {
                    this.labelControl32.BackColor = System.Drawing.Color.Red; this.labelControl32.Focus();

                    Msg.Warning("请回答第   22   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl32.BackColor = System.Drawing.Color.Empty;

                if (t23 == 0)
                {
                    this.labelControl33.BackColor = System.Drawing.Color.Red; this.labelControl33.Focus();

                    Msg.Warning("请回答第   23   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl33.BackColor = System.Drawing.Color.Empty;

                if (t24 == 0)
                {
                    this.labelControl34.BackColor = System.Drawing.Color.Red; this.labelControl34.Focus();

                    Msg.Warning("请回答第   24   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl34.BackColor = System.Drawing.Color.Empty;

                if (t25 == 0)
                {
                    this.labelControl35.BackColor = System.Drawing.Color.Red; this.labelControl35.Focus();

                    Msg.Warning("请回答第   25   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl35.BackColor = System.Drawing.Color.Empty;

                if (t26 == 0)
                {
                    this.labelControl36.BackColor = System.Drawing.Color.Red; this.labelControl36.Focus();

                    Msg.Warning("请回答第   26   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl36.BackColor = System.Drawing.Color.Empty;

                if (t27 == 0)
                {
                    this.labelControl37.BackColor = System.Drawing.Color.Red; this.labelControl37.Focus();

                    Msg.Warning("请回答第   27   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl37.BackColor = System.Drawing.Color.Empty;

                if (t28 == 0)
                {
                    this.labelControl38.BackColor = System.Drawing.Color.Red; this.labelControl38.Focus();

                    Msg.Warning("请回答第   28   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl38.BackColor = System.Drawing.Color.Empty;

                if (t29 == 0)
                {
                    this.labelControl39.BackColor = System.Drawing.Color.Red; this.labelControl39.Focus();

                    Msg.Warning("请回答第   29   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl39.BackColor = System.Drawing.Color.Empty;

                if (t30 == 0)
                {
                    this.labelControl40.BackColor = System.Drawing.Color.Red; this.labelControl40.Focus();

                    Msg.Warning("请回答第   30   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl40.BackColor = System.Drawing.Color.Empty;

                if (t31 == 0)
                {
                    this.labelControl41.BackColor = System.Drawing.Color.Red; this.labelControl41.Focus();

                    Msg.Warning("请回答第   31   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl41.BackColor = System.Drawing.Color.Empty;

                if (t32 == 0)
                {
                    this.labelControl42.BackColor = System.Drawing.Color.Red; this.labelControl42.Focus();
                    Msg.Warning("请回答第   32   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl42.BackColor = System.Drawing.Color.Empty;

                if (t33 == 0)
                {
                    this.labelControl43.BackColor = System.Drawing.Color.Red; this.labelControl43.Focus();
                    Msg.Warning("请回答第   33   题后再进行保存！");
                    return false;
                }
                else
                    this.labelControl43.BackColor = System.Drawing.Color.Empty;
            }

            if (string.IsNullOrEmpty(this.txt医生签名.Text.Trim()))
            {
                Msg.Warning("医生签名是必填项，请确认！");
                this.txt医生签名.Focus();
                return false;
            }
            if (Convert.ToDateTime(this.txt创建时间.Text) < this.dte填表日期.DateTime)
            {
                Msg.Warning("本次随访日期不能大于创建日期！");
                this.dte填表日期.Focus();
                return false;
            }
            return true;
        }

        private void UC老年人中医药健康管理服务记录表_Load(object sender, EventArgs e)
        {

            this.ucpinghdf.Txt1.Enabled = false;
            this.ucpinghdf.Txt1.BackColor = System.Drawing.Color.White;
            this.ucqiydf.Txt1.Enabled = false;
            this.ucqiydf.Txt1.BackColor = System.Drawing.Color.White;

            this.ucqxdf.Txt1.Enabled = false;
            this.ucqxdf.Txt1.BackColor = System.Drawing.Color.White;

            this.ucshirdf.Txt1.Enabled = false;
            this.ucshirdf.Txt1.BackColor = System.Drawing.Color.White;

            this.uctansdf.Txt1.Enabled = false;
            this.uctansdf.Txt1.BackColor = System.Drawing.Color.White;

            this.uctebdf.Txt1.Enabled = false;
            this.uctebdf.Txt1.BackColor = System.Drawing.Color.White;

            this.ucxueydf.Txt1.Enabled = false;
            this.ucxueydf.Txt1.BackColor = System.Drawing.Color.White;

            this.ucyangdf.Txt1.Enabled = false;
            this.ucyangdf.Txt1.BackColor = System.Drawing.Color.White;

            this.ucyindf.Txt1.Enabled = false;
            this.ucyindf.Txt1.BackColor = System.Drawing.Color.White;
        }

        private void setControlReadonly(bool p)
        {
            for (int i = 0; i < this.Controls.Count; i++)
            {
                if (Controls[i] is CheckEdit)
                {
                    CheckEdit chk = (CheckEdit)Controls[i];
                    chk.Properties.ReadOnly = p;
                }
            }
        }
    }
}
