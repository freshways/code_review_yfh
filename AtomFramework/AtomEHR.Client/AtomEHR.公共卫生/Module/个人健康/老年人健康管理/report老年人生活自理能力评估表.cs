﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using AtomEHR.Business;
using System.Data;
using AtomEHR.Models;


namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    public partial class report老年人生活自理能力评估表 : DevExpress.XtraReports.UI.XtraReport
    {
        string docNo;
        DataSet _ds老年人;
        bll老年人随访 _bll = new bll老年人随访();
        public report老年人生活自理能力评估表()
        {
            InitializeComponent();
       
        }
        public report老年人生活自理能力评估表(string _docNo,string _date)
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds老年人 = _bll.GetInfoByFangshi(_docNo, _date, true);
            DoBindingDataSource(_ds老年人);
        }

        private void DoBindingDataSource(DataSet _ds老年人)
        {
            DataTable dt老年人 = _ds老年人.Tables[Models.tb_老年人随访.__TableName];
            if (dt老年人 == null || dt老年人.Rows.Count == 0) return;

            DataTable dt健康档案 = _ds老年人.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.txt随访日期.Text = Convert.ToDateTime(dt老年人.Rows[0][tb_老年人随访.随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日           

            
            string 进餐评分 = dt老年人.Rows[0][tb_老年人随访.进餐评分].ToString();
            for(int i=1;i<dt老年人.Rows.Count;i++)
            {
                if (进餐评分 == "")
                {
                    进餐评分=dt老年人.Rows[i][tb_老年人随访.进餐评分].ToString();
                }
            }
            this.txt进餐.Text = 进餐评分;
            switch (进餐评分)
            {
                case "0":
                    this.txt进餐独立.Text = "0";
                    this.txt进餐轻度.Text = "0";
                    this.txt进餐中度.Text = "0";
                    this.txt进餐不能.Text = "0";                   
                    break;
                case "3":
                    this.txt进餐独立.Text = "0";
                    this.txt进餐轻度.Text = "0";
                    this.txt进餐中度.Text = "3";
                    this.txt进餐不能.Text = "0";
                    break;
                case "5":
                    this.txt进餐独立.Text = "0";
                    this.txt进餐轻度.Text = "0";
                    this.txt进餐中度.Text = "0";
                    this.txt进餐不能.Text = "5";
                    break;
                default:
                    break;

            }
            
            string 梳洗评分 = dt老年人.Rows[0][tb_老年人随访.梳洗评分].ToString();
            for(int i=1;i<dt老年人.Rows.Count;i++)
            {
                if (梳洗评分 == "")
                {
                    梳洗评分=dt老年人.Rows[i][tb_老年人随访.梳洗评分].ToString();
                }
            }
            this.txt梳洗.Text = 梳洗评分;
            switch (梳洗评分)
            {
                case "0":
                    this.txt梳洗独立.Text = "0";
                    this.txt梳洗轻度.Text = "0";
                    this.txt梳洗中度.Text = "0";
                    this.txt梳洗不能.Text = "0";
                    break;
                case "1":
                    this.txt梳洗独立.Text = "0";
                    this.txt梳洗轻度.Text = "1";
                    this.txt梳洗中度.Text = "0";
                    this.txt梳洗不能.Text = "0";
                    break;
                case "3":
                    this.txt梳洗独立.Text = "0";
                    this.txt梳洗轻度.Text = "0";
                    this.txt梳洗中度.Text = "3";
                    this.txt梳洗不能.Text = "0";
                    break;
                case "7":
                    this.txt梳洗独立.Text = "0";
                    this.txt梳洗轻度.Text = "0";
                    this.txt梳洗中度.Text = "0";
                    this.txt梳洗不能.Text = "7";
                    break;
                default:
                    break;

            }
            
            string 穿衣评分 = dt老年人.Rows[0][tb_老年人随访.穿衣评分].ToString();
            for(int i=1;i<dt老年人.Rows.Count;i++)
            {
                if (穿衣评分 == "")
                {
                    穿衣评分=dt老年人.Rows[i][tb_老年人随访.穿衣评分].ToString();
                }
            }
            this.txt穿衣.Text = 穿衣评分;
            switch (穿衣评分)
            {
                case "0":
                    this.txt穿衣独立.Text = "0";
                    this.txt穿衣轻度.Text = "0";
                    this.txt穿衣中度.Text = "0";
                    this.txt穿衣不能.Text = "0";
                    break;
                case "3":
                    this.txt穿衣独立.Text = "0";
                    this.txt穿衣轻度.Text = "0";
                    this.txt穿衣中度.Text = "3";
                    this.txt穿衣不能.Text = "0";
                    break;
                case "7":
                    this.txt穿衣独立.Text = "0";
                    this.txt穿衣轻度.Text = "0";
                    this.txt穿衣中度.Text = "0";
                    this.txt穿衣不能.Text = "7";
                    break;
                default:
                    break;

            }
            
            string 如厕评分 = dt老年人.Rows[0][tb_老年人随访.如厕评分].ToString();
            for(int i=1;i<dt老年人.Rows.Count;i++)
            {
                if (如厕评分 == "")
                {
                    如厕评分=dt老年人.Rows[i][tb_老年人随访.如厕评分].ToString();
                }
            }
            this.txt如厕.Text = 如厕评分;
            switch (如厕评分)
            {
                case "0":
                    this.txt如厕独立.Text = "0";
                    this.txt如厕轻度.Text = "0";
                    this.txt如厕中度.Text = "0";
                    this.txt如厕不能.Text = "0";
                    break;
                case "1":
                    this.txt如厕独立.Text = "0";
                    this.txt如厕轻度.Text = "1";
                    this.txt如厕中度.Text = "0";
                    this.txt如厕不能.Text = "0";
                    break;
                case "5":
                    this.txt如厕独立.Text = "0";
                    this.txt如厕轻度.Text = "0";
                    this.txt如厕中度.Text = "5";
                    this.txt如厕不能.Text = "0";
                    break;
                case "10":
                    this.txt如厕独立.Text = "0";
                    this.txt如厕轻度.Text = "0";
                    this.txt如厕中度.Text = "0";
                    this.txt如厕不能.Text = "10";
                    break;
                default:
                    break;

            }
            
            string 活动评分 = dt老年人.Rows[0][tb_老年人随访.活动评分].ToString();
            for (int i = 1; i < dt老年人.Rows.Count;i++ )
            {
                if (活动评分 == "")
                {
                    活动评分 = dt老年人.Rows[i][tb_老年人随访.活动评分].ToString();
                }
            }
            this.txt活动.Text = 活动评分;
            switch (活动评分)
            {
                case "0":
                    this.txt活动独立.Text = "0";
                    this.txt活动轻度.Text = "0";
                    this.txt活动中度.Text = "0";
                    this.txt活动不能.Text = "0";
                    break;
                case "1":
                    this.txt活动独立.Text = "0";
                    this.txt活动轻度.Text = "1";
                    this.txt活动中度.Text = "0";
                    this.txt活动不能.Text = "0";
                    break;
                case "5":
                    this.txt活动独立.Text = "0";
                    this.txt活动轻度.Text = "0";
                    this.txt活动中度.Text = "5";
                    this.txt活动不能.Text = "0";
                    break;
                case "10":
                    this.txt活动独立.Text = "0";
                    this.txt活动轻度.Text = "0";
                    this.txt活动中度.Text = "0";
                    this.txt活动不能.Text = "10";
                    break;
                default:
                    break;

            }
            this.txt总评分.Text = dt老年人.Rows[0][tb_老年人随访.总评分].ToString();
            for (int i = 1; i < dt老年人.Rows.Count; i++)
            {
                if (txt总评分.Text == "")
                {
                    this.txt总评分.Text = dt老年人.Rows[i][tb_老年人随访.总评分].ToString();
                }
            }
        }
    }
}
