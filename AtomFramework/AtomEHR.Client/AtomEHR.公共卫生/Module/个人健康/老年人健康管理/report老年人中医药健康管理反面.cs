﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    public partial class report老年人中医药健康管理反面: DevExpress.XtraReports.UI.XtraReport
    {
        #region Fields
        DataSet _ds老年人;
        string docNo;
        bll老年人中医药特征管理 _bll中医药 = new bll老年人中医药特征管理();
        #endregion
        public report老年人中医药健康管理反面()
        {
            InitializeComponent();
        }
        public report老年人中医药健康管理反面(string docNo,string _date)
        {
            InitializeComponent();
            this.docNo = docNo;
            _ds老年人 = _bll中医药.GetInfoByOldZYY(docNo, _date, true);
            DoBindingDataSource(_ds老年人);
        }

        private void DoBindingDataSource(DataSet _ds老年人)
        {
            DataTable dt老年人 = _ds老年人.Tables[Models.tb_老年人中医药特征管理.__TableName];
            if (dt老年人 == null || dt老年人.Rows.Count == 0) return;

            DataTable dt老年人基本信息 = _ds老年人.Tables[Models.tb_老年人基本信息.__TableName];
            if (dt老年人基本信息 == null || dt老年人基本信息.Rows.Count == 0) return;

            DataTable dt健康档案 = _ds老年人.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            //特征问答
            //获取单挑数据
            DataRow[] dr老年人中医药 = dt老年人.Select("个人档案编号='" + docNo + "'");
            if (dr老年人中医药.Length == 1)
            {
                for (int i = 19; i < 33; i++)
                {
                    //每次循环按顺序获取一个特征值
                    string str_特征 = dr老年人中医药[0]["特征" + (i + 1)].ToString();
                    string xrTable_特征 = "xrTable_特征_" + (i + 1) + "_" + str_特征;
                    XRLabel XRT_特征 = (XRLabel)FindControl(xrTable_特征, false);
                    XRT_特征.Text = str_特征;
                }
            }
            else
            {
                //XtraMessageBox.Show("数据发生重复性错误!");
                return;
            }
            //气虚质
            //气虚质得分
            txt气虚质得分.Text = dr老年人中医药[0]["气虚质得分"].ToString();
            //气虚质辨识
            switch (dr老年人中医药[0]["气虚质辨识"].ToString())
            {
                case "1":
                    xrLabel_气虚质辨识_是.Text = "✔";
                    break;
                case "2":
                    xrLabel_气虚质辨识_倾向是.Text = "✔";
                    break;
                default:
                    break;
            }
            //气虚质指导
            string str_气虚质指导 = dr老年人中医药[0]["气虚质指导"].ToString();
            if (!string.IsNullOrEmpty(str_气虚质指导))
            {
                string[] strs_气虚质指导 = str_气虚质指导.Split(',');
                for (int i = 0; i < strs_气虚质指导.Length; i++)
                {
                    string xrLabel_气虚质指导 = "xrLabel_气虚质指导" + strs_气虚质指导[i];
                    XRLabel XRL_气虚质指导 = (XRLabel)FindControl(xrLabel_气虚质指导, false);
                    XRL_气虚质指导.Text = "✔";
                }
            }
            //气虚质指导其他
            txt气虚质指导其他.Text = dr老年人中医药[0]["气虚质其他"].ToString();
            //阳虚质
            //阳虚质得分
            txt阳虚质得分.Text = dr老年人中医药[0]["阳虚质得分"].ToString();
            //阳虚质辨识
            switch (dr老年人中医药[0]["阳虚质辨识"].ToString())
            {
                case "1":
                    xrLabel_阳虚质辨识_是.Text = "✔";
                    break;
                case "2":
                    xrLabel_阳虚质辨识_倾向是.Text = "✔";
                    break;
                default:
                    break;
            }
            //阳虚质指导
            string str_阳虚质指导 = dr老年人中医药[0]["阳虚质指导"].ToString();
            if (!string.IsNullOrEmpty(str_阳虚质指导))
            {
                string[] strs_阳虚质指导 = str_阳虚质指导.Split(',');
                for (int i = 0; i < strs_阳虚质指导.Length; i++)
                {
                    string xrLabel_阳虚质指导 = "xrLabel_阳虚质指导" + strs_阳虚质指导[i];
                    XRLabel XRL_阳虚质指导 = (XRLabel)FindControl(xrLabel_阳虚质指导, false);
                    XRL_阳虚质指导.Text = "✔";
                }
            }
            //阳虚质指导其他
            txt阳虚质指导其他.Text = dr老年人中医药[0]["阳虚质其他"].ToString();
            //阴虚质
            //阴虚质得分
            txt阴虚质得分.Text = dr老年人中医药[0]["阴虚质得分"].ToString();
            //阴虚质辨识
            switch (dr老年人中医药[0]["阴虚质辨识"].ToString())
            {
                case "1":
                    xrLabel_阴虚质辨识_是.Text = "✔";
                    break;
                case "2":
                    xrLabel_阴虚质辨识_倾向是.Text = "✔";
                    break;
                default:
                    break;
            }
            //阴虚质指导
            string str_阴虚质指导 = dr老年人中医药[0]["阴虚质指导"].ToString();
            if (!string.IsNullOrEmpty(str_阴虚质指导))
            {
                string[] strs_阴虚质指导 = str_阴虚质指导.Split(',');
                for (int i = 0; i < strs_阴虚质指导.Length; i++)
                {
                    string xrLabel_阴虚质指导 = "xrLabel_阴虚质指导" + strs_阴虚质指导[i];
                    XRLabel XRL_阴虚质指导 = (XRLabel)FindControl(xrLabel_阴虚质指导, false);
                    XRL_阴虚质指导.Text = "✔";
                }
            }
            //阴虚质指导其他
            txt阴虚质指导其他.Text = dr老年人中医药[0]["阴虚质其他"].ToString();
            //痰湿质
            //痰湿质得分
            txt痰湿质得分.Text = dr老年人中医药[0]["痰湿质得分"].ToString();
            //痰湿质辨识
            switch (dr老年人中医药[0]["痰湿质辨识"].ToString())
            {
                case "1":
                    xrLabel_痰湿质辨识_是.Text = "✔";
                    break;
                case "2":
                    xrLabel_痰湿质辨识_倾向是.Text = "✔";
                    break;
                default:
                    break;
            }
            //痰湿质指导
            string str_痰湿质指导 = dr老年人中医药[0]["痰湿质指导"].ToString();
            if (!string.IsNullOrEmpty(str_痰湿质指导))
            {
                string[] strs_痰湿质指导 = str_痰湿质指导.Split(',');
                for (int i = 0; i < strs_痰湿质指导.Length; i++)
                {
                    string xrLabel_痰湿质指导 = "xrLabel_痰湿质指导" + strs_痰湿质指导[i];
                    XRLabel XRL_痰湿质指导 = (XRLabel)FindControl(xrLabel_痰湿质指导, false);
                    XRL_痰湿质指导.Text = "✔";
                }
            }
            //痰湿质指导其他
            txt痰湿质指导其他.Text = dr老年人中医药[0]["痰湿质其他"].ToString();
            //湿热质
            //湿热质得分
            txt湿热质得分.Text = dr老年人中医药[0]["湿热质得分"].ToString();
            //湿热质辨识
            switch (dr老年人中医药[0]["湿热质辨识"].ToString())
            {
                case "1":
                    xrLabel_湿热质辨识_是.Text = "✔";
                    break;
                case "2":
                    xrLabel_湿热质辨识_倾向是.Text = "✔";
                    break;
                default:
                    break;
            }
            //湿热质指导
            string str_湿热质指导 = dr老年人中医药[0]["湿热质指导"].ToString();
            if (!string.IsNullOrEmpty(str_湿热质指导))
            {
                string[] strs_湿热质指导 = str_湿热质指导.Split(',');
                for (int i = 0; i < strs_湿热质指导.Length; i++)
                {
                    string xrLabel_湿热质指导 = "xrLabel_湿热质指导" + strs_湿热质指导[i];
                    XRLabel XRL_湿热质指导 = (XRLabel)FindControl(xrLabel_湿热质指导, false);
                    XRL_湿热质指导.Text = "✔";
                }
            }
            //湿热质指导其他
            txt湿热质指导其他.Text = dr老年人中医药[0]["湿热质其他"].ToString();
            //血瘀质
            //血瘀质得分
            txt血瘀质得分.Text = dr老年人中医药[0]["血瘀质得分"].ToString();
            //血瘀质辨识
            switch (dr老年人中医药[0]["血瘀质辨识"].ToString())
            {
                case "1":
                    xrLabel_血瘀质辨识_是.Text = "✔";
                    break;
                case "2":
                    xrLabel_血瘀质辨识_倾向是.Text = "✔";
                    break;
                default:
                    break;
            }
            //血瘀质指导
            string str_血瘀质指导 = dr老年人中医药[0]["血瘀质指导"].ToString();
            if (!string.IsNullOrEmpty(str_血瘀质指导))
            {
                string[] strs_血瘀质指导 = str_血瘀质指导.Split(',');
                for (int i = 0; i < strs_血瘀质指导.Length; i++)
                {
                    string xrLabel_血瘀质指导 = "xrLabel_血瘀质指导" + strs_血瘀质指导[i];
                    XRLabel XRL_血瘀质指导 = (XRLabel)FindControl(xrLabel_血瘀质指导, false);
                    XRL_血瘀质指导.Text = "✔";
                }
            }
            //血瘀质指导其他
            txt血瘀质指导其他.Text = dr老年人中医药[0]["血瘀质其他"].ToString();
            //气郁质
            //气郁质得分
            txt气郁质得分.Text = dr老年人中医药[0]["气郁质得分"].ToString();
            //气郁质辨识
            switch (dr老年人中医药[0]["气郁质辨识"].ToString())
            {
                case "1":
                    xrLabel_气郁质辨识_是.Text = "✔";
                    break;
                case "2":
                    xrLabel_气郁质辨识_倾向是.Text = "✔";
                    break;
                default:
                    break;
            }
            //气郁质指导
            string str_气郁质指导 = dr老年人中医药[0]["气郁质指导"].ToString();
            if (!string.IsNullOrEmpty(str_气郁质指导))
            {
                string[] strs_气郁质指导 = str_气郁质指导.Split(',');
                for (int i = 0; i < strs_气郁质指导.Length; i++)
                {
                    string xrLabel_气郁质指导 = "xrLabel_气郁质指导" + strs_气郁质指导[i];
                    XRLabel XRL_气郁质指导 = (XRLabel)FindControl(xrLabel_气郁质指导, false);
                    XRL_气郁质指导.Text = "✔";
                }
            }
            //气郁质指导其他
            txt气郁质指导其他.Text = dr老年人中医药[0]["气郁质其他"].ToString();
            //特禀质
            //特禀质得分
            txt特禀质得分.Text = dr老年人中医药[0]["特禀质得分"].ToString();
            //特禀质辨识
            switch (dr老年人中医药[0]["特禀质辨识"].ToString())
            {
                case "1":
                    xrLabel_特禀质辨识_是.Text = "✔";
                    break;
                case "2":
                    xrLabel_特禀质辨识_倾向是.Text = "✔";
                    break;
                default:
                    break;
            }
            //特禀质指导
            string str_特禀质指导 = dr老年人中医药[0]["特禀质指导"].ToString();
            if (!string.IsNullOrEmpty(str_特禀质指导))
            {
                string[] strs_特禀质指导 = str_特禀质指导.Split(',');
                for (int i = 0; i < strs_特禀质指导.Length; i++)
                {
                    string xrLabel_特禀质指导 = "xrLabel_特禀质指导" + strs_特禀质指导[i];
                    XRLabel XRL_特禀质指导 = (XRLabel)FindControl(xrLabel_特禀质指导, false);
                    XRL_特禀质指导.Text = "✔";
                }
            }
            //特禀质指导其他
            txt特禀质指导其他.Text = dr老年人中医药[0]["特禀质其他"].ToString();
            //平和质
            //平和质得分
            txt平和质得分.Text = dr老年人中医药[0]["平和质得分"].ToString();
            //平和质辨识
            switch (dr老年人中医药[0]["平和质辨识"].ToString())
            {
                case "1":
                    xrLabel_平和质辨识_是.Text = "✔";
                    break;
                case "2":
                    xrLabel_平和质辨识_倾向是.Text = "✔";
                    break;
                default:
                    break;
            }
            //平和质指导
            string str_平和质指导 = dr老年人中医药[0]["平和质指导"].ToString();
            if (!string.IsNullOrEmpty(str_平和质指导))
            {
                string[] strs_平和质指导 = str_平和质指导.Split(',');
                for (int i = 0; i < strs_平和质指导.Length; i++)
                {
                    string xrLabel_平和质指导 = "xrLabel_平和质指导" + strs_平和质指导[i];
                    XRLabel XRL_平和质指导 = (XRLabel)FindControl(xrLabel_平和质指导, false);
                    XRL_平和质指导.Text = "✔";
                }
            }
            //平和质指导其他
            txt平和质指导其他.Text = dr老年人中医药[0]["平和质其他"].ToString();

            this.txt填表日期.Text = Convert.ToDateTime(dt老年人.Rows[0][tb_老年人中医药特征管理.发生时间]).ToLongDateString().ToString();//将时间格式转换为年月日
            this.txt医生签名.Text = dt老年人.Rows[0][tb_老年人中医药特征管理.医生签名].ToString();

        }
    }
}
