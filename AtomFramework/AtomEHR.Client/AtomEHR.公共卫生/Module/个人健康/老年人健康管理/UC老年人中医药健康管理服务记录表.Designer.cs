﻿namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    partial class UC老年人中医药健康管理服务记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC老年人中医药健康管理服务记录表));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControl44 = new DevExpress.XtraEditors.LabelControl();
            this.sp9 = new DevExpress.XtraEditors.LabelControl();
            this.sp8 = new DevExpress.XtraEditors.LabelControl();
            this.sp7 = new DevExpress.XtraEditors.LabelControl();
            this.sp6 = new DevExpress.XtraEditors.LabelControl();
            this.sp5 = new DevExpress.XtraEditors.LabelControl();
            this.sp4 = new DevExpress.XtraEditors.LabelControl();
            this.sp3 = new DevExpress.XtraEditors.LabelControl();
            this.sp2 = new DevExpress.XtraEditors.LabelControl();
            this.sp1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl43 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl42 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl41 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.flow2 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk21 = new DevExpress.XtraEditors.CheckEdit();
            this.chk22 = new DevExpress.XtraEditors.CheckEdit();
            this.chk23 = new DevExpress.XtraEditors.CheckEdit();
            this.chk24 = new DevExpress.XtraEditors.CheckEdit();
            this.chk25 = new DevExpress.XtraEditors.CheckEdit();
            this.chk26 = new DevExpress.XtraEditors.CheckEdit();
            this.txt2 = new DevExpress.XtraEditors.TextEdit();
            this.flow1 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk11 = new DevExpress.XtraEditors.CheckEdit();
            this.chk12 = new DevExpress.XtraEditors.CheckEdit();
            this.chk13 = new DevExpress.XtraEditors.CheckEdit();
            this.chk14 = new DevExpress.XtraEditors.CheckEdit();
            this.chk15 = new DevExpress.XtraEditors.CheckEdit();
            this.chk16 = new DevExpress.XtraEditors.CheckEdit();
            this.txt1 = new DevExpress.XtraEditors.TextEdit();
            this.ucpinghdf = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uctebdf = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flow9 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit214 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit215 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit216 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit217 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit218 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit219 = new DevExpress.XtraEditors.CheckEdit();
            this.txt9 = new DevExpress.XtraEditors.TextEdit();
            this.flow8 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit208 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit209 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit210 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit211 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit212 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit213 = new DevExpress.XtraEditors.CheckEdit();
            this.txt8 = new DevExpress.XtraEditors.TextEdit();
            this.flow7 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit202 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit203 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit204 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit205 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit206 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit207 = new DevExpress.XtraEditors.CheckEdit();
            this.txt7 = new DevExpress.XtraEditors.TextEdit();
            this.flow6 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit196 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit197 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit198 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit199 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit200 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit201 = new DevExpress.XtraEditors.CheckEdit();
            this.txt6 = new DevExpress.XtraEditors.TextEdit();
            this.flow5 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit190 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit191 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit192 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit193 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit194 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit195 = new DevExpress.XtraEditors.CheckEdit();
            this.txt5 = new DevExpress.XtraEditors.TextEdit();
            this.flow4 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit184 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit185 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit186 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit187 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit188 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit189 = new DevExpress.XtraEditors.CheckEdit();
            this.txt4 = new DevExpress.XtraEditors.TextEdit();
            this.flow3 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit178 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit179 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit180 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit181 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit182 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit183 = new DevExpress.XtraEditors.CheckEdit();
            this.txt3 = new DevExpress.XtraEditors.TextEdit();
            this.ucqiydf = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucxueydf = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucshirdf = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uctansdf = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucyindf = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucyangdf = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucqxdf = new AtomEHR.Library.UserControls.UCLblTxt();
            this.txt最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建人 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit165 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit164 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit163 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit162 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit161 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit160 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit159 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit158 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit157 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit156 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit155 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit154 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit153 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit152 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit151 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit150 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit149 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit148 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit147 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit146 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit145 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit144 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit143 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit142 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit141 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit140 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit139 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit138 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit137 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit136 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit135 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit134 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit133 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit132 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit131 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit130 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit129 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit128 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit127 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit126 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit125 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit124 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit123 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit122 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit121 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit120 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit119 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit118 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit117 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit116 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit115 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit114 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit113 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit112 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit111 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit110 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit109 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit108 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit107 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit106 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit105 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit104 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit103 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit102 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit101 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit100 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit99 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit98 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit97 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit96 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit95 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit94 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit93 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit92 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit91 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit90 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit89 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit88 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit87 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit86 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit85 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit84 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit83 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit82 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit81 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit80 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit79 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit78 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit77 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit76 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit75 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit74 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit73 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit72 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit71 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit70 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit69 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit68 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit67 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit66 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit65 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit64 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit63 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit62 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit61 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit60 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit59 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit58 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit57 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit56 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit55 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit54 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit53 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit52 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit51 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit50 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit49 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit48 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit47 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit46 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit45 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit44 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit43 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit42 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit41 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit40 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit39 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit38 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit37 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit36 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit35 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit34 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit33 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit32 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit31 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit30 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit29 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit28 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit26 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit27 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit25 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit24 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit23 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit22 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit20 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit19 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit18 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit17 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.chkA3 = new DevExpress.XtraEditors.CheckEdit();
            this.chkA2 = new DevExpress.XtraEditors.CheckEdit();
            this.chkA1 = new DevExpress.XtraEditors.CheckEdit();
            this.txt居住状态 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.dte填表日期 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem64 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem65 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem66 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem67 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem68 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem69 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem71 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem72 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem73 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem74 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem70 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem75 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem76 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem77 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem78 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem79 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem80 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem81 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem82 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem83 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem84 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem85 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem86 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem87 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem88 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem89 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem90 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem91 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem92 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem93 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem94 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem96 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem97 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem98 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem99 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem100 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem101 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem102 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem103 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem104 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem105 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem106 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem107 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem108 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem109 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem110 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem111 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem112 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem113 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem114 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem115 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem116 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem117 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem118 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem119 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem120 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem121 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem122 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem123 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem124 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem125 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem126 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem127 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem128 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem129 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem130 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem131 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem132 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem133 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem134 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem135 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem136 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem137 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem138 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem139 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem140 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem141 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem142 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem143 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem144 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem145 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem146 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem147 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem148 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem149 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem150 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem151 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem152 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem153 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem154 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem155 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem156 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem157 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem158 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem159 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem160 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem161 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem162 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem163 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem164 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem165 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem166 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem167 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem168 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem169 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem170 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem171 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem172 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem173 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem174 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem175 = new DevExpress.XtraLayout.LayoutControlItem();
            this.group体质 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem40 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem51 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl填表日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl医生签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem178 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl最近更新时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl当前所属机构 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl创建机构 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl创建人 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl最近修改人 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem176 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem177 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem179 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem181 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem182 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem183 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem184 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem185 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem186 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem187 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem188 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem189 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem190 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem191 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem192 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem193 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem194 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem195 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem196 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem197 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem198 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem199 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem200 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem201 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem202 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem203 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem204 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem239 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem240 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem241 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem242 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem243 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem244 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem245 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem246 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem247 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem248 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem205 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn健康指导 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem95 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem180 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.flow2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt2.Properties)).BeginInit();
            this.flow1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt1.Properties)).BeginInit();
            this.flow9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit214.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit215.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit216.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit217.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit218.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit219.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt9.Properties)).BeginInit();
            this.flow8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit208.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit209.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit210.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit211.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit212.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit213.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt8.Properties)).BeginInit();
            this.flow7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit202.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit203.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit204.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit205.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit206.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit207.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt7.Properties)).BeginInit();
            this.flow6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit196.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit197.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit198.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit199.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit200.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit201.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt6.Properties)).BeginInit();
            this.flow5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit190.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit191.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit192.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit193.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit194.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit195.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt5.Properties)).BeginInit();
            this.flow4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit184.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit185.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit186.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit187.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit188.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit189.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4.Properties)).BeginInit();
            this.flow3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit178.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit179.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit180.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit181.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit182.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit183.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit165.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit164.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit163.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit162.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit161.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit160.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit159.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit158.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit157.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit156.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit155.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit154.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit153.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit152.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit151.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit150.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit149.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit148.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit147.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit146.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit145.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit144.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit143.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit142.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit141.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit140.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit139.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit138.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit137.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit136.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit135.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit134.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit133.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit132.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit131.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit130.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit129.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit128.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit127.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit126.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit125.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit124.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit123.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit122.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit121.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit120.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit119.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit118.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit117.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit116.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit115.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit114.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit113.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit112.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit111.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit110.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit109.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit108.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit107.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit106.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit105.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit104.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit103.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit102.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit101.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit100.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit99.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit98.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit97.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit96.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit95.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit94.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit93.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit92.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit91.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit90.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit89.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit88.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit87.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit86.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit85.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit84.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit83.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit82.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit81.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit80.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit79.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit78.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit77.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit76.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit75.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit74.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit73.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit72.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit71.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit70.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit69.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit68.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit67.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit66.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit65.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit64.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit63.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit62.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit61.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit60.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit59.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit58.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit57.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit56.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit55.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit54.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit50.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit49.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkA3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkA2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkA1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte填表日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte填表日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem87)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem88)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem92)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem93)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem94)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem109)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem110)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem111)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem112)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem113)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem114)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem115)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem119)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem164)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem165)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem166)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem167)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem168)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem169)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem170)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem171)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem172)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem173)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem174)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem175)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group体质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl填表日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl医生签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem178)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl最近更新时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl当前所属机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl创建机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl创建人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl最近修改人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem176)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem177)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem179)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem181)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem182)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem183)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem184)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem185)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem186)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem187)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem188)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem189)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem190)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem191)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem192)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem193)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem194)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem195)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem196)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem197)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem198)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem199)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem200)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem201)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem202)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem203)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem204)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem239)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem240)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem241)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem242)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem243)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem244)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem245)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem246)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem247)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem248)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem205)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem95)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem180)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(4);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.labelControl44);
            this.layoutControl1.Controls.Add(this.sp9);
            this.layoutControl1.Controls.Add(this.sp8);
            this.layoutControl1.Controls.Add(this.sp7);
            this.layoutControl1.Controls.Add(this.sp6);
            this.layoutControl1.Controls.Add(this.sp5);
            this.layoutControl1.Controls.Add(this.sp4);
            this.layoutControl1.Controls.Add(this.sp3);
            this.layoutControl1.Controls.Add(this.sp2);
            this.layoutControl1.Controls.Add(this.sp1);
            this.layoutControl1.Controls.Add(this.labelControl43);
            this.layoutControl1.Controls.Add(this.labelControl42);
            this.layoutControl1.Controls.Add(this.labelControl41);
            this.layoutControl1.Controls.Add(this.labelControl40);
            this.layoutControl1.Controls.Add(this.labelControl39);
            this.layoutControl1.Controls.Add(this.labelControl38);
            this.layoutControl1.Controls.Add(this.labelControl37);
            this.layoutControl1.Controls.Add(this.labelControl36);
            this.layoutControl1.Controls.Add(this.labelControl35);
            this.layoutControl1.Controls.Add(this.labelControl34);
            this.layoutControl1.Controls.Add(this.labelControl33);
            this.layoutControl1.Controls.Add(this.labelControl32);
            this.layoutControl1.Controls.Add(this.labelControl31);
            this.layoutControl1.Controls.Add(this.labelControl30);
            this.layoutControl1.Controls.Add(this.labelControl29);
            this.layoutControl1.Controls.Add(this.labelControl28);
            this.layoutControl1.Controls.Add(this.labelControl27);
            this.layoutControl1.Controls.Add(this.labelControl26);
            this.layoutControl1.Controls.Add(this.labelControl25);
            this.layoutControl1.Controls.Add(this.labelControl24);
            this.layoutControl1.Controls.Add(this.labelControl23);
            this.layoutControl1.Controls.Add(this.labelControl22);
            this.layoutControl1.Controls.Add(this.labelControl21);
            this.layoutControl1.Controls.Add(this.labelControl20);
            this.layoutControl1.Controls.Add(this.labelControl19);
            this.layoutControl1.Controls.Add(this.labelControl18);
            this.layoutControl1.Controls.Add(this.labelControl17);
            this.layoutControl1.Controls.Add(this.labelControl16);
            this.layoutControl1.Controls.Add(this.labelControl15);
            this.layoutControl1.Controls.Add(this.labelControl14);
            this.layoutControl1.Controls.Add(this.labelControl13);
            this.layoutControl1.Controls.Add(this.labelControl12);
            this.layoutControl1.Controls.Add(this.labelControl11);
            this.layoutControl1.Controls.Add(this.labelControl10);
            this.layoutControl1.Controls.Add(this.labelControl9);
            this.layoutControl1.Controls.Add(this.labelControl8);
            this.layoutControl1.Controls.Add(this.labelControl7);
            this.layoutControl1.Controls.Add(this.labelControl6);
            this.layoutControl1.Controls.Add(this.labelControl5);
            this.layoutControl1.Controls.Add(this.labelControl4);
            this.layoutControl1.Controls.Add(this.labelControl3);
            this.layoutControl1.Controls.Add(this.labelControl2);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.flow2);
            this.layoutControl1.Controls.Add(this.flow1);
            this.layoutControl1.Controls.Add(this.ucpinghdf);
            this.layoutControl1.Controls.Add(this.uctebdf);
            this.layoutControl1.Controls.Add(this.flow9);
            this.layoutControl1.Controls.Add(this.flow8);
            this.layoutControl1.Controls.Add(this.flow7);
            this.layoutControl1.Controls.Add(this.flow6);
            this.layoutControl1.Controls.Add(this.flow5);
            this.layoutControl1.Controls.Add(this.flow4);
            this.layoutControl1.Controls.Add(this.flow3);
            this.layoutControl1.Controls.Add(this.ucqiydf);
            this.layoutControl1.Controls.Add(this.ucxueydf);
            this.layoutControl1.Controls.Add(this.ucshirdf);
            this.layoutControl1.Controls.Add(this.uctansdf);
            this.layoutControl1.Controls.Add(this.ucyindf);
            this.layoutControl1.Controls.Add(this.ucyangdf);
            this.layoutControl1.Controls.Add(this.ucqxdf);
            this.layoutControl1.Controls.Add(this.txt最近修改人);
            this.layoutControl1.Controls.Add(this.txt创建人);
            this.layoutControl1.Controls.Add(this.txt创建机构);
            this.layoutControl1.Controls.Add(this.txt当前所属机构);
            this.layoutControl1.Controls.Add(this.txt最近更新时间);
            this.layoutControl1.Controls.Add(this.txt创建时间);
            this.layoutControl1.Controls.Add(this.txt医生签名);
            this.layoutControl1.Controls.Add(this.checkEdit165);
            this.layoutControl1.Controls.Add(this.checkEdit164);
            this.layoutControl1.Controls.Add(this.checkEdit163);
            this.layoutControl1.Controls.Add(this.checkEdit162);
            this.layoutControl1.Controls.Add(this.checkEdit161);
            this.layoutControl1.Controls.Add(this.checkEdit160);
            this.layoutControl1.Controls.Add(this.checkEdit159);
            this.layoutControl1.Controls.Add(this.checkEdit158);
            this.layoutControl1.Controls.Add(this.checkEdit157);
            this.layoutControl1.Controls.Add(this.checkEdit156);
            this.layoutControl1.Controls.Add(this.checkEdit155);
            this.layoutControl1.Controls.Add(this.checkEdit154);
            this.layoutControl1.Controls.Add(this.checkEdit153);
            this.layoutControl1.Controls.Add(this.checkEdit152);
            this.layoutControl1.Controls.Add(this.checkEdit151);
            this.layoutControl1.Controls.Add(this.checkEdit150);
            this.layoutControl1.Controls.Add(this.checkEdit149);
            this.layoutControl1.Controls.Add(this.checkEdit148);
            this.layoutControl1.Controls.Add(this.checkEdit147);
            this.layoutControl1.Controls.Add(this.checkEdit146);
            this.layoutControl1.Controls.Add(this.checkEdit145);
            this.layoutControl1.Controls.Add(this.checkEdit144);
            this.layoutControl1.Controls.Add(this.checkEdit143);
            this.layoutControl1.Controls.Add(this.checkEdit142);
            this.layoutControl1.Controls.Add(this.checkEdit141);
            this.layoutControl1.Controls.Add(this.checkEdit140);
            this.layoutControl1.Controls.Add(this.checkEdit139);
            this.layoutControl1.Controls.Add(this.checkEdit138);
            this.layoutControl1.Controls.Add(this.checkEdit137);
            this.layoutControl1.Controls.Add(this.checkEdit136);
            this.layoutControl1.Controls.Add(this.checkEdit135);
            this.layoutControl1.Controls.Add(this.checkEdit134);
            this.layoutControl1.Controls.Add(this.checkEdit133);
            this.layoutControl1.Controls.Add(this.checkEdit132);
            this.layoutControl1.Controls.Add(this.checkEdit131);
            this.layoutControl1.Controls.Add(this.checkEdit130);
            this.layoutControl1.Controls.Add(this.checkEdit129);
            this.layoutControl1.Controls.Add(this.checkEdit128);
            this.layoutControl1.Controls.Add(this.checkEdit127);
            this.layoutControl1.Controls.Add(this.checkEdit126);
            this.layoutControl1.Controls.Add(this.checkEdit125);
            this.layoutControl1.Controls.Add(this.checkEdit124);
            this.layoutControl1.Controls.Add(this.checkEdit123);
            this.layoutControl1.Controls.Add(this.checkEdit122);
            this.layoutControl1.Controls.Add(this.checkEdit121);
            this.layoutControl1.Controls.Add(this.checkEdit120);
            this.layoutControl1.Controls.Add(this.checkEdit119);
            this.layoutControl1.Controls.Add(this.checkEdit118);
            this.layoutControl1.Controls.Add(this.checkEdit117);
            this.layoutControl1.Controls.Add(this.checkEdit116);
            this.layoutControl1.Controls.Add(this.checkEdit115);
            this.layoutControl1.Controls.Add(this.checkEdit114);
            this.layoutControl1.Controls.Add(this.checkEdit113);
            this.layoutControl1.Controls.Add(this.checkEdit112);
            this.layoutControl1.Controls.Add(this.checkEdit111);
            this.layoutControl1.Controls.Add(this.checkEdit110);
            this.layoutControl1.Controls.Add(this.checkEdit109);
            this.layoutControl1.Controls.Add(this.checkEdit108);
            this.layoutControl1.Controls.Add(this.checkEdit107);
            this.layoutControl1.Controls.Add(this.checkEdit106);
            this.layoutControl1.Controls.Add(this.checkEdit105);
            this.layoutControl1.Controls.Add(this.checkEdit104);
            this.layoutControl1.Controls.Add(this.checkEdit103);
            this.layoutControl1.Controls.Add(this.checkEdit102);
            this.layoutControl1.Controls.Add(this.checkEdit101);
            this.layoutControl1.Controls.Add(this.checkEdit100);
            this.layoutControl1.Controls.Add(this.checkEdit99);
            this.layoutControl1.Controls.Add(this.checkEdit98);
            this.layoutControl1.Controls.Add(this.checkEdit97);
            this.layoutControl1.Controls.Add(this.checkEdit96);
            this.layoutControl1.Controls.Add(this.checkEdit95);
            this.layoutControl1.Controls.Add(this.checkEdit94);
            this.layoutControl1.Controls.Add(this.checkEdit93);
            this.layoutControl1.Controls.Add(this.checkEdit92);
            this.layoutControl1.Controls.Add(this.checkEdit91);
            this.layoutControl1.Controls.Add(this.checkEdit90);
            this.layoutControl1.Controls.Add(this.checkEdit89);
            this.layoutControl1.Controls.Add(this.checkEdit88);
            this.layoutControl1.Controls.Add(this.checkEdit87);
            this.layoutControl1.Controls.Add(this.checkEdit86);
            this.layoutControl1.Controls.Add(this.checkEdit85);
            this.layoutControl1.Controls.Add(this.checkEdit84);
            this.layoutControl1.Controls.Add(this.checkEdit83);
            this.layoutControl1.Controls.Add(this.checkEdit82);
            this.layoutControl1.Controls.Add(this.checkEdit81);
            this.layoutControl1.Controls.Add(this.checkEdit80);
            this.layoutControl1.Controls.Add(this.checkEdit79);
            this.layoutControl1.Controls.Add(this.checkEdit78);
            this.layoutControl1.Controls.Add(this.checkEdit77);
            this.layoutControl1.Controls.Add(this.checkEdit76);
            this.layoutControl1.Controls.Add(this.checkEdit75);
            this.layoutControl1.Controls.Add(this.checkEdit74);
            this.layoutControl1.Controls.Add(this.checkEdit73);
            this.layoutControl1.Controls.Add(this.checkEdit72);
            this.layoutControl1.Controls.Add(this.checkEdit71);
            this.layoutControl1.Controls.Add(this.checkEdit70);
            this.layoutControl1.Controls.Add(this.checkEdit69);
            this.layoutControl1.Controls.Add(this.checkEdit68);
            this.layoutControl1.Controls.Add(this.checkEdit67);
            this.layoutControl1.Controls.Add(this.checkEdit66);
            this.layoutControl1.Controls.Add(this.checkEdit65);
            this.layoutControl1.Controls.Add(this.checkEdit64);
            this.layoutControl1.Controls.Add(this.checkEdit63);
            this.layoutControl1.Controls.Add(this.checkEdit62);
            this.layoutControl1.Controls.Add(this.checkEdit61);
            this.layoutControl1.Controls.Add(this.checkEdit60);
            this.layoutControl1.Controls.Add(this.checkEdit59);
            this.layoutControl1.Controls.Add(this.checkEdit58);
            this.layoutControl1.Controls.Add(this.checkEdit57);
            this.layoutControl1.Controls.Add(this.checkEdit56);
            this.layoutControl1.Controls.Add(this.checkEdit55);
            this.layoutControl1.Controls.Add(this.checkEdit54);
            this.layoutControl1.Controls.Add(this.checkEdit53);
            this.layoutControl1.Controls.Add(this.checkEdit52);
            this.layoutControl1.Controls.Add(this.checkEdit51);
            this.layoutControl1.Controls.Add(this.checkEdit50);
            this.layoutControl1.Controls.Add(this.checkEdit49);
            this.layoutControl1.Controls.Add(this.checkEdit48);
            this.layoutControl1.Controls.Add(this.checkEdit47);
            this.layoutControl1.Controls.Add(this.checkEdit46);
            this.layoutControl1.Controls.Add(this.checkEdit45);
            this.layoutControl1.Controls.Add(this.checkEdit44);
            this.layoutControl1.Controls.Add(this.checkEdit43);
            this.layoutControl1.Controls.Add(this.checkEdit42);
            this.layoutControl1.Controls.Add(this.checkEdit41);
            this.layoutControl1.Controls.Add(this.checkEdit40);
            this.layoutControl1.Controls.Add(this.checkEdit39);
            this.layoutControl1.Controls.Add(this.checkEdit38);
            this.layoutControl1.Controls.Add(this.checkEdit37);
            this.layoutControl1.Controls.Add(this.checkEdit36);
            this.layoutControl1.Controls.Add(this.checkEdit35);
            this.layoutControl1.Controls.Add(this.checkEdit34);
            this.layoutControl1.Controls.Add(this.checkEdit33);
            this.layoutControl1.Controls.Add(this.checkEdit32);
            this.layoutControl1.Controls.Add(this.checkEdit31);
            this.layoutControl1.Controls.Add(this.checkEdit30);
            this.layoutControl1.Controls.Add(this.checkEdit29);
            this.layoutControl1.Controls.Add(this.checkEdit28);
            this.layoutControl1.Controls.Add(this.checkEdit26);
            this.layoutControl1.Controls.Add(this.checkEdit27);
            this.layoutControl1.Controls.Add(this.checkEdit25);
            this.layoutControl1.Controls.Add(this.checkEdit24);
            this.layoutControl1.Controls.Add(this.checkEdit23);
            this.layoutControl1.Controls.Add(this.checkEdit22);
            this.layoutControl1.Controls.Add(this.checkEdit21);
            this.layoutControl1.Controls.Add(this.checkEdit20);
            this.layoutControl1.Controls.Add(this.checkEdit19);
            this.layoutControl1.Controls.Add(this.checkEdit18);
            this.layoutControl1.Controls.Add(this.checkEdit17);
            this.layoutControl1.Controls.Add(this.checkEdit16);
            this.layoutControl1.Controls.Add(this.checkEdit15);
            this.layoutControl1.Controls.Add(this.checkEdit14);
            this.layoutControl1.Controls.Add(this.checkEdit13);
            this.layoutControl1.Controls.Add(this.checkEdit12);
            this.layoutControl1.Controls.Add(this.checkEdit11);
            this.layoutControl1.Controls.Add(this.checkEdit10);
            this.layoutControl1.Controls.Add(this.checkEdit9);
            this.layoutControl1.Controls.Add(this.checkEdit8);
            this.layoutControl1.Controls.Add(this.checkEdit7);
            this.layoutControl1.Controls.Add(this.checkEdit6);
            this.layoutControl1.Controls.Add(this.checkEdit5);
            this.layoutControl1.Controls.Add(this.checkEdit4);
            this.layoutControl1.Controls.Add(this.chkA3);
            this.layoutControl1.Controls.Add(this.chkA2);
            this.layoutControl1.Controls.Add(this.chkA1);
            this.layoutControl1.Controls.Add(this.txt居住状态);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt个人档案号);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.dte填表日期);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(798, 466);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // labelControl44
            // 
            this.labelControl44.Location = new System.Drawing.Point(3, 1297);
            this.labelControl44.Name = "labelControl44";
            this.labelControl44.Size = new System.Drawing.Size(51, 36);
            this.labelControl44.StyleController = this.layoutControl1;
            this.labelControl44.TabIndex = 252;
            this.labelControl44.Text = "体质辨识";
            // 
            // sp9
            // 
            this.sp9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sp9.Location = new System.Drawing.Point(698, 1273);
            this.sp9.Name = "sp9";
            this.sp9.Size = new System.Drawing.Size(76, 20);
            this.sp9.StyleController = this.layoutControl1;
            this.sp9.TabIndex = 251;
            this.sp9.Text = "平和质";
            // 
            // sp8
            // 
            this.sp8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sp8.Location = new System.Drawing.Point(618, 1273);
            this.sp8.Name = "sp8";
            this.sp8.Size = new System.Drawing.Size(76, 20);
            this.sp8.StyleController = this.layoutControl1;
            this.sp8.TabIndex = 250;
            this.sp8.Text = "特禀质";
            // 
            // sp7
            // 
            this.sp7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sp7.Location = new System.Drawing.Point(538, 1273);
            this.sp7.Name = "sp7";
            this.sp7.Size = new System.Drawing.Size(76, 20);
            this.sp7.StyleController = this.layoutControl1;
            this.sp7.TabIndex = 249;
            this.sp7.Text = "气郁质";
            // 
            // sp6
            // 
            this.sp6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sp6.Location = new System.Drawing.Point(458, 1273);
            this.sp6.Name = "sp6";
            this.sp6.Size = new System.Drawing.Size(76, 20);
            this.sp6.StyleController = this.layoutControl1;
            this.sp6.TabIndex = 248;
            this.sp6.Text = "血瘀质";
            // 
            // sp5
            // 
            this.sp5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sp5.Location = new System.Drawing.Point(378, 1273);
            this.sp5.Name = "sp5";
            this.sp5.Size = new System.Drawing.Size(76, 20);
            this.sp5.StyleController = this.layoutControl1;
            this.sp5.TabIndex = 247;
            this.sp5.Text = "湿热质";
            // 
            // sp4
            // 
            this.sp4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sp4.Location = new System.Drawing.Point(298, 1273);
            this.sp4.Name = "sp4";
            this.sp4.Size = new System.Drawing.Size(76, 20);
            this.sp4.StyleController = this.layoutControl1;
            this.sp4.TabIndex = 246;
            this.sp4.Text = "痰湿质";
            // 
            // sp3
            // 
            this.sp3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sp3.Location = new System.Drawing.Point(218, 1273);
            this.sp3.Name = "sp3";
            this.sp3.Size = new System.Drawing.Size(76, 20);
            this.sp3.StyleController = this.layoutControl1;
            this.sp3.TabIndex = 245;
            this.sp3.Text = "阴虚质";
            // 
            // sp2
            // 
            this.sp2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sp2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.sp2.Location = new System.Drawing.Point(138, 1273);
            this.sp2.Name = "sp2";
            this.sp2.Size = new System.Drawing.Size(76, 20);
            this.sp2.StyleController = this.layoutControl1;
            this.sp2.TabIndex = 244;
            this.sp2.Text = "阳虚质";
            // 
            // sp1
            // 
            this.sp1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.sp1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.sp1.Location = new System.Drawing.Point(58, 1273);
            this.sp1.Name = "sp1";
            this.sp1.Size = new System.Drawing.Size(76, 20);
            this.sp1.StyleController = this.layoutControl1;
            this.sp1.TabIndex = 243;
            this.sp1.Text = "气虚质";
            // 
            // labelControl43
            // 
            this.labelControl43.Location = new System.Drawing.Point(3, 1245);
            this.labelControl43.Name = "labelControl43";
            this.labelControl43.Size = new System.Drawing.Size(386, 24);
            this.labelControl43.StyleController = this.layoutControl1;
            this.labelControl43.TabIndex = 242;
            this.labelControl43.Text = "(33)您舌下静脉瘀紫或增粗吗？（可由调查员辅助观察后填写）";
            // 
            // labelControl42
            // 
            this.labelControl42.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl42.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl42.Location = new System.Drawing.Point(3, 1205);
            this.labelControl42.Name = "labelControl42";
            this.labelControl42.Size = new System.Drawing.Size(386, 36);
            this.labelControl42.StyleController = this.layoutControl1;
            this.labelControl42.TabIndex = 241;
            this.labelControl42.Text = "(32)您舌苔厚腻或有舌苔厚厚的感觉吗?（如果自我感觉不清楚可由调查员观察后填写）";
            // 
            // labelControl41
            // 
            this.labelControl41.Location = new System.Drawing.Point(3, 1177);
            this.labelControl41.Name = "labelControl41";
            this.labelControl41.Size = new System.Drawing.Size(386, 24);
            this.labelControl41.StyleController = this.layoutControl1;
            this.labelControl41.TabIndex = 240;
            this.labelControl41.Text = "(31)您容易大便干燥吗?";
            // 
            // labelControl40
            // 
            this.labelControl40.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl40.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl40.Location = new System.Drawing.Point(3, 1137);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(386, 36);
            this.labelControl40.StyleController = this.layoutControl1;
            this.labelControl40.TabIndex = 239;
            this.labelControl40.Text = "(30)您有大便黏滞不爽、解不尽的感觉吗?(大便容易粘在马桶或便坑壁上)";
            // 
            // labelControl39
            // 
            this.labelControl39.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl39.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl39.Location = new System.Drawing.Point(3, 1097);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(386, 36);
            this.labelControl39.StyleController = this.layoutControl1;
            this.labelControl39.TabIndex = 238;
            this.labelControl39.Text = "(29)您吃(喝)凉的东西会感到不舒服或者怕吃(喝)凉的东西吗？（指不喜欢吃凉的食物，或吃了凉的食物后会不舒服）";
            // 
            // labelControl38
            // 
            this.labelControl38.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl38.Location = new System.Drawing.Point(3, 1019);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(386, 74);
            this.labelControl38.StyleController = this.layoutControl1;
            this.labelControl38.TabIndex = 237;
            this.labelControl38.Text = "(28)您腹部肥大吗?（指腹部脂肪肥厚）";
            // 
            // labelControl37
            // 
            this.labelControl37.Location = new System.Drawing.Point(3, 991);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(386, 24);
            this.labelControl37.StyleController = this.layoutControl1;
            this.labelControl37.TabIndex = 236;
            this.labelControl37.Text = "(27)您感到口苦或嘴里有异味吗?（指口苦或口臭）";
            // 
            // labelControl36
            // 
            this.labelControl36.Location = new System.Drawing.Point(3, 963);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(386, 24);
            this.labelControl36.StyleController = this.layoutControl1;
            this.labelControl36.TabIndex = 235;
            this.labelControl36.Text = "(26)您感到口干咽燥、总想喝水吗？";
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(3, 935);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(386, 24);
            this.labelControl35.StyleController = this.layoutControl1;
            this.labelControl35.TabIndex = 234;
            this.labelControl35.Text = "(25)您有皮肤湿疹、疮疖吗？";
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(3, 879);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(386, 24);
            this.labelControl34.StyleController = this.layoutControl1;
            this.labelControl34.TabIndex = 232;
            this.labelControl34.Text = "(23)您面部或鼻部有油腻感或者油亮发光吗?（指脸上或鼻子）";
            // 
            // labelControl33
            // 
            this.labelControl33.Location = new System.Drawing.Point(3, 907);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(386, 24);
            this.labelControl33.StyleController = this.layoutControl1;
            this.labelControl33.TabIndex = 233;
            this.labelControl33.Text = "(24)您面色或目眶晦黯，或出现褐色斑块/斑点吗?";
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(3, 851);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(386, 24);
            this.labelControl32.StyleController = this.layoutControl1;
            this.labelControl32.TabIndex = 231;
            this.labelControl32.Text = "(22)您有肢体麻木或固定部位疼痛的感觉吗？";
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(3, 823);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(386, 24);
            this.labelControl31.StyleController = this.layoutControl1;
            this.labelControl31.TabIndex = 230;
            this.labelControl31.Text = "(21)您皮肤或口唇干吗?";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl30.Location = new System.Drawing.Point(3, 783);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(386, 36);
            this.labelControl30.StyleController = this.layoutControl1;
            this.labelControl30.TabIndex = 229;
            this.labelControl30.Text = "(20)您的皮肤一抓就红，并出现抓痕吗?（指被指甲或钝物划过后皮肤的反应）";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl29.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl29.Location = new System.Drawing.Point(3, 743);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(386, 36);
            this.labelControl29.StyleController = this.layoutControl1;
            this.labelControl29.TabIndex = 228;
            this.labelControl29.Text = "(19)您的皮肤在不知不觉中会出现青紫瘀斑、皮下出血吗?（指皮肤在没有外伤的情况下出现青一块紫一块的情况）";
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(3, 715);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(386, 24);
            this.labelControl28.StyleController = this.layoutControl1;
            this.labelControl28.TabIndex = 227;
            this.labelControl28.Text = "(18)您的皮肤容易起荨麻疹吗? (包括风团、风疹块、风疙瘩)";
            // 
            // labelControl27
            // 
            this.labelControl27.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl27.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl27.Location = new System.Drawing.Point(3, 675);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(386, 36);
            this.labelControl27.StyleController = this.layoutControl1;
            this.labelControl27.TabIndex = 226;
            this.labelControl27.Text = "(17)您容易过敏(对药物、食物、气味、花粉或在季节交替、气候变化时)吗?";
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(3, 647);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(386, 24);
            this.labelControl26.StyleController = this.layoutControl1;
            this.labelControl26.TabIndex = 225;
            this.labelControl26.Text = "(16)您有口粘口腻，或睡眠打鼾吗？";
            // 
            // labelControl25
            // 
            this.labelControl25.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl25.Location = new System.Drawing.Point(3, 619);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(386, 24);
            this.labelControl25.StyleController = this.layoutControl1;
            this.labelControl25.TabIndex = 224;
            this.labelControl25.Text = "(15)您没有感冒时也会鼻塞、流鼻涕吗?";
            // 
            // labelControl24
            // 
            this.labelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl24.Location = new System.Drawing.Point(3, 569);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(386, 46);
            this.labelControl24.StyleController = this.layoutControl1;
            this.labelControl24.TabIndex = 223;
            this.labelControl24.Text = "(14)您容易患感冒吗?（指每年感冒的次数）";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl23.Location = new System.Drawing.Point(3, 529);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(386, 36);
            this.labelControl23.StyleController = this.layoutControl1;
            this.labelControl23.TabIndex = 222;
            this.labelControl23.Text = "(13)您比一般人耐受不了寒冷吗？（指比别人容易害怕冬天或是夏天的冷空调、电扇等）";
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.Location = new System.Drawing.Point(3, 489);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(386, 36);
            this.labelControl22.StyleController = this.layoutControl1;
            this.labelControl22.TabIndex = 221;
            this.labelControl22.Text = "(12)您胃脘部、背部或腰膝部怕冷吗？（指上腹部、背部、腰部或膝关节等，有一处或多处怕冷）";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(3, 461);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(386, 24);
            this.labelControl21.StyleController = this.layoutControl1;
            this.labelControl21.TabIndex = 220;
            this.labelControl21.Text = "(11)您手脚发凉吗?（不包含因周围温度低或穿的少导致的手脚发冷）";
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(3, 433);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(386, 24);
            this.labelControl20.StyleController = this.layoutControl1;
            this.labelControl20.TabIndex = 219;
            this.labelControl20.Text = "(10)您眼睛干涩吗?";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(3, 383);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(386, 46);
            this.labelControl19.StyleController = this.layoutControl1;
            this.labelControl19.TabIndex = 218;
            this.labelControl19.Text = "(9)您感到身体超重不轻松吗?(感觉身体沉重)\r\n[BMI指数=体重（kg）/身高2（m）]";
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(3, 355);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(386, 24);
            this.labelControl18.StyleController = this.layoutControl1;
            this.labelControl18.TabIndex = 217;
            this.labelControl18.Text = "(8)您容易感到害怕或受到惊吓吗?";
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(3, 327);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(386, 24);
            this.labelControl17.StyleController = this.layoutControl1;
            this.labelControl17.TabIndex = 216;
            this.labelControl17.Text = "(7)您因为生活状态改变而感到孤独、失落吗？";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(3, 299);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(386, 24);
            this.labelControl16.StyleController = this.layoutControl1;
            this.labelControl16.TabIndex = 215;
            this.labelControl16.Text = "(6)您容易精神紧张、焦虑不安吗?（指遇事是否心情紧张）";
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(3, 271);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(386, 24);
            this.labelControl15.StyleController = this.layoutControl1;
            this.labelControl15.TabIndex = 214;
            this.labelControl15.Text = "(5)您感到闷闷不乐、情绪低沉吗?（指心情不愉快，情绪低落）";
            // 
            // labelControl14
            // 
            this.labelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl14.Location = new System.Drawing.Point(3, 243);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(386, 24);
            this.labelControl14.StyleController = this.layoutControl1;
            this.labelControl14.TabIndex = 213;
            this.labelControl14.Text = "(4)您说话声音低弱无力吗?（指说话没有力气）";
            // 
            // labelControl13
            // 
            this.labelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl13.Location = new System.Drawing.Point(3, 215);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(386, 24);
            this.labelControl13.StyleController = this.layoutControl1;
            this.labelControl13.TabIndex = 212;
            this.labelControl13.Text = "(3)您容易气短，呼吸短促，接不上气吗？";
            // 
            // labelControl12
            // 
            this.labelControl12.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Location = new System.Drawing.Point(3, 175);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(386, 36);
            this.labelControl12.StyleController = this.layoutControl1;
            this.labelControl12.TabIndex = 211;
            this.labelControl12.Text = "(2)您容易疲乏吗？（指体力如何，是否稍微活动一下或做一点家务劳动就感到累）";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl11.Location = new System.Drawing.Point(3, 147);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(386, 24);
            this.labelControl11.StyleController = this.layoutControl1;
            this.labelControl11.TabIndex = 210;
            this.labelControl11.Text = "(1)您精力充沛吗？（指精神头足，乐于做事）";
            // 
            // labelControl10
            // 
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Location = new System.Drawing.Point(3, 97);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(386, 46);
            this.labelControl10.StyleController = this.layoutControl1;
            this.labelControl10.TabIndex = 209;
            this.labelControl10.Text = "请根据近一年的体验和感觉，回答以下问题";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Location = new System.Drawing.Point(698, 1321);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(81, 14);
            this.labelControl9.StyleController = this.layoutControl1;
            this.labelControl9.TabIndex = 208;
            this.labelControl9.Text = " ";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl8.Location = new System.Drawing.Point(618, 1321);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(76, 14);
            this.labelControl8.StyleController = this.layoutControl1;
            this.labelControl8.TabIndex = 207;
            this.labelControl8.Text = " ";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl7.Location = new System.Drawing.Point(538, 1321);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(76, 14);
            this.labelControl7.StyleController = this.layoutControl1;
            this.labelControl7.TabIndex = 206;
            this.labelControl7.Text = " ";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl6.Location = new System.Drawing.Point(458, 1321);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(76, 14);
            this.labelControl6.StyleController = this.layoutControl1;
            this.labelControl6.TabIndex = 205;
            this.labelControl6.Text = " ";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl5.Location = new System.Drawing.Point(378, 1321);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(76, 14);
            this.labelControl5.StyleController = this.layoutControl1;
            this.labelControl5.TabIndex = 204;
            this.labelControl5.Text = " ";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl4.Location = new System.Drawing.Point(298, 1321);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(76, 14);
            this.labelControl4.StyleController = this.layoutControl1;
            this.labelControl4.TabIndex = 203;
            this.labelControl4.Text = " ";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.Location = new System.Drawing.Point(218, 1321);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(76, 14);
            this.labelControl3.StyleController = this.layoutControl1;
            this.labelControl3.TabIndex = 202;
            this.labelControl3.Text = " ";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.Location = new System.Drawing.Point(138, 1321);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(76, 14);
            this.labelControl2.StyleController = this.layoutControl1;
            this.labelControl2.TabIndex = 201;
            this.labelControl2.Text = " ";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.Location = new System.Drawing.Point(58, 1321);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(76, 14);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 200;
            this.labelControl1.Text = " ";
            // 
            // flow2
            // 
            this.flow2.BackColor = System.Drawing.Color.White;
            this.flow2.Controls.Add(this.chk21);
            this.flow2.Controls.Add(this.chk22);
            this.flow2.Controls.Add(this.chk23);
            this.flow2.Controls.Add(this.chk24);
            this.flow2.Controls.Add(this.chk25);
            this.flow2.Controls.Add(this.chk26);
            this.flow2.Controls.Add(this.txt2);
            this.flow2.Location = new System.Drawing.Point(138, 1339);
            this.flow2.Name = "flow2";
            this.flow2.Size = new System.Drawing.Size(76, 146);
            this.flow2.TabIndex = 196;
            // 
            // chk21
            // 
            this.chk21.Location = new System.Drawing.Point(0, 0);
            this.chk21.Margin = new System.Windows.Forms.Padding(0);
            this.chk21.Name = "chk21";
            this.chk21.Properties.Caption = "情志调摄";
            this.chk21.Size = new System.Drawing.Size(75, 19);
            this.chk21.TabIndex = 7;
            this.chk21.Tag = "1";
            // 
            // chk22
            // 
            this.chk22.Location = new System.Drawing.Point(0, 19);
            this.chk22.Margin = new System.Windows.Forms.Padding(0);
            this.chk22.Name = "chk22";
            this.chk22.Properties.Caption = "饮食调养";
            this.chk22.Size = new System.Drawing.Size(75, 19);
            this.chk22.TabIndex = 8;
            this.chk22.Tag = "2";
            // 
            // chk23
            // 
            this.chk23.Location = new System.Drawing.Point(0, 38);
            this.chk23.Margin = new System.Windows.Forms.Padding(0);
            this.chk23.Name = "chk23";
            this.chk23.Properties.Caption = "起居调摄";
            this.chk23.Size = new System.Drawing.Size(75, 19);
            this.chk23.TabIndex = 9;
            this.chk23.Tag = "3";
            // 
            // chk24
            // 
            this.chk24.Location = new System.Drawing.Point(0, 57);
            this.chk24.Margin = new System.Windows.Forms.Padding(0);
            this.chk24.Name = "chk24";
            this.chk24.Properties.Caption = "运动保健";
            this.chk24.Size = new System.Drawing.Size(75, 19);
            this.chk24.TabIndex = 10;
            this.chk24.Tag = "4";
            // 
            // chk25
            // 
            this.chk25.Location = new System.Drawing.Point(0, 76);
            this.chk25.Margin = new System.Windows.Forms.Padding(0);
            this.chk25.Name = "chk25";
            this.chk25.Properties.Caption = "穴位保健";
            this.chk25.Size = new System.Drawing.Size(75, 19);
            this.chk25.TabIndex = 11;
            this.chk25.Tag = "5";
            // 
            // chk26
            // 
            this.chk26.Location = new System.Drawing.Point(0, 95);
            this.chk26.Margin = new System.Windows.Forms.Padding(0);
            this.chk26.Name = "chk26";
            this.chk26.Properties.Caption = "其他";
            this.chk26.Size = new System.Drawing.Size(75, 19);
            this.chk26.TabIndex = 12;
            this.chk26.Tag = "6";
            // 
            // txt2
            // 
            this.txt2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt2.Location = new System.Drawing.Point(0, 114);
            this.txt2.Margin = new System.Windows.Forms.Padding(0);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(75, 20);
            this.txt2.TabIndex = 13;
            // 
            // flow1
            // 
            this.flow1.BackColor = System.Drawing.Color.White;
            this.flow1.Controls.Add(this.chk11);
            this.flow1.Controls.Add(this.chk12);
            this.flow1.Controls.Add(this.chk13);
            this.flow1.Controls.Add(this.chk14);
            this.flow1.Controls.Add(this.chk15);
            this.flow1.Controls.Add(this.chk16);
            this.flow1.Controls.Add(this.txt1);
            this.flow1.Location = new System.Drawing.Point(58, 1339);
            this.flow1.Name = "flow1";
            this.flow1.Size = new System.Drawing.Size(76, 146);
            this.flow1.TabIndex = 195;
            // 
            // chk11
            // 
            this.chk11.Location = new System.Drawing.Point(0, 0);
            this.chk11.Margin = new System.Windows.Forms.Padding(0);
            this.chk11.Name = "chk11";
            this.chk11.Properties.Caption = "情志调摄";
            this.chk11.Size = new System.Drawing.Size(75, 19);
            this.chk11.TabIndex = 7;
            this.chk11.Tag = "1";
            // 
            // chk12
            // 
            this.chk12.Location = new System.Drawing.Point(0, 19);
            this.chk12.Margin = new System.Windows.Forms.Padding(0);
            this.chk12.Name = "chk12";
            this.chk12.Properties.Caption = "饮食调养";
            this.chk12.Size = new System.Drawing.Size(75, 19);
            this.chk12.TabIndex = 8;
            this.chk12.Tag = "2";
            // 
            // chk13
            // 
            this.chk13.Location = new System.Drawing.Point(0, 38);
            this.chk13.Margin = new System.Windows.Forms.Padding(0);
            this.chk13.Name = "chk13";
            this.chk13.Properties.Caption = "起居调摄";
            this.chk13.Size = new System.Drawing.Size(75, 19);
            this.chk13.TabIndex = 9;
            this.chk13.Tag = "3";
            // 
            // chk14
            // 
            this.chk14.Location = new System.Drawing.Point(0, 57);
            this.chk14.Margin = new System.Windows.Forms.Padding(0);
            this.chk14.Name = "chk14";
            this.chk14.Properties.Caption = "运动保健";
            this.chk14.Size = new System.Drawing.Size(75, 19);
            this.chk14.TabIndex = 10;
            this.chk14.Tag = "4";
            // 
            // chk15
            // 
            this.chk15.Location = new System.Drawing.Point(0, 76);
            this.chk15.Margin = new System.Windows.Forms.Padding(0);
            this.chk15.Name = "chk15";
            this.chk15.Properties.Caption = "穴位保健";
            this.chk15.Size = new System.Drawing.Size(75, 19);
            this.chk15.TabIndex = 11;
            this.chk15.Tag = "5";
            // 
            // chk16
            // 
            this.chk16.Location = new System.Drawing.Point(0, 95);
            this.chk16.Margin = new System.Windows.Forms.Padding(0);
            this.chk16.Name = "chk16";
            this.chk16.Properties.Caption = "其他";
            this.chk16.Size = new System.Drawing.Size(75, 19);
            this.chk16.TabIndex = 12;
            this.chk16.Tag = "6";
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(0, 114);
            this.txt1.Margin = new System.Windows.Forms.Padding(0);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(75, 20);
            this.txt1.TabIndex = 13;
            // 
            // ucpinghdf
            // 
            this.ucpinghdf.BackColor = System.Drawing.Color.White;
            this.ucpinghdf.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucpinghdf.Lbl1Text = "得分：";
            this.ucpinghdf.Location = new System.Drawing.Point(698, 1297);
            this.ucpinghdf.Margin = new System.Windows.Forms.Padding(4);
            this.ucpinghdf.Name = "ucpinghdf";
            this.ucpinghdf.Size = new System.Drawing.Size(81, 20);
            this.ucpinghdf.TabIndex = 194;
            this.ucpinghdf.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // uctebdf
            // 
            this.uctebdf.BackColor = System.Drawing.Color.White;
            this.uctebdf.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uctebdf.Lbl1Text = "得分:";
            this.uctebdf.Location = new System.Drawing.Point(618, 1297);
            this.uctebdf.Margin = new System.Windows.Forms.Padding(4);
            this.uctebdf.Name = "uctebdf";
            this.uctebdf.Size = new System.Drawing.Size(76, 20);
            this.uctebdf.TabIndex = 193;
            this.uctebdf.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // flow9
            // 
            this.flow9.BackColor = System.Drawing.Color.White;
            this.flow9.Controls.Add(this.checkEdit214);
            this.flow9.Controls.Add(this.checkEdit215);
            this.flow9.Controls.Add(this.checkEdit216);
            this.flow9.Controls.Add(this.checkEdit217);
            this.flow9.Controls.Add(this.checkEdit218);
            this.flow9.Controls.Add(this.checkEdit219);
            this.flow9.Controls.Add(this.txt9);
            this.flow9.Location = new System.Drawing.Point(698, 1339);
            this.flow9.Name = "flow9";
            this.flow9.Size = new System.Drawing.Size(81, 146);
            this.flow9.TabIndex = 199;
            // 
            // checkEdit214
            // 
            this.checkEdit214.Location = new System.Drawing.Point(0, 0);
            this.checkEdit214.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit214.Name = "checkEdit214";
            this.checkEdit214.Properties.Caption = "情志调摄";
            this.checkEdit214.Size = new System.Drawing.Size(75, 19);
            this.checkEdit214.TabIndex = 7;
            this.checkEdit214.Tag = "1";
            // 
            // checkEdit215
            // 
            this.checkEdit215.Location = new System.Drawing.Point(0, 19);
            this.checkEdit215.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit215.Name = "checkEdit215";
            this.checkEdit215.Properties.Caption = "饮食调养";
            this.checkEdit215.Size = new System.Drawing.Size(75, 19);
            this.checkEdit215.TabIndex = 8;
            this.checkEdit215.Tag = "2";
            // 
            // checkEdit216
            // 
            this.checkEdit216.Location = new System.Drawing.Point(0, 38);
            this.checkEdit216.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit216.Name = "checkEdit216";
            this.checkEdit216.Properties.Caption = "起居调摄";
            this.checkEdit216.Size = new System.Drawing.Size(75, 19);
            this.checkEdit216.TabIndex = 9;
            this.checkEdit216.Tag = "3";
            // 
            // checkEdit217
            // 
            this.checkEdit217.Location = new System.Drawing.Point(0, 57);
            this.checkEdit217.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit217.Name = "checkEdit217";
            this.checkEdit217.Properties.Caption = "运动保健";
            this.checkEdit217.Size = new System.Drawing.Size(75, 19);
            this.checkEdit217.TabIndex = 10;
            this.checkEdit217.Tag = "4";
            // 
            // checkEdit218
            // 
            this.checkEdit218.Location = new System.Drawing.Point(0, 76);
            this.checkEdit218.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit218.Name = "checkEdit218";
            this.checkEdit218.Properties.Caption = "穴位保健";
            this.checkEdit218.Size = new System.Drawing.Size(75, 19);
            this.checkEdit218.TabIndex = 11;
            this.checkEdit218.Tag = "5";
            // 
            // checkEdit219
            // 
            this.checkEdit219.Location = new System.Drawing.Point(0, 95);
            this.checkEdit219.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit219.Name = "checkEdit219";
            this.checkEdit219.Properties.Caption = "其他";
            this.checkEdit219.Size = new System.Drawing.Size(75, 19);
            this.checkEdit219.TabIndex = 12;
            this.checkEdit219.Tag = "6";
            // 
            // txt9
            // 
            this.txt9.Location = new System.Drawing.Point(0, 114);
            this.txt9.Margin = new System.Windows.Forms.Padding(0);
            this.txt9.Name = "txt9";
            this.txt9.Size = new System.Drawing.Size(75, 20);
            this.txt9.TabIndex = 13;
            // 
            // flow8
            // 
            this.flow8.BackColor = System.Drawing.Color.White;
            this.flow8.Controls.Add(this.checkEdit208);
            this.flow8.Controls.Add(this.checkEdit209);
            this.flow8.Controls.Add(this.checkEdit210);
            this.flow8.Controls.Add(this.checkEdit211);
            this.flow8.Controls.Add(this.checkEdit212);
            this.flow8.Controls.Add(this.checkEdit213);
            this.flow8.Controls.Add(this.txt8);
            this.flow8.Location = new System.Drawing.Point(618, 1339);
            this.flow8.Name = "flow8";
            this.flow8.Size = new System.Drawing.Size(76, 146);
            this.flow8.TabIndex = 199;
            // 
            // checkEdit208
            // 
            this.checkEdit208.Location = new System.Drawing.Point(0, 0);
            this.checkEdit208.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit208.Name = "checkEdit208";
            this.checkEdit208.Properties.Caption = "情志调摄";
            this.checkEdit208.Size = new System.Drawing.Size(75, 19);
            this.checkEdit208.TabIndex = 7;
            this.checkEdit208.Tag = "1";
            // 
            // checkEdit209
            // 
            this.checkEdit209.Location = new System.Drawing.Point(0, 19);
            this.checkEdit209.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit209.Name = "checkEdit209";
            this.checkEdit209.Properties.Caption = "饮食调养";
            this.checkEdit209.Size = new System.Drawing.Size(75, 19);
            this.checkEdit209.TabIndex = 8;
            this.checkEdit209.Tag = "2";
            // 
            // checkEdit210
            // 
            this.checkEdit210.Location = new System.Drawing.Point(0, 38);
            this.checkEdit210.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit210.Name = "checkEdit210";
            this.checkEdit210.Properties.Caption = "起居调摄";
            this.checkEdit210.Size = new System.Drawing.Size(75, 19);
            this.checkEdit210.TabIndex = 9;
            this.checkEdit210.Tag = "3";
            // 
            // checkEdit211
            // 
            this.checkEdit211.Location = new System.Drawing.Point(0, 57);
            this.checkEdit211.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit211.Name = "checkEdit211";
            this.checkEdit211.Properties.Caption = "运动保健";
            this.checkEdit211.Size = new System.Drawing.Size(75, 19);
            this.checkEdit211.TabIndex = 10;
            this.checkEdit211.Tag = "4";
            // 
            // checkEdit212
            // 
            this.checkEdit212.Location = new System.Drawing.Point(0, 76);
            this.checkEdit212.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit212.Name = "checkEdit212";
            this.checkEdit212.Properties.Caption = "穴位保健";
            this.checkEdit212.Size = new System.Drawing.Size(75, 19);
            this.checkEdit212.TabIndex = 11;
            this.checkEdit212.Tag = "5";
            // 
            // checkEdit213
            // 
            this.checkEdit213.Location = new System.Drawing.Point(0, 95);
            this.checkEdit213.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit213.Name = "checkEdit213";
            this.checkEdit213.Properties.Caption = "其他";
            this.checkEdit213.Size = new System.Drawing.Size(75, 19);
            this.checkEdit213.TabIndex = 12;
            this.checkEdit213.Tag = "6";
            // 
            // txt8
            // 
            this.txt8.Location = new System.Drawing.Point(0, 114);
            this.txt8.Margin = new System.Windows.Forms.Padding(0);
            this.txt8.Name = "txt8";
            this.txt8.Size = new System.Drawing.Size(75, 20);
            this.txt8.TabIndex = 13;
            // 
            // flow7
            // 
            this.flow7.BackColor = System.Drawing.Color.White;
            this.flow7.Controls.Add(this.checkEdit202);
            this.flow7.Controls.Add(this.checkEdit203);
            this.flow7.Controls.Add(this.checkEdit204);
            this.flow7.Controls.Add(this.checkEdit205);
            this.flow7.Controls.Add(this.checkEdit206);
            this.flow7.Controls.Add(this.checkEdit207);
            this.flow7.Controls.Add(this.txt7);
            this.flow7.Location = new System.Drawing.Point(538, 1339);
            this.flow7.Name = "flow7";
            this.flow7.Size = new System.Drawing.Size(76, 146);
            this.flow7.TabIndex = 199;
            // 
            // checkEdit202
            // 
            this.checkEdit202.Location = new System.Drawing.Point(0, 0);
            this.checkEdit202.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit202.Name = "checkEdit202";
            this.checkEdit202.Properties.Caption = "情志调摄";
            this.checkEdit202.Size = new System.Drawing.Size(75, 19);
            this.checkEdit202.TabIndex = 7;
            this.checkEdit202.Tag = "1";
            // 
            // checkEdit203
            // 
            this.checkEdit203.Location = new System.Drawing.Point(0, 19);
            this.checkEdit203.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit203.Name = "checkEdit203";
            this.checkEdit203.Properties.Caption = "饮食调养";
            this.checkEdit203.Size = new System.Drawing.Size(75, 19);
            this.checkEdit203.TabIndex = 8;
            this.checkEdit203.Tag = "2";
            // 
            // checkEdit204
            // 
            this.checkEdit204.Location = new System.Drawing.Point(0, 38);
            this.checkEdit204.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit204.Name = "checkEdit204";
            this.checkEdit204.Properties.Caption = "起居调摄";
            this.checkEdit204.Size = new System.Drawing.Size(75, 19);
            this.checkEdit204.TabIndex = 9;
            this.checkEdit204.Tag = "3";
            // 
            // checkEdit205
            // 
            this.checkEdit205.Location = new System.Drawing.Point(0, 57);
            this.checkEdit205.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit205.Name = "checkEdit205";
            this.checkEdit205.Properties.Caption = "运动保健";
            this.checkEdit205.Size = new System.Drawing.Size(75, 19);
            this.checkEdit205.TabIndex = 10;
            this.checkEdit205.Tag = "4";
            // 
            // checkEdit206
            // 
            this.checkEdit206.Location = new System.Drawing.Point(0, 76);
            this.checkEdit206.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit206.Name = "checkEdit206";
            this.checkEdit206.Properties.Caption = "穴位保健";
            this.checkEdit206.Size = new System.Drawing.Size(75, 19);
            this.checkEdit206.TabIndex = 11;
            this.checkEdit206.Tag = "5";
            // 
            // checkEdit207
            // 
            this.checkEdit207.Location = new System.Drawing.Point(0, 95);
            this.checkEdit207.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit207.Name = "checkEdit207";
            this.checkEdit207.Properties.Caption = "其他";
            this.checkEdit207.Size = new System.Drawing.Size(75, 19);
            this.checkEdit207.TabIndex = 12;
            this.checkEdit207.Tag = "6";
            // 
            // txt7
            // 
            this.txt7.Location = new System.Drawing.Point(0, 114);
            this.txt7.Margin = new System.Windows.Forms.Padding(0);
            this.txt7.Name = "txt7";
            this.txt7.Size = new System.Drawing.Size(75, 20);
            this.txt7.TabIndex = 13;
            // 
            // flow6
            // 
            this.flow6.BackColor = System.Drawing.Color.White;
            this.flow6.Controls.Add(this.checkEdit196);
            this.flow6.Controls.Add(this.checkEdit197);
            this.flow6.Controls.Add(this.checkEdit198);
            this.flow6.Controls.Add(this.checkEdit199);
            this.flow6.Controls.Add(this.checkEdit200);
            this.flow6.Controls.Add(this.checkEdit201);
            this.flow6.Controls.Add(this.txt6);
            this.flow6.Location = new System.Drawing.Point(458, 1339);
            this.flow6.Name = "flow6";
            this.flow6.Size = new System.Drawing.Size(76, 146);
            this.flow6.TabIndex = 198;
            // 
            // checkEdit196
            // 
            this.checkEdit196.Location = new System.Drawing.Point(0, 0);
            this.checkEdit196.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit196.Name = "checkEdit196";
            this.checkEdit196.Properties.Caption = "情志调摄";
            this.checkEdit196.Size = new System.Drawing.Size(75, 19);
            this.checkEdit196.TabIndex = 7;
            this.checkEdit196.Tag = "1";
            // 
            // checkEdit197
            // 
            this.checkEdit197.Location = new System.Drawing.Point(0, 19);
            this.checkEdit197.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit197.Name = "checkEdit197";
            this.checkEdit197.Properties.Caption = "饮食调养";
            this.checkEdit197.Size = new System.Drawing.Size(75, 19);
            this.checkEdit197.TabIndex = 8;
            this.checkEdit197.Tag = "2";
            // 
            // checkEdit198
            // 
            this.checkEdit198.Location = new System.Drawing.Point(0, 38);
            this.checkEdit198.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit198.Name = "checkEdit198";
            this.checkEdit198.Properties.Caption = "起居调摄";
            this.checkEdit198.Size = new System.Drawing.Size(75, 19);
            this.checkEdit198.TabIndex = 9;
            this.checkEdit198.Tag = "3";
            // 
            // checkEdit199
            // 
            this.checkEdit199.Location = new System.Drawing.Point(0, 57);
            this.checkEdit199.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit199.Name = "checkEdit199";
            this.checkEdit199.Properties.Caption = "运动保健";
            this.checkEdit199.Size = new System.Drawing.Size(75, 19);
            this.checkEdit199.TabIndex = 10;
            this.checkEdit199.Tag = "4";
            // 
            // checkEdit200
            // 
            this.checkEdit200.Location = new System.Drawing.Point(0, 76);
            this.checkEdit200.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit200.Name = "checkEdit200";
            this.checkEdit200.Properties.Caption = "穴位保健";
            this.checkEdit200.Size = new System.Drawing.Size(75, 19);
            this.checkEdit200.TabIndex = 11;
            this.checkEdit200.Tag = "5";
            // 
            // checkEdit201
            // 
            this.checkEdit201.Location = new System.Drawing.Point(0, 95);
            this.checkEdit201.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit201.Name = "checkEdit201";
            this.checkEdit201.Properties.Caption = "其他";
            this.checkEdit201.Size = new System.Drawing.Size(75, 19);
            this.checkEdit201.TabIndex = 12;
            this.checkEdit201.Tag = "6";
            // 
            // txt6
            // 
            this.txt6.Location = new System.Drawing.Point(0, 114);
            this.txt6.Margin = new System.Windows.Forms.Padding(0);
            this.txt6.Name = "txt6";
            this.txt6.Size = new System.Drawing.Size(75, 20);
            this.txt6.TabIndex = 13;
            // 
            // flow5
            // 
            this.flow5.BackColor = System.Drawing.Color.White;
            this.flow5.Controls.Add(this.checkEdit190);
            this.flow5.Controls.Add(this.checkEdit191);
            this.flow5.Controls.Add(this.checkEdit192);
            this.flow5.Controls.Add(this.checkEdit193);
            this.flow5.Controls.Add(this.checkEdit194);
            this.flow5.Controls.Add(this.checkEdit195);
            this.flow5.Controls.Add(this.txt5);
            this.flow5.Location = new System.Drawing.Point(378, 1339);
            this.flow5.Name = "flow5";
            this.flow5.Size = new System.Drawing.Size(76, 146);
            this.flow5.TabIndex = 198;
            // 
            // checkEdit190
            // 
            this.checkEdit190.Location = new System.Drawing.Point(0, 0);
            this.checkEdit190.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit190.Name = "checkEdit190";
            this.checkEdit190.Properties.Caption = "情志调摄";
            this.checkEdit190.Size = new System.Drawing.Size(75, 19);
            this.checkEdit190.TabIndex = 7;
            this.checkEdit190.Tag = "1";
            // 
            // checkEdit191
            // 
            this.checkEdit191.Location = new System.Drawing.Point(0, 19);
            this.checkEdit191.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit191.Name = "checkEdit191";
            this.checkEdit191.Properties.Caption = "饮食调养";
            this.checkEdit191.Size = new System.Drawing.Size(75, 19);
            this.checkEdit191.TabIndex = 8;
            this.checkEdit191.Tag = "2";
            // 
            // checkEdit192
            // 
            this.checkEdit192.Location = new System.Drawing.Point(0, 38);
            this.checkEdit192.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit192.Name = "checkEdit192";
            this.checkEdit192.Properties.Caption = "起居调摄";
            this.checkEdit192.Size = new System.Drawing.Size(75, 19);
            this.checkEdit192.TabIndex = 9;
            this.checkEdit192.Tag = "3";
            // 
            // checkEdit193
            // 
            this.checkEdit193.Location = new System.Drawing.Point(0, 57);
            this.checkEdit193.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit193.Name = "checkEdit193";
            this.checkEdit193.Properties.Caption = "运动保健";
            this.checkEdit193.Size = new System.Drawing.Size(75, 19);
            this.checkEdit193.TabIndex = 10;
            this.checkEdit193.Tag = "4";
            // 
            // checkEdit194
            // 
            this.checkEdit194.Location = new System.Drawing.Point(0, 76);
            this.checkEdit194.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit194.Name = "checkEdit194";
            this.checkEdit194.Properties.Caption = "穴位保健";
            this.checkEdit194.Size = new System.Drawing.Size(75, 19);
            this.checkEdit194.TabIndex = 11;
            this.checkEdit194.Tag = "5";
            // 
            // checkEdit195
            // 
            this.checkEdit195.Location = new System.Drawing.Point(0, 95);
            this.checkEdit195.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit195.Name = "checkEdit195";
            this.checkEdit195.Properties.Caption = "其他";
            this.checkEdit195.Size = new System.Drawing.Size(75, 19);
            this.checkEdit195.TabIndex = 12;
            this.checkEdit195.Tag = "6";
            // 
            // txt5
            // 
            this.txt5.Location = new System.Drawing.Point(0, 114);
            this.txt5.Margin = new System.Windows.Forms.Padding(0);
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(75, 20);
            this.txt5.TabIndex = 13;
            // 
            // flow4
            // 
            this.flow4.BackColor = System.Drawing.Color.White;
            this.flow4.Controls.Add(this.checkEdit184);
            this.flow4.Controls.Add(this.checkEdit185);
            this.flow4.Controls.Add(this.checkEdit186);
            this.flow4.Controls.Add(this.checkEdit187);
            this.flow4.Controls.Add(this.checkEdit188);
            this.flow4.Controls.Add(this.checkEdit189);
            this.flow4.Controls.Add(this.txt4);
            this.flow4.Location = new System.Drawing.Point(298, 1339);
            this.flow4.Name = "flow4";
            this.flow4.Size = new System.Drawing.Size(76, 146);
            this.flow4.TabIndex = 197;
            // 
            // checkEdit184
            // 
            this.checkEdit184.Location = new System.Drawing.Point(0, 0);
            this.checkEdit184.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit184.Name = "checkEdit184";
            this.checkEdit184.Properties.Caption = "情志调摄";
            this.checkEdit184.Size = new System.Drawing.Size(75, 19);
            this.checkEdit184.TabIndex = 7;
            this.checkEdit184.Tag = "1";
            // 
            // checkEdit185
            // 
            this.checkEdit185.Location = new System.Drawing.Point(0, 19);
            this.checkEdit185.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit185.Name = "checkEdit185";
            this.checkEdit185.Properties.Caption = "饮食调养";
            this.checkEdit185.Size = new System.Drawing.Size(75, 19);
            this.checkEdit185.TabIndex = 8;
            this.checkEdit185.Tag = "2";
            // 
            // checkEdit186
            // 
            this.checkEdit186.Location = new System.Drawing.Point(0, 38);
            this.checkEdit186.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit186.Name = "checkEdit186";
            this.checkEdit186.Properties.Caption = "起居调摄";
            this.checkEdit186.Size = new System.Drawing.Size(75, 19);
            this.checkEdit186.TabIndex = 9;
            this.checkEdit186.Tag = "3";
            // 
            // checkEdit187
            // 
            this.checkEdit187.Location = new System.Drawing.Point(0, 57);
            this.checkEdit187.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit187.Name = "checkEdit187";
            this.checkEdit187.Properties.Caption = "运动保健";
            this.checkEdit187.Size = new System.Drawing.Size(75, 19);
            this.checkEdit187.TabIndex = 10;
            this.checkEdit187.Tag = "4";
            // 
            // checkEdit188
            // 
            this.checkEdit188.Location = new System.Drawing.Point(0, 76);
            this.checkEdit188.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit188.Name = "checkEdit188";
            this.checkEdit188.Properties.Caption = "穴位保健";
            this.checkEdit188.Size = new System.Drawing.Size(75, 19);
            this.checkEdit188.TabIndex = 11;
            this.checkEdit188.Tag = "5";
            // 
            // checkEdit189
            // 
            this.checkEdit189.Location = new System.Drawing.Point(0, 95);
            this.checkEdit189.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit189.Name = "checkEdit189";
            this.checkEdit189.Properties.Caption = "其他";
            this.checkEdit189.Size = new System.Drawing.Size(75, 19);
            this.checkEdit189.TabIndex = 12;
            this.checkEdit189.Tag = "6";
            // 
            // txt4
            // 
            this.txt4.Location = new System.Drawing.Point(0, 114);
            this.txt4.Margin = new System.Windows.Forms.Padding(0);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(75, 20);
            this.txt4.TabIndex = 13;
            // 
            // flow3
            // 
            this.flow3.BackColor = System.Drawing.Color.White;
            this.flow3.Controls.Add(this.checkEdit178);
            this.flow3.Controls.Add(this.checkEdit179);
            this.flow3.Controls.Add(this.checkEdit180);
            this.flow3.Controls.Add(this.checkEdit181);
            this.flow3.Controls.Add(this.checkEdit182);
            this.flow3.Controls.Add(this.checkEdit183);
            this.flow3.Controls.Add(this.txt3);
            this.flow3.Location = new System.Drawing.Point(218, 1339);
            this.flow3.Name = "flow3";
            this.flow3.Size = new System.Drawing.Size(76, 146);
            this.flow3.TabIndex = 196;
            // 
            // checkEdit178
            // 
            this.checkEdit178.Location = new System.Drawing.Point(0, 0);
            this.checkEdit178.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit178.Name = "checkEdit178";
            this.checkEdit178.Properties.Caption = "情志调摄";
            this.checkEdit178.Size = new System.Drawing.Size(75, 19);
            this.checkEdit178.TabIndex = 7;
            this.checkEdit178.Tag = "1";
            // 
            // checkEdit179
            // 
            this.checkEdit179.Location = new System.Drawing.Point(0, 19);
            this.checkEdit179.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit179.Name = "checkEdit179";
            this.checkEdit179.Properties.Caption = "饮食调养";
            this.checkEdit179.Size = new System.Drawing.Size(75, 19);
            this.checkEdit179.TabIndex = 8;
            this.checkEdit179.Tag = "2";
            // 
            // checkEdit180
            // 
            this.checkEdit180.Location = new System.Drawing.Point(0, 38);
            this.checkEdit180.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit180.Name = "checkEdit180";
            this.checkEdit180.Properties.Caption = "起居调摄";
            this.checkEdit180.Size = new System.Drawing.Size(75, 19);
            this.checkEdit180.TabIndex = 9;
            this.checkEdit180.Tag = "3";
            // 
            // checkEdit181
            // 
            this.checkEdit181.Location = new System.Drawing.Point(0, 57);
            this.checkEdit181.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit181.Name = "checkEdit181";
            this.checkEdit181.Properties.Caption = "运动保健";
            this.checkEdit181.Size = new System.Drawing.Size(75, 19);
            this.checkEdit181.TabIndex = 10;
            this.checkEdit181.Tag = "4";
            // 
            // checkEdit182
            // 
            this.checkEdit182.Location = new System.Drawing.Point(0, 76);
            this.checkEdit182.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit182.Name = "checkEdit182";
            this.checkEdit182.Properties.Caption = "穴位保健";
            this.checkEdit182.Size = new System.Drawing.Size(75, 19);
            this.checkEdit182.TabIndex = 11;
            this.checkEdit182.Tag = "5";
            // 
            // checkEdit183
            // 
            this.checkEdit183.Location = new System.Drawing.Point(0, 95);
            this.checkEdit183.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit183.Name = "checkEdit183";
            this.checkEdit183.Properties.Caption = "其他";
            this.checkEdit183.Size = new System.Drawing.Size(75, 19);
            this.checkEdit183.TabIndex = 12;
            this.checkEdit183.Tag = "6";
            // 
            // txt3
            // 
            this.txt3.Location = new System.Drawing.Point(0, 114);
            this.txt3.Margin = new System.Windows.Forms.Padding(0);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(75, 20);
            this.txt3.TabIndex = 13;
            // 
            // ucqiydf
            // 
            this.ucqiydf.BackColor = System.Drawing.Color.White;
            this.ucqiydf.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucqiydf.Lbl1Text = "得分：";
            this.ucqiydf.Location = new System.Drawing.Point(538, 1297);
            this.ucqiydf.Margin = new System.Windows.Forms.Padding(4);
            this.ucqiydf.Name = "ucqiydf";
            this.ucqiydf.Size = new System.Drawing.Size(76, 20);
            this.ucqiydf.TabIndex = 192;
            this.ucqiydf.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // ucxueydf
            // 
            this.ucxueydf.BackColor = System.Drawing.Color.White;
            this.ucxueydf.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucxueydf.Lbl1Text = "得分：";
            this.ucxueydf.Location = new System.Drawing.Point(458, 1297);
            this.ucxueydf.Margin = new System.Windows.Forms.Padding(4);
            this.ucxueydf.Name = "ucxueydf";
            this.ucxueydf.Size = new System.Drawing.Size(76, 20);
            this.ucxueydf.TabIndex = 191;
            this.ucxueydf.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // ucshirdf
            // 
            this.ucshirdf.BackColor = System.Drawing.Color.White;
            this.ucshirdf.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucshirdf.Lbl1Text = "得分：";
            this.ucshirdf.Location = new System.Drawing.Point(378, 1297);
            this.ucshirdf.Margin = new System.Windows.Forms.Padding(4);
            this.ucshirdf.Name = "ucshirdf";
            this.ucshirdf.Size = new System.Drawing.Size(76, 20);
            this.ucshirdf.TabIndex = 190;
            this.ucshirdf.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // uctansdf
            // 
            this.uctansdf.BackColor = System.Drawing.Color.White;
            this.uctansdf.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uctansdf.Lbl1Text = "得分：";
            this.uctansdf.Location = new System.Drawing.Point(298, 1297);
            this.uctansdf.Margin = new System.Windows.Forms.Padding(4);
            this.uctansdf.Name = "uctansdf";
            this.uctansdf.Size = new System.Drawing.Size(76, 20);
            this.uctansdf.TabIndex = 189;
            this.uctansdf.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // ucyindf
            // 
            this.ucyindf.BackColor = System.Drawing.Color.White;
            this.ucyindf.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucyindf.Lbl1Text = "得分：";
            this.ucyindf.Location = new System.Drawing.Point(218, 1297);
            this.ucyindf.Margin = new System.Windows.Forms.Padding(4);
            this.ucyindf.Name = "ucyindf";
            this.ucyindf.Size = new System.Drawing.Size(76, 20);
            this.ucyindf.TabIndex = 188;
            this.ucyindf.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // ucyangdf
            // 
            this.ucyangdf.BackColor = System.Drawing.Color.White;
            this.ucyangdf.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucyangdf.Lbl1Text = "得分：";
            this.ucyangdf.Location = new System.Drawing.Point(138, 1297);
            this.ucyangdf.Margin = new System.Windows.Forms.Padding(4);
            this.ucyangdf.Name = "ucyangdf";
            this.ucyangdf.Size = new System.Drawing.Size(76, 20);
            this.ucyangdf.TabIndex = 187;
            this.ucyangdf.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // ucqxdf
            // 
            this.ucqxdf.BackColor = System.Drawing.Color.White;
            this.ucqxdf.Lbl1Size = new System.Drawing.Size(30, 18);
            this.ucqxdf.Lbl1Text = "得分：";
            this.ucqxdf.Location = new System.Drawing.Point(58, 1297);
            this.ucqxdf.Margin = new System.Windows.Forms.Padding(4);
            this.ucqxdf.Name = "ucqxdf";
            this.ucqxdf.Size = new System.Drawing.Size(76, 20);
            this.ucqxdf.TabIndex = 186;
            this.ucqxdf.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // txt最近修改人
            // 
            this.txt最近修改人.Location = new System.Drawing.Point(584, 1537);
            this.txt最近修改人.Name = "txt最近修改人";
            this.txt最近修改人.Properties.ReadOnly = true;
            this.txt最近修改人.Size = new System.Drawing.Size(195, 20);
            this.txt最近修改人.StyleController = this.layoutControl1;
            this.txt最近修改人.TabIndex = 185;
            // 
            // txt创建人
            // 
            this.txt创建人.Location = new System.Drawing.Point(334, 1537);
            this.txt创建人.Name = "txt创建人";
            this.txt创建人.Properties.ReadOnly = true;
            this.txt创建人.Size = new System.Drawing.Size(165, 20);
            this.txt创建人.StyleController = this.layoutControl1;
            this.txt创建人.TabIndex = 184;
            // 
            // txt创建机构
            // 
            this.txt创建机构.Location = new System.Drawing.Point(88, 1537);
            this.txt创建机构.Name = "txt创建机构";
            this.txt创建机构.Properties.ReadOnly = true;
            this.txt创建机构.Size = new System.Drawing.Size(161, 20);
            this.txt创建机构.StyleController = this.layoutControl1;
            this.txt创建机构.TabIndex = 183;
            // 
            // txt当前所属机构
            // 
            this.txt当前所属机构.Location = new System.Drawing.Point(584, 1513);
            this.txt当前所属机构.Name = "txt当前所属机构";
            this.txt当前所属机构.Properties.ReadOnly = true;
            this.txt当前所属机构.Size = new System.Drawing.Size(195, 20);
            this.txt当前所属机构.StyleController = this.layoutControl1;
            this.txt当前所属机构.TabIndex = 182;
            // 
            // txt最近更新时间
            // 
            this.txt最近更新时间.Location = new System.Drawing.Point(334, 1513);
            this.txt最近更新时间.Name = "txt最近更新时间";
            this.txt最近更新时间.Properties.ReadOnly = true;
            this.txt最近更新时间.Size = new System.Drawing.Size(165, 20);
            this.txt最近更新时间.StyleController = this.layoutControl1;
            this.txt最近更新时间.TabIndex = 181;
            // 
            // txt创建时间
            // 
            this.txt创建时间.Location = new System.Drawing.Point(88, 1513);
            this.txt创建时间.Name = "txt创建时间";
            this.txt创建时间.Properties.ReadOnly = true;
            this.txt创建时间.Size = new System.Drawing.Size(161, 20);
            this.txt创建时间.StyleController = this.layoutControl1;
            this.txt创建时间.TabIndex = 180;
            // 
            // txt医生签名
            // 
            this.txt医生签名.Location = new System.Drawing.Point(308, 1489);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Size = new System.Drawing.Size(191, 20);
            this.txt医生签名.StyleController = this.layoutControl1;
            this.txt医生签名.TabIndex = 179;
            // 
            // checkEdit165
            // 
            this.checkEdit165.Location = new System.Drawing.Point(673, 1245);
            this.checkEdit165.Name = "checkEdit165";
            this.checkEdit165.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit165.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit165.Properties.AutoHeight = false;
            this.checkEdit165.Properties.Caption = "5";
            this.checkEdit165.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit165.Properties.RadioGroupIndex = 33;
            this.checkEdit165.Size = new System.Drawing.Size(106, 24);
            this.checkEdit165.StyleController = this.layoutControl1;
            this.checkEdit165.TabIndex = 177;
            this.checkEdit165.TabStop = false;
            this.checkEdit165.Tag = "5";
            this.checkEdit165.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit164
            // 
            this.checkEdit164.Location = new System.Drawing.Point(603, 1245);
            this.checkEdit164.Name = "checkEdit164";
            this.checkEdit164.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit164.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit164.Properties.AutoHeight = false;
            this.checkEdit164.Properties.Caption = "4";
            this.checkEdit164.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit164.Properties.RadioGroupIndex = 33;
            this.checkEdit164.Size = new System.Drawing.Size(66, 24);
            this.checkEdit164.StyleController = this.layoutControl1;
            this.checkEdit164.TabIndex = 176;
            this.checkEdit164.TabStop = false;
            this.checkEdit164.Tag = "4";
            this.checkEdit164.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit163
            // 
            this.checkEdit163.Location = new System.Drawing.Point(533, 1245);
            this.checkEdit163.Name = "checkEdit163";
            this.checkEdit163.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit163.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit163.Properties.AutoHeight = false;
            this.checkEdit163.Properties.Caption = "3";
            this.checkEdit163.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit163.Properties.RadioGroupIndex = 33;
            this.checkEdit163.Size = new System.Drawing.Size(66, 24);
            this.checkEdit163.StyleController = this.layoutControl1;
            this.checkEdit163.TabIndex = 175;
            this.checkEdit163.TabStop = false;
            this.checkEdit163.Tag = "3";
            this.checkEdit163.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit162
            // 
            this.checkEdit162.Location = new System.Drawing.Point(463, 1245);
            this.checkEdit162.Name = "checkEdit162";
            this.checkEdit162.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit162.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit162.Properties.AutoHeight = false;
            this.checkEdit162.Properties.Caption = "2";
            this.checkEdit162.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit162.Properties.RadioGroupIndex = 33;
            this.checkEdit162.Size = new System.Drawing.Size(66, 24);
            this.checkEdit162.StyleController = this.layoutControl1;
            this.checkEdit162.TabIndex = 174;
            this.checkEdit162.TabStop = false;
            this.checkEdit162.Tag = "2";
            this.checkEdit162.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit161
            // 
            this.checkEdit161.Location = new System.Drawing.Point(393, 1245);
            this.checkEdit161.Name = "checkEdit161";
            this.checkEdit161.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit161.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit161.Properties.AutoHeight = false;
            this.checkEdit161.Properties.Caption = "1";
            this.checkEdit161.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit161.Properties.RadioGroupIndex = 33;
            this.checkEdit161.Size = new System.Drawing.Size(66, 24);
            this.checkEdit161.StyleController = this.layoutControl1;
            this.checkEdit161.TabIndex = 173;
            this.checkEdit161.TabStop = false;
            this.checkEdit161.Tag = "1";
            this.checkEdit161.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit160
            // 
            this.checkEdit160.Location = new System.Drawing.Point(673, 1205);
            this.checkEdit160.Name = "checkEdit160";
            this.checkEdit160.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit160.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit160.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit160.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit160.Properties.AutoHeight = false;
            this.checkEdit160.Properties.Caption = "5                                                           ";
            this.checkEdit160.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit160.Properties.RadioGroupIndex = 32;
            this.checkEdit160.Size = new System.Drawing.Size(106, 36);
            this.checkEdit160.StyleController = this.layoutControl1;
            this.checkEdit160.TabIndex = 172;
            this.checkEdit160.TabStop = false;
            this.checkEdit160.Tag = "5";
            this.checkEdit160.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit159
            // 
            this.checkEdit159.Location = new System.Drawing.Point(603, 1205);
            this.checkEdit159.Name = "checkEdit159";
            this.checkEdit159.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit159.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit159.Properties.AutoHeight = false;
            this.checkEdit159.Properties.Caption = "4";
            this.checkEdit159.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit159.Properties.RadioGroupIndex = 32;
            this.checkEdit159.Size = new System.Drawing.Size(66, 36);
            this.checkEdit159.StyleController = this.layoutControl1;
            this.checkEdit159.TabIndex = 171;
            this.checkEdit159.TabStop = false;
            this.checkEdit159.Tag = "4";
            this.checkEdit159.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit158
            // 
            this.checkEdit158.Location = new System.Drawing.Point(533, 1205);
            this.checkEdit158.Name = "checkEdit158";
            this.checkEdit158.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit158.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit158.Properties.AutoHeight = false;
            this.checkEdit158.Properties.Caption = "3";
            this.checkEdit158.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit158.Properties.RadioGroupIndex = 32;
            this.checkEdit158.Size = new System.Drawing.Size(66, 36);
            this.checkEdit158.StyleController = this.layoutControl1;
            this.checkEdit158.TabIndex = 170;
            this.checkEdit158.TabStop = false;
            this.checkEdit158.Tag = "3";
            this.checkEdit158.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit157
            // 
            this.checkEdit157.Location = new System.Drawing.Point(463, 1205);
            this.checkEdit157.Name = "checkEdit157";
            this.checkEdit157.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit157.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit157.Properties.AutoHeight = false;
            this.checkEdit157.Properties.Caption = "2";
            this.checkEdit157.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit157.Properties.RadioGroupIndex = 32;
            this.checkEdit157.Size = new System.Drawing.Size(66, 36);
            this.checkEdit157.StyleController = this.layoutControl1;
            this.checkEdit157.TabIndex = 169;
            this.checkEdit157.TabStop = false;
            this.checkEdit157.Tag = "2";
            this.checkEdit157.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit156
            // 
            this.checkEdit156.Location = new System.Drawing.Point(393, 1205);
            this.checkEdit156.Name = "checkEdit156";
            this.checkEdit156.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit156.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit156.Properties.AutoHeight = false;
            this.checkEdit156.Properties.Caption = "1";
            this.checkEdit156.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit156.Properties.RadioGroupIndex = 32;
            this.checkEdit156.Size = new System.Drawing.Size(66, 36);
            this.checkEdit156.StyleController = this.layoutControl1;
            this.checkEdit156.TabIndex = 168;
            this.checkEdit156.TabStop = false;
            this.checkEdit156.Tag = "1";
            this.checkEdit156.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit155
            // 
            this.checkEdit155.Location = new System.Drawing.Point(673, 1177);
            this.checkEdit155.Name = "checkEdit155";
            this.checkEdit155.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit155.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit155.Properties.AutoHeight = false;
            this.checkEdit155.Properties.Caption = "5";
            this.checkEdit155.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit155.Properties.RadioGroupIndex = 31;
            this.checkEdit155.Size = new System.Drawing.Size(106, 24);
            this.checkEdit155.StyleController = this.layoutControl1;
            this.checkEdit155.TabIndex = 167;
            this.checkEdit155.TabStop = false;
            this.checkEdit155.Tag = "5";
            this.checkEdit155.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit154
            // 
            this.checkEdit154.Location = new System.Drawing.Point(603, 1177);
            this.checkEdit154.Name = "checkEdit154";
            this.checkEdit154.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit154.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit154.Properties.AutoHeight = false;
            this.checkEdit154.Properties.Caption = "4";
            this.checkEdit154.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit154.Properties.RadioGroupIndex = 31;
            this.checkEdit154.Size = new System.Drawing.Size(66, 24);
            this.checkEdit154.StyleController = this.layoutControl1;
            this.checkEdit154.TabIndex = 166;
            this.checkEdit154.TabStop = false;
            this.checkEdit154.Tag = "4";
            this.checkEdit154.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit153
            // 
            this.checkEdit153.Location = new System.Drawing.Point(533, 1177);
            this.checkEdit153.Name = "checkEdit153";
            this.checkEdit153.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit153.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit153.Properties.AutoHeight = false;
            this.checkEdit153.Properties.Caption = "3";
            this.checkEdit153.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit153.Properties.RadioGroupIndex = 31;
            this.checkEdit153.Size = new System.Drawing.Size(66, 24);
            this.checkEdit153.StyleController = this.layoutControl1;
            this.checkEdit153.TabIndex = 165;
            this.checkEdit153.TabStop = false;
            this.checkEdit153.Tag = "3";
            this.checkEdit153.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit152
            // 
            this.checkEdit152.Location = new System.Drawing.Point(463, 1177);
            this.checkEdit152.Name = "checkEdit152";
            this.checkEdit152.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit152.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit152.Properties.AutoHeight = false;
            this.checkEdit152.Properties.Caption = "2";
            this.checkEdit152.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit152.Properties.RadioGroupIndex = 31;
            this.checkEdit152.Size = new System.Drawing.Size(66, 24);
            this.checkEdit152.StyleController = this.layoutControl1;
            this.checkEdit152.TabIndex = 164;
            this.checkEdit152.TabStop = false;
            this.checkEdit152.Tag = "2";
            this.checkEdit152.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit151
            // 
            this.checkEdit151.Location = new System.Drawing.Point(393, 1177);
            this.checkEdit151.Name = "checkEdit151";
            this.checkEdit151.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit151.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit151.Properties.AutoHeight = false;
            this.checkEdit151.Properties.Caption = "1";
            this.checkEdit151.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit151.Properties.RadioGroupIndex = 31;
            this.checkEdit151.Size = new System.Drawing.Size(66, 24);
            this.checkEdit151.StyleController = this.layoutControl1;
            this.checkEdit151.TabIndex = 163;
            this.checkEdit151.TabStop = false;
            this.checkEdit151.Tag = "1";
            this.checkEdit151.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit150
            // 
            this.checkEdit150.Location = new System.Drawing.Point(673, 1137);
            this.checkEdit150.Name = "checkEdit150";
            this.checkEdit150.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit150.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit150.Properties.AutoHeight = false;
            this.checkEdit150.Properties.Caption = "5";
            this.checkEdit150.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit150.Properties.RadioGroupIndex = 30;
            this.checkEdit150.Size = new System.Drawing.Size(106, 36);
            this.checkEdit150.StyleController = this.layoutControl1;
            this.checkEdit150.TabIndex = 162;
            this.checkEdit150.TabStop = false;
            this.checkEdit150.Tag = "5";
            this.checkEdit150.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit149
            // 
            this.checkEdit149.Location = new System.Drawing.Point(603, 1137);
            this.checkEdit149.Name = "checkEdit149";
            this.checkEdit149.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit149.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit149.Properties.AutoHeight = false;
            this.checkEdit149.Properties.Caption = "4";
            this.checkEdit149.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit149.Properties.RadioGroupIndex = 30;
            this.checkEdit149.Size = new System.Drawing.Size(66, 36);
            this.checkEdit149.StyleController = this.layoutControl1;
            this.checkEdit149.TabIndex = 161;
            this.checkEdit149.TabStop = false;
            this.checkEdit149.Tag = "4";
            this.checkEdit149.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit148
            // 
            this.checkEdit148.Location = new System.Drawing.Point(533, 1137);
            this.checkEdit148.Name = "checkEdit148";
            this.checkEdit148.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit148.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit148.Properties.AutoHeight = false;
            this.checkEdit148.Properties.Caption = "3";
            this.checkEdit148.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit148.Properties.RadioGroupIndex = 30;
            this.checkEdit148.Size = new System.Drawing.Size(66, 36);
            this.checkEdit148.StyleController = this.layoutControl1;
            this.checkEdit148.TabIndex = 160;
            this.checkEdit148.TabStop = false;
            this.checkEdit148.Tag = "3";
            this.checkEdit148.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit147
            // 
            this.checkEdit147.Location = new System.Drawing.Point(463, 1137);
            this.checkEdit147.Name = "checkEdit147";
            this.checkEdit147.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit147.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit147.Properties.AutoHeight = false;
            this.checkEdit147.Properties.Caption = "2";
            this.checkEdit147.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit147.Properties.RadioGroupIndex = 30;
            this.checkEdit147.Size = new System.Drawing.Size(66, 36);
            this.checkEdit147.StyleController = this.layoutControl1;
            this.checkEdit147.TabIndex = 159;
            this.checkEdit147.TabStop = false;
            this.checkEdit147.Tag = "2";
            this.checkEdit147.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit146
            // 
            this.checkEdit146.Location = new System.Drawing.Point(393, 1137);
            this.checkEdit146.Name = "checkEdit146";
            this.checkEdit146.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit146.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit146.Properties.AutoHeight = false;
            this.checkEdit146.Properties.Caption = "1";
            this.checkEdit146.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit146.Properties.RadioGroupIndex = 30;
            this.checkEdit146.Size = new System.Drawing.Size(66, 36);
            this.checkEdit146.StyleController = this.layoutControl1;
            this.checkEdit146.TabIndex = 158;
            this.checkEdit146.TabStop = false;
            this.checkEdit146.Tag = "1";
            this.checkEdit146.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit145
            // 
            this.checkEdit145.Location = new System.Drawing.Point(673, 1097);
            this.checkEdit145.Name = "checkEdit145";
            this.checkEdit145.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit145.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit145.Properties.AutoHeight = false;
            this.checkEdit145.Properties.Caption = "5";
            this.checkEdit145.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit145.Properties.RadioGroupIndex = 29;
            this.checkEdit145.Size = new System.Drawing.Size(106, 36);
            this.checkEdit145.StyleController = this.layoutControl1;
            this.checkEdit145.TabIndex = 157;
            this.checkEdit145.TabStop = false;
            this.checkEdit145.Tag = "5";
            this.checkEdit145.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit144
            // 
            this.checkEdit144.Location = new System.Drawing.Point(603, 1097);
            this.checkEdit144.Name = "checkEdit144";
            this.checkEdit144.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit144.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit144.Properties.AutoHeight = false;
            this.checkEdit144.Properties.Caption = "4";
            this.checkEdit144.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit144.Properties.RadioGroupIndex = 29;
            this.checkEdit144.Size = new System.Drawing.Size(66, 36);
            this.checkEdit144.StyleController = this.layoutControl1;
            this.checkEdit144.TabIndex = 156;
            this.checkEdit144.TabStop = false;
            this.checkEdit144.Tag = "4";
            this.checkEdit144.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit143
            // 
            this.checkEdit143.Location = new System.Drawing.Point(533, 1097);
            this.checkEdit143.Name = "checkEdit143";
            this.checkEdit143.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit143.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit143.Properties.AutoHeight = false;
            this.checkEdit143.Properties.Caption = "3";
            this.checkEdit143.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit143.Properties.RadioGroupIndex = 29;
            this.checkEdit143.Size = new System.Drawing.Size(66, 36);
            this.checkEdit143.StyleController = this.layoutControl1;
            this.checkEdit143.TabIndex = 155;
            this.checkEdit143.TabStop = false;
            this.checkEdit143.Tag = "3";
            this.checkEdit143.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit142
            // 
            this.checkEdit142.Location = new System.Drawing.Point(463, 1097);
            this.checkEdit142.Name = "checkEdit142";
            this.checkEdit142.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit142.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit142.Properties.AutoHeight = false;
            this.checkEdit142.Properties.Caption = "2";
            this.checkEdit142.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit142.Properties.RadioGroupIndex = 29;
            this.checkEdit142.Size = new System.Drawing.Size(66, 36);
            this.checkEdit142.StyleController = this.layoutControl1;
            this.checkEdit142.TabIndex = 154;
            this.checkEdit142.TabStop = false;
            this.checkEdit142.Tag = "2";
            this.checkEdit142.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit141
            // 
            this.checkEdit141.Location = new System.Drawing.Point(393, 1097);
            this.checkEdit141.Name = "checkEdit141";
            this.checkEdit141.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit141.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit141.Properties.AutoHeight = false;
            this.checkEdit141.Properties.Caption = "1";
            this.checkEdit141.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit141.Properties.RadioGroupIndex = 29;
            this.checkEdit141.Size = new System.Drawing.Size(66, 36);
            this.checkEdit141.StyleController = this.layoutControl1;
            this.checkEdit141.TabIndex = 153;
            this.checkEdit141.TabStop = false;
            this.checkEdit141.Tag = "1";
            this.checkEdit141.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit140
            // 
            this.checkEdit140.Location = new System.Drawing.Point(673, 1019);
            this.checkEdit140.Name = "checkEdit140";
            this.checkEdit140.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit140.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit140.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit140.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit140.Properties.AutoHeight = false;
            this.checkEdit140.Properties.Caption = "5（腹围>105cm或3.15尺）";
            this.checkEdit140.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit140.Properties.RadioGroupIndex = 28;
            this.checkEdit140.Size = new System.Drawing.Size(106, 74);
            this.checkEdit140.StyleController = this.layoutControl1;
            this.checkEdit140.TabIndex = 152;
            this.checkEdit140.TabStop = false;
            this.checkEdit140.Tag = "5";
            this.checkEdit140.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit139
            // 
            this.checkEdit139.Location = new System.Drawing.Point(603, 1019);
            this.checkEdit139.Name = "checkEdit139";
            this.checkEdit139.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit139.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit139.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit139.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit139.Properties.AutoHeight = false;
            this.checkEdit139.Properties.Caption = "4(腹围91-105cm，2.71-3.15尺)";
            this.checkEdit139.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit139.Properties.RadioGroupIndex = 28;
            this.checkEdit139.Size = new System.Drawing.Size(66, 74);
            this.checkEdit139.StyleController = this.layoutControl1;
            this.checkEdit139.TabIndex = 151;
            this.checkEdit139.TabStop = false;
            this.checkEdit139.Tag = "4";
            this.checkEdit139.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit138
            // 
            this.checkEdit138.Location = new System.Drawing.Point(533, 1019);
            this.checkEdit138.Name = "checkEdit138";
            this.checkEdit138.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit138.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit138.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit138.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit138.Properties.AutoHeight = false;
            this.checkEdit138.Properties.Caption = "3(腹围86-90cm，2.56-2.7尺)";
            this.checkEdit138.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit138.Properties.RadioGroupIndex = 28;
            this.checkEdit138.Size = new System.Drawing.Size(66, 74);
            this.checkEdit138.StyleController = this.layoutControl1;
            this.checkEdit138.TabIndex = 150;
            this.checkEdit138.TabStop = false;
            this.checkEdit138.Tag = "3";
            this.checkEdit138.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit137
            // 
            this.checkEdit137.Location = new System.Drawing.Point(463, 1019);
            this.checkEdit137.Name = "checkEdit137";
            this.checkEdit137.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit137.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit137.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit137.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit137.Properties.AutoHeight = false;
            this.checkEdit137.Properties.Caption = "2(腹围80-85cm，2.4-2.55尺)";
            this.checkEdit137.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit137.Properties.RadioGroupIndex = 28;
            this.checkEdit137.Size = new System.Drawing.Size(66, 74);
            this.checkEdit137.StyleController = this.layoutControl1;
            this.checkEdit137.TabIndex = 149;
            this.checkEdit137.TabStop = false;
            this.checkEdit137.Tag = "2";
            this.checkEdit137.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit136
            // 
            this.checkEdit136.Location = new System.Drawing.Point(393, 1019);
            this.checkEdit136.Name = "checkEdit136";
            this.checkEdit136.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit136.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit136.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit136.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit136.Properties.AutoHeight = false;
            this.checkEdit136.Properties.Caption = "1（腹围<80cm，相当于2.4尺）";
            this.checkEdit136.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit136.Properties.RadioGroupIndex = 28;
            this.checkEdit136.Size = new System.Drawing.Size(66, 74);
            this.checkEdit136.StyleController = this.layoutControl1;
            this.checkEdit136.TabIndex = 148;
            this.checkEdit136.TabStop = false;
            this.checkEdit136.Tag = "1";
            this.checkEdit136.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit135
            // 
            this.checkEdit135.Location = new System.Drawing.Point(673, 991);
            this.checkEdit135.Name = "checkEdit135";
            this.checkEdit135.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit135.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit135.Properties.AutoHeight = false;
            this.checkEdit135.Properties.Caption = "5";
            this.checkEdit135.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit135.Properties.RadioGroupIndex = 27;
            this.checkEdit135.Size = new System.Drawing.Size(106, 24);
            this.checkEdit135.StyleController = this.layoutControl1;
            this.checkEdit135.TabIndex = 147;
            this.checkEdit135.TabStop = false;
            this.checkEdit135.Tag = "5";
            this.checkEdit135.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit134
            // 
            this.checkEdit134.Location = new System.Drawing.Point(603, 991);
            this.checkEdit134.Name = "checkEdit134";
            this.checkEdit134.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit134.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit134.Properties.AutoHeight = false;
            this.checkEdit134.Properties.Caption = "4";
            this.checkEdit134.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit134.Properties.RadioGroupIndex = 27;
            this.checkEdit134.Size = new System.Drawing.Size(66, 24);
            this.checkEdit134.StyleController = this.layoutControl1;
            this.checkEdit134.TabIndex = 146;
            this.checkEdit134.TabStop = false;
            this.checkEdit134.Tag = "4";
            this.checkEdit134.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit133
            // 
            this.checkEdit133.Location = new System.Drawing.Point(533, 991);
            this.checkEdit133.Name = "checkEdit133";
            this.checkEdit133.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit133.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit133.Properties.AutoHeight = false;
            this.checkEdit133.Properties.Caption = "3";
            this.checkEdit133.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit133.Properties.RadioGroupIndex = 27;
            this.checkEdit133.Size = new System.Drawing.Size(66, 24);
            this.checkEdit133.StyleController = this.layoutControl1;
            this.checkEdit133.TabIndex = 145;
            this.checkEdit133.TabStop = false;
            this.checkEdit133.Tag = "3";
            this.checkEdit133.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit132
            // 
            this.checkEdit132.Location = new System.Drawing.Point(463, 991);
            this.checkEdit132.Name = "checkEdit132";
            this.checkEdit132.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit132.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit132.Properties.AutoHeight = false;
            this.checkEdit132.Properties.Caption = "2";
            this.checkEdit132.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit132.Properties.RadioGroupIndex = 27;
            this.checkEdit132.Size = new System.Drawing.Size(66, 24);
            this.checkEdit132.StyleController = this.layoutControl1;
            this.checkEdit132.TabIndex = 144;
            this.checkEdit132.TabStop = false;
            this.checkEdit132.Tag = "2";
            this.checkEdit132.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit131
            // 
            this.checkEdit131.Location = new System.Drawing.Point(393, 991);
            this.checkEdit131.Name = "checkEdit131";
            this.checkEdit131.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit131.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit131.Properties.AutoHeight = false;
            this.checkEdit131.Properties.Caption = "1";
            this.checkEdit131.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit131.Properties.RadioGroupIndex = 27;
            this.checkEdit131.Size = new System.Drawing.Size(66, 24);
            this.checkEdit131.StyleController = this.layoutControl1;
            this.checkEdit131.TabIndex = 143;
            this.checkEdit131.TabStop = false;
            this.checkEdit131.Tag = "1";
            this.checkEdit131.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit130
            // 
            this.checkEdit130.Location = new System.Drawing.Point(673, 963);
            this.checkEdit130.Name = "checkEdit130";
            this.checkEdit130.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit130.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit130.Properties.AutoHeight = false;
            this.checkEdit130.Properties.Caption = "5";
            this.checkEdit130.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit130.Properties.RadioGroupIndex = 26;
            this.checkEdit130.Size = new System.Drawing.Size(106, 24);
            this.checkEdit130.StyleController = this.layoutControl1;
            this.checkEdit130.TabIndex = 142;
            this.checkEdit130.TabStop = false;
            this.checkEdit130.Tag = "5";
            this.checkEdit130.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit129
            // 
            this.checkEdit129.Location = new System.Drawing.Point(603, 963);
            this.checkEdit129.Name = "checkEdit129";
            this.checkEdit129.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit129.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit129.Properties.AutoHeight = false;
            this.checkEdit129.Properties.Caption = "4";
            this.checkEdit129.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit129.Properties.RadioGroupIndex = 26;
            this.checkEdit129.Size = new System.Drawing.Size(66, 24);
            this.checkEdit129.StyleController = this.layoutControl1;
            this.checkEdit129.TabIndex = 141;
            this.checkEdit129.TabStop = false;
            this.checkEdit129.Tag = "4";
            this.checkEdit129.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit128
            // 
            this.checkEdit128.Location = new System.Drawing.Point(533, 963);
            this.checkEdit128.Name = "checkEdit128";
            this.checkEdit128.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit128.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit128.Properties.AutoHeight = false;
            this.checkEdit128.Properties.Caption = "3";
            this.checkEdit128.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit128.Properties.RadioGroupIndex = 26;
            this.checkEdit128.Size = new System.Drawing.Size(66, 24);
            this.checkEdit128.StyleController = this.layoutControl1;
            this.checkEdit128.TabIndex = 140;
            this.checkEdit128.TabStop = false;
            this.checkEdit128.Tag = "3";
            this.checkEdit128.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit127
            // 
            this.checkEdit127.Location = new System.Drawing.Point(463, 963);
            this.checkEdit127.Name = "checkEdit127";
            this.checkEdit127.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit127.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit127.Properties.AutoHeight = false;
            this.checkEdit127.Properties.Caption = "2";
            this.checkEdit127.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit127.Properties.RadioGroupIndex = 26;
            this.checkEdit127.Size = new System.Drawing.Size(66, 24);
            this.checkEdit127.StyleController = this.layoutControl1;
            this.checkEdit127.TabIndex = 139;
            this.checkEdit127.TabStop = false;
            this.checkEdit127.Tag = "2";
            this.checkEdit127.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit126
            // 
            this.checkEdit126.Location = new System.Drawing.Point(393, 963);
            this.checkEdit126.Name = "checkEdit126";
            this.checkEdit126.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit126.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit126.Properties.AutoHeight = false;
            this.checkEdit126.Properties.Caption = "1";
            this.checkEdit126.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit126.Properties.RadioGroupIndex = 26;
            this.checkEdit126.Size = new System.Drawing.Size(66, 24);
            this.checkEdit126.StyleController = this.layoutControl1;
            this.checkEdit126.TabIndex = 138;
            this.checkEdit126.TabStop = false;
            this.checkEdit126.Tag = "1";
            this.checkEdit126.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit125
            // 
            this.checkEdit125.Location = new System.Drawing.Point(673, 935);
            this.checkEdit125.Name = "checkEdit125";
            this.checkEdit125.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit125.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit125.Properties.AutoHeight = false;
            this.checkEdit125.Properties.Caption = "5";
            this.checkEdit125.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit125.Properties.RadioGroupIndex = 25;
            this.checkEdit125.Size = new System.Drawing.Size(106, 24);
            this.checkEdit125.StyleController = this.layoutControl1;
            this.checkEdit125.TabIndex = 137;
            this.checkEdit125.TabStop = false;
            this.checkEdit125.Tag = "5";
            this.checkEdit125.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit124
            // 
            this.checkEdit124.Location = new System.Drawing.Point(603, 935);
            this.checkEdit124.Name = "checkEdit124";
            this.checkEdit124.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit124.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit124.Properties.AutoHeight = false;
            this.checkEdit124.Properties.Caption = "4";
            this.checkEdit124.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit124.Properties.RadioGroupIndex = 25;
            this.checkEdit124.Size = new System.Drawing.Size(66, 24);
            this.checkEdit124.StyleController = this.layoutControl1;
            this.checkEdit124.TabIndex = 136;
            this.checkEdit124.TabStop = false;
            this.checkEdit124.Tag = "4";
            this.checkEdit124.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit123
            // 
            this.checkEdit123.Location = new System.Drawing.Point(533, 935);
            this.checkEdit123.Name = "checkEdit123";
            this.checkEdit123.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit123.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit123.Properties.AutoHeight = false;
            this.checkEdit123.Properties.Caption = "3";
            this.checkEdit123.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit123.Properties.RadioGroupIndex = 25;
            this.checkEdit123.Size = new System.Drawing.Size(66, 24);
            this.checkEdit123.StyleController = this.layoutControl1;
            this.checkEdit123.TabIndex = 135;
            this.checkEdit123.TabStop = false;
            this.checkEdit123.Tag = "3";
            this.checkEdit123.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit122
            // 
            this.checkEdit122.Location = new System.Drawing.Point(463, 935);
            this.checkEdit122.Name = "checkEdit122";
            this.checkEdit122.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit122.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit122.Properties.AutoHeight = false;
            this.checkEdit122.Properties.Caption = "2";
            this.checkEdit122.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit122.Properties.RadioGroupIndex = 25;
            this.checkEdit122.Size = new System.Drawing.Size(66, 24);
            this.checkEdit122.StyleController = this.layoutControl1;
            this.checkEdit122.TabIndex = 134;
            this.checkEdit122.TabStop = false;
            this.checkEdit122.Tag = "2";
            this.checkEdit122.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit121
            // 
            this.checkEdit121.Location = new System.Drawing.Point(393, 935);
            this.checkEdit121.Name = "checkEdit121";
            this.checkEdit121.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit121.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit121.Properties.AutoHeight = false;
            this.checkEdit121.Properties.Caption = "1";
            this.checkEdit121.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit121.Properties.RadioGroupIndex = 25;
            this.checkEdit121.Size = new System.Drawing.Size(66, 24);
            this.checkEdit121.StyleController = this.layoutControl1;
            this.checkEdit121.TabIndex = 133;
            this.checkEdit121.TabStop = false;
            this.checkEdit121.Tag = "1";
            this.checkEdit121.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit120
            // 
            this.checkEdit120.Location = new System.Drawing.Point(673, 907);
            this.checkEdit120.Name = "checkEdit120";
            this.checkEdit120.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit120.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit120.Properties.AutoHeight = false;
            this.checkEdit120.Properties.Caption = "5";
            this.checkEdit120.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit120.Properties.RadioGroupIndex = 24;
            this.checkEdit120.Size = new System.Drawing.Size(106, 24);
            this.checkEdit120.StyleController = this.layoutControl1;
            this.checkEdit120.TabIndex = 132;
            this.checkEdit120.TabStop = false;
            this.checkEdit120.Tag = "5";
            this.checkEdit120.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit119
            // 
            this.checkEdit119.Location = new System.Drawing.Point(603, 907);
            this.checkEdit119.Name = "checkEdit119";
            this.checkEdit119.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit119.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit119.Properties.AutoHeight = false;
            this.checkEdit119.Properties.Caption = "4";
            this.checkEdit119.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit119.Properties.RadioGroupIndex = 24;
            this.checkEdit119.Size = new System.Drawing.Size(66, 24);
            this.checkEdit119.StyleController = this.layoutControl1;
            this.checkEdit119.TabIndex = 131;
            this.checkEdit119.TabStop = false;
            this.checkEdit119.Tag = "4";
            this.checkEdit119.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit118
            // 
            this.checkEdit118.Location = new System.Drawing.Point(533, 907);
            this.checkEdit118.Name = "checkEdit118";
            this.checkEdit118.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit118.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit118.Properties.AutoHeight = false;
            this.checkEdit118.Properties.Caption = "3";
            this.checkEdit118.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit118.Properties.RadioGroupIndex = 24;
            this.checkEdit118.Size = new System.Drawing.Size(66, 24);
            this.checkEdit118.StyleController = this.layoutControl1;
            this.checkEdit118.TabIndex = 130;
            this.checkEdit118.TabStop = false;
            this.checkEdit118.Tag = "3";
            this.checkEdit118.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit117
            // 
            this.checkEdit117.Location = new System.Drawing.Point(463, 907);
            this.checkEdit117.Name = "checkEdit117";
            this.checkEdit117.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit117.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit117.Properties.AutoHeight = false;
            this.checkEdit117.Properties.Caption = "2";
            this.checkEdit117.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit117.Properties.RadioGroupIndex = 24;
            this.checkEdit117.Size = new System.Drawing.Size(66, 24);
            this.checkEdit117.StyleController = this.layoutControl1;
            this.checkEdit117.TabIndex = 129;
            this.checkEdit117.TabStop = false;
            this.checkEdit117.Tag = "2";
            this.checkEdit117.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit116
            // 
            this.checkEdit116.Location = new System.Drawing.Point(393, 907);
            this.checkEdit116.Name = "checkEdit116";
            this.checkEdit116.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit116.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit116.Properties.AutoHeight = false;
            this.checkEdit116.Properties.Caption = "1";
            this.checkEdit116.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit116.Properties.RadioGroupIndex = 24;
            this.checkEdit116.Size = new System.Drawing.Size(66, 24);
            this.checkEdit116.StyleController = this.layoutControl1;
            this.checkEdit116.TabIndex = 128;
            this.checkEdit116.TabStop = false;
            this.checkEdit116.Tag = "1";
            this.checkEdit116.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit115
            // 
            this.checkEdit115.Location = new System.Drawing.Point(673, 879);
            this.checkEdit115.Name = "checkEdit115";
            this.checkEdit115.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit115.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit115.Properties.AutoHeight = false;
            this.checkEdit115.Properties.Caption = "5";
            this.checkEdit115.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit115.Properties.RadioGroupIndex = 23;
            this.checkEdit115.Size = new System.Drawing.Size(106, 24);
            this.checkEdit115.StyleController = this.layoutControl1;
            this.checkEdit115.TabIndex = 127;
            this.checkEdit115.TabStop = false;
            this.checkEdit115.Tag = "5";
            this.checkEdit115.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit114
            // 
            this.checkEdit114.Location = new System.Drawing.Point(603, 879);
            this.checkEdit114.Name = "checkEdit114";
            this.checkEdit114.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit114.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit114.Properties.AutoHeight = false;
            this.checkEdit114.Properties.Caption = "4";
            this.checkEdit114.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit114.Properties.RadioGroupIndex = 23;
            this.checkEdit114.Size = new System.Drawing.Size(66, 24);
            this.checkEdit114.StyleController = this.layoutControl1;
            this.checkEdit114.TabIndex = 126;
            this.checkEdit114.TabStop = false;
            this.checkEdit114.Tag = "4";
            this.checkEdit114.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit113
            // 
            this.checkEdit113.Location = new System.Drawing.Point(533, 879);
            this.checkEdit113.Name = "checkEdit113";
            this.checkEdit113.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit113.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit113.Properties.AutoHeight = false;
            this.checkEdit113.Properties.Caption = "3";
            this.checkEdit113.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit113.Properties.RadioGroupIndex = 23;
            this.checkEdit113.Size = new System.Drawing.Size(66, 24);
            this.checkEdit113.StyleController = this.layoutControl1;
            this.checkEdit113.TabIndex = 125;
            this.checkEdit113.TabStop = false;
            this.checkEdit113.Tag = "3";
            this.checkEdit113.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit112
            // 
            this.checkEdit112.Location = new System.Drawing.Point(463, 879);
            this.checkEdit112.Name = "checkEdit112";
            this.checkEdit112.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit112.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit112.Properties.AutoHeight = false;
            this.checkEdit112.Properties.Caption = "2";
            this.checkEdit112.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit112.Properties.RadioGroupIndex = 23;
            this.checkEdit112.Size = new System.Drawing.Size(66, 24);
            this.checkEdit112.StyleController = this.layoutControl1;
            this.checkEdit112.TabIndex = 124;
            this.checkEdit112.TabStop = false;
            this.checkEdit112.Tag = "2";
            this.checkEdit112.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit111
            // 
            this.checkEdit111.Location = new System.Drawing.Point(393, 879);
            this.checkEdit111.Name = "checkEdit111";
            this.checkEdit111.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit111.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit111.Properties.AutoHeight = false;
            this.checkEdit111.Properties.Caption = "1";
            this.checkEdit111.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit111.Properties.RadioGroupIndex = 23;
            this.checkEdit111.Size = new System.Drawing.Size(66, 24);
            this.checkEdit111.StyleController = this.layoutControl1;
            this.checkEdit111.TabIndex = 123;
            this.checkEdit111.TabStop = false;
            this.checkEdit111.Tag = "1";
            this.checkEdit111.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit110
            // 
            this.checkEdit110.Location = new System.Drawing.Point(673, 851);
            this.checkEdit110.Name = "checkEdit110";
            this.checkEdit110.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit110.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit110.Properties.AutoHeight = false;
            this.checkEdit110.Properties.Caption = "5";
            this.checkEdit110.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit110.Properties.RadioGroupIndex = 22;
            this.checkEdit110.Size = new System.Drawing.Size(106, 24);
            this.checkEdit110.StyleController = this.layoutControl1;
            this.checkEdit110.TabIndex = 122;
            this.checkEdit110.TabStop = false;
            this.checkEdit110.Tag = "5";
            this.checkEdit110.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit109
            // 
            this.checkEdit109.Location = new System.Drawing.Point(603, 851);
            this.checkEdit109.Name = "checkEdit109";
            this.checkEdit109.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit109.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit109.Properties.AutoHeight = false;
            this.checkEdit109.Properties.Caption = "4";
            this.checkEdit109.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit109.Properties.RadioGroupIndex = 22;
            this.checkEdit109.Size = new System.Drawing.Size(66, 24);
            this.checkEdit109.StyleController = this.layoutControl1;
            this.checkEdit109.TabIndex = 121;
            this.checkEdit109.TabStop = false;
            this.checkEdit109.Tag = "4";
            this.checkEdit109.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit108
            // 
            this.checkEdit108.Location = new System.Drawing.Point(533, 851);
            this.checkEdit108.Name = "checkEdit108";
            this.checkEdit108.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit108.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit108.Properties.AutoHeight = false;
            this.checkEdit108.Properties.Caption = "3";
            this.checkEdit108.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit108.Properties.RadioGroupIndex = 22;
            this.checkEdit108.Size = new System.Drawing.Size(66, 24);
            this.checkEdit108.StyleController = this.layoutControl1;
            this.checkEdit108.TabIndex = 120;
            this.checkEdit108.TabStop = false;
            this.checkEdit108.Tag = "3";
            this.checkEdit108.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit107
            // 
            this.checkEdit107.Location = new System.Drawing.Point(463, 851);
            this.checkEdit107.Name = "checkEdit107";
            this.checkEdit107.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit107.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit107.Properties.AutoHeight = false;
            this.checkEdit107.Properties.Caption = "2";
            this.checkEdit107.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit107.Properties.RadioGroupIndex = 22;
            this.checkEdit107.Size = new System.Drawing.Size(66, 24);
            this.checkEdit107.StyleController = this.layoutControl1;
            this.checkEdit107.TabIndex = 119;
            this.checkEdit107.TabStop = false;
            this.checkEdit107.Tag = "2";
            this.checkEdit107.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit106
            // 
            this.checkEdit106.Location = new System.Drawing.Point(393, 851);
            this.checkEdit106.Name = "checkEdit106";
            this.checkEdit106.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit106.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit106.Properties.AutoHeight = false;
            this.checkEdit106.Properties.Caption = "1";
            this.checkEdit106.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit106.Properties.RadioGroupIndex = 22;
            this.checkEdit106.Size = new System.Drawing.Size(66, 24);
            this.checkEdit106.StyleController = this.layoutControl1;
            this.checkEdit106.TabIndex = 118;
            this.checkEdit106.TabStop = false;
            this.checkEdit106.Tag = "1";
            this.checkEdit106.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit105
            // 
            this.checkEdit105.Location = new System.Drawing.Point(673, 823);
            this.checkEdit105.Name = "checkEdit105";
            this.checkEdit105.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit105.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit105.Properties.AutoHeight = false;
            this.checkEdit105.Properties.Caption = "5";
            this.checkEdit105.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit105.Properties.RadioGroupIndex = 21;
            this.checkEdit105.Size = new System.Drawing.Size(106, 24);
            this.checkEdit105.StyleController = this.layoutControl1;
            this.checkEdit105.TabIndex = 117;
            this.checkEdit105.TabStop = false;
            this.checkEdit105.Tag = "5";
            this.checkEdit105.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit104
            // 
            this.checkEdit104.Location = new System.Drawing.Point(603, 823);
            this.checkEdit104.Name = "checkEdit104";
            this.checkEdit104.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit104.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit104.Properties.AutoHeight = false;
            this.checkEdit104.Properties.Caption = "4";
            this.checkEdit104.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit104.Properties.RadioGroupIndex = 21;
            this.checkEdit104.Size = new System.Drawing.Size(66, 24);
            this.checkEdit104.StyleController = this.layoutControl1;
            this.checkEdit104.TabIndex = 116;
            this.checkEdit104.TabStop = false;
            this.checkEdit104.Tag = "4";
            this.checkEdit104.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit103
            // 
            this.checkEdit103.Location = new System.Drawing.Point(533, 823);
            this.checkEdit103.Name = "checkEdit103";
            this.checkEdit103.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit103.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit103.Properties.AutoHeight = false;
            this.checkEdit103.Properties.Caption = "3";
            this.checkEdit103.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit103.Properties.RadioGroupIndex = 21;
            this.checkEdit103.Size = new System.Drawing.Size(66, 24);
            this.checkEdit103.StyleController = this.layoutControl1;
            this.checkEdit103.TabIndex = 115;
            this.checkEdit103.TabStop = false;
            this.checkEdit103.Tag = "3";
            this.checkEdit103.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit102
            // 
            this.checkEdit102.Location = new System.Drawing.Point(463, 823);
            this.checkEdit102.Name = "checkEdit102";
            this.checkEdit102.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit102.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit102.Properties.AutoHeight = false;
            this.checkEdit102.Properties.Caption = "2";
            this.checkEdit102.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit102.Properties.RadioGroupIndex = 21;
            this.checkEdit102.Size = new System.Drawing.Size(66, 24);
            this.checkEdit102.StyleController = this.layoutControl1;
            this.checkEdit102.TabIndex = 114;
            this.checkEdit102.TabStop = false;
            this.checkEdit102.Tag = "2";
            this.checkEdit102.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit101
            // 
            this.checkEdit101.Location = new System.Drawing.Point(393, 823);
            this.checkEdit101.Name = "checkEdit101";
            this.checkEdit101.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit101.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit101.Properties.AutoHeight = false;
            this.checkEdit101.Properties.Caption = "1";
            this.checkEdit101.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit101.Properties.RadioGroupIndex = 21;
            this.checkEdit101.Size = new System.Drawing.Size(66, 24);
            this.checkEdit101.StyleController = this.layoutControl1;
            this.checkEdit101.TabIndex = 113;
            this.checkEdit101.TabStop = false;
            this.checkEdit101.Tag = "1";
            this.checkEdit101.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit100
            // 
            this.checkEdit100.Location = new System.Drawing.Point(673, 783);
            this.checkEdit100.Name = "checkEdit100";
            this.checkEdit100.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit100.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit100.Properties.AutoHeight = false;
            this.checkEdit100.Properties.Caption = "5";
            this.checkEdit100.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit100.Properties.RadioGroupIndex = 20;
            this.checkEdit100.Size = new System.Drawing.Size(106, 36);
            this.checkEdit100.StyleController = this.layoutControl1;
            this.checkEdit100.TabIndex = 112;
            this.checkEdit100.TabStop = false;
            this.checkEdit100.Tag = "5";
            this.checkEdit100.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit99
            // 
            this.checkEdit99.Location = new System.Drawing.Point(603, 783);
            this.checkEdit99.Name = "checkEdit99";
            this.checkEdit99.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit99.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit99.Properties.AutoHeight = false;
            this.checkEdit99.Properties.Caption = "4";
            this.checkEdit99.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit99.Properties.RadioGroupIndex = 20;
            this.checkEdit99.Size = new System.Drawing.Size(66, 36);
            this.checkEdit99.StyleController = this.layoutControl1;
            this.checkEdit99.TabIndex = 111;
            this.checkEdit99.TabStop = false;
            this.checkEdit99.Tag = "4";
            this.checkEdit99.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit98
            // 
            this.checkEdit98.Location = new System.Drawing.Point(533, 783);
            this.checkEdit98.Name = "checkEdit98";
            this.checkEdit98.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit98.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit98.Properties.AutoHeight = false;
            this.checkEdit98.Properties.Caption = "3";
            this.checkEdit98.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit98.Properties.RadioGroupIndex = 20;
            this.checkEdit98.Size = new System.Drawing.Size(66, 36);
            this.checkEdit98.StyleController = this.layoutControl1;
            this.checkEdit98.TabIndex = 110;
            this.checkEdit98.TabStop = false;
            this.checkEdit98.Tag = "3";
            this.checkEdit98.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit97
            // 
            this.checkEdit97.Location = new System.Drawing.Point(463, 783);
            this.checkEdit97.Name = "checkEdit97";
            this.checkEdit97.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit97.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit97.Properties.AutoHeight = false;
            this.checkEdit97.Properties.Caption = "2";
            this.checkEdit97.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit97.Properties.RadioGroupIndex = 20;
            this.checkEdit97.Size = new System.Drawing.Size(66, 36);
            this.checkEdit97.StyleController = this.layoutControl1;
            this.checkEdit97.TabIndex = 109;
            this.checkEdit97.TabStop = false;
            this.checkEdit97.Tag = "2";
            this.checkEdit97.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit96
            // 
            this.checkEdit96.Location = new System.Drawing.Point(393, 783);
            this.checkEdit96.Name = "checkEdit96";
            this.checkEdit96.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit96.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit96.Properties.AutoHeight = false;
            this.checkEdit96.Properties.Caption = "1";
            this.checkEdit96.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit96.Properties.RadioGroupIndex = 20;
            this.checkEdit96.Size = new System.Drawing.Size(66, 36);
            this.checkEdit96.StyleController = this.layoutControl1;
            this.checkEdit96.TabIndex = 108;
            this.checkEdit96.TabStop = false;
            this.checkEdit96.Tag = "1";
            this.checkEdit96.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit95
            // 
            this.checkEdit95.Location = new System.Drawing.Point(673, 743);
            this.checkEdit95.Name = "checkEdit95";
            this.checkEdit95.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit95.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit95.Properties.AutoHeight = false;
            this.checkEdit95.Properties.Caption = "5";
            this.checkEdit95.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit95.Properties.RadioGroupIndex = 19;
            this.checkEdit95.Size = new System.Drawing.Size(106, 36);
            this.checkEdit95.StyleController = this.layoutControl1;
            this.checkEdit95.TabIndex = 107;
            this.checkEdit95.TabStop = false;
            this.checkEdit95.Tag = "5";
            this.checkEdit95.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit94
            // 
            this.checkEdit94.Location = new System.Drawing.Point(603, 743);
            this.checkEdit94.Name = "checkEdit94";
            this.checkEdit94.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit94.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit94.Properties.AutoHeight = false;
            this.checkEdit94.Properties.Caption = "4";
            this.checkEdit94.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit94.Properties.RadioGroupIndex = 19;
            this.checkEdit94.Size = new System.Drawing.Size(66, 36);
            this.checkEdit94.StyleController = this.layoutControl1;
            this.checkEdit94.TabIndex = 106;
            this.checkEdit94.TabStop = false;
            this.checkEdit94.Tag = "4";
            this.checkEdit94.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit93
            // 
            this.checkEdit93.Location = new System.Drawing.Point(533, 743);
            this.checkEdit93.Name = "checkEdit93";
            this.checkEdit93.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit93.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit93.Properties.AutoHeight = false;
            this.checkEdit93.Properties.Caption = "3";
            this.checkEdit93.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit93.Properties.RadioGroupIndex = 19;
            this.checkEdit93.Size = new System.Drawing.Size(66, 36);
            this.checkEdit93.StyleController = this.layoutControl1;
            this.checkEdit93.TabIndex = 105;
            this.checkEdit93.TabStop = false;
            this.checkEdit93.Tag = "3";
            this.checkEdit93.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit92
            // 
            this.checkEdit92.Location = new System.Drawing.Point(463, 743);
            this.checkEdit92.Name = "checkEdit92";
            this.checkEdit92.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit92.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit92.Properties.AutoHeight = false;
            this.checkEdit92.Properties.Caption = "2";
            this.checkEdit92.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit92.Properties.RadioGroupIndex = 19;
            this.checkEdit92.Size = new System.Drawing.Size(66, 36);
            this.checkEdit92.StyleController = this.layoutControl1;
            this.checkEdit92.TabIndex = 104;
            this.checkEdit92.TabStop = false;
            this.checkEdit92.Tag = "2";
            this.checkEdit92.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit91
            // 
            this.checkEdit91.Location = new System.Drawing.Point(393, 743);
            this.checkEdit91.Name = "checkEdit91";
            this.checkEdit91.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit91.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit91.Properties.AutoHeight = false;
            this.checkEdit91.Properties.Caption = "1";
            this.checkEdit91.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit91.Properties.RadioGroupIndex = 19;
            this.checkEdit91.Size = new System.Drawing.Size(66, 36);
            this.checkEdit91.StyleController = this.layoutControl1;
            this.checkEdit91.TabIndex = 103;
            this.checkEdit91.TabStop = false;
            this.checkEdit91.Tag = "1";
            this.checkEdit91.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit90
            // 
            this.checkEdit90.Location = new System.Drawing.Point(673, 715);
            this.checkEdit90.Name = "checkEdit90";
            this.checkEdit90.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit90.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit90.Properties.AutoHeight = false;
            this.checkEdit90.Properties.Caption = "5";
            this.checkEdit90.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit90.Properties.RadioGroupIndex = 18;
            this.checkEdit90.Size = new System.Drawing.Size(106, 24);
            this.checkEdit90.StyleController = this.layoutControl1;
            this.checkEdit90.TabIndex = 102;
            this.checkEdit90.TabStop = false;
            this.checkEdit90.Tag = "5";
            this.checkEdit90.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit89
            // 
            this.checkEdit89.Location = new System.Drawing.Point(603, 715);
            this.checkEdit89.Name = "checkEdit89";
            this.checkEdit89.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit89.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit89.Properties.AutoHeight = false;
            this.checkEdit89.Properties.Caption = "4";
            this.checkEdit89.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit89.Properties.RadioGroupIndex = 18;
            this.checkEdit89.Size = new System.Drawing.Size(66, 24);
            this.checkEdit89.StyleController = this.layoutControl1;
            this.checkEdit89.TabIndex = 101;
            this.checkEdit89.TabStop = false;
            this.checkEdit89.Tag = "4";
            this.checkEdit89.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit88
            // 
            this.checkEdit88.Location = new System.Drawing.Point(533, 715);
            this.checkEdit88.Name = "checkEdit88";
            this.checkEdit88.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit88.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit88.Properties.AutoHeight = false;
            this.checkEdit88.Properties.Caption = "3";
            this.checkEdit88.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit88.Properties.RadioGroupIndex = 18;
            this.checkEdit88.Size = new System.Drawing.Size(66, 24);
            this.checkEdit88.StyleController = this.layoutControl1;
            this.checkEdit88.TabIndex = 100;
            this.checkEdit88.TabStop = false;
            this.checkEdit88.Tag = "3";
            this.checkEdit88.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit87
            // 
            this.checkEdit87.Location = new System.Drawing.Point(463, 715);
            this.checkEdit87.Name = "checkEdit87";
            this.checkEdit87.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit87.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit87.Properties.AutoHeight = false;
            this.checkEdit87.Properties.Caption = "2";
            this.checkEdit87.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit87.Properties.RadioGroupIndex = 18;
            this.checkEdit87.Size = new System.Drawing.Size(66, 24);
            this.checkEdit87.StyleController = this.layoutControl1;
            this.checkEdit87.TabIndex = 99;
            this.checkEdit87.TabStop = false;
            this.checkEdit87.Tag = "2";
            this.checkEdit87.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit86
            // 
            this.checkEdit86.Location = new System.Drawing.Point(393, 715);
            this.checkEdit86.Name = "checkEdit86";
            this.checkEdit86.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit86.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit86.Properties.AutoHeight = false;
            this.checkEdit86.Properties.Caption = "1";
            this.checkEdit86.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit86.Properties.RadioGroupIndex = 18;
            this.checkEdit86.Size = new System.Drawing.Size(66, 24);
            this.checkEdit86.StyleController = this.layoutControl1;
            this.checkEdit86.TabIndex = 98;
            this.checkEdit86.TabStop = false;
            this.checkEdit86.Tag = "1";
            this.checkEdit86.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit85
            // 
            this.checkEdit85.Location = new System.Drawing.Point(673, 675);
            this.checkEdit85.Name = "checkEdit85";
            this.checkEdit85.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit85.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit85.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit85.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit85.Properties.AutoHeight = false;
            this.checkEdit85.Properties.Caption = "5(每次遇到上述原因都过敏)";
            this.checkEdit85.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit85.Properties.RadioGroupIndex = 17;
            this.checkEdit85.Size = new System.Drawing.Size(106, 36);
            this.checkEdit85.StyleController = this.layoutControl1;
            this.checkEdit85.TabIndex = 97;
            this.checkEdit85.TabStop = false;
            this.checkEdit85.Tag = "5";
            this.checkEdit85.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit84
            // 
            this.checkEdit84.Location = new System.Drawing.Point(603, 675);
            this.checkEdit84.Name = "checkEdit84";
            this.checkEdit84.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit84.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit84.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit84.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit84.Properties.AutoHeight = false;
            this.checkEdit84.Properties.Caption = "4(一年5、6次)";
            this.checkEdit84.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit84.Properties.RadioGroupIndex = 17;
            this.checkEdit84.Size = new System.Drawing.Size(66, 36);
            this.checkEdit84.StyleController = this.layoutControl1;
            this.checkEdit84.TabIndex = 96;
            this.checkEdit84.TabStop = false;
            this.checkEdit84.Tag = "4";
            this.checkEdit84.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit83
            // 
            this.checkEdit83.Location = new System.Drawing.Point(533, 675);
            this.checkEdit83.Name = "checkEdit83";
            this.checkEdit83.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit83.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit83.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit83.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit83.Properties.AutoHeight = false;
            this.checkEdit83.Properties.Caption = "3(一年3、4次)";
            this.checkEdit83.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit83.Properties.RadioGroupIndex = 17;
            this.checkEdit83.Size = new System.Drawing.Size(66, 36);
            this.checkEdit83.StyleController = this.layoutControl1;
            this.checkEdit83.TabIndex = 95;
            this.checkEdit83.TabStop = false;
            this.checkEdit83.Tag = "3";
            this.checkEdit83.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit82
            // 
            this.checkEdit82.Location = new System.Drawing.Point(463, 675);
            this.checkEdit82.Name = "checkEdit82";
            this.checkEdit82.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit82.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit82.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit82.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit82.Properties.AutoHeight = false;
            this.checkEdit82.Properties.Caption = "2(一年1、2次)";
            this.checkEdit82.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit82.Properties.RadioGroupIndex = 17;
            this.checkEdit82.Size = new System.Drawing.Size(66, 36);
            this.checkEdit82.StyleController = this.layoutControl1;
            this.checkEdit82.TabIndex = 94;
            this.checkEdit82.TabStop = false;
            this.checkEdit82.Tag = "2";
            this.checkEdit82.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit81
            // 
            this.checkEdit81.Location = new System.Drawing.Point(393, 675);
            this.checkEdit81.Name = "checkEdit81";
            this.checkEdit81.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit81.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit81.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit81.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit81.Properties.AutoHeight = false;
            this.checkEdit81.Properties.Caption = "1(从来没有)";
            this.checkEdit81.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit81.Properties.RadioGroupIndex = 17;
            this.checkEdit81.Size = new System.Drawing.Size(66, 36);
            this.checkEdit81.StyleController = this.layoutControl1;
            this.checkEdit81.TabIndex = 93;
            this.checkEdit81.TabStop = false;
            this.checkEdit81.Tag = "1";
            this.checkEdit81.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit80
            // 
            this.checkEdit80.Location = new System.Drawing.Point(673, 647);
            this.checkEdit80.Name = "checkEdit80";
            this.checkEdit80.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit80.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit80.Properties.AutoHeight = false;
            this.checkEdit80.Properties.Caption = "5";
            this.checkEdit80.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit80.Properties.RadioGroupIndex = 16;
            this.checkEdit80.Size = new System.Drawing.Size(106, 24);
            this.checkEdit80.StyleController = this.layoutControl1;
            this.checkEdit80.TabIndex = 92;
            this.checkEdit80.TabStop = false;
            this.checkEdit80.Tag = "5";
            this.checkEdit80.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit79
            // 
            this.checkEdit79.Location = new System.Drawing.Point(603, 647);
            this.checkEdit79.Name = "checkEdit79";
            this.checkEdit79.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit79.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit79.Properties.AutoHeight = false;
            this.checkEdit79.Properties.Caption = "4";
            this.checkEdit79.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit79.Properties.RadioGroupIndex = 16;
            this.checkEdit79.Size = new System.Drawing.Size(66, 24);
            this.checkEdit79.StyleController = this.layoutControl1;
            this.checkEdit79.TabIndex = 91;
            this.checkEdit79.TabStop = false;
            this.checkEdit79.Tag = "4";
            this.checkEdit79.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit78
            // 
            this.checkEdit78.Location = new System.Drawing.Point(533, 647);
            this.checkEdit78.Name = "checkEdit78";
            this.checkEdit78.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit78.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit78.Properties.AutoHeight = false;
            this.checkEdit78.Properties.Caption = "3";
            this.checkEdit78.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit78.Properties.RadioGroupIndex = 16;
            this.checkEdit78.Size = new System.Drawing.Size(66, 24);
            this.checkEdit78.StyleController = this.layoutControl1;
            this.checkEdit78.TabIndex = 90;
            this.checkEdit78.TabStop = false;
            this.checkEdit78.Tag = "3";
            this.checkEdit78.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit77
            // 
            this.checkEdit77.Location = new System.Drawing.Point(463, 647);
            this.checkEdit77.Name = "checkEdit77";
            this.checkEdit77.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit77.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit77.Properties.AutoHeight = false;
            this.checkEdit77.Properties.Caption = "2";
            this.checkEdit77.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit77.Properties.RadioGroupIndex = 16;
            this.checkEdit77.Size = new System.Drawing.Size(66, 24);
            this.checkEdit77.StyleController = this.layoutControl1;
            this.checkEdit77.TabIndex = 89;
            this.checkEdit77.TabStop = false;
            this.checkEdit77.Tag = "2";
            this.checkEdit77.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit76
            // 
            this.checkEdit76.Location = new System.Drawing.Point(393, 647);
            this.checkEdit76.Name = "checkEdit76";
            this.checkEdit76.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit76.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit76.Properties.AutoHeight = false;
            this.checkEdit76.Properties.Caption = "1";
            this.checkEdit76.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit76.Properties.RadioGroupIndex = 16;
            this.checkEdit76.Size = new System.Drawing.Size(66, 24);
            this.checkEdit76.StyleController = this.layoutControl1;
            this.checkEdit76.TabIndex = 88;
            this.checkEdit76.TabStop = false;
            this.checkEdit76.Tag = "1";
            this.checkEdit76.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit75
            // 
            this.checkEdit75.Location = new System.Drawing.Point(673, 619);
            this.checkEdit75.Name = "checkEdit75";
            this.checkEdit75.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit75.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit75.Properties.AutoHeight = false;
            this.checkEdit75.Properties.Caption = "5";
            this.checkEdit75.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit75.Properties.RadioGroupIndex = 15;
            this.checkEdit75.Size = new System.Drawing.Size(106, 24);
            this.checkEdit75.StyleController = this.layoutControl1;
            this.checkEdit75.TabIndex = 87;
            this.checkEdit75.TabStop = false;
            this.checkEdit75.Tag = "5";
            this.checkEdit75.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit74
            // 
            this.checkEdit74.Location = new System.Drawing.Point(603, 619);
            this.checkEdit74.Name = "checkEdit74";
            this.checkEdit74.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit74.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit74.Properties.AutoHeight = false;
            this.checkEdit74.Properties.Caption = "4";
            this.checkEdit74.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit74.Properties.RadioGroupIndex = 15;
            this.checkEdit74.Size = new System.Drawing.Size(66, 24);
            this.checkEdit74.StyleController = this.layoutControl1;
            this.checkEdit74.TabIndex = 86;
            this.checkEdit74.TabStop = false;
            this.checkEdit74.Tag = "4";
            this.checkEdit74.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit73
            // 
            this.checkEdit73.Location = new System.Drawing.Point(533, 619);
            this.checkEdit73.Name = "checkEdit73";
            this.checkEdit73.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit73.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit73.Properties.AutoHeight = false;
            this.checkEdit73.Properties.Caption = "3";
            this.checkEdit73.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit73.Properties.RadioGroupIndex = 15;
            this.checkEdit73.Size = new System.Drawing.Size(66, 24);
            this.checkEdit73.StyleController = this.layoutControl1;
            this.checkEdit73.TabIndex = 85;
            this.checkEdit73.TabStop = false;
            this.checkEdit73.Tag = "3";
            this.checkEdit73.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit72
            // 
            this.checkEdit72.Location = new System.Drawing.Point(463, 619);
            this.checkEdit72.Name = "checkEdit72";
            this.checkEdit72.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit72.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit72.Properties.AutoHeight = false;
            this.checkEdit72.Properties.Caption = "2";
            this.checkEdit72.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit72.Properties.RadioGroupIndex = 15;
            this.checkEdit72.Size = new System.Drawing.Size(66, 24);
            this.checkEdit72.StyleController = this.layoutControl1;
            this.checkEdit72.TabIndex = 84;
            this.checkEdit72.TabStop = false;
            this.checkEdit72.Tag = "2";
            this.checkEdit72.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit71
            // 
            this.checkEdit71.Location = new System.Drawing.Point(393, 619);
            this.checkEdit71.Name = "checkEdit71";
            this.checkEdit71.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit71.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit71.Properties.AutoHeight = false;
            this.checkEdit71.Properties.Caption = "1";
            this.checkEdit71.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit71.Properties.RadioGroupIndex = 15;
            this.checkEdit71.Size = new System.Drawing.Size(66, 24);
            this.checkEdit71.StyleController = this.layoutControl1;
            this.checkEdit71.TabIndex = 83;
            this.checkEdit71.TabStop = false;
            this.checkEdit71.Tag = "1";
            this.checkEdit71.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit70
            // 
            this.checkEdit70.Location = new System.Drawing.Point(673, 569);
            this.checkEdit70.Name = "checkEdit70";
            this.checkEdit70.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit70.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit70.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit70.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit70.Properties.AutoHeight = false;
            this.checkEdit70.Properties.Caption = "5(几乎每月都感冒)";
            this.checkEdit70.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit70.Properties.RadioGroupIndex = 14;
            this.checkEdit70.Size = new System.Drawing.Size(106, 46);
            this.checkEdit70.StyleController = this.layoutControl1;
            this.checkEdit70.TabIndex = 82;
            this.checkEdit70.TabStop = false;
            this.checkEdit70.Tag = "5";
            this.checkEdit70.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit69
            // 
            this.checkEdit69.Location = new System.Drawing.Point(603, 569);
            this.checkEdit69.Name = "checkEdit69";
            this.checkEdit69.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit69.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit69.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit69.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit69.Properties.AutoHeight = false;
            this.checkEdit69.Properties.Caption = "4(一年8次以上)";
            this.checkEdit69.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit69.Properties.RadioGroupIndex = 14;
            this.checkEdit69.Size = new System.Drawing.Size(66, 46);
            this.checkEdit69.StyleController = this.layoutControl1;
            this.checkEdit69.TabIndex = 81;
            this.checkEdit69.TabStop = false;
            this.checkEdit69.Tag = "4";
            this.checkEdit69.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit68
            // 
            this.checkEdit68.Location = new System.Drawing.Point(533, 569);
            this.checkEdit68.Name = "checkEdit68";
            this.checkEdit68.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit68.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit68.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit68.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit68.Properties.AutoHeight = false;
            this.checkEdit68.Properties.Caption = "3(一年感冒5-6次)";
            this.checkEdit68.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit68.Properties.RadioGroupIndex = 14;
            this.checkEdit68.Size = new System.Drawing.Size(66, 46);
            this.checkEdit68.StyleController = this.layoutControl1;
            this.checkEdit68.TabIndex = 80;
            this.checkEdit68.TabStop = false;
            this.checkEdit68.Tag = "3";
            this.checkEdit68.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit67
            // 
            this.checkEdit67.Location = new System.Drawing.Point(463, 569);
            this.checkEdit67.Name = "checkEdit67";
            this.checkEdit67.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit67.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit67.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit67.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit67.Properties.AutoHeight = false;
            this.checkEdit67.Properties.Caption = "2(一年感冒2-4次)";
            this.checkEdit67.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit67.Properties.RadioGroupIndex = 14;
            this.checkEdit67.Size = new System.Drawing.Size(66, 46);
            this.checkEdit67.StyleController = this.layoutControl1;
            this.checkEdit67.TabIndex = 79;
            this.checkEdit67.TabStop = false;
            this.checkEdit67.Tag = "2";
            this.checkEdit67.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit66
            // 
            this.checkEdit66.Location = new System.Drawing.Point(393, 569);
            this.checkEdit66.Name = "checkEdit66";
            this.checkEdit66.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit66.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit66.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit66.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit66.Properties.AutoHeight = false;
            this.checkEdit66.Properties.Caption = "1(一年＜2次)";
            this.checkEdit66.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit66.Properties.RadioGroupIndex = 14;
            this.checkEdit66.Size = new System.Drawing.Size(66, 46);
            this.checkEdit66.StyleController = this.layoutControl1;
            this.checkEdit66.TabIndex = 78;
            this.checkEdit66.TabStop = false;
            this.checkEdit66.Tag = "1";
            this.checkEdit66.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit65
            // 
            this.checkEdit65.Location = new System.Drawing.Point(673, 529);
            this.checkEdit65.Name = "checkEdit65";
            this.checkEdit65.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit65.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit65.Properties.AutoHeight = false;
            this.checkEdit65.Properties.Caption = "5";
            this.checkEdit65.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit65.Properties.RadioGroupIndex = 13;
            this.checkEdit65.Size = new System.Drawing.Size(106, 36);
            this.checkEdit65.StyleController = this.layoutControl1;
            this.checkEdit65.TabIndex = 77;
            this.checkEdit65.TabStop = false;
            this.checkEdit65.Tag = "5";
            this.checkEdit65.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit64
            // 
            this.checkEdit64.Location = new System.Drawing.Point(603, 529);
            this.checkEdit64.Name = "checkEdit64";
            this.checkEdit64.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit64.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit64.Properties.AutoHeight = false;
            this.checkEdit64.Properties.Caption = "4";
            this.checkEdit64.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit64.Properties.RadioGroupIndex = 13;
            this.checkEdit64.Size = new System.Drawing.Size(66, 36);
            this.checkEdit64.StyleController = this.layoutControl1;
            this.checkEdit64.TabIndex = 76;
            this.checkEdit64.TabStop = false;
            this.checkEdit64.Tag = "4";
            this.checkEdit64.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit63
            // 
            this.checkEdit63.Location = new System.Drawing.Point(533, 529);
            this.checkEdit63.Name = "checkEdit63";
            this.checkEdit63.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit63.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit63.Properties.AutoHeight = false;
            this.checkEdit63.Properties.Caption = "3";
            this.checkEdit63.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit63.Properties.RadioGroupIndex = 13;
            this.checkEdit63.Size = new System.Drawing.Size(66, 36);
            this.checkEdit63.StyleController = this.layoutControl1;
            this.checkEdit63.TabIndex = 75;
            this.checkEdit63.TabStop = false;
            this.checkEdit63.Tag = "3";
            this.checkEdit63.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit62
            // 
            this.checkEdit62.Location = new System.Drawing.Point(463, 529);
            this.checkEdit62.Name = "checkEdit62";
            this.checkEdit62.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit62.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit62.Properties.AutoHeight = false;
            this.checkEdit62.Properties.Caption = "2";
            this.checkEdit62.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit62.Properties.RadioGroupIndex = 13;
            this.checkEdit62.Size = new System.Drawing.Size(66, 36);
            this.checkEdit62.StyleController = this.layoutControl1;
            this.checkEdit62.TabIndex = 74;
            this.checkEdit62.TabStop = false;
            this.checkEdit62.Tag = "2";
            this.checkEdit62.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit61
            // 
            this.checkEdit61.Location = new System.Drawing.Point(393, 529);
            this.checkEdit61.Name = "checkEdit61";
            this.checkEdit61.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit61.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit61.Properties.AutoHeight = false;
            this.checkEdit61.Properties.Caption = "1";
            this.checkEdit61.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit61.Properties.RadioGroupIndex = 13;
            this.checkEdit61.Size = new System.Drawing.Size(66, 36);
            this.checkEdit61.StyleController = this.layoutControl1;
            this.checkEdit61.TabIndex = 73;
            this.checkEdit61.TabStop = false;
            this.checkEdit61.Tag = "1";
            this.checkEdit61.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit60
            // 
            this.checkEdit60.Location = new System.Drawing.Point(673, 489);
            this.checkEdit60.Name = "checkEdit60";
            this.checkEdit60.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit60.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit60.Properties.AutoHeight = false;
            this.checkEdit60.Properties.Caption = "5";
            this.checkEdit60.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit60.Properties.RadioGroupIndex = 12;
            this.checkEdit60.Size = new System.Drawing.Size(106, 36);
            this.checkEdit60.StyleController = this.layoutControl1;
            this.checkEdit60.TabIndex = 72;
            this.checkEdit60.TabStop = false;
            this.checkEdit60.Tag = "5";
            this.checkEdit60.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit59
            // 
            this.checkEdit59.Location = new System.Drawing.Point(603, 489);
            this.checkEdit59.Name = "checkEdit59";
            this.checkEdit59.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit59.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit59.Properties.AutoHeight = false;
            this.checkEdit59.Properties.Caption = "4";
            this.checkEdit59.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit59.Properties.RadioGroupIndex = 12;
            this.checkEdit59.Size = new System.Drawing.Size(66, 36);
            this.checkEdit59.StyleController = this.layoutControl1;
            this.checkEdit59.TabIndex = 71;
            this.checkEdit59.TabStop = false;
            this.checkEdit59.Tag = "4";
            this.checkEdit59.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit58
            // 
            this.checkEdit58.Location = new System.Drawing.Point(533, 489);
            this.checkEdit58.Name = "checkEdit58";
            this.checkEdit58.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit58.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit58.Properties.AutoHeight = false;
            this.checkEdit58.Properties.Caption = "3";
            this.checkEdit58.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit58.Properties.RadioGroupIndex = 12;
            this.checkEdit58.Size = new System.Drawing.Size(66, 36);
            this.checkEdit58.StyleController = this.layoutControl1;
            this.checkEdit58.TabIndex = 70;
            this.checkEdit58.TabStop = false;
            this.checkEdit58.Tag = "3";
            this.checkEdit58.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit57
            // 
            this.checkEdit57.Location = new System.Drawing.Point(463, 489);
            this.checkEdit57.Name = "checkEdit57";
            this.checkEdit57.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit57.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit57.Properties.AutoHeight = false;
            this.checkEdit57.Properties.Caption = "2";
            this.checkEdit57.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit57.Properties.RadioGroupIndex = 12;
            this.checkEdit57.Size = new System.Drawing.Size(66, 36);
            this.checkEdit57.StyleController = this.layoutControl1;
            this.checkEdit57.TabIndex = 69;
            this.checkEdit57.TabStop = false;
            this.checkEdit57.Tag = "2";
            this.checkEdit57.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit56
            // 
            this.checkEdit56.Location = new System.Drawing.Point(393, 489);
            this.checkEdit56.Name = "checkEdit56";
            this.checkEdit56.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit56.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit56.Properties.AutoHeight = false;
            this.checkEdit56.Properties.Caption = "1";
            this.checkEdit56.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit56.Properties.RadioGroupIndex = 12;
            this.checkEdit56.Size = new System.Drawing.Size(66, 36);
            this.checkEdit56.StyleController = this.layoutControl1;
            this.checkEdit56.TabIndex = 68;
            this.checkEdit56.TabStop = false;
            this.checkEdit56.Tag = "1";
            this.checkEdit56.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit55
            // 
            this.checkEdit55.Location = new System.Drawing.Point(673, 461);
            this.checkEdit55.Name = "checkEdit55";
            this.checkEdit55.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit55.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit55.Properties.AutoHeight = false;
            this.checkEdit55.Properties.Caption = "5";
            this.checkEdit55.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit55.Properties.RadioGroupIndex = 11;
            this.checkEdit55.Size = new System.Drawing.Size(106, 24);
            this.checkEdit55.StyleController = this.layoutControl1;
            this.checkEdit55.TabIndex = 67;
            this.checkEdit55.TabStop = false;
            this.checkEdit55.Tag = "5";
            this.checkEdit55.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit54
            // 
            this.checkEdit54.Location = new System.Drawing.Point(603, 461);
            this.checkEdit54.Name = "checkEdit54";
            this.checkEdit54.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit54.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit54.Properties.AutoHeight = false;
            this.checkEdit54.Properties.Caption = "4";
            this.checkEdit54.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit54.Properties.RadioGroupIndex = 11;
            this.checkEdit54.Size = new System.Drawing.Size(66, 24);
            this.checkEdit54.StyleController = this.layoutControl1;
            this.checkEdit54.TabIndex = 66;
            this.checkEdit54.TabStop = false;
            this.checkEdit54.Tag = "4";
            this.checkEdit54.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit53
            // 
            this.checkEdit53.Location = new System.Drawing.Point(533, 461);
            this.checkEdit53.Name = "checkEdit53";
            this.checkEdit53.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit53.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit53.Properties.AutoHeight = false;
            this.checkEdit53.Properties.Caption = "3";
            this.checkEdit53.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit53.Properties.RadioGroupIndex = 11;
            this.checkEdit53.Size = new System.Drawing.Size(66, 24);
            this.checkEdit53.StyleController = this.layoutControl1;
            this.checkEdit53.TabIndex = 65;
            this.checkEdit53.TabStop = false;
            this.checkEdit53.Tag = "3";
            this.checkEdit53.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit52
            // 
            this.checkEdit52.Location = new System.Drawing.Point(463, 461);
            this.checkEdit52.Name = "checkEdit52";
            this.checkEdit52.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit52.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit52.Properties.AutoHeight = false;
            this.checkEdit52.Properties.Caption = "2";
            this.checkEdit52.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit52.Properties.RadioGroupIndex = 11;
            this.checkEdit52.Size = new System.Drawing.Size(66, 24);
            this.checkEdit52.StyleController = this.layoutControl1;
            this.checkEdit52.TabIndex = 64;
            this.checkEdit52.TabStop = false;
            this.checkEdit52.Tag = "2";
            this.checkEdit52.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit51
            // 
            this.checkEdit51.Location = new System.Drawing.Point(393, 461);
            this.checkEdit51.Name = "checkEdit51";
            this.checkEdit51.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit51.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit51.Properties.AutoHeight = false;
            this.checkEdit51.Properties.Caption = "1";
            this.checkEdit51.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit51.Properties.RadioGroupIndex = 11;
            this.checkEdit51.Size = new System.Drawing.Size(66, 24);
            this.checkEdit51.StyleController = this.layoutControl1;
            this.checkEdit51.TabIndex = 63;
            this.checkEdit51.TabStop = false;
            this.checkEdit51.Tag = "1";
            this.checkEdit51.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit50
            // 
            this.checkEdit50.Location = new System.Drawing.Point(673, 433);
            this.checkEdit50.Name = "checkEdit50";
            this.checkEdit50.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit50.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit50.Properties.AutoHeight = false;
            this.checkEdit50.Properties.Caption = "5";
            this.checkEdit50.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit50.Properties.RadioGroupIndex = 10;
            this.checkEdit50.Size = new System.Drawing.Size(106, 24);
            this.checkEdit50.StyleController = this.layoutControl1;
            this.checkEdit50.TabIndex = 62;
            this.checkEdit50.TabStop = false;
            this.checkEdit50.Tag = "5";
            this.checkEdit50.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit49
            // 
            this.checkEdit49.Location = new System.Drawing.Point(603, 433);
            this.checkEdit49.Name = "checkEdit49";
            this.checkEdit49.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit49.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit49.Properties.AutoHeight = false;
            this.checkEdit49.Properties.Caption = "4";
            this.checkEdit49.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit49.Properties.RadioGroupIndex = 10;
            this.checkEdit49.Size = new System.Drawing.Size(66, 24);
            this.checkEdit49.StyleController = this.layoutControl1;
            this.checkEdit49.TabIndex = 61;
            this.checkEdit49.TabStop = false;
            this.checkEdit49.Tag = "4";
            this.checkEdit49.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit48
            // 
            this.checkEdit48.Location = new System.Drawing.Point(533, 433);
            this.checkEdit48.Name = "checkEdit48";
            this.checkEdit48.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit48.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit48.Properties.AutoHeight = false;
            this.checkEdit48.Properties.Caption = "3";
            this.checkEdit48.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit48.Properties.RadioGroupIndex = 10;
            this.checkEdit48.Size = new System.Drawing.Size(66, 24);
            this.checkEdit48.StyleController = this.layoutControl1;
            this.checkEdit48.TabIndex = 60;
            this.checkEdit48.TabStop = false;
            this.checkEdit48.Tag = "3";
            this.checkEdit48.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit47
            // 
            this.checkEdit47.Location = new System.Drawing.Point(463, 433);
            this.checkEdit47.Name = "checkEdit47";
            this.checkEdit47.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit47.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit47.Properties.AutoHeight = false;
            this.checkEdit47.Properties.Caption = "2";
            this.checkEdit47.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit47.Properties.RadioGroupIndex = 10;
            this.checkEdit47.Size = new System.Drawing.Size(66, 24);
            this.checkEdit47.StyleController = this.layoutControl1;
            this.checkEdit47.TabIndex = 59;
            this.checkEdit47.TabStop = false;
            this.checkEdit47.Tag = "2";
            this.checkEdit47.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit46
            // 
            this.checkEdit46.Location = new System.Drawing.Point(393, 433);
            this.checkEdit46.Name = "checkEdit46";
            this.checkEdit46.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit46.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit46.Properties.AutoHeight = false;
            this.checkEdit46.Properties.Caption = "1";
            this.checkEdit46.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit46.Properties.RadioGroupIndex = 10;
            this.checkEdit46.Size = new System.Drawing.Size(66, 24);
            this.checkEdit46.StyleController = this.layoutControl1;
            this.checkEdit46.TabIndex = 58;
            this.checkEdit46.TabStop = false;
            this.checkEdit46.Tag = "1";
            this.checkEdit46.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit45
            // 
            this.checkEdit45.Location = new System.Drawing.Point(673, 383);
            this.checkEdit45.Name = "checkEdit45";
            this.checkEdit45.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit45.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit45.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit45.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit45.Properties.AutoHeight = false;
            this.checkEdit45.Properties.Caption = "5(BMI≥28)";
            this.checkEdit45.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit45.Properties.RadioGroupIndex = 9;
            this.checkEdit45.Size = new System.Drawing.Size(106, 46);
            this.checkEdit45.StyleController = this.layoutControl1;
            this.checkEdit45.TabIndex = 57;
            this.checkEdit45.TabStop = false;
            this.checkEdit45.Tag = "5";
            this.checkEdit45.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit44
            // 
            this.checkEdit44.Location = new System.Drawing.Point(603, 383);
            this.checkEdit44.Name = "checkEdit44";
            this.checkEdit44.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit44.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit44.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit44.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit44.Properties.AutoHeight = false;
            this.checkEdit44.Properties.Caption = "4（26≤BMI＜28）";
            this.checkEdit44.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit44.Properties.RadioGroupIndex = 9;
            this.checkEdit44.Size = new System.Drawing.Size(66, 46);
            this.checkEdit44.StyleController = this.layoutControl1;
            this.checkEdit44.TabIndex = 56;
            this.checkEdit44.TabStop = false;
            this.checkEdit44.Tag = "4";
            this.checkEdit44.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit43
            // 
            this.checkEdit43.Location = new System.Drawing.Point(533, 383);
            this.checkEdit43.Name = "checkEdit43";
            this.checkEdit43.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit43.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit43.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit43.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit43.Properties.AutoHeight = false;
            this.checkEdit43.Properties.Caption = "3(25≤BMI＜26)";
            this.checkEdit43.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit43.Properties.RadioGroupIndex = 9;
            this.checkEdit43.Size = new System.Drawing.Size(66, 46);
            this.checkEdit43.StyleController = this.layoutControl1;
            this.checkEdit43.TabIndex = 55;
            this.checkEdit43.TabStop = false;
            this.checkEdit43.Tag = "3";
            this.checkEdit43.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit42
            // 
            this.checkEdit42.Location = new System.Drawing.Point(463, 383);
            this.checkEdit42.Name = "checkEdit42";
            this.checkEdit42.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit42.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit42.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit42.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit42.Properties.AutoHeight = false;
            this.checkEdit42.Properties.Caption = "2(24≤BMI＜25)";
            this.checkEdit42.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit42.Properties.RadioGroupIndex = 9;
            this.checkEdit42.Size = new System.Drawing.Size(66, 46);
            this.checkEdit42.StyleController = this.layoutControl1;
            this.checkEdit42.TabIndex = 54;
            this.checkEdit42.TabStop = false;
            this.checkEdit42.Tag = "2";
            this.checkEdit42.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit41
            // 
            this.checkEdit41.Location = new System.Drawing.Point(393, 383);
            this.checkEdit41.Name = "checkEdit41";
            this.checkEdit41.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit41.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit41.Properties.Appearance.Options.UseTextOptions = true;
            this.checkEdit41.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.checkEdit41.Properties.AutoHeight = false;
            this.checkEdit41.Properties.Caption = "1(BMI＜24)";
            this.checkEdit41.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit41.Properties.RadioGroupIndex = 9;
            this.checkEdit41.Size = new System.Drawing.Size(66, 46);
            this.checkEdit41.StyleController = this.layoutControl1;
            this.checkEdit41.TabIndex = 53;
            this.checkEdit41.TabStop = false;
            this.checkEdit41.Tag = "1";
            this.checkEdit41.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit40
            // 
            this.checkEdit40.Location = new System.Drawing.Point(673, 355);
            this.checkEdit40.Name = "checkEdit40";
            this.checkEdit40.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit40.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit40.Properties.AutoHeight = false;
            this.checkEdit40.Properties.Caption = "5";
            this.checkEdit40.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit40.Properties.RadioGroupIndex = 8;
            this.checkEdit40.Size = new System.Drawing.Size(106, 24);
            this.checkEdit40.StyleController = this.layoutControl1;
            this.checkEdit40.TabIndex = 52;
            this.checkEdit40.TabStop = false;
            this.checkEdit40.Tag = "5";
            this.checkEdit40.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit39
            // 
            this.checkEdit39.Location = new System.Drawing.Point(603, 355);
            this.checkEdit39.Name = "checkEdit39";
            this.checkEdit39.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit39.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit39.Properties.AutoHeight = false;
            this.checkEdit39.Properties.Caption = "4";
            this.checkEdit39.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit39.Properties.RadioGroupIndex = 8;
            this.checkEdit39.Size = new System.Drawing.Size(66, 24);
            this.checkEdit39.StyleController = this.layoutControl1;
            this.checkEdit39.TabIndex = 51;
            this.checkEdit39.TabStop = false;
            this.checkEdit39.Tag = "4";
            this.checkEdit39.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit38
            // 
            this.checkEdit38.Location = new System.Drawing.Point(533, 355);
            this.checkEdit38.Name = "checkEdit38";
            this.checkEdit38.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit38.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit38.Properties.AutoHeight = false;
            this.checkEdit38.Properties.Caption = "3";
            this.checkEdit38.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit38.Properties.RadioGroupIndex = 8;
            this.checkEdit38.Size = new System.Drawing.Size(66, 24);
            this.checkEdit38.StyleController = this.layoutControl1;
            this.checkEdit38.TabIndex = 50;
            this.checkEdit38.TabStop = false;
            this.checkEdit38.Tag = "3";
            this.checkEdit38.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit37
            // 
            this.checkEdit37.Location = new System.Drawing.Point(463, 355);
            this.checkEdit37.Name = "checkEdit37";
            this.checkEdit37.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit37.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit37.Properties.AutoHeight = false;
            this.checkEdit37.Properties.Caption = "2";
            this.checkEdit37.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit37.Properties.RadioGroupIndex = 8;
            this.checkEdit37.Size = new System.Drawing.Size(66, 24);
            this.checkEdit37.StyleController = this.layoutControl1;
            this.checkEdit37.TabIndex = 49;
            this.checkEdit37.TabStop = false;
            this.checkEdit37.Tag = "2";
            this.checkEdit37.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit36
            // 
            this.checkEdit36.Location = new System.Drawing.Point(393, 355);
            this.checkEdit36.Name = "checkEdit36";
            this.checkEdit36.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit36.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit36.Properties.AutoHeight = false;
            this.checkEdit36.Properties.Caption = "1";
            this.checkEdit36.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit36.Properties.RadioGroupIndex = 8;
            this.checkEdit36.Size = new System.Drawing.Size(66, 24);
            this.checkEdit36.StyleController = this.layoutControl1;
            this.checkEdit36.TabIndex = 48;
            this.checkEdit36.TabStop = false;
            this.checkEdit36.Tag = "1";
            this.checkEdit36.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit35
            // 
            this.checkEdit35.Location = new System.Drawing.Point(673, 327);
            this.checkEdit35.Name = "checkEdit35";
            this.checkEdit35.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit35.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit35.Properties.AutoHeight = false;
            this.checkEdit35.Properties.Caption = "5";
            this.checkEdit35.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit35.Properties.RadioGroupIndex = 7;
            this.checkEdit35.Size = new System.Drawing.Size(106, 24);
            this.checkEdit35.StyleController = this.layoutControl1;
            this.checkEdit35.TabIndex = 47;
            this.checkEdit35.TabStop = false;
            this.checkEdit35.Tag = "5";
            this.checkEdit35.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit34
            // 
            this.checkEdit34.Location = new System.Drawing.Point(603, 327);
            this.checkEdit34.Name = "checkEdit34";
            this.checkEdit34.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit34.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit34.Properties.AutoHeight = false;
            this.checkEdit34.Properties.Caption = "4";
            this.checkEdit34.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit34.Properties.RadioGroupIndex = 7;
            this.checkEdit34.Size = new System.Drawing.Size(66, 24);
            this.checkEdit34.StyleController = this.layoutControl1;
            this.checkEdit34.TabIndex = 46;
            this.checkEdit34.TabStop = false;
            this.checkEdit34.Tag = "4";
            this.checkEdit34.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit33
            // 
            this.checkEdit33.Location = new System.Drawing.Point(533, 327);
            this.checkEdit33.Name = "checkEdit33";
            this.checkEdit33.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit33.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit33.Properties.AutoHeight = false;
            this.checkEdit33.Properties.Caption = "3";
            this.checkEdit33.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit33.Properties.RadioGroupIndex = 7;
            this.checkEdit33.Size = new System.Drawing.Size(66, 24);
            this.checkEdit33.StyleController = this.layoutControl1;
            this.checkEdit33.TabIndex = 45;
            this.checkEdit33.TabStop = false;
            this.checkEdit33.Tag = "3";
            this.checkEdit33.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit32
            // 
            this.checkEdit32.Location = new System.Drawing.Point(463, 327);
            this.checkEdit32.Name = "checkEdit32";
            this.checkEdit32.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit32.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit32.Properties.AutoHeight = false;
            this.checkEdit32.Properties.Caption = "2";
            this.checkEdit32.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit32.Properties.RadioGroupIndex = 7;
            this.checkEdit32.Size = new System.Drawing.Size(66, 24);
            this.checkEdit32.StyleController = this.layoutControl1;
            this.checkEdit32.TabIndex = 44;
            this.checkEdit32.TabStop = false;
            this.checkEdit32.Tag = "2";
            this.checkEdit32.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit31
            // 
            this.checkEdit31.Location = new System.Drawing.Point(393, 327);
            this.checkEdit31.Name = "checkEdit31";
            this.checkEdit31.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit31.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit31.Properties.AutoHeight = false;
            this.checkEdit31.Properties.Caption = "1";
            this.checkEdit31.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit31.Properties.RadioGroupIndex = 7;
            this.checkEdit31.Size = new System.Drawing.Size(66, 24);
            this.checkEdit31.StyleController = this.layoutControl1;
            this.checkEdit31.TabIndex = 43;
            this.checkEdit31.TabStop = false;
            this.checkEdit31.Tag = "1";
            this.checkEdit31.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit30
            // 
            this.checkEdit30.Location = new System.Drawing.Point(673, 299);
            this.checkEdit30.Name = "checkEdit30";
            this.checkEdit30.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit30.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit30.Properties.AutoHeight = false;
            this.checkEdit30.Properties.Caption = "5";
            this.checkEdit30.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit30.Properties.RadioGroupIndex = 6;
            this.checkEdit30.Size = new System.Drawing.Size(106, 24);
            this.checkEdit30.StyleController = this.layoutControl1;
            this.checkEdit30.TabIndex = 42;
            this.checkEdit30.TabStop = false;
            this.checkEdit30.Tag = "5";
            this.checkEdit30.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit29
            // 
            this.checkEdit29.Location = new System.Drawing.Point(603, 299);
            this.checkEdit29.Name = "checkEdit29";
            this.checkEdit29.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit29.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit29.Properties.AutoHeight = false;
            this.checkEdit29.Properties.Caption = "4";
            this.checkEdit29.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit29.Properties.RadioGroupIndex = 6;
            this.checkEdit29.Size = new System.Drawing.Size(66, 24);
            this.checkEdit29.StyleController = this.layoutControl1;
            this.checkEdit29.TabIndex = 41;
            this.checkEdit29.TabStop = false;
            this.checkEdit29.Tag = "4";
            this.checkEdit29.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit28
            // 
            this.checkEdit28.Location = new System.Drawing.Point(533, 299);
            this.checkEdit28.Name = "checkEdit28";
            this.checkEdit28.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit28.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit28.Properties.AutoHeight = false;
            this.checkEdit28.Properties.Caption = "3";
            this.checkEdit28.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit28.Properties.RadioGroupIndex = 6;
            this.checkEdit28.Size = new System.Drawing.Size(66, 24);
            this.checkEdit28.StyleController = this.layoutControl1;
            this.checkEdit28.TabIndex = 40;
            this.checkEdit28.TabStop = false;
            this.checkEdit28.Tag = "3";
            this.checkEdit28.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit26
            // 
            this.checkEdit26.Location = new System.Drawing.Point(463, 299);
            this.checkEdit26.Name = "checkEdit26";
            this.checkEdit26.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit26.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit26.Properties.AutoHeight = false;
            this.checkEdit26.Properties.Caption = "2";
            this.checkEdit26.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit26.Properties.RadioGroupIndex = 6;
            this.checkEdit26.Size = new System.Drawing.Size(66, 24);
            this.checkEdit26.StyleController = this.layoutControl1;
            this.checkEdit26.TabIndex = 39;
            this.checkEdit26.TabStop = false;
            this.checkEdit26.Tag = "2";
            this.checkEdit26.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit27
            // 
            this.checkEdit27.Location = new System.Drawing.Point(393, 299);
            this.checkEdit27.Name = "checkEdit27";
            this.checkEdit27.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit27.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit27.Properties.AutoHeight = false;
            this.checkEdit27.Properties.Caption = "1";
            this.checkEdit27.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit27.Properties.RadioGroupIndex = 6;
            this.checkEdit27.Size = new System.Drawing.Size(66, 24);
            this.checkEdit27.StyleController = this.layoutControl1;
            this.checkEdit27.TabIndex = 38;
            this.checkEdit27.TabStop = false;
            this.checkEdit27.Tag = "1";
            this.checkEdit27.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit25
            // 
            this.checkEdit25.Location = new System.Drawing.Point(673, 271);
            this.checkEdit25.Name = "checkEdit25";
            this.checkEdit25.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit25.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit25.Properties.AutoHeight = false;
            this.checkEdit25.Properties.Caption = "5";
            this.checkEdit25.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit25.Properties.RadioGroupIndex = 5;
            this.checkEdit25.Size = new System.Drawing.Size(106, 24);
            this.checkEdit25.StyleController = this.layoutControl1;
            this.checkEdit25.TabIndex = 36;
            this.checkEdit25.TabStop = false;
            this.checkEdit25.Tag = "5";
            this.checkEdit25.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit24
            // 
            this.checkEdit24.Location = new System.Drawing.Point(603, 271);
            this.checkEdit24.Name = "checkEdit24";
            this.checkEdit24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit24.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit24.Properties.AutoHeight = false;
            this.checkEdit24.Properties.Caption = "4";
            this.checkEdit24.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit24.Properties.RadioGroupIndex = 5;
            this.checkEdit24.Size = new System.Drawing.Size(66, 24);
            this.checkEdit24.StyleController = this.layoutControl1;
            this.checkEdit24.TabIndex = 35;
            this.checkEdit24.TabStop = false;
            this.checkEdit24.Tag = "4";
            this.checkEdit24.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit23
            // 
            this.checkEdit23.Location = new System.Drawing.Point(533, 271);
            this.checkEdit23.Name = "checkEdit23";
            this.checkEdit23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit23.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit23.Properties.AutoHeight = false;
            this.checkEdit23.Properties.Caption = "3";
            this.checkEdit23.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit23.Properties.RadioGroupIndex = 5;
            this.checkEdit23.Size = new System.Drawing.Size(66, 24);
            this.checkEdit23.StyleController = this.layoutControl1;
            this.checkEdit23.TabIndex = 34;
            this.checkEdit23.TabStop = false;
            this.checkEdit23.Tag = "3";
            this.checkEdit23.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit22
            // 
            this.checkEdit22.Location = new System.Drawing.Point(463, 271);
            this.checkEdit22.Name = "checkEdit22";
            this.checkEdit22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit22.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit22.Properties.AutoHeight = false;
            this.checkEdit22.Properties.Caption = "2";
            this.checkEdit22.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit22.Properties.RadioGroupIndex = 5;
            this.checkEdit22.Size = new System.Drawing.Size(66, 24);
            this.checkEdit22.StyleController = this.layoutControl1;
            this.checkEdit22.TabIndex = 33;
            this.checkEdit22.TabStop = false;
            this.checkEdit22.Tag = "2";
            this.checkEdit22.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit21
            // 
            this.checkEdit21.Location = new System.Drawing.Point(393, 271);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit21.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit21.Properties.AutoHeight = false;
            this.checkEdit21.Properties.Caption = "1";
            this.checkEdit21.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit21.Properties.RadioGroupIndex = 5;
            this.checkEdit21.Size = new System.Drawing.Size(66, 24);
            this.checkEdit21.StyleController = this.layoutControl1;
            this.checkEdit21.TabIndex = 32;
            this.checkEdit21.TabStop = false;
            this.checkEdit21.Tag = "1";
            this.checkEdit21.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit20
            // 
            this.checkEdit20.Location = new System.Drawing.Point(673, 243);
            this.checkEdit20.Name = "checkEdit20";
            this.checkEdit20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit20.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit20.Properties.AutoHeight = false;
            this.checkEdit20.Properties.Caption = "5";
            this.checkEdit20.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit20.Properties.RadioGroupIndex = 4;
            this.checkEdit20.Size = new System.Drawing.Size(106, 24);
            this.checkEdit20.StyleController = this.layoutControl1;
            this.checkEdit20.TabIndex = 30;
            this.checkEdit20.TabStop = false;
            this.checkEdit20.Tag = "5";
            this.checkEdit20.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit19
            // 
            this.checkEdit19.Location = new System.Drawing.Point(603, 243);
            this.checkEdit19.Name = "checkEdit19";
            this.checkEdit19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit19.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit19.Properties.AutoHeight = false;
            this.checkEdit19.Properties.Caption = "4";
            this.checkEdit19.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit19.Properties.RadioGroupIndex = 4;
            this.checkEdit19.Size = new System.Drawing.Size(66, 24);
            this.checkEdit19.StyleController = this.layoutControl1;
            this.checkEdit19.TabIndex = 29;
            this.checkEdit19.TabStop = false;
            this.checkEdit19.Tag = "4";
            this.checkEdit19.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit18
            // 
            this.checkEdit18.Location = new System.Drawing.Point(533, 243);
            this.checkEdit18.Name = "checkEdit18";
            this.checkEdit18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit18.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit18.Properties.AutoHeight = false;
            this.checkEdit18.Properties.Caption = "3";
            this.checkEdit18.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit18.Properties.RadioGroupIndex = 4;
            this.checkEdit18.Size = new System.Drawing.Size(66, 24);
            this.checkEdit18.StyleController = this.layoutControl1;
            this.checkEdit18.TabIndex = 28;
            this.checkEdit18.TabStop = false;
            this.checkEdit18.Tag = "3";
            this.checkEdit18.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit17
            // 
            this.checkEdit17.Location = new System.Drawing.Point(463, 243);
            this.checkEdit17.Name = "checkEdit17";
            this.checkEdit17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit17.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit17.Properties.AutoHeight = false;
            this.checkEdit17.Properties.Caption = "2";
            this.checkEdit17.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit17.Properties.RadioGroupIndex = 4;
            this.checkEdit17.Size = new System.Drawing.Size(66, 24);
            this.checkEdit17.StyleController = this.layoutControl1;
            this.checkEdit17.TabIndex = 27;
            this.checkEdit17.TabStop = false;
            this.checkEdit17.Tag = "2";
            this.checkEdit17.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(393, 243);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit16.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit16.Properties.AutoHeight = false;
            this.checkEdit16.Properties.Caption = "1";
            this.checkEdit16.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit16.Properties.RadioGroupIndex = 4;
            this.checkEdit16.Size = new System.Drawing.Size(66, 24);
            this.checkEdit16.StyleController = this.layoutControl1;
            this.checkEdit16.TabIndex = 26;
            this.checkEdit16.TabStop = false;
            this.checkEdit16.Tag = "1";
            this.checkEdit16.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit15
            // 
            this.checkEdit15.Location = new System.Drawing.Point(393, 215);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit15.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit15.Properties.AutoHeight = false;
            this.checkEdit15.Properties.Caption = "1";
            this.checkEdit15.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit15.Properties.RadioGroupIndex = 3;
            this.checkEdit15.Size = new System.Drawing.Size(66, 24);
            this.checkEdit15.StyleController = this.layoutControl1;
            this.checkEdit15.TabIndex = 21;
            this.checkEdit15.TabStop = false;
            this.checkEdit15.Tag = "1";
            this.checkEdit15.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(533, 215);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit14.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit14.Properties.AutoHeight = false;
            this.checkEdit14.Properties.Caption = "3";
            this.checkEdit14.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit14.Properties.RadioGroupIndex = 3;
            this.checkEdit14.Size = new System.Drawing.Size(66, 24);
            this.checkEdit14.StyleController = this.layoutControl1;
            this.checkEdit14.TabIndex = 23;
            this.checkEdit14.TabStop = false;
            this.checkEdit14.Tag = "3";
            this.checkEdit14.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(463, 215);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit13.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit13.Properties.AutoHeight = false;
            this.checkEdit13.Properties.Caption = "2";
            this.checkEdit13.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit13.Properties.RadioGroupIndex = 3;
            this.checkEdit13.Size = new System.Drawing.Size(66, 24);
            this.checkEdit13.StyleController = this.layoutControl1;
            this.checkEdit13.TabIndex = 22;
            this.checkEdit13.TabStop = false;
            this.checkEdit13.Tag = "2";
            this.checkEdit13.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(603, 215);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit12.Properties.AutoHeight = false;
            this.checkEdit12.Properties.Caption = "4";
            this.checkEdit12.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit12.Properties.RadioGroupIndex = 3;
            this.checkEdit12.Size = new System.Drawing.Size(66, 24);
            this.checkEdit12.StyleController = this.layoutControl1;
            this.checkEdit12.TabIndex = 24;
            this.checkEdit12.TabStop = false;
            this.checkEdit12.Tag = "4";
            this.checkEdit12.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(673, 215);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit11.Properties.AutoHeight = false;
            this.checkEdit11.Properties.Caption = "5";
            this.checkEdit11.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit11.Properties.RadioGroupIndex = 3;
            this.checkEdit11.Size = new System.Drawing.Size(106, 24);
            this.checkEdit11.StyleController = this.layoutControl1;
            this.checkEdit11.TabIndex = 25;
            this.checkEdit11.TabStop = false;
            this.checkEdit11.Tag = "5";
            this.checkEdit11.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(673, 175);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit10.Properties.AutoHeight = false;
            this.checkEdit10.Properties.Caption = "5";
            this.checkEdit10.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit10.Properties.RadioGroupIndex = 2;
            this.checkEdit10.Size = new System.Drawing.Size(106, 36);
            this.checkEdit10.StyleController = this.layoutControl1;
            this.checkEdit10.TabIndex = 20;
            this.checkEdit10.TabStop = false;
            this.checkEdit10.Tag = "5";
            this.checkEdit10.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(463, 175);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit9.Properties.AutoHeight = false;
            this.checkEdit9.Properties.Caption = "2";
            this.checkEdit9.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit9.Properties.RadioGroupIndex = 2;
            this.checkEdit9.Size = new System.Drawing.Size(66, 36);
            this.checkEdit9.StyleController = this.layoutControl1;
            this.checkEdit9.TabIndex = 17;
            this.checkEdit9.TabStop = false;
            this.checkEdit9.Tag = "2";
            this.checkEdit9.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit8
            // 
            this.checkEdit8.Location = new System.Drawing.Point(533, 175);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit8.Properties.AutoHeight = false;
            this.checkEdit8.Properties.Caption = "3";
            this.checkEdit8.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit8.Properties.RadioGroupIndex = 2;
            this.checkEdit8.Size = new System.Drawing.Size(66, 36);
            this.checkEdit8.StyleController = this.layoutControl1;
            this.checkEdit8.TabIndex = 18;
            this.checkEdit8.TabStop = false;
            this.checkEdit8.Tag = "3";
            this.checkEdit8.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit7
            // 
            this.checkEdit7.Location = new System.Drawing.Point(603, 175);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit7.Properties.AutoHeight = false;
            this.checkEdit7.Properties.Caption = "4";
            this.checkEdit7.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit7.Properties.RadioGroupIndex = 2;
            this.checkEdit7.Size = new System.Drawing.Size(66, 36);
            this.checkEdit7.StyleController = this.layoutControl1;
            this.checkEdit7.TabIndex = 19;
            this.checkEdit7.TabStop = false;
            this.checkEdit7.Tag = "4";
            this.checkEdit7.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(393, 175);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit6.Properties.AutoHeight = false;
            this.checkEdit6.Properties.Caption = "1";
            this.checkEdit6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit6.Properties.RadioGroupIndex = 2;
            this.checkEdit6.Size = new System.Drawing.Size(66, 36);
            this.checkEdit6.StyleController = this.layoutControl1;
            this.checkEdit6.TabIndex = 16;
            this.checkEdit6.TabStop = false;
            this.checkEdit6.Tag = "1";
            this.checkEdit6.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(673, 147);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit5.Properties.AutoHeight = false;
            this.checkEdit5.Properties.Caption = "5";
            this.checkEdit5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit5.Properties.RadioGroupIndex = 1;
            this.checkEdit5.Size = new System.Drawing.Size(106, 24);
            this.checkEdit5.StyleController = this.layoutControl1;
            this.checkEdit5.TabIndex = 15;
            this.checkEdit5.TabStop = false;
            this.checkEdit5.Tag = "5";
            this.checkEdit5.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(603, 147);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.checkEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit4.Properties.AutoHeight = false;
            this.checkEdit4.Properties.Caption = "4";
            this.checkEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit4.Properties.RadioGroupIndex = 1;
            this.checkEdit4.Size = new System.Drawing.Size(66, 24);
            this.checkEdit4.StyleController = this.layoutControl1;
            this.checkEdit4.TabIndex = 14;
            this.checkEdit4.TabStop = false;
            this.checkEdit4.Tag = "4";
            this.checkEdit4.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chkA3
            // 
            this.chkA3.Location = new System.Drawing.Point(533, 147);
            this.chkA3.Name = "chkA3";
            this.chkA3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chkA3.Properties.Appearance.Options.UseBackColor = true;
            this.chkA3.Properties.AutoHeight = false;
            this.chkA3.Properties.Caption = "3";
            this.chkA3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkA3.Properties.RadioGroupIndex = 1;
            this.chkA3.Size = new System.Drawing.Size(66, 24);
            this.chkA3.StyleController = this.layoutControl1;
            this.chkA3.TabIndex = 13;
            this.chkA3.TabStop = false;
            this.chkA3.Tag = "3";
            this.chkA3.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chkA2
            // 
            this.chkA2.Location = new System.Drawing.Point(463, 147);
            this.chkA2.Name = "chkA2";
            this.chkA2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chkA2.Properties.Appearance.Options.UseBackColor = true;
            this.chkA2.Properties.AutoHeight = false;
            this.chkA2.Properties.Caption = "2";
            this.chkA2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkA2.Properties.RadioGroupIndex = 1;
            this.chkA2.Size = new System.Drawing.Size(66, 24);
            this.chkA2.StyleController = this.layoutControl1;
            this.chkA2.TabIndex = 12;
            this.chkA2.TabStop = false;
            this.chkA2.Tag = "2";
            this.chkA2.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chkA1
            // 
            this.chkA1.Location = new System.Drawing.Point(393, 147);
            this.chkA1.Name = "chkA1";
            this.chkA1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.chkA1.Properties.Appearance.Options.UseBackColor = true;
            this.chkA1.Properties.AutoHeight = false;
            this.chkA1.Properties.Caption = "1";
            this.chkA1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chkA1.Properties.RadioGroupIndex = 1;
            this.chkA1.Size = new System.Drawing.Size(66, 24);
            this.chkA1.StyleController = this.layoutControl1;
            this.chkA1.TabIndex = 11;
            this.chkA1.TabStop = false;
            this.chkA1.Tag = "1";
            this.chkA1.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // txt居住状态
            // 
            this.txt居住状态.Location = new System.Drawing.Point(58, 73);
            this.txt居住状态.Name = "txt居住状态";
            this.txt居住状态.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住状态.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住状态.Properties.ReadOnly = true;
            this.txt居住状态.Size = new System.Drawing.Size(201, 20);
            this.txt居住状态.StyleController = this.layoutControl1;
            this.txt居住状态.TabIndex = 9;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(578, 49);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(201, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 8;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(318, 49);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(201, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 7;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(58, 49);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(201, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 6;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(578, 25);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(201, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 5;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(318, 25);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(201, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 4;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(58, 25);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案号.Properties.ReadOnly = true;
            this.txt个人档案号.Size = new System.Drawing.Size(201, 20);
            this.txt个人档案号.StyleController = this.layoutControl1;
            this.txt个人档案号.TabIndex = 3;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(318, 73);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(461, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 10;
            // 
            // dte填表日期
            // 
            this.dte填表日期.EditValue = null;
            this.dte填表日期.Location = new System.Drawing.Point(58, 1489);
            this.dte填表日期.Name = "dte填表日期";
            this.dte填表日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte填表日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte填表日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte填表日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte填表日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte填表日期.Size = new System.Drawing.Size(191, 20);
            this.dte填表日期.StyleController = this.layoutControl1;
            this.dte填表日期.TabIndex = 178;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "老年人中医药健康管理服务记录表";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem1,
            this.emptySpaceItem2,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem24,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem30,
            this.layoutControlItem35,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem36,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem40,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layoutControlItem49,
            this.layoutControlItem45,
            this.layoutControlItem51,
            this.layoutControlItem52,
            this.layoutControlItem53,
            this.layoutControlItem54,
            this.layoutControlItem50,
            this.layoutControlItem56,
            this.layoutControlItem57,
            this.layoutControlItem59,
            this.layoutControlItem55,
            this.layoutControlItem58,
            this.layoutControlItem60,
            this.layoutControlItem61,
            this.layoutControlItem62,
            this.layoutControlItem63,
            this.layoutControlItem64,
            this.layoutControlItem65,
            this.layoutControlItem66,
            this.layoutControlItem67,
            this.layoutControlItem68,
            this.layoutControlItem69,
            this.layoutControlItem71,
            this.layoutControlItem72,
            this.layoutControlItem73,
            this.layoutControlItem74,
            this.layoutControlItem70,
            this.layoutControlItem75,
            this.layoutControlItem76,
            this.layoutControlItem77,
            this.layoutControlItem78,
            this.layoutControlItem79,
            this.layoutControlItem80,
            this.layoutControlItem81,
            this.layoutControlItem82,
            this.layoutControlItem83,
            this.layoutControlItem84,
            this.layoutControlItem85,
            this.layoutControlItem86,
            this.layoutControlItem87,
            this.layoutControlItem88,
            this.layoutControlItem89,
            this.layoutControlItem90,
            this.layoutControlItem91,
            this.layoutControlItem92,
            this.layoutControlItem93,
            this.layoutControlItem94,
            this.layoutControlItem96,
            this.layoutControlItem97,
            this.layoutControlItem98,
            this.layoutControlItem99,
            this.layoutControlItem100,
            this.layoutControlItem101,
            this.layoutControlItem102,
            this.layoutControlItem103,
            this.layoutControlItem104,
            this.layoutControlItem105,
            this.layoutControlItem106,
            this.layoutControlItem107,
            this.layoutControlItem108,
            this.layoutControlItem109,
            this.layoutControlItem110,
            this.layoutControlItem111,
            this.layoutControlItem112,
            this.layoutControlItem113,
            this.layoutControlItem114,
            this.layoutControlItem115,
            this.layoutControlItem116,
            this.layoutControlItem117,
            this.layoutControlItem118,
            this.layoutControlItem119,
            this.layoutControlItem120,
            this.layoutControlItem121,
            this.layoutControlItem122,
            this.layoutControlItem123,
            this.layoutControlItem124,
            this.layoutControlItem125,
            this.layoutControlItem126,
            this.layoutControlItem127,
            this.layoutControlItem128,
            this.layoutControlItem129,
            this.layoutControlItem130,
            this.layoutControlItem131,
            this.layoutControlItem132,
            this.layoutControlItem133,
            this.layoutControlItem134,
            this.layoutControlItem135,
            this.layoutControlItem136,
            this.layoutControlItem137,
            this.layoutControlItem138,
            this.layoutControlItem139,
            this.layoutControlItem140,
            this.layoutControlItem141,
            this.layoutControlItem142,
            this.layoutControlItem143,
            this.layoutControlItem144,
            this.layoutControlItem145,
            this.layoutControlItem146,
            this.layoutControlItem147,
            this.layoutControlItem148,
            this.layoutControlItem149,
            this.layoutControlItem150,
            this.layoutControlItem151,
            this.layoutControlItem152,
            this.layoutControlItem153,
            this.layoutControlItem154,
            this.layoutControlItem155,
            this.layoutControlItem156,
            this.layoutControlItem157,
            this.layoutControlItem158,
            this.layoutControlItem159,
            this.layoutControlItem160,
            this.layoutControlItem161,
            this.layoutControlItem162,
            this.layoutControlItem163,
            this.layoutControlItem164,
            this.layoutControlItem165,
            this.layoutControlItem166,
            this.layoutControlItem167,
            this.layoutControlItem168,
            this.layoutControlItem169,
            this.layoutControlItem170,
            this.layoutControlItem171,
            this.layoutControlItem172,
            this.layoutControlItem173,
            this.layoutControlItem174,
            this.layoutControlItem175,
            this.group体质,
            this.layoutControlItem205,
            this.layout1,
            this.layout2,
            this.layout3,
            this.layout4,
            this.layout5,
            this.layout6,
            this.layout7,
            this.layout8,
            this.layout9,
            this.layout10,
            this.layout11,
            this.layout12,
            this.layout13,
            this.layout14,
            this.layout15,
            this.layout16,
            this.layout17,
            this.layout18,
            this.layout19,
            this.layout20,
            this.layout21,
            this.layout22,
            this.layout24,
            this.layout23,
            this.layout25,
            this.layout26,
            this.layout27,
            this.layout28,
            this.layout29,
            this.layout30,
            this.layout31,
            this.layout32,
            this.layout33});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(782, 1560);
            this.layoutControlGroup1.Text = "老年人中医药健康管理服务记录表";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt个人档案号;
            this.layoutControlItem2.CustomizationFormText = "档案编号";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "档案编号";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt姓名;
            this.layoutControlItem3.CustomizationFormText = "姓 名 ";
            this.layoutControlItem3.Location = new System.Drawing.Point(260, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "姓 名 ";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt性别;
            this.layoutControlItem4.CustomizationFormText = "性 别 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(520, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem4.Text = "性 别 ";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt身份证号;
            this.layoutControlItem5.CustomizationFormText = "身份证号 ";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "身份证号 ";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt出生日期;
            this.layoutControlItem6.CustomizationFormText = "出生日期 ";
            this.layoutControlItem6.Location = new System.Drawing.Point(260, 24);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "出生日期 ";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt联系电话;
            this.layoutControlItem7.CustomizationFormText = "联系电话 ";
            this.layoutControlItem7.Location = new System.Drawing.Point(520, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem7.Text = "联系电话 ";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt居住状态;
            this.layoutControlItem8.CustomizationFormText = "居住状态 ";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "居住状态 ";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt居住地址;
            this.layoutControlItem1.CustomizationFormText = "居住地址 ";
            this.layoutControlItem1.Location = new System.Drawing.Point(260, 48);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(520, 24);
            this.layoutControlItem1.Text = "居住地址 ";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(52, 14);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem2.CustomizationFormText = "经常(相当/多数时间)";
            this.emptySpaceItem2.Location = new System.Drawing.Point(600, 72);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(70, 50);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(70, 50);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(70, 50);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "经常\r\n(相当/多数时间)";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem3.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem3.CustomizationFormText = "总是(非常/每天)";
            this.emptySpaceItem3.Location = new System.Drawing.Point(670, 72);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(70, 50);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(70, 50);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(110, 50);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "总是\r\n(非常/每天)";
            this.emptySpaceItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(52, 20);
            this.emptySpaceItem3.TextVisible = true;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem4.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem4.CustomizationFormText = "没有\r\n(根本不/从来没有)";
            this.emptySpaceItem4.Location = new System.Drawing.Point(390, 72);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(70, 50);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(70, 50);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(70, 50);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "没有\r\n(根本不/从来没有)";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(52, 0);
            this.emptySpaceItem4.TextVisible = true;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem5.CustomizationFormText = "很少\r\n(有一点/偶尔)";
            this.emptySpaceItem5.Location = new System.Drawing.Point(460, 72);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(70, 50);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(70, 50);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(70, 50);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "很少\r\n(有一点/偶尔)";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(52, 0);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem6.CustomizationFormText = "有时\r\n(有些/少数时间)";
            this.emptySpaceItem6.Location = new System.Drawing.Point(530, 72);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(70, 50);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(70, 50);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(70, 50);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "有时\r\n(有些/少数时间)";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(52, 0);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.chkA1;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(390, 122);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.chkA2;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(460, 122);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.chkA3;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(530, 122);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.checkEdit4;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(600, 122);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.checkEdit5;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(670, 122);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(110, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.checkEdit6;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(390, 150);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.checkEdit7;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(600, 150);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.checkEdit8;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(530, 150);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.checkEdit9;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(460, 150);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.checkEdit10;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(670, 150);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.checkEdit11;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(670, 190);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(110, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.checkEdit12;
            this.layoutControlItem20.CustomizationFormText = "layoutControlItem20";
            this.layoutControlItem20.Location = new System.Drawing.Point(600, 190);
            this.layoutControlItem20.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "layoutControlItem20";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextToControlDistance = 0;
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.checkEdit13;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(460, 190);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "layoutControlItem21";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.checkEdit14;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(530, 190);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "layoutControlItem22";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem22.TextToControlDistance = 0;
            this.layoutControlItem22.TextVisible = false;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.checkEdit15;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(390, 190);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.checkEdit17;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(460, 218);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.checkEdit18;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(530, 218);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "layoutControlItem26";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextToControlDistance = 0;
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.checkEdit19;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(600, 218);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "layoutControlItem27";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextToControlDistance = 0;
            this.layoutControlItem27.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.checkEdit20;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(670, 218);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "layoutControlItem28";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextToControlDistance = 0;
            this.layoutControlItem28.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.checkEdit16;
            this.layoutControlItem24.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem24.Location = new System.Drawing.Point(390, 218);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "layoutControlItem24";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem24.TextToControlDistance = 0;
            this.layoutControlItem24.TextVisible = false;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.checkEdit22;
            this.layoutControlItem31.CustomizationFormText = "layoutControlItem31";
            this.layoutControlItem31.Location = new System.Drawing.Point(460, 246);
            this.layoutControlItem31.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "layoutControlItem31";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextToControlDistance = 0;
            this.layoutControlItem31.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.checkEdit23;
            this.layoutControlItem32.CustomizationFormText = "layoutControlItem32";
            this.layoutControlItem32.Location = new System.Drawing.Point(530, 246);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "layoutControlItem32";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextToControlDistance = 0;
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.checkEdit24;
            this.layoutControlItem33.CustomizationFormText = "layoutControlItem33";
            this.layoutControlItem33.Location = new System.Drawing.Point(600, 246);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "layoutControlItem33";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextToControlDistance = 0;
            this.layoutControlItem33.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.checkEdit25;
            this.layoutControlItem34.CustomizationFormText = "layoutControlItem34";
            this.layoutControlItem34.Location = new System.Drawing.Point(670, 246);
            this.layoutControlItem34.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(110, 23);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "layoutControlItem34";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.checkEdit21;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(390, 246);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "layoutControlItem30";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextToControlDistance = 0;
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.checkEdit26;
            this.layoutControlItem35.CustomizationFormText = "layoutControlItem35";
            this.layoutControlItem35.Location = new System.Drawing.Point(460, 274);
            this.layoutControlItem35.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "layoutControlItem35";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextToControlDistance = 0;
            this.layoutControlItem35.TextVisible = false;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.checkEdit28;
            this.layoutControlItem37.CustomizationFormText = "layoutControlItem37";
            this.layoutControlItem37.Location = new System.Drawing.Point(530, 274);
            this.layoutControlItem37.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Text = "layoutControlItem37";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem37.TextToControlDistance = 0;
            this.layoutControlItem37.TextVisible = false;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.checkEdit29;
            this.layoutControlItem38.CustomizationFormText = "layoutControlItem38";
            this.layoutControlItem38.Location = new System.Drawing.Point(600, 274);
            this.layoutControlItem38.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "layoutControlItem38";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem38.TextToControlDistance = 0;
            this.layoutControlItem38.TextVisible = false;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.checkEdit30;
            this.layoutControlItem39.CustomizationFormText = "layoutControlItem39";
            this.layoutControlItem39.Location = new System.Drawing.Point(670, 274);
            this.layoutControlItem39.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(110, 23);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "layoutControlItem39";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem39.TextToControlDistance = 0;
            this.layoutControlItem39.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.checkEdit27;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem36";
            this.layoutControlItem36.Location = new System.Drawing.Point(390, 274);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "layoutControlItem36";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem36.TextToControlDistance = 0;
            this.layoutControlItem36.TextVisible = false;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.checkEdit32;
            this.layoutControlItem41.CustomizationFormText = "layoutControlItem41";
            this.layoutControlItem41.Location = new System.Drawing.Point(460, 302);
            this.layoutControlItem41.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem41.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem41.Text = "layoutControlItem41";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem41.TextToControlDistance = 0;
            this.layoutControlItem41.TextVisible = false;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.checkEdit33;
            this.layoutControlItem42.CustomizationFormText = "layoutControlItem42";
            this.layoutControlItem42.Location = new System.Drawing.Point(530, 302);
            this.layoutControlItem42.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Text = "layoutControlItem42";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem42.TextToControlDistance = 0;
            this.layoutControlItem42.TextVisible = false;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.checkEdit34;
            this.layoutControlItem43.CustomizationFormText = "layoutControlItem43";
            this.layoutControlItem43.Location = new System.Drawing.Point(600, 302);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "layoutControlItem43";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem43.TextToControlDistance = 0;
            this.layoutControlItem43.TextVisible = false;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.checkEdit35;
            this.layoutControlItem44.CustomizationFormText = "layoutControlItem44";
            this.layoutControlItem44.Location = new System.Drawing.Point(670, 302);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(110, 23);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "layoutControlItem44";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem44.TextToControlDistance = 0;
            this.layoutControlItem44.TextVisible = false;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.checkEdit31;
            this.layoutControlItem40.CustomizationFormText = "layoutControlItem40";
            this.layoutControlItem40.Location = new System.Drawing.Point(390, 302);
            this.layoutControlItem40.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "layoutControlItem40";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem40.TextToControlDistance = 0;
            this.layoutControlItem40.TextVisible = false;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.Control = this.checkEdit37;
            this.layoutControlItem46.CustomizationFormText = "layoutControlItem46";
            this.layoutControlItem46.Location = new System.Drawing.Point(460, 330);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "layoutControlItem46";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem46.TextToControlDistance = 0;
            this.layoutControlItem46.TextVisible = false;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.Control = this.checkEdit38;
            this.layoutControlItem47.CustomizationFormText = "layoutControlItem47";
            this.layoutControlItem47.Location = new System.Drawing.Point(530, 330);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "layoutControlItem47";
            this.layoutControlItem47.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem47.TextToControlDistance = 0;
            this.layoutControlItem47.TextVisible = false;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.checkEdit39;
            this.layoutControlItem48.CustomizationFormText = "layoutControlItem48";
            this.layoutControlItem48.Location = new System.Drawing.Point(600, 330);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "layoutControlItem48";
            this.layoutControlItem48.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem48.TextToControlDistance = 0;
            this.layoutControlItem48.TextVisible = false;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.checkEdit40;
            this.layoutControlItem49.CustomizationFormText = "layoutControlItem49";
            this.layoutControlItem49.Location = new System.Drawing.Point(670, 330);
            this.layoutControlItem49.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem49.MinSize = new System.Drawing.Size(110, 23);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem49.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem49.Text = "layoutControlItem49";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem49.TextToControlDistance = 0;
            this.layoutControlItem49.TextVisible = false;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.checkEdit36;
            this.layoutControlItem45.CustomizationFormText = "layoutControlItem45";
            this.layoutControlItem45.Location = new System.Drawing.Point(390, 330);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "layoutControlItem45";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem45.TextToControlDistance = 0;
            this.layoutControlItem45.TextVisible = false;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem51.Control = this.checkEdit42;
            this.layoutControlItem51.CustomizationFormText = "layoutControlItem51";
            this.layoutControlItem51.Location = new System.Drawing.Point(460, 358);
            this.layoutControlItem51.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem51.MinSize = new System.Drawing.Size(70, 40);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(70, 50);
            this.layoutControlItem51.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem51.Text = "layoutControlItem51";
            this.layoutControlItem51.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem51.TextToControlDistance = 0;
            this.layoutControlItem51.TextVisible = false;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem52.Control = this.checkEdit43;
            this.layoutControlItem52.CustomizationFormText = "layoutControlItem52";
            this.layoutControlItem52.Location = new System.Drawing.Point(530, 358);
            this.layoutControlItem52.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem52.MinSize = new System.Drawing.Size(70, 40);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(70, 50);
            this.layoutControlItem52.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem52.Text = "layoutControlItem52";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem52.TextToControlDistance = 0;
            this.layoutControlItem52.TextVisible = false;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem53.Control = this.checkEdit44;
            this.layoutControlItem53.CustomizationFormText = "layoutControlItem53";
            this.layoutControlItem53.Location = new System.Drawing.Point(600, 358);
            this.layoutControlItem53.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem53.MinSize = new System.Drawing.Size(70, 40);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(70, 50);
            this.layoutControlItem53.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem53.Text = "layoutControlItem53";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem53.TextToControlDistance = 0;
            this.layoutControlItem53.TextVisible = false;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem54.Control = this.checkEdit45;
            this.layoutControlItem54.CustomizationFormText = "layoutControlItem54";
            this.layoutControlItem54.Location = new System.Drawing.Point(670, 358);
            this.layoutControlItem54.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(110, 40);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(110, 50);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Text = "layoutControlItem54";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem54.TextToControlDistance = 0;
            this.layoutControlItem54.TextVisible = false;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem50.Control = this.checkEdit41;
            this.layoutControlItem50.CustomizationFormText = "layoutControlItem50";
            this.layoutControlItem50.Location = new System.Drawing.Point(390, 358);
            this.layoutControlItem50.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem50.MinSize = new System.Drawing.Size(70, 44);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(70, 50);
            this.layoutControlItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem50.Text = "layoutControlItem50";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem50.TextToControlDistance = 0;
            this.layoutControlItem50.TextVisible = false;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.Control = this.checkEdit47;
            this.layoutControlItem56.CustomizationFormText = "layoutControlItem56";
            this.layoutControlItem56.Location = new System.Drawing.Point(460, 408);
            this.layoutControlItem56.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem56.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem56.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem56.Text = "layoutControlItem56";
            this.layoutControlItem56.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem56.TextToControlDistance = 0;
            this.layoutControlItem56.TextVisible = false;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.Control = this.checkEdit48;
            this.layoutControlItem57.CustomizationFormText = "layoutControlItem57";
            this.layoutControlItem57.Location = new System.Drawing.Point(530, 408);
            this.layoutControlItem57.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem57.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem57.Text = "layoutControlItem57";
            this.layoutControlItem57.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem57.TextToControlDistance = 0;
            this.layoutControlItem57.TextVisible = false;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.Control = this.checkEdit50;
            this.layoutControlItem59.CustomizationFormText = "layoutControlItem59";
            this.layoutControlItem59.Location = new System.Drawing.Point(670, 408);
            this.layoutControlItem59.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem59.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem59.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem59.Text = "layoutControlItem59";
            this.layoutControlItem59.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem59.TextToControlDistance = 0;
            this.layoutControlItem59.TextVisible = false;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.Control = this.checkEdit46;
            this.layoutControlItem55.CustomizationFormText = "layoutControlItem55";
            this.layoutControlItem55.Location = new System.Drawing.Point(390, 408);
            this.layoutControlItem55.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem55.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem55.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem55.Text = "layoutControlItem55";
            this.layoutControlItem55.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem55.TextToControlDistance = 0;
            this.layoutControlItem55.TextVisible = false;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.Control = this.checkEdit49;
            this.layoutControlItem58.CustomizationFormText = "layoutControlItem58";
            this.layoutControlItem58.Location = new System.Drawing.Point(600, 408);
            this.layoutControlItem58.MaxSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem58.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem58.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem58.Text = "layoutControlItem58";
            this.layoutControlItem58.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem58.TextToControlDistance = 0;
            this.layoutControlItem58.TextVisible = false;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.Control = this.checkEdit51;
            this.layoutControlItem60.CustomizationFormText = "layoutControlItem60";
            this.layoutControlItem60.Location = new System.Drawing.Point(390, 436);
            this.layoutControlItem60.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem60.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem60.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem60.Text = "layoutControlItem60";
            this.layoutControlItem60.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem60.TextToControlDistance = 0;
            this.layoutControlItem60.TextVisible = false;
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.Control = this.checkEdit52;
            this.layoutControlItem61.CustomizationFormText = "layoutControlItem61";
            this.layoutControlItem61.Location = new System.Drawing.Point(460, 436);
            this.layoutControlItem61.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem61.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem61.Text = "layoutControlItem61";
            this.layoutControlItem61.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem61.TextToControlDistance = 0;
            this.layoutControlItem61.TextVisible = false;
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.Control = this.checkEdit53;
            this.layoutControlItem62.CustomizationFormText = "layoutControlItem62";
            this.layoutControlItem62.Location = new System.Drawing.Point(530, 436);
            this.layoutControlItem62.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem62.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem62.Name = "layoutControlItem62";
            this.layoutControlItem62.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem62.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem62.Text = "layoutControlItem62";
            this.layoutControlItem62.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem62.TextToControlDistance = 0;
            this.layoutControlItem62.TextVisible = false;
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.Control = this.checkEdit54;
            this.layoutControlItem63.CustomizationFormText = "layoutControlItem63";
            this.layoutControlItem63.Location = new System.Drawing.Point(600, 436);
            this.layoutControlItem63.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem63.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem63.Name = "layoutControlItem63";
            this.layoutControlItem63.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem63.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem63.Text = "layoutControlItem63";
            this.layoutControlItem63.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem63.TextToControlDistance = 0;
            this.layoutControlItem63.TextVisible = false;
            // 
            // layoutControlItem64
            // 
            this.layoutControlItem64.Control = this.checkEdit55;
            this.layoutControlItem64.CustomizationFormText = "layoutControlItem64";
            this.layoutControlItem64.Location = new System.Drawing.Point(670, 436);
            this.layoutControlItem64.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem64.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem64.Name = "layoutControlItem64";
            this.layoutControlItem64.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem64.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem64.Text = "layoutControlItem64";
            this.layoutControlItem64.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem64.TextToControlDistance = 0;
            this.layoutControlItem64.TextVisible = false;
            // 
            // layoutControlItem65
            // 
            this.layoutControlItem65.Control = this.checkEdit56;
            this.layoutControlItem65.CustomizationFormText = "layoutControlItem65";
            this.layoutControlItem65.Location = new System.Drawing.Point(390, 464);
            this.layoutControlItem65.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem65.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem65.Name = "layoutControlItem65";
            this.layoutControlItem65.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem65.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem65.Text = "layoutControlItem65";
            this.layoutControlItem65.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem65.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem65.TextToControlDistance = 0;
            this.layoutControlItem65.TextVisible = false;
            // 
            // layoutControlItem66
            // 
            this.layoutControlItem66.Control = this.checkEdit57;
            this.layoutControlItem66.CustomizationFormText = "layoutControlItem66";
            this.layoutControlItem66.Location = new System.Drawing.Point(460, 464);
            this.layoutControlItem66.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem66.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem66.Name = "layoutControlItem66";
            this.layoutControlItem66.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem66.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem66.Text = "layoutControlItem66";
            this.layoutControlItem66.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem66.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem66.TextToControlDistance = 0;
            this.layoutControlItem66.TextVisible = false;
            // 
            // layoutControlItem67
            // 
            this.layoutControlItem67.Control = this.checkEdit58;
            this.layoutControlItem67.CustomizationFormText = "layoutControlItem67";
            this.layoutControlItem67.Location = new System.Drawing.Point(530, 464);
            this.layoutControlItem67.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem67.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem67.Name = "layoutControlItem67";
            this.layoutControlItem67.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem67.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem67.Text = "layoutControlItem67";
            this.layoutControlItem67.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem67.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem67.TextToControlDistance = 0;
            this.layoutControlItem67.TextVisible = false;
            // 
            // layoutControlItem68
            // 
            this.layoutControlItem68.Control = this.checkEdit59;
            this.layoutControlItem68.CustomizationFormText = "layoutControlItem68";
            this.layoutControlItem68.Location = new System.Drawing.Point(600, 464);
            this.layoutControlItem68.MaxSize = new System.Drawing.Size(70, 0);
            this.layoutControlItem68.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem68.Name = "layoutControlItem68";
            this.layoutControlItem68.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem68.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem68.Text = "layoutControlItem68";
            this.layoutControlItem68.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem68.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem68.TextToControlDistance = 0;
            this.layoutControlItem68.TextVisible = false;
            // 
            // layoutControlItem69
            // 
            this.layoutControlItem69.Control = this.checkEdit60;
            this.layoutControlItem69.CustomizationFormText = "layoutControlItem69";
            this.layoutControlItem69.Location = new System.Drawing.Point(670, 464);
            this.layoutControlItem69.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem69.MinSize = new System.Drawing.Size(110, 23);
            this.layoutControlItem69.Name = "layoutControlItem69";
            this.layoutControlItem69.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem69.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem69.Text = "layoutControlItem69";
            this.layoutControlItem69.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem69.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem69.TextToControlDistance = 0;
            this.layoutControlItem69.TextVisible = false;
            // 
            // layoutControlItem71
            // 
            this.layoutControlItem71.Control = this.checkEdit62;
            this.layoutControlItem71.CustomizationFormText = "layoutControlItem71";
            this.layoutControlItem71.Location = new System.Drawing.Point(460, 504);
            this.layoutControlItem71.Name = "layoutControlItem71";
            this.layoutControlItem71.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem71.Text = "layoutControlItem71";
            this.layoutControlItem71.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem71.TextToControlDistance = 0;
            this.layoutControlItem71.TextVisible = false;
            // 
            // layoutControlItem72
            // 
            this.layoutControlItem72.Control = this.checkEdit63;
            this.layoutControlItem72.CustomizationFormText = "layoutControlItem72";
            this.layoutControlItem72.Location = new System.Drawing.Point(530, 504);
            this.layoutControlItem72.Name = "layoutControlItem72";
            this.layoutControlItem72.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem72.Text = "layoutControlItem72";
            this.layoutControlItem72.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem72.TextToControlDistance = 0;
            this.layoutControlItem72.TextVisible = false;
            // 
            // layoutControlItem73
            // 
            this.layoutControlItem73.Control = this.checkEdit64;
            this.layoutControlItem73.CustomizationFormText = "layoutControlItem73";
            this.layoutControlItem73.Location = new System.Drawing.Point(600, 504);
            this.layoutControlItem73.Name = "layoutControlItem73";
            this.layoutControlItem73.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem73.Text = "layoutControlItem73";
            this.layoutControlItem73.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem73.TextToControlDistance = 0;
            this.layoutControlItem73.TextVisible = false;
            // 
            // layoutControlItem74
            // 
            this.layoutControlItem74.Control = this.checkEdit65;
            this.layoutControlItem74.CustomizationFormText = "layoutControlItem74";
            this.layoutControlItem74.Location = new System.Drawing.Point(670, 504);
            this.layoutControlItem74.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem74.MinSize = new System.Drawing.Size(110, 31);
            this.layoutControlItem74.Name = "layoutControlItem74";
            this.layoutControlItem74.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem74.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem74.Text = "layoutControlItem74";
            this.layoutControlItem74.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem74.TextToControlDistance = 0;
            this.layoutControlItem74.TextVisible = false;
            // 
            // layoutControlItem70
            // 
            this.layoutControlItem70.Control = this.checkEdit61;
            this.layoutControlItem70.CustomizationFormText = "layoutControlItem70";
            this.layoutControlItem70.Location = new System.Drawing.Point(390, 504);
            this.layoutControlItem70.Name = "layoutControlItem70";
            this.layoutControlItem70.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem70.Text = "layoutControlItem70";
            this.layoutControlItem70.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem70.TextToControlDistance = 0;
            this.layoutControlItem70.TextVisible = false;
            // 
            // layoutControlItem75
            // 
            this.layoutControlItem75.Control = this.checkEdit66;
            this.layoutControlItem75.CustomizationFormText = "layoutControlItem75";
            this.layoutControlItem75.Location = new System.Drawing.Point(390, 544);
            this.layoutControlItem75.Name = "layoutControlItem75";
            this.layoutControlItem75.Size = new System.Drawing.Size(70, 50);
            this.layoutControlItem75.Text = "layoutControlItem75";
            this.layoutControlItem75.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem75.TextToControlDistance = 0;
            this.layoutControlItem75.TextVisible = false;
            // 
            // layoutControlItem76
            // 
            this.layoutControlItem76.Control = this.checkEdit67;
            this.layoutControlItem76.CustomizationFormText = "layoutControlItem76";
            this.layoutControlItem76.Location = new System.Drawing.Point(460, 544);
            this.layoutControlItem76.Name = "layoutControlItem76";
            this.layoutControlItem76.Size = new System.Drawing.Size(70, 50);
            this.layoutControlItem76.Text = "layoutControlItem76";
            this.layoutControlItem76.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem76.TextToControlDistance = 0;
            this.layoutControlItem76.TextVisible = false;
            // 
            // layoutControlItem77
            // 
            this.layoutControlItem77.Control = this.checkEdit68;
            this.layoutControlItem77.CustomizationFormText = "layoutControlItem77";
            this.layoutControlItem77.Location = new System.Drawing.Point(530, 544);
            this.layoutControlItem77.Name = "layoutControlItem77";
            this.layoutControlItem77.Size = new System.Drawing.Size(70, 50);
            this.layoutControlItem77.Text = "layoutControlItem77";
            this.layoutControlItem77.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem77.TextToControlDistance = 0;
            this.layoutControlItem77.TextVisible = false;
            // 
            // layoutControlItem78
            // 
            this.layoutControlItem78.Control = this.checkEdit69;
            this.layoutControlItem78.CustomizationFormText = "layoutControlItem78";
            this.layoutControlItem78.Location = new System.Drawing.Point(600, 544);
            this.layoutControlItem78.Name = "layoutControlItem78";
            this.layoutControlItem78.Size = new System.Drawing.Size(70, 50);
            this.layoutControlItem78.Text = "layoutControlItem78";
            this.layoutControlItem78.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem78.TextToControlDistance = 0;
            this.layoutControlItem78.TextVisible = false;
            // 
            // layoutControlItem79
            // 
            this.layoutControlItem79.Control = this.checkEdit70;
            this.layoutControlItem79.CustomizationFormText = "layoutControlItem79";
            this.layoutControlItem79.Location = new System.Drawing.Point(670, 544);
            this.layoutControlItem79.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem79.MinSize = new System.Drawing.Size(110, 36);
            this.layoutControlItem79.Name = "layoutControlItem79";
            this.layoutControlItem79.Size = new System.Drawing.Size(110, 50);
            this.layoutControlItem79.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem79.Text = "layoutControlItem79";
            this.layoutControlItem79.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem79.TextToControlDistance = 0;
            this.layoutControlItem79.TextVisible = false;
            // 
            // layoutControlItem80
            // 
            this.layoutControlItem80.Control = this.checkEdit71;
            this.layoutControlItem80.CustomizationFormText = "layoutControlItem80";
            this.layoutControlItem80.Location = new System.Drawing.Point(390, 594);
            this.layoutControlItem80.Name = "layoutControlItem80";
            this.layoutControlItem80.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem80.Text = "layoutControlItem80";
            this.layoutControlItem80.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem80.TextToControlDistance = 0;
            this.layoutControlItem80.TextVisible = false;
            // 
            // layoutControlItem81
            // 
            this.layoutControlItem81.Control = this.checkEdit72;
            this.layoutControlItem81.CustomizationFormText = "layoutControlItem81";
            this.layoutControlItem81.Location = new System.Drawing.Point(460, 594);
            this.layoutControlItem81.Name = "layoutControlItem81";
            this.layoutControlItem81.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem81.Text = "layoutControlItem81";
            this.layoutControlItem81.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem81.TextToControlDistance = 0;
            this.layoutControlItem81.TextVisible = false;
            // 
            // layoutControlItem82
            // 
            this.layoutControlItem82.Control = this.checkEdit73;
            this.layoutControlItem82.CustomizationFormText = "layoutControlItem82";
            this.layoutControlItem82.Location = new System.Drawing.Point(530, 594);
            this.layoutControlItem82.Name = "layoutControlItem82";
            this.layoutControlItem82.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem82.Text = "layoutControlItem82";
            this.layoutControlItem82.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem82.TextToControlDistance = 0;
            this.layoutControlItem82.TextVisible = false;
            // 
            // layoutControlItem83
            // 
            this.layoutControlItem83.Control = this.checkEdit74;
            this.layoutControlItem83.CustomizationFormText = "layoutControlItem83";
            this.layoutControlItem83.Location = new System.Drawing.Point(600, 594);
            this.layoutControlItem83.Name = "layoutControlItem83";
            this.layoutControlItem83.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem83.Text = "layoutControlItem83";
            this.layoutControlItem83.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem83.TextToControlDistance = 0;
            this.layoutControlItem83.TextVisible = false;
            // 
            // layoutControlItem84
            // 
            this.layoutControlItem84.Control = this.checkEdit75;
            this.layoutControlItem84.CustomizationFormText = "layoutControlItem84";
            this.layoutControlItem84.Location = new System.Drawing.Point(670, 594);
            this.layoutControlItem84.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem84.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem84.Name = "layoutControlItem84";
            this.layoutControlItem84.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem84.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem84.Text = "layoutControlItem84";
            this.layoutControlItem84.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem84.TextToControlDistance = 0;
            this.layoutControlItem84.TextVisible = false;
            // 
            // layoutControlItem85
            // 
            this.layoutControlItem85.Control = this.checkEdit76;
            this.layoutControlItem85.CustomizationFormText = "layoutControlItem85";
            this.layoutControlItem85.Location = new System.Drawing.Point(390, 622);
            this.layoutControlItem85.Name = "layoutControlItem85";
            this.layoutControlItem85.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem85.Text = "layoutControlItem85";
            this.layoutControlItem85.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem85.TextToControlDistance = 0;
            this.layoutControlItem85.TextVisible = false;
            // 
            // layoutControlItem86
            // 
            this.layoutControlItem86.Control = this.checkEdit77;
            this.layoutControlItem86.CustomizationFormText = "layoutControlItem86";
            this.layoutControlItem86.Location = new System.Drawing.Point(460, 622);
            this.layoutControlItem86.Name = "layoutControlItem86";
            this.layoutControlItem86.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem86.Text = "layoutControlItem86";
            this.layoutControlItem86.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem86.TextToControlDistance = 0;
            this.layoutControlItem86.TextVisible = false;
            // 
            // layoutControlItem87
            // 
            this.layoutControlItem87.Control = this.checkEdit78;
            this.layoutControlItem87.CustomizationFormText = "layoutControlItem87";
            this.layoutControlItem87.Location = new System.Drawing.Point(530, 622);
            this.layoutControlItem87.Name = "layoutControlItem87";
            this.layoutControlItem87.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem87.Text = "layoutControlItem87";
            this.layoutControlItem87.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem87.TextToControlDistance = 0;
            this.layoutControlItem87.TextVisible = false;
            // 
            // layoutControlItem88
            // 
            this.layoutControlItem88.Control = this.checkEdit79;
            this.layoutControlItem88.CustomizationFormText = "layoutControlItem88";
            this.layoutControlItem88.Location = new System.Drawing.Point(600, 622);
            this.layoutControlItem88.Name = "layoutControlItem88";
            this.layoutControlItem88.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem88.Text = "layoutControlItem88";
            this.layoutControlItem88.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem88.TextToControlDistance = 0;
            this.layoutControlItem88.TextVisible = false;
            // 
            // layoutControlItem89
            // 
            this.layoutControlItem89.Control = this.checkEdit80;
            this.layoutControlItem89.CustomizationFormText = "layoutControlItem89";
            this.layoutControlItem89.Location = new System.Drawing.Point(670, 622);
            this.layoutControlItem89.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem89.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem89.Name = "layoutControlItem89";
            this.layoutControlItem89.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem89.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem89.Text = "layoutControlItem89";
            this.layoutControlItem89.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem89.TextToControlDistance = 0;
            this.layoutControlItem89.TextVisible = false;
            // 
            // layoutControlItem90
            // 
            this.layoutControlItem90.Control = this.checkEdit81;
            this.layoutControlItem90.CustomizationFormText = "layoutControlItem90";
            this.layoutControlItem90.Location = new System.Drawing.Point(390, 650);
            this.layoutControlItem90.Name = "layoutControlItem90";
            this.layoutControlItem90.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem90.Text = "layoutControlItem90";
            this.layoutControlItem90.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem90.TextToControlDistance = 0;
            this.layoutControlItem90.TextVisible = false;
            // 
            // layoutControlItem91
            // 
            this.layoutControlItem91.Control = this.checkEdit82;
            this.layoutControlItem91.CustomizationFormText = "layoutControlItem91";
            this.layoutControlItem91.Location = new System.Drawing.Point(460, 650);
            this.layoutControlItem91.Name = "layoutControlItem91";
            this.layoutControlItem91.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem91.Text = "layoutControlItem91";
            this.layoutControlItem91.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem91.TextToControlDistance = 0;
            this.layoutControlItem91.TextVisible = false;
            // 
            // layoutControlItem92
            // 
            this.layoutControlItem92.Control = this.checkEdit83;
            this.layoutControlItem92.CustomizationFormText = "layoutControlItem92";
            this.layoutControlItem92.Location = new System.Drawing.Point(530, 650);
            this.layoutControlItem92.Name = "layoutControlItem92";
            this.layoutControlItem92.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem92.Text = "layoutControlItem92";
            this.layoutControlItem92.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem92.TextToControlDistance = 0;
            this.layoutControlItem92.TextVisible = false;
            // 
            // layoutControlItem93
            // 
            this.layoutControlItem93.Control = this.checkEdit84;
            this.layoutControlItem93.CustomizationFormText = "layoutControlItem93";
            this.layoutControlItem93.Location = new System.Drawing.Point(600, 650);
            this.layoutControlItem93.Name = "layoutControlItem93";
            this.layoutControlItem93.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem93.Text = "layoutControlItem93";
            this.layoutControlItem93.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem93.TextToControlDistance = 0;
            this.layoutControlItem93.TextVisible = false;
            // 
            // layoutControlItem94
            // 
            this.layoutControlItem94.Control = this.checkEdit85;
            this.layoutControlItem94.CustomizationFormText = "layoutControlItem94";
            this.layoutControlItem94.Location = new System.Drawing.Point(670, 650);
            this.layoutControlItem94.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem94.MinSize = new System.Drawing.Size(110, 36);
            this.layoutControlItem94.Name = "layoutControlItem94";
            this.layoutControlItem94.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem94.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem94.Text = "layoutControlItem94";
            this.layoutControlItem94.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem94.TextToControlDistance = 0;
            this.layoutControlItem94.TextVisible = false;
            // 
            // layoutControlItem96
            // 
            this.layoutControlItem96.Control = this.checkEdit86;
            this.layoutControlItem96.CustomizationFormText = "layoutControlItem96";
            this.layoutControlItem96.Location = new System.Drawing.Point(390, 690);
            this.layoutControlItem96.Name = "layoutControlItem96";
            this.layoutControlItem96.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem96.Text = "layoutControlItem96";
            this.layoutControlItem96.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem96.TextToControlDistance = 0;
            this.layoutControlItem96.TextVisible = false;
            // 
            // layoutControlItem97
            // 
            this.layoutControlItem97.Control = this.checkEdit87;
            this.layoutControlItem97.CustomizationFormText = "layoutControlItem97";
            this.layoutControlItem97.Location = new System.Drawing.Point(460, 690);
            this.layoutControlItem97.Name = "layoutControlItem97";
            this.layoutControlItem97.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem97.Text = "layoutControlItem97";
            this.layoutControlItem97.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem97.TextToControlDistance = 0;
            this.layoutControlItem97.TextVisible = false;
            // 
            // layoutControlItem98
            // 
            this.layoutControlItem98.Control = this.checkEdit88;
            this.layoutControlItem98.CustomizationFormText = "layoutControlItem98";
            this.layoutControlItem98.Location = new System.Drawing.Point(530, 690);
            this.layoutControlItem98.Name = "layoutControlItem98";
            this.layoutControlItem98.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem98.Text = "layoutControlItem98";
            this.layoutControlItem98.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem98.TextToControlDistance = 0;
            this.layoutControlItem98.TextVisible = false;
            // 
            // layoutControlItem99
            // 
            this.layoutControlItem99.Control = this.checkEdit89;
            this.layoutControlItem99.CustomizationFormText = "layoutControlItem99";
            this.layoutControlItem99.Location = new System.Drawing.Point(600, 690);
            this.layoutControlItem99.Name = "layoutControlItem99";
            this.layoutControlItem99.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem99.Text = "layoutControlItem99";
            this.layoutControlItem99.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem99.TextToControlDistance = 0;
            this.layoutControlItem99.TextVisible = false;
            // 
            // layoutControlItem100
            // 
            this.layoutControlItem100.Control = this.checkEdit90;
            this.layoutControlItem100.CustomizationFormText = "layoutControlItem100";
            this.layoutControlItem100.Location = new System.Drawing.Point(670, 690);
            this.layoutControlItem100.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem100.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem100.Name = "layoutControlItem100";
            this.layoutControlItem100.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem100.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem100.Text = "layoutControlItem100";
            this.layoutControlItem100.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem100.TextToControlDistance = 0;
            this.layoutControlItem100.TextVisible = false;
            // 
            // layoutControlItem101
            // 
            this.layoutControlItem101.Control = this.checkEdit91;
            this.layoutControlItem101.CustomizationFormText = "layoutControlItem101";
            this.layoutControlItem101.Location = new System.Drawing.Point(390, 718);
            this.layoutControlItem101.Name = "layoutControlItem101";
            this.layoutControlItem101.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem101.Text = "layoutControlItem101";
            this.layoutControlItem101.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem101.TextToControlDistance = 0;
            this.layoutControlItem101.TextVisible = false;
            // 
            // layoutControlItem102
            // 
            this.layoutControlItem102.Control = this.checkEdit92;
            this.layoutControlItem102.CustomizationFormText = "layoutControlItem102";
            this.layoutControlItem102.Location = new System.Drawing.Point(460, 718);
            this.layoutControlItem102.Name = "layoutControlItem102";
            this.layoutControlItem102.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem102.Text = "layoutControlItem102";
            this.layoutControlItem102.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem102.TextToControlDistance = 0;
            this.layoutControlItem102.TextVisible = false;
            // 
            // layoutControlItem103
            // 
            this.layoutControlItem103.Control = this.checkEdit93;
            this.layoutControlItem103.CustomizationFormText = "layoutControlItem103";
            this.layoutControlItem103.Location = new System.Drawing.Point(530, 718);
            this.layoutControlItem103.Name = "layoutControlItem103";
            this.layoutControlItem103.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem103.Text = "layoutControlItem103";
            this.layoutControlItem103.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem103.TextToControlDistance = 0;
            this.layoutControlItem103.TextVisible = false;
            // 
            // layoutControlItem104
            // 
            this.layoutControlItem104.Control = this.checkEdit94;
            this.layoutControlItem104.CustomizationFormText = "layoutControlItem104";
            this.layoutControlItem104.Location = new System.Drawing.Point(600, 718);
            this.layoutControlItem104.Name = "layoutControlItem104";
            this.layoutControlItem104.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem104.Text = "layoutControlItem104";
            this.layoutControlItem104.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem104.TextToControlDistance = 0;
            this.layoutControlItem104.TextVisible = false;
            // 
            // layoutControlItem105
            // 
            this.layoutControlItem105.Control = this.checkEdit95;
            this.layoutControlItem105.CustomizationFormText = "layoutControlItem105";
            this.layoutControlItem105.Location = new System.Drawing.Point(670, 718);
            this.layoutControlItem105.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem105.MinSize = new System.Drawing.Size(110, 23);
            this.layoutControlItem105.Name = "layoutControlItem105";
            this.layoutControlItem105.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem105.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem105.Text = "layoutControlItem105";
            this.layoutControlItem105.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem105.TextToControlDistance = 0;
            this.layoutControlItem105.TextVisible = false;
            // 
            // layoutControlItem106
            // 
            this.layoutControlItem106.Control = this.checkEdit96;
            this.layoutControlItem106.CustomizationFormText = "layoutControlItem106";
            this.layoutControlItem106.Location = new System.Drawing.Point(390, 758);
            this.layoutControlItem106.Name = "layoutControlItem106";
            this.layoutControlItem106.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem106.Text = "layoutControlItem106";
            this.layoutControlItem106.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem106.TextToControlDistance = 0;
            this.layoutControlItem106.TextVisible = false;
            // 
            // layoutControlItem107
            // 
            this.layoutControlItem107.Control = this.checkEdit97;
            this.layoutControlItem107.CustomizationFormText = "layoutControlItem107";
            this.layoutControlItem107.Location = new System.Drawing.Point(460, 758);
            this.layoutControlItem107.Name = "layoutControlItem107";
            this.layoutControlItem107.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem107.Text = "layoutControlItem107";
            this.layoutControlItem107.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem107.TextToControlDistance = 0;
            this.layoutControlItem107.TextVisible = false;
            // 
            // layoutControlItem108
            // 
            this.layoutControlItem108.Control = this.checkEdit98;
            this.layoutControlItem108.CustomizationFormText = "layoutControlItem108";
            this.layoutControlItem108.Location = new System.Drawing.Point(530, 758);
            this.layoutControlItem108.Name = "layoutControlItem108";
            this.layoutControlItem108.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem108.Text = "layoutControlItem108";
            this.layoutControlItem108.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem108.TextToControlDistance = 0;
            this.layoutControlItem108.TextVisible = false;
            // 
            // layoutControlItem109
            // 
            this.layoutControlItem109.Control = this.checkEdit99;
            this.layoutControlItem109.CustomizationFormText = "layoutControlItem109";
            this.layoutControlItem109.Location = new System.Drawing.Point(600, 758);
            this.layoutControlItem109.Name = "layoutControlItem109";
            this.layoutControlItem109.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem109.Text = "layoutControlItem109";
            this.layoutControlItem109.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem109.TextToControlDistance = 0;
            this.layoutControlItem109.TextVisible = false;
            // 
            // layoutControlItem110
            // 
            this.layoutControlItem110.Control = this.checkEdit100;
            this.layoutControlItem110.CustomizationFormText = "layoutControlItem110";
            this.layoutControlItem110.Location = new System.Drawing.Point(670, 758);
            this.layoutControlItem110.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem110.MinSize = new System.Drawing.Size(110, 23);
            this.layoutControlItem110.Name = "layoutControlItem110";
            this.layoutControlItem110.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem110.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem110.Text = "layoutControlItem110";
            this.layoutControlItem110.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem110.TextToControlDistance = 0;
            this.layoutControlItem110.TextVisible = false;
            // 
            // layoutControlItem111
            // 
            this.layoutControlItem111.Control = this.checkEdit101;
            this.layoutControlItem111.CustomizationFormText = "layoutControlItem111";
            this.layoutControlItem111.Location = new System.Drawing.Point(390, 798);
            this.layoutControlItem111.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem111.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem111.Name = "layoutControlItem111";
            this.layoutControlItem111.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem111.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem111.Text = "layoutControlItem111";
            this.layoutControlItem111.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem111.TextToControlDistance = 0;
            this.layoutControlItem111.TextVisible = false;
            // 
            // layoutControlItem112
            // 
            this.layoutControlItem112.Control = this.checkEdit102;
            this.layoutControlItem112.CustomizationFormText = "layoutControlItem112";
            this.layoutControlItem112.Location = new System.Drawing.Point(460, 798);
            this.layoutControlItem112.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem112.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem112.Name = "layoutControlItem112";
            this.layoutControlItem112.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem112.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem112.Text = "layoutControlItem112";
            this.layoutControlItem112.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem112.TextToControlDistance = 0;
            this.layoutControlItem112.TextVisible = false;
            // 
            // layoutControlItem113
            // 
            this.layoutControlItem113.Control = this.checkEdit103;
            this.layoutControlItem113.CustomizationFormText = "layoutControlItem113";
            this.layoutControlItem113.Location = new System.Drawing.Point(530, 798);
            this.layoutControlItem113.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem113.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem113.Name = "layoutControlItem113";
            this.layoutControlItem113.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem113.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem113.Text = "layoutControlItem113";
            this.layoutControlItem113.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem113.TextToControlDistance = 0;
            this.layoutControlItem113.TextVisible = false;
            // 
            // layoutControlItem114
            // 
            this.layoutControlItem114.Control = this.checkEdit104;
            this.layoutControlItem114.CustomizationFormText = "layoutControlItem114";
            this.layoutControlItem114.Location = new System.Drawing.Point(600, 798);
            this.layoutControlItem114.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem114.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem114.Name = "layoutControlItem114";
            this.layoutControlItem114.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem114.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem114.Text = "layoutControlItem114";
            this.layoutControlItem114.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem114.TextToControlDistance = 0;
            this.layoutControlItem114.TextVisible = false;
            // 
            // layoutControlItem115
            // 
            this.layoutControlItem115.Control = this.checkEdit105;
            this.layoutControlItem115.CustomizationFormText = "layoutControlItem115";
            this.layoutControlItem115.Location = new System.Drawing.Point(670, 798);
            this.layoutControlItem115.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem115.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem115.Name = "layoutControlItem115";
            this.layoutControlItem115.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem115.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem115.Text = "layoutControlItem115";
            this.layoutControlItem115.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem115.TextToControlDistance = 0;
            this.layoutControlItem115.TextVisible = false;
            // 
            // layoutControlItem116
            // 
            this.layoutControlItem116.Control = this.checkEdit106;
            this.layoutControlItem116.CustomizationFormText = "layoutControlItem116";
            this.layoutControlItem116.Location = new System.Drawing.Point(390, 826);
            this.layoutControlItem116.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem116.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem116.Name = "layoutControlItem116";
            this.layoutControlItem116.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem116.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem116.Text = "layoutControlItem116";
            this.layoutControlItem116.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem116.TextToControlDistance = 0;
            this.layoutControlItem116.TextVisible = false;
            // 
            // layoutControlItem117
            // 
            this.layoutControlItem117.Control = this.checkEdit107;
            this.layoutControlItem117.CustomizationFormText = "layoutControlItem117";
            this.layoutControlItem117.Location = new System.Drawing.Point(460, 826);
            this.layoutControlItem117.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem117.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem117.Name = "layoutControlItem117";
            this.layoutControlItem117.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem117.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem117.Text = "layoutControlItem117";
            this.layoutControlItem117.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem117.TextToControlDistance = 0;
            this.layoutControlItem117.TextVisible = false;
            // 
            // layoutControlItem118
            // 
            this.layoutControlItem118.Control = this.checkEdit108;
            this.layoutControlItem118.CustomizationFormText = "layoutControlItem118";
            this.layoutControlItem118.Location = new System.Drawing.Point(530, 826);
            this.layoutControlItem118.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem118.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem118.Name = "layoutControlItem118";
            this.layoutControlItem118.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem118.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem118.Text = "layoutControlItem118";
            this.layoutControlItem118.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem118.TextToControlDistance = 0;
            this.layoutControlItem118.TextVisible = false;
            // 
            // layoutControlItem119
            // 
            this.layoutControlItem119.Control = this.checkEdit109;
            this.layoutControlItem119.CustomizationFormText = "layoutControlItem119";
            this.layoutControlItem119.Location = new System.Drawing.Point(600, 826);
            this.layoutControlItem119.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem119.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem119.Name = "layoutControlItem119";
            this.layoutControlItem119.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem119.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem119.Text = "layoutControlItem119";
            this.layoutControlItem119.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem119.TextToControlDistance = 0;
            this.layoutControlItem119.TextVisible = false;
            // 
            // layoutControlItem120
            // 
            this.layoutControlItem120.Control = this.checkEdit110;
            this.layoutControlItem120.CustomizationFormText = "layoutControlItem120";
            this.layoutControlItem120.Location = new System.Drawing.Point(670, 826);
            this.layoutControlItem120.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem120.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem120.Name = "layoutControlItem120";
            this.layoutControlItem120.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem120.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem120.Text = "layoutControlItem120";
            this.layoutControlItem120.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem120.TextToControlDistance = 0;
            this.layoutControlItem120.TextVisible = false;
            // 
            // layoutControlItem121
            // 
            this.layoutControlItem121.Control = this.checkEdit111;
            this.layoutControlItem121.CustomizationFormText = "layoutControlItem121";
            this.layoutControlItem121.Location = new System.Drawing.Point(390, 854);
            this.layoutControlItem121.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem121.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem121.Name = "layoutControlItem121";
            this.layoutControlItem121.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem121.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem121.Text = "layoutControlItem121";
            this.layoutControlItem121.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem121.TextToControlDistance = 0;
            this.layoutControlItem121.TextVisible = false;
            // 
            // layoutControlItem122
            // 
            this.layoutControlItem122.Control = this.checkEdit112;
            this.layoutControlItem122.CustomizationFormText = "layoutControlItem122";
            this.layoutControlItem122.Location = new System.Drawing.Point(460, 854);
            this.layoutControlItem122.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem122.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem122.Name = "layoutControlItem122";
            this.layoutControlItem122.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem122.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem122.Text = "layoutControlItem122";
            this.layoutControlItem122.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem122.TextToControlDistance = 0;
            this.layoutControlItem122.TextVisible = false;
            // 
            // layoutControlItem123
            // 
            this.layoutControlItem123.Control = this.checkEdit113;
            this.layoutControlItem123.CustomizationFormText = "layoutControlItem123";
            this.layoutControlItem123.Location = new System.Drawing.Point(530, 854);
            this.layoutControlItem123.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem123.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem123.Name = "layoutControlItem123";
            this.layoutControlItem123.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem123.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem123.Text = "layoutControlItem123";
            this.layoutControlItem123.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem123.TextToControlDistance = 0;
            this.layoutControlItem123.TextVisible = false;
            // 
            // layoutControlItem124
            // 
            this.layoutControlItem124.Control = this.checkEdit114;
            this.layoutControlItem124.CustomizationFormText = "layoutControlItem124";
            this.layoutControlItem124.Location = new System.Drawing.Point(600, 854);
            this.layoutControlItem124.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem124.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem124.Name = "layoutControlItem124";
            this.layoutControlItem124.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem124.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem124.Text = "layoutControlItem124";
            this.layoutControlItem124.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem124.TextToControlDistance = 0;
            this.layoutControlItem124.TextVisible = false;
            // 
            // layoutControlItem125
            // 
            this.layoutControlItem125.Control = this.checkEdit115;
            this.layoutControlItem125.CustomizationFormText = "layoutControlItem125";
            this.layoutControlItem125.Location = new System.Drawing.Point(670, 854);
            this.layoutControlItem125.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem125.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem125.Name = "layoutControlItem125";
            this.layoutControlItem125.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem125.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem125.Text = "layoutControlItem125";
            this.layoutControlItem125.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem125.TextToControlDistance = 0;
            this.layoutControlItem125.TextVisible = false;
            // 
            // layoutControlItem126
            // 
            this.layoutControlItem126.Control = this.checkEdit116;
            this.layoutControlItem126.CustomizationFormText = "layoutControlItem126";
            this.layoutControlItem126.Location = new System.Drawing.Point(390, 882);
            this.layoutControlItem126.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem126.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem126.Name = "layoutControlItem126";
            this.layoutControlItem126.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem126.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem126.Text = "layoutControlItem126";
            this.layoutControlItem126.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem126.TextToControlDistance = 0;
            this.layoutControlItem126.TextVisible = false;
            // 
            // layoutControlItem127
            // 
            this.layoutControlItem127.Control = this.checkEdit117;
            this.layoutControlItem127.CustomizationFormText = "layoutControlItem127";
            this.layoutControlItem127.Location = new System.Drawing.Point(460, 882);
            this.layoutControlItem127.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem127.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem127.Name = "layoutControlItem127";
            this.layoutControlItem127.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem127.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem127.Text = "layoutControlItem127";
            this.layoutControlItem127.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem127.TextToControlDistance = 0;
            this.layoutControlItem127.TextVisible = false;
            // 
            // layoutControlItem128
            // 
            this.layoutControlItem128.Control = this.checkEdit118;
            this.layoutControlItem128.CustomizationFormText = "layoutControlItem128";
            this.layoutControlItem128.Location = new System.Drawing.Point(530, 882);
            this.layoutControlItem128.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem128.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem128.Name = "layoutControlItem128";
            this.layoutControlItem128.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem128.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem128.Text = "layoutControlItem128";
            this.layoutControlItem128.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem128.TextToControlDistance = 0;
            this.layoutControlItem128.TextVisible = false;
            // 
            // layoutControlItem129
            // 
            this.layoutControlItem129.Control = this.checkEdit119;
            this.layoutControlItem129.CustomizationFormText = "layoutControlItem129";
            this.layoutControlItem129.Location = new System.Drawing.Point(600, 882);
            this.layoutControlItem129.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem129.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem129.Name = "layoutControlItem129";
            this.layoutControlItem129.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem129.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem129.Text = "layoutControlItem129";
            this.layoutControlItem129.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem129.TextToControlDistance = 0;
            this.layoutControlItem129.TextVisible = false;
            // 
            // layoutControlItem130
            // 
            this.layoutControlItem130.Control = this.checkEdit120;
            this.layoutControlItem130.CustomizationFormText = "layoutControlItem130";
            this.layoutControlItem130.Location = new System.Drawing.Point(670, 882);
            this.layoutControlItem130.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem130.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem130.Name = "layoutControlItem130";
            this.layoutControlItem130.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem130.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem130.Text = "layoutControlItem130";
            this.layoutControlItem130.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem130.TextToControlDistance = 0;
            this.layoutControlItem130.TextVisible = false;
            // 
            // layoutControlItem131
            // 
            this.layoutControlItem131.Control = this.checkEdit121;
            this.layoutControlItem131.CustomizationFormText = "layoutControlItem131";
            this.layoutControlItem131.Location = new System.Drawing.Point(390, 910);
            this.layoutControlItem131.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem131.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem131.Name = "layoutControlItem131";
            this.layoutControlItem131.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem131.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem131.Text = "layoutControlItem131";
            this.layoutControlItem131.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem131.TextToControlDistance = 0;
            this.layoutControlItem131.TextVisible = false;
            // 
            // layoutControlItem132
            // 
            this.layoutControlItem132.Control = this.checkEdit122;
            this.layoutControlItem132.CustomizationFormText = "layoutControlItem132";
            this.layoutControlItem132.Location = new System.Drawing.Point(460, 910);
            this.layoutControlItem132.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem132.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem132.Name = "layoutControlItem132";
            this.layoutControlItem132.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem132.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem132.Text = "layoutControlItem132";
            this.layoutControlItem132.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem132.TextToControlDistance = 0;
            this.layoutControlItem132.TextVisible = false;
            // 
            // layoutControlItem133
            // 
            this.layoutControlItem133.Control = this.checkEdit123;
            this.layoutControlItem133.CustomizationFormText = "layoutControlItem133";
            this.layoutControlItem133.Location = new System.Drawing.Point(530, 910);
            this.layoutControlItem133.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem133.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem133.Name = "layoutControlItem133";
            this.layoutControlItem133.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem133.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem133.Text = "layoutControlItem133";
            this.layoutControlItem133.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem133.TextToControlDistance = 0;
            this.layoutControlItem133.TextVisible = false;
            // 
            // layoutControlItem134
            // 
            this.layoutControlItem134.Control = this.checkEdit124;
            this.layoutControlItem134.CustomizationFormText = "layoutControlItem134";
            this.layoutControlItem134.Location = new System.Drawing.Point(600, 910);
            this.layoutControlItem134.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem134.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem134.Name = "layoutControlItem134";
            this.layoutControlItem134.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem134.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem134.Text = "layoutControlItem134";
            this.layoutControlItem134.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem134.TextToControlDistance = 0;
            this.layoutControlItem134.TextVisible = false;
            // 
            // layoutControlItem135
            // 
            this.layoutControlItem135.Control = this.checkEdit125;
            this.layoutControlItem135.CustomizationFormText = "layoutControlItem135";
            this.layoutControlItem135.Location = new System.Drawing.Point(670, 910);
            this.layoutControlItem135.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem135.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem135.Name = "layoutControlItem135";
            this.layoutControlItem135.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem135.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem135.Text = "layoutControlItem135";
            this.layoutControlItem135.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem135.TextToControlDistance = 0;
            this.layoutControlItem135.TextVisible = false;
            // 
            // layoutControlItem136
            // 
            this.layoutControlItem136.Control = this.checkEdit126;
            this.layoutControlItem136.CustomizationFormText = "layoutControlItem136";
            this.layoutControlItem136.Location = new System.Drawing.Point(390, 938);
            this.layoutControlItem136.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem136.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem136.Name = "layoutControlItem136";
            this.layoutControlItem136.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem136.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem136.Text = "layoutControlItem136";
            this.layoutControlItem136.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem136.TextToControlDistance = 0;
            this.layoutControlItem136.TextVisible = false;
            // 
            // layoutControlItem137
            // 
            this.layoutControlItem137.Control = this.checkEdit127;
            this.layoutControlItem137.CustomizationFormText = "layoutControlItem137";
            this.layoutControlItem137.Location = new System.Drawing.Point(460, 938);
            this.layoutControlItem137.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem137.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem137.Name = "layoutControlItem137";
            this.layoutControlItem137.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem137.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem137.Text = "layoutControlItem137";
            this.layoutControlItem137.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem137.TextToControlDistance = 0;
            this.layoutControlItem137.TextVisible = false;
            // 
            // layoutControlItem138
            // 
            this.layoutControlItem138.Control = this.checkEdit128;
            this.layoutControlItem138.CustomizationFormText = "layoutControlItem138";
            this.layoutControlItem138.Location = new System.Drawing.Point(530, 938);
            this.layoutControlItem138.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem138.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem138.Name = "layoutControlItem138";
            this.layoutControlItem138.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem138.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem138.Text = "layoutControlItem138";
            this.layoutControlItem138.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem138.TextToControlDistance = 0;
            this.layoutControlItem138.TextVisible = false;
            // 
            // layoutControlItem139
            // 
            this.layoutControlItem139.Control = this.checkEdit129;
            this.layoutControlItem139.CustomizationFormText = "layoutControlItem139";
            this.layoutControlItem139.Location = new System.Drawing.Point(600, 938);
            this.layoutControlItem139.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem139.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem139.Name = "layoutControlItem139";
            this.layoutControlItem139.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem139.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem139.Text = "layoutControlItem139";
            this.layoutControlItem139.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem139.TextToControlDistance = 0;
            this.layoutControlItem139.TextVisible = false;
            // 
            // layoutControlItem140
            // 
            this.layoutControlItem140.Control = this.checkEdit130;
            this.layoutControlItem140.CustomizationFormText = "layoutControlItem140";
            this.layoutControlItem140.Location = new System.Drawing.Point(670, 938);
            this.layoutControlItem140.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem140.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem140.Name = "layoutControlItem140";
            this.layoutControlItem140.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem140.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem140.Text = "layoutControlItem140";
            this.layoutControlItem140.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem140.TextToControlDistance = 0;
            this.layoutControlItem140.TextVisible = false;
            // 
            // layoutControlItem141
            // 
            this.layoutControlItem141.Control = this.checkEdit131;
            this.layoutControlItem141.CustomizationFormText = "layoutControlItem141";
            this.layoutControlItem141.Location = new System.Drawing.Point(390, 966);
            this.layoutControlItem141.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem141.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem141.Name = "layoutControlItem141";
            this.layoutControlItem141.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem141.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem141.Text = "layoutControlItem141";
            this.layoutControlItem141.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem141.TextToControlDistance = 0;
            this.layoutControlItem141.TextVisible = false;
            // 
            // layoutControlItem142
            // 
            this.layoutControlItem142.Control = this.checkEdit132;
            this.layoutControlItem142.CustomizationFormText = "layoutControlItem142";
            this.layoutControlItem142.Location = new System.Drawing.Point(460, 966);
            this.layoutControlItem142.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem142.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem142.Name = "layoutControlItem142";
            this.layoutControlItem142.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem142.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem142.Text = "layoutControlItem142";
            this.layoutControlItem142.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem142.TextToControlDistance = 0;
            this.layoutControlItem142.TextVisible = false;
            // 
            // layoutControlItem143
            // 
            this.layoutControlItem143.Control = this.checkEdit133;
            this.layoutControlItem143.CustomizationFormText = "layoutControlItem143";
            this.layoutControlItem143.Location = new System.Drawing.Point(530, 966);
            this.layoutControlItem143.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem143.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem143.Name = "layoutControlItem143";
            this.layoutControlItem143.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem143.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem143.Text = "layoutControlItem143";
            this.layoutControlItem143.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem143.TextToControlDistance = 0;
            this.layoutControlItem143.TextVisible = false;
            // 
            // layoutControlItem144
            // 
            this.layoutControlItem144.Control = this.checkEdit134;
            this.layoutControlItem144.CustomizationFormText = "layoutControlItem144";
            this.layoutControlItem144.Location = new System.Drawing.Point(600, 966);
            this.layoutControlItem144.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem144.MinSize = new System.Drawing.Size(70, 28);
            this.layoutControlItem144.Name = "layoutControlItem144";
            this.layoutControlItem144.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem144.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem144.Text = "layoutControlItem144";
            this.layoutControlItem144.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem144.TextToControlDistance = 0;
            this.layoutControlItem144.TextVisible = false;
            // 
            // layoutControlItem145
            // 
            this.layoutControlItem145.Control = this.checkEdit135;
            this.layoutControlItem145.CustomizationFormText = "layoutControlItem145";
            this.layoutControlItem145.Location = new System.Drawing.Point(670, 966);
            this.layoutControlItem145.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem145.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem145.Name = "layoutControlItem145";
            this.layoutControlItem145.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem145.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem145.Text = "layoutControlItem145";
            this.layoutControlItem145.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem145.TextToControlDistance = 0;
            this.layoutControlItem145.TextVisible = false;
            // 
            // layoutControlItem146
            // 
            this.layoutControlItem146.Control = this.checkEdit136;
            this.layoutControlItem146.CustomizationFormText = "layoutControlItem146";
            this.layoutControlItem146.Location = new System.Drawing.Point(390, 994);
            this.layoutControlItem146.MinSize = new System.Drawing.Size(70, 78);
            this.layoutControlItem146.Name = "layoutControlItem146";
            this.layoutControlItem146.Size = new System.Drawing.Size(70, 78);
            this.layoutControlItem146.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem146.Text = "layoutControlItem146";
            this.layoutControlItem146.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem146.TextToControlDistance = 0;
            this.layoutControlItem146.TextVisible = false;
            // 
            // layoutControlItem147
            // 
            this.layoutControlItem147.Control = this.checkEdit137;
            this.layoutControlItem147.CustomizationFormText = "layoutControlItem147";
            this.layoutControlItem147.Location = new System.Drawing.Point(460, 994);
            this.layoutControlItem147.MinSize = new System.Drawing.Size(70, 78);
            this.layoutControlItem147.Name = "layoutControlItem147";
            this.layoutControlItem147.Size = new System.Drawing.Size(70, 78);
            this.layoutControlItem147.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem147.Text = "layoutControlItem147";
            this.layoutControlItem147.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem147.TextToControlDistance = 0;
            this.layoutControlItem147.TextVisible = false;
            // 
            // layoutControlItem148
            // 
            this.layoutControlItem148.Control = this.checkEdit138;
            this.layoutControlItem148.CustomizationFormText = "layoutControlItem148";
            this.layoutControlItem148.Location = new System.Drawing.Point(530, 994);
            this.layoutControlItem148.MinSize = new System.Drawing.Size(70, 78);
            this.layoutControlItem148.Name = "layoutControlItem148";
            this.layoutControlItem148.Size = new System.Drawing.Size(70, 78);
            this.layoutControlItem148.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem148.Text = "layoutControlItem148";
            this.layoutControlItem148.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem148.TextToControlDistance = 0;
            this.layoutControlItem148.TextVisible = false;
            // 
            // layoutControlItem149
            // 
            this.layoutControlItem149.Control = this.checkEdit139;
            this.layoutControlItem149.CustomizationFormText = "layoutControlItem149";
            this.layoutControlItem149.Location = new System.Drawing.Point(600, 994);
            this.layoutControlItem149.MinSize = new System.Drawing.Size(70, 78);
            this.layoutControlItem149.Name = "layoutControlItem149";
            this.layoutControlItem149.Size = new System.Drawing.Size(70, 78);
            this.layoutControlItem149.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem149.Text = "layoutControlItem149";
            this.layoutControlItem149.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem149.TextToControlDistance = 0;
            this.layoutControlItem149.TextVisible = false;
            // 
            // layoutControlItem150
            // 
            this.layoutControlItem150.Control = this.checkEdit140;
            this.layoutControlItem150.CustomizationFormText = "layoutControlItem150";
            this.layoutControlItem150.Location = new System.Drawing.Point(670, 994);
            this.layoutControlItem150.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem150.MinSize = new System.Drawing.Size(110, 36);
            this.layoutControlItem150.Name = "layoutControlItem150";
            this.layoutControlItem150.Size = new System.Drawing.Size(110, 78);
            this.layoutControlItem150.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem150.Text = "layoutControlItem150";
            this.layoutControlItem150.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem150.TextToControlDistance = 0;
            this.layoutControlItem150.TextVisible = false;
            // 
            // layoutControlItem151
            // 
            this.layoutControlItem151.Control = this.checkEdit141;
            this.layoutControlItem151.CustomizationFormText = "layoutControlItem151";
            this.layoutControlItem151.Location = new System.Drawing.Point(390, 1072);
            this.layoutControlItem151.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem151.Name = "layoutControlItem151";
            this.layoutControlItem151.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem151.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem151.Text = "layoutControlItem151";
            this.layoutControlItem151.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem151.TextToControlDistance = 0;
            this.layoutControlItem151.TextVisible = false;
            // 
            // layoutControlItem152
            // 
            this.layoutControlItem152.Control = this.checkEdit142;
            this.layoutControlItem152.CustomizationFormText = "layoutControlItem152";
            this.layoutControlItem152.Location = new System.Drawing.Point(460, 1072);
            this.layoutControlItem152.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem152.Name = "layoutControlItem152";
            this.layoutControlItem152.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem152.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem152.Text = "layoutControlItem152";
            this.layoutControlItem152.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem152.TextToControlDistance = 0;
            this.layoutControlItem152.TextVisible = false;
            // 
            // layoutControlItem153
            // 
            this.layoutControlItem153.Control = this.checkEdit143;
            this.layoutControlItem153.CustomizationFormText = "layoutControlItem153";
            this.layoutControlItem153.Location = new System.Drawing.Point(530, 1072);
            this.layoutControlItem153.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem153.Name = "layoutControlItem153";
            this.layoutControlItem153.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem153.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem153.Text = "layoutControlItem153";
            this.layoutControlItem153.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem153.TextToControlDistance = 0;
            this.layoutControlItem153.TextVisible = false;
            // 
            // layoutControlItem154
            // 
            this.layoutControlItem154.Control = this.checkEdit144;
            this.layoutControlItem154.CustomizationFormText = "layoutControlItem154";
            this.layoutControlItem154.Location = new System.Drawing.Point(600, 1072);
            this.layoutControlItem154.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem154.Name = "layoutControlItem154";
            this.layoutControlItem154.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem154.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem154.Text = "layoutControlItem154";
            this.layoutControlItem154.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem154.TextToControlDistance = 0;
            this.layoutControlItem154.TextVisible = false;
            // 
            // layoutControlItem155
            // 
            this.layoutControlItem155.Control = this.checkEdit145;
            this.layoutControlItem155.CustomizationFormText = "layoutControlItem155";
            this.layoutControlItem155.Location = new System.Drawing.Point(670, 1072);
            this.layoutControlItem155.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem155.MinSize = new System.Drawing.Size(110, 23);
            this.layoutControlItem155.Name = "layoutControlItem155";
            this.layoutControlItem155.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem155.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem155.Text = "layoutControlItem155";
            this.layoutControlItem155.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem155.TextToControlDistance = 0;
            this.layoutControlItem155.TextVisible = false;
            // 
            // layoutControlItem156
            // 
            this.layoutControlItem156.Control = this.checkEdit146;
            this.layoutControlItem156.CustomizationFormText = "layoutControlItem156";
            this.layoutControlItem156.Location = new System.Drawing.Point(390, 1112);
            this.layoutControlItem156.Name = "layoutControlItem156";
            this.layoutControlItem156.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem156.Text = "layoutControlItem156";
            this.layoutControlItem156.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem156.TextToControlDistance = 0;
            this.layoutControlItem156.TextVisible = false;
            // 
            // layoutControlItem157
            // 
            this.layoutControlItem157.Control = this.checkEdit147;
            this.layoutControlItem157.CustomizationFormText = "layoutControlItem157";
            this.layoutControlItem157.Location = new System.Drawing.Point(460, 1112);
            this.layoutControlItem157.Name = "layoutControlItem157";
            this.layoutControlItem157.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem157.Text = "layoutControlItem157";
            this.layoutControlItem157.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem157.TextToControlDistance = 0;
            this.layoutControlItem157.TextVisible = false;
            // 
            // layoutControlItem158
            // 
            this.layoutControlItem158.Control = this.checkEdit148;
            this.layoutControlItem158.CustomizationFormText = "layoutControlItem158";
            this.layoutControlItem158.Location = new System.Drawing.Point(530, 1112);
            this.layoutControlItem158.Name = "layoutControlItem158";
            this.layoutControlItem158.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem158.Text = "layoutControlItem158";
            this.layoutControlItem158.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem158.TextToControlDistance = 0;
            this.layoutControlItem158.TextVisible = false;
            // 
            // layoutControlItem159
            // 
            this.layoutControlItem159.Control = this.checkEdit149;
            this.layoutControlItem159.CustomizationFormText = "layoutControlItem159";
            this.layoutControlItem159.Location = new System.Drawing.Point(600, 1112);
            this.layoutControlItem159.Name = "layoutControlItem159";
            this.layoutControlItem159.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem159.Text = "layoutControlItem159";
            this.layoutControlItem159.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem159.TextToControlDistance = 0;
            this.layoutControlItem159.TextVisible = false;
            // 
            // layoutControlItem160
            // 
            this.layoutControlItem160.Control = this.checkEdit150;
            this.layoutControlItem160.CustomizationFormText = "layoutControlItem160";
            this.layoutControlItem160.Location = new System.Drawing.Point(670, 1112);
            this.layoutControlItem160.MaxSize = new System.Drawing.Size(110, 40);
            this.layoutControlItem160.MinSize = new System.Drawing.Size(110, 40);
            this.layoutControlItem160.Name = "layoutControlItem160";
            this.layoutControlItem160.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem160.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem160.Text = "layoutControlItem160";
            this.layoutControlItem160.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem160.TextToControlDistance = 0;
            this.layoutControlItem160.TextVisible = false;
            // 
            // layoutControlItem161
            // 
            this.layoutControlItem161.Control = this.checkEdit151;
            this.layoutControlItem161.CustomizationFormText = "layoutControlItem161";
            this.layoutControlItem161.Location = new System.Drawing.Point(390, 1152);
            this.layoutControlItem161.Name = "layoutControlItem161";
            this.layoutControlItem161.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem161.Text = "layoutControlItem161";
            this.layoutControlItem161.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem161.TextToControlDistance = 0;
            this.layoutControlItem161.TextVisible = false;
            // 
            // layoutControlItem162
            // 
            this.layoutControlItem162.Control = this.checkEdit152;
            this.layoutControlItem162.CustomizationFormText = "layoutControlItem162";
            this.layoutControlItem162.Location = new System.Drawing.Point(460, 1152);
            this.layoutControlItem162.Name = "layoutControlItem162";
            this.layoutControlItem162.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem162.Text = "layoutControlItem162";
            this.layoutControlItem162.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem162.TextToControlDistance = 0;
            this.layoutControlItem162.TextVisible = false;
            // 
            // layoutControlItem163
            // 
            this.layoutControlItem163.Control = this.checkEdit153;
            this.layoutControlItem163.CustomizationFormText = "layoutControlItem163";
            this.layoutControlItem163.Location = new System.Drawing.Point(530, 1152);
            this.layoutControlItem163.Name = "layoutControlItem163";
            this.layoutControlItem163.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem163.Text = "layoutControlItem163";
            this.layoutControlItem163.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem163.TextToControlDistance = 0;
            this.layoutControlItem163.TextVisible = false;
            // 
            // layoutControlItem164
            // 
            this.layoutControlItem164.Control = this.checkEdit154;
            this.layoutControlItem164.CustomizationFormText = "layoutControlItem164";
            this.layoutControlItem164.Location = new System.Drawing.Point(600, 1152);
            this.layoutControlItem164.Name = "layoutControlItem164";
            this.layoutControlItem164.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem164.Text = "layoutControlItem164";
            this.layoutControlItem164.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem164.TextToControlDistance = 0;
            this.layoutControlItem164.TextVisible = false;
            // 
            // layoutControlItem165
            // 
            this.layoutControlItem165.Control = this.checkEdit155;
            this.layoutControlItem165.CustomizationFormText = "layoutControlItem165";
            this.layoutControlItem165.Location = new System.Drawing.Point(670, 1152);
            this.layoutControlItem165.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem165.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem165.Name = "layoutControlItem165";
            this.layoutControlItem165.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem165.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem165.Text = "layoutControlItem165";
            this.layoutControlItem165.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem165.TextToControlDistance = 0;
            this.layoutControlItem165.TextVisible = false;
            // 
            // layoutControlItem166
            // 
            this.layoutControlItem166.Control = this.checkEdit156;
            this.layoutControlItem166.CustomizationFormText = "layoutControlItem166";
            this.layoutControlItem166.Location = new System.Drawing.Point(390, 1180);
            this.layoutControlItem166.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem166.Name = "layoutControlItem166";
            this.layoutControlItem166.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem166.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem166.Text = "layoutControlItem166";
            this.layoutControlItem166.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem166.TextToControlDistance = 0;
            this.layoutControlItem166.TextVisible = false;
            // 
            // layoutControlItem167
            // 
            this.layoutControlItem167.Control = this.checkEdit157;
            this.layoutControlItem167.CustomizationFormText = "layoutControlItem167";
            this.layoutControlItem167.Location = new System.Drawing.Point(460, 1180);
            this.layoutControlItem167.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem167.Name = "layoutControlItem167";
            this.layoutControlItem167.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem167.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem167.Text = "layoutControlItem167";
            this.layoutControlItem167.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem167.TextToControlDistance = 0;
            this.layoutControlItem167.TextVisible = false;
            // 
            // layoutControlItem168
            // 
            this.layoutControlItem168.Control = this.checkEdit158;
            this.layoutControlItem168.CustomizationFormText = "layoutControlItem168";
            this.layoutControlItem168.Location = new System.Drawing.Point(530, 1180);
            this.layoutControlItem168.MinSize = new System.Drawing.Size(70, 23);
            this.layoutControlItem168.Name = "layoutControlItem168";
            this.layoutControlItem168.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem168.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem168.Text = "layoutControlItem168";
            this.layoutControlItem168.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem168.TextToControlDistance = 0;
            this.layoutControlItem168.TextVisible = false;
            // 
            // layoutControlItem169
            // 
            this.layoutControlItem169.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem169.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem169.Control = this.checkEdit159;
            this.layoutControlItem169.CustomizationFormText = "layoutControlItem169";
            this.layoutControlItem169.Location = new System.Drawing.Point(600, 1180);
            this.layoutControlItem169.MinSize = new System.Drawing.Size(70, 1);
            this.layoutControlItem169.Name = "layoutControlItem169";
            this.layoutControlItem169.Size = new System.Drawing.Size(70, 40);
            this.layoutControlItem169.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem169.Text = "layoutControlItem169";
            this.layoutControlItem169.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem169.TextToControlDistance = 0;
            this.layoutControlItem169.TextVisible = false;
            // 
            // layoutControlItem170
            // 
            this.layoutControlItem170.Control = this.checkEdit160;
            this.layoutControlItem170.CustomizationFormText = "layoutControlItem170";
            this.layoutControlItem170.Location = new System.Drawing.Point(670, 1180);
            this.layoutControlItem170.MaxSize = new System.Drawing.Size(110, 0);
            this.layoutControlItem170.MinSize = new System.Drawing.Size(110, 1);
            this.layoutControlItem170.Name = "layoutControlItem170";
            this.layoutControlItem170.Size = new System.Drawing.Size(110, 40);
            this.layoutControlItem170.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem170.Text = "layoutControlItem170";
            this.layoutControlItem170.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem170.TextToControlDistance = 0;
            this.layoutControlItem170.TextVisible = false;
            // 
            // layoutControlItem171
            // 
            this.layoutControlItem171.Control = this.checkEdit161;
            this.layoutControlItem171.CustomizationFormText = "layoutControlItem171";
            this.layoutControlItem171.Location = new System.Drawing.Point(390, 1220);
            this.layoutControlItem171.Name = "layoutControlItem171";
            this.layoutControlItem171.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem171.Text = "layoutControlItem171";
            this.layoutControlItem171.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem171.TextToControlDistance = 0;
            this.layoutControlItem171.TextVisible = false;
            // 
            // layoutControlItem172
            // 
            this.layoutControlItem172.Control = this.checkEdit162;
            this.layoutControlItem172.CustomizationFormText = "layoutControlItem172";
            this.layoutControlItem172.Location = new System.Drawing.Point(460, 1220);
            this.layoutControlItem172.Name = "layoutControlItem172";
            this.layoutControlItem172.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem172.Text = "layoutControlItem172";
            this.layoutControlItem172.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem172.TextToControlDistance = 0;
            this.layoutControlItem172.TextVisible = false;
            // 
            // layoutControlItem173
            // 
            this.layoutControlItem173.Control = this.checkEdit163;
            this.layoutControlItem173.CustomizationFormText = "layoutControlItem173";
            this.layoutControlItem173.Location = new System.Drawing.Point(530, 1220);
            this.layoutControlItem173.Name = "layoutControlItem173";
            this.layoutControlItem173.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem173.Text = "layoutControlItem173";
            this.layoutControlItem173.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem173.TextToControlDistance = 0;
            this.layoutControlItem173.TextVisible = false;
            // 
            // layoutControlItem174
            // 
            this.layoutControlItem174.Control = this.checkEdit164;
            this.layoutControlItem174.CustomizationFormText = "layoutControlItem174";
            this.layoutControlItem174.Location = new System.Drawing.Point(600, 1220);
            this.layoutControlItem174.Name = "layoutControlItem174";
            this.layoutControlItem174.Size = new System.Drawing.Size(70, 28);
            this.layoutControlItem174.Text = "layoutControlItem174";
            this.layoutControlItem174.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem174.TextToControlDistance = 0;
            this.layoutControlItem174.TextVisible = false;
            // 
            // layoutControlItem175
            // 
            this.layoutControlItem175.Control = this.checkEdit165;
            this.layoutControlItem175.CustomizationFormText = "layoutControlItem175";
            this.layoutControlItem175.Location = new System.Drawing.Point(670, 1220);
            this.layoutControlItem175.MaxSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem175.MinSize = new System.Drawing.Size(110, 28);
            this.layoutControlItem175.Name = "layoutControlItem175";
            this.layoutControlItem175.Size = new System.Drawing.Size(110, 28);
            this.layoutControlItem175.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem175.Text = "layoutControlItem175";
            this.layoutControlItem175.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem175.TextToControlDistance = 0;
            this.layoutControlItem175.TextVisible = false;
            // 
            // group体质
            // 
            this.group体质.CustomizationFormText = "layoutControlGroup2";
            this.group体质.GroupBordersVisible = false;
            this.group体质.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem40,
            this.emptySpaceItem51,
            this.lbl填表日期,
            this.lbl医生签名,
            this.layoutControlGroup3,
            this.layoutControlItem176,
            this.layoutControlItem177,
            this.layoutControlItem179,
            this.layoutControlItem181,
            this.layoutControlItem182,
            this.layoutControlItem183,
            this.layoutControlItem184,
            this.layoutControlItem185,
            this.layoutControlItem186,
            this.layoutControlItem187,
            this.layoutControlItem188,
            this.layoutControlItem189,
            this.layoutControlItem190,
            this.layoutControlItem191,
            this.layoutControlItem192,
            this.layoutControlItem193,
            this.layoutControlItem194,
            this.layoutControlItem195,
            this.layoutControlItem196,
            this.layoutControlItem197,
            this.layoutControlItem198,
            this.layoutControlItem199,
            this.layoutControlItem200,
            this.layoutControlItem201,
            this.layoutControlItem202,
            this.layoutControlItem203,
            this.layoutControlItem204,
            this.layoutControlItem239,
            this.layoutControlItem240,
            this.layoutControlItem241,
            this.layoutControlItem242,
            this.layoutControlItem243,
            this.layoutControlItem244,
            this.layoutControlItem245,
            this.layoutControlItem246,
            this.layoutControlItem247,
            this.layoutControlItem248});
            this.group体质.Location = new System.Drawing.Point(0, 1248);
            this.group体质.Name = "group体质";
            this.group体质.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.group体质.Size = new System.Drawing.Size(780, 288);
            this.group体质.Text = "group体质";
            // 
            // emptySpaceItem40
            // 
            this.emptySpaceItem40.AllowHotTrack = false;
            this.emptySpaceItem40.CustomizationFormText = "体质类型";
            this.emptySpaceItem40.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem40.MaxSize = new System.Drawing.Size(55, 0);
            this.emptySpaceItem40.MinSize = new System.Drawing.Size(55, 24);
            this.emptySpaceItem40.Name = "emptySpaceItem40";
            this.emptySpaceItem40.Size = new System.Drawing.Size(55, 24);
            this.emptySpaceItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem40.Text = "体质类型";
            this.emptySpaceItem40.TextSize = new System.Drawing.Size(52, 0);
            this.emptySpaceItem40.TextVisible = true;
            // 
            // emptySpaceItem51
            // 
            this.emptySpaceItem51.AllowHotTrack = false;
            this.emptySpaceItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem51.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem51.CustomizationFormText = "中医药保健指导";
            this.emptySpaceItem51.Location = new System.Drawing.Point(0, 66);
            this.emptySpaceItem51.MaxSize = new System.Drawing.Size(55, 150);
            this.emptySpaceItem51.MinSize = new System.Drawing.Size(55, 150);
            this.emptySpaceItem51.Name = "emptySpaceItem51";
            this.emptySpaceItem51.Size = new System.Drawing.Size(55, 150);
            this.emptySpaceItem51.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem51.Text = "中医药保健指导";
            this.emptySpaceItem51.TextSize = new System.Drawing.Size(52, 0);
            this.emptySpaceItem51.TextVisible = true;
            // 
            // lbl填表日期
            // 
            this.lbl填表日期.Control = this.dte填表日期;
            this.lbl填表日期.CustomizationFormText = "填表日期";
            this.lbl填表日期.Location = new System.Drawing.Point(0, 216);
            this.lbl填表日期.MaxSize = new System.Drawing.Size(250, 24);
            this.lbl填表日期.MinSize = new System.Drawing.Size(133, 24);
            this.lbl填表日期.Name = "lbl填表日期";
            this.lbl填表日期.Size = new System.Drawing.Size(250, 24);
            this.lbl填表日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl填表日期.Text = "随访日期";
            this.lbl填表日期.TextSize = new System.Drawing.Size(52, 14);
            // 
            // lbl医生签名
            // 
            this.lbl医生签名.Control = this.txt医生签名;
            this.lbl医生签名.CustomizationFormText = "医生签名";
            this.lbl医生签名.Location = new System.Drawing.Point(250, 216);
            this.lbl医生签名.MaxSize = new System.Drawing.Size(250, 24);
            this.lbl医生签名.MinSize = new System.Drawing.Size(250, 24);
            this.lbl医生签名.Name = "lbl医生签名";
            this.lbl医生签名.Size = new System.Drawing.Size(530, 24);
            this.lbl医生签名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl医生签名.Text = "医生签名";
            this.lbl医生签名.TextSize = new System.Drawing.Size(52, 14);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem178,
            this.lbl最近更新时间,
            this.lbl当前所属机构,
            this.lbl创建机构,
            this.lbl创建人,
            this.lbl最近修改人});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 240);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(780, 48);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            // 
            // layoutControlItem178
            // 
            this.layoutControlItem178.Control = this.txt创建时间;
            this.layoutControlItem178.CustomizationFormText = "创建时间 ";
            this.layoutControlItem178.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem178.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem178.MinSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem178.Name = "layoutControlItem178";
            this.layoutControlItem178.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem178.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem178.Text = "录入时间 ";
            this.layoutControlItem178.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem178.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem178.TextToControlDistance = 5;
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.Control = this.txt最近更新时间;
            this.lbl最近更新时间.CustomizationFormText = "最近更新时间 ";
            this.lbl最近更新时间.Location = new System.Drawing.Point(250, 0);
            this.lbl最近更新时间.MaxSize = new System.Drawing.Size(250, 24);
            this.lbl最近更新时间.MinSize = new System.Drawing.Size(250, 24);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(250, 24);
            this.lbl最近更新时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl最近更新时间.Text = "最近更新时间 ";
            this.lbl最近更新时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl最近更新时间.TextSize = new System.Drawing.Size(76, 14);
            this.lbl最近更新时间.TextToControlDistance = 5;
            // 
            // lbl当前所属机构
            // 
            this.lbl当前所属机构.Control = this.txt当前所属机构;
            this.lbl当前所属机构.CustomizationFormText = "当前所属机构 ";
            this.lbl当前所属机构.Location = new System.Drawing.Point(500, 0);
            this.lbl当前所属机构.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl当前所属机构.MinSize = new System.Drawing.Size(133, 24);
            this.lbl当前所属机构.Name = "lbl当前所属机构";
            this.lbl当前所属机构.Size = new System.Drawing.Size(280, 24);
            this.lbl当前所属机构.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl当前所属机构.Text = "当前所属机构 ";
            this.lbl当前所属机构.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl当前所属机构.TextSize = new System.Drawing.Size(76, 14);
            this.lbl当前所属机构.TextToControlDistance = 5;
            // 
            // lbl创建机构
            // 
            this.lbl创建机构.Control = this.txt创建机构;
            this.lbl创建机构.CustomizationFormText = "创建机构 ";
            this.lbl创建机构.Location = new System.Drawing.Point(0, 24);
            this.lbl创建机构.MaxSize = new System.Drawing.Size(250, 24);
            this.lbl创建机构.MinSize = new System.Drawing.Size(250, 24);
            this.lbl创建机构.Name = "lbl创建机构";
            this.lbl创建机构.Size = new System.Drawing.Size(250, 24);
            this.lbl创建机构.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl创建机构.Text = "创建机构 ";
            this.lbl创建机构.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl创建机构.TextSize = new System.Drawing.Size(80, 14);
            this.lbl创建机构.TextToControlDistance = 5;
            // 
            // lbl创建人
            // 
            this.lbl创建人.Control = this.txt创建人;
            this.lbl创建人.CustomizationFormText = "创建人 ";
            this.lbl创建人.Location = new System.Drawing.Point(250, 24);
            this.lbl创建人.MaxSize = new System.Drawing.Size(250, 24);
            this.lbl创建人.MinSize = new System.Drawing.Size(250, 24);
            this.lbl创建人.Name = "lbl创建人";
            this.lbl创建人.Size = new System.Drawing.Size(250, 24);
            this.lbl创建人.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl创建人.Text = "创建人 ";
            this.lbl创建人.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl创建人.TextSize = new System.Drawing.Size(76, 14);
            this.lbl创建人.TextToControlDistance = 5;
            // 
            // lbl最近修改人
            // 
            this.lbl最近修改人.Control = this.txt最近修改人;
            this.lbl最近修改人.CustomizationFormText = "最近修改人 ";
            this.lbl最近修改人.Location = new System.Drawing.Point(500, 24);
            this.lbl最近修改人.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl最近修改人.MinSize = new System.Drawing.Size(133, 24);
            this.lbl最近修改人.Name = "lbl最近修改人";
            this.lbl最近修改人.Size = new System.Drawing.Size(280, 24);
            this.lbl最近修改人.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl最近修改人.Text = "最近修改人 ";
            this.lbl最近修改人.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl最近修改人.TextSize = new System.Drawing.Size(76, 14);
            this.lbl最近修改人.TextToControlDistance = 5;
            // 
            // layoutControlItem176
            // 
            this.layoutControlItem176.Control = this.ucqxdf;
            this.layoutControlItem176.CustomizationFormText = "layoutControlItem176";
            this.layoutControlItem176.Location = new System.Drawing.Point(55, 24);
            this.layoutControlItem176.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem176.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem176.Name = "layoutControlItem176";
            this.layoutControlItem176.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem176.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem176.Text = "layoutControlItem176";
            this.layoutControlItem176.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem176.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem176.TextToControlDistance = 0;
            this.layoutControlItem176.TextVisible = false;
            // 
            // layoutControlItem177
            // 
            this.layoutControlItem177.Control = this.ucyangdf;
            this.layoutControlItem177.CustomizationFormText = "layoutControlItem177";
            this.layoutControlItem177.Location = new System.Drawing.Point(135, 24);
            this.layoutControlItem177.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem177.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem177.Name = "layoutControlItem177";
            this.layoutControlItem177.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem177.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem177.Text = "layoutControlItem177";
            this.layoutControlItem177.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem177.TextToControlDistance = 0;
            this.layoutControlItem177.TextVisible = false;
            // 
            // layoutControlItem179
            // 
            this.layoutControlItem179.Control = this.ucyindf;
            this.layoutControlItem179.CustomizationFormText = "layoutControlItem179";
            this.layoutControlItem179.Location = new System.Drawing.Point(215, 24);
            this.layoutControlItem179.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem179.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem179.Name = "layoutControlItem179";
            this.layoutControlItem179.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem179.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem179.Text = "layoutControlItem179";
            this.layoutControlItem179.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem179.TextToControlDistance = 0;
            this.layoutControlItem179.TextVisible = false;
            // 
            // layoutControlItem181
            // 
            this.layoutControlItem181.Control = this.uctansdf;
            this.layoutControlItem181.CustomizationFormText = "layoutControlItem181";
            this.layoutControlItem181.Location = new System.Drawing.Point(295, 24);
            this.layoutControlItem181.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem181.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem181.Name = "layoutControlItem181";
            this.layoutControlItem181.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem181.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem181.Text = "layoutControlItem181";
            this.layoutControlItem181.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem181.TextToControlDistance = 0;
            this.layoutControlItem181.TextVisible = false;
            // 
            // layoutControlItem182
            // 
            this.layoutControlItem182.Control = this.ucshirdf;
            this.layoutControlItem182.CustomizationFormText = "layoutControlItem182";
            this.layoutControlItem182.Location = new System.Drawing.Point(375, 24);
            this.layoutControlItem182.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem182.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem182.Name = "layoutControlItem182";
            this.layoutControlItem182.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem182.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem182.Text = "layoutControlItem182";
            this.layoutControlItem182.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem182.TextToControlDistance = 0;
            this.layoutControlItem182.TextVisible = false;
            // 
            // layoutControlItem183
            // 
            this.layoutControlItem183.Control = this.ucxueydf;
            this.layoutControlItem183.CustomizationFormText = "layoutControlItem183";
            this.layoutControlItem183.Location = new System.Drawing.Point(455, 24);
            this.layoutControlItem183.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem183.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem183.Name = "layoutControlItem183";
            this.layoutControlItem183.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem183.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem183.Text = "layoutControlItem183";
            this.layoutControlItem183.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem183.TextToControlDistance = 0;
            this.layoutControlItem183.TextVisible = false;
            // 
            // layoutControlItem184
            // 
            this.layoutControlItem184.Control = this.ucqiydf;
            this.layoutControlItem184.CustomizationFormText = "layoutControlItem184";
            this.layoutControlItem184.Location = new System.Drawing.Point(535, 24);
            this.layoutControlItem184.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem184.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem184.Name = "layoutControlItem184";
            this.layoutControlItem184.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem184.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem184.Text = "layoutControlItem184";
            this.layoutControlItem184.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem184.TextToControlDistance = 0;
            this.layoutControlItem184.TextVisible = false;
            // 
            // layoutControlItem185
            // 
            this.layoutControlItem185.Control = this.uctebdf;
            this.layoutControlItem185.CustomizationFormText = "layoutControlItem185";
            this.layoutControlItem185.Location = new System.Drawing.Point(615, 24);
            this.layoutControlItem185.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem185.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem185.Name = "layoutControlItem185";
            this.layoutControlItem185.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem185.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem185.Text = "layoutControlItem185";
            this.layoutControlItem185.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem185.TextToControlDistance = 0;
            this.layoutControlItem185.TextVisible = false;
            // 
            // layoutControlItem186
            // 
            this.layoutControlItem186.Control = this.ucpinghdf;
            this.layoutControlItem186.CustomizationFormText = "layoutControlItem186";
            this.layoutControlItem186.Location = new System.Drawing.Point(695, 24);
            this.layoutControlItem186.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem186.Name = "layoutControlItem186";
            this.layoutControlItem186.Size = new System.Drawing.Size(85, 24);
            this.layoutControlItem186.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem186.Text = "layoutControlItem186";
            this.layoutControlItem186.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem186.TextToControlDistance = 0;
            this.layoutControlItem186.TextVisible = false;
            // 
            // layoutControlItem187
            // 
            this.layoutControlItem187.Control = this.flow1;
            this.layoutControlItem187.CustomizationFormText = "layoutControlItem187";
            this.layoutControlItem187.Location = new System.Drawing.Point(55, 66);
            this.layoutControlItem187.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem187.MinSize = new System.Drawing.Size(80, 150);
            this.layoutControlItem187.Name = "layoutControlItem187";
            this.layoutControlItem187.Size = new System.Drawing.Size(80, 150);
            this.layoutControlItem187.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem187.Text = "layoutControlItem187";
            this.layoutControlItem187.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem187.TextToControlDistance = 0;
            this.layoutControlItem187.TextVisible = false;
            // 
            // layoutControlItem188
            // 
            this.layoutControlItem188.Control = this.flow2;
            this.layoutControlItem188.CustomizationFormText = "layoutControlItem188";
            this.layoutControlItem188.Location = new System.Drawing.Point(135, 66);
            this.layoutControlItem188.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem188.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem188.Name = "layoutControlItem188";
            this.layoutControlItem188.Size = new System.Drawing.Size(80, 150);
            this.layoutControlItem188.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem188.Text = "layoutControlItem188";
            this.layoutControlItem188.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem188.TextToControlDistance = 0;
            this.layoutControlItem188.TextVisible = false;
            // 
            // layoutControlItem189
            // 
            this.layoutControlItem189.Control = this.flow3;
            this.layoutControlItem189.CustomizationFormText = "layoutControlItem189";
            this.layoutControlItem189.Location = new System.Drawing.Point(215, 66);
            this.layoutControlItem189.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem189.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem189.Name = "layoutControlItem189";
            this.layoutControlItem189.Size = new System.Drawing.Size(80, 150);
            this.layoutControlItem189.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem189.Text = "layoutControlItem189";
            this.layoutControlItem189.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem189.TextToControlDistance = 0;
            this.layoutControlItem189.TextVisible = false;
            // 
            // layoutControlItem190
            // 
            this.layoutControlItem190.Control = this.flow4;
            this.layoutControlItem190.CustomizationFormText = "layoutControlItem190";
            this.layoutControlItem190.Location = new System.Drawing.Point(295, 66);
            this.layoutControlItem190.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem190.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem190.Name = "layoutControlItem190";
            this.layoutControlItem190.Size = new System.Drawing.Size(80, 150);
            this.layoutControlItem190.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem190.Text = "layoutControlItem190";
            this.layoutControlItem190.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem190.TextToControlDistance = 0;
            this.layoutControlItem190.TextVisible = false;
            // 
            // layoutControlItem191
            // 
            this.layoutControlItem191.Control = this.flow5;
            this.layoutControlItem191.CustomizationFormText = "layoutControlItem191";
            this.layoutControlItem191.Location = new System.Drawing.Point(375, 66);
            this.layoutControlItem191.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem191.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem191.Name = "layoutControlItem191";
            this.layoutControlItem191.Size = new System.Drawing.Size(80, 150);
            this.layoutControlItem191.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem191.Text = "layoutControlItem191";
            this.layoutControlItem191.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem191.TextToControlDistance = 0;
            this.layoutControlItem191.TextVisible = false;
            // 
            // layoutControlItem192
            // 
            this.layoutControlItem192.Control = this.flow6;
            this.layoutControlItem192.CustomizationFormText = "layoutControlItem192";
            this.layoutControlItem192.Location = new System.Drawing.Point(455, 66);
            this.layoutControlItem192.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem192.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem192.Name = "layoutControlItem192";
            this.layoutControlItem192.Size = new System.Drawing.Size(80, 150);
            this.layoutControlItem192.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem192.Text = "layoutControlItem192";
            this.layoutControlItem192.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem192.TextToControlDistance = 0;
            this.layoutControlItem192.TextVisible = false;
            // 
            // layoutControlItem193
            // 
            this.layoutControlItem193.Control = this.flow7;
            this.layoutControlItem193.CustomizationFormText = "layoutControlItem193";
            this.layoutControlItem193.Location = new System.Drawing.Point(535, 66);
            this.layoutControlItem193.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem193.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem193.Name = "layoutControlItem193";
            this.layoutControlItem193.Size = new System.Drawing.Size(80, 150);
            this.layoutControlItem193.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem193.Text = "layoutControlItem193";
            this.layoutControlItem193.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem193.TextToControlDistance = 0;
            this.layoutControlItem193.TextVisible = false;
            // 
            // layoutControlItem194
            // 
            this.layoutControlItem194.Control = this.flow8;
            this.layoutControlItem194.CustomizationFormText = "layoutControlItem194";
            this.layoutControlItem194.Location = new System.Drawing.Point(615, 66);
            this.layoutControlItem194.MaxSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem194.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem194.Name = "layoutControlItem194";
            this.layoutControlItem194.Size = new System.Drawing.Size(80, 150);
            this.layoutControlItem194.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem194.Text = "layoutControlItem194";
            this.layoutControlItem194.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem194.TextToControlDistance = 0;
            this.layoutControlItem194.TextVisible = false;
            // 
            // layoutControlItem195
            // 
            this.layoutControlItem195.Control = this.flow9;
            this.layoutControlItem195.CustomizationFormText = "layoutControlItem195";
            this.layoutControlItem195.Location = new System.Drawing.Point(695, 66);
            this.layoutControlItem195.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem195.Name = "layoutControlItem195";
            this.layoutControlItem195.Size = new System.Drawing.Size(85, 150);
            this.layoutControlItem195.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem195.Text = "layoutControlItem195";
            this.layoutControlItem195.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem195.TextToControlDistance = 0;
            this.layoutControlItem195.TextVisible = false;
            // 
            // layoutControlItem196
            // 
            this.layoutControlItem196.Control = this.labelControl1;
            this.layoutControlItem196.CustomizationFormText = "layoutControlItem196";
            this.layoutControlItem196.Location = new System.Drawing.Point(55, 48);
            this.layoutControlItem196.MaxSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem196.MinSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem196.Name = "layoutControlItem196";
            this.layoutControlItem196.Size = new System.Drawing.Size(80, 18);
            this.layoutControlItem196.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem196.Text = "layoutControlItem196";
            this.layoutControlItem196.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem196.TextToControlDistance = 0;
            this.layoutControlItem196.TextVisible = false;
            // 
            // layoutControlItem197
            // 
            this.layoutControlItem197.Control = this.labelControl2;
            this.layoutControlItem197.CustomizationFormText = "layoutControlItem197";
            this.layoutControlItem197.Location = new System.Drawing.Point(135, 48);
            this.layoutControlItem197.MaxSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem197.MinSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem197.Name = "layoutControlItem197";
            this.layoutControlItem197.Size = new System.Drawing.Size(80, 18);
            this.layoutControlItem197.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem197.Text = "layoutControlItem197";
            this.layoutControlItem197.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem197.TextToControlDistance = 0;
            this.layoutControlItem197.TextVisible = false;
            // 
            // layoutControlItem198
            // 
            this.layoutControlItem198.Control = this.labelControl3;
            this.layoutControlItem198.CustomizationFormText = "layoutControlItem198";
            this.layoutControlItem198.Location = new System.Drawing.Point(215, 48);
            this.layoutControlItem198.MaxSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem198.MinSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem198.Name = "layoutControlItem198";
            this.layoutControlItem198.Size = new System.Drawing.Size(80, 18);
            this.layoutControlItem198.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem198.Text = "layoutControlItem198";
            this.layoutControlItem198.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem198.TextToControlDistance = 0;
            this.layoutControlItem198.TextVisible = false;
            // 
            // layoutControlItem199
            // 
            this.layoutControlItem199.Control = this.labelControl4;
            this.layoutControlItem199.CustomizationFormText = "layoutControlItem199";
            this.layoutControlItem199.Location = new System.Drawing.Point(295, 48);
            this.layoutControlItem199.MaxSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem199.MinSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem199.Name = "layoutControlItem199";
            this.layoutControlItem199.Size = new System.Drawing.Size(80, 18);
            this.layoutControlItem199.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem199.Text = "layoutControlItem199";
            this.layoutControlItem199.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem199.TextToControlDistance = 0;
            this.layoutControlItem199.TextVisible = false;
            // 
            // layoutControlItem200
            // 
            this.layoutControlItem200.Control = this.labelControl5;
            this.layoutControlItem200.CustomizationFormText = "layoutControlItem200";
            this.layoutControlItem200.Location = new System.Drawing.Point(375, 48);
            this.layoutControlItem200.MaxSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem200.MinSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem200.Name = "layoutControlItem200";
            this.layoutControlItem200.Size = new System.Drawing.Size(80, 18);
            this.layoutControlItem200.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem200.Text = "layoutControlItem200";
            this.layoutControlItem200.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem200.TextToControlDistance = 0;
            this.layoutControlItem200.TextVisible = false;
            // 
            // layoutControlItem201
            // 
            this.layoutControlItem201.Control = this.labelControl6;
            this.layoutControlItem201.CustomizationFormText = "layoutControlItem201";
            this.layoutControlItem201.Location = new System.Drawing.Point(455, 48);
            this.layoutControlItem201.MaxSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem201.MinSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem201.Name = "layoutControlItem201";
            this.layoutControlItem201.Size = new System.Drawing.Size(80, 18);
            this.layoutControlItem201.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem201.Text = "layoutControlItem201";
            this.layoutControlItem201.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem201.TextToControlDistance = 0;
            this.layoutControlItem201.TextVisible = false;
            // 
            // layoutControlItem202
            // 
            this.layoutControlItem202.Control = this.labelControl7;
            this.layoutControlItem202.CustomizationFormText = "layoutControlItem202";
            this.layoutControlItem202.Location = new System.Drawing.Point(535, 48);
            this.layoutControlItem202.MaxSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem202.MinSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem202.Name = "layoutControlItem202";
            this.layoutControlItem202.Size = new System.Drawing.Size(80, 18);
            this.layoutControlItem202.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem202.Text = "layoutControlItem202";
            this.layoutControlItem202.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem202.TextToControlDistance = 0;
            this.layoutControlItem202.TextVisible = false;
            // 
            // layoutControlItem203
            // 
            this.layoutControlItem203.Control = this.labelControl8;
            this.layoutControlItem203.CustomizationFormText = "layoutControlItem203";
            this.layoutControlItem203.Location = new System.Drawing.Point(615, 48);
            this.layoutControlItem203.MaxSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem203.MinSize = new System.Drawing.Size(80, 18);
            this.layoutControlItem203.Name = "layoutControlItem203";
            this.layoutControlItem203.Size = new System.Drawing.Size(80, 18);
            this.layoutControlItem203.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem203.Text = "layoutControlItem203";
            this.layoutControlItem203.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem203.TextToControlDistance = 0;
            this.layoutControlItem203.TextVisible = false;
            // 
            // layoutControlItem204
            // 
            this.layoutControlItem204.Control = this.labelControl9;
            this.layoutControlItem204.CustomizationFormText = "layoutControlItem204";
            this.layoutControlItem204.Location = new System.Drawing.Point(695, 48);
            this.layoutControlItem204.Name = "layoutControlItem204";
            this.layoutControlItem204.Size = new System.Drawing.Size(85, 18);
            this.layoutControlItem204.Text = "layoutControlItem204";
            this.layoutControlItem204.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem204.TextToControlDistance = 0;
            this.layoutControlItem204.TextVisible = false;
            // 
            // layoutControlItem239
            // 
            this.layoutControlItem239.Control = this.sp1;
            this.layoutControlItem239.CustomizationFormText = "layoutControlItem239";
            this.layoutControlItem239.Location = new System.Drawing.Point(55, 0);
            this.layoutControlItem239.MaxSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem239.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem239.Name = "layoutControlItem239";
            this.layoutControlItem239.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem239.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem239.Text = "layoutControlItem239";
            this.layoutControlItem239.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem239.TextToControlDistance = 0;
            this.layoutControlItem239.TextVisible = false;
            // 
            // layoutControlItem240
            // 
            this.layoutControlItem240.Control = this.sp2;
            this.layoutControlItem240.CustomizationFormText = "layoutControlItem240";
            this.layoutControlItem240.Location = new System.Drawing.Point(135, 0);
            this.layoutControlItem240.MaxSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem240.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem240.Name = "layoutControlItem240";
            this.layoutControlItem240.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem240.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem240.Text = "layoutControlItem240";
            this.layoutControlItem240.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem240.TextToControlDistance = 0;
            this.layoutControlItem240.TextVisible = false;
            // 
            // layoutControlItem241
            // 
            this.layoutControlItem241.Control = this.sp3;
            this.layoutControlItem241.CustomizationFormText = "layoutControlItem241";
            this.layoutControlItem241.Location = new System.Drawing.Point(215, 0);
            this.layoutControlItem241.MaxSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem241.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem241.Name = "layoutControlItem241";
            this.layoutControlItem241.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem241.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem241.Text = "layoutControlItem241";
            this.layoutControlItem241.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem241.TextToControlDistance = 0;
            this.layoutControlItem241.TextVisible = false;
            // 
            // layoutControlItem242
            // 
            this.layoutControlItem242.Control = this.sp4;
            this.layoutControlItem242.CustomizationFormText = "layoutControlItem242";
            this.layoutControlItem242.Location = new System.Drawing.Point(295, 0);
            this.layoutControlItem242.MaxSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem242.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem242.Name = "layoutControlItem242";
            this.layoutControlItem242.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem242.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem242.Text = "layoutControlItem242";
            this.layoutControlItem242.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem242.TextToControlDistance = 0;
            this.layoutControlItem242.TextVisible = false;
            // 
            // layoutControlItem243
            // 
            this.layoutControlItem243.Control = this.sp5;
            this.layoutControlItem243.CustomizationFormText = "layoutControlItem243";
            this.layoutControlItem243.Location = new System.Drawing.Point(375, 0);
            this.layoutControlItem243.MaxSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem243.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem243.Name = "layoutControlItem243";
            this.layoutControlItem243.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem243.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem243.Text = "layoutControlItem243";
            this.layoutControlItem243.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem243.TextToControlDistance = 0;
            this.layoutControlItem243.TextVisible = false;
            // 
            // layoutControlItem244
            // 
            this.layoutControlItem244.Control = this.sp6;
            this.layoutControlItem244.CustomizationFormText = "layoutControlItem244";
            this.layoutControlItem244.Location = new System.Drawing.Point(455, 0);
            this.layoutControlItem244.MaxSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem244.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem244.Name = "layoutControlItem244";
            this.layoutControlItem244.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem244.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem244.Text = "layoutControlItem244";
            this.layoutControlItem244.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem244.TextToControlDistance = 0;
            this.layoutControlItem244.TextVisible = false;
            // 
            // layoutControlItem245
            // 
            this.layoutControlItem245.Control = this.sp7;
            this.layoutControlItem245.CustomizationFormText = "layoutControlItem245";
            this.layoutControlItem245.Location = new System.Drawing.Point(535, 0);
            this.layoutControlItem245.MaxSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem245.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem245.Name = "layoutControlItem245";
            this.layoutControlItem245.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem245.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem245.Text = "layoutControlItem245";
            this.layoutControlItem245.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem245.TextToControlDistance = 0;
            this.layoutControlItem245.TextVisible = false;
            // 
            // layoutControlItem246
            // 
            this.layoutControlItem246.Control = this.sp8;
            this.layoutControlItem246.CustomizationFormText = "layoutControlItem246";
            this.layoutControlItem246.Location = new System.Drawing.Point(615, 0);
            this.layoutControlItem246.MaxSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem246.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem246.Name = "layoutControlItem246";
            this.layoutControlItem246.Size = new System.Drawing.Size(80, 24);
            this.layoutControlItem246.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem246.Text = "layoutControlItem246";
            this.layoutControlItem246.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem246.TextToControlDistance = 0;
            this.layoutControlItem246.TextVisible = false;
            // 
            // layoutControlItem247
            // 
            this.layoutControlItem247.Control = this.sp9;
            this.layoutControlItem247.CustomizationFormText = "layoutControlItem247";
            this.layoutControlItem247.Location = new System.Drawing.Point(695, 0);
            this.layoutControlItem247.MaxSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem247.MinSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem247.Name = "layoutControlItem247";
            this.layoutControlItem247.Size = new System.Drawing.Size(85, 24);
            this.layoutControlItem247.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem247.Text = "layoutControlItem247";
            this.layoutControlItem247.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem247.TextToControlDistance = 0;
            this.layoutControlItem247.TextVisible = false;
            // 
            // layoutControlItem248
            // 
            this.layoutControlItem248.Control = this.labelControl44;
            this.layoutControlItem248.CustomizationFormText = "layoutControlItem248";
            this.layoutControlItem248.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem248.MaxSize = new System.Drawing.Size(55, 40);
            this.layoutControlItem248.MinSize = new System.Drawing.Size(55, 40);
            this.layoutControlItem248.Name = "layoutControlItem248";
            this.layoutControlItem248.Size = new System.Drawing.Size(55, 42);
            this.layoutControlItem248.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem248.Text = "layoutControlItem248";
            this.layoutControlItem248.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem248.TextToControlDistance = 0;
            this.layoutControlItem248.TextVisible = false;
            // 
            // layoutControlItem205
            // 
            this.layoutControlItem205.Control = this.labelControl10;
            this.layoutControlItem205.CustomizationFormText = "layoutControlItem205";
            this.layoutControlItem205.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem205.MaxSize = new System.Drawing.Size(390, 50);
            this.layoutControlItem205.MinSize = new System.Drawing.Size(390, 50);
            this.layoutControlItem205.Name = "layoutControlItem205";
            this.layoutControlItem205.Size = new System.Drawing.Size(390, 50);
            this.layoutControlItem205.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem205.Text = "layoutControlItem205";
            this.layoutControlItem205.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem205.TextToControlDistance = 0;
            this.layoutControlItem205.TextVisible = false;
            // 
            // layout1
            // 
            this.layout1.Control = this.labelControl11;
            this.layout1.CustomizationFormText = "layoutControlItem206";
            this.layout1.Location = new System.Drawing.Point(0, 122);
            this.layout1.MaxSize = new System.Drawing.Size(390, 28);
            this.layout1.MinSize = new System.Drawing.Size(390, 28);
            this.layout1.Name = "layout1";
            this.layout1.Size = new System.Drawing.Size(390, 28);
            this.layout1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout1.Text = "layout1";
            this.layout1.TextSize = new System.Drawing.Size(0, 0);
            this.layout1.TextToControlDistance = 0;
            this.layout1.TextVisible = false;
            // 
            // layout2
            // 
            this.layout2.Control = this.labelControl12;
            this.layout2.CustomizationFormText = "layoutControlItem207";
            this.layout2.Location = new System.Drawing.Point(0, 150);
            this.layout2.MaxSize = new System.Drawing.Size(441, 40);
            this.layout2.MinSize = new System.Drawing.Size(390, 40);
            this.layout2.Name = "layout2";
            this.layout2.Size = new System.Drawing.Size(390, 40);
            this.layout2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout2.Text = "layout2";
            this.layout2.TextSize = new System.Drawing.Size(0, 0);
            this.layout2.TextToControlDistance = 0;
            this.layout2.TextVisible = false;
            // 
            // layout3
            // 
            this.layout3.Control = this.labelControl13;
            this.layout3.CustomizationFormText = "layoutControlItem208";
            this.layout3.Location = new System.Drawing.Point(0, 190);
            this.layout3.MaxSize = new System.Drawing.Size(390, 28);
            this.layout3.MinSize = new System.Drawing.Size(390, 28);
            this.layout3.Name = "layout3";
            this.layout3.Size = new System.Drawing.Size(390, 28);
            this.layout3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout3.Text = "layout3";
            this.layout3.TextSize = new System.Drawing.Size(0, 0);
            this.layout3.TextToControlDistance = 0;
            this.layout3.TextVisible = false;
            // 
            // layout4
            // 
            this.layout4.Control = this.labelControl14;
            this.layout4.CustomizationFormText = "(4)您说话声音低弱无力吗?（指说话没有力气）";
            this.layout4.Location = new System.Drawing.Point(0, 218);
            this.layout4.MaxSize = new System.Drawing.Size(0, 28);
            this.layout4.MinSize = new System.Drawing.Size(390, 28);
            this.layout4.Name = "layout4";
            this.layout4.Size = new System.Drawing.Size(390, 28);
            this.layout4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout4.Text = "(4)您说话声音低弱无力吗?（指说话没有力气）";
            this.layout4.TextSize = new System.Drawing.Size(0, 0);
            this.layout4.TextToControlDistance = 0;
            this.layout4.TextVisible = false;
            // 
            // layout5
            // 
            this.layout5.Control = this.labelControl15;
            this.layout5.CustomizationFormText = "layoutControlItem210";
            this.layout5.Location = new System.Drawing.Point(0, 246);
            this.layout5.MaxSize = new System.Drawing.Size(390, 28);
            this.layout5.MinSize = new System.Drawing.Size(390, 28);
            this.layout5.Name = "layout5";
            this.layout5.Size = new System.Drawing.Size(390, 28);
            this.layout5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout5.Text = "layout5";
            this.layout5.TextSize = new System.Drawing.Size(0, 0);
            this.layout5.TextToControlDistance = 0;
            this.layout5.TextVisible = false;
            // 
            // layout6
            // 
            this.layout6.Control = this.labelControl16;
            this.layout6.CustomizationFormText = "layoutControlItem211";
            this.layout6.Location = new System.Drawing.Point(0, 274);
            this.layout6.MaxSize = new System.Drawing.Size(390, 28);
            this.layout6.MinSize = new System.Drawing.Size(390, 28);
            this.layout6.Name = "layout6";
            this.layout6.Size = new System.Drawing.Size(390, 28);
            this.layout6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout6.Text = "layout6";
            this.layout6.TextSize = new System.Drawing.Size(0, 0);
            this.layout6.TextToControlDistance = 0;
            this.layout6.TextVisible = false;
            // 
            // layout7
            // 
            this.layout7.Control = this.labelControl17;
            this.layout7.CustomizationFormText = "layoutControlItem212";
            this.layout7.Location = new System.Drawing.Point(0, 302);
            this.layout7.MaxSize = new System.Drawing.Size(390, 28);
            this.layout7.MinSize = new System.Drawing.Size(390, 28);
            this.layout7.Name = "layout7";
            this.layout7.Size = new System.Drawing.Size(390, 28);
            this.layout7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout7.Text = "layout7";
            this.layout7.TextSize = new System.Drawing.Size(0, 0);
            this.layout7.TextToControlDistance = 0;
            this.layout7.TextVisible = false;
            // 
            // layout8
            // 
            this.layout8.Control = this.labelControl18;
            this.layout8.CustomizationFormText = "layoutControlItem213";
            this.layout8.Location = new System.Drawing.Point(0, 330);
            this.layout8.MaxSize = new System.Drawing.Size(390, 28);
            this.layout8.MinSize = new System.Drawing.Size(390, 28);
            this.layout8.Name = "layout8";
            this.layout8.Size = new System.Drawing.Size(390, 28);
            this.layout8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout8.Text = "layout8";
            this.layout8.TextSize = new System.Drawing.Size(0, 0);
            this.layout8.TextToControlDistance = 0;
            this.layout8.TextVisible = false;
            // 
            // layout9
            // 
            this.layout9.Control = this.labelControl19;
            this.layout9.CustomizationFormText = "layoutControlItem214";
            this.layout9.Location = new System.Drawing.Point(0, 358);
            this.layout9.MaxSize = new System.Drawing.Size(390, 50);
            this.layout9.MinSize = new System.Drawing.Size(390, 50);
            this.layout9.Name = "layout9";
            this.layout9.Size = new System.Drawing.Size(390, 50);
            this.layout9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout9.Text = "layout9";
            this.layout9.TextSize = new System.Drawing.Size(0, 0);
            this.layout9.TextToControlDistance = 0;
            this.layout9.TextVisible = false;
            // 
            // layout10
            // 
            this.layout10.Control = this.labelControl20;
            this.layout10.CustomizationFormText = "layoutControlItem215";
            this.layout10.Location = new System.Drawing.Point(0, 408);
            this.layout10.MaxSize = new System.Drawing.Size(390, 28);
            this.layout10.MinSize = new System.Drawing.Size(390, 28);
            this.layout10.Name = "layout10";
            this.layout10.Size = new System.Drawing.Size(390, 28);
            this.layout10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout10.Text = "layout10";
            this.layout10.TextSize = new System.Drawing.Size(0, 0);
            this.layout10.TextToControlDistance = 0;
            this.layout10.TextVisible = false;
            // 
            // layout11
            // 
            this.layout11.Control = this.labelControl21;
            this.layout11.CustomizationFormText = "layoutControlItem216";
            this.layout11.Location = new System.Drawing.Point(0, 436);
            this.layout11.MaxSize = new System.Drawing.Size(390, 28);
            this.layout11.MinSize = new System.Drawing.Size(390, 28);
            this.layout11.Name = "layout11";
            this.layout11.Size = new System.Drawing.Size(390, 28);
            this.layout11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout11.Text = "layout11";
            this.layout11.TextSize = new System.Drawing.Size(0, 0);
            this.layout11.TextToControlDistance = 0;
            this.layout11.TextVisible = false;
            // 
            // layout12
            // 
            this.layout12.Control = this.labelControl22;
            this.layout12.CustomizationFormText = "layoutControlItem217";
            this.layout12.Location = new System.Drawing.Point(0, 464);
            this.layout12.MaxSize = new System.Drawing.Size(0, 40);
            this.layout12.MinSize = new System.Drawing.Size(390, 40);
            this.layout12.Name = "layout12";
            this.layout12.Size = new System.Drawing.Size(390, 40);
            this.layout12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout12.Text = "layout12";
            this.layout12.TextSize = new System.Drawing.Size(0, 0);
            this.layout12.TextToControlDistance = 0;
            this.layout12.TextVisible = false;
            // 
            // layout13
            // 
            this.layout13.Control = this.labelControl23;
            this.layout13.CustomizationFormText = "layoutControlItem218";
            this.layout13.Location = new System.Drawing.Point(0, 504);
            this.layout13.MaxSize = new System.Drawing.Size(472, 40);
            this.layout13.MinSize = new System.Drawing.Size(390, 40);
            this.layout13.Name = "layout13";
            this.layout13.Size = new System.Drawing.Size(390, 40);
            this.layout13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout13.Text = "layout13";
            this.layout13.TextSize = new System.Drawing.Size(0, 0);
            this.layout13.TextToControlDistance = 0;
            this.layout13.TextVisible = false;
            // 
            // layout14
            // 
            this.layout14.Control = this.labelControl24;
            this.layout14.CustomizationFormText = "layoutControlItem219";
            this.layout14.Location = new System.Drawing.Point(0, 544);
            this.layout14.MaxSize = new System.Drawing.Size(0, 50);
            this.layout14.MinSize = new System.Drawing.Size(390, 50);
            this.layout14.Name = "layout14";
            this.layout14.Size = new System.Drawing.Size(390, 50);
            this.layout14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout14.Text = "layout14";
            this.layout14.TextSize = new System.Drawing.Size(0, 0);
            this.layout14.TextToControlDistance = 0;
            this.layout14.TextVisible = false;
            // 
            // layout15
            // 
            this.layout15.Control = this.labelControl25;
            this.layout15.CustomizationFormText = "layoutControlItem220";
            this.layout15.Location = new System.Drawing.Point(0, 594);
            this.layout15.MaxSize = new System.Drawing.Size(0, 28);
            this.layout15.MinSize = new System.Drawing.Size(390, 28);
            this.layout15.Name = "layout15";
            this.layout15.Size = new System.Drawing.Size(390, 28);
            this.layout15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout15.Text = "layout15";
            this.layout15.TextSize = new System.Drawing.Size(0, 0);
            this.layout15.TextToControlDistance = 0;
            this.layout15.TextVisible = false;
            // 
            // layout16
            // 
            this.layout16.Control = this.labelControl26;
            this.layout16.CustomizationFormText = "layoutControlItem221";
            this.layout16.Location = new System.Drawing.Point(0, 622);
            this.layout16.MaxSize = new System.Drawing.Size(390, 28);
            this.layout16.MinSize = new System.Drawing.Size(390, 28);
            this.layout16.Name = "layout16";
            this.layout16.Size = new System.Drawing.Size(390, 28);
            this.layout16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout16.Text = "layout16";
            this.layout16.TextSize = new System.Drawing.Size(0, 0);
            this.layout16.TextToControlDistance = 0;
            this.layout16.TextVisible = false;
            // 
            // layout17
            // 
            this.layout17.Control = this.labelControl27;
            this.layout17.CustomizationFormText = "layoutControlItem222";
            this.layout17.Location = new System.Drawing.Point(0, 650);
            this.layout17.MaxSize = new System.Drawing.Size(404, 40);
            this.layout17.MinSize = new System.Drawing.Size(390, 40);
            this.layout17.Name = "layout17";
            this.layout17.Size = new System.Drawing.Size(390, 40);
            this.layout17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout17.Text = "layout17";
            this.layout17.TextSize = new System.Drawing.Size(0, 0);
            this.layout17.TextToControlDistance = 0;
            this.layout17.TextVisible = false;
            // 
            // layout18
            // 
            this.layout18.Control = this.labelControl28;
            this.layout18.CustomizationFormText = "layoutControlItem223";
            this.layout18.Location = new System.Drawing.Point(0, 690);
            this.layout18.MaxSize = new System.Drawing.Size(390, 28);
            this.layout18.MinSize = new System.Drawing.Size(390, 28);
            this.layout18.Name = "layout18";
            this.layout18.Size = new System.Drawing.Size(390, 28);
            this.layout18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout18.Text = "layout18";
            this.layout18.TextSize = new System.Drawing.Size(0, 0);
            this.layout18.TextToControlDistance = 0;
            this.layout18.TextVisible = false;
            // 
            // layout19
            // 
            this.layout19.Control = this.labelControl29;
            this.layout19.CustomizationFormText = "layoutControlItem224";
            this.layout19.Location = new System.Drawing.Point(0, 718);
            this.layout19.MaxSize = new System.Drawing.Size(610, 40);
            this.layout19.MinSize = new System.Drawing.Size(390, 40);
            this.layout19.Name = "layout19";
            this.layout19.Size = new System.Drawing.Size(390, 40);
            this.layout19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout19.Text = "layout19";
            this.layout19.TextSize = new System.Drawing.Size(0, 0);
            this.layout19.TextToControlDistance = 0;
            this.layout19.TextVisible = false;
            // 
            // layout20
            // 
            this.layout20.Control = this.labelControl30;
            this.layout20.CustomizationFormText = "layoutControlItem225";
            this.layout20.Location = new System.Drawing.Point(0, 758);
            this.layout20.MaxSize = new System.Drawing.Size(418, 40);
            this.layout20.MinSize = new System.Drawing.Size(390, 40);
            this.layout20.Name = "layout20";
            this.layout20.Size = new System.Drawing.Size(390, 40);
            this.layout20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout20.Text = "layout20";
            this.layout20.TextSize = new System.Drawing.Size(0, 0);
            this.layout20.TextToControlDistance = 0;
            this.layout20.TextVisible = false;
            // 
            // layout21
            // 
            this.layout21.Control = this.labelControl31;
            this.layout21.CustomizationFormText = "layoutControlItem226";
            this.layout21.Location = new System.Drawing.Point(0, 798);
            this.layout21.MaxSize = new System.Drawing.Size(390, 28);
            this.layout21.MinSize = new System.Drawing.Size(390, 28);
            this.layout21.Name = "layout21";
            this.layout21.Size = new System.Drawing.Size(390, 28);
            this.layout21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout21.Text = "layout21";
            this.layout21.TextSize = new System.Drawing.Size(0, 0);
            this.layout21.TextToControlDistance = 0;
            this.layout21.TextVisible = false;
            // 
            // layout22
            // 
            this.layout22.Control = this.labelControl32;
            this.layout22.CustomizationFormText = "layoutControlItem227";
            this.layout22.Location = new System.Drawing.Point(0, 826);
            this.layout22.MaxSize = new System.Drawing.Size(390, 28);
            this.layout22.MinSize = new System.Drawing.Size(390, 28);
            this.layout22.Name = "layout22";
            this.layout22.Size = new System.Drawing.Size(390, 28);
            this.layout22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout22.Text = "layout22";
            this.layout22.TextSize = new System.Drawing.Size(0, 0);
            this.layout22.TextToControlDistance = 0;
            this.layout22.TextVisible = false;
            // 
            // layout24
            // 
            this.layout24.Control = this.labelControl33;
            this.layout24.CustomizationFormText = "layoutControlItem228";
            this.layout24.Location = new System.Drawing.Point(0, 882);
            this.layout24.MaxSize = new System.Drawing.Size(390, 28);
            this.layout24.MinSize = new System.Drawing.Size(390, 28);
            this.layout24.Name = "layout24";
            this.layout24.Size = new System.Drawing.Size(390, 28);
            this.layout24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout24.Text = "layout24";
            this.layout24.TextSize = new System.Drawing.Size(0, 0);
            this.layout24.TextToControlDistance = 0;
            this.layout24.TextVisible = false;
            // 
            // layout23
            // 
            this.layout23.Control = this.labelControl34;
            this.layout23.CustomizationFormText = "layoutControlItem229";
            this.layout23.Location = new System.Drawing.Point(0, 854);
            this.layout23.MaxSize = new System.Drawing.Size(390, 28);
            this.layout23.MinSize = new System.Drawing.Size(390, 28);
            this.layout23.Name = "layout23";
            this.layout23.Size = new System.Drawing.Size(390, 28);
            this.layout23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout23.Text = "layout23";
            this.layout23.TextSize = new System.Drawing.Size(0, 0);
            this.layout23.TextToControlDistance = 0;
            this.layout23.TextVisible = false;
            // 
            // layout25
            // 
            this.layout25.Control = this.labelControl35;
            this.layout25.CustomizationFormText = "layoutControlItem230";
            this.layout25.Location = new System.Drawing.Point(0, 910);
            this.layout25.MaxSize = new System.Drawing.Size(390, 28);
            this.layout25.MinSize = new System.Drawing.Size(390, 28);
            this.layout25.Name = "layout25";
            this.layout25.Size = new System.Drawing.Size(390, 28);
            this.layout25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout25.Text = "layout25";
            this.layout25.TextSize = new System.Drawing.Size(0, 0);
            this.layout25.TextToControlDistance = 0;
            this.layout25.TextVisible = false;
            // 
            // layout26
            // 
            this.layout26.Control = this.labelControl36;
            this.layout26.CustomizationFormText = "layoutControlItem231";
            this.layout26.Location = new System.Drawing.Point(0, 938);
            this.layout26.MaxSize = new System.Drawing.Size(390, 28);
            this.layout26.MinSize = new System.Drawing.Size(390, 28);
            this.layout26.Name = "layout26";
            this.layout26.Size = new System.Drawing.Size(390, 28);
            this.layout26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout26.Text = "layout26";
            this.layout26.TextSize = new System.Drawing.Size(0, 0);
            this.layout26.TextToControlDistance = 0;
            this.layout26.TextVisible = false;
            // 
            // layout27
            // 
            this.layout27.Control = this.labelControl37;
            this.layout27.CustomizationFormText = "layoutControlItem232";
            this.layout27.Location = new System.Drawing.Point(0, 966);
            this.layout27.MaxSize = new System.Drawing.Size(390, 28);
            this.layout27.MinSize = new System.Drawing.Size(390, 28);
            this.layout27.Name = "layout27";
            this.layout27.Size = new System.Drawing.Size(390, 28);
            this.layout27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout27.Text = "layout27";
            this.layout27.TextSize = new System.Drawing.Size(0, 0);
            this.layout27.TextToControlDistance = 0;
            this.layout27.TextVisible = false;
            // 
            // layout28
            // 
            this.layout28.Control = this.labelControl38;
            this.layout28.CustomizationFormText = "layoutControlItem233";
            this.layout28.Location = new System.Drawing.Point(0, 994);
            this.layout28.MaxSize = new System.Drawing.Size(0, 78);
            this.layout28.MinSize = new System.Drawing.Size(390, 78);
            this.layout28.Name = "layout28";
            this.layout28.Size = new System.Drawing.Size(390, 78);
            this.layout28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout28.Text = "layout28";
            this.layout28.TextSize = new System.Drawing.Size(0, 0);
            this.layout28.TextToControlDistance = 0;
            this.layout28.TextVisible = false;
            // 
            // layout29
            // 
            this.layout29.Control = this.labelControl39;
            this.layout29.CustomizationFormText = "layoutControlItem234";
            this.layout29.Location = new System.Drawing.Point(0, 1072);
            this.layout29.MaxSize = new System.Drawing.Size(390, 40);
            this.layout29.MinSize = new System.Drawing.Size(390, 40);
            this.layout29.Name = "layout29";
            this.layout29.Size = new System.Drawing.Size(390, 40);
            this.layout29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout29.Text = "layout29";
            this.layout29.TextSize = new System.Drawing.Size(0, 0);
            this.layout29.TextToControlDistance = 0;
            this.layout29.TextVisible = false;
            // 
            // layout30
            // 
            this.layout30.Control = this.labelControl40;
            this.layout30.CustomizationFormText = "layoutControlItem235";
            this.layout30.Location = new System.Drawing.Point(0, 1112);
            this.layout30.MaxSize = new System.Drawing.Size(390, 40);
            this.layout30.MinSize = new System.Drawing.Size(390, 40);
            this.layout30.Name = "layout30";
            this.layout30.Size = new System.Drawing.Size(390, 40);
            this.layout30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout30.Text = "layout30";
            this.layout30.TextSize = new System.Drawing.Size(0, 0);
            this.layout30.TextToControlDistance = 0;
            this.layout30.TextVisible = false;
            // 
            // layout31
            // 
            this.layout31.Control = this.labelControl41;
            this.layout31.CustomizationFormText = "layoutControlItem236";
            this.layout31.Location = new System.Drawing.Point(0, 1152);
            this.layout31.MaxSize = new System.Drawing.Size(390, 28);
            this.layout31.MinSize = new System.Drawing.Size(390, 28);
            this.layout31.Name = "layout31";
            this.layout31.Size = new System.Drawing.Size(390, 28);
            this.layout31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout31.Text = "layout31";
            this.layout31.TextSize = new System.Drawing.Size(0, 0);
            this.layout31.TextToControlDistance = 0;
            this.layout31.TextVisible = false;
            // 
            // layout32
            // 
            this.layout32.Control = this.labelControl42;
            this.layout32.CustomizationFormText = "layoutControlItem237";
            this.layout32.Location = new System.Drawing.Point(0, 1180);
            this.layout32.MaxSize = new System.Drawing.Size(390, 40);
            this.layout32.MinSize = new System.Drawing.Size(390, 40);
            this.layout32.Name = "layout32";
            this.layout32.Size = new System.Drawing.Size(390, 40);
            this.layout32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout32.Text = "layout32";
            this.layout32.TextSize = new System.Drawing.Size(0, 0);
            this.layout32.TextToControlDistance = 0;
            this.layout32.TextVisible = false;
            // 
            // layout33
            // 
            this.layout33.Control = this.labelControl43;
            this.layout33.CustomizationFormText = "layoutControlItem238";
            this.layout33.Location = new System.Drawing.Point(0, 1220);
            this.layout33.MaxSize = new System.Drawing.Size(390, 28);
            this.layout33.MinSize = new System.Drawing.Size(390, 28);
            this.layout33.Name = "layout33";
            this.layout33.Size = new System.Drawing.Size(390, 28);
            this.layout33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout33.Text = "layout33";
            this.layout33.TextSize = new System.Drawing.Size(0, 0);
            this.layout33.TextToControlDistance = 0;
            this.layout33.TextVisible = false;
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(798, 32);
            this.panelControl1.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Controls.Add(this.btn健康指导);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(794, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn重置
            // 
            this.btn重置.Image = ((System.Drawing.Image)(resources.GetObject("btn重置.Image")));
            this.btn重置.Location = new System.Drawing.Point(84, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "重置";
            // 
            // btn健康指导
            // 
            this.btn健康指导.Image = ((System.Drawing.Image)(resources.GetObject("btn健康指导.Image")));
            this.btn健康指导.Location = new System.Drawing.Point(165, 3);
            this.btn健康指导.Name = "btn健康指导";
            this.btn健康指导.Size = new System.Drawing.Size(92, 23);
            this.btn健康指导.TabIndex = 2;
            this.btn健康指导.Text = "健康指导";
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.checkEdit16;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem24";
            this.layoutControlItem29.Location = new System.Drawing.Point(330, 210);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(80, 23);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(80, 23);
            this.layoutControlItem29.Name = "layoutControlItem24";
            this.layoutControlItem29.Size = new System.Drawing.Size(80, 23);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "layoutControlItem24";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlItem95
            // 
            this.layoutControlItem95.Control = this.checkEdit81;
            this.layoutControlItem95.CustomizationFormText = "layoutControlItem90";
            this.layoutControlItem95.Location = new System.Drawing.Point(350, 648);
            this.layoutControlItem95.Name = "layoutControlItem90";
            this.layoutControlItem95.Size = new System.Drawing.Size(70, 50);
            this.layoutControlItem95.Text = "layoutControlItem90";
            this.layoutControlItem95.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem95.TextToControlDistance = 0;
            this.layoutControlItem95.TextVisible = false;
            // 
            // layoutControlItem180
            // 
            this.layoutControlItem180.Control = this.ucyindf;
            this.layoutControlItem180.CustomizationFormText = "layoutControlItem179";
            this.layoutControlItem180.Location = new System.Drawing.Point(445, 24);
            this.layoutControlItem180.Name = "layoutControlItem179";
            this.layoutControlItem180.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem180.Text = "layoutControlItem179";
            this.layoutControlItem180.TextSize = new System.Drawing.Size(119, 14);
            this.layoutControlItem180.TextToControlDistance = 5;
            // 
            // UC老年人中医药健康管理服务记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UC老年人中医药健康管理服务记录表";
            this.Size = new System.Drawing.Size(798, 498);
            this.Load += new System.EventHandler(this.UC老年人中医药健康管理服务记录表_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.flow2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt2.Properties)).EndInit();
            this.flow1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt1.Properties)).EndInit();
            this.flow9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit214.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit215.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit216.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit217.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit218.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit219.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt9.Properties)).EndInit();
            this.flow8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit208.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit209.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit210.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit211.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit212.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit213.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt8.Properties)).EndInit();
            this.flow7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit202.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit203.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit204.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit205.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit206.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit207.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt7.Properties)).EndInit();
            this.flow6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit196.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit197.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit198.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit199.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit200.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit201.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt6.Properties)).EndInit();
            this.flow5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit190.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit191.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit192.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit193.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit194.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit195.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt5.Properties)).EndInit();
            this.flow4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit184.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit185.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit186.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit187.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit188.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit189.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt4.Properties)).EndInit();
            this.flow3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit178.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit179.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit180.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit181.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit182.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit183.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit165.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit164.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit163.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit162.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit161.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit160.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit159.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit158.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit157.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit156.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit155.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit154.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit153.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit152.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit151.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit150.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit149.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit148.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit147.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit146.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit145.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit144.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit143.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit142.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit141.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit140.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit139.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit138.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit137.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit136.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit135.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit134.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit133.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit132.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit131.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit130.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit129.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit128.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit127.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit126.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit125.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit124.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit123.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit122.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit121.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit120.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit119.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit118.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit117.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit116.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit115.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit114.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit113.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit112.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit111.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit110.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit109.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit108.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit107.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit106.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit105.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit104.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit103.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit102.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit101.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit100.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit99.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit98.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit97.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit96.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit95.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit94.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit93.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit92.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit91.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit90.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit89.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit88.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit87.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit86.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit85.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit84.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit83.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit82.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit81.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit80.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit79.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit78.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit77.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit76.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit75.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit74.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit73.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit72.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit71.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit70.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit69.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit68.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit67.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit66.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit65.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit64.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit63.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit62.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit61.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit60.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit59.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit58.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit57.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit56.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit55.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit54.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit50.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit49.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkA3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkA2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkA1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte填表日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte填表日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem87)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem88)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem92)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem93)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem94)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem109)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem110)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem111)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem112)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem113)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem114)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem115)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem119)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem164)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem165)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem166)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem167)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem168)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem169)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem170)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem171)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem172)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem173)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem174)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem175)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group体质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl填表日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl医生签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem178)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl最近更新时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl当前所属机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl创建机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl创建人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl最近修改人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem176)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem177)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem179)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem181)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem182)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem183)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem184)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem185)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem186)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem187)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem188)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem189)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem190)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem191)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem192)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem193)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem194)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem195)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem196)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem197)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem198)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem199)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem200)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem201)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem202)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem203)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem204)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem239)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem240)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem241)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem242)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem243)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem244)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem245)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem246)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem247)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem248)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem205)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem95)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem180)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraEditors.SimpleButton btn健康指导;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txt居住状态;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit chkA3;
        private DevExpress.XtraEditors.CheckEdit chkA2;
        private DevExpress.XtraEditors.CheckEdit chkA1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraEditors.CheckEdit checkEdit20;
        private DevExpress.XtraEditors.CheckEdit checkEdit19;
        private DevExpress.XtraEditors.CheckEdit checkEdit18;
        private DevExpress.XtraEditors.CheckEdit checkEdit17;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraEditors.CheckEdit checkEdit25;
        private DevExpress.XtraEditors.CheckEdit checkEdit24;
        private DevExpress.XtraEditors.CheckEdit checkEdit23;
        private DevExpress.XtraEditors.CheckEdit checkEdit22;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.CheckEdit checkEdit30;
        private DevExpress.XtraEditors.CheckEdit checkEdit29;
        private DevExpress.XtraEditors.CheckEdit checkEdit28;
        private DevExpress.XtraEditors.CheckEdit checkEdit26;
        private DevExpress.XtraEditors.CheckEdit checkEdit27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.CheckEdit checkEdit35;
        private DevExpress.XtraEditors.CheckEdit checkEdit34;
        private DevExpress.XtraEditors.CheckEdit checkEdit33;
        private DevExpress.XtraEditors.CheckEdit checkEdit32;
        private DevExpress.XtraEditors.CheckEdit checkEdit31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraEditors.CheckEdit checkEdit40;
        private DevExpress.XtraEditors.CheckEdit checkEdit39;
        private DevExpress.XtraEditors.CheckEdit checkEdit38;
        private DevExpress.XtraEditors.CheckEdit checkEdit37;
        private DevExpress.XtraEditors.CheckEdit checkEdit36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraEditors.CheckEdit checkEdit45;
        private DevExpress.XtraEditors.CheckEdit checkEdit44;
        private DevExpress.XtraEditors.CheckEdit checkEdit43;
        private DevExpress.XtraEditors.CheckEdit checkEdit42;
        private DevExpress.XtraEditors.CheckEdit checkEdit41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraEditors.CheckEdit checkEdit50;
        private DevExpress.XtraEditors.CheckEdit checkEdit49;
        private DevExpress.XtraEditors.CheckEdit checkEdit48;
        private DevExpress.XtraEditors.CheckEdit checkEdit47;
        private DevExpress.XtraEditors.CheckEdit checkEdit46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraEditors.CheckEdit checkEdit54;
        private DevExpress.XtraEditors.CheckEdit checkEdit53;
        private DevExpress.XtraEditors.CheckEdit checkEdit52;
        private DevExpress.XtraEditors.CheckEdit checkEdit51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private DevExpress.XtraEditors.CheckEdit checkEdit55;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem64;
        private DevExpress.XtraEditors.CheckEdit checkEdit60;
        private DevExpress.XtraEditors.CheckEdit checkEdit59;
        private DevExpress.XtraEditors.CheckEdit checkEdit58;
        private DevExpress.XtraEditors.CheckEdit checkEdit57;
        private DevExpress.XtraEditors.CheckEdit checkEdit56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem65;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem66;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem67;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem68;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem69;
        private DevExpress.XtraEditors.CheckEdit checkEdit65;
        private DevExpress.XtraEditors.CheckEdit checkEdit64;
        private DevExpress.XtraEditors.CheckEdit checkEdit63;
        private DevExpress.XtraEditors.CheckEdit checkEdit62;
        private DevExpress.XtraEditors.CheckEdit checkEdit61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem71;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem72;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem73;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem74;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem70;
        private DevExpress.XtraEditors.CheckEdit checkEdit70;
        private DevExpress.XtraEditors.CheckEdit checkEdit69;
        private DevExpress.XtraEditors.CheckEdit checkEdit68;
        private DevExpress.XtraEditors.CheckEdit checkEdit67;
        private DevExpress.XtraEditors.CheckEdit checkEdit66;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem75;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem76;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem77;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem78;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem79;
        private DevExpress.XtraEditors.CheckEdit checkEdit75;
        private DevExpress.XtraEditors.CheckEdit checkEdit74;
        private DevExpress.XtraEditors.CheckEdit checkEdit73;
        private DevExpress.XtraEditors.CheckEdit checkEdit72;
        private DevExpress.XtraEditors.CheckEdit checkEdit71;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem80;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem81;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem82;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem83;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem84;
        private DevExpress.XtraEditors.CheckEdit checkEdit80;
        private DevExpress.XtraEditors.CheckEdit checkEdit79;
        private DevExpress.XtraEditors.CheckEdit checkEdit78;
        private DevExpress.XtraEditors.CheckEdit checkEdit77;
        private DevExpress.XtraEditors.CheckEdit checkEdit76;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem85;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem86;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem87;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem88;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem89;
        private DevExpress.XtraEditors.CheckEdit checkEdit85;
        private DevExpress.XtraEditors.CheckEdit checkEdit84;
        private DevExpress.XtraEditors.CheckEdit checkEdit83;
        private DevExpress.XtraEditors.CheckEdit checkEdit82;
        private DevExpress.XtraEditors.CheckEdit checkEdit81;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem90;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem91;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem92;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem93;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem94;
        private DevExpress.XtraEditors.CheckEdit checkEdit90;
        private DevExpress.XtraEditors.CheckEdit checkEdit89;
        private DevExpress.XtraEditors.CheckEdit checkEdit88;
        private DevExpress.XtraEditors.CheckEdit checkEdit87;
        private DevExpress.XtraEditors.CheckEdit checkEdit86;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem96;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem97;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem98;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem99;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem100;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem95;
        private DevExpress.XtraEditors.CheckEdit checkEdit95;
        private DevExpress.XtraEditors.CheckEdit checkEdit94;
        private DevExpress.XtraEditors.CheckEdit checkEdit93;
        private DevExpress.XtraEditors.CheckEdit checkEdit92;
        private DevExpress.XtraEditors.CheckEdit checkEdit91;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem101;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem102;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem103;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem104;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem105;
        private DevExpress.XtraEditors.CheckEdit checkEdit100;
        private DevExpress.XtraEditors.CheckEdit checkEdit99;
        private DevExpress.XtraEditors.CheckEdit checkEdit98;
        private DevExpress.XtraEditors.CheckEdit checkEdit97;
        private DevExpress.XtraEditors.CheckEdit checkEdit96;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem106;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem107;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem108;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem109;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem110;
        private DevExpress.XtraEditors.CheckEdit checkEdit105;
        private DevExpress.XtraEditors.CheckEdit checkEdit104;
        private DevExpress.XtraEditors.CheckEdit checkEdit103;
        private DevExpress.XtraEditors.CheckEdit checkEdit102;
        private DevExpress.XtraEditors.CheckEdit checkEdit101;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem111;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem112;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem113;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem114;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem115;
        private DevExpress.XtraEditors.CheckEdit checkEdit110;
        private DevExpress.XtraEditors.CheckEdit checkEdit109;
        private DevExpress.XtraEditors.CheckEdit checkEdit108;
        private DevExpress.XtraEditors.CheckEdit checkEdit107;
        private DevExpress.XtraEditors.CheckEdit checkEdit106;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem116;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem117;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem118;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem119;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem120;
        private DevExpress.XtraEditors.CheckEdit checkEdit115;
        private DevExpress.XtraEditors.CheckEdit checkEdit114;
        private DevExpress.XtraEditors.CheckEdit checkEdit113;
        private DevExpress.XtraEditors.CheckEdit checkEdit112;
        private DevExpress.XtraEditors.CheckEdit checkEdit111;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem121;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem122;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem123;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem124;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem125;
        private DevExpress.XtraEditors.CheckEdit checkEdit120;
        private DevExpress.XtraEditors.CheckEdit checkEdit119;
        private DevExpress.XtraEditors.CheckEdit checkEdit118;
        private DevExpress.XtraEditors.CheckEdit checkEdit117;
        private DevExpress.XtraEditors.CheckEdit checkEdit116;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem126;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem127;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem128;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem129;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem130;
        private DevExpress.XtraEditors.CheckEdit checkEdit125;
        private DevExpress.XtraEditors.CheckEdit checkEdit124;
        private DevExpress.XtraEditors.CheckEdit checkEdit123;
        private DevExpress.XtraEditors.CheckEdit checkEdit122;
        private DevExpress.XtraEditors.CheckEdit checkEdit121;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem131;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem132;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem133;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem134;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem135;
        private DevExpress.XtraEditors.CheckEdit checkEdit130;
        private DevExpress.XtraEditors.CheckEdit checkEdit129;
        private DevExpress.XtraEditors.CheckEdit checkEdit128;
        private DevExpress.XtraEditors.CheckEdit checkEdit127;
        private DevExpress.XtraEditors.CheckEdit checkEdit126;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem136;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem137;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem138;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem139;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem140;
        private DevExpress.XtraEditors.CheckEdit checkEdit135;
        private DevExpress.XtraEditors.CheckEdit checkEdit134;
        private DevExpress.XtraEditors.CheckEdit checkEdit133;
        private DevExpress.XtraEditors.CheckEdit checkEdit132;
        private DevExpress.XtraEditors.CheckEdit checkEdit131;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem141;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem142;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem143;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem144;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem145;
        private DevExpress.XtraEditors.CheckEdit checkEdit140;
        private DevExpress.XtraEditors.CheckEdit checkEdit139;
        private DevExpress.XtraEditors.CheckEdit checkEdit138;
        private DevExpress.XtraEditors.CheckEdit checkEdit137;
        private DevExpress.XtraEditors.CheckEdit checkEdit136;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem146;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem147;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem148;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem149;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem150;
        private DevExpress.XtraEditors.CheckEdit checkEdit145;
        private DevExpress.XtraEditors.CheckEdit checkEdit144;
        private DevExpress.XtraEditors.CheckEdit checkEdit143;
        private DevExpress.XtraEditors.CheckEdit checkEdit142;
        private DevExpress.XtraEditors.CheckEdit checkEdit141;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem151;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem152;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem153;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem154;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem155;
        private DevExpress.XtraEditors.CheckEdit checkEdit150;
        private DevExpress.XtraEditors.CheckEdit checkEdit149;
        private DevExpress.XtraEditors.CheckEdit checkEdit148;
        private DevExpress.XtraEditors.CheckEdit checkEdit147;
        private DevExpress.XtraEditors.CheckEdit checkEdit146;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem156;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem157;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem158;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem159;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem160;
        private DevExpress.XtraEditors.CheckEdit checkEdit155;
        private DevExpress.XtraEditors.CheckEdit checkEdit154;
        private DevExpress.XtraEditors.CheckEdit checkEdit153;
        private DevExpress.XtraEditors.CheckEdit checkEdit152;
        private DevExpress.XtraEditors.CheckEdit checkEdit151;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem161;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem162;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem163;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem164;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem165;
        private DevExpress.XtraEditors.CheckEdit checkEdit160;
        private DevExpress.XtraEditors.CheckEdit checkEdit159;
        private DevExpress.XtraEditors.CheckEdit checkEdit158;
        private DevExpress.XtraEditors.CheckEdit checkEdit157;
        private DevExpress.XtraEditors.CheckEdit checkEdit156;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem166;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem167;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem168;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem169;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem170;
        private DevExpress.XtraEditors.CheckEdit checkEdit165;
        private DevExpress.XtraEditors.CheckEdit checkEdit164;
        private DevExpress.XtraEditors.CheckEdit checkEdit163;
        private DevExpress.XtraEditors.CheckEdit checkEdit162;
        private DevExpress.XtraEditors.CheckEdit checkEdit161;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem171;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem172;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem173;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem174;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem175;
        private DevExpress.XtraLayout.LayoutControlGroup group体质;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem40;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem51;
        private DevExpress.XtraEditors.TextEdit txt医生签名;
        private DevExpress.XtraLayout.LayoutControlItem lbl填表日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl医生签名;
        private DevExpress.XtraEditors.TextEdit txt最近修改人;
        private DevExpress.XtraEditors.TextEdit txt创建人;
        private DevExpress.XtraEditors.TextEdit txt创建机构;
        private DevExpress.XtraEditors.TextEdit txt当前所属机构;
        private DevExpress.XtraEditors.TextEdit txt最近更新时间;
        private DevExpress.XtraEditors.TextEdit txt创建时间;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem178;
        private DevExpress.XtraLayout.LayoutControlItem lbl最近更新时间;
        private DevExpress.XtraLayout.LayoutControlItem lbl当前所属机构;
        private DevExpress.XtraLayout.LayoutControlItem lbl创建机构;
        private DevExpress.XtraLayout.LayoutControlItem lbl创建人;
        private DevExpress.XtraLayout.LayoutControlItem lbl最近修改人;
        private Library.UserControls.UCLblTxt ucqxdf;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem176;
        private Library.UserControls.UCLblTxt ucqiydf;
        private Library.UserControls.UCLblTxt ucxueydf;
        private Library.UserControls.UCLblTxt ucshirdf;
        private Library.UserControls.UCLblTxt uctansdf;
        private Library.UserControls.UCLblTxt ucyindf;
        private Library.UserControls.UCLblTxt ucyangdf;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem177;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem179;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem181;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem182;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem183;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem184;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem180;
        private Library.UserControls.UCLblTxt ucpinghdf;
        private Library.UserControls.UCLblTxt uctebdf;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem185;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem186;
        private System.Windows.Forms.FlowLayoutPanel flow1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem187;
        private System.Windows.Forms.FlowLayoutPanel flow2;
        private DevExpress.XtraEditors.CheckEdit chk11;
        private DevExpress.XtraEditors.CheckEdit chk12;
        private DevExpress.XtraEditors.CheckEdit chk13;
        private DevExpress.XtraEditors.CheckEdit chk14;
        private DevExpress.XtraEditors.CheckEdit chk15;
        private DevExpress.XtraEditors.CheckEdit chk16;
        private DevExpress.XtraEditors.TextEdit txt1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem188;
        private DevExpress.XtraEditors.CheckEdit chk21;
        private DevExpress.XtraEditors.CheckEdit chk22;
        private DevExpress.XtraEditors.CheckEdit chk23;
        private DevExpress.XtraEditors.CheckEdit chk24;
        private DevExpress.XtraEditors.CheckEdit chk25;
        private DevExpress.XtraEditors.CheckEdit chk26;
        private DevExpress.XtraEditors.TextEdit txt2;
        private System.Windows.Forms.FlowLayoutPanel flow9;
        private DevExpress.XtraEditors.CheckEdit checkEdit214;
        private DevExpress.XtraEditors.CheckEdit checkEdit215;
        private DevExpress.XtraEditors.CheckEdit checkEdit216;
        private DevExpress.XtraEditors.CheckEdit checkEdit217;
        private DevExpress.XtraEditors.CheckEdit checkEdit218;
        private DevExpress.XtraEditors.CheckEdit checkEdit219;
        private DevExpress.XtraEditors.TextEdit txt9;
        private System.Windows.Forms.FlowLayoutPanel flow8;
        private DevExpress.XtraEditors.CheckEdit checkEdit208;
        private DevExpress.XtraEditors.CheckEdit checkEdit209;
        private DevExpress.XtraEditors.CheckEdit checkEdit210;
        private DevExpress.XtraEditors.CheckEdit checkEdit211;
        private DevExpress.XtraEditors.CheckEdit checkEdit212;
        private DevExpress.XtraEditors.CheckEdit checkEdit213;
        private DevExpress.XtraEditors.TextEdit txt8;
        private System.Windows.Forms.FlowLayoutPanel flow7;
        private DevExpress.XtraEditors.CheckEdit checkEdit202;
        private DevExpress.XtraEditors.CheckEdit checkEdit203;
        private DevExpress.XtraEditors.CheckEdit checkEdit204;
        private DevExpress.XtraEditors.CheckEdit checkEdit205;
        private DevExpress.XtraEditors.CheckEdit checkEdit206;
        private DevExpress.XtraEditors.CheckEdit checkEdit207;
        private DevExpress.XtraEditors.TextEdit txt7;
        private System.Windows.Forms.FlowLayoutPanel flow6;
        private DevExpress.XtraEditors.CheckEdit checkEdit196;
        private DevExpress.XtraEditors.CheckEdit checkEdit197;
        private DevExpress.XtraEditors.CheckEdit checkEdit198;
        private DevExpress.XtraEditors.CheckEdit checkEdit199;
        private DevExpress.XtraEditors.CheckEdit checkEdit200;
        private DevExpress.XtraEditors.CheckEdit checkEdit201;
        private DevExpress.XtraEditors.TextEdit txt6;
        private System.Windows.Forms.FlowLayoutPanel flow5;
        private DevExpress.XtraEditors.CheckEdit checkEdit190;
        private DevExpress.XtraEditors.CheckEdit checkEdit191;
        private DevExpress.XtraEditors.CheckEdit checkEdit192;
        private DevExpress.XtraEditors.CheckEdit checkEdit193;
        private DevExpress.XtraEditors.CheckEdit checkEdit194;
        private DevExpress.XtraEditors.CheckEdit checkEdit195;
        private DevExpress.XtraEditors.TextEdit txt5;
        private System.Windows.Forms.FlowLayoutPanel flow4;
        private DevExpress.XtraEditors.CheckEdit checkEdit184;
        private DevExpress.XtraEditors.CheckEdit checkEdit185;
        private DevExpress.XtraEditors.CheckEdit checkEdit186;
        private DevExpress.XtraEditors.CheckEdit checkEdit187;
        private DevExpress.XtraEditors.CheckEdit checkEdit188;
        private DevExpress.XtraEditors.CheckEdit checkEdit189;
        private DevExpress.XtraEditors.TextEdit txt4;
        private System.Windows.Forms.FlowLayoutPanel flow3;
        private DevExpress.XtraEditors.CheckEdit checkEdit178;
        private DevExpress.XtraEditors.CheckEdit checkEdit179;
        private DevExpress.XtraEditors.CheckEdit checkEdit180;
        private DevExpress.XtraEditors.CheckEdit checkEdit181;
        private DevExpress.XtraEditors.CheckEdit checkEdit182;
        private DevExpress.XtraEditors.CheckEdit checkEdit183;
        private DevExpress.XtraEditors.TextEdit txt3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem189;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem190;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem191;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem192;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem193;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem194;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem195;
        private DevExpress.XtraEditors.DateEdit dte填表日期;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem196;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem197;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem198;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem199;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem200;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem201;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem202;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem203;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem204;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem205;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraLayout.LayoutControlItem layout1;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraLayout.LayoutControlItem layout2;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraLayout.LayoutControlItem layout3;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraLayout.LayoutControlItem layout4;
        private DevExpress.XtraLayout.LayoutControlItem layout5;
        private DevExpress.XtraLayout.LayoutControlItem layout6;
        private DevExpress.XtraLayout.LayoutControlItem layout7;
        private DevExpress.XtraLayout.LayoutControlItem layout8;
        private DevExpress.XtraLayout.LayoutControlItem layout9;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.LabelControl labelControl25;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraLayout.LayoutControlItem layout10;
        private DevExpress.XtraLayout.LayoutControlItem layout11;
        private DevExpress.XtraLayout.LayoutControlItem layout12;
        private DevExpress.XtraLayout.LayoutControlItem layout13;
        private DevExpress.XtraLayout.LayoutControlItem layout14;
        private DevExpress.XtraLayout.LayoutControlItem layout15;
        private DevExpress.XtraLayout.LayoutControlItem layout16;
        private DevExpress.XtraLayout.LayoutControlItem layout17;
        private DevExpress.XtraLayout.LayoutControlItem layout18;
        private DevExpress.XtraLayout.LayoutControlItem layout19;
        private DevExpress.XtraLayout.LayoutControlItem layout20;
        private DevExpress.XtraLayout.LayoutControlItem layout21;
        private DevExpress.XtraEditors.LabelControl labelControl41;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraLayout.LayoutControlItem layout22;
        private DevExpress.XtraLayout.LayoutControlItem layout24;
        private DevExpress.XtraLayout.LayoutControlItem layout23;
        private DevExpress.XtraLayout.LayoutControlItem layout25;
        private DevExpress.XtraLayout.LayoutControlItem layout26;
        private DevExpress.XtraLayout.LayoutControlItem layout27;
        private DevExpress.XtraLayout.LayoutControlItem layout28;
        private DevExpress.XtraLayout.LayoutControlItem layout29;
        private DevExpress.XtraLayout.LayoutControlItem layout30;
        private DevExpress.XtraLayout.LayoutControlItem layout31;
        private DevExpress.XtraEditors.LabelControl labelControl43;
        private DevExpress.XtraEditors.LabelControl labelControl42;
        private DevExpress.XtraLayout.LayoutControlItem layout32;
        private DevExpress.XtraLayout.LayoutControlItem layout33;
        private DevExpress.XtraEditors.LabelControl sp1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem239;
        private DevExpress.XtraEditors.LabelControl labelControl44;
        private DevExpress.XtraEditors.LabelControl sp9;
        private DevExpress.XtraEditors.LabelControl sp8;
        private DevExpress.XtraEditors.LabelControl sp7;
        private DevExpress.XtraEditors.LabelControl sp6;
        private DevExpress.XtraEditors.LabelControl sp5;
        private DevExpress.XtraEditors.LabelControl sp4;
        private DevExpress.XtraEditors.LabelControl sp3;
        private DevExpress.XtraEditors.LabelControl sp2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem240;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem241;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem242;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem243;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem244;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem245;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem246;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem247;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem248;
    }
}
