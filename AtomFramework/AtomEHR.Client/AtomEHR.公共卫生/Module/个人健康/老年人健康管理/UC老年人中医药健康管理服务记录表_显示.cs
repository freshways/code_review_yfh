﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business.Security;
using AtomEHR.Common;
using DevExpress.XtraEditors;
using AtomEHR.Models;
using DevExpress.XtraReports.UI;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    public partial class UC老年人中医药健康管理服务记录表_显示 : UserControlBase
    {
        #region Fields
        AtomEHR.Business.bll老年人中医药特征管理 _Bll = new Business.bll老年人中医药特征管理();
        DataSet _ds老年人中医药;
        frm个人健康 _frm;
        string _docNo;
        string _id;//表的主键，通过id来查询一条数据
        #endregion

        public UC老年人中医药健康管理服务记录表_显示()
        {
            InitializeComponent();
        }
        public UC老年人中医药健康管理服务记录表_显示(Form frm)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _id = _frm._param == null ? "" : _frm._param.ToString();
            if (string.IsNullOrEmpty(_id))
            {
                _ds老年人中医药 = _Bll.GetAllDataByKey(_docNo);
            }
            else
            {
                _ds老年人中医药 = _Bll.GetOneDataByKey(_docNo, _id, true);
            }

            DoBindingSummaryEditor(_ds老年人中医药);//绑定数据
        }
        private void DoBindingSummaryEditor(DataSet _ds老年人中医药)
        {
            if (_ds老年人中医药 == null) return;
            if (_ds老年人中医药.Tables.Count == 0) return;
            ////if (_ds老年人随访.Tables.Contains(tb_老年人随访.__TableName))
            ////{
            DataTable dt老年人信息 = _ds老年人中医药.Tables[tb_老年人基本信息.__TableName];
            DataTable dt中医药 = _ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName];
            if (dt老年人信息.Rows.Count == 1)
            {
                DataRow dr = dt老年人信息.Rows[0];
                this.txt姓名.Text = util.DESEncrypt.DES解密(dr[tb_老年人基本信息.姓名].ToString());
                this.txt性别.Text = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_老年人基本信息.性别].ToString());
                this.txt个人档案号.Text = dr[tb_老年人基本信息.个人档案编号].ToString();
                this.txt身份证号.Text = dr[tb_老年人基本信息.身份证号].ToString();
                this.txt出生日期.Text = dr[tb_老年人基本信息.出生日期].ToString();
                this.txt联系电话.Text = dr[tb_老年人基本信息.联系电话].ToString();
                this.txt居住状态.Text = _BLL.ReturnDis字典显示("jzzk", dr[tb_老年人基本信息.常住类型].ToString());
                this.txt居住地址.Text = _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.省].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.市].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.区].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.街道].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.居委会].ToString()) + dr[tb_老年人基本信息.居住地址].ToString();

            }
            if (dt中医药.Rows.Count == 1)
            {
                BindData(dt中医药.Rows[0]);
            }

            this.txt创建人.Text = _Bll.Return用户名称(dt中医药.Rows[0][tb_老年人中医药特征管理.创建人].ToString());
            this.txt创建时间.Text = dt中医药.Rows[0][tb_老年人中医药特征管理.创建时间].ToString();
            this.txt最近更新时间.Text = dt中医药.Rows[0][tb_老年人中医药特征管理.修改时间].ToString();
            this.txt当前所属机构.Text = _Bll.Return机构名称(dt中医药.Rows[0][tb_老年人中医药特征管理.所属机构].ToString());
            this.txt创建机构.Text = _Bll.Return机构名称(dt中医药.Rows[0][tb_老年人中医药特征管理.创建机构].ToString());// Loginer.CurrentUser.所属机构名称;
            this.txt最近修改人.Text = _Bll.Return用户名称(dt中医药.Rows[0][tb_老年人中医药特征管理.修改人].ToString());
            this.txt医生签名.Text = dt中医药.Rows[0][tb_老年人中医药特征管理.医生签名].ToString();
        }

        private void BindData(DataRow dataRow)
        {
            _id = dataRow[tb_老年人中医药特征管理.ID].ToString();
            if (dataRow == null) return;
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征1].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征1]);
                SetRadioValue(1, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征2].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征2]);
                SetRadioValue(2, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征3].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征3]);
                SetRadioValue(3, score);
            }

            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征4].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征4]);
                SetRadioValue(4, score);
            }

            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征5].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征5]);
                SetRadioValue(5, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征6].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征6]);
                SetRadioValue(6, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征7].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征7]);
                SetRadioValue(7, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征8].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征8]);
                SetRadioValue(8, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征9].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征9]);
                SetRadioValue(9, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征10].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征10]);
                SetRadioValue(10, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征11].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征11]);
                SetRadioValue(11, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征12].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征12]);
                SetRadioValue(12, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征13].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征13]);
                SetRadioValue(13, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征14].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征14]);
                SetRadioValue(14, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征15].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征15]);
                SetRadioValue(15, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征16].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征16]);
                SetRadioValue(16, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征17].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征17]);
                SetRadioValue(17, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征18].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征18]);
                SetRadioValue(18, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征19].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征19]);
                SetRadioValue(19, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征20].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征20]);
                SetRadioValue(20, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征21].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征21]);
                SetRadioValue(21, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征22].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征22]);
                SetRadioValue(22, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征23].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征23]);
                SetRadioValue(23, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征24].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征24]);
                SetRadioValue(24, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征25].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征25]);
                SetRadioValue(25, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征26].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征26]);
                SetRadioValue(26, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征27].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征27]);
                SetRadioValue(27, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征28].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征28]);
                SetRadioValue(28, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征29].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征29]);
                SetRadioValue(29, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征30].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征30]);
                SetRadioValue(30, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征31].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征31]);
                SetRadioValue(31, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征32].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征32]);
                SetRadioValue(32, score);
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特征33].ToString()))
            {
                int score = Convert.ToInt32(dataRow[tb_老年人中医药特征管理.特征33]);
                SetRadioValue(33, score);
            }

            string text = string.Empty;
            this.ucqxdf.Txt1.Text = dataRow[tb_老年人中医药特征管理.气虚质得分].ToString();
            if (dataRow[tb_老年人中医药特征管理.气虚质辨识].ToString() == "1")
            {
                this.labelControl1.Text = "是";
                this.sp1.ForeColor = System.Drawing.Color.Red;
            }
            else if (dataRow[tb_老年人中医药特征管理.气虚质辨识].ToString() == "2")
            {
                this.labelControl1.Text = "倾向是";
                this.sp1.ForeColor = System.Drawing.Color.Blue;
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.气虚质指导].ToString()))
            {
                string[] results = dataRow[tb_老年人中医药特征管理.气虚质指导].ToString().Split(',');
                for (int i = 0; i < results.Length; i++)
                {
                    text += _Bll.ReturnDis字典显示("zyybjzd", results[i]) + ",\r\n";
                }
                text += dataRow[tb_老年人中医药特征管理.气虚质其他].ToString();
                this.txt1.Text = text;
            }

            this.ucyangdf.Txt1.Text = dataRow[tb_老年人中医药特征管理.阳虚质得分].ToString();
            if (dataRow[tb_老年人中医药特征管理.阳虚质辨识].ToString() == "1")
            {
                this.labelControl2.Text = "是";
                this.sp2.ForeColor = System.Drawing.Color.Red;
            }
            else if (dataRow[tb_老年人中医药特征管理.阳虚质辨识].ToString() == "2")
            {
                this.labelControl2.Text = "倾向是";
                this.sp2.ForeColor = System.Drawing.Color.Blue;
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.阳虚质指导].ToString()))
            {
                text = string.Empty;
                string[] results = dataRow[tb_老年人中医药特征管理.阳虚质指导].ToString().Split(',');
                for (int i = 0; i < results.Length; i++)
                {
                    text += _Bll.ReturnDis字典显示("zyybjzd", results[i]) + ",\r\n";
                }
                text += dataRow[tb_老年人中医药特征管理.阳虚质其他].ToString();
                this.txt2.Text = text;
            }

            this.ucyindf.Txt1.Text = dataRow[tb_老年人中医药特征管理.阴虚质得分].ToString();
            if (dataRow[tb_老年人中医药特征管理.阴虚质辨识].ToString() == "1")
            {
                this.labelControl3.Text = "是";
                this.sp3.ForeColor = System.Drawing.Color.Red;
            }
            else if (dataRow[tb_老年人中医药特征管理.阴虚质辨识].ToString() == "2")
            {
                this.labelControl3.Text = "倾向是";
                this.sp3.ForeColor = System.Drawing.Color.Blue;
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.阴虚质指导].ToString()))
            {
                text = string.Empty;
                string[] results = dataRow[tb_老年人中医药特征管理.阴虚质指导].ToString().Split(',');
                for (int i = 0; i < results.Length; i++)
                {
                    text += _Bll.ReturnDis字典显示("zyybjzd", results[i]) + ",\r\n";
                }
                text += dataRow[tb_老年人中医药特征管理.阴虚质其他].ToString();
                this.txt3.Text = text;
            }

            this.uctansdf.Txt1.Text = dataRow[tb_老年人中医药特征管理.痰湿质得分].ToString();
            if (dataRow[tb_老年人中医药特征管理.痰湿质辨识].ToString() == "1")
            {
                this.labelControl4.Text = "是";
                this.sp4.ForeColor = System.Drawing.Color.Red;
            }
            else if (dataRow[tb_老年人中医药特征管理.痰湿质辨识].ToString() == "2")
            {
                this.labelControl4.Text = "倾向是";
                this.sp4.ForeColor = System.Drawing.Color.Blue;
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.痰湿质指导].ToString()))
            {
                text = string.Empty;
                string[] results = dataRow[tb_老年人中医药特征管理.痰湿质指导].ToString().Split(',');
                for (int i = 0; i < results.Length; i++)
                {
                    text += _Bll.ReturnDis字典显示("zyybjzd", results[i]) + ",\r\n";
                }
                text += dataRow[tb_老年人中医药特征管理.痰湿质其他].ToString();
                this.txt4.Text = text;
            }

            this.ucshirdf.Txt1.Text = dataRow[tb_老年人中医药特征管理.湿热质得分].ToString();
            if (dataRow[tb_老年人中医药特征管理.湿热质辨识].ToString() == "1")
            {
                this.labelControl5.Text = "是";
                this.sp5.ForeColor = System.Drawing.Color.Red;
            }
            else if (dataRow[tb_老年人中医药特征管理.湿热质辨识].ToString() == "2")
            {
                this.labelControl5.Text = "倾向是";
                this.sp5.ForeColor = System.Drawing.Color.Blue;
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.湿热质指导].ToString()))
            {
                text = string.Empty;
                string[] results = dataRow[tb_老年人中医药特征管理.湿热质指导].ToString().Split(',');
                for (int i = 0; i < results.Length; i++)
                {
                    text += _Bll.ReturnDis字典显示("zyybjzd", results[i]) + ",\r\n";
                }
                text += dataRow[tb_老年人中医药特征管理.湿热质其他].ToString();
                this.txt5.Text = text;
            }

            this.ucxueydf.Txt1.Text = dataRow[tb_老年人中医药特征管理.血瘀质得分].ToString();
            if (dataRow[tb_老年人中医药特征管理.血瘀质辨识].ToString() == "1")
            {
                this.labelControl6.Text = "是";
                this.sp6.ForeColor = System.Drawing.Color.Red;
            }
            else if (dataRow[tb_老年人中医药特征管理.血瘀质辨识].ToString() == "2")
            {
                this.labelControl6.Text = "倾向是";
                this.sp6.ForeColor = System.Drawing.Color.Blue;
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.血瘀质指导].ToString()))
            {
                text = string.Empty;
                string[] results = dataRow[tb_老年人中医药特征管理.血瘀质指导].ToString().Split(',');
                for (int i = 0; i < results.Length; i++)
                {
                    text += _Bll.ReturnDis字典显示("zyybjzd", results[i]) + ",\r\n";
                }
                text += dataRow[tb_老年人中医药特征管理.血瘀质其他].ToString();
                this.txt6.Text = text;
            }

            this.ucqiydf.Txt1.Text = dataRow[tb_老年人中医药特征管理.气郁质得分].ToString();
            if (dataRow[tb_老年人中医药特征管理.气郁质辨识].ToString() == "1")
            {
                this.labelControl7.Text = "是";
                this.sp7.ForeColor = System.Drawing.Color.Red;
            }
            else if (dataRow[tb_老年人中医药特征管理.气郁质辨识].ToString() == "2")
            {
                this.labelControl7.Text = "倾向是";
                this.sp7.ForeColor = System.Drawing.Color.Blue;
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.气郁质指导].ToString()))
            {
                text = string.Empty;
                string[] results = dataRow[tb_老年人中医药特征管理.气郁质指导].ToString().Split(',');
                for (int i = 0; i < results.Length; i++)
                {
                    text += _Bll.ReturnDis字典显示("zyybjzd", results[i]) + ",\r\n";
                }
                text += dataRow[tb_老年人中医药特征管理.气郁质其他].ToString();
                this.txt7.Text = text;
            }

            this.uctebdf.Txt1.Text = dataRow[tb_老年人中医药特征管理.特禀质得分].ToString();
            if (dataRow[tb_老年人中医药特征管理.特禀质辨识].ToString() == "1")
            {
                this.labelControl8.Text = "是";
                this.sp7.ForeColor = System.Drawing.Color.Red;
            }
            else if (dataRow[tb_老年人中医药特征管理.特禀质辨识].ToString() == "2")
            {
                this.labelControl8.Text = "倾向是";
                this.sp7.ForeColor = System.Drawing.Color.Blue;
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.特禀质指导].ToString()))
            {
                text = string.Empty;
                string[] results = dataRow[tb_老年人中医药特征管理.特禀质指导].ToString().Split(',');
                for (int i = 0; i < results.Length; i++)
                {
                    text += _Bll.ReturnDis字典显示("zyybjzd", results[i]) + ",\r\n";
                }
                text += dataRow[tb_老年人中医药特征管理.特禀质其他].ToString();
                this.txt8.Text = text;
            }

            this.ucpinghdf.Txt1.Text = dataRow[tb_老年人中医药特征管理.平和质得分].ToString();
            if (dataRow[tb_老年人中医药特征管理.平和质辨识].ToString() == "1")
            {
                this.labelControl9.Text = "是";
                this.sp9.ForeColor = System.Drawing.Color.Red;
            }
            else if (dataRow[tb_老年人中医药特征管理.平和质辨识].ToString() == "2")
            {
                this.labelControl9.Text = "基本是";
                this.sp9.ForeColor = System.Drawing.Color.Blue;
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人中医药特征管理.平和质指导].ToString()))
            {
                text = string.Empty;
                string[] results = dataRow[tb_老年人中医药特征管理.平和质指导].ToString().Split(',');
                for (int i = 0; i < results.Length; i++)
                {
                    text += _Bll.ReturnDis字典显示("zyybjzd", results[i]) + ",\r\n";
                }
                text += dataRow[tb_老年人中医药特征管理.平和质其他].ToString();
                this.txt9.Text = text;
            }

            this.dte填表日期.Text = dataRow[tb_老年人中医药特征管理.发生时间].ToString();
            this.txt医生签名.Text = dataRow[tb_老年人中医药特征管理.医生签名].ToString();
        }
        /// <summary>
        /// 根据index 和 tag 来选中对应的checkedit
        /// </summary>
        /// <param name="groupIndex"></param>
        /// <param name="score"></param>
        private void SetRadioValue(int groupIndex, int score)
        {
            for (int i = 0; i < this.layoutControl1.Controls.Count; i++)
            {
                if (layoutControl1.Controls[i] is CheckEdit)
                {
                    CheckEdit chk = (CheckEdit)layoutControl1.Controls[i];
                    if (chk.Properties.RadioGroupIndex == groupIndex)//同一个组下的
                    {
                        int tag = Convert.ToInt32(chk.Tag);
                        if (tag == score)
                        {

                            //chk.Visible = true;
                            chk.ForeColor = System.Drawing.Color.Blue;
                        }
                        else
                        {
                            //chk.Text = "";
                            chk.Visible = false;
                            chk.ForeColor = System.Drawing.Color.White;

                        }
                    }
                }
            }
        }

        private void UC老年人中医药健康管理服务记录表_Load(object sender, EventArgs e)
        {
            this.ucpinghdf.Txt1.Properties.ReadOnly = true;
            this.ucpinghdf.Txt1.BackColor = System.Drawing.Color.White;
            this.ucqiydf.Txt1.Properties.ReadOnly = true;
            this.ucqiydf.Txt1.BackColor = System.Drawing.Color.White;
            this.ucqxdf.Txt1.Properties.ReadOnly = true;
            this.ucqxdf.Txt1.BackColor = System.Drawing.Color.White;
            this.ucshirdf.Txt1.Properties.ReadOnly = true;
            this.ucshirdf.Txt1.BackColor = System.Drawing.Color.White;
            this.uctansdf.Txt1.Properties.ReadOnly = true;
            this.uctansdf.Txt1.BackColor = System.Drawing.Color.White;
            this.uctebdf.Txt1.Properties.ReadOnly = true;
            this.uctebdf.Txt1.BackColor = System.Drawing.Color.White;
            this.ucxueydf.Txt1.Properties.ReadOnly = true;
            this.ucxueydf.Txt1.BackColor = System.Drawing.Color.White;
            this.ucyangdf.Txt1.Properties.ReadOnly = true;
            this.ucyangdf.Txt1.BackColor = System.Drawing.Color.White;
            this.ucyindf.Txt1.Properties.ReadOnly = true;
            this.ucyindf.Txt1.BackColor = System.Drawing.Color.White;

            int isallow = DataDictCache.Cache.IsAllow延时加载数据();
            if (isallow > 0 && !Loginer.CurrentUser.IsSubAdmin())
            {
                this.lbl最近更新时间.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.lbl最近修改人.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

        private void setControlReadonly(bool p)
        {
            for (int i = 0; i < this.Controls.Count; i++)
            {
                if (Controls[i] is CheckEdit)
                {
                    CheckEdit chk = (CheckEdit)Controls[i];
                    chk.Properties.ReadOnly = p;
                }
            }
        }

        private void btn添加_Click(object sender, EventArgs e)
        {
            UC老年人中医药健康管理服务记录表 uc = new UC老年人中医药健康管理服务记录表(_frm, UpdateType.Add);
            ShowControl(uc, DockStyle.Fill);
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.所属机构].ToString()))
            {
                _frm._param = _id;
                UC老年人中医药健康管理服务记录表 uc = new UC老年人中医药健康管理服务记录表(_frm, UpdateType.Modify);
                ShowControl(uc, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_ds老年人中医药.Tables[tb_老年人中医药特征管理.__TableName].Rows[0][tb_老年人中医药特征管理.所属机构].ToString()))
            {
                if (Msg.AskQuestion("是否要删除此条记录？"))
                {
                    if (_Bll.Delete(_id))
                    {
                        Msg.ShowInformation("删除数据成功！");
                        UC老年人中医药健康管理服务记录表列表 uc = new UC老年人中医药健康管理服务记录表列表(_frm);
                        ShowControl(uc);
                    }
                    else
                    {
                        Msg.ShowInformation("删除数据失败，请联系管理员！");
                    }
                }
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }


        }

        //导出正面
        private void btn导出正面_Click(object sender, EventArgs e)
        {
            string docNo = this.txt个人档案号.Text.Trim();
            string _date = this.dte填表日期.Text.Trim();
            if(!string.IsNullOrEmpty(docNo))
            {
                report老年人中医药健康管理 report1 = new report老年人中医药健康管理(docNo, _date);
                ReportPrintTool tool1 = new ReportPrintTool(report1);
                tool1.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }

        }

        //导出反面
        private void btn导出反面_Click(object sender, EventArgs e)
        {
            string docNo = this.txt个人档案号.Text.Trim();
            string _date = this.dte填表日期.Text.Trim();
            report老年人中医药健康管理反面 report2 = new report老年人中医药健康管理反面(docNo, _date);
            ReportPrintTool tool2 = new ReportPrintTool(report2);
            tool2.ShowPreviewDialog();
        }
    }
}
