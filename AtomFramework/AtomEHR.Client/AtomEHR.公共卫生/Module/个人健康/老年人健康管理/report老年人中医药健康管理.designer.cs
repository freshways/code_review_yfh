﻿namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    partial class report老年人中医药健康管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable40 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_19_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_19_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable39 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_19_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_19_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_19_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable38 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_18_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_18_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_18_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_18_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_18_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_17_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_17_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_17_5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_16_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_16_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_16_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_17_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_17_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_16_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_16_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_15_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_15_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_15_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_15_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_15_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_14_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_14_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_14_5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_14_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_14_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_13_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_13_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_13_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_13_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_13_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_12_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_12_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_12_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_12_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_12_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_11_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_11_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_11_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_11_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_11_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_10_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_10_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_10_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_10_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_10_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_9_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_9_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_9_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_9_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_9_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_9_5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_9_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_9_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_9_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_9_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_8_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_8_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_8_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_8_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_8_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_7_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_7_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_7_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_7_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_7_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_6_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_6_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_6_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_5_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_5_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_5_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_4_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_4_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_4_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_3_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_3_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_3_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_6_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_6_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_5_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_5_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_4_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_4_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_3_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_3_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_2_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_2_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_2_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_2_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_2_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_1_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_1_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_1_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_1_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_1_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt姓名,
            this.xrTable40,
            this.xrTable39,
            this.xrTable38,
            this.xrTable37,
            this.xrTable36,
            this.xrTable35,
            this.xrTable34,
            this.xrTable33,
            this.xrTable32,
            this.xrTable31,
            this.xrTable30,
            this.xrTable29,
            this.xrTable28,
            this.xrTable27,
            this.xrTable25,
            this.xrTable26,
            this.xrTable24,
            this.xrTable23,
            this.xrTable22,
            this.xrTable21,
            this.xrTable19,
            this.xrTable20,
            this.xrTable17,
            this.xrTable18,
            this.xrTable15,
            this.xrTable16,
            this.xrTable13,
            this.xrTable11,
            this.xrTable9,
            this.xrTable8,
            this.xrTable14,
            this.xrTable12,
            this.xrTable10,
            this.xrTable7,
            this.xrTable6,
            this.xrTable5,
            this.xrTable4,
            this.xrTable3,
            this.xrTable2,
            this.xrTable1,
            this.xrLabel2,
            this.xrLabel3,
            this.xrLabel_编号1,
            this.xrLabel_编号2,
            this.xrLabel_编号4,
            this.xrLabel_编号3,
            this.xrLabel_编号5,
            this.xrLabel_编号6,
            this.xrLabel_编号7,
            this.xrLabel_编号8,
            this.xrLine1,
            this.xrLabel1});
            this.Detail.Font = new System.Drawing.Font("宋体", 10F);
            this.Detail.HeightF = 844.7083F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt姓名
            // 
            this.txt姓名.Font = new System.Drawing.Font("宋体", 11F);
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(83.33334F, 60.70836F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(150.8333F, 23F);
            this.txt姓名.StylePriority.UseFont = false;
            this.txt姓名.StylePriority.UseTextAlignment = false;
            this.txt姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable40
            // 
            this.xrTable40.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable40.LocationFloat = new DevExpress.Utils.PointFloat(0F, 733.7084F);
            this.xrTable40.Name = "xrTable40";
            this.xrTable40.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40});
            this.xrTable40.SizeF = new System.Drawing.SizeF(809.9999F, 45F);
            this.xrTable40.StylePriority.UseBorders = false;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell118,
            this.xrTable_特征_19_1,
            this.xrTable_特征_19_2});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel54,
            this.xrLabel53});
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Weight = 2.2222222222222223D;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(10F, 23F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(381.1666F, 22F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "（指皮肤在没有外伤的情况下出现青一块紫一块的情况）";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(10F, 0F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(409.2916F, 23F);
            this.xrLabel53.StylePriority.UseBorders = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.Text = "（19）您的皮肤在不知不觉中会出现青紫瘀斑、皮下出血吗？";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_19_1
            // 
            this.xrTable_特征_19_1.Name = "xrTable_特征_19_1";
            this.xrTable_特征_19_1.Weight = 0.3888888888888889D;
            // 
            // xrTable_特征_19_2
            // 
            this.xrTable_特征_19_2.Name = "xrTable_特征_19_2";
            this.xrTable_特征_19_2.Weight = 0.3888888888888889D;
            // 
            // xrTable39
            // 
            this.xrTable39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable39.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 733.7084F);
            this.xrTable39.Name = "xrTable39";
            this.xrTable39.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39});
            this.xrTable39.SizeF = new System.Drawing.SizeF(315.0001F, 45F);
            this.xrTable39.StylePriority.UseBorders = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_19_3,
            this.xrTable_特征_19_4,
            this.xrTable_特征_19_5});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTable_特征_19_3
            // 
            this.xrTable_特征_19_3.Name = "xrTable_特征_19_3";
            this.xrTable_特征_19_3.Weight = 0.99973434967590735D;
            // 
            // xrTable_特征_19_4
            // 
            this.xrTable_特征_19_4.Name = "xrTable_特征_19_4";
            this.xrTable_特征_19_4.Weight = 0.99973434967590724D;
            // 
            // xrTable_特征_19_5
            // 
            this.xrTable_特征_19_5.Name = "xrTable_特征_19_5";
            this.xrTable_特征_19_5.Weight = 0.99973435589372106D;
            // 
            // xrTable38
            // 
            this.xrTable38.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable38.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 703.7084F);
            this.xrTable38.Name = "xrTable38";
            this.xrTable38.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable38.SizeF = new System.Drawing.SizeF(315.0001F, 30F);
            this.xrTable38.StylePriority.UseBorders = false;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_18_3,
            this.xrTable_特征_18_4,
            this.xrTable_特征_18_5});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTable_特征_18_3
            // 
            this.xrTable_特征_18_3.Name = "xrTable_特征_18_3";
            this.xrTable_特征_18_3.Weight = 0.999734567549257D;
            // 
            // xrTable_特征_18_4
            // 
            this.xrTable_特征_18_4.Name = "xrTable_特征_18_4";
            this.xrTable_特征_18_4.Weight = 0.999734567549257D;
            // 
            // xrTable_特征_18_5
            // 
            this.xrTable_特征_18_5.Name = "xrTable_特征_18_5";
            this.xrTable_特征_18_5.Weight = 0.999734501434224D;
            // 
            // xrTable37
            // 
            this.xrTable37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable37.LocationFloat = new DevExpress.Utils.PointFloat(0F, 703.7085F);
            this.xrTable37.Name = "xrTable37";
            this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37});
            this.xrTable37.SizeF = new System.Drawing.SizeF(809.9999F, 30F);
            this.xrTable37.StylePriority.UseBorders = false;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTable_特征_18_1,
            this.xrTable_特征_18_2});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel52});
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 2.2222222222222223D;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(10F, 3F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(440.5416F, 23F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "（18）您的皮肤容易起荨麻疹吗？（包括风团、风疹块、风疙瘩）";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_18_1
            // 
            this.xrTable_特征_18_1.Name = "xrTable_特征_18_1";
            this.xrTable_特征_18_1.Weight = 0.3888888888888889D;
            // 
            // xrTable_特征_18_2
            // 
            this.xrTable_特征_18_2.Name = "xrTable_特征_18_2";
            this.xrTable_特征_18_2.Weight = 0.3888888888888889D;
            // 
            // xrTable36
            // 
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.LocationFloat = new DevExpress.Utils.PointFloat(809.9999F, 658.7085F);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable36.SizeF = new System.Drawing.SizeF(315.0001F, 45F);
            this.xrTable36.StylePriority.UseBorders = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_17_3,
            this.xrLabel47});
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Weight = 1D;
            // 
            // xrTable_特征_17_3
            // 
            this.xrTable_特征_17_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_17_3.LocationFloat = new DevExpress.Utils.PointFloat(27.54181F, 1F);
            this.xrTable_特征_17_3.Name = "xrTable_特征_17_3";
            this.xrTable_特征_17_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_17_3.SizeF = new System.Drawing.SizeF(49.66687F, 20F);
            this.xrTable_特征_17_3.StylePriority.UseBorders = false;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(104.6669F, 23F);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.Text = "一年3- 4次";
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_17_4,
            this.xrLabel49});
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 1D;
            // 
            // xrTable_特征_17_4
            // 
            this.xrTable_特征_17_4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_17_4.LocationFloat = new DevExpress.Utils.PointFloat(27.54181F, 1F);
            this.xrTable_特征_17_4.Name = "xrTable_特征_17_4";
            this.xrTable_特征_17_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_17_4.SizeF = new System.Drawing.SizeF(52.82269F, 20F);
            this.xrTable_特征_17_4.StylePriority.UseBorders = false;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(0F, 21F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(104.0364F, 23F);
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.Text = "一年5- 6次";
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_17_5,
            this.xrLabel51});
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Weight = 1D;
            // 
            // xrTable_特征_17_5
            // 
            this.xrTable_特征_17_5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_17_5.LocationFloat = new DevExpress.Utils.PointFloat(31.37996F, 1.000027F);
            this.xrTable_特征_17_5.Name = "xrTable_特征_17_5";
            this.xrTable_特征_17_5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_17_5.SizeF = new System.Drawing.SizeF(45F, 20F);
            this.xrTable_特征_17_5.StylePriority.UseBorders = false;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 6.75F);
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(0.3334122F, 20.99998F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(104F, 23F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "每次遇到上述原因都过敏";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable35
            // 
            this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable35.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 628.7085F);
            this.xrTable35.Name = "xrTable35";
            this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTable35.SizeF = new System.Drawing.SizeF(315.0001F, 30F);
            this.xrTable35.StylePriority.UseBorders = false;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_16_3,
            this.xrTable_特征_16_4,
            this.xrTable_特征_16_5});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTable_特征_16_3
            // 
            this.xrTable_特征_16_3.Name = "xrTable_特征_16_3";
            this.xrTable_特征_16_3.Weight = 1D;
            // 
            // xrTable_特征_16_4
            // 
            this.xrTable_特征_16_4.Name = "xrTable_特征_16_4";
            this.xrTable_特征_16_4.Weight = 1D;
            // 
            // xrTable_特征_16_5
            // 
            this.xrTable_特征_16_5.Name = "xrTable_特征_16_5";
            this.xrTable_特征_16_5.Weight = 1D;
            // 
            // xrTable34
            // 
            this.xrTable34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable34.LocationFloat = new DevExpress.Utils.PointFloat(0F, 658.7085F);
            this.xrTable34.Name = "xrTable34";
            this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrTable34.SizeF = new System.Drawing.SizeF(809.9999F, 45F);
            this.xrTable34.StylePriority.UseBorders = false;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel41});
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Weight = 2.2222222222222223D;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(10.08396F, 10.00006F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(548F, 23F);
            this.xrLabel41.StylePriority.UseBorders = false;
            this.xrLabel41.StylePriority.UseTextAlignment = false;
            this.xrLabel41.Text = "（17）您容易过敏（对药物、食物、气味、花粉或在季节交替、气候变化时）吗？";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_17_1,
            this.xrLabel43});
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 0.38889069733796289D;
            // 
            // xrTable_特征_17_1
            // 
            this.xrTable_特征_17_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_17_1.LocationFloat = new DevExpress.Utils.PointFloat(27.70862F, 1.00001F);
            this.xrTable_特征_17_1.Name = "xrTable_特征_17_1";
            this.xrTable_特征_17_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_17_1.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrTable_特征_17_1.StylePriority.UseBorders = false;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 20.99999F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.Text = "从来没有";
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_17_2,
            this.xrLabel45});
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Weight = 0.38888708043981479D;
            // 
            // xrTable_特征_17_2
            // 
            this.xrTable_特征_17_2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_17_2.LocationFloat = new DevExpress.Utils.PointFloat(27.70813F, 1.00001F);
            this.xrTable_特征_17_2.Name = "xrTable_特征_17_2";
            this.xrTable_特征_17_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_17_2.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrTable_特征_17_2.StylePriority.UseBorders = false;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(0F, 20.99999F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.Text = "一年1- 2次";
            // 
            // xrTable33
            // 
            this.xrTable33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable33.LocationFloat = new DevExpress.Utils.PointFloat(0F, 628.7085F);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrTable33.SizeF = new System.Drawing.SizeF(809.9999F, 30F);
            this.xrTable33.StylePriority.UseBorders = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.xrTable_特征_16_1,
            this.xrTable_特征_16_2});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel40});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 2.2222222222222223D;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(10.08396F, 3F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(265F, 23F);
            this.xrLabel40.StylePriority.UseBorders = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "（16）您有口粘口腻，或睡眠打鼾吗？";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_16_1
            // 
            this.xrTable_特征_16_1.Name = "xrTable_特征_16_1";
            this.xrTable_特征_16_1.Weight = 0.3888891149450231D;
            // 
            // xrTable_特征_16_2
            // 
            this.xrTable_特征_16_2.Name = "xrTable_特征_16_2";
            this.xrTable_特征_16_2.Weight = 0.38888866283275458D;
            // 
            // xrTable32
            // 
            this.xrTable32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable32.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 598.7085F);
            this.xrTable32.Name = "xrTable32";
            this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.xrTable32.SizeF = new System.Drawing.SizeF(315.0001F, 30F);
            this.xrTable32.StylePriority.UseBorders = false;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_15_3,
            this.xrTable_特征_15_4,
            this.xrTable_特征_15_5});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTable_特征_15_3
            // 
            this.xrTable_特征_15_3.Name = "xrTable_特征_15_3";
            this.xrTable_特征_15_3.Weight = 1D;
            // 
            // xrTable_特征_15_4
            // 
            this.xrTable_特征_15_4.Name = "xrTable_特征_15_4";
            this.xrTable_特征_15_4.Weight = 1D;
            // 
            // xrTable_特征_15_5
            // 
            this.xrTable_特征_15_5.Name = "xrTable_特征_15_5";
            this.xrTable_特征_15_5.Weight = 1D;
            // 
            // xrTable31
            // 
            this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable31.LocationFloat = new DevExpress.Utils.PointFloat(0F, 598.7085F);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable31.SizeF = new System.Drawing.SizeF(809.9999F, 30F);
            this.xrTable31.StylePriority.UseBorders = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell91,
            this.xrTable_特征_15_1,
            this.xrTable_特征_15_2});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel39});
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Weight = 2.2222222222222223D;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(10.08396F, 3.000061F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(293.75F, 23F);
            this.xrLabel39.StylePriority.UseBorders = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "（15）您没有感冒时也会鼻塞、流鼻涕吗？";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_15_1
            // 
            this.xrTable_特征_15_1.Name = "xrTable_特征_15_1";
            this.xrTable_特征_15_1.Weight = 0.38888888888888878D;
            // 
            // xrTable_特征_15_2
            // 
            this.xrTable_特征_15_2.Name = "xrTable_特征_15_2";
            this.xrTable_特征_15_2.Weight = 0.3888888888888889D;
            // 
            // xrTable30
            // 
            this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable30.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 553.7085F);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrTable30.SizeF = new System.Drawing.SizeF(315.0001F, 45F);
            this.xrTable30.StylePriority.UseBorders = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell88,
            this.xrTableCell89,
            this.xrTableCell90});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_14_3,
            this.xrLabel34});
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 1D;
            // 
            // xrTable_特征_14_3
            // 
            this.xrTable_特征_14_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_14_3.LocationFloat = new DevExpress.Utils.PointFloat(25.34339F, 0F);
            this.xrTable_特征_14_3.Name = "xrTable_特征_14_3";
            this.xrTable_特征_14_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_14_3.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrTable_特征_14_3.StylePriority.UseBorders = false;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(0F, 19.99998F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.Text = "一年感冒5- 6次";
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_14_4,
            this.xrLabel36});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 1D;
            // 
            // xrTable_特征_14_4
            // 
            this.xrTable_特征_14_4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_14_4.LocationFloat = new DevExpress.Utils.PointFloat(30.03082F, 0F);
            this.xrTable_特征_14_4.Name = "xrTable_特征_14_4";
            this.xrTable_特征_14_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_14_4.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrTable_特征_14_4.StylePriority.UseBorders = false;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(0F, 20.00001F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.Text = "一年8次以上";
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_14_5,
            this.xrLabel38});
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 1D;
            // 
            // xrTable_特征_14_5
            // 
            this.xrTable_特征_14_5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_14_5.LocationFloat = new DevExpress.Utils.PointFloat(31.04655F, 0F);
            this.xrTable_特征_14_5.Name = "xrTable_特征_14_5";
            this.xrTable_特征_14_5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_14_5.SizeF = new System.Drawing.SizeF(45F, 20F);
            this.xrTable_特征_14_5.StylePriority.UseBorders = false;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(0F, 19.99995F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(104F, 23F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "几乎每月都感冒";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable29
            // 
            this.xrTable29.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable29.LocationFloat = new DevExpress.Utils.PointFloat(0F, 553.7085F);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTable29.SizeF = new System.Drawing.SizeF(809.9999F, 45F);
            this.xrTable29.StylePriority.UseBorders = false;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel28});
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 2.2222222222222223D;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(10.08396F, 10F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(325F, 23F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "（14）您容易患感冒吗？（指每年感冒的次数）";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel30,
            this.xrTable_特征_14_1});
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 0.3888888888888889D;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(0F, 19.99998F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.Text = "一年<2次";
            // 
            // xrTable_特征_14_1
            // 
            this.xrTable_特征_14_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_14_1.LocationFloat = new DevExpress.Utils.PointFloat(27.70837F, 0F);
            this.xrTable_特征_14_1.Name = "xrTable_特征_14_1";
            this.xrTable_特征_14_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_14_1.SizeF = new System.Drawing.SizeF(50.00001F, 20F);
            this.xrTable_特征_14_1.StylePriority.UseBorders = false;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_14_2,
            this.xrLabel32});
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Weight = 0.3888888888888889D;
            // 
            // xrTable_特征_14_2
            // 
            this.xrTable_特征_14_2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_14_2.LocationFloat = new DevExpress.Utils.PointFloat(25.62517F, 0F);
            this.xrTable_特征_14_2.Name = "xrTable_特征_14_2";
            this.xrTable_特征_14_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_14_2.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrTable_特征_14_2.StylePriority.UseBorders = false;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(0F, 19.99998F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel32.StylePriority.UseBorders = false;
            this.xrLabel32.Text = "一年感冒2- 4次";
            // 
            // xrTable28
            // 
            this.xrTable28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable28.LocationFloat = new DevExpress.Utils.PointFloat(809.9999F, 523.7085F);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable28.SizeF = new System.Drawing.SizeF(315.0001F, 30F);
            this.xrTable28.StylePriority.UseBorders = false;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_13_3,
            this.xrTable_特征_13_4,
            this.xrTable_特征_13_5});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTable_特征_13_3
            // 
            this.xrTable_特征_13_3.Name = "xrTable_特征_13_3";
            this.xrTable_特征_13_3.Weight = 1D;
            // 
            // xrTable_特征_13_4
            // 
            this.xrTable_特征_13_4.Name = "xrTable_特征_13_4";
            this.xrTable_特征_13_4.Weight = 1D;
            // 
            // xrTable_特征_13_5
            // 
            this.xrTable_特征_13_5.Name = "xrTable_特征_13_5";
            this.xrTable_特征_13_5.Weight = 1D;
            // 
            // xrTable27
            // 
            this.xrTable27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable27.LocationFloat = new DevExpress.Utils.PointFloat(0F, 523.7085F);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTable27.SizeF = new System.Drawing.SizeF(809.9999F, 30F);
            this.xrTable27.StylePriority.UseBorders = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTable_特征_13_1,
            this.xrTable_特征_13_2});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel27});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Text = "xrTableCell79";
            this.xrTableCell79.Weight = 2.2222222222222223D;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(10.08395F, 4F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(576.3743F, 23F);
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "（13）您比一般人耐受不了寒冷吗？(指比别人容易害怕冬天或是夏天的冷空调、电风扇等)";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_13_1
            // 
            this.xrTable_特征_13_1.Name = "xrTable_特征_13_1";
            this.xrTable_特征_13_1.Weight = 0.38888888888888884D;
            // 
            // xrTable_特征_13_2
            // 
            this.xrTable_特征_13_2.Name = "xrTable_特征_13_2";
            this.xrTable_特征_13_2.Weight = 0.38888888888888884D;
            // 
            // xrTable25
            // 
            this.xrTable25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable25.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 473.7084F);
            this.xrTable25.Name = "xrTable25";
            this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTable25.SizeF = new System.Drawing.SizeF(315.0001F, 49.99997F);
            this.xrTable25.StylePriority.UseBorders = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_12_3,
            this.xrTable_特征_12_4,
            this.xrTable_特征_12_5});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTable_特征_12_3
            // 
            this.xrTable_特征_12_3.Name = "xrTable_特征_12_3";
            this.xrTable_特征_12_3.Weight = 1D;
            // 
            // xrTable_特征_12_4
            // 
            this.xrTable_特征_12_4.Name = "xrTable_特征_12_4";
            this.xrTable_特征_12_4.Weight = 1D;
            // 
            // xrTable_特征_12_5
            // 
            this.xrTable_特征_12_5.Name = "xrTable_特征_12_5";
            this.xrTable_特征_12_5.Weight = 1D;
            // 
            // xrTable26
            // 
            this.xrTable26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable26.LocationFloat = new DevExpress.Utils.PointFloat(0.0003814697F, 473.7084F);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable26.SizeF = new System.Drawing.SizeF(810F, 49.99997F);
            this.xrTable26.StylePriority.UseBorders = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTable_特征_12_1,
            this.xrTable_特征_12_2});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel26,
            this.xrLabel25});
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 2.2222222222222223D;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(10.41671F, 23.00005F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(171.4583F, 23.00006F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "有一处或多处怕冷）";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(10.08357F, 0F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(535.8334F, 23.00006F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "（12）您胃脘部、背部或腰膝部怕冷吗？（指上腹部、背部、腰部或膝关节等，";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_12_1
            // 
            this.xrTable_特征_12_1.Name = "xrTable_特征_12_1";
            this.xrTable_特征_12_1.Weight = 0.388889001916956D;
            // 
            // xrTable_特征_12_2
            // 
            this.xrTable_特征_12_2.Name = "xrTable_特征_12_2";
            this.xrTable_特征_12_2.Weight = 0.38888877586082182D;
            // 
            // xrTable24
            // 
            this.xrTable24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable24.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 443.7084F);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable24.SizeF = new System.Drawing.SizeF(315.0001F, 30.00003F);
            this.xrTable24.StylePriority.UseBorders = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_11_3,
            this.xrTable_特征_11_4,
            this.xrTable_特征_11_5});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTable_特征_11_3
            // 
            this.xrTable_特征_11_3.Name = "xrTable_特征_11_3";
            this.xrTable_特征_11_3.Weight = 1D;
            // 
            // xrTable_特征_11_4
            // 
            this.xrTable_特征_11_4.Name = "xrTable_特征_11_4";
            this.xrTable_特征_11_4.Weight = 1D;
            // 
            // xrTable_特征_11_5
            // 
            this.xrTable_特征_11_5.Name = "xrTable_特征_11_5";
            this.xrTable_特征_11_5.Weight = 1D;
            // 
            // xrTable23
            // 
            this.xrTable23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable23.LocationFloat = new DevExpress.Utils.PointFloat(0.0003814697F, 443.7084F);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable23.SizeF = new System.Drawing.SizeF(810F, 30.00003F);
            this.xrTable23.StylePriority.UseBorders = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTable_特征_11_1,
            this.xrTable_特征_11_2});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel24});
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Weight = 2.2222222222222223D;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(10F, 6.999938F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(490F, 23.00006F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "（11）您手脚发凉吗？（不包含因周围温度低或穿的少导致的手脚发冷）";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_11_1
            // 
            this.xrTable_特征_11_1.Name = "xrTable_特征_11_1";
            this.xrTable_特征_11_1.Weight = 0.388889001916956D;
            // 
            // xrTable_特征_11_2
            // 
            this.xrTable_特征_11_2.Name = "xrTable_特征_11_2";
            this.xrTable_特征_11_2.Weight = 0.38888877586082182D;
            // 
            // xrTable22
            // 
            this.xrTable22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable22.LocationFloat = new DevExpress.Utils.PointFloat(809.9999F, 413.7084F);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.xrTable22.SizeF = new System.Drawing.SizeF(315.0001F, 29.99997F);
            this.xrTable22.StylePriority.UseBorders = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_10_3,
            this.xrTable_特征_10_4,
            this.xrTable_特征_10_5});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTable_特征_10_3
            // 
            this.xrTable_特征_10_3.Name = "xrTable_特征_10_3";
            this.xrTable_特征_10_3.Weight = 1D;
            // 
            // xrTable_特征_10_4
            // 
            this.xrTable_特征_10_4.Name = "xrTable_特征_10_4";
            this.xrTable_特征_10_4.Weight = 1D;
            // 
            // xrTable_特征_10_5
            // 
            this.xrTable_特征_10_5.Name = "xrTable_特征_10_5";
            this.xrTable_特征_10_5.Weight = 1D;
            // 
            // xrTable21
            // 
            this.xrTable21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 413.7084F);
            this.xrTable21.Name = "xrTable21";
            this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable21.SizeF = new System.Drawing.SizeF(809.9999F, 29.99997F);
            this.xrTable21.StylePriority.UseBorders = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTable_特征_10_1,
            this.xrTable_特征_10_2});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel23});
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 1.8978049550813063D;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7.000061F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(160F, 23F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "（10）您眼睛干涩吗？";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_10_1
            // 
            this.xrTable_特征_10_1.Name = "xrTable_特征_10_1";
            this.xrTable_特征_10_1.Weight = 0.3321158899549671D;
            // 
            // xrTable_特征_10_2
            // 
            this.xrTable_特征_10_2.Name = "xrTable_特征_10_2";
            this.xrTable_特征_10_2.Weight = 0.33211596594462928D;
            // 
            // xrTable19
            // 
            this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable19.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 363.7084F);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable19.SizeF = new System.Drawing.SizeF(315.0001F, 50F);
            this.xrTable19.StylePriority.UseBorders = false;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_9_3,
            this.xrTable_9_4,
            this.xrTable_9_5});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTable_9_3
            // 
            this.xrTable_9_3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_9_3,
            this.xrLabel33});
            this.xrTable_9_3.Name = "xrTable_9_3";
            this.xrTable_9_3.Weight = 1D;
            // 
            // xrTable_特征_9_3
            // 
            this.xrTable_特征_9_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_9_3.LocationFloat = new DevExpress.Utils.PointFloat(27.5414F, 7F);
            this.xrTable_特征_9_3.Name = "xrTable_特征_9_3";
            this.xrTable_特征_9_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_9_3.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrTable_特征_9_3.StylePriority.UseBorders = false;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26.99999F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel33.StylePriority.UseBorders = false;
            this.xrLabel33.Text = "25≤BMI<26";
            // 
            // xrTable_9_4
            // 
            this.xrTable_9_4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_9_4,
            this.xrLabel35});
            this.xrTable_9_4.Name = "xrTable_9_4";
            this.xrTable_9_4.Weight = 1D;
            // 
            // xrTable_特征_9_4
            // 
            this.xrTable_特征_9_4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_9_4.LocationFloat = new DevExpress.Utils.PointFloat(30.03082F, 6.999908F);
            this.xrTable_特征_9_4.Name = "xrTable_特征_9_4";
            this.xrTable_特征_9_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_9_4.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrTable_特征_9_4.StylePriority.UseBorders = false;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(0F, 27.00002F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.Text = "26≤BMI<28";
            // 
            // xrTable_9_5
            // 
            this.xrTable_9_5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_9_5,
            this.xrLabel37});
            this.xrTable_9_5.Name = "xrTable_9_5";
            this.xrTable_9_5.Weight = 1D;
            // 
            // xrTable_特征_9_5
            // 
            this.xrTable_特征_9_5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_9_5.LocationFloat = new DevExpress.Utils.PointFloat(26.37929F, 6.999908F);
            this.xrTable_特征_9_5.Name = "xrTable_特征_9_5";
            this.xrTable_特征_9_5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_9_5.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrTable_特征_9_5.StylePriority.UseBorders = false;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(0F, 27.00005F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel37.StylePriority.UseBorders = false;
            this.xrLabel37.Text = "BMI≥28";
            // 
            // xrTable20
            // 
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.LocationFloat = new DevExpress.Utils.PointFloat(0F, 363.7083F);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable20.SizeF = new System.Drawing.SizeF(809.9999F, 50F);
            this.xrTable20.StylePriority.UseBorders = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.xrTable_9_1,
            this.xrTable_9_2});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel22,
            this.xrLabel21});
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell58.Weight = 2.1947860620406385D;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(10.08345F, 22.99999F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(265F, 23F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "[BMI  指数=体重（kg）/身高²（m）]";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(10.08345F, 0F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(352F, 23F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "（9）您感到身体超重不轻松吗？（感觉身体沉重）";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_9_1
            // 
            this.xrTable_9_1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_9_1,
            this.xrLabel29});
            this.xrTable_9_1.Name = "xrTable_9_1";
            this.xrTable_9_1.Weight = 0.38408756870324279D;
            // 
            // xrTable_特征_9_1
            // 
            this.xrTable_特征_9_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_9_1.LocationFloat = new DevExpress.Utils.PointFloat(27.70864F, 6.999995F);
            this.xrTable_特征_9_1.Name = "xrTable_特征_9_1";
            this.xrTable_特征_9_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_9_1.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrTable_特征_9_1.StylePriority.UseBorders = false;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26.99999F);
            this.xrLabel29.Multiline = true;
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.Text = "BMI<24\r\n";
            // 
            // xrTable_9_2
            // 
            this.xrTable_9_2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_9_2,
            this.xrLabel31});
            this.xrTable_9_2.Name = "xrTable_9_2";
            this.xrTable_9_2.Weight = 0.38408752377000743D;
            // 
            // xrTable_特征_9_2
            // 
            this.xrTable_特征_9_2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_9_2.LocationFloat = new DevExpress.Utils.PointFloat(27.70859F, 7F);
            this.xrTable_特征_9_2.Name = "xrTable_特征_9_2";
            this.xrTable_特征_9_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_9_2.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrTable_特征_9_2.StylePriority.UseBorders = false;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(0F, 27.00007F);
            this.xrLabel31.Multiline = true;
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel31.StylePriority.UseBorders = false;
            this.xrLabel31.Text = "24≤BMI<25";
            // 
            // xrTable17
            // 
            this.xrTable17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 333.7083F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable17.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable17.StylePriority.UseBorders = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTable_特征_8_1,
            this.xrTable_特征_8_2});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel20});
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell49.Weight = 2.222222227136486D;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7.000031F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(265F, 23F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "（8）您容易感到害怕或受到惊吓吗？";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_8_1
            // 
            this.xrTable_特征_8_1.Name = "xrTable_特征_8_1";
            this.xrTable_特征_8_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_8_2
            // 
            this.xrTable_特征_8_2.Name = "xrTable_特征_8_2";
            this.xrTable_特征_8_2.Weight = 0.388888886431757D;
            // 
            // xrTable18
            // 
            this.xrTable18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(810.0002F, 333.7084F);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable18.SizeF = new System.Drawing.SizeF(314.9999F, 30F);
            this.xrTable18.StylePriority.UseBorders = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_8_3,
            this.xrTable_特征_8_4,
            this.xrTable_特征_8_5});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTable_特征_8_3
            // 
            this.xrTable_特征_8_3.Name = "xrTable_特征_8_3";
            this.xrTable_特征_8_3.Weight = 1D;
            // 
            // xrTable_特征_8_4
            // 
            this.xrTable_特征_8_4.Name = "xrTable_特征_8_4";
            this.xrTable_特征_8_4.Weight = 1D;
            // 
            // xrTable_特征_8_5
            // 
            this.xrTable_特征_8_5.Name = "xrTable_特征_8_5";
            this.xrTable_特征_8_5.Weight = 1D;
            // 
            // xrTable15
            // 
            this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 303.7083F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable15.SizeF = new System.Drawing.SizeF(809.9999F, 30F);
            this.xrTable15.StylePriority.UseBorders = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTable_特征_7_1,
            this.xrTable_特征_7_2});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel19});
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell43.Weight = 2.222222227136486D;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7.000001F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(340.625F, 22.99997F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "（7）您因为生活状态改变而感到孤独，失落吗？";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_7_1
            // 
            this.xrTable_特征_7_1.Name = "xrTable_特征_7_1";
            this.xrTable_特征_7_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_7_2
            // 
            this.xrTable_特征_7_2.Name = "xrTable_特征_7_2";
            this.xrTable_特征_7_2.Weight = 0.388888886431757D;
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(810.0002F, 303.7084F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable16.SizeF = new System.Drawing.SizeF(314.9999F, 30F);
            this.xrTable16.StylePriority.UseBorders = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_7_3,
            this.xrTable_特征_7_4,
            this.xrTable_特征_7_5});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTable_特征_7_3
            // 
            this.xrTable_特征_7_3.Name = "xrTable_特征_7_3";
            this.xrTable_特征_7_3.Weight = 1D;
            // 
            // xrTable_特征_7_4
            // 
            this.xrTable_特征_7_4.Name = "xrTable_特征_7_4";
            this.xrTable_特征_7_4.Weight = 1D;
            // 
            // xrTable_特征_7_5
            // 
            this.xrTable_特征_7_5.Name = "xrTable_特征_7_5";
            this.xrTable_特征_7_5.Weight = 1D;
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 273.7084F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable13.SizeF = new System.Drawing.SizeF(315.0001F, 30F);
            this.xrTable13.StylePriority.UseBorders = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_6_3,
            this.xrTable_特征_6_4,
            this.xrTable_特征_6_5});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTable_特征_6_3
            // 
            this.xrTable_特征_6_3.Name = "xrTable_特征_6_3";
            this.xrTable_特征_6_3.Weight = 1D;
            // 
            // xrTable_特征_6_4
            // 
            this.xrTable_特征_6_4.Name = "xrTable_特征_6_4";
            this.xrTable_特征_6_4.Weight = 1D;
            // 
            // xrTable_特征_6_5
            // 
            this.xrTable_特征_6_5.Name = "xrTable_特征_6_5";
            this.xrTable_特征_6_5.Weight = 1D;
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 243.7084F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable11.SizeF = new System.Drawing.SizeF(315.0001F, 30F);
            this.xrTable11.StylePriority.UseBorders = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_5_3,
            this.xrTable_特征_5_4,
            this.xrTable_特征_5_5});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTable_特征_5_3
            // 
            this.xrTable_特征_5_3.Name = "xrTable_特征_5_3";
            this.xrTable_特征_5_3.Weight = 1D;
            // 
            // xrTable_特征_5_4
            // 
            this.xrTable_特征_5_4.Name = "xrTable_特征_5_4";
            this.xrTable_特征_5_4.Weight = 1D;
            // 
            // xrTable_特征_5_5
            // 
            this.xrTable_特征_5_5.Name = "xrTable_特征_5_5";
            this.xrTable_特征_5_5.Weight = 1D;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 213.7083F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(315.0001F, 30.00002F);
            this.xrTable9.StylePriority.UseBorders = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_4_3,
            this.xrTable_特征_4_4,
            this.xrTable_特征_4_5});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTable_特征_4_3
            // 
            this.xrTable_特征_4_3.Name = "xrTable_特征_4_3";
            this.xrTable_特征_4_3.Weight = 1D;
            // 
            // xrTable_特征_4_4
            // 
            this.xrTable_特征_4_4.Name = "xrTable_特征_4_4";
            this.xrTable_特征_4_4.Weight = 1D;
            // 
            // xrTable_特征_4_5
            // 
            this.xrTable_特征_4_5.Name = "xrTable_特征_4_5";
            this.xrTable_特征_4_5.Weight = 1D;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 183.7083F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(314.9999F, 30F);
            this.xrTable8.StylePriority.UseBorders = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_3_3,
            this.xrTable_特征_3_4,
            this.xrTable_特征_3_5});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTable_特征_3_3
            // 
            this.xrTable_特征_3_3.Name = "xrTable_特征_3_3";
            this.xrTable_特征_3_3.Weight = 1D;
            // 
            // xrTable_特征_3_4
            // 
            this.xrTable_特征_3_4.Name = "xrTable_特征_3_4";
            this.xrTable_特征_3_4.Weight = 1D;
            // 
            // xrTable_特征_3_5
            // 
            this.xrTable_特征_3_5.Name = "xrTable_特征_3_5";
            this.xrTable_特征_3_5.Weight = 1D;
            // 
            // xrTable14
            // 
            this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 273.7084F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable14.SizeF = new System.Drawing.SizeF(809.9999F, 30F);
            this.xrTable14.StylePriority.UseBorders = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTable_特征_6_1,
            this.xrTable_特征_6_2});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel18});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell40.Weight = 2.222222227136486D;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7.000032F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(428.1251F, 23F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "（6）您容易精神紧张，焦虑不安吗？（指遇事是否心情紧张）";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_6_1
            // 
            this.xrTable_特征_6_1.Name = "xrTable_特征_6_1";
            this.xrTable_特征_6_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_6_2
            // 
            this.xrTable_特征_6_2.Name = "xrTable_特征_6_2";
            this.xrTable_特征_6_2.Weight = 0.388888886431757D;
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(0.0003814697F, 243.7083F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable12.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable12.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTable_特征_5_1,
            this.xrTable_特征_5_2});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel17});
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell34.Weight = 2.222222227136486D;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(456.25F, 23F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "（5）您感到闷闷不乐，情绪低沉吗？（指心情不愉快，情绪低落）";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_5_1
            // 
            this.xrTable_特征_5_1.Name = "xrTable_特征_5_1";
            this.xrTable_特征_5_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_5_2
            // 
            this.xrTable_特征_5_2.Name = "xrTable_特征_5_2";
            this.xrTable_特征_5_2.Weight = 0.388888886431757D;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(0.0003814697F, 213.7083F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(810F, 30.00002F);
            this.xrTable10.StylePriority.UseBorders = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTable_特征_4_1,
            this.xrTable_特征_4_2});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel16});
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell28.Weight = 2.222222227136486D;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7.000001F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(352.0834F, 23F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "（4）您说话声音低弱无力吗？（指说话没有力气）";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_4_1
            // 
            this.xrTable_特征_4_1.Name = "xrTable_特征_4_1";
            this.xrTable_特征_4_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_4_2
            // 
            this.xrTable_特征_4_2.Name = "xrTable_特征_4_2";
            this.xrTable_特征_4_2.Weight = 0.388888886431757D;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0.0002543131F, 183.7083F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable7.StylePriority.UseBorders = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTable_特征_3_1,
            this.xrTable_特征_3_2});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel15});
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 2.222222227136486D;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(9.999744F, 5.000007F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(305.2085F, 23F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "（3）您容易气短，呼吸短促，接不上气吗？";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_3_1
            // 
            this.xrTable_特征_3_1.Name = "xrTable_特征_3_1";
            this.xrTable_特征_3_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_3_2
            // 
            this.xrTable_特征_3_2.Name = "xrTable_特征_3_2";
            this.xrTable_特征_3_2.Weight = 0.388888886431757D;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(810.0002F, 153.7083F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(315.0001F, 30F);
            this.xrTable6.StylePriority.UseBorders = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_2_3,
            this.xrTable_特征_2_4,
            this.xrTable_特征_2_5});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTable_特征_2_3
            // 
            this.xrTable_特征_2_3.Name = "xrTable_特征_2_3";
            this.xrTable_特征_2_3.Weight = 1D;
            // 
            // xrTable_特征_2_4
            // 
            this.xrTable_特征_2_4.Name = "xrTable_特征_2_4";
            this.xrTable_特征_2_4.Weight = 1D;
            // 
            // xrTable_特征_2_5
            // 
            this.xrTable_特征_2_5.Name = "xrTable_特征_2_5";
            this.xrTable_特征_2_5.Weight = 1D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0.0002543131F, 153.7083F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable5.StylePriority.UseBorders = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTable_特征_2_1,
            this.xrTable_特征_2_2});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel14});
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 2.222222227136486D;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(9.999744F, 6.999969F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(576.4583F, 23.00002F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "（2）您容易疲乏吗？（指体力如何，是否稍微活动一下或做一点家务劳动就感到累）";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_2_1
            // 
            this.xrTable_特征_2_1.Name = "xrTable_特征_2_1";
            this.xrTable_特征_2_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_2_2
            // 
            this.xrTable_特征_2_2.Name = "xrTable_特征_2_2";
            this.xrTable_特征_2_2.Weight = 0.388888886431757D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 123.7083F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(314.9999F, 30F);
            this.xrTable4.StylePriority.UseBorders = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_1_3,
            this.xrTable_特征_1_4,
            this.xrTable_特征_1_5});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTable_特征_1_3
            // 
            this.xrTable_特征_1_3.Name = "xrTable_特征_1_3";
            this.xrTable_特征_1_3.Weight = 1D;
            // 
            // xrTable_特征_1_4
            // 
            this.xrTable_特征_1_4.Name = "xrTable_特征_1_4";
            this.xrTable_特征_1_4.Weight = 1D;
            // 
            // xrTable_特征_1_5
            // 
            this.xrTable_特征_1_5.Name = "xrTable_特征_1_5";
            this.xrTable_特征_1_5.Weight = 1D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0.0002543131F, 123.7083F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTable_特征_1_1,
            this.xrTable_特征_1_2});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel13});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell7.Weight = 1.3629629659770448D;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(9.999744F, 7.000032F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(340.6251F, 22.99998F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "（1）您精力充沛吗？（指精神头足，乐于做事）";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_1_1
            // 
            this.xrTable_特征_1_1.Name = "xrTable_特征_1_1";
            this.xrTable_特征_1_1.Weight = 0.23851851701147767D;
            // 
            // xrTable_特征_1_2
            // 
            this.xrTable_特征_1_2.Name = "xrTable_特征_1_2";
            this.xrTable_特征_1_2.Weight = 0.23851851701147767D;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(810.0004F, 83.70835F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(314.9999F, 40F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "有时\r\n(有些/少数时间)";
            this.xrTableCell4.Weight = 1.0038303539503697D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Multiline = true;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "经常\r\n(相当/多数时间)";
            this.xrTableCell5.Weight = 1.0038303940956266D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Multiline = true;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "总是\r\n(非常/每天)";
            this.xrTableCell6.Weight = 1.0038303940956268D;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0.0003178914F, 83.70832F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(810.0001F, 40.00001F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel12});
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 3.2028469507607245D;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(10.41678F, 10.00001F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(314.6668F, 23F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "请根据近一年的体检和感觉，回答一下问题。";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Multiline = true;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "没有\r\n(根本不/从来没有)";
            this.xrTableCell2.Weight = 0.56049822906960411D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Multiline = true;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "很少\r\n(有一点/偶尔) ";
            this.xrTableCell3.Weight = 0.56049823653977815D;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 60.70836F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(83.33334F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓名:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(820.0002F, 60.70836F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(65.34369F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "编号：";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号1
            // 
            this.xrLabel_编号1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号1.LocationFloat = new DevExpress.Utils.PointFloat(885.3439F, 63.70836F);
            this.xrLabel_编号1.Name = "xrLabel_编号1";
            this.xrLabel_编号1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号1.SizeF = new System.Drawing.SizeF(15.00006F, 15F);
            this.xrLabel_编号1.StylePriority.UseBorders = false;
            this.xrLabel_编号1.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号2
            // 
            this.xrLabel_编号2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号2.LocationFloat = new DevExpress.Utils.PointFloat(909.0157F, 63.70836F);
            this.xrLabel_编号2.Name = "xrLabel_编号2";
            this.xrLabel_编号2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号2.SizeF = new System.Drawing.SizeF(14.99994F, 15F);
            this.xrLabel_编号2.StylePriority.UseBorders = false;
            this.xrLabel_编号2.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号4
            // 
            this.xrLabel_编号4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号4.LocationFloat = new DevExpress.Utils.PointFloat(980.0313F, 63.70836F);
            this.xrLabel_编号4.Name = "xrLabel_编号4";
            this.xrLabel_编号4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号4.SizeF = new System.Drawing.SizeF(15.00006F, 15F);
            this.xrLabel_编号4.StylePriority.UseBorders = false;
            this.xrLabel_编号4.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号3
            // 
            this.xrLabel_编号3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号3.LocationFloat = new DevExpress.Utils.PointFloat(932.6876F, 63.70836F);
            this.xrLabel_编号3.Name = "xrLabel_编号3";
            this.xrLabel_编号3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号3.SizeF = new System.Drawing.SizeF(14.99994F, 15F);
            this.xrLabel_编号3.StylePriority.UseBorders = false;
            this.xrLabel_编号3.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号5
            // 
            this.xrLabel_编号5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号5.LocationFloat = new DevExpress.Utils.PointFloat(1003.703F, 63.70836F);
            this.xrLabel_编号5.Name = "xrLabel_编号5";
            this.xrLabel_编号5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号5.SizeF = new System.Drawing.SizeF(15.00006F, 15F);
            this.xrLabel_编号5.StylePriority.UseBorders = false;
            this.xrLabel_编号5.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号6
            // 
            this.xrLabel_编号6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号6.LocationFloat = new DevExpress.Utils.PointFloat(1027.375F, 63.70836F);
            this.xrLabel_编号6.Name = "xrLabel_编号6";
            this.xrLabel_编号6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号6.StylePriority.UseBorders = false;
            this.xrLabel_编号6.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号7
            // 
            this.xrLabel_编号7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号7.LocationFloat = new DevExpress.Utils.PointFloat(1051.047F, 63.70836F);
            this.xrLabel_编号7.Name = "xrLabel_编号7";
            this.xrLabel_编号7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号7.StylePriority.UseBorders = false;
            this.xrLabel_编号7.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号8
            // 
            this.xrLabel_编号8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号8.LocationFloat = new DevExpress.Utils.PointFloat(1074.719F, 63.70836F);
            this.xrLabel_编号8.Name = "xrLabel_编号8";
            this.xrLabel_编号8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号8.StylePriority.UseBorders = false;
            this.xrLabel_编号8.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(956.3594F, 63.70836F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(14.99994F, 15F);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(255.0102F, 22.91667F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(618.667F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "国家基本公共卫生服务项目老年人中医药健康管理服务记录表";
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 10F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 14F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report老年人中医药健康管理
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(21, 17, 10, 14);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号8;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_1_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_1_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_1_5;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_1_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_1_2;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_2_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_2_2;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_6_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_6_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_6_5;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_5_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_5_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_5_5;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_4_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_4_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_4_5;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_3_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_3_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_3_5;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_6_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_6_2;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_5_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_5_2;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_4_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_4_2;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_3_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_3_2;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_2_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_2_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_2_5;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_9_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_9_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_9_5;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_9_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_9_2;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_8_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_8_2;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_8_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_8_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_8_5;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_7_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_7_2;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_7_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_7_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_7_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_10_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_10_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_10_5;
        private DevExpress.XtraReports.UI.XRTable xrTable21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_10_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_10_2;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_11_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_11_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_11_5;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_11_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_11_2;
        private DevExpress.XtraReports.UI.XRTable xrTable25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_12_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_12_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_12_5;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_12_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_12_2;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_13_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_13_2;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_13_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_13_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_13_5;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_14_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_14_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_14_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_14_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_14_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_15_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_15_2;
        private DevExpress.XtraReports.UI.XRTable xrTable32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_15_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_15_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_15_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_16_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_16_2;
        private DevExpress.XtraReports.UI.XRTable xrTable34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRTable xrTable35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_16_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_16_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_16_5;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_17_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_17_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_17_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_17_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_17_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRTable xrTable40;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_19_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_19_2;
        private DevExpress.XtraReports.UI.XRTable xrTable39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_19_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_19_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_19_5;
        private DevExpress.XtraReports.UI.XRTable xrTable38;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_18_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_18_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_18_5;
        private DevExpress.XtraReports.UI.XRTable xrTable37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_18_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_18_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_9_3;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_9_4;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_9_5;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_9_1;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_9_2;
    }
}
