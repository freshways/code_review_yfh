﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    public partial class report老年人中医药健康管理 : DevExpress.XtraReports.UI.XtraReport
    {
        #region Fields
        DataSet _ds老年人;
        string docNo;
        bll老年人中医药特征管理 _bll中医药 = new bll老年人中医药特征管理();
        #endregion
        public report老年人中医药健康管理()
        {
            InitializeComponent();
        }

        public report老年人中医药健康管理(string docNo,string _date)
        {
            InitializeComponent();
            this.docNo = docNo;
            _ds老年人 = _bll中医药.GetInfoByOldZYY(docNo, _date, true);
            DoBindingDataSource(_ds老年人);
            //拆解个人档案编号后8位
            char[] char_编号 = docNo.Substring(docNo.Length - 8, 8).ToCharArray();
            for (int i = 0; i < char_编号.Length; i++)
            {
                string xrName = "xrLabel_编号" + (i + 1);
                XRLabel xrl = (XRLabel)FindControl(xrName, false);
                if (xrl != null)
                    xrl.Text = char_编号[i].ToString();
            }
        }

        private void DoBindingDataSource(DataSet _ds老年人)
        {
            DataTable dt老年人 = _ds老年人.Tables[Models.tb_老年人中医药特征管理.__TableName];
            if (dt老年人 == null || dt老年人.Rows.Count == 0) return;

            DataTable dt老年人基本信息 = _ds老年人.Tables[Models.tb_老年人基本信息.__TableName];
            if (dt老年人基本信息 == null || dt老年人基本信息.Rows.Count == 0) return;

            DataTable dt健康档案 = _ds老年人.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.txt姓名.Text = dt健康档案.Rows[0][tb_健康档案.姓名].ToString();

            ////xrTableCell_特征1
            //string 特征1 = dt老年人.Rows[0][Models.tb_老年人中医药特征管理.特征1].ToString();
            //string xrName = "xrTableCell_特征" + 特征1;
            //XRLabel xrl = (XRLabel)FindControl(xrName, false);
            //if (xrl != null)
            //    xrl.Text = 特征1;

            //特征问答
            //获取单挑数据
            DataRow dr老年人中医药 = dt老年人.Rows[0];
            if (dr老年人中医药 != null)
            {
                for (int i = 0; i < 19; i++)
                {
                    //每次循环按顺序获取一个特征值
                    string str_特征 = dr老年人中医药["特征" + (i + 1)].ToString();
                    string xrTable_特征 = "xrTable_特征_" + (i + 1) + "_" + str_特征;
                    XRLabel XRT_特征 = (XRLabel)FindControl(xrTable_特征, false);
                    if (XRT_特征 != null)
                        XRT_特征.Text = str_特征;
                }
            }
            else
            {
                
            }
       
        }
    }
}
