﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using AtomEHR.Models;
using AtomEHR.Business.Security;
using AtomEHR.Common;
using AtomEHR.公共卫生.Module.个人健康.老年人健康管理;
using DevExpress.XtraReports.UI;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    public partial class UC老年人生活自理能力评估表_显示 : UserControlBaseNavBar
    {
        #region Fields
        AtomEHR.Business.bll老年人随访 _Bll = new Business.bll老年人随访();
        DataSet _ds老年人随访;
        public frm个人健康 _frm = null;
        string _docNo;
        string _familyNo;
        string _id;
        DataRow _dr当前数据;
        #endregion
        public UC老年人生活自理能力评估表_显示()
        {
            InitializeComponent();
        }
        public UC老年人生活自理能力评估表_显示(Form parentForm)
        {
            InitializeComponent();
            _frm = (frm个人健康)parentForm;
            _docNo = _frm._docNo;
            _familyNo = _frm._familyDocNo;
            _id = _frm._param == null ? "" : _frm._param.ToString();
            //根据个人档案编号 查询全部的  体检数据
            _ds老年人随访 = _Bll.GetAllDataByKey(_docNo);
            DoBindingDataSource(_ds老年人随访);//绑定数据
            SetItemColorToRed(_id);
        }
        private void DoBindingDataSource(DataSet _ds老年人随访)
        {
            if (_ds老年人随访 == null) return;
            DataTable dt老年人随访 = _ds老年人随访.Tables[1];
            DataTable dt老年人基本信息 = _ds老年人随访.Tables[0];
            #region 先绑定老年人基本信息
            Bind基本信息(dt老年人基本信息);
            #endregion
            #region 创建页面左边的导航树
            //<<<<<<< .mine
            //            navBarControl1.Groups.Clear();
            //            string odlyaer = "";
            //            for (int i = 0; i < dt老年人随访.Rows.Count; i++)
            //            {
            //                string yaer = Convert.ToDateTime(dt老年人随访.Rows[i][tb_老年人随访.随访日期]).Year.ToString();
            //                if (yaer != odlyaer)
            //                {
            //                    CreateNavBarButton(navBarControl1, dt老年人随访, yaer, tb_老年人随访.随访日期);
            //                    odlyaer = yaer;
            //                }
            //            }
            //=======

            CreateNavBarButton_new(dt老年人随访, tb_老年人随访.随访日期);
            #endregion

            //如果date为空，则默认绑定第一条体检数据
            //否则  根据date进行查找对应的 体检数据 进行绑定
            DataRow row;
            if (string.IsNullOrEmpty(_id) && dt老年人随访.Rows.Count > 0)
            {
                row = dt老年人随访.Rows[0];
            }
            else
            {
                DataRow[] rows = dt老年人随访.Select("ID='" + _id + "'");
                if (rows.Length == 1)
                {
                    row = rows[0];
                }
                else
                {
                    return;
                }
            }
            DoBindingSummaryEditor(row);
        }
        private void Bind基本信息(DataTable dt老年人基本信息)
        {
            if (dt老年人基本信息 == null || dt老年人基本信息.Rows.Count == 0) return;
            DataRow dr = dt老年人基本信息.Rows[0];
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[tb_老年人基本信息.姓名].ToString());
            this.txt性别.Text = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_老年人基本信息.性别].ToString());
            this.txt个人档案号.Text = dr[tb_老年人基本信息.个人档案编号].ToString();
            this.txt身份证号.Text = dr[tb_老年人基本信息.身份证号].ToString();
            this.txt出生日期.Text = dr[tb_老年人基本信息.出生日期].ToString();
            this.txt联系电话.Text = dr[tb_老年人基本信息.联系电话].ToString();
            this.txt居住地址.Text = _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.省].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.市].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.区].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.街道].ToString()) + _bll地区.Get名称By地区ID(dr[tb_老年人基本信息.居委会].ToString()) + dr[tb_老年人基本信息.居住地址].ToString();
        }
        protected override void DoBindingSummaryEditor(object dataSource)
        {
            _dr当前数据 = (DataRow)dataSource;
            if (_dr当前数据 == null) return;
            if (!string.IsNullOrEmpty(_dr当前数据[tb_老年人随访.进餐评分].ToString()))
            {
                string 进餐评分 = _dr当前数据[tb_老年人随访.进餐评分].ToString();
                this.result1.Text = 进餐评分;
                switch (进餐评分)
                {
                    case "0":
                        this.chk11.Checked = true;
                        break;
                    case "3":
                        this.chk13.Checked = true;
                        break;
                    case "5":
                        this.chk14.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(_dr当前数据[tb_老年人随访.梳洗评分].ToString()))
            {
                string 梳洗评分 = _dr当前数据[tb_老年人随访.梳洗评分].ToString();
                this.result2.Text = 梳洗评分;
                switch (梳洗评分)
                {
                    case "0":
                        this.chk21.Checked = true;
                        break;
                    case "1":
                        this.chk22.Checked = true;
                        break;
                    case "3":
                        this.chk23.Checked = true;
                        break;
                    case "7":
                        this.chk24.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(_dr当前数据[tb_老年人随访.穿衣评分].ToString()))
            {
                string 穿衣评分 = _dr当前数据[tb_老年人随访.穿衣评分].ToString();
                this.result3.Text = 穿衣评分;
                switch (穿衣评分)
                {
                    case "0":
                        this.chk31.Checked = true;
                        break;
                    case "3":
                        this.chk33.Checked = true;
                        break;
                    case "7":
                        this.chk34.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(_dr当前数据[tb_老年人随访.如厕评分].ToString()))
            {
                string 如厕评分 = _dr当前数据[tb_老年人随访.如厕评分].ToString();
                this.result4.Text = 如厕评分;
                switch (如厕评分)
                {
                    case "0":
                        this.chk41.Checked = true;
                        break;
                    case "1":
                        this.chk42.Checked = true;
                        break;
                    case "5":
                        this.chk43.Checked = true;
                        break;
                    case "10":
                        this.chk44.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(_dr当前数据[tb_老年人随访.活动评分].ToString()))
            {
                string 活动评分 = _dr当前数据[tb_老年人随访.活动评分].ToString();
                this.result5.Text = 活动评分;
                switch (活动评分)
                {
                    case "0":
                        this.chk51.Checked = true;
                        break;
                    case "1":
                        this.chk52.Checked = true;
                        break;
                    case "5":
                        this.chk53.Checked = true;
                        break;
                    case "10":
                        this.chk54.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            this.lbl缺项.Text = string.Format("考核项：7    缺项：{0}     完整度：{1} %", _dr当前数据[tb_老年人随访.缺项].ToString(), _dr当前数据[tb_老年人随访.完整度].ToString());
            this.result.Text = string.IsNullOrEmpty(_dr当前数据[tb_老年人随访.总评分].ToString()) ? " " : _dr当前数据[tb_老年人随访.总评分].ToString();

            this.txt下次随访目标.Text = _dr当前数据[tb_老年人随访.下次随访目标].ToString();//(非考核项)
            this.dte随访日期.Text = _dr当前数据[tb_老年人随访.随访日期].ToString();
            this.txt随访医生签名.Text = _dr当前数据[tb_老年人随访.随访医生].ToString();
            this.dte下次随访日期.Text = _dr当前数据[tb_老年人随访.下次随访日期].ToString();

            this.txt创建人.Text = _Bll.Return用户名称(_dr当前数据[tb_老年人随访.创建人].ToString());
            this.txt创建时间.Text = _dr当前数据[tb_老年人随访.创建时间].ToString();
            this.txt最近更新时间.Text = _dr当前数据[tb_老年人随访.更新时间].ToString();
            this.txt当前所属机构.Text = _Bll.Return机构名称(_dr当前数据[tb_老年人随访.所属机构].ToString());
            this.txt创建机构.Text = _Bll.Return机构名称(_dr当前数据[tb_老年人随访.创建机构].ToString());// Loginer.CurrentUser.所属机构名称;
            this.txt最近修改人.Text = _Bll.Return用户名称(_dr当前数据[tb_老年人随访.更新人].ToString());
            this.txt录入医生.Text = _Bll.Return用户名称(_dr当前数据[tb_老年人随访.创建人].ToString());

        }
        private void UC老年人生活自理能力评估表_Load(object sender, EventArgs e)
        {
            int isallow = DataDictCache.Cache.IsAllow延时加载数据();
            if (isallow > 0 && !Loginer.CurrentUser.IsSubAdmin())
            {
                this.lbl最近更新时间.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.lbl最近修改人.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }
        private void result_TextChanged(object sender, EventArgs e)
        {
            int result1 = string.IsNullOrEmpty(this.result1.Text.Trim()) ? 0 : Convert.ToInt32(this.result1.Text.Trim());
            int result2 = string.IsNullOrEmpty(this.result2.Text.Trim()) ? 0 : Convert.ToInt32(this.result2.Text.Trim());
            int result3 = string.IsNullOrEmpty(this.layout3.Text.Trim()) ? 0 : Convert.ToInt32(this.layout3.Text.Trim());
            int result4 = string.IsNullOrEmpty(this.layout4.Text.Trim()) ? 0 : Convert.ToInt32(this.layout4.Text.Trim());
            int result5 = string.IsNullOrEmpty(this.layout5.Text.Trim()) ? 0 : Convert.ToInt32(this.layout5.Text.Trim());

            this.lyoutzong.Text = Convert.ToString(result1 + result2 + result3 + result4 + result5);
        }
        private void btn添加随访_Click(object sender, EventArgs e)
        {
            UC老年人生活自理能力评估表 uc = new UC老年人生活自理能力评估表(_frm, AtomEHR.Common.UpdateType.Add);
            ShowControl(uc);
        }
        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_老年人随访.所属机构].ToString()))
            {
                _id = _dr当前数据["ID"].ToString();
                _frm._param = _id;
                UC老年人生活自理能力评估表 uc = new UC老年人生活自理能力评估表(_frm, AtomEHR.Common.UpdateType.Modify);
                ShowControl(uc, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }
        private void btn删除_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_老年人随访.所属机构].ToString()))
            {
                if (Msg.AskQuestion("确定要删除数据么？"))
                {
                    //TODO: 删除
                    _id = _dr当前数据["ID"].ToString();
                    string happenYear = this.dte随访日期.Text.Substring(0, 4);
                    //TODO:删除 数据
                    SaveResult result = _Bll.fun删除随访(_id, _docNo, happenYear);
                    if (result.Success)
                    {
                        Msg.ShowInformation("删除数据成功！");
                        _id = "";
                        _ds老年人随访 = _Bll.GetAllDataByKey(_docNo);
                        DoBindingDataSource(_ds老年人随访);//绑定数据
                        this.layoutControl1.VerticalScroll.Value = 0;
                    }
                }
                else
                {
                    Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
                }
            }
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            string docNo = this.txt个人档案号.Text.Trim();
            string date = this.dte随访日期.Text.Trim();
            if (!string.IsNullOrEmpty(docNo))
            {
                report老年人生活自理能力评估表 report = new report老年人生活自理能力评估表(docNo,date);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }
        }

    }
}
