﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Business.Security;

namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    public partial class UC老年人生活自理能力评估表 : UserControlBase
    {

        #region Fields
        AtomEHR.Business.bll老年人随访 _Bll = new Business.bll老年人随访();
        DataSet _ds老年人随访;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        #endregion

        public UC老年人生活自理能力评估表()
        {
            InitializeComponent();
        }
        public UC老年人生活自理能力评估表(Form frm, UpdateType _updateType)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _serverDateTime = _Bll.ServiceDateTime;
            _UpdateType = _updateType;
            _id = _frm._param as string;
            if (_UpdateType == UpdateType.Add)//添加随访
            {
                _Bll.GetBusinessByKey(_docNo, true);
                //_Bll.NewBusiness();
                _ds老年人随访 = _Bll.CurrentBusiness;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                _ds老年人随访 = _Bll.GetOneDataByKey(_docNo, _id, true);
                this.dte随访日期.Properties.ReadOnly = true;
                this.dte下次随访日期.Properties.ReadOnly = true;
            }
            DoBindingSummaryEditor(_ds老年人随访);//绑定数据
        }
        private void DoBindingSummaryEditor(DataSet _ds老年人随访)
        {
            if (_ds老年人随访 == null) return;
            if (_ds老年人随访.Tables.Count == 0) return;
            DataTable dt老年人信息 = _ds老年人随访.Tables[tb_老年人基本信息.__TableName];
            DataTable dt随访 = _ds老年人随访.Tables[tb_老年人随访.__TableName];
            if (dt老年人信息.Rows.Count == 1)
            {
                DataRow dr = dt老年人信息.Rows[0];
                this.txt姓名.Text = util.DESEncrypt.DES解密(dr[tb_老年人基本信息.姓名].ToString());
                this.txt性别.Text = _BLL.ReturnDis字典显示("xb_xingbie", dr[tb_老年人基本信息.性别].ToString());
                this.txt个人档案号.Text = dr[tb_老年人基本信息.个人档案编号].ToString();
                this.txt身份证号.Text = dr[tb_老年人基本信息.身份证号].ToString();
                this.txt出生日期.Text = dr[tb_老年人基本信息.出生日期].ToString();
                this.txt联系电话.Text = dr[tb_老年人基本信息.联系电话].ToString();
                this.txt居住地址.Text = _Bll.Return地区名称(dr[tb_老年人基本信息.省].ToString()) + _Bll.Return地区名称(dr[tb_老年人基本信息.市].ToString()) + _Bll.Return地区名称(dr[tb_老年人基本信息.区].ToString()) + _Bll.Return地区名称(dr[tb_老年人基本信息.街道].ToString()) + _Bll.Return地区名称(dr[tb_老年人基本信息.居委会].ToString()) + dr[tb_老年人基本信息.居住地址].ToString();

            }
            if (dt随访.Rows.Count == 1)
            {
                BindSuiFangData(dt随访.Rows[0]);
            }
            if (_UpdateType == UpdateType.Add)
            {
                this.txt创建人.Text = Loginer.CurrentUser.AccountName;
                this.txt创建时间.Text = _serverDateTime;
                this.txt最近更新时间.Text = _serverDateTime;
                this.txt当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
                this.txt创建机构.Text = Loginer.CurrentUser.所属机构名称;
                this.txt最近修改人.Text = Loginer.CurrentUser.AccountName;
                this.txt录入医生.Text = Loginer.CurrentUser.AccountName;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                this.txt创建人.Text = _Bll.Return用户名称(dt随访.Rows[0][tb_老年人随访.创建人].ToString());
                this.txt创建时间.Text = dt随访.Rows[0][tb_老年人随访.创建时间].ToString();
                this.txt最近更新时间.Text = _serverDateTime;
                this.txt当前所属机构.Text = _Bll.Return机构名称(dt随访.Rows[0][tb_老年人随访.所属机构].ToString());
                this.txt创建机构.Text = _Bll.Return机构名称(dt随访.Rows[0][tb_老年人随访.创建机构].ToString());// Loginer.CurrentUser.所属机构名称;
                this.txt最近修改人.Text = Loginer.CurrentUser.AccountName; //Loginer.CurrentUser.AccountName;
                this.txt录入医生.Text = dt随访.Rows[0][tb_老年人随访.随访医生].ToString();
            }
        }
        private void BindSuiFangData(DataRow dataRow)
        {
            if (dataRow == null) return;
            if (!string.IsNullOrEmpty(dataRow[tb_老年人随访.进餐评分].ToString()))
            {
                string 进餐评分 = dataRow[tb_老年人随访.进餐评分].ToString();
                this.result1.Text = 进餐评分;
                switch (进餐评分)
                {
                    case "0":
                        this.chk11.Checked = true;
                        break;
                    case "3":
                        this.chk13.Checked = true;
                        break;
                    case "5":
                        this.chk14.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人随访.梳洗评分].ToString()))
            {
                string 梳洗评分 = dataRow[tb_老年人随访.梳洗评分].ToString();
                this.result2.Text = 梳洗评分;
                switch (梳洗评分)
                {
                    case "0":
                        this.chk21.Checked = true;
                        break;
                    case "1":
                        this.chk22.Checked = true;
                        break;
                    case "3":
                        this.chk23.Checked = true;
                        break;
                    case "7":
                        this.chk24.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人随访.穿衣评分].ToString()))
            {
                string 穿衣评分 = dataRow[tb_老年人随访.穿衣评分].ToString();
                this.result3.Text = 穿衣评分;
                switch (穿衣评分)
                {
                    case "0":
                        this.chk31.Checked = true;
                        break;
                    case "3":
                        this.chk33.Checked = true;
                        break;
                    case "7":
                        this.chk34.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人随访.如厕评分].ToString()))
            {
                string 如厕评分 = dataRow[tb_老年人随访.如厕评分].ToString();
                this.result4.Text = 如厕评分;
                switch (如厕评分)
                {
                    case "0":
                        this.chk41.Checked = true;
                        break;
                    case "1":
                        this.chk42.Checked = true;
                        break;
                    case "5":
                        this.chk43.Checked = true;
                        break;
                    case "10":
                        this.chk44.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            if (!string.IsNullOrEmpty(dataRow[tb_老年人随访.活动评分].ToString()))
            {
                string 活动评分 = dataRow[tb_老年人随访.活动评分].ToString();
                this.result5.Text = 活动评分;
                switch (活动评分)
                {
                    case "0":
                        this.chk51.Checked = true;
                        break;
                    case "1":
                        this.chk52.Checked = true;
                        break;
                    case "5":
                        this.chk53.Checked = true;
                        break;
                    case "10":
                        this.chk54.Checked = true;
                        break;
                    default:
                        break;
                }
            }

            this.result.Text = string.IsNullOrEmpty(dataRow[tb_老年人随访.总评分].ToString()) ? " " : dataRow[tb_老年人随访.总评分].ToString();

            this.txt下次随访目标.Text = dataRow[tb_老年人随访.下次随访目标].ToString();
            this.dte随访日期.Text = dataRow[tb_老年人随访.随访日期].ToString();
            this.txt随访医生签名.Text = dataRow[tb_老年人随访.随访医生].ToString();
            this.dte下次随访日期.Text = dataRow[tb_老年人随访.下次随访日期].ToString();


        }
        private void UC老年人生活自理能力评估表_Load(object sender, EventArgs e)
        {
            #region 设置默认值
            this.chk11.Checked = true;
            this.chk21.Checked = true;
            this.chk31.Checked = true;
            this.chk41.Checked = true;
            this.chk51.Checked = true;
            this.result1.Text = "0";
            this.result2.Text = "0";
            this.result3.Text = "0";
            this.result4.Text = "0";
            this.result5.Text = "0";
            #endregion
        }
        private void result_TextChanged(object sender, EventArgs e)
        {
            int result1 = string.IsNullOrEmpty(this.result1.Text.Trim()) ? 0 : Convert.ToInt32(this.result1.Text.Trim());
            int result2 = string.IsNullOrEmpty(this.result2.Text.Trim()) ? 0 : Convert.ToInt32(this.result2.Text.Trim());
            int result3 = string.IsNullOrEmpty(this.result3.Text.Trim()) ? 0 : Convert.ToInt32(this.result3.Text.Trim());
            int result4 = string.IsNullOrEmpty(this.result4.Text.Trim()) ? 0 : Convert.ToInt32(this.result4.Text.Trim());
            int result5 = string.IsNullOrEmpty(this.result5.Text.Trim()) ? 0 : Convert.ToInt32(this.result5.Text.Trim());

            this.result.Text = Convert.ToString(result1 + result2 + result3 + result4 + result5);
        }
        private void btn保存_Click(object sender, EventArgs e)
        {
            if (check验证())
            {
                if (_UpdateType == UpdateType.Add)//添加
                {
                    if (Msg.AskQuestion("信息保存后，‘随访日期’与‘下次随访日期’将不允许修改，确认保存信息？"))
                    {
                        string happentime = this.dte随访日期.Text.Trim();
                        //判断此随访时间 是否已经添加  随访
                        if (!_Bll.isExists(_docNo, happentime))
                        {
                            //获取随访次数
                            int sfcs = _Bll.getbndsfcs(_docNo, happentime.Substring(0, 4));
                            if (sfcs == 0) sfcs = 1;
                            else sfcs += 1;
                            #region tb_老年人基本信息
                            _ds老年人随访.Tables[tb_老年人基本信息.__TableName].Rows[0][tb_老年人基本信息.下次随访时间] = this.dte下次随访日期.Text;
                            #endregion
                            #region tb_老年人随访
                            int i = Get缺项();
                            int j = Get完整度(i);
                            DataRow row = _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows.Add();
                            row[tb_老年人随访.进餐评分] = this.result1.Text.Trim();
                            row[tb_老年人随访.梳洗评分] = this.result2.Text.Trim();
                            row[tb_老年人随访.穿衣评分] = this.result3.Text.Trim();
                            row[tb_老年人随访.如厕评分] = this.result4.Text.Trim();
                            row[tb_老年人随访.活动评分] = this.result5.Text.Trim();
                            row[tb_老年人随访.总评分] = this.result.Text.Trim();
                            row[tb_老年人随访.下次随访目标] = this.txt下次随访目标.Text.Trim();
                            row[tb_老年人随访.随访日期] = this.dte随访日期.Text;
                            row[tb_老年人随访.随访医生] = this.txt随访医生签名.Text.Trim();
                            //根据用户要求，下次随访日期默认“随访日期”的一年以后
                            string str随访日期 = this.dte随访日期.Text;
                            this.dte下次随访日期.Text = Convert.ToDateTime(str随访日期).AddYears(1).ToShortDateString();
                            row[tb_老年人随访.下次随访日期] = this.dte下次随访日期.Text.Trim();
                            row[tb_老年人随访.随访次数] = sfcs;
                            row[tb_老年人随访.缺项] = i;
                            row[tb_老年人随访.完整度] = j;
                            row[tb_老年人随访.个人档案编号] = _docNo;
                            row[tb_老年人随访.创建时间] = this.txt创建时间.Text;
                            row[tb_老年人随访.更新时间] = this.txt最近更新时间.Text.Trim();
                            row[tb_老年人随访.所属机构] = Loginer.CurrentUser.所属机构;
                            row[tb_老年人随访.创建机构] = Loginer.CurrentUser.所属机构;
                            row[tb_老年人随访.创建人] = Loginer.CurrentUser.用户编码;
                            row[tb_老年人随访.更新人] = Loginer.CurrentUser.用户编码;
                            #endregion
                            #region tb_健康档案_个人健康特征
                            _ds老年人随访.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.老年人随访] = i + "," + j;
                            #endregion
                        }
                        else
                        {
                            Msg.Warning("对不起，随访时间为“" + this.dte随访日期.Text.Trim() + "”的随访已经创建，请修改随访时间！");
                            return;
                        }

                    }
                }
                else //修改
                {
                    #region tb_老年人随访
                    int i = Get缺项();
                    int j = Get完整度(i);
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.进餐评分] = this.result1.Text.Trim();
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.梳洗评分] = this.result2.Text.Trim();
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.穿衣评分] = this.result3.Text.Trim();
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.如厕评分] = this.result4.Text.Trim();
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.活动评分] = this.result5.Text.Trim();
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.总评分] = this.result.Text.Trim();
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.下次随访目标] = this.txt下次随访目标.Text.Trim();
                    //_ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.随访日期] = this.dte随访日期.Text;
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.随访医生] = this.txt随访医生签名.Text.Trim();
                    //_ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.下次随访日期] = this.dte下次随访日期.Text.Trim();
                    //_ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.随访次数] = sfcs;
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.缺项] = i;
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.完整度] = j;
                    //_ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.个人档案编号] = _docNo;
                    //_ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.创建时间] = this.txt创建时间.Text;
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.更新时间] = this.txt最近更新时间.Text.Trim();
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.所属机构] = Loginer.CurrentUser.所属机构;
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.创建机构] = Loginer.CurrentUser.所属机构;
                    _ds老年人随访.Tables[tb_老年人随访.__TableName].Rows[0][tb_老年人随访.更新人] = Loginer.CurrentUser.用户编码;
                    #endregion
                    #region tb_健康档案_个人健康特征
                    _ds老年人随访.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.老年人随访] = i + "," + j;
                    #endregion
                }
                _Bll.WriteLog();//TODO: 日志
                SaveResult result = _Bll.Save(_ds老年人随访);
                if (result.Success)//保存成功
                {
                    Msg.ShowInformation("保存成功！");
                    if (_UpdateType == UpdateType.Modify)//修改后的
                    {
                        UC老年人生活自理能力评估表_显示 uc = new UC老年人生活自理能力评估表_显示(_frm);
                        ShowControl(uc, DockStyle.Fill);
                    }
                    else
                    {
                        _frm._param = "";
                        UC老年人生活自理能力评估表_显示 uc = new UC老年人生活自理能力评估表_显示(_frm);
                        ShowControl(uc, DockStyle.Fill);
                    }
                }
            }
        }
        private int Get缺项()
        {
            int i = 0;
            if (this.result1.Text.Trim() == "") i++;
            if (this.result2.Text.Trim() == "") i++;
            if (this.result3.Text.Trim() == "") i++;
            if (this.result4.Text.Trim() == "") i++;
            if (this.result5.Text.Trim() == "") i++;
            //if (this.txt下次随访目标.Text.Trim() == "") i++;(非考核项)
            if (this.dte随访日期.Text.Trim() == "") i++;
            //if (this.dte下次随访日期.Text.Trim() == "") i++;(非考核项)
            if (this.txt随访医生签名.Text.Trim() == "") i++;
            return i;
        }
        public int Get完整度(int i)
        {
            int k = Convert.ToInt32((7 - i) * 100 / 7.0);
            return k;
        }
        private bool check验证()
        {
            if (string.IsNullOrEmpty(this.dte随访日期.Text.Trim()))
            {
                Msg.Warning("随访日期是必填项！");
                this.dte随访日期.Focus();
                return false;
            }
            //if (string.IsNullOrEmpty(this.dte下次随访日期.Text.Trim()))
            //{
            //    Msg.Warning("下次随访日期是必填项！");
            //    this.dte下次随访日期.Focus();
            //    return false;
            //}
            //if (this.dte下次随访日期.DateTime <= this.dte随访日期.DateTime)
            //{
            //    Msg.Warning("下次随访日期填写不能小于本次随访日期！");
            //    this.dte下次随访日期.Focus();
            //    return false;
            //}
            if (Convert.ToDateTime(this.txt创建时间.Text) < this.dte随访日期.DateTime)
            {
                Msg.Warning("本次随访日期不能大于创建日期！");
                this.dte随访日期.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(result1.Text.Trim()) || string.IsNullOrEmpty(result2.Text.Trim()) || string.IsNullOrEmpty(result3.Text.Trim()) || string.IsNullOrEmpty(result4.Text.Trim()) || string.IsNullOrEmpty(result5.Text.Trim()))
            {
                Msg.Warning("请将五个评估项全部选择！");
                return false;
            }
            return true;
        }
        private void btn重置_Click(object sender, EventArgs e)
        {

        }
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit chk = (CheckEdit)sender;
            string name = chk.Name;//chk11
            string result = "result";//result
            if (chk.Checked)
            {
                int value = Convert.ToInt32(chk.Tag);
                result += name.Substring(3, 1);
                Control ctrl = this.Controls.Find(result, true)[0];//根据name找到控件
                if (ctrl is LabelControl)
                {
                    ctrl.Text = value.ToString();
                }
            }
        }
    }
}
