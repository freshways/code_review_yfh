﻿namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    partial class report体质辨识结果
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(report体质辨识结果));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel医院名称 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel个人档案编号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl报告日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.报告日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel平和质是否 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel平和质分数 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel阳虚质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel阳虚质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel阴虚质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel阴虚质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel痰湿质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel痰湿质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel湿热质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel湿热质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血瘀质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血瘀质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel气郁质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel气郁质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel特禀质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel特禀质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel气虚质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel气虚质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrRichText1 = new DevExpress.XtraReports.UI.XRRichText();
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrRichText1,
            this.xrLine2,
            this.xrLabel24,
            this.xrLabel气郁质,
            this.xrLabel气郁质得分,
            this.xrLabel特禀质得分,
            this.xrLabel特禀质,
            this.xrLabel29,
            this.xrLabel气虚质得分,
            this.xrLabel气虚质,
            this.xrLabel32,
            this.xrLabel15,
            this.xrLabel痰湿质,
            this.xrLabel痰湿质得分,
            this.xrLabel湿热质得分,
            this.xrLabel湿热质,
            this.xrLabel20,
            this.xrLabel血瘀质得分,
            this.xrLabel血瘀质,
            this.xrLabel23,
            this.xrLabel12,
            this.xrLabel阴虚质,
            this.xrLabel阴虚质得分,
            this.xrLabel9,
            this.xrLabel阳虚质,
            this.xrLabel阳虚质得分,
            this.xrLabel平和质分数,
            this.xrLabel平和质是否,
            this.xrLabel8,
            this.xrLabel4,
            this.xrLabel6,
            this.xrLabel7,
            this.xrLabel性别,
            this.xrLabel5,
            this.xrLabel姓名,
            this.xrLabel2,
            this.报告日期,
            this.lbl报告日期,
            this.xrLabel3,
            this.xrLabel个人档案编号,
            this.xrLine1,
            this.xrLabel1,
            this.xrPictureBox1,
            this.xrLabel医院名称});
            this.Detail.HeightF = 557.2917F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(196.8749F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(68.75001F, 56.04166F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // xrLabel医院名称
            // 
            this.xrLabel医院名称.Font = new System.Drawing.Font("隶书", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel医院名称.LocationFloat = new DevExpress.Utils.PointFloat(265.6249F, 13.95833F);
            this.xrLabel医院名称.Name = "xrLabel医院名称";
            this.xrLabel医院名称.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel医院名称.SizeF = new System.Drawing.SizeF(317.7083F, 28.54168F);
            this.xrLabel医院名称.StylePriority.UseFont = false;
            this.xrLabel医院名称.StylePriority.UseTextAlignment = false;
            this.xrLabel医院名称.Text = "姚店子中心医院";
            this.xrLabel医院名称.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("华文宋体", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 56.04167F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(724F, 45.83335F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "中 医 体 质 辨 识 报 告 单";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 123F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(725.0417F, 2.166668F);
            // 
            // xrLabel个人档案编号
            // 
            this.xrLabel个人档案编号.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel个人档案编号.LocationFloat = new DevExpress.Utils.PointFloat(38.54167F, 100F);
            this.xrLabel个人档案编号.Name = "xrLabel个人档案编号";
            this.xrLabel个人档案编号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel个人档案编号.SizeF = new System.Drawing.SizeF(64.58333F, 23F);
            this.xrLabel个人档案编号.StylePriority.UseFont = false;
            this.xrLabel个人档案编号.StylePriority.UseTextAlignment = false;
            this.xrLabel个人档案编号.Text = "编号：";
            this.xrLabel个人档案编号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(103.125F, 100F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(200F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "371327199001154119";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl报告日期
            // 
            this.lbl报告日期.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl报告日期.LocationFloat = new DevExpress.Utils.PointFloat(502.125F, 100F);
            this.lbl报告日期.Name = "lbl报告日期";
            this.lbl报告日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.lbl报告日期.SizeF = new System.Drawing.SizeF(104.1665F, 23F);
            this.lbl报告日期.StylePriority.UseFont = false;
            this.lbl报告日期.StylePriority.UseTextAlignment = false;
            this.lbl报告日期.Text = "报告日期：";
            this.lbl报告日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // 报告日期
            // 
            this.报告日期.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.报告日期.LocationFloat = new DevExpress.Utils.PointFloat(606.2916F, 100F);
            this.报告日期.Name = "报告日期";
            this.报告日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.报告日期.SizeF = new System.Drawing.SizeF(117.7084F, 23F);
            this.报告日期.StylePriority.UseFont = false;
            this.报告日期.StylePriority.UseTextAlignment = false;
            this.报告日期.Text = "2015-09-09";
            this.报告日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(38.54167F, 132.2917F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(64.58333F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓名：";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(103.125F, 132.2917F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel姓名.StylePriority.UseFont = false;
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.Text = "王茂国";
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel性别
            // 
            this.xrLabel性别.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel性别.LocationFloat = new DevExpress.Utils.PointFloat(315.125F, 132.2917F);
            this.xrLabel性别.Name = "xrLabel性别";
            this.xrLabel性别.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel性别.SizeF = new System.Drawing.SizeF(39.58331F, 23F);
            this.xrLabel性别.StylePriority.UseFont = false;
            this.xrLabel性别.StylePriority.UseTextAlignment = false;
            this.xrLabel性别.Text = "男";
            this.xrLabel性别.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(250.5417F, 132.2917F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(64.58333F, 23F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "性别：";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(526.5834F, 132.2917F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "76";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(462F, 132.2917F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(64.58333F, 23F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "年龄：";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(38.54167F, 155.2917F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(106.25F, 23F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "体质类型：";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(196.8749F, 155.2917F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(67.70834F, 23F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "平和质";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel平和质是否
            // 
            this.xrLabel平和质是否.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel平和质是否.LocationFloat = new DevExpress.Utils.PointFloat(264.5833F, 155.2917F);
            this.xrLabel平和质是否.Name = "xrLabel平和质是否";
            this.xrLabel平和质是否.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel平和质是否.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel平和质是否.StylePriority.UseFont = false;
            this.xrLabel平和质是否.StylePriority.UseTextAlignment = false;
            this.xrLabel平和质是否.Text = "是";
            this.xrLabel平和质是否.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel平和质分数
            // 
            this.xrLabel平和质分数.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel平和质分数.LocationFloat = new DevExpress.Utils.PointFloat(315.125F, 155.2917F);
            this.xrLabel平和质分数.Name = "xrLabel平和质分数";
            this.xrLabel平和质分数.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel平和质分数.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel平和质分数.StylePriority.UseFont = false;
            this.xrLabel平和质分数.StylePriority.UseTextAlignment = false;
            this.xrLabel平和质分数.Text = "22";
            this.xrLabel平和质分数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(374.2291F, 155.2917F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(67.70834F, 23F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "阳虚质";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel阳虚质
            // 
            this.xrLabel阳虚质.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel阳虚质.LocationFloat = new DevExpress.Utils.PointFloat(441.9374F, 155.2917F);
            this.xrLabel阳虚质.Name = "xrLabel阳虚质";
            this.xrLabel阳虚质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阳虚质.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel阳虚质.StylePriority.UseFont = false;
            this.xrLabel阳虚质.StylePriority.UseTextAlignment = false;
            this.xrLabel阳虚质.Text = "是";
            this.xrLabel阳虚质.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel阳虚质得分
            // 
            this.xrLabel阳虚质得分.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel阳虚质得分.LocationFloat = new DevExpress.Utils.PointFloat(492.4792F, 155.2917F);
            this.xrLabel阳虚质得分.Name = "xrLabel阳虚质得分";
            this.xrLabel阳虚质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阳虚质得分.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel阳虚质得分.StylePriority.UseFont = false;
            this.xrLabel阳虚质得分.StylePriority.UseTextAlignment = false;
            this.xrLabel阳虚质得分.Text = "22";
            this.xrLabel阳虚质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(558.8749F, 155.2917F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(67.70834F, 23F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "阴虚质";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel阴虚质
            // 
            this.xrLabel阴虚质.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel阴虚质.LocationFloat = new DevExpress.Utils.PointFloat(626.5833F, 155.2917F);
            this.xrLabel阴虚质.Name = "xrLabel阴虚质";
            this.xrLabel阴虚质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阴虚质.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel阴虚质.StylePriority.UseFont = false;
            this.xrLabel阴虚质.StylePriority.UseTextAlignment = false;
            this.xrLabel阴虚质.Text = "是";
            this.xrLabel阴虚质.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel阴虚质得分
            // 
            this.xrLabel阴虚质得分.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel阴虚质得分.LocationFloat = new DevExpress.Utils.PointFloat(677.125F, 155.2917F);
            this.xrLabel阴虚质得分.Name = "xrLabel阴虚质得分";
            this.xrLabel阴虚质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阴虚质得分.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel阴虚质得分.StylePriority.UseFont = false;
            this.xrLabel阴虚质得分.StylePriority.UseTextAlignment = false;
            this.xrLabel阴虚质得分.Text = "22";
            this.xrLabel阴虚质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(196.8749F, 178.2917F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(67.70834F, 23F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "痰湿质";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel痰湿质
            // 
            this.xrLabel痰湿质.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel痰湿质.LocationFloat = new DevExpress.Utils.PointFloat(264.5833F, 178.2917F);
            this.xrLabel痰湿质.Name = "xrLabel痰湿质";
            this.xrLabel痰湿质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel痰湿质.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel痰湿质.StylePriority.UseFont = false;
            this.xrLabel痰湿质.StylePriority.UseTextAlignment = false;
            this.xrLabel痰湿质.Text = "是";
            this.xrLabel痰湿质.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel痰湿质得分
            // 
            this.xrLabel痰湿质得分.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel痰湿质得分.LocationFloat = new DevExpress.Utils.PointFloat(315.125F, 178.2917F);
            this.xrLabel痰湿质得分.Name = "xrLabel痰湿质得分";
            this.xrLabel痰湿质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel痰湿质得分.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel痰湿质得分.StylePriority.UseFont = false;
            this.xrLabel痰湿质得分.StylePriority.UseTextAlignment = false;
            this.xrLabel痰湿质得分.Text = "22";
            this.xrLabel痰湿质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel湿热质得分
            // 
            this.xrLabel湿热质得分.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel湿热质得分.LocationFloat = new DevExpress.Utils.PointFloat(492.4791F, 178.2917F);
            this.xrLabel湿热质得分.Name = "xrLabel湿热质得分";
            this.xrLabel湿热质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel湿热质得分.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel湿热质得分.StylePriority.UseFont = false;
            this.xrLabel湿热质得分.StylePriority.UseTextAlignment = false;
            this.xrLabel湿热质得分.Text = "22";
            this.xrLabel湿热质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel湿热质
            // 
            this.xrLabel湿热质.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel湿热质.LocationFloat = new DevExpress.Utils.PointFloat(441.9374F, 178.2917F);
            this.xrLabel湿热质.Name = "xrLabel湿热质";
            this.xrLabel湿热质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel湿热质.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel湿热质.StylePriority.UseFont = false;
            this.xrLabel湿热质.StylePriority.UseTextAlignment = false;
            this.xrLabel湿热质.Text = "是";
            this.xrLabel湿热质.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(374.2291F, 178.2917F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(67.70834F, 23F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "湿热质";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel血瘀质得分
            // 
            this.xrLabel血瘀质得分.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel血瘀质得分.LocationFloat = new DevExpress.Utils.PointFloat(677.1249F, 178.2917F);
            this.xrLabel血瘀质得分.Name = "xrLabel血瘀质得分";
            this.xrLabel血瘀质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血瘀质得分.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel血瘀质得分.StylePriority.UseFont = false;
            this.xrLabel血瘀质得分.StylePriority.UseTextAlignment = false;
            this.xrLabel血瘀质得分.Text = "22";
            this.xrLabel血瘀质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel血瘀质
            // 
            this.xrLabel血瘀质.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel血瘀质.LocationFloat = new DevExpress.Utils.PointFloat(626.5832F, 178.2917F);
            this.xrLabel血瘀质.Name = "xrLabel血瘀质";
            this.xrLabel血瘀质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血瘀质.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel血瘀质.StylePriority.UseFont = false;
            this.xrLabel血瘀质.StylePriority.UseTextAlignment = false;
            this.xrLabel血瘀质.Text = "是";
            this.xrLabel血瘀质.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(558.8748F, 178.2917F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(67.70834F, 23F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "血瘀质";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(196.8749F, 201.2917F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(67.70834F, 23F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "气郁质";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel气郁质
            // 
            this.xrLabel气郁质.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel气郁质.LocationFloat = new DevExpress.Utils.PointFloat(264.5833F, 201.2917F);
            this.xrLabel气郁质.Name = "xrLabel气郁质";
            this.xrLabel气郁质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel气郁质.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel气郁质.StylePriority.UseFont = false;
            this.xrLabel气郁质.StylePriority.UseTextAlignment = false;
            this.xrLabel气郁质.Text = "是";
            this.xrLabel气郁质.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel气郁质得分
            // 
            this.xrLabel气郁质得分.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel气郁质得分.LocationFloat = new DevExpress.Utils.PointFloat(315.1251F, 201.2917F);
            this.xrLabel气郁质得分.Name = "xrLabel气郁质得分";
            this.xrLabel气郁质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel气郁质得分.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel气郁质得分.StylePriority.UseFont = false;
            this.xrLabel气郁质得分.StylePriority.UseTextAlignment = false;
            this.xrLabel气郁质得分.Text = "22";
            this.xrLabel气郁质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel特禀质得分
            // 
            this.xrLabel特禀质得分.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel特禀质得分.LocationFloat = new DevExpress.Utils.PointFloat(492.4791F, 201.2917F);
            this.xrLabel特禀质得分.Name = "xrLabel特禀质得分";
            this.xrLabel特禀质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel特禀质得分.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel特禀质得分.StylePriority.UseFont = false;
            this.xrLabel特禀质得分.StylePriority.UseTextAlignment = false;
            this.xrLabel特禀质得分.Text = "22";
            this.xrLabel特禀质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel特禀质
            // 
            this.xrLabel特禀质.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel特禀质.LocationFloat = new DevExpress.Utils.PointFloat(441.9374F, 201.2917F);
            this.xrLabel特禀质.Name = "xrLabel特禀质";
            this.xrLabel特禀质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel特禀质.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel特禀质.StylePriority.UseFont = false;
            this.xrLabel特禀质.StylePriority.UseTextAlignment = false;
            this.xrLabel特禀质.Text = "是";
            this.xrLabel特禀质.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(374.2291F, 201.2917F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(67.70834F, 23F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "特禀质";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel气虚质得分
            // 
            this.xrLabel气虚质得分.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel气虚质得分.LocationFloat = new DevExpress.Utils.PointFloat(677.1249F, 201.2917F);
            this.xrLabel气虚质得分.Name = "xrLabel气虚质得分";
            this.xrLabel气虚质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel气虚质得分.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel气虚质得分.StylePriority.UseFont = false;
            this.xrLabel气虚质得分.StylePriority.UseTextAlignment = false;
            this.xrLabel气虚质得分.Text = "22";
            this.xrLabel气虚质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel气虚质
            // 
            this.xrLabel气虚质.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel气虚质.LocationFloat = new DevExpress.Utils.PointFloat(626.5833F, 201.2917F);
            this.xrLabel气虚质.Name = "xrLabel气虚质";
            this.xrLabel气虚质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel气虚质.SizeF = new System.Drawing.SizeF(28.125F, 23F);
            this.xrLabel气虚质.StylePriority.UseFont = false;
            this.xrLabel气虚质.StylePriority.UseTextAlignment = false;
            this.xrLabel气虚质.Text = "是";
            this.xrLabel气虚质.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("华文宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(558.8748F, 201.2917F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(67.70834F, 23F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "气虚质";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine2
            // 
            this.xrLine2.LineWidth = 2;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 224.2916F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(725.0417F, 2.166668F);
            // 
            // xrRichText1
            // 
            this.xrRichText1.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrRichText1.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 226.4583F);
            this.xrRichText1.Name = "xrRichText1";
            this.xrRichText1.SerializableRtfString = resources.GetString("xrRichText1.SerializableRtfString");
            this.xrRichText1.SizeF = new System.Drawing.SizeF(724F, 133.9584F);
            // 
            // report体质辨识结果
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(51, 52, 0, 100);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrRichText1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel医院名称;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel 报告日期;
        private DevExpress.XtraReports.UI.XRLabel lbl报告日期;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel个人档案编号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel平和质分数;
        private DevExpress.XtraReports.UI.XRLabel xrLabel平和质是否;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阴虚质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阴虚质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阳虚质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阳虚质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel气郁质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel气郁质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel特禀质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel特禀质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel气虚质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel气虚质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel痰湿质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel痰湿质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel湿热质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel湿热质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血瘀质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血瘀质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRRichText xrRichText1;
    }
}
