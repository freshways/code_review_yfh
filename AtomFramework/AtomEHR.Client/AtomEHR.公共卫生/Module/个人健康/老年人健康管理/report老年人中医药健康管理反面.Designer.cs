﻿namespace AtomEHR.公共卫生.Module.个人健康.老年人健康管理
{
    partial class report老年人中医药健康管理反面
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable39 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt医生签名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable38 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt填表日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_气郁质指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气郁质指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气郁质指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气郁质指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气郁质指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气郁质指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt气郁质指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt气郁质其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel118 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel119 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_特禀质指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_特禀质指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_特禀质指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_特禀质指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_特禀质指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt特禀质指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt特禀质其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel123 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel124 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel125 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel126 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel127 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel128 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_特禀质指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_平和质指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_平和质指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_平和质指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_平和质指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_平和质指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt平和质指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt平和质其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel131 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel132 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel133 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel134 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel135 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel136 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_平和质指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_痰湿质指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_痰湿质指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_痰湿质指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_痰湿质指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_痰湿质指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt痰湿质指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt痰湿质其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_痰湿质指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_湿热质指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_湿热质指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_湿热质指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_湿热质指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt湿热质指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt湿热质其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel99 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel100 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel101 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_湿热质指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_湿热质指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血瘀质指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_血瘀质指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_血瘀质指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_血瘀质指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_血瘀质指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血瘀质指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血瘀质其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel107 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel109 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel110 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel112 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_血瘀质指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_气虚质指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气虚质指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气虚质指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气虚质指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt气虚质指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt气虚质其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气虚质指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气虚质指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_阳虚质指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阳虚质指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阳虚质指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阳虚质指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阳虚质指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阳虚质指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阳虚质指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阳虚质其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_阴虚质指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阴虚质指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阴虚质指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阴虚质指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阴虚质指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阴虚质指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阴虚质其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel83 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阴虚质指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_气郁质辨识_倾向是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt气郁质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气郁质辨识_是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_特禀质辨识_倾向是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt特禀质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_特禀质辨识_是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt平和质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_平和质辨识_是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_平和质辨识_倾向是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_痰湿质辨识_倾向是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_痰湿质辨识_是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt痰湿质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_湿热质辨识_倾向是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_湿热质辨识_是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt湿热质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血瘀质辨识_倾向是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_血瘀质辨识_是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血瘀质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_气虚质辨识_是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_气虚质辨识_倾向是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt气虚质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_阳虚质辨识_倾向是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阳虚质辨识_是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阳虚质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_阴虚质辨识_倾向是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_阴虚质辨识_是 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阴虚质得分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_33_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_33_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_33_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_33_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_33_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_32_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_32_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_32_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_32_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_32_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_31_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_31_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_31_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_31_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_31_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_30_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_30_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_30_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_30_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_30_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_29_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_29_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_29_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_29_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_29_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_28_3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_28_4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_28_5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_28_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_28_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_27_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_27_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_27_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_27_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_27_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_26_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_26_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_26_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_26_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_26_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_25_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_25_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_25_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_24_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_24_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_24_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_23_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_23_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_23_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_22_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_22_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_22_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_25_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_25_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_24_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_24_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_23_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_23_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_22_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_22_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_21_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_21_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_21_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_21_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_21_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTable_特征_20_3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_20_4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_20_5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_特征_20_1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_特征_20_2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable39,
            this.xrTable38,
            this.xrTable37,
            this.xrTable36,
            this.xrTable34,
            this.xrLabel64,
            this.xrTable30,
            this.xrTable29,
            this.xrTable26,
            this.xrLabel22,
            this.xrLabel12,
            this.xrTable25,
            this.xrTable2,
            this.xrTable1,
            this.xrTable35,
            this.xrTable33,
            this.xrTable32,
            this.xrTable31,
            this.xrTable28,
            this.xrTable27,
            this.xrTable24,
            this.xrTable23,
            this.xrTable22,
            this.xrTable21,
            this.xrTable19,
            this.xrTable20,
            this.xrTable17,
            this.xrTable18,
            this.xrTable15,
            this.xrTable16,
            this.xrTable13,
            this.xrTable11,
            this.xrTable9,
            this.xrTable8,
            this.xrTable14,
            this.xrTable12,
            this.xrTable10,
            this.xrTable7,
            this.xrTable6,
            this.xrTable5,
            this.xrTable4,
            this.xrTable3});
            this.Detail.Font = new System.Drawing.Font("宋体", 10F);
            this.Detail.HeightF = 853.1247F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable39
            // 
            this.xrTable39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable39.LocationFloat = new DevExpress.Utils.PointFloat(599.9996F, 762.0005F);
            this.xrTable39.Name = "xrTable39";
            this.xrTable39.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39});
            this.xrTable39.SizeF = new System.Drawing.SizeF(525F, 30F);
            this.xrTable39.StylePriority.UseBorders = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell114,
            this.xrTableCell115});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Text = "医生签名";
            this.xrTableCell114.Weight = 1.1203451155676925D;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt医生签名});
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Weight = 1.6805177139510623D;
            // 
            // txt医生签名
            // 
            this.txt医生签名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt医生签名.LocationFloat = new DevExpress.Utils.PointFloat(4.563782F, 1F);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt医生签名.SizeF = new System.Drawing.SizeF(126.0417F, 25F);
            this.txt医生签名.StylePriority.UseBorders = false;
            this.txt医生签名.StylePriority.UseTextAlignment = false;
            this.txt医生签名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable38
            // 
            this.xrTable38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable38.LocationFloat = new DevExpress.Utils.PointFloat(0F, 762.0002F);
            this.xrTable38.Name = "xrTable38";
            this.xrTable38.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable38.SizeF = new System.Drawing.SizeF(600F, 30F);
            this.xrTable38.StylePriority.UseBorders = false;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.xrTableCell113});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Text = "填表日期";
            this.xrTableCell112.Weight = 0.96000000000000008D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt填表日期});
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 2.2399998372166987D;
            // 
            // txt填表日期
            // 
            this.txt填表日期.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt填表日期.LocationFloat = new DevExpress.Utils.PointFloat(9.562047F, 1.000159F);
            this.txt填表日期.Name = "txt填表日期";
            this.txt填表日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt填表日期.SizeF = new System.Drawing.SizeF(150F, 25F);
            this.txt填表日期.StylePriority.UseBorders = false;
            this.txt填表日期.StylePriority.UseTextAlignment = false;
            this.txt填表日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable37
            // 
            this.xrTable37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable37.LocationFloat = new DevExpress.Utils.PointFloat(810.0009F, 607.0004F);
            this.xrTable37.Name = "xrTable37";
            this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37});
            this.xrTable37.SizeF = new System.Drawing.SizeF(315F, 155F);
            this.xrTable37.StylePriority.UseBorders = false;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell110,
            this.xrTableCell111});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_气郁质指导1,
            this.xrLabel_气郁质指导3,
            this.xrLabel_气郁质指导2,
            this.xrLabel_气郁质指导4,
            this.xrLabel_气郁质指导6,
            this.xrLabel_气郁质指导5,
            this.txt气郁质指导其他,
            this.txt气郁质其他,
            this.xrLabel115,
            this.xrLabel116,
            this.xrLabel117,
            this.xrLabel118,
            this.xrLabel119,
            this.xrLabel120});
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 1D;
            // 
            // xrLabel_气郁质指导1
            // 
            this.xrLabel_气郁质指导1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气郁质指导1.CanGrow = false;
            this.xrLabel_气郁质指导1.LocationFloat = new DevExpress.Utils.PointFloat(85.00006F, 4.999939F);
            this.xrLabel_气郁质指导1.Name = "xrLabel_气郁质指导1";
            this.xrLabel_气郁质指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气郁质指导1.SizeF = new System.Drawing.SizeF(19.99872F, 20F);
            this.xrLabel_气郁质指导1.StylePriority.UseBorders = false;
            // 
            // xrLabel_气郁质指导3
            // 
            this.xrLabel_气郁质指导3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气郁质指导3.CanGrow = false;
            this.xrLabel_气郁质指导3.LocationFloat = new DevExpress.Utils.PointFloat(84.99994F, 44.99994F);
            this.xrLabel_气郁质指导3.Name = "xrLabel_气郁质指导3";
            this.xrLabel_气郁质指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气郁质指导3.SizeF = new System.Drawing.SizeF(19.99915F, 20F);
            this.xrLabel_气郁质指导3.StylePriority.UseBorders = false;
            // 
            // xrLabel_气郁质指导2
            // 
            this.xrLabel_气郁质指导2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气郁质指导2.CanGrow = false;
            this.xrLabel_气郁质指导2.LocationFloat = new DevExpress.Utils.PointFloat(84.9999F, 24.99994F);
            this.xrLabel_气郁质指导2.Name = "xrLabel_气郁质指导2";
            this.xrLabel_气郁质指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气郁质指导2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_气郁质指导2.StylePriority.UseBorders = false;
            // 
            // xrLabel_气郁质指导4
            // 
            this.xrLabel_气郁质指导4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气郁质指导4.CanGrow = false;
            this.xrLabel_气郁质指导4.LocationFloat = new DevExpress.Utils.PointFloat(85F, 65.00006F);
            this.xrLabel_气郁质指导4.Name = "xrLabel_气郁质指导4";
            this.xrLabel_气郁质指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气郁质指导4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_气郁质指导4.StylePriority.UseBorders = false;
            // 
            // xrLabel_气郁质指导6
            // 
            this.xrLabel_气郁质指导6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气郁质指导6.CanGrow = false;
            this.xrLabel_气郁质指导6.LocationFloat = new DevExpress.Utils.PointFloat(84.99998F, 104.9999F);
            this.xrLabel_气郁质指导6.Name = "xrLabel_气郁质指导6";
            this.xrLabel_气郁质指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气郁质指导6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_气郁质指导6.StylePriority.UseBorders = false;
            // 
            // xrLabel_气郁质指导5
            // 
            this.xrLabel_气郁质指导5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气郁质指导5.CanGrow = false;
            this.xrLabel_气郁质指导5.LocationFloat = new DevExpress.Utils.PointFloat(85F, 84.99995F);
            this.xrLabel_气郁质指导5.Name = "xrLabel_气郁质指导5";
            this.xrLabel_气郁质指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气郁质指导5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_气郁质指导5.StylePriority.UseBorders = false;
            // 
            // txt气郁质指导其他
            // 
            this.txt气郁质指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt气郁质指导其他.LocationFloat = new DevExpress.Utils.PointFloat(10F, 125.0001F);
            this.txt气郁质指导其他.Name = "txt气郁质指导其他";
            this.txt气郁质指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt气郁质指导其他.SizeF = new System.Drawing.SizeF(85F, 25F);
            this.txt气郁质指导其他.StylePriority.UseBorders = false;
            // 
            // txt气郁质其他
            // 
            this.txt气郁质其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt气郁质其他.LocationFloat = new DevExpress.Utils.PointFloat(55F, 105.0001F);
            this.txt气郁质其他.Name = "txt气郁质其他";
            this.txt气郁质其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt气郁质其他.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt气郁质其他.StylePriority.UseBorders = false;
            // 
            // xrLabel115
            // 
            this.xrLabel115.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(0F, 105.0001F);
            this.xrLabel115.Name = "xrLabel115";
            this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel115.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.xrLabel115.StylePriority.UseBorders = false;
            this.xrLabel115.Text = "6 其他";
            // 
            // xrLabel116
            // 
            this.xrLabel116.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.00005F);
            this.xrLabel116.Name = "xrLabel116";
            this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel116.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel116.StylePriority.UseBorders = false;
            this.xrLabel116.Text = "5 穴位保健";
            // 
            // xrLabel117
            // 
            this.xrLabel117.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64.99998F);
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel117.StylePriority.UseBorders = false;
            this.xrLabel117.Text = "4 运动保健";
            // 
            // xrLabel118
            // 
            this.xrLabel118.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel118.LocationFloat = new DevExpress.Utils.PointFloat(0F, 44.99998F);
            this.xrLabel118.Name = "xrLabel118";
            this.xrLabel118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel118.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel118.StylePriority.UseBorders = false;
            this.xrLabel118.Text = "3 起居调摄";
            // 
            // xrLabel119
            // 
            this.xrLabel119.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel119.LocationFloat = new DevExpress.Utils.PointFloat(0F, 24.99994F);
            this.xrLabel119.Name = "xrLabel119";
            this.xrLabel119.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel119.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel119.StylePriority.UseBorders = false;
            this.xrLabel119.Text = "2 饮食调养";
            // 
            // xrLabel120
            // 
            this.xrLabel120.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999939F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel120.StylePriority.UseBorders = false;
            this.xrLabel120.Text = "1 情志调摄";
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_特禀质指导1,
            this.xrLabel_特禀质指导2,
            this.xrLabel_特禀质指导3,
            this.xrLabel_特禀质指导4,
            this.xrLabel_特禀质指导5,
            this.txt特禀质指导其他,
            this.txt特禀质其他,
            this.xrLabel123,
            this.xrLabel124,
            this.xrLabel125,
            this.xrLabel126,
            this.xrLabel127,
            this.xrLabel128,
            this.xrLabel_特禀质指导6});
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 1D;
            // 
            // xrLabel_特禀质指导1
            // 
            this.xrLabel_特禀质指导1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_特禀质指导1.CanGrow = false;
            this.xrLabel_特禀质指导1.LocationFloat = new DevExpress.Utils.PointFloat(85.00019F, 4.999929F);
            this.xrLabel_特禀质指导1.Name = "xrLabel_特禀质指导1";
            this.xrLabel_特禀质指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_特禀质指导1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_特禀质指导1.StylePriority.UseBorders = false;
            // 
            // xrLabel_特禀质指导2
            // 
            this.xrLabel_特禀质指导2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_特禀质指导2.CanGrow = false;
            this.xrLabel_特禀质指导2.LocationFloat = new DevExpress.Utils.PointFloat(85.00019F, 24.99991F);
            this.xrLabel_特禀质指导2.Name = "xrLabel_特禀质指导2";
            this.xrLabel_特禀质指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_特禀质指导2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_特禀质指导2.StylePriority.UseBorders = false;
            // 
            // xrLabel_特禀质指导3
            // 
            this.xrLabel_特禀质指导3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_特禀质指导3.CanGrow = false;
            this.xrLabel_特禀质指导3.LocationFloat = new DevExpress.Utils.PointFloat(85.00018F, 44.99994F);
            this.xrLabel_特禀质指导3.Name = "xrLabel_特禀质指导3";
            this.xrLabel_特禀质指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_特禀质指导3.SizeF = new System.Drawing.SizeF(19.9986F, 20F);
            this.xrLabel_特禀质指导3.StylePriority.UseBorders = false;
            // 
            // xrLabel_特禀质指导4
            // 
            this.xrLabel_特禀质指导4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_特禀质指导4.CanGrow = false;
            this.xrLabel_特禀质指导4.LocationFloat = new DevExpress.Utils.PointFloat(85.00015F, 65.00006F);
            this.xrLabel_特禀质指导4.Name = "xrLabel_特禀质指导4";
            this.xrLabel_特禀质指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_特禀质指导4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_特禀质指导4.StylePriority.UseBorders = false;
            // 
            // xrLabel_特禀质指导5
            // 
            this.xrLabel_特禀质指导5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_特禀质指导5.CanGrow = false;
            this.xrLabel_特禀质指导5.LocationFloat = new DevExpress.Utils.PointFloat(85.00012F, 84.99988F);
            this.xrLabel_特禀质指导5.Name = "xrLabel_特禀质指导5";
            this.xrLabel_特禀质指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_特禀质指导5.SizeF = new System.Drawing.SizeF(19.99854F, 20.00006F);
            this.xrLabel_特禀质指导5.StylePriority.UseBorders = false;
            // 
            // txt特禀质指导其他
            // 
            this.txt特禀质指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt特禀质指导其他.LocationFloat = new DevExpress.Utils.PointFloat(9.12439F, 125.0002F);
            this.txt特禀质指导其他.Name = "txt特禀质指导其他";
            this.txt特禀质指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt特禀质指导其他.SizeF = new System.Drawing.SizeF(85F, 25F);
            this.txt特禀质指导其他.StylePriority.UseBorders = false;
            // 
            // txt特禀质其他
            // 
            this.txt特禀质其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt特禀质其他.LocationFloat = new DevExpress.Utils.PointFloat(55F, 105.0001F);
            this.txt特禀质其他.Name = "txt特禀质其他";
            this.txt特禀质其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt特禀质其他.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt特禀质其他.StylePriority.UseBorders = false;
            // 
            // xrLabel123
            // 
            this.xrLabel123.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel123.LocationFloat = new DevExpress.Utils.PointFloat(0F, 105.0001F);
            this.xrLabel123.Name = "xrLabel123";
            this.xrLabel123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel123.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.xrLabel123.StylePriority.UseBorders = false;
            this.xrLabel123.Text = "6 其他";
            // 
            // xrLabel124
            // 
            this.xrLabel124.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel124.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.00005F);
            this.xrLabel124.Name = "xrLabel124";
            this.xrLabel124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel124.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel124.StylePriority.UseBorders = false;
            this.xrLabel124.Text = "5 穴位保健";
            // 
            // xrLabel125
            // 
            this.xrLabel125.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel125.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64.99998F);
            this.xrLabel125.Name = "xrLabel125";
            this.xrLabel125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel125.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel125.StylePriority.UseBorders = false;
            this.xrLabel125.Text = "4 运动保健";
            // 
            // xrLabel126
            // 
            this.xrLabel126.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel126.LocationFloat = new DevExpress.Utils.PointFloat(0F, 44.99994F);
            this.xrLabel126.Name = "xrLabel126";
            this.xrLabel126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel126.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel126.StylePriority.UseBorders = false;
            this.xrLabel126.Text = "3 起居调摄";
            // 
            // xrLabel127
            // 
            this.xrLabel127.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel127.LocationFloat = new DevExpress.Utils.PointFloat(0F, 24.99994F);
            this.xrLabel127.Name = "xrLabel127";
            this.xrLabel127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel127.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel127.StylePriority.UseBorders = false;
            this.xrLabel127.Text = "2 饮食调养";
            // 
            // xrLabel128
            // 
            this.xrLabel128.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel128.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999939F);
            this.xrLabel128.Name = "xrLabel128";
            this.xrLabel128.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel128.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel128.StylePriority.UseBorders = false;
            this.xrLabel128.Text = "1 情志调摄";
            // 
            // xrLabel_特禀质指导6
            // 
            this.xrLabel_特禀质指导6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_特禀质指导6.CanGrow = false;
            this.xrLabel_特禀质指导6.LocationFloat = new DevExpress.Utils.PointFloat(85.00003F, 104.9999F);
            this.xrLabel_特禀质指导6.Name = "xrLabel_特禀质指导6";
            this.xrLabel_特禀质指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_特禀质指导6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_特禀质指导6.StylePriority.UseBorders = false;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_平和质指导6,
            this.xrLabel_平和质指导5,
            this.xrLabel_平和质指导4,
            this.xrLabel_平和质指导3,
            this.xrLabel_平和质指导2,
            this.txt平和质指导其他,
            this.txt平和质其他,
            this.xrLabel131,
            this.xrLabel132,
            this.xrLabel133,
            this.xrLabel134,
            this.xrLabel135,
            this.xrLabel136,
            this.xrLabel_平和质指导1});
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Weight = 1D;
            // 
            // xrLabel_平和质指导6
            // 
            this.xrLabel_平和质指导6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_平和质指导6.CanGrow = false;
            this.xrLabel_平和质指导6.LocationFloat = new DevExpress.Utils.PointFloat(85.00003F, 104.9998F);
            this.xrLabel_平和质指导6.Name = "xrLabel_平和质指导6";
            this.xrLabel_平和质指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_平和质指导6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_平和质指导6.StylePriority.UseBorders = false;
            // 
            // xrLabel_平和质指导5
            // 
            this.xrLabel_平和质指导5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_平和质指导5.CanGrow = false;
            this.xrLabel_平和质指导5.LocationFloat = new DevExpress.Utils.PointFloat(85.00003F, 84.9999F);
            this.xrLabel_平和质指导5.Name = "xrLabel_平和质指导5";
            this.xrLabel_平和质指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_平和质指导5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_平和质指导5.StylePriority.UseBorders = false;
            // 
            // xrLabel_平和质指导4
            // 
            this.xrLabel_平和质指导4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_平和质指导4.CanGrow = false;
            this.xrLabel_平和质指导4.LocationFloat = new DevExpress.Utils.PointFloat(85.00006F, 64.99988F);
            this.xrLabel_平和质指导4.Name = "xrLabel_平和质指导4";
            this.xrLabel_平和质指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_平和质指导4.SizeF = new System.Drawing.SizeF(19.99878F, 20F);
            this.xrLabel_平和质指导4.StylePriority.UseBorders = false;
            // 
            // xrLabel_平和质指导3
            // 
            this.xrLabel_平和质指导3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_平和质指导3.CanGrow = false;
            this.xrLabel_平和质指导3.LocationFloat = new DevExpress.Utils.PointFloat(85.00007F, 44.99992F);
            this.xrLabel_平和质指导3.Name = "xrLabel_平和质指导3";
            this.xrLabel_平和质指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_平和质指导3.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_平和质指导3.StylePriority.UseBorders = false;
            // 
            // xrLabel_平和质指导2
            // 
            this.xrLabel_平和质指导2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_平和质指导2.CanGrow = false;
            this.xrLabel_平和质指导2.LocationFloat = new DevExpress.Utils.PointFloat(85.00007F, 24.99994F);
            this.xrLabel_平和质指导2.Name = "xrLabel_平和质指导2";
            this.xrLabel_平和质指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_平和质指导2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_平和质指导2.StylePriority.UseBorders = false;
            // 
            // txt平和质指导其他
            // 
            this.txt平和质指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt平和质指导其他.LocationFloat = new DevExpress.Utils.PointFloat(6.999573F, 125.0001F);
            this.txt平和质指导其他.Name = "txt平和质指导其他";
            this.txt平和质指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt平和质指导其他.SizeF = new System.Drawing.SizeF(85F, 25F);
            this.txt平和质指导其他.StylePriority.UseBorders = false;
            // 
            // txt平和质其他
            // 
            this.txt平和质其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt平和质其他.LocationFloat = new DevExpress.Utils.PointFloat(54.99994F, 105.0001F);
            this.txt平和质其他.Name = "txt平和质其他";
            this.txt平和质其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt平和质其他.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt平和质其他.StylePriority.UseBorders = false;
            // 
            // xrLabel131
            // 
            this.xrLabel131.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel131.LocationFloat = new DevExpress.Utils.PointFloat(0F, 105.0001F);
            this.xrLabel131.Name = "xrLabel131";
            this.xrLabel131.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel131.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.xrLabel131.StylePriority.UseBorders = false;
            this.xrLabel131.Text = "6 其他";
            // 
            // xrLabel132
            // 
            this.xrLabel132.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel132.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.00005F);
            this.xrLabel132.Name = "xrLabel132";
            this.xrLabel132.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel132.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel132.StylePriority.UseBorders = false;
            this.xrLabel132.Text = "5 穴位保健";
            // 
            // xrLabel133
            // 
            this.xrLabel133.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel133.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64.99998F);
            this.xrLabel133.Name = "xrLabel133";
            this.xrLabel133.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel133.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel133.StylePriority.UseBorders = false;
            this.xrLabel133.Text = "4 运动保健";
            // 
            // xrLabel134
            // 
            this.xrLabel134.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel134.LocationFloat = new DevExpress.Utils.PointFloat(0F, 44.99994F);
            this.xrLabel134.Name = "xrLabel134";
            this.xrLabel134.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel134.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel134.StylePriority.UseBorders = false;
            this.xrLabel134.Text = "3 起居调摄";
            // 
            // xrLabel135
            // 
            this.xrLabel135.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel135.LocationFloat = new DevExpress.Utils.PointFloat(0F, 24.99994F);
            this.xrLabel135.Name = "xrLabel135";
            this.xrLabel135.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel135.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel135.StylePriority.UseBorders = false;
            this.xrLabel135.Text = "2 饮食调养";
            // 
            // xrLabel136
            // 
            this.xrLabel136.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel136.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999939F);
            this.xrLabel136.Name = "xrLabel136";
            this.xrLabel136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel136.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel136.StylePriority.UseBorders = false;
            this.xrLabel136.Text = "1 情志调摄";
            // 
            // xrLabel_平和质指导1
            // 
            this.xrLabel_平和质指导1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_平和质指导1.CanGrow = false;
            this.xrLabel_平和质指导1.LocationFloat = new DevExpress.Utils.PointFloat(85.00007F, 4.99995F);
            this.xrLabel_平和质指导1.Name = "xrLabel_平和质指导1";
            this.xrLabel_平和质指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_平和质指导1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_平和质指导1.StylePriority.UseBorders = false;
            // 
            // xrTable36
            // 
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.LocationFloat = new DevExpress.Utils.PointFloat(494.9996F, 607.0005F);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable36.SizeF = new System.Drawing.SizeF(315F, 155F);
            this.xrTable36.StylePriority.UseBorders = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_痰湿质指导6,
            this.xrLabel_痰湿质指导5,
            this.xrLabel_痰湿质指导4,
            this.xrLabel_痰湿质指导3,
            this.xrLabel_痰湿质指导2,
            this.txt痰湿质指导其他,
            this.txt痰湿质其他,
            this.xrLabel91,
            this.xrLabel92,
            this.xrLabel93,
            this.xrLabel94,
            this.xrLabel95,
            this.xrLabel96,
            this.xrLabel_痰湿质指导1});
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Weight = 1D;
            // 
            // xrLabel_痰湿质指导6
            // 
            this.xrLabel_痰湿质指导6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_痰湿质指导6.CanGrow = false;
            this.xrLabel_痰湿质指导6.LocationFloat = new DevExpress.Utils.PointFloat(85F, 104.9999F);
            this.xrLabel_痰湿质指导6.Name = "xrLabel_痰湿质指导6";
            this.xrLabel_痰湿质指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_痰湿质指导6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_痰湿质指导6.StylePriority.UseBorders = false;
            // 
            // xrLabel_痰湿质指导5
            // 
            this.xrLabel_痰湿质指导5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_痰湿质指导5.CanGrow = false;
            this.xrLabel_痰湿质指导5.LocationFloat = new DevExpress.Utils.PointFloat(85.00071F, 84.99995F);
            this.xrLabel_痰湿质指导5.Name = "xrLabel_痰湿质指导5";
            this.xrLabel_痰湿质指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_痰湿质指导5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_痰湿质指导5.StylePriority.UseBorders = false;
            // 
            // xrLabel_痰湿质指导4
            // 
            this.xrLabel_痰湿质指导4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_痰湿质指导4.CanGrow = false;
            this.xrLabel_痰湿质指导4.LocationFloat = new DevExpress.Utils.PointFloat(85F, 64.99995F);
            this.xrLabel_痰湿质指导4.Name = "xrLabel_痰湿质指导4";
            this.xrLabel_痰湿质指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_痰湿质指导4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_痰湿质指导4.StylePriority.UseBorders = false;
            // 
            // xrLabel_痰湿质指导3
            // 
            this.xrLabel_痰湿质指导3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_痰湿质指导3.CanGrow = false;
            this.xrLabel_痰湿质指导3.LocationFloat = new DevExpress.Utils.PointFloat(85F, 44.99995F);
            this.xrLabel_痰湿质指导3.Name = "xrLabel_痰湿质指导3";
            this.xrLabel_痰湿质指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_痰湿质指导3.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_痰湿质指导3.StylePriority.UseBorders = false;
            // 
            // xrLabel_痰湿质指导2
            // 
            this.xrLabel_痰湿质指导2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_痰湿质指导2.CanGrow = false;
            this.xrLabel_痰湿质指导2.LocationFloat = new DevExpress.Utils.PointFloat(85F, 24.99995F);
            this.xrLabel_痰湿质指导2.Name = "xrLabel_痰湿质指导2";
            this.xrLabel_痰湿质指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_痰湿质指导2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_痰湿质指导2.StylePriority.UseBorders = false;
            // 
            // txt痰湿质指导其他
            // 
            this.txt痰湿质指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt痰湿质指导其他.LocationFloat = new DevExpress.Utils.PointFloat(10.00003F, 125.0001F);
            this.txt痰湿质指导其他.Name = "txt痰湿质指导其他";
            this.txt痰湿质指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt痰湿质指导其他.SizeF = new System.Drawing.SizeF(85F, 25F);
            this.txt痰湿质指导其他.StylePriority.UseBorders = false;
            // 
            // txt痰湿质其他
            // 
            this.txt痰湿质其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt痰湿质其他.LocationFloat = new DevExpress.Utils.PointFloat(55.00003F, 105.0001F);
            this.txt痰湿质其他.Name = "txt痰湿质其他";
            this.txt痰湿质其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt痰湿质其他.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt痰湿质其他.StylePriority.UseBorders = false;
            // 
            // xrLabel91
            // 
            this.xrLabel91.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel91.LocationFloat = new DevExpress.Utils.PointFloat(0F, 105.0001F);
            this.xrLabel91.Name = "xrLabel91";
            this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel91.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.xrLabel91.StylePriority.UseBorders = false;
            this.xrLabel91.Text = "6 其他";
            // 
            // xrLabel92
            // 
            this.xrLabel92.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.00005F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel92.StylePriority.UseBorders = false;
            this.xrLabel92.Text = "5 穴位保健";
            // 
            // xrLabel93
            // 
            this.xrLabel93.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64.99998F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel93.StylePriority.UseBorders = false;
            this.xrLabel93.Text = "4 运动保健";
            // 
            // xrLabel94
            // 
            this.xrLabel94.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(0F, 44.99994F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel94.StylePriority.UseBorders = false;
            this.xrLabel94.Text = "3 起居调摄";
            // 
            // xrLabel95
            // 
            this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(0F, 24.99994F);
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel95.StylePriority.UseBorders = false;
            this.xrLabel95.Text = "2 饮食调养";
            // 
            // xrLabel96
            // 
            this.xrLabel96.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999939F);
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel96.StylePriority.UseBorders = false;
            this.xrLabel96.Text = "1 情志调摄";
            // 
            // xrLabel_痰湿质指导1
            // 
            this.xrLabel_痰湿质指导1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_痰湿质指导1.CanGrow = false;
            this.xrLabel_痰湿质指导1.LocationFloat = new DevExpress.Utils.PointFloat(85F, 4.999949F);
            this.xrLabel_痰湿质指导1.Name = "xrLabel_痰湿质指导1";
            this.xrLabel_痰湿质指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_痰湿质指导1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_痰湿质指导1.StylePriority.UseBorders = false;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_湿热质指导4,
            this.xrLabel_湿热质指导3,
            this.xrLabel_湿热质指导2,
            this.xrLabel_湿热质指导1,
            this.txt湿热质指导其他,
            this.txt湿热质其他,
            this.xrLabel99,
            this.xrLabel100,
            this.xrLabel101,
            this.xrLabel102,
            this.xrLabel103,
            this.xrLabel104,
            this.xrLabel_湿热质指导5,
            this.xrLabel_湿热质指导6});
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 1D;
            // 
            // xrLabel_湿热质指导4
            // 
            this.xrLabel_湿热质指导4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_湿热质指导4.CanGrow = false;
            this.xrLabel_湿热质指导4.LocationFloat = new DevExpress.Utils.PointFloat(84.99991F, 65.00012F);
            this.xrLabel_湿热质指导4.Name = "xrLabel_湿热质指导4";
            this.xrLabel_湿热质指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_湿热质指导4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_湿热质指导4.StylePriority.UseBorders = false;
            // 
            // xrLabel_湿热质指导3
            // 
            this.xrLabel_湿热质指导3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_湿热质指导3.CanGrow = false;
            this.xrLabel_湿热质指导3.LocationFloat = new DevExpress.Utils.PointFloat(85.0006F, 44.99997F);
            this.xrLabel_湿热质指导3.Name = "xrLabel_湿热质指导3";
            this.xrLabel_湿热质指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_湿热质指导3.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_湿热质指导3.StylePriority.UseBorders = false;
            // 
            // xrLabel_湿热质指导2
            // 
            this.xrLabel_湿热质指导2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_湿热质指导2.CanGrow = false;
            this.xrLabel_湿热质指导2.LocationFloat = new DevExpress.Utils.PointFloat(85.00056F, 24.99995F);
            this.xrLabel_湿热质指导2.Name = "xrLabel_湿热质指导2";
            this.xrLabel_湿热质指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_湿热质指导2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_湿热质指导2.StylePriority.UseBorders = false;
            // 
            // xrLabel_湿热质指导1
            // 
            this.xrLabel_湿热质指导1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_湿热质指导1.CanGrow = false;
            this.xrLabel_湿热质指导1.LocationFloat = new DevExpress.Utils.PointFloat(85.00056F, 4.999949F);
            this.xrLabel_湿热质指导1.Name = "xrLabel_湿热质指导1";
            this.xrLabel_湿热质指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_湿热质指导1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_湿热质指导1.StylePriority.UseBorders = false;
            // 
            // txt湿热质指导其他
            // 
            this.txt湿热质指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt湿热质指导其他.LocationFloat = new DevExpress.Utils.PointFloat(4.562408F, 125.0001F);
            this.txt湿热质指导其他.Name = "txt湿热质指导其他";
            this.txt湿热质指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt湿热质指导其他.SizeF = new System.Drawing.SizeF(85F, 25F);
            this.txt湿热质指导其他.StylePriority.UseBorders = false;
            // 
            // txt湿热质其他
            // 
            this.txt湿热质其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt湿热质其他.LocationFloat = new DevExpress.Utils.PointFloat(54.99997F, 105.0001F);
            this.txt湿热质其他.Name = "txt湿热质其他";
            this.txt湿热质其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt湿热质其他.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt湿热质其他.StylePriority.UseBorders = false;
            // 
            // xrLabel99
            // 
            this.xrLabel99.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel99.LocationFloat = new DevExpress.Utils.PointFloat(0F, 105.0001F);
            this.xrLabel99.Name = "xrLabel99";
            this.xrLabel99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel99.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.xrLabel99.StylePriority.UseBorders = false;
            this.xrLabel99.Text = "6 其他";
            // 
            // xrLabel100
            // 
            this.xrLabel100.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel100.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.0001F);
            this.xrLabel100.Name = "xrLabel100";
            this.xrLabel100.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel100.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel100.StylePriority.UseBorders = false;
            this.xrLabel100.Text = "5 穴位保健";
            // 
            // xrLabel101
            // 
            this.xrLabel101.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel101.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64.99998F);
            this.xrLabel101.Name = "xrLabel101";
            this.xrLabel101.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel101.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel101.StylePriority.UseBorders = false;
            this.xrLabel101.Text = "4 运动保健";
            // 
            // xrLabel102
            // 
            this.xrLabel102.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(0F, 44.99994F);
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel102.StylePriority.UseBorders = false;
            this.xrLabel102.Text = "3 起居调摄";
            // 
            // xrLabel103
            // 
            this.xrLabel103.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(0F, 24.99994F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel103.StylePriority.UseBorders = false;
            this.xrLabel103.Text = "2 饮食调养";
            // 
            // xrLabel104
            // 
            this.xrLabel104.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999939F);
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel104.StylePriority.UseBorders = false;
            this.xrLabel104.Text = "1 情志调摄";
            // 
            // xrLabel_湿热质指导5
            // 
            this.xrLabel_湿热质指导5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_湿热质指导5.CanGrow = false;
            this.xrLabel_湿热质指导5.LocationFloat = new DevExpress.Utils.PointFloat(85F, 85.00015F);
            this.xrLabel_湿热质指导5.Name = "xrLabel_湿热质指导5";
            this.xrLabel_湿热质指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_湿热质指导5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_湿热质指导5.StylePriority.UseBorders = false;
            // 
            // xrLabel_湿热质指导6
            // 
            this.xrLabel_湿热质指导6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_湿热质指导6.CanGrow = false;
            this.xrLabel_湿热质指导6.LocationFloat = new DevExpress.Utils.PointFloat(85.00046F, 105.0001F);
            this.xrLabel_湿热质指导6.Name = "xrLabel_湿热质指导6";
            this.xrLabel_湿热质指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_湿热质指导6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_湿热质指导6.StylePriority.UseBorders = false;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血瘀质指导6,
            this.xrLabel_血瘀质指导5,
            this.xrLabel_血瘀质指导4,
            this.xrLabel_血瘀质指导3,
            this.xrLabel_血瘀质指导2,
            this.txt血瘀质指导其他,
            this.txt血瘀质其他,
            this.xrLabel107,
            this.xrLabel109,
            this.xrLabel110,
            this.xrLabel111,
            this.xrLabel112,
            this.xrLabel108,
            this.xrLabel_血瘀质指导1});
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Weight = 1D;
            // 
            // xrLabel_血瘀质指导6
            // 
            this.xrLabel_血瘀质指导6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_血瘀质指导6.CanGrow = false;
            this.xrLabel_血瘀质指导6.LocationFloat = new DevExpress.Utils.PointFloat(85.00064F, 105.0002F);
            this.xrLabel_血瘀质指导6.Name = "xrLabel_血瘀质指导6";
            this.xrLabel_血瘀质指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血瘀质指导6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_血瘀质指导6.StylePriority.UseBorders = false;
            // 
            // xrLabel_血瘀质指导5
            // 
            this.xrLabel_血瘀质指导5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_血瘀质指导5.CanGrow = false;
            this.xrLabel_血瘀质指导5.LocationFloat = new DevExpress.Utils.PointFloat(84.99997F, 85.00018F);
            this.xrLabel_血瘀质指导5.Name = "xrLabel_血瘀质指导5";
            this.xrLabel_血瘀质指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血瘀质指导5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_血瘀质指导5.StylePriority.UseBorders = false;
            // 
            // xrLabel_血瘀质指导4
            // 
            this.xrLabel_血瘀质指导4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_血瘀质指导4.CanGrow = false;
            this.xrLabel_血瘀质指导4.LocationFloat = new DevExpress.Utils.PointFloat(84.99997F, 64.99995F);
            this.xrLabel_血瘀质指导4.Name = "xrLabel_血瘀质指导4";
            this.xrLabel_血瘀质指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血瘀质指导4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_血瘀质指导4.StylePriority.UseBorders = false;
            // 
            // xrLabel_血瘀质指导3
            // 
            this.xrLabel_血瘀质指导3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_血瘀质指导3.CanGrow = false;
            this.xrLabel_血瘀质指导3.LocationFloat = new DevExpress.Utils.PointFloat(85.00064F, 45F);
            this.xrLabel_血瘀质指导3.Name = "xrLabel_血瘀质指导3";
            this.xrLabel_血瘀质指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血瘀质指导3.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_血瘀质指导3.StylePriority.UseBorders = false;
            // 
            // xrLabel_血瘀质指导2
            // 
            this.xrLabel_血瘀质指导2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_血瘀质指导2.CanGrow = false;
            this.xrLabel_血瘀质指导2.LocationFloat = new DevExpress.Utils.PointFloat(85.00064F, 24.99994F);
            this.xrLabel_血瘀质指导2.Name = "xrLabel_血瘀质指导2";
            this.xrLabel_血瘀质指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血瘀质指导2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_血瘀质指导2.StylePriority.UseBorders = false;
            // 
            // txt血瘀质指导其他
            // 
            this.txt血瘀质指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血瘀质指导其他.LocationFloat = new DevExpress.Utils.PointFloat(6.999969F, 125.0001F);
            this.txt血瘀质指导其他.Name = "txt血瘀质指导其他";
            this.txt血瘀质指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血瘀质指导其他.SizeF = new System.Drawing.SizeF(85F, 25F);
            this.txt血瘀质指导其他.StylePriority.UseBorders = false;
            // 
            // txt血瘀质其他
            // 
            this.txt血瘀质其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血瘀质其他.LocationFloat = new DevExpress.Utils.PointFloat(55.00003F, 105.0001F);
            this.txt血瘀质其他.Name = "txt血瘀质其他";
            this.txt血瘀质其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血瘀质其他.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt血瘀质其他.StylePriority.UseBorders = false;
            // 
            // xrLabel107
            // 
            this.xrLabel107.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel107.LocationFloat = new DevExpress.Utils.PointFloat(0F, 105.0001F);
            this.xrLabel107.Name = "xrLabel107";
            this.xrLabel107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel107.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.xrLabel107.StylePriority.UseBorders = false;
            this.xrLabel107.Text = "6 其他";
            // 
            // xrLabel109
            // 
            this.xrLabel109.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel109.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64.99998F);
            this.xrLabel109.Name = "xrLabel109";
            this.xrLabel109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel109.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel109.StylePriority.UseBorders = false;
            this.xrLabel109.Text = "4 运动保健";
            // 
            // xrLabel110
            // 
            this.xrLabel110.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel110.LocationFloat = new DevExpress.Utils.PointFloat(0F, 44.99994F);
            this.xrLabel110.Name = "xrLabel110";
            this.xrLabel110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel110.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel110.StylePriority.UseBorders = false;
            this.xrLabel110.Text = "3 起居调摄";
            // 
            // xrLabel111
            // 
            this.xrLabel111.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(0F, 24.99994F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel111.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel111.StylePriority.UseBorders = false;
            this.xrLabel111.Text = "2 饮食调养";
            // 
            // xrLabel112
            // 
            this.xrLabel112.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel112.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999939F);
            this.xrLabel112.Name = "xrLabel112";
            this.xrLabel112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel112.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel112.StylePriority.UseBorders = false;
            this.xrLabel112.Text = "1 情志调摄";
            // 
            // xrLabel108
            // 
            this.xrLabel108.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(0.000559489F, 84.99995F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel108.StylePriority.UseBorders = false;
            this.xrLabel108.Text = "5 穴位保健";
            // 
            // xrLabel_血瘀质指导1
            // 
            this.xrLabel_血瘀质指导1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_血瘀质指导1.CanGrow = false;
            this.xrLabel_血瘀质指导1.LocationFloat = new DevExpress.Utils.PointFloat(84.99995F, 4.999949F);
            this.xrLabel_血瘀质指导1.Name = "xrLabel_血瘀质指导1";
            this.xrLabel_血瘀质指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血瘀质指导1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_血瘀质指导1.StylePriority.UseBorders = false;
            // 
            // xrTable34
            // 
            this.xrTable34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable34.LocationFloat = new DevExpress.Utils.PointFloat(179.9997F, 607.0005F);
            this.xrTable34.Name = "xrTable34";
            this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrTable34.SizeF = new System.Drawing.SizeF(315F, 155F);
            this.xrTable34.StylePriority.UseBorders = false;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_气虚质指导6,
            this.xrLabel_气虚质指导5,
            this.xrLabel_气虚质指导4,
            this.xrLabel_气虚质指导3,
            this.txt气虚质指导其他,
            this.txt气虚质其他,
            this.xrLabel67,
            this.xrLabel68,
            this.xrLabel69,
            this.xrLabel70,
            this.xrLabel71,
            this.xrLabel72,
            this.xrLabel_气虚质指导1,
            this.xrLabel_气虚质指导2});
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Weight = 1D;
            // 
            // xrLabel_气虚质指导6
            // 
            this.xrLabel_气虚质指导6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气虚质指导6.CanGrow = false;
            this.xrLabel_气虚质指导6.LocationFloat = new DevExpress.Utils.PointFloat(84.99995F, 105.0001F);
            this.xrLabel_气虚质指导6.Name = "xrLabel_气虚质指导6";
            this.xrLabel_气虚质指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气虚质指导6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_气虚质指导6.StylePriority.UseBorders = false;
            // 
            // xrLabel_气虚质指导5
            // 
            this.xrLabel_气虚质指导5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气虚质指导5.CanGrow = false;
            this.xrLabel_气虚质指导5.LocationFloat = new DevExpress.Utils.PointFloat(84.99995F, 85F);
            this.xrLabel_气虚质指导5.Name = "xrLabel_气虚质指导5";
            this.xrLabel_气虚质指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气虚质指导5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_气虚质指导5.StylePriority.UseBorders = false;
            // 
            // xrLabel_气虚质指导4
            // 
            this.xrLabel_气虚质指导4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气虚质指导4.CanGrow = false;
            this.xrLabel_气虚质指导4.LocationFloat = new DevExpress.Utils.PointFloat(85.00036F, 65F);
            this.xrLabel_气虚质指导4.Name = "xrLabel_气虚质指导4";
            this.xrLabel_气虚质指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气虚质指导4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_气虚质指导4.StylePriority.UseBorders = false;
            // 
            // xrLabel_气虚质指导3
            // 
            this.xrLabel_气虚质指导3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气虚质指导3.CanGrow = false;
            this.xrLabel_气虚质指导3.LocationFloat = new DevExpress.Utils.PointFloat(85.00037F, 45.00002F);
            this.xrLabel_气虚质指导3.Name = "xrLabel_气虚质指导3";
            this.xrLabel_气虚质指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气虚质指导3.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_气虚质指导3.StylePriority.UseBorders = false;
            // 
            // txt气虚质指导其他
            // 
            this.txt气虚质指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt气虚质指导其他.LocationFloat = new DevExpress.Utils.PointFloat(10.00031F, 125.0001F);
            this.txt气虚质指导其他.Name = "txt气虚质指导其他";
            this.txt气虚质指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt气虚质指导其他.SizeF = new System.Drawing.SizeF(85F, 25F);
            this.txt气虚质指导其他.StylePriority.UseBorders = false;
            // 
            // txt气虚质其他
            // 
            this.txt气虚质其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt气虚质其他.LocationFloat = new DevExpress.Utils.PointFloat(54.99992F, 105.0002F);
            this.txt气虚质其他.Name = "txt气虚质其他";
            this.txt气虚质其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt气虚质其他.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt气虚质其他.StylePriority.UseBorders = false;
            // 
            // xrLabel67
            // 
            this.xrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(0.0003306071F, 105.0002F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.xrLabel67.StylePriority.UseBorders = false;
            this.xrLabel67.Text = "6 其他";
            // 
            // xrLabel68
            // 
            this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.00015F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel68.StylePriority.UseBorders = false;
            this.xrLabel68.Text = "5 穴位保健";
            // 
            // xrLabel69
            // 
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(0F, 65F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel69.StylePriority.UseBorders = false;
            this.xrLabel69.Text = "4 运动保健";
            // 
            // xrLabel70
            // 
            this.xrLabel70.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(0.0003306071F, 44.99995F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel70.StylePriority.UseBorders = false;
            this.xrLabel70.Text = "3 起居调摄";
            // 
            // xrLabel71
            // 
            this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(0F, 24.99995F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel71.StylePriority.UseBorders = false;
            this.xrLabel71.Text = "2 饮食调养";
            // 
            // xrLabel72
            // 
            this.xrLabel72.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999949F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel72.StylePriority.UseBorders = false;
            this.xrLabel72.Text = "1 情志调摄";
            // 
            // xrLabel_气虚质指导1
            // 
            this.xrLabel_气虚质指导1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气虚质指导1.CanGrow = false;
            this.xrLabel_气虚质指导1.LocationFloat = new DevExpress.Utils.PointFloat(84.99994F, 4.999939F);
            this.xrLabel_气虚质指导1.Name = "xrLabel_气虚质指导1";
            this.xrLabel_气虚质指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气虚质指导1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_气虚质指导1.StylePriority.UseBorders = false;
            // 
            // xrLabel_气虚质指导2
            // 
            this.xrLabel_气虚质指导2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气虚质指导2.CanGrow = false;
            this.xrLabel_气虚质指导2.LocationFloat = new DevExpress.Utils.PointFloat(84.99995F, 24.99995F);
            this.xrLabel_气虚质指导2.Name = "xrLabel_气虚质指导2";
            this.xrLabel_气虚质指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气虚质指导2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_气虚质指导2.StylePriority.UseBorders = false;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_阳虚质指导6,
            this.xrLabel_阳虚质指导5,
            this.xrLabel_阳虚质指导4,
            this.xrLabel_阳虚质指导3,
            this.xrLabel_阳虚质指导2,
            this.xrLabel_阳虚质指导1,
            this.txt阳虚质指导其他,
            this.txt阳虚质其他,
            this.xrLabel75,
            this.xrLabel76,
            this.xrLabel77,
            this.xrLabel78,
            this.xrLabel79,
            this.xrLabel80});
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 1D;
            // 
            // xrLabel_阳虚质指导6
            // 
            this.xrLabel_阳虚质指导6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阳虚质指导6.CanGrow = false;
            this.xrLabel_阳虚质指导6.LocationFloat = new DevExpress.Utils.PointFloat(84.99995F, 105.0001F);
            this.xrLabel_阳虚质指导6.Name = "xrLabel_阳虚质指导6";
            this.xrLabel_阳虚质指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阳虚质指导6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阳虚质指导6.StylePriority.UseBorders = false;
            // 
            // xrLabel_阳虚质指导5
            // 
            this.xrLabel_阳虚质指导5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阳虚质指导5.CanGrow = false;
            this.xrLabel_阳虚质指导5.LocationFloat = new DevExpress.Utils.PointFloat(84.99994F, 85.00012F);
            this.xrLabel_阳虚质指导5.Name = "xrLabel_阳虚质指导5";
            this.xrLabel_阳虚质指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阳虚质指导5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阳虚质指导5.StylePriority.UseBorders = false;
            // 
            // xrLabel_阳虚质指导4
            // 
            this.xrLabel_阳虚质指导4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阳虚质指导4.CanGrow = false;
            this.xrLabel_阳虚质指导4.LocationFloat = new DevExpress.Utils.PointFloat(85.00036F, 65.0001F);
            this.xrLabel_阳虚质指导4.Name = "xrLabel_阳虚质指导4";
            this.xrLabel_阳虚质指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阳虚质指导4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阳虚质指导4.StylePriority.UseBorders = false;
            // 
            // xrLabel_阳虚质指导3
            // 
            this.xrLabel_阳虚质指导3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阳虚质指导3.CanGrow = false;
            this.xrLabel_阳虚质指导3.LocationFloat = new DevExpress.Utils.PointFloat(85.00036F, 45F);
            this.xrLabel_阳虚质指导3.Name = "xrLabel_阳虚质指导3";
            this.xrLabel_阳虚质指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阳虚质指导3.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阳虚质指导3.StylePriority.UseBorders = false;
            // 
            // xrLabel_阳虚质指导2
            // 
            this.xrLabel_阳虚质指导2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阳虚质指导2.CanGrow = false;
            this.xrLabel_阳虚质指导2.LocationFloat = new DevExpress.Utils.PointFloat(84.99995F, 24.99995F);
            this.xrLabel_阳虚质指导2.Name = "xrLabel_阳虚质指导2";
            this.xrLabel_阳虚质指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阳虚质指导2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阳虚质指导2.StylePriority.UseBorders = false;
            // 
            // xrLabel_阳虚质指导1
            // 
            this.xrLabel_阳虚质指导1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阳虚质指导1.CanGrow = false;
            this.xrLabel_阳虚质指导1.LocationFloat = new DevExpress.Utils.PointFloat(85F, 4.999949F);
            this.xrLabel_阳虚质指导1.Name = "xrLabel_阳虚质指导1";
            this.xrLabel_阳虚质指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阳虚质指导1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阳虚质指导1.StylePriority.UseBorders = false;
            // 
            // txt阳虚质指导其他
            // 
            this.txt阳虚质指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt阳虚质指导其他.LocationFloat = new DevExpress.Utils.PointFloat(7.124725F, 125.0002F);
            this.txt阳虚质指导其他.Name = "txt阳虚质指导其他";
            this.txt阳虚质指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阳虚质指导其他.SizeF = new System.Drawing.SizeF(85F, 25F);
            this.txt阳虚质指导其他.StylePriority.UseBorders = false;
            // 
            // txt阳虚质其他
            // 
            this.txt阳虚质其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt阳虚质其他.LocationFloat = new DevExpress.Utils.PointFloat(55.00037F, 105.0001F);
            this.txt阳虚质其他.Name = "txt阳虚质其他";
            this.txt阳虚质其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阳虚质其他.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt阳虚质其他.StylePriority.UseBorders = false;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(0F, 105.0001F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.xrLabel75.StylePriority.UseBorders = false;
            this.xrLabel75.Text = "6 其他";
            // 
            // xrLabel76
            // 
            this.xrLabel76.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(0.0003662109F, 85.00007F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel76.StylePriority.UseBorders = false;
            this.xrLabel76.Text = "5 穴位保健";
            // 
            // xrLabel77
            // 
            this.xrLabel77.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(0.0003662109F, 64.99999F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel77.StylePriority.UseBorders = false;
            this.xrLabel77.Text = "4 运动保健";
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(0.0003662109F, 44.99998F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel78.StylePriority.UseBorders = false;
            this.xrLabel78.Text = "3 起居调摄";
            // 
            // xrLabel79
            // 
            this.xrLabel79.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(0.0003662109F, 24.99997F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel79.StylePriority.UseBorders = false;
            this.xrLabel79.Text = "2 饮食调养";
            // 
            // xrLabel80
            // 
            this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999956F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel80.StylePriority.UseBorders = false;
            this.xrLabel80.Text = "1 情志调摄";
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_阴虚质指导6,
            this.xrLabel_阴虚质指导5,
            this.xrLabel_阴虚质指导4,
            this.xrLabel_阴虚质指导3,
            this.xrLabel_阴虚质指导2,
            this.txt阴虚质指导其他,
            this.txt阴虚质其他,
            this.xrLabel83,
            this.xrLabel84,
            this.xrLabel85,
            this.xrLabel86,
            this.xrLabel87,
            this.xrLabel88,
            this.xrLabel_阴虚质指导1});
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Weight = 1D;
            // 
            // xrLabel_阴虚质指导6
            // 
            this.xrLabel_阴虚质指导6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阴虚质指导6.CanGrow = false;
            this.xrLabel_阴虚质指导6.LocationFloat = new DevExpress.Utils.PointFloat(85F, 105F);
            this.xrLabel_阴虚质指导6.Name = "xrLabel_阴虚质指导6";
            this.xrLabel_阴虚质指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阴虚质指导6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阴虚质指导6.StylePriority.UseBorders = false;
            // 
            // xrLabel_阴虚质指导5
            // 
            this.xrLabel_阴虚质指导5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阴虚质指导5.CanGrow = false;
            this.xrLabel_阴虚质指导5.LocationFloat = new DevExpress.Utils.PointFloat(85F, 85F);
            this.xrLabel_阴虚质指导5.Name = "xrLabel_阴虚质指导5";
            this.xrLabel_阴虚质指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阴虚质指导5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阴虚质指导5.StylePriority.UseBorders = false;
            // 
            // xrLabel_阴虚质指导4
            // 
            this.xrLabel_阴虚质指导4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阴虚质指导4.CanGrow = false;
            this.xrLabel_阴虚质指导4.LocationFloat = new DevExpress.Utils.PointFloat(84.9999F, 65F);
            this.xrLabel_阴虚质指导4.Name = "xrLabel_阴虚质指导4";
            this.xrLabel_阴虚质指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阴虚质指导4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阴虚质指导4.StylePriority.UseBorders = false;
            // 
            // xrLabel_阴虚质指导3
            // 
            this.xrLabel_阴虚质指导3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阴虚质指导3.CanGrow = false;
            this.xrLabel_阴虚质指导3.LocationFloat = new DevExpress.Utils.PointFloat(85F, 45F);
            this.xrLabel_阴虚质指导3.Name = "xrLabel_阴虚质指导3";
            this.xrLabel_阴虚质指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阴虚质指导3.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阴虚质指导3.StylePriority.UseBorders = false;
            // 
            // xrLabel_阴虚质指导2
            // 
            this.xrLabel_阴虚质指导2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阴虚质指导2.CanGrow = false;
            this.xrLabel_阴虚质指导2.LocationFloat = new DevExpress.Utils.PointFloat(85F, 24.99995F);
            this.xrLabel_阴虚质指导2.Name = "xrLabel_阴虚质指导2";
            this.xrLabel_阴虚质指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阴虚质指导2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阴虚质指导2.StylePriority.UseBorders = false;
            // 
            // txt阴虚质指导其他
            // 
            this.txt阴虚质指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt阴虚质指导其他.LocationFloat = new DevExpress.Utils.PointFloat(5.124756F, 125.0003F);
            this.txt阴虚质指导其他.Name = "txt阴虚质指导其他";
            this.txt阴虚质指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阴虚质指导其他.SizeF = new System.Drawing.SizeF(85F, 25F);
            this.txt阴虚质指导其他.StylePriority.UseBorders = false;
            // 
            // txt阴虚质其他
            // 
            this.txt阴虚质其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt阴虚质其他.LocationFloat = new DevExpress.Utils.PointFloat(55F, 105.0001F);
            this.txt阴虚质其他.Name = "txt阴虚质其他";
            this.txt阴虚质其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阴虚质其他.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt阴虚质其他.StylePriority.UseBorders = false;
            // 
            // xrLabel83
            // 
            this.xrLabel83.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel83.LocationFloat = new DevExpress.Utils.PointFloat(0F, 105.0001F);
            this.xrLabel83.Name = "xrLabel83";
            this.xrLabel83.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel83.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.xrLabel83.StylePriority.UseBorders = false;
            this.xrLabel83.Text = "6 其他";
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.00007F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.Text = "5 穴位保健";
            // 
            // xrLabel85
            // 
            this.xrLabel85.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel85.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64.99999F);
            this.xrLabel85.Name = "xrLabel85";
            this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel85.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel85.StylePriority.UseBorders = false;
            this.xrLabel85.Text = "4 运动保健";
            // 
            // xrLabel86
            // 
            this.xrLabel86.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(0F, 44.99998F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(85F, 20F);
            this.xrLabel86.StylePriority.UseBorders = false;
            this.xrLabel86.Text = "3 起居调摄";
            // 
            // xrLabel87
            // 
            this.xrLabel87.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(0F, 24.99997F);
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel87.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel87.StylePriority.UseBorders = false;
            this.xrLabel87.Text = "2 饮食调养";
            // 
            // xrLabel88
            // 
            this.xrLabel88.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.999956F);
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel88.SizeF = new System.Drawing.SizeF(84.99995F, 20F);
            this.xrLabel88.StylePriority.UseBorders = false;
            this.xrLabel88.Text = "1 情志调摄";
            // 
            // xrLabel_阴虚质指导1
            // 
            this.xrLabel_阴虚质指导1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阴虚质指导1.CanGrow = false;
            this.xrLabel_阴虚质指导1.LocationFloat = new DevExpress.Utils.PointFloat(85F, 4.999949F);
            this.xrLabel_阴虚质指导1.Name = "xrLabel_阴虚质指导1";
            this.xrLabel_阴虚质指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阴虚质指导1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.xrLabel_阴虚质指导1.StylePriority.UseBorders = false;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(0F, 607.0004F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(180F, 155F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.Text = "中医药保健指导";
            // 
            // xrTable30
            // 
            this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable30.LocationFloat = new DevExpress.Utils.PointFloat(810.0005F, 527.0004F);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrTable30.SizeF = new System.Drawing.SizeF(315F, 80F);
            this.xrTable30.StylePriority.UseBorders = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell88,
            this.xrTableCell89,
            this.xrTableCell90});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_气郁质辨识_倾向是,
            this.xrLabel49,
            this.xrLabel50,
            this.xrLabel29,
            this.txt气郁质得分,
            this.xrLabel_气郁质辨识_是});
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 1D;
            // 
            // xrLabel_气郁质辨识_倾向是
            // 
            this.xrLabel_气郁质辨识_倾向是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气郁质辨识_倾向是.CanGrow = false;
            this.xrLabel_气郁质辨识_倾向是.LocationFloat = new DevExpress.Utils.PointFloat(84.99905F, 51.99997F);
            this.xrLabel_气郁质辨识_倾向是.Name = "xrLabel_气郁质辨识_倾向是";
            this.xrLabel_气郁质辨识_倾向是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气郁质辨识_倾向是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_气郁质辨识_倾向是.StylePriority.UseBorders = false;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(0F, 28.99998F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.Text = "2 是";
            // 
            // xrLabel50
            // 
            this.xrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(0F, 52.00002F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(75F, 23F);
            this.xrLabel50.StylePriority.UseBorders = false;
            this.xrLabel50.Text = "3 倾向是";
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(0.0004272461F, 5.49984F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.Text = " 1 得分";
            // 
            // txt气郁质得分
            // 
            this.txt气郁质得分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt气郁质得分.LocationFloat = new DevExpress.Utils.PointFloat(55.00043F, 5.499808F);
            this.txt气郁质得分.Name = "txt气郁质得分";
            this.txt气郁质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt气郁质得分.SizeF = new System.Drawing.SizeF(36.75055F, 23F);
            this.txt气郁质得分.StylePriority.UseBorders = false;
            this.txt气郁质得分.StylePriority.UseTextAlignment = false;
            this.txt气郁质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_气郁质辨识_是
            // 
            this.xrLabel_气郁质辨识_是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气郁质辨识_是.CanGrow = false;
            this.xrLabel_气郁质辨识_是.LocationFloat = new DevExpress.Utils.PointFloat(84.99905F, 28.99999F);
            this.xrLabel_气郁质辨识_是.Name = "xrLabel_气郁质辨识_是";
            this.xrLabel_气郁质辨识_是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气郁质辨识_是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_气郁质辨识_是.StylePriority.UseBorders = false;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_特禀质辨识_倾向是,
            this.xrLabel51,
            this.xrLabel52,
            this.xrLabel31,
            this.txt特禀质得分,
            this.xrLabel_特禀质辨识_是});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 1D;
            // 
            // xrLabel_特禀质辨识_倾向是
            // 
            this.xrLabel_特禀质辨识_倾向是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_特禀质辨识_倾向是.LocationFloat = new DevExpress.Utils.PointFloat(84.03589F, 52.00002F);
            this.xrLabel_特禀质辨识_倾向是.Name = "xrLabel_特禀质辨识_倾向是";
            this.xrLabel_特禀质辨识_倾向是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_特禀质辨识_倾向是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_特禀质辨识_倾向是.StylePriority.UseBorders = false;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(0F, 28.99998F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.Text = "2 是";
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(0.0004272461F, 52.00002F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(75F, 23F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.Text = "3 倾向是";
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.500062F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel31.StylePriority.UseBorders = false;
            this.xrLabel31.Text = " 1 得分";
            // 
            // txt特禀质得分
            // 
            this.txt特禀质得分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt特禀质得分.LocationFloat = new DevExpress.Utils.PointFloat(55.00043F, 5.49995F);
            this.txt特禀质得分.Name = "txt特禀质得分";
            this.txt特禀质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt特禀质得分.SizeF = new System.Drawing.SizeF(36.75055F, 23F);
            this.txt特禀质得分.StylePriority.UseBorders = false;
            this.txt特禀质得分.StylePriority.UseTextAlignment = false;
            this.txt特禀质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_特禀质辨识_是
            // 
            this.xrLabel_特禀质辨识_是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_特禀质辨识_是.LocationFloat = new DevExpress.Utils.PointFloat(84.0359F, 28.99999F);
            this.xrLabel_特禀质辨识_是.Name = "xrLabel_特禀质辨识_是";
            this.xrLabel_特禀质辨识_是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_特禀质辨识_是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_特禀质辨识_是.StylePriority.UseBorders = false;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel53,
            this.xrLabel54,
            this.xrLabel35,
            this.txt平和质得分,
            this.xrLabel_平和质辨识_是,
            this.xrLabel_平和质辨识_倾向是});
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 1D;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(0F, 28.99998F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.xrLabel53.StylePriority.UseBorders = false;
            this.xrLabel53.Text = "2 是";
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(0F, 52.00002F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(75F, 23F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.Text = "3 倾向是";
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(0.0004272461F, 5.499808F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.Text = " 1 得分";
            // 
            // txt平和质得分
            // 
            this.txt平和质得分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt平和质得分.LocationFloat = new DevExpress.Utils.PointFloat(55.00049F, 5.49995F);
            this.txt平和质得分.Name = "txt平和质得分";
            this.txt平和质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt平和质得分.SizeF = new System.Drawing.SizeF(36.75055F, 23F);
            this.txt平和质得分.StylePriority.UseBorders = false;
            this.txt平和质得分.StylePriority.UseTextAlignment = false;
            this.txt平和质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_平和质辨识_是
            // 
            this.xrLabel_平和质辨识_是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_平和质辨识_是.CanGrow = false;
            this.xrLabel_平和质辨识_是.LocationFloat = new DevExpress.Utils.PointFloat(84.99896F, 28.99999F);
            this.xrLabel_平和质辨识_是.Name = "xrLabel_平和质辨识_是";
            this.xrLabel_平和质辨识_是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_平和质辨识_是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_平和质辨识_是.StylePriority.UseBorders = false;
            // 
            // xrLabel_平和质辨识_倾向是
            // 
            this.xrLabel_平和质辨识_倾向是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_平和质辨识_倾向是.CanGrow = false;
            this.xrLabel_平和质辨识_倾向是.LocationFloat = new DevExpress.Utils.PointFloat(84.99896F, 51.99997F);
            this.xrLabel_平和质辨识_倾向是.Name = "xrLabel_平和质辨识_倾向是";
            this.xrLabel_平和质辨识_倾向是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_平和质辨识_倾向是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_平和质辨识_倾向是.StylePriority.UseBorders = false;
            // 
            // xrTable29
            // 
            this.xrTable29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable29.LocationFloat = new DevExpress.Utils.PointFloat(494.9996F, 527.0004F);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTable29.SizeF = new System.Drawing.SizeF(315F, 80F);
            this.xrTable29.StylePriority.UseBorders = false;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell87});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_痰湿质辨识_倾向是,
            this.xrLabel_痰湿质辨识_是,
            this.xrLabel38,
            this.xrLabel41,
            this.xrLabel42,
            this.txt痰湿质得分});
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 1D;
            // 
            // xrLabel_痰湿质辨识_倾向是
            // 
            this.xrLabel_痰湿质辨识_倾向是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_痰湿质辨识_倾向是.CanGrow = false;
            this.xrLabel_痰湿质辨识_倾向是.LocationFloat = new DevExpress.Utils.PointFloat(85.00072F, 52.0001F);
            this.xrLabel_痰湿质辨识_倾向是.Name = "xrLabel_痰湿质辨识_倾向是";
            this.xrLabel_痰湿质辨识_倾向是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_痰湿质辨识_倾向是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_痰湿质辨识_倾向是.StylePriority.UseBorders = false;
            // 
            // xrLabel_痰湿质辨识_是
            // 
            this.xrLabel_痰湿质辨识_是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_痰湿质辨识_是.CanGrow = false;
            this.xrLabel_痰湿质辨识_是.LocationFloat = new DevExpress.Utils.PointFloat(85.00011F, 29.00008F);
            this.xrLabel_痰湿质辨识_是.Name = "xrLabel_痰湿质辨识_是";
            this.xrLabel_痰湿质辨识_是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_痰湿质辨识_是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_痰湿质辨识_是.StylePriority.UseBorders = false;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(0.0005187988F, 28.50008F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.Text = "2 是";
            // 
            // xrLabel41
            // 
            this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(9.155273E-05F, 51.5001F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(75F, 23F);
            this.xrLabel41.StylePriority.UseBorders = false;
            this.xrLabel41.Text = "3 倾向是";
            // 
            // xrLabel42
            // 
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(9.155273E-05F, 5.500031F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel42.StylePriority.UseBorders = false;
            this.xrLabel42.Text = " 1 得分";
            // 
            // txt痰湿质得分
            // 
            this.txt痰湿质得分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt痰湿质得分.LocationFloat = new DevExpress.Utils.PointFloat(55.00009F, 5.499967F);
            this.txt痰湿质得分.Name = "txt痰湿质得分";
            this.txt痰湿质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt痰湿质得分.SizeF = new System.Drawing.SizeF(36.75055F, 23F);
            this.txt痰湿质得分.StylePriority.UseBorders = false;
            this.txt痰湿质得分.StylePriority.UseTextAlignment = false;
            this.txt痰湿质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_湿热质辨识_倾向是,
            this.xrLabel_湿热质辨识_是,
            this.xrLabel43,
            this.xrLabel45,
            this.xrLabel44,
            this.txt湿热质得分});
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 1D;
            // 
            // xrLabel_湿热质辨识_倾向是
            // 
            this.xrLabel_湿热质辨识_倾向是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_湿热质辨识_倾向是.CanGrow = false;
            this.xrLabel_湿热质辨识_倾向是.LocationFloat = new DevExpress.Utils.PointFloat(84.99989F, 52.00014F);
            this.xrLabel_湿热质辨识_倾向是.Name = "xrLabel_湿热质辨识_倾向是";
            this.xrLabel_湿热质辨识_倾向是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_湿热质辨识_倾向是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_湿热质辨识_倾向是.StylePriority.UseBorders = false;
            // 
            // xrLabel_湿热质辨识_是
            // 
            this.xrLabel_湿热质辨识_是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_湿热质辨识_是.CanGrow = false;
            this.xrLabel_湿热质辨识_是.LocationFloat = new DevExpress.Utils.PointFloat(85.0006F, 29.00008F);
            this.xrLabel_湿热质辨识_是.Name = "xrLabel_湿热质辨识_是";
            this.xrLabel_湿热质辨识_是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_湿热质辨识_是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_湿热质辨识_是.StylePriority.UseBorders = false;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(0.0006408691F, 28.50018F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.Text = "2 是";
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(0.0007019043F, 51.50019F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(75F, 23F);
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.Text = "3 倾向是";
            // 
            // xrLabel44
            // 
            this.xrLabel44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(0.0006408691F, 5.500011F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel44.StylePriority.UseBorders = false;
            this.xrLabel44.Text = " 1 得分";
            // 
            // txt湿热质得分
            // 
            this.txt湿热质得分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt湿热质得分.LocationFloat = new DevExpress.Utils.PointFloat(54.99997F, 5.49995F);
            this.txt湿热质得分.Name = "txt湿热质得分";
            this.txt湿热质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt湿热质得分.SizeF = new System.Drawing.SizeF(36.75055F, 23F);
            this.txt湿热质得分.StylePriority.UseBorders = false;
            this.txt湿热质得分.StylePriority.UseTextAlignment = false;
            this.txt湿热质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血瘀质辨识_倾向是,
            this.xrLabel_血瘀质辨识_是,
            this.xrLabel46,
            this.xrLabel47,
            this.xrLabel48,
            this.txt血瘀质得分});
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Weight = 1D;
            // 
            // xrLabel_血瘀质辨识_倾向是
            // 
            this.xrLabel_血瘀质辨识_倾向是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_血瘀质辨识_倾向是.CanGrow = false;
            this.xrLabel_血瘀质辨识_倾向是.LocationFloat = new DevExpress.Utils.PointFloat(85.00085F, 52.0001F);
            this.xrLabel_血瘀质辨识_倾向是.Name = "xrLabel_血瘀质辨识_倾向是";
            this.xrLabel_血瘀质辨识_倾向是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血瘀质辨识_倾向是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_血瘀质辨识_倾向是.StylePriority.UseBorders = false;
            // 
            // xrLabel_血瘀质辨识_是
            // 
            this.xrLabel_血瘀质辨识_是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_血瘀质辨识_是.CanGrow = false;
            this.xrLabel_血瘀质辨识_是.LocationFloat = new DevExpress.Utils.PointFloat(85.00085F, 29.00012F);
            this.xrLabel_血瘀质辨识_是.Name = "xrLabel_血瘀质辨识_是";
            this.xrLabel_血瘀质辨识_是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血瘀质辨识_是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_血瘀质辨识_是.StylePriority.UseBorders = false;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(0.000579834F, 28.99998F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.xrLabel46.StylePriority.UseBorders = false;
            this.xrLabel46.Text = "2 是";
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(0.0006408691F, 52.00002F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(75F, 23F);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.Text = "3 倾向是";
            // 
            // xrLabel48
            // 
            this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(0.000579834F, 5.500011F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel48.StylePriority.UseBorders = false;
            this.xrLabel48.Text = " 1 得分";
            // 
            // txt血瘀质得分
            // 
            this.txt血瘀质得分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血瘀质得分.LocationFloat = new DevExpress.Utils.PointFloat(55.00058F, 5.49995F);
            this.txt血瘀质得分.Name = "txt血瘀质得分";
            this.txt血瘀质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血瘀质得分.SizeF = new System.Drawing.SizeF(36.75055F, 23F);
            this.txt血瘀质得分.StylePriority.UseBorders = false;
            this.txt血瘀质得分.StylePriority.UseTextAlignment = false;
            this.txt血瘀质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable26
            // 
            this.xrTable26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable26.LocationFloat = new DevExpress.Utils.PointFloat(179.9997F, 527.0004F);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable26.SizeF = new System.Drawing.SizeF(315F, 80F);
            this.xrTable26.StylePriority.UseBorders = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_气虚质辨识_是,
            this.xrLabel_气虚质辨识_倾向是,
            this.xrLabel28,
            this.xrLabel26,
            this.xrLabel25,
            this.txt气虚质得分});
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 1D;
            // 
            // xrLabel_气虚质辨识_是
            // 
            this.xrLabel_气虚质辨识_是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气虚质辨识_是.CanGrow = false;
            this.xrLabel_气虚质辨识_是.LocationFloat = new DevExpress.Utils.PointFloat(85.00037F, 29.00012F);
            this.xrLabel_气虚质辨识_是.Name = "xrLabel_气虚质辨识_是";
            this.xrLabel_气虚质辨识_是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气虚质辨识_是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_气虚质辨识_是.StylePriority.UseBorders = false;
            // 
            // xrLabel_气虚质辨识_倾向是
            // 
            this.xrLabel_气虚质辨识_倾向是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_气虚质辨识_倾向是.CanGrow = false;
            this.xrLabel_气虚质辨识_倾向是.LocationFloat = new DevExpress.Utils.PointFloat(85.00037F, 52.00006F);
            this.xrLabel_气虚质辨识_倾向是.Name = "xrLabel_气虚质辨识_倾向是";
            this.xrLabel_气虚质辨识_倾向是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_气虚质辨识_倾向是.SizeF = new System.Drawing.SizeF(20F, 23.00006F);
            this.xrLabel_气虚质辨识_倾向是.StylePriority.UseBorders = false;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(0F, 52.00012F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(73.68713F, 23F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.Text = "3 倾向是";
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(0F, 29.00009F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.Text = "2 是";
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.500061F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(55.00014F, 23F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.Text = " 1 得分";
            // 
            // txt气虚质得分
            // 
            this.txt气虚质得分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt气虚质得分.LocationFloat = new DevExpress.Utils.PointFloat(55.00015F, 5.500158F);
            this.txt气虚质得分.Name = "txt气虚质得分";
            this.txt气虚质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt气虚质得分.SizeF = new System.Drawing.SizeF(35.43765F, 23F);
            this.txt气虚质得分.StylePriority.UseBorders = false;
            this.txt气虚质得分.StylePriority.UseTextAlignment = false;
            this.txt气虚质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_阳虚质辨识_倾向是,
            this.xrLabel_阳虚质辨识_是,
            this.xrLabel30,
            this.txt阳虚质得分,
            this.xrLabel32,
            this.xrLabel33});
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Weight = 1D;
            // 
            // xrLabel_阳虚质辨识_倾向是
            // 
            this.xrLabel_阳虚质辨识_倾向是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阳虚质辨识_倾向是.CanGrow = false;
            this.xrLabel_阳虚质辨识_倾向是.LocationFloat = new DevExpress.Utils.PointFloat(85.00036F, 52.00012F);
            this.xrLabel_阳虚质辨识_倾向是.Name = "xrLabel_阳虚质辨识_倾向是";
            this.xrLabel_阳虚质辨识_倾向是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阳虚质辨识_倾向是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_阳虚质辨识_倾向是.StylePriority.UseBorders = false;
            // 
            // xrLabel_阳虚质辨识_是
            // 
            this.xrLabel_阳虚质辨识_是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阳虚质辨识_是.CanGrow = false;
            this.xrLabel_阳虚质辨识_是.LocationFloat = new DevExpress.Utils.PointFloat(85.00036F, 29.00009F);
            this.xrLabel_阳虚质辨识_是.Name = "xrLabel_阳虚质辨识_是";
            this.xrLabel_阳虚质辨识_是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阳虚质辨识_是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_阳虚质辨识_是.StylePriority.UseBorders = false;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(0.0003662109F, 5.500031F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.Text = " 1 得分";
            // 
            // txt阳虚质得分
            // 
            this.txt阳虚质得分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt阳虚质得分.LocationFloat = new DevExpress.Utils.PointFloat(55.00037F, 5.49995F);
            this.txt阳虚质得分.Name = "txt阳虚质得分";
            this.txt阳虚质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阳虚质得分.SizeF = new System.Drawing.SizeF(36.75055F, 23F);
            this.txt阳虚质得分.StylePriority.UseBorders = false;
            this.txt阳虚质得分.StylePriority.UseTextAlignment = false;
            this.txt阳虚质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(0.0003662109F, 28.50002F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.xrLabel32.StylePriority.UseBorders = false;
            this.xrLabel32.Text = "2 是";
            // 
            // xrLabel33
            // 
            this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(0.0003662109F, 51.50006F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(75F, 23F);
            this.xrLabel33.StylePriority.UseBorders = false;
            this.xrLabel33.Text = "3 倾向是";
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_阴虚质辨识_倾向是,
            this.xrLabel_阴虚质辨识_是,
            this.xrLabel34,
            this.txt阴虚质得分,
            this.xrLabel36,
            this.xrLabel37});
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 1D;
            // 
            // xrLabel_阴虚质辨识_倾向是
            // 
            this.xrLabel_阴虚质辨识_倾向是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阴虚质辨识_倾向是.CanGrow = false;
            this.xrLabel_阴虚质辨识_倾向是.LocationFloat = new DevExpress.Utils.PointFloat(84.9999F, 52.00012F);
            this.xrLabel_阴虚质辨识_倾向是.Name = "xrLabel_阴虚质辨识_倾向是";
            this.xrLabel_阴虚质辨识_倾向是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阴虚质辨识_倾向是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_阴虚质辨识_倾向是.StylePriority.UseBorders = false;
            // 
            // xrLabel_阴虚质辨识_是
            // 
            this.xrLabel_阴虚质辨识_是.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel_阴虚质辨识_是.CanGrow = false;
            this.xrLabel_阴虚质辨识_是.LocationFloat = new DevExpress.Utils.PointFloat(85F, 29.00009F);
            this.xrLabel_阴虚质辨识_是.Name = "xrLabel_阴虚质辨识_是";
            this.xrLabel_阴虚质辨识_是.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阴虚质辨识_是.SizeF = new System.Drawing.SizeF(20F, 23F);
            this.xrLabel_阴虚质辨识_是.StylePriority.UseBorders = false;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(0.08432007F, 5.500011F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.Text = " 1 得分";
            // 
            // txt阴虚质得分
            // 
            this.txt阴虚质得分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt阴虚质得分.LocationFloat = new DevExpress.Utils.PointFloat(55.08432F, 5.500061F);
            this.txt阴虚质得分.Name = "txt阴虚质得分";
            this.txt阴虚质得分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阴虚质得分.SizeF = new System.Drawing.SizeF(36.75055F, 23F);
            this.txt阴虚质得分.StylePriority.UseBorders = false;
            this.txt阴虚质得分.StylePriority.UseTextAlignment = false;
            this.txt阴虚质得分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(0.0003662109F, 28.5F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.Text = "2 是";
            // 
            // xrLabel37
            // 
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(0.0004272461F, 51.50005F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(75F, 23F);
            this.xrLabel37.StylePriority.UseBorders = false;
            this.xrLabel37.Text = "3 倾向是";
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 527.0004F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(180F, 80F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.Text = "体质辨识";
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 497.0002F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(180F, 30F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "  体质类型";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable25
            // 
            this.xrTable25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable25.LocationFloat = new DevExpress.Utils.PointFloat(809.9996F, 497.0002F);
            this.xrTable25.Name = "xrTable25";
            this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTable25.SizeF = new System.Drawing.SizeF(315.0001F, 30F);
            this.xrTable25.StylePriority.UseBorders = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell75});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Text = "气郁质";
            this.xrTableCell73.Weight = 0.875205034271782D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Text = "特禀质";
            this.xrTableCell74.Weight = 0.87521222939962273D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Text = "平和质";
            this.xrTableCell75.Weight = 0.87519849327074328D;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(494.9996F, 497.0004F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "痰湿质";
            this.xrTableCell4.Weight = 0.87314639416983109D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "湿热质";
            this.xrTableCell5.Weight = 0.87314636673380508D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "血瘀质";
            this.xrTableCell6.Weight = 0.87314635010093256D;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(179.9997F, 497.0002F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "气虚质";
            this.xrTableCell1.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "阳虚质";
            this.xrTableCell2.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "阴虚质";
            this.xrTableCell3.Weight = 1D;
            // 
            // xrTable35
            // 
            this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable35.LocationFloat = new DevExpress.Utils.PointFloat(810.0005F, 467F);
            this.xrTable35.Name = "xrTable35";
            this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTable35.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable35.StylePriority.UseBorders = false;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_33_3,
            this.xrTable_特征_33_4,
            this.xrTable_特征_33_5});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTable_特征_33_3
            // 
            this.xrTable_特征_33_3.Name = "xrTable_特征_33_3";
            this.xrTable_特征_33_3.Weight = 1D;
            // 
            // xrTable_特征_33_4
            // 
            this.xrTable_特征_33_4.Name = "xrTable_特征_33_4";
            this.xrTable_特征_33_4.Weight = 1D;
            // 
            // xrTable_特征_33_5
            // 
            this.xrTable_特征_33_5.Name = "xrTable_特征_33_5";
            this.xrTable_特征_33_5.Weight = 1D;
            // 
            // xrTable33
            // 
            this.xrTable33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable33.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 467.0001F);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrTable33.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable33.StylePriority.UseBorders = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.xrTable_特征_33_1,
            this.xrTable_特征_33_2});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel40});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 2.2222222222222223D;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(10.08396F, 2F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(435F, 23F);
            this.xrLabel40.StylePriority.UseBorders = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "（33）您舌下静脉瘀紫或增粗吗？（可由调查员辅助观察后填写）";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_33_1
            // 
            this.xrTable_特征_33_1.Name = "xrTable_特征_33_1";
            this.xrTable_特征_33_1.Weight = 0.3888891149450231D;
            // 
            // xrTable_特征_33_2
            // 
            this.xrTable_特征_33_2.Name = "xrTable_特征_33_2";
            this.xrTable_特征_33_2.Weight = 0.38888866283275458D;
            // 
            // xrTable32
            // 
            this.xrTable32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable32.LocationFloat = new DevExpress.Utils.PointFloat(810.0005F, 436.9999F);
            this.xrTable32.Name = "xrTable32";
            this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.xrTable32.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable32.StylePriority.UseBorders = false;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_32_3,
            this.xrTable_特征_32_4,
            this.xrTable_特征_32_5});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTable_特征_32_3
            // 
            this.xrTable_特征_32_3.Name = "xrTable_特征_32_3";
            this.xrTable_特征_32_3.Weight = 1D;
            // 
            // xrTable_特征_32_4
            // 
            this.xrTable_特征_32_4.Name = "xrTable_特征_32_4";
            this.xrTable_特征_32_4.Weight = 1D;
            // 
            // xrTable_特征_32_5
            // 
            this.xrTable_特征_32_5.Name = "xrTable_特征_32_5";
            this.xrTable_特征_32_5.Weight = 1D;
            // 
            // xrTable31
            // 
            this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable31.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 437.0001F);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable31.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable31.StylePriority.UseBorders = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell91,
            this.xrTable_特征_32_1,
            this.xrTable_特征_32_2});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel39});
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Weight = 2.2222222222222223D;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(10.08396F, 2.000061F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(574.478F, 23F);
            this.xrLabel39.StylePriority.UseBorders = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "（32）您舌苔厚腻或有舌苔厚厚的感觉吗？（如果自我感觉不清楚可由调查员观察后填写）";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_32_1
            // 
            this.xrTable_特征_32_1.Name = "xrTable_特征_32_1";
            this.xrTable_特征_32_1.Weight = 0.38888888888888878D;
            // 
            // xrTable_特征_32_2
            // 
            this.xrTable_特征_32_2.Name = "xrTable_特征_32_2";
            this.xrTable_特征_32_2.Weight = 0.3888888888888889D;
            // 
            // xrTable28
            // 
            this.xrTable28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable28.LocationFloat = new DevExpress.Utils.PointFloat(810F, 406.9999F);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable28.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable28.StylePriority.UseBorders = false;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_31_3,
            this.xrTable_特征_31_4,
            this.xrTable_特征_31_5});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTable_特征_31_3
            // 
            this.xrTable_特征_31_3.Name = "xrTable_特征_31_3";
            this.xrTable_特征_31_3.Weight = 1D;
            // 
            // xrTable_特征_31_4
            // 
            this.xrTable_特征_31_4.Name = "xrTable_特征_31_4";
            this.xrTable_特征_31_4.Weight = 1D;
            // 
            // xrTable_特征_31_5
            // 
            this.xrTable_特征_31_5.Name = "xrTable_特征_31_5";
            this.xrTable_特征_31_5.Weight = 1D;
            // 
            // xrTable27
            // 
            this.xrTable27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable27.LocationFloat = new DevExpress.Utils.PointFloat(0F, 407.0001F);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTable27.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable27.StylePriority.UseBorders = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTable_特征_31_1,
            this.xrTable_特征_31_2});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel27});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Text = "xrTableCell79";
            this.xrTableCell79.Weight = 2.2222222222222223D;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(10.08396F, 7F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(189.9161F, 23F);
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "（31）您容易大便干燥吗？";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_31_1
            // 
            this.xrTable_特征_31_1.Name = "xrTable_特征_31_1";
            this.xrTable_特征_31_1.Weight = 0.38888888888888884D;
            // 
            // xrTable_特征_31_2
            // 
            this.xrTable_特征_31_2.Name = "xrTable_特征_31_2";
            this.xrTable_特征_31_2.Weight = 0.38888888888888884D;
            // 
            // xrTable24
            // 
            this.xrTable24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable24.LocationFloat = new DevExpress.Utils.PointFloat(810.0002F, 376.9999F);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable24.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable24.StylePriority.UseBorders = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_30_3,
            this.xrTable_特征_30_4,
            this.xrTable_特征_30_5});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTable_特征_30_3
            // 
            this.xrTable_特征_30_3.Name = "xrTable_特征_30_3";
            this.xrTable_特征_30_3.Weight = 1D;
            // 
            // xrTable_特征_30_4
            // 
            this.xrTable_特征_30_4.Name = "xrTable_特征_30_4";
            this.xrTable_特征_30_4.Weight = 1D;
            // 
            // xrTable_特征_30_5
            // 
            this.xrTable_特征_30_5.Name = "xrTable_特征_30_5";
            this.xrTable_特征_30_5.Weight = 1D;
            // 
            // xrTable23
            // 
            this.xrTable23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable23.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 377.0001F);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable23.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable23.StylePriority.UseBorders = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTable_特征_30_1,
            this.xrTable_特征_30_2});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel24});
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Weight = 2.2222222222222223D;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(10F, 6.999939F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(530F, 23.00006F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "（30）您有大便黏滞不爽、解不尽的感觉吗？（大便容易粘在马桶或便坑壁上）";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_30_1
            // 
            this.xrTable_特征_30_1.Name = "xrTable_特征_30_1";
            this.xrTable_特征_30_1.Weight = 0.388889001916956D;
            // 
            // xrTable_特征_30_2
            // 
            this.xrTable_特征_30_2.Name = "xrTable_特征_30_2";
            this.xrTable_特征_30_2.Weight = 0.38888877586082182D;
            // 
            // xrTable22
            // 
            this.xrTable22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable22.LocationFloat = new DevExpress.Utils.PointFloat(809.9997F, 326.9999F);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.xrTable22.SizeF = new System.Drawing.SizeF(315F, 50F);
            this.xrTable22.StylePriority.UseBorders = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_29_3,
            this.xrTable_特征_29_4,
            this.xrTable_特征_29_5});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTable_特征_29_3
            // 
            this.xrTable_特征_29_3.Name = "xrTable_特征_29_3";
            this.xrTable_特征_29_3.Weight = 1D;
            // 
            // xrTable_特征_29_4
            // 
            this.xrTable_特征_29_4.Name = "xrTable_特征_29_4";
            this.xrTable_特征_29_4.Weight = 1D;
            // 
            // xrTable_特征_29_5
            // 
            this.xrTable_特征_29_5.Name = "xrTable_特征_29_5";
            this.xrTable_特征_29_5.Weight = 1D;
            // 
            // xrTable21
            // 
            this.xrTable21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 327.0001F);
            this.xrTable21.Name = "xrTable21";
            this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable21.SizeF = new System.Drawing.SizeF(810F, 50F);
            this.xrTable21.StylePriority.UseBorders = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTable_特征_29_1,
            this.xrTable_特征_29_2});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.xrLabel23});
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 1.8978049550813063D;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(10.08396F, 23.00003F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(362.4164F, 23F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "（指不喜欢吃凉的食物，或吃了凉的食物后会不舒服）";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(10F, 0F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(409.2916F, 23F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "（29）您吃(喝)凉的东西会感到不舒服或怕吃(喝)凉的东西吗？";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_29_1
            // 
            this.xrTable_特征_29_1.Name = "xrTable_特征_29_1";
            this.xrTable_特征_29_1.Weight = 0.3321158899549671D;
            // 
            // xrTable_特征_29_2
            // 
            this.xrTable_特征_29_2.Name = "xrTable_特征_29_2";
            this.xrTable_特征_29_2.Weight = 0.33211596594462928D;
            // 
            // xrTable19
            // 
            this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable19.LocationFloat = new DevExpress.Utils.PointFloat(810.0003F, 276.9999F);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable19.SizeF = new System.Drawing.SizeF(314.9999F, 50F);
            this.xrTable19.StylePriority.UseBorders = false;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell57});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_28_3,
            this.xrLabel6});
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.99999941871279763D;
            // 
            // xrTable_特征_28_3
            // 
            this.xrTable_特征_28_3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_28_3.LocationFloat = new DevExpress.Utils.PointFloat(27.70834F, 1.75F);
            this.xrTable_特征_28_3.Name = "xrTable_特征_28_3";
            this.xrTable_特征_28_3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_28_3.SizeF = new System.Drawing.SizeF(50F, 18F);
            this.xrTable_特征_28_3.StylePriority.UseBorders = false;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.Font = new System.Drawing.Font("宋体", 8F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 19.75003F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(105F, 28.49997F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "(腹围86-90cm，        2.56-2.7尺)";
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_28_4,
            this.xrLabel8});
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Weight = 1D;
            // 
            // xrTable_特征_28_4
            // 
            this.xrTable_特征_28_4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_28_4.LocationFloat = new DevExpress.Utils.PointFloat(27.70837F, 1.75F);
            this.xrTable_特征_28_4.Name = "xrTable_特征_28_4";
            this.xrTable_特征_28_4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_28_4.SizeF = new System.Drawing.SizeF(50F, 18F);
            this.xrTable_特征_28_4.StylePriority.UseBorders = false;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel8.Font = new System.Drawing.Font("宋体", 8F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 19.75003F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(104.0362F, 30.24994F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.Text = "(腹围91-105cm，        2.71-3.15尺)";
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_28_5,
            this.xrLabel10});
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Weight = 1D;
            // 
            // xrTable_特征_28_5
            // 
            this.xrTable_特征_28_5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_28_5.LocationFloat = new DevExpress.Utils.PointFloat(27.70837F, 1.75F);
            this.xrTable_特征_28_5.Name = "xrTable_特征_28_5";
            this.xrTable_特征_28_5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_28_5.SizeF = new System.Drawing.SizeF(50F, 18F);
            this.xrTable_特征_28_5.StylePriority.UseBorders = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel10.Font = new System.Drawing.Font("宋体", 8F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 19.75003F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(105.0001F, 28.49997F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.Text = "(腹围>105cm，            或3.5尺)";
            // 
            // xrTable20
            // 
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.LocationFloat = new DevExpress.Utils.PointFloat(0.0002642473F, 277.0001F);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable20.SizeF = new System.Drawing.SizeF(810F, 50F);
            this.xrTable20.StylePriority.UseBorders = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel21});
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell58.Weight = 2.222222227136486D;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(10.08369F, 9.999969F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(290F, 23F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "（28）您腹部肥大吗？（指腹部脂肪肥厚）";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_28_1,
            this.xrLabel2});
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_28_1
            // 
            this.xrTable_特征_28_1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_28_1.LocationFloat = new DevExpress.Utils.PointFloat(27.70837F, 0F);
            this.xrTable_特征_28_1.Name = "xrTable_特征_28_1";
            this.xrTable_特征_28_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_28_1.SizeF = new System.Drawing.SizeF(50F, 18F);
            this.xrTable_特征_28_1.StylePriority.UseBorders = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Font = new System.Drawing.Font("宋体", 8F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 18F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(105F, 30.25F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "(腹围<80cm，              相当于2.4尺)";
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_特征_28_2,
            this.xrLabel4});
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_28_2
            // 
            this.xrTable_特征_28_2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable_特征_28_2.LocationFloat = new DevExpress.Utils.PointFloat(27.70837F, 1.75F);
            this.xrTable_特征_28_2.Name = "xrTable_特征_28_2";
            this.xrTable_特征_28_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable_特征_28_2.SizeF = new System.Drawing.SizeF(50F, 18F);
            this.xrTable_特征_28_2.StylePriority.UseBorders = false;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Font = new System.Drawing.Font("宋体", 8F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 19.75003F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(105F, 28.49997F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "(腹围80-85cm，          2.4-2.55尺)";
            // 
            // xrTable17
            // 
            this.xrTable17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 247F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable17.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable17.StylePriority.UseBorders = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTable_特征_27_1,
            this.xrTable_特征_27_2});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel20});
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell49.Weight = 2.222222227136486D;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7.000031F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(362.5004F, 23F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "（27）您感到口苦或嘴里有异味吗？（指口苦或口臭）";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_27_1
            // 
            this.xrTable_特征_27_1.Name = "xrTable_特征_27_1";
            this.xrTable_特征_27_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_27_2
            // 
            this.xrTable_特征_27_2.Name = "xrTable_特征_27_2";
            this.xrTable_特征_27_2.Weight = 0.388888886431757D;
            // 
            // xrTable18
            // 
            this.xrTable18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(809.9999F, 247F);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable18.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable18.StylePriority.UseBorders = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_27_3,
            this.xrTable_特征_27_4,
            this.xrTable_特征_27_5});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTable_特征_27_3
            // 
            this.xrTable_特征_27_3.Name = "xrTable_特征_27_3";
            this.xrTable_特征_27_3.Weight = 1D;
            // 
            // xrTable_特征_27_4
            // 
            this.xrTable_特征_27_4.Name = "xrTable_特征_27_4";
            this.xrTable_特征_27_4.Weight = 1D;
            // 
            // xrTable_特征_27_5
            // 
            this.xrTable_特征_27_5.Name = "xrTable_特征_27_5";
            this.xrTable_特征_27_5.Weight = 1D;
            // 
            // xrTable15
            // 
            this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 217F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable15.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable15.StylePriority.UseBorders = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTable_特征_26_1,
            this.xrTable_特征_26_2});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel19});
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell43.Weight = 2.222222227136486D;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(265F, 22.99997F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "（26）您感到口干咽燥、总想喝水吗？";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_26_1
            // 
            this.xrTable_特征_26_1.Name = "xrTable_特征_26_1";
            this.xrTable_特征_26_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_26_2
            // 
            this.xrTable_特征_26_2.Name = "xrTable_特征_26_2";
            this.xrTable_特征_26_2.Weight = 0.388888886431757D;
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(809.9999F, 217F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable16.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable16.StylePriority.UseBorders = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_26_3,
            this.xrTable_特征_26_4,
            this.xrTable_特征_26_5});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTable_特征_26_3
            // 
            this.xrTable_特征_26_3.Name = "xrTable_特征_26_3";
            this.xrTable_特征_26_3.Weight = 1D;
            // 
            // xrTable_特征_26_4
            // 
            this.xrTable_特征_26_4.Name = "xrTable_特征_26_4";
            this.xrTable_特征_26_4.Weight = 1D;
            // 
            // xrTable_特征_26_5
            // 
            this.xrTable_特征_26_5.Name = "xrTable_特征_26_5";
            this.xrTable_特征_26_5.Weight = 1D;
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(810.0003F, 187F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable13.SizeF = new System.Drawing.SizeF(314.9999F, 30F);
            this.xrTable13.StylePriority.UseBorders = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_25_3,
            this.xrTable_特征_25_4,
            this.xrTable_特征_25_5});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTable_特征_25_3
            // 
            this.xrTable_特征_25_3.Name = "xrTable_特征_25_3";
            this.xrTable_特征_25_3.Weight = 1D;
            // 
            // xrTable_特征_25_4
            // 
            this.xrTable_特征_25_4.Name = "xrTable_特征_25_4";
            this.xrTable_特征_25_4.Weight = 1D;
            // 
            // xrTable_特征_25_5
            // 
            this.xrTable_特征_25_5.Name = "xrTable_特征_25_5";
            this.xrTable_特征_25_5.Weight = 1D;
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(810.0002F, 157F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable11.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable11.StylePriority.UseBorders = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_24_3,
            this.xrTable_特征_24_4,
            this.xrTable_特征_24_5});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTable_特征_24_3
            // 
            this.xrTable_特征_24_3.Name = "xrTable_特征_24_3";
            this.xrTable_特征_24_3.Weight = 1D;
            // 
            // xrTable_特征_24_4
            // 
            this.xrTable_特征_24_4.Name = "xrTable_特征_24_4";
            this.xrTable_特征_24_4.Weight = 1D;
            // 
            // xrTable_特征_24_5
            // 
            this.xrTable_特征_24_5.Name = "xrTable_特征_24_5";
            this.xrTable_特征_24_5.Weight = 1D;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(810.0002F, 127F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable9.StylePriority.UseBorders = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_23_3,
            this.xrTable_特征_23_4,
            this.xrTable_特征_23_5});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTable_特征_23_3
            // 
            this.xrTable_特征_23_3.Name = "xrTable_特征_23_3";
            this.xrTable_特征_23_3.Weight = 1D;
            // 
            // xrTable_特征_23_4
            // 
            this.xrTable_特征_23_4.Name = "xrTable_特征_23_4";
            this.xrTable_特征_23_4.Weight = 1D;
            // 
            // xrTable_特征_23_5
            // 
            this.xrTable_特征_23_5.Name = "xrTable_特征_23_5";
            this.xrTable_特征_23_5.Weight = 1D;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(810.0001F, 97.00002F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable8.StylePriority.UseBorders = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_22_3,
            this.xrTable_特征_22_4,
            this.xrTable_特征_22_5});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTable_特征_22_3
            // 
            this.xrTable_特征_22_3.Name = "xrTable_特征_22_3";
            this.xrTable_特征_22_3.Weight = 1D;
            // 
            // xrTable_特征_22_4
            // 
            this.xrTable_特征_22_4.Name = "xrTable_特征_22_4";
            this.xrTable_特征_22_4.Weight = 1D;
            // 
            // xrTable_特征_22_5
            // 
            this.xrTable_特征_22_5.Name = "xrTable_特征_22_5";
            this.xrTable_特征_22_5.Weight = 1D;
            // 
            // xrTable14
            // 
            this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(0.0002642473F, 187F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable14.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable14.StylePriority.UseBorders = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTable_特征_25_1,
            this.xrTable_特征_25_2});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel18});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell40.Weight = 2.222222227136486D;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7.000031F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(216.7503F, 23F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "（25）您有皮肤湿疹、疮疖吗？";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_25_1
            // 
            this.xrTable_特征_25_1.Name = "xrTable_特征_25_1";
            this.xrTable_特征_25_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_25_2
            // 
            this.xrTable_特征_25_2.Name = "xrTable_特征_25_2";
            this.xrTable_特征_25_2.Weight = 0.388888886431757D;
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(0.0001370907F, 157F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable12.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable12.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTable_特征_24_1,
            this.xrTable_特征_24_2});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel17});
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell34.Weight = 2.222222227136486D;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(352.0836F, 22.99998F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "（24）您面色或目眶晦黯，或出现褐色斑块/斑点吗？";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_24_1
            // 
            this.xrTable_特征_24_1.Name = "xrTable_特征_24_1";
            this.xrTable_特征_24_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_24_2
            // 
            this.xrTable_特征_24_2.Name = "xrTable_特征_24_2";
            this.xrTable_特征_24_2.Weight = 0.388888886431757D;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(0.0001370907F, 127F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable10.StylePriority.UseBorders = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTable_特征_23_1,
            this.xrTable_特征_23_2});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel16});
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell28.Weight = 2.222222227136486D;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(455F, 23F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "（23）您的面部或鼻部有油腻感或者油亮发光吗？（指脸上或鼻子）";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_23_1
            // 
            this.xrTable_特征_23_1.Name = "xrTable_特征_23_1";
            this.xrTable_特征_23_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_23_2
            // 
            this.xrTable_特征_23_2.Name = "xrTable_特征_23_2";
            this.xrTable_特征_23_2.Weight = 0.388888886431757D;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(9.934108E-06F, 97.00002F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable7.StylePriority.UseBorders = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTable_特征_22_1,
            this.xrTable_特征_22_2});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel15});
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 2.222222227136486D;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(10F, 7F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(325F, 23F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "（22）您有肢体麻木或固定部位疼痛的感觉吗？";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_22_1
            // 
            this.xrTable_特征_22_1.Name = "xrTable_特征_22_1";
            this.xrTable_特征_22_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_22_2
            // 
            this.xrTable_特征_22_2.Name = "xrTable_特征_22_2";
            this.xrTable_特征_22_2.Weight = 0.388888886431757D;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(809.9999F, 67.00002F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable6.StylePriority.UseBorders = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_21_3,
            this.xrTable_特征_21_4,
            this.xrTable_特征_21_5});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTable_特征_21_3
            // 
            this.xrTable_特征_21_3.Name = "xrTable_特征_21_3";
            this.xrTable_特征_21_3.Weight = 1D;
            // 
            // xrTable_特征_21_4
            // 
            this.xrTable_特征_21_4.Name = "xrTable_特征_21_4";
            this.xrTable_特征_21_4.Weight = 1D;
            // 
            // xrTable_特征_21_5
            // 
            this.xrTable_特征_21_5.Name = "xrTable_特征_21_5";
            this.xrTable_特征_21_5.Weight = 1D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(9.934108E-06F, 67.00002F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable5.StylePriority.UseBorders = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTable_特征_21_1,
            this.xrTable_特征_21_2});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel14});
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 2.222222227136486D;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(10.00005F, 6.999969F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(190F, 23.00002F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "（21）您皮肤或口唇干吗？";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_21_1
            // 
            this.xrTable_特征_21_1.Name = "xrTable_特征_21_1";
            this.xrTable_特征_21_1.Weight = 0.388888886431757D;
            // 
            // xrTable_特征_21_2
            // 
            this.xrTable_特征_21_2.Name = "xrTable_特征_21_2";
            this.xrTable_特征_21_2.Weight = 0.388888886431757D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(810.0001F, 37.00002F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(315F, 30F);
            this.xrTable4.StylePriority.UseBorders = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTable_特征_20_3,
            this.xrTable_特征_20_4,
            this.xrTable_特征_20_5});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTable_特征_20_3
            // 
            this.xrTable_特征_20_3.Name = "xrTable_特征_20_3";
            this.xrTable_特征_20_3.Weight = 1D;
            // 
            // xrTable_特征_20_4
            // 
            this.xrTable_特征_20_4.Name = "xrTable_特征_20_4";
            this.xrTable_特征_20_4.Weight = 1D;
            // 
            // xrTable_特征_20_5
            // 
            this.xrTable_特征_20_5.Name = "xrTable_特征_20_5";
            this.xrTable_特征_20_5.Weight = 1D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(9.934108E-06F, 37F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(810F, 30F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTable_特征_20_1,
            this.xrTable_特征_20_2});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel13});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 1.3629629659770448D;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(10.08401F, 7.000033F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(548.0839F, 22.99998F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "（20）您的皮肤一抓就红，并出现抓痕吗？（指被指甲或钝物划过后皮肤的反应）";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable_特征_20_1
            // 
            this.xrTable_特征_20_1.Name = "xrTable_特征_20_1";
            this.xrTable_特征_20_1.Weight = 0.23851851701147767D;
            // 
            // xrTable_特征_20_2
            // 
            this.xrTable_特征_20_2.Name = "xrTable_特征_20_2";
            this.xrTable_特征_20_2.Weight = 0.23851851701147767D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 5F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report老年人中医药健康管理反面
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(23, 19, 5, 0);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_20_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_20_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_20_5;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_20_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_20_2;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_21_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_21_2;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_25_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_25_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_25_5;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_24_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_24_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_24_5;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_23_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_23_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_23_5;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_22_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_22_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_22_5;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_25_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_25_2;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_24_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_24_2;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_23_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_23_2;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_22_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_22_2;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_21_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_21_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_21_5;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_27_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_27_2;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_27_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_27_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_27_5;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_26_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_26_2;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_26_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_26_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_26_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_29_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_29_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_29_5;
        private DevExpress.XtraReports.UI.XRTable xrTable21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_29_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_29_2;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_30_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_30_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_30_5;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_30_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_30_2;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_31_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_31_2;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_31_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_31_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_31_5;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_32_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_32_2;
        private DevExpress.XtraReports.UI.XRTable xrTable32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_32_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_32_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_32_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_33_1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_33_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRTable xrTable35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_33_3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_33_4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_特征_33_5;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_28_3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_28_4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_28_5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_28_1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrTable_特征_28_2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRTable xrTable25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRLabel txt气虚质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel txt阳虚质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel txt阴虚质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRLabel txt气郁质得分;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRLabel txt特禀质得分;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRLabel txt平和质得分;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel txt痰湿质得分;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel txt湿热质得分;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel txt血瘀质得分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRTable xrTable34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRLabel txt阳虚质指导其他;
        private DevExpress.XtraReports.UI.XRLabel txt阳虚质其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel txt阴虚质指导其他;
        private DevExpress.XtraReports.UI.XRLabel txt阴虚质其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel83;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel xrLabel85;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel88;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRLabel txt痰湿质指导其他;
        private DevExpress.XtraReports.UI.XRLabel txt痰湿质其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel91;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRLabel txt湿热质指导其他;
        private DevExpress.XtraReports.UI.XRLabel txt湿热质其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel99;
        private DevExpress.XtraReports.UI.XRLabel xrLabel100;
        private DevExpress.XtraReports.UI.XRLabel xrLabel101;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRLabel xrLabel104;
        private DevExpress.XtraReports.UI.XRLabel txt血瘀质指导其他;
        private DevExpress.XtraReports.UI.XRLabel txt血瘀质其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel107;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRLabel xrLabel109;
        private DevExpress.XtraReports.UI.XRLabel xrLabel110;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel xrLabel112;
        private DevExpress.XtraReports.UI.XRTable xrTable37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRLabel txt气郁质指导其他;
        private DevExpress.XtraReports.UI.XRLabel txt气郁质其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel115;
        private DevExpress.XtraReports.UI.XRLabel xrLabel116;
        private DevExpress.XtraReports.UI.XRLabel xrLabel117;
        private DevExpress.XtraReports.UI.XRLabel xrLabel118;
        private DevExpress.XtraReports.UI.XRLabel xrLabel119;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRLabel txt特禀质指导其他;
        private DevExpress.XtraReports.UI.XRLabel txt特禀质其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel123;
        private DevExpress.XtraReports.UI.XRLabel xrLabel124;
        private DevExpress.XtraReports.UI.XRLabel xrLabel125;
        private DevExpress.XtraReports.UI.XRLabel xrLabel126;
        private DevExpress.XtraReports.UI.XRLabel xrLabel127;
        private DevExpress.XtraReports.UI.XRLabel xrLabel128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRLabel txt平和质指导其他;
        private DevExpress.XtraReports.UI.XRLabel txt平和质其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel131;
        private DevExpress.XtraReports.UI.XRLabel xrLabel132;
        private DevExpress.XtraReports.UI.XRLabel xrLabel133;
        private DevExpress.XtraReports.UI.XRLabel xrLabel134;
        private DevExpress.XtraReports.UI.XRLabel xrLabel135;
        private DevExpress.XtraReports.UI.XRLabel xrLabel136;
        private DevExpress.XtraReports.UI.XRLabel txt气虚质指导其他;
        private DevExpress.XtraReports.UI.XRLabel txt气虚质其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRTable xrTable39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTable xrTable38;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRLabel txt填表日期;
        private DevExpress.XtraReports.UI.XRLabel txt医生签名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气虚质指导6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气虚质指导5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气虚质指导4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气虚质指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气虚质指导1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气虚质指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阳虚质指导6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阳虚质指导5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阳虚质指导4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阳虚质指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阳虚质指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阳虚质指导1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_痰湿质指导6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_痰湿质指导5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_痰湿质指导4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_痰湿质指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_痰湿质指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_痰湿质指导1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_湿热质指导4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_湿热质指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_湿热质指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_湿热质指导1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_湿热质指导5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_湿热质指导6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血瘀质指导6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血瘀质指导5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血瘀质指导4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血瘀质指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血瘀质指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血瘀质指导1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴虚质指导6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴虚质指导5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴虚质指导4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴虚质指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴虚质指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴虚质指导1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气郁质指导1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气郁质指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气郁质指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气郁质指导4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气郁质指导6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气郁质指导5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_特禀质指导6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_特禀质指导1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_特禀质指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_特禀质指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_特禀质指导4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_特禀质指导5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_平和质指导6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_平和质指导5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_平和质指导4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_平和质指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_平和质指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_平和质指导1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气虚质辨识_是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气虚质辨识_倾向是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_痰湿质辨识_倾向是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_痰湿质辨识_是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阳虚质辨识_倾向是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阳虚质辨识_是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴虚质辨识_倾向是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴虚质辨识_是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气郁质辨识_倾向是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_气郁质辨识_是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_特禀质辨识_倾向是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_特禀质辨识_是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_平和质辨识_是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_平和质辨识_倾向是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_湿热质辨识_倾向是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_湿热质辨识_是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血瘀质辨识_倾向是;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血瘀质辨识_是;
    }
}
