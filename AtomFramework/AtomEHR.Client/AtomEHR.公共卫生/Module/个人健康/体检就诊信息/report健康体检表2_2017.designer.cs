﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class report健康体检表2_2017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell363 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_大便潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_糖化血红蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell393 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_乙型肝炎表面抗原 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell396 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell394 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血清谷丙转氨酶 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell395 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell397 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血清谷草转氨酶 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell401 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_白蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell399 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell400 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_总胆红素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell402 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell403 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_综合胆红素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell405 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血清肌酐 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell406 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血尿素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell409 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血钾浓度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell410 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell411 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血钠浓度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell413 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_总胆固醇 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell414 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell415 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_甘油三酯 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell416 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell417 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血清低密度脂蛋白胆固醇 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell418 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell419 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血清高密度脂蛋白胆固醇 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell420 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心电图其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心电图1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心电图2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心电图3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心电图4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心电图5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心电图6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell421 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell422 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_胸部X线片异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_胸部X线片 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell359 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell425 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell424 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_腹部B超异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell361 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_腹部B超 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell362 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell355 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell356 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell427 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell428 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_B超其他异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell357 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_B超其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell358 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell351 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell352 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell423 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell426 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫颈涂片异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell353 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫颈涂片 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell354 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_辅助检查其他 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell347 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell348 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell349 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell350 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell344 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell429 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell430 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_脑血管疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_脑血管疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_脑血管疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_脑血管疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_脑血管疾病4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_脑血管疾病5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell346 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell431 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell432 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_肾脏疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_肾脏疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_肾脏疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_肾脏疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_肾脏疾病4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_肾脏疾病5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell433 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心血管疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心血管疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心血管疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心血管疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心血管疾病4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心血管疾病5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell434 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell435 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_眼部疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_眼部疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_眼部疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_眼部疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell436 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_神经系统其他疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_神经系统其他疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_神经系统其他疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_神经系统其他疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell364 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell365 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell366 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell367 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell372 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell373 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell437 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell438 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_其他系统疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell374 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_其他系统疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_其他系统疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_其他系统疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell375 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell368 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell369 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell454 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell452 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell453 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell370 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell371 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell384 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_入院日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell462 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_出院日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史原因1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史机构及科室1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史病案号1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell387 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell381 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_入院日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell464 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_出院日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史原因2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史机构及科室2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史病案号2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell383 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell376 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell377 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell389 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell388 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell378 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell379 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow77 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell391 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell439 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_建床日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell467 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_撤床日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史原因1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史机构及科室1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史病案号1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell444 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow78 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell445 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell446 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_建床日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell468 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_撤床日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史原因2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史机构及科室2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史病案号2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell451 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_口唇 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_缺齿1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_缺齿2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_龋齿1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_龋齿2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_义齿1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_义齿2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_缺齿3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_缺齿4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_龋齿3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_龋齿4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_义齿3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_义齿4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_齿列1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_齿列2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_咽部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_脏器功能医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_视力左眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_视力右眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_矫正视力左眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_矫正视力右眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_听力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_运动能力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_眼底异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_眼底 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_眼底医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_皮肤其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_巩膜其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_巩膜 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_淋巴结其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_淋巴结 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_桶状胸 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_呼吸音异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_呼吸音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_罗音其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_罗音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心律 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_杂音有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_杂音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_查体医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_压痛有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_压痛 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_包块有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_包块 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_肝大有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_肝大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_脾大有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_脾大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_移动性浊音有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_移动性浊音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_下肢水肿 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_肛门指诊其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_肛门指诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_肛门指诊医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_乳腺其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_乳腺2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_乳腺3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_乳腺4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_乳腺1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_乳腺医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_外阴异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_外阴 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_阴道异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_阴道 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫颈异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫颈 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_妇科医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫体异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_附件异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_附件 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_查体其他 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_查体其他医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_ABO = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_Rh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血红蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_白细胞 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血小板 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿糖 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿酮体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_空腹血糖1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_空腹血糖2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_同型半胱氨酸 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿微量白蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrCrossBandLine1 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.xrTable1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 2963.65F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(64, 64, 64, 64, 254F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(2103.01F, 166.2535F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow12,
            this.xrTableRow14,
            this.xrTableRow18,
            this.xrTableRow37,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow54,
            this.xrTableRow57,
            this.xrTableRow59,
            this.xrTableRow61,
            this.xrTableRow70,
            this.xrTableRow69,
            this.xrTableRow68,
            this.xrTableRow65,
            this.xrTableRow67,
            this.xrTableRow66,
            this.xrTableRow63,
            this.xrTableRow64,
            this.xrTableRow62,
            this.xrTableRow60,
            this.xrTableRow58,
            this.xrTableRow56,
            this.xrTableRow55,
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow71,
            this.xrTableRow73,
            this.xrTableRow72,
            this.xrTableRow76,
            this.xrTableRow75,
            this.xrTableRow74,
            this.xrTableRow77,
            this.xrTableRow78});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2009.294F, 2476.5F);
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell112,
            this.xrTableCell363,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.Weight = 0.11161843716525768D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.Dpi = 254F;
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.StylePriority.UseTextAlignment = false;
            this.xrTableCell112.Text = "  大便潜血*";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell112.Weight = 0.30993814107294138D;
            // 
            // xrTableCell363
            // 
            this.xrTableCell363.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell363.Dpi = 254F;
            this.xrTableCell363.Name = "xrTableCell363";
            this.xrTableCell363.StylePriority.UseBorders = false;
            this.xrTableCell363.StylePriority.UseTextAlignment = false;
            this.xrTableCell363.Text = "  1  阴性    2  阳性";
            this.xrTableCell363.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell363.Weight = 1.4516736402579293D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_大便潜血});
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.Weight = 0.12676978150387153D;
            // 
            // xrLabel_大便潜血
            // 
            this.xrLabel_大便潜血.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_大便潜血.Dpi = 254F;
            this.xrLabel_大便潜血.LocationFloat = new DevExpress.Utils.PointFloat(48.91278F, 7.999964F);
            this.xrLabel_大便潜血.Name = "xrLabel_大便潜血";
            this.xrLabel_大便潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_大便潜血.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_大便潜血.StylePriority.UseBorders = false;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.Weight = 0.19876670037244218D;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132,
            this.xrTableCell136,
            this.xrTableCell392,
            this.xrTableCell156,
            this.xrTableCell171});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell132.Dpi = 254F;
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Weight = 0.11161843716525768D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell136.Dpi = 254F;
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.StylePriority.UseBorders = false;
            this.xrTableCell136.StylePriority.UseTextAlignment = false;
            this.xrTableCell136.Text = "糖化血红蛋白*";
            this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell136.Weight = 0.30993814107294138D;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell392.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_糖化血红蛋白});
            this.xrTableCell392.Dpi = 254F;
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.StylePriority.UseBorders = false;
            this.xrTableCell392.Weight = 0.38619166578026387D;
            // 
            // xrLabel_糖化血红蛋白
            // 
            this.xrLabel_糖化血红蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_糖化血红蛋白.Dpi = 254F;
            this.xrLabel_糖化血红蛋白.LocationFloat = new DevExpress.Utils.PointFloat(25F, 0F);
            this.xrLabel_糖化血红蛋白.Name = "xrLabel_糖化血红蛋白";
            this.xrLabel_糖化血红蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_糖化血红蛋白.SizeF = new System.Drawing.SizeF(327.9126F, 49.95313F);
            this.xrLabel_糖化血红蛋白.StylePriority.UseBorders = false;
            this.xrLabel_糖化血红蛋白.StylePriority.UseTextAlignment = false;
            this.xrLabel_糖化血红蛋白.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell156.Dpi = 254F;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.StylePriority.UseTextAlignment = false;
            this.xrTableCell156.Text = "  %";
            this.xrTableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell156.Weight = 1.1922517559815371D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell171.Dpi = 254F;
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.StylePriority.UseBorders = false;
            this.xrTableCell171.Weight = 0.19876670037244218D;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell172,
            this.xrTableCell248,
            this.xrTableCell249,
            this.xrTableCell250});
            this.xrTableRow14.Dpi = 254F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell172.Dpi = 254F;
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.StylePriority.UseBorders = false;
            this.xrTableCell172.Weight = 0.11161843716525768D;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell248.Dpi = 254F;
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.StylePriority.UseBorders = false;
            this.xrTableCell248.StylePriority.UseTextAlignment = false;
            this.xrTableCell248.Text = "乙型肝炎";
            this.xrTableCell248.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell248.Weight = 0.30993814107294138D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell249.Dpi = 254F;
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.StylePriority.UseBorders = false;
            this.xrTableCell249.Weight = 1.5784434217618009D;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell250.Dpi = 254F;
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseBorders = false;
            this.xrTableCell250.Weight = 0.19876670037244218D;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell251,
            this.xrTableCell252,
            this.xrTableCell393,
            this.xrTableCell253,
            this.xrTableCell254});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell251.Dpi = 254F;
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.StylePriority.UseBorders = false;
            this.xrTableCell251.Weight = 0.11161843716525768D;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell252.Dpi = 254F;
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.StylePriority.UseBorders = false;
            this.xrTableCell252.StylePriority.UseTextAlignment = false;
            this.xrTableCell252.Text = "  表面抗原*";
            this.xrTableCell252.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell252.Weight = 0.30993814107294138D;
            // 
            // xrTableCell393
            // 
            this.xrTableCell393.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell393.Dpi = 254F;
            this.xrTableCell393.Name = "xrTableCell393";
            this.xrTableCell393.StylePriority.UseBorders = false;
            this.xrTableCell393.StylePriority.UseTextAlignment = false;
            this.xrTableCell393.Text = "  1  阴性    2  阳性";
            this.xrTableCell393.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell393.Weight = 1.4516739074206186D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell253.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_乙型肝炎表面抗原});
            this.xrTableCell253.Dpi = 254F;
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.StylePriority.UseBorders = false;
            this.xrTableCell253.Weight = 0.12676951434118211D;
            // 
            // xrLabel_乙型肝炎表面抗原
            // 
            this.xrLabel_乙型肝炎表面抗原.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_乙型肝炎表面抗原.Dpi = 254F;
            this.xrLabel_乙型肝炎表面抗原.LocationFloat = new DevExpress.Utils.PointFloat(48.91252F, 7.999964F);
            this.xrLabel_乙型肝炎表面抗原.Name = "xrLabel_乙型肝炎表面抗原";
            this.xrLabel_乙型肝炎表面抗原.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_乙型肝炎表面抗原.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_乙型肝炎表面抗原.StylePriority.UseBorders = false;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell254.Dpi = 254F;
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.StylePriority.UseBorders = false;
            this.xrTableCell254.Weight = 0.19876670037244218D;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell255,
            this.xrTableCell256,
            this.xrTableCell396,
            this.xrTableCell394,
            this.xrTableCell395,
            this.xrTableCell397,
            this.xrTableCell257,
            this.xrTableCell258});
            this.xrTableRow37.Dpi = 254F;
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell255.Dpi = 254F;
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.StylePriority.UseBorders = false;
            this.xrTableCell255.Weight = 0.11161843716525768D;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell256.Dpi = 254F;
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.StylePriority.UseBorders = false;
            this.xrTableCell256.StylePriority.UseTextAlignment = false;
            this.xrTableCell256.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell256.Weight = 0.30993814107294138D;
            // 
            // xrTableCell396
            // 
            this.xrTableCell396.Dpi = 254F;
            this.xrTableCell396.Name = "xrTableCell396";
            this.xrTableCell396.Text = "血清谷丙转氨酶";
            this.xrTableCell396.Weight = 0.325122825080712D;
            // 
            // xrTableCell394
            // 
            this.xrTableCell394.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血清谷丙转氨酶});
            this.xrTableCell394.Dpi = 254F;
            this.xrTableCell394.Name = "xrTableCell394";
            this.xrTableCell394.Weight = 0.27357458564221404D;
            // 
            // xrLabel_血清谷丙转氨酶
            // 
            this.xrLabel_血清谷丙转氨酶.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血清谷丙转氨酶.Dpi = 254F;
            this.xrLabel_血清谷丙转氨酶.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4F);
            this.xrLabel_血清谷丙转氨酶.Name = "xrLabel_血清谷丙转氨酶";
            this.xrLabel_血清谷丙转氨酶.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血清谷丙转氨酶.SizeF = new System.Drawing.SizeF(249.9998F, 49.95313F);
            this.xrLabel_血清谷丙转氨酶.StylePriority.UseBorders = false;
            this.xrLabel_血清谷丙转氨酶.StylePriority.UseTextAlignment = false;
            this.xrLabel_血清谷丙转氨酶.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell395
            // 
            this.xrTableCell395.Dpi = 254F;
            this.xrTableCell395.Name = "xrTableCell395";
            this.xrTableCell395.Text = "U/L\t血清谷草转氨酶";
            this.xrTableCell395.Weight = 0.52491217702929438D;
            // 
            // xrTableCell397
            // 
            this.xrTableCell397.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血清谷草转氨酶});
            this.xrTableCell397.Dpi = 254F;
            this.xrTableCell397.Name = "xrTableCell397";
            this.xrTableCell397.Weight = 0.27357459816088148D;
            // 
            // xrLabel_血清谷草转氨酶
            // 
            this.xrLabel_血清谷草转氨酶.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血清谷草转氨酶.Dpi = 254F;
            this.xrLabel_血清谷草转氨酶.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_血清谷草转氨酶.Name = "xrLabel_血清谷草转氨酶";
            this.xrLabel_血清谷草转氨酶.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血清谷草转氨酶.SizeF = new System.Drawing.SizeF(249.9998F, 49.95313F);
            this.xrLabel_血清谷草转氨酶.StylePriority.UseBorders = false;
            this.xrLabel_血清谷草转氨酶.StylePriority.UseTextAlignment = false;
            this.xrLabel_血清谷草转氨酶.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell257.Dpi = 254F;
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.StylePriority.UseBorders = false;
            this.xrTableCell257.StylePriority.UseTextAlignment = false;
            this.xrTableCell257.Text = "  U/L";
            this.xrTableCell257.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell257.Weight = 0.181259235848699D;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell258.Dpi = 254F;
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.StylePriority.UseBorders = false;
            this.xrTableCell258.Weight = 0.19876670037244218D;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell259,
            this.xrTableCell260,
            this.xrTableCell401,
            this.xrTableCell398,
            this.xrTableCell399,
            this.xrTableCell400,
            this.xrTableCell261,
            this.xrTableCell262});
            this.xrTableRow45.Dpi = 254F;
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1D;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell259.Dpi = 254F;
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.StylePriority.UseBorders = false;
            this.xrTableCell259.Text = "辅";
            this.xrTableCell259.Weight = 0.11161843716525768D;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell260.Dpi = 254F;
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.StylePriority.UseBorders = false;
            this.xrTableCell260.StylePriority.UseTextAlignment = false;
            this.xrTableCell260.Text = "  肝  功  能*";
            this.xrTableCell260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell260.Weight = 0.30993814107294138D;
            // 
            // xrTableCell401
            // 
            this.xrTableCell401.Dpi = 254F;
            this.xrTableCell401.Name = "xrTableCell401";
            this.xrTableCell401.Text = "白蛋白";
            this.xrTableCell401.Weight = 0.1514029579022175D;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_白蛋白});
            this.xrTableCell398.Dpi = 254F;
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.Weight = 0.447294719983398D;
            // 
            // xrLabel_白蛋白
            // 
            this.xrLabel_白蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_白蛋白.Dpi = 254F;
            this.xrLabel_白蛋白.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_白蛋白.Name = "xrLabel_白蛋白";
            this.xrLabel_白蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_白蛋白.SizeF = new System.Drawing.SizeF(408.75F, 49.95309F);
            this.xrLabel_白蛋白.StylePriority.UseBorders = false;
            this.xrLabel_白蛋白.StylePriority.UseTextAlignment = false;
            this.xrLabel_白蛋白.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell399
            // 
            this.xrTableCell399.Dpi = 254F;
            this.xrTableCell399.Name = "xrTableCell399";
            this.xrTableCell399.Text = "g/L\t总胆红素";
            this.xrTableCell399.Weight = 0.39751725154814588D;
            // 
            // xrTableCell400
            // 
            this.xrTableCell400.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_总胆红素});
            this.xrTableCell400.Dpi = 254F;
            this.xrTableCell400.Name = "xrTableCell400";
            this.xrTableCell400.Weight = 0.35001117942180543D;
            // 
            // xrLabel_总胆红素
            // 
            this.xrLabel_总胆红素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_总胆红素.Dpi = 254F;
            this.xrLabel_总胆红素.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_总胆红素.Name = "xrLabel_总胆红素";
            this.xrLabel_总胆红素.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_总胆红素.SizeF = new System.Drawing.SizeF(319.8499F, 49.95313F);
            this.xrLabel_总胆红素.StylePriority.UseBorders = false;
            this.xrLabel_总胆红素.StylePriority.UseTextAlignment = false;
            this.xrLabel_总胆红素.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell261.Dpi = 254F;
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseBorders = false;
            this.xrTableCell261.StylePriority.UseTextAlignment = false;
            this.xrTableCell261.Text = "  μ mol/L";
            this.xrTableCell261.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell261.Weight = 0.23221731290623418D;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell262.Dpi = 254F;
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.StylePriority.UseBorders = false;
            this.xrTableCell262.Weight = 0.19876670037244218D;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell402,
            this.xrTableCell403,
            this.xrTableCell265,
            this.xrTableCell266});
            this.xrTableRow46.Dpi = 254F;
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1D;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell263.Dpi = 254F;
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBorders = false;
            this.xrTableCell263.Weight = 0.11161843716525768D;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell264.Dpi = 254F;
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.StylePriority.UseBorders = false;
            this.xrTableCell264.StylePriority.UseTextAlignment = false;
            this.xrTableCell264.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell264.Weight = 0.30993814107294138D;
            // 
            // xrTableCell402
            // 
            this.xrTableCell402.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell402.Dpi = 254F;
            this.xrTableCell402.Name = "xrTableCell402";
            this.xrTableCell402.StylePriority.UseBorders = false;
            this.xrTableCell402.Text = "综合胆红素";
            this.xrTableCell402.Weight = 0.23563455331080002D;
            // 
            // xrTableCell403
            // 
            this.xrTableCell403.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell403.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_综合胆红素});
            this.xrTableCell403.Dpi = 254F;
            this.xrTableCell403.Name = "xrTableCell403";
            this.xrTableCell403.StylePriority.UseBorders = false;
            this.xrTableCell403.Weight = 0.3630631663281525D;
            // 
            // xrLabel_综合胆红素
            // 
            this.xrLabel_综合胆红素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_综合胆红素.Dpi = 254F;
            this.xrLabel_综合胆红素.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_综合胆红素.Name = "xrLabel_综合胆红素";
            this.xrLabel_综合胆红素.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_综合胆红素.SizeF = new System.Drawing.SizeF(331.7769F, 49.95319F);
            this.xrLabel_综合胆红素.StylePriority.UseBorders = false;
            this.xrLabel_综合胆红素.StylePriority.UseTextAlignment = false;
            this.xrLabel_综合胆红素.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell265.Dpi = 254F;
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseBorders = false;
            this.xrTableCell265.StylePriority.UseTextAlignment = false;
            this.xrTableCell265.Text = " μ mol/L";
            this.xrTableCell265.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell265.Weight = 0.97974570212284817D;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell266.Dpi = 254F;
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.StylePriority.UseBorders = false;
            this.xrTableCell266.Weight = 0.19876670037244218D;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell267,
            this.xrTableCell268,
            this.xrTableCell405,
            this.xrTableCell404,
            this.xrTableCell406,
            this.xrTableCell407,
            this.xrTableCell269,
            this.xrTableCell270});
            this.xrTableRow47.Dpi = 254F;
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1D;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell267.Dpi = 254F;
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseBorders = false;
            this.xrTableCell267.Text = "助";
            this.xrTableCell267.Weight = 0.11161843716525768D;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell268.Dpi = 254F;
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.StylePriority.UseBorders = false;
            this.xrTableCell268.StylePriority.UseTextAlignment = false;
            this.xrTableCell268.Text = "  肾  功  能*";
            this.xrTableCell268.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell268.Weight = 0.30993814107294138D;
            // 
            // xrTableCell405
            // 
            this.xrTableCell405.Dpi = 254F;
            this.xrTableCell405.Name = "xrTableCell405";
            this.xrTableCell405.StylePriority.UseTextAlignment = false;
            this.xrTableCell405.Text = "  血清肌酐";
            this.xrTableCell405.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell405.Weight = 0.18614694803558454D;
            // 
            // xrTableCell404
            // 
            this.xrTableCell404.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血清肌酐});
            this.xrTableCell404.Dpi = 254F;
            this.xrTableCell404.Name = "xrTableCell404";
            this.xrTableCell404.Weight = 0.246369960000051D;
            // 
            // xrLabel_血清肌酐
            // 
            this.xrLabel_血清肌酐.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血清肌酐.Dpi = 254F;
            this.xrLabel_血清肌酐.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_血清肌酐.Name = "xrLabel_血清肌酐";
            this.xrLabel_血清肌酐.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血清肌酐.SizeF = new System.Drawing.SizeF(225.1396F, 49.95319F);
            this.xrLabel_血清肌酐.StylePriority.UseBorders = false;
            this.xrLabel_血清肌酐.StylePriority.UseTextAlignment = false;
            this.xrLabel_血清肌酐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell406
            // 
            this.xrTableCell406.Dpi = 254F;
            this.xrTableCell406.Name = "xrTableCell406";
            this.xrTableCell406.StylePriority.UseTextAlignment = false;
            this.xrTableCell406.Text = "  μ mol/L\t血尿素";
            this.xrTableCell406.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell406.Weight = 0.34596927927233723D;
            // 
            // xrTableCell407
            // 
            this.xrTableCell407.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血尿素});
            this.xrTableCell407.Dpi = 254F;
            this.xrTableCell407.Name = "xrTableCell407";
            this.xrTableCell407.Weight = 0.29037939848905964D;
            // 
            // xrLabel_血尿素
            // 
            this.xrLabel_血尿素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血尿素.Dpi = 254F;
            this.xrLabel_血尿素.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.746521F);
            this.xrLabel_血尿素.Name = "xrLabel_血尿素";
            this.xrLabel_血尿素.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血尿素.SizeF = new System.Drawing.SizeF(265.3564F, 49.95319F);
            this.xrLabel_血尿素.StylePriority.UseBorders = false;
            this.xrLabel_血尿素.StylePriority.UseTextAlignment = false;
            this.xrLabel_血尿素.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell269.Dpi = 254F;
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.StylePriority.UseBorders = false;
            this.xrTableCell269.StylePriority.UseTextAlignment = false;
            this.xrTableCell269.Text = "  mmol/L";
            this.xrTableCell269.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell269.Weight = 0.50957783596476858D;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell270.Dpi = 254F;
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.StylePriority.UseBorders = false;
            this.xrTableCell270.Weight = 0.19876670037244218D;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell271,
            this.xrTableCell272,
            this.xrTableCell409,
            this.xrTableCell408,
            this.xrTableCell410,
            this.xrTableCell411,
            this.xrTableCell273,
            this.xrTableCell274});
            this.xrTableRow48.Dpi = 254F;
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1D;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell271.Dpi = 254F;
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.StylePriority.UseBorders = false;
            this.xrTableCell271.Weight = 0.11161843716525768D;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell272.Dpi = 254F;
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.StylePriority.UseBorders = false;
            this.xrTableCell272.StylePriority.UseTextAlignment = false;
            this.xrTableCell272.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell272.Weight = 0.30993814107294138D;
            // 
            // xrTableCell409
            // 
            this.xrTableCell409.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell409.Dpi = 254F;
            this.xrTableCell409.Name = "xrTableCell409";
            this.xrTableCell409.StylePriority.UseBorders = false;
            this.xrTableCell409.StylePriority.UseTextAlignment = false;
            this.xrTableCell409.Text = "  血钾浓度";
            this.xrTableCell409.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell409.Weight = 0.18614694803558454D;
            // 
            // xrTableCell408
            // 
            this.xrTableCell408.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell408.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血钾浓度});
            this.xrTableCell408.Dpi = 254F;
            this.xrTableCell408.Name = "xrTableCell408";
            this.xrTableCell408.StylePriority.UseBorders = false;
            this.xrTableCell408.Weight = 0.246369960000051D;
            // 
            // xrLabel_血钾浓度
            // 
            this.xrLabel_血钾浓度.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血钾浓度.Dpi = 254F;
            this.xrLabel_血钾浓度.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.746521F);
            this.xrLabel_血钾浓度.Name = "xrLabel_血钾浓度";
            this.xrLabel_血钾浓度.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血钾浓度.SizeF = new System.Drawing.SizeF(225.1396F, 49.95319F);
            this.xrLabel_血钾浓度.StylePriority.UseBorders = false;
            this.xrLabel_血钾浓度.StylePriority.UseTextAlignment = false;
            this.xrLabel_血钾浓度.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell410
            // 
            this.xrTableCell410.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell410.Dpi = 254F;
            this.xrTableCell410.Name = "xrTableCell410";
            this.xrTableCell410.StylePriority.UseBorders = false;
            this.xrTableCell410.StylePriority.UseTextAlignment = false;
            this.xrTableCell410.Text = "  mmol/L\t血钠浓度";
            this.xrTableCell410.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell410.Weight = 0.38997818761496739D;
            // 
            // xrTableCell411
            // 
            this.xrTableCell411.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell411.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血钠浓度});
            this.xrTableCell411.Dpi = 254F;
            this.xrTableCell411.Name = "xrTableCell411";
            this.xrTableCell411.StylePriority.UseBorders = false;
            this.xrTableCell411.Weight = 0.24637033986741663D;
            // 
            // xrLabel_血钠浓度
            // 
            this.xrLabel_血钠浓度.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血钠浓度.Dpi = 254F;
            this.xrLabel_血钠浓度.LocationFloat = new DevExpress.Utils.PointFloat(0.0004882813F, 0.746521F);
            this.xrLabel_血钠浓度.Name = "xrLabel_血钠浓度";
            this.xrLabel_血钠浓度.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血钠浓度.SizeF = new System.Drawing.SizeF(225.1396F, 49.95319F);
            this.xrLabel_血钠浓度.StylePriority.UseBorders = false;
            this.xrLabel_血钠浓度.StylePriority.UseTextAlignment = false;
            this.xrLabel_血钠浓度.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell273.Dpi = 254F;
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.StylePriority.UseBorders = false;
            this.xrTableCell273.StylePriority.UseTextAlignment = false;
            this.xrTableCell273.Text = "  mmol/L";
            this.xrTableCell273.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell273.Weight = 0.50957798624378126D;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell274.Dpi = 254F;
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.StylePriority.UseBorders = false;
            this.xrTableCell274.Weight = 0.19876670037244218D;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell275,
            this.xrTableCell276,
            this.xrTableCell413,
            this.xrTableC,
            this.xrTableCell414,
            this.xrTableCell415,
            this.xrTableCell277,
            this.xrTableCell278});
            this.xrTableRow49.Dpi = 254F;
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1D;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell275.Dpi = 254F;
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.StylePriority.UseBorders = false;
            this.xrTableCell275.Text = "检";
            this.xrTableCell275.Weight = 0.11161843716525768D;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell276.Dpi = 254F;
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.StylePriority.UseBorders = false;
            this.xrTableCell276.StylePriority.UseTextAlignment = false;
            this.xrTableCell276.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell276.Weight = 0.30993814107294138D;
            // 
            // xrTableCell413
            // 
            this.xrTableCell413.Dpi = 254F;
            this.xrTableCell413.Name = "xrTableCell413";
            this.xrTableCell413.StylePriority.UseTextAlignment = false;
            this.xrTableCell413.Text = "  总胆固醇";
            this.xrTableCell413.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell413.Weight = 0.18614694803558454D;
            // 
            // xrTableC
            // 
            this.xrTableC.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_总胆固醇});
            this.xrTableC.Dpi = 254F;
            this.xrTableC.Name = "xrTableC";
            this.xrTableC.Weight = 0.2463696928373616D;
            // 
            // xrLabel_总胆固醇
            // 
            this.xrLabel_总胆固醇.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_总胆固醇.Dpi = 254F;
            this.xrLabel_总胆固醇.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.0002583821F);
            this.xrLabel_总胆固醇.Name = "xrLabel_总胆固醇";
            this.xrLabel_总胆固醇.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_总胆固醇.SizeF = new System.Drawing.SizeF(225.1396F, 49.95319F);
            this.xrLabel_总胆固醇.StylePriority.UseBorders = false;
            this.xrLabel_总胆固醇.StylePriority.UseTextAlignment = false;
            this.xrLabel_总胆固醇.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell414
            // 
            this.xrTableCell414.Dpi = 254F;
            this.xrTableCell414.Name = "xrTableCell414";
            this.xrTableCell414.StylePriority.UseTextAlignment = false;
            this.xrTableCell414.Text = "  mmol/L\t甘油三酯";
            this.xrTableCell414.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell414.Weight = 0.3899781876149675D;
            // 
            // xrTableCell415
            // 
            this.xrTableCell415.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_甘油三酯});
            this.xrTableCell415.Dpi = 254F;
            this.xrTableCell415.Name = "xrTableCell415";
            this.xrTableCell415.Weight = 0.24637049014642931D;
            // 
            // xrLabel_甘油三酯
            // 
            this.xrLabel_甘油三酯.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_甘油三酯.Dpi = 254F;
            this.xrLabel_甘油三酯.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.0001291911F);
            this.xrLabel_甘油三酯.Name = "xrLabel_甘油三酯";
            this.xrLabel_甘油三酯.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_甘油三酯.SizeF = new System.Drawing.SizeF(225.1396F, 49.95319F);
            this.xrLabel_甘油三酯.StylePriority.UseBorders = false;
            this.xrLabel_甘油三酯.StylePriority.UseTextAlignment = false;
            this.xrLabel_甘油三酯.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell277.Dpi = 254F;
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.StylePriority.UseBorders = false;
            this.xrTableCell277.StylePriority.UseTextAlignment = false;
            this.xrTableCell277.Text = "  mmol/L";
            this.xrTableCell277.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell277.Weight = 0.50957810312745788D;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell278.Dpi = 254F;
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.StylePriority.UseBorders = false;
            this.xrTableCell278.Weight = 0.19876670037244218D;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell279,
            this.xrTableCell280,
            this.xrTableCell416,
            this.xrTableCell417,
            this.xrTableCell281,
            this.xrTableCell282});
            this.xrTableRow50.Dpi = 254F;
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1D;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell279.Dpi = 254F;
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.StylePriority.UseBorders = false;
            this.xrTableCell279.Weight = 0.11161843716525768D;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell280.Dpi = 254F;
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.StylePriority.UseBorders = false;
            this.xrTableCell280.StylePriority.UseTextAlignment = false;
            this.xrTableCell280.Text = "  血        脂*";
            this.xrTableCell280.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell280.Weight = 0.30993814107294138D;
            // 
            // xrTableCell416
            // 
            this.xrTableCell416.Dpi = 254F;
            this.xrTableCell416.Name = "xrTableCell416";
            this.xrTableCell416.StylePriority.UseTextAlignment = false;
            this.xrTableCell416.Text = "  血清低密度脂蛋白胆固醇";
            this.xrTableCell416.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell416.Weight = 0.47884238405836033D;
            // 
            // xrTableCell417
            // 
            this.xrTableCell417.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血清低密度脂蛋白胆固醇});
            this.xrTableCell417.Dpi = 254F;
            this.xrTableCell417.Name = "xrTableCell417";
            this.xrTableCell417.Weight = 0.31037946876188582D;
            // 
            // xrLabel_血清低密度脂蛋白胆固醇
            // 
            this.xrLabel_血清低密度脂蛋白胆固醇.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血清低密度脂蛋白胆固醇.Dpi = 254F;
            this.xrLabel_血清低密度脂蛋白胆固醇.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0.0001831055F);
            this.xrLabel_血清低密度脂蛋白胆固醇.Name = "xrLabel_血清低密度脂蛋白胆固醇";
            this.xrLabel_血清低密度脂蛋白胆固醇.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血清低密度脂蛋白胆固醇.SizeF = new System.Drawing.SizeF(283.6331F, 49.95313F);
            this.xrLabel_血清低密度脂蛋白胆固醇.StylePriority.UseBorders = false;
            this.xrLabel_血清低密度脂蛋白胆固醇.StylePriority.UseTextAlignment = false;
            this.xrLabel_血清低密度脂蛋白胆固醇.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell281.Dpi = 254F;
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.StylePriority.UseBorders = false;
            this.xrTableCell281.StylePriority.UseTextAlignment = false;
            this.xrTableCell281.Text = "  mmol/L";
            this.xrTableCell281.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell281.Weight = 0.78922156894155471D;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell282.Dpi = 254F;
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.StylePriority.UseBorders = false;
            this.xrTableCell282.Weight = 0.19876670037244218D;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell283,
            this.xrTableCell284,
            this.xrTableCell418,
            this.xrTableCell419,
            this.xrTableCell285,
            this.xrTableCell286});
            this.xrTableRow51.Dpi = 254F;
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1D;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell283.Dpi = 254F;
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.StylePriority.UseBorders = false;
            this.xrTableCell283.Text = "查";
            this.xrTableCell283.Weight = 0.11161843716525768D;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell284.Dpi = 254F;
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.StylePriority.UseBorders = false;
            this.xrTableCell284.StylePriority.UseTextAlignment = false;
            this.xrTableCell284.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell284.Weight = 0.30993814107294138D;
            // 
            // xrTableCell418
            // 
            this.xrTableCell418.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell418.Dpi = 254F;
            this.xrTableCell418.Name = "xrTableCell418";
            this.xrTableCell418.StylePriority.UseBorders = false;
            this.xrTableCell418.StylePriority.UseTextAlignment = false;
            this.xrTableCell418.Text = "  血清高密度脂蛋白胆固醇";
            this.xrTableCell418.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell418.Weight = 0.47884238405836033D;
            // 
            // xrTableCell419
            // 
            this.xrTableCell419.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell419.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血清高密度脂蛋白胆固醇});
            this.xrTableCell419.Dpi = 254F;
            this.xrTableCell419.Name = "xrTableCell419";
            this.xrTableCell419.StylePriority.UseBorders = false;
            this.xrTableCell419.Weight = 0.31037946876188582D;
            // 
            // xrLabel_血清高密度脂蛋白胆固醇
            // 
            this.xrLabel_血清高密度脂蛋白胆固醇.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血清高密度脂蛋白胆固醇.Dpi = 254F;
            this.xrLabel_血清高密度脂蛋白胆固醇.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_血清高密度脂蛋白胆固醇.Name = "xrLabel_血清高密度脂蛋白胆固醇";
            this.xrLabel_血清高密度脂蛋白胆固醇.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血清高密度脂蛋白胆固醇.SizeF = new System.Drawing.SizeF(283.6331F, 49.95313F);
            this.xrLabel_血清高密度脂蛋白胆固醇.StylePriority.UseBorders = false;
            this.xrLabel_血清高密度脂蛋白胆固醇.StylePriority.UseTextAlignment = false;
            this.xrLabel_血清高密度脂蛋白胆固醇.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell285.Dpi = 254F;
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.StylePriority.UseBorders = false;
            this.xrTableCell285.StylePriority.UseTextAlignment = false;
            this.xrTableCell285.Text = "  mmol/L";
            this.xrTableCell285.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell285.Weight = 0.78922156894155471D;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell286.Dpi = 254F;
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.StylePriority.UseBorders = false;
            this.xrTableCell286.Weight = 0.19876670037244218D;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell295,
            this.xrTableCell296,
            this.xrTableCell297,
            this.xrTableCell298});
            this.xrTableRow54.Dpi = 254F;
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1D;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell295.Dpi = 254F;
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.StylePriority.UseBorders = false;
            this.xrTableCell295.Weight = 0.11161843716525768D;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell296.Dpi = 254F;
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.StylePriority.UseBorders = false;
            this.xrTableCell296.StylePriority.UseTextAlignment = false;
            this.xrTableCell296.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell296.Weight = 0.30993814107294138D;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell297.Dpi = 254F;
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.StylePriority.UseBorders = false;
            this.xrTableCell297.StylePriority.UseTextAlignment = false;
            this.xrTableCell297.Text = "  1  正常      2  ST-T改变      3  陈旧性心肌梗塞      4  窦性心动过速      5  窦性心动过缓";
            this.xrTableCell297.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell297.Weight = 1.5784434217618009D;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell298.Dpi = 254F;
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.StylePriority.UseBorders = false;
            this.xrTableCell298.Weight = 0.19876670037244218D;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell307,
            this.xrTableCell308,
            this.xrTableCell420,
            this.xrTableCell309,
            this.xrTableCell310});
            this.xrTableRow57.Dpi = 254F;
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 1D;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell307.Dpi = 254F;
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.StylePriority.UseBorders = false;
            this.xrTableCell307.Weight = 0.11161843716525768D;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell308.Dpi = 254F;
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.StylePriority.UseBorders = false;
            this.xrTableCell308.StylePriority.UseTextAlignment = false;
            this.xrTableCell308.Text = "  心  电  图*";
            this.xrTableCell308.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell308.Weight = 0.30993814107294138D;
            // 
            // xrTableCell420
            // 
            this.xrTableCell420.Dpi = 254F;
            this.xrTableCell420.Name = "xrTableCell420";
            this.xrTableCell420.StylePriority.UseTextAlignment = false;
            this.xrTableCell420.Text = "  6  早搏      7  房颤      8  房室传导阻滞      9  其他";
            this.xrTableCell420.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell420.Weight = 0.8517608897814275D;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell309.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心电图其他});
            this.xrTableCell309.Dpi = 254F;
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.StylePriority.UseBorders = false;
            this.xrTableCell309.Weight = 0.72668253198037336D;
            // 
            // xrLabel_心电图其他
            // 
            this.xrLabel_心电图其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_心电图其他.Dpi = 254F;
            this.xrLabel_心电图其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_心电图其他.Name = "xrLabel_心电图其他";
            this.xrLabel_心电图其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心电图其他.SizeF = new System.Drawing.SizeF(639.0625F, 49.95306F);
            this.xrLabel_心电图其他.StylePriority.UseBorders = false;
            this.xrLabel_心电图其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_心电图其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell310.Dpi = 254F;
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.StylePriority.UseBorders = false;
            this.xrTableCell310.Weight = 0.19876670037244218D;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell315,
            this.xrTableCell316,
            this.xrTableCell317,
            this.xrTableCell318});
            this.xrTableRow59.Dpi = 254F;
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 1D;
            // 
            // xrTableCell315
            // 
            this.xrTableCell315.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell315.Dpi = 254F;
            this.xrTableCell315.Name = "xrTableCell315";
            this.xrTableCell315.StylePriority.UseBorders = false;
            this.xrTableCell315.Weight = 0.11161843716525768D;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell316.Dpi = 254F;
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.StylePriority.UseBorders = false;
            this.xrTableCell316.StylePriority.UseTextAlignment = false;
            this.xrTableCell316.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell316.Weight = 0.30993814107294138D;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell317.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心电图1,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel_心电图2,
            this.xrLabel_心电图3,
            this.xrLabel22,
            this.xrLabel_心电图4,
            this.xrLabel20,
            this.xrLabel_心电图5,
            this.xrLabel18,
            this.xrLabel_心电图6});
            this.xrTableCell317.Dpi = 254F;
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.StylePriority.UseBorders = false;
            this.xrTableCell317.Weight = 1.5784434217618009D;
            // 
            // xrLabel_心电图1
            // 
            this.xrLabel_心电图1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图1.Dpi = 254F;
            this.xrLabel_心电图1.LocationFloat = new DevExpress.Utils.PointFloat(956.161F, 8.000029F);
            this.xrLabel_心电图1.Name = "xrLabel_心电图1";
            this.xrLabel_心电图1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心电图1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_心电图1.StylePriority.UseBorders = false;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.Dpi = 254F;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(998.0939F, 7.999899F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "/";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.Dpi = 254F;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(1081.96F, 8.000029F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "/";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心电图2
            // 
            this.xrLabel_心电图2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图2.Dpi = 254F;
            this.xrLabel_心电图2.LocationFloat = new DevExpress.Utils.PointFloat(1040.027F, 7.999899F);
            this.xrLabel_心电图2.Name = "xrLabel_心电图2";
            this.xrLabel_心电图2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心电图2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_心电图2.StylePriority.UseBorders = false;
            // 
            // xrLabel_心电图3
            // 
            this.xrLabel_心电图3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图3.Dpi = 254F;
            this.xrLabel_心电图3.LocationFloat = new DevExpress.Utils.PointFloat(1123.893F, 8.000029F);
            this.xrLabel_心电图3.Name = "xrLabel_心电图3";
            this.xrLabel_心电图3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心电图3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_心电图3.StylePriority.UseBorders = false;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.Dpi = 254F;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(1165.827F, 8.000029F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "/";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心电图4
            // 
            this.xrLabel_心电图4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图4.Dpi = 254F;
            this.xrLabel_心电图4.LocationFloat = new DevExpress.Utils.PointFloat(1207.76F, 8.000029F);
            this.xrLabel_心电图4.Name = "xrLabel_心电图4";
            this.xrLabel_心电图4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心电图4.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_心电图4.StylePriority.UseBorders = false;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.Dpi = 254F;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(1249.693F, 8.000029F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "/";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心电图5
            // 
            this.xrLabel_心电图5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图5.Dpi = 254F;
            this.xrLabel_心电图5.LocationFloat = new DevExpress.Utils.PointFloat(1291.626F, 7.999899F);
            this.xrLabel_心电图5.Name = "xrLabel_心电图5";
            this.xrLabel_心电图5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心电图5.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_心电图5.StylePriority.UseBorders = false;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Dpi = 254F;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(1333.559F, 7.999964F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "/";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心电图6
            // 
            this.xrLabel_心电图6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图6.Dpi = 254F;
            this.xrLabel_心电图6.LocationFloat = new DevExpress.Utils.PointFloat(1375.492F, 8F);
            this.xrLabel_心电图6.Name = "xrLabel_心电图6";
            this.xrLabel_心电图6.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心电图6.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_心电图6.StylePriority.UseBorders = false;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell318.Dpi = 254F;
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.StylePriority.UseBorders = false;
            this.xrTableCell318.Weight = 0.19876670037244218D;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell323,
            this.xrTableCell324,
            this.xrTableCell421,
            this.xrTableCell422,
            this.xrTableCell325,
            this.xrTableCell326});
            this.xrTableRow61.Dpi = 254F;
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 1D;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell323.Dpi = 254F;
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.StylePriority.UseBorders = false;
            this.xrTableCell323.Weight = 0.11161843716525768D;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell324.Dpi = 254F;
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.StylePriority.UseBorders = false;
            this.xrTableCell324.StylePriority.UseTextAlignment = false;
            this.xrTableCell324.Text = "  胸部X线片*";
            this.xrTableCell324.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell324.Weight = 0.30993814107294138D;
            // 
            // xrTableCell421
            // 
            this.xrTableCell421.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell421.Dpi = 254F;
            this.xrTableCell421.Name = "xrTableCell421";
            this.xrTableCell421.StylePriority.UseBorders = false;
            this.xrTableCell421.StylePriority.UseTextAlignment = false;
            this.xrTableCell421.Text = "  1  正常\t2  异常";
            this.xrTableCell421.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell421.Weight = 0.34913142522249607D;
            // 
            // xrTableCell422
            // 
            this.xrTableCell422.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell422.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_胸部X线片异常});
            this.xrTableCell422.Dpi = 254F;
            this.xrTableCell422.Name = "xrTableCell422";
            this.xrTableCell422.StylePriority.UseBorders = false;
            this.xrTableCell422.Weight = 1.1025422233934341D;
            // 
            // xrLabel_胸部X线片异常
            // 
            this.xrLabel_胸部X线片异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_胸部X线片异常.Dpi = 254F;
            this.xrLabel_胸部X线片异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.893494F);
            this.xrLabel_胸部X线片异常.Name = "xrLabel_胸部X线片异常";
            this.xrLabel_胸部X线片异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_胸部X线片异常.SizeF = new System.Drawing.SizeF(523.9897F, 49.95313F);
            this.xrLabel_胸部X线片异常.StylePriority.UseBorders = false;
            this.xrLabel_胸部X线片异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_胸部X线片异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell325.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_胸部X线片});
            this.xrTableCell325.Dpi = 254F;
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.StylePriority.UseBorders = false;
            this.xrTableCell325.Weight = 0.12676977314587062D;
            // 
            // xrLabel_胸部X线片
            // 
            this.xrLabel_胸部X线片.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_胸部X线片.Dpi = 254F;
            this.xrLabel_胸部X线片.LocationFloat = new DevExpress.Utils.PointFloat(48.91278F, 8F);
            this.xrLabel_胸部X线片.Name = "xrLabel_胸部X线片";
            this.xrLabel_胸部X线片.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_胸部X线片.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_胸部X线片.StylePriority.UseBorders = false;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell326.Dpi = 254F;
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.StylePriority.UseBorders = false;
            this.xrTableCell326.Weight = 0.19876670037244218D;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell359,
            this.xrTableCell360,
            this.xrTableCell425,
            this.xrTableCell424,
            this.xrTableCell361,
            this.xrTableCell362});
            this.xrTableRow70.Dpi = 254F;
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 1D;
            // 
            // xrTableCell359
            // 
            this.xrTableCell359.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell359.Dpi = 254F;
            this.xrTableCell359.Name = "xrTableCell359";
            this.xrTableCell359.StylePriority.UseBorders = false;
            this.xrTableCell359.Weight = 0.11161843716525768D;
            // 
            // xrTableCell360
            // 
            this.xrTableCell360.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell360.Dpi = 254F;
            this.xrTableCell360.Name = "xrTableCell360";
            this.xrTableCell360.StylePriority.UseBorders = false;
            this.xrTableCell360.StylePriority.UseTextAlignment = false;
            this.xrTableCell360.Text = "  B     超*";
            this.xrTableCell360.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell360.Weight = 0.30993814107294138D;
            // 
            // xrTableCell425
            // 
            this.xrTableCell425.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell425.Dpi = 254F;
            this.xrTableCell425.Name = "xrTableCell425";
            this.xrTableCell425.StylePriority.UseBorders = false;
            this.xrTableCell425.StylePriority.UseTextAlignment = false;
            this.xrTableCell425.Text = "  腹部B超：\t1  正常\t2  异常";
            this.xrTableCell425.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell425.Weight = 0.56369832195615155D;
            // 
            // xrTableCell424
            // 
            this.xrTableCell424.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell424.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_腹部B超异常});
            this.xrTableCell424.Dpi = 254F;
            this.xrTableCell424.Name = "xrTableCell424";
            this.xrTableCell424.StylePriority.UseBorders = false;
            this.xrTableCell424.Weight = 0.88797559382246827D;
            // 
            // xrLabel_腹部B超异常
            // 
            this.xrLabel_腹部B超异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_腹部B超异常.Dpi = 254F;
            this.xrLabel_腹部B超异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_腹部B超异常.Name = "xrLabel_腹部B超异常";
            this.xrLabel_腹部B超异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_腹部B超异常.SizeF = new System.Drawing.SizeF(327.9126F, 49.95313F);
            this.xrLabel_腹部B超异常.StylePriority.UseBorders = false;
            this.xrLabel_腹部B超异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_腹部B超异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell361
            // 
            this.xrTableCell361.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell361.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_腹部B超});
            this.xrTableCell361.Dpi = 254F;
            this.xrTableCell361.Name = "xrTableCell361";
            this.xrTableCell361.StylePriority.UseBorders = false;
            this.xrTableCell361.Weight = 0.12676950598318121D;
            // 
            // xrLabel_腹部B超
            // 
            this.xrLabel_腹部B超.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_腹部B超.Dpi = 254F;
            this.xrLabel_腹部B超.LocationFloat = new DevExpress.Utils.PointFloat(48.91235F, 8F);
            this.xrLabel_腹部B超.Name = "xrLabel_腹部B超";
            this.xrLabel_腹部B超.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_腹部B超.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_腹部B超.StylePriority.UseBorders = false;
            // 
            // xrTableCell362
            // 
            this.xrTableCell362.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell362.Dpi = 254F;
            this.xrTableCell362.Name = "xrTableCell362";
            this.xrTableCell362.StylePriority.UseBorders = false;
            this.xrTableCell362.Weight = 0.19876670037244218D;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell355,
            this.xrTableCell356,
            this.xrTableCell427,
            this.xrTableCell428,
            this.xrTableCell357,
            this.xrTableCell358});
            this.xrTableRow69.Dpi = 254F;
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 1D;
            // 
            // xrTableCell355
            // 
            this.xrTableCell355.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell355.Dpi = 254F;
            this.xrTableCell355.Name = "xrTableCell355";
            this.xrTableCell355.StylePriority.UseBorders = false;
            this.xrTableCell355.Weight = 0.11161843716525768D;
            // 
            // xrTableCell356
            // 
            this.xrTableCell356.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell356.Dpi = 254F;
            this.xrTableCell356.Name = "xrTableCell356";
            this.xrTableCell356.StylePriority.UseBorders = false;
            this.xrTableCell356.StylePriority.UseTextAlignment = false;
            this.xrTableCell356.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell356.Weight = 0.30993814107294138D;
            // 
            // xrTableCell427
            // 
            this.xrTableCell427.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell427.Dpi = 254F;
            this.xrTableCell427.Name = "xrTableCell427";
            this.xrTableCell427.StylePriority.UseBorders = false;
            this.xrTableCell427.StylePriority.UseTextAlignment = false;
            this.xrTableCell427.Text = "  其他\t1  正常\t2  异常";
            this.xrTableCell427.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell427.Weight = 0.56369832195615155D;
            // 
            // xrTableCell428
            // 
            this.xrTableCell428.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell428.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_B超其他异常});
            this.xrTableCell428.Dpi = 254F;
            this.xrTableCell428.Name = "xrTableCell428";
            this.xrTableCell428.StylePriority.UseBorders = false;
            this.xrTableCell428.Weight = 0.88797559382246827D;
            // 
            // xrLabel_B超其他异常
            // 
            this.xrLabel_B超其他异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_B超其他异常.Dpi = 254F;
            this.xrLabel_B超其他异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_B超其他异常.Name = "xrLabel_B超其他异常";
            this.xrLabel_B超其他异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_B超其他异常.SizeF = new System.Drawing.SizeF(327.9126F, 49.95313F);
            this.xrLabel_B超其他异常.StylePriority.UseBorders = false;
            this.xrLabel_B超其他异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_B超其他异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell357
            // 
            this.xrTableCell357.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell357.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_B超其他});
            this.xrTableCell357.Dpi = 254F;
            this.xrTableCell357.Name = "xrTableCell357";
            this.xrTableCell357.StylePriority.UseBorders = false;
            this.xrTableCell357.Weight = 0.12676950598318121D;
            // 
            // xrLabel_B超其他
            // 
            this.xrLabel_B超其他.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_B超其他.Dpi = 254F;
            this.xrLabel_B超其他.LocationFloat = new DevExpress.Utils.PointFloat(48.91235F, 8F);
            this.xrLabel_B超其他.Name = "xrLabel_B超其他";
            this.xrLabel_B超其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_B超其他.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_B超其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell358
            // 
            this.xrTableCell358.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell358.Dpi = 254F;
            this.xrTableCell358.Name = "xrTableCell358";
            this.xrTableCell358.StylePriority.UseBorders = false;
            this.xrTableCell358.Weight = 0.19876670037244218D;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell351,
            this.xrTableCell352,
            this.xrTableCell423,
            this.xrTableCell426,
            this.xrTableCell353,
            this.xrTableCell354});
            this.xrTableRow68.Dpi = 254F;
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 1D;
            // 
            // xrTableCell351
            // 
            this.xrTableCell351.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell351.Dpi = 254F;
            this.xrTableCell351.Name = "xrTableCell351";
            this.xrTableCell351.StylePriority.UseBorders = false;
            this.xrTableCell351.Weight = 0.11161843716525768D;
            // 
            // xrTableCell352
            // 
            this.xrTableCell352.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell352.Dpi = 254F;
            this.xrTableCell352.Name = "xrTableCell352";
            this.xrTableCell352.StylePriority.UseBorders = false;
            this.xrTableCell352.StylePriority.UseTextAlignment = false;
            this.xrTableCell352.Text = "  宫颈涂片*";
            this.xrTableCell352.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell352.Weight = 0.30993814107294138D;
            // 
            // xrTableCell423
            // 
            this.xrTableCell423.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell423.Dpi = 254F;
            this.xrTableCell423.Name = "xrTableCell423";
            this.xrTableCell423.StylePriority.UseBorders = false;
            this.xrTableCell423.StylePriority.UseTextAlignment = false;
            this.xrTableCell423.Text = "  1  正常\t2  异常";
            this.xrTableCell423.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell423.Weight = 0.34913142522249607D;
            // 
            // xrTableCell426
            // 
            this.xrTableCell426.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell426.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫颈涂片异常});
            this.xrTableCell426.Dpi = 254F;
            this.xrTableCell426.Name = "xrTableCell426";
            this.xrTableCell426.StylePriority.UseBorders = false;
            this.xrTableCell426.Weight = 1.1025422233934341D;
            // 
            // xrLabel_宫颈涂片异常
            // 
            this.xrLabel_宫颈涂片异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_宫颈涂片异常.Dpi = 254F;
            this.xrLabel_宫颈涂片异常.LocationFloat = new DevExpress.Utils.PointFloat(0.0001291911F, 0F);
            this.xrLabel_宫颈涂片异常.Name = "xrLabel_宫颈涂片异常";
            this.xrLabel_宫颈涂片异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_宫颈涂片异常.SizeF = new System.Drawing.SizeF(523.9897F, 49.95313F);
            this.xrLabel_宫颈涂片异常.StylePriority.UseBorders = false;
            this.xrLabel_宫颈涂片异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_宫颈涂片异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell353
            // 
            this.xrTableCell353.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell353.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫颈涂片});
            this.xrTableCell353.Dpi = 254F;
            this.xrTableCell353.Name = "xrTableCell353";
            this.xrTableCell353.StylePriority.UseBorders = false;
            this.xrTableCell353.Weight = 0.12676977314587062D;
            // 
            // xrLabel_宫颈涂片
            // 
            this.xrLabel_宫颈涂片.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_宫颈涂片.Dpi = 254F;
            this.xrLabel_宫颈涂片.LocationFloat = new DevExpress.Utils.PointFloat(48.91252F, 8F);
            this.xrLabel_宫颈涂片.Name = "xrLabel_宫颈涂片";
            this.xrLabel_宫颈涂片.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_宫颈涂片.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_宫颈涂片.StylePriority.UseBorders = false;
            // 
            // xrTableCell354
            // 
            this.xrTableCell354.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell354.Dpi = 254F;
            this.xrTableCell354.Name = "xrTableCell354";
            this.xrTableCell354.StylePriority.UseBorders = false;
            this.xrTableCell354.Weight = 0.19876670037244218D;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell339,
            this.xrTableCell340,
            this.xrTable_辅助检查其他,
            this.xrTableCell342});
            this.xrTableRow65.Dpi = 254F;
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 1D;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell339.Dpi = 254F;
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.StylePriority.UseBorders = false;
            this.xrTableCell339.Weight = 0.11161843716525768D;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell340.Dpi = 254F;
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.StylePriority.UseBorders = false;
            this.xrTableCell340.StylePriority.UseTextAlignment = false;
            this.xrTableCell340.Text = "  其        他*";
            this.xrTableCell340.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell340.Weight = 0.30993814107294138D;
            // 
            // xrTable_辅助检查其他
            // 
            this.xrTable_辅助检查其他.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_辅助检查其他.Dpi = 254F;
            this.xrTable_辅助检查其他.Name = "xrTable_辅助检查其他";
            this.xrTable_辅助检查其他.StylePriority.UseBorders = false;
            this.xrTable_辅助检查其他.Weight = 1.5784434217618009D;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell342.Dpi = 254F;
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.StylePriority.UseBorders = false;
            this.xrTableCell342.Weight = 0.19876670037244218D;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell347,
            this.xrTableCell348,
            this.xrTableCell349,
            this.xrTableCell350});
            this.xrTableRow67.Dpi = 254F;
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 1D;
            // 
            // xrTableCell347
            // 
            this.xrTableCell347.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell347.Dpi = 254F;
            this.xrTableCell347.Name = "xrTableCell347";
            this.xrTableCell347.StylePriority.UseBorders = false;
            this.xrTableCell347.Weight = 0.11161843716525768D;
            // 
            // xrTableCell348
            // 
            this.xrTableCell348.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell348.Dpi = 254F;
            this.xrTableCell348.Name = "xrTableCell348";
            this.xrTableCell348.StylePriority.UseBorders = false;
            this.xrTableCell348.StylePriority.UseTextAlignment = false;
            this.xrTableCell348.Text = "脑血管疾病";
            this.xrTableCell348.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell348.Weight = 0.30993814107294138D;
            // 
            // xrTableCell349
            // 
            this.xrTableCell349.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell349.Dpi = 254F;
            this.xrTableCell349.Name = "xrTableCell349";
            this.xrTableCell349.StylePriority.UseBorders = false;
            this.xrTableCell349.StylePriority.UseTextAlignment = false;
            this.xrTableCell349.Text = "  1  未发现      2  缺血性卒中      3  脑出血      4  蛛网膜下腔出血      5  短暂性脑缺血发作";
            this.xrTableCell349.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell349.Weight = 1.5784434217618009D;
            // 
            // xrTableCell350
            // 
            this.xrTableCell350.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell350.Dpi = 254F;
            this.xrTableCell350.Name = "xrTableCell350";
            this.xrTableCell350.StylePriority.UseBorders = false;
            this.xrTableCell350.Weight = 0.19876670037244218D;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell343,
            this.xrTableCell344,
            this.xrTableCell429,
            this.xrTableCell430,
            this.xrTableCell345,
            this.xrTableCell346});
            this.xrTableRow66.Dpi = 254F;
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 1D;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell343.Dpi = 254F;
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.StylePriority.UseBorders = false;
            this.xrTableCell343.Weight = 0.11161843716525768D;
            // 
            // xrTableCell344
            // 
            this.xrTableCell344.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell344.Dpi = 254F;
            this.xrTableCell344.Name = "xrTableCell344";
            this.xrTableCell344.StylePriority.UseBorders = false;
            this.xrTableCell344.StylePriority.UseTextAlignment = false;
            this.xrTableCell344.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell344.Weight = 0.30993814107294138D;
            // 
            // xrTableCell429
            // 
            this.xrTableCell429.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell429.Dpi = 254F;
            this.xrTableCell429.Name = "xrTableCell429";
            this.xrTableCell429.StylePriority.UseBorders = false;
            this.xrTableCell429.StylePriority.UseTextAlignment = false;
            this.xrTableCell429.Text = "  6  其他";
            this.xrTableCell429.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell429.Weight = 0.15140325012057609D;
            // 
            // xrTableCell430
            // 
            this.xrTableCell430.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell430.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_脑血管疾病其他});
            this.xrTableCell430.Dpi = 254F;
            this.xrTableCell430.Name = "xrTableCell430";
            this.xrTableCell430.StylePriority.UseBorders = false;
            this.xrTableCell430.Weight = 0.57340167083644622D;
            // 
            // xrLabel_脑血管疾病其他
            // 
            this.xrLabel_脑血管疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_脑血管疾病其他.Dpi = 254F;
            this.xrLabel_脑血管疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0.0001291911F, 0F);
            this.xrLabel_脑血管疾病其他.Name = "xrLabel_脑血管疾病其他";
            this.xrLabel_脑血管疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_脑血管疾病其他.SizeF = new System.Drawing.SizeF(523.9897F, 49.95313F);
            this.xrLabel_脑血管疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_脑血管疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_脑血管疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell345
            // 
            this.xrTableCell345.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell345.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel64,
            this.xrLabel_脑血管疾病1,
            this.xrLabel_脑血管疾病2,
            this.xrLabel58,
            this.xrLabel_脑血管疾病3,
            this.xrLabel54,
            this.xrLabel_脑血管疾病4,
            this.xrLabel50,
            this.xrLabel_脑血管疾病5});
            this.xrTableCell345.Dpi = 254F;
            this.xrTableCell345.Name = "xrTableCell345";
            this.xrTableCell345.StylePriority.UseBorders = false;
            this.xrTableCell345.Weight = 0.85363850080477854D;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel64.Dpi = 254F;
            this.xrLabel64.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(419.6134F, 8.000093F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "/";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_脑血管疾病1
            // 
            this.xrLabel_脑血管疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脑血管疾病1.Dpi = 254F;
            this.xrLabel_脑血管疾病1.LocationFloat = new DevExpress.Utils.PointFloat(377.539F, 8.000157F);
            this.xrLabel_脑血管疾病1.Name = "xrLabel_脑血管疾病1";
            this.xrLabel_脑血管疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_脑血管疾病1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_脑血管疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel_脑血管疾病2
            // 
            this.xrLabel_脑血管疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脑血管疾病2.Dpi = 254F;
            this.xrLabel_脑血管疾病2.LocationFloat = new DevExpress.Utils.PointFloat(461.5464F, 8.000029F);
            this.xrLabel_脑血管疾病2.Name = "xrLabel_脑血管疾病2";
            this.xrLabel_脑血管疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_脑血管疾病2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_脑血管疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel58
            // 
            this.xrLabel58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel58.Dpi = 254F;
            this.xrLabel58.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(503.4796F, 8.000029F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel58.StylePriority.UseBorders = false;
            this.xrLabel58.StylePriority.UseFont = false;
            this.xrLabel58.StylePriority.UseTextAlignment = false;
            this.xrLabel58.Text = "/";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_脑血管疾病3
            // 
            this.xrLabel_脑血管疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脑血管疾病3.Dpi = 254F;
            this.xrLabel_脑血管疾病3.LocationFloat = new DevExpress.Utils.PointFloat(545.413F, 8.000029F);
            this.xrLabel_脑血管疾病3.Name = "xrLabel_脑血管疾病3";
            this.xrLabel_脑血管疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_脑血管疾病3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_脑血管疾病3.StylePriority.UseBorders = false;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel54.Dpi = 254F;
            this.xrLabel54.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(587.3461F, 8.000029F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "/";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_脑血管疾病4
            // 
            this.xrLabel_脑血管疾病4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脑血管疾病4.Dpi = 254F;
            this.xrLabel_脑血管疾病4.LocationFloat = new DevExpress.Utils.PointFloat(629.2789F, 8.000029F);
            this.xrLabel_脑血管疾病4.Name = "xrLabel_脑血管疾病4";
            this.xrLabel_脑血管疾病4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_脑血管疾病4.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_脑血管疾病4.StylePriority.UseBorders = false;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel50.Dpi = 254F;
            this.xrLabel50.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(671.212F, 7.999964F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel50.StylePriority.UseBorders = false;
            this.xrLabel50.StylePriority.UseFont = false;
            this.xrLabel50.StylePriority.UseTextAlignment = false;
            this.xrLabel50.Text = "/";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_脑血管疾病5
            // 
            this.xrLabel_脑血管疾病5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脑血管疾病5.Dpi = 254F;
            this.xrLabel_脑血管疾病5.LocationFloat = new DevExpress.Utils.PointFloat(713.1451F, 8F);
            this.xrLabel_脑血管疾病5.Name = "xrLabel_脑血管疾病5";
            this.xrLabel_脑血管疾病5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_脑血管疾病5.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_脑血管疾病5.StylePriority.UseBorders = false;
            // 
            // xrTableCell346
            // 
            this.xrTableCell346.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell346.Dpi = 254F;
            this.xrTableCell346.Name = "xrTableCell346";
            this.xrTableCell346.StylePriority.UseBorders = false;
            this.xrTableCell346.Weight = 0.19876670037244218D;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell331,
            this.xrTableCell332,
            this.xrTableCell333,
            this.xrTableCell334});
            this.xrTableRow63.Dpi = 254F;
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 1D;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell331.Dpi = 254F;
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.StylePriority.UseBorders = false;
            this.xrTableCell331.Text = "现";
            this.xrTableCell331.Weight = 0.11161843716525768D;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell332.Dpi = 254F;
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.StylePriority.UseBorders = false;
            this.xrTableCell332.StylePriority.UseTextAlignment = false;
            this.xrTableCell332.Text = "肾脏疾病";
            this.xrTableCell332.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell332.Weight = 0.30993814107294138D;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell333.Dpi = 254F;
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.StylePriority.UseBorders = false;
            this.xrTableCell333.StylePriority.UseTextAlignment = false;
            this.xrTableCell333.Text = "  1  未发现      2  糖尿病肾病      3  肾功能衰竭      4  急性肾炎      5  慢性肾炎";
            this.xrTableCell333.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell333.Weight = 1.5784434217618009D;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell334.Dpi = 254F;
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.StylePriority.UseBorders = false;
            this.xrTableCell334.Weight = 0.19876670037244218D;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell335,
            this.xrTableCell336,
            this.xrTableCell431,
            this.xrTableCell432,
            this.xrTableCell337,
            this.xrTableCell338});
            this.xrTableRow64.Dpi = 254F;
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 1D;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell335.Dpi = 254F;
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.StylePriority.UseBorders = false;
            this.xrTableCell335.Text = "存";
            this.xrTableCell335.Weight = 0.11161843716525768D;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell336.Dpi = 254F;
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.StylePriority.UseBorders = false;
            this.xrTableCell336.StylePriority.UseTextAlignment = false;
            this.xrTableCell336.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell336.Weight = 0.30993814107294138D;
            // 
            // xrTableCell431
            // 
            this.xrTableCell431.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell431.Dpi = 254F;
            this.xrTableCell431.Name = "xrTableCell431";
            this.xrTableCell431.StylePriority.UseBorders = false;
            this.xrTableCell431.StylePriority.UseTextAlignment = false;
            this.xrTableCell431.Text = "  6  其他";
            this.xrTableCell431.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell431.Weight = 0.15140325012057609D;
            // 
            // xrTableCell432
            // 
            this.xrTableCell432.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell432.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_肾脏疾病其他});
            this.xrTableCell432.Dpi = 254F;
            this.xrTableCell432.Name = "xrTableCell432";
            this.xrTableCell432.StylePriority.UseBorders = false;
            this.xrTableCell432.Weight = 0.57340167083644622D;
            // 
            // xrLabel_肾脏疾病其他
            // 
            this.xrLabel_肾脏疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_肾脏疾病其他.Dpi = 254F;
            this.xrLabel_肾脏疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_肾脏疾病其他.Name = "xrLabel_肾脏疾病其他";
            this.xrLabel_肾脏疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_肾脏疾病其他.SizeF = new System.Drawing.SizeF(523.9897F, 49.95313F);
            this.xrLabel_肾脏疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_肾脏疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_肾脏疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell337.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel65,
            this.xrLabel_肾脏疾病1,
            this.xrLabel_肾脏疾病2,
            this.xrLabel59,
            this.xrLabel_肾脏疾病3,
            this.xrLabel55,
            this.xrLabel_肾脏疾病4,
            this.xrLabel51,
            this.xrLabel_肾脏疾病5});
            this.xrTableCell337.Dpi = 254F;
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.StylePriority.UseBorders = false;
            this.xrTableCell337.Weight = 0.85363850080477854D;
            // 
            // xrLabel65
            // 
            this.xrLabel65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel65.Dpi = 254F;
            this.xrLabel65.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(419.4721F, 7.999964F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel65.StylePriority.UseBorders = false;
            this.xrLabel65.StylePriority.UseFont = false;
            this.xrLabel65.StylePriority.UseTextAlignment = false;
            this.xrLabel65.Text = "/";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_肾脏疾病1
            // 
            this.xrLabel_肾脏疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肾脏疾病1.Dpi = 254F;
            this.xrLabel_肾脏疾病1.LocationFloat = new DevExpress.Utils.PointFloat(377.5391F, 7.999964F);
            this.xrLabel_肾脏疾病1.Name = "xrLabel_肾脏疾病1";
            this.xrLabel_肾脏疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_肾脏疾病1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_肾脏疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel_肾脏疾病2
            // 
            this.xrLabel_肾脏疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肾脏疾病2.Dpi = 254F;
            this.xrLabel_肾脏疾病2.LocationFloat = new DevExpress.Utils.PointFloat(461.5464F, 8.000029F);
            this.xrLabel_肾脏疾病2.Name = "xrLabel_肾脏疾病2";
            this.xrLabel_肾脏疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_肾脏疾病2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_肾脏疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel59.Dpi = 254F;
            this.xrLabel59.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(503.4796F, 8.000029F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel59.StylePriority.UseBorders = false;
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = "/";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_肾脏疾病3
            // 
            this.xrLabel_肾脏疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肾脏疾病3.Dpi = 254F;
            this.xrLabel_肾脏疾病3.LocationFloat = new DevExpress.Utils.PointFloat(545.413F, 7.999964F);
            this.xrLabel_肾脏疾病3.Name = "xrLabel_肾脏疾病3";
            this.xrLabel_肾脏疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_肾脏疾病3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_肾脏疾病3.StylePriority.UseBorders = false;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel55.Dpi = 254F;
            this.xrLabel55.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(587.3461F, 7.999964F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel55.StylePriority.UseBorders = false;
            this.xrLabel55.StylePriority.UseFont = false;
            this.xrLabel55.StylePriority.UseTextAlignment = false;
            this.xrLabel55.Text = "/";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_肾脏疾病4
            // 
            this.xrLabel_肾脏疾病4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肾脏疾病4.Dpi = 254F;
            this.xrLabel_肾脏疾病4.LocationFloat = new DevExpress.Utils.PointFloat(629.2789F, 7.999964F);
            this.xrLabel_肾脏疾病4.Name = "xrLabel_肾脏疾病4";
            this.xrLabel_肾脏疾病4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_肾脏疾病4.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_肾脏疾病4.StylePriority.UseBorders = false;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.Dpi = 254F;
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(671.2119F, 7.999964F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "/";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_肾脏疾病5
            // 
            this.xrLabel_肾脏疾病5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肾脏疾病5.Dpi = 254F;
            this.xrLabel_肾脏疾病5.LocationFloat = new DevExpress.Utils.PointFloat(713.1448F, 8F);
            this.xrLabel_肾脏疾病5.Name = "xrLabel_肾脏疾病5";
            this.xrLabel_肾脏疾病5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_肾脏疾病5.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_肾脏疾病5.StylePriority.UseBorders = false;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell338.Dpi = 254F;
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.StylePriority.UseBorders = false;
            this.xrTableCell338.Weight = 0.19876670037244218D;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell327,
            this.xrTableCell328,
            this.xrTableCell329,
            this.xrTableCell330});
            this.xrTableRow62.Dpi = 254F;
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 1D;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell327.Dpi = 254F;
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.StylePriority.UseBorders = false;
            this.xrTableCell327.Text = "主";
            this.xrTableCell327.Weight = 0.11161843716525768D;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell328.Dpi = 254F;
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.StylePriority.UseBorders = false;
            this.xrTableCell328.StylePriority.UseTextAlignment = false;
            this.xrTableCell328.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell328.Weight = 0.30993814107294138D;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell329.Dpi = 254F;
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.StylePriority.UseBorders = false;
            this.xrTableCell329.StylePriority.UseTextAlignment = false;
            this.xrTableCell329.Text = " 1  未发现     2  心肌梗死     3  心绞痛     4  冠状动脉血运重建     5  充血性心力衰竭    6  心";
            this.xrTableCell329.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell329.Weight = 1.5784434217618009D;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell330.Dpi = 254F;
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.StylePriority.UseBorders = false;
            this.xrTableCell330.Weight = 0.19876670037244218D;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell319,
            this.xrTableCell320,
            this.xrTableCell433,
            this.xrTableCell321,
            this.xrTableCell322});
            this.xrTableRow60.Dpi = 254F;
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 1D;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell319.Dpi = 254F;
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.StylePriority.UseBorders = false;
            this.xrTableCell319.Text = "要";
            this.xrTableCell319.Weight = 0.11161843716525768D;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell320.Dpi = 254F;
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.StylePriority.UseBorders = false;
            this.xrTableCell320.StylePriority.UseTextAlignment = false;
            this.xrTableCell320.Text = "心血管疾病";
            this.xrTableCell320.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell320.Weight = 0.30993814107294138D;
            // 
            // xrTableCell433
            // 
            this.xrTableCell433.Dpi = 254F;
            this.xrTableCell433.Name = "xrTableCell433";
            this.xrTableCell433.StylePriority.UseTextAlignment = false;
            this.xrTableCell433.Text = " 前区疼痛    7  高血压    8  夹层动脉瘤    9  动脉闭塞性疾病    10  其他";
            this.xrTableCell433.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell433.Weight = 1.2107821267254424D;
            // 
            // xrTableCell321
            // 
            this.xrTableCell321.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell321.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心血管疾病其他});
            this.xrTableCell321.Dpi = 254F;
            this.xrTableCell321.Name = "xrTableCell321";
            this.xrTableCell321.StylePriority.UseBorders = false;
            this.xrTableCell321.Weight = 0.36766129503635853D;
            // 
            // xrLabel_心血管疾病其他
            // 
            this.xrLabel_心血管疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_心血管疾病其他.Dpi = 254F;
            this.xrLabel_心血管疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 0.0001831055F);
            this.xrLabel_心血管疾病其他.Name = "xrLabel_心血管疾病其他";
            this.xrLabel_心血管疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心血管疾病其他.SizeF = new System.Drawing.SizeF(310.9788F, 49.95313F);
            this.xrLabel_心血管疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_心血管疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_心血管疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell322
            // 
            this.xrTableCell322.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell322.Dpi = 254F;
            this.xrTableCell322.Name = "xrTableCell322";
            this.xrTableCell322.StylePriority.UseBorders = false;
            this.xrTableCell322.Weight = 0.19876670037244218D;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell311,
            this.xrTableCell312,
            this.xrTableCell313,
            this.xrTableCell314});
            this.xrTableRow58.Dpi = 254F;
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 1D;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell311.Dpi = 254F;
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.StylePriority.UseBorders = false;
            this.xrTableCell311.Text = "健";
            this.xrTableCell311.Weight = 0.11161843716525768D;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell312.Dpi = 254F;
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.StylePriority.UseBorders = false;
            this.xrTableCell312.StylePriority.UseTextAlignment = false;
            this.xrTableCell312.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell312.Weight = 0.30993814107294138D;
            // 
            // xrTableCell313
            // 
            this.xrTableCell313.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell313.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel66,
            this.xrLabel_心血管疾病1,
            this.xrLabel_心血管疾病2,
            this.xrLabel69,
            this.xrLabel_心血管疾病3,
            this.xrLabel71,
            this.xrLabel_心血管疾病4,
            this.xrLabel73,
            this.xrLabel_心血管疾病5});
            this.xrTableCell313.Dpi = 254F;
            this.xrTableCell313.Name = "xrTableCell313";
            this.xrTableCell313.StylePriority.UseBorders = false;
            this.xrTableCell313.Weight = 1.5784434217618009D;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel66.Dpi = 254F;
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(1057.758F, 8.000029F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel66.StylePriority.UseBorders = false;
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.StylePriority.UseTextAlignment = false;
            this.xrLabel66.Text = "/";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心血管疾病1
            // 
            this.xrLabel_心血管疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心血管疾病1.Dpi = 254F;
            this.xrLabel_心血管疾病1.LocationFloat = new DevExpress.Utils.PointFloat(1007.758F, 8.000029F);
            this.xrLabel_心血管疾病1.Name = "xrLabel_心血管疾病1";
            this.xrLabel_心血管疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心血管疾病1.SizeF = new System.Drawing.SizeF(50F, 47.83679F);
            this.xrLabel_心血管疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel_心血管疾病2
            // 
            this.xrLabel_心血管疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心血管疾病2.Dpi = 254F;
            this.xrLabel_心血管疾病2.LocationFloat = new DevExpress.Utils.PointFloat(1099.692F, 8.000029F);
            this.xrLabel_心血管疾病2.Name = "xrLabel_心血管疾病2";
            this.xrLabel_心血管疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心血管疾病2.SizeF = new System.Drawing.SizeF(50F, 47.83679F);
            this.xrLabel_心血管疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel69.Dpi = 254F;
            this.xrLabel69.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(1149.692F, 8.000029F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel69.StylePriority.UseBorders = false;
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.Text = "/";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心血管疾病3
            // 
            this.xrLabel_心血管疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心血管疾病3.Dpi = 254F;
            this.xrLabel_心血管疾病3.LocationFloat = new DevExpress.Utils.PointFloat(1191.625F, 8.000029F);
            this.xrLabel_心血管疾病3.Name = "xrLabel_心血管疾病3";
            this.xrLabel_心血管疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心血管疾病3.SizeF = new System.Drawing.SizeF(50F, 47.83679F);
            this.xrLabel_心血管疾病3.StylePriority.UseBorders = false;
            // 
            // xrLabel71
            // 
            this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel71.Dpi = 254F;
            this.xrLabel71.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(1241.625F, 8.000029F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel71.StylePriority.UseBorders = false;
            this.xrLabel71.StylePriority.UseFont = false;
            this.xrLabel71.StylePriority.UseTextAlignment = false;
            this.xrLabel71.Text = "/";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心血管疾病4
            // 
            this.xrLabel_心血管疾病4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心血管疾病4.Dpi = 254F;
            this.xrLabel_心血管疾病4.LocationFloat = new DevExpress.Utils.PointFloat(1283.559F, 8F);
            this.xrLabel_心血管疾病4.Name = "xrLabel_心血管疾病4";
            this.xrLabel_心血管疾病4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心血管疾病4.SizeF = new System.Drawing.SizeF(50F, 47.83679F);
            this.xrLabel_心血管疾病4.StylePriority.UseBorders = false;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel73.Dpi = 254F;
            this.xrLabel73.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(1333.559F, 8.000046F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel73.StylePriority.UseBorders = false;
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "/";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心血管疾病5
            // 
            this.xrLabel_心血管疾病5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心血管疾病5.Dpi = 254F;
            this.xrLabel_心血管疾病5.LocationFloat = new DevExpress.Utils.PointFloat(1376F, 8.000046F);
            this.xrLabel_心血管疾病5.Name = "xrLabel_心血管疾病5";
            this.xrLabel_心血管疾病5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心血管疾病5.SizeF = new System.Drawing.SizeF(50F, 47.83679F);
            this.xrLabel_心血管疾病5.StylePriority.UseBorders = false;
            this.xrLabel_心血管疾病5.StylePriority.UseTextAlignment = false;
            this.xrLabel_心血管疾病5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell314.Dpi = 254F;
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.StylePriority.UseBorders = false;
            this.xrTableCell314.Weight = 0.19876670037244218D;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell303,
            this.xrTableCell304,
            this.xrTableCell305,
            this.xrTableCell306});
            this.xrTableRow56.Dpi = 254F;
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 1D;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell303.Dpi = 254F;
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.StylePriority.UseBorders = false;
            this.xrTableCell303.Text = "康";
            this.xrTableCell303.Weight = 0.11161843716525768D;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell304.Dpi = 254F;
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.StylePriority.UseBorders = false;
            this.xrTableCell304.StylePriority.UseTextAlignment = false;
            this.xrTableCell304.Text = "眼部疾病";
            this.xrTableCell304.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell304.Weight = 0.30993814107294138D;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell305.Dpi = 254F;
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.StylePriority.UseBorders = false;
            this.xrTableCell305.StylePriority.UseTextAlignment = false;
            this.xrTableCell305.Text = "  1  未发现        2  视网膜出血或渗出        3  视乳头水肿        4  白内障";
            this.xrTableCell305.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell305.Weight = 1.5784434217618009D;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell306.Dpi = 254F;
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.StylePriority.UseBorders = false;
            this.xrTableCell306.Weight = 0.19876670037244218D;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell299,
            this.xrTableCell300,
            this.xrTableCell434,
            this.xrTableCell435,
            this.xrTableCell301,
            this.xrTableCell302});
            this.xrTableRow55.Dpi = 254F;
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 1D;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell299.Dpi = 254F;
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.StylePriority.UseBorders = false;
            this.xrTableCell299.Text = "问";
            this.xrTableCell299.Weight = 0.11161843716525768D;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell300.Dpi = 254F;
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.StylePriority.UseBorders = false;
            this.xrTableCell300.StylePriority.UseTextAlignment = false;
            this.xrTableCell300.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell300.Weight = 0.30993814107294138D;
            // 
            // xrTableCell434
            // 
            this.xrTableCell434.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell434.Dpi = 254F;
            this.xrTableCell434.Name = "xrTableCell434";
            this.xrTableCell434.StylePriority.UseBorders = false;
            this.xrTableCell434.StylePriority.UseTextAlignment = false;
            this.xrTableCell434.Text = "  5  其他";
            this.xrTableCell434.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell434.Weight = 0.15140325012057609D;
            // 
            // xrTableCell435
            // 
            this.xrTableCell435.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell435.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_眼部疾病其他});
            this.xrTableCell435.Dpi = 254F;
            this.xrTableCell435.Name = "xrTableCell435";
            this.xrTableCell435.StylePriority.UseBorders = false;
            this.xrTableCell435.Weight = 0.57340193799913552D;
            // 
            // xrLabel_眼部疾病其他
            // 
            this.xrLabel_眼部疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_眼部疾病其他.Dpi = 254F;
            this.xrLabel_眼部疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0.0001291911F, 0F);
            this.xrLabel_眼部疾病其他.Name = "xrLabel_眼部疾病其他";
            this.xrLabel_眼部疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_眼部疾病其他.SizeF = new System.Drawing.SizeF(523.9897F, 49.95313F);
            this.xrLabel_眼部疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_眼部疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_眼部疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell301.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_眼部疾病1,
            this.xrLabel84,
            this.xrLabel_眼部疾病2,
            this.xrLabel78,
            this.xrLabel_眼部疾病3});
            this.xrTableCell301.Dpi = 254F;
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.StylePriority.UseBorders = false;
            this.xrTableCell301.Weight = 0.85363823364208924D;
            // 
            // xrLabel_眼部疾病1
            // 
            this.xrLabel_眼部疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_眼部疾病1.Dpi = 254F;
            this.xrLabel_眼部疾病1.LocationFloat = new DevExpress.Utils.PointFloat(545.4119F, 8.000029F);
            this.xrLabel_眼部疾病1.Name = "xrLabel_眼部疾病1";
            this.xrLabel_眼部疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_眼部疾病1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_眼部疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel84.Dpi = 254F;
            this.xrLabel84.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(587.345F, 8.000029F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.StylePriority.UseFont = false;
            this.xrLabel84.StylePriority.UseTextAlignment = false;
            this.xrLabel84.Text = "/";
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_眼部疾病2
            // 
            this.xrLabel_眼部疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_眼部疾病2.Dpi = 254F;
            this.xrLabel_眼部疾病2.LocationFloat = new DevExpress.Utils.PointFloat(629.2781F, 7.999899F);
            this.xrLabel_眼部疾病2.Name = "xrLabel_眼部疾病2";
            this.xrLabel_眼部疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_眼部疾病2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_眼部疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel78.Dpi = 254F;
            this.xrLabel78.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(671.2114F, 8.000029F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel78.StylePriority.UseBorders = false;
            this.xrLabel78.StylePriority.UseFont = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "/";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_眼部疾病3
            // 
            this.xrLabel_眼部疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_眼部疾病3.Dpi = 254F;
            this.xrLabel_眼部疾病3.LocationFloat = new DevExpress.Utils.PointFloat(713.1446F, 8F);
            this.xrLabel_眼部疾病3.Name = "xrLabel_眼部疾病3";
            this.xrLabel_眼部疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_眼部疾病3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_眼部疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell302.Dpi = 254F;
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.StylePriority.UseBorders = false;
            this.xrTableCell302.Weight = 0.19876670037244218D;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell287,
            this.xrTableCell288,
            this.xrTableCell436,
            this.xrTableCell289,
            this.xrTableCell290});
            this.xrTableRow52.Dpi = 254F;
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 1D;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell287.Dpi = 254F;
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.StylePriority.UseBorders = false;
            this.xrTableCell287.Text = "题";
            this.xrTableCell287.Weight = 0.11161843716525768D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell288.Dpi = 254F;
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.StylePriority.UseBorders = false;
            this.xrTableCell288.StylePriority.UseTextAlignment = false;
            this.xrTableCell288.Text = "神经系统其他疾";
            this.xrTableCell288.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell288.Weight = 0.30993814107294138D;
            // 
            // xrTableCell436
            // 
            this.xrTableCell436.Dpi = 254F;
            this.xrTableCell436.Name = "xrTableCell436";
            this.xrTableCell436.StylePriority.UseTextAlignment = false;
            this.xrTableCell436.Text = "  1  未发现   2  阿尔茨海默症（老年性痴呆）   3  帕金森症   4  其他";
            this.xrTableCell436.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell436.Weight = 1.17008954224211D;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell289.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_神经系统其他疾病其他});
            this.xrTableCell289.Dpi = 254F;
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.StylePriority.UseBorders = false;
            this.xrTableCell289.Weight = 0.40835387951969065D;
            // 
            // xrLabel_神经系统其他疾病其他
            // 
            this.xrLabel_神经系统其他疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_神经系统其他疾病其他.Dpi = 254F;
            this.xrLabel_神经系统其他疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_神经系统其他疾病其他.Name = "xrLabel_神经系统其他疾病其他";
            this.xrLabel_神经系统其他疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_神经系统其他疾病其他.SizeF = new System.Drawing.SizeF(348.1648F, 49.95313F);
            this.xrLabel_神经系统其他疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_神经系统其他疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_神经系统其他疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell290.Dpi = 254F;
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.StylePriority.UseBorders = false;
            this.xrTableCell290.Weight = 0.19876670037244218D;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell291,
            this.xrTableCell292,
            this.xrTableCell293,
            this.xrTableCell294});
            this.xrTableRow53.Dpi = 254F;
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1D;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell291.Dpi = 254F;
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.StylePriority.UseBorders = false;
            this.xrTableCell291.Weight = 0.11161843716525768D;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell292.Dpi = 254F;
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.StylePriority.UseBorders = false;
            this.xrTableCell292.StylePriority.UseTextAlignment = false;
            this.xrTableCell292.Text = "病";
            this.xrTableCell292.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell292.Weight = 0.30993814107294138D;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell293.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_神经系统其他疾病1,
            this.xrLabel85,
            this.xrLabel_神经系统其他疾病2,
            this.xrLabel79,
            this.xrLabel_神经系统其他疾病3});
            this.xrTableCell293.Dpi = 254F;
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.StylePriority.UseBorders = false;
            this.xrTableCell293.Weight = 1.5784434217618009D;
            // 
            // xrLabel_神经系统其他疾病1
            // 
            this.xrLabel_神经系统其他疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_神经系统其他疾病1.Dpi = 254F;
            this.xrLabel_神经系统其他疾病1.LocationFloat = new DevExpress.Utils.PointFloat(1207.759F, 8.000287F);
            this.xrLabel_神经系统其他疾病1.Name = "xrLabel_神经系统其他疾病1";
            this.xrLabel_神经系统其他疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_神经系统其他疾病1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_神经系统其他疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel85
            // 
            this.xrLabel85.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel85.Dpi = 254F;
            this.xrLabel85.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel85.LocationFloat = new DevExpress.Utils.PointFloat(1249.692F, 8.000157F);
            this.xrLabel85.Name = "xrLabel85";
            this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel85.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel85.StylePriority.UseBorders = false;
            this.xrLabel85.StylePriority.UseFont = false;
            this.xrLabel85.StylePriority.UseTextAlignment = false;
            this.xrLabel85.Text = "/";
            this.xrLabel85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_神经系统其他疾病2
            // 
            this.xrLabel_神经系统其他疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_神经系统其他疾病2.Dpi = 254F;
            this.xrLabel_神经系统其他疾病2.LocationFloat = new DevExpress.Utils.PointFloat(1291.625F, 7.999899F);
            this.xrLabel_神经系统其他疾病2.Name = "xrLabel_神经系统其他疾病2";
            this.xrLabel_神经系统其他疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_神经系统其他疾病2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_神经系统其他疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel79
            // 
            this.xrLabel79.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel79.Dpi = 254F;
            this.xrLabel79.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(1333.558F, 7.999899F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel79.StylePriority.UseBorders = false;
            this.xrLabel79.StylePriority.UseFont = false;
            this.xrLabel79.StylePriority.UseTextAlignment = false;
            this.xrLabel79.Text = "/";
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_神经系统其他疾病3
            // 
            this.xrLabel_神经系统其他疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_神经系统其他疾病3.Dpi = 254F;
            this.xrLabel_神经系统其他疾病3.LocationFloat = new DevExpress.Utils.PointFloat(1375.491F, 8F);
            this.xrLabel_神经系统其他疾病3.Name = "xrLabel_神经系统其他疾病3";
            this.xrLabel_神经系统其他疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_神经系统其他疾病3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_神经系统其他疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell294.Dpi = 254F;
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.StylePriority.UseBorders = false;
            this.xrTableCell294.Weight = 0.19876670037244218D;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell364,
            this.xrTableCell365,
            this.xrTableCell366,
            this.xrTableCell367});
            this.xrTableRow71.Dpi = 254F;
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Weight = 1D;
            // 
            // xrTableCell364
            // 
            this.xrTableCell364.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell364.Dpi = 254F;
            this.xrTableCell364.Name = "xrTableCell364";
            this.xrTableCell364.StylePriority.UseBorders = false;
            this.xrTableCell364.Weight = 0.11161843716525768D;
            // 
            // xrTableCell365
            // 
            this.xrTableCell365.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell365.Dpi = 254F;
            this.xrTableCell365.Name = "xrTableCell365";
            this.xrTableCell365.StylePriority.UseBorders = false;
            this.xrTableCell365.StylePriority.UseTextAlignment = false;
            this.xrTableCell365.Text = "其他系统疾病";
            this.xrTableCell365.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell365.Weight = 0.30993814107294138D;
            // 
            // xrTableCell366
            // 
            this.xrTableCell366.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell366.Dpi = 254F;
            this.xrTableCell366.Name = "xrTableCell366";
            this.xrTableCell366.StylePriority.UseBorders = false;
            this.xrTableCell366.StylePriority.UseTextAlignment = false;
            this.xrTableCell366.Text = "  1  未发现        2  糖尿病        3  慢性支气管炎        4  慢性阻塞性肺气肿        5  恶性肿瘤";
            this.xrTableCell366.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell366.Weight = 1.5784434217618009D;
            // 
            // xrTableCell367
            // 
            this.xrTableCell367.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell367.Dpi = 254F;
            this.xrTableCell367.Name = "xrTableCell367";
            this.xrTableCell367.StylePriority.UseBorders = false;
            this.xrTableCell367.Weight = 0.19876670037244218D;
            // 
            // xrTableRow73
            // 
            this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell372,
            this.xrTableCell373,
            this.xrTableCell437,
            this.xrTableCell438,
            this.xrTableCell374,
            this.xrTableCell375});
            this.xrTableRow73.Dpi = 254F;
            this.xrTableRow73.Name = "xrTableRow73";
            this.xrTableRow73.Weight = 1D;
            // 
            // xrTableCell372
            // 
            this.xrTableCell372.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell372.Dpi = 254F;
            this.xrTableCell372.Name = "xrTableCell372";
            this.xrTableCell372.StylePriority.UseBorders = false;
            this.xrTableCell372.Weight = 0.11161843716525768D;
            // 
            // xrTableCell373
            // 
            this.xrTableCell373.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell373.Dpi = 254F;
            this.xrTableCell373.Name = "xrTableCell373";
            this.xrTableCell373.StylePriority.UseBorders = false;
            this.xrTableCell373.StylePriority.UseTextAlignment = false;
            this.xrTableCell373.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell373.Weight = 0.30993814107294138D;
            // 
            // xrTableCell437
            // 
            this.xrTableCell437.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell437.Dpi = 254F;
            this.xrTableCell437.Name = "xrTableCell437";
            this.xrTableCell437.StylePriority.UseBorders = false;
            this.xrTableCell437.StylePriority.UseTextAlignment = false;
            this.xrTableCell437.Text = "  6  老年性骨关节病        7  其他";
            this.xrTableCell437.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell437.Weight = 0.5636983303141524D;
            // 
            // xrTableCell438
            // 
            this.xrTableCell438.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell438.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_其他系统疾病其他});
            this.xrTableCell438.Dpi = 254F;
            this.xrTableCell438.Name = "xrTableCell438";
            this.xrTableCell438.StylePriority.UseBorders = false;
            this.xrTableCell438.Weight = 0.57340086934837786D;
            // 
            // xrLabel_其他系统疾病其他
            // 
            this.xrLabel_其他系统疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_其他系统疾病其他.Dpi = 254F;
            this.xrLabel_其他系统疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.0001291911F);
            this.xrLabel_其他系统疾病其他.Name = "xrLabel_其他系统疾病其他";
            this.xrLabel_其他系统疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_其他系统疾病其他.SizeF = new System.Drawing.SizeF(523.9897F, 49.95313F);
            this.xrLabel_其他系统疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_其他系统疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_其他系统疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell374
            // 
            this.xrTableCell374.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell374.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_其他系统疾病1,
            this.xrLabel86,
            this.xrLabel_其他系统疾病2,
            this.xrLabel80,
            this.xrLabel_其他系统疾病3});
            this.xrTableCell374.Dpi = 254F;
            this.xrTableCell374.Name = "xrTableCell374";
            this.xrTableCell374.StylePriority.UseBorders = false;
            this.xrTableCell374.Weight = 0.44134422209927054D;
            // 
            // xrLabel_其他系统疾病1
            // 
            this.xrLabel_其他系统疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_其他系统疾病1.Dpi = 254F;
            this.xrLabel_其他系统疾病1.LocationFloat = new DevExpress.Utils.PointFloat(168.6471F, 7.999899F);
            this.xrLabel_其他系统疾病1.Name = "xrLabel_其他系统疾病1";
            this.xrLabel_其他系统疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_其他系统疾病1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_其他系统疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel86.Dpi = 254F;
            this.xrLabel86.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(210.5799F, 7.999899F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel86.StylePriority.UseBorders = false;
            this.xrLabel86.StylePriority.UseFont = false;
            this.xrLabel86.StylePriority.UseTextAlignment = false;
            this.xrLabel86.Text = "/";
            this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_其他系统疾病2
            // 
            this.xrLabel_其他系统疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_其他系统疾病2.Dpi = 254F;
            this.xrLabel_其他系统疾病2.LocationFloat = new DevExpress.Utils.PointFloat(252.5133F, 8.000029F);
            this.xrLabel_其他系统疾病2.Name = "xrLabel_其他系统疾病2";
            this.xrLabel_其他系统疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_其他系统疾病2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_其他系统疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel80
            // 
            this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel80.Dpi = 254F;
            this.xrLabel80.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(294.4461F, 8.000029F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel80.StylePriority.UseBorders = false;
            this.xrLabel80.StylePriority.UseFont = false;
            this.xrLabel80.StylePriority.UseTextAlignment = false;
            this.xrLabel80.Text = "/";
            this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_其他系统疾病3
            // 
            this.xrLabel_其他系统疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_其他系统疾病3.Dpi = 254F;
            this.xrLabel_其他系统疾病3.LocationFloat = new DevExpress.Utils.PointFloat(336.3789F, 8F);
            this.xrLabel_其他系统疾病3.Name = "xrLabel_其他系统疾病3";
            this.xrLabel_其他系统疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_其他系统疾病3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_其他系统疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableCell375
            // 
            this.xrTableCell375.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell375.Dpi = 254F;
            this.xrTableCell375.Name = "xrTableCell375";
            this.xrTableCell375.StylePriority.UseBorders = false;
            this.xrTableCell375.Weight = 0.19876670037244218D;
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell368,
            this.xrTableCell369,
            this.xrTableCell454,
            this.xrTableCell452,
            this.xrTableCell453,
            this.xrTableCell370,
            this.xrTableCell371});
            this.xrTableRow72.Dpi = 254F;
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Weight = 1D;
            // 
            // xrTableCell368
            // 
            this.xrTableCell368.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell368.Dpi = 254F;
            this.xrTableCell368.Name = "xrTableCell368";
            this.xrTableCell368.StylePriority.UseBorders = false;
            this.xrTableCell368.Weight = 0.11161843716525768D;
            // 
            // xrTableCell369
            // 
            this.xrTableCell369.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell369.Dpi = 254F;
            this.xrTableCell369.Name = "xrTableCell369";
            this.xrTableCell369.StylePriority.UseBorders = false;
            this.xrTableCell369.StylePriority.UseTextAlignment = false;
            this.xrTableCell369.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell369.Weight = 0.30993814107294138D;
            // 
            // xrTableCell454
            // 
            this.xrTableCell454.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell454.Dpi = 254F;
            this.xrTableCell454.Name = "xrTableCell454";
            this.xrTableCell454.StylePriority.UseBorders = false;
            this.xrTableCell454.Text = "入/出院日期";
            this.xrTableCell454.Weight = 0.4155718222398348D;
            // 
            // xrTableCell452
            // 
            this.xrTableCell452.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell452.Dpi = 254F;
            this.xrTableCell452.Name = "xrTableCell452";
            this.xrTableCell452.StylePriority.UseBorders = false;
            this.xrTableCell452.Text = "原    因";
            this.xrTableCell452.Weight = 0.41997521377395236D;
            // 
            // xrTableCell453
            // 
            this.xrTableCell453.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell453.Dpi = 254F;
            this.xrTableCell453.Name = "xrTableCell453";
            this.xrTableCell453.StylePriority.UseBorders = false;
            this.xrTableCell453.Text = "医疗机构及科室名称";
            this.xrTableCell453.Weight = 0.40619193064266251D;
            // 
            // xrTableCell370
            // 
            this.xrTableCell370.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell370.Dpi = 254F;
            this.xrTableCell370.Name = "xrTableCell370";
            this.xrTableCell370.StylePriority.UseBorders = false;
            this.xrTableCell370.Text = "病  案  号";
            this.xrTableCell370.Weight = 0.33670445510535119D;
            // 
            // xrTableCell371
            // 
            this.xrTableCell371.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell371.Dpi = 254F;
            this.xrTableCell371.Name = "xrTableCell371";
            this.xrTableCell371.StylePriority.UseBorders = false;
            this.xrTableCell371.Weight = 0.19876670037244218D;
            // 
            // xrTableRow76
            // 
            this.xrTableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell384,
            this.xrTableCell385,
            this.xrTable_入院日期1,
            this.xrTableCell462,
            this.xrTable_出院日期1,
            this.xrTable_住院史原因1,
            this.xrTable_住院史机构及科室1,
            this.xrTable_住院史病案号1,
            this.xrTableCell387});
            this.xrTableRow76.Dpi = 254F;
            this.xrTableRow76.Name = "xrTableRow76";
            this.xrTableRow76.Weight = 1D;
            // 
            // xrTableCell384
            // 
            this.xrTableCell384.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell384.Dpi = 254F;
            this.xrTableCell384.Name = "xrTableCell384";
            this.xrTableCell384.StylePriority.UseBorders = false;
            this.xrTableCell384.Text = "住院";
            this.xrTableCell384.Weight = 0.11161843716525768D;
            // 
            // xrTableCell385
            // 
            this.xrTableCell385.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell385.Dpi = 254F;
            this.xrTableCell385.Name = "xrTableCell385";
            this.xrTableCell385.StylePriority.UseBorders = false;
            this.xrTableCell385.StylePriority.UseTextAlignment = false;
            this.xrTableCell385.Text = "住院史";
            this.xrTableCell385.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell385.Weight = 0.30993814107294138D;
            // 
            // xrTable_入院日期1
            // 
            this.xrTable_入院日期1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_入院日期1.Dpi = 254F;
            this.xrTable_入院日期1.Name = "xrTable_入院日期1";
            this.xrTable_入院日期1.StylePriority.UseBorders = false;
            this.xrTable_入院日期1.Weight = 0.19309583705996553D;
            // 
            // xrTableCell462
            // 
            this.xrTableCell462.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell462.Dpi = 254F;
            this.xrTableCell462.Name = "xrTableCell462";
            this.xrTableCell462.StylePriority.UseBorders = false;
            this.xrTableCell462.Text = "/";
            this.xrTableCell462.Weight = 0.029376405757334877D;
            // 
            // xrTable_出院日期1
            // 
            this.xrTable_出院日期1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_出院日期1.Dpi = 254F;
            this.xrTable_出院日期1.Name = "xrTable_出院日期1";
            this.xrTable_出院日期1.StylePriority.UseBorders = false;
            this.xrTable_出院日期1.Weight = 0.19309989667822813D;
            // 
            // xrTable_住院史原因1
            // 
            this.xrTable_住院史原因1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史原因1.Dpi = 254F;
            this.xrTable_住院史原因1.Name = "xrTable_住院史原因1";
            this.xrTable_住院史原因1.StylePriority.UseBorders = false;
            this.xrTable_住院史原因1.Weight = 0.41997489651825864D;
            // 
            // xrTable_住院史机构及科室1
            // 
            this.xrTable_住院史机构及科室1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史机构及科室1.Dpi = 254F;
            this.xrTable_住院史机构及科室1.Name = "xrTable_住院史机构及科室1";
            this.xrTable_住院史机构及科室1.StylePriority.UseBorders = false;
            this.xrTable_住院史机构及科室1.Weight = 0.40619193064266251D;
            // 
            // xrTable_住院史病案号1
            // 
            this.xrTable_住院史病案号1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史病案号1.Dpi = 254F;
            this.xrTable_住院史病案号1.Name = "xrTable_住院史病案号1";
            this.xrTable_住院史病案号1.StylePriority.UseBorders = false;
            this.xrTable_住院史病案号1.Weight = 0.33670445510535119D;
            // 
            // xrTableCell387
            // 
            this.xrTableCell387.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell387.Dpi = 254F;
            this.xrTableCell387.Name = "xrTableCell387";
            this.xrTableCell387.StylePriority.UseBorders = false;
            this.xrTableCell387.Weight = 0.19876670037244218D;
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell380,
            this.xrTableCell381,
            this.xrTable_入院日期2,
            this.xrTableCell464,
            this.xrTable_出院日期2,
            this.xrTable_住院史原因2,
            this.xrTable_住院史机构及科室2,
            this.xrTable_住院史病案号2,
            this.xrTableCell383});
            this.xrTableRow75.Dpi = 254F;
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Weight = 1D;
            // 
            // xrTableCell380
            // 
            this.xrTableCell380.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell380.Dpi = 254F;
            this.xrTableCell380.Name = "xrTableCell380";
            this.xrTableCell380.StylePriority.UseBorders = false;
            this.xrTableCell380.Text = "治疗";
            this.xrTableCell380.Weight = 0.11161843716525768D;
            // 
            // xrTableCell381
            // 
            this.xrTableCell381.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell381.Dpi = 254F;
            this.xrTableCell381.Name = "xrTableCell381";
            this.xrTableCell381.StylePriority.UseBorders = false;
            this.xrTableCell381.StylePriority.UseTextAlignment = false;
            this.xrTableCell381.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell381.Weight = 0.30993814107294138D;
            // 
            // xrTable_入院日期2
            // 
            this.xrTable_入院日期2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_入院日期2.Dpi = 254F;
            this.xrTable_入院日期2.Name = "xrTable_入院日期2";
            this.xrTable_入院日期2.StylePriority.UseBorders = false;
            this.xrTable_入院日期2.Weight = 0.19309583705996553D;
            // 
            // xrTableCell464
            // 
            this.xrTableCell464.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell464.Dpi = 254F;
            this.xrTableCell464.Name = "xrTableCell464";
            this.xrTableCell464.StylePriority.UseBorders = false;
            this.xrTableCell464.Text = "/";
            this.xrTableCell464.Weight = 0.029376407844543397D;
            // 
            // xrTable_出院日期2
            // 
            this.xrTable_出院日期2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_出院日期2.Dpi = 254F;
            this.xrTable_出院日期2.Name = "xrTable_出院日期2";
            this.xrTable_出院日期2.StylePriority.UseBorders = false;
            this.xrTable_出院日期2.Weight = 0.19309931017263649D;
            // 
            // xrTable_住院史原因2
            // 
            this.xrTable_住院史原因2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史原因2.Dpi = 254F;
            this.xrTable_住院史原因2.Name = "xrTable_住院史原因2";
            this.xrTable_住院史原因2.StylePriority.UseBorders = false;
            this.xrTable_住院史原因2.Weight = 0.41997548093664178D;
            // 
            // xrTable_住院史机构及科室2
            // 
            this.xrTable_住院史机构及科室2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史机构及科室2.Dpi = 254F;
            this.xrTable_住院史机构及科室2.Name = "xrTable_住院史机构及科室2";
            this.xrTable_住院史机构及科室2.StylePriority.UseBorders = false;
            this.xrTable_住院史机构及科室2.Weight = 0.40619193064266251D;
            // 
            // xrTable_住院史病案号2
            // 
            this.xrTable_住院史病案号2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史病案号2.Dpi = 254F;
            this.xrTable_住院史病案号2.Name = "xrTable_住院史病案号2";
            this.xrTable_住院史病案号2.StylePriority.UseBorders = false;
            this.xrTable_住院史病案号2.Weight = 0.33670445510535119D;
            // 
            // xrTableCell383
            // 
            this.xrTableCell383.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell383.Dpi = 254F;
            this.xrTableCell383.Name = "xrTableCell383";
            this.xrTableCell383.StylePriority.UseBorders = false;
            this.xrTableCell383.Weight = 0.19876670037244218D;
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell376,
            this.xrTableCell377,
            this.xrTableCell389,
            this.xrTableCell388,
            this.xrTableCell390,
            this.xrTableCell378,
            this.xrTableCell379});
            this.xrTableRow74.Dpi = 254F;
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Weight = 1D;
            // 
            // xrTableCell376
            // 
            this.xrTableCell376.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell376.Dpi = 254F;
            this.xrTableCell376.Name = "xrTableCell376";
            this.xrTableCell376.StylePriority.UseBorders = false;
            this.xrTableCell376.Text = "情况";
            this.xrTableCell376.Weight = 0.11161843716525768D;
            // 
            // xrTableCell377
            // 
            this.xrTableCell377.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell377.Dpi = 254F;
            this.xrTableCell377.Name = "xrTableCell377";
            this.xrTableCell377.StylePriority.UseBorders = false;
            this.xrTableCell377.StylePriority.UseTextAlignment = false;
            this.xrTableCell377.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell377.Weight = 0.30993814107294138D;
            // 
            // xrTableCell389
            // 
            this.xrTableCell389.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell389.Dpi = 254F;
            this.xrTableCell389.Name = "xrTableCell389";
            this.xrTableCell389.StylePriority.UseBorders = false;
            this.xrTableCell389.Text = "建/撤床日期";
            this.xrTableCell389.Weight = 0.41557155507714538D;
            // 
            // xrTableCell388
            // 
            this.xrTableCell388.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell388.Dpi = 254F;
            this.xrTableCell388.Name = "xrTableCell388";
            this.xrTableCell388.StylePriority.UseBorders = false;
            this.xrTableCell388.Text = "原    因";
            this.xrTableCell388.Weight = 0.41997548093664178D;
            // 
            // xrTableCell390
            // 
            this.xrTableCell390.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell390.Dpi = 254F;
            this.xrTableCell390.Name = "xrTableCell390";
            this.xrTableCell390.StylePriority.UseBorders = false;
            this.xrTableCell390.Text = "医疗机构及科室名称";
            this.xrTableCell390.Weight = 0.40619193064266251D;
            // 
            // xrTableCell378
            // 
            this.xrTableCell378.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell378.Dpi = 254F;
            this.xrTableCell378.Name = "xrTableCell378";
            this.xrTableCell378.StylePriority.UseBorders = false;
            this.xrTableCell378.Text = "病  案  号";
            this.xrTableCell378.Weight = 0.33670445510535119D;
            // 
            // xrTableCell379
            // 
            this.xrTableCell379.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell379.Dpi = 254F;
            this.xrTableCell379.Name = "xrTableCell379";
            this.xrTableCell379.StylePriority.UseBorders = false;
            this.xrTableCell379.Weight = 0.19876670037244218D;
            // 
            // xrTableRow77
            // 
            this.xrTableRow77.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell391,
            this.xrTableCell439,
            this.xrTable_建床日期1,
            this.xrTableCell467,
            this.xrTable_撤床日期1,
            this.xrTable_家庭病床史原因1,
            this.xrTable_家庭病床史机构及科室1,
            this.xrTable_家庭病床史病案号1,
            this.xrTableCell444});
            this.xrTableRow77.Dpi = 254F;
            this.xrTableRow77.Name = "xrTableRow77";
            this.xrTableRow77.Weight = 1D;
            // 
            // xrTableCell391
            // 
            this.xrTableCell391.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell391.Dpi = 254F;
            this.xrTableCell391.Name = "xrTableCell391";
            this.xrTableCell391.StylePriority.UseBorders = false;
            this.xrTableCell391.Weight = 0.11161843716525768D;
            // 
            // xrTableCell439
            // 
            this.xrTableCell439.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell439.Dpi = 254F;
            this.xrTableCell439.Name = "xrTableCell439";
            this.xrTableCell439.StylePriority.UseBorders = false;
            this.xrTableCell439.Text = "家庭病床史";
            this.xrTableCell439.Weight = 0.30993814107294138D;
            // 
            // xrTable_建床日期1
            // 
            this.xrTable_建床日期1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_建床日期1.Dpi = 254F;
            this.xrTable_建床日期1.Name = "xrTable_建床日期1";
            this.xrTable_建床日期1.StylePriority.UseBorders = false;
            this.xrTable_建床日期1.Weight = 0.19309583705996553D;
            // 
            // xrTableCell467
            // 
            this.xrTableCell467.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell467.Dpi = 254F;
            this.xrTableCell467.Name = "xrTableCell467";
            this.xrTableCell467.StylePriority.UseBorders = false;
            this.xrTableCell467.Text = "/";
            this.xrTableCell467.Weight = 0.029376405757334877D;
            // 
            // xrTable_撤床日期1
            // 
            this.xrTable_撤床日期1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_撤床日期1.Dpi = 254F;
            this.xrTable_撤床日期1.Name = "xrTable_撤床日期1";
            this.xrTable_撤床日期1.StylePriority.UseBorders = false;
            this.xrTable_撤床日期1.Weight = 0.19309931225984497D;
            // 
            // xrTable_家庭病床史原因1
            // 
            this.xrTable_家庭病床史原因1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史原因1.Dpi = 254F;
            this.xrTable_家庭病床史原因1.Name = "xrTable_家庭病床史原因1";
            this.xrTable_家庭病床史原因1.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史原因1.Weight = 0.41997548093664178D;
            // 
            // xrTable_家庭病床史机构及科室1
            // 
            this.xrTable_家庭病床史机构及科室1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史机构及科室1.Dpi = 254F;
            this.xrTable_家庭病床史机构及科室1.Name = "xrTable_家庭病床史机构及科室1";
            this.xrTable_家庭病床史机构及科室1.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史机构及科室1.Weight = 0.40619193064266251D;
            // 
            // xrTable_家庭病床史病案号1
            // 
            this.xrTable_家庭病床史病案号1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史病案号1.Dpi = 254F;
            this.xrTable_家庭病床史病案号1.Name = "xrTable_家庭病床史病案号1";
            this.xrTable_家庭病床史病案号1.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史病案号1.Weight = 0.33670445510535119D;
            // 
            // xrTableCell444
            // 
            this.xrTableCell444.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell444.Dpi = 254F;
            this.xrTableCell444.Name = "xrTableCell444";
            this.xrTableCell444.StylePriority.UseBorders = false;
            this.xrTableCell444.Weight = 0.19876670037244218D;
            // 
            // xrTableRow78
            // 
            this.xrTableRow78.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell445,
            this.xrTableCell446,
            this.xrTable_建床日期2,
            this.xrTableCell468,
            this.xrTable_撤床日期2,
            this.xrTable_家庭病床史原因2,
            this.xrTable_家庭病床史机构及科室2,
            this.xrTable_家庭病床史病案号2,
            this.xrTableCell451});
            this.xrTableRow78.Dpi = 254F;
            this.xrTableRow78.Name = "xrTableRow78";
            this.xrTableRow78.Weight = 1D;
            // 
            // xrTableCell445
            // 
            this.xrTableCell445.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell445.Dpi = 254F;
            this.xrTableCell445.Name = "xrTableCell445";
            this.xrTableCell445.StylePriority.UseBorders = false;
            this.xrTableCell445.Weight = 0.11161843716525768D;
            // 
            // xrTableCell446
            // 
            this.xrTableCell446.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell446.Dpi = 254F;
            this.xrTableCell446.Name = "xrTableCell446";
            this.xrTableCell446.StylePriority.UseBorders = false;
            this.xrTableCell446.Weight = 0.30993814107294138D;
            // 
            // xrTable_建床日期2
            // 
            this.xrTable_建床日期2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_建床日期2.Dpi = 254F;
            this.xrTable_建床日期2.Name = "xrTable_建床日期2";
            this.xrTable_建床日期2.StylePriority.UseBorders = false;
            this.xrTable_建床日期2.Weight = 0.19309583705996553D;
            // 
            // xrTableCell468
            // 
            this.xrTableCell468.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell468.Dpi = 254F;
            this.xrTableCell468.Name = "xrTableCell468";
            this.xrTableCell468.StylePriority.UseBorders = false;
            this.xrTableCell468.Text = "/";
            this.xrTableCell468.Weight = 0.029376405757334877D;
            // 
            // xrTable_撤床日期2
            // 
            this.xrTable_撤床日期2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_撤床日期2.Dpi = 254F;
            this.xrTable_撤床日期2.Name = "xrTable_撤床日期2";
            this.xrTable_撤床日期2.StylePriority.UseBorders = false;
            this.xrTable_撤床日期2.Weight = 0.19309931225984497D;
            // 
            // xrTable_家庭病床史原因2
            // 
            this.xrTable_家庭病床史原因2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史原因2.Dpi = 254F;
            this.xrTable_家庭病床史原因2.Name = "xrTable_家庭病床史原因2";
            this.xrTable_家庭病床史原因2.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史原因2.Weight = 0.41997548093664178D;
            // 
            // xrTable_家庭病床史机构及科室2
            // 
            this.xrTable_家庭病床史机构及科室2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史机构及科室2.Dpi = 254F;
            this.xrTable_家庭病床史机构及科室2.Name = "xrTable_家庭病床史机构及科室2";
            this.xrTable_家庭病床史机构及科室2.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史机构及科室2.Weight = 0.40619193064266251D;
            // 
            // xrTable_家庭病床史病案号2
            // 
            this.xrTable_家庭病床史病案号2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史病案号2.Dpi = 254F;
            this.xrTable_家庭病床史病案号2.Name = "xrTable_家庭病床史病案号2";
            this.xrTable_家庭病床史病案号2.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史病案号2.Weight = 0.33670445510535119D;
            // 
            // xrTableCell451
            // 
            this.xrTableCell451.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell451.Dpi = 254F;
            this.xrTableCell451.Name = "xrTableCell451";
            this.xrTableCell451.StylePriority.UseBorders = false;
            this.xrTableCell451.Weight = 0.19876670037244218D;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(10.21294F, 166.2535F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6,
            this.xrTableRow1,
            this.xrTableRow5,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow4,
            this.xrTableRow3,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow13,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow19,
            this.xrTableRow32,
            this.xrTableRow35,
            this.xrTableRow34,
            this.xrTableRow33,
            this.xrTableRow20,
            this.xrTableRow22,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow31,
            this.xrTableRow30,
            this.xrTableRow29,
            this.xrTableRow28,
            this.xrTableRow27,
            this.xrTableRow24,
            this.xrTableRow23,
            this.xrTableRow21,
            this.xrTableRow39,
            this.xrTableRow44,
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow41,
            this.xrTableRow40,
            this.xrTableRow36,
            this.xrTableRow38,
            this.xrTableRow17});
            this.xrTable1.SizeF = new System.Drawing.SizeF(2009.294F, 2476.5F);
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell1,
            this.xrTableCell36,
            this.xrTableCell17,
            this.xrTableCell18});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Weight = 0.11161843716525768D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Weight = 0.30993814107294138D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "  口唇   1  红润    2  苍白    3  发绀    4 皲裂    5  疱疹";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell36.Weight = 1.4424083213046319D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_口唇});
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Weight = 0.13603510045716885D;
            // 
            // xrLabel_口唇
            // 
            this.xrLabel_口唇.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_口唇.Dpi = 254F;
            this.xrLabel_口唇.LocationFloat = new DevExpress.Utils.PointFloat(57.37944F, 8F);
            this.xrLabel_口唇.Name = "xrLabel_口唇";
            this.xrLabel_口唇.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_口唇.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_口唇.StylePriority.UseBorders = false;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.Weight = 0.19876670037244218D;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell53,
            this.xrTable_缺齿1,
            this.xrTable_缺齿2,
            this.xrTableCell52,
            this.xrTable_龋齿1,
            this.xrTable_龋齿2,
            this.xrTableCell51,
            this.xrTable_义齿1,
            this.xrTable_义齿2,
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "脏";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell2.Weight = 0.11161843716525768D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.Text = "口          腔";
            this.xrTableCell3.Weight = 0.30993814107294138D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Weight = 0.36733943285742759D;
            // 
            // xrTable_缺齿1
            // 
            this.xrTable_缺齿1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_缺齿1.Dpi = 254F;
            this.xrTable_缺齿1.Name = "xrTable_缺齿1";
            this.xrTable_缺齿1.StylePriority.UseBorders = false;
            this.xrTable_缺齿1.Weight = 0.075490209616103421D;
            // 
            // xrTable_缺齿2
            // 
            this.xrTable_缺齿2.Dpi = 254F;
            this.xrTable_缺齿2.Name = "xrTable_缺齿2";
            this.xrTable_缺齿2.Weight = 0.075484617986793623D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Weight = 0.13571818287749282D;
            // 
            // xrTable_龋齿1
            // 
            this.xrTable_龋齿1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_龋齿1.Dpi = 254F;
            this.xrTable_龋齿1.Name = "xrTable_龋齿1";
            this.xrTable_龋齿1.StylePriority.UseBorders = false;
            this.xrTable_龋齿1.Weight = 0.0754848183588107D;
            // 
            // xrTable_龋齿2
            // 
            this.xrTable_龋齿2.Dpi = 254F;
            this.xrTable_龋齿2.Name = "xrTable_龋齿2";
            this.xrTable_龋齿2.Weight = 0.075484415525276266D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.31355231998845645D;
            // 
            // xrTable_义齿1
            // 
            this.xrTable_义齿1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_义齿1.Dpi = 254F;
            this.xrTable_义齿1.Name = "xrTable_义齿1";
            this.xrTable_义齿1.StylePriority.UseBorders = false;
            this.xrTable_义齿1.Weight = 0.075484818358810657D;
            // 
            // xrTable_义齿2
            // 
            this.xrTable_义齿2.Dpi = 254F;
            this.xrTable_义齿2.Name = "xrTable_义齿2";
            this.xrTable_义齿2.Weight = 0.075484584591457432D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Weight = 0.30892002160117193D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.Weight = 0.19876670037244218D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell43,
            this.xrTable_缺齿3,
            this.xrTable_缺齿4,
            this.xrTableCell41,
            this.xrTable_龋齿3,
            this.xrTable_龋齿4,
            this.xrTableCell40,
            this.xrTable_义齿3,
            this.xrTable_义齿4,
            this.xrTableCell22,
            this.xrTableCell23});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "器";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell20.Weight = 0.11161843716525768D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.Weight = 0.30993814107294138D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.Text = "  齿列  1 正常  2 缺齿";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell43.Weight = 0.36733946625276392D;
            // 
            // xrTable_缺齿3
            // 
            this.xrTable_缺齿3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable_缺齿3.Dpi = 254F;
            this.xrTable_缺齿3.Name = "xrTable_缺齿3";
            this.xrTable_缺齿3.StylePriority.UseBorders = false;
            this.xrTable_缺齿3.Weight = 0.075490092732426753D;
            // 
            // xrTable_缺齿4
            // 
            this.xrTable_缺齿4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable_缺齿4.Dpi = 254F;
            this.xrTable_缺齿4.Name = "xrTable_缺齿4";
            this.xrTable_缺齿4.StylePriority.UseBorders = false;
            this.xrTable_缺齿4.Weight = 0.075484703560050825D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            this.xrTableCell41.Text = "  3 龋齿";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.13571822670657974D;
            // 
            // xrTable_龋齿3
            // 
            this.xrTable_龋齿3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable_龋齿3.Dpi = 254F;
            this.xrTable_龋齿3.Name = "xrTable_龋齿3";
            this.xrTable_龋齿3.StylePriority.UseBorders = false;
            this.xrTable_龋齿3.Weight = 0.075484705649551134D;
            // 
            // xrTable_龋齿4
            // 
            this.xrTable_龋齿4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable_龋齿4.Dpi = 254F;
            this.xrTable_龋齿4.Name = "xrTable_龋齿4";
            this.xrTable_龋齿4.StylePriority.UseBorders = false;
            this.xrTable_龋齿4.Weight = 0.075484705649551065D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.Text = "  4 义齿（假牙）";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell40.Weight = 0.31355229703145443D;
            // 
            // xrTable_义齿3
            // 
            this.xrTable_义齿3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable_义齿3.Dpi = 254F;
            this.xrTable_义齿3.Name = "xrTable_义齿3";
            this.xrTable_义齿3.StylePriority.UseBorders = false;
            this.xrTable_义齿3.Weight = 0.075484701475134086D;
            // 
            // xrTable_义齿4
            // 
            this.xrTable_义齿4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable_义齿4.Dpi = 254F;
            this.xrTable_义齿4.Name = "xrTable_义齿4";
            this.xrTable_义齿4.StylePriority.UseBorders = false;
            this.xrTable_义齿4.Weight = 0.075484701475134058D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_齿列1,
            this.xrLabel4,
            this.xrLabel_齿列2});
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Weight = 0.3089198212291549D;
            // 
            // xrLabel_齿列1
            // 
            this.xrLabel_齿列1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_齿列1.Dpi = 254F;
            this.xrLabel_齿列1.LocationFloat = new DevExpress.Utils.PointFloat(131.5F, 0F);
            this.xrLabel_齿列1.Name = "xrLabel_齿列1";
            this.xrLabel_齿列1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_齿列1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_齿列1.StylePriority.UseBorders = false;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(173.4331F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "/";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_齿列2
            // 
            this.xrLabel_齿列2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_齿列2.Dpi = 254F;
            this.xrLabel_齿列2.LocationFloat = new DevExpress.Utils.PointFloat(215.3662F, 0F);
            this.xrLabel_齿列2.Name = "xrLabel_齿列2";
            this.xrLabel_齿列2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_齿列2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_齿列2.StylePriority.UseBorders = false;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.Weight = 0.19876670037244218D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell55,
            this.xrTableCell26,
            this.xrTable_脏器功能医师签名});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "功";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell24.Weight = 0.11161843716525768D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Weight = 0.30993814107294138D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = "  咽部   1  无充血   2  充血    3  淋巴滤泡增生";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1.4424081710256191D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_咽部});
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Weight = 0.13603525073618172D;
            // 
            // xrLabel_咽部
            // 
            this.xrLabel_咽部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_咽部.Dpi = 254F;
            this.xrLabel_咽部.LocationFloat = new DevExpress.Utils.PointFloat(57.37944F, 8F);
            this.xrLabel_咽部.Name = "xrLabel_咽部";
            this.xrLabel_咽部.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_咽部.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_咽部.StylePriority.UseBorders = false;
            // 
            // xrTable_脏器功能医师签名
            // 
            this.xrTable_脏器功能医师签名.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_脏器功能医师签名.Dpi = 254F;
            this.xrTable_脏器功能医师签名.Name = "xrTable_脏器功能医师签名";
            this.xrTable_脏器功能医师签名.StylePriority.UseBorders = false;
            this.xrTable_脏器功能医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell62,
            this.xrTableCell57,
            this.xrTableCell61,
            this.xrTableCell56,
            this.xrTableCell60,
            this.xrTableCell58,
            this.xrTableCell64,
            this.xrTableCell59,
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "能";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell28.Weight = 0.11161843716525768D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.Text = "视          力";
            this.xrTableCell29.Weight = 0.30993814107294138D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.Text = "左眼";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell62.Weight = 0.093073540808464625D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell57.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_视力左眼});
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.Weight = 0.16414476056812913D;
            // 
            // xrLabel_视力左眼
            // 
            this.xrLabel_视力左眼.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_视力左眼.Dpi = 254F;
            this.xrLabel_视力左眼.LocationFloat = new DevExpress.Utils.PointFloat(9.536743E-06F, 4.74649F);
            this.xrLabel_视力左眼.Name = "xrLabel_视力左眼";
            this.xrLabel_视力左眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_视力左眼.SizeF = new System.Drawing.SizeF(150F, 49.95313F);
            this.xrLabel_视力左眼.StylePriority.UseBorders = false;
            this.xrLabel_视力左眼.StylePriority.UseTextAlignment = false;
            this.xrLabel_视力左眼.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.Text = "右眼";
            this.xrTableCell61.Weight = 0.091490159385355319D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell56.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_视力右眼});
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseBorders = false;
            this.xrTableCell56.Weight = 0.16414475221929503D;
            // 
            // xrLabel_视力右眼
            // 
            this.xrLabel_视力右眼.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_视力右眼.Dpi = 254F;
            this.xrLabel_视力右眼.LocationFloat = new DevExpress.Utils.PointFloat(7.05719E-05F, 4.74649F);
            this.xrLabel_视力右眼.Name = "xrLabel_视力右眼";
            this.xrLabel_视力右眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_视力右眼.SizeF = new System.Drawing.SizeF(150F, 49.95313F);
            this.xrLabel_视力右眼.StylePriority.UseBorders = false;
            this.xrLabel_视力右眼.StylePriority.UseTextAlignment = false;
            this.xrLabel_视力右眼.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.Text = "（矫正视力：左眼";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell60.Weight = 0.33847360839952351D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell58.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_矫正视力左眼});
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.Weight = 0.16414476056812916D;
            // 
            // xrLabel_矫正视力左眼
            // 
            this.xrLabel_矫正视力左眼.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_矫正视力左眼.Dpi = 254F;
            this.xrLabel_矫正视力左眼.LocationFloat = new DevExpress.Utils.PointFloat(7.05719E-05F, 4.74649F);
            this.xrLabel_矫正视力左眼.Name = "xrLabel_矫正视力左眼";
            this.xrLabel_矫正视力左眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_矫正视力左眼.SizeF = new System.Drawing.SizeF(150F, 49.95313F);
            this.xrLabel_矫正视力左眼.StylePriority.UseBorders = false;
            this.xrLabel_矫正视力左眼.StylePriority.UseTextAlignment = false;
            this.xrLabel_矫正视力左眼.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.Text = "右眼";
            this.xrTableCell64.Weight = 0.086345113176205426D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell59.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_矫正视力右眼});
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.Weight = 0.16414475847862886D;
            // 
            // xrLabel_矫正视力右眼
            // 
            this.xrLabel_矫正视力右眼.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_矫正视力右眼.Dpi = 254F;
            this.xrLabel_矫正视力右眼.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.74649F);
            this.xrLabel_矫正视力右眼.Name = "xrLabel_矫正视力右眼";
            this.xrLabel_矫正视力右眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_矫正视力右眼.SizeF = new System.Drawing.SizeF(149.9999F, 49.95313F);
            this.xrLabel_矫正视力右眼.StylePriority.UseBorders = false;
            this.xrLabel_矫正视力右眼.StylePriority.UseTextAlignment = false;
            this.xrLabel_矫正视力右眼.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "）";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell30.Weight = 0.31248196815806983D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.Weight = 0.19876670037244218D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell63,
            this.xrTableCell34,
            this.xrTableCell35});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell32.Weight = 0.11161843716525768D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.Text = "听          力";
            this.xrTableCell33.Weight = 0.30993814107294138D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "  1  听见\t\t2  听不清或无法听见";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell63.Weight = 1.4424081710256191D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_听力});
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Weight = 0.13603525073618172D;
            // 
            // xrLabel_听力
            // 
            this.xrLabel_听力.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_听力.Dpi = 254F;
            this.xrLabel_听力.LocationFloat = new DevExpress.Utils.PointFloat(57.37918F, 8F);
            this.xrLabel_听力.Name = "xrLabel_听力";
            this.xrLabel_听力.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_听力.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_听力.StylePriority.UseBorders = false;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseBorders = false;
            this.xrTableCell35.Weight = 0.19876670037244218D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell101,
            this.xrTableCell15,
            this.xrTableCell19});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Weight = 0.11161843716525768D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.Text = "运动能力";
            this.xrTableCell14.Weight = 0.30993814107294138D;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell101.Dpi = 254F;
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.StylePriority.UseTextAlignment = false;
            this.xrTableCell101.Text = "  1  可顺利完成\t2  无法独立完成任何一个动作";
            this.xrTableCell101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell101.Weight = 1.4424081710256191D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_运动能力});
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Weight = 0.13603525073618172D;
            // 
            // xrLabel_运动能力
            // 
            this.xrLabel_运动能力.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_运动能力.Dpi = 254F;
            this.xrLabel_运动能力.LocationFloat = new DevExpress.Utils.PointFloat(57.37918F, 8F);
            this.xrLabel_运动能力.Name = "xrLabel_运动能力";
            this.xrLabel_运动能力.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_运动能力.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_运动能力.StylePriority.UseBorders = false;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.Weight = 0.19876670037244218D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell193,
            this.xrTableCell194,
            this.xrTableCell11,
            this.xrTable_眼底医师签名});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.Weight = 0.11161843716525768D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Text = "  眼          底*";
            this.xrTableCell10.Weight = 0.30993814107294138D;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell193.Dpi = 254F;
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.StylePriority.UseBorders = false;
            this.xrTableCell193.StylePriority.UseTextAlignment = false;
            this.xrTableCell193.Text = "  1  正常    2  异常";
            this.xrTableCell193.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell193.Weight = 0.29817361532765035D;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell194.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_眼底异常});
            this.xrTableCell194.Dpi = 254F;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.StylePriority.UseBorders = false;
            this.xrTableCell194.Weight = 1.1442345640559697D;
            // 
            // xrLabel_眼底异常
            // 
            this.xrLabel_眼底异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_眼底异常.Dpi = 254F;
            this.xrLabel_眼底异常.LocationFloat = new DevExpress.Utils.PointFloat(7.05719E-05F, 0F);
            this.xrLabel_眼底异常.Name = "xrLabel_眼底异常";
            this.xrLabel_眼底异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_眼底异常.SizeF = new System.Drawing.SizeF(400F, 49.95319F);
            this.xrLabel_眼底异常.StylePriority.UseBorders = false;
            this.xrLabel_眼底异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_眼底异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_眼底});
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.Weight = 0.13603524237818082D;
            // 
            // xrLabel_眼底
            // 
            this.xrLabel_眼底.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_眼底.Dpi = 254F;
            this.xrLabel_眼底.LocationFloat = new DevExpress.Utils.PointFloat(57.37893F, 8F);
            this.xrLabel_眼底.Name = "xrLabel_眼底";
            this.xrLabel_眼底.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_眼底.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_眼底.StylePriority.UseBorders = false;
            // 
            // xrTable_眼底医师签名
            // 
            this.xrTable_眼底医师签名.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_眼底医师签名.Dpi = 254F;
            this.xrTable_眼底医师签名.Name = "xrTable_眼底医师签名";
            this.xrTable_眼底医师签名.StylePriority.UseBorders = false;
            this.xrTable_眼底医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell67,
            this.xrTableCell68});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.Weight = 0.11161843716525768D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.Text = "皮          肤";
            this.xrTableCell66.Weight = 0.30993814107294138D;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell195.Dpi = 254F;
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.StylePriority.UseBorders = false;
            this.xrTableCell195.StylePriority.UseTextAlignment = false;
            this.xrTableCell195.Text = "  1  正常    2  潮红    3  苍白    4  发绀    5  黄染    6  色素沉着    7  其他";
            this.xrTableCell195.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell195.Weight = 1.1806704871618643D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell196.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_皮肤其他});
            this.xrTableCell196.Dpi = 254F;
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.StylePriority.UseBorders = false;
            this.xrTableCell196.Weight = 0.26173782580310057D;
            // 
            // xrLabel_皮肤其他
            // 
            this.xrLabel_皮肤其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_皮肤其他.Dpi = 254F;
            this.xrLabel_皮肤其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_皮肤其他.Name = "xrLabel_皮肤其他";
            this.xrLabel_皮肤其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_皮肤其他.SizeF = new System.Drawing.SizeF(226.23F, 49.95313F);
            this.xrLabel_皮肤其他.StylePriority.UseBorders = false;
            this.xrLabel_皮肤其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_皮肤其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_皮肤});
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.Weight = 0.13603510879683606D;
            // 
            // xrLabel_皮肤
            // 
            this.xrLabel_皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_皮肤.Dpi = 254F;
            this.xrLabel_皮肤.LocationFloat = new DevExpress.Utils.PointFloat(57.37944F, 8F);
            this.xrLabel_皮肤.Name = "xrLabel_皮肤";
            this.xrLabel_皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_皮肤.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_皮肤.StylePriority.UseBorders = false;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.Weight = 0.19876670037244218D;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell71,
            this.xrTableCell72});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.Weight = 0.11161843716525768D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Text = "巩         膜";
            this.xrTableCell70.Weight = 0.30993814107294138D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell197.Dpi = 254F;
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.StylePriority.UseBorders = false;
            this.xrTableCell197.StylePriority.UseTextAlignment = false;
            this.xrTableCell197.Text = "  1  正常    2  黄染    3  充血    4  其他";
            this.xrTableCell197.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell197.Weight = 0.620134377946544D;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell198.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_巩膜其他});
            this.xrTableCell198.Dpi = 254F;
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseBorders = false;
            this.xrTableCell198.Weight = 0.82227393501842094D;
            // 
            // xrLabel_巩膜其他
            // 
            this.xrLabel_巩膜其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_巩膜其他.Dpi = 254F;
            this.xrLabel_巩膜其他.LocationFloat = new DevExpress.Utils.PointFloat(7.05719E-05F, 0.0001831055F);
            this.xrLabel_巩膜其他.Name = "xrLabel_巩膜其他";
            this.xrLabel_巩膜其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_巩膜其他.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_巩膜其他.StylePriority.UseBorders = false;
            this.xrLabel_巩膜其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_巩膜其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell71.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_巩膜});
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.Weight = 0.13603510879683606D;
            // 
            // xrLabel_巩膜
            // 
            this.xrLabel_巩膜.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_巩膜.Dpi = 254F;
            this.xrLabel_巩膜.LocationFloat = new DevExpress.Utils.PointFloat(57.37867F, 8F);
            this.xrLabel_巩膜.Name = "xrLabel_巩膜";
            this.xrLabel_巩膜.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_巩膜.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_巩膜.StylePriority.UseBorders = false;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.Weight = 0.19876670037244218D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell199,
            this.xrTableCell200,
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.Weight = 0.11161843716525768D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell78.Dpi = 254F;
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseBorders = false;
            this.xrTableCell78.Text = "淋  巴  结";
            this.xrTableCell78.Weight = 0.30993814107294138D;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell199.Dpi = 254F;
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.StylePriority.UseBorders = false;
            this.xrTableCell199.StylePriority.UseTextAlignment = false;
            this.xrTableCell199.Text = "  1  未触及    2  锁骨长    3  腋窝    4  其他";
            this.xrTableCell199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell199.Weight = 0.7058361612770887D;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell200.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_淋巴结其他});
            this.xrTableCell200.Dpi = 254F;
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.StylePriority.UseBorders = false;
            this.xrTableCell200.Weight = 0.73657201810653139D;
            // 
            // xrLabel_淋巴结其他
            // 
            this.xrLabel_淋巴结其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_淋巴结其他.Dpi = 254F;
            this.xrLabel_淋巴结其他.LocationFloat = new DevExpress.Utils.PointFloat(0.0001291911F, 0.0001291911F);
            this.xrLabel_淋巴结其他.Name = "xrLabel_淋巴结其他";
            this.xrLabel_淋巴结其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_淋巴结其他.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_淋巴结其他.StylePriority.UseBorders = false;
            this.xrLabel_淋巴结其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_淋巴结其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_淋巴结});
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseBorders = false;
            this.xrTableCell79.Weight = 0.13603524237818082D;
            // 
            // xrLabel_淋巴结
            // 
            this.xrLabel_淋巴结.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_淋巴结.Dpi = 254F;
            this.xrLabel_淋巴结.LocationFloat = new DevExpress.Utils.PointFloat(57.37893F, 8F);
            this.xrLabel_淋巴结.Name = "xrLabel_淋巴结";
            this.xrLabel_淋巴结.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_淋巴结.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_淋巴结.StylePriority.UseBorders = false;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell80.Dpi = 254F;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.Weight = 0.19876670037244218D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell12,
            this.xrTableCell87,
            this.xrTableCell88});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell85.Dpi = 254F;
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.Weight = 0.11161843716525768D;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell86.Dpi = 254F;
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseBorders = false;
            this.xrTableCell86.Weight = 0.30993814107294138D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "  桶状胸：1  否\t2  是";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell12.Weight = 1.4424081710256191D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell87.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_桶状胸});
            this.xrTableCell87.Dpi = 254F;
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBorders = false;
            this.xrTableCell87.Weight = 0.13603525073618172D;
            // 
            // xrLabel_桶状胸
            // 
            this.xrLabel_桶状胸.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_桶状胸.Dpi = 254F;
            this.xrLabel_桶状胸.LocationFloat = new DevExpress.Utils.PointFloat(57.37957F, 8F);
            this.xrLabel_桶状胸.Name = "xrLabel_桶状胸";
            this.xrLabel_桶状胸.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_桶状胸.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_桶状胸.StylePriority.UseBorders = false;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell88.Dpi = 254F;
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.Weight = 0.19876670037244218D;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell201,
            this.xrTableCell202,
            this.xrTableCell91,
            this.xrTableCell92});
            this.xrTableRow16.Dpi = 254F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell89.Dpi = 254F;
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.Weight = 0.11161843716525768D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell90.Dpi = 254F;
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.Text = "肺";
            this.xrTableCell90.Weight = 0.30993814107294138D;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Dpi = 254F;
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.StylePriority.UseTextAlignment = false;
            this.xrTableCell201.Text = "  呼吸音：1  正常\t2  异常";
            this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell201.Weight = 0.5622278000808274D;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_呼吸音异常});
            this.xrTableCell202.Dpi = 254F;
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Weight = 0.88018037930279258D;
            // 
            // xrLabel_呼吸音异常
            // 
            this.xrLabel_呼吸音异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_呼吸音异常.Dpi = 254F;
            this.xrLabel_呼吸音异常.LocationFloat = new DevExpress.Utils.PointFloat(2.166664F, 0F);
            this.xrLabel_呼吸音异常.Name = "xrLabel_呼吸音异常";
            this.xrLabel_呼吸音异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_呼吸音异常.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_呼吸音异常.StylePriority.UseBorders = false;
            this.xrLabel_呼吸音异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_呼吸音异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell91.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_呼吸音});
            this.xrTableCell91.Dpi = 254F;
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Weight = 0.13603524237818082D;
            // 
            // xrLabel_呼吸音
            // 
            this.xrLabel_呼吸音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_呼吸音.Dpi = 254F;
            this.xrLabel_呼吸音.LocationFloat = new DevExpress.Utils.PointFloat(57.37957F, 8F);
            this.xrLabel_呼吸音.Name = "xrLabel_呼吸音";
            this.xrLabel_呼吸音.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_呼吸音.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_呼吸音.StylePriority.UseBorders = false;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell92.Dpi = 254F;
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Weight = 0.19876670037244218D;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell37,
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell38,
            this.xrTableCell39});
            this.xrTableRow19.Dpi = 254F;
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.Weight = 0.11161843716525768D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Weight = 0.30993814107294138D;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell203.Dpi = 254F;
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.StylePriority.UseBorders = false;
            this.xrTableCell203.StylePriority.UseTextAlignment = false;
            this.xrTableCell203.Text = "  罗    音：1  无\t2  干罗音\t3  湿罗音\t4  其他";
            this.xrTableCell203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell203.Weight = 1.0023181525299041D;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell204.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_罗音其他});
            this.xrTableCell204.Dpi = 254F;
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.StylePriority.UseBorders = false;
            this.xrTableCell204.Weight = 0.4400900268537159D;
            // 
            // xrLabel_罗音其他
            // 
            this.xrLabel_罗音其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_罗音其他.Dpi = 254F;
            this.xrLabel_罗音其他.LocationFloat = new DevExpress.Utils.PointFloat(1.111155F, 0F);
            this.xrLabel_罗音其他.Name = "xrLabel_罗音其他";
            this.xrLabel_罗音其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_罗音其他.SizeF = new System.Drawing.SizeF(325.9166F, 49.95306F);
            this.xrLabel_罗音其他.StylePriority.UseBorders = false;
            this.xrLabel_罗音其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_罗音其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_罗音});
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.Weight = 0.13603524237818082D;
            // 
            // xrLabel_罗音
            // 
            this.xrLabel_罗音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_罗音.Dpi = 254F;
            this.xrLabel_罗音.LocationFloat = new DevExpress.Utils.PointFloat(57.37893F, 8F);
            this.xrLabel_罗音.Name = "xrLabel_罗音";
            this.xrLabel_罗音.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_罗音.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_罗音.StylePriority.UseBorders = false;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.Weight = 0.19876670037244218D;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell207,
            this.xrTableCell205,
            this.xrTableCell206,
            this.xrTableCell143,
            this.xrTableCell144});
            this.xrTableRow32.Dpi = 254F;
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell141.Dpi = 254F;
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.StylePriority.UseBorders = false;
            this.xrTableCell141.Weight = 0.11161843716525768D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell142.Dpi = 254F;
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.Text = "心          脏";
            this.xrTableCell142.Weight = 0.30993814107294138D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Dpi = 254F;
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.StylePriority.UseTextAlignment = false;
            this.xrTableCell207.Text = "  心率";
            this.xrTableCell207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell207.Weight = 0.11160360682451073D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心率});
            this.xrTableCell205.Dpi = 254F;
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Weight = 0.27922049301388258D;
            // 
            // xrLabel_心率
            // 
            this.xrLabel_心率.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_心率.Dpi = 254F;
            this.xrLabel_心率.LocationFloat = new DevExpress.Utils.PointFloat(4.005432E-05F, 0F);
            this.xrLabel_心率.Name = "xrLabel_心率";
            this.xrLabel_心率.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心率.SizeF = new System.Drawing.SizeF(255.1594F, 49.95313F);
            this.xrLabel_心率.StylePriority.UseBorders = false;
            this.xrLabel_心率.StylePriority.UseTextAlignment = false;
            this.xrLabel_心率.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Dpi = 254F;
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.StylePriority.UseTextAlignment = false;
            this.xrTableCell206.Text = "次/分钟\t心律：1  齐    2  不齐    3  绝对不齐";
            this.xrTableCell206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell206.Weight = 1.0515843467079162D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell143.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心律});
            this.xrTableCell143.Dpi = 254F;
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.Weight = 0.13603497521549135D;
            // 
            // xrLabel_心律
            // 
            this.xrLabel_心律.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心律.Dpi = 254F;
            this.xrLabel_心律.LocationFloat = new DevExpress.Utils.PointFloat(57.37931F, 8F);
            this.xrLabel_心律.Name = "xrLabel_心律";
            this.xrLabel_心律.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_心律.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_心律.StylePriority.UseBorders = false;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell144.Dpi = 254F;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.Weight = 0.19876670037244218D;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.xrTableCell154,
            this.xrTableCell208,
            this.xrTableCell209,
            this.xrTableCell155,
            this.xrTable_查体医师签名});
            this.xrTableRow35.Dpi = 254F;
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell153.Dpi = 254F;
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.Weight = 0.11161843716525768D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell154.Dpi = 254F;
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseBorders = false;
            this.xrTableCell154.Weight = 0.30993814107294138D;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell208.Dpi = 254F;
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.StylePriority.UseBorders = false;
            this.xrTableCell208.StylePriority.UseTextAlignment = false;
            this.xrTableCell208.Text = "  杂音：1  无\t2  有";
            this.xrTableCell208.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell208.Weight = 0.52979989339518041D;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell209.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_杂音有});
            this.xrTableCell209.Dpi = 254F;
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.StylePriority.UseBorders = false;
            this.xrTableCell209.Weight = 0.91260882031381851D;
            // 
            // xrLabel_杂音有
            // 
            this.xrLabel_杂音有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_杂音有.Dpi = 254F;
            this.xrLabel_杂音有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.893425F);
            this.xrLabel_杂音有.Name = "xrLabel_杂音有";
            this.xrLabel_杂音有.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_杂音有.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_杂音有.StylePriority.UseBorders = false;
            this.xrLabel_杂音有.StylePriority.UseTextAlignment = false;
            this.xrLabel_杂音有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell155.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_杂音});
            this.xrTableCell155.Dpi = 254F;
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseBorders = false;
            this.xrTableCell155.Weight = 0.13603470805280193D;
            // 
            // xrLabel_杂音
            // 
            this.xrLabel_杂音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_杂音.Dpi = 254F;
            this.xrLabel_杂音.LocationFloat = new DevExpress.Utils.PointFloat(57.37918F, 8F);
            this.xrLabel_杂音.Name = "xrLabel_杂音";
            this.xrLabel_杂音.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_杂音.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_杂音.StylePriority.UseBorders = false;
            // 
            // xrTable_查体医师签名
            // 
            this.xrTable_查体医师签名.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_查体医师签名.Dpi = 254F;
            this.xrTable_查体医师签名.Name = "xrTable_查体医师签名";
            this.xrTable_查体医师签名.StylePriority.UseBorders = false;
            this.xrTable_查体医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149,
            this.xrTableCell150,
            this.xrTableCell210,
            this.xrTableCell211,
            this.xrTableCell151,
            this.xrTableCell152});
            this.xrTableRow34.Dpi = 254F;
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell149.Dpi = 254F;
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseBorders = false;
            this.xrTableCell149.Weight = 0.11161843716525768D;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell150.Dpi = 254F;
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.StylePriority.UseBorders = false;
            this.xrTableCell150.Weight = 0.30993814107294138D;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Dpi = 254F;
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.StylePriority.UseTextAlignment = false;
            this.xrTableCell210.Text = "  压痛：1  无\t2  有";
            this.xrTableCell210.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell210.Weight = 0.529799993581189D;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_压痛有});
            this.xrTableCell211.Dpi = 254F;
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.StylePriority.UseTextAlignment = false;
            this.xrTableCell211.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell211.Weight = 0.91260912087184409D;
            // 
            // xrLabel_压痛有
            // 
            this.xrLabel_压痛有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_压痛有.Dpi = 254F;
            this.xrLabel_压痛有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_压痛有.Name = "xrLabel_压痛有";
            this.xrLabel_压痛有.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_压痛有.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_压痛有.StylePriority.UseBorders = false;
            this.xrLabel_压痛有.StylePriority.UseTextAlignment = false;
            this.xrLabel_压痛有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell151.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_压痛});
            this.xrTableCell151.Dpi = 254F;
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseBorders = false;
            this.xrTableCell151.Weight = 0.13603430730876776D;
            // 
            // xrLabel_压痛
            // 
            this.xrLabel_压痛.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_压痛.Dpi = 254F;
            this.xrLabel_压痛.LocationFloat = new DevExpress.Utils.PointFloat(57.37789F, 8F);
            this.xrLabel_压痛.Name = "xrLabel_压痛";
            this.xrLabel_压痛.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_压痛.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_压痛.StylePriority.UseBorders = false;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell152.Dpi = 254F;
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseBorders = false;
            this.xrTableCell152.Weight = 0.19876670037244218D;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell145,
            this.xrTableCell146,
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell147,
            this.xrTableCell148});
            this.xrTableRow33.Dpi = 254F;
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell145.Dpi = 254F;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.Weight = 0.11161843716525768D;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell146.Dpi = 254F;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBorders = false;
            this.xrTableCell146.Weight = 0.30993814107294138D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Dpi = 254F;
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.StylePriority.UseTextAlignment = false;
            this.xrTableCell212.Text = "  包块：1  无\t2  有";
            this.xrTableCell212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell212.Weight = 0.529799993581189D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_包块有});
            this.xrTableCell213.Dpi = 254F;
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.StylePriority.UseTextAlignment = false;
            this.xrTableCell213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell213.Weight = 0.912608185802431D;
            // 
            // xrLabel_包块有
            // 
            this.xrLabel_包块有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_包块有.Dpi = 254F;
            this.xrLabel_包块有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_包块有.Name = "xrLabel_包块有";
            this.xrLabel_包块有.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_包块有.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_包块有.StylePriority.UseBorders = false;
            this.xrLabel_包块有.StylePriority.UseTextAlignment = false;
            this.xrLabel_包块有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell147.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_包块});
            this.xrTableCell147.Dpi = 254F;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBorders = false;
            this.xrTableCell147.Weight = 0.13603524237818082D;
            // 
            // xrLabel_包块
            // 
            this.xrLabel_包块.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_包块.Dpi = 254F;
            this.xrLabel_包块.LocationFloat = new DevExpress.Utils.PointFloat(57.37867F, 8F);
            this.xrLabel_包块.Name = "xrLabel_包块";
            this.xrLabel_包块.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_包块.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_包块.StylePriority.UseBorders = false;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell148.Dpi = 254F;
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.StylePriority.UseBorders = false;
            this.xrTableCell148.Weight = 0.19876670037244218D;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell44,
            this.xrTableCell214,
            this.xrTableCell215,
            this.xrTableCell45,
            this.xrTableCell46});
            this.xrTableRow20.Dpi = 254F;
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.Text = "查";
            this.xrTableCell42.Weight = 0.11161843716525768D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.Text = "腹          部";
            this.xrTableCell44.Weight = 0.30993814107294138D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Dpi = 254F;
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.StylePriority.UseTextAlignment = false;
            this.xrTableCell214.Text = "  肝大：1  无\t2  有";
            this.xrTableCell214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell214.Weight = 0.529799993581189D;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_肝大有});
            this.xrTableCell215.Dpi = 254F;
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.StylePriority.UseTextAlignment = false;
            this.xrTableCell215.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell215.Weight = 0.912608185802431D;
            // 
            // xrLabel_肝大有
            // 
            this.xrLabel_肝大有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_肝大有.Dpi = 254F;
            this.xrLabel_肝大有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_肝大有.Name = "xrLabel_肝大有";
            this.xrLabel_肝大有.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_肝大有.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_肝大有.StylePriority.UseBorders = false;
            this.xrLabel_肝大有.StylePriority.UseTextAlignment = false;
            this.xrLabel_肝大有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell45.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_肝大});
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Weight = 0.13603524237818082D;
            // 
            // xrLabel_肝大
            // 
            this.xrLabel_肝大.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肝大.Dpi = 254F;
            this.xrLabel_肝大.LocationFloat = new DevExpress.Utils.PointFloat(57.37944F, 8F);
            this.xrLabel_肝大.Name = "xrLabel_肝大";
            this.xrLabel_肝大.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_肝大.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_肝大.StylePriority.UseBorders = false;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Weight = 0.19876670037244218D;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.xrTableCell102,
            this.xrTableCell216,
            this.xrTableCell217,
            this.xrTableCell103,
            this.xrTableCell104});
            this.xrTableRow22.Dpi = 254F;
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.Text = "体";
            this.xrTableCell54.Weight = 0.11161843716525768D;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell102.Dpi = 254F;
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseBorders = false;
            this.xrTableCell102.Weight = 0.30993814107294138D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Dpi = 254F;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.StylePriority.UseTextAlignment = false;
            this.xrTableCell216.Text = "  脾大：1  无\t2  有";
            this.xrTableCell216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell216.Weight = 0.529799993581189D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_脾大有});
            this.xrTableCell217.Dpi = 254F;
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.StylePriority.UseTextAlignment = false;
            this.xrTableCell217.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell217.Weight = 0.912608185802431D;
            // 
            // xrLabel_脾大有
            // 
            this.xrLabel_脾大有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_脾大有.Dpi = 254F;
            this.xrLabel_脾大有.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 2.246521F);
            this.xrLabel_脾大有.Name = "xrLabel_脾大有";
            this.xrLabel_脾大有.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_脾大有.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_脾大有.StylePriority.UseBorders = false;
            this.xrLabel_脾大有.StylePriority.UseTextAlignment = false;
            this.xrLabel_脾大有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell103.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_脾大});
            this.xrTableCell103.Dpi = 254F;
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.Weight = 0.13603524237818082D;
            // 
            // xrLabel_脾大
            // 
            this.xrLabel_脾大.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脾大.Dpi = 254F;
            this.xrLabel_脾大.LocationFloat = new DevExpress.Utils.PointFloat(57.37944F, 8F);
            this.xrLabel_脾大.Name = "xrLabel_脾大";
            this.xrLabel_脾大.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_脾大.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_脾大.StylePriority.UseBorders = false;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell104.Dpi = 254F;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseBorders = false;
            this.xrTableCell104.Weight = 0.19876670037244218D;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell218,
            this.xrTableCell219,
            this.xrTableCell115,
            this.xrTableCell116});
            this.xrTableRow25.Dpi = 254F;
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell113.Dpi = 254F;
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.StylePriority.UseBorders = false;
            this.xrTableCell113.Weight = 0.11161843716525768D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell114.Dpi = 254F;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Weight = 0.30993814107294138D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell218.Dpi = 254F;
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.StylePriority.UseBorders = false;
            this.xrTableCell218.StylePriority.UseTextAlignment = false;
            this.xrTableCell218.Text = "  移动性浊音：1  无\t2  有";
            this.xrTableCell218.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell218.Weight = 0.529799993581189D;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell219.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_移动性浊音有});
            this.xrTableCell219.Dpi = 254F;
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.StylePriority.UseBorders = false;
            this.xrTableCell219.StylePriority.UseTextAlignment = false;
            this.xrTableCell219.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell219.Weight = 0.912608185802431D;
            // 
            // xrLabel_移动性浊音有
            // 
            this.xrLabel_移动性浊音有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_移动性浊音有.Dpi = 254F;
            this.xrLabel_移动性浊音有.LocationFloat = new DevExpress.Utils.PointFloat(7.05719E-05F, 0F);
            this.xrLabel_移动性浊音有.Name = "xrLabel_移动性浊音有";
            this.xrLabel_移动性浊音有.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_移动性浊音有.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_移动性浊音有.StylePriority.UseBorders = false;
            this.xrLabel_移动性浊音有.StylePriority.UseTextAlignment = false;
            this.xrLabel_移动性浊音有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_移动性浊音});
            this.xrTableCell115.Dpi = 254F;
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Weight = 0.13603524237818082D;
            // 
            // xrLabel_移动性浊音
            // 
            this.xrLabel_移动性浊音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_移动性浊音.Dpi = 254F;
            this.xrLabel_移动性浊音.LocationFloat = new DevExpress.Utils.PointFloat(57.37944F, 8F);
            this.xrLabel_移动性浊音.Name = "xrLabel_移动性浊音";
            this.xrLabel_移动性浊音.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_移动性浊音.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_移动性浊音.StylePriority.UseBorders = false;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell116.Dpi = 254F;
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Weight = 0.19876670037244218D;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell220,
            this.xrTableCell119,
            this.xrTableCell120});
            this.xrTableRow26.Dpi = 254F;
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell117.Dpi = 254F;
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Weight = 0.11161843716525768D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell118.Dpi = 254F;
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.Text = "下肢水肿";
            this.xrTableCell118.Weight = 0.30993814107294138D;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell220.Dpi = 254F;
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.StylePriority.UseBorders = false;
            this.xrTableCell220.StylePriority.UseTextAlignment = false;
            this.xrTableCell220.Text = "  1  无    2  单侧    3  双侧不对称    4  双侧对称";
            this.xrTableCell220.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell220.Weight = 1.4424089725136875D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell119.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_下肢水肿});
            this.xrTableCell119.Dpi = 254F;
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.Weight = 0.13603444924811337D;
            // 
            // xrLabel_下肢水肿
            // 
            this.xrLabel_下肢水肿.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_下肢水肿.Dpi = 254F;
            this.xrLabel_下肢水肿.LocationFloat = new DevExpress.Utils.PointFloat(57.37893F, 8F);
            this.xrLabel_下肢水肿.Name = "xrLabel_下肢水肿";
            this.xrLabel_下肢水肿.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_下肢水肿.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_下肢水肿.StylePriority.UseBorders = false;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell120.Dpi = 254F;
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.Weight = 0.19876670037244218D;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell221,
            this.xrTableCell139,
            this.xrTableCell140});
            this.xrTableRow31.Dpi = 254F;
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell137.Dpi = 254F;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.StylePriority.UseBorders = false;
            this.xrTableCell137.Weight = 0.11161843716525768D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell138.Dpi = 254F;
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseBorders = false;
            this.xrTableCell138.Text = "足背动脉搏动*";
            this.xrTableCell138.Weight = 0.30993814107294138D;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell221.Dpi = 254F;
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseBorders = false;
            this.xrTableCell221.StylePriority.UseTextAlignment = false;
            this.xrTableCell221.Text = "  1  未触及    2  触及双侧对称    3  触及左侧弱或消失    4  触及右侧弱或消失";
            this.xrTableCell221.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell221.Weight = 1.4424080708396108D;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell139.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动});
            this.xrTableCell139.Dpi = 254F;
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.StylePriority.UseBorders = false;
            this.xrTableCell139.Weight = 0.13603535092219021D;
            // 
            // xrLabel_足背动脉搏动
            // 
            this.xrLabel_足背动脉搏动.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动.Dpi = 254F;
            this.xrLabel_足背动脉搏动.LocationFloat = new DevExpress.Utils.PointFloat(57.37867F, 8F);
            this.xrLabel_足背动脉搏动.Name = "xrLabel_足背动脉搏动";
            this.xrLabel_足背动脉搏动.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_足背动脉搏动.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_足背动脉搏动.StylePriority.UseBorders = false;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell140.Dpi = 254F;
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.Weight = 0.19876670037244218D;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell222,
            this.xrTableCell223,
            this.xrTableCell135,
            this.xrTable_肛门指诊医师签名});
            this.xrTableRow30.Dpi = 254F;
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell133.Dpi = 254F;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.Weight = 0.11161843716525768D;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell134.Dpi = 254F;
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.StylePriority.UseBorders = false;
            this.xrTableCell134.Text = "  肛门指诊*";
            this.xrTableCell134.Weight = 0.30993814107294138D;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell222.Dpi = 254F;
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.StylePriority.UseBorders = false;
            this.xrTableCell222.StylePriority.UseTextAlignment = false;
            this.xrTableCell222.Text = "  1  未及异常    2  触痛    3  包块    4  前列腺异常    5  其他";
            this.xrTableCell222.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell222.Weight = 0.9907367835242229D;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell223.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_肛门指诊其他});
            this.xrTableCell223.Dpi = 254F;
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.StylePriority.UseBorders = false;
            this.xrTableCell223.Weight = 0.451671395859397D;
            // 
            // xrLabel_肛门指诊其他
            // 
            this.xrLabel_肛门指诊其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_肛门指诊其他.Dpi = 254F;
            this.xrLabel_肛门指诊其他.LocationFloat = new DevExpress.Utils.PointFloat(7.05719E-05F, 0F);
            this.xrLabel_肛门指诊其他.Name = "xrLabel_肛门指诊其他";
            this.xrLabel_肛门指诊其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_肛门指诊其他.SizeF = new System.Drawing.SizeF(317.2466F, 49.95313F);
            this.xrLabel_肛门指诊其他.StylePriority.UseBorders = false;
            this.xrLabel_肛门指诊其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_肛门指诊其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell135.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_肛门指诊});
            this.xrTableCell135.Dpi = 254F;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.StylePriority.UseBorders = false;
            this.xrTableCell135.Weight = 0.13603524237818082D;
            // 
            // xrLabel_肛门指诊
            // 
            this.xrLabel_肛门指诊.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肛门指诊.Dpi = 254F;
            this.xrLabel_肛门指诊.LocationFloat = new DevExpress.Utils.PointFloat(57.37841F, 8F);
            this.xrLabel_肛门指诊.Name = "xrLabel_肛门指诊";
            this.xrLabel_肛门指诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_肛门指诊.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_肛门指诊.StylePriority.UseBorders = false;
            // 
            // xrTable_肛门指诊医师签名
            // 
            this.xrTable_肛门指诊医师签名.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_肛门指诊医师签名.Dpi = 254F;
            this.xrTable_肛门指诊医师签名.Name = "xrTable_肛门指诊医师签名";
            this.xrTable_肛门指诊医师签名.StylePriority.UseBorders = false;
            this.xrTable_肛门指诊医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell224,
            this.xrTableCell225,
            this.xrTableCell131,
            this.xrTable_乳腺医师签名});
            this.xrTableRow29.Dpi = 254F;
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell129.Dpi = 254F;
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.Weight = 0.11161843716525768D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell130.Dpi = 254F;
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.StylePriority.UseBorders = false;
            this.xrTableCell130.Text = "  乳        腺*";
            this.xrTableCell130.Weight = 0.30993814107294138D;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell224.Dpi = 254F;
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.StylePriority.UseBorders = false;
            this.xrTableCell224.StylePriority.UseTextAlignment = false;
            this.xrTableCell224.Text = "  1 未见异常  2 乳房切除  3 异常泌乳  4 乳腺包块  5其他";
            this.xrTableCell224.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell224.Weight = 0.979155481309214D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell225.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_乳腺其他});
            this.xrTableCell225.Dpi = 254F;
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseBorders = false;
            this.xrTableCell225.Weight = 0.21488342609713182D;
            // 
            // xrLabel_乳腺其他
            // 
            this.xrLabel_乳腺其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_乳腺其他.Dpi = 254F;
            this.xrLabel_乳腺其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_乳腺其他.Name = "xrLabel_乳腺其他";
            this.xrLabel_乳腺其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_乳腺其他.SizeF = new System.Drawing.SizeF(196.3663F, 49.95313F);
            this.xrLabel_乳腺其他.StylePriority.UseBorders = false;
            this.xrLabel_乳腺其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_乳腺其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell131.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_乳腺2,
            this.xrLabel38,
            this.xrLabel_乳腺3,
            this.xrLabel36,
            this.xrLabel_乳腺4,
            this.xrLabel13,
            this.xrLabel_乳腺1});
            this.xrTableCell131.Dpi = 254F;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.Weight = 0.384404514355455D;
            // 
            // xrLabel_乳腺2
            // 
            this.xrLabel_乳腺2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_乳腺2.Dpi = 254F;
            this.xrLabel_乳腺2.LocationFloat = new DevExpress.Utils.PointFloat(116.6126F, 8.000029F);
            this.xrLabel_乳腺2.Name = "xrLabel_乳腺2";
            this.xrLabel_乳腺2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_乳腺2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_乳腺2.StylePriority.UseBorders = false;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.Dpi = 254F;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(158.5456F, 7.999964F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "/";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_乳腺3
            // 
            this.xrLabel_乳腺3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_乳腺3.Dpi = 254F;
            this.xrLabel_乳腺3.LocationFloat = new DevExpress.Utils.PointFloat(200.4787F, 8.000029F);
            this.xrLabel_乳腺3.Name = "xrLabel_乳腺3";
            this.xrLabel_乳腺3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_乳腺3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_乳腺3.StylePriority.UseBorders = false;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.Dpi = 254F;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(242.4119F, 8.000029F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "/";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_乳腺4
            // 
            this.xrLabel_乳腺4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_乳腺4.Dpi = 254F;
            this.xrLabel_乳腺4.LocationFloat = new DevExpress.Utils.PointFloat(284.345F, 8F);
            this.xrLabel_乳腺4.Name = "xrLabel_乳腺4";
            this.xrLabel_乳腺4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_乳腺4.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_乳腺4.StylePriority.UseBorders = false;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Dpi = 254F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(74.6794F, 8.000029F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "/";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_乳腺1
            // 
            this.xrLabel_乳腺1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_乳腺1.Dpi = 254F;
            this.xrLabel_乳腺1.LocationFloat = new DevExpress.Utils.PointFloat(32.74629F, 8.000029F);
            this.xrLabel_乳腺1.Name = "xrLabel_乳腺1";
            this.xrLabel_乳腺1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_乳腺1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_乳腺1.StylePriority.UseBorders = false;
            // 
            // xrTable_乳腺医师签名
            // 
            this.xrTable_乳腺医师签名.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_乳腺医师签名.Dpi = 254F;
            this.xrTable_乳腺医师签名.Name = "xrTable_乳腺医师签名";
            this.xrTable_乳腺医师签名.StylePriority.UseBorders = false;
            this.xrTable_乳腺医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.xrTableCell226,
            this.xrTableCell126,
            this.xrTableCell97,
            this.xrTableCell98,
            this.xrTableCell127,
            this.xrTableCell128});
            this.xrTableRow28.Dpi = 254F;
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell125.Dpi = 254F;
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.StylePriority.UseBorders = false;
            this.xrTableCell125.Weight = 0.11161843716525768D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Dpi = 254F;
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Weight = 0.15496907053647069D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell126.Dpi = 254F;
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseBorders = false;
            this.xrTableCell126.Text = "外阴";
            this.xrTableCell126.Weight = 0.15496907053647069D;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell97.Dpi = 254F;
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.StylePriority.UseTextAlignment = false;
            this.xrTableCell97.Text = "  1  未见异常\t2  异常";
            this.xrTableCell97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell97.Weight = 0.56459880215889446D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell98.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_外阴异常});
            this.xrTableCell98.Dpi = 254F;
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseBorders = false;
            this.xrTableCell98.Weight = 0.87780924364338087D;
            // 
            // xrLabel_外阴异常
            // 
            this.xrLabel_外阴异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_外阴异常.Dpi = 254F;
            this.xrLabel_外阴异常.LocationFloat = new DevExpress.Utils.PointFloat(0.0001291911F, 0F);
            this.xrLabel_外阴异常.Name = "xrLabel_外阴异常";
            this.xrLabel_外阴异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_外阴异常.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_外阴异常.StylePriority.UseBorders = false;
            this.xrLabel_外阴异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_外阴异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell127.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_外阴});
            this.xrTableCell127.Dpi = 254F;
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.Weight = 0.13603537595952553D;
            // 
            // xrLabel_外阴
            // 
            this.xrLabel_外阴.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_外阴.Dpi = 254F;
            this.xrLabel_外阴.LocationFloat = new DevExpress.Utils.PointFloat(57.37867F, 8F);
            this.xrLabel_外阴.Name = "xrLabel_外阴";
            this.xrLabel_外阴.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_外阴.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_外阴.StylePriority.UseBorders = false;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell128.Dpi = 254F;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseBorders = false;
            this.xrTableCell128.Weight = 0.19876670037244218D;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.xrTableCell227,
            this.xrTableCell122,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell123,
            this.xrTableCell124});
            this.xrTableRow27.Dpi = 254F;
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell121.Dpi = 254F;
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Weight = 0.11161843716525768D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Dpi = 254F;
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Weight = 0.15496907053647069D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell122.Dpi = 254F;
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.Text = "阴道";
            this.xrTableCell122.Weight = 0.15496907053647069D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell99.Dpi = 254F;
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.StylePriority.UseTextAlignment = false;
            this.xrTableCell99.Text = "  1  未见异常\t2  异常";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell99.Weight = 0.56459880215889446D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell100.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_阴道异常});
            this.xrTableCell100.Dpi = 254F;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.Weight = 0.87780924364338087D;
            // 
            // xrLabel_阴道异常
            // 
            this.xrLabel_阴道异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_阴道异常.Dpi = 254F;
            this.xrLabel_阴道异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_阴道异常.Name = "xrLabel_阴道异常";
            this.xrLabel_阴道异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_阴道异常.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_阴道异常.StylePriority.UseBorders = false;
            this.xrLabel_阴道异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_阴道异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell123.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_阴道});
            this.xrTableCell123.Dpi = 254F;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.Weight = 0.13603537595952553D;
            // 
            // xrLabel_阴道
            // 
            this.xrLabel_阴道.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_阴道.Dpi = 254F;
            this.xrLabel_阴道.LocationFloat = new DevExpress.Utils.PointFloat(57.3797F, 8F);
            this.xrLabel_阴道.Name = "xrLabel_阴道";
            this.xrLabel_阴道.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_阴道.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_阴道.StylePriority.UseBorders = false;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell124.Dpi = 254F;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Weight = 0.19876670037244218D;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell228,
            this.xrTableCell110,
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTableCell111,
            this.xrTable_妇科医师签名});
            this.xrTableRow24.Dpi = 254F;
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell109.Dpi = 254F;
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.Weight = 0.11161843716525768D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Dpi = 254F;
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Text = "妇科*";
            this.xrTableCell228.Weight = 0.15496907053647069D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell110.Dpi = 254F;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.StylePriority.UseTextAlignment = false;
            this.xrTableCell110.Text = "宫颈";
            this.xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell110.Weight = 0.15496907053647069D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell231.Dpi = 254F;
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.StylePriority.UseBorders = false;
            this.xrTableCell231.StylePriority.UseTextAlignment = false;
            this.xrTableCell231.Text = "  1  未见异常\t2  异常";
            this.xrTableCell231.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell231.Weight = 0.56459880215889446D;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell232.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫颈异常});
            this.xrTableCell232.Dpi = 254F;
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.StylePriority.UseBorders = false;
            this.xrTableCell232.Weight = 0.87780924364338087D;
            // 
            // xrLabel_宫颈异常
            // 
            this.xrLabel_宫颈异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_宫颈异常.Dpi = 254F;
            this.xrLabel_宫颈异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_宫颈异常.Name = "xrLabel_宫颈异常";
            this.xrLabel_宫颈异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_宫颈异常.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_宫颈异常.StylePriority.UseBorders = false;
            this.xrLabel_宫颈异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_宫颈异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell111.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫颈});
            this.xrTableCell111.Dpi = 254F;
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseBorders = false;
            this.xrTableCell111.Weight = 0.13603537595952553D;
            // 
            // xrLabel_宫颈
            // 
            this.xrLabel_宫颈.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_宫颈.Dpi = 254F;
            this.xrLabel_宫颈.LocationFloat = new DevExpress.Utils.PointFloat(57.37841F, 8F);
            this.xrLabel_宫颈.Name = "xrLabel_宫颈";
            this.xrLabel_宫颈.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_宫颈.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_宫颈.StylePriority.UseBorders = false;
            // 
            // xrTable_妇科医师签名
            // 
            this.xrTable_妇科医师签名.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_妇科医师签名.Dpi = 254F;
            this.xrTable_妇科医师签名.Name = "xrTable_妇科医师签名";
            this.xrTable_妇科医师签名.StylePriority.UseBorders = false;
            this.xrTable_妇科医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell229,
            this.xrTableCell106,
            this.xrTableCell233,
            this.xrTableCell234,
            this.xrTableCell107,
            this.xrTableCell108});
            this.xrTableRow23.Dpi = 254F;
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell105.Dpi = 254F;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.Weight = 0.11161843716525768D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Dpi = 254F;
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.Weight = 0.15496907053647069D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell106.Dpi = 254F;
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.Text = "宫体";
            this.xrTableCell106.Weight = 0.15496907053647069D;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell233.Dpi = 254F;
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.StylePriority.UseBorders = false;
            this.xrTableCell233.StylePriority.UseTextAlignment = false;
            this.xrTableCell233.Text = "  1  未见异常\t2  异常";
            this.xrTableCell233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell233.Weight = 0.56459880215889446D;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell234.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫体异常});
            this.xrTableCell234.Dpi = 254F;
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.StylePriority.UseBorders = false;
            this.xrTableCell234.Weight = 0.87780924364338087D;
            // 
            // xrLabel_宫体异常
            // 
            this.xrLabel_宫体异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_宫体异常.Dpi = 254F;
            this.xrLabel_宫体异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_宫体异常.Name = "xrLabel_宫体异常";
            this.xrLabel_宫体异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_宫体异常.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_宫体异常.StylePriority.UseBorders = false;
            this.xrLabel_宫体异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_宫体异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell107.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫体});
            this.xrTableCell107.Dpi = 254F;
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.Weight = 0.13603537595952553D;
            // 
            // xrLabel_宫体
            // 
            this.xrLabel_宫体.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_宫体.Dpi = 254F;
            this.xrLabel_宫体.LocationFloat = new DevExpress.Utils.PointFloat(57.37841F, 8F);
            this.xrLabel_宫体.Name = "xrLabel_宫体";
            this.xrLabel_宫体.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_宫体.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_宫体.StylePriority.UseBorders = false;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell108.Dpi = 254F;
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.Weight = 0.19876670037244218D;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell47,
            this.xrTableCell230,
            this.xrTableCell48,
            this.xrTableCell235,
            this.xrTableCell236,
            this.xrTableCell49,
            this.xrTableCell50});
            this.xrTableRow21.Dpi = 254F;
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.Weight = 0.11161843716525768D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell230.Dpi = 254F;
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.StylePriority.UseBorders = false;
            this.xrTableCell230.Weight = 0.15496907053647069D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Text = "附件";
            this.xrTableCell48.Weight = 0.15496907053647069D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell235.Dpi = 254F;
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.StylePriority.UseBorders = false;
            this.xrTableCell235.StylePriority.UseTextAlignment = false;
            this.xrTableCell235.Text = "  1  未见异常\t2  异常";
            this.xrTableCell235.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell235.Weight = 0.56459880215889446D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell236.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_附件异常});
            this.xrTableCell236.Dpi = 254F;
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.StylePriority.UseBorders = false;
            this.xrTableCell236.Weight = 0.87780924364338087D;
            // 
            // xrLabel_附件异常
            // 
            this.xrLabel_附件异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_附件异常.Dpi = 254F;
            this.xrLabel_附件异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_附件异常.Name = "xrLabel_附件异常";
            this.xrLabel_附件异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_附件异常.SizeF = new System.Drawing.SizeF(400F, 49.95313F);
            this.xrLabel_附件异常.StylePriority.UseBorders = false;
            this.xrLabel_附件异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_附件异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_附件});
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.Weight = 0.13603537595952553D;
            // 
            // xrLabel_附件
            // 
            this.xrLabel_附件.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_附件.Dpi = 254F;
            this.xrLabel_附件.LocationFloat = new DevExpress.Utils.PointFloat(57.3797F, 8F);
            this.xrLabel_附件.Name = "xrLabel_附件";
            this.xrLabel_附件.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_附件.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_附件.StylePriority.UseBorders = false;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.Weight = 0.19876670037244218D;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTable_查体其他,
            this.xrTable_查体其他医师签名});
            this.xrTableRow39.Dpi = 254F;
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell169.Dpi = 254F;
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.StylePriority.UseBorders = false;
            this.xrTableCell169.Weight = 0.11161843716525768D;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell170.Dpi = 254F;
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.Text = "  其        他*";
            this.xrTableCell170.Weight = 0.30993814107294138D;
            // 
            // xrTable_查体其他
            // 
            this.xrTable_查体其他.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_查体其他.Dpi = 254F;
            this.xrTable_查体其他.Name = "xrTable_查体其他";
            this.xrTable_查体其他.StylePriority.UseBorders = false;
            this.xrTable_查体其他.StylePriority.UseTextAlignment = false;
            this.xrTable_查体其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable_查体其他.Weight = 1.5784434217618009D;
            // 
            // xrTable_查体其他医师签名
            // 
            this.xrTable_查体其他医师签名.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_查体其他医师签名.Dpi = 254F;
            this.xrTable_查体其他医师签名.Name = "xrTable_查体其他医师签名";
            this.xrTable_查体其他医师签名.StylePriority.UseBorders = false;
            this.xrTable_查体其他医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell189,
            this.xrTableCell190,
            this.xrTableCell75,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell191,
            this.xrTableCell192});
            this.xrTableRow44.Dpi = 254F;
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1D;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell189.Dpi = 254F;
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.StylePriority.UseBorders = false;
            this.xrTableCell189.Weight = 0.11161843716525768D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell190.Dpi = 254F;
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseBorders = false;
            this.xrTableCell190.Text = "血          型";
            this.xrTableCell190.Weight = 0.30993814107294138D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.Text = "ABO";
            this.xrTableCell75.Weight = 0.11160357342917462D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell73.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_ABO});
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.Weight = 0.45299516193904754D;
            // 
            // xrLabel_ABO
            // 
            this.xrLabel_ABO.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_ABO.Dpi = 254F;
            this.xrLabel_ABO.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_ABO.Name = "xrLabel_ABO";
            this.xrLabel_ABO.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_ABO.SizeF = new System.Drawing.SizeF(200F, 49.95313F);
            this.xrLabel_ABO.StylePriority.UseBorders = false;
            this.xrLabel_ABO.StylePriority.UseTextAlignment = false;
            this.xrLabel_ABO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.Text = "Rh*";
            this.xrTableCell74.Weight = 0.089433653715924488D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell191.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_Rh});
            this.xrTableCell191.Dpi = 254F;
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.StylePriority.UseBorders = false;
            this.xrTableCell191.Weight = 0.92441103267765423D;
            // 
            // xrLabel_Rh
            // 
            this.xrLabel_Rh.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_Rh.Dpi = 254F;
            this.xrLabel_Rh.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_Rh.Name = "xrLabel_Rh";
            this.xrLabel_Rh.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_Rh.SizeF = new System.Drawing.SizeF(200F, 49.95313F);
            this.xrLabel_Rh.StylePriority.UseBorders = false;
            this.xrLabel_Rh.StylePriority.UseTextAlignment = false;
            this.xrLabel_Rh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell192.Dpi = 254F;
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.StylePriority.UseBorders = false;
            this.xrTableCell192.Weight = 0.19876670037244218D;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell181,
            this.xrTableCell182,
            this.xrTableCell82,
            this.xrTableCell161,
            this.xrTableCell76,
            this.xrTableCell84,
            this.xrTableCell81,
            this.xrTableCell83,
            this.xrTableCell183,
            this.xrTableCell184});
            this.xrTableRow42.Dpi = 254F;
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell181.Dpi = 254F;
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.StylePriority.UseBorders = false;
            this.xrTableCell181.Weight = 0.11161843716525768D;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell182.Dpi = 254F;
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.Text = "  血  常  规*";
            this.xrTableCell182.Weight = 0.30993814107294138D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Dpi = 254F;
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseTextAlignment = false;
            this.xrTableCell82.Text = "  血红蛋白";
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell82.Weight = 0.18383078109952405D;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血红蛋白});
            this.xrTableCell161.Dpi = 254F;
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Weight = 0.20244520372559244D;
            // 
            // xrLabel_血红蛋白
            // 
            this.xrLabel_血红蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血红蛋白.Dpi = 254F;
            this.xrLabel_血红蛋白.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.746521F);
            this.xrLabel_血红蛋白.Name = "xrLabel_血红蛋白";
            this.xrLabel_血红蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血红蛋白.SizeF = new System.Drawing.SizeF(185F, 49.95313F);
            this.xrLabel_血红蛋白.StylePriority.UseBorders = false;
            this.xrLabel_血红蛋白.StylePriority.UseTextAlignment = false;
            this.xrLabel_血红蛋白.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Dpi = 254F;
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.Text = "g/L    白细胞";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell76.Weight = 0.25238846330730713D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_白细胞});
            this.xrTableCell84.Dpi = 254F;
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Weight = 0.20244520372559247D;
            // 
            // xrLabel_白细胞
            // 
            this.xrLabel_白细胞.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_白细胞.Dpi = 254F;
            this.xrLabel_白细胞.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 1.746521F);
            this.xrLabel_白细胞.Name = "xrLabel_白细胞";
            this.xrLabel_白细胞.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_白细胞.SizeF = new System.Drawing.SizeF(185F, 49.95313F);
            this.xrLabel_白细胞.StylePriority.UseBorders = false;
            this.xrLabel_白细胞.StylePriority.UseTextAlignment = false;
            this.xrLabel_白细胞.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Dpi = 254F;
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Text = "×10^9/L    血小板";
            this.xrTableCell81.Weight = 0.33693406670398385D;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血小板});
            this.xrTableCell83.Dpi = 254F;
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.20244520372559244D;
            // 
            // xrLabel_血小板
            // 
            this.xrLabel_血小板.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血小板.Dpi = 254F;
            this.xrLabel_血小板.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.746521F);
            this.xrLabel_血小板.Name = "xrLabel_血小板";
            this.xrLabel_血小板.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血小板.SizeF = new System.Drawing.SizeF(185F, 49.95313F);
            this.xrLabel_血小板.StylePriority.UseBorders = false;
            this.xrLabel_血小板.StylePriority.UseTextAlignment = false;
            this.xrLabel_血小板.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell183.Dpi = 254F;
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.StylePriority.UseBorders = false;
            this.xrTableCell183.Text = "×10^9/L";
            this.xrTableCell183.Weight = 0.19795449947420848D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell184.Dpi = 254F;
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseBorders = false;
            this.xrTableCell184.Weight = 0.19876670037244218D;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell162,
            this.xrTableCell187,
            this.xrTableCell188});
            this.xrTableRow43.Dpi = 254F;
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell185.Dpi = 254F;
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.StylePriority.UseBorders = false;
            this.xrTableCell185.Weight = 0.11161843716525768D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell186.Dpi = 254F;
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseBorders = false;
            this.xrTableCell186.Weight = 0.30993814107294138D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell162.Dpi = 254F;
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseBorders = false;
            this.xrTableCell162.Text = "其他";
            this.xrTableCell162.Weight = 0.11160362353134568D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell187.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血常规其他});
            this.xrTableCell187.Dpi = 254F;
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.StylePriority.UseBorders = false;
            this.xrTableCell187.Weight = 1.4668397982304553D;
            // 
            // xrLabel_血常规其他
            // 
            this.xrLabel_血常规其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血常规其他.Dpi = 254F;
            this.xrLabel_血常规其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_血常规其他.Name = "xrLabel_血常规其他";
            this.xrLabel_血常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_血常规其他.SizeF = new System.Drawing.SizeF(800F, 49.95313F);
            this.xrLabel_血常规其他.StylePriority.UseBorders = false;
            this.xrLabel_血常规其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_血常规其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell188.Dpi = 254F;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseBorders = false;
            this.xrTableCell188.Weight = 0.19876670037244218D;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell241,
            this.xrTableCell164,
            this.xrTableCell240,
            this.xrTableCell163,
            this.xrTableCell239,
            this.xrTableCell237,
            this.xrTableCell238,
            this.xrTableCell179,
            this.xrTableCell180});
            this.xrTableRow41.Dpi = 254F;
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell177.Dpi = 254F;
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.StylePriority.UseBorders = false;
            this.xrTableCell177.Weight = 0.11161843716525768D;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell178.Dpi = 254F;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.Text = "  尿  常  规*";
            this.xrTableCell178.Weight = 0.30993814107294138D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Dpi = 254F;
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.StylePriority.UseBorders = false;
            this.xrTableCell241.Text = "尿蛋白";
            this.xrTableCell241.Weight = 0.13131580094146936D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿蛋白});
            this.xrTableCell164.Dpi = 254F;
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseBorders = false;
            this.xrTableCell164.Weight = 0.26329505449898088D;
            // 
            // xrLabel_尿蛋白
            // 
            this.xrLabel_尿蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿蛋白.Dpi = 254F;
            this.xrLabel_尿蛋白.LocationFloat = new DevExpress.Utils.PointFloat(0.0001621246F, 0F);
            this.xrLabel_尿蛋白.Name = "xrLabel_尿蛋白";
            this.xrLabel_尿蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_尿蛋白.SizeF = new System.Drawing.SizeF(240.6061F, 49.95313F);
            this.xrLabel_尿蛋白.StylePriority.UseBorders = false;
            this.xrLabel_尿蛋白.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿蛋白.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Dpi = 254F;
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.StylePriority.UseBorders = false;
            this.xrTableCell240.Text = "尿糖";
            this.xrTableCell240.Weight = 0.10395834154328126D;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿糖});
            this.xrTableCell163.Dpi = 254F;
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.StylePriority.UseBorders = false;
            this.xrTableCell163.Weight = 0.26329914542766286D;
            // 
            // xrLabel_尿糖
            // 
            this.xrLabel_尿糖.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿糖.Dpi = 254F;
            this.xrLabel_尿糖.LocationFloat = new DevExpress.Utils.PointFloat(0.003799438F, 0F);
            this.xrLabel_尿糖.Name = "xrLabel_尿糖";
            this.xrLabel_尿糖.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_尿糖.SizeF = new System.Drawing.SizeF(240.6061F, 49.95313F);
            this.xrLabel_尿糖.StylePriority.UseBorders = false;
            this.xrLabel_尿糖.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿糖.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Dpi = 254F;
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.StylePriority.UseBorders = false;
            this.xrTableCell239.Text = "尿酮体";
            this.xrTableCell239.Weight = 0.1313158092903034D;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿酮体});
            this.xrTableCell237.Dpi = 254F;
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.StylePriority.UseBorders = false;
            this.xrTableCell237.Weight = 0.26329912038116071D;
            // 
            // xrLabel_尿酮体
            // 
            this.xrLabel_尿酮体.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿酮体.Dpi = 254F;
            this.xrLabel_尿酮体.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 0F);
            this.xrLabel_尿酮体.Name = "xrLabel_尿酮体";
            this.xrLabel_尿酮体.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_尿酮体.SizeF = new System.Drawing.SizeF(240.6061F, 49.95313F);
            this.xrLabel_尿酮体.StylePriority.UseBorders = false;
            this.xrLabel_尿酮体.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿酮体.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Dpi = 254F;
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseBorders = false;
            this.xrTableCell238.Text = "尿潜血";
            this.xrTableCell238.Weight = 0.1313158092903034D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell179.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿潜血});
            this.xrTableCell179.Dpi = 254F;
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.StylePriority.UseBorders = false;
            this.xrTableCell179.Weight = 0.29064434038863907D;
            // 
            // xrLabel_尿潜血
            // 
            this.xrLabel_尿潜血.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿潜血.Dpi = 254F;
            this.xrLabel_尿潜血.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_尿潜血.Name = "xrLabel_尿潜血";
            this.xrLabel_尿潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_尿潜血.SizeF = new System.Drawing.SizeF(240.6061F, 49.95313F);
            this.xrLabel_尿潜血.StylePriority.UseBorders = false;
            this.xrLabel_尿潜血.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿潜血.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell180.Dpi = 254F;
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.StylePriority.UseBorders = false;
            this.xrTableCell180.Weight = 0.19876670037244218D;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell242,
            this.xrTableCell175,
            this.xrTableCell176});
            this.xrTableRow40.Dpi = 254F;
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell173.Dpi = 254F;
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.StylePriority.UseBorders = false;
            this.xrTableCell173.Weight = 0.11161843716525768D;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell174.Dpi = 254F;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.StylePriority.UseBorders = false;
            this.xrTableCell174.Weight = 0.30993814107294138D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell242.Dpi = 254F;
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.StylePriority.UseBorders = false;
            this.xrTableCell242.Text = "其他";
            this.xrTableCell242.Weight = 0.11160362353134568D;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell175.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿常规其他});
            this.xrTableCell175.Dpi = 254F;
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseBorders = false;
            this.xrTableCell175.Weight = 1.4668397982304553D;
            // 
            // xrLabel_尿常规其他
            // 
            this.xrLabel_尿常规其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿常规其他.Dpi = 254F;
            this.xrLabel_尿常规其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_尿常规其他.Name = "xrLabel_尿常规其他";
            this.xrLabel_尿常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_尿常规其他.SizeF = new System.Drawing.SizeF(800F, 49.95313F);
            this.xrLabel_尿常规其他.StylePriority.UseBorders = false;
            this.xrLabel_尿常规其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿常规其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell176.Dpi = 254F;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.StylePriority.UseBorders = false;
            this.xrTableCell176.Weight = 0.19876670037244218D;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell244,
            this.xrTableCell243,
            this.xrTableCell245,
            this.xrTableCell159,
            this.xrTableCell160});
            this.xrTableRow36.Dpi = 254F;
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell157.Dpi = 254F;
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.StylePriority.UseBorders = false;
            this.xrTableCell157.Weight = 0.11161843716525768D;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell158.Dpi = 254F;
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.Text = "  空腹血糖*";
            this.xrTableCell158.Weight = 0.30993814107294138D;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell244.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_空腹血糖1});
            this.xrTableCell244.Dpi = 254F;
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.StylePriority.UseBorders = false;
            this.xrTableCell244.Weight = 0.39394742369190966D;
            // 
            // xrLabel_空腹血糖1
            // 
            this.xrLabel_空腹血糖1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_空腹血糖1.Dpi = 254F;
            this.xrLabel_空腹血糖1.LocationFloat = new DevExpress.Utils.PointFloat(30.00003F, 0F);
            this.xrLabel_空腹血糖1.Name = "xrLabel_空腹血糖1";
            this.xrLabel_空腹血糖1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_空腹血糖1.SizeF = new System.Drawing.SizeF(330F, 49.95313F);
            this.xrLabel_空腹血糖1.StylePriority.UseBorders = false;
            this.xrLabel_空腹血糖1.StylePriority.UseTextAlignment = false;
            this.xrLabel_空腹血糖1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell243.Dpi = 254F;
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.StylePriority.UseBorders = false;
            this.xrTableCell243.StylePriority.UseTextAlignment = false;
            this.xrTableCell243.Text = "mmol/L    或";
            this.xrTableCell243.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell243.Weight = 0.2261868707662939D;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell245.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_空腹血糖2});
            this.xrTableCell245.Dpi = 254F;
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.StylePriority.UseBorders = false;
            this.xrTableCell245.Weight = 0.36151375631077176D;
            // 
            // xrLabel_空腹血糖2
            // 
            this.xrLabel_空腹血糖2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_空腹血糖2.Dpi = 254F;
            this.xrLabel_空腹血糖2.LocationFloat = new DevExpress.Utils.PointFloat(0.3613475F, 0F);
            this.xrLabel_空腹血糖2.Name = "xrLabel_空腹血糖2";
            this.xrLabel_空腹血糖2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_空腹血糖2.SizeF = new System.Drawing.SizeF(330F, 49.95313F);
            this.xrLabel_空腹血糖2.StylePriority.UseBorders = false;
            this.xrLabel_空腹血糖2.StylePriority.UseTextAlignment = false;
            this.xrLabel_空腹血糖2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell159.Dpi = 254F;
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.StylePriority.UseBorders = false;
            this.xrTableCell159.StylePriority.UseTextAlignment = false;
            this.xrTableCell159.Text = "  mg/dL";
            this.xrTableCell159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell159.Weight = 0.59679537099282554D;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell160.Dpi = 254F;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.Weight = 0.19876670037244218D;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell246,
            this.xrTableCell167,
            this.xrTableCell168});
            this.xrTableRow38.Dpi = 254F;
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell165.Dpi = 254F;
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.StylePriority.UseBorders = false;
            this.xrTableCell165.Weight = 0.11161843716525768D;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell166.Dpi = 254F;
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.Text = "同型半胱氨酸*";
            this.xrTableCell166.Weight = 0.30993814107294138D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell246.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_同型半胱氨酸});
            this.xrTableCell246.Dpi = 254F;
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.StylePriority.UseBorders = false;
            this.xrTableCell246.Weight = 0.67804095581226054D;
            // 
            // xrLabel_同型半胱氨酸
            // 
            this.xrLabel_同型半胱氨酸.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_同型半胱氨酸.Dpi = 254F;
            this.xrLabel_同型半胱氨酸.LocationFloat = new DevExpress.Utils.PointFloat(30.00007F, 0F);
            this.xrLabel_同型半胱氨酸.Name = "xrLabel_同型半胱氨酸";
            this.xrLabel_同型半胱氨酸.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_同型半胱氨酸.SizeF = new System.Drawing.SizeF(589.6124F, 49.95313F);
            this.xrLabel_同型半胱氨酸.StylePriority.UseBorders = false;
            this.xrLabel_同型半胱氨酸.StylePriority.UseTextAlignment = false;
            this.xrLabel_同型半胱氨酸.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell167.Dpi = 254F;
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.StylePriority.UseBorders = false;
            this.xrTableCell167.StylePriority.UseTextAlignment = false;
            this.xrTableCell167.Text = "  umol/L";
            this.xrTableCell167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell167.Weight = 0.90040246594954032D;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell168.Dpi = 254F;
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.StylePriority.UseBorders = false;
            this.xrTableCell168.Weight = 0.19876670037244218D;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell247,
            this.xrTableCell95,
            this.xrTableCell96});
            this.xrTableRow17.Dpi = 254F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell93.Dpi = 254F;
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 0.11161843716525768D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Dpi = 254F;
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.Text = "尿微量白蛋白*";
            this.xrTableCell94.Weight = 0.30993814107294138D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell247.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿微量白蛋白});
            this.xrTableCell247.Dpi = 254F;
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.StylePriority.UseBorders = false;
            this.xrTableCell247.Weight = 0.39394743204991062D;
            // 
            // xrLabel_尿微量白蛋白
            // 
            this.xrLabel_尿微量白蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿微量白蛋白.Dpi = 254F;
            this.xrLabel_尿微量白蛋白.LocationFloat = new DevExpress.Utils.PointFloat(30.00007F, 0F);
            this.xrLabel_尿微量白蛋白.Name = "xrLabel_尿微量白蛋白";
            this.xrLabel_尿微量白蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_尿微量白蛋白.SizeF = new System.Drawing.SizeF(330F, 49.95313F);
            this.xrLabel_尿微量白蛋白.StylePriority.UseBorders = false;
            this.xrLabel_尿微量白蛋白.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿微量白蛋白.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell95.Dpi = 254F;
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.StylePriority.UseTextAlignment = false;
            this.xrTableCell95.Text = "mg/dL";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell95.Weight = 1.1844959897118903D;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell96.Dpi = 254F;
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.StylePriority.UseBorders = false;
            this.xrTableCell96.Weight = 0.19876670037244218D;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCrossBandLine1
            // 
            this.xrCrossBandLine1.Dpi = 254F;
            this.xrCrossBandLine1.EndBand = this.Detail;
            this.xrCrossBandLine1.EndPointFloat = new DevExpress.Utils.PointFloat(2050F, 2948.633F);
            this.xrCrossBandLine1.LocationFloat = new DevExpress.Utils.PointFloat(2050F, 0F);
            this.xrCrossBandLine1.Name = "xrCrossBandLine1";
            this.xrCrossBandLine1.StartBand = this.Detail;
            this.xrCrossBandLine1.StartPointFloat = new DevExpress.Utils.PointFloat(2050F, 0F);
            this.xrCrossBandLine1.Visible = false;
            this.xrCrossBandLine1.WidthF = 3F;
            // 
            // report健康体检表2_2017
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.CrossBandControls.AddRange(new DevExpress.XtraReports.UI.XRCrossBandControl[] {
            this.xrCrossBandLine1});
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(38, 43, 0, 0);
            this.PageHeight = 2970;
            this.PageWidth = 4200;
            this.PaperKind = System.Drawing.Printing.PaperKind.A3;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_糖化血红蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乳腺1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_脏器功能医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_眼底医师签名;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_口唇;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_缺齿3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_缺齿4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_龋齿3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_龋齿4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_义齿3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_义齿4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_缺齿1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_缺齿2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_龋齿1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_龋齿2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_义齿1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_义齿2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_齿列1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_齿列2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_咽部;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_视力左眼;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_视力右眼;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_矫正视力左眼;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_矫正视力右眼;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_听力;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_运动能力;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_查体医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_肛门指诊医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_乳腺医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_妇科医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_查体其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_查体其他医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼底异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼底;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_皮肤其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_皮肤;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_巩膜其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_巩膜;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_淋巴结其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_淋巴结;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_桶状胸;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_呼吸音;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_罗音;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_呼吸音异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_罗音其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心率;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_杂音有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心律;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_杂音;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_压痛有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_包块有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肝大有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脾大有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_移动性浊音有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_压痛;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_包块;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肝大;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脾大;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_移动性浊音;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_下肢水肿;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肛门指诊;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乳腺4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乳腺2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乳腺3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肛门指诊其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乳腺其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_外阴异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴道异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫颈异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫体异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_附件异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_外阴;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴道;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫颈;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫体;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_附件;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_ABO;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_Rh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血红蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_白细胞;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血小板;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血常规其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿蛋白;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿酮体;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿糖;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿潜血;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿常规其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_空腹血糖1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_空腹血糖2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_同型半胱氨酸;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿微量白蛋白;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell359;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell361;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell362;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell355;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell356;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell357;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell358;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell351;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell352;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell353;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell354;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_辅助检查其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell347;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell348;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell349;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell350;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell344;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell346;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell364;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell365;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell366;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell367;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell372;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell373;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell374;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell375;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell368;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell369;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell370;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell371;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell384;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史病案号1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell387;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell381;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史病案号2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell383;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell376;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell377;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell378;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell379;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell363;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_大便潜血;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell393;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乙型肝炎表面抗原;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell396;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell394;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell395;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell397;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell401;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell399;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell400;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell402;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell403;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血清低密度脂蛋白胆固醇;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血清谷丙转氨酶;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血清谷草转氨酶;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_白蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_总胆红素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_综合胆红素;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell405;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell406;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell409;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell410;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell411;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血清肌酐;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血尿素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血钾浓度;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血钠浓度;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell413;
        private DevExpress.XtraReports.UI.XRTableCell xrTableC;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell414;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell415;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell416;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell417;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell418;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell419;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血清高密度脂蛋白胆固醇;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell420;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell421;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell422;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_胸部X线片;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_胸部X线片异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell425;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell424;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_腹部B超;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell427;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell428;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_B超其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_腹部B超异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_B超其他异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell423;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell426;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫颈涂片异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫颈涂片;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell429;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell430;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell431;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell432;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell433;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell434;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell435;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell436;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell437;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell438;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼部疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_其他系统疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_神经系统其他疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼部疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼部疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼部疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_神经系统其他疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel85;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_神经系统其他疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_神经系统其他疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_其他系统疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_其他系统疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_其他系统疾病3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell389;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell388;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell391;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell439;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_撤床日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史原因1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史机构及科室1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史病案号1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell444;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell445;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell446;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_撤床日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史原因2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史机构及科室2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史病案号2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell451;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell454;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell452;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell453;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_出院日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史原因1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史机构及科室1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_出院日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史原因2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史机构及科室2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_入院日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell462;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_入院日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell464;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_建床日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell467;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_建床日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell468;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_总胆固醇;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_甘油三酯;
    }
}
