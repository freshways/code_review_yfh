﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class UC会诊记录表_显示 : UserControl
    {
        public UC会诊记录表_显示()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            UC会诊记录表 control = new UC会诊记录表();
            Control parent = this.Parent;
            if (parent == null) return;
            Size size = control.Size;
            parent.Controls.Clear();
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(WaitForm1));
            parent.Controls.Add(control);
            control.Size = size;
            int maxSize = parent.Width;
            int minSize = control.Width;

            if (maxSize >= minSize)
            {
                control.Left = (maxSize - minSize) / 2;
            }
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {

        }
    }
}
