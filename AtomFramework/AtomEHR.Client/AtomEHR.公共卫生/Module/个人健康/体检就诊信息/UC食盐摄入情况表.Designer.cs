﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class UC食盐摄入情况表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC食盐摄入情况表));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.btn填表说明 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.lab当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.lab最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.lab创建人 = new DevExpress.XtraEditors.TextEdit();
            this.lab创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.lbl创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt家庭膳食3 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt家庭膳食2 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl14 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl15 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt家庭膳食1 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt家庭膳食11 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.txt医生签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.radio随访方式 = new DevExpress.XtraEditors.RadioGroup();
            this.txt下次随访时间 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lab当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFocusForSave.Location = new System.Drawing.Point(0, 0);
            this.txtFocusForSave.Size = new System.Drawing.Size(815, 14);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Controls.Add(this.btn填表说明);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(815, 28);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn重置
            // 
            this.btn重置.Image = ((System.Drawing.Image)(resources.GetObject("btn重置.Image")));
            this.btn重置.Location = new System.Drawing.Point(84, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "取消";
            this.btn重置.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // btn填表说明
            // 
            this.btn填表说明.Image = ((System.Drawing.Image)(resources.GetObject("btn填表说明.Image")));
            this.btn填表说明.Location = new System.Drawing.Point(165, 3);
            this.btn填表说明.Name = "btn填表说明";
            this.btn填表说明.Size = new System.Drawing.Size(75, 23);
            this.btn填表说明.TabIndex = 2;
            this.btn填表说明.Text = "填表说明";
            this.btn填表说明.Visible = false;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.lab当前所属机构);
            this.layoutControl1.Controls.Add(this.lab最近修改人);
            this.layoutControl1.Controls.Add(this.lab创建人);
            this.layoutControl1.Controls.Add(this.lab创建机构);
            this.layoutControl1.Controls.Add(this.lbl最近更新时间);
            this.layoutControl1.Controls.Add(this.lbl创建时间);
            this.layoutControl1.Controls.Add(this.txt家庭膳食3);
            this.layoutControl1.Controls.Add(this.txt家庭膳食2);
            this.layoutControl1.Controls.Add(this.ucLblTxtLbl14);
            this.layoutControl1.Controls.Add(this.ucLblTxtLbl15);
            this.layoutControl1.Controls.Add(this.txt家庭膳食1);
            this.layoutControl1.Controls.Add(this.txt家庭膳食11);
            this.layoutControl1.Controls.Add(this.txt医生签名);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt个人档案号);
            this.layoutControl1.Controls.Add(this.txt随访日期);
            this.layoutControl1.Controls.Add(this.radio随访方式);
            this.layoutControl1.Controls.Add(this.txt下次随访时间);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 28);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1038, 489, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(815, 548);
            this.layoutControl1.TabIndex = 124;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // lab当前所属机构
            // 
            this.lab当前所属机构.Location = new System.Drawing.Point(647, 523);
            this.lab当前所属机构.Name = "lab当前所属机构";
            this.lab当前所属机构.Properties.ReadOnly = true;
            this.lab当前所属机构.Size = new System.Drawing.Size(163, 20);
            this.lab当前所属机构.StyleController = this.layoutControl1;
            this.lab当前所属机构.TabIndex = 146;
            // 
            // lab最近修改人
            // 
            this.lab最近修改人.Location = new System.Drawing.Point(370, 523);
            this.lab最近修改人.Name = "lab最近修改人";
            this.lab最近修改人.Properties.ReadOnly = true;
            this.lab最近修改人.Size = new System.Drawing.Size(188, 20);
            this.lab最近修改人.StyleController = this.layoutControl1;
            this.lab最近修改人.TabIndex = 145;
            // 
            // lab创建人
            // 
            this.lab创建人.Location = new System.Drawing.Point(60, 523);
            this.lab创建人.Name = "lab创建人";
            this.lab创建人.Properties.ReadOnly = true;
            this.lab创建人.Size = new System.Drawing.Size(221, 20);
            this.lab创建人.StyleController = this.layoutControl1;
            this.lab创建人.TabIndex = 144;
            // 
            // lab创建机构
            // 
            this.lab创建机构.Location = new System.Drawing.Point(647, 499);
            this.lab创建机构.Name = "lab创建机构";
            this.lab创建机构.Properties.ReadOnly = true;
            this.lab创建机构.Size = new System.Drawing.Size(163, 20);
            this.lab创建机构.StyleController = this.layoutControl1;
            this.lab创建机构.TabIndex = 143;
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.Location = new System.Drawing.Point(370, 499);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Properties.ReadOnly = true;
            this.lbl最近更新时间.Size = new System.Drawing.Size(188, 20);
            this.lbl最近更新时间.StyleController = this.layoutControl1;
            this.lbl最近更新时间.TabIndex = 142;
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.Location = new System.Drawing.Point(60, 499);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Properties.ReadOnly = true;
            this.lbl创建时间.Size = new System.Drawing.Size(221, 20);
            this.lbl创建时间.StyleController = this.layoutControl1;
            this.lbl创建时间.TabIndex = 141;
            // 
            // txt家庭膳食3
            // 
            this.txt家庭膳食3.Lbl1Size = new System.Drawing.Size(300, 18);
            this.txt家庭膳食3.Lbl1Text = "3.  过去的3个月，您家平均一个月吃";
            this.txt家庭膳食3.Lbl2Size = new System.Drawing.Size(46, 18);
            this.txt家庭膳食3.Lbl2Text = "克酱油？";
            this.txt家庭膳食3.Location = new System.Drawing.Point(13, 363);
            this.txt家庭膳食3.Margin = new System.Windows.Forms.Padding(0);
            this.txt家庭膳食3.Name = "txt家庭膳食3";
            this.txt家庭膳食3.Size = new System.Drawing.Size(789, 59);
            this.txt家庭膳食3.TabIndex = 140;
            this.txt家庭膳食3.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt家庭膳食2
            // 
            this.txt家庭膳食2.Lbl1Size = new System.Drawing.Size(300, 18);
            this.txt家庭膳食2.Lbl1Text = "2.  过去的3个月，您家平均一个月吃";
            this.txt家庭膳食2.Lbl2Size = new System.Drawing.Size(46, 18);
            this.txt家庭膳食2.Lbl2Text = "克盐？";
            this.txt家庭膳食2.Location = new System.Drawing.Point(13, 298);
            this.txt家庭膳食2.Margin = new System.Windows.Forms.Padding(0);
            this.txt家庭膳食2.Name = "txt家庭膳食2";
            this.txt家庭膳食2.Size = new System.Drawing.Size(789, 61);
            this.txt家庭膳食2.TabIndex = 139;
            this.txt家庭膳食2.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // ucLblTxtLbl14
            // 
            this.ucLblTxtLbl14.Lbl1Size = new System.Drawing.Size(550, 18);
            this.ucLblTxtLbl14.Lbl1Text = "  过去的3个月，您家平均一个月吃";
            this.ucLblTxtLbl14.Lbl2Size = new System.Drawing.Size(50, 18);
            this.ucLblTxtLbl14.Lbl2Text = "克酱油？";
            this.ucLblTxtLbl14.Location = new System.Drawing.Point(127, -357);
            this.ucLblTxtLbl14.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl14.Name = "ucLblTxtLbl14";
            this.ucLblTxtLbl14.Size = new System.Drawing.Size(651, 20);
            this.ucLblTxtLbl14.TabIndex = 127;
            this.ucLblTxtLbl14.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // ucLblTxtLbl15
            // 
            this.ucLblTxtLbl15.Lbl1Size = new System.Drawing.Size(550, 18);
            this.ucLblTxtLbl15.Lbl1Text = "  过去的3个月，您家平均一个月吃";
            this.ucLblTxtLbl15.Lbl2Size = new System.Drawing.Size(50, 18);
            this.ucLblTxtLbl15.Lbl2Text = "克盐？";
            this.ucLblTxtLbl15.Location = new System.Drawing.Point(127, -357);
            this.ucLblTxtLbl15.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl15.Name = "ucLblTxtLbl15";
            this.ucLblTxtLbl15.Size = new System.Drawing.Size(651, 20);
            this.ucLblTxtLbl15.TabIndex = 128;
            this.ucLblTxtLbl15.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt家庭膳食1
            // 
            this.txt家庭膳食1.Lbl1Size = new System.Drawing.Size(300, 18);
            this.txt家庭膳食1.Lbl1Text = "1.  过去3个月内，常住在家中一起吃饭的人数？：";
            this.txt家庭膳食1.Lbl2Size = new System.Drawing.Size(350, 18);
            this.txt家庭膳食1.Lbl2Text = "（包括没有户籍但在您家居住的人，如亲戚、保姆等）";
            this.txt家庭膳食1.Location = new System.Drawing.Point(13, 170);
            this.txt家庭膳食1.Margin = new System.Windows.Forms.Padding(0);
            this.txt家庭膳食1.Name = "txt家庭膳食1";
            this.txt家庭膳食1.Size = new System.Drawing.Size(789, 60);
            this.txt家庭膳食1.TabIndex = 125;
            this.txt家庭膳食1.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt家庭膳食11
            // 
            this.txt家庭膳食11.Lbl1Size = new System.Drawing.Size(300, 18);
            this.txt家庭膳食11.Lbl1Text = "1.1  其中一起吃饭的低于6岁的儿童有几人？";
            this.txt家庭膳食11.Lbl2Size = new System.Drawing.Size(50, 18);
            this.txt家庭膳食11.Lbl2Text = "";
            this.txt家庭膳食11.Location = new System.Drawing.Point(13, 234);
            this.txt家庭膳食11.Margin = new System.Windows.Forms.Padding(0);
            this.txt家庭膳食11.Name = "txt家庭膳食11";
            this.txt家庭膳食11.Size = new System.Drawing.Size(789, 60);
            this.txt家庭膳食11.TabIndex = 126;
            this.txt家庭膳食11.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt医生签名
            // 
            this.txt医生签名.Location = new System.Drawing.Point(490, 469);
            this.txt医生签名.Name = "txt医生签名";
            this.txt医生签名.Size = new System.Drawing.Size(314, 20);
            this.txt医生签名.StyleController = this.layoutControl1;
            this.txt医生签名.TabIndex = 40;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(94, 110);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(493, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 14;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(467, 86);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(335, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 13;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(94, 86);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(288, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 12;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(467, 62);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(335, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 11;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(94, 62);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(288, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 10;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(467, 38);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(335, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(94, 38);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.txt个人档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案号.Properties.ReadOnly = true;
            this.txt个人档案号.Size = new System.Drawing.Size(288, 20);
            this.txt个人档案号.StyleController = this.layoutControl1;
            this.txt个人档案号.TabIndex = 4;
            // 
            // txt随访日期
            // 
            this.txt随访日期.EditValue = null;
            this.txt随访日期.Location = new System.Drawing.Point(116, 440);
            this.txt随访日期.Name = "txt随访日期";
            this.txt随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt随访日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txt随访日期.Size = new System.Drawing.Size(289, 20);
            this.txt随访日期.StyleController = this.layoutControl1;
            this.txt随访日期.TabIndex = 8;
            // 
            // radio随访方式
            // 
            this.radio随访方式.EditValue = "";
            this.radio随访方式.Location = new System.Drawing.Point(490, 440);
            this.radio随访方式.Name = "radio随访方式";
            this.radio随访方式.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "门诊"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "家庭"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "电话")});
            this.radio随访方式.Size = new System.Drawing.Size(314, 25);
            this.radio随访方式.StyleController = this.layoutControl1;
            this.radio随访方式.TabIndex = 9;
            // 
            // txt下次随访时间
            // 
            this.txt下次随访时间.EditValue = null;
            this.txt下次随访时间.Location = new System.Drawing.Point(116, 464);
            this.txt下次随访时间.Name = "txt下次随访时间";
            this.txt下次随访时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt下次随访时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt下次随访时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.txt下次随访时间.Size = new System.Drawing.Size(289, 20);
            this.txt下次随访时间.StyleController = this.layoutControl1;
            this.txt下次随访时间.TabIndex = 39;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup4,
            this.layoutControlGroup3,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem40,
            this.layoutControlItem42});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup1.Size = new System.Drawing.Size(815, 548);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup2.CustomizationFormText = "食盐摄入情况表";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup2.Size = new System.Drawing.Size(809, 137);
            this.layoutControlGroup2.Text = "食盐摄入情况表";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt个人档案号;
            this.layoutControlItem1.CustomizationFormText = "个人档案编号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(373, 24);
            this.layoutControlItem1.Text = "个人档案编号";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt性别;
            this.layoutControlItem7.CustomizationFormText = "性别";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(373, 24);
            this.layoutControlItem7.Text = "性别";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名";
            this.layoutControlItem2.Location = new System.Drawing.Point(373, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(420, 24);
            this.layoutControlItem2.Text = "姓名";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt出生日期;
            this.layoutControlItem8.CustomizationFormText = "出生日期";
            this.layoutControlItem8.Location = new System.Drawing.Point(373, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(420, 24);
            this.layoutControlItem8.Text = "出生日期";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt身份证号;
            this.layoutControlItem9.CustomizationFormText = "身份证号";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(373, 24);
            this.layoutControlItem9.Text = "身份证号";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt联系电话;
            this.layoutControlItem10.CustomizationFormText = "联系电话";
            this.layoutControlItem10.Location = new System.Drawing.Point(373, 48);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(420, 24);
            this.layoutControlItem10.Text = "联系电话";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt居住地址;
            this.layoutControlItem11.CustomizationFormText = "家庭住址";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(578, 24);
            this.layoutControlItem11.Text = "家庭住址";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(78, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(578, 72);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(215, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem30,
            this.layoutControlItem31});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 429);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(3, 3, 3, 3);
            this.layoutControlGroup4.Size = new System.Drawing.Size(809, 65);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt随访日期;
            this.layoutControlItem5.CustomizationFormText = "随访日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(398, 24);
            this.layoutControlItem5.Text = "随访日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.radio随访方式;
            this.layoutControlItem6.CustomizationFormText = "随访方式";
            this.layoutControlItem6.Location = new System.Drawing.Point(398, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(399, 29);
            this.layoutControlItem6.Text = "随访方式";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.txt下次随访时间;
            this.layoutControlItem30.CustomizationFormText = "下次随访时间";
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(398, 29);
            this.layoutControlItem30.Text = "下次随访时间";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.txt医生签名;
            this.layoutControlItem31.CustomizationFormText = "随访医生签名";
            this.layoutControlItem31.Location = new System.Drawing.Point(398, 29);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(399, 24);
            this.layoutControlItem31.Text = "随访医生";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(78, 14);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "食盐摄入情况";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 137);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup3.Size = new System.Drawing.Size(809, 292);
            this.layoutControlGroup3.Text = "食盐摄入情况";
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem32.Control = this.txt家庭膳食1;
            this.layoutControlItem32.CustomizationFormText = "1.";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(793, 64);
            this.layoutControlItem32.Text = "1.  ";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextToControlDistance = 0;
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem33.Control = this.txt家庭膳食11;
            this.layoutControlItem33.CustomizationFormText = "1.1";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 64);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(793, 64);
            this.layoutControlItem33.Text = "1.1  ";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextToControlDistance = 0;
            this.layoutControlItem33.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem34.Control = this.txt家庭膳食2;
            this.layoutControlItem34.CustomizationFormText = "2.  ";
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 128);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(793, 65);
            this.layoutControlItem34.Text = "2.  ";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem34.TextToControlDistance = 0;
            this.layoutControlItem34.TextVisible = false;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem35.Control = this.txt家庭膳食3;
            this.layoutControlItem35.CustomizationFormText = "3.  ";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 193);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(793, 63);
            this.layoutControlItem35.Text = "3.  ";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem35.TextToControlDistance = 0;
            this.layoutControlItem35.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.Control = this.lbl创建时间;
            this.layoutControlItem36.CustomizationFormText = "录入时间";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 494);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(280, 24);
            this.layoutControlItem36.Text = "录入时间";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.lbl最近更新时间;
            this.layoutControlItem37.CustomizationFormText = "最近更新时间";
            this.layoutControlItem37.Location = new System.Drawing.Point(280, 494);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(277, 24);
            this.layoutControlItem37.Text = "最近更新时间";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.Control = this.lab创建机构;
            this.layoutControlItem38.CustomizationFormText = "创建机构";
            this.layoutControlItem38.Location = new System.Drawing.Point(557, 494);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(252, 24);
            this.layoutControlItem38.Text = "创建机构";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.lab创建人;
            this.layoutControlItem39.CustomizationFormText = "录入人";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 518);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(280, 24);
            this.layoutControlItem39.Text = "录入人";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.lab最近修改人;
            this.layoutControlItem40.CustomizationFormText = "最近更新人";
            this.layoutControlItem40.Location = new System.Drawing.Point(280, 518);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(277, 24);
            this.layoutControlItem40.Text = "最近更新人";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.lab当前所属机构;
            this.layoutControlItem42.CustomizationFormText = "当前所属机构";
            this.layoutControlItem42.Location = new System.Drawing.Point(557, 518);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(252, 24);
            this.layoutControlItem42.Text = "当前所属机构";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.CustomizationFormText = "日吸烟量";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.Name = "item2";
            this.layoutControlItem14.Size = new System.Drawing.Size(197, 30);
            this.layoutControlItem14.Tag = "check";
            this.layoutControlItem14.Text = "日吸烟量";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(82, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup12.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(205, 56);
            this.layoutControlGroup12.Text = "layoutControlGroup12";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlGroup9.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(205, 38);
            this.layoutControlGroup9.Text = "layoutControlGroup9";
            // 
            // UC食盐摄入情况表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UC食盐摄入情况表";
            this.Size = new System.Drawing.Size(815, 576);
            this.Load += new System.EventHandler(this.UC高血压高危干预_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lab当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lab创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医生签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下次随访时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraEditors.SimpleButton btn填表说明;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.DateEdit txt随访日期;
        private DevExpress.XtraEditors.RadioGroup radio随访方式;
        private DevExpress.XtraEditors.TextEdit txt医生签名;
        private DevExpress.XtraEditors.DateEdit txt下次随访时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl14;
        private Library.UserControls.UCLblTxtLbl txt家庭膳食1;
        private Library.UserControls.UCLblTxtLbl txt家庭膳食11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl15;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private Library.UserControls.UCLblTxtLbl txt家庭膳食3;
        private Library.UserControls.UCLblTxtLbl txt家庭膳食2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraEditors.TextEdit lab当前所属机构;
        private DevExpress.XtraEditors.TextEdit lab最近修改人;
        private DevExpress.XtraEditors.TextEdit lab创建人;
        private DevExpress.XtraEditors.TextEdit lab创建机构;
        private DevExpress.XtraEditors.TextEdit lbl最近更新时间;
        private DevExpress.XtraEditors.TextEdit lbl创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
    }
}
