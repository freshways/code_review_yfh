﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class UC接诊记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC接诊记录表));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn填表说明 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutPanel1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.btn选择客观资料 = new DevExpress.XtraEditors.SimpleButton();
            this.btn选择评估资料 = new DevExpress.XtraEditors.SimpleButton();
            this.btn选择主观资料 = new DevExpress.XtraEditors.SimpleButton();
            this.txt接诊医生 = new DevExpress.XtraEditors.TextEdit();
            this.txt接诊时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt客观资料 = new DevExpress.XtraEditors.MemoEdit();
            this.txt主观资料 = new DevExpress.XtraEditors.MemoEdit();
            this.lab考核项 = new DevExpress.XtraEditors.LabelControl();
            this.lbl个人档案号 = new DevExpress.XtraEditors.LabelControl();
            this.lbl性别 = new DevExpress.XtraEditors.LabelControl();
            this.lbl出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.lbl姓名 = new DevExpress.XtraEditors.LabelControl();
            this.lbl身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.lbl联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl居住地址 = new DevExpress.XtraEditors.LabelControl();
            this.txt评估 = new DevExpress.XtraEditors.MemoEdit();
            this.txt处置计划 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPanel1)).BeginInit();
            this.LayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt客观资料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主观资料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt评估.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt处置计划.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Controls.Add(this.btn填表说明);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(651, 28);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 3;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn填表说明
            // 
            this.btn填表说明.Image = ((System.Drawing.Image)(resources.GetObject("btn填表说明.Image")));
            this.btn填表说明.Location = new System.Drawing.Point(165, 3);
            this.btn填表说明.Name = "btn填表说明";
            this.btn填表说明.Size = new System.Drawing.Size(75, 23);
            this.btn填表说明.TabIndex = 3;
            this.btn填表说明.Text = "填表说明";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.CustomizationFormText = "接诊记录表";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(649, 551);
            this.layoutControlGroup2.Text = "接诊记录表";
            // 
            // LayoutPanel1
            // 
            this.LayoutPanel1.Controls.Add(this.simpleButton4);
            this.LayoutPanel1.Controls.Add(this.btn选择客观资料);
            this.LayoutPanel1.Controls.Add(this.btn选择评估资料);
            this.LayoutPanel1.Controls.Add(this.btn选择主观资料);
            this.LayoutPanel1.Controls.Add(this.txt接诊医生);
            this.LayoutPanel1.Controls.Add(this.txt接诊时间);
            this.LayoutPanel1.Controls.Add(this.txt客观资料);
            this.LayoutPanel1.Controls.Add(this.txt主观资料);
            this.LayoutPanel1.Controls.Add(this.lab考核项);
            this.LayoutPanel1.Controls.Add(this.lbl个人档案号);
            this.LayoutPanel1.Controls.Add(this.lbl性别);
            this.LayoutPanel1.Controls.Add(this.lbl出生日期);
            this.LayoutPanel1.Controls.Add(this.lbl姓名);
            this.LayoutPanel1.Controls.Add(this.lbl身份证号);
            this.LayoutPanel1.Controls.Add(this.lbl联系电话);
            this.LayoutPanel1.Controls.Add(this.lbl创建时间);
            this.LayoutPanel1.Controls.Add(this.lbl当前所属机构);
            this.LayoutPanel1.Controls.Add(this.lbl创建人);
            this.LayoutPanel1.Controls.Add(this.lbl最近更新时间);
            this.LayoutPanel1.Controls.Add(this.lbl创建机构);
            this.LayoutPanel1.Controls.Add(this.lbl最近修改人);
            this.LayoutPanel1.Controls.Add(this.lbl居住地址);
            this.LayoutPanel1.Controls.Add(this.txt评估);
            this.LayoutPanel1.Controls.Add(this.txt处置计划);
            this.LayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutPanel1.Location = new System.Drawing.Point(0, 28);
            this.LayoutPanel1.Name = "LayoutPanel1";
            this.LayoutPanel1.Root = this.layoutControlGroup1;
            this.LayoutPanel1.Size = new System.Drawing.Size(651, 478);
            this.LayoutPanel1.TabIndex = 4;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(600, 369);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(29, 22);
            this.simpleButton4.StyleController = this.LayoutPanel1;
            this.simpleButton4.TabIndex = 58;
            this.simpleButton4.Text = "...";
            // 
            // btn选择客观资料
            // 
            this.btn选择客观资料.Location = new System.Drawing.Point(600, 304);
            this.btn选择客观资料.Name = "btn选择客观资料";
            this.btn选择客观资料.Size = new System.Drawing.Size(29, 22);
            this.btn选择客观资料.StyleController = this.LayoutPanel1;
            this.btn选择客观资料.TabIndex = 57;
            this.btn选择客观资料.Text = "...";
            // 
            // btn选择评估资料
            // 
            this.btn选择评估资料.Location = new System.Drawing.Point(600, 239);
            this.btn选择评估资料.Name = "btn选择评估资料";
            this.btn选择评估资料.Size = new System.Drawing.Size(29, 22);
            this.btn选择评估资料.StyleController = this.LayoutPanel1;
            this.btn选择评估资料.TabIndex = 56;
            this.btn选择评估资料.Text = "...";
            // 
            // btn选择主观资料
            // 
            this.btn选择主观资料.Location = new System.Drawing.Point(600, 174);
            this.btn选择主观资料.Name = "btn选择主观资料";
            this.btn选择主观资料.Size = new System.Drawing.Size(29, 22);
            this.btn选择主观资料.StyleController = this.LayoutPanel1;
            this.btn选择主观资料.TabIndex = 55;
            this.btn选择主观资料.Text = "...";
            // 
            // txt接诊医生
            // 
            this.txt接诊医生.Location = new System.Drawing.Point(434, 150);
            this.txt接诊医生.Margin = new System.Windows.Forms.Padding(0);
            this.txt接诊医生.Name = "txt接诊医生";
            this.txt接诊医生.Size = new System.Drawing.Size(195, 20);
            this.txt接诊医生.StyleController = this.LayoutPanel1;
            this.txt接诊医生.TabIndex = 29;
            // 
            // txt接诊时间
            // 
            this.txt接诊时间.EditValue = null;
            this.txt接诊时间.Location = new System.Drawing.Point(116, 150);
            this.txt接诊时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt接诊时间.Name = "txt接诊时间";
            this.txt接诊时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt接诊时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt接诊时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt接诊时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt接诊时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt接诊时间.Size = new System.Drawing.Size(203, 20);
            this.txt接诊时间.StyleController = this.LayoutPanel1;
            this.txt接诊时间.TabIndex = 28;
            // 
            // txt客观资料
            // 
            this.txt客观资料.Location = new System.Drawing.Point(116, 304);
            this.txt客观资料.Margin = new System.Windows.Forms.Padding(0);
            this.txt客观资料.Name = "txt客观资料";
            this.txt客观资料.Size = new System.Drawing.Size(480, 61);
            this.txt客观资料.StyleController = this.LayoutPanel1;
            this.txt客观资料.TabIndex = 53;
            this.txt客观资料.UseOptimizedRendering = true;
            // 
            // txt主观资料
            // 
            this.txt主观资料.Location = new System.Drawing.Point(116, 174);
            this.txt主观资料.Margin = new System.Windows.Forms.Padding(0);
            this.txt主观资料.Name = "txt主观资料";
            this.txt主观资料.Size = new System.Drawing.Size(480, 61);
            this.txt主观资料.StyleController = this.LayoutPanel1;
            this.txt主观资料.TabIndex = 54;
            this.txt主观资料.UseOptimizedRendering = true;
            // 
            // lab考核项
            // 
            this.lab考核项.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lab考核项.Location = new System.Drawing.Point(5, 30);
            this.lab考核项.Margin = new System.Windows.Forms.Padding(0);
            this.lab考核项.Name = "lab考核项";
            this.lab考核项.Size = new System.Drawing.Size(624, 20);
            this.lab考核项.StyleController = this.LayoutPanel1;
            this.lab考核项.TabIndex = 1;
            this.lab考核项.Text = "考核项：6    缺项：6     完整度：0 % ";
            // 
            // lbl个人档案号
            // 
            this.lbl个人档案号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl个人档案号.Location = new System.Drawing.Point(116, 54);
            this.lbl个人档案号.Name = "lbl个人档案号";
            this.lbl个人档案号.Size = new System.Drawing.Size(203, 20);
            this.lbl个人档案号.StyleController = this.LayoutPanel1;
            this.lbl个人档案号.TabIndex = 40;
            this.lbl个人档案号.Text = "档案号";
            // 
            // lbl性别
            // 
            this.lbl性别.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl性别.Location = new System.Drawing.Point(116, 78);
            this.lbl性别.Name = "lbl性别";
            this.lbl性别.Size = new System.Drawing.Size(203, 20);
            this.lbl性别.StyleController = this.LayoutPanel1;
            this.lbl性别.TabIndex = 41;
            this.lbl性别.Text = "性别";
            // 
            // lbl出生日期
            // 
            this.lbl出生日期.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl出生日期.Location = new System.Drawing.Point(116, 102);
            this.lbl出生日期.Name = "lbl出生日期";
            this.lbl出生日期.Size = new System.Drawing.Size(203, 20);
            this.lbl出生日期.StyleController = this.LayoutPanel1;
            this.lbl出生日期.TabIndex = 42;
            this.lbl出生日期.Text = "出生日期";
            // 
            // lbl姓名
            // 
            this.lbl姓名.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl姓名.Location = new System.Drawing.Point(434, 54);
            this.lbl姓名.Name = "lbl姓名";
            this.lbl姓名.Size = new System.Drawing.Size(195, 20);
            this.lbl姓名.StyleController = this.LayoutPanel1;
            this.lbl姓名.TabIndex = 43;
            this.lbl姓名.Text = "姓名";
            // 
            // lbl身份证号
            // 
            this.lbl身份证号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl身份证号.Location = new System.Drawing.Point(434, 78);
            this.lbl身份证号.Name = "lbl身份证号";
            this.lbl身份证号.Size = new System.Drawing.Size(195, 20);
            this.lbl身份证号.StyleController = this.LayoutPanel1;
            this.lbl身份证号.TabIndex = 44;
            this.lbl身份证号.Text = "身份证";
            // 
            // lbl联系电话
            // 
            this.lbl联系电话.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl联系电话.Location = new System.Drawing.Point(434, 102);
            this.lbl联系电话.Name = "lbl联系电话";
            this.lbl联系电话.Size = new System.Drawing.Size(195, 20);
            this.lbl联系电话.StyleController = this.LayoutPanel1;
            this.lbl联系电话.TabIndex = 45;
            this.lbl联系电话.Text = "联系电话";
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Location = new System.Drawing.Point(116, 434);
            this.lbl创建时间.Margin = new System.Windows.Forms.Padding(0);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(203, 20);
            this.lbl创建时间.StyleController = this.LayoutPanel1;
            this.lbl创建时间.TabIndex = 46;
            this.lbl创建时间.Text = "创建时间";
            // 
            // lbl当前所属机构
            // 
            this.lbl当前所属机构.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl当前所属机构.Location = new System.Drawing.Point(116, 458);
            this.lbl当前所属机构.Margin = new System.Windows.Forms.Padding(0);
            this.lbl当前所属机构.Name = "lbl当前所属机构";
            this.lbl当前所属机构.Size = new System.Drawing.Size(203, 20);
            this.lbl当前所属机构.StyleController = this.LayoutPanel1;
            this.lbl当前所属机构.TabIndex = 47;
            this.lbl当前所属机构.Text = "所属机构";
            // 
            // lbl创建人
            // 
            this.lbl创建人.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建人.Location = new System.Drawing.Point(116, 482);
            this.lbl创建人.Margin = new System.Windows.Forms.Padding(0);
            this.lbl创建人.Name = "lbl创建人";
            this.lbl创建人.Size = new System.Drawing.Size(203, 20);
            this.lbl创建人.StyleController = this.LayoutPanel1;
            this.lbl创建人.TabIndex = 48;
            this.lbl创建人.Text = "创建人";
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近更新时间.Location = new System.Drawing.Point(434, 434);
            this.lbl最近更新时间.Margin = new System.Windows.Forms.Padding(0);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(195, 20);
            this.lbl最近更新时间.StyleController = this.LayoutPanel1;
            this.lbl最近更新时间.TabIndex = 49;
            this.lbl最近更新时间.Text = "更新时间";
            // 
            // lbl创建机构
            // 
            this.lbl创建机构.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建机构.Location = new System.Drawing.Point(434, 458);
            this.lbl创建机构.Margin = new System.Windows.Forms.Padding(0);
            this.lbl创建机构.Name = "lbl创建机构";
            this.lbl创建机构.Size = new System.Drawing.Size(195, 20);
            this.lbl创建机构.StyleController = this.LayoutPanel1;
            this.lbl创建机构.TabIndex = 50;
            this.lbl创建机构.Text = "创建机构";
            // 
            // lbl最近修改人
            // 
            this.lbl最近修改人.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近修改人.Location = new System.Drawing.Point(434, 482);
            this.lbl最近修改人.Margin = new System.Windows.Forms.Padding(0);
            this.lbl最近修改人.Name = "lbl最近修改人";
            this.lbl最近修改人.Size = new System.Drawing.Size(195, 20);
            this.lbl最近修改人.StyleController = this.LayoutPanel1;
            this.lbl最近修改人.TabIndex = 51;
            this.lbl最近修改人.Text = "修改人";
            // 
            // lbl居住地址
            // 
            this.lbl居住地址.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl居住地址.Location = new System.Drawing.Point(116, 126);
            this.lbl居住地址.Name = "lbl居住地址";
            this.lbl居住地址.Size = new System.Drawing.Size(513, 20);
            this.lbl居住地址.StyleController = this.LayoutPanel1;
            this.lbl居住地址.TabIndex = 52;
            this.lbl居住地址.Text = "居住地址";
            // 
            // txt评估
            // 
            this.txt评估.Location = new System.Drawing.Point(116, 239);
            this.txt评估.Name = "txt评估";
            this.txt评估.Size = new System.Drawing.Size(480, 61);
            this.txt评估.StyleController = this.LayoutPanel1;
            this.txt评估.TabIndex = 32;
            this.txt评估.UseOptimizedRendering = true;
            // 
            // txt处置计划
            // 
            this.txt处置计划.Location = new System.Drawing.Point(116, 369);
            this.txt处置计划.Name = "txt处置计划";
            this.txt处置计划.Size = new System.Drawing.Size(480, 61);
            this.txt处置计划.StyleController = this.LayoutPanel1;
            this.txt处置计划.TabIndex = 33;
            this.txt处置计划.UseOptimizedRendering = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "接诊记录表";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem21,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem29,
            this.layoutControlItem32,
            this.layoutControlItem7,
            this.layoutControlItem22,
            this.layoutControlItem33,
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem27,
            this.layoutControlItem30,
            this.layoutControlItem28,
            this.layoutControlItem31,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup2";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(634, 507);
            this.layoutControlGroup1.Text = "接诊记录表";
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.lbl个人档案号;
            this.layoutControlItem21.CustomizationFormText = "lbl个人档案号item";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(16, 24);
            this.layoutControlItem21.Name = "lbl个人档案号item";
            this.layoutControlItem21.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "个人档案号:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.lbl出生日期;
            this.layoutControlItem23.CustomizationFormText = "lbl出生日期item";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(115, 24);
            this.layoutControlItem23.Name = "lbl出生日期item";
            this.layoutControlItem23.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "出生日期:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.lbl姓名;
            this.layoutControlItem24.CustomizationFormText = "lbl姓名item";
            this.layoutControlItem24.Location = new System.Drawing.Point(318, 24);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem24.Name = "lbl姓名item";
            this.layoutControlItem24.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "姓名:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.lbl身份证号;
            this.layoutControlItem25.CustomizationFormText = "lbl身份证号item";
            this.layoutControlItem25.Location = new System.Drawing.Point(318, 48);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(115, 24);
            this.layoutControlItem25.Name = "lbl身份证号item";
            this.layoutControlItem25.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "身份证号:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.lbl联系电话;
            this.layoutControlItem26.CustomizationFormText = "lbl联系电话item";
            this.layoutControlItem26.Location = new System.Drawing.Point(318, 72);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(115, 24);
            this.layoutControlItem26.Name = "lbl联系电话item";
            this.layoutControlItem26.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "联系电话:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.lbl创建人;
            this.layoutControlItem29.CustomizationFormText = "lbl创建人item";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 452);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem29.Name = "lbl创建人item";
            this.layoutControlItem29.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "创建人:";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.lbl最近修改人;
            this.layoutControlItem32.CustomizationFormText = "lbl最近修改人item";
            this.layoutControlItem32.Location = new System.Drawing.Point(318, 452);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem32.Name = "lbl最近修改人item";
            this.layoutControlItem32.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "最近修改人:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lab考核项;
            this.layoutControlItem7.CustomizationFormText = "labelControl2item";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(116, 24);
            this.layoutControlItem7.Name = "labelControl2item";
            this.layoutControlItem7.Size = new System.Drawing.Size(628, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "考核项";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.lbl性别;
            this.layoutControlItem22.CustomizationFormText = "lbl性别item";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(115, 24);
            this.layoutControlItem22.Name = "lbl性别item";
            this.layoutControlItem22.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "性别:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.lbl居住地址;
            this.layoutControlItem33.CustomizationFormText = "lbl居住地址item";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(115, 24);
            this.layoutControlItem33.Name = "lbl居住地址item";
            this.layoutControlItem33.Size = new System.Drawing.Size(628, 24);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "居住地址:";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt接诊时间;
            this.layoutControlItem4.CustomizationFormText = "接诊时间";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem4.Name = "dtp接诊时间item";
            this.layoutControlItem4.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem4.Tag = "check";
            this.layoutControlItem4.Text = "接诊时间:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt接诊医生;
            this.layoutControlItem1.CustomizationFormText = "接诊医生";
            this.layoutControlItem1.Location = new System.Drawing.Point(318, 120);
            this.layoutControlItem1.Name = "txt接诊医生item";
            this.layoutControlItem1.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem1.Tag = "check";
            this.layoutControlItem1.Text = "接诊医生:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt主观资料;
            this.layoutControlItem6.CustomizationFormText = "就诊者的主观资料";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 75);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(117, 65);
            this.layoutControlItem6.Name = "memoEdit1item";
            this.layoutControlItem6.Size = new System.Drawing.Size(595, 65);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Tag = "check";
            this.layoutControlItem6.Text = "就诊者的主观资料:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt客观资料;
            this.layoutControlItem5.CustomizationFormText = "就诊者的客观资料";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 274);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(117, 65);
            this.layoutControlItem5.Name = "txt就诊者的客观资料item";
            this.layoutControlItem5.Size = new System.Drawing.Size(595, 65);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Tag = "check";
            this.layoutControlItem5.Text = "就诊者的客观资料:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt处置计划;
            this.layoutControlItem3.CustomizationFormText = "处置计划";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 339);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(113, 65);
            this.layoutControlItem3.Name = "txt处置计划item";
            this.layoutControlItem3.Size = new System.Drawing.Size(595, 65);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Tag = "check";
            this.layoutControlItem3.Text = "处置计划:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.lbl创建时间;
            this.layoutControlItem27.CustomizationFormText = "lbl创建时间item";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 404);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem27.Name = "lbl创建时间item";
            this.layoutControlItem27.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "创建时间:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.lbl最近更新时间;
            this.layoutControlItem30.CustomizationFormText = "lbl最近更新时间item";
            this.layoutControlItem30.Location = new System.Drawing.Point(318, 404);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem30.Name = "lbl最近更新时间item";
            this.layoutControlItem30.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "最近更新时间:";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.lbl当前所属机构;
            this.layoutControlItem28.CustomizationFormText = "lbl当前所属机构item";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 428);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem28.Name = "lbl当前所属机构item";
            this.layoutControlItem28.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "当前所属机构:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.lbl创建机构;
            this.layoutControlItem31.CustomizationFormText = "lbl创建机构item";
            this.layoutControlItem31.Location = new System.Drawing.Point(318, 428);
            this.layoutControlItem31.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem31.Name = "lbl创建机构item";
            this.layoutControlItem31.Size = new System.Drawing.Size(310, 24);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "创建机构:";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt评估;
            this.layoutControlItem2.CustomizationFormText = "评估";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 209);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(50, 65);
            this.layoutControlItem2.Name = "txt评估item";
            this.layoutControlItem2.Size = new System.Drawing.Size(595, 65);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Tag = "check";
            this.layoutControlItem2.Text = "评估:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btn选择主观资料;
            this.layoutControlItem8.CustomizationFormText = "...";
            this.layoutControlItem8.Location = new System.Drawing.Point(595, 144);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(33, 65);
            this.layoutControlItem8.Text = "...";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            this.layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btn选择评估资料;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(595, 209);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(33, 65);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            this.layoutControlItem9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btn选择客观资料;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(595, 274);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(33, 65);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            this.layoutControlItem10.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButton4;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(595, 339);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(33, 65);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            this.layoutControlItem11.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(84, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 4;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // UC接诊记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UC接诊记录表";
            this.Size = new System.Drawing.Size(651, 506);
            this.Load += new System.EventHandler(this.UC接诊记录表_Load);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.LayoutPanel1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPanel1)).EndInit();
            this.LayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt客观资料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主观资料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt评估.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt处置计划.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControl LayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton btn选择客观资料;
        private DevExpress.XtraEditors.SimpleButton btn选择评估资料;
        private DevExpress.XtraEditors.SimpleButton btn选择主观资料;
        private DevExpress.XtraEditors.TextEdit txt接诊医生;
        private DevExpress.XtraEditors.DateEdit txt接诊时间;
        private DevExpress.XtraEditors.MemoEdit txt客观资料;
        private DevExpress.XtraEditors.MemoEdit txt主观资料;
        private DevExpress.XtraEditors.LabelControl lab考核项;
        private DevExpress.XtraEditors.LabelControl lbl个人档案号;
        private DevExpress.XtraEditors.LabelControl lbl性别;
        private DevExpress.XtraEditors.LabelControl lbl出生日期;
        private DevExpress.XtraEditors.LabelControl lbl姓名;
        private DevExpress.XtraEditors.LabelControl lbl身份证号;
        private DevExpress.XtraEditors.LabelControl lbl联系电话;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraEditors.LabelControl lbl当前所属机构;
        private DevExpress.XtraEditors.LabelControl lbl创建人;
        private DevExpress.XtraEditors.LabelControl lbl最近更新时间;
        private DevExpress.XtraEditors.LabelControl lbl创建机构;
        private DevExpress.XtraEditors.LabelControl lbl最近修改人;
        private DevExpress.XtraEditors.LabelControl lbl居住地址;
        private DevExpress.XtraEditors.MemoEdit txt评估;
        private DevExpress.XtraEditors.MemoEdit txt处置计划;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.SimpleButton btn填表说明;
        private DevExpress.XtraEditors.SimpleButton btn导出;

    }
}
