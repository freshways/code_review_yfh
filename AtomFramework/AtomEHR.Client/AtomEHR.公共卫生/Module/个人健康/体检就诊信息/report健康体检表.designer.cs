﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class report健康体检表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体检日期年 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体检日期月 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体检日期日 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel责任医生 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel症状其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel146 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel145 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel144 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel143 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel142 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel141 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel140 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel139 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel138 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel症状1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体温 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel脉率 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel呼吸频率 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血压左侧1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血压左侧2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血压右侧2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel123 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel血压右侧1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel身高 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体重 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel腰围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体重指数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel老年人健康状态自我评估 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel老年人生活自理能力评估 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel老年人认知功能分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel老年人认知功能 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel老年人情感状态 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel锻炼频率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel每次锻炼时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel锻炼方式 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel饮食习惯1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel饮食习惯2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel饮食习惯3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel吸烟状况 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel日吸烟量 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel开始吸烟年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel戒烟年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel饮酒频率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel日饮酒量 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel戒酒年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel是否戒酒 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel开始饮酒年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel近一年是否醉酒 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel饮酒种类其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel饮酒种类1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel饮酒种类2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel饮酒种类3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel饮酒种类4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel工种 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel从业时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel职业病有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel粉尘 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel粉尘有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel粉尘防护有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel放射物质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel放射物质有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel放射物质防护措施有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel物理因素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel物理因素有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel物理因素防护措施有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel化学因素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel化学因素有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel化学因素防护措施有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel毒物其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel毒物其他有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel毒物其他防护措施有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable住院史日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel住院史原因1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel住院史医疗机构1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel住院史病案号1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable住院史日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel住院史原因2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel住院史医疗机构2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel住院史病案号2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable家庭病床史日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable家庭病床史原因1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable家庭病床史医疗机构1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable家庭病床史病案号1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable家庭病床史日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable家庭病床史原因2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable家庭病床史医疗机构2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable家庭病床史病案号2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药名称1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用法1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用量1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药时间1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药依从性1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药名称2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用法2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用量2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药时间2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药依从性2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药名称3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用法3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用量3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药时间3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药依从性3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药名称4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用法4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用量4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药时间4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药依从性4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药名称5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用法5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用量5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药时间5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药依从性5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药名称6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用法6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药用量6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药时间6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable用药依从性6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable接种名称1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel接种日期1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel接种机构1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable接种名称2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel接种日期2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel接种机构2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable接种名称3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel接种日期3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel接种机构3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体检有无异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体检异常1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体检异常2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体检异常3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel体检异常4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel健康指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel健康指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel健康指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel健康指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel危险控制因素1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel危险控制因素cell = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel危险控制因素2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel危险控制因素3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel危险控制因素4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel危险控制因素5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel危险控制因素6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel减体重目标 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel建议接种疫苗 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel危险控制因素其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrLabel坚持锻炼时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel老年人情感状态分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel姓名,
            this.xrLabel2,
            this.xrLabel1,
            this.xrTable2,
            this.xrTable1,
            this.xrLabel12,
            this.xrLabel14,
            this.xrLabel13,
            this.xrLine1,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7});
            this.Detail.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.Detail.HeightF = 1147.052F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(1220.833F, 29.16335F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(40.62671F, 19.08621F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "编号";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel姓名
            // 
            this.xrLabel姓名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel姓名.LocationFloat = new DevExpress.Utils.PointFloat(900.0049F, 29.16335F);
            this.xrLabel姓名.Name = "xrLabel姓名";
            this.xrLabel姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel姓名.SizeF = new System.Drawing.SizeF(152.0833F, 19.08621F);
            this.xrLabel姓名.StylePriority.UseBorders = false;
            this.xrLabel姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(840.625F, 29.16335F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(59.38F, 19.08621F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓名：";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel1.Font = new System.Drawing.Font("仿宋", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(1105.361F, 6.163343F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(122.5556F, 23F);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "健康体检表";
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(812F, 58.33333F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18});
            this.xrTable2.SizeF = new System.Drawing.SizeF(750.0833F, 994.571F);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrLabel体检日期年,
            this.xrTableCell289,
            this.xrLabel体检日期月,
            this.xrTableCell299,
            this.xrLabel体检日期日,
            this.xrTableCell296,
            this.xrTableCell297,
            this.xrLabel责任医生});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.Weight = 1.08D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "体检日期";
            this.xrTableCell4.Weight = 0.40839912665965628D;
            // 
            // xrLabel体检日期年
            // 
            this.xrLabel体检日期年.Name = "xrLabel体检日期年";
            this.xrLabel体检日期年.Weight = 0.20868848709880478D;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell289.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.StylePriority.UseBorders = false;
            this.xrTableCell289.StylePriority.UseFont = false;
            this.xrTableCell289.Text = "年";
            this.xrTableCell289.Weight = 0.16465871833833057D;
            // 
            // xrLabel体检日期月
            // 
            this.xrLabel体检日期月.Name = "xrLabel体检日期月";
            this.xrLabel体检日期月.Weight = 0.19022497338613237D;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell299.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.StylePriority.UseBorders = false;
            this.xrTableCell299.StylePriority.UseFont = false;
            this.xrTableCell299.Text = "月";
            this.xrTableCell299.Weight = 0.1592362300438232D;
            // 
            // xrLabel体检日期日
            // 
            this.xrLabel体检日期日.Name = "xrLabel体检日期日";
            this.xrLabel体检日期日.Weight = 0.15211983469582874D;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell296.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.StylePriority.UseBorders = false;
            this.xrTableCell296.StylePriority.UseFont = false;
            this.xrTableCell296.StylePriority.UseTextAlignment = false;
            this.xrTableCell296.Text = "日";
            this.xrTableCell296.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell296.Weight = 0.33649323321620217D;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell297.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.StylePriority.UseBorders = false;
            this.xrTableCell297.StylePriority.UseFont = false;
            this.xrTableCell297.Text = "责任医生";
            this.xrTableCell297.Weight = 0.535772955049105D;
            // 
            // xrLabel责任医生
            // 
            this.xrLabel责任医生.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel责任医生.Name = "xrLabel责任医生";
            this.xrLabel责任医生.StylePriority.UseBorders = false;
            this.xrLabel责任医生.Weight = 0.84440644151211708D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell9});
            this.xrTableRow3.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.StylePriority.UseFont = false;
            this.xrTableRow3.Weight = 1.08D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseFont = false;
            this.xrTableCell7.Text = "内容";
            this.xrTableCell7.Weight = 0.17997998798473508D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.StylePriority.UseFont = false;
            this.xrTableCell9.Text = "检  查  项  目";
            this.xrTableCell9.Weight = 2.8200200120152652D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell10});
            this.xrTableRow4.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.StylePriority.UseBorders = false;
            this.xrTableRow4.StylePriority.UseFont = false;
            this.xrTableRow4.Weight = 3.66001847069594D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell8.Multiline = true;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseFont = false;
            this.xrTableCell8.Text = "症\r\n\r\n状";
            this.xrTableCell8.Weight = 0.17997997272764338D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel15,
            this.xrLabel16,
            this.xrLabel17,
            this.xrLabel症状其他,
            this.xrLabel症状1,
            this.xrLabel145,
            this.xrLabel144,
            this.xrLabel143,
            this.xrLabel142,
            this.xrLabel141,
            this.xrLabel140,
            this.xrLabel139,
            this.xrLabel138,
            this.xrLabel症状2,
            this.xrLabel症状3,
            this.xrLabel症状5,
            this.xrLabel症状6,
            this.xrLabel症状7,
            this.xrLabel症状8,
            this.xrLabel症状9,
            this.xrLabel症状10,
            this.xrLabel症状4,
            this.xrLabel146});
            this.xrTableCell10.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseFont = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "1无症状 2头痛 3头晕 4心悸 5胸闷 6胸痛 7慢性咳嗽 8咳痰 9呼吸困难 10多饮\r\n11多尿 12体重下降 13乏力 14关节肿痛 15视力模糊 16手" +
    "脚麻木 17尿急 18尿痛";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 2.8200200272723563D;
            // 
            // xrLabel症状其他
            // 
            this.xrLabel症状其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel症状其他.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel症状其他.LocationFloat = new DevExpress.Utils.PointFloat(496.6397F, 46.52904F);
            this.xrLabel症状其他.Name = "xrLabel症状其他";
            this.xrLabel症状其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状其他.SizeF = new System.Drawing.SizeF(112.5914F, 23F);
            this.xrLabel症状其他.StylePriority.UseBorders = false;
            this.xrLabel症状其他.StylePriority.UseFont = false;
            this.xrLabel症状其他.StylePriority.UseTextAlignment = false;
            this.xrLabel症状其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel146
            // 
            this.xrLabel146.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel146.CanGrow = false;
            this.xrLabel146.LocationFloat = new DevExpress.Utils.PointFloat(322.145F, 69.52904F);
            this.xrLabel146.Name = "xrLabel146";
            this.xrLabel146.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel146.SizeF = new System.Drawing.SizeF(8F, 21F);
            this.xrLabel146.StylePriority.UseBorders = false;
            this.xrLabel146.Text = "/";
            // 
            // xrLabel145
            // 
            this.xrLabel145.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel145.CanGrow = false;
            this.xrLabel145.LocationFloat = new DevExpress.Utils.PointFloat(351.9998F, 69.52904F);
            this.xrLabel145.Name = "xrLabel145";
            this.xrLabel145.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel145.SizeF = new System.Drawing.SizeF(8F, 21F);
            this.xrLabel145.StylePriority.UseBorders = false;
            this.xrLabel145.Text = "/";
            // 
            // xrLabel144
            // 
            this.xrLabel144.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel144.CanGrow = false;
            this.xrLabel144.LocationFloat = new DevExpress.Utils.PointFloat(381.7591F, 69.52904F);
            this.xrLabel144.Name = "xrLabel144";
            this.xrLabel144.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel144.SizeF = new System.Drawing.SizeF(8F, 21F);
            this.xrLabel144.StylePriority.UseBorders = false;
            this.xrLabel144.Text = "/";
            // 
            // xrLabel143
            // 
            this.xrLabel143.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel143.CanGrow = false;
            this.xrLabel143.LocationFloat = new DevExpress.Utils.PointFloat(411.7258F, 69.52904F);
            this.xrLabel143.Name = "xrLabel143";
            this.xrLabel143.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel143.SizeF = new System.Drawing.SizeF(8F, 21F);
            this.xrLabel143.StylePriority.UseBorders = false;
            this.xrLabel143.Text = "/";
            // 
            // xrLabel142
            // 
            this.xrLabel142.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel142.CanGrow = false;
            this.xrLabel142.LocationFloat = new DevExpress.Utils.PointFloat(442.1819F, 69.52904F);
            this.xrLabel142.Name = "xrLabel142";
            this.xrLabel142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel142.SizeF = new System.Drawing.SizeF(8F, 21F);
            this.xrLabel142.StylePriority.UseBorders = false;
            this.xrLabel142.Text = "/";
            // 
            // xrLabel141
            // 
            this.xrLabel141.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel141.CanGrow = false;
            this.xrLabel141.LocationFloat = new DevExpress.Utils.PointFloat(472.3191F, 69.52904F);
            this.xrLabel141.Name = "xrLabel141";
            this.xrLabel141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel141.SizeF = new System.Drawing.SizeF(8F, 21F);
            this.xrLabel141.StylePriority.UseBorders = false;
            this.xrLabel141.Text = "/";
            // 
            // xrLabel140
            // 
            this.xrLabel140.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel140.CanGrow = false;
            this.xrLabel140.LocationFloat = new DevExpress.Utils.PointFloat(502.4564F, 69.52904F);
            this.xrLabel140.Name = "xrLabel140";
            this.xrLabel140.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel140.SizeF = new System.Drawing.SizeF(8F, 21F);
            this.xrLabel140.StylePriority.UseBorders = false;
            this.xrLabel140.Text = "/";
            // 
            // xrLabel139
            // 
            this.xrLabel139.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel139.CanGrow = false;
            this.xrLabel139.LocationFloat = new DevExpress.Utils.PointFloat(532.5938F, 69.52904F);
            this.xrLabel139.Name = "xrLabel139";
            this.xrLabel139.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel139.SizeF = new System.Drawing.SizeF(8F, 21F);
            this.xrLabel139.StylePriority.UseBorders = false;
            this.xrLabel139.Text = "/";
            // 
            // xrLabel138
            // 
            this.xrLabel138.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel138.CanGrow = false;
            this.xrLabel138.LocationFloat = new DevExpress.Utils.PointFloat(562.1514F, 69.52904F);
            this.xrLabel138.Name = "xrLabel138";
            this.xrLabel138.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel138.SizeF = new System.Drawing.SizeF(8F, 21F);
            this.xrLabel138.StylePriority.UseBorders = false;
            this.xrLabel138.Text = "/";
            // 
            // xrLabel症状2
            // 
            this.xrLabel症状2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel症状2.CanGrow = false;
            this.xrLabel症状2.LocationFloat = new DevExpress.Utils.PointFloat(330.145F, 73.34596F);
            this.xrLabel症状2.Name = "xrLabel症状2";
            this.xrLabel症状2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel症状2.StylePriority.UseBorders = false;
            // 
            // xrLabel症状3
            // 
            this.xrLabel症状3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel症状3.CanGrow = false;
            this.xrLabel症状3.LocationFloat = new DevExpress.Utils.PointFloat(360.2318F, 73.34596F);
            this.xrLabel症状3.Name = "xrLabel症状3";
            this.xrLabel症状3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel症状3.StylePriority.UseBorders = false;
            // 
            // xrLabel症状5
            // 
            this.xrLabel症状5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel症状5.CanGrow = false;
            this.xrLabel症状5.LocationFloat = new DevExpress.Utils.PointFloat(420.4058F, 73.34596F);
            this.xrLabel症状5.Name = "xrLabel症状5";
            this.xrLabel症状5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel症状5.StylePriority.UseBorders = false;
            // 
            // xrLabel症状6
            // 
            this.xrLabel症状6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel症状6.CanGrow = false;
            this.xrLabel症状6.LocationFloat = new DevExpress.Utils.PointFloat(450.4927F, 73.34596F);
            this.xrLabel症状6.Name = "xrLabel症状6";
            this.xrLabel症状6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel症状6.StylePriority.UseBorders = false;
            // 
            // xrLabel症状7
            // 
            this.xrLabel症状7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel症状7.CanGrow = false;
            this.xrLabel症状7.LocationFloat = new DevExpress.Utils.PointFloat(480.5796F, 73.34596F);
            this.xrLabel症状7.Name = "xrLabel症状7";
            this.xrLabel症状7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel症状7.StylePriority.UseBorders = false;
            // 
            // xrLabel症状8
            // 
            this.xrLabel症状8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel症状8.CanGrow = false;
            this.xrLabel症状8.LocationFloat = new DevExpress.Utils.PointFloat(510.6662F, 73.34596F);
            this.xrLabel症状8.Name = "xrLabel症状8";
            this.xrLabel症状8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel症状8.StylePriority.UseBorders = false;
            // 
            // xrLabel症状9
            // 
            this.xrLabel症状9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel症状9.CanGrow = false;
            this.xrLabel症状9.LocationFloat = new DevExpress.Utils.PointFloat(540.7532F, 73.34596F);
            this.xrLabel症状9.Name = "xrLabel症状9";
            this.xrLabel症状9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状9.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel症状9.StylePriority.UseBorders = false;
            // 
            // xrLabel症状10
            // 
            this.xrLabel症状10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel症状10.CanGrow = false;
            this.xrLabel症状10.LocationFloat = new DevExpress.Utils.PointFloat(570.1514F, 73.34596F);
            this.xrLabel症状10.Name = "xrLabel症状10";
            this.xrLabel症状10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状10.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel症状10.StylePriority.UseBorders = false;
            // 
            // xrLabel症状4
            // 
            this.xrLabel症状4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel症状4.CanGrow = false;
            this.xrLabel症状4.LocationFloat = new DevExpress.Utils.PointFloat(390.3189F, 73.34596F);
            this.xrLabel症状4.Name = "xrLabel症状4";
            this.xrLabel症状4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel症状4.StylePriority.UseBorders = false;
            // 
            // xrLabel症状1
            // 
            this.xrLabel症状1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel症状1.CanGrow = false;
            this.xrLabel症状1.LocationFloat = new DevExpress.Utils.PointFloat(300.0579F, 73.34596F);
            this.xrLabel症状1.Name = "xrLabel症状1";
            this.xrLabel症状1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel症状1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel症状1.StylePriority.UseBorders = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell32,
            this.xrLabel体温,
            this.xrTableCell15,
            this.xrTableCell33,
            this.xrLabel脉率,
            this.xrTableCell16});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 1.0741358870416218D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.Weight = 0.17997997272764338D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Text = "体  温";
            this.xrTableCell32.Weight = 0.60176594313516185D;
            // 
            // xrLabel体温
            // 
            this.xrLabel体温.Name = "xrLabel体温";
            this.xrLabel体温.StylePriority.UseTextAlignment = false;
            this.xrLabel体温.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrLabel体温.Weight = 0.53124274766190982D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100F);
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.StylePriority.UsePadding = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "℃";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.24087267138631913D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Text = "脉率";
            this.xrTableCell33.Weight = 0.304834790509131D;
            // 
            // xrLabel脉率
            // 
            this.xrLabel脉率.Name = "xrLabel脉率";
            this.xrLabel脉率.StylePriority.UseTextAlignment = false;
            this.xrLabel脉率.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrLabel脉率.Weight = 0.64130407737201267D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Text = "次/分钟";
            this.xrTableCell16.Weight = 0.49999979720782189D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCell184,
            this.xrLabel呼吸频率,
            this.xrTableCell185,
            this.xrTableCell18,
            this.xrTableCell117});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.StylePriority.UseBorders = false;
            this.xrTableRow7.Weight = 1.9095748753651574D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Weight = 0.17997997272764338D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Text = "呼吸频率";
            this.xrTableCell184.Weight = 0.6017659430809732D;
            // 
            // xrLabel呼吸频率
            // 
            this.xrLabel呼吸频率.Name = "xrLabel呼吸频率";
            this.xrLabel呼吸频率.StylePriority.UseTextAlignment = false;
            this.xrLabel呼吸频率.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrLabel呼吸频率.Weight = 0.408899600377997D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100F);
            this.xrTableCell185.StylePriority.UseBorders = false;
            this.xrTableCell185.StylePriority.UsePadding = false;
            this.xrTableCell185.StylePriority.UseTextAlignment = false;
            this.xrTableCell185.Text = "次/分钟";
            this.xrTableCell185.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell185.Weight = 0.36321600117384911D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Text = "血压";
            this.xrTableCell18.Weight = 0.30483478987341905D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell117.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel6,
            this.xrLabel4,
            this.xrLabel血压左侧1,
            this.xrLabel120,
            this.xrLabel血压左侧2,
            this.xrLabel122,
            this.xrLabel血压右侧2,
            this.xrLabel123,
            this.xrLabel血压右侧1,
            this.xrLabel5});
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Weight = 1.141303692766118D;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(2.986877F, 23.00003F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(71.86267F, 22.99997F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.Text = "右 侧";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(2.986877F, 3.051758E-05F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(71.86267F, 22.99997F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.Text = "左 侧";
            // 
            // xrLabel血压左侧1
            // 
            this.xrLabel血压左侧1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel血压左侧1.LocationFloat = new DevExpress.Utils.PointFloat(75.26093F, 6.357829E-05F);
            this.xrLabel血压左侧1.Name = "xrLabel血压左侧1";
            this.xrLabel血压左侧1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血压左侧1.SizeF = new System.Drawing.SizeF(48.00623F, 22.99998F);
            this.xrLabel血压左侧1.StylePriority.UseBorders = false;
            // 
            // xrLabel120
            // 
            this.xrLabel120.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(123.6196F, 6.357829E-05F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(17.70837F, 22.99997F);
            this.xrLabel120.StylePriority.UseBorders = false;
            this.xrLabel120.Text = "/";
            // 
            // xrLabel血压左侧2
            // 
            this.xrLabel血压左侧2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel血压左侧2.LocationFloat = new DevExpress.Utils.PointFloat(154.6606F, 6.357829E-05F);
            this.xrLabel血压左侧2.Name = "xrLabel血压左侧2";
            this.xrLabel血压左侧2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血压左侧2.SizeF = new System.Drawing.SizeF(34.76282F, 22.99998F);
            this.xrLabel血压左侧2.StylePriority.UseBorders = false;
            // 
            // xrLabel122
            // 
            this.xrLabel122.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(189.5276F, 6.103516E-05F);
            this.xrLabel122.Name = "xrLabel122";
            this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel122.SizeF = new System.Drawing.SizeF(56.91455F, 22.99997F);
            this.xrLabel122.StylePriority.UseBorders = false;
            this.xrLabel122.Text = "mmHg";
            // 
            // xrLabel血压右侧2
            // 
            this.xrLabel血压右侧2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel血压右侧2.LocationFloat = new DevExpress.Utils.PointFloat(154.7425F, 23.00005F);
            this.xrLabel血压右侧2.Name = "xrLabel血压右侧2";
            this.xrLabel血压右侧2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血压右侧2.SizeF = new System.Drawing.SizeF(34.76282F, 22.99998F);
            this.xrLabel血压右侧2.StylePriority.UseBorders = false;
            // 
            // xrLabel123
            // 
            this.xrLabel123.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel123.LocationFloat = new DevExpress.Utils.PointFloat(123.6196F, 23.00003F);
            this.xrLabel123.Name = "xrLabel123";
            this.xrLabel123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel123.SizeF = new System.Drawing.SizeF(17.70837F, 22.99997F);
            this.xrLabel123.StylePriority.UseBorders = false;
            this.xrLabel123.Text = "/";
            // 
            // xrLabel血压右侧1
            // 
            this.xrLabel血压右侧1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel血压右侧1.LocationFloat = new DevExpress.Utils.PointFloat(75.61322F, 23.00005F);
            this.xrLabel血压右侧1.Name = "xrLabel血压右侧1";
            this.xrLabel血压右侧1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血压右侧1.SizeF = new System.Drawing.SizeF(48.00623F, 22.99998F);
            this.xrLabel血压右侧1.StylePriority.UseBorders = false;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(189.5276F, 23.00003F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(56.91455F, 22.99997F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.Text = "mmHg";
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell194,
            this.xrLabel身高,
            this.xrTableCell205,
            this.xrTableCell21,
            this.xrLabel体重,
            this.xrTableCell22});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.StylePriority.UseBorders = false;
            this.xrTableRow8.Weight = 1.0741358753809056D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.Weight = 0.17997997272764338D;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Text = "身高";
            this.xrTableCell194.Weight = 0.60176594308093345D;
            // 
            // xrLabel身高
            // 
            this.xrLabel身高.Name = "xrLabel身高";
            this.xrLabel身高.Weight = 0.53124225917119416D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100F);
            this.xrTableCell205.StylePriority.UseBorders = false;
            this.xrTableCell205.StylePriority.UsePadding = false;
            this.xrTableCell205.StylePriority.UseTextAlignment = false;
            this.xrTableCell205.Text = "cm";
            this.xrTableCell205.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell205.Weight = 0.24087315929556075D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Text = "体重";
            this.xrTableCell21.Weight = 0.30483527810043487D;
            // 
            // xrLabel体重
            // 
            this.xrLabel体重.Name = "xrLabel体重";
            this.xrLabel体重.Weight = 0.89932986263105752D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100F);
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UsePadding = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "kg";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell22.Weight = 0.24197352499317551D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell206,
            this.xrLabel腰围,
            this.xrTableCell207,
            this.xrTableCell24,
            this.xrLabel体重指数,
            this.xrTableCell25});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorders = false;
            this.xrTableRow9.Weight = 1.7902264424431977D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.Weight = 0.17997997272764338D;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.Text = "腰围";
            this.xrTableCell206.Weight = 0.60176594308094833D;
            // 
            // xrLabel腰围
            // 
            this.xrLabel腰围.Name = "xrLabel腰围";
            this.xrLabel腰围.Weight = 0.5312422591710998D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 10, 0, 0, 100F);
            this.xrTableCell207.StylePriority.UseBorders = false;
            this.xrTableCell207.StylePriority.UsePadding = false;
            this.xrTableCell207.StylePriority.UseTextAlignment = false;
            this.xrTableCell207.Text = "cm";
            this.xrTableCell207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell207.Weight = 0.24087315929564027D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Multiline = true;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Text = "体质指数\r\n（BMI）";
            this.xrTableCell24.Weight = 0.30483527810043487D;
            // 
            // xrLabel体重指数
            // 
            this.xrLabel体重指数.Name = "xrLabel体重指数";
            this.xrLabel体重指数.Weight = 0.89932980732520285D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Text = "kg/㎡";
            this.xrTableCell25.Weight = 0.2419735802990301D;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.xrTableCell34,
            this.xrTableCell27,
            this.xrTableCell28});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.StylePriority.UseBorders = false;
            this.xrTableRow10.Weight = 1.7902264547277311D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell26.Multiline = true;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Text = "一\r\n般";
            this.xrTableCell26.Weight = 0.17997997272764338D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Multiline = true;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Text = "老年人健康状态\r\n自我评估*";
            this.xrTableCell34.Weight = 0.60176643136217733D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "1满意 2基本满意 3说不清楚 4不太满意 5不满意";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell27.Weight = 2.1220982747085255D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel老年人健康状态自我评估});
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.Weight = 0.096155321201653265D;
            // 
            // xrLabel老年人健康状态自我评估
            // 
            this.xrLabel老年人健康状态自我评估.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel老年人健康状态自我评估.CanGrow = false;
            this.xrLabel老年人健康状态自我评估.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel老年人健康状态自我评估.LocationFloat = new DevExpress.Utils.PointFloat(1.68F, 10.80708F);
            this.xrLabel老年人健康状态自我评估.Name = "xrLabel老年人健康状态自我评估";
            this.xrLabel老年人健康状态自我评估.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人健康状态自我评估.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel老年人健康状态自我评估.StylePriority.UseBorders = false;
            this.xrLabel老年人健康状态自我评估.StylePriority.UseFont = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell36,
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.StylePriority.UseBorders = false;
            this.xrTableRow11.Weight = 1.7902264237701737D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell29.Multiline = true;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.Text = "状\r\n况";
            this.xrTableCell29.Weight = 0.17997997272764338D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Multiline = true;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "老年人生活自理能力自我评估*";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell36.Weight = 0.60176643130802887D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Multiline = true;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "1可自理（0~3分）      2轻度依赖（4~8分）\r\n3中度依赖（9~18分）   4不能自理（≥19分）";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell30.Weight = 2.1220980306492057D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel老年人生活自理能力评估});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.Text = "xrTableCell31";
            this.xrTableCell31.Weight = 0.096155565315121427D;
            // 
            // xrLabel老年人生活自理能力评估
            // 
            this.xrLabel老年人生活自理能力评估.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel老年人生活自理能力评估.CanGrow = false;
            this.xrLabel老年人生活自理能力评估.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel老年人生活自理能力评估.LocationFloat = new DevExpress.Utils.PointFloat(1.578125F, 15F);
            this.xrLabel老年人生活自理能力评估.Name = "xrLabel老年人生活自理能力评估";
            this.xrLabel老年人生活自理能力评估.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人生活自理能力评估.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel老年人生活自理能力评估.StylePriority.UseBorders = false;
            this.xrLabel老年人生活自理能力评估.StylePriority.UseFont = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 1.7902263892124779D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Weight = 0.17997997272764338D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Multiline = true;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "老年人\r\n认知功能*";
            this.xrTableCell38.Weight = 0.601766431308009D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel18,
            this.xrLabel老年人认知功能分});
            this.xrTableCell39.Multiline = true;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrTableCell39.StylePriority.UsePadding = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell39.Weight = 2.1184218658513547D;
            // 
            // xrLabel老年人认知功能分
            // 
            this.xrLabel老年人认知功能分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel老年人认知功能分.LocationFloat = new DevExpress.Utils.PointFloat(344.1168F, 5F);
            this.xrLabel老年人认知功能分.Name = "xrLabel老年人认知功能分";
            this.xrLabel老年人认知功能分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人认知功能分.SizeF = new System.Drawing.SizeF(99.1925F, 34.74408F);
            this.xrLabel老年人认知功能分.StylePriority.UseBorders = false;
            this.xrLabel老年人认知功能分.StylePriority.UseTextAlignment = false;
            this.xrLabel老年人认知功能分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel老年人认知功能});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.Weight = 0.09983173011299229D;
            // 
            // xrLabel老年人认知功能
            // 
            this.xrLabel老年人认知功能.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel老年人认知功能.CanGrow = false;
            this.xrLabel老年人认知功能.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel老年人认知功能.LocationFloat = new DevExpress.Utils.PointFloat(1.041504F, 16.53767F);
            this.xrLabel老年人认知功能.Name = "xrLabel老年人认知功能";
            this.xrLabel老年人认知功能.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人认知功能.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel老年人认知功能.StylePriority.UseBorders = false;
            this.xrLabel老年人认知功能.StylePriority.UseFont = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.StylePriority.UseBorders = false;
            this.xrTableRow13.Weight = 1.7902263788480439D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.Weight = 0.17997997272764338D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Multiline = true;
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "老年人\r\n情感状态*";
            this.xrTableCell42.Weight = 0.60176643130802887D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel老年人情感状态分,
            this.xrLabel19});
            this.xrTableCell43.Multiline = true;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell43.Weight = 2.1184228987774487D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel老年人情感状态});
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.Weight = 0.099830697186877718D;
            // 
            // xrLabel老年人情感状态
            // 
            this.xrLabel老年人情感状态.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel老年人情感状态.CanGrow = false;
            this.xrLabel老年人情感状态.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel老年人情感状态.LocationFloat = new DevExpress.Utils.PointFloat(1.041443F, 17.85823F);
            this.xrLabel老年人情感状态.Name = "xrLabel老年人情感状态";
            this.xrLabel老年人情感状态.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人情感状态.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel老年人情感状态.StylePriority.UseBorders = false;
            this.xrLabel老年人情感状态.StylePriority.UseFont = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell48});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.StylePriority.UseBorders = false;
            this.xrTableRow14.Weight = 3.1229504632773386D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Weight = 0.17997997272764338D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Text = "体育锻炼";
            this.xrTableCell46.Weight = 0.601766431308009D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell48.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel20,
            this.xrLabel21,
            this.xrLabel22,
            this.xrLabel23,
            this.xrLabel24,
            this.xrLabel25,
            this.xrLabel26,
            this.xrLabel每次锻炼时间,
            this.xrLabel坚持锻炼时间,
            this.xrLabel锻炼方式,
            this.xrLabel锻炼频率});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Weight = 2.218253595964347D;
            // 
            // xrLabel锻炼频率
            // 
            this.xrLabel锻炼频率.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel锻炼频率.CanGrow = false;
            this.xrLabel锻炼频率.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel锻炼频率.LocationFloat = new DevExpress.Utils.PointFloat(531.5232F, 5.358013F);
            this.xrLabel锻炼频率.Name = "xrLabel锻炼频率";
            this.xrLabel锻炼频率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel锻炼频率.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel锻炼频率.StylePriority.UseBorders = false;
            this.xrLabel锻炼频率.StylePriority.UseFont = false;
            // 
            // xrLabel每次锻炼时间
            // 
            this.xrLabel每次锻炼时间.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel每次锻炼时间.LocationFloat = new DevExpress.Utils.PointFloat(132.7009F, 26.03111F);
            this.xrLabel每次锻炼时间.Name = "xrLabel每次锻炼时间";
            this.xrLabel每次锻炼时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel每次锻炼时间.SizeF = new System.Drawing.SizeF(60F, 26.03101F);
            this.xrLabel每次锻炼时间.StylePriority.UseBorders = false;
            // 
            // xrLabel锻炼方式
            // 
            this.xrLabel锻炼方式.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel锻炼方式.LocationFloat = new DevExpress.Utils.PointFloat(132.7007F, 52.06343F);
            this.xrLabel锻炼方式.Name = "xrLabel锻炼方式";
            this.xrLabel锻炼方式.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel锻炼方式.SizeF = new System.Drawing.SizeF(420.5F, 26.03F);
            this.xrLabel锻炼方式.StylePriority.UseBorders = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell221,
            this.xrTableCell215,
            this.xrTableCell220,
            this.xrTableCell222,
            this.xrTableCell52});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 1.8697921306359668D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.Weight = 0.17997997272764338D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.Text = "饮食习惯";
            this.xrTableCell50.Weight = 0.601766431308009D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = "1荤素均衡 2荤食为主 3素食为主 4嗜盐 5嗜油 6嗜糖";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell51.Weight = 1.8881100435091522D;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell221.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel饮食习惯1});
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseBorders = false;
            this.xrTableCell221.Weight = 0.072720924559111236D;
            // 
            // xrLabel饮食习惯1
            // 
            this.xrLabel饮食习惯1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel饮食习惯1.CanGrow = false;
            this.xrLabel饮食习惯1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel饮食习惯1.LocationFloat = new DevExpress.Utils.PointFloat(3.045273F, 12F);
            this.xrLabel饮食习惯1.Name = "xrLabel饮食习惯1";
            this.xrLabel饮食习惯1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel饮食习惯1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel饮食习惯1.StylePriority.UseBorders = false;
            this.xrLabel饮食习惯1.StylePriority.UseFont = false;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.StylePriority.UseBorders = false;
            this.xrTableCell215.Text = "/";
            this.xrTableCell215.Weight = 0.039447766832717522D;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell220.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel饮食习惯2});
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.StylePriority.UseBorders = false;
            this.xrTableCell220.Weight = 0.08465416927924059D;
            // 
            // xrLabel饮食习惯2
            // 
            this.xrLabel饮食习惯2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel饮食习惯2.CanGrow = false;
            this.xrLabel饮食习惯2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel饮食习惯2.LocationFloat = new DevExpress.Utils.PointFloat(3.99971F, 11.99998F);
            this.xrLabel饮食习惯2.Name = "xrLabel饮食习惯2";
            this.xrLabel饮食习惯2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel饮食习惯2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel饮食习惯2.StylePriority.UseBorders = false;
            this.xrLabel饮食习惯2.StylePriority.UseFont = false;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.StylePriority.UseBorders = false;
            this.xrTableCell222.Text = "/";
            this.xrTableCell222.Weight = 0.043195889015388858D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel饮食习惯3});
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseBorders = false;
            this.xrTableCell52.Weight = 0.090124802768736439D;
            // 
            // xrLabel饮食习惯3
            // 
            this.xrLabel饮食习惯3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel饮食习惯3.CanGrow = false;
            this.xrLabel饮食习惯3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel饮食习惯3.LocationFloat = new DevExpress.Utils.PointFloat(0.5337397F, 11.99998F);
            this.xrLabel饮食习惯3.Name = "xrLabel饮食习惯3";
            this.xrLabel饮食习惯3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel饮食习惯3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel饮食习惯3.StylePriority.UseBorders = false;
            this.xrLabel饮食习惯3.StylePriority.UseFont = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.xrTableCell54,
            this.xrTableCell56});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.Weight = 3.1826248197232361D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 5, 100F);
            this.xrTableCell53.StylePriority.UseBorders = false;
            this.xrTableCell53.StylePriority.UsePadding = false;
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.Text = "生";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell53.Weight = 0.17997997272764338D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "吸烟情况";
            this.xrTableCell54.Weight = 0.601766431308009D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Weight = 2.218253595964347D;
            // 
            // xrTable6
            // 
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.670288E-05F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow51,
            this.xrTableRow52,
            this.xrTableRow53});
            this.xrTable6.SizeF = new System.Drawing.SizeF(554.6245F, 78.09338F);
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell223,
            this.xrTableCell224,
            this.xrTableCell225});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1D;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Text = "吸烟状况";
            this.xrTableCell223.Weight = 0.71778794608527063D;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.Text = "1 从不吸烟   2 已戒烟    3 吸烟";
            this.xrTableCell224.Weight = 2.160286059890367D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell225.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel吸烟状况});
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseBorders = false;
            this.xrTableCell225.Weight = 0.12192599402436222D;
            // 
            // xrLabel吸烟状况
            // 
            this.xrLabel吸烟状况.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel吸烟状况.CanGrow = false;
            this.xrLabel吸烟状况.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel吸烟状况.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.172998F);
            this.xrLabel吸烟状况.Name = "xrLabel吸烟状况";
            this.xrLabel吸烟状况.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel吸烟状况.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel吸烟状况.StylePriority.UseBorders = false;
            this.xrLabel吸烟状况.StylePriority.UseFont = false;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell226,
            this.xrTableCell231,
            this.xrTableCell227});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 1D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Text = "日吸烟量";
            this.xrTableCell55.Weight = 0.71778794608527063D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Text = "平均";
            this.xrTableCell226.Weight = 0.32643287703770496D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell231.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel日吸烟量});
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.StylePriority.UseBorders = false;
            this.xrTableCell231.Weight = 0.7067374128387115D;
            // 
            // xrLabel日吸烟量
            // 
            this.xrLabel日吸烟量.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel日吸烟量.LocationFloat = new DevExpress.Utils.PointFloat(0.8638916F, 8.773804E-05F);
            this.xrLabel日吸烟量.Name = "xrLabel日吸烟量";
            this.xrLabel日吸烟量.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel日吸烟量.SizeF = new System.Drawing.SizeF(100.9935F, 24.40186F);
            this.xrLabel日吸烟量.StylePriority.UseBorders = false;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.StylePriority.UseBorders = false;
            this.xrTableCell227.Text = "支";
            this.xrTableCell227.Weight = 1.2490417640383127D;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell228,
            this.xrTableCell234,
            this.xrTableCell232,
            this.xrTableCell233,
            this.xrTableCell229,
            this.xrTableCell230});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseBorders = false;
            this.xrTableCell228.Text = "开始吸烟年龄";
            this.xrTableCell228.Weight = 0.71778794608527063D;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell234.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel开始吸烟年龄});
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.StylePriority.UseBorders = false;
            this.xrTableCell234.Weight = 0.63069640828301377D;
            // 
            // xrLabel开始吸烟年龄
            // 
            this.xrLabel开始吸烟年龄.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel开始吸烟年龄.LocationFloat = new DevExpress.Utils.PointFloat(9.999878F, 0.4642792F);
            this.xrLabel开始吸烟年龄.Name = "xrLabel开始吸烟年龄";
            this.xrLabel开始吸烟年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel开始吸烟年龄.SizeF = new System.Drawing.SizeF(96.59998F, 16.72711F);
            this.xrLabel开始吸烟年龄.StylePriority.UseBorders = false;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.StylePriority.UseBorders = false;
            this.xrTableCell232.Text = "岁";
            this.xrTableCell232.Weight = 0.14356544329631396D;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.StylePriority.UseTextAlignment = false;
            this.xrTableCell233.Text = "戒烟年龄";
            this.xrTableCell233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell233.Weight = 0.61294216009502178D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell229.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel戒烟年龄});
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseBorders = false;
            this.xrTableCell229.Weight = 0.77308204821601767D;
            // 
            // xrLabel戒烟年龄
            // 
            this.xrLabel戒烟年龄.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel戒烟年龄.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.4642792F);
            this.xrLabel戒烟年龄.Name = "xrLabel戒烟年龄";
            this.xrLabel戒烟年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel戒烟年龄.SizeF = new System.Drawing.SizeF(130.9652F, 23.70917F);
            this.xrLabel戒烟年龄.StylePriority.UseBorders = false;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.StylePriority.UseBorders = false;
            this.xrTableCell230.Text = "岁";
            this.xrTableCell230.Weight = 0.12192599402436222D;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.xrTableCell58,
            this.xrTableCell60});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.StylePriority.UseBorders = false;
            this.xrTableRow17.Weight = 5.8521571326792623D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell57.Multiline = true;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.StylePriority.UsePadding = false;
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.Text = "活\r\n方\r\n式";
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell57.Weight = 0.17997997272764338D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Text = "饮酒情况";
            this.xrTableCell58.Weight = 0.6017661871944564D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7});
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Weight = 2.2182538400778995D;
            // 
            // xrTable7
            // 
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.03860092F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow54,
            this.xrTableRow55,
            this.xrTableRow56,
            this.xrTableRow57,
            this.xrTableRow58});
            this.xrTable7.SizeF = new System.Drawing.SizeF(554.6246F, 130.1171F);
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell235,
            this.xrTableCell236,
            this.xrTableCell237});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Text = "饮酒频率";
            this.xrTableCell235.Weight = 0.71778816956140423D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Text = "1 从不  2 偶尔  3 经常  4 每天";
            this.xrTableCell236.Weight = 2.1602838674559197D;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell237.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel饮酒频率});
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.StylePriority.UseBorders = false;
            this.xrTableCell237.Weight = 0.12192796298267583D;
            // 
            // xrLabel饮酒频率
            // 
            this.xrLabel饮酒频率.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel饮酒频率.CanGrow = false;
            this.xrLabel饮酒频率.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel饮酒频率.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel饮酒频率.Name = "xrLabel饮酒频率";
            this.xrLabel饮酒频率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel饮酒频率.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel饮酒频率.StylePriority.UseBorders = false;
            this.xrLabel饮酒频率.StylePriority.UseFont = false;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59,
            this.xrTableCell238,
            this.xrTableCell249,
            this.xrTableCell239});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 1D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Text = "日饮酒量";
            this.xrTableCell59.Weight = 0.71778816956140423D;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.Text = "平均";
            this.xrTableCell238.Weight = 0.32643294834379422D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell249.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel日饮酒量});
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.StylePriority.UseBorders = false;
            this.xrTableCell249.Weight = 0.40166062646687339D;
            // 
            // xrLabel日饮酒量
            // 
            this.xrLabel日饮酒量.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel日饮酒量.LocationFloat = new DevExpress.Utils.PointFloat(3.892334F, 0F);
            this.xrLabel日饮酒量.Name = "xrLabel日饮酒量";
            this.xrLabel日饮酒量.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel日饮酒量.SizeF = new System.Drawing.SizeF(70.36462F, 24.40186F);
            this.xrLabel日饮酒量.StylePriority.UseBorders = false;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.StylePriority.UseBorders = false;
            this.xrTableCell239.Text = "两";
            this.xrTableCell239.Weight = 1.5541182556279281D;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell240,
            this.xrTableCell251,
            this.xrTableCell241,
            this.xrTableCell250,
            this.xrTableCell242});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 1D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Text = "是否戒酒";
            this.xrTableCell240.Weight = 0.71778816956140423D;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.Text = "1未戒酒 2已戒酒，戒酒年龄：";
            this.xrTableCell251.Weight = 1.5523591953506948D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell241.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel戒酒年龄});
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.StylePriority.UseBorders = false;
            this.xrTableCell241.Weight = 0.31680377596035508D;
            // 
            // xrLabel戒酒年龄
            // 
            this.xrLabel戒酒年龄.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel戒酒年龄.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 8.773804E-05F);
            this.xrLabel戒酒年龄.Name = "xrLabel戒酒年龄";
            this.xrLabel戒酒年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel戒酒年龄.SizeF = new System.Drawing.SizeF(56.91565F, 24F);
            this.xrLabel戒酒年龄.StylePriority.UseBorders = false;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseBorders = false;
            this.xrTableCell250.Text = "岁";
            this.xrTableCell250.Weight = 0.29154540744351293D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell242.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel是否戒酒});
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.StylePriority.UseBorders = false;
            this.xrTableCell242.Weight = 0.12150345168403294D;
            // 
            // xrLabel是否戒酒
            // 
            this.xrLabel是否戒酒.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel是否戒酒.CanGrow = false;
            this.xrLabel是否戒酒.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel是否戒酒.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel是否戒酒.Name = "xrLabel是否戒酒";
            this.xrLabel是否戒酒.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel是否戒酒.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel是否戒酒.StylePriority.UseBorders = false;
            this.xrLabel是否戒酒.StylePriority.UseFont = false;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell243,
            this.xrTableCell253,
            this.xrTableCell244,
            this.xrTableCell252,
            this.xrTableCell254,
            this.xrTableCell245});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 1D;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Text = "开始饮酒年龄";
            this.xrTableCell243.Weight = 0.71778816956140423D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel开始饮酒年龄});
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Weight = 0.63069505134182646D;
            // 
            // xrLabel开始饮酒年龄
            // 
            this.xrLabel开始饮酒年龄.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.027737F);
            this.xrLabel开始饮酒年龄.Name = "xrLabel开始饮酒年龄";
            this.xrLabel开始饮酒年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel开始饮酒年龄.SizeF = new System.Drawing.SizeF(116.5996F, 24.40186F);
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.StylePriority.UseBorders = false;
            this.xrTableCell244.Text = "岁";
            this.xrTableCell244.Weight = 0.14356475847723194D;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Text = "近一年内是否醉酒";
            this.xrTableCell252.Weight = 0.87799923705640959D;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Text = "1是 2否";
            this.xrTableCell254.Weight = 0.50802595302428832D;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell245.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel近一年是否醉酒});
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.StylePriority.UseBorders = false;
            this.xrTableCell245.Weight = 0.12192683053883943D;
            // 
            // xrLabel近一年是否醉酒
            // 
            this.xrLabel近一年是否醉酒.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel近一年是否醉酒.CanGrow = false;
            this.xrLabel近一年是否醉酒.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel近一年是否醉酒.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel近一年是否醉酒.Name = "xrLabel近一年是否醉酒";
            this.xrLabel近一年是否醉酒.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel近一年是否醉酒.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel近一年是否醉酒.StylePriority.UseBorders = false;
            this.xrLabel近一年是否醉酒.StylePriority.UseFont = false;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell246,
            this.xrTableCell247,
            this.xrTableCell260,
            this.xrTableCell257,
            this.xrTableCell261,
            this.xrTableCell255,
            this.xrTableCell258,
            this.xrTableCell256,
            this.xrTableCell259,
            this.xrTableCell248});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 1D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Text = "饮酒种类";
            this.xrTableCell246.Weight = 0.71778816956140423D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Text = "1白酒 2啤酒 3红酒 4黄酒 5其他";
            this.xrTableCell247.Weight = 1.3872038066845314D;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell260.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel饮酒种类其他});
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.StylePriority.UseBorders = false;
            this.xrTableCell260.Weight = 0.32794697584089194D;
            // 
            // xrLabel饮酒种类其他
            // 
            this.xrLabel饮酒种类其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel饮酒种类其他.LocationFloat = new DevExpress.Utils.PointFloat(1.284058F, 0F);
            this.xrLabel饮酒种类其他.Name = "xrLabel饮酒种类其他";
            this.xrLabel饮酒种类其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel饮酒种类其他.SizeF = new System.Drawing.SizeF(59.34497F, 24F);
            this.xrLabel饮酒种类其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell257.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel饮酒种类1});
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.StylePriority.UseBorders = false;
            this.xrTableCell257.Weight = 0.10818129192156507D;
            // 
            // xrLabel饮酒种类1
            // 
            this.xrLabel饮酒种类1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel饮酒种类1.CanGrow = false;
            this.xrLabel饮酒种类1.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel饮酒种类1.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2.804977F);
            this.xrLabel饮酒种类1.Name = "xrLabel饮酒种类1";
            this.xrLabel饮酒种类1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel饮酒种类1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel饮酒种类1.StylePriority.UseBorders = false;
            this.xrLabel饮酒种类1.StylePriority.UseFont = false;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseBorders = false;
            this.xrTableCell261.Text = "/";
            this.xrTableCell261.Weight = 0.045831216520051968D;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell255.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel饮酒种类2});
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.StylePriority.UseBorders = false;
            this.xrTableCell255.Weight = 0.10818128598049025D;
            // 
            // xrLabel饮酒种类2
            // 
            this.xrLabel饮酒种类2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel饮酒种类2.CanGrow = false;
            this.xrLabel饮酒种类2.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel饮酒种类2.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel饮酒种类2.Name = "xrLabel饮酒种类2";
            this.xrLabel饮酒种类2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel饮酒种类2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel饮酒种类2.StylePriority.UseBorders = false;
            this.xrLabel饮酒种类2.StylePriority.UseFont = false;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.StylePriority.UseBorders = false;
            this.xrTableCell258.Text = "/";
            this.xrTableCell258.Weight = 0.031712243643765584D;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell256.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel饮酒种类3});
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.StylePriority.UseBorders = false;
            this.xrTableCell256.Weight = 0.092852741291084964D;
            // 
            // xrLabel饮酒种类3
            // 
            this.xrLabel饮酒种类3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel饮酒种类3.CanGrow = false;
            this.xrLabel饮酒种类3.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel饮酒种类3.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel饮酒种类3.Name = "xrLabel饮酒种类3";
            this.xrLabel饮酒种类3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel饮酒种类3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel饮酒种类3.StylePriority.UseBorders = false;
            this.xrLabel饮酒种类3.StylePriority.UseFont = false;
            this.xrLabel饮酒种类3.StylePriority.UsePadding = false;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.StylePriority.UseBorders = false;
            this.xrTableCell259.Text = "/";
            this.xrTableCell259.Weight = 0.055302268556214958D;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell248.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel饮酒种类4});
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.StylePriority.UseBorders = false;
            this.xrTableCell248.Weight = 0.125D;
            // 
            // xrLabel饮酒种类4
            // 
            this.xrLabel饮酒种类4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel饮酒种类4.CanGrow = false;
            this.xrLabel饮酒种类4.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel饮酒种类4.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel饮酒种类4.Name = "xrLabel饮酒种类4";
            this.xrLabel饮酒种类4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel饮酒种类4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel饮酒种类4.StylePriority.UseBorders = false;
            this.xrLabel饮酒种类4.StylePriority.UseFont = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell64});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.StylePriority.UseBorders = false;
            this.xrTableRow18.Weight = 6.7103091954449647D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.Weight = 0.17997997272764338D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell62.Multiline = true;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.Text = "职业病危害因素\r\n接触史";
            this.xrTableCell62.Weight = 0.60176594313522147D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Weight = 2.2182540841371345D;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.670288E-05F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow59,
            this.xrTableRow60,
            this.xrTableRow61,
            this.xrTableRow62,
            this.xrTableRow63,
            this.xrTableRow64});
            this.xrTable8.SizeF = new System.Drawing.SizeF(554.6255F, 156.187F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseTextAlignment = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell264,
            this.xrTableCell262,
            this.xrTableCell286,
            this.xrTableCell285,
            this.xrTableCell263});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 1D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.Text = "1无 2有（工种";
            this.xrTableCell63.Weight = 0.71846029065075379D;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel工种});
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.Weight = 0.45274623289905186D;
            // 
            // xrLabel工种
            // 
            this.xrLabel工种.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel工种.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 0F);
            this.xrLabel工种.Name = "xrLabel工种";
            this.xrLabel工种.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel工种.SizeF = new System.Drawing.SizeF(82.92F, 24F);
            this.xrLabel工种.StylePriority.UseBorders = false;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.StylePriority.UseBorders = false;
            this.xrTableCell262.Text = "从业时间";
            this.xrTableCell262.Weight = 0.49861368111617121D;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel从业时间});
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.Weight = 0.45532732698058231D;
            // 
            // xrLabel从业时间
            // 
            this.xrLabel从业时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel从业时间.LocationFloat = new DevExpress.Utils.PointFloat(8.341662F, 0.9783745F);
            this.xrLabel从业时间.Name = "xrLabel从业时间";
            this.xrLabel从业时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel从业时间.SizeF = new System.Drawing.SizeF(65.83716F, 24F);
            this.xrLabel从业时间.StylePriority.UseBorders = false;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.StylePriority.UseBorders = false;
            this.xrTableCell285.Text = "年）";
            this.xrTableCell285.Weight = 0.75296476410840019D;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell263.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel职业病有无});
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBorders = false;
            this.xrTableCell263.Weight = 0.1218877042450407D;
            // 
            // xrLabel职业病有无
            // 
            this.xrLabel职业病有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel职业病有无.CanGrow = false;
            this.xrLabel职业病有无.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel职业病有无.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel职业病有无.Name = "xrLabel职业病有无";
            this.xrLabel职业病有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel职业病有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel职业病有无.StylePriority.UseBorders = false;
            this.xrLabel职业病有无.StylePriority.UseFont = false;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell265,
            this.xrTableCell288,
            this.xrTableCell266,
            this.xrTableCell267,
            this.xrTableCell268});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 1D;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseBorders = false;
            this.xrTableCell265.Text = "毒物种类 粉尘";
            this.xrTableCell265.Weight = 0.71846029065075379D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel粉尘});
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.Weight = 0.87671148300673707D;
            // 
            // xrLabel粉尘
            // 
            this.xrLabel粉尘.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel粉尘.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.629395E-05F);
            this.xrLabel粉尘.Name = "xrLabel粉尘";
            this.xrLabel粉尘.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel粉尘.SizeF = new System.Drawing.SizeF(155.4305F, 24F);
            this.xrLabel粉尘.StylePriority.UseBorders = false;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.StylePriority.UseBorders = false;
            this.xrTableCell266.Text = "防护措施 1无 2有";
            this.xrTableCell266.Weight = 0.88635402015481912D;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel粉尘有});
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.Weight = 0.39658640760002606D;
            // 
            // xrLabel粉尘有
            // 
            this.xrLabel粉尘有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel粉尘有.LocationFloat = new DevExpress.Utils.PointFloat(0.0001220703F, 8.773804E-05F);
            this.xrLabel粉尘有.Name = "xrLabel粉尘有";
            this.xrLabel粉尘有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel粉尘有.SizeF = new System.Drawing.SizeF(70.89185F, 24F);
            this.xrLabel粉尘有.StylePriority.UseBorders = false;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell268.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel粉尘防护有无});
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.StylePriority.UseBorders = false;
            this.xrTableCell268.Weight = 0.12188779858766399D;
            // 
            // xrLabel粉尘防护有无
            // 
            this.xrLabel粉尘防护有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel粉尘防护有无.CanGrow = false;
            this.xrLabel粉尘防护有无.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel粉尘防护有无.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel粉尘防护有无.Name = "xrLabel粉尘防护有无";
            this.xrLabel粉尘防护有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel粉尘防护有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel粉尘防护有无.StylePriority.UseBorders = false;
            this.xrLabel粉尘防护有无.StylePriority.UseFont = false;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell269,
            this.xrTableCell270,
            this.xrTableCell271,
            this.xrTableCell287,
            this.xrTableCell272});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 1D;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.StylePriority.UseBorders = false;
            this.xrTableCell269.StylePriority.UseTextAlignment = false;
            this.xrTableCell269.Text = "放射物质";
            this.xrTableCell269.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell269.Weight = 0.89032872204560676D;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel放射物质});
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.Weight = 0.70484291683670808D;
            // 
            // xrLabel放射物质
            // 
            this.xrLabel放射物质.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel放射物质.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 1.525879E-05F);
            this.xrLabel放射物质.Name = "xrLabel放射物质";
            this.xrLabel放射物质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel放射物质.SizeF = new System.Drawing.SizeF(122.9999F, 24F);
            this.xrLabel放射物质.StylePriority.UseBorders = false;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.StylePriority.UseBorders = false;
            this.xrTableCell271.Text = "防护措施 1无 2有";
            this.xrTableCell271.Weight = 0.88635507813995162D;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel放射物质有});
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.Weight = 0.39658574046290423D;
            // 
            // xrLabel放射物质有
            // 
            this.xrLabel放射物质有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel放射物质有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.44302F);
            this.xrLabel放射物质有.Name = "xrLabel放射物质有";
            this.xrLabel放射物质有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel放射物质有.SizeF = new System.Drawing.SizeF(71.81091F, 20.55707F);
            this.xrLabel放射物质有.StylePriority.UseBorders = false;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell272.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel放射物质防护措施有无});
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.StylePriority.UseBorders = false;
            this.xrTableCell272.Weight = 0.12188754251482931D;
            // 
            // xrLabel放射物质防护措施有无
            // 
            this.xrLabel放射物质防护措施有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel放射物质防护措施有无.CanGrow = false;
            this.xrLabel放射物质防护措施有无.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel放射物质防护措施有无.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel放射物质防护措施有无.Name = "xrLabel放射物质防护措施有无";
            this.xrLabel放射物质防护措施有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel放射物质防护措施有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel放射物质防护措施有无.StylePriority.UseBorders = false;
            this.xrLabel放射物质防护措施有无.StylePriority.UseFont = false;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell273,
            this.xrTableCell274,
            this.xrTableCell275,
            this.xrTableCell290,
            this.xrTableCell276});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 1D;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.StylePriority.UseBorders = false;
            this.xrTableCell273.StylePriority.UseTextAlignment = false;
            this.xrTableCell273.Text = "物理因素";
            this.xrTableCell273.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell273.Weight = 0.8903296924268751D;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel物理因素});
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.Weight = 0.704842916836708D;
            // 
            // xrLabel物理因素
            // 
            this.xrLabel物理因素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel物理因素.LocationFloat = new DevExpress.Utils.PointFloat(0.08312988F, 1.328445F);
            this.xrLabel物理因素.Name = "xrLabel物理因素";
            this.xrLabel物理因素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel物理因素.SizeF = new System.Drawing.SizeF(122.9999F, 24F);
            this.xrLabel物理因素.StylePriority.UseBorders = false;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.StylePriority.UseBorders = false;
            this.xrTableCell275.Text = "防护措施 1无 2有";
            this.xrTableCell275.Weight = 0.88635467381442323D;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel物理因素有});
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.Weight = 0.39658503963198827D;
            // 
            // xrLabel物理因素有
            // 
            this.xrLabel物理因素有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel物理因素有.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 1.328457F);
            this.xrLabel物理因素有.Name = "xrLabel物理因素有";
            this.xrLabel物理因素有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel物理因素有.SizeF = new System.Drawing.SizeF(71.81067F, 24F);
            this.xrLabel物理因素有.StylePriority.UseBorders = false;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell276.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel物理因素防护措施有无});
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.StylePriority.UseBorders = false;
            this.xrTableCell276.Weight = 0.12188767729000546D;
            // 
            // xrLabel物理因素防护措施有无
            // 
            this.xrLabel物理因素防护措施有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel物理因素防护措施有无.CanGrow = false;
            this.xrLabel物理因素防护措施有无.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel物理因素防护措施有无.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel物理因素防护措施有无.Name = "xrLabel物理因素防护措施有无";
            this.xrLabel物理因素防护措施有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel物理因素防护措施有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel物理因素防护措施有无.StylePriority.UseBorders = false;
            this.xrLabel物理因素防护措施有无.StylePriority.UseFont = false;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell277,
            this.xrTableCell278,
            this.xrTableCell279,
            this.xrTableCell292,
            this.xrTableCell280});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 1D;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.StylePriority.UseBorders = false;
            this.xrTableCell277.StylePriority.UseTextAlignment = false;
            this.xrTableCell277.Text = "化学物质";
            this.xrTableCell277.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell277.Weight = 0.8903296924268751D;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel化学因素});
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.Weight = 0.704842916836708D;
            // 
            // xrLabel化学因素
            // 
            this.xrLabel化学因素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel化学因素.LocationFloat = new DevExpress.Utils.PointFloat(0.08312988F, 0F);
            this.xrLabel化学因素.Name = "xrLabel化学因素";
            this.xrLabel化学因素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel化学因素.SizeF = new System.Drawing.SizeF(122.9999F, 24F);
            this.xrLabel化学因素.StylePriority.UseBorders = false;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.StylePriority.UseBorders = false;
            this.xrTableCell279.Text = "防护措施 1无 2有";
            this.xrTableCell279.Weight = 0.88635532073526857D;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel化学因素有});
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.Weight = 0.39658450053128375D;
            // 
            // xrLabel化学因素有
            // 
            this.xrLabel化学因素有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel化学因素有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.670288E-05F);
            this.xrLabel化学因素有.Name = "xrLabel化学因素有";
            this.xrLabel化学因素有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel化学因素有.SizeF = new System.Drawing.SizeF(71.81067F, 24F);
            this.xrLabel化学因素有.StylePriority.UseBorders = false;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell280.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel化学因素防护措施有无});
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.StylePriority.UseBorders = false;
            this.xrTableCell280.Weight = 0.12188756946986457D;
            // 
            // xrLabel化学因素防护措施有无
            // 
            this.xrLabel化学因素防护措施有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel化学因素防护措施有无.CanGrow = false;
            this.xrLabel化学因素防护措施有无.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel化学因素防护措施有无.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel化学因素防护措施有无.Name = "xrLabel化学因素防护措施有无";
            this.xrLabel化学因素防护措施有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel化学因素防护措施有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel化学因素防护措施有无.StylePriority.UseBorders = false;
            this.xrLabel化学因素防护措施有无.StylePriority.UseFont = false;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell281,
            this.xrTableCell282,
            this.xrTableCell283,
            this.xrTableCell293,
            this.xrTableCell284});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 1D;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.StylePriority.UseBorders = false;
            this.xrTableCell281.StylePriority.UseTextAlignment = false;
            this.xrTableCell281.Text = "其他";
            this.xrTableCell281.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell281.Weight = 0.67834565897244181D;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel毒物其他});
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.Weight = 0.9168263033702958D;
            // 
            // xrLabel毒物其他
            // 
            this.xrLabel毒物其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel毒物其他.LocationFloat = new DevExpress.Utils.PointFloat(0.0001831055F, 0F);
            this.xrLabel毒物其他.Name = "xrLabel毒物其他";
            this.xrLabel毒物其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel毒物其他.SizeF = new System.Drawing.SizeF(162.9998F, 24F);
            this.xrLabel毒物其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.StylePriority.UseBorders = false;
            this.xrTableCell283.Text = "防护措施 1无 2有";
            this.xrTableCell283.Weight = 0.88591217995612792D;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel毒物其他有});
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.Weight = 0.39395523900760188D;
            // 
            // xrLabel毒物其他有
            // 
            this.xrLabel毒物其他有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel毒物其他有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 8.773804E-05F);
            this.xrLabel毒物其他有.Name = "xrLabel毒物其他有";
            this.xrLabel毒物其他有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel毒物其他有.SizeF = new System.Drawing.SizeF(71.89282F, 24F);
            this.xrLabel毒物其他有.StylePriority.UseBorders = false;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell284.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel毒物其他防护措施有无});
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.StylePriority.UseBorders = false;
            this.xrTableCell284.Weight = 0.12496061869353259D;
            // 
            // xrLabel毒物其他防护措施有无
            // 
            this.xrLabel毒物其他防护措施有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel毒物其他防护措施有无.CanGrow = false;
            this.xrLabel毒物其他防护措施有无.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel毒物其他防护措施有无.LocationFloat = new DevExpress.Utils.PointFloat(2F, 2F);
            this.xrLabel毒物其他防护措施有无.Name = "xrLabel毒物其他防护措施有无";
            this.xrLabel毒物其他防护措施有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel毒物其他防护措施有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel毒物其他防护措施有无.StylePriority.UseBorders = false;
            this.xrLabel毒物其他防护措施有无.StylePriority.UseFont = false;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(10F, 58.33333F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow23,
            this.xrTableRow22,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36});
            this.xrTable1.SizeF = new System.Drawing.SizeF(763.5417F, 987.0711F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell74,
            this.xrTableCell1,
            this.xrTableCell75,
            this.xrTableCell2,
            this.xrTableCell76,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseBorders = false;
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell74.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.StylePriority.UseFont = false;
            this.xrTableCell74.Weight = 0.38457756702411017D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Weight = 0.58046059115485527D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseFont = false;
            this.xrTableCell75.Text = "入/出院日期";
            this.xrTableCell75.Weight = 0.56608374977824449D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseFont = false;
            this.xrTableCell2.Text = "原因";
            this.xrTableCell2.Weight = 0.50031026810404911D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseFont = false;
            this.xrTableCell76.Text = "医疗机构名称";
            this.xrTableCell76.Weight = 0.75477843615072571D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "病案号";
            this.xrTableCell3.Weight = 0.50695957847253381D;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTable住院史日期1,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.StylePriority.UseBorders = false;
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell65.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.StylePriority.UseFont = false;
            this.xrTableCell65.Text = "住院";
            this.xrTableCell65.Weight = 0.38457756702411017D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell66.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.StylePriority.UseFont = false;
            this.xrTableCell66.Text = "住院史";
            this.xrTableCell66.Weight = 0.58046059115485527D;
            // 
            // xrTable住院史日期1
            // 
            this.xrTable住院史日期1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable住院史日期1.Name = "xrTable住院史日期1";
            this.xrTable住院史日期1.StylePriority.UseFont = false;
            this.xrTable住院史日期1.Text = "/";
            this.xrTable住院史日期1.Weight = 0.56608381558969934D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel住院史原因1});
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 0.500310268104049D;
            // 
            // xrLabel住院史原因1
            // 
            this.xrLabel住院史原因1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel住院史原因1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.3457031F);
            this.xrLabel住院史原因1.Name = "xrLabel住院史原因1";
            this.xrLabel住院史原因1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel住院史原因1.SizeF = new System.Drawing.SizeF(116F, 31.25154F);
            this.xrLabel住院史原因1.StylePriority.UseFont = false;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel住院史医疗机构1});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Weight = 0.7547785677736355D;
            // 
            // xrLabel住院史医疗机构1
            // 
            this.xrLabel住院史医疗机构1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrLabel住院史医疗机构1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel住院史医疗机构1.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 0F);
            this.xrLabel住院史医疗机构1.Name = "xrLabel住院史医疗机构1";
            this.xrLabel住院史医疗机构1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel住院史医疗机构1.SizeF = new System.Drawing.SizeF(175F, 31.25154F);
            this.xrLabel住院史医疗机构1.StylePriority.UseBorders = false;
            this.xrLabel住院史医疗机构1.StylePriority.UseFont = false;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell70.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel住院史病案号1});
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Weight = 0.50695938103816918D;
            // 
            // xrLabel住院史病案号1
            // 
            this.xrLabel住院史病案号1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel住院史病案号1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel住院史病案号1.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 0.3457069F);
            this.xrLabel住院史病案号1.Name = "xrLabel住院史病案号1";
            this.xrLabel住院史病案号1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel住院史病案号1.SizeF = new System.Drawing.SizeF(117.5417F, 31.25154F);
            this.xrLabel住院史病案号1.StylePriority.UseBorders = false;
            this.xrLabel住院史病案号1.StylePriority.UseFont = false;
            this.xrLabel住院史病案号1.StylePriority.UseTextAlignment = false;
            this.xrLabel住院史病案号1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTable住院史日期2,
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.StylePriority.UseBorders = false;
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell71.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.StylePriority.UseFont = false;
            this.xrTableCell71.Text = "治疗";
            this.xrTableCell71.Weight = 0.38457756702411017D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell72.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.StylePriority.UseFont = false;
            this.xrTableCell72.Weight = 0.58046059115485527D;
            // 
            // xrTable住院史日期2
            // 
            this.xrTable住院史日期2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable住院史日期2.Name = "xrTable住院史日期2";
            this.xrTable住院史日期2.StylePriority.UseFont = false;
            this.xrTable住院史日期2.Text = "/";
            this.xrTable住院史日期2.Weight = 0.56608381558969934D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel住院史原因2});
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Weight = 0.50031013648113931D;
            // 
            // xrLabel住院史原因2
            // 
            this.xrLabel住院史原因2.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrLabel住院史原因2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel住院史原因2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.178914E-05F);
            this.xrLabel住院史原因2.Name = "xrLabel住院史原因2";
            this.xrLabel住院史原因2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel住院史原因2.SizeF = new System.Drawing.SizeF(116F, 31.25154F);
            this.xrLabel住院史原因2.StylePriority.UseBorders = false;
            this.xrLabel住院史原因2.StylePriority.UseFont = false;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel住院史医疗机构2});
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.75477883101945487D;
            // 
            // xrLabel住院史医疗机构2
            // 
            this.xrLabel住院史医疗机构2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel住院史医疗机构2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.3457222F);
            this.xrLabel住院史医疗机构2.Name = "xrLabel住院史医疗机构2";
            this.xrLabel住院史医疗机构2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel住院史医疗机构2.SizeF = new System.Drawing.SizeF(175.0002F, 31.25154F);
            this.xrLabel住院史医疗机构2.StylePriority.UseFont = false;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel住院史病案号2});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseBorders = false;
            this.xrTableCell79.Weight = 0.50695924941525949D;
            // 
            // xrLabel住院史病案号2
            // 
            this.xrLabel住院史病案号2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel住院史病案号2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel住院史病案号2.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 0.3457387F);
            this.xrLabel住院史病案号2.Name = "xrLabel住院史病案号2";
            this.xrLabel住院史病案号2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel住院史病案号2.SizeF = new System.Drawing.SizeF(117.5417F, 31.25154F);
            this.xrLabel住院史病案号2.StylePriority.UseBorders = false;
            this.xrLabel住院史病案号2.StylePriority.UseFont = false;
            this.xrLabel住院史病案号2.StylePriority.UseTextAlignment = false;
            this.xrLabel住院史病案号2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell84,
            this.xrTableCell85});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.StylePriority.UseBorders = false;
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell80.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.StylePriority.UseFont = false;
            this.xrTableCell80.Text = "情况";
            this.xrTableCell80.Weight = 0.38457756702411017D;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell81.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.StylePriority.UseFont = false;
            this.xrTableCell81.StylePriority.UseTextAlignment = false;
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell81.Weight = 0.58046059115485527D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseFont = false;
            this.xrTableCell82.Text = "建/撤床日期";
            this.xrTableCell82.Weight = 0.56608381558969934D;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseFont = false;
            this.xrTableCell83.Text = "原因";
            this.xrTableCell83.Weight = 0.50031013648113931D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseFont = false;
            this.xrTableCell84.Text = "医疗机构名称";
            this.xrTableCell84.Weight = 0.75477883101945487D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell85.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.StylePriority.UseFont = false;
            this.xrTableCell85.Text = "病案号";
            this.xrTableCell85.Weight = 0.50695924941525949D;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell92,
            this.xrTableCell93,
            this.xrTable家庭病床史日期1,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell92.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.StylePriority.UseFont = false;
            this.xrTableCell92.Weight = 0.38457753411838275D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell93.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.StylePriority.UseFont = false;
            this.xrTableCell93.Text = "家庭";
            this.xrTableCell93.Weight = 0.58046062406058274D;
            // 
            // xrTable家庭病床史日期1
            // 
            this.xrTable家庭病床史日期1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable家庭病床史日期1.Name = "xrTable家庭病床史日期1";
            this.xrTable家庭病床史日期1.StylePriority.UseFont = false;
            this.xrTable家庭病床史日期1.Text = "/";
            this.xrTable家庭病床史日期1.Weight = 0.56608381558969934D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable家庭病床史原因1});
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.50031013648113931D;
            // 
            // xrTable家庭病床史原因1
            // 
            this.xrTable家庭病床史原因1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable家庭病床史原因1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable家庭病床史原因1.Name = "xrTable家庭病床史原因1";
            this.xrTable家庭病床史原因1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable家庭病床史原因1.SizeF = new System.Drawing.SizeF(116F, 31.25154F);
            this.xrTable家庭病床史原因1.StylePriority.UseBorders = false;
            this.xrTable家庭病床史原因1.StylePriority.UseFont = false;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable家庭病床史医疗机构1});
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Weight = 0.75477883101945487D;
            // 
            // xrTable家庭病床史医疗机构1
            // 
            this.xrTable家庭病床史医疗机构1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable家庭病床史医疗机构1.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 0F);
            this.xrTable家庭病床史医疗机构1.Name = "xrTable家庭病床史医疗机构1";
            this.xrTable家庭病床史医疗机构1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable家庭病床史医疗机构1.SizeF = new System.Drawing.SizeF(175.0002F, 31.25154F);
            this.xrTable家庭病床史医疗机构1.StylePriority.UseBorders = false;
            this.xrTable家庭病床史医疗机构1.StylePriority.UseFont = false;
            this.xrTable家庭病床史医疗机构1.StylePriority.UseTextAlignment = false;
            this.xrTable家庭病床史医疗机构1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell97.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable家庭病床史病案号1});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.Weight = 0.50695924941525949D;
            // 
            // xrTable家庭病床史病案号1
            // 
            this.xrTable家庭病床史病案号1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable家庭病床史病案号1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable家庭病床史病案号1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable家庭病床史病案号1.Name = "xrTable家庭病床史病案号1";
            this.xrTable家庭病床史病案号1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable家庭病床史病案号1.SizeF = new System.Drawing.SizeF(117.5417F, 31.25154F);
            this.xrTable家庭病床史病案号1.StylePriority.UseBorders = false;
            this.xrTable家庭病床史病案号1.StylePriority.UseFont = false;
            this.xrTable家庭病床史病案号1.StylePriority.UseTextAlignment = false;
            this.xrTable家庭病床史病案号1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTable家庭病床史日期2,
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell91});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.StylePriority.UseBorders = false;
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseFont = false;
            this.xrTableCell86.Weight = 0.38457753411838275D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseFont = false;
            this.xrTableCell87.StylePriority.UseTextAlignment = false;
            this.xrTableCell87.Text = "病床史";
            this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell87.Weight = 0.58046062406058274D;
            // 
            // xrTable家庭病床史日期2
            // 
            this.xrTable家庭病床史日期2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable家庭病床史日期2.Name = "xrTable家庭病床史日期2";
            this.xrTable家庭病床史日期2.StylePriority.UseFont = false;
            this.xrTable家庭病床史日期2.Text = "/";
            this.xrTable家庭病床史日期2.Weight = 0.56608381558969934D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable家庭病床史原因2});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 0.50031013648113931D;
            // 
            // xrTable家庭病床史原因2
            // 
            this.xrTable家庭病床史原因2.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTable家庭病床史原因2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable家庭病床史原因2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable家庭病床史原因2.Name = "xrTable家庭病床史原因2";
            this.xrTable家庭病床史原因2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable家庭病床史原因2.SizeF = new System.Drawing.SizeF(116F, 31.25154F);
            this.xrTable家庭病床史原因2.StylePriority.UseBorders = false;
            this.xrTable家庭病床史原因2.StylePriority.UseFont = false;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable家庭病床史医疗机构2});
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 0.75477883101945487D;
            // 
            // xrTable家庭病床史医疗机构2
            // 
            this.xrTable家庭病床史医疗机构2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable家庭病床史医疗机构2.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 0.3457069F);
            this.xrTable家庭病床史医疗机构2.Name = "xrTable家庭病床史医疗机构2";
            this.xrTable家庭病床史医疗机构2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable家庭病床史医疗机构2.SizeF = new System.Drawing.SizeF(175.0002F, 31.25154F);
            this.xrTable家庭病床史医疗机构2.StylePriority.UseFont = false;
            this.xrTable家庭病床史医疗机构2.StylePriority.UseTextAlignment = false;
            this.xrTable家庭病床史医疗机构2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell91.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable家庭病床史病案号2});
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Weight = 0.50695924941525949D;
            // 
            // xrTable家庭病床史病案号2
            // 
            this.xrTable家庭病床史病案号2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable家庭病床史病案号2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable家庭病床史病案号2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable家庭病床史病案号2.Name = "xrTable家庭病床史病案号2";
            this.xrTable家庭病床史病案号2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable家庭病床史病案号2.SizeF = new System.Drawing.SizeF(117.5417F, 31.25154F);
            this.xrTable家庭病床史病案号2.StylePriority.UseBorders = false;
            this.xrTable家庭病床史病案号2.StylePriority.UseFont = false;
            this.xrTable家庭病床史病案号2.StylePriority.UseTextAlignment = false;
            this.xrTable家庭病床史病案号2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell98,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102,
            this.xrTableCell103});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.StylePriority.UseBorders = false;
            this.xrTableRow24.Weight = 1.9999992152624135D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseFont = false;
            this.xrTableCell98.Weight = 0.38457753411838275D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseFont = false;
            this.xrTableCell99.Text = "药物名称";
            this.xrTableCell99.Weight = 0.58046062406058274D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseFont = false;
            this.xrTableCell100.Text = "用法";
            this.xrTableCell100.Weight = 0.350432840226911D;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseFont = false;
            this.xrTableCell101.Text = "用量";
            this.xrTableCell101.Weight = 0.4059628347599194D;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseFont = false;
            this.xrTableCell102.Text = "用药时间";
            this.xrTableCell102.Weight = 0.66492408245441326D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell103.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell103.Multiline = true;
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.StylePriority.UseFont = false;
            this.xrTableCell103.Text = "服药依从性\r\n1规律 2间断 3不服药";
            this.xrTableCell103.Weight = 0.90681227506430928D;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104,
            this.xrTable用药名称1,
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell109});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.StylePriority.UseBorders = false;
            this.xrTableRow25.Weight = 1.0000000435965033D;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell104.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseBorders = false;
            this.xrTableCell104.StylePriority.UseFont = false;
            this.xrTableCell104.Weight = 0.38457750121265533D;
            // 
            // xrTable用药名称1
            // 
            this.xrTable用药名称1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药名称1.Name = "xrTable用药名称1";
            this.xrTable用药名称1.StylePriority.UseFont = false;
            this.xrTable用药名称1.StylePriority.UseTextAlignment = false;
            this.xrTable用药名称1.Text = "1";
            this.xrTable用药名称1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable用药名称1.Weight = 0.58046065696631011D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell106.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用法1});
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.Weight = 0.350432840226911D;
            // 
            // xrTable用药用法1
            // 
            this.xrTable用药用法1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药用法1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用法1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.051758E-05F);
            this.xrTable用药用法1.Name = "xrTable用药用法1";
            this.xrTable用药用法1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用法1.SizeF = new System.Drawing.SizeF(81.25002F, 31.5972F);
            this.xrTable用药用法1.StylePriority.UseBorders = false;
            this.xrTable用药用法1.StylePriority.UseFont = false;
            this.xrTable用药用法1.StylePriority.UseTextAlignment = false;
            this.xrTable用药用法1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell107.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用量1});
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.Weight = 0.4059628347599194D;
            // 
            // xrTable用药用量1
            // 
            this.xrTable用药用量1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药用量1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用量1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药用量1.Name = "xrTable用药用量1";
            this.xrTable用药用量1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用量1.SizeF = new System.Drawing.SizeF(94.12497F, 31.59723F);
            this.xrTable用药用量1.StylePriority.UseBorders = false;
            this.xrTable用药用量1.StylePriority.UseFont = false;
            this.xrTable用药用量1.StylePriority.UseTextAlignment = false;
            this.xrTable用药用量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell108.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药时间1});
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.Weight = 0.66492408245441326D;
            // 
            // xrTable用药时间1
            // 
            this.xrTable用药时间1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药时间1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药时间1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药时间1.Name = "xrTable用药时间1";
            this.xrTable用药时间1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药时间1.SizeF = new System.Drawing.SizeF(154.1667F, 31.59723F);
            this.xrTable用药时间1.StylePriority.UseBorders = false;
            this.xrTable用药时间1.StylePriority.UseFont = false;
            this.xrTable用药时间1.StylePriority.UseTextAlignment = false;
            this.xrTable用药时间1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell109.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药依从性1});
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.Weight = 0.90681227506430928D;
            // 
            // xrTable用药依从性1
            // 
            this.xrTable用药依从性1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药依从性1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药依从性1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.051758E-05F);
            this.xrTable用药依从性1.Name = "xrTable用药依从性1";
            this.xrTable用药依从性1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药依从性1.SizeF = new System.Drawing.SizeF(210.2501F, 31.59723F);
            this.xrTable用药依从性1.StylePriority.UseBorders = false;
            this.xrTable用药依从性1.StylePriority.UseFont = false;
            this.xrTable用药依从性1.StylePriority.UseTextAlignment = false;
            this.xrTable用药依从性1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell110,
            this.xrTable用药名称2,
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell115});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.StylePriority.UseBorders = false;
            this.xrTableRow26.Weight = 1.0000000435965033D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell110.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.StylePriority.UseFont = false;
            this.xrTableCell110.Text = "主要";
            this.xrTableCell110.Weight = 0.38457750121265533D;
            // 
            // xrTable用药名称2
            // 
            this.xrTable用药名称2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药名称2.Name = "xrTable用药名称2";
            this.xrTable用药名称2.StylePriority.UseFont = false;
            this.xrTable用药名称2.StylePriority.UseTextAlignment = false;
            this.xrTable用药名称2.Text = "2";
            this.xrTable用药名称2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable用药名称2.Weight = 0.58046065696631011D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用法2});
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseTextAlignment = false;
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell112.Weight = 0.350432840226911D;
            // 
            // xrTable用药用法2
            // 
            this.xrTable用药用法2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用法2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药用法2.Name = "xrTable用药用法2";
            this.xrTable用药用法2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用法2.SizeF = new System.Drawing.SizeF(81.25002F, 31.59726F);
            this.xrTable用药用法2.StylePriority.UseBorders = false;
            this.xrTable用药用法2.StylePriority.UseFont = false;
            this.xrTable用药用法2.StylePriority.UseTextAlignment = false;
            this.xrTable用药用法2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用量2});
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 0.4059628347599194D;
            // 
            // xrTable用药用量2
            // 
            this.xrTable用药用量2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用量2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.xrTable用药用量2.Name = "xrTable用药用量2";
            this.xrTable用药用量2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用量2.SizeF = new System.Drawing.SizeF(93.5751F, 31.59723F);
            this.xrTable用药用量2.StylePriority.UseBorders = false;
            this.xrTable用药用量2.StylePriority.UseFont = false;
            this.xrTable用药用量2.StylePriority.UseTextAlignment = false;
            this.xrTable用药用量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药时间2});
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Weight = 0.66492408245441326D;
            // 
            // xrTable用药时间2
            // 
            this.xrTable用药时间2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable用药时间2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药时间2.LocationFloat = new DevExpress.Utils.PointFloat(0.07501221F, 1.525879E-05F);
            this.xrTable用药时间2.Name = "xrTable用药时间2";
            this.xrTable用药时间2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药时间2.SizeF = new System.Drawing.SizeF(154.0917F, 31.59723F);
            this.xrTable用药时间2.StylePriority.UseBorders = false;
            this.xrTable用药时间2.StylePriority.UseFont = false;
            this.xrTable用药时间2.StylePriority.UseTextAlignment = false;
            this.xrTable用药时间2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell115.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药依从性2});
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Weight = 0.90681227506430928D;
            // 
            // xrTable用药依从性2
            // 
            this.xrTable用药依从性2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药依从性2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药依从性2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药依从性2.Name = "xrTable用药依从性2";
            this.xrTable用药依从性2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药依从性2.SizeF = new System.Drawing.SizeF(210.2501F, 31.59723F);
            this.xrTable用药依从性2.StylePriority.UseBorders = false;
            this.xrTable用药依从性2.StylePriority.UseFont = false;
            this.xrTable用药依从性2.StylePriority.UseTextAlignment = false;
            this.xrTable用药依从性2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell116,
            this.xrTable用药名称3,
            this.xrTableCell118,
            this.xrTableCell119,
            this.xrTableCell120,
            this.xrTableCell121});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.StylePriority.UseBorders = false;
            this.xrTableRow27.Weight = 1.0000000435965033D;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell116.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.StylePriority.UseFont = false;
            this.xrTableCell116.Text = "用药";
            this.xrTableCell116.Weight = 0.38457746830692785D;
            // 
            // xrTable用药名称3
            // 
            this.xrTable用药名称3.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药名称3.Name = "xrTable用药名称3";
            this.xrTable用药名称3.StylePriority.UseFont = false;
            this.xrTable用药名称3.StylePriority.UseTextAlignment = false;
            this.xrTable用药名称3.Text = "3";
            this.xrTable用药名称3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable用药名称3.Weight = 0.58046068987203758D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell118.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用法3});
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.Weight = 0.350432840226911D;
            // 
            // xrTable用药用法3
            // 
            this.xrTable用药用法3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药用法3.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用法3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药用法3.Name = "xrTable用药用法3";
            this.xrTable用药用法3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用法3.SizeF = new System.Drawing.SizeF(81.25002F, 31.59723F);
            this.xrTable用药用法3.StylePriority.UseBorders = false;
            this.xrTable用药用法3.StylePriority.UseFont = false;
            this.xrTable用药用法3.StylePriority.UseTextAlignment = false;
            this.xrTable用药用法3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell119.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用量3});
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.Weight = 0.4059628347599194D;
            // 
            // xrTable用药用量3
            // 
            this.xrTable用药用量3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药用量3.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用量3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药用量3.Name = "xrTable用药用量3";
            this.xrTable用药用量3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用量3.SizeF = new System.Drawing.SizeF(93.5751F, 31.59723F);
            this.xrTable用药用量3.StylePriority.UseBorders = false;
            this.xrTable用药用量3.StylePriority.UseFont = false;
            this.xrTable用药用量3.StylePriority.UseTextAlignment = false;
            this.xrTable用药用量3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药时间3});
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Weight = 0.66492408245441326D;
            // 
            // xrTable用药时间3
            // 
            this.xrTable用药时间3.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药时间3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药时间3.Name = "xrTable用药时间3";
            this.xrTable用药时间3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药时间3.SizeF = new System.Drawing.SizeF(154.1667F, 31.25153F);
            this.xrTable用药时间3.StylePriority.UseFont = false;
            this.xrTable用药时间3.StylePriority.UseTextAlignment = false;
            this.xrTable用药时间3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell121.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药依从性3});
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Weight = 0.90681227506430928D;
            // 
            // xrTable用药依从性3
            // 
            this.xrTable用药依从性3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药依从性3.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药依从性3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药依从性3.Name = "xrTable用药依从性3";
            this.xrTable用药依从性3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药依从性3.SizeF = new System.Drawing.SizeF(210.2501F, 31.59723F);
            this.xrTable用药依从性3.StylePriority.UseBorders = false;
            this.xrTable用药依从性3.StylePriority.UseFont = false;
            this.xrTable用药依从性3.StylePriority.UseTextAlignment = false;
            this.xrTable用药依从性3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell122,
            this.xrTable用药名称4,
            this.xrTableCell124,
            this.xrTableCell125,
            this.xrTableCell126,
            this.xrTableCell127});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.StylePriority.UseBorders = false;
            this.xrTableRow28.Weight = 1.0000000435965033D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell122.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.StylePriority.UseFont = false;
            this.xrTableCell122.Text = "情况";
            this.xrTableCell122.Weight = 0.38457756702411017D;
            // 
            // xrTable用药名称4
            // 
            this.xrTable用药名称4.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药名称4.Name = "xrTable用药名称4";
            this.xrTable用药名称4.StylePriority.UseFont = false;
            this.xrTable用药名称4.StylePriority.UseTextAlignment = false;
            this.xrTable用药名称4.Text = "4";
            this.xrTable用药名称4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable用药名称4.Weight = 0.58046059115485527D;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell124.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用法4});
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Weight = 0.350432840226911D;
            // 
            // xrTable用药用法4
            // 
            this.xrTable用药用法4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药用法4.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用法4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药用法4.Name = "xrTable用药用法4";
            this.xrTable用药用法4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用法4.SizeF = new System.Drawing.SizeF(81.25002F, 31.5972F);
            this.xrTable用药用法4.StylePriority.UseBorders = false;
            this.xrTable用药用法4.StylePriority.UseFont = false;
            this.xrTable用药用法4.StylePriority.UseTextAlignment = false;
            this.xrTable用药用法4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用量4});
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 0.4059628347599194D;
            // 
            // xrTable用药用量4
            // 
            this.xrTable用药用量4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药用量4.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用量4.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 0F);
            this.xrTable用药用量4.Name = "xrTable用药用量4";
            this.xrTable用药用量4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用量4.SizeF = new System.Drawing.SizeF(93.57504F, 31.59723F);
            this.xrTable用药用量4.StylePriority.UseBorders = false;
            this.xrTable用药用量4.StylePriority.UseFont = false;
            this.xrTable用药用量4.StylePriority.UseTextAlignment = false;
            this.xrTable用药用量4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell126.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药时间4});
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseBorders = false;
            this.xrTableCell126.Weight = 0.66492408245441326D;
            // 
            // xrTable用药时间4
            // 
            this.xrTable用药时间4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药时间4.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药时间4.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 0F);
            this.xrTable用药时间4.Name = "xrTable用药时间4";
            this.xrTable用药时间4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药时间4.SizeF = new System.Drawing.SizeF(154.1667F, 31.59723F);
            this.xrTable用药时间4.StylePriority.UseBorders = false;
            this.xrTable用药时间4.StylePriority.UseFont = false;
            this.xrTable用药时间4.StylePriority.UseTextAlignment = false;
            this.xrTable用药时间4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell127.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药依从性4});
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.Weight = 0.90681227506430928D;
            // 
            // xrTable用药依从性4
            // 
            this.xrTable用药依从性4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药依从性4.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药依从性4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药依从性4.Name = "xrTable用药依从性4";
            this.xrTable用药依从性4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药依从性4.SizeF = new System.Drawing.SizeF(210.2501F, 31.59723F);
            this.xrTable用药依从性4.StylePriority.UseBorders = false;
            this.xrTable用药依从性4.StylePriority.UseFont = false;
            this.xrTable用药依从性4.StylePriority.UseTextAlignment = false;
            this.xrTable用药依从性4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell128,
            this.xrTable用药名称5,
            this.xrTableCell130,
            this.xrTableCell131,
            this.xrTableCell132,
            this.xrTableCell133});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.StylePriority.UseBorders = false;
            this.xrTableRow29.Weight = 1.0000000435965033D;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell128.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseBorders = false;
            this.xrTableCell128.StylePriority.UseFont = false;
            this.xrTableCell128.Weight = 0.38457756702411017D;
            // 
            // xrTable用药名称5
            // 
            this.xrTable用药名称5.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药名称5.Name = "xrTable用药名称5";
            this.xrTable用药名称5.StylePriority.UseFont = false;
            this.xrTable用药名称5.StylePriority.UseTextAlignment = false;
            this.xrTable用药名称5.Text = "5";
            this.xrTable用药名称5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable用药名称5.Weight = 0.58046059115485527D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用法5});
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Weight = 0.350432840226911D;
            // 
            // xrTable用药用法5
            // 
            this.xrTable用药用法5.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用法5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.3457069F);
            this.xrTable用药用法5.Name = "xrTable用药用法5";
            this.xrTable用药用法5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用法5.SizeF = new System.Drawing.SizeF(81.25002F, 31.25153F);
            this.xrTable用药用法5.StylePriority.UseBorders = false;
            this.xrTable用药用法5.StylePriority.UseFont = false;
            this.xrTable用药用法5.StylePriority.UseTextAlignment = false;
            this.xrTable用药用法5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell131.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用量5});
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.Weight = 0.4059628347599194D;
            // 
            // xrTable用药用量5
            // 
            this.xrTable用药用量5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药用量5.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用量5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药用量5.Name = "xrTable用药用量5";
            this.xrTable用药用量5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用量5.SizeF = new System.Drawing.SizeF(93.5751F, 31.59723F);
            this.xrTable用药用量5.StylePriority.UseBorders = false;
            this.xrTable用药用量5.StylePriority.UseFont = false;
            this.xrTable用药用量5.StylePriority.UseTextAlignment = false;
            this.xrTable用药用量5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药时间5});
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Weight = 0.66492408245441326D;
            // 
            // xrTable用药时间5
            // 
            this.xrTable用药时间5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药时间5.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药时间5.LocationFloat = new DevExpress.Utils.PointFloat(0.07501221F, 0F);
            this.xrTable用药时间5.Name = "xrTable用药时间5";
            this.xrTable用药时间5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药时间5.SizeF = new System.Drawing.SizeF(154.0917F, 31.59723F);
            this.xrTable用药时间5.StylePriority.UseBorders = false;
            this.xrTable用药时间5.StylePriority.UseFont = false;
            this.xrTable用药时间5.StylePriority.UseTextAlignment = false;
            this.xrTable用药时间5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell133.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药依从性5});
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.Weight = 0.90681227506430928D;
            // 
            // xrTable用药依从性5
            // 
            this.xrTable用药依从性5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药依从性5.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药依从性5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药依从性5.Name = "xrTable用药依从性5";
            this.xrTable用药依从性5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药依从性5.SizeF = new System.Drawing.SizeF(210.2501F, 31.59723F);
            this.xrTable用药依从性5.StylePriority.UseBorders = false;
            this.xrTable用药依从性5.StylePriority.UseFont = false;
            this.xrTable用药依从性5.StylePriority.UseTextAlignment = false;
            this.xrTable用药依从性5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell134,
            this.xrTable用药名称6,
            this.xrTableCell136,
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell139});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.StylePriority.UseBorders = false;
            this.xrTableRow30.Weight = 1.0000000435965033D;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.StylePriority.UseFont = false;
            this.xrTableCell134.Weight = 0.38457750121265533D;
            // 
            // xrTable用药名称6
            // 
            this.xrTable用药名称6.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药名称6.Name = "xrTable用药名称6";
            this.xrTable用药名称6.StylePriority.UseFont = false;
            this.xrTable用药名称6.StylePriority.UseTextAlignment = false;
            this.xrTable用药名称6.Text = "6";
            this.xrTable用药名称6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable用药名称6.Weight = 0.58046065696631011D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用法6});
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Weight = 0.350432840226911D;
            // 
            // xrTable用药用法6
            // 
            this.xrTable用药用法6.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用法6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.3457069F);
            this.xrTable用药用法6.Name = "xrTable用药用法6";
            this.xrTable用药用法6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用法6.SizeF = new System.Drawing.SizeF(81.25002F, 31.25153F);
            this.xrTable用药用法6.StylePriority.UseFont = false;
            this.xrTable用药用法6.StylePriority.UseTextAlignment = false;
            this.xrTable用药用法6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药用量6});
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Weight = 0.4059628347599194D;
            // 
            // xrTable用药用量6
            // 
            this.xrTable用药用量6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药用量6.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药用量6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药用量6.Name = "xrTable用药用量6";
            this.xrTable用药用量6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药用量6.SizeF = new System.Drawing.SizeF(93.5751F, 31.59723F);
            this.xrTable用药用量6.StylePriority.UseBorders = false;
            this.xrTable用药用量6.StylePriority.UseFont = false;
            this.xrTable用药用量6.StylePriority.UseTextAlignment = false;
            this.xrTable用药用量6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药时间6});
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Weight = 0.66492408245441326D;
            // 
            // xrTable用药时间6
            // 
            this.xrTable用药时间6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药时间6.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药时间6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药时间6.Name = "xrTable用药时间6";
            this.xrTable用药时间6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药时间6.SizeF = new System.Drawing.SizeF(154.1667F, 31.59723F);
            this.xrTable用药时间6.StylePriority.UseBorders = false;
            this.xrTable用药时间6.StylePriority.UseFont = false;
            this.xrTable用药时间6.StylePriority.UseTextAlignment = false;
            this.xrTable用药时间6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell139.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable用药依从性6});
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.StylePriority.UseBorders = false;
            this.xrTableCell139.Weight = 0.90681227506430928D;
            // 
            // xrTable用药依从性6
            // 
            this.xrTable用药依从性6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable用药依从性6.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable用药依从性6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable用药依从性6.Name = "xrTable用药依从性6";
            this.xrTable用药依从性6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTable用药依从性6.SizeF = new System.Drawing.SizeF(210.2501F, 31.59723F);
            this.xrTable用药依从性6.StylePriority.UseBorders = false;
            this.xrTable用药依从性6.StylePriority.UseFont = false;
            this.xrTable用药依从性6.StylePriority.UseTextAlignment = false;
            this.xrTable用药依从性6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell140,
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell145});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.StylePriority.UseBorders = false;
            this.xrTableRow31.Weight = 1.0000000435965033D;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell140.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.StylePriority.UseFont = false;
            this.xrTableCell140.Weight = 0.38457740249547295D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.StylePriority.UseFont = false;
            this.xrTableCell141.Text = "名称";
            this.xrTableCell141.Weight = 0.58046075568349242D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell142.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.StylePriority.UseFont = false;
            this.xrTableCell142.Text = "接种日期";
            this.xrTableCell142.Weight = 0.58074812140124177D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell145.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.StylePriority.UseFont = false;
            this.xrTableCell145.Text = "接种机构";
            this.xrTableCell145.Weight = 1.7473839111043112D;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell143,
            this.xrTable接种名称1,
            this.xrTableCell146,
            this.xrTableCell147});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.StylePriority.UseBorders = false;
            this.xrTableRow32.Weight = 1.0000000435965033D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell143.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.StylePriority.UseFont = false;
            this.xrTableCell143.Text = "非免疫";
            this.xrTableCell143.Weight = 0.38457740249547295D;
            // 
            // xrTable接种名称1
            // 
            this.xrTable接种名称1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable接种名称1.Name = "xrTable接种名称1";
            this.xrTable接种名称1.StylePriority.UseFont = false;
            this.xrTable接种名称1.StylePriority.UseTextAlignment = false;
            this.xrTable接种名称1.Text = "1";
            this.xrTable接种名称1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable接种名称1.Weight = 0.58046075568349242D;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell146.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel接种日期1});
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBorders = false;
            this.xrTableCell146.Weight = 0.58074805558978693D;
            // 
            // xrLabel接种日期1
            // 
            this.xrLabel接种日期1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel接种日期1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel接种日期1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel接种日期1.Name = "xrLabel接种日期1";
            this.xrLabel接种日期1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel接种日期1.SizeF = new System.Drawing.SizeF(124.65F, 31.59723F);
            this.xrLabel接种日期1.StylePriority.UseBorders = false;
            this.xrLabel接种日期1.StylePriority.UseFont = false;
            this.xrLabel接种日期1.StylePriority.UseTextAlignment = false;
            this.xrLabel接种日期1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell147.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel接种机构1});
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBorders = false;
            this.xrTableCell147.Weight = 1.747383976915766D;
            // 
            // xrLabel接种机构1
            // 
            this.xrLabel接种机构1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel接种机构1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel接种机构1.LocationFloat = new DevExpress.Utils.PointFloat(9.399902F, 0F);
            this.xrLabel接种机构1.Name = "xrLabel接种机构1";
            this.xrLabel接种机构1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel接种机构1.SizeF = new System.Drawing.SizeF(384.2308F, 31.59723F);
            this.xrLabel接种机构1.StylePriority.UseBorders = false;
            this.xrLabel接种机构1.StylePriority.UseFont = false;
            this.xrLabel接种机构1.StylePriority.UseTextAlignment = false;
            this.xrLabel接种机构1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148,
            this.xrTable接种名称2,
            this.xrTableCell150,
            this.xrTableCell151});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.StylePriority.UseBorders = false;
            this.xrTableRow33.Weight = 1.0000000435965033D;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell148.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.StylePriority.UseBorders = false;
            this.xrTableCell148.StylePriority.UseFont = false;
            this.xrTableCell148.Text = "规划预防";
            this.xrTableCell148.Weight = 0.384577402495473D;
            // 
            // xrTable接种名称2
            // 
            this.xrTable接种名称2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable接种名称2.Name = "xrTable接种名称2";
            this.xrTable接种名称2.StylePriority.UseFont = false;
            this.xrTable接种名称2.StylePriority.UseTextAlignment = false;
            this.xrTable接种名称2.Text = "2";
            this.xrTable接种名称2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable接种名称2.Weight = 0.58046075568349254D;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell150.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel接种日期2});
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.StylePriority.UseBorders = false;
            this.xrTableCell150.Weight = 0.58074812140124177D;
            // 
            // xrLabel接种日期2
            // 
            this.xrLabel接种日期2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel接种日期2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel接种日期2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel接种日期2.Name = "xrLabel接种日期2";
            this.xrLabel接种日期2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel接种日期2.SizeF = new System.Drawing.SizeF(124.65F, 31.25153F);
            this.xrLabel接种日期2.StylePriority.UseBorders = false;
            this.xrLabel接种日期2.StylePriority.UseFont = false;
            this.xrLabel接种日期2.StylePriority.UseTextAlignment = false;
            this.xrLabel接种日期2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell151.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel接种机构2});
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseBorders = false;
            this.xrTableCell151.Weight = 1.7473839111043112D;
            // 
            // xrLabel接种机构2
            // 
            this.xrLabel接种机构2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel接种机构2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel接种机构2.LocationFloat = new DevExpress.Utils.PointFloat(3.399933F, 0F);
            this.xrLabel接种机构2.Name = "xrLabel接种机构2";
            this.xrLabel接种机构2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel接种机构2.SizeF = new System.Drawing.SizeF(390.2307F, 31.59723F);
            this.xrLabel接种机构2.StylePriority.UseBorders = false;
            this.xrLabel接种机构2.StylePriority.UseFont = false;
            this.xrLabel接种机构2.StylePriority.UseTextAlignment = false;
            this.xrLabel接种机构2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell152,
            this.xrTable接种名称3,
            this.xrTableCell154,
            this.xrTableCell155});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.StylePriority.UseBorders = false;
            this.xrTableRow34.Weight = 1.0000000435965033D;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseFont = false;
            this.xrTableCell152.Text = "接种史";
            this.xrTableCell152.Weight = 0.38457746830692785D;
            // 
            // xrTable接种名称3
            // 
            this.xrTable接种名称3.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTable接种名称3.Name = "xrTable接种名称3";
            this.xrTable接种名称3.StylePriority.UseFont = false;
            this.xrTable接种名称3.StylePriority.UseTextAlignment = false;
            this.xrTable接种名称3.Text = "3";
            this.xrTable接种名称3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable接种名称3.Weight = 0.58046068987203758D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell154.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel接种日期3});
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseBorders = false;
            this.xrTableCell154.Weight = 0.58074805558978693D;
            // 
            // xrLabel接种日期3
            // 
            this.xrLabel接种日期3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel接种日期3.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel接种日期3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel接种日期3.Name = "xrLabel接种日期3";
            this.xrLabel接种日期3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel接种日期3.SizeF = new System.Drawing.SizeF(124.6501F, 31.25153F);
            this.xrLabel接种日期3.StylePriority.UseBorders = false;
            this.xrLabel接种日期3.StylePriority.UseFont = false;
            this.xrLabel接种日期3.StylePriority.UseTextAlignment = false;
            this.xrLabel接种日期3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell155.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel接种机构3});
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseBorders = false;
            this.xrTableCell155.Weight = 1.7473839769157662D;
            // 
            // xrLabel接种机构3
            // 
            this.xrLabel接种机构3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel接种机构3.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel接种机构3.LocationFloat = new DevExpress.Utils.PointFloat(3.399933F, 0F);
            this.xrLabel接种机构3.Name = "xrLabel接种机构3";
            this.xrLabel接种机构3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel接种机构3.SizeF = new System.Drawing.SizeF(390.2307F, 31.59729F);
            this.xrLabel接种机构3.StylePriority.UseBorders = false;
            this.xrLabel接种机构3.StylePriority.UseFont = false;
            this.xrLabel接种机构3.StylePriority.UseTextAlignment = false;
            this.xrLabel接种机构3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell156,
            this.xrTableCell157});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.StylePriority.UseBorders = false;
            this.xrTableRow35.Weight = 5.9999925105771537D;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell156.Multiline = true;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseFont = false;
            this.xrTableCell156.Text = "健\r\n康\r\n评\r\n价";
            this.xrTableCell156.Weight = 0.38457743540120037D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.xrTableCell157.Multiline = true;
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.StylePriority.UseBorders = false;
            this.xrTableCell157.StylePriority.UseTextAlignment = false;
            this.xrTableCell157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell157.Weight = 2.9085927552833177D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(7.629395E-06F, 4.577637E-05F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42});
            this.xrTable3.SizeF = new System.Drawing.SizeF(674.3752F, 189.5832F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell164,
            this.xrTableCell166});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell164.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseBorders = false;
            this.xrTableCell164.StylePriority.UseFont = false;
            this.xrTableCell164.Text = "1体检无异常";
            this.xrTableCell164.Weight = 5.9402546618521255D;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell166.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel体检有无异常});
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.Weight = 0.24307968873381292D;
            // 
            // xrLabel体检有无异常
            // 
            this.xrLabel体检有无异常.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel体检有无异常.CanGrow = false;
            this.xrLabel体检有无异常.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel体检有无异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.83574F);
            this.xrLabel体检有无异常.Name = "xrLabel体检有无异常";
            this.xrLabel体检有无异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体检有无异常.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel体检有无异常.StylePriority.UseBorders = false;
            this.xrLabel体检有无异常.StylePriority.UseFont = false;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159,
            this.xrTableCell165});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell159.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.StylePriority.UseBorders = false;
            this.xrTableCell159.StylePriority.UseFont = false;
            this.xrTableCell159.Text = "2有异常";
            this.xrTableCell159.Weight = 0.79166664123535158D;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell165.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.StylePriority.UseBorders = false;
            this.xrTableCell165.StylePriority.UseFont = false;
            this.xrTableCell165.Weight = 5.3916677093505863D;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell167,
            this.xrTableCell168});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell167.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.StylePriority.UseBorders = false;
            this.xrTableCell167.StylePriority.UseFont = false;
            this.xrTableCell167.Text = "异常1";
            this.xrTableCell167.Weight = 0.79166664123535158D;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell168.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel体检异常1});
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.StylePriority.UseBorders = false;
            this.xrTableCell168.Weight = 5.3916677093505863D;
            // 
            // xrLabel体检异常1
            // 
            this.xrLabel体检异常1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel体检异常1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel体检异常1.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 0F);
            this.xrLabel体检异常1.Name = "xrLabel体检异常1";
            this.xrLabel体检异常1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体检异常1.SizeF = new System.Drawing.SizeF(557.426F, 29F);
            this.xrLabel体检异常1.StylePriority.UseBorders = false;
            this.xrLabel体检异常1.StylePriority.UseFont = false;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell169,
            this.xrTableCell170});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell169.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.StylePriority.UseBorders = false;
            this.xrTableCell169.StylePriority.UseFont = false;
            this.xrTableCell169.Text = "异常2";
            this.xrTableCell169.Weight = 0.79166664123535158D;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell170.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel体检异常2});
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.Weight = 5.3916677093505863D;
            // 
            // xrLabel体检异常2
            // 
            this.xrLabel体检异常2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel体检异常2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel体检异常2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel体检异常2.Name = "xrLabel体检异常2";
            this.xrLabel体检异常2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体检异常2.SizeF = new System.Drawing.SizeF(557.4257F, 29F);
            this.xrLabel体检异常2.StylePriority.UseBorders = false;
            this.xrLabel体检异常2.StylePriority.UseFont = false;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171,
            this.xrTableCell172});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell171.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.StylePriority.UseBorders = false;
            this.xrTableCell171.StylePriority.UseFont = false;
            this.xrTableCell171.Text = "异常3";
            this.xrTableCell171.Weight = 0.79166664123535158D;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell172.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel体检异常3});
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.StylePriority.UseBorders = false;
            this.xrTableCell172.Weight = 5.3916677093505863D;
            // 
            // xrLabel体检异常3
            // 
            this.xrLabel体检异常3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel体检异常3.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel体检异常3.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 2.597153F);
            this.xrLabel体检异常3.Name = "xrLabel体检异常3";
            this.xrLabel体检异常3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体检异常3.SizeF = new System.Drawing.SizeF(557.426F, 29F);
            this.xrLabel体检异常3.StylePriority.UseBorders = false;
            this.xrLabel体检异常3.StylePriority.UseFont = false;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell173,
            this.xrTableCell174});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell173.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.StylePriority.UseBorders = false;
            this.xrTableCell173.StylePriority.UseFont = false;
            this.xrTableCell173.Text = "异常4";
            this.xrTableCell173.Weight = 0.79166664123535158D;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell174.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel体检异常4});
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.StylePriority.UseBorders = false;
            this.xrTableCell174.Weight = 5.3916677093505863D;
            // 
            // xrLabel体检异常4
            // 
            this.xrLabel体检异常4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel体检异常4.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel体检异常4.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 1.334518F);
            this.xrLabel体检异常4.Name = "xrLabel体检异常4";
            this.xrLabel体检异常4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel体检异常4.SizeF = new System.Drawing.SizeF(557.426F, 27F);
            this.xrLabel体检异常4.StylePriority.UseBorders = false;
            this.xrLabel体检异常4.StylePriority.UseFont = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell160,
            this.xrTableCell158,
            this.xrTableCell179,
            this.xrTableCell177,
            this.xrTableCell175,
            this.xrTableCell180,
            this.xrTableCell176,
            this.xrTableCell178,
            this.xrTableCell161,
            this.xrTableCell163});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.StylePriority.UseBorders = false;
            this.xrTableRow36.Weight = 6.9327918891340055D;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell160.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell160.Multiline = true;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.StylePriority.UseFont = false;
            this.xrTableCell160.Text = "健\r\n康\r\n指\r\n导";
            this.xrTableCell160.Weight = 0.384577402495473D;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell158.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell158.Multiline = true;
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.StylePriority.UseFont = false;
            this.xrTableCell158.StylePriority.UseTextAlignment = false;
            this.xrTableCell158.Text = "1定期随访\r\n\r\n2纳入慢性病患者健康管理\r\n\r\n3建议复查\r\n\r\n4建议转诊";
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell158.Weight = 1.0106470586171135D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel健康指导1});
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 0.056069253594324943D;
            // 
            // xrLabel健康指导1
            // 
            this.xrLabel健康指导1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel健康指导1.CanGrow = false;
            this.xrLabel健康指导1.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 172.4609F);
            this.xrLabel健康指导1.Name = "xrLabel健康指导1";
            this.xrLabel健康指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel健康指导1.SizeF = new System.Drawing.SizeF(12F, 12F);
            this.xrLabel健康指导1.StylePriority.UseBorders = false;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Multiline = true;
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 13, 100F);
            this.xrTableCell177.StylePriority.UsePadding = false;
            this.xrTableCell177.StylePriority.UseTextAlignment = false;
            this.xrTableCell177.Text = "\r\n\r\n\r\n\r\n\r\n\r\n/";
            this.xrTableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell177.Weight = 0.038817176058650107D;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel健康指导2});
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Weight = 0.056069253594324943D;
            // 
            // xrLabel健康指导2
            // 
            this.xrLabel健康指导2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel健康指导2.CanGrow = false;
            this.xrLabel健康指导2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 172.4609F);
            this.xrLabel健康指导2.Name = "xrLabel健康指导2";
            this.xrLabel健康指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel健康指导2.SizeF = new System.Drawing.SizeF(12F, 12F);
            this.xrLabel健康指导2.StylePriority.UseBorders = false;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Multiline = true;
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 12, 100F);
            this.xrTableCell180.StylePriority.UsePadding = false;
            this.xrTableCell180.StylePriority.UseTextAlignment = false;
            this.xrTableCell180.Text = "\r\n\r\n\r\n\r\n\r\n\r\n/";
            this.xrTableCell180.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell180.Weight = 0.043130195565905888D;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel健康指导3});
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Weight = 0.056069253758774362D;
            // 
            // xrLabel健康指导3
            // 
            this.xrLabel健康指导3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel健康指导3.CanGrow = false;
            this.xrLabel健康指导3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 172.4609F);
            this.xrLabel健康指导3.Name = "xrLabel健康指导3";
            this.xrLabel健康指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel健康指导3.SizeF = new System.Drawing.SizeF(12F, 12F);
            this.xrLabel健康指导3.StylePriority.UseBorders = false;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Multiline = true;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 12, 100F);
            this.xrTableCell178.StylePriority.UsePadding = false;
            this.xrTableCell178.StylePriority.UseTextAlignment = false;
            this.xrTableCell178.Text = "\r\n\r\n\r\n\r\n\r\n\r\n/";
            this.xrTableCell178.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell178.Weight = 0.033247601497088615D;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell161.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel健康指导4});
            this.xrTableCell161.Multiline = true;
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.StylePriority.UseBorders = false;
            this.xrTableCell161.StylePriority.UseTextAlignment = false;
            this.xrTableCell161.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell161.Weight = 0.056069253594324936D;
            // 
            // xrLabel健康指导4
            // 
            this.xrLabel健康指导4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel健康指导4.CanGrow = false;
            this.xrLabel健康指导4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 172.4609F);
            this.xrLabel健康指导4.Name = "xrLabel健康指导4";
            this.xrLabel健康指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel健康指导4.SizeF = new System.Drawing.SizeF(12F, 12F);
            this.xrLabel健康指导4.StylePriority.UseBorders = false;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Weight = 1.5584737419085382D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.577637E-05F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47});
            this.xrTable4.SizeF = new System.Drawing.SizeF(361.3419F, 221.2267F);
            this.xrTable4.StylePriority.UseBorders = false;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162,
            this.xrTableCell198,
            this.xrTableCell202,
            this.xrLabel危险控制因素cell,
            this.xrTableCell204,
            this.xrTableCell199,
            this.xrTableCell201,
            this.xrTableCell196,
            this.xrTableCell203,
            this.xrTableCell197,
            this.xrTableCell200,
            this.xrTableCell183});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell162.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseBorders = false;
            this.xrTableCell162.StylePriority.UseFont = false;
            this.xrTableCell162.Text = "危险因素控制：";
            this.xrTableCell162.Weight = 1.7624997693174196D;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell198.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel危险控制因素1});
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseBorders = false;
            this.xrTableCell198.Weight = 0.15044256455238944D;
            // 
            // xrLabel危险控制因素1
            // 
            this.xrLabel危险控制因素1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel危险控制因素1.CanGrow = false;
            this.xrLabel危险控制因素1.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel危险控制因素1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 9.999985F);
            this.xrLabel危险控制因素1.Name = "xrLabel危险控制因素1";
            this.xrLabel危险控制因素1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel危险控制因素1.SizeF = new System.Drawing.SizeF(14F, 18.62476F);
            this.xrLabel危险控制因素1.StylePriority.UseBorders = false;
            this.xrLabel危险控制因素1.StylePriority.UseFont = false;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 10, 0, 100F);
            this.xrTableCell202.StylePriority.UseBorders = false;
            this.xrTableCell202.StylePriority.UsePadding = false;
            this.xrTableCell202.Text = "/";
            this.xrTableCell202.Weight = 0.15044256802397282D;
            // 
            // xrLabel危险控制因素cell
            // 
            this.xrLabel危险控制因素cell.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel危险控制因素cell.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel危险控制因素2});
            this.xrLabel危险控制因素cell.Name = "xrLabel危险控制因素cell";
            this.xrLabel危险控制因素cell.StylePriority.UseBorders = false;
            this.xrLabel危险控制因素cell.Weight = 0.15044256802397285D;
            // 
            // xrLabel危险控制因素2
            // 
            this.xrLabel危险控制因素2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel危险控制因素2.CanGrow = false;
            this.xrLabel危险控制因素2.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel危险控制因素2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 9.999985F);
            this.xrLabel危险控制因素2.Name = "xrLabel危险控制因素2";
            this.xrLabel危险控制因素2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel危险控制因素2.SizeF = new System.Drawing.SizeF(14F, 18.62476F);
            this.xrLabel危险控制因素2.StylePriority.UseBorders = false;
            this.xrLabel危险控制因素2.StylePriority.UseFont = false;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 10, 0, 100F);
            this.xrTableCell204.StylePriority.UseBorders = false;
            this.xrTableCell204.StylePriority.UsePadding = false;
            this.xrTableCell204.Text = "/";
            this.xrTableCell204.Weight = 0.15044256615399532D;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell199.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel危险控制因素3});
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.StylePriority.UseBorders = false;
            this.xrTableCell199.Weight = 0.15044256615399532D;
            // 
            // xrLabel危险控制因素3
            // 
            this.xrLabel危险控制因素3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel危险控制因素3.CanGrow = false;
            this.xrLabel危险控制因素3.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel危险控制因素3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 9.999802F);
            this.xrLabel危险控制因素3.Name = "xrLabel危险控制因素3";
            this.xrLabel危险控制因素3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel危险控制因素3.SizeF = new System.Drawing.SizeF(14F, 18.62476F);
            this.xrLabel危险控制因素3.StylePriority.UseBorders = false;
            this.xrLabel危险控制因素3.StylePriority.UseFont = false;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 10, 0, 100F);
            this.xrTableCell201.StylePriority.UseBorders = false;
            this.xrTableCell201.StylePriority.UsePadding = false;
            this.xrTableCell201.Text = "/";
            this.xrTableCell201.Weight = 0.15044258528375779D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell196.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel危险控制因素4});
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.StylePriority.UseBorders = false;
            this.xrTableCell196.Weight = 0.15044258528375765D;
            // 
            // xrLabel危险控制因素4
            // 
            this.xrLabel危险控制因素4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel危险控制因素4.CanGrow = false;
            this.xrLabel危险控制因素4.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel危险控制因素4.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 9.999741F);
            this.xrLabel危险控制因素4.Name = "xrLabel危险控制因素4";
            this.xrLabel危险控制因素4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel危险控制因素4.SizeF = new System.Drawing.SizeF(14F, 18.62476F);
            this.xrLabel危险控制因素4.StylePriority.UseBorders = false;
            this.xrLabel危险控制因素4.StylePriority.UseFont = false;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 10, 0, 100F);
            this.xrTableCell203.StylePriority.UseBorders = false;
            this.xrTableCell203.StylePriority.UsePadding = false;
            this.xrTableCell203.Text = "/";
            this.xrTableCell203.Weight = 0.15044258528375767D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell197.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel危险控制因素5});
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.StylePriority.UseBorders = false;
            this.xrTableCell197.Weight = 0.15044256615399543D;
            // 
            // xrLabel危险控制因素5
            // 
            this.xrLabel危险控制因素5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel危险控制因素5.CanGrow = false;
            this.xrLabel危险控制因素5.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel危险控制因素5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 9.999985F);
            this.xrLabel危险控制因素5.Name = "xrLabel危险控制因素5";
            this.xrLabel危险控制因素5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel危险控制因素5.SizeF = new System.Drawing.SizeF(14F, 18.62476F);
            this.xrLabel危险控制因素5.StylePriority.UseBorders = false;
            this.xrLabel危险控制因素5.StylePriority.UseFont = false;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 10, 0, 100F);
            this.xrTableCell200.StylePriority.UseBorders = false;
            this.xrTableCell200.StylePriority.UsePadding = false;
            this.xrTableCell200.Text = "/";
            this.xrTableCell200.Weight = 0.15044256615399543D;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel危险控制因素6});
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Weight = 0.38724137973217831D;
            // 
            // xrLabel危险控制因素6
            // 
            this.xrLabel危险控制因素6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel危险控制因素6.CanGrow = false;
            this.xrLabel危险控制因素6.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel危险控制因素6.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 9.999985F);
            this.xrLabel危险控制因素6.Name = "xrLabel危险控制因素6";
            this.xrLabel危险控制因素6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel危险控制因素6.SizeF = new System.Drawing.SizeF(14F, 18.62476F);
            this.xrLabel危险控制因素6.StylePriority.UseBorders = false;
            this.xrLabel危险控制因素6.StylePriority.UseFont = false;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell186});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseFont = false;
            this.xrTableCell186.Text = "1戒烟 2健康饮酒 3饮食 4锻炼";
            this.xrTableCell186.Weight = 3.6541668701171877D;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell189});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell187.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.StylePriority.UseBorders = false;
            this.xrTableCell187.StylePriority.UseFont = false;
            this.xrTableCell187.Text = "5减体重（目标";
            this.xrTableCell187.Weight = 1.7624997200526009D;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell188.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel减体重目标});
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseBorders = false;
            this.xrTableCell188.Weight = 1.6848958206611815D;
            // 
            // xrLabel减体重目标
            // 
            this.xrLabel减体重目标.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel减体重目标.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrLabel减体重目标.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.0001029968F);
            this.xrLabel减体重目标.Name = "xrLabel减体重目标";
            this.xrLabel减体重目标.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel减体重目标.SizeF = new System.Drawing.SizeF(156.6982F, 31F);
            this.xrLabel减体重目标.StylePriority.UseBorders = false;
            this.xrLabel减体重目标.StylePriority.UseFont = false;
            this.xrLabel减体重目标.StylePriority.UseTextAlignment = false;
            this.xrLabel减体重目标.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Text = "）";
            this.xrTableCell189.Weight = 0.20677132940340504D;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell190,
            this.xrTableCell191,
            this.xrTableCell192});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell190.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseBorders = false;
            this.xrTableCell190.StylePriority.UseFont = false;
            this.xrTableCell190.Text = "6建议接种疫苗";
            this.xrTableCell190.Weight = 1.7624995674647361D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell191.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel建议接种疫苗});
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.StylePriority.UseBorders = false;
            this.xrTableCell191.Weight = 1.68489963535781D;
            // 
            // xrLabel建议接种疫苗
            // 
            this.xrLabel建议接种疫苗.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel建议接种疫苗.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrLabel建议接种疫苗.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel建议接种疫苗.Name = "xrLabel建议接种疫苗";
            this.xrLabel建议接种疫苗.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel建议接种疫苗.SizeF = new System.Drawing.SizeF(156.6983F, 31F);
            this.xrLabel建议接种疫苗.StylePriority.UseBorders = false;
            this.xrLabel建议接种疫苗.StylePriority.UseFont = false;
            this.xrLabel建议接种疫苗.StylePriority.UseTextAlignment = false;
            this.xrLabel建议接种疫苗.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 0.20676766729464169D;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell193,
            this.xrTableCell195});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1.8345514208946798D;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell193.Font = new System.Drawing.Font("仿宋", 14.25F);
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.StylePriority.UseBorders = false;
            this.xrTableCell193.StylePriority.UseFont = false;
            this.xrTableCell193.Text = "7其他";
            this.xrTableCell193.Weight = 1D;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel危险控制因素其他});
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Weight = 2.6541668701171877D;
            // 
            // xrLabel危险控制因素其他
            // 
            this.xrLabel危险控制因素其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel危险控制因素其他.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrLabel危险控制因素其他.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 3.541546F);
            this.xrLabel危险控制因素其他.Name = "xrLabel危险控制因素其他";
            this.xrLabel危险控制因素其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel危险控制因素其他.SizeF = new System.Drawing.SizeF(242.0107F, 62.28375F);
            this.xrLabel危险控制因素其他.StylePriority.UseBorders = false;
            this.xrLabel危险控制因素其他.StylePriority.UseFont = false;
            this.xrLabel危险控制因素其他.StylePriority.UseTextAlignment = false;
            this.xrLabel危险控制因素其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.BorderWidth = 2F;
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(1264.208F, 29.16335F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(15.62512F, 19.08621F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseBorderWidth = false;
            this.xrLabel12.StylePriority.UseFont = false;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel14.BorderWidth = 2F;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(1286.063F, 29.16335F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(15.62512F, 19.08621F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseBorderWidth = false;
            this.xrLabel14.StylePriority.UseFont = false;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel13.BorderWidth = 2F;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(1307.917F, 29.16335F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(15.62512F, 19.08621F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseBorderWidth = false;
            this.xrLabel13.StylePriority.UseFont = false;
            // 
            // xrLine1
            // 
            this.xrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine1.BorderWidth = 2F;
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(1327.083F, 29.16335F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(15F, 19.08621F);
            this.xrLine1.StylePriority.UseBorders = false;
            this.xrLine1.StylePriority.UseBorderWidth = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.BorderWidth = 2F;
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(1342.083F, 29.16335F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(15.62512F, 19.08621F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseBorderWidth = false;
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.BorderWidth = 2F;
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(1367.24F, 29.16335F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(15.62512F, 19.08621F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseBorderWidth = false;
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.BorderWidth = 2F;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(1392.396F, 29.16335F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(15.62512F, 19.08621F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseBorderWidth = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.BorderWidth = 2F;
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(1417.552F, 29.16335F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(15.62512F, 19.08621F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseBorderWidth = false;
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.BorderWidth = 2F;
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(1442.709F, 29.16335F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(15.62512F, 19.08621F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseBorderWidth = false;
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 18F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 17F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel坚持锻炼时间
            // 
            this.xrLabel坚持锻炼时间.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel坚持锻炼时间.LocationFloat = new DevExpress.Utils.PointFloat(406.025F, 26.03001F);
            this.xrLabel坚持锻炼时间.Name = "xrLabel坚持锻炼时间";
            this.xrLabel坚持锻炼时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel坚持锻炼时间.SizeF = new System.Drawing.SizeF(78.76648F, 26F);
            this.xrLabel坚持锻炼时间.StylePriority.UseBorders = false;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel15.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(1.159119F, 2.529041F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(702F, 22F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "1无症状 2头痛 3头晕 4心悸 5胸闷 6胸痛 7慢性咳嗽 8咳痰 9呼吸困难 10多饮";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel16.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(1.159119F, 24.52904F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(702F, 22F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "11多尿 12体重下降 13乏力 14关节肿痛 15视力模糊 16手脚麻木 17尿急 18尿痛";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel17.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(1.988159F, 46.52904F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(493.5912F, 23F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "19便秘 20腹泻 21恶心呕吐 22眼花 23耳鸣 24乳房胀痛 25其他";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 1.144409E-05F);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(344.1168F, 45F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "1粗筛阴性\r\n2粗筛阳性，简易智力状态检查，总分";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.144409E-05F);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(344.1168F, 45.00003F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "1粗筛阴性\r\n2粗筛阳性，老年人抑郁评分检查，总分";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel老年人情感状态分
            // 
            this.xrLabel老年人情感状态分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel老年人情感状态分.LocationFloat = new DevExpress.Utils.PointFloat(344.8807F, 4.99995F);
            this.xrLabel老年人情感状态分.Name = "xrLabel老年人情感状态分";
            this.xrLabel老年人情感状态分.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel老年人情感状态分.SizeF = new System.Drawing.SizeF(98.42871F, 34.74F);
            this.xrLabel老年人情感状态分.StylePriority.UseBorders = false;
            this.xrLabel老年人情感状态分.StylePriority.UseTextAlignment = false;
            this.xrLabel老年人情感状态分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrLabel20.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.722046E-05F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(132.7F, 26.03111F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UsePadding = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "锻炼频率";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrLabel21.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(132.7007F, 0F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(352.0908F, 26F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UsePadding = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "1每天 2每周一次以上 3偶尔 4不锻炼";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel22.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26.03119F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(132.7F, 26.03111F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UsePadding = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "每次锻炼时间";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel23.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(192.7009F, 26.03116F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(74.2F, 26.03111F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UsePadding = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "分钟";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel24.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(266.9009F, 26.03116F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(139F, 26.03111F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UsePadding = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "坚持锻炼时间";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel25.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(484.7915F, 26.03111F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(68.5F, 26.03111F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UsePadding = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "年";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel26.Font = new System.Drawing.Font("仿宋", 12.25F);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(0F, 52.0623F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(132.7F, 26.03111F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UsePadding = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "锻炼方式";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // report健康体检表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(39, 42, 18, 17);
            this.PageHeight = 1169;
            this.PageWidth = 1654;
            this.PaperKind = System.Drawing.Printing.PaperKind.A3;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人健康状态自我评估;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人生活自理能力评估;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人认知功能分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人认知功能;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人情感状态;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTable住院史日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRLabel xrLabel住院史原因1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel住院史医疗机构1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRLabel xrLabel住院史病案号1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTable住院史日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRLabel xrLabel住院史原因2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel住院史医疗机构2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel住院史病案号2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTable家庭病床史日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRLabel xrTable家庭病床史原因1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRLabel xrTable家庭病床史医疗机构1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRLabel xrTable家庭病床史病案号1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTable家庭病床史日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRLabel xrTable家庭病床史原因2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRLabel xrTable家庭病床史医疗机构2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRLabel xrTable家庭病床史病案号2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTable用药名称1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTable用药名称2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTable用药名称3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTable用药名称4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTable用药名称5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTable用药名称6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTable接种名称1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTable接种名称2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell xrTable接种名称3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体检有无异常;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体检异常1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体检异常2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体检异常3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRLabel xrLabel体检异常4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRLabel xrLabel健康指导1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRLabel xrLabel健康指导2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRLabel xrLabel健康指导3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRLabel xrLabel健康指导4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRLabel xrLabel危险控制因素1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel危险控制因素cell;
        private DevExpress.XtraReports.UI.XRLabel xrLabel危险控制因素2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRLabel xrLabel危险控制因素3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRLabel xrLabel危险控制因素4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRLabel xrLabel危险控制因素5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRLabel xrLabel危险控制因素6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRLabel xrLabel减体重目标;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRLabel xrLabel建议接种疫苗;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRLabel xrLabel危险控制因素其他;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用量2;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用法1;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用法2;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药时间3;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用法4;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用法5;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用法6;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用量1;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药时间1;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药依从性1;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药时间2;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药依从性2;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用法3;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用量3;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药依从性3;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用量4;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药时间4;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药依从性4;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用量5;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药时间5;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药依从性5;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药用量6;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药时间6;
        private DevExpress.XtraReports.UI.XRLabel xrTable用药依从性6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel接种日期1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel接种机构1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel接种日期2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel接种机构2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel接种日期3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel接种机构3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel锻炼频率;
        private DevExpress.XtraReports.UI.XRLabel xrLabel每次锻炼时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel锻炼方式;
        private DevExpress.XtraReports.UI.XRLabel xrLabel饮食习惯1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel饮食习惯2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel饮食习惯3;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRLabel xrLabel日吸烟量;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRLabel xrLabel开始吸烟年龄;
        private DevExpress.XtraReports.UI.XRLabel xrLabel戒烟年龄;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRLabel xrLabel饮酒频率;
        private DevExpress.XtraReports.UI.XRLabel xrLabel吸烟状况;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRLabel xrLabel日饮酒量;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRLabel xrLabel戒酒年龄;
        private DevExpress.XtraReports.UI.XRLabel xrLabel是否戒酒;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRLabel xrLabel开始饮酒年龄;
        private DevExpress.XtraReports.UI.XRLabel xrLabel近一年是否醉酒;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRLabel xrLabel饮酒种类其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel饮酒种类1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel饮酒种类3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel饮酒种类4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel饮酒种类2;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRLabel xrLabel工种;
        private DevExpress.XtraReports.UI.XRLabel xrLabel从业时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel职业病有无;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRLabel xrLabel粉尘;
        private DevExpress.XtraReports.UI.XRLabel xrLabel粉尘有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRLabel xrLabel放射物质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel粉尘防护有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel放射物质有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel放射物质防护措施有无;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRLabel xrLabel物理因素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel化学因素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel毒物其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel物理因素有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel物理因素防护措施有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel化学因素有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel化学因素防护措施有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel毒物其他有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel毒物其他防护措施有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel138;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel146;
        private DevExpress.XtraReports.UI.XRLabel xrLabel145;
        private DevExpress.XtraReports.UI.XRLabel xrLabel144;
        private DevExpress.XtraReports.UI.XRLabel xrLabel143;
        private DevExpress.XtraReports.UI.XRLabel xrLabel142;
        private DevExpress.XtraReports.UI.XRLabel xrLabel141;
        private DevExpress.XtraReports.UI.XRLabel xrLabel140;
        private DevExpress.XtraReports.UI.XRLabel xrLabel139;
        private DevExpress.XtraReports.UI.XRLabel xrLabel症状其他;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel体检日期年;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel体检日期月;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel体检日期日;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel责任医生;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel体温;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel脉率;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel呼吸频率;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血压左侧1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血压左侧2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel122;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血压右侧2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel123;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血压右侧1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel身高;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel体重;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel腰围;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel体重指数;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel坚持锻炼时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel老年人情感状态分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
    }
}
