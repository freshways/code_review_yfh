﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Library;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC食盐摄入情况表 : UserControlBase
    {
        DataRow[] _dr个人档案信息 = null;
        string _ID = "";
        public UC食盐摄入情况表()
        {
            InitializeComponent();
        }
        public UC食盐摄入情况表(DataRow[] dr, UpdateType _UpdateType, object ID)
        {
            InitializeComponent();

            base._UpdateType = _UpdateType;
            _dr个人档案信息 = dr;
            _ID = ID == null ? "" : ID.ToString();
            _BLL = new bll健康档案_食盐情况();


            //默认绑定
            txt个人档案号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = dr[0][tb_健康档案.姓名].ToString();
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
            this.txt医生签名.Text = Loginer.CurrentUser.AccountName;//医生签名默认为登录人姓名

        }
        
        private void UC高血压高危干预_Load(object sender, EventArgs e)
        {
            if (_UpdateType == UpdateType.Add)
            {
                _BLL.GetBusinessByKey("-", true);//下载一个空业务单据            
                _BLL.NewBusiness(); //增加一条主表记录
                _BLL.DataBinder.Rows[0][tb_健康档案_食盐情况.个人档案编号] = _dr个人档案信息[0][tb_健康档案.个人档案编号].ToString();
                //绑定联系电话
                string str联系电话 = _dr个人档案信息[0][tb_健康档案.本人电话].ToString();
                
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                if (_ID != null && _ID != "")
                {
                    ((bll健康档案_食盐情况)_BLL).GetBusinessByKeyEdit(_ID, true);
                }
                else return;
            }

            DoBindingSummaryEditor(_BLL.CurrentBusiness.Tables[tb_健康档案_食盐情况.__TableName]);
        }

        #region 保存相关
        private void btn保存_Click(object sender, EventArgs e)
        {
            UpdateLastControl();
            if (!Msg.AskQuestion("信息保存后，‘随访日期‘将不允许修改，确认保存信息？")) return;
            if (_UpdateType == UpdateType.None) return;

            if (!ValidatingSummaryData()) return; //检查主表数据合法性

            //设置颜色
            //Set考核项颜色_new(Layout1, lab考核项);

            _BLL.DataBinder.Rows[0][tb_健康档案_食盐情况.随访日期] = this.txt随访日期.Text;
            //根据用户要求，下次随访时间默认“发生时间”的3个月以后
            if (string.IsNullOrEmpty(txt下次随访时间.Text))
            {
                string str发生时间 = this.txt随访日期.Text;
                this.txt下次随访时间.Text = Convert.ToDateTime(str发生时间).AddMonths(3).ToShortDateString();
                _BLL.DataBinder.Rows[0][tb_健康档案_食盐情况.下次随访日期] = this.txt下次随访时间.Text;
            }
            else
            {
                _BLL.DataBinder.Rows[0][tb_健康档案_食盐情况.下次随访日期] = this.txt下次随访时间.Text;
            }


            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                Msg.ShowInformation("保存成功!");

                this._UpdateType = UpdateType.None; // 最后情况操作状态
                //保存后跳转到显示页面
                UC食盐摄入情况表_显示 control = new UC食盐摄入情况表_显示(_dr个人档案信息, this.lbl创建时间.Text);
                ShowControl(control, DockStyle.Fill);
            }
            else
                Msg.Warning("保存失败!");
        }

        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {
            if (string.IsNullOrEmpty(ConvertEx.ToString(txt随访日期.Text)))
            {
                Msg.Warning("随访日期不能为空!");
                txt随访日期.Focus();
                return false;
            }

            #region 下次随访时间默认为发生时间的3个月以后不用判断
            //if (string.IsNullOrEmpty(ConvertEx.ToString(txt下次随访时间.Text)))
            //{
            //    Msg.Warning("下次随访日期不能为空!");
            //    txt下次随访时间.Focus();
            //    return false;
            //}
            #endregion

            //if (this.txt随访日期.DateTime > Convert.ToDateTime(lbl创建时间.Text))
            //{
            //    Msg.Warning("随访日期不能大于填写日期!");
            //    txt随访日期.Focus();
            //    return false;
            //}

            return true;
        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;

            dataSource.Rows[0][tb_健康档案_食盐情况.个人档案编号] = txt个人档案号.Text;

            //家庭膳食
            DataBinder.BindingTextEdit(txt家庭膳食1.Txt1, dataSource, tb_健康档案_食盐情况.家庭膳食1);
            DataBinder.BindingTextEdit(txt家庭膳食11.Txt1, dataSource, tb_健康档案_食盐情况.家庭膳食1_1);
            DataBinder.BindingTextEdit(txt家庭膳食2.Txt1, dataSource, tb_健康档案_食盐情况.家庭膳食2);
            DataBinder.BindingTextEdit(txt家庭膳食3.Txt1, dataSource, tb_健康档案_食盐情况.家庭膳食3);

            DataBinder.BindingTextEditDateTime(txt随访日期, dataSource, tb_健康档案_食盐情况.随访日期);
            DataBinder.BindingRadioEdit(radio随访方式, dataSource, tb_健康档案_食盐情况.随访方式);

            DataBinder.BindingTextEditDateTime(txt下次随访时间, dataSource, tb_健康档案_食盐情况.下次随访日期);
            DataBinder.BindingTextEdit(txt医生签名, dataSource, tb_健康档案_食盐情况.随访医生签名);
            

            //非编辑项
            this.lab当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_健康档案_食盐情况.所属机构].ToString());
            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_健康档案_食盐情况.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_健康档案_食盐情况.创建人].ToString());
            this.lbl创建时间.Text = dataSource.Rows[0][tb_健康档案_食盐情况.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_健康档案_食盐情况.修改人].ToString());
            this.lbl最近更新时间.Text = dataSource.Rows[0][tb_健康档案_食盐情况.修改时间].ToString();
        }
        #endregion

        private void btn取消_Click(object sender, EventArgs e)
        {
            UC食盐摄入情况表_显示 control = new UC食盐摄入情况表_显示(_dr个人档案信息, _UpdateType == UpdateType.Add ? null : this.lbl创建时间.Text);
            ShowControl(control, DockStyle.Fill);
        }
    }
}
