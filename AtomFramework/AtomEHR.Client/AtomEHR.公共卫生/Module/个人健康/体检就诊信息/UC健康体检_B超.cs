﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.健康档案.辖区健康档案;
using DevExpress.XtraReports.UI;
using AtomEHR.Interfaces;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class UC健康体检_B超 : UserControlBase
    {
        public UC健康体检_B超()
        {
            InitializeComponent();
        }

        public UC健康体检_B超(string _xm, string _danh, string _bc, string _bcyc, string _tjrq, UpdateType _UpdateType)
        {
            base._UpdateType = _UpdateType; //设置状态 修改/新增   

            _BLL = new bll健康体检_B超();
            InitializeComponent();

            lbl个人档案号.Text = _danh;
            this.lbl姓名.Text = _xm;
            this.lblB超.Text = _bc == "1" ? "正常" : "异常";
            this.lblB超异常.Text = _bcyc;
            this.txt体检日期.Text = _tjrq;
        }

        private void UC健康体检_B超_Load(object sender, EventArgs e)
        {
            ((bll健康体检_B超)_BLL).GetBusinessByDate(lbl个人档案号.Text, this.txt体检日期.Text, true);

            if (_BLL.CurrentBusiness.Tables[tb_健康体检_B超.__TableName] == null || _BLL.CurrentBusiness.Tables[tb_健康体检_B超.__TableName].Rows.Count <= 0)
            {
                _UpdateType = UpdateType.Add;
                _BLL.GetBusinessByKey("-", true);//下载一个空业务单据            
                _BLL.NewBusiness(); //增加一条主表记录   

                _BLL.CurrentBusiness.Tables[tb_健康体检_B超.__TableName].Rows[0][tb_健康体检_B超.个人档案编号] = lbl个人档案号.Text;
                _BLL.CurrentBusiness.Tables[tb_健康体检_B超.__TableName].Rows[0][tb_健康体检_B超.检查日期] = this.txt体检日期.Text;
            }
            else
            {
                _UpdateType = UpdateType.Modify;
            }

            DoBindingSummaryEditor(_BLL.CurrentBusiness.Tables[tb_健康体检_B超.__TableName]);
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            if (!ValidatingSummaryData()) return; //检查主表数据合法性

            if (_UpdateType == UpdateType.None) return; //操作状态是空直接返回
            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据

                //((bll健康体检_B超)BLL).Set个人健康特征(lbl个人档案号.Text);
                this._UpdateType = UpdateType.None;
                Msg.ShowInformation("保存成功!");
                //关闭主窗体
                Form f = this.Parent as Form;
                if (f != null)
                    f.Close();
            }
            else
                Msg.Warning("保存失败!");
        }

        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {
            if (string.IsNullOrEmpty(ConvertEx.ToString(txt体检日期.Text)))
            {
                Msg.Warning("接诊时间不能为空!");
                txt体检日期.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null || dataSource.Rows.Count <= 0) return;
            
            DataBinder.BindingTextEdit(txt体检日期, dataSource, tb_健康体检_B超.检查日期);
            DataBinder.BindingTextEdit(txt备注, dataSource, tb_健康体检_B超.备注);
            DataBinder.BindingTextEdit(txt超声所见, dataSource, tb_健康体检_B超.超声所见);
            DataBinder.BindingTextEdit(txt超声提示, dataSource, tb_健康体检_B超.超声提示);

            //非编辑项
            this.lbl当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_健康体检_B超.所属机构].ToString());
            this.lbl创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_健康体检_B超.所属机构].ToString());
            this.lbl创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_健康体检_B超.创建人].ToString());
            this.lbl创建时间.Text = dataSource.Rows[0][tb_健康体检_B超.创建日期].ToString();
        }

        private void btn选择模板_Click(object sender, EventArgs e)
        {
            bllB超报告模板 bll_B超模板 = new bllB超报告模板();
            //打开通用查询窗体
            frmFuzzySearch.Execute(sender as ButtonEdit, bll_B超模板 as IFuzzySearchSupportable, this.SearchStockCallBack);
        }
        /// <summary>
        /// 通用查询回调方法
        /// </summary>        
        private void SearchStockCallBack(DataRow resultRow)
        {
            if (resultRow == null) return;

            this.txt超声所见.Text = ConvertEx.ToString(resultRow[tb_健康体检_B超.超声所见]);
            this.txt超声提示.Text = ConvertEx.ToString(resultRow[tb_健康体检_B超.超声提示]);
        } 

    }
}
