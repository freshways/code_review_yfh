﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class report健康体检表A4_2_2017 : DevExpress.XtraReports.UI.XtraReport
    {
        //Begin WXF 2018-11-03 | 19:00
        //健康体检表A4版界面与代码均为复制A3版稍作修改

        //End

        DataSet ds体检;
        bll健康体检 bll = new bll健康体检();
        public report健康体检表A4_2_2017()
        {
            InitializeComponent();
        }

        public report健康体检表A4_2_2017(string docNo, string date, DataSet _dsTJ = null)
        {
            InitializeComponent();
            if (_dsTJ == null)
            {
                ds体检 = bll.GetReportDataByKey(docNo, date);
            }
            else
            {
                ds体检 = _dsTJ;
            }

            BindData(ds体检);
            try
            {
                bll医生信息 bll手签 = new bll医生信息();
                //string RGID = ds体检.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构].ToString();
                DataTable dt = bll手签.GetSummaryData(false);
                if (dt.Select("c查体模块='脏器功能' ").Length > 0)
                {
                    System.Drawing.Image img脏器功能 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='脏器功能' ")[0]["s手签"]);
                    this.xrPictureBox脏器功能.Image = img脏器功能;
                }
                if (dt.Select("c查体模块='一般体格检查' ").Length > 0)
                {
                    System.Drawing.Image img一般体格检查 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='一般体格检查' ")[0]["s手签"]);
                    this.xrPictureBox一般体格检查.Image = img一般体格检查;
                }
                if (dt.Select("c查体模块='辅助检查' ").Length > 0)
                {
                    System.Drawing.Image img辅助检查 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='辅助检查' ")[0]["s手签"]);
                    this.xrPictureBox辅助检查.Image = img辅助检查;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void BindData(DataSet ds体检)
        {
            DataTable tb健康体检 = ds体检.Tables[tb_健康体检.__TableName];
            DataTable tb健康状态 = ds体检.Tables[tb_健康档案_健康状态.__TableName];

            if (tb健康体检.Rows.Count == 1)
            {
                //口腔
                this.xrLabel_口唇.Text = tb健康体检.Rows[0][tb_健康体检.口唇].ToString();
                //齿列
                string 齿列 = tb健康体检.Rows[0][tb_健康体检.齿列].ToString();
                string[] 齿列s = 齿列.Split(',');
                for (int i = 0; i < 齿列s.Length; i++)
                {
                    if (string.IsNullOrEmpty(齿列s[i]))
                    {
                        continue;
                    }

                    if (i > 1) break;

                    string xrName = "xrLabel_齿列" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = 齿列s[i];
                }
                if (tb健康体检.Rows[0][tb_健康体检.齿列缺齿] != null && !string.IsNullOrEmpty(tb健康体检.Rows[0][tb_健康体检.齿列缺齿].ToString()))
                {
                    string[] 缺齿 = tb健康体检.Rows[0][tb_健康体检.齿列缺齿].ToString().Split('@');

                    this.xrTable_缺齿1.Text = 缺齿[0];
                    this.xrTable_缺齿2.Text = 缺齿[1];
                    this.xrTable_缺齿3.Text = 缺齿[2];
                    this.xrTable_缺齿4.Text = 缺齿[3];
                }
                if (tb健康体检.Rows[0][tb_健康体检.齿列龋齿] != null && !string.IsNullOrEmpty(tb健康体检.Rows[0][tb_健康体检.齿列龋齿].ToString()))
                {
                    string[] 龋齿 = tb健康体检.Rows[0][tb_健康体检.齿列龋齿].ToString().Split('@');

                    this.xrTable_龋齿1.Text = 龋齿[0];
                    this.xrTable_龋齿2.Text = 龋齿[1];
                    this.xrTable_龋齿3.Text = 龋齿[2];
                    this.xrTable_龋齿4.Text = 龋齿[3];
                }
                if (tb健康体检.Rows[0][tb_健康体检.齿列义齿] != null && !string.IsNullOrEmpty(tb健康体检.Rows[0][tb_健康体检.齿列义齿].ToString()))
                {
                    string[] 义齿 = tb健康体检.Rows[0][tb_健康体检.齿列义齿].ToString().Split('@');

                    this.xrTable_义齿1.Text = 义齿[0];
                    this.xrTable_义齿2.Text = 义齿[1];
                    this.xrTable_义齿3.Text = 义齿[2];
                    this.xrTable_义齿4.Text = 义齿[3];
                }
                //咽部
                this.xrLabel_咽部.Text = tb健康体检.Rows[0][tb_健康体检.咽部].ToString();
                //视力
                this.xrLabel_视力左眼.Text = tb健康体检.Rows[0][tb_健康体检.左眼视力].ToString();
                this.xrLabel_视力右眼.Text = tb健康体检.Rows[0][tb_健康体检.右眼视力].ToString();
                this.xrLabel_矫正视力左眼.Text = tb健康体检.Rows[0][tb_健康体检.左眼矫正].ToString();
                this.xrLabel_矫正视力右眼.Text = tb健康体检.Rows[0][tb_健康体检.右眼矫正].ToString();
                //听力
                this.xrLabel_听力.Text = tb健康体检.Rows[0][tb_健康体检.听力].ToString();
                //运动能力
                this.xrLabel_运动能力.Text = tb健康体检.Rows[0][tb_健康体检.运动功能].ToString();
                //眼底
                string 眼底 = tb健康体检.Rows[0][tb_健康体检.眼底].ToString();
                this.xrLabel_眼底.Text = 眼底 == "3" ? "" : 眼底;
                this.xrLabel_眼底异常.Text = tb健康体检.Rows[0][tb_健康体检.眼底异常].ToString();
                //皮肤
                string 皮肤 = tb健康体检.Rows[0][tb_健康体检.皮肤].ToString();
                string[] 皮肤s = 皮肤.Split(',');
                this.xrLabel_皮肤.Text = 皮肤s[0] == "99" ? "7" : 皮肤s[0];
                this.xrLabel_皮肤其他.Text = tb健康体检.Rows[0][tb_健康体检.皮肤其他].ToString();
                //巩膜
                string 巩膜 = tb健康体检.Rows[0][tb_健康体检.巩膜].ToString();
                string[] 巩膜s = 巩膜.Split(',');
                this.xrLabel_巩膜.Text = 巩膜s[0] == "99" ? "4" : 巩膜s[0];
                this.xrLabel_巩膜其他.Text = tb健康体检.Rows[0][tb_健康体检.巩膜其他].ToString();
                //淋巴结
                string 淋巴结 = tb健康体检.Rows[0][tb_健康体检.淋巴结].ToString();
                string[] 淋巴结s = 淋巴结.Split(',');
                this.xrLabel_淋巴结.Text = 淋巴结s[0];
                this.xrLabel_淋巴结其他.Text = tb健康体检.Rows[0][tb_健康体检.淋巴结其他].ToString();
                //桶状胸
                this.xrLabel_桶状胸.Text = tb健康体检.Rows[0][tb_健康体检.桶状胸].ToString() == "2" ? "1" : "2";
                //呼吸音
                this.xrLabel_呼吸音.Text = tb健康体检.Rows[0][tb_健康体检.呼吸音].ToString();
                this.xrLabel_呼吸音异常.Text = tb健康体检.Rows[0][tb_健康体检.呼吸音异常].ToString();
                //罗音
                string 罗音 = tb健康体检.Rows[0][tb_健康体检.罗音].ToString();
                string[] 罗音s = 罗音.Split(',');
                this.xrLabel_罗音.Text = 罗音s[0];
                this.xrLabel_罗音其他.Text = tb健康体检.Rows[0][tb_健康体检.罗音异常].ToString();
                //心率
                this.xrLabel_心率.Text = tb健康体检.Rows[0][tb_健康体检.心率].ToString();
                //心律
                this.xrLabel_心律.Text = tb健康体检.Rows[0][tb_健康体检.心律].ToString();
                //杂音
                this.xrLabel_杂音.Text = tb健康体检.Rows[0][tb_健康体检.杂音].ToString();
                this.xrLabel_杂音有.Text = tb健康体检.Rows[0][tb_健康体检.杂音有].ToString();
                //压痛
                this.xrLabel_压痛.Text = tb健康体检.Rows[0][tb_健康体检.压痛].ToString();
                this.xrLabel_压痛有.Text = tb健康体检.Rows[0][tb_健康体检.压痛有].ToString();
                //包块
                this.xrLabel_包块.Text = tb健康体检.Rows[0][tb_健康体检.包块].ToString();
                this.xrLabel_包块有.Text = tb健康体检.Rows[0][tb_健康体检.包块有].ToString();
                //肝大
                this.xrLabel_肝大.Text = tb健康体检.Rows[0][tb_健康体检.肝大].ToString();
                this.xrLabel_肝大有.Text = tb健康体检.Rows[0][tb_健康体检.肝大有].ToString();
                //脾大
                this.xrLabel_脾大.Text = tb健康体检.Rows[0][tb_健康体检.脾大].ToString();
                this.xrLabel_脾大有.Text = tb健康体检.Rows[0][tb_健康体检.脾大有].ToString();
                //移动性浊音
                this.xrLabel_移动性浊音.Text = tb健康体检.Rows[0][tb_健康体检.浊音].ToString();
                this.xrLabel_移动性浊音有.Text = tb健康体检.Rows[0][tb_健康体检.浊音有].ToString();
                //下肢水肿
                this.xrLabel_下肢水肿.Text = tb健康体检.Rows[0][tb_健康体检.下肢水肿].ToString();
                //足背动脉搏动
                this.xrLabel_足背动脉搏动.Text = tb健康体检.Rows[0][tb_健康体检.足背动脉搏动].ToString();
                //肛门指诊
                string 肛门指诊 = tb健康体检.Rows[0][tb_健康体检.肛门指诊].ToString();
                string[] 肛门指诊s = 肛门指诊.Split(',');
                this.xrLabel_肛门指诊.Text = 肛门指诊s[0] == "99" ? "5" : 肛门指诊s[0];
                this.xrLabel_肛门指诊其他.Text = tb健康体检.Rows[0][tb_健康体检.肛门指诊异常].ToString();
                //乳腺
                string 乳腺 = tb健康体检.Rows[0][tb_健康体检.乳腺].ToString();
                string[] 乳腺s = 乳腺.Split(',');
                for (int i = 0; i < 乳腺s.Length; i++)
                {
                    if (string.IsNullOrEmpty(乳腺s[i]))
                    {
                        continue;
                    }

                    if (i > 3) break;

                    string xrName = "xrLabel_乳腺" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = 乳腺s[i] == "99" ? "5" : 乳腺s[i];
                }
                this.xrLabel_乳腺其他.Text = tb健康体检.Rows[0][tb_健康体检.乳腺其他].ToString();
                //外阴
                this.xrLabel_外阴.Text = tb健康体检.Rows[0][tb_健康体检.外阴].ToString();
                this.xrLabel_外阴异常.Text = tb健康体检.Rows[0][tb_健康体检.外阴异常].ToString();
                //阴道
                this.xrLabel_阴道.Text = tb健康体检.Rows[0][tb_健康体检.阴道].ToString();
                this.xrLabel_阴道异常.Text = tb健康体检.Rows[0][tb_健康体检.阴道异常].ToString();
                //宫颈
                this.xrLabel_宫颈.Text = tb健康体检.Rows[0][tb_健康体检.宫颈].ToString();
                this.xrLabel_宫颈异常.Text = tb健康体检.Rows[0][tb_健康体检.宫颈异常].ToString();
                //宫体
                this.xrLabel_宫体.Text = tb健康体检.Rows[0][tb_健康体检.宫体].ToString();
                this.xrLabel_宫体异常.Text = tb健康体检.Rows[0][tb_健康体检.宫体异常].ToString();
                //附件
                this.xrLabel_附件.Text = tb健康体检.Rows[0][tb_健康体检.附件].ToString();
                this.xrLabel_附件异常.Text = tb健康体检.Rows[0][tb_健康体检.附件异常].ToString();
                //查体其他
                this.xrTable_查体其他.Text = tb健康体检.Rows[0][tb_健康体检.查体其他].ToString();
                //ABO
                string 血型_ABO = tb健康状态.Rows[0][tb_健康档案_健康状态.血型].ToString();
                switch (血型_ABO)
                {
                    case "1":
                        this.xrLabel_ABO.Text = "A";
                        break;
                    case "2":
                        this.xrLabel_ABO.Text = "AB";
                        break;
                    case "3":
                        this.xrLabel_ABO.Text = "B";
                        break;
                    case "4":
                        this.xrLabel_ABO.Text = "O";
                        break;
                    case "5":
                        this.xrLabel_ABO.Text = "不祥";
                        break;
                    default:
                        this.xrLabel_ABO.Text = "";
                        break;
                }
                //Rh
                string 血型_Rh = tb健康状态.Rows[0][tb_健康档案_健康状态.RH].ToString();
                switch (血型_Rh)
                {
                    case "1":
                        this.xrLabel_Rh.Text = "RH阳性";
                        break;
                    case "2":
                        this.xrLabel_Rh.Text = "RH阴性";
                        break;
                    case "3":
                        this.xrLabel_Rh.Text = "不祥";
                        break;
                    default:
                        this.xrLabel_Rh.Text = "";
                        break;
                }
                //血红蛋白
                this.xrLabel_血红蛋白.Text = tb健康体检.Rows[0][tb_健康体检.血红蛋白].ToString();
                //白细胞
                this.xrLabel_白细胞.Text = tb健康体检.Rows[0][tb_健康体检.白细胞].ToString();
                //血小板
                this.xrLabel_血小板.Text = tb健康体检.Rows[0][tb_健康体检.血小板].ToString();
                //血常规其他
                this.xrLabel_血常规其他.Text = tb健康体检.Rows[0][tb_健康体检.血常规其他].ToString();
                //尿蛋白
                this.xrLabel_尿蛋白.Text = tb健康体检.Rows[0][tb_健康体检.尿蛋白].ToString();
                //尿糖
                this.xrLabel_尿糖.Text = tb健康体检.Rows[0][tb_健康体检.尿糖].ToString();
                //尿酮体
                this.xrLabel_尿酮体.Text = tb健康体检.Rows[0][tb_健康体检.尿酮体].ToString();
                //尿潜血
                this.xrLabel_尿潜血.Text = tb健康体检.Rows[0][tb_健康体检.尿潜血].ToString();
                //尿常规其他
                this.xrLabel_尿常规其他.Text = tb健康体检.Rows[0][tb_健康体检.尿常规其他].ToString();
                //空腹血糖
                this.xrLabel_空腹血糖1.Text = tb健康体检.Rows[0][tb_健康体检.空腹血糖].ToString();
                this.xrLabel_空腹血糖2.Text = tb健康体检.Rows[0][tb_健康体检.餐后2H血糖].ToString();
                //同型半胱氨酸
                this.xrLabel_同型半胱氨酸.Text = tb健康体检.Rows[0][tb_健康体检.同型半胱氨酸].ToString();
                //尿微量白蛋白
                this.xrLabel_尿微量白蛋白.Text = tb健康体检.Rows[0][tb_健康体检.尿微量白蛋白].ToString();
            }
        }
    }
}
