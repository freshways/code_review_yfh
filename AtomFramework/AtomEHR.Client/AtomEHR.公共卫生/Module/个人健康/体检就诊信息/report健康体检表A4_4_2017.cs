﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;
using System.Text;
using AtomEHR.Common;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class report健康体检表A4_4_2017 : DevExpress.XtraReports.UI.XtraReport
    {

        //Begin WXF 2018-11-03 | 19:00
        //健康体检表A4版界面与代码均为复制A3版稍作修改

        //End
											 
        DataSet ds体检;
        bll健康体检 bll = new bll健康体检();
        public report健康体检表A4_4_2017()
        {
            InitializeComponent();
        }

        public report健康体检表A4_4_2017(string docNo, string date, DataSet _dsTJ = null)
        {
            InitializeComponent();
            if (_dsTJ == null)
            {
                ds体检 = bll.GetReportDataByKey(docNo, date);
            }
            else
            {
                ds体检 = _dsTJ;
            }

            BindData(ds体检);
            try
            {
                bll医生信息 bll手签 = new bll医生信息();
                //string RGID = ds体检.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构].ToString();
                DataTable dt = bll手签.GetSummaryData(false);
                if (dt.Select("c查体模块='主要用药情况' ").Length > 0)
                {
                    System.Drawing.Image img主要用药情况 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='主要用药情况' ")[0]["s手签"]);
                    this.xrPictureBox主要用药情况.Image = img主要用药情况;
                }
                if (dt.Select("c查体模块='健康评价' ").Length > 0)
                {
                    System.Drawing.Image img健康评价 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='健康评价' ")[0]["s手签"]);
                    this.xrPictureBox健康评价.Image = img健康评价;
                }
                if (dt.Select("c查体模块='健康指导' ").Length > 0)
                {
                    System.Drawing.Image img健康指导 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='健康指导' ")[0]["s手签"]);
                    this.xrPictureBox健康指导.Image = img健康指导;
                }
                if (dt.Select("c查体模块='结果反馈' ").Length > 0)
                {
                    System.Drawing.Image img结果反馈 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='结果反馈' ")[0]["s手签"]);
                    this.xrPictureBox反馈人签字.Image = img结果反馈;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void BindData(DataSet ds体检)
        {
            DataTable tb健康体检 = ds体检.Tables[tb_健康体检.__TableName];
            DataTable tb用药情况表 = ds体检.Tables[tb_健康体检_用药情况表.__TableName];
            DataTable tb接种史 = ds体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName];
            DataTable tb健康档案 = ds体检.Tables[tb_健康档案.__TableName];

            if (tb健康体检.Rows.Count == 1)
            {
                //第四页
                if (tb用药情况表 != null && tb用药情况表.Rows.Count > 0)
                {
                    for (int i = 0; i < tb用药情况表.Rows.Count; i++)
                    {
                        if (i >= 6) break;

                        string 用药名称 = "xrTable_药物名称" + (i + 1);
                        string 用药用法 = "xrTable_药物用法" + (i + 1);
                        string 用药用量 = "xrTable_药物用量" + (i + 1);
                        string 用药时间 = "xrTable_用药时间" + (i + 1);
                        string 用药依从性 = "xrTable_服药依从性" + (i + 1);

                        XRTableCell xr用药名称 = (XRTableCell)FindControl(用药名称, false);
                        XRTableCell xr用药用法 = (XRTableCell)FindControl(用药用法, false);
                        XRTableCell xr用药用量 = (XRTableCell)FindControl(用药用量, false);
                        XRTableCell xr用药时间 = (XRTableCell)FindControl(用药时间, false);
                        XRTableCell xr用药依从性 = (XRTableCell)FindControl(用药依从性, false);

                        object obj_药物名称 = tb用药情况表.Rows[i][tb_健康体检_用药情况表.药物名称];
                        object obj_用法 = tb用药情况表.Rows[i][tb_健康体检_用药情况表.用法];
                        object obj_用药时间 = tb用药情况表.Rows[i][tb_健康体检_用药情况表.用药时间];
                        object obj_用量 = tb用药情况表.Rows[i][tb_健康体检_用药情况表.用量];
                        object obj_服药依从性 = tb用药情况表.Rows[i][tb_健康体检_用药情况表.服药依从性];

                        xr用药名称.Text = (obj_药物名称 == null ? "" : obj_药物名称).ToString();
                        xr用药用法.Text = (obj_用法 == null ? "" : obj_用法).ToString();
                        xr用药时间.Text = (obj_用药时间 == null ? "" : obj_用药时间).ToString();
                        xr用药用量.Text = (obj_用量 == null ? "" : obj_用量).ToString();
                        xr用药依从性.Text = (obj_服药依从性 == null ? "" : obj_服药依从性).ToString();
                    }

                }

                if (tb接种史 != null && tb接种史.Rows.Count > 0)
                {
                    for (int i = 0; i < tb接种史.Rows.Count; i++)
                    {
                        if (i > 3) break;

                        string 接种名称 = "xrTable_接种史名称" + (i + 1);
                        string 接种日期 = "xrTable_接种日期" + (i + 1);
                        string 接种机构 = "xrTable_接种机构" + (i + 1);

                        XRTableCell xr接种名称 = (XRTableCell)FindControl(接种名称, false);
                        XRTableCell xr接种日期 = (XRTableCell)FindControl(接种日期, false);
                        XRTableCell xr接种机构 = (XRTableCell)FindControl(接种机构, false);

                        object obj_接种名称 = tb接种史.Rows[i][tb_健康体检_非免疫规划预防接种史.接种名称];
                        object obj_接种日期 = tb接种史.Rows[i][tb_健康体检_非免疫规划预防接种史.接种日期];
                        object obj_接种机构 = tb接种史.Rows[i][tb_健康体检_非免疫规划预防接种史.接种机构];
                        xr接种名称.Text = (obj_接种名称 == null ? "" : obj_接种名称).ToString();
                        if ((obj_接种日期 == null ? "" : obj_接种日期.ToString()).Length >= 10)
                        {
                            xr接种日期.Text = obj_接种日期.ToString().Substring(0, 10);
                        }
                        xr接种机构.Text = (obj_接种机构 == null ? "" : obj_接种机构).ToString();
                    }

                }

                this.xrLabel_健康评价.Text = tb健康体检.Rows[0][tb_健康体检.健康评价].ToString();
                this.xrLabel_健康评价异常1.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常1].ToString();
                this.xrLabel_健康评价异常2.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常2].ToString();
                this.xrLabel_健康评价异常3.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常3].ToString();
                this.xrLabel_健康评价异常4.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常4].ToString();

                string zhidao = tb健康体检.Rows[0][tb_健康体检.健康指导].ToString();
                int j_zhidao = 1;
                if (!string.IsNullOrEmpty(zhidao))
                {
                    string[] zhidaos = zhidao.Split(',');
                    for (int i = 0; i < zhidaos.Length; i++)
                    {
                        if (zhidaos[i] == "1")
                        {
                            continue;
                        }

                        string result_zhidao = string.Empty;
                        switch (zhidaos[i])
                        {
                            case "2":
                                result_zhidao = "1";
                                break;
                            case "3":
                                result_zhidao = "2";
                                break;
                            case "4":
                                result_zhidao = "3";
                                break;
                            default:
                                result_zhidao = zhidaos[i];
                                break;
                        }

                        string xrName = "xrLabel_健康指导" + j_zhidao;
                        XRLabel xrl = (XRLabel)FindControl(xrName, false);
                        xrl.Text = result_zhidao;
                        j_zhidao += 1;
                    }

                }

                string wxkzys = tb健康体检.Rows[0][tb_健康体检.危险因素控制].ToString();
                int j_wxkzys = 1;
                if (!string.IsNullOrEmpty(wxkzys))
                {
                    string[] wxkzyss = wxkzys.Split(',');
                    for (int i = 0; i < wxkzyss.Length; i++)
                    {
                        if (wxkzyss[i] == "5" || wxkzyss[i] == "6" || wxkzyss[i] == "7" || wxkzyss[i] == "8" || wxkzyss[i] == "9" || wxkzyss[i] == "10" || wxkzyss[i] == "11")
                        {
                            continue;
                        }

                        string result_wxkzys = string.Empty;
                        switch (wxkzyss[i])
                        {
                            case "97":
                                result_wxkzys = "5";
                                break;
                            case "98":
                                result_wxkzys = "6";
                                break;
                            case "99":
                                result_wxkzys = "7";
                                break;
                            default:
                                result_wxkzys = wxkzyss[i];
                                break;
                        }

                        string xrName = "xrLabel_危险控制因素" + j_wxkzys;
                        XRLabel xrl = (XRLabel)FindControl(xrName, false);
                        xrl.Text = result_wxkzys;
                        j_wxkzys += 1;
                    }
                }
                xrLabel_减重目标.Text = tb健康体检.Rows[0][tb_健康体检.危险因素控制体重].ToString();
                //xrLabel_建议接种疫苗.Text = tb健康体检.Rows[0][tb_健康体检.危险因素控制疫苗].ToString();
                //wxf 2018年7月24日 11:07:54 拼接拼接危险控制因素建议接种疫苗
                StringBuilder SB_建议接种疫苗 = new StringBuilder();
                if (!string.IsNullOrEmpty(wxkzys))
                {
                    string[] wxkzyss = wxkzys.Split(',');
                    if (Array.IndexOf(wxkzyss, "5") >= 0)
                    {
                        SB_建议接种疫苗.Append("接种疫苗,");
                    }
                    if (Array.IndexOf(wxkzyss, "6") >= 0)
                    {
                        SB_建议接种疫苗.Append("接种流感和肺炎疫苗,");
                    }
                }
                SB_建议接种疫苗.Append(tb健康体检.Rows[0][tb_健康体检.危险因素控制疫苗].ToString());
                string str_建议接种疫苗 = SB_建议接种疫苗.ToString();
                if (str_建议接种疫苗.Length <= 15)
                {
                    xrLabel_建议接种疫苗.Text = str_建议接种疫苗;
                }
                else
                {
                    xrLabel_建议接种疫苗.Text = str_建议接种疫苗.Substring(0, 15);
                    xrLabel_建议接种疫苗补充.Text = str_建议接种疫苗.Substring(15, str_建议接种疫苗.Length - 15);
                }
                //xrLabel_危险控制因素其他.Text = tb健康体检.Rows[0][tb_健康体检.危险因素控制其他].ToString();
                //wxf 2018年7月24日 11:17:48 拼接拼接危险控制因素其他
                StringBuilder SB_危险控制因素其他 = new StringBuilder();
                //yfh 2019年3月25日 优先把其他里面的内容填充进去,然后判断是否包含,防止重复
                SB_危险控制因素其他.Append(tb健康体检.Rows[0][tb_健康体检.危险因素控制其他].ToString());
                if (!string.IsNullOrEmpty(wxkzys))
                {
                    string[] wxkzyss = wxkzys.Split(',');
                    if (Array.IndexOf(wxkzyss, "7") >= 0 && !SB_危险控制因素其他.ToString().Contains("低盐饮食"))
                    {
                        SB_危险控制因素其他.Append("低盐饮食,");
                    }
                    if (Array.IndexOf(wxkzyss, "8") >= 0 && !SB_危险控制因素其他.ToString().Contains("低脂饮食"))
                    {
                        SB_危险控制因素其他.Append("低脂饮食,");
                    }
                    if (Array.IndexOf(wxkzyss, "9") >= 0 && !SB_危险控制因素其他.ToString().Contains("防滑防跌倒"))
                    {
                        SB_危险控制因素其他.Append("防滑防跌倒，防骨质疏松,");
                    }
                    if (Array.IndexOf(wxkzyss, "10") >= 0 && !SB_危险控制因素其他.ToString().Contains("防意外伤害和自救"))
                    {
                        SB_危险控制因素其他.Append("防意外伤害和自救,");
                    }
                    if (Array.IndexOf(wxkzyss, "11") >= 0 && !SB_危险控制因素其他.ToString().Contains("糖尿病饮食"))
                    {
                        SB_危险控制因素其他.Append("糖尿病饮食,");
                    }
                }
                string str_危险控制因素其他 = SB_危险控制因素其他.ToString();
                if (str_危险控制因素其他.Length <= 20)
                {
                    xrLabel_危险控制因素其他.Text = str_危险控制因素其他;
                }
                else
                {
                    xrLabel_危险控制因素其他.Text = str_危险控制因素其他.Substring(0, 20);
                    xrLabel_危险控制因素其他补充.Text = str_危险控制因素其他.Substring(20, str_危险控制因素其他.Length - 20);
                }

                DataTable dt反馈人 = ds体检.Tables["tb_健康体检_结果反馈"];
                string s本人签字 = string.Empty;
                string s家属签字 = string.Empty;
                string s反馈人签字 = string.Empty;
                string s反馈时间 = string.Empty;
                if (dt反馈人 != null && dt反馈人.Rows.Count > 0)
                {
                    s本人签字 = dt反馈人.Rows[0]["本人签字"].ToString();
                    s家属签字 = dt反馈人.Rows[0]["家属签字"].ToString();
                    s反馈人签字 = dt反馈人.Rows[0]["反馈人签字"].ToString();
                    s反馈时间 = dt反馈人.Rows[0]["反馈时间"].ToString();                    
                }
                if (!string.IsNullOrEmpty(s本人签字))
                {
                    xrPictureBox本人签字.Image = Base64Utility.Base64ToBitmap(s本人签字);
                }
                else
                {
                    xrPictureBox本人签字.Image = Base64Utility.Base64ToBitmap(tb健康档案.Rows[0][tb_健康档案.本人或家属签字].ToString());
                }
                if (!string.IsNullOrEmpty(s家属签字))
                    xrPictureBox家属签字.Image = Base64Utility.Base64ToBitmap(s家属签字);
                if (!string.IsNullOrEmpty(s反馈人签字))
                    xrPictureBox反馈人签字.Image = Base64Utility.Base64ToBitmap(s反馈人签字);
                if (string.IsNullOrEmpty(s反馈时间))
                {
                    DateTime t反馈时间 ;
                    if (DateTime.TryParse(tb健康体检.Rows[0][tb_健康体检.体检日期].ToString(), out t反馈时间))
                        s反馈时间 = t反馈时间.AddDays(7).ToString("yyyy-MM-dd");
                    else
                    {
                        s反馈时间 = Convert.ToDateTime(tb健康体检.Rows[0][tb_健康体检.创建时间].ToString()).AddDays(7).ToString("yyyy-MM-dd");
                    }
                }
                string[] fksj = s反馈时间.Split('-');
                //防止反馈时间为空造成的数组超界
                if (fksj.Length == 3)
                {
                    this.xrLabel_反馈时间年.Text = fksj[0];
                    this.xrLabel_反馈时间月.Text = fksj[1];
                    this.xrLabel_反馈时间日.Text = fksj[2];
                }
            }
        }

    }
}
