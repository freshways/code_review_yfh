﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class UC健康体检表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC健康体检表));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio其他B超 = new DevExpress.XtraEditors.RadioGroup();
            this.txt其他B超异常 = new DevExpress.XtraEditors.TextEdit();
            this.txt空腹血糖2 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt同型半胱氨酸 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.cboRh = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cboABO = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btn清空选择 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gc接种史 = new DevExpress.XtraGrid.GridControl();
            this.gv接种史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col接种名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col接种日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte接种日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col接种机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col个人档案编号4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn预防接种史删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn预防接种史添加 = new DevExpress.XtraEditors.SimpleButton();
            this.gc用药情况 = new DevExpress.XtraGrid.GridControl();
            this.gv用药情况 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col药物名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用法 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用药时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col服药依从性 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lkp服药依从性 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.col个人档案编号3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col创建时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte用药时间 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.btn添加用药情况 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除用药情况 = new DevExpress.XtraEditors.SimpleButton();
            this.radio预防接种史 = new DevExpress.XtraEditors.RadioGroup();
            this.btn病床史删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn病床史添加 = new DevExpress.XtraEditors.SimpleButton();
            this.gc建床史 = new DevExpress.XtraGrid.GridControl();
            this.gv建床史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col建床日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte建床日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col撤床日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte撤床日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col原因2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col医疗机构名称2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病案号2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col个人档案编号2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col类型2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radio家庭病床史 = new DevExpress.XtraEditors.RadioGroup();
            this.btn住院史删除 = new DevExpress.XtraEditors.SimpleButton();
            this.radio主要用药情况 = new DevExpress.XtraEditors.RadioGroup();
            this.btn住院史添加 = new DevExpress.XtraEditors.SimpleButton();
            this.gc住院史 = new DevExpress.XtraGrid.GridControl();
            this.gv住院史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col入院日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte入院日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col出院日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dte出院日期 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col原因 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col医疗机构名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col个人档案编号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col类型 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.radio住院史 = new DevExpress.XtraEditors.RadioGroup();
            this.txt齿列_其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿1 = new DevExpress.XtraEditors.TextEdit();
            this.chk齿列_义齿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk齿列_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.chk齿列_龋齿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk齿列_缺齿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk齿列_正常 = new DevExpress.XtraEditors.CheckEdit();
            this.txt职业病其他防护 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业病其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt化学防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt化学因素 = new DevExpress.XtraEditors.TextEdit();
            this.txt物理防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt物理因素 = new DevExpress.XtraEditors.TextEdit();
            this.radio职业病其他防护措施有无 = new DevExpress.XtraEditors.RadioGroup();
            this.txt放射物质防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt放射物质 = new DevExpress.XtraEditors.TextEdit();
            this.radio化学防护措施有无 = new DevExpress.XtraEditors.RadioGroup();
            this.radio物理防护措施有无 = new DevExpress.XtraEditors.RadioGroup();
            this.txt粉尘防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt粉尘 = new DevExpress.XtraEditors.TextEdit();
            this.radio放射物质防护措施有无 = new DevExpress.XtraEditors.RadioGroup();
            this.radio粉尘防护措施有无 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio职业病有无 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl38 = new DevExpress.XtraEditors.LabelControl();
            this.txt工种 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl39 = new DevExpress.XtraEditors.LabelControl();
            this.txt从业时间 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl40 = new DevExpress.XtraEditors.LabelControl();
            this.flow老年人认知能力 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio老年人认知能力 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl36 = new DevExpress.XtraEditors.LabelControl();
            this.txt老年人认知能力总分 = new DevExpress.XtraEditors.TextEdit();
            this.radio老年人生活自理能力评估 = new DevExpress.XtraEditors.RadioGroup();
            this.flow老年人情感状态 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio老年人情感状态 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl37 = new DevExpress.XtraEditors.LabelControl();
            this.txt老年人情感状态总分 = new DevExpress.XtraEditors.TextEdit();
            this.radio老年人健康状态评估 = new DevExpress.XtraEditors.RadioGroup();
            this.flow健康指导 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk纳入慢性病管理 = new DevExpress.XtraEditors.CheckEdit();
            this.chk建议复查 = new DevExpress.XtraEditors.CheckEdit();
            this.chk定期随访 = new DevExpress.XtraEditors.CheckEdit();
            this.chk建议转诊 = new DevExpress.XtraEditors.CheckEdit();
            this.txt最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建人 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.panelControl6 = new DevExpress.XtraEditors.PanelControl();
            this.flow危险因素控制 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk戒烟 = new DevExpress.XtraEditors.CheckEdit();
            this.chk健康饮酒 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食 = new DevExpress.XtraEditors.CheckEdit();
            this.chk锻炼 = new DevExpress.XtraEditors.CheckEdit();
            this.chk减体重 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl35 = new DevExpress.XtraEditors.LabelControl();
            this.txt减体重目标 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl34 = new DevExpress.XtraEditors.LabelControl();
            this.chk建议疫苗接种 = new DevExpress.XtraEditors.CheckEdit();
            this.chk接种疫苗 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.txt建议疫苗接种 = new DevExpress.XtraEditors.TextEdit();
            this.chk危险因素其他 = new DevExpress.XtraEditors.CheckEdit();
            this.chk低盐饮食 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.txt危险因素其他 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl33 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.txt体检异常4 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl32 = new DevExpress.XtraEditors.LabelControl();
            this.txt体检异常3 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl31 = new DevExpress.XtraEditors.LabelControl();
            this.txt体检异常2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.txt体检异常1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.radio健康评价 = new DevExpress.XtraEditors.RadioGroup();
            this.flow脑血管疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk脑血管疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑血管疾病_缺血性卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑血管疾病_脑出血 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑血管疾病_蛛网膜下腔出血 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑血管疾病_短暂性脑缺血发作 = new DevExpress.XtraEditors.CheckEdit();
            this.chk脑血管疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt脑血管疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow其他系统疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk其他系统疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_慢性支气管炎 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_肺气肿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_恶性肿瘤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_老年性骨关节病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk其他系统疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt其他系统疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow神经系统疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk神经系统疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk神经系统疾病_老年性痴呆 = new DevExpress.XtraEditors.CheckEdit();
            this.chk神经系统疾病_帕金森病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk神经系统疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt神经系统疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel45 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio特秉质 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel44 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio气郁质 = new DevExpress.XtraEditors.RadioGroup();
            this.flow眼部疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk眼部疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk眼部疾病_视网膜出血或渗出 = new DevExpress.XtraEditors.CheckEdit();
            this.chk眼部疾病_视乳头水肿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk眼部疾病_白内障 = new DevExpress.XtraEditors.CheckEdit();
            this.chk眼部疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt眼部疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow血管疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk血管疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk血管疾病_夹层动脉瘤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk血管疾病_动脉闭塞性疾病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk血管疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt血管疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow心血管疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk心血管疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心血管疾病_心肌梗死 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心血管疾病_心绞痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心血管疾病_冠状动脉血运重建 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心血管疾病_充血性心力衰竭 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心血管疾病_心前区疼痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心血管疾病_高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心血管疾病_夹层动脉瘤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心血管疾病_动脉闭塞性疾病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心血管疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt心血管疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow肾脏疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk肾脏疾病_未发现 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肾脏疾病_糖尿病肾病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肾脏疾病_肾功能衰竭 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肾脏疾病_急性肾炎 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肾脏疾病_慢性肾炎 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肾脏疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt肾脏疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel43 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio血瘀质 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel42 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio湿热质 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel41 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio痰湿质 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel40 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio阴虚质 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel39 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio阳虚质 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel38 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio气虚质 = new DevExpress.XtraEditors.RadioGroup();
            this.txt辅助检查_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel37 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio平和质 = new DevExpress.XtraEditors.RadioGroup();
            this.txt血清高密度脂蛋白胆固醇 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel36 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio宫颈涂片 = new DevExpress.XtraEditors.RadioGroup();
            this.txt宫颈涂片 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel35 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio腹部B超 = new DevExpress.XtraEditors.RadioGroup();
            this.txt腹部B超异常 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel34 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio胸部X线片 = new DevExpress.XtraEditors.RadioGroup();
            this.txt胸部X线片 = new DevExpress.XtraEditors.TextEdit();
            this.txt甘油三酯 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血清低密度脂蛋白胆固醇 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt总胆固醇 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血钾浓度 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血钠浓度 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血尿素氮 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血清肌酐 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血清谷草转氨酶 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt白蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt总胆红素 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt结合胆红素 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血清谷丙转氨酶 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel33 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio乙型肝炎表面抗原 = new DevExpress.XtraEditors.RadioGroup();
            this.txt糖化血红蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt尿微量白蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel32 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio大便潜血 = new DevExpress.XtraEditors.RadioGroup();
            this.flow心电图 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk心电图_正常 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心电图_改变 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心电图_心肌梗塞 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心电图_心动过速 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心电图_心动过缓 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心电图_早搏 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心电图_房颤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心电图_房室传导阻滞 = new DevExpress.XtraEditors.CheckEdit();
            this.chk心电图_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt心电图_其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt空腹血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel30 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txt尿蛋白 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txt尿糖 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txt尿胴体 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txt尿潜血 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txt尿常规其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel29 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt血常规_其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt白细胞 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血小板 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt血红蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.radio足背动脉搏动 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel28 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt查体_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel27 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio附件 = new DevExpress.XtraEditors.RadioGroup();
            this.txt附件 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel26 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio宫体 = new DevExpress.XtraEditors.RadioGroup();
            this.txt宫体 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel25 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio宫颈 = new DevExpress.XtraEditors.RadioGroup();
            this.txt宫颈 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel24 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio阴道 = new DevExpress.XtraEditors.RadioGroup();
            this.txt阴道 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel23 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio外阴 = new DevExpress.XtraEditors.RadioGroup();
            this.txt外阴 = new DevExpress.XtraEditors.TextEdit();
            this.radio下肢水肿 = new DevExpress.XtraEditors.RadioGroup();
            this.radio心律 = new DevExpress.XtraEditors.RadioGroup();
            this.flowLayoutPanel20 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio移动性浊音 = new DevExpress.XtraEditors.RadioGroup();
            this.txt移动性浊音 = new DevExpress.XtraEditors.TextEdit();
            this.flow肛门指诊 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk肛门指诊_未见异常 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肛门指诊_触痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肛门指诊_包块 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肛门指诊_前列腺异常 = new DevExpress.XtraEditors.CheckEdit();
            this.chk肛门指诊_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt肛门指诊_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow乳腺 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk乳腺_未见异常 = new DevExpress.XtraEditors.CheckEdit();
            this.chk乳腺_乳房切除 = new DevExpress.XtraEditors.CheckEdit();
            this.chk乳腺_异常泌乳 = new DevExpress.XtraEditors.CheckEdit();
            this.chk乳腺_乳腺包块 = new DevExpress.XtraEditors.CheckEdit();
            this.chk乳腺_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt乳腺_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel19 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio脾大 = new DevExpress.XtraEditors.RadioGroup();
            this.txt脾大 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel18 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio肝大 = new DevExpress.XtraEditors.RadioGroup();
            this.txt肝大 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel17 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio包块 = new DevExpress.XtraEditors.RadioGroup();
            this.txt包块 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio压痛 = new DevExpress.XtraEditors.RadioGroup();
            this.txt压痛 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio杂音 = new DevExpress.XtraEditors.RadioGroup();
            this.txt杂音 = new DevExpress.XtraEditors.TextEdit();
            this.txt心率 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio呼吸音 = new DevExpress.XtraEditors.RadioGroup();
            this.txt呼吸音_异常 = new DevExpress.XtraEditors.TextEdit();
            this.radio桶状胸 = new DevExpress.XtraEditors.RadioGroup();
            this.flow巩膜 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk巩膜_正常 = new DevExpress.XtraEditors.CheckEdit();
            this.chk巩膜_黄染 = new DevExpress.XtraEditors.CheckEdit();
            this.chk巩膜_充血 = new DevExpress.XtraEditors.CheckEdit();
            this.chk巩膜_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt巩膜_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow皮肤 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk皮肤_正常 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_潮红 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_苍白 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_发绀 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_黄染 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_色素沉着 = new DevExpress.XtraEditors.CheckEdit();
            this.chk皮肤_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt皮肤_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow罗音 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk罗音_无 = new DevExpress.XtraEditors.CheckEdit();
            this.chk罗音_干罗音 = new DevExpress.XtraEditors.CheckEdit();
            this.chk罗音_湿罗音 = new DevExpress.XtraEditors.CheckEdit();
            this.chk罗音_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt罗音_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio眼底 = new DevExpress.XtraEditors.RadioGroup();
            this.txt眼底 = new DevExpress.XtraEditors.TextEdit();
            this.flow听力 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio听力_听见 = new DevExpress.XtraEditors.CheckEdit();
            this.radio听力_听不清 = new DevExpress.XtraEditors.CheckEdit();
            this.flow淋巴结 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk淋巴结_未触及 = new DevExpress.XtraEditors.CheckEdit();
            this.chk淋巴结_锁骨上 = new DevExpress.XtraEditors.CheckEdit();
            this.chk淋巴结_腋窝 = new DevExpress.XtraEditors.CheckEdit();
            this.chk淋巴结_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt淋巴结_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow运动功能 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio运动能力_可顺利完成 = new DevExpress.XtraEditors.CheckEdit();
            this.radio运动能力_无法完成 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txt左眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt右眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txt矫正左眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txt矫正右眼视力 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.flow咽部 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio咽部_无充血 = new DevExpress.XtraEditors.CheckEdit();
            this.radio咽部_充血 = new DevExpress.XtraEditors.CheckEdit();
            this.radio咽部_淋巴滤泡增生 = new DevExpress.XtraEditors.CheckEdit();
            this.radio咽部_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt咽部_其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow口唇 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio口唇 = new DevExpress.XtraEditors.RadioGroup();
            this.txt口唇其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow饮酒种类 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk饮酒种类_白酒 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮酒种类_啤酒 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮酒种类_红酒 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮酒种类_黄酒 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮酒种类_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt饮酒种类_其他 = new DevExpress.XtraEditors.TextEdit();
            this.radio近一年内是否曾醉酒 = new DevExpress.XtraEditors.RadioGroup();
            this.txt开始饮酒年龄 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt戒酒年龄 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.radio是否戒酒 = new DevExpress.XtraEditors.RadioGroup();
            this.txt日饮酒量 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.radio饮酒频率 = new DevExpress.XtraEditors.RadioGroup();
            this.radio吸烟情况 = new DevExpress.XtraEditors.RadioGroup();
            this.txt戒烟年龄 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt开始吸烟年龄 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt日吸烟量 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.flow饮食习惯 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk饮食习惯_荤素均衡 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食习惯_荤食为主 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食习惯_素食为主 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食习惯_嗜盐 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食习惯_嗜油 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮食习惯_嗜糖 = new DevExpress.XtraEditors.CheckEdit();
            this.txt生活方式_锻炼方式 = new DevExpress.XtraEditors.TextEdit();
            this.txt生活方式_坚持锻炼时间 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt生活方式_每次锻炼时间 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.radio生活方式_锻炼频率 = new DevExpress.XtraEditors.RadioGroup();
            this.txt一般情况_体重指数 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt一般情况_腰围 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt一般情况_体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt一般情况_身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt一般情况_血压右侧原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt一般情况_血压左侧原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt一般情况_血压右侧 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt一般情况_血压左侧 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.txt一般情况_呼吸频率 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt一般情况_脉率 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt一般情况_体温 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flow症状 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk症状_无症状 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_头痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_头晕 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_心悸 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_胸闷 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_胸痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_慢性咳嗽 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_咳痰 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_呼吸困难 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_多饮 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_多尿 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_体重下降 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_乏力 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_关节肿痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_视力模糊 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_手脚麻木 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_尿急 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_尿痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_便秘 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_腹泻 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_恶心呕吐 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_眼花 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_耳鸣 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_乳房胀痛 = new DevExpress.XtraEditors.CheckEdit();
            this.chk症状_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt症状_其他 = new DevExpress.XtraEditors.TextEdit();
            this.text责任医生 = new DevExpress.XtraEditors.TextEdit();
            this.text居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.text联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.text出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.text身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.text姓名 = new DevExpress.XtraEditors.TextEdit();
            this.text个人档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.dte体检日期 = new DevExpress.XtraEditors.DateEdit();
            this.dte创建时间 = new DevExpress.XtraEditors.DateEdit();
            this.dte最近更新时间 = new DevExpress.XtraEditors.DateEdit();
            this.lbl血管疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl体检日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl责任医生 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl体温 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl呼吸频率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl身高 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl腰围 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl体重指数 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl体重 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem4 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lbl脉率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血压左侧 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血压右侧 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.group老年人评估 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem128 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem25 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.group症状 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem122 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem127 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem123 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem126 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem125 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem124 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl饮食习惯 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup19 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl锻炼频率 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup21 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl吸烟状况 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup22 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl饮酒情况 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup20 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl放射物质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl物理因素 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem135 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl职业病其他 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem138 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem136 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem131 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl防护措施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem139 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem137 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem133 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem134 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem129 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl粉尘 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem130 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem132 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup23 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl职业病危害因素接触史 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem26 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl视力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl听力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl运动能力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem143 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem140 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem144 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem146 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem145 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem147 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem141 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem148 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem149 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem150 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem151 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem152 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem155 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem142 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem158 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem24 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup24 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup25 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl齿列总 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl咽部 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem153 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem154 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl皮肤 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl巩膜 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl淋巴结 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl罗音 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl心率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem69 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.group妇科 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem64 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem65 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem66 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem67 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem68 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup29 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup26 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup27 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup28 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.group辅助查体 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem70 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem72 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem71 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem74 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem75 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem76 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem78 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem79 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem80 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem81 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem82 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血清谷丙转氨酶 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血清谷草转氨酶 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem86 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl总胆红素 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem84 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血清肌酐 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血尿素氮 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem91 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem90 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl总胆固醇 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl甘油三酯 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血清低密度脂蛋白胆固醇 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血清高密度脂蛋白胆固醇 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem96 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem98 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem99 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup30 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup31 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup32 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup33 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl腹部B超 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl其他B超 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem100 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem101 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem102 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem103 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem104 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem105 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem106 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem107 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem108 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl脑血管疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl肾脏疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl心血管疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl眼部疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl神经系统疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl其他系统疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lblgc住院史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl住院史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem157 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem159 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblgc家庭病床史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem156 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem160 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem161 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem27 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem28 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem116 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem117 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem29 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lblgc用药情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup15 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem118 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem162 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem163 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem30 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lblgc接种史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.group健康评价 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem120 = new DevExpress.XtraLayout.LayoutControlItem();
            this.group健康指导 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem121 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem73 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel53 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.btn读取化验信息 = new DevExpress.XtraEditors.SimpleButton();
            this.btn读取住院信息 = new DevExpress.XtraEditors.SimpleButton();
            this.btn读取用药信息 = new DevExpress.XtraEditors.SimpleButton();
            this.btn生成健康评价 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio其他B超.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他B超异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboABO.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc接种史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv接种史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte接种日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte接种日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp服药依从性)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte用药时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte用药时间.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio预防接种史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc建床史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv建床史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte建床日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte建床日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte撤床日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte撤床日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio家庭病床史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio主要用药情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte入院日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte入院日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出院日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出院日期.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio住院史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt齿列_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_义齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_龋齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_缺齿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_正常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他防护.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学因素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理因素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio职业病其他防护措施有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio化学防护措施有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio物理防护措施有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio放射物质防护措施有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio粉尘防护措施有无.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio职业病有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt工种.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt从业时间.Properties)).BeginInit();
            this.flow老年人认知能力.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人认知能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人认知能力总分.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人生活自理能力评估.Properties)).BeginInit();
            this.flow老年人情感状态.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人情感状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人情感状态总分.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人健康状态评估.Properties)).BeginInit();
            this.flow健康指导.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk纳入慢性病管理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk建议复查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk定期随访.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk建议转诊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).BeginInit();
            this.panelControl6.SuspendLayout();
            this.flow危险因素控制.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk戒烟.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk健康饮酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk锻炼.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk减体重.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt减体重目标.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk建议疫苗接种.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk接种疫苗.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt建议疫苗接种.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk危险因素其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk低盐饮食.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt危险因素其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检异常4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检异常3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检异常2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检异常1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio健康评价.Properties)).BeginInit();
            this.flow脑血管疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_缺血性卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_脑出血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_蛛网膜下腔出血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_短暂性脑缺血发作.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑血管疾病_其他.Properties)).BeginInit();
            this.flow其他系统疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_慢性支气管炎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_肺气肿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_恶性肿瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_老年性骨关节病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他系统疾病_其他.Properties)).BeginInit();
            this.flow神经系统疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_老年性痴呆.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_帕金森病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt神经系统疾病_其他.Properties)).BeginInit();
            this.flowLayoutPanel45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio特秉质.Properties)).BeginInit();
            this.flowLayoutPanel44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio气郁质.Properties)).BeginInit();
            this.flow眼部疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_视网膜出血或渗出.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_视乳头水肿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_白内障.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼部疾病_其他.Properties)).BeginInit();
            this.flow血管疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk血管疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk血管疾病_夹层动脉瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk血管疾病_动脉闭塞性疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk血管疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血管疾病_其他.Properties)).BeginInit();
            this.flow心血管疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_心肌梗死.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_心绞痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_冠状动脉血运重建.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_充血性心力衰竭.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_心前区疼痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_夹层动脉瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_动脉闭塞性疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心血管疾病_其他.Properties)).BeginInit();
            this.flow肾脏疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_未发现.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_糖尿病肾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_肾功能衰竭.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_急性肾炎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_慢性肾炎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肾脏疾病_其他.Properties)).BeginInit();
            this.flowLayoutPanel43.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio血瘀质.Properties)).BeginInit();
            this.flowLayoutPanel42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio湿热质.Properties)).BeginInit();
            this.flowLayoutPanel41.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio痰湿质.Properties)).BeginInit();
            this.flowLayoutPanel40.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio阴虚质.Properties)).BeginInit();
            this.flowLayoutPanel39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio阳虚质.Properties)).BeginInit();
            this.flowLayoutPanel38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio气虚质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查_其他.Properties)).BeginInit();
            this.flowLayoutPanel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio平和质.Properties)).BeginInit();
            this.flowLayoutPanel36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio宫颈涂片.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫颈涂片.Properties)).BeginInit();
            this.flowLayoutPanel35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio腹部B超.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt腹部B超异常.Properties)).BeginInit();
            this.flowLayoutPanel34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio胸部X线片.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胸部X线片.Properties)).BeginInit();
            this.flowLayoutPanel33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio乙型肝炎表面抗原.Properties)).BeginInit();
            this.flowLayoutPanel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio大便潜血.Properties)).BeginInit();
            this.flow心电图.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_正常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_改变.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_心肌梗塞.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_心动过速.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_心动过缓.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_早搏.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_房颤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_房室传导阻滞.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电图_其他.Properties)).BeginInit();
            this.flowLayoutPanel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿蛋白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿糖.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿胴体.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿潜血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿常规其他.Properties)).BeginInit();
            this.flowLayoutPanel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt血常规_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio足背动脉搏动.Properties)).BeginInit();
            this.flowLayoutPanel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt查体_其他.Properties)).BeginInit();
            this.flowLayoutPanel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio附件.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt附件.Properties)).BeginInit();
            this.flowLayoutPanel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio宫体.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫体.Properties)).BeginInit();
            this.flowLayoutPanel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio宫颈.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫颈.Properties)).BeginInit();
            this.flowLayoutPanel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio阴道.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阴道.Properties)).BeginInit();
            this.flowLayoutPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio外阴.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外阴.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio下肢水肿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio心律.Properties)).BeginInit();
            this.flowLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio移动性浊音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt移动性浊音.Properties)).BeginInit();
            this.flow肛门指诊.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk肛门指诊_未见异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肛门指诊_触痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肛门指诊_包块.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肛门指诊_前列腺异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肛门指诊_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肛门指诊_其他.Properties)).BeginInit();
            this.flow乳腺.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk乳腺_未见异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk乳腺_乳房切除.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk乳腺_异常泌乳.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk乳腺_乳腺包块.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk乳腺_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳腺_其他.Properties)).BeginInit();
            this.flowLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio脾大.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脾大.Properties)).BeginInit();
            this.flowLayoutPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio肝大.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肝大.Properties)).BeginInit();
            this.flowLayoutPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio包块.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt包块.Properties)).BeginInit();
            this.flowLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio压痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt压痛.Properties)).BeginInit();
            this.flowLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio杂音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt杂音.Properties)).BeginInit();
            this.flowLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio呼吸音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt呼吸音_异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio桶状胸.Properties)).BeginInit();
            this.flow巩膜.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_正常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_黄染.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_充血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt巩膜_其他.Properties)).BeginInit();
            this.flow皮肤.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_正常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_潮红.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_苍白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_发绀.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_黄染.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_色素沉着.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt皮肤_其他.Properties)).BeginInit();
            this.flow罗音.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_干罗音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_湿罗音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt罗音_其他.Properties)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio眼底.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼底.Properties)).BeginInit();
            this.flow听力.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio听力_听见.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio听力_听不清.Properties)).BeginInit();
            this.flow淋巴结.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_未触及.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_锁骨上.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_腋窝.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt淋巴结_其他.Properties)).BeginInit();
            this.flow运动功能.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio运动能力_可顺利完成.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio运动能力_无法完成.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt矫正左眼视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt矫正右眼视力.Properties)).BeginInit();
            this.flow咽部.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_无充血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_充血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_淋巴滤泡增生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt咽部_其他.Properties)).BeginInit();
            this.flow口唇.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio口唇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt口唇其他.Properties)).BeginInit();
            this.flow饮酒种类.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_白酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_啤酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_红酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_黄酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒种类_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio近一年内是否曾醉酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否戒酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio饮酒频率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio吸烟情况.Properties)).BeginInit();
            this.flow饮食习惯.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_荤素均衡.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_荤食为主.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_素食为主.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜盐.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜油.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜糖.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活方式_锻炼方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio生活方式_锻炼频率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt一般情况_血压右侧原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt一般情况_血压左侧原因.Properties)).BeginInit();
            this.flow症状.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_无症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_头痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_头晕.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_心悸.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_胸闷.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_胸痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_慢性咳嗽.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_咳痰.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_呼吸困难.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_多饮.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_多尿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_体重下降.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_乏力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_关节肿痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_视力模糊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_手脚麻木.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_尿急.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_尿痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_便秘.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_腹泻.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_恶心呕吐.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_眼花.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_耳鸣.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_乳房胀痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.text责任医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.text居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.text联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.text出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.text身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.text姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.text个人档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte创建时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血管疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体检日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl责任医生)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体温)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl呼吸频率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl身高)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl腰围)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重指数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脉率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压左侧)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压右侧)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group老年人评估)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem128)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group症状)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem127)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem123)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮食习惯)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl锻炼频率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl吸烟状况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮酒情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl放射物质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl物理因素)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem135)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病其他)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem138)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem136)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem131)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl防护措施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem139)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem137)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem133)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem134)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem129)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl粉尘)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem130)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem132)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病危害因素接触史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl视力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl听力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl运动能力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem143)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem140)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem144)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem146)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem145)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem147)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem141)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem148)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem149)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem150)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem151)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem152)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem155)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem142)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem158)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl齿列总)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl咽部)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem153)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem154)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl皮肤)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl巩膜)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl淋巴结)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl罗音)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group妇科)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group辅助查体)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清谷丙转氨酶)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清谷草转氨酶)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl总胆红素)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清肌酐)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血尿素氮)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem90)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl总胆固醇)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl甘油三酯)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清低密度脂蛋白胆固醇)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清高密度脂蛋白胆固醇)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem96)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem98)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem99)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl腹部B超)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他B超)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem100)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem101)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem102)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem103)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem104)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem105)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem106)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem107)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem108)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脑血管疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肾脏疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心血管疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl眼部疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl神经系统疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他系统疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem157)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem159)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc家庭病床史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem156)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem160)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem161)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem116)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem117)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem118)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem162)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem163)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc接种史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group健康评价)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem120)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group健康指导)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem121)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            this.flowLayoutPanel53.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(4);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.txt空腹血糖2);
            this.layoutControl1.Controls.Add(this.txt同型半胱氨酸);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.cboRh);
            this.layoutControl1.Controls.Add(this.cboABO);
            this.layoutControl1.Controls.Add(this.btn清空选择);
            this.layoutControl1.Controls.Add(this.layoutControl3);
            this.layoutControl1.Controls.Add(this.layoutControl2);
            this.layoutControl1.Controls.Add(this.gc接种史);
            this.layoutControl1.Controls.Add(this.btn预防接种史删除);
            this.layoutControl1.Controls.Add(this.btn预防接种史添加);
            this.layoutControl1.Controls.Add(this.gc用药情况);
            this.layoutControl1.Controls.Add(this.btn添加用药情况);
            this.layoutControl1.Controls.Add(this.btn删除用药情况);
            this.layoutControl1.Controls.Add(this.radio预防接种史);
            this.layoutControl1.Controls.Add(this.btn病床史删除);
            this.layoutControl1.Controls.Add(this.btn病床史添加);
            this.layoutControl1.Controls.Add(this.gc建床史);
            this.layoutControl1.Controls.Add(this.radio家庭病床史);
            this.layoutControl1.Controls.Add(this.btn住院史删除);
            this.layoutControl1.Controls.Add(this.radio主要用药情况);
            this.layoutControl1.Controls.Add(this.btn住院史添加);
            this.layoutControl1.Controls.Add(this.gc住院史);
            this.layoutControl1.Controls.Add(this.radio住院史);
            this.layoutControl1.Controls.Add(this.txt齿列_其他);
            this.layoutControl1.Controls.Add(this.txt义齿4);
            this.layoutControl1.Controls.Add(this.txt义齿3);
            this.layoutControl1.Controls.Add(this.txt义齿2);
            this.layoutControl1.Controls.Add(this.txt义齿1);
            this.layoutControl1.Controls.Add(this.txt龋齿4);
            this.layoutControl1.Controls.Add(this.txt龋齿3);
            this.layoutControl1.Controls.Add(this.txt龋齿2);
            this.layoutControl1.Controls.Add(this.txt龋齿1);
            this.layoutControl1.Controls.Add(this.txt缺齿4);
            this.layoutControl1.Controls.Add(this.txt缺齿2);
            this.layoutControl1.Controls.Add(this.txt缺齿3);
            this.layoutControl1.Controls.Add(this.txt缺齿1);
            this.layoutControl1.Controls.Add(this.chk齿列_义齿);
            this.layoutControl1.Controls.Add(this.chk齿列_其他);
            this.layoutControl1.Controls.Add(this.chk齿列_龋齿);
            this.layoutControl1.Controls.Add(this.chk齿列_缺齿);
            this.layoutControl1.Controls.Add(this.chk齿列_正常);
            this.layoutControl1.Controls.Add(this.txt职业病其他防护);
            this.layoutControl1.Controls.Add(this.txt职业病其他);
            this.layoutControl1.Controls.Add(this.txt化学防护措施);
            this.layoutControl1.Controls.Add(this.txt化学因素);
            this.layoutControl1.Controls.Add(this.txt物理防护措施);
            this.layoutControl1.Controls.Add(this.txt物理因素);
            this.layoutControl1.Controls.Add(this.radio职业病其他防护措施有无);
            this.layoutControl1.Controls.Add(this.txt放射物质防护措施);
            this.layoutControl1.Controls.Add(this.txt放射物质);
            this.layoutControl1.Controls.Add(this.radio化学防护措施有无);
            this.layoutControl1.Controls.Add(this.radio物理防护措施有无);
            this.layoutControl1.Controls.Add(this.txt粉尘防护措施);
            this.layoutControl1.Controls.Add(this.txt粉尘);
            this.layoutControl1.Controls.Add(this.radio放射物质防护措施有无);
            this.layoutControl1.Controls.Add(this.radio粉尘防护措施有无);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel1);
            this.layoutControl1.Controls.Add(this.flow老年人认知能力);
            this.layoutControl1.Controls.Add(this.radio老年人生活自理能力评估);
            this.layoutControl1.Controls.Add(this.flow老年人情感状态);
            this.layoutControl1.Controls.Add(this.radio老年人健康状态评估);
            this.layoutControl1.Controls.Add(this.flow健康指导);
            this.layoutControl1.Controls.Add(this.txt最近修改人);
            this.layoutControl1.Controls.Add(this.txt创建人);
            this.layoutControl1.Controls.Add(this.txt创建机构);
            this.layoutControl1.Controls.Add(this.txt当前所属机构);
            this.layoutControl1.Controls.Add(this.panelControl6);
            this.layoutControl1.Controls.Add(this.panelControl5);
            this.layoutControl1.Controls.Add(this.flow脑血管疾病);
            this.layoutControl1.Controls.Add(this.flow其他系统疾病);
            this.layoutControl1.Controls.Add(this.flow神经系统疾病);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel45);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel44);
            this.layoutControl1.Controls.Add(this.flow眼部疾病);
            this.layoutControl1.Controls.Add(this.flow血管疾病);
            this.layoutControl1.Controls.Add(this.flow心血管疾病);
            this.layoutControl1.Controls.Add(this.flow肾脏疾病);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel43);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel42);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel41);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel40);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel39);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel38);
            this.layoutControl1.Controls.Add(this.txt辅助检查_其他);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel37);
            this.layoutControl1.Controls.Add(this.txt血清高密度脂蛋白胆固醇);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel36);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel35);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel34);
            this.layoutControl1.Controls.Add(this.txt甘油三酯);
            this.layoutControl1.Controls.Add(this.txt血清低密度脂蛋白胆固醇);
            this.layoutControl1.Controls.Add(this.txt总胆固醇);
            this.layoutControl1.Controls.Add(this.txt血钾浓度);
            this.layoutControl1.Controls.Add(this.txt血钠浓度);
            this.layoutControl1.Controls.Add(this.txt血尿素氮);
            this.layoutControl1.Controls.Add(this.txt血清肌酐);
            this.layoutControl1.Controls.Add(this.txt血清谷草转氨酶);
            this.layoutControl1.Controls.Add(this.txt白蛋白);
            this.layoutControl1.Controls.Add(this.txt总胆红素);
            this.layoutControl1.Controls.Add(this.txt结合胆红素);
            this.layoutControl1.Controls.Add(this.txt血清谷丙转氨酶);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel33);
            this.layoutControl1.Controls.Add(this.txt糖化血红蛋白);
            this.layoutControl1.Controls.Add(this.txt尿微量白蛋白);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel32);
            this.layoutControl1.Controls.Add(this.flow心电图);
            this.layoutControl1.Controls.Add(this.txt空腹血糖);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel30);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel29);
            this.layoutControl1.Controls.Add(this.txt白细胞);
            this.layoutControl1.Controls.Add(this.txt血小板);
            this.layoutControl1.Controls.Add(this.txt血红蛋白);
            this.layoutControl1.Controls.Add(this.radio足背动脉搏动);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel28);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel27);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel26);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel25);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel24);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel23);
            this.layoutControl1.Controls.Add(this.radio下肢水肿);
            this.layoutControl1.Controls.Add(this.radio心律);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel20);
            this.layoutControl1.Controls.Add(this.flow肛门指诊);
            this.layoutControl1.Controls.Add(this.flow乳腺);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel19);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel18);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel17);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel16);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel15);
            this.layoutControl1.Controls.Add(this.txt心率);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel13);
            this.layoutControl1.Controls.Add(this.radio桶状胸);
            this.layoutControl1.Controls.Add(this.flow巩膜);
            this.layoutControl1.Controls.Add(this.flow皮肤);
            this.layoutControl1.Controls.Add(this.flow罗音);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel9);
            this.layoutControl1.Controls.Add(this.flow听力);
            this.layoutControl1.Controls.Add(this.flow淋巴结);
            this.layoutControl1.Controls.Add(this.flow运动功能);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Controls.Add(this.flow咽部);
            this.layoutControl1.Controls.Add(this.flow口唇);
            this.layoutControl1.Controls.Add(this.flow饮酒种类);
            this.layoutControl1.Controls.Add(this.radio近一年内是否曾醉酒);
            this.layoutControl1.Controls.Add(this.txt开始饮酒年龄);
            this.layoutControl1.Controls.Add(this.txt戒酒年龄);
            this.layoutControl1.Controls.Add(this.radio是否戒酒);
            this.layoutControl1.Controls.Add(this.txt日饮酒量);
            this.layoutControl1.Controls.Add(this.radio饮酒频率);
            this.layoutControl1.Controls.Add(this.radio吸烟情况);
            this.layoutControl1.Controls.Add(this.txt戒烟年龄);
            this.layoutControl1.Controls.Add(this.txt开始吸烟年龄);
            this.layoutControl1.Controls.Add(this.txt日吸烟量);
            this.layoutControl1.Controls.Add(this.flow饮食习惯);
            this.layoutControl1.Controls.Add(this.txt生活方式_锻炼方式);
            this.layoutControl1.Controls.Add(this.txt生活方式_坚持锻炼时间);
            this.layoutControl1.Controls.Add(this.txt生活方式_每次锻炼时间);
            this.layoutControl1.Controls.Add(this.radio生活方式_锻炼频率);
            this.layoutControl1.Controls.Add(this.txt一般情况_体重指数);
            this.layoutControl1.Controls.Add(this.txt一般情况_腰围);
            this.layoutControl1.Controls.Add(this.txt一般情况_体重);
            this.layoutControl1.Controls.Add(this.txt一般情况_身高);
            this.layoutControl1.Controls.Add(this.txt一般情况_血压右侧原因);
            this.layoutControl1.Controls.Add(this.txt一般情况_血压左侧原因);
            this.layoutControl1.Controls.Add(this.txt一般情况_血压右侧);
            this.layoutControl1.Controls.Add(this.txt一般情况_血压左侧);
            this.layoutControl1.Controls.Add(this.txt一般情况_呼吸频率);
            this.layoutControl1.Controls.Add(this.txt一般情况_脉率);
            this.layoutControl1.Controls.Add(this.txt一般情况_体温);
            this.layoutControl1.Controls.Add(this.flow症状);
            this.layoutControl1.Controls.Add(this.text责任医生);
            this.layoutControl1.Controls.Add(this.text居住地址);
            this.layoutControl1.Controls.Add(this.text联系电话);
            this.layoutControl1.Controls.Add(this.text出生日期);
            this.layoutControl1.Controls.Add(this.text身份证号);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.text姓名);
            this.layoutControl1.Controls.Add(this.text个人档案编号);
            this.layoutControl1.Controls.Add(this.dte体检日期);
            this.layoutControl1.Controls.Add(this.dte创建时间);
            this.layoutControl1.Controls.Add(this.dte最近更新时间);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl血管疾病});
            this.layoutControl1.Location = new System.Drawing.Point(0, 33);
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(566, 240, 250, 350);
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(800, 465);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.radio其他B超);
            this.flowLayoutPanel4.Controls.Add(this.txt其他B超异常);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(208, -530);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(562, 26);
            this.flowLayoutPanel4.TabIndex = 181;
            // 
            // radio其他B超
            // 
            this.radio其他B超.EditValue = "3";
            this.radio其他B超.Location = new System.Drawing.Point(3, 3);
            this.radio其他B超.Name = "radio其他B超";
            this.radio其他B超.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "未检测")});
            this.radio其他B超.Size = new System.Drawing.Size(189, 20);
            this.radio其他B超.TabIndex = 0;
            this.radio其他B超.SelectedIndexChanged += new System.EventHandler(this.radio其他B超_SelectedIndexChanged);
            // 
            // txt其他B超异常
            // 
            this.txt其他B超异常.Location = new System.Drawing.Point(198, 3);
            this.txt其他B超异常.Name = "txt其他B超异常";
            this.txt其他B超异常.Size = new System.Drawing.Size(252, 20);
            this.txt其他B超异常.TabIndex = 1;
            // 
            // txt空腹血糖2
            // 
            this.txt空腹血糖2.Lbl1Size = new System.Drawing.Size(75, 18);
            this.txt空腹血糖2.Lbl1Text = "mg/dL";
            this.txt空腹血糖2.Location = new System.Drawing.Point(317, -1098);
            this.txt空腹血糖2.Margin = new System.Windows.Forms.Padding(4);
            this.txt空腹血糖2.Name = "txt空腹血糖2";
            this.txt空腹血糖2.Size = new System.Drawing.Size(453, 20);
            this.txt空腹血糖2.TabIndex = 180;
            this.txt空腹血糖2.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt同型半胱氨酸
            // 
            this.txt同型半胱氨酸.Lbl1Size = new System.Drawing.Size(75, 18);
            this.txt同型半胱氨酸.Lbl1Text = "umol/L";
            this.txt同型半胱氨酸.Location = new System.Drawing.Point(108, -1074);
            this.txt同型半胱氨酸.Margin = new System.Windows.Forms.Padding(4);
            this.txt同型半胱氨酸.Name = "txt同型半胱氨酸";
            this.txt同型半胱氨酸.Size = new System.Drawing.Size(662, 20);
            this.txt同型半胱氨酸.TabIndex = 179;
            this.txt同型半胱氨酸.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Location = new System.Drawing.Point(495, -1242);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(275, 20);
            this.flowLayoutPanel2.TabIndex = 177;
            // 
            // cboRh
            // 
            this.cboRh.Location = new System.Drawing.Point(399, -1242);
            this.cboRh.Name = "cboRh";
            this.cboRh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboRh.Size = new System.Drawing.Size(92, 20);
            this.cboRh.StyleController = this.layoutControl1;
            this.cboRh.TabIndex = 176;
            // 
            // cboABO
            // 
            this.cboABO.Location = new System.Drawing.Point(201, -1242);
            this.cboABO.Name = "cboABO";
            this.cboABO.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboABO.Size = new System.Drawing.Size(99, 20);
            this.cboABO.StyleController = this.layoutControl1;
            this.cboABO.TabIndex = 175;
            // 
            // btn清空选择
            // 
            this.btn清空选择.Location = new System.Drawing.Point(104, -444);
            this.btn清空选择.Name = "btn清空选择";
            this.btn清空选择.Size = new System.Drawing.Size(107, 22);
            this.btn清空选择.StyleController = this.layoutControl1;
            this.btn清空选择.TabIndex = 171;
            this.btn清空选择.Text = "清空选择";
            this.btn清空选择.Click += new System.EventHandler(this.btn清空选择_Click);
            // 
            // layoutControl3
            // 
            this.layoutControl3.Location = new System.Drawing.Point(5, -444);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup16;
            this.layoutControl3.Size = new System.Drawing.Size(95, 22);
            this.layoutControl3.TabIndex = 172;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.CustomizationFormText = "layoutControlGroup16";
            this.layoutControlGroup16.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup16.GroupBordersVisible = false;
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Size = new System.Drawing.Size(95, 22);
            this.layoutControlGroup16.Text = "layoutControlGroup16";
            this.layoutControlGroup16.TextVisible = false;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Location = new System.Drawing.Point(215, -444);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(547, 22);
            this.layoutControl2.TabIndex = 170;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(547, 22);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // gc接种史
            // 
            this.gc接种史.Location = new System.Drawing.Point(112, 394);
            this.gc接种史.MainView = this.gv接种史;
            this.gc接种史.Name = "gc接种史";
            this.gc接种史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.dte接种日期});
            this.gc接种史.Size = new System.Drawing.Size(601, 86);
            this.gc接种史.TabIndex = 138;
            this.gc接种史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv接种史});
            // 
            // gv接种史
            // 
            this.gv接种史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col接种名称,
            this.col接种日期,
            this.col接种机构,
            this.col个人档案编号4,
            this.col创建日期});
            this.gv接种史.GridControl = this.gc接种史;
            this.gv接种史.Name = "gv接种史";
            this.gv接种史.OptionsView.ColumnAutoWidth = false;
            this.gv接种史.OptionsView.ShowGroupPanel = false;
            // 
            // col接种名称
            // 
            this.col接种名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col接种名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col接种名称.Caption = "接种名称";
            this.col接种名称.FieldName = "接种名称";
            this.col接种名称.Name = "col接种名称";
            this.col接种名称.Visible = true;
            this.col接种名称.VisibleIndex = 0;
            this.col接种名称.Width = 148;
            // 
            // col接种日期
            // 
            this.col接种日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col接种日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col接种日期.Caption = "接种日期";
            this.col接种日期.ColumnEdit = this.dte接种日期;
            this.col接种日期.FieldName = "接种日期";
            this.col接种日期.Name = "col接种日期";
            this.col接种日期.Visible = true;
            this.col接种日期.VisibleIndex = 1;
            this.col接种日期.Width = 164;
            // 
            // dte接种日期
            // 
            this.dte接种日期.AutoHeight = false;
            this.dte接种日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte接种日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte接种日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.dte接种日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dte接种日期.EditFormat.FormatString = "yyyy-MM-dd";
            this.dte接种日期.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dte接种日期.Mask.EditMask = "yyyy-MM-dd";
            this.dte接种日期.Mask.UseMaskAsDisplayFormat = true;
            this.dte接种日期.Name = "dte接种日期";
            // 
            // col接种机构
            // 
            this.col接种机构.AppearanceHeader.Options.UseTextOptions = true;
            this.col接种机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col接种机构.Caption = "接种机构";
            this.col接种机构.FieldName = "接种机构";
            this.col接种机构.Name = "col接种机构";
            this.col接种机构.Visible = true;
            this.col接种机构.VisibleIndex = 2;
            this.col接种机构.Width = 176;
            // 
            // col个人档案编号4
            // 
            this.col个人档案编号4.Caption = "个人档案编号";
            this.col个人档案编号4.FieldName = "创建日期";
            this.col个人档案编号4.Name = "col个人档案编号4";
            // 
            // col创建日期
            // 
            this.col创建日期.Caption = "gridColumn2";
            this.col创建日期.FieldName = "创建日期";
            this.col创建日期.Name = "col创建日期";
            // 
            // btn预防接种史删除
            // 
            this.btn预防接种史删除.Enabled = false;
            this.btn预防接种史删除.Image = ((System.Drawing.Image)(resources.GetObject("btn预防接种史删除.Image")));
            this.btn预防接种史删除.Location = new System.Drawing.Point(194, 365);
            this.btn预防接种史删除.Name = "btn预防接种史删除";
            this.btn预防接种史删除.Size = new System.Drawing.Size(144, 22);
            this.btn预防接种史删除.StyleController = this.layoutControl1;
            this.btn预防接种史删除.TabIndex = 13;
            this.btn预防接种史删除.Text = "删除当前行";
            this.btn预防接种史删除.Click += new System.EventHandler(this.btn预防接种史删除_Click);
            // 
            // btn预防接种史添加
            // 
            this.btn预防接种史添加.Enabled = false;
            this.btn预防接种史添加.Image = ((System.Drawing.Image)(resources.GetObject("btn预防接种史添加.Image")));
            this.btn预防接种史添加.Location = new System.Drawing.Point(117, 365);
            this.btn预防接种史添加.Name = "btn预防接种史添加";
            this.btn预防接种史添加.Size = new System.Drawing.Size(73, 22);
            this.btn预防接种史添加.StyleController = this.layoutControl1;
            this.btn预防接种史添加.TabIndex = 12;
            this.btn预防接种史添加.Text = "添加";
            this.btn预防接种史添加.Click += new System.EventHandler(this.btn预防接种史添加_Click);
            // 
            // gc用药情况
            // 
            this.gc用药情况.Location = new System.Drawing.Point(112, 232);
            this.gc用药情况.MainView = this.gv用药情况;
            this.gc用药情况.Name = "gc用药情况";
            this.gc用药情况.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.dte用药时间,
            this.lkp服药依从性});
            this.gc用药情况.Size = new System.Drawing.Size(601, 86);
            this.gc用药情况.TabIndex = 137;
            this.gc用药情况.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv用药情况});
            // 
            // gv用药情况
            // 
            this.gv用药情况.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col药物名称,
            this.col用法,
            this.col用量,
            this.col用药时间,
            this.col服药依从性,
            this.col个人档案编号3,
            this.col创建时间});
            this.gv用药情况.GridControl = this.gc用药情况;
            this.gv用药情况.Name = "gv用药情况";
            this.gv用药情况.OptionsView.ColumnAutoWidth = false;
            this.gv用药情况.OptionsView.ShowGroupPanel = false;
            // 
            // col药物名称
            // 
            this.col药物名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col药物名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col药物名称.Caption = "药物名称";
            this.col药物名称.FieldName = "药物名称";
            this.col药物名称.Name = "col药物名称";
            this.col药物名称.Visible = true;
            this.col药物名称.VisibleIndex = 0;
            this.col药物名称.Width = 114;
            // 
            // col用法
            // 
            this.col用法.AppearanceHeader.Options.UseTextOptions = true;
            this.col用法.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col用法.Caption = "用法";
            this.col用法.FieldName = "用法";
            this.col用法.Name = "col用法";
            this.col用法.Visible = true;
            this.col用法.VisibleIndex = 1;
            this.col用法.Width = 119;
            // 
            // col用量
            // 
            this.col用量.AppearanceHeader.Options.UseTextOptions = true;
            this.col用量.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col用量.Caption = "用量";
            this.col用量.FieldName = "用量";
            this.col用量.Name = "col用量";
            this.col用量.Visible = true;
            this.col用量.VisibleIndex = 2;
            this.col用量.Width = 100;
            // 
            // col用药时间
            // 
            this.col用药时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col用药时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col用药时间.Caption = "用药时间";
            this.col用药时间.FieldName = "用药时间";
            this.col用药时间.Name = "col用药时间";
            this.col用药时间.Visible = true;
            this.col用药时间.VisibleIndex = 3;
            this.col用药时间.Width = 90;
            // 
            // col服药依从性
            // 
            this.col服药依从性.AppearanceHeader.Options.UseTextOptions = true;
            this.col服药依从性.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服药依从性.Caption = "服药依从性";
            this.col服药依从性.ColumnEdit = this.lkp服药依从性;
            this.col服药依从性.FieldName = "服药依从性";
            this.col服药依从性.Name = "col服药依从性";
            this.col服药依从性.Visible = true;
            this.col服药依从性.VisibleIndex = 4;
            this.col服药依从性.Width = 90;
            // 
            // lkp服药依从性
            // 
            this.lkp服药依从性.AutoHeight = false;
            this.lkp服药依从性.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp服药依从性.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "服药依从性")});
            this.lkp服药依从性.Name = "lkp服药依从性";
            this.lkp服药依从性.NullText = "";
            // 
            // col个人档案编号3
            // 
            this.col个人档案编号3.Caption = "个人档案编号";
            this.col个人档案编号3.FieldName = "个人档案编号";
            this.col个人档案编号3.Name = "col个人档案编号3";
            // 
            // col创建时间
            // 
            this.col创建时间.Caption = "创建时间";
            this.col创建时间.FieldName = "创建时间";
            this.col创建时间.Name = "col创建时间";
            // 
            // dte用药时间
            // 
            this.dte用药时间.AutoHeight = false;
            this.dte用药时间.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte用药时间.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte用药时间.Name = "dte用药时间";
            // 
            // btn添加用药情况
            // 
            this.btn添加用药情况.Enabled = false;
            this.btn添加用药情况.Image = ((System.Drawing.Image)(resources.GetObject("btn添加用药情况.Image")));
            this.btn添加用药情况.Location = new System.Drawing.Point(116, 203);
            this.btn添加用药情况.Name = "btn添加用药情况";
            this.btn添加用药情况.Size = new System.Drawing.Size(80, 22);
            this.btn添加用药情况.StyleController = this.layoutControl1;
            this.btn添加用药情况.TabIndex = 12;
            this.btn添加用药情况.Text = "添加";
            this.btn添加用药情况.Click += new System.EventHandler(this.btn添加用药情况_Click);
            // 
            // btn删除用药情况
            // 
            this.btn删除用药情况.Enabled = false;
            this.btn删除用药情况.Image = ((System.Drawing.Image)(resources.GetObject("btn删除用药情况.Image")));
            this.btn删除用药情况.Location = new System.Drawing.Point(200, 203);
            this.btn删除用药情况.Name = "btn删除用药情况";
            this.btn删除用药情况.Size = new System.Drawing.Size(136, 22);
            this.btn删除用药情况.StyleController = this.layoutControl1;
            this.btn删除用药情况.TabIndex = 13;
            this.btn删除用药情况.Text = "删除当前行";
            this.btn删除用药情况.Click += new System.EventHandler(this.btn删除用药情况_Click);
            // 
            // radio预防接种史
            // 
            this.radio预防接种史.EditValue = "2";
            this.radio预防接种史.Location = new System.Drawing.Point(17, 365);
            this.radio预防接种史.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio预防接种史.Name = "radio预防接种史";
            this.radio预防接种史.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio预防接种史.Size = new System.Drawing.Size(96, 25);
            this.radio预防接种史.StyleController = this.layoutControl1;
            this.radio预防接种史.TabIndex = 155;
            this.radio预防接种史.SelectedIndexChanged += new System.EventHandler(this.radio预防接种史_SelectedIndexChanged);
            // 
            // btn病床史删除
            // 
            this.btn病床史删除.Enabled = false;
            this.btn病床史删除.Image = ((System.Drawing.Image)(resources.GetObject("btn病床史删除.Image")));
            this.btn病床史删除.Location = new System.Drawing.Point(261, 44);
            this.btn病床史删除.Name = "btn病床史删除";
            this.btn病床史删除.Size = new System.Drawing.Size(90, 22);
            this.btn病床史删除.StyleController = this.layoutControl1;
            this.btn病床史删除.TabIndex = 13;
            this.btn病床史删除.Text = "删除当前行";
            this.btn病床史删除.Click += new System.EventHandler(this.btn病床史删除_Click);
            // 
            // btn病床史添加
            // 
            this.btn病床史添加.Enabled = false;
            this.btn病床史添加.Image = ((System.Drawing.Image)(resources.GetObject("btn病床史添加.Image")));
            this.btn病床史添加.Location = new System.Drawing.Point(203, 44);
            this.btn病床史添加.Name = "btn病床史添加";
            this.btn病床史添加.Size = new System.Drawing.Size(54, 22);
            this.btn病床史添加.StyleController = this.layoutControl1;
            this.btn病床史添加.TabIndex = 12;
            this.btn病床史添加.Text = "添加";
            this.btn病床史添加.Click += new System.EventHandler(this.btn病床史添加_Click);
            // 
            // gc建床史
            // 
            this.gc建床史.Location = new System.Drawing.Point(108, 73);
            this.gc建床史.MainView = this.gv建床史;
            this.gc建床史.Name = "gc建床史";
            this.gc建床史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.dte撤床日期,
            this.dte建床日期});
            this.gc建床史.Size = new System.Drawing.Size(601, 86);
            this.gc建床史.TabIndex = 136;
            this.gc建床史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv建床史});
            // 
            // gv建床史
            // 
            this.gv建床史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col建床日期,
            this.col撤床日期,
            this.col原因2,
            this.col医疗机构名称2,
            this.col病案号2,
            this.col个人档案编号2,
            this.col类型2});
            this.gv建床史.GridControl = this.gc建床史;
            this.gv建床史.Name = "gv建床史";
            this.gv建床史.OptionsView.ColumnAutoWidth = false;
            this.gv建床史.OptionsView.ShowGroupPanel = false;
            // 
            // col建床日期
            // 
            this.col建床日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col建床日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col建床日期.Caption = "建床日期";
            this.col建床日期.ColumnEdit = this.dte建床日期;
            this.col建床日期.FieldName = "入院日期";
            this.col建床日期.Name = "col建床日期";
            this.col建床日期.Visible = true;
            this.col建床日期.VisibleIndex = 0;
            this.col建床日期.Width = 104;
            // 
            // dte建床日期
            // 
            this.dte建床日期.AutoHeight = false;
            this.dte建床日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte建床日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte建床日期.Mask.EditMask = "yyyy-MM-dd";
            this.dte建床日期.Mask.UseMaskAsDisplayFormat = true;
            this.dte建床日期.Name = "dte建床日期";
            // 
            // col撤床日期
            // 
            this.col撤床日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col撤床日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col撤床日期.Caption = "撤床日期";
            this.col撤床日期.ColumnEdit = this.dte撤床日期;
            this.col撤床日期.FieldName = "出院日期";
            this.col撤床日期.Name = "col撤床日期";
            this.col撤床日期.Visible = true;
            this.col撤床日期.VisibleIndex = 1;
            this.col撤床日期.Width = 94;
            // 
            // dte撤床日期
            // 
            this.dte撤床日期.AutoHeight = false;
            this.dte撤床日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte撤床日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte撤床日期.Mask.EditMask = "yyyy-MM-dd";
            this.dte撤床日期.Mask.UseMaskAsDisplayFormat = true;
            this.dte撤床日期.Name = "dte撤床日期";
            // 
            // col原因2
            // 
            this.col原因2.AppearanceHeader.Options.UseTextOptions = true;
            this.col原因2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col原因2.Caption = "原因";
            this.col原因2.FieldName = "原因";
            this.col原因2.Name = "col原因2";
            this.col原因2.Visible = true;
            this.col原因2.VisibleIndex = 2;
            this.col原因2.Width = 133;
            // 
            // col医疗机构名称2
            // 
            this.col医疗机构名称2.AppearanceHeader.Options.UseTextOptions = true;
            this.col医疗机构名称2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col医疗机构名称2.Caption = "医疗机构名称";
            this.col医疗机构名称2.FieldName = "医疗机构名称";
            this.col医疗机构名称2.Name = "col医疗机构名称2";
            this.col医疗机构名称2.Visible = true;
            this.col医疗机构名称2.VisibleIndex = 3;
            this.col医疗机构名称2.Width = 110;
            // 
            // col病案号2
            // 
            this.col病案号2.AppearanceHeader.Options.UseTextOptions = true;
            this.col病案号2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col病案号2.Caption = "病案号";
            this.col病案号2.FieldName = "病案号";
            this.col病案号2.Name = "col病案号2";
            this.col病案号2.Visible = true;
            this.col病案号2.VisibleIndex = 4;
            this.col病案号2.Width = 100;
            // 
            // col个人档案编号2
            // 
            this.col个人档案编号2.Caption = "个人档案编号";
            this.col个人档案编号2.FieldName = "个人档案编号";
            this.col个人档案编号2.Name = "col个人档案编号2";
            // 
            // col类型2
            // 
            this.col类型2.Caption = "类型";
            this.col类型2.FieldName = "类型";
            this.col类型2.Name = "col类型2";
            // 
            // radio家庭病床史
            // 
            this.radio家庭病床史.EditValue = "2";
            this.radio家庭病床史.Location = new System.Drawing.Point(108, 44);
            this.radio家庭病床史.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio家庭病床史.Name = "radio家庭病床史";
            this.radio家庭病床史.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio家庭病床史.Size = new System.Drawing.Size(91, 25);
            this.radio家庭病床史.StyleController = this.layoutControl1;
            this.radio家庭病床史.TabIndex = 153;
            this.radio家庭病床史.SelectedIndexChanged += new System.EventHandler(this.radio家庭病床史_SelectedIndexChanged);
            // 
            // btn住院史删除
            // 
            this.btn住院史删除.Enabled = false;
            this.btn住院史删除.Image = ((System.Drawing.Image)(resources.GetObject("btn住院史删除.Image")));
            this.btn住院史删除.Location = new System.Drawing.Point(261, -75);
            this.btn住院史删除.Name = "btn住院史删除";
            this.btn住院史删除.Size = new System.Drawing.Size(90, 22);
            this.btn住院史删除.StyleController = this.layoutControl1;
            this.btn住院史删除.TabIndex = 13;
            this.btn住院史删除.Tag = "2";
            this.btn住院史删除.Text = "删除当前行";
            this.btn住院史删除.Click += new System.EventHandler(this.btn住院史删除_Click);
            // 
            // radio主要用药情况
            // 
            this.radio主要用药情况.EditValue = "2";
            this.radio主要用药情况.Location = new System.Drawing.Point(17, 203);
            this.radio主要用药情况.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio主要用药情况.Name = "radio主要用药情况";
            this.radio主要用药情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio主要用药情况.Size = new System.Drawing.Size(95, 25);
            this.radio主要用药情况.StyleController = this.layoutControl1;
            this.radio主要用药情况.TabIndex = 154;
            this.radio主要用药情况.SelectedIndexChanged += new System.EventHandler(this.radio主要用药情况_SelectedIndexChanged);
            // 
            // btn住院史添加
            // 
            this.btn住院史添加.Enabled = false;
            this.btn住院史添加.Image = ((System.Drawing.Image)(resources.GetObject("btn住院史添加.Image")));
            this.btn住院史添加.Location = new System.Drawing.Point(203, -75);
            this.btn住院史添加.Name = "btn住院史添加";
            this.btn住院史添加.Size = new System.Drawing.Size(54, 22);
            this.btn住院史添加.StyleController = this.layoutControl1;
            this.btn住院史添加.TabIndex = 12;
            this.btn住院史添加.Tag = "0";
            this.btn住院史添加.Text = "添加";
            this.btn住院史添加.Click += new System.EventHandler(this.btn住院史添加_Click);
            // 
            // gc住院史
            // 
            this.gc住院史.Location = new System.Drawing.Point(108, -46);
            this.gc住院史.MainView = this.gv住院史;
            this.gc住院史.Name = "gc住院史";
            this.gc住院史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.dte入院日期,
            this.dte出院日期});
            this.gc住院史.Size = new System.Drawing.Size(601, 86);
            this.gc住院史.TabIndex = 14;
            this.gc住院史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv住院史});
            // 
            // gv住院史
            // 
            this.gv住院史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col入院日期,
            this.col出院日期,
            this.col原因,
            this.col医疗机构名称,
            this.col病案号,
            this.col个人档案编号,
            this.col类型});
            this.gv住院史.GridControl = this.gc住院史;
            this.gv住院史.Name = "gv住院史";
            this.gv住院史.OptionsView.ColumnAutoWidth = false;
            this.gv住院史.OptionsView.ShowGroupPanel = false;
            // 
            // col入院日期
            // 
            this.col入院日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col入院日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col入院日期.Caption = "入院日期";
            this.col入院日期.ColumnEdit = this.dte入院日期;
            this.col入院日期.FieldName = "入院日期";
            this.col入院日期.Name = "col入院日期";
            this.col入院日期.Visible = true;
            this.col入院日期.VisibleIndex = 0;
            this.col入院日期.Width = 93;
            // 
            // dte入院日期
            // 
            this.dte入院日期.AutoHeight = false;
            this.dte入院日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte入院日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte入院日期.Mask.EditMask = "yyyy-MM-dd";
            this.dte入院日期.Mask.UseMaskAsDisplayFormat = true;
            this.dte入院日期.Name = "dte入院日期";
            // 
            // col出院日期
            // 
            this.col出院日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出院日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出院日期.Caption = "出院日期";
            this.col出院日期.ColumnEdit = this.dte出院日期;
            this.col出院日期.FieldName = "出院日期";
            this.col出院日期.Name = "col出院日期";
            this.col出院日期.Visible = true;
            this.col出院日期.VisibleIndex = 1;
            this.col出院日期.Width = 89;
            // 
            // dte出院日期
            // 
            this.dte出院日期.AutoHeight = false;
            this.dte出院日期.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出院日期.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出院日期.Mask.EditMask = "yyyy-MM-dd";
            this.dte出院日期.Mask.UseMaskAsDisplayFormat = true;
            this.dte出院日期.Name = "dte出院日期";
            // 
            // col原因
            // 
            this.col原因.AppearanceHeader.Options.UseTextOptions = true;
            this.col原因.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col原因.Caption = "原因";
            this.col原因.FieldName = "原因";
            this.col原因.Name = "col原因";
            this.col原因.Visible = true;
            this.col原因.VisibleIndex = 2;
            this.col原因.Width = 120;
            // 
            // col医疗机构名称
            // 
            this.col医疗机构名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col医疗机构名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col医疗机构名称.Caption = "医疗机构名称";
            this.col医疗机构名称.FieldName = "医疗机构名称";
            this.col医疗机构名称.Name = "col医疗机构名称";
            this.col医疗机构名称.Visible = true;
            this.col医疗机构名称.VisibleIndex = 3;
            this.col医疗机构名称.Width = 96;
            // 
            // col病案号
            // 
            this.col病案号.AppearanceHeader.Options.UseTextOptions = true;
            this.col病案号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col病案号.Caption = "病案号";
            this.col病案号.FieldName = "病案号";
            this.col病案号.Name = "col病案号";
            this.col病案号.Visible = true;
            this.col病案号.VisibleIndex = 4;
            this.col病案号.Width = 106;
            // 
            // col个人档案编号
            // 
            this.col个人档案编号.AppearanceHeader.Options.UseTextOptions = true;
            this.col个人档案编号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col个人档案编号.Caption = "个人档案编号";
            this.col个人档案编号.FieldName = "个人档案编号";
            this.col个人档案编号.Name = "col个人档案编号";
            // 
            // col类型
            // 
            this.col类型.AppearanceHeader.Options.UseTextOptions = true;
            this.col类型.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col类型.Caption = "类型";
            this.col类型.FieldName = "类型";
            this.col类型.Name = "col类型";
            // 
            // radio住院史
            // 
            this.radio住院史.EditValue = "2";
            this.radio住院史.Location = new System.Drawing.Point(108, -75);
            this.radio住院史.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio住院史.Name = "radio住院史";
            this.radio住院史.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无")});
            this.radio住院史.Size = new System.Drawing.Size(91, 25);
            this.radio住院史.StyleController = this.layoutControl1;
            this.radio住院史.TabIndex = 152;
            this.radio住院史.SelectedIndexChanged += new System.EventHandler(this.radio住院史_SelectedIndexChanged);
            // 
            // txt齿列_其他
            // 
            this.txt齿列_其他.Enabled = false;
            this.txt齿列_其他.Location = new System.Drawing.Point(467, -2088);
            this.txt齿列_其他.Name = "txt齿列_其他";
            this.txt齿列_其他.Size = new System.Drawing.Size(303, 20);
            this.txt齿列_其他.StyleController = this.layoutControl1;
            this.txt齿列_其他.TabIndex = 77;
            // 
            // txt义齿4
            // 
            this.txt义齿4.Enabled = false;
            this.txt义齿4.Location = new System.Drawing.Point(307, -2072);
            this.txt义齿4.Name = "txt义齿4";
            this.txt义齿4.Size = new System.Drawing.Size(96, 20);
            this.txt义齿4.StyleController = this.layoutControl1;
            this.txt义齿4.TabIndex = 76;
            // 
            // txt义齿3
            // 
            this.txt义齿3.Enabled = false;
            this.txt义齿3.Location = new System.Drawing.Point(207, -2072);
            this.txt义齿3.Name = "txt义齿3";
            this.txt义齿3.Size = new System.Drawing.Size(96, 20);
            this.txt义齿3.StyleController = this.layoutControl1;
            this.txt义齿3.TabIndex = 73;
            // 
            // txt义齿2
            // 
            this.txt义齿2.Enabled = false;
            this.txt义齿2.Location = new System.Drawing.Point(307, -2096);
            this.txt义齿2.Name = "txt义齿2";
            this.txt义齿2.Size = new System.Drawing.Size(96, 20);
            this.txt义齿2.StyleController = this.layoutControl1;
            this.txt义齿2.TabIndex = 74;
            // 
            // txt义齿1
            // 
            this.txt义齿1.Enabled = false;
            this.txt义齿1.Location = new System.Drawing.Point(207, -2096);
            this.txt义齿1.Name = "txt义齿1";
            this.txt义齿1.Size = new System.Drawing.Size(96, 20);
            this.txt义齿1.StyleController = this.layoutControl1;
            this.txt义齿1.TabIndex = 72;
            // 
            // txt龋齿4
            // 
            this.txt龋齿4.Enabled = false;
            this.txt龋齿4.Location = new System.Drawing.Point(567, -2120);
            this.txt龋齿4.Name = "txt龋齿4";
            this.txt龋齿4.Size = new System.Drawing.Size(96, 20);
            this.txt龋齿4.StyleController = this.layoutControl1;
            this.txt龋齿4.TabIndex = 70;
            // 
            // txt龋齿3
            // 
            this.txt龋齿3.Enabled = false;
            this.txt龋齿3.Location = new System.Drawing.Point(467, -2120);
            this.txt龋齿3.Name = "txt龋齿3";
            this.txt龋齿3.Size = new System.Drawing.Size(96, 20);
            this.txt龋齿3.StyleController = this.layoutControl1;
            this.txt龋齿3.TabIndex = 69;
            // 
            // txt龋齿2
            // 
            this.txt龋齿2.Enabled = false;
            this.txt龋齿2.Location = new System.Drawing.Point(567, -2144);
            this.txt龋齿2.Name = "txt龋齿2";
            this.txt龋齿2.Size = new System.Drawing.Size(96, 20);
            this.txt龋齿2.StyleController = this.layoutControl1;
            this.txt龋齿2.TabIndex = 68;
            // 
            // txt龋齿1
            // 
            this.txt龋齿1.Enabled = false;
            this.txt龋齿1.Location = new System.Drawing.Point(467, -2144);
            this.txt龋齿1.Name = "txt龋齿1";
            this.txt龋齿1.Size = new System.Drawing.Size(96, 20);
            this.txt龋齿1.StyleController = this.layoutControl1;
            this.txt龋齿1.TabIndex = 67;
            // 
            // txt缺齿4
            // 
            this.txt缺齿4.Enabled = false;
            this.txt缺齿4.Location = new System.Drawing.Point(307, -2120);
            this.txt缺齿4.Name = "txt缺齿4";
            this.txt缺齿4.Size = new System.Drawing.Size(96, 20);
            this.txt缺齿4.StyleController = this.layoutControl1;
            this.txt缺齿4.TabIndex = 65;
            // 
            // txt缺齿2
            // 
            this.txt缺齿2.Enabled = false;
            this.txt缺齿2.Location = new System.Drawing.Point(307, -2144);
            this.txt缺齿2.Name = "txt缺齿2";
            this.txt缺齿2.Size = new System.Drawing.Size(96, 20);
            this.txt缺齿2.StyleController = this.layoutControl1;
            this.txt缺齿2.TabIndex = 63;
            // 
            // txt缺齿3
            // 
            this.txt缺齿3.Enabled = false;
            this.txt缺齿3.Location = new System.Drawing.Point(207, -2120);
            this.txt缺齿3.Name = "txt缺齿3";
            this.txt缺齿3.Size = new System.Drawing.Size(96, 20);
            this.txt缺齿3.StyleController = this.layoutControl1;
            this.txt缺齿3.TabIndex = 64;
            // 
            // txt缺齿1
            // 
            this.txt缺齿1.Enabled = false;
            this.txt缺齿1.Location = new System.Drawing.Point(207, -2144);
            this.txt缺齿1.Name = "txt缺齿1";
            this.txt缺齿1.Size = new System.Drawing.Size(96, 20);
            this.txt缺齿1.StyleController = this.layoutControl1;
            this.txt缺齿1.TabIndex = 62;
            // 
            // chk齿列_义齿
            // 
            this.chk齿列_义齿.Enabled = false;
            this.chk齿列_义齿.Location = new System.Drawing.Point(147, -2088);
            this.chk齿列_义齿.Name = "chk齿列_义齿";
            this.chk齿列_义齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk齿列_义齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk齿列_义齿.Properties.Caption = "义齿(假牙)";
            this.chk齿列_义齿.Size = new System.Drawing.Size(56, 32);
            this.chk齿列_义齿.StyleController = this.layoutControl1;
            this.chk齿列_义齿.TabIndex = 71;
            this.chk齿列_义齿.CheckedChanged += new System.EventHandler(this.chk齿列_义齿_CheckedChanged);
            // 
            // chk齿列_其他
            // 
            this.chk齿列_其他.Enabled = false;
            this.chk齿列_其他.Location = new System.Drawing.Point(407, -2088);
            this.chk齿列_其他.Name = "chk齿列_其他";
            this.chk齿列_其他.Properties.Appearance.Options.UseTextOptions = true;
            this.chk齿列_其他.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk齿列_其他.Properties.Caption = "其他";
            this.chk齿列_其他.Size = new System.Drawing.Size(56, 19);
            this.chk齿列_其他.StyleController = this.layoutControl1;
            this.chk齿列_其他.TabIndex = 119;
            this.chk齿列_其他.CheckedChanged += new System.EventHandler(this.chk齿列_其他_CheckedChanged);
            // 
            // chk齿列_龋齿
            // 
            this.chk齿列_龋齿.Enabled = false;
            this.chk齿列_龋齿.Location = new System.Drawing.Point(407, -2136);
            this.chk齿列_龋齿.Name = "chk齿列_龋齿";
            this.chk齿列_龋齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk齿列_龋齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk齿列_龋齿.Properties.Caption = "龋齿";
            this.chk齿列_龋齿.Size = new System.Drawing.Size(56, 19);
            this.chk齿列_龋齿.StyleController = this.layoutControl1;
            this.chk齿列_龋齿.TabIndex = 66;
            this.chk齿列_龋齿.CheckedChanged += new System.EventHandler(this.chk齿列_龋齿_CheckedChanged);
            // 
            // chk齿列_缺齿
            // 
            this.chk齿列_缺齿.Enabled = false;
            this.chk齿列_缺齿.Location = new System.Drawing.Point(147, -2136);
            this.chk齿列_缺齿.Name = "chk齿列_缺齿";
            this.chk齿列_缺齿.Properties.Appearance.Options.UseTextOptions = true;
            this.chk齿列_缺齿.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.chk齿列_缺齿.Properties.Caption = "缺齿";
            this.chk齿列_缺齿.Size = new System.Drawing.Size(56, 19);
            this.chk齿列_缺齿.StyleController = this.layoutControl1;
            this.chk齿列_缺齿.TabIndex = 61;
            this.chk齿列_缺齿.CheckedChanged += new System.EventHandler(this.chk齿列_缺齿_CheckedChanged);
            // 
            // chk齿列_正常
            // 
            this.chk齿列_正常.EditValue = true;
            this.chk齿列_正常.Location = new System.Drawing.Point(147, -2167);
            this.chk齿列_正常.Name = "chk齿列_正常";
            this.chk齿列_正常.Properties.Caption = "正常";
            this.chk齿列_正常.Size = new System.Drawing.Size(56, 19);
            this.chk齿列_正常.StyleController = this.layoutControl1;
            this.chk齿列_正常.TabIndex = 60;
            this.chk齿列_正常.CheckedChanged += new System.EventHandler(this.chk齿列_正常_CheckedChanged);
            // 
            // txt职业病其他防护
            // 
            this.txt职业病其他防护.Enabled = false;
            this.txt职业病其他防护.Location = new System.Drawing.Point(453, -2253);
            this.txt职业病其他防护.Name = "txt职业病其他防护";
            this.txt职业病其他防护.Size = new System.Drawing.Size(317, 20);
            this.txt职业病其他防护.StyleController = this.layoutControl1;
            this.txt职业病其他防护.TabIndex = 58;
            // 
            // txt职业病其他
            // 
            this.txt职业病其他.Enabled = false;
            this.txt职业病其他.Location = new System.Drawing.Point(150, -2253);
            this.txt职业病其他.Name = "txt职业病其他";
            this.txt职业病其他.Size = new System.Drawing.Size(120, 20);
            this.txt职业病其他.StyleController = this.layoutControl1;
            this.txt职业病其他.TabIndex = 56;
            // 
            // txt化学防护措施
            // 
            this.txt化学防护措施.Enabled = false;
            this.txt化学防护措施.Location = new System.Drawing.Point(453, -2277);
            this.txt化学防护措施.Name = "txt化学防护措施";
            this.txt化学防护措施.Size = new System.Drawing.Size(317, 20);
            this.txt化学防护措施.StyleController = this.layoutControl1;
            this.txt化学防护措施.TabIndex = 55;
            // 
            // txt化学因素
            // 
            this.txt化学因素.Enabled = false;
            this.txt化学因素.Location = new System.Drawing.Point(150, -2277);
            this.txt化学因素.Name = "txt化学因素";
            this.txt化学因素.Size = new System.Drawing.Size(120, 20);
            this.txt化学因素.StyleController = this.layoutControl1;
            this.txt化学因素.TabIndex = 53;
            // 
            // txt物理防护措施
            // 
            this.txt物理防护措施.Enabled = false;
            this.txt物理防护措施.Location = new System.Drawing.Point(453, -2301);
            this.txt物理防护措施.Name = "txt物理防护措施";
            this.txt物理防护措施.Size = new System.Drawing.Size(317, 20);
            this.txt物理防护措施.StyleController = this.layoutControl1;
            this.txt物理防护措施.TabIndex = 52;
            // 
            // txt物理因素
            // 
            this.txt物理因素.Enabled = false;
            this.txt物理因素.Location = new System.Drawing.Point(150, -2301);
            this.txt物理因素.Name = "txt物理因素";
            this.txt物理因素.Size = new System.Drawing.Size(120, 20);
            this.txt物理因素.StyleController = this.layoutControl1;
            this.txt物理因素.TabIndex = 51;
            // 
            // radio职业病其他防护措施有无
            // 
            this.radio职业病其他防护措施有无.EditValue = "1";
            this.radio职业病其他防护措施有无.Enabled = false;
            this.radio职业病其他防护措施有无.Location = new System.Drawing.Point(329, -2253);
            this.radio职业病其他防护措施有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio职业病其他防护措施有无.Name = "radio职业病其他防护措施有无";
            this.radio职业病其他防护措施有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio职业病其他防护措施有无.Size = new System.Drawing.Size(120, 20);
            this.radio职业病其他防护措施有无.StyleController = this.layoutControl1;
            this.radio职业病其他防护措施有无.TabIndex = 57;
            this.radio职业病其他防护措施有无.SelectedIndexChanged += new System.EventHandler(this.radio职业病其他防护措施有无_SelectedIndexChanged);
            // 
            // txt放射物质防护措施
            // 
            this.txt放射物质防护措施.Enabled = false;
            this.txt放射物质防护措施.Location = new System.Drawing.Point(453, -2325);
            this.txt放射物质防护措施.Name = "txt放射物质防护措施";
            this.txt放射物质防护措施.Size = new System.Drawing.Size(317, 20);
            this.txt放射物质防护措施.StyleController = this.layoutControl1;
            this.txt放射物质防护措施.TabIndex = 49;
            // 
            // txt放射物质
            // 
            this.txt放射物质.Enabled = false;
            this.txt放射物质.Location = new System.Drawing.Point(150, -2325);
            this.txt放射物质.Name = "txt放射物质";
            this.txt放射物质.Size = new System.Drawing.Size(120, 20);
            this.txt放射物质.StyleController = this.layoutControl1;
            this.txt放射物质.TabIndex = 47;
            // 
            // radio化学防护措施有无
            // 
            this.radio化学防护措施有无.EditValue = "1";
            this.radio化学防护措施有无.Enabled = false;
            this.radio化学防护措施有无.Location = new System.Drawing.Point(329, -2277);
            this.radio化学防护措施有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio化学防护措施有无.Name = "radio化学防护措施有无";
            this.radio化学防护措施有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio化学防护措施有无.Size = new System.Drawing.Size(120, 20);
            this.radio化学防护措施有无.StyleController = this.layoutControl1;
            this.radio化学防护措施有无.TabIndex = 54;
            this.radio化学防护措施有无.SelectedIndexChanged += new System.EventHandler(this.radio化学物质防护措施有无_SelectedIndexChanged);
            // 
            // radio物理防护措施有无
            // 
            this.radio物理防护措施有无.EditValue = "1";
            this.radio物理防护措施有无.Enabled = false;
            this.radio物理防护措施有无.Location = new System.Drawing.Point(329, -2301);
            this.radio物理防护措施有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio物理防护措施有无.Name = "radio物理防护措施有无";
            this.radio物理防护措施有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio物理防护措施有无.Size = new System.Drawing.Size(120, 20);
            this.radio物理防护措施有无.StyleController = this.layoutControl1;
            this.radio物理防护措施有无.TabIndex = 12;
            this.radio物理防护措施有无.SelectedIndexChanged += new System.EventHandler(this.radio物理因素防护措施有无_SelectedIndexChanged);
            // 
            // txt粉尘防护措施
            // 
            this.txt粉尘防护措施.Enabled = false;
            this.txt粉尘防护措施.Location = new System.Drawing.Point(453, -2349);
            this.txt粉尘防护措施.Name = "txt粉尘防护措施";
            this.txt粉尘防护措施.Size = new System.Drawing.Size(317, 20);
            this.txt粉尘防护措施.StyleController = this.layoutControl1;
            this.txt粉尘防护措施.TabIndex = 46;
            // 
            // txt粉尘
            // 
            this.txt粉尘.Enabled = false;
            this.txt粉尘.Location = new System.Drawing.Point(150, -2349);
            this.txt粉尘.Name = "txt粉尘";
            this.txt粉尘.Size = new System.Drawing.Size(120, 20);
            this.txt粉尘.StyleController = this.layoutControl1;
            this.txt粉尘.TabIndex = 44;
            // 
            // radio放射物质防护措施有无
            // 
            this.radio放射物质防护措施有无.EditValue = "1";
            this.radio放射物质防护措施有无.Enabled = false;
            this.radio放射物质防护措施有无.Location = new System.Drawing.Point(329, -2325);
            this.radio放射物质防护措施有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio放射物质防护措施有无.Name = "radio放射物质防护措施有无";
            this.radio放射物质防护措施有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio放射物质防护措施有无.Size = new System.Drawing.Size(120, 20);
            this.radio放射物质防护措施有无.StyleController = this.layoutControl1;
            this.radio放射物质防护措施有无.TabIndex = 48;
            this.radio放射物质防护措施有无.SelectedIndexChanged += new System.EventHandler(this.radio放射物质防护措施有无_SelectedIndexChanged);
            // 
            // radio粉尘防护措施有无
            // 
            this.radio粉尘防护措施有无.EditValue = "1";
            this.radio粉尘防护措施有无.Enabled = false;
            this.radio粉尘防护措施有无.Location = new System.Drawing.Point(329, -2349);
            this.radio粉尘防护措施有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio粉尘防护措施有无.Name = "radio粉尘防护措施有无";
            this.radio粉尘防护措施有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio粉尘防护措施有无.Size = new System.Drawing.Size(120, 20);
            this.radio粉尘防护措施有无.StyleController = this.layoutControl1;
            this.radio粉尘防护措施有无.TabIndex = 45;
            this.radio粉尘防护措施有无.SelectedIndexChanged += new System.EventHandler(this.radio粉尘防护措施有无_SelectedIndexChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel1.Controls.Add(this.radio职业病有无);
            this.flowLayoutPanel1.Controls.Add(this.labelControl38);
            this.flowLayoutPanel1.Controls.Add(this.txt工种);
            this.flowLayoutPanel1.Controls.Add(this.labelControl39);
            this.flowLayoutPanel1.Controls.Add(this.txt从业时间);
            this.flowLayoutPanel1.Controls.Add(this.labelControl40);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(95, -2373);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(675, 20);
            this.flowLayoutPanel1.TabIndex = 43;
            // 
            // radio职业病有无
            // 
            this.radio职业病有无.EditValue = "1";
            this.radio职业病有无.Location = new System.Drawing.Point(3, 0);
            this.radio职业病有无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio职业病有无.Name = "radio职业病有无";
            this.radio职业病有无.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "有")});
            this.radio职业病有无.Size = new System.Drawing.Size(87, 20);
            this.radio职业病有无.TabIndex = 0;
            this.radio职业病有无.SelectedIndexChanged += new System.EventHandler(this.radion职业病有无_SelectedIndexChanged);
            // 
            // labelControl38
            // 
            this.labelControl38.Location = new System.Drawing.Point(96, 3);
            this.labelControl38.Name = "labelControl38";
            this.labelControl38.Size = new System.Drawing.Size(36, 14);
            this.labelControl38.TabIndex = 7;
            this.labelControl38.Text = "（工种";
            // 
            // txt工种
            // 
            this.txt工种.Enabled = false;
            this.txt工种.Location = new System.Drawing.Point(138, 0);
            this.txt工种.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt工种.Name = "txt工种";
            this.txt工种.Size = new System.Drawing.Size(100, 20);
            this.txt工种.TabIndex = 1;
            // 
            // labelControl39
            // 
            this.labelControl39.Location = new System.Drawing.Point(244, 3);
            this.labelControl39.Name = "labelControl39";
            this.labelControl39.Size = new System.Drawing.Size(48, 14);
            this.labelControl39.TabIndex = 9;
            this.labelControl39.Text = "从业时间";
            // 
            // txt从业时间
            // 
            this.txt从业时间.Enabled = false;
            this.txt从业时间.Location = new System.Drawing.Point(298, 0);
            this.txt从业时间.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt从业时间.Name = "txt从业时间";
            this.txt从业时间.Size = new System.Drawing.Size(100, 20);
            this.txt从业时间.TabIndex = 2;
            // 
            // labelControl40
            // 
            this.labelControl40.Location = new System.Drawing.Point(404, 3);
            this.labelControl40.Name = "labelControl40";
            this.labelControl40.Size = new System.Drawing.Size(24, 14);
            this.labelControl40.TabIndex = 11;
            this.labelControl40.Text = "年）";
            // 
            // flow老年人认知能力
            // 
            this.flow老年人认知能力.BackColor = System.Drawing.Color.White;
            this.flow老年人认知能力.Controls.Add(this.radio老年人认知能力);
            this.flow老年人认知能力.Controls.Add(this.labelControl36);
            this.flow老年人认知能力.Controls.Add(this.txt老年人认知能力总分);
            this.flow老年人认知能力.Location = new System.Drawing.Point(98, -2810);
            this.flow老年人认知能力.Name = "flow老年人认知能力";
            this.flow老年人认知能力.Size = new System.Drawing.Size(672, 36);
            this.flow老年人认知能力.TabIndex = 27;
            // 
            // radio老年人认知能力
            // 
            this.radio老年人认知能力.EditValue = "1";
            this.radio老年人认知能力.Location = new System.Drawing.Point(3, 5);
            this.radio老年人认知能力.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.radio老年人认知能力.Name = "radio老年人认知能力";
            this.radio老年人认知能力.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "阴性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "阳性")});
            this.radio老年人认知能力.Size = new System.Drawing.Size(121, 24);
            this.radio老年人认知能力.TabIndex = 0;
            this.radio老年人认知能力.SelectedIndexChanged += new System.EventHandler(this.radio老年人认知能力_SelectedIndexChanged);
            // 
            // labelControl36
            // 
            this.labelControl36.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl36.Location = new System.Drawing.Point(130, 7);
            this.labelControl36.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.labelControl36.Name = "labelControl36";
            this.labelControl36.Size = new System.Drawing.Size(156, 14);
            this.labelControl36.TabIndex = 1;
            this.labelControl36.Text = "     简易智力状态检查,总分   ";
            // 
            // txt老年人认知能力总分
            // 
            this.txt老年人认知能力总分.Enabled = false;
            this.txt老年人认知能力总分.Location = new System.Drawing.Point(292, 5);
            this.txt老年人认知能力总分.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.txt老年人认知能力总分.Name = "txt老年人认知能力总分";
            this.txt老年人认知能力总分.Size = new System.Drawing.Size(100, 20);
            this.txt老年人认知能力总分.TabIndex = 1;
            // 
            // radio老年人生活自理能力评估
            // 
            this.radio老年人生活自理能力评估.EditValue = "1";
            this.radio老年人生活自理能力评估.Location = new System.Drawing.Point(98, -2860);
            this.radio老年人生活自理能力评估.Name = "radio老年人生活自理能力评估";
            this.radio老年人生活自理能力评估.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "可自理(0～3分)"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "轻度依赖(4～8分)"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "中度依赖(9～18分)"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "不能自理(≥19分)")});
            this.radio老年人生活自理能力评估.Size = new System.Drawing.Size(355, 46);
            this.radio老年人生活自理能力评估.StyleController = this.layoutControl1;
            this.radio老年人生活自理能力评估.TabIndex = 26;
            // 
            // flow老年人情感状态
            // 
            this.flow老年人情感状态.BackColor = System.Drawing.Color.White;
            this.flow老年人情感状态.Controls.Add(this.radio老年人情感状态);
            this.flow老年人情感状态.Controls.Add(this.labelControl37);
            this.flow老年人情感状态.Controls.Add(this.txt老年人情感状态总分);
            this.flow老年人情感状态.Location = new System.Drawing.Point(98, -2770);
            this.flow老年人情感状态.Name = "flow老年人情感状态";
            this.flow老年人情感状态.Size = new System.Drawing.Size(672, 36);
            this.flow老年人情感状态.TabIndex = 28;
            // 
            // radio老年人情感状态
            // 
            this.radio老年人情感状态.EditValue = "1";
            this.radio老年人情感状态.Location = new System.Drawing.Point(3, 5);
            this.radio老年人情感状态.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.radio老年人情感状态.Name = "radio老年人情感状态";
            this.radio老年人情感状态.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "阴性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "阳性")});
            this.radio老年人情感状态.Size = new System.Drawing.Size(121, 24);
            this.radio老年人情感状态.TabIndex = 0;
            this.radio老年人情感状态.SelectedIndexChanged += new System.EventHandler(this.radio老年人情感状态_SelectedIndexChanged);
            // 
            // labelControl37
            // 
            this.labelControl37.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl37.Location = new System.Drawing.Point(130, 7);
            this.labelControl37.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.labelControl37.Name = "labelControl37";
            this.labelControl37.Size = new System.Drawing.Size(168, 14);
            this.labelControl37.TabIndex = 1;
            this.labelControl37.Text = "     老年人抑郁评分检查,总分   ";
            // 
            // txt老年人情感状态总分
            // 
            this.txt老年人情感状态总分.Enabled = false;
            this.txt老年人情感状态总分.Location = new System.Drawing.Point(304, 5);
            this.txt老年人情感状态总分.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.txt老年人情感状态总分.Name = "txt老年人情感状态总分";
            this.txt老年人情感状态总分.Size = new System.Drawing.Size(100, 20);
            this.txt老年人情感状态总分.TabIndex = 1;
            // 
            // radio老年人健康状态评估
            // 
            this.radio老年人健康状态评估.AutoSizeInLayoutControl = true;
            this.radio老年人健康状态评估.EditValue = "1";
            this.radio老年人健康状态评估.Location = new System.Drawing.Point(98, -2900);
            this.radio老年人健康状态评估.Name = "radio老年人健康状态评估";
            this.radio老年人健康状态评估.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "满意"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "基本满意"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "说不清楚"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "不太满意"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "不满意")});
            this.radio老年人健康状态评估.Size = new System.Drawing.Size(355, 36);
            this.radio老年人健康状态评估.StyleController = this.layoutControl1;
            this.radio老年人健康状态评估.TabIndex = 25;
            // 
            // flow健康指导
            // 
            this.flow健康指导.Controls.Add(this.chk纳入慢性病管理);
            this.flow健康指导.Controls.Add(this.chk建议复查);
            this.flow健康指导.Controls.Add(this.chk定期随访);
            this.flow健康指导.Controls.Add(this.chk建议转诊);
            this.flow健康指导.Location = new System.Drawing.Point(13, 628);
            this.flow健康指导.Name = "flow健康指导";
            this.flow健康指导.Size = new System.Drawing.Size(132, 106);
            this.flow健康指导.TabIndex = 157;
            // 
            // chk纳入慢性病管理
            // 
            this.chk纳入慢性病管理.Location = new System.Drawing.Point(3, 3);
            this.chk纳入慢性病管理.Name = "chk纳入慢性病管理";
            this.chk纳入慢性病管理.Properties.Caption = "纳入慢性病患者健康管理";
            this.chk纳入慢性病管理.Size = new System.Drawing.Size(124, 19);
            this.chk纳入慢性病管理.TabIndex = 2;
            this.chk纳入慢性病管理.Tag = "2";
            // 
            // chk建议复查
            // 
            this.chk建议复查.Location = new System.Drawing.Point(3, 28);
            this.chk建议复查.Name = "chk建议复查";
            this.chk建议复查.Properties.Caption = "建议复查";
            this.chk建议复查.Size = new System.Drawing.Size(75, 19);
            this.chk建议复查.TabIndex = 0;
            this.chk建议复查.Tag = "3";
            // 
            // chk定期随访
            // 
            this.chk定期随访.Location = new System.Drawing.Point(3, 53);
            this.chk定期随访.Name = "chk定期随访";
            this.chk定期随访.Properties.Caption = "定期随访";
            this.chk定期随访.Size = new System.Drawing.Size(75, 19);
            this.chk定期随访.TabIndex = 1;
            this.chk定期随访.Tag = "1";
            this.chk定期随访.Visible = false;
            // 
            // chk建议转诊
            // 
            this.chk建议转诊.Location = new System.Drawing.Point(3, 78);
            this.chk建议转诊.Name = "chk建议转诊";
            this.chk建议转诊.Properties.Caption = "建议转诊";
            this.chk建议转诊.Size = new System.Drawing.Size(75, 19);
            this.chk建议转诊.TabIndex = 3;
            this.chk建议转诊.Tag = "4";
            // 
            // txt最近修改人
            // 
            this.txt最近修改人.Location = new System.Drawing.Point(559, 795);
            this.txt最近修改人.Name = "txt最近修改人";
            this.txt最近修改人.Properties.ReadOnly = true;
            this.txt最近修改人.Size = new System.Drawing.Size(214, 20);
            this.txt最近修改人.StyleController = this.layoutControl1;
            this.txt最近修改人.TabIndex = 164;
            // 
            // txt创建人
            // 
            this.txt创建人.Location = new System.Drawing.Point(326, 771);
            this.txt创建人.Name = "txt创建人";
            this.txt创建人.Properties.ReadOnly = true;
            this.txt创建人.Size = new System.Drawing.Size(150, 20);
            this.txt创建人.StyleController = this.layoutControl1;
            this.txt创建人.TabIndex = 163;
            // 
            // txt创建机构
            // 
            this.txt创建机构.Location = new System.Drawing.Point(559, 771);
            this.txt创建机构.Name = "txt创建机构";
            this.txt创建机构.Properties.ReadOnly = true;
            this.txt创建机构.Size = new System.Drawing.Size(214, 20);
            this.txt创建机构.StyleController = this.layoutControl1;
            this.txt创建机构.TabIndex = 162;
            // 
            // txt当前所属机构
            // 
            this.txt当前所属机构.Location = new System.Drawing.Point(89, 795);
            this.txt当前所属机构.Name = "txt当前所属机构";
            this.txt当前所属机构.Properties.ReadOnly = true;
            this.txt当前所属机构.Size = new System.Drawing.Size(154, 20);
            this.txt当前所属机构.StyleController = this.layoutControl1;
            this.txt当前所属机构.TabIndex = 161;
            // 
            // panelControl6
            // 
            this.panelControl6.Controls.Add(this.flow危险因素控制);
            this.panelControl6.Controls.Add(this.labelControl33);
            this.panelControl6.Location = new System.Drawing.Point(149, 628);
            this.panelControl6.Name = "panelControl6";
            this.panelControl6.Size = new System.Drawing.Size(621, 106);
            this.panelControl6.TabIndex = 158;
            // 
            // flow危险因素控制
            // 
            this.flow危险因素控制.Controls.Add(this.chk戒烟);
            this.flow危险因素控制.Controls.Add(this.chk健康饮酒);
            this.flow危险因素控制.Controls.Add(this.chk饮食);
            this.flow危险因素控制.Controls.Add(this.chk锻炼);
            this.flow危险因素控制.Controls.Add(this.chk减体重);
            this.flow危险因素控制.Controls.Add(this.labelControl35);
            this.flow危险因素控制.Controls.Add(this.txt减体重目标);
            this.flow危险因素控制.Controls.Add(this.labelControl34);
            this.flow危险因素控制.Controls.Add(this.chk建议疫苗接种);
            this.flow危险因素控制.Controls.Add(this.chk接种疫苗);
            this.flow危险因素控制.Controls.Add(this.checkEdit1);
            this.flow危险因素控制.Controls.Add(this.txt建议疫苗接种);
            this.flow危险因素控制.Controls.Add(this.chk危险因素其他);
            this.flow危险因素控制.Controls.Add(this.chk低盐饮食);
            this.flow危险因素控制.Controls.Add(this.checkEdit3);
            this.flow危险因素控制.Controls.Add(this.checkEdit4);
            this.flow危险因素控制.Controls.Add(this.checkEdit5);
            this.flow危险因素控制.Controls.Add(this.checkEdit2);
            this.flow危险因素控制.Controls.Add(this.txt危险因素其他);
            this.flow危险因素控制.Dock = System.Windows.Forms.DockStyle.Left;
            this.flow危险因素控制.Location = new System.Drawing.Point(2, 16);
            this.flow危险因素控制.Name = "flow危险因素控制";
            this.flow危险因素控制.Size = new System.Drawing.Size(614, 88);
            this.flow危险因素控制.TabIndex = 159;
            // 
            // chk戒烟
            // 
            this.chk戒烟.Location = new System.Drawing.Point(3, 3);
            this.chk戒烟.Name = "chk戒烟";
            this.chk戒烟.Properties.Caption = "1 戒烟";
            this.chk戒烟.Size = new System.Drawing.Size(68, 19);
            this.chk戒烟.TabIndex = 0;
            this.chk戒烟.Tag = "1";
            // 
            // chk健康饮酒
            // 
            this.chk健康饮酒.Location = new System.Drawing.Point(77, 3);
            this.chk健康饮酒.Name = "chk健康饮酒";
            this.chk健康饮酒.Properties.Caption = "2 健康饮酒";
            this.chk健康饮酒.Size = new System.Drawing.Size(101, 19);
            this.chk健康饮酒.TabIndex = 1;
            this.chk健康饮酒.Tag = "2";
            // 
            // chk饮食
            // 
            this.chk饮食.Location = new System.Drawing.Point(184, 3);
            this.chk饮食.Name = "chk饮食";
            this.chk饮食.Properties.Caption = "3 饮食";
            this.chk饮食.Size = new System.Drawing.Size(79, 19);
            this.chk饮食.TabIndex = 2;
            this.chk饮食.Tag = "3";
            // 
            // chk锻炼
            // 
            this.chk锻炼.Location = new System.Drawing.Point(269, 3);
            this.chk锻炼.Name = "chk锻炼";
            this.chk锻炼.Properties.Caption = "4 锻炼";
            this.chk锻炼.Size = new System.Drawing.Size(73, 19);
            this.chk锻炼.TabIndex = 3;
            this.chk锻炼.Tag = "4";
            // 
            // chk减体重
            // 
            this.chk减体重.Location = new System.Drawing.Point(348, 3);
            this.chk减体重.Name = "chk减体重";
            this.chk减体重.Properties.Caption = "5 减体重";
            this.chk减体重.Size = new System.Drawing.Size(85, 19);
            this.chk减体重.TabIndex = 4;
            this.chk减体重.Tag = "97";
            this.chk减体重.CheckedChanged += new System.EventHandler(this.chk减体重_CheckedChanged);
            // 
            // labelControl35
            // 
            this.labelControl35.Location = new System.Drawing.Point(439, 3);
            this.labelControl35.Name = "labelControl35";
            this.labelControl35.Size = new System.Drawing.Size(48, 14);
            this.labelControl35.TabIndex = 13;
            this.labelControl35.Text = "（目标：";
            // 
            // txt减体重目标
            // 
            this.txt减体重目标.Location = new System.Drawing.Point(493, 3);
            this.txt减体重目标.Name = "txt减体重目标";
            this.txt减体重目标.Size = new System.Drawing.Size(55, 20);
            this.txt减体重目标.TabIndex = 5;
            // 
            // labelControl34
            // 
            this.labelControl34.Location = new System.Drawing.Point(554, 3);
            this.labelControl34.Name = "labelControl34";
            this.labelControl34.Size = new System.Drawing.Size(12, 14);
            this.labelControl34.TabIndex = 12;
            this.labelControl34.Text = "）";
            // 
            // chk建议疫苗接种
            // 
            this.chk建议疫苗接种.Location = new System.Drawing.Point(3, 29);
            this.chk建议疫苗接种.Name = "chk建议疫苗接种";
            this.chk建议疫苗接种.Properties.Caption = "6 建议疫苗接种";
            this.chk建议疫苗接种.Size = new System.Drawing.Size(111, 19);
            this.chk建议疫苗接种.TabIndex = 6;
            this.chk建议疫苗接种.Tag = "98";
            // 
            // chk接种疫苗
            // 
            this.chk接种疫苗.Location = new System.Drawing.Point(120, 29);
            this.chk接种疫苗.Name = "chk接种疫苗";
            this.chk接种疫苗.Properties.Caption = "1.接种疫苗";
            this.chk接种疫苗.Size = new System.Drawing.Size(81, 19);
            this.chk接种疫苗.TabIndex = 14;
            this.chk接种疫苗.Tag = "5";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(207, 29);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "2.接种流感和肺炎疫苗";
            this.checkEdit1.Size = new System.Drawing.Size(144, 19);
            this.checkEdit1.TabIndex = 15;
            this.checkEdit1.Tag = "6";
            // 
            // txt建议疫苗接种
            // 
            this.txt建议疫苗接种.Location = new System.Drawing.Point(357, 29);
            this.txt建议疫苗接种.Name = "txt建议疫苗接种";
            this.txt建议疫苗接种.Size = new System.Drawing.Size(76, 20);
            this.txt建议疫苗接种.TabIndex = 7;
            // 
            // chk危险因素其他
            // 
            this.chk危险因素其他.Location = new System.Drawing.Point(439, 29);
            this.chk危险因素其他.Name = "chk危险因素其他";
            this.chk危险因素其他.Properties.Caption = "7 其他";
            this.chk危险因素其他.Size = new System.Drawing.Size(60, 19);
            this.chk危险因素其他.TabIndex = 8;
            this.chk危险因素其他.Tag = "99";
            // 
            // chk低盐饮食
            // 
            this.chk低盐饮食.Location = new System.Drawing.Point(505, 29);
            this.chk低盐饮食.Name = "chk低盐饮食";
            this.chk低盐饮食.Properties.Caption = "1.低盐饮食";
            this.chk低盐饮食.Size = new System.Drawing.Size(81, 19);
            this.chk低盐饮食.TabIndex = 16;
            this.chk低盐饮食.Tag = "7";
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(3, 55);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "2.低脂饮食";
            this.checkEdit3.Size = new System.Drawing.Size(79, 19);
            this.checkEdit3.TabIndex = 17;
            this.checkEdit3.Tag = "8";
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(88, 55);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "3.防滑防跌倒，防骨质疏松";
            this.checkEdit4.Size = new System.Drawing.Size(161, 19);
            this.checkEdit4.TabIndex = 18;
            this.checkEdit4.Tag = "9";
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(255, 55);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "4.防意外伤害和自救";
            this.checkEdit5.Size = new System.Drawing.Size(123, 19);
            this.checkEdit5.TabIndex = 19;
            this.checkEdit5.Tag = "10";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(384, 55);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "5.糖尿病饮食";
            this.checkEdit2.Size = new System.Drawing.Size(85, 19);
            this.checkEdit2.TabIndex = 20;
            this.checkEdit2.Tag = "11";
            // 
            // txt危险因素其他
            // 
            this.txt危险因素其他.EditValue = "";
            this.txt危险因素其他.Location = new System.Drawing.Point(475, 55);
            this.txt危险因素其他.Name = "txt危险因素其他";
            this.txt危险因素其他.Size = new System.Drawing.Size(131, 20);
            this.txt危险因素其他.TabIndex = 9;
            // 
            // labelControl33
            // 
            this.labelControl33.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelControl33.Location = new System.Drawing.Point(2, 2);
            this.labelControl33.Name = "labelControl33";
            this.labelControl33.Size = new System.Drawing.Size(84, 14);
            this.labelControl33.TabIndex = 4;
            this.labelControl33.Text = "危险因素控制：";
            // 
            // panelControl5
            // 
            this.panelControl5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelControl5.Controls.Add(this.txt体检异常4);
            this.panelControl5.Controls.Add(this.labelControl32);
            this.panelControl5.Controls.Add(this.txt体检异常3);
            this.panelControl5.Controls.Add(this.labelControl31);
            this.panelControl5.Controls.Add(this.txt体检异常2);
            this.panelControl5.Controls.Add(this.labelControl27);
            this.panelControl5.Controls.Add(this.txt体检异常1);
            this.panelControl5.Controls.Add(this.labelControl26);
            this.panelControl5.Controls.Add(this.radio健康评价);
            this.panelControl5.Location = new System.Drawing.Point(17, 528);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(749, 56);
            this.panelControl5.TabIndex = 156;
            // 
            // txt体检异常4
            // 
            this.txt体检异常4.Enabled = false;
            this.txt体检异常4.Location = new System.Drawing.Point(346, 25);
            this.txt体检异常4.Name = "txt体检异常4";
            this.txt体检异常4.Size = new System.Drawing.Size(143, 20);
            this.txt体检异常4.TabIndex = 9;
            // 
            // labelControl32
            // 
            this.labelControl32.Location = new System.Drawing.Point(305, 28);
            this.labelControl32.Name = "labelControl32";
            this.labelControl32.Size = new System.Drawing.Size(43, 14);
            this.labelControl32.TabIndex = 8;
            this.labelControl32.Text = "异常4：";
            // 
            // txt体检异常3
            // 
            this.txt体检异常3.Enabled = false;
            this.txt体检异常3.Location = new System.Drawing.Point(153, 25);
            this.txt体检异常3.Name = "txt体检异常3";
            this.txt体检异常3.Size = new System.Drawing.Size(143, 20);
            this.txt体检异常3.TabIndex = 7;
            // 
            // labelControl31
            // 
            this.labelControl31.Location = new System.Drawing.Point(112, 28);
            this.labelControl31.Name = "labelControl31";
            this.labelControl31.Size = new System.Drawing.Size(43, 14);
            this.labelControl31.TabIndex = 6;
            this.labelControl31.Text = "异常3：";
            // 
            // txt体检异常2
            // 
            this.txt体检异常2.Enabled = false;
            this.txt体检异常2.Location = new System.Drawing.Point(345, 2);
            this.txt体检异常2.Name = "txt体检异常2";
            this.txt体检异常2.Size = new System.Drawing.Size(143, 20);
            this.txt体检异常2.TabIndex = 5;
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(304, 5);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(43, 14);
            this.labelControl27.TabIndex = 4;
            this.labelControl27.Text = "异常2：";
            // 
            // txt体检异常1
            // 
            this.txt体检异常1.Enabled = false;
            this.txt体检异常1.Location = new System.Drawing.Point(153, 2);
            this.txt体检异常1.Name = "txt体检异常1";
            this.txt体检异常1.Size = new System.Drawing.Size(143, 20);
            this.txt体检异常1.TabIndex = 3;
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(112, 5);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(43, 14);
            this.labelControl26.TabIndex = 2;
            this.labelControl26.Text = "异常1：";
            // 
            // radio健康评价
            // 
            this.radio健康评价.EditValue = "1";
            this.radio健康评价.Location = new System.Drawing.Point(0, 1);
            this.radio健康评价.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio健康评价.Name = "radio健康评价";
            this.radio健康评价.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "体检无异常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有异常")});
            this.radio健康评价.Size = new System.Drawing.Size(98, 44);
            this.radio健康评价.TabIndex = 156;
            this.radio健康评价.SelectedIndexChanged += new System.EventHandler(this.radio健康评价_SelectedIndexChanged);
            // 
            // flow脑血管疾病
            // 
            this.flow脑血管疾病.BackColor = System.Drawing.Color.White;
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_未发现);
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_缺血性卒中);
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_脑出血);
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_蛛网膜下腔出血);
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_短暂性脑缺血发作);
            this.flow脑血管疾病.Controls.Add(this.chk脑血管疾病_其他);
            this.flow脑血管疾病.Controls.Add(this.txt脑血管疾病_其他);
            this.flow脑血管疾病.Location = new System.Drawing.Point(108, -387);
            this.flow脑血管疾病.Name = "flow脑血管疾病";
            this.flow脑血管疾病.Size = new System.Drawing.Size(662, 46);
            this.flow脑血管疾病.TabIndex = 145;
            // 
            // chk脑血管疾病_未发现
            // 
            this.chk脑血管疾病_未发现.EditValue = true;
            this.chk脑血管疾病_未发现.Location = new System.Drawing.Point(0, 0);
            this.chk脑血管疾病_未发现.Margin = new System.Windows.Forms.Padding(0);
            this.chk脑血管疾病_未发现.Name = "chk脑血管疾病_未发现";
            this.chk脑血管疾病_未发现.Properties.Caption = "未发现";
            this.chk脑血管疾病_未发现.Size = new System.Drawing.Size(63, 19);
            this.chk脑血管疾病_未发现.TabIndex = 0;
            this.chk脑血管疾病_未发现.Tag = "1";
            this.chk脑血管疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk脑血管疾病_缺血性卒中
            // 
            this.chk脑血管疾病_缺血性卒中.Location = new System.Drawing.Point(63, 0);
            this.chk脑血管疾病_缺血性卒中.Margin = new System.Windows.Forms.Padding(0);
            this.chk脑血管疾病_缺血性卒中.Name = "chk脑血管疾病_缺血性卒中";
            this.chk脑血管疾病_缺血性卒中.Properties.Caption = "缺血性卒中";
            this.chk脑血管疾病_缺血性卒中.Size = new System.Drawing.Size(80, 19);
            this.chk脑血管疾病_缺血性卒中.TabIndex = 1;
            this.chk脑血管疾病_缺血性卒中.Tag = "2";
            // 
            // chk脑血管疾病_脑出血
            // 
            this.chk脑血管疾病_脑出血.Location = new System.Drawing.Point(143, 0);
            this.chk脑血管疾病_脑出血.Margin = new System.Windows.Forms.Padding(0);
            this.chk脑血管疾病_脑出血.Name = "chk脑血管疾病_脑出血";
            this.chk脑血管疾病_脑出血.Properties.Caption = "脑出血";
            this.chk脑血管疾病_脑出血.Size = new System.Drawing.Size(56, 19);
            this.chk脑血管疾病_脑出血.TabIndex = 2;
            this.chk脑血管疾病_脑出血.Tag = "3";
            // 
            // chk脑血管疾病_蛛网膜下腔出血
            // 
            this.chk脑血管疾病_蛛网膜下腔出血.Location = new System.Drawing.Point(199, 0);
            this.chk脑血管疾病_蛛网膜下腔出血.Margin = new System.Windows.Forms.Padding(0);
            this.chk脑血管疾病_蛛网膜下腔出血.Name = "chk脑血管疾病_蛛网膜下腔出血";
            this.chk脑血管疾病_蛛网膜下腔出血.Properties.Caption = "蛛网膜下腔出血";
            this.chk脑血管疾病_蛛网膜下腔出血.Size = new System.Drawing.Size(104, 19);
            this.chk脑血管疾病_蛛网膜下腔出血.TabIndex = 3;
            this.chk脑血管疾病_蛛网膜下腔出血.Tag = "4";
            // 
            // chk脑血管疾病_短暂性脑缺血发作
            // 
            this.chk脑血管疾病_短暂性脑缺血发作.Location = new System.Drawing.Point(303, 0);
            this.chk脑血管疾病_短暂性脑缺血发作.Margin = new System.Windows.Forms.Padding(0);
            this.chk脑血管疾病_短暂性脑缺血发作.Name = "chk脑血管疾病_短暂性脑缺血发作";
            this.chk脑血管疾病_短暂性脑缺血发作.Properties.Caption = "短暂性脑缺血发作";
            this.chk脑血管疾病_短暂性脑缺血发作.Size = new System.Drawing.Size(118, 19);
            this.chk脑血管疾病_短暂性脑缺血发作.TabIndex = 4;
            this.chk脑血管疾病_短暂性脑缺血发作.Tag = "5";
            // 
            // chk脑血管疾病_其他
            // 
            this.chk脑血管疾病_其他.Location = new System.Drawing.Point(421, 0);
            this.chk脑血管疾病_其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk脑血管疾病_其他.Name = "chk脑血管疾病_其他";
            this.chk脑血管疾病_其他.Properties.Caption = "其他";
            this.chk脑血管疾病_其他.Size = new System.Drawing.Size(55, 19);
            this.chk脑血管疾病_其他.TabIndex = 5;
            this.chk脑血管疾病_其他.Tag = "99";
            this.chk脑血管疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt脑血管疾病_其他
            // 
            this.txt脑血管疾病_其他.Location = new System.Drawing.Point(479, 0);
            this.txt脑血管疾病_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt脑血管疾病_其他.Name = "txt脑血管疾病_其他";
            this.txt脑血管疾病_其他.Size = new System.Drawing.Size(130, 20);
            this.txt脑血管疾病_其他.TabIndex = 6;
            // 
            // flow其他系统疾病
            // 
            this.flow其他系统疾病.BackColor = System.Drawing.Color.White;
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_未发现);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_糖尿病);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_慢性支气管炎);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_肺气肿);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_恶性肿瘤);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_老年性骨关节病);
            this.flow其他系统疾病.Controls.Add(this.chk其他系统疾病_其他);
            this.flow其他系统疾病.Controls.Add(this.txt其他系统疾病_其他);
            this.flow其他系统疾病.Location = new System.Drawing.Point(108, -161);
            this.flow其他系统疾病.Name = "flow其他系统疾病";
            this.flow其他系统疾病.Size = new System.Drawing.Size(662, 46);
            this.flow其他系统疾病.TabIndex = 151;
            // 
            // chk其他系统疾病_未发现
            // 
            this.chk其他系统疾病_未发现.EditValue = true;
            this.chk其他系统疾病_未发现.Location = new System.Drawing.Point(3, 3);
            this.chk其他系统疾病_未发现.Name = "chk其他系统疾病_未发现";
            this.chk其他系统疾病_未发现.Properties.Caption = "未发现";
            this.chk其他系统疾病_未发现.Size = new System.Drawing.Size(75, 19);
            this.chk其他系统疾病_未发现.TabIndex = 0;
            this.chk其他系统疾病_未发现.Tag = "1";
            this.chk其他系统疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk其他系统疾病_糖尿病
            // 
            this.chk其他系统疾病_糖尿病.Location = new System.Drawing.Point(84, 3);
            this.chk其他系统疾病_糖尿病.Name = "chk其他系统疾病_糖尿病";
            this.chk其他系统疾病_糖尿病.Properties.Caption = "糖尿病";
            this.chk其他系统疾病_糖尿病.Size = new System.Drawing.Size(75, 19);
            this.chk其他系统疾病_糖尿病.TabIndex = 1;
            this.chk其他系统疾病_糖尿病.Tag = "2";
            // 
            // chk其他系统疾病_慢性支气管炎
            // 
            this.chk其他系统疾病_慢性支气管炎.Location = new System.Drawing.Point(165, 3);
            this.chk其他系统疾病_慢性支气管炎.Name = "chk其他系统疾病_慢性支气管炎";
            this.chk其他系统疾病_慢性支气管炎.Properties.Caption = "慢性支气管炎";
            this.chk其他系统疾病_慢性支气管炎.Size = new System.Drawing.Size(106, 19);
            this.chk其他系统疾病_慢性支气管炎.TabIndex = 2;
            this.chk其他系统疾病_慢性支气管炎.Tag = "3";
            // 
            // chk其他系统疾病_肺气肿
            // 
            this.chk其他系统疾病_肺气肿.Location = new System.Drawing.Point(277, 3);
            this.chk其他系统疾病_肺气肿.Name = "chk其他系统疾病_肺气肿";
            this.chk其他系统疾病_肺气肿.Properties.Caption = "慢性阻塞性肺气肿";
            this.chk其他系统疾病_肺气肿.Size = new System.Drawing.Size(131, 19);
            this.chk其他系统疾病_肺气肿.TabIndex = 3;
            this.chk其他系统疾病_肺气肿.Tag = "4";
            // 
            // chk其他系统疾病_恶性肿瘤
            // 
            this.chk其他系统疾病_恶性肿瘤.Location = new System.Drawing.Point(414, 3);
            this.chk其他系统疾病_恶性肿瘤.Name = "chk其他系统疾病_恶性肿瘤";
            this.chk其他系统疾病_恶性肿瘤.Properties.Caption = "恶性肿瘤";
            this.chk其他系统疾病_恶性肿瘤.Size = new System.Drawing.Size(75, 19);
            this.chk其他系统疾病_恶性肿瘤.TabIndex = 4;
            this.chk其他系统疾病_恶性肿瘤.Tag = "5";
            // 
            // chk其他系统疾病_老年性骨关节病
            // 
            this.chk其他系统疾病_老年性骨关节病.Location = new System.Drawing.Point(495, 3);
            this.chk其他系统疾病_老年性骨关节病.Name = "chk其他系统疾病_老年性骨关节病";
            this.chk其他系统疾病_老年性骨关节病.Properties.Caption = "老年性骨关节病";
            this.chk其他系统疾病_老年性骨关节病.Size = new System.Drawing.Size(114, 19);
            this.chk其他系统疾病_老年性骨关节病.TabIndex = 5;
            this.chk其他系统疾病_老年性骨关节病.Tag = "6";
            // 
            // chk其他系统疾病_其他
            // 
            this.chk其他系统疾病_其他.Location = new System.Drawing.Point(3, 28);
            this.chk其他系统疾病_其他.Name = "chk其他系统疾病_其他";
            this.chk其他系统疾病_其他.Properties.Caption = "其他";
            this.chk其他系统疾病_其他.Size = new System.Drawing.Size(60, 19);
            this.chk其他系统疾病_其他.TabIndex = 6;
            this.chk其他系统疾病_其他.Tag = "99";
            this.chk其他系统疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt其他系统疾病_其他
            // 
            this.txt其他系统疾病_其他.Enabled = false;
            this.txt其他系统疾病_其他.Location = new System.Drawing.Point(66, 25);
            this.txt其他系统疾病_其他.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt其他系统疾病_其他.Name = "txt其他系统疾病_其他";
            this.txt其他系统疾病_其他.Size = new System.Drawing.Size(184, 20);
            this.txt其他系统疾病_其他.TabIndex = 7;
            // 
            // flow神经系统疾病
            // 
            this.flow神经系统疾病.BackColor = System.Drawing.Color.White;
            this.flow神经系统疾病.Controls.Add(this.chk神经系统疾病_未发现);
            this.flow神经系统疾病.Controls.Add(this.chk神经系统疾病_老年性痴呆);
            this.flow神经系统疾病.Controls.Add(this.chk神经系统疾病_帕金森病);
            this.flow神经系统疾病.Controls.Add(this.chk神经系统疾病_其他);
            this.flow神经系统疾病.Controls.Add(this.txt神经系统疾病_其他);
            this.flow神经系统疾病.Location = new System.Drawing.Point(108, -211);
            this.flow神经系统疾病.Name = "flow神经系统疾病";
            this.flow神经系统疾病.Size = new System.Drawing.Size(662, 46);
            this.flow神经系统疾病.TabIndex = 150;
            // 
            // chk神经系统疾病_未发现
            // 
            this.chk神经系统疾病_未发现.EditValue = true;
            this.chk神经系统疾病_未发现.Location = new System.Drawing.Point(3, 3);
            this.chk神经系统疾病_未发现.Name = "chk神经系统疾病_未发现";
            this.chk神经系统疾病_未发现.Properties.Caption = "未发现";
            this.chk神经系统疾病_未发现.Size = new System.Drawing.Size(65, 19);
            this.chk神经系统疾病_未发现.TabIndex = 0;
            this.chk神经系统疾病_未发现.Tag = "1";
            this.chk神经系统疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk神经系统疾病_老年性痴呆
            // 
            this.chk神经系统疾病_老年性痴呆.Location = new System.Drawing.Point(74, 3);
            this.chk神经系统疾病_老年性痴呆.Name = "chk神经系统疾病_老年性痴呆";
            this.chk神经系统疾病_老年性痴呆.Properties.Caption = "阿尔茨海默症（老年性痴呆）";
            this.chk神经系统疾病_老年性痴呆.Size = new System.Drawing.Size(182, 19);
            this.chk神经系统疾病_老年性痴呆.TabIndex = 1;
            this.chk神经系统疾病_老年性痴呆.Tag = "2";
            // 
            // chk神经系统疾病_帕金森病
            // 
            this.chk神经系统疾病_帕金森病.Location = new System.Drawing.Point(262, 3);
            this.chk神经系统疾病_帕金森病.Name = "chk神经系统疾病_帕金森病";
            this.chk神经系统疾病_帕金森病.Properties.Caption = "帕金森病";
            this.chk神经系统疾病_帕金森病.Size = new System.Drawing.Size(75, 19);
            this.chk神经系统疾病_帕金森病.TabIndex = 2;
            this.chk神经系统疾病_帕金森病.Tag = "3";
            // 
            // chk神经系统疾病_其他
            // 
            this.chk神经系统疾病_其他.Location = new System.Drawing.Point(343, 3);
            this.chk神经系统疾病_其他.Name = "chk神经系统疾病_其他";
            this.chk神经系统疾病_其他.Properties.Caption = "其他";
            this.chk神经系统疾病_其他.Size = new System.Drawing.Size(55, 19);
            this.chk神经系统疾病_其他.TabIndex = 3;
            this.chk神经系统疾病_其他.Tag = "99";
            this.chk神经系统疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt神经系统疾病_其他
            // 
            this.txt神经系统疾病_其他.Enabled = false;
            this.txt神经系统疾病_其他.Location = new System.Drawing.Point(401, 0);
            this.txt神经系统疾病_其他.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt神经系统疾病_其他.Name = "txt神经系统疾病_其他";
            this.txt神经系统疾病_其他.Size = new System.Drawing.Size(208, 20);
            this.txt神经系统疾病_其他.TabIndex = 4;
            // 
            // flowLayoutPanel45
            // 
            this.flowLayoutPanel45.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel45.Controls.Add(this.radio特秉质);
            this.flowLayoutPanel45.Location = new System.Drawing.Point(100, -226);
            this.flowLayoutPanel45.Name = "flowLayoutPanel45";
            this.flowLayoutPanel45.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel45.TabIndex = 144;
            // 
            // radio特秉质
            // 
            this.radio特秉质.Location = new System.Drawing.Point(3, 0);
            this.radio特秉质.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio特秉质.Name = "radio特秉质";
            this.radio特秉质.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "倾向是")});
            this.radio特秉质.Size = new System.Drawing.Size(151, 22);
            this.radio特秉质.TabIndex = 2;
            // 
            // flowLayoutPanel44
            // 
            this.flowLayoutPanel44.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel44.Controls.Add(this.radio气郁质);
            this.flowLayoutPanel44.Location = new System.Drawing.Point(100, -250);
            this.flowLayoutPanel44.Name = "flowLayoutPanel44";
            this.flowLayoutPanel44.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel44.TabIndex = 143;
            // 
            // radio气郁质
            // 
            this.radio气郁质.Location = new System.Drawing.Point(3, 0);
            this.radio气郁质.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio气郁质.Name = "radio气郁质";
            this.radio气郁质.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "倾向是")});
            this.radio气郁质.Size = new System.Drawing.Size(151, 22);
            this.radio气郁质.TabIndex = 1;
            // 
            // flow眼部疾病
            // 
            this.flow眼部疾病.BackColor = System.Drawing.Color.White;
            this.flow眼部疾病.Controls.Add(this.chk眼部疾病_未发现);
            this.flow眼部疾病.Controls.Add(this.chk眼部疾病_视网膜出血或渗出);
            this.flow眼部疾病.Controls.Add(this.chk眼部疾病_视乳头水肿);
            this.flow眼部疾病.Controls.Add(this.chk眼部疾病_白内障);
            this.flow眼部疾病.Controls.Add(this.chk眼部疾病_其他);
            this.flow眼部疾病.Controls.Add(this.txt眼部疾病_其他);
            this.flow眼部疾病.Location = new System.Drawing.Point(108, -237);
            this.flow眼部疾病.Name = "flow眼部疾病";
            this.flow眼部疾病.Size = new System.Drawing.Size(662, 22);
            this.flow眼部疾病.TabIndex = 149;
            // 
            // chk眼部疾病_未发现
            // 
            this.chk眼部疾病_未发现.EditValue = true;
            this.chk眼部疾病_未发现.Location = new System.Drawing.Point(0, 0);
            this.chk眼部疾病_未发现.Margin = new System.Windows.Forms.Padding(0);
            this.chk眼部疾病_未发现.Name = "chk眼部疾病_未发现";
            this.chk眼部疾病_未发现.Properties.Caption = "未发现";
            this.chk眼部疾病_未发现.Size = new System.Drawing.Size(63, 19);
            this.chk眼部疾病_未发现.TabIndex = 0;
            this.chk眼部疾病_未发现.Tag = "1";
            this.chk眼部疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk眼部疾病_视网膜出血或渗出
            // 
            this.chk眼部疾病_视网膜出血或渗出.Location = new System.Drawing.Point(63, 0);
            this.chk眼部疾病_视网膜出血或渗出.Margin = new System.Windows.Forms.Padding(0);
            this.chk眼部疾病_视网膜出血或渗出.Name = "chk眼部疾病_视网膜出血或渗出";
            this.chk眼部疾病_视网膜出血或渗出.Properties.Caption = "视网膜出血或渗出";
            this.chk眼部疾病_视网膜出血或渗出.Size = new System.Drawing.Size(128, 19);
            this.chk眼部疾病_视网膜出血或渗出.TabIndex = 1;
            this.chk眼部疾病_视网膜出血或渗出.Tag = "2";
            // 
            // chk眼部疾病_视乳头水肿
            // 
            this.chk眼部疾病_视乳头水肿.Location = new System.Drawing.Point(191, 0);
            this.chk眼部疾病_视乳头水肿.Margin = new System.Windows.Forms.Padding(0);
            this.chk眼部疾病_视乳头水肿.Name = "chk眼部疾病_视乳头水肿";
            this.chk眼部疾病_视乳头水肿.Properties.Caption = "视乳头水肿";
            this.chk眼部疾病_视乳头水肿.Size = new System.Drawing.Size(94, 19);
            this.chk眼部疾病_视乳头水肿.TabIndex = 2;
            this.chk眼部疾病_视乳头水肿.Tag = "3";
            // 
            // chk眼部疾病_白内障
            // 
            this.chk眼部疾病_白内障.Location = new System.Drawing.Point(285, 0);
            this.chk眼部疾病_白内障.Margin = new System.Windows.Forms.Padding(0);
            this.chk眼部疾病_白内障.Name = "chk眼部疾病_白内障";
            this.chk眼部疾病_白内障.Properties.Caption = "白内障";
            this.chk眼部疾病_白内障.Size = new System.Drawing.Size(69, 19);
            this.chk眼部疾病_白内障.TabIndex = 3;
            this.chk眼部疾病_白内障.Tag = "4";
            // 
            // chk眼部疾病_其他
            // 
            this.chk眼部疾病_其他.Location = new System.Drawing.Point(354, 0);
            this.chk眼部疾病_其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk眼部疾病_其他.Name = "chk眼部疾病_其他";
            this.chk眼部疾病_其他.Properties.Caption = "其他";
            this.chk眼部疾病_其他.Size = new System.Drawing.Size(55, 19);
            this.chk眼部疾病_其他.TabIndex = 5;
            this.chk眼部疾病_其他.Tag = "99";
            this.chk眼部疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt眼部疾病_其他
            // 
            this.txt眼部疾病_其他.Location = new System.Drawing.Point(412, 0);
            this.txt眼部疾病_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt眼部疾病_其他.Name = "txt眼部疾病_其他";
            this.txt眼部疾病_其他.Size = new System.Drawing.Size(197, 20);
            this.txt眼部疾病_其他.TabIndex = 6;
            // 
            // flow血管疾病
            // 
            this.flow血管疾病.BackColor = System.Drawing.Color.White;
            this.flow血管疾病.Controls.Add(this.chk血管疾病_未发现);
            this.flow血管疾病.Controls.Add(this.chk血管疾病_夹层动脉瘤);
            this.flow血管疾病.Controls.Add(this.chk血管疾病_动脉闭塞性疾病);
            this.flow血管疾病.Controls.Add(this.chk血管疾病_其他);
            this.flow血管疾病.Controls.Add(this.txt血管疾病_其他);
            this.flow血管疾病.Location = new System.Drawing.Point(108, 100);
            this.flow血管疾病.Name = "flow血管疾病";
            this.flow血管疾病.Size = new System.Drawing.Size(662, 22);
            this.flow血管疾病.TabIndex = 148;
            // 
            // chk血管疾病_未发现
            // 
            this.chk血管疾病_未发现.EditValue = true;
            this.chk血管疾病_未发现.Location = new System.Drawing.Point(0, 0);
            this.chk血管疾病_未发现.Margin = new System.Windows.Forms.Padding(0);
            this.chk血管疾病_未发现.Name = "chk血管疾病_未发现";
            this.chk血管疾病_未发现.Properties.Caption = "未发现";
            this.chk血管疾病_未发现.Size = new System.Drawing.Size(63, 19);
            this.chk血管疾病_未发现.TabIndex = 0;
            this.chk血管疾病_未发现.Tag = "1";
            this.chk血管疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk血管疾病_夹层动脉瘤
            // 
            this.chk血管疾病_夹层动脉瘤.Location = new System.Drawing.Point(63, 0);
            this.chk血管疾病_夹层动脉瘤.Margin = new System.Windows.Forms.Padding(0);
            this.chk血管疾病_夹层动脉瘤.Name = "chk血管疾病_夹层动脉瘤";
            this.chk血管疾病_夹层动脉瘤.Properties.Caption = "夹层动脉瘤";
            this.chk血管疾病_夹层动脉瘤.Size = new System.Drawing.Size(99, 19);
            this.chk血管疾病_夹层动脉瘤.TabIndex = 1;
            this.chk血管疾病_夹层动脉瘤.Tag = "2";
            // 
            // chk血管疾病_动脉闭塞性疾病
            // 
            this.chk血管疾病_动脉闭塞性疾病.Location = new System.Drawing.Point(162, 0);
            this.chk血管疾病_动脉闭塞性疾病.Margin = new System.Windows.Forms.Padding(0);
            this.chk血管疾病_动脉闭塞性疾病.Name = "chk血管疾病_动脉闭塞性疾病";
            this.chk血管疾病_动脉闭塞性疾病.Properties.Caption = "动脉闭塞性疾病";
            this.chk血管疾病_动脉闭塞性疾病.Size = new System.Drawing.Size(117, 19);
            this.chk血管疾病_动脉闭塞性疾病.TabIndex = 2;
            this.chk血管疾病_动脉闭塞性疾病.Tag = "3";
            // 
            // chk血管疾病_其他
            // 
            this.chk血管疾病_其他.Location = new System.Drawing.Point(279, 0);
            this.chk血管疾病_其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk血管疾病_其他.Name = "chk血管疾病_其他";
            this.chk血管疾病_其他.Properties.Caption = "其他";
            this.chk血管疾病_其他.Size = new System.Drawing.Size(55, 19);
            this.chk血管疾病_其他.TabIndex = 5;
            this.chk血管疾病_其他.Tag = "99";
            this.chk血管疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt血管疾病_其他
            // 
            this.txt血管疾病_其他.Location = new System.Drawing.Point(337, 0);
            this.txt血管疾病_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt血管疾病_其他.Name = "txt血管疾病_其他";
            this.txt血管疾病_其他.Size = new System.Drawing.Size(100, 20);
            this.txt血管疾病_其他.TabIndex = 6;
            // 
            // flow心血管疾病
            // 
            this.flow心血管疾病.BackColor = System.Drawing.Color.White;
            this.flow心血管疾病.Controls.Add(this.chk心血管疾病_未发现);
            this.flow心血管疾病.Controls.Add(this.chk心血管疾病_心肌梗死);
            this.flow心血管疾病.Controls.Add(this.chk心血管疾病_心绞痛);
            this.flow心血管疾病.Controls.Add(this.chk心血管疾病_冠状动脉血运重建);
            this.flow心血管疾病.Controls.Add(this.chk心血管疾病_充血性心力衰竭);
            this.flow心血管疾病.Controls.Add(this.chk心血管疾病_心前区疼痛);
            this.flow心血管疾病.Controls.Add(this.chk心血管疾病_高血压);
            this.flow心血管疾病.Controls.Add(this.chk心血管疾病_夹层动脉瘤);
            this.flow心血管疾病.Controls.Add(this.chk心血管疾病_动脉闭塞性疾病);
            this.flow心血管疾病.Controls.Add(this.chk心血管疾病_其他);
            this.flow心血管疾病.Controls.Add(this.txt心血管疾病_其他);
            this.flow心血管疾病.Location = new System.Drawing.Point(108, -287);
            this.flow心血管疾病.Name = "flow心血管疾病";
            this.flow心血管疾病.Size = new System.Drawing.Size(662, 46);
            this.flow心血管疾病.TabIndex = 147;
            // 
            // chk心血管疾病_未发现
            // 
            this.chk心血管疾病_未发现.EditValue = true;
            this.chk心血管疾病_未发现.Location = new System.Drawing.Point(0, 0);
            this.chk心血管疾病_未发现.Margin = new System.Windows.Forms.Padding(0);
            this.chk心血管疾病_未发现.Name = "chk心血管疾病_未发现";
            this.chk心血管疾病_未发现.Properties.Caption = "未发现";
            this.chk心血管疾病_未发现.Size = new System.Drawing.Size(63, 19);
            this.chk心血管疾病_未发现.TabIndex = 0;
            this.chk心血管疾病_未发现.Tag = "1";
            this.chk心血管疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk心血管疾病_心肌梗死
            // 
            this.chk心血管疾病_心肌梗死.Location = new System.Drawing.Point(63, 0);
            this.chk心血管疾病_心肌梗死.Margin = new System.Windows.Forms.Padding(0);
            this.chk心血管疾病_心肌梗死.Name = "chk心血管疾病_心肌梗死";
            this.chk心血管疾病_心肌梗死.Properties.Caption = "心肌梗死";
            this.chk心血管疾病_心肌梗死.Size = new System.Drawing.Size(71, 19);
            this.chk心血管疾病_心肌梗死.TabIndex = 1;
            this.chk心血管疾病_心肌梗死.Tag = "2";
            // 
            // chk心血管疾病_心绞痛
            // 
            this.chk心血管疾病_心绞痛.Location = new System.Drawing.Point(134, 0);
            this.chk心血管疾病_心绞痛.Margin = new System.Windows.Forms.Padding(0);
            this.chk心血管疾病_心绞痛.Name = "chk心血管疾病_心绞痛";
            this.chk心血管疾病_心绞痛.Properties.Caption = "心绞痛";
            this.chk心血管疾病_心绞痛.Size = new System.Drawing.Size(68, 19);
            this.chk心血管疾病_心绞痛.TabIndex = 2;
            this.chk心血管疾病_心绞痛.Tag = "3";
            // 
            // chk心血管疾病_冠状动脉血运重建
            // 
            this.chk心血管疾病_冠状动脉血运重建.Location = new System.Drawing.Point(202, 0);
            this.chk心血管疾病_冠状动脉血运重建.Margin = new System.Windows.Forms.Padding(0);
            this.chk心血管疾病_冠状动脉血运重建.Name = "chk心血管疾病_冠状动脉血运重建";
            this.chk心血管疾病_冠状动脉血运重建.Properties.Caption = "冠状动脉血运重建";
            this.chk心血管疾病_冠状动脉血运重建.Size = new System.Drawing.Size(129, 19);
            this.chk心血管疾病_冠状动脉血运重建.TabIndex = 3;
            this.chk心血管疾病_冠状动脉血运重建.Tag = "4";
            // 
            // chk心血管疾病_充血性心力衰竭
            // 
            this.chk心血管疾病_充血性心力衰竭.Location = new System.Drawing.Point(331, 0);
            this.chk心血管疾病_充血性心力衰竭.Margin = new System.Windows.Forms.Padding(0);
            this.chk心血管疾病_充血性心力衰竭.Name = "chk心血管疾病_充血性心力衰竭";
            this.chk心血管疾病_充血性心力衰竭.Properties.Caption = "充血性心力衰竭";
            this.chk心血管疾病_充血性心力衰竭.Size = new System.Drawing.Size(115, 19);
            this.chk心血管疾病_充血性心力衰竭.TabIndex = 4;
            this.chk心血管疾病_充血性心力衰竭.Tag = "5";
            // 
            // chk心血管疾病_心前区疼痛
            // 
            this.chk心血管疾病_心前区疼痛.Location = new System.Drawing.Point(446, 0);
            this.chk心血管疾病_心前区疼痛.Margin = new System.Windows.Forms.Padding(0);
            this.chk心血管疾病_心前区疼痛.Name = "chk心血管疾病_心前区疼痛";
            this.chk心血管疾病_心前区疼痛.Properties.Caption = "心前区疼痛";
            this.chk心血管疾病_心前区疼痛.Size = new System.Drawing.Size(89, 19);
            this.chk心血管疾病_心前区疼痛.TabIndex = 5;
            this.chk心血管疾病_心前区疼痛.Tag = "6";
            // 
            // chk心血管疾病_高血压
            // 
            this.chk心血管疾病_高血压.Location = new System.Drawing.Point(538, 3);
            this.chk心血管疾病_高血压.Name = "chk心血管疾病_高血压";
            this.chk心血管疾病_高血压.Properties.Caption = "高血压";
            this.chk心血管疾病_高血压.Size = new System.Drawing.Size(75, 19);
            this.chk心血管疾病_高血压.TabIndex = 6;
            this.chk心血管疾病_高血压.Tag = "7";
            // 
            // chk心血管疾病_夹层动脉瘤
            // 
            this.chk心血管疾病_夹层动脉瘤.Location = new System.Drawing.Point(3, 28);
            this.chk心血管疾病_夹层动脉瘤.Name = "chk心血管疾病_夹层动脉瘤";
            this.chk心血管疾病_夹层动脉瘤.Properties.Caption = "夹层动脉瘤";
            this.chk心血管疾病_夹层动脉瘤.Size = new System.Drawing.Size(97, 19);
            this.chk心血管疾病_夹层动脉瘤.TabIndex = 7;
            this.chk心血管疾病_夹层动脉瘤.Tag = "8";
            // 
            // chk心血管疾病_动脉闭塞性疾病
            // 
            this.chk心血管疾病_动脉闭塞性疾病.Location = new System.Drawing.Point(106, 28);
            this.chk心血管疾病_动脉闭塞性疾病.Name = "chk心血管疾病_动脉闭塞性疾病";
            this.chk心血管疾病_动脉闭塞性疾病.Properties.Caption = "动脉闭塞性疾病";
            this.chk心血管疾病_动脉闭塞性疾病.Size = new System.Drawing.Size(119, 19);
            this.chk心血管疾病_动脉闭塞性疾病.TabIndex = 8;
            this.chk心血管疾病_动脉闭塞性疾病.Tag = "9";
            // 
            // chk心血管疾病_其他
            // 
            this.chk心血管疾病_其他.Location = new System.Drawing.Point(228, 25);
            this.chk心血管疾病_其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk心血管疾病_其他.Name = "chk心血管疾病_其他";
            this.chk心血管疾病_其他.Properties.Caption = "其他";
            this.chk心血管疾病_其他.Size = new System.Drawing.Size(55, 19);
            this.chk心血管疾病_其他.TabIndex = 9;
            this.chk心血管疾病_其他.Tag = "99";
            this.chk心血管疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt心血管疾病_其他
            // 
            this.txt心血管疾病_其他.Location = new System.Drawing.Point(286, 25);
            this.txt心血管疾病_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt心血管疾病_其他.Name = "txt心血管疾病_其他";
            this.txt心血管疾病_其他.Size = new System.Drawing.Size(151, 20);
            this.txt心血管疾病_其他.TabIndex = 10;
            // 
            // flow肾脏疾病
            // 
            this.flow肾脏疾病.BackColor = System.Drawing.Color.White;
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_未发现);
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_糖尿病肾病);
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_肾功能衰竭);
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_急性肾炎);
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_慢性肾炎);
            this.flow肾脏疾病.Controls.Add(this.chk肾脏疾病_其他);
            this.flow肾脏疾病.Controls.Add(this.txt肾脏疾病_其他);
            this.flow肾脏疾病.Location = new System.Drawing.Point(108, -337);
            this.flow肾脏疾病.Name = "flow肾脏疾病";
            this.flow肾脏疾病.Size = new System.Drawing.Size(662, 46);
            this.flow肾脏疾病.TabIndex = 146;
            // 
            // chk肾脏疾病_未发现
            // 
            this.chk肾脏疾病_未发现.EditValue = true;
            this.chk肾脏疾病_未发现.Location = new System.Drawing.Point(0, 0);
            this.chk肾脏疾病_未发现.Margin = new System.Windows.Forms.Padding(0);
            this.chk肾脏疾病_未发现.Name = "chk肾脏疾病_未发现";
            this.chk肾脏疾病_未发现.Properties.Caption = "未发现";
            this.chk肾脏疾病_未发现.Size = new System.Drawing.Size(63, 19);
            this.chk肾脏疾病_未发现.TabIndex = 0;
            this.chk肾脏疾病_未发现.Tag = "1";
            this.chk肾脏疾病_未发现.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk肾脏疾病_糖尿病肾病
            // 
            this.chk肾脏疾病_糖尿病肾病.Location = new System.Drawing.Point(63, 0);
            this.chk肾脏疾病_糖尿病肾病.Margin = new System.Windows.Forms.Padding(0);
            this.chk肾脏疾病_糖尿病肾病.Name = "chk肾脏疾病_糖尿病肾病";
            this.chk肾脏疾病_糖尿病肾病.Properties.Caption = "糖尿病肾病";
            this.chk肾脏疾病_糖尿病肾病.Size = new System.Drawing.Size(94, 19);
            this.chk肾脏疾病_糖尿病肾病.TabIndex = 1;
            this.chk肾脏疾病_糖尿病肾病.Tag = "2";
            // 
            // chk肾脏疾病_肾功能衰竭
            // 
            this.chk肾脏疾病_肾功能衰竭.Location = new System.Drawing.Point(157, 0);
            this.chk肾脏疾病_肾功能衰竭.Margin = new System.Windows.Forms.Padding(0);
            this.chk肾脏疾病_肾功能衰竭.Name = "chk肾脏疾病_肾功能衰竭";
            this.chk肾脏疾病_肾功能衰竭.Properties.Caption = "肾功能衰竭";
            this.chk肾脏疾病_肾功能衰竭.Size = new System.Drawing.Size(87, 19);
            this.chk肾脏疾病_肾功能衰竭.TabIndex = 2;
            this.chk肾脏疾病_肾功能衰竭.Tag = "3";
            // 
            // chk肾脏疾病_急性肾炎
            // 
            this.chk肾脏疾病_急性肾炎.Location = new System.Drawing.Point(244, 0);
            this.chk肾脏疾病_急性肾炎.Margin = new System.Windows.Forms.Padding(0);
            this.chk肾脏疾病_急性肾炎.Name = "chk肾脏疾病_急性肾炎";
            this.chk肾脏疾病_急性肾炎.Properties.Caption = "急性肾炎";
            this.chk肾脏疾病_急性肾炎.Size = new System.Drawing.Size(76, 19);
            this.chk肾脏疾病_急性肾炎.TabIndex = 3;
            this.chk肾脏疾病_急性肾炎.Tag = "4";
            // 
            // chk肾脏疾病_慢性肾炎
            // 
            this.chk肾脏疾病_慢性肾炎.Location = new System.Drawing.Point(320, 0);
            this.chk肾脏疾病_慢性肾炎.Margin = new System.Windows.Forms.Padding(0);
            this.chk肾脏疾病_慢性肾炎.Name = "chk肾脏疾病_慢性肾炎";
            this.chk肾脏疾病_慢性肾炎.Properties.Caption = "慢性肾炎";
            this.chk肾脏疾病_慢性肾炎.Size = new System.Drawing.Size(78, 19);
            this.chk肾脏疾病_慢性肾炎.TabIndex = 4;
            this.chk肾脏疾病_慢性肾炎.Tag = "5";
            // 
            // chk肾脏疾病_其他
            // 
            this.chk肾脏疾病_其他.Location = new System.Drawing.Point(398, 0);
            this.chk肾脏疾病_其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk肾脏疾病_其他.Name = "chk肾脏疾病_其他";
            this.chk肾脏疾病_其他.Properties.Caption = "其他";
            this.chk肾脏疾病_其他.Size = new System.Drawing.Size(55, 19);
            this.chk肾脏疾病_其他.TabIndex = 5;
            this.chk肾脏疾病_其他.Tag = "99";
            this.chk肾脏疾病_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt肾脏疾病_其他
            // 
            this.txt肾脏疾病_其他.Location = new System.Drawing.Point(456, 0);
            this.txt肾脏疾病_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt肾脏疾病_其他.Name = "txt肾脏疾病_其他";
            this.txt肾脏疾病_其他.Size = new System.Drawing.Size(153, 20);
            this.txt肾脏疾病_其他.TabIndex = 6;
            // 
            // flowLayoutPanel43
            // 
            this.flowLayoutPanel43.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel43.Controls.Add(this.radio血瘀质);
            this.flowLayoutPanel43.Location = new System.Drawing.Point(100, -274);
            this.flowLayoutPanel43.Name = "flowLayoutPanel43";
            this.flowLayoutPanel43.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel43.TabIndex = 142;
            // 
            // radio血瘀质
            // 
            this.radio血瘀质.Location = new System.Drawing.Point(3, 0);
            this.radio血瘀质.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio血瘀质.Name = "radio血瘀质";
            this.radio血瘀质.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "倾向是")});
            this.radio血瘀质.Size = new System.Drawing.Size(151, 22);
            this.radio血瘀质.TabIndex = 1;
            // 
            // flowLayoutPanel42
            // 
            this.flowLayoutPanel42.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel42.Controls.Add(this.radio湿热质);
            this.flowLayoutPanel42.Location = new System.Drawing.Point(100, -298);
            this.flowLayoutPanel42.Name = "flowLayoutPanel42";
            this.flowLayoutPanel42.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel42.TabIndex = 141;
            // 
            // radio湿热质
            // 
            this.radio湿热质.Location = new System.Drawing.Point(3, 0);
            this.radio湿热质.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio湿热质.Name = "radio湿热质";
            this.radio湿热质.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "倾向是")});
            this.radio湿热质.Size = new System.Drawing.Size(151, 22);
            this.radio湿热质.TabIndex = 1;
            // 
            // flowLayoutPanel41
            // 
            this.flowLayoutPanel41.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel41.Controls.Add(this.radio痰湿质);
            this.flowLayoutPanel41.Location = new System.Drawing.Point(100, -322);
            this.flowLayoutPanel41.Name = "flowLayoutPanel41";
            this.flowLayoutPanel41.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel41.TabIndex = 140;
            // 
            // radio痰湿质
            // 
            this.radio痰湿质.Location = new System.Drawing.Point(3, 0);
            this.radio痰湿质.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio痰湿质.Name = "radio痰湿质";
            this.radio痰湿质.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "倾向是")});
            this.radio痰湿质.Size = new System.Drawing.Size(151, 22);
            this.radio痰湿质.TabIndex = 1;
            // 
            // flowLayoutPanel40
            // 
            this.flowLayoutPanel40.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel40.Controls.Add(this.radio阴虚质);
            this.flowLayoutPanel40.Location = new System.Drawing.Point(100, -346);
            this.flowLayoutPanel40.Name = "flowLayoutPanel40";
            this.flowLayoutPanel40.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel40.TabIndex = 139;
            // 
            // radio阴虚质
            // 
            this.radio阴虚质.Location = new System.Drawing.Point(3, 0);
            this.radio阴虚质.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio阴虚质.Name = "radio阴虚质";
            this.radio阴虚质.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "倾向是")});
            this.radio阴虚质.Size = new System.Drawing.Size(151, 22);
            this.radio阴虚质.TabIndex = 1;
            // 
            // flowLayoutPanel39
            // 
            this.flowLayoutPanel39.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel39.Controls.Add(this.radio阳虚质);
            this.flowLayoutPanel39.Location = new System.Drawing.Point(100, -370);
            this.flowLayoutPanel39.Name = "flowLayoutPanel39";
            this.flowLayoutPanel39.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel39.TabIndex = 138;
            // 
            // radio阳虚质
            // 
            this.radio阳虚质.Location = new System.Drawing.Point(3, 0);
            this.radio阳虚质.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio阳虚质.Name = "radio阳虚质";
            this.radio阳虚质.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "倾向是")});
            this.radio阳虚质.Size = new System.Drawing.Size(151, 22);
            this.radio阳虚质.TabIndex = 1;
            // 
            // flowLayoutPanel38
            // 
            this.flowLayoutPanel38.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel38.Controls.Add(this.radio气虚质);
            this.flowLayoutPanel38.Location = new System.Drawing.Point(100, -394);
            this.flowLayoutPanel38.Name = "flowLayoutPanel38";
            this.flowLayoutPanel38.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel38.TabIndex = 137;
            // 
            // radio气虚质
            // 
            this.radio气虚质.Location = new System.Drawing.Point(3, 0);
            this.radio气虚质.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio气虚质.Name = "radio气虚质";
            this.radio气虚质.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "倾向是")});
            this.radio气虚质.Size = new System.Drawing.Size(151, 22);
            this.radio气虚质.TabIndex = 0;
            // 
            // txt辅助检查_其他
            // 
            this.txt辅助检查_其他.Location = new System.Drawing.Point(108, -476);
            this.txt辅助检查_其他.Name = "txt辅助检查_其他";
            this.txt辅助检查_其他.Size = new System.Drawing.Size(662, 20);
            this.txt辅助检查_其他.StyleController = this.layoutControl1;
            this.txt辅助检查_其他.TabIndex = 135;
            // 
            // flowLayoutPanel37
            // 
            this.flowLayoutPanel37.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel37.Controls.Add(this.radio平和质);
            this.flowLayoutPanel37.Location = new System.Drawing.Point(100, -418);
            this.flowLayoutPanel37.Name = "flowLayoutPanel37";
            this.flowLayoutPanel37.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel37.TabIndex = 136;
            // 
            // radio平和质
            // 
            this.radio平和质.Location = new System.Drawing.Point(3, 0);
            this.radio平和质.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio平和质.Name = "radio平和质";
            this.radio平和质.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "基本是")});
            this.radio平和质.Size = new System.Drawing.Size(151, 22);
            this.radio平和质.TabIndex = 0;
            // 
            // txt血清高密度脂蛋白胆固醇
            // 
            this.txt血清高密度脂蛋白胆固醇.BackColor = System.Drawing.Color.White;
            this.txt血清高密度脂蛋白胆固醇.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt血清高密度脂蛋白胆固醇.Lbl1Text = "mmol/L";
            this.txt血清高密度脂蛋白胆固醇.Location = new System.Drawing.Point(260, -608);
            this.txt血清高密度脂蛋白胆固醇.Margin = new System.Windows.Forms.Padding(0);
            this.txt血清高密度脂蛋白胆固醇.Name = "txt血清高密度脂蛋白胆固醇";
            this.txt血清高密度脂蛋白胆固醇.Size = new System.Drawing.Size(510, 20);
            this.txt血清高密度脂蛋白胆固醇.TabIndex = 131;
            this.txt血清高密度脂蛋白胆固醇.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel36
            // 
            this.flowLayoutPanel36.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel36.Controls.Add(this.radio宫颈涂片);
            this.flowLayoutPanel36.Controls.Add(this.txt宫颈涂片);
            this.flowLayoutPanel36.Location = new System.Drawing.Point(108, -500);
            this.flowLayoutPanel36.Name = "flowLayoutPanel36";
            this.flowLayoutPanel36.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel36.TabIndex = 134;
            // 
            // radio宫颈涂片
            // 
            this.radio宫颈涂片.EditValue = "3";
            this.radio宫颈涂片.Location = new System.Drawing.Point(3, 0);
            this.radio宫颈涂片.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio宫颈涂片.Name = "radio宫颈涂片";
            this.radio宫颈涂片.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "未检测")});
            this.radio宫颈涂片.Size = new System.Drawing.Size(172, 22);
            this.radio宫颈涂片.TabIndex = 98;
            this.radio宫颈涂片.SelectedIndexChanged += new System.EventHandler(this.radio宫颈涂片_SelectedIndexChanged);
            // 
            // txt宫颈涂片
            // 
            this.txt宫颈涂片.Enabled = false;
            this.txt宫颈涂片.Location = new System.Drawing.Point(175, 0);
            this.txt宫颈涂片.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt宫颈涂片.Name = "txt宫颈涂片";
            this.txt宫颈涂片.Size = new System.Drawing.Size(184, 20);
            this.txt宫颈涂片.TabIndex = 99;
            // 
            // flowLayoutPanel35
            // 
            this.flowLayoutPanel35.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel35.Controls.Add(this.radio腹部B超);
            this.flowLayoutPanel35.Controls.Add(this.txt腹部B超异常);
            this.flowLayoutPanel35.Location = new System.Drawing.Point(208, -560);
            this.flowLayoutPanel35.Name = "flowLayoutPanel35";
            this.flowLayoutPanel35.Size = new System.Drawing.Size(562, 26);
            this.flowLayoutPanel35.TabIndex = 133;
            // 
            // radio腹部B超
            // 
            this.radio腹部B超.EditValue = "3";
            this.radio腹部B超.Location = new System.Drawing.Point(3, 0);
            this.radio腹部B超.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio腹部B超.Name = "radio腹部B超";
            this.radio腹部B超.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "未检测")});
            this.radio腹部B超.Size = new System.Drawing.Size(189, 22);
            this.radio腹部B超.TabIndex = 96;
            this.radio腹部B超.SelectedIndexChanged += new System.EventHandler(this.radioB超_SelectedIndexChanged);
            // 
            // txt腹部B超异常
            // 
            this.txt腹部B超异常.Enabled = false;
            this.txt腹部B超异常.Location = new System.Drawing.Point(192, 0);
            this.txt腹部B超异常.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt腹部B超异常.Name = "txt腹部B超异常";
            this.txt腹部B超异常.Size = new System.Drawing.Size(250, 20);
            this.txt腹部B超异常.TabIndex = 97;
            // 
            // flowLayoutPanel34
            // 
            this.flowLayoutPanel34.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel34.Controls.Add(this.radio胸部X线片);
            this.flowLayoutPanel34.Controls.Add(this.txt胸部X线片);
            this.flowLayoutPanel34.Location = new System.Drawing.Point(108, -584);
            this.flowLayoutPanel34.Name = "flowLayoutPanel34";
            this.flowLayoutPanel34.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel34.TabIndex = 132;
            // 
            // radio胸部X线片
            // 
            this.radio胸部X线片.EditValue = "3";
            this.radio胸部X线片.Location = new System.Drawing.Point(3, 0);
            this.radio胸部X线片.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio胸部X线片.Name = "radio胸部X线片";
            this.radio胸部X线片.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "未检测")});
            this.radio胸部X线片.Size = new System.Drawing.Size(172, 22);
            this.radio胸部X线片.TabIndex = 94;
            this.radio胸部X线片.SelectedIndexChanged += new System.EventHandler(this.radio胸部X线片_SelectedIndexChanged);
            // 
            // txt胸部X线片
            // 
            this.txt胸部X线片.Enabled = false;
            this.txt胸部X线片.Location = new System.Drawing.Point(175, 0);
            this.txt胸部X线片.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt胸部X线片.Name = "txt胸部X线片";
            this.txt胸部X线片.Size = new System.Drawing.Size(184, 20);
            this.txt胸部X线片.TabIndex = 95;
            // 
            // txt甘油三酯
            // 
            this.txt甘油三酯.BackColor = System.Drawing.Color.White;
            this.txt甘油三酯.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt甘油三酯.Lbl1Text = "mmol/L";
            this.txt甘油三酯.Location = new System.Drawing.Point(260, -656);
            this.txt甘油三酯.Margin = new System.Windows.Forms.Padding(0);
            this.txt甘油三酯.Name = "txt甘油三酯";
            this.txt甘油三酯.Size = new System.Drawing.Size(510, 20);
            this.txt甘油三酯.TabIndex = 129;
            this.txt甘油三酯.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt血清低密度脂蛋白胆固醇
            // 
            this.txt血清低密度脂蛋白胆固醇.BackColor = System.Drawing.Color.White;
            this.txt血清低密度脂蛋白胆固醇.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt血清低密度脂蛋白胆固醇.Lbl1Text = "mmol/L";
            this.txt血清低密度脂蛋白胆固醇.Location = new System.Drawing.Point(260, -632);
            this.txt血清低密度脂蛋白胆固醇.Margin = new System.Windows.Forms.Padding(0);
            this.txt血清低密度脂蛋白胆固醇.Name = "txt血清低密度脂蛋白胆固醇";
            this.txt血清低密度脂蛋白胆固醇.Size = new System.Drawing.Size(510, 20);
            this.txt血清低密度脂蛋白胆固醇.TabIndex = 130;
            this.txt血清低密度脂蛋白胆固醇.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt总胆固醇
            // 
            this.txt总胆固醇.BackColor = System.Drawing.Color.White;
            this.txt总胆固醇.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt总胆固醇.Lbl1Text = "mmol/L";
            this.txt总胆固醇.Location = new System.Drawing.Point(260, -680);
            this.txt总胆固醇.Margin = new System.Windows.Forms.Padding(0);
            this.txt总胆固醇.Name = "txt总胆固醇";
            this.txt总胆固醇.Size = new System.Drawing.Size(510, 20);
            this.txt总胆固醇.TabIndex = 128;
            this.txt总胆固醇.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt血钾浓度
            // 
            this.txt血钾浓度.BackColor = System.Drawing.Color.White;
            this.txt血钾浓度.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt血钾浓度.Lbl1Text = "umol/L";
            this.txt血钾浓度.Location = new System.Drawing.Point(260, -728);
            this.txt血钾浓度.Margin = new System.Windows.Forms.Padding(0);
            this.txt血钾浓度.Name = "txt血钾浓度";
            this.txt血钾浓度.Size = new System.Drawing.Size(510, 20);
            this.txt血钾浓度.TabIndex = 126;
            this.txt血钾浓度.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt血钠浓度
            // 
            this.txt血钠浓度.BackColor = System.Drawing.Color.White;
            this.txt血钠浓度.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt血钠浓度.Lbl1Text = "umol/L";
            this.txt血钠浓度.Location = new System.Drawing.Point(260, -704);
            this.txt血钠浓度.Margin = new System.Windows.Forms.Padding(0);
            this.txt血钠浓度.Name = "txt血钠浓度";
            this.txt血钠浓度.Size = new System.Drawing.Size(510, 20);
            this.txt血钠浓度.TabIndex = 127;
            this.txt血钠浓度.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt血尿素氮
            // 
            this.txt血尿素氮.BackColor = System.Drawing.Color.White;
            this.txt血尿素氮.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt血尿素氮.Lbl1Text = "umol/L";
            this.txt血尿素氮.Location = new System.Drawing.Point(260, -752);
            this.txt血尿素氮.Margin = new System.Windows.Forms.Padding(0);
            this.txt血尿素氮.Name = "txt血尿素氮";
            this.txt血尿素氮.Size = new System.Drawing.Size(510, 20);
            this.txt血尿素氮.TabIndex = 125;
            this.txt血尿素氮.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt血清肌酐
            // 
            this.txt血清肌酐.BackColor = System.Drawing.Color.White;
            this.txt血清肌酐.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt血清肌酐.Lbl1Text = "umol/L";
            this.txt血清肌酐.Location = new System.Drawing.Point(260, -776);
            this.txt血清肌酐.Margin = new System.Windows.Forms.Padding(0);
            this.txt血清肌酐.Name = "txt血清肌酐";
            this.txt血清肌酐.Size = new System.Drawing.Size(510, 20);
            this.txt血清肌酐.TabIndex = 124;
            this.txt血清肌酐.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt血清谷草转氨酶
            // 
            this.txt血清谷草转氨酶.BackColor = System.Drawing.Color.White;
            this.txt血清谷草转氨酶.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt血清谷草转氨酶.Lbl1Text = "U/L";
            this.txt血清谷草转氨酶.Location = new System.Drawing.Point(260, -872);
            this.txt血清谷草转氨酶.Margin = new System.Windows.Forms.Padding(0);
            this.txt血清谷草转氨酶.Name = "txt血清谷草转氨酶";
            this.txt血清谷草转氨酶.Size = new System.Drawing.Size(510, 20);
            this.txt血清谷草转氨酶.TabIndex = 120;
            this.txt血清谷草转氨酶.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt白蛋白
            // 
            this.txt白蛋白.BackColor = System.Drawing.Color.White;
            this.txt白蛋白.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt白蛋白.Lbl1Text = "g/L";
            this.txt白蛋白.Location = new System.Drawing.Point(260, -848);
            this.txt白蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt白蛋白.Name = "txt白蛋白";
            this.txt白蛋白.Size = new System.Drawing.Size(510, 20);
            this.txt白蛋白.TabIndex = 121;
            this.txt白蛋白.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt总胆红素
            // 
            this.txt总胆红素.BackColor = System.Drawing.Color.White;
            this.txt总胆红素.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt总胆红素.Lbl1Text = "umol/L";
            this.txt总胆红素.Location = new System.Drawing.Point(260, -824);
            this.txt总胆红素.Margin = new System.Windows.Forms.Padding(0);
            this.txt总胆红素.Name = "txt总胆红素";
            this.txt总胆红素.Size = new System.Drawing.Size(510, 20);
            this.txt总胆红素.TabIndex = 122;
            this.txt总胆红素.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt结合胆红素
            // 
            this.txt结合胆红素.BackColor = System.Drawing.Color.White;
            this.txt结合胆红素.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt结合胆红素.Lbl1Text = "umol/L";
            this.txt结合胆红素.Location = new System.Drawing.Point(260, -800);
            this.txt结合胆红素.Margin = new System.Windows.Forms.Padding(0);
            this.txt结合胆红素.Name = "txt结合胆红素";
            this.txt结合胆红素.Size = new System.Drawing.Size(510, 20);
            this.txt结合胆红素.TabIndex = 123;
            this.txt结合胆红素.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt血清谷丙转氨酶
            // 
            this.txt血清谷丙转氨酶.BackColor = System.Drawing.Color.White;
            this.txt血清谷丙转氨酶.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt血清谷丙转氨酶.Lbl1Text = "U/L";
            this.txt血清谷丙转氨酶.Location = new System.Drawing.Point(260, -896);
            this.txt血清谷丙转氨酶.Margin = new System.Windows.Forms.Padding(0);
            this.txt血清谷丙转氨酶.Name = "txt血清谷丙转氨酶";
            this.txt血清谷丙转氨酶.Size = new System.Drawing.Size(510, 20);
            this.txt血清谷丙转氨酶.TabIndex = 119;
            this.txt血清谷丙转氨酶.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel33
            // 
            this.flowLayoutPanel33.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel33.Controls.Add(this.radio乙型肝炎表面抗原);
            this.flowLayoutPanel33.Location = new System.Drawing.Point(108, -928);
            this.flowLayoutPanel33.Name = "flowLayoutPanel33";
            this.flowLayoutPanel33.Size = new System.Drawing.Size(662, 28);
            this.flowLayoutPanel33.TabIndex = 118;
            // 
            // radio乙型肝炎表面抗原
            // 
            this.radio乙型肝炎表面抗原.EditValue = "3";
            this.radio乙型肝炎表面抗原.Location = new System.Drawing.Point(3, 0);
            this.radio乙型肝炎表面抗原.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio乙型肝炎表面抗原.Name = "radio乙型肝炎表面抗原";
            this.radio乙型肝炎表面抗原.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "阴性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "阳性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "未检测")});
            this.radio乙型肝炎表面抗原.Size = new System.Drawing.Size(172, 28);
            this.radio乙型肝炎表面抗原.TabIndex = 80;
            // 
            // txt糖化血红蛋白
            // 
            this.txt糖化血红蛋白.BackColor = System.Drawing.Color.White;
            this.txt糖化血红蛋白.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt糖化血红蛋白.Lbl1Text = "% ";
            this.txt糖化血红蛋白.Location = new System.Drawing.Point(108, -952);
            this.txt糖化血红蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt糖化血红蛋白.Name = "txt糖化血红蛋白";
            this.txt糖化血红蛋白.Size = new System.Drawing.Size(662, 20);
            this.txt糖化血红蛋白.TabIndex = 117;
            this.txt糖化血红蛋白.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt尿微量白蛋白
            // 
            this.txt尿微量白蛋白.BackColor = System.Drawing.Color.White;
            this.txt尿微量白蛋白.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt尿微量白蛋白.Lbl1Text = "mg/dL ";
            this.txt尿微量白蛋白.Location = new System.Drawing.Point(108, -1000);
            this.txt尿微量白蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt尿微量白蛋白.Name = "txt尿微量白蛋白";
            this.txt尿微量白蛋白.Size = new System.Drawing.Size(662, 20);
            this.txt尿微量白蛋白.TabIndex = 115;
            this.txt尿微量白蛋白.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel32
            // 
            this.flowLayoutPanel32.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel32.Controls.Add(this.radio大便潜血);
            this.flowLayoutPanel32.Location = new System.Drawing.Point(108, -976);
            this.flowLayoutPanel32.Name = "flowLayoutPanel32";
            this.flowLayoutPanel32.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel32.TabIndex = 116;
            // 
            // radio大便潜血
            // 
            this.radio大便潜血.EditValue = "3";
            this.radio大便潜血.Location = new System.Drawing.Point(3, 0);
            this.radio大便潜血.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio大便潜血.Name = "radio大便潜血";
            this.radio大便潜血.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "阴性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "阳性"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "未检测")});
            this.radio大便潜血.Size = new System.Drawing.Size(172, 22);
            this.radio大便潜血.TabIndex = 78;
            // 
            // flow心电图
            // 
            this.flow心电图.BackColor = System.Drawing.Color.White;
            this.flow心电图.Controls.Add(this.chk心电图_正常);
            this.flow心电图.Controls.Add(this.chk心电图_改变);
            this.flow心电图.Controls.Add(this.chk心电图_心肌梗塞);
            this.flow心电图.Controls.Add(this.chk心电图_心动过速);
            this.flow心电图.Controls.Add(this.chk心电图_心动过缓);
            this.flow心电图.Controls.Add(this.chk心电图_早搏);
            this.flow心电图.Controls.Add(this.chk心电图_房颤);
            this.flow心电图.Controls.Add(this.chk心电图_房室传导阻滞);
            this.flow心电图.Controls.Add(this.chk心电图_其他);
            this.flow心电图.Controls.Add(this.txt心电图_其他);
            this.flow心电图.Location = new System.Drawing.Point(108, -1050);
            this.flow心电图.Name = "flow心电图";
            this.flow心电图.Size = new System.Drawing.Size(662, 46);
            this.flow心电图.TabIndex = 114;
            // 
            // chk心电图_正常
            // 
            this.chk心电图_正常.Location = new System.Drawing.Point(3, 3);
            this.chk心电图_正常.Name = "chk心电图_正常";
            this.chk心电图_正常.Properties.Caption = "正常";
            this.chk心电图_正常.Size = new System.Drawing.Size(60, 19);
            this.chk心电图_正常.TabIndex = 0;
            this.chk心电图_正常.Tag = "1";
            this.chk心电图_正常.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk心电图_改变
            // 
            this.chk心电图_改变.Location = new System.Drawing.Point(69, 3);
            this.chk心电图_改变.Name = "chk心电图_改变";
            this.chk心电图_改变.Properties.Caption = "ST-T 改变";
            this.chk心电图_改变.Size = new System.Drawing.Size(85, 19);
            this.chk心电图_改变.TabIndex = 1;
            this.chk心电图_改变.Tag = "2";
            // 
            // chk心电图_心肌梗塞
            // 
            this.chk心电图_心肌梗塞.Location = new System.Drawing.Point(160, 3);
            this.chk心电图_心肌梗塞.Name = "chk心电图_心肌梗塞";
            this.chk心电图_心肌梗塞.Properties.Caption = "陈旧性心肌梗塞";
            this.chk心电图_心肌梗塞.Size = new System.Drawing.Size(114, 19);
            this.chk心电图_心肌梗塞.TabIndex = 2;
            this.chk心电图_心肌梗塞.Tag = "3";
            // 
            // chk心电图_心动过速
            // 
            this.chk心电图_心动过速.Location = new System.Drawing.Point(280, 3);
            this.chk心电图_心动过速.Name = "chk心电图_心动过速";
            this.chk心电图_心动过速.Properties.Caption = "窦性心动过速";
            this.chk心电图_心动过速.Size = new System.Drawing.Size(102, 19);
            this.chk心电图_心动过速.TabIndex = 3;
            this.chk心电图_心动过速.Tag = "4";
            // 
            // chk心电图_心动过缓
            // 
            this.chk心电图_心动过缓.Location = new System.Drawing.Point(388, 3);
            this.chk心电图_心动过缓.Name = "chk心电图_心动过缓";
            this.chk心电图_心动过缓.Properties.Caption = "窦性心动过缓";
            this.chk心电图_心动过缓.Size = new System.Drawing.Size(102, 19);
            this.chk心电图_心动过缓.TabIndex = 4;
            this.chk心电图_心动过缓.Tag = "5";
            // 
            // chk心电图_早搏
            // 
            this.chk心电图_早搏.Location = new System.Drawing.Point(496, 3);
            this.chk心电图_早搏.Name = "chk心电图_早搏";
            this.chk心电图_早搏.Properties.Caption = "早搏";
            this.chk心电图_早搏.Size = new System.Drawing.Size(60, 19);
            this.chk心电图_早搏.TabIndex = 5;
            this.chk心电图_早搏.Tag = "6";
            // 
            // chk心电图_房颤
            // 
            this.chk心电图_房颤.Location = new System.Drawing.Point(562, 3);
            this.chk心电图_房颤.Name = "chk心电图_房颤";
            this.chk心电图_房颤.Properties.Caption = "房颤";
            this.chk心电图_房颤.Size = new System.Drawing.Size(75, 19);
            this.chk心电图_房颤.TabIndex = 6;
            this.chk心电图_房颤.Tag = "7";
            // 
            // chk心电图_房室传导阻滞
            // 
            this.chk心电图_房室传导阻滞.Location = new System.Drawing.Point(3, 28);
            this.chk心电图_房室传导阻滞.Name = "chk心电图_房室传导阻滞";
            this.chk心电图_房室传导阻滞.Properties.Caption = "房室传导阻滞";
            this.chk心电图_房室传导阻滞.Size = new System.Drawing.Size(100, 19);
            this.chk心电图_房室传导阻滞.TabIndex = 7;
            this.chk心电图_房室传导阻滞.Tag = "8";
            // 
            // chk心电图_其他
            // 
            this.chk心电图_其他.Location = new System.Drawing.Point(109, 28);
            this.chk心电图_其他.Name = "chk心电图_其他";
            this.chk心电图_其他.Properties.Caption = "其他";
            this.chk心电图_其他.Size = new System.Drawing.Size(66, 19);
            this.chk心电图_其他.TabIndex = 8;
            this.chk心电图_其他.Tag = "99";
            this.chk心电图_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt心电图_其他
            // 
            this.txt心电图_其他.Location = new System.Drawing.Point(178, 25);
            this.txt心电图_其他.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt心电图_其他.Name = "txt心电图_其他";
            this.txt心电图_其他.Size = new System.Drawing.Size(184, 20);
            this.txt心电图_其他.TabIndex = 9;
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.BackColor = System.Drawing.Color.White;
            this.txt空腹血糖.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt空腹血糖.Lbl1Text = "mmol/L ";
            this.txt空腹血糖.Location = new System.Drawing.Point(108, -1098);
            this.txt空腹血糖.Margin = new System.Windows.Forms.Padding(0);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Size = new System.Drawing.Size(170, 20);
            this.txt空腹血糖.TabIndex = 112;
            this.txt空腹血糖.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel30
            // 
            this.flowLayoutPanel30.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel30.Controls.Add(this.labelControl6);
            this.flowLayoutPanel30.Controls.Add(this.txt尿蛋白);
            this.flowLayoutPanel30.Controls.Add(this.labelControl7);
            this.flowLayoutPanel30.Controls.Add(this.txt尿糖);
            this.flowLayoutPanel30.Controls.Add(this.labelControl8);
            this.flowLayoutPanel30.Controls.Add(this.txt尿胴体);
            this.flowLayoutPanel30.Controls.Add(this.labelControl9);
            this.flowLayoutPanel30.Controls.Add(this.txt尿潜血);
            this.flowLayoutPanel30.Controls.Add(this.labelControl10);
            this.flowLayoutPanel30.Controls.Add(this.txt尿常规其他);
            this.flowLayoutPanel30.Location = new System.Drawing.Point(103, -1122);
            this.flowLayoutPanel30.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel30.Name = "flowLayoutPanel30";
            this.flowLayoutPanel30.Size = new System.Drawing.Size(667, 20);
            this.flowLayoutPanel30.TabIndex = 111;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(3, 3);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(36, 14);
            this.labelControl6.TabIndex = 0;
            this.labelControl6.Text = "尿蛋白";
            // 
            // txt尿蛋白
            // 
            this.txt尿蛋白.Location = new System.Drawing.Point(45, 0);
            this.txt尿蛋白.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt尿蛋白.Name = "txt尿蛋白";
            this.txt尿蛋白.Size = new System.Drawing.Size(60, 20);
            this.txt尿蛋白.TabIndex = 0;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(111, 3);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(24, 14);
            this.labelControl7.TabIndex = 2;
            this.labelControl7.Text = "尿糖";
            // 
            // txt尿糖
            // 
            this.txt尿糖.Location = new System.Drawing.Point(141, 0);
            this.txt尿糖.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt尿糖.Name = "txt尿糖";
            this.txt尿糖.Size = new System.Drawing.Size(60, 20);
            this.txt尿糖.TabIndex = 1;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(207, 3);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(36, 14);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "尿酮体";
            // 
            // txt尿胴体
            // 
            this.txt尿胴体.Location = new System.Drawing.Point(249, 0);
            this.txt尿胴体.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt尿胴体.Name = "txt尿胴体";
            this.txt尿胴体.Size = new System.Drawing.Size(60, 20);
            this.txt尿胴体.TabIndex = 2;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(315, 3);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(36, 14);
            this.labelControl9.TabIndex = 6;
            this.labelControl9.Text = "尿潜血";
            // 
            // txt尿潜血
            // 
            this.txt尿潜血.Location = new System.Drawing.Point(357, 0);
            this.txt尿潜血.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt尿潜血.Name = "txt尿潜血";
            this.txt尿潜血.Size = new System.Drawing.Size(60, 20);
            this.txt尿潜血.TabIndex = 3;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(423, 3);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(28, 14);
            this.labelControl10.TabIndex = 8;
            this.labelControl10.Text = "其他 ";
            // 
            // txt尿常规其他
            // 
            this.txt尿常规其他.Location = new System.Drawing.Point(457, 0);
            this.txt尿常规其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt尿常规其他.Name = "txt尿常规其他";
            this.txt尿常规其他.Size = new System.Drawing.Size(60, 20);
            this.txt尿常规其他.TabIndex = 4;
            // 
            // flowLayoutPanel29
            // 
            this.flowLayoutPanel29.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel29.Controls.Add(this.txt血常规_其他);
            this.flowLayoutPanel29.Location = new System.Drawing.Point(200, -1146);
            this.flowLayoutPanel29.Name = "flowLayoutPanel29";
            this.flowLayoutPanel29.Size = new System.Drawing.Size(570, 20);
            this.flowLayoutPanel29.TabIndex = 110;
            // 
            // txt血常规_其他
            // 
            this.txt血常规_其他.Location = new System.Drawing.Point(0, 0);
            this.txt血常规_其他.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt血常规_其他.Name = "txt血常规_其他";
            this.txt血常规_其他.Size = new System.Drawing.Size(172, 20);
            this.txt血常规_其他.TabIndex = 66;
            // 
            // txt白细胞
            // 
            this.txt白细胞.BackColor = System.Drawing.Color.White;
            this.txt白细胞.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt白细胞.Lbl1Text = "×10  9  /L";
            this.txt白细胞.Location = new System.Drawing.Point(200, -1194);
            this.txt白细胞.Margin = new System.Windows.Forms.Padding(0);
            this.txt白细胞.Name = "txt白细胞";
            this.txt白细胞.Size = new System.Drawing.Size(570, 20);
            this.txt白细胞.TabIndex = 108;
            this.txt白细胞.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt血小板
            // 
            this.txt血小板.BackColor = System.Drawing.Color.White;
            this.txt血小板.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt血小板.Lbl1Text = "×10  9  /L";
            this.txt血小板.Location = new System.Drawing.Point(200, -1170);
            this.txt血小板.Margin = new System.Windows.Forms.Padding(0);
            this.txt血小板.Name = "txt血小板";
            this.txt血小板.Size = new System.Drawing.Size(570, 20);
            this.txt血小板.TabIndex = 109;
            this.txt血小板.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt血红蛋白
            // 
            this.txt血红蛋白.BackColor = System.Drawing.Color.White;
            this.txt血红蛋白.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt血红蛋白.Lbl1Text = "g/L";
            this.txt血红蛋白.Location = new System.Drawing.Point(200, -1218);
            this.txt血红蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.txt血红蛋白.Name = "txt血红蛋白";
            this.txt血红蛋白.Size = new System.Drawing.Size(570, 20);
            this.txt血红蛋白.TabIndex = 107;
            this.txt血红蛋白.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // radio足背动脉搏动
            // 
            this.radio足背动脉搏动.EditValue = "2";
            this.radio足背动脉搏动.Location = new System.Drawing.Point(108, -1500);
            this.radio足背动脉搏动.Name = "radio足背动脉搏动";
            this.radio足背动脉搏动.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "未触及"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "触及双侧对称"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "触及左侧弱或消失"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "触及右侧弱或消失")});
            this.radio足背动脉搏动.Size = new System.Drawing.Size(501, 22);
            this.radio足背动脉搏动.StyleController = this.layoutControl1;
            this.radio足背动脉搏动.TabIndex = 98;
            // 
            // flowLayoutPanel28
            // 
            this.flowLayoutPanel28.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel28.Controls.Add(this.txt查体_其他);
            this.flowLayoutPanel28.Location = new System.Drawing.Point(108, -1302);
            this.flowLayoutPanel28.Name = "flowLayoutPanel28";
            this.flowLayoutPanel28.Size = new System.Drawing.Size(662, 20);
            this.flowLayoutPanel28.TabIndex = 106;
            // 
            // txt查体_其他
            // 
            this.txt查体_其他.Location = new System.Drawing.Point(0, 0);
            this.txt查体_其他.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt查体_其他.Name = "txt查体_其他";
            this.txt查体_其他.Size = new System.Drawing.Size(511, 20);
            this.txt查体_其他.TabIndex = 1;
            // 
            // flowLayoutPanel27
            // 
            this.flowLayoutPanel27.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel27.Controls.Add(this.radio附件);
            this.flowLayoutPanel27.Controls.Add(this.txt附件);
            this.flowLayoutPanel27.Location = new System.Drawing.Point(200, -1326);
            this.flowLayoutPanel27.Name = "flowLayoutPanel27";
            this.flowLayoutPanel27.Size = new System.Drawing.Size(570, 20);
            this.flowLayoutPanel27.TabIndex = 105;
            // 
            // radio附件
            // 
            this.radio附件.Location = new System.Drawing.Point(3, 0);
            this.radio附件.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio附件.Name = "radio附件";
            this.radio附件.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "未见异常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio附件.Size = new System.Drawing.Size(151, 22);
            this.radio附件.TabIndex = 2;
            this.radio附件.SelectedIndexChanged += new System.EventHandler(this.radio附件_SelectedIndexChanged);
            // 
            // txt附件
            // 
            this.txt附件.Enabled = false;
            this.txt附件.Location = new System.Drawing.Point(154, 0);
            this.txt附件.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt附件.Name = "txt附件";
            this.txt附件.Size = new System.Drawing.Size(184, 20);
            this.txt附件.TabIndex = 1;
            // 
            // flowLayoutPanel26
            // 
            this.flowLayoutPanel26.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel26.Controls.Add(this.radio宫体);
            this.flowLayoutPanel26.Controls.Add(this.txt宫体);
            this.flowLayoutPanel26.Location = new System.Drawing.Point(200, -1350);
            this.flowLayoutPanel26.Name = "flowLayoutPanel26";
            this.flowLayoutPanel26.Size = new System.Drawing.Size(570, 20);
            this.flowLayoutPanel26.TabIndex = 104;
            // 
            // radio宫体
            // 
            this.radio宫体.Location = new System.Drawing.Point(3, 0);
            this.radio宫体.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio宫体.Name = "radio宫体";
            this.radio宫体.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "未见异常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio宫体.Size = new System.Drawing.Size(151, 22);
            this.radio宫体.TabIndex = 2;
            this.radio宫体.SelectedIndexChanged += new System.EventHandler(this.radio宫体_SelectedIndexChanged);
            // 
            // txt宫体
            // 
            this.txt宫体.Enabled = false;
            this.txt宫体.Location = new System.Drawing.Point(154, 0);
            this.txt宫体.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt宫体.Name = "txt宫体";
            this.txt宫体.Size = new System.Drawing.Size(184, 20);
            this.txt宫体.TabIndex = 1;
            // 
            // flowLayoutPanel25
            // 
            this.flowLayoutPanel25.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel25.Controls.Add(this.radio宫颈);
            this.flowLayoutPanel25.Controls.Add(this.txt宫颈);
            this.flowLayoutPanel25.Location = new System.Drawing.Point(200, -1374);
            this.flowLayoutPanel25.Name = "flowLayoutPanel25";
            this.flowLayoutPanel25.Size = new System.Drawing.Size(570, 20);
            this.flowLayoutPanel25.TabIndex = 103;
            // 
            // radio宫颈
            // 
            this.radio宫颈.Location = new System.Drawing.Point(3, 0);
            this.radio宫颈.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio宫颈.Name = "radio宫颈";
            this.radio宫颈.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "未见异常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio宫颈.Size = new System.Drawing.Size(151, 22);
            this.radio宫颈.TabIndex = 2;
            this.radio宫颈.SelectedIndexChanged += new System.EventHandler(this.radio宫颈_SelectedIndexChanged);
            // 
            // txt宫颈
            // 
            this.txt宫颈.Enabled = false;
            this.txt宫颈.Location = new System.Drawing.Point(154, 0);
            this.txt宫颈.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt宫颈.Name = "txt宫颈";
            this.txt宫颈.Size = new System.Drawing.Size(184, 20);
            this.txt宫颈.TabIndex = 1;
            // 
            // flowLayoutPanel24
            // 
            this.flowLayoutPanel24.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel24.Controls.Add(this.radio阴道);
            this.flowLayoutPanel24.Controls.Add(this.txt阴道);
            this.flowLayoutPanel24.Location = new System.Drawing.Point(200, -1398);
            this.flowLayoutPanel24.Name = "flowLayoutPanel24";
            this.flowLayoutPanel24.Size = new System.Drawing.Size(570, 20);
            this.flowLayoutPanel24.TabIndex = 102;
            // 
            // radio阴道
            // 
            this.radio阴道.Location = new System.Drawing.Point(3, 0);
            this.radio阴道.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio阴道.Name = "radio阴道";
            this.radio阴道.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "未见异常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio阴道.Size = new System.Drawing.Size(151, 22);
            this.radio阴道.TabIndex = 2;
            this.radio阴道.SelectedIndexChanged += new System.EventHandler(this.radio阴道_SelectedIndexChanged);
            // 
            // txt阴道
            // 
            this.txt阴道.Enabled = false;
            this.txt阴道.Location = new System.Drawing.Point(154, 0);
            this.txt阴道.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt阴道.Name = "txt阴道";
            this.txt阴道.Size = new System.Drawing.Size(184, 20);
            this.txt阴道.TabIndex = 1;
            // 
            // flowLayoutPanel23
            // 
            this.flowLayoutPanel23.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel23.Controls.Add(this.radio外阴);
            this.flowLayoutPanel23.Controls.Add(this.txt外阴);
            this.flowLayoutPanel23.Location = new System.Drawing.Point(200, -1422);
            this.flowLayoutPanel23.Name = "flowLayoutPanel23";
            this.flowLayoutPanel23.Size = new System.Drawing.Size(570, 20);
            this.flowLayoutPanel23.TabIndex = 101;
            // 
            // radio外阴
            // 
            this.radio外阴.Location = new System.Drawing.Point(3, 0);
            this.radio外阴.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio外阴.Name = "radio外阴";
            this.radio外阴.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "未见异常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio外阴.Size = new System.Drawing.Size(151, 22);
            this.radio外阴.TabIndex = 0;
            this.radio外阴.SelectedIndexChanged += new System.EventHandler(this.radio外阴_SelectedIndexChanged);
            // 
            // txt外阴
            // 
            this.txt外阴.Enabled = false;
            this.txt外阴.Location = new System.Drawing.Point(154, 0);
            this.txt外阴.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt外阴.Name = "txt外阴";
            this.txt外阴.Size = new System.Drawing.Size(184, 20);
            this.txt外阴.TabIndex = 1;
            // 
            // radio下肢水肿
            // 
            this.radio下肢水肿.EditValue = "1";
            this.radio下肢水肿.Location = new System.Drawing.Point(108, -1526);
            this.radio下肢水肿.Name = "radio下肢水肿";
            this.radio下肢水肿.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "单侧"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "双侧不对称"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "双侧对称")});
            this.radio下肢水肿.Size = new System.Drawing.Size(401, 22);
            this.radio下肢水肿.StyleController = this.layoutControl1;
            this.radio下肢水肿.TabIndex = 97;
            // 
            // radio心律
            // 
            this.radio心律.EditValue = "1";
            this.radio心律.Location = new System.Drawing.Point(200, -1708);
            this.radio心律.Name = "radio心律";
            this.radio心律.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "齐"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "不齐"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "绝对不齐")});
            this.radio心律.Size = new System.Drawing.Size(216, 22);
            this.radio心律.StyleController = this.layoutControl1;
            this.radio心律.TabIndex = 90;
            // 
            // flowLayoutPanel20
            // 
            this.flowLayoutPanel20.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel20.Controls.Add(this.radio移动性浊音);
            this.flowLayoutPanel20.Controls.Add(this.txt移动性浊音);
            this.flowLayoutPanel20.Location = new System.Drawing.Point(200, -1552);
            this.flowLayoutPanel20.Name = "flowLayoutPanel20";
            this.flowLayoutPanel20.Size = new System.Drawing.Size(570, 22);
            this.flowLayoutPanel20.TabIndex = 96;
            // 
            // radio移动性浊音
            // 
            this.radio移动性浊音.EditValue = "1";
            this.radio移动性浊音.Location = new System.Drawing.Point(3, 0);
            this.radio移动性浊音.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio移动性浊音.Name = "radio移动性浊音";
            this.radio移动性浊音.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio移动性浊音.Size = new System.Drawing.Size(126, 22);
            this.radio移动性浊音.TabIndex = 2;
            this.radio移动性浊音.SelectedIndexChanged += new System.EventHandler(this.radio移动性浊音_SelectedIndexChanged);
            // 
            // txt移动性浊音
            // 
            this.txt移动性浊音.Enabled = false;
            this.txt移动性浊音.Location = new System.Drawing.Point(129, 0);
            this.txt移动性浊音.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt移动性浊音.Name = "txt移动性浊音";
            this.txt移动性浊音.Size = new System.Drawing.Size(184, 20);
            this.txt移动性浊音.TabIndex = 1;
            // 
            // flow肛门指诊
            // 
            this.flow肛门指诊.BackColor = System.Drawing.Color.White;
            this.flow肛门指诊.Controls.Add(this.chk肛门指诊_未见异常);
            this.flow肛门指诊.Controls.Add(this.chk肛门指诊_触痛);
            this.flow肛门指诊.Controls.Add(this.chk肛门指诊_包块);
            this.flow肛门指诊.Controls.Add(this.chk肛门指诊_前列腺异常);
            this.flow肛门指诊.Controls.Add(this.chk肛门指诊_其他);
            this.flow肛门指诊.Controls.Add(this.txt肛门指诊_其他);
            this.flow肛门指诊.Location = new System.Drawing.Point(108, -1474);
            this.flow肛门指诊.Name = "flow肛门指诊";
            this.flow肛门指诊.Size = new System.Drawing.Size(662, 22);
            this.flow肛门指诊.TabIndex = 99;
            // 
            // chk肛门指诊_未见异常
            // 
            this.chk肛门指诊_未见异常.Location = new System.Drawing.Point(3, 0);
            this.chk肛门指诊_未见异常.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk肛门指诊_未见异常.Name = "chk肛门指诊_未见异常";
            this.chk肛门指诊_未见异常.Properties.Caption = "未见异常";
            this.chk肛门指诊_未见异常.Size = new System.Drawing.Size(78, 19);
            this.chk肛门指诊_未见异常.TabIndex = 1;
            this.chk肛门指诊_未见异常.Tag = "1";
            this.chk肛门指诊_未见异常.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk肛门指诊_触痛
            // 
            this.chk肛门指诊_触痛.Location = new System.Drawing.Point(87, 0);
            this.chk肛门指诊_触痛.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk肛门指诊_触痛.Name = "chk肛门指诊_触痛";
            this.chk肛门指诊_触痛.Properties.Caption = "触痛";
            this.chk肛门指诊_触痛.Size = new System.Drawing.Size(56, 19);
            this.chk肛门指诊_触痛.TabIndex = 2;
            this.chk肛门指诊_触痛.Tag = "2";
            // 
            // chk肛门指诊_包块
            // 
            this.chk肛门指诊_包块.Location = new System.Drawing.Point(149, 0);
            this.chk肛门指诊_包块.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk肛门指诊_包块.Name = "chk肛门指诊_包块";
            this.chk肛门指诊_包块.Properties.Caption = "包块";
            this.chk肛门指诊_包块.Size = new System.Drawing.Size(46, 19);
            this.chk肛门指诊_包块.TabIndex = 3;
            this.chk肛门指诊_包块.Tag = "3";
            // 
            // chk肛门指诊_前列腺异常
            // 
            this.chk肛门指诊_前列腺异常.Location = new System.Drawing.Point(201, 0);
            this.chk肛门指诊_前列腺异常.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk肛门指诊_前列腺异常.Name = "chk肛门指诊_前列腺异常";
            this.chk肛门指诊_前列腺异常.Properties.Caption = "前列腺异常";
            this.chk肛门指诊_前列腺异常.Size = new System.Drawing.Size(85, 19);
            this.chk肛门指诊_前列腺异常.TabIndex = 4;
            this.chk肛门指诊_前列腺异常.Tag = "4";
            // 
            // chk肛门指诊_其他
            // 
            this.chk肛门指诊_其他.Location = new System.Drawing.Point(292, 0);
            this.chk肛门指诊_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk肛门指诊_其他.Name = "chk肛门指诊_其他";
            this.chk肛门指诊_其他.Properties.Caption = "其他";
            this.chk肛门指诊_其他.Size = new System.Drawing.Size(54, 19);
            this.chk肛门指诊_其他.TabIndex = 5;
            this.chk肛门指诊_其他.Tag = "99";
            this.chk肛门指诊_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt肛门指诊_其他
            // 
            this.txt肛门指诊_其他.Enabled = false;
            this.txt肛门指诊_其他.Location = new System.Drawing.Point(352, 0);
            this.txt肛门指诊_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt肛门指诊_其他.Name = "txt肛门指诊_其他";
            this.txt肛门指诊_其他.Size = new System.Drawing.Size(100, 20);
            this.txt肛门指诊_其他.TabIndex = 6;
            // 
            // flow乳腺
            // 
            this.flow乳腺.BackColor = System.Drawing.Color.White;
            this.flow乳腺.Controls.Add(this.chk乳腺_未见异常);
            this.flow乳腺.Controls.Add(this.chk乳腺_乳房切除);
            this.flow乳腺.Controls.Add(this.chk乳腺_异常泌乳);
            this.flow乳腺.Controls.Add(this.chk乳腺_乳腺包块);
            this.flow乳腺.Controls.Add(this.chk乳腺_其他);
            this.flow乳腺.Controls.Add(this.txt乳腺_其他);
            this.flow乳腺.Location = new System.Drawing.Point(108, -1448);
            this.flow乳腺.Name = "flow乳腺";
            this.flow乳腺.Size = new System.Drawing.Size(662, 22);
            this.flow乳腺.TabIndex = 100;
            // 
            // chk乳腺_未见异常
            // 
            this.chk乳腺_未见异常.Location = new System.Drawing.Point(3, 0);
            this.chk乳腺_未见异常.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk乳腺_未见异常.Name = "chk乳腺_未见异常";
            this.chk乳腺_未见异常.Properties.Caption = "未见异常";
            this.chk乳腺_未见异常.Size = new System.Drawing.Size(78, 19);
            this.chk乳腺_未见异常.TabIndex = 1;
            this.chk乳腺_未见异常.Tag = "1";
            this.chk乳腺_未见异常.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk乳腺_乳房切除
            // 
            this.chk乳腺_乳房切除.Location = new System.Drawing.Point(87, 0);
            this.chk乳腺_乳房切除.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk乳腺_乳房切除.Name = "chk乳腺_乳房切除";
            this.chk乳腺_乳房切除.Properties.Caption = "乳房切除";
            this.chk乳腺_乳房切除.Size = new System.Drawing.Size(73, 19);
            this.chk乳腺_乳房切除.TabIndex = 2;
            this.chk乳腺_乳房切除.Tag = "2";
            // 
            // chk乳腺_异常泌乳
            // 
            this.chk乳腺_异常泌乳.Location = new System.Drawing.Point(166, 0);
            this.chk乳腺_异常泌乳.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk乳腺_异常泌乳.Name = "chk乳腺_异常泌乳";
            this.chk乳腺_异常泌乳.Properties.Caption = "异常泌乳";
            this.chk乳腺_异常泌乳.Size = new System.Drawing.Size(73, 19);
            this.chk乳腺_异常泌乳.TabIndex = 3;
            this.chk乳腺_异常泌乳.Tag = "3";
            // 
            // chk乳腺_乳腺包块
            // 
            this.chk乳腺_乳腺包块.Location = new System.Drawing.Point(245, 0);
            this.chk乳腺_乳腺包块.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk乳腺_乳腺包块.Name = "chk乳腺_乳腺包块";
            this.chk乳腺_乳腺包块.Properties.Caption = "乳腺包块";
            this.chk乳腺_乳腺包块.Size = new System.Drawing.Size(75, 19);
            this.chk乳腺_乳腺包块.TabIndex = 4;
            this.chk乳腺_乳腺包块.Tag = "4";
            // 
            // chk乳腺_其他
            // 
            this.chk乳腺_其他.Location = new System.Drawing.Point(326, 0);
            this.chk乳腺_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk乳腺_其他.Name = "chk乳腺_其他";
            this.chk乳腺_其他.Properties.Caption = "其他";
            this.chk乳腺_其他.Size = new System.Drawing.Size(54, 19);
            this.chk乳腺_其他.TabIndex = 5;
            this.chk乳腺_其他.Tag = "99";
            this.chk乳腺_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt乳腺_其他
            // 
            this.txt乳腺_其他.Enabled = false;
            this.txt乳腺_其他.Location = new System.Drawing.Point(386, 0);
            this.txt乳腺_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt乳腺_其他.Name = "txt乳腺_其他";
            this.txt乳腺_其他.Size = new System.Drawing.Size(100, 20);
            this.txt乳腺_其他.TabIndex = 6;
            // 
            // flowLayoutPanel19
            // 
            this.flowLayoutPanel19.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel19.Controls.Add(this.radio脾大);
            this.flowLayoutPanel19.Controls.Add(this.txt脾大);
            this.flowLayoutPanel19.Location = new System.Drawing.Point(200, -1578);
            this.flowLayoutPanel19.Name = "flowLayoutPanel19";
            this.flowLayoutPanel19.Size = new System.Drawing.Size(570, 22);
            this.flowLayoutPanel19.TabIndex = 95;
            // 
            // radio脾大
            // 
            this.radio脾大.EditValue = "1";
            this.radio脾大.Location = new System.Drawing.Point(3, 0);
            this.radio脾大.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio脾大.Name = "radio脾大";
            this.radio脾大.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio脾大.Size = new System.Drawing.Size(126, 22);
            this.radio脾大.TabIndex = 2;
            this.radio脾大.SelectedIndexChanged += new System.EventHandler(this.radio脾大_SelectedIndexChanged);
            // 
            // txt脾大
            // 
            this.txt脾大.Enabled = false;
            this.txt脾大.Location = new System.Drawing.Point(129, 0);
            this.txt脾大.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt脾大.Name = "txt脾大";
            this.txt脾大.Size = new System.Drawing.Size(184, 20);
            this.txt脾大.TabIndex = 1;
            // 
            // flowLayoutPanel18
            // 
            this.flowLayoutPanel18.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel18.Controls.Add(this.radio肝大);
            this.flowLayoutPanel18.Controls.Add(this.txt肝大);
            this.flowLayoutPanel18.Location = new System.Drawing.Point(200, -1604);
            this.flowLayoutPanel18.Name = "flowLayoutPanel18";
            this.flowLayoutPanel18.Size = new System.Drawing.Size(570, 22);
            this.flowLayoutPanel18.TabIndex = 94;
            // 
            // radio肝大
            // 
            this.radio肝大.EditValue = "1";
            this.radio肝大.Location = new System.Drawing.Point(3, 0);
            this.radio肝大.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio肝大.Name = "radio肝大";
            this.radio肝大.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio肝大.Size = new System.Drawing.Size(126, 22);
            this.radio肝大.TabIndex = 2;
            this.radio肝大.SelectedIndexChanged += new System.EventHandler(this.radio肝大_SelectedIndexChanged);
            // 
            // txt肝大
            // 
            this.txt肝大.Enabled = false;
            this.txt肝大.Location = new System.Drawing.Point(129, 0);
            this.txt肝大.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt肝大.Name = "txt肝大";
            this.txt肝大.Size = new System.Drawing.Size(184, 20);
            this.txt肝大.TabIndex = 1;
            // 
            // flowLayoutPanel17
            // 
            this.flowLayoutPanel17.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel17.Controls.Add(this.radio包块);
            this.flowLayoutPanel17.Controls.Add(this.txt包块);
            this.flowLayoutPanel17.Location = new System.Drawing.Point(200, -1630);
            this.flowLayoutPanel17.Name = "flowLayoutPanel17";
            this.flowLayoutPanel17.Size = new System.Drawing.Size(570, 22);
            this.flowLayoutPanel17.TabIndex = 93;
            // 
            // radio包块
            // 
            this.radio包块.EditValue = "1";
            this.radio包块.Location = new System.Drawing.Point(3, 0);
            this.radio包块.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio包块.Name = "radio包块";
            this.radio包块.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio包块.Size = new System.Drawing.Size(126, 22);
            this.radio包块.TabIndex = 0;
            this.radio包块.SelectedIndexChanged += new System.EventHandler(this.radio包块_SelectedIndexChanged);
            // 
            // txt包块
            // 
            this.txt包块.Enabled = false;
            this.txt包块.Location = new System.Drawing.Point(129, 0);
            this.txt包块.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt包块.Name = "txt包块";
            this.txt包块.Size = new System.Drawing.Size(184, 20);
            this.txt包块.TabIndex = 1;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel16.Controls.Add(this.radio压痛);
            this.flowLayoutPanel16.Controls.Add(this.txt压痛);
            this.flowLayoutPanel16.Location = new System.Drawing.Point(200, -1656);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(570, 22);
            this.flowLayoutPanel16.TabIndex = 92;
            // 
            // radio压痛
            // 
            this.radio压痛.EditValue = "1";
            this.radio压痛.Location = new System.Drawing.Point(3, 0);
            this.radio压痛.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio压痛.Name = "radio压痛";
            this.radio压痛.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio压痛.Size = new System.Drawing.Size(126, 22);
            this.radio压痛.TabIndex = 0;
            this.radio压痛.SelectedIndexChanged += new System.EventHandler(this.radio压痛_SelectedIndexChanged);
            // 
            // txt压痛
            // 
            this.txt压痛.Enabled = false;
            this.txt压痛.Location = new System.Drawing.Point(129, 0);
            this.txt压痛.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt压痛.Name = "txt压痛";
            this.txt压痛.Size = new System.Drawing.Size(184, 20);
            this.txt压痛.TabIndex = 1;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel15.Controls.Add(this.radio杂音);
            this.flowLayoutPanel15.Controls.Add(this.txt杂音);
            this.flowLayoutPanel15.Location = new System.Drawing.Point(200, -1682);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(570, 22);
            this.flowLayoutPanel15.TabIndex = 91;
            // 
            // radio杂音
            // 
            this.radio杂音.EditValue = "1";
            this.radio杂音.Location = new System.Drawing.Point(3, 0);
            this.radio杂音.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio杂音.Name = "radio杂音";
            this.radio杂音.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "有")});
            this.radio杂音.Size = new System.Drawing.Size(126, 22);
            this.radio杂音.TabIndex = 0;
            this.radio杂音.SelectedIndexChanged += new System.EventHandler(this.radio杂音_SelectedIndexChanged);
            // 
            // txt杂音
            // 
            this.txt杂音.Enabled = false;
            this.txt杂音.Location = new System.Drawing.Point(129, 0);
            this.txt杂音.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt杂音.Name = "txt杂音";
            this.txt杂音.Size = new System.Drawing.Size(184, 20);
            this.txt杂音.TabIndex = 1;
            // 
            // txt心率
            // 
            this.txt心率.BackColor = System.Drawing.Color.White;
            this.txt心率.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt心率.Lbl1Text = "次/分";
            this.txt心率.Location = new System.Drawing.Point(200, -1734);
            this.txt心率.Margin = new System.Windows.Forms.Padding(0);
            this.txt心率.Name = "txt心率";
            this.txt心率.Size = new System.Drawing.Size(570, 22);
            this.txt心率.TabIndex = 89;
            this.txt心率.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel13.Controls.Add(this.radio呼吸音);
            this.flowLayoutPanel13.Controls.Add(this.txt呼吸音_异常);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(200, -1786);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(570, 22);
            this.flowLayoutPanel13.TabIndex = 87;
            // 
            // radio呼吸音
            // 
            this.radio呼吸音.EditValue = "1";
            this.radio呼吸音.Location = new System.Drawing.Point(3, 0);
            this.radio呼吸音.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio呼吸音.Name = "radio呼吸音";
            this.radio呼吸音.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio呼吸音.Size = new System.Drawing.Size(126, 22);
            this.radio呼吸音.TabIndex = 0;
            this.radio呼吸音.SelectedIndexChanged += new System.EventHandler(this.radio呼吸音_SelectedIndexChanged);
            // 
            // txt呼吸音_异常
            // 
            this.txt呼吸音_异常.Enabled = false;
            this.txt呼吸音_异常.Location = new System.Drawing.Point(129, 0);
            this.txt呼吸音_异常.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt呼吸音_异常.Name = "txt呼吸音_异常";
            this.txt呼吸音_异常.Size = new System.Drawing.Size(184, 20);
            this.txt呼吸音_异常.TabIndex = 1;
            // 
            // radio桶状胸
            // 
            this.radio桶状胸.EditValue = "2";
            this.radio桶状胸.Location = new System.Drawing.Point(200, -1812);
            this.radio桶状胸.Name = "radio桶状胸";
            this.radio桶状胸.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio桶状胸.Size = new System.Drawing.Size(104, 22);
            this.radio桶状胸.StyleController = this.layoutControl1;
            this.radio桶状胸.TabIndex = 86;
            // 
            // flow巩膜
            // 
            this.flow巩膜.BackColor = System.Drawing.Color.White;
            this.flow巩膜.Controls.Add(this.chk巩膜_正常);
            this.flow巩膜.Controls.Add(this.chk巩膜_黄染);
            this.flow巩膜.Controls.Add(this.chk巩膜_充血);
            this.flow巩膜.Controls.Add(this.chk巩膜_其他);
            this.flow巩膜.Controls.Add(this.txt巩膜_其他);
            this.flow巩膜.Location = new System.Drawing.Point(108, -1864);
            this.flow巩膜.Name = "flow巩膜";
            this.flow巩膜.Size = new System.Drawing.Size(662, 22);
            this.flow巩膜.TabIndex = 84;
            // 
            // chk巩膜_正常
            // 
            this.chk巩膜_正常.EditValue = true;
            this.chk巩膜_正常.Location = new System.Drawing.Point(3, 0);
            this.chk巩膜_正常.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk巩膜_正常.Name = "chk巩膜_正常";
            this.chk巩膜_正常.Properties.Caption = "正常";
            this.chk巩膜_正常.Size = new System.Drawing.Size(54, 19);
            this.chk巩膜_正常.TabIndex = 1;
            this.chk巩膜_正常.Tag = "1";
            this.chk巩膜_正常.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk巩膜_黄染
            // 
            this.chk巩膜_黄染.Location = new System.Drawing.Point(63, 0);
            this.chk巩膜_黄染.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk巩膜_黄染.Name = "chk巩膜_黄染";
            this.chk巩膜_黄染.Properties.Caption = "黄染";
            this.chk巩膜_黄染.Size = new System.Drawing.Size(46, 19);
            this.chk巩膜_黄染.TabIndex = 2;
            this.chk巩膜_黄染.Tag = "2";
            // 
            // chk巩膜_充血
            // 
            this.chk巩膜_充血.Location = new System.Drawing.Point(115, 0);
            this.chk巩膜_充血.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk巩膜_充血.Name = "chk巩膜_充血";
            this.chk巩膜_充血.Properties.Caption = "充血";
            this.chk巩膜_充血.Size = new System.Drawing.Size(50, 19);
            this.chk巩膜_充血.TabIndex = 3;
            this.chk巩膜_充血.Tag = "3";
            // 
            // chk巩膜_其他
            // 
            this.chk巩膜_其他.Location = new System.Drawing.Point(171, 0);
            this.chk巩膜_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk巩膜_其他.Name = "chk巩膜_其他";
            this.chk巩膜_其他.Properties.Caption = "其他";
            this.chk巩膜_其他.Size = new System.Drawing.Size(54, 19);
            this.chk巩膜_其他.TabIndex = 4;
            this.chk巩膜_其他.Tag = "99";
            this.chk巩膜_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt巩膜_其他
            // 
            this.txt巩膜_其他.Enabled = false;
            this.txt巩膜_其他.Location = new System.Drawing.Point(231, 0);
            this.txt巩膜_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt巩膜_其他.Name = "txt巩膜_其他";
            this.txt巩膜_其他.Size = new System.Drawing.Size(100, 20);
            this.txt巩膜_其他.TabIndex = 5;
            // 
            // flow皮肤
            // 
            this.flow皮肤.BackColor = System.Drawing.Color.White;
            this.flow皮肤.Controls.Add(this.chk皮肤_正常);
            this.flow皮肤.Controls.Add(this.chk皮肤_潮红);
            this.flow皮肤.Controls.Add(this.chk皮肤_苍白);
            this.flow皮肤.Controls.Add(this.chk皮肤_发绀);
            this.flow皮肤.Controls.Add(this.chk皮肤_黄染);
            this.flow皮肤.Controls.Add(this.chk皮肤_色素沉着);
            this.flow皮肤.Controls.Add(this.chk皮肤_其他);
            this.flow皮肤.Controls.Add(this.txt皮肤_其他);
            this.flow皮肤.Location = new System.Drawing.Point(108, -1890);
            this.flow皮肤.Margin = new System.Windows.Forms.Padding(0);
            this.flow皮肤.Name = "flow皮肤";
            this.flow皮肤.Size = new System.Drawing.Size(662, 22);
            this.flow皮肤.TabIndex = 83;
            // 
            // chk皮肤_正常
            // 
            this.chk皮肤_正常.EditValue = true;
            this.chk皮肤_正常.Location = new System.Drawing.Point(3, 0);
            this.chk皮肤_正常.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk皮肤_正常.Name = "chk皮肤_正常";
            this.chk皮肤_正常.Properties.Caption = "正常";
            this.chk皮肤_正常.Size = new System.Drawing.Size(54, 19);
            this.chk皮肤_正常.TabIndex = 0;
            this.chk皮肤_正常.Tag = "1";
            this.chk皮肤_正常.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk皮肤_潮红
            // 
            this.chk皮肤_潮红.Location = new System.Drawing.Point(63, 0);
            this.chk皮肤_潮红.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk皮肤_潮红.Name = "chk皮肤_潮红";
            this.chk皮肤_潮红.Properties.Caption = "潮红";
            this.chk皮肤_潮红.Size = new System.Drawing.Size(46, 19);
            this.chk皮肤_潮红.TabIndex = 1;
            this.chk皮肤_潮红.Tag = "2";
            // 
            // chk皮肤_苍白
            // 
            this.chk皮肤_苍白.Location = new System.Drawing.Point(115, 0);
            this.chk皮肤_苍白.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk皮肤_苍白.Name = "chk皮肤_苍白";
            this.chk皮肤_苍白.Properties.Caption = "苍白";
            this.chk皮肤_苍白.Size = new System.Drawing.Size(50, 19);
            this.chk皮肤_苍白.TabIndex = 2;
            this.chk皮肤_苍白.Tag = "3";
            // 
            // chk皮肤_发绀
            // 
            this.chk皮肤_发绀.Location = new System.Drawing.Point(171, 0);
            this.chk皮肤_发绀.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk皮肤_发绀.Name = "chk皮肤_发绀";
            this.chk皮肤_发绀.Properties.Caption = "发绀";
            this.chk皮肤_发绀.Size = new System.Drawing.Size(54, 19);
            this.chk皮肤_发绀.TabIndex = 3;
            this.chk皮肤_发绀.Tag = "4";
            // 
            // chk皮肤_黄染
            // 
            this.chk皮肤_黄染.Location = new System.Drawing.Point(231, 0);
            this.chk皮肤_黄染.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk皮肤_黄染.Name = "chk皮肤_黄染";
            this.chk皮肤_黄染.Properties.Caption = "黄染";
            this.chk皮肤_黄染.Size = new System.Drawing.Size(52, 19);
            this.chk皮肤_黄染.TabIndex = 4;
            this.chk皮肤_黄染.Tag = "5";
            // 
            // chk皮肤_色素沉着
            // 
            this.chk皮肤_色素沉着.Location = new System.Drawing.Point(289, 0);
            this.chk皮肤_色素沉着.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk皮肤_色素沉着.Name = "chk皮肤_色素沉着";
            this.chk皮肤_色素沉着.Properties.Caption = "色素沉着";
            this.chk皮肤_色素沉着.Size = new System.Drawing.Size(75, 19);
            this.chk皮肤_色素沉着.TabIndex = 5;
            this.chk皮肤_色素沉着.Tag = "6";
            // 
            // chk皮肤_其他
            // 
            this.chk皮肤_其他.Location = new System.Drawing.Point(370, 0);
            this.chk皮肤_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk皮肤_其他.Name = "chk皮肤_其他";
            this.chk皮肤_其他.Properties.Caption = "其他";
            this.chk皮肤_其他.Size = new System.Drawing.Size(52, 19);
            this.chk皮肤_其他.TabIndex = 6;
            this.chk皮肤_其他.Tag = "99";
            this.chk皮肤_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt皮肤_其他
            // 
            this.txt皮肤_其他.Enabled = false;
            this.txt皮肤_其他.Location = new System.Drawing.Point(428, 0);
            this.txt皮肤_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt皮肤_其他.Name = "txt皮肤_其他";
            this.txt皮肤_其他.Size = new System.Drawing.Size(100, 20);
            this.txt皮肤_其他.TabIndex = 7;
            // 
            // flow罗音
            // 
            this.flow罗音.BackColor = System.Drawing.Color.White;
            this.flow罗音.Controls.Add(this.chk罗音_无);
            this.flow罗音.Controls.Add(this.chk罗音_干罗音);
            this.flow罗音.Controls.Add(this.chk罗音_湿罗音);
            this.flow罗音.Controls.Add(this.chk罗音_其他);
            this.flow罗音.Controls.Add(this.txt罗音_其他);
            this.flow罗音.Location = new System.Drawing.Point(200, -1760);
            this.flow罗音.Name = "flow罗音";
            this.flow罗音.Size = new System.Drawing.Size(570, 22);
            this.flow罗音.TabIndex = 88;
            // 
            // chk罗音_无
            // 
            this.chk罗音_无.EditValue = true;
            this.chk罗音_无.Location = new System.Drawing.Point(3, 0);
            this.chk罗音_无.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk罗音_无.Name = "chk罗音_无";
            this.chk罗音_无.Properties.Caption = "无";
            this.chk罗音_无.Size = new System.Drawing.Size(36, 19);
            this.chk罗音_无.TabIndex = 1;
            this.chk罗音_无.Tag = "1";
            this.chk罗音_无.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk罗音_干罗音
            // 
            this.chk罗音_干罗音.Location = new System.Drawing.Point(45, 0);
            this.chk罗音_干罗音.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk罗音_干罗音.Name = "chk罗音_干罗音";
            this.chk罗音_干罗音.Properties.Caption = "干罗音";
            this.chk罗音_干罗音.Size = new System.Drawing.Size(71, 19);
            this.chk罗音_干罗音.TabIndex = 2;
            this.chk罗音_干罗音.Tag = "2";
            // 
            // chk罗音_湿罗音
            // 
            this.chk罗音_湿罗音.Location = new System.Drawing.Point(122, 0);
            this.chk罗音_湿罗音.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk罗音_湿罗音.Name = "chk罗音_湿罗音";
            this.chk罗音_湿罗音.Properties.Caption = "湿罗音";
            this.chk罗音_湿罗音.Size = new System.Drawing.Size(69, 19);
            this.chk罗音_湿罗音.TabIndex = 3;
            this.chk罗音_湿罗音.Tag = "3";
            // 
            // chk罗音_其他
            // 
            this.chk罗音_其他.Location = new System.Drawing.Point(197, 0);
            this.chk罗音_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk罗音_其他.Name = "chk罗音_其他";
            this.chk罗音_其他.Properties.Caption = "其他";
            this.chk罗音_其他.Size = new System.Drawing.Size(54, 19);
            this.chk罗音_其他.TabIndex = 4;
            this.chk罗音_其他.Tag = "4";
            this.chk罗音_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt罗音_其他
            // 
            this.txt罗音_其他.Enabled = false;
            this.txt罗音_其他.Location = new System.Drawing.Point(257, 0);
            this.txt罗音_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt罗音_其他.Name = "txt罗音_其他";
            this.txt罗音_其他.Size = new System.Drawing.Size(100, 20);
            this.txt罗音_其他.TabIndex = 5;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel9.Controls.Add(this.radio眼底);
            this.flowLayoutPanel9.Controls.Add(this.txt眼底);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(108, -1916);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(662, 22);
            this.flowLayoutPanel9.TabIndex = 82;
            // 
            // radio眼底
            // 
            this.radio眼底.EditValue = "0";
            this.radio眼底.Location = new System.Drawing.Point(3, 0);
            this.radio眼底.Margin = new System.Windows.Forms.Padding(3, 0, 0, 3);
            this.radio眼底.Name = "radio眼底";
            this.radio眼底.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "正常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "异常")});
            this.radio眼底.Size = new System.Drawing.Size(126, 22);
            this.radio眼底.TabIndex = 0;
            this.radio眼底.SelectedIndexChanged += new System.EventHandler(this.radio眼底_SelectedIndexChanged);
            // 
            // txt眼底
            // 
            this.txt眼底.Enabled = false;
            this.txt眼底.Location = new System.Drawing.Point(129, 0);
            this.txt眼底.Margin = new System.Windows.Forms.Padding(0, 0, 3, 3);
            this.txt眼底.Name = "txt眼底";
            this.txt眼底.Size = new System.Drawing.Size(184, 20);
            this.txt眼底.TabIndex = 1;
            // 
            // flow听力
            // 
            this.flow听力.BackColor = System.Drawing.Color.White;
            this.flow听力.Controls.Add(this.radio听力_听见);
            this.flow听力.Controls.Add(this.radio听力_听不清);
            this.flow听力.Location = new System.Drawing.Point(88, -2000);
            this.flow听力.Margin = new System.Windows.Forms.Padding(0);
            this.flow听力.Name = "flow听力";
            this.flow听力.Size = new System.Drawing.Size(682, 20);
            this.flow听力.TabIndex = 80;
            // 
            // radio听力_听见
            // 
            this.radio听力_听见.EditValue = true;
            this.radio听力_听见.Location = new System.Drawing.Point(3, 0);
            this.radio听力_听见.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio听力_听见.Name = "radio听力_听见";
            this.radio听力_听见.Properties.Caption = "听见";
            this.radio听力_听见.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio听力_听见.Properties.RadioGroupIndex = 2;
            this.radio听力_听见.Size = new System.Drawing.Size(55, 19);
            this.radio听力_听见.TabIndex = 0;
            this.radio听力_听见.Tag = "1";
            // 
            // radio听力_听不清
            // 
            this.radio听力_听不清.Location = new System.Drawing.Point(64, 0);
            this.radio听力_听不清.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio听力_听不清.Name = "radio听力_听不清";
            this.radio听力_听不清.Properties.Caption = "听不清或无法听见";
            this.radio听力_听不清.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio听力_听不清.Properties.RadioGroupIndex = 2;
            this.radio听力_听不清.Size = new System.Drawing.Size(272, 19);
            this.radio听力_听不清.TabIndex = 1;
            this.radio听力_听不清.TabStop = false;
            this.radio听力_听不清.Tag = "2";
            // 
            // flow淋巴结
            // 
            this.flow淋巴结.BackColor = System.Drawing.Color.White;
            this.flow淋巴结.Controls.Add(this.chk淋巴结_未触及);
            this.flow淋巴结.Controls.Add(this.chk淋巴结_锁骨上);
            this.flow淋巴结.Controls.Add(this.chk淋巴结_腋窝);
            this.flow淋巴结.Controls.Add(this.chk淋巴结_其他);
            this.flow淋巴结.Controls.Add(this.txt淋巴结_其他);
            this.flow淋巴结.Location = new System.Drawing.Point(108, -1838);
            this.flow淋巴结.Name = "flow淋巴结";
            this.flow淋巴结.Size = new System.Drawing.Size(662, 22);
            this.flow淋巴结.TabIndex = 85;
            // 
            // chk淋巴结_未触及
            // 
            this.chk淋巴结_未触及.EditValue = true;
            this.chk淋巴结_未触及.Location = new System.Drawing.Point(3, 0);
            this.chk淋巴结_未触及.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk淋巴结_未触及.Name = "chk淋巴结_未触及";
            this.chk淋巴结_未触及.Properties.Caption = "未触及";
            this.chk淋巴结_未触及.Size = new System.Drawing.Size(68, 19);
            this.chk淋巴结_未触及.TabIndex = 1;
            this.chk淋巴结_未触及.Tag = "1";
            this.chk淋巴结_未触及.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk淋巴结_锁骨上
            // 
            this.chk淋巴结_锁骨上.Location = new System.Drawing.Point(77, 0);
            this.chk淋巴结_锁骨上.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk淋巴结_锁骨上.Name = "chk淋巴结_锁骨上";
            this.chk淋巴结_锁骨上.Properties.Caption = "锁骨上";
            this.chk淋巴结_锁骨上.Size = new System.Drawing.Size(71, 19);
            this.chk淋巴结_锁骨上.TabIndex = 2;
            this.chk淋巴结_锁骨上.Tag = "2";
            // 
            // chk淋巴结_腋窝
            // 
            this.chk淋巴结_腋窝.Location = new System.Drawing.Point(154, 0);
            this.chk淋巴结_腋窝.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk淋巴结_腋窝.Name = "chk淋巴结_腋窝";
            this.chk淋巴结_腋窝.Properties.Caption = "腋窝";
            this.chk淋巴结_腋窝.Size = new System.Drawing.Size(50, 19);
            this.chk淋巴结_腋窝.TabIndex = 3;
            this.chk淋巴结_腋窝.Tag = "3";
            // 
            // chk淋巴结_其他
            // 
            this.chk淋巴结_其他.Location = new System.Drawing.Point(210, 0);
            this.chk淋巴结_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.chk淋巴结_其他.Name = "chk淋巴结_其他";
            this.chk淋巴结_其他.Properties.Caption = "其他";
            this.chk淋巴结_其他.Size = new System.Drawing.Size(54, 19);
            this.chk淋巴结_其他.TabIndex = 4;
            this.chk淋巴结_其他.Tag = "4";
            this.chk淋巴结_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt淋巴结_其他
            // 
            this.txt淋巴结_其他.Enabled = false;
            this.txt淋巴结_其他.Location = new System.Drawing.Point(270, 0);
            this.txt淋巴结_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt淋巴结_其他.Name = "txt淋巴结_其他";
            this.txt淋巴结_其他.Size = new System.Drawing.Size(100, 20);
            this.txt淋巴结_其他.TabIndex = 5;
            // 
            // flow运动功能
            // 
            this.flow运动功能.BackColor = System.Drawing.Color.White;
            this.flow运动功能.Controls.Add(this.radio运动能力_可顺利完成);
            this.flow运动功能.Controls.Add(this.radio运动能力_无法完成);
            this.flow运动功能.Location = new System.Drawing.Point(88, -1976);
            this.flow运动功能.Margin = new System.Windows.Forms.Padding(0);
            this.flow运动功能.Name = "flow运动功能";
            this.flow运动功能.Size = new System.Drawing.Size(682, 20);
            this.flow运动功能.TabIndex = 81;
            // 
            // radio运动能力_可顺利完成
            // 
            this.radio运动能力_可顺利完成.EditValue = true;
            this.radio运动能力_可顺利完成.Location = new System.Drawing.Point(3, 0);
            this.radio运动能力_可顺利完成.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio运动能力_可顺利完成.Name = "radio运动能力_可顺利完成";
            this.radio运动能力_可顺利完成.Properties.Caption = "可顺利完成";
            this.radio运动能力_可顺利完成.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio运动能力_可顺利完成.Properties.RadioGroupIndex = 1;
            this.radio运动能力_可顺利完成.Size = new System.Drawing.Size(95, 19);
            this.radio运动能力_可顺利完成.TabIndex = 0;
            this.radio运动能力_可顺利完成.Tag = "1";
            // 
            // radio运动能力_无法完成
            // 
            this.radio运动能力_无法完成.Location = new System.Drawing.Point(104, 0);
            this.radio运动能力_无法完成.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio运动能力_无法完成.Name = "radio运动能力_无法完成";
            this.radio运动能力_无法完成.Properties.Caption = "无法独立完成任何一个动作";
            this.radio运动能力_无法完成.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio运动能力_无法完成.Properties.RadioGroupIndex = 1;
            this.radio运动能力_无法完成.Size = new System.Drawing.Size(272, 19);
            this.radio运动能力_无法完成.TabIndex = 1;
            this.radio运动能力_无法完成.TabStop = false;
            this.radio运动能力_无法完成.Tag = "2";
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel6.Controls.Add(this.labelControl1);
            this.flowLayoutPanel6.Controls.Add(this.txt左眼视力);
            this.flowLayoutPanel6.Controls.Add(this.labelControl2);
            this.flowLayoutPanel6.Controls.Add(this.txt右眼视力);
            this.flowLayoutPanel6.Controls.Add(this.labelControl3);
            this.flowLayoutPanel6.Controls.Add(this.txt矫正左眼视力);
            this.flowLayoutPanel6.Controls.Add(this.labelControl4);
            this.flowLayoutPanel6.Controls.Add(this.txt矫正右眼视力);
            this.flowLayoutPanel6.Controls.Add(this.labelControl5);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(88, -2024);
            this.flowLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(682, 20);
            this.flowLayoutPanel6.TabIndex = 79;
            // 
            // labelControl1
            // 
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(3, 1);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(24, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "左眼";
            // 
            // txt左眼视力
            // 
            this.txt左眼视力.EnterMoveNextControl = true;
            this.txt左眼视力.Location = new System.Drawing.Point(33, 0);
            this.txt左眼视力.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt左眼视力.Name = "txt左眼视力";
            this.txt左眼视力.Size = new System.Drawing.Size(60, 20);
            this.txt左眼视力.TabIndex = 0;
            // 
            // labelControl2
            // 
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(99, 1);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(24, 14);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "右眼";
            // 
            // txt右眼视力
            // 
            this.txt右眼视力.EnterMoveNextControl = true;
            this.txt右眼视力.Location = new System.Drawing.Point(129, 0);
            this.txt右眼视力.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt右眼视力.Name = "txt右眼视力";
            this.txt右眼视力.Size = new System.Drawing.Size(60, 20);
            this.txt右眼视力.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Location = new System.Drawing.Point(195, 1);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(97, 14);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "(矫正视力：左眼";
            // 
            // txt矫正左眼视力
            // 
            this.txt矫正左眼视力.EnterMoveNextControl = true;
            this.txt矫正左眼视力.Location = new System.Drawing.Point(298, 0);
            this.txt矫正左眼视力.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt矫正左眼视力.Name = "txt矫正左眼视力";
            this.txt矫正左眼视力.Size = new System.Drawing.Size(60, 20);
            this.txt矫正左眼视力.TabIndex = 2;
            // 
            // labelControl4
            // 
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(364, 1);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(24, 14);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "右眼";
            // 
            // txt矫正右眼视力
            // 
            this.txt矫正右眼视力.EnterMoveNextControl = true;
            this.txt矫正右眼视力.Location = new System.Drawing.Point(394, 0);
            this.txt矫正右眼视力.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt矫正右眼视力.Name = "txt矫正右眼视力";
            this.txt矫正右眼视力.Size = new System.Drawing.Size(60, 20);
            this.txt矫正右眼视力.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Location = new System.Drawing.Point(460, 1);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(24, 14);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = ")";
            // 
            // flow咽部
            // 
            this.flow咽部.BackColor = System.Drawing.Color.White;
            this.flow咽部.Controls.Add(this.radio咽部_无充血);
            this.flow咽部.Controls.Add(this.radio咽部_充血);
            this.flow咽部.Controls.Add(this.radio咽部_淋巴滤泡增生);
            this.flow咽部.Controls.Add(this.radio咽部_其他);
            this.flow咽部.Controls.Add(this.txt咽部_其他);
            this.flow咽部.Location = new System.Drawing.Point(145, -2048);
            this.flow咽部.Name = "flow咽部";
            this.flow咽部.Size = new System.Drawing.Size(625, 20);
            this.flow咽部.TabIndex = 78;
            // 
            // radio咽部_无充血
            // 
            this.radio咽部_无充血.EditValue = true;
            this.radio咽部_无充血.Location = new System.Drawing.Point(3, 0);
            this.radio咽部_无充血.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio咽部_无充血.Name = "radio咽部_无充血";
            this.radio咽部_无充血.Properties.Caption = "无充血";
            this.radio咽部_无充血.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio咽部_无充血.Properties.RadioGroupIndex = 0;
            this.radio咽部_无充血.Size = new System.Drawing.Size(63, 19);
            this.radio咽部_无充血.TabIndex = 0;
            this.radio咽部_无充血.Tag = "1";
            // 
            // radio咽部_充血
            // 
            this.radio咽部_充血.Location = new System.Drawing.Point(72, 0);
            this.radio咽部_充血.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio咽部_充血.Name = "radio咽部_充血";
            this.radio咽部_充血.Properties.Caption = "充血";
            this.radio咽部_充血.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio咽部_充血.Properties.RadioGroupIndex = 0;
            this.radio咽部_充血.Size = new System.Drawing.Size(50, 19);
            this.radio咽部_充血.TabIndex = 1;
            this.radio咽部_充血.TabStop = false;
            this.radio咽部_充血.Tag = "2";
            // 
            // radio咽部_淋巴滤泡增生
            // 
            this.radio咽部_淋巴滤泡增生.Location = new System.Drawing.Point(128, 0);
            this.radio咽部_淋巴滤泡增生.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio咽部_淋巴滤泡增生.Name = "radio咽部_淋巴滤泡增生";
            this.radio咽部_淋巴滤泡增生.Properties.Caption = "淋巴滤泡增生";
            this.radio咽部_淋巴滤泡增生.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio咽部_淋巴滤泡增生.Properties.RadioGroupIndex = 0;
            this.radio咽部_淋巴滤泡增生.Size = new System.Drawing.Size(95, 19);
            this.radio咽部_淋巴滤泡增生.TabIndex = 2;
            this.radio咽部_淋巴滤泡增生.TabStop = false;
            this.radio咽部_淋巴滤泡增生.Tag = "3";
            // 
            // radio咽部_其他
            // 
            this.radio咽部_其他.Location = new System.Drawing.Point(229, 0);
            this.radio咽部_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.radio咽部_其他.Name = "radio咽部_其他";
            this.radio咽部_其他.Properties.Caption = "其他";
            this.radio咽部_其他.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.radio咽部_其他.Properties.RadioGroupIndex = 0;
            this.radio咽部_其他.Size = new System.Drawing.Size(53, 19);
            this.radio咽部_其他.TabIndex = 3;
            this.radio咽部_其他.TabStop = false;
            this.radio咽部_其他.Tag = "4";
            this.radio咽部_其他.CheckedChanged += new System.EventHandler(this.radio咽部_其他_CheckedChanged);
            // 
            // txt咽部_其他
            // 
            this.txt咽部_其他.Enabled = false;
            this.txt咽部_其他.Location = new System.Drawing.Point(288, 0);
            this.txt咽部_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt咽部_其他.Name = "txt咽部_其他";
            this.txt咽部_其他.Size = new System.Drawing.Size(137, 20);
            this.txt咽部_其他.TabIndex = 4;
            // 
            // flow口唇
            // 
            this.flow口唇.BackColor = System.Drawing.Color.White;
            this.flow口唇.Controls.Add(this.radio口唇);
            this.flow口唇.Controls.Add(this.txt口唇其他);
            this.flow口唇.Location = new System.Drawing.Point(145, -2193);
            this.flow口唇.Margin = new System.Windows.Forms.Padding(0);
            this.flow口唇.Name = "flow口唇";
            this.flow口唇.Size = new System.Drawing.Size(625, 22);
            this.flow口唇.TabIndex = 59;
            // 
            // radio口唇
            // 
            this.radio口唇.EditValue = "1";
            this.radio口唇.Location = new System.Drawing.Point(0, 0);
            this.radio口唇.Margin = new System.Windows.Forms.Padding(0);
            this.radio口唇.Name = "radio口唇";
            this.radio口唇.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "红润"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "苍白"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "发绀"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "皲裂"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("5", "疱疹"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("6", "其他")});
            this.radio口唇.Size = new System.Drawing.Size(279, 22);
            this.radio口唇.TabIndex = 0;
            this.radio口唇.SelectedIndexChanged += new System.EventHandler(this.radio口唇_SelectedIndexChanged);
            // 
            // txt口唇其他
            // 
            this.txt口唇其他.Enabled = false;
            this.txt口唇其他.Location = new System.Drawing.Point(282, 0);
            this.txt口唇其他.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.txt口唇其他.Name = "txt口唇其他";
            this.txt口唇其他.Size = new System.Drawing.Size(143, 20);
            this.txt口唇其他.TabIndex = 1;
            // 
            // flow饮酒种类
            // 
            this.flow饮酒种类.BackColor = System.Drawing.Color.White;
            this.flow饮酒种类.Controls.Add(this.chk饮酒种类_白酒);
            this.flow饮酒种类.Controls.Add(this.chk饮酒种类_啤酒);
            this.flow饮酒种类.Controls.Add(this.chk饮酒种类_红酒);
            this.flow饮酒种类.Controls.Add(this.chk饮酒种类_黄酒);
            this.flow饮酒种类.Controls.Add(this.chk饮酒种类_其他);
            this.flow饮酒种类.Controls.Add(this.txt饮酒种类_其他);
            this.flow饮酒种类.Location = new System.Drawing.Point(200, -2401);
            this.flow饮酒种类.Margin = new System.Windows.Forms.Padding(0);
            this.flow饮酒种类.Name = "flow饮酒种类";
            this.flow饮酒种类.Size = new System.Drawing.Size(570, 24);
            this.flow饮酒种类.TabIndex = 42;
            // 
            // chk饮酒种类_白酒
            // 
            this.chk饮酒种类_白酒.Enabled = false;
            this.chk饮酒种类_白酒.Location = new System.Drawing.Point(3, 0);
            this.chk饮酒种类_白酒.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.chk饮酒种类_白酒.Name = "chk饮酒种类_白酒";
            this.chk饮酒种类_白酒.Properties.Caption = "白酒";
            this.chk饮酒种类_白酒.Size = new System.Drawing.Size(57, 19);
            this.chk饮酒种类_白酒.TabIndex = 0;
            this.chk饮酒种类_白酒.Tag = "1";
            // 
            // chk饮酒种类_啤酒
            // 
            this.chk饮酒种类_啤酒.Enabled = false;
            this.chk饮酒种类_啤酒.Location = new System.Drawing.Point(66, 0);
            this.chk饮酒种类_啤酒.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.chk饮酒种类_啤酒.Name = "chk饮酒种类_啤酒";
            this.chk饮酒种类_啤酒.Properties.Caption = "啤酒";
            this.chk饮酒种类_啤酒.Size = new System.Drawing.Size(55, 19);
            this.chk饮酒种类_啤酒.TabIndex = 1;
            this.chk饮酒种类_啤酒.Tag = "2";
            // 
            // chk饮酒种类_红酒
            // 
            this.chk饮酒种类_红酒.Enabled = false;
            this.chk饮酒种类_红酒.Location = new System.Drawing.Point(127, 0);
            this.chk饮酒种类_红酒.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.chk饮酒种类_红酒.Name = "chk饮酒种类_红酒";
            this.chk饮酒种类_红酒.Properties.Caption = "红酒";
            this.chk饮酒种类_红酒.Size = new System.Drawing.Size(52, 19);
            this.chk饮酒种类_红酒.TabIndex = 2;
            this.chk饮酒种类_红酒.Tag = "3";
            // 
            // chk饮酒种类_黄酒
            // 
            this.chk饮酒种类_黄酒.Enabled = false;
            this.chk饮酒种类_黄酒.Location = new System.Drawing.Point(185, 0);
            this.chk饮酒种类_黄酒.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.chk饮酒种类_黄酒.Name = "chk饮酒种类_黄酒";
            this.chk饮酒种类_黄酒.Properties.Caption = "黄酒";
            this.chk饮酒种类_黄酒.Size = new System.Drawing.Size(61, 19);
            this.chk饮酒种类_黄酒.TabIndex = 3;
            this.chk饮酒种类_黄酒.Tag = "4";
            // 
            // chk饮酒种类_其他
            // 
            this.chk饮酒种类_其他.Enabled = false;
            this.chk饮酒种类_其他.Location = new System.Drawing.Point(252, 0);
            this.chk饮酒种类_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 1);
            this.chk饮酒种类_其他.Name = "chk饮酒种类_其他";
            this.chk饮酒种类_其他.Properties.Caption = "其他";
            this.chk饮酒种类_其他.Size = new System.Drawing.Size(64, 19);
            this.chk饮酒种类_其他.TabIndex = 4;
            this.chk饮酒种类_其他.Tag = "99";
            this.chk饮酒种类_其他.CheckedChanged += new System.EventHandler(this.chk饮酒种类_其他_CheckedChanged);
            // 
            // txt饮酒种类_其他
            // 
            this.txt饮酒种类_其他.Enabled = false;
            this.txt饮酒种类_其他.Location = new System.Drawing.Point(322, 0);
            this.txt饮酒种类_其他.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.txt饮酒种类_其他.Name = "txt饮酒种类_其他";
            this.txt饮酒种类_其他.Size = new System.Drawing.Size(100, 20);
            this.txt饮酒种类_其他.TabIndex = 5;
            // 
            // radio近一年内是否曾醉酒
            // 
            this.radio近一年内是否曾醉酒.Enabled = false;
            this.radio近一年内是否曾醉酒.Location = new System.Drawing.Point(495, -2430);
            this.radio近一年内是否曾醉酒.Name = "radio近一年内是否曾醉酒";
            this.radio近一年内是否曾醉酒.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "是"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "否")});
            this.radio近一年内是否曾醉酒.Size = new System.Drawing.Size(85, 25);
            this.radio近一年内是否曾醉酒.StyleController = this.layoutControl1;
            this.radio近一年内是否曾醉酒.TabIndex = 41;
            // 
            // txt开始饮酒年龄
            // 
            this.txt开始饮酒年龄.BackColor = System.Drawing.Color.White;
            this.txt开始饮酒年龄.Enabled = false;
            this.txt开始饮酒年龄.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt开始饮酒年龄.Lbl1Text = "岁";
            this.txt开始饮酒年龄.Location = new System.Drawing.Point(200, -2430);
            this.txt开始饮酒年龄.Margin = new System.Windows.Forms.Padding(0);
            this.txt开始饮酒年龄.Name = "txt开始饮酒年龄";
            this.txt开始饮酒年龄.Size = new System.Drawing.Size(166, 25);
            this.txt开始饮酒年龄.TabIndex = 43;
            this.txt开始饮酒年龄.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt戒酒年龄
            // 
            this.txt戒酒年龄.BackColor = System.Drawing.Color.White;
            this.txt戒酒年龄.Enabled = false;
            this.txt戒酒年龄.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt戒酒年龄.Lbl1Text = "岁";
            this.txt戒酒年龄.Location = new System.Drawing.Point(495, -2459);
            this.txt戒酒年龄.Margin = new System.Windows.Forms.Padding(0);
            this.txt戒酒年龄.Name = "txt戒酒年龄";
            this.txt戒酒年龄.Size = new System.Drawing.Size(275, 25);
            this.txt戒酒年龄.TabIndex = 42;
            this.txt戒酒年龄.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // radio是否戒酒
            // 
            this.radio是否戒酒.Enabled = false;
            this.radio是否戒酒.Location = new System.Drawing.Point(200, -2459);
            this.radio是否戒酒.Name = "radio是否戒酒";
            this.radio是否戒酒.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "未戒酒"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "已戒酒")});
            this.radio是否戒酒.Size = new System.Drawing.Size(166, 25);
            this.radio是否戒酒.StyleController = this.layoutControl1;
            this.radio是否戒酒.TabIndex = 40;
            // 
            // txt日饮酒量
            // 
            this.txt日饮酒量.BackColor = System.Drawing.Color.White;
            this.txt日饮酒量.Enabled = false;
            this.txt日饮酒量.Lbl1Size = new System.Drawing.Size(60, 18);
            this.txt日饮酒量.Lbl1Text = "     平均";
            this.txt日饮酒量.Lbl2Size = new System.Drawing.Size(56, 18);
            this.txt日饮酒量.Lbl2Text = "（两）";
            this.txt日饮酒量.Location = new System.Drawing.Point(200, -2483);
            this.txt日饮酒量.Margin = new System.Windows.Forms.Padding(0);
            this.txt日饮酒量.Name = "txt日饮酒量";
            this.txt日饮酒量.Size = new System.Drawing.Size(570, 20);
            this.txt日饮酒量.TabIndex = 39;
            this.txt日饮酒量.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // radio饮酒频率
            // 
            this.radio饮酒频率.EditValue = "1";
            this.radio饮酒频率.Location = new System.Drawing.Point(200, -2512);
            this.radio饮酒频率.Name = "radio饮酒频率";
            this.radio饮酒频率.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "从不"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "偶尔"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "经常"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "每天")});
            this.radio饮酒频率.Size = new System.Drawing.Size(205, 25);
            this.radio饮酒频率.StyleController = this.layoutControl1;
            this.radio饮酒频率.TabIndex = 38;
            this.radio饮酒频率.SelectedIndexChanged += new System.EventHandler(this.radio饮酒频率_SelectedIndexChanged);
            // 
            // radio吸烟情况
            // 
            this.radio吸烟情况.EditValue = "1";
            this.radio吸烟情况.Location = new System.Drawing.Point(200, -2589);
            this.radio吸烟情况.Name = "radio吸烟情况";
            this.radio吸烟情况.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "从不吸烟"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "已戒烟"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "吸烟")});
            this.radio吸烟情况.Size = new System.Drawing.Size(211, 25);
            this.radio吸烟情况.StyleController = this.layoutControl1;
            this.radio吸烟情况.TabIndex = 34;
            this.radio吸烟情况.SelectedIndexChanged += new System.EventHandler(this.radio吸烟情况_SelectedIndexChanged);
            // 
            // txt戒烟年龄
            // 
            this.txt戒烟年龄.BackColor = System.Drawing.Color.White;
            this.txt戒烟年龄.Enabled = false;
            this.txt戒烟年龄.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt戒烟年龄.Lbl1Text = "岁";
            this.txt戒烟年龄.Location = new System.Drawing.Point(494, -2536);
            this.txt戒烟年龄.Margin = new System.Windows.Forms.Padding(0);
            this.txt戒烟年龄.Name = "txt戒烟年龄";
            this.txt戒烟年龄.Size = new System.Drawing.Size(276, 20);
            this.txt戒烟年龄.TabIndex = 37;
            this.txt戒烟年龄.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt开始吸烟年龄
            // 
            this.txt开始吸烟年龄.BackColor = System.Drawing.Color.White;
            this.txt开始吸烟年龄.Enabled = false;
            this.txt开始吸烟年龄.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt开始吸烟年龄.Lbl1Text = "岁";
            this.txt开始吸烟年龄.Location = new System.Drawing.Point(200, -2536);
            this.txt开始吸烟年龄.Margin = new System.Windows.Forms.Padding(0);
            this.txt开始吸烟年龄.Name = "txt开始吸烟年龄";
            this.txt开始吸烟年龄.Size = new System.Drawing.Size(165, 20);
            this.txt开始吸烟年龄.TabIndex = 36;
            this.txt开始吸烟年龄.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.BackColor = System.Drawing.Color.White;
            this.txt日吸烟量.Enabled = false;
            this.txt日吸烟量.Lbl1Size = new System.Drawing.Size(60, 18);
            this.txt日吸烟量.Lbl1Text = "      平均";
            this.txt日吸烟量.Lbl2Size = new System.Drawing.Size(30, 18);
            this.txt日吸烟量.Lbl2Text = "（支）";
            this.txt日吸烟量.Location = new System.Drawing.Point(200, -2560);
            this.txt日吸烟量.Margin = new System.Windows.Forms.Padding(0);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Size = new System.Drawing.Size(570, 20);
            this.txt日吸烟量.TabIndex = 35;
            this.txt日吸烟量.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flow饮食习惯
            // 
            this.flow饮食习惯.BackColor = System.Drawing.Color.White;
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_荤素均衡);
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_荤食为主);
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_素食为主);
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_嗜盐);
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_嗜油);
            this.flow饮食习惯.Controls.Add(this.chk饮食习惯_嗜糖);
            this.flow饮食习惯.Location = new System.Drawing.Point(98, -2617);
            this.flow饮食习惯.Margin = new System.Windows.Forms.Padding(0);
            this.flow饮食习惯.Name = "flow饮食习惯";
            this.flow饮食习惯.Size = new System.Drawing.Size(672, 24);
            this.flow饮食习惯.TabIndex = 33;
            // 
            // chk饮食习惯_荤素均衡
            // 
            this.chk饮食习惯_荤素均衡.EditValue = true;
            this.chk饮食习惯_荤素均衡.Location = new System.Drawing.Point(3, 1);
            this.chk饮食习惯_荤素均衡.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_荤素均衡.Name = "chk饮食习惯_荤素均衡";
            this.chk饮食习惯_荤素均衡.Properties.Caption = "荤素均衡";
            this.chk饮食习惯_荤素均衡.Size = new System.Drawing.Size(75, 19);
            this.chk饮食习惯_荤素均衡.TabIndex = 0;
            this.chk饮食习惯_荤素均衡.Tag = "1";
            // 
            // chk饮食习惯_荤食为主
            // 
            this.chk饮食习惯_荤食为主.Location = new System.Drawing.Point(84, 1);
            this.chk饮食习惯_荤食为主.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_荤食为主.Name = "chk饮食习惯_荤食为主";
            this.chk饮食习惯_荤食为主.Properties.Caption = "荤食为主";
            this.chk饮食习惯_荤食为主.Size = new System.Drawing.Size(75, 19);
            this.chk饮食习惯_荤食为主.TabIndex = 1;
            this.chk饮食习惯_荤食为主.Tag = "2";
            // 
            // chk饮食习惯_素食为主
            // 
            this.chk饮食习惯_素食为主.Location = new System.Drawing.Point(165, 1);
            this.chk饮食习惯_素食为主.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_素食为主.Name = "chk饮食习惯_素食为主";
            this.chk饮食习惯_素食为主.Properties.Caption = "素食为主";
            this.chk饮食习惯_素食为主.Size = new System.Drawing.Size(75, 19);
            this.chk饮食习惯_素食为主.TabIndex = 2;
            this.chk饮食习惯_素食为主.Tag = "3";
            // 
            // chk饮食习惯_嗜盐
            // 
            this.chk饮食习惯_嗜盐.Location = new System.Drawing.Point(246, 1);
            this.chk饮食习惯_嗜盐.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_嗜盐.Name = "chk饮食习惯_嗜盐";
            this.chk饮食习惯_嗜盐.Properties.Caption = "嗜盐";
            this.chk饮食习惯_嗜盐.Size = new System.Drawing.Size(56, 19);
            this.chk饮食习惯_嗜盐.TabIndex = 3;
            this.chk饮食习惯_嗜盐.Tag = "4";
            // 
            // chk饮食习惯_嗜油
            // 
            this.chk饮食习惯_嗜油.Location = new System.Drawing.Point(308, 1);
            this.chk饮食习惯_嗜油.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_嗜油.Name = "chk饮食习惯_嗜油";
            this.chk饮食习惯_嗜油.Properties.Caption = "嗜油";
            this.chk饮食习惯_嗜油.Size = new System.Drawing.Size(59, 19);
            this.chk饮食习惯_嗜油.TabIndex = 4;
            this.chk饮食习惯_嗜油.Tag = "5";
            // 
            // chk饮食习惯_嗜糖
            // 
            this.chk饮食习惯_嗜糖.Location = new System.Drawing.Point(373, 1);
            this.chk饮食习惯_嗜糖.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.chk饮食习惯_嗜糖.Name = "chk饮食习惯_嗜糖";
            this.chk饮食习惯_嗜糖.Properties.Caption = "嗜糖";
            this.chk饮食习惯_嗜糖.Size = new System.Drawing.Size(53, 19);
            this.chk饮食习惯_嗜糖.TabIndex = 5;
            this.chk饮食习惯_嗜糖.Tag = "6";
            // 
            // txt生活方式_锻炼方式
            // 
            this.txt生活方式_锻炼方式.Location = new System.Drawing.Point(200, -2641);
            this.txt生活方式_锻炼方式.Name = "txt生活方式_锻炼方式";
            this.txt生活方式_锻炼方式.Size = new System.Drawing.Size(570, 20);
            this.txt生活方式_锻炼方式.StyleController = this.layoutControl1;
            this.txt生活方式_锻炼方式.TabIndex = 32;
            // 
            // txt生活方式_坚持锻炼时间
            // 
            this.txt生活方式_坚持锻炼时间.BackColor = System.Drawing.Color.White;
            this.txt生活方式_坚持锻炼时间.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt生活方式_坚持锻炼时间.Lbl1Text = "年";
            this.txt生活方式_坚持锻炼时间.Location = new System.Drawing.Point(476, -2665);
            this.txt生活方式_坚持锻炼时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt生活方式_坚持锻炼时间.Name = "txt生活方式_坚持锻炼时间";
            this.txt生活方式_坚持锻炼时间.Size = new System.Drawing.Size(294, 20);
            this.txt生活方式_坚持锻炼时间.TabIndex = 31;
            this.txt生活方式_坚持锻炼时间.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt生活方式_每次锻炼时间
            // 
            this.txt生活方式_每次锻炼时间.BackColor = System.Drawing.Color.White;
            this.txt生活方式_每次锻炼时间.Lbl1Size = new System.Drawing.Size(30, 18);
            this.txt生活方式_每次锻炼时间.Lbl1Text = "分钟";
            this.txt生活方式_每次锻炼时间.Location = new System.Drawing.Point(200, -2665);
            this.txt生活方式_每次锻炼时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt生活方式_每次锻炼时间.Name = "txt生活方式_每次锻炼时间";
            this.txt生活方式_每次锻炼时间.Size = new System.Drawing.Size(167, 20);
            this.txt生活方式_每次锻炼时间.TabIndex = 30;
            this.txt生活方式_每次锻炼时间.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // radio生活方式_锻炼频率
            // 
            this.radio生活方式_锻炼频率.EditValue = "4";
            this.radio生活方式_锻炼频率.Location = new System.Drawing.Point(200, -2694);
            this.radio生活方式_锻炼频率.Name = "radio生活方式_锻炼频率";
            this.radio生活方式_锻炼频率.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "每天"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "每周一次以上"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "偶尔"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("4", "不锻炼")});
            this.radio生活方式_锻炼频率.Size = new System.Drawing.Size(380, 25);
            this.radio生活方式_锻炼频率.StyleController = this.layoutControl1;
            this.radio生活方式_锻炼频率.TabIndex = 29;
            this.radio生活方式_锻炼频率.SelectedIndexChanged += new System.EventHandler(this.radio生活方式_锻炼频率_SelectedIndexChanged);
            // 
            // txt一般情况_体重指数
            // 
            this.txt一般情况_体重指数.BackColor = System.Drawing.Color.White;
            this.txt一般情况_体重指数.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt一般情况_体重指数.Lbl1Text = "Kg/㎡";
            this.txt一般情况_体重指数.Location = new System.Drawing.Point(330, -2924);
            this.txt一般情况_体重指数.Margin = new System.Windows.Forms.Padding(0);
            this.txt一般情况_体重指数.Name = "txt一般情况_体重指数";
            this.txt一般情况_体重指数.Size = new System.Drawing.Size(440, 20);
            this.txt一般情况_体重指数.TabIndex = 24;
            this.txt一般情况_体重指数.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt一般情况_腰围
            // 
            this.txt一般情况_腰围.BackColor = System.Drawing.Color.White;
            this.txt一般情况_腰围.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt一般情况_腰围.Lbl1Text = "cm";
            this.txt一般情况_腰围.Location = new System.Drawing.Point(98, -2924);
            this.txt一般情况_腰围.Margin = new System.Windows.Forms.Padding(0);
            this.txt一般情况_腰围.Name = "txt一般情况_腰围";
            this.txt一般情况_腰围.Size = new System.Drawing.Size(133, 20);
            this.txt一般情况_腰围.TabIndex = 23;
            this.txt一般情况_腰围.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt一般情况_体重
            // 
            this.txt一般情况_体重.BackColor = System.Drawing.Color.White;
            this.txt一般情况_体重.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt一般情况_体重.Lbl1Text = "kg";
            this.txt一般情况_体重.Location = new System.Drawing.Point(330, -2948);
            this.txt一般情况_体重.Margin = new System.Windows.Forms.Padding(0);
            this.txt一般情况_体重.Name = "txt一般情况_体重";
            this.txt一般情况_体重.Size = new System.Drawing.Size(440, 20);
            this.txt一般情况_体重.TabIndex = 22;
            this.txt一般情况_体重.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt一般情况_身高
            // 
            this.txt一般情况_身高.BackColor = System.Drawing.Color.White;
            this.txt一般情况_身高.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt一般情况_身高.Lbl1Text = "cm";
            this.txt一般情况_身高.Location = new System.Drawing.Point(98, -2948);
            this.txt一般情况_身高.Margin = new System.Windows.Forms.Padding(0);
            this.txt一般情况_身高.Name = "txt一般情况_身高";
            this.txt一般情况_身高.Size = new System.Drawing.Size(133, 20);
            this.txt一般情况_身高.TabIndex = 21;
            this.txt一般情况_身高.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt一般情况_血压右侧原因
            // 
            this.txt一般情况_血压右侧原因.Location = new System.Drawing.Point(624, -2972);
            this.txt一般情况_血压右侧原因.Name = "txt一般情况_血压右侧原因";
            this.txt一般情况_血压右侧原因.Size = new System.Drawing.Size(146, 20);
            this.txt一般情况_血压右侧原因.StyleController = this.layoutControl1;
            this.txt一般情况_血压右侧原因.TabIndex = 20;
            // 
            // txt一般情况_血压左侧原因
            // 
            this.txt一般情况_血压左侧原因.EditValue = "";
            this.txt一般情况_血压左侧原因.Location = new System.Drawing.Point(624, -2996);
            this.txt一般情况_血压左侧原因.Name = "txt一般情况_血压左侧原因";
            this.txt一般情况_血压左侧原因.Properties.Mask.EditMask = "##.##";
            this.txt一般情况_血压左侧原因.Size = new System.Drawing.Size(146, 20);
            this.txt一般情况_血压左侧原因.StyleController = this.layoutControl1;
            this.txt一般情况_血压左侧原因.TabIndex = 18;
            // 
            // txt一般情况_血压右侧
            // 
            this.txt一般情况_血压右侧.BackColor = System.Drawing.Color.White;
            this.txt一般情况_血压右侧.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt一般情况_血压右侧.Lbl1Text = "/";
            this.txt一般情况_血压右侧.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt一般情况_血压右侧.Lbl2Text = "mmHg";
            this.txt一般情况_血压右侧.Location = new System.Drawing.Point(367, -2972);
            this.txt一般情况_血压右侧.Margin = new System.Windows.Forms.Padding(4);
            this.txt一般情况_血压右侧.Name = "txt一般情况_血压右侧";
            this.txt一般情况_血压右侧.Size = new System.Drawing.Size(168, 20);
            this.txt一般情况_血压右侧.TabIndex = 19;
            this.txt一般情况_血压右侧.Txt1EditValue = null;
            this.txt一般情况_血压右侧.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt一般情况_血压右侧.Txt2EditValue = null;
            this.txt一般情况_血压右侧.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt一般情况_血压左侧
            // 
            this.txt一般情况_血压左侧.BackColor = System.Drawing.Color.White;
            this.txt一般情况_血压左侧.Lbl1Size = new System.Drawing.Size(10, 14);
            this.txt一般情况_血压左侧.Lbl1Text = "/";
            this.txt一般情况_血压左侧.Lbl2Size = new System.Drawing.Size(40, 14);
            this.txt一般情况_血压左侧.Lbl2Text = "mmHg";
            this.txt一般情况_血压左侧.Location = new System.Drawing.Point(367, -2996);
            this.txt一般情况_血压左侧.Margin = new System.Windows.Forms.Padding(4);
            this.txt一般情况_血压左侧.Name = "txt一般情况_血压左侧";
            this.txt一般情况_血压左侧.Size = new System.Drawing.Size(168, 20);
            this.txt一般情况_血压左侧.TabIndex = 17;
            this.txt一般情况_血压左侧.Txt1EditValue = null;
            this.txt一般情况_血压左侧.Txt1Size = new System.Drawing.Size(50, 20);
            this.txt一般情况_血压左侧.Txt2EditValue = null;
            this.txt一般情况_血压左侧.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // txt一般情况_呼吸频率
            // 
            this.txt一般情况_呼吸频率.BackColor = System.Drawing.Color.White;
            this.txt一般情况_呼吸频率.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt一般情况_呼吸频率.Lbl1Text = "次/分钟";
            this.txt一般情况_呼吸频率.Location = new System.Drawing.Point(98, -2996);
            this.txt一般情况_呼吸频率.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.txt一般情况_呼吸频率.Name = "txt一般情况_呼吸频率";
            this.txt一般情况_呼吸频率.Padding = new System.Windows.Forms.Padding(0, 12, 0, 0);
            this.txt一般情况_呼吸频率.Size = new System.Drawing.Size(133, 44);
            this.txt一般情况_呼吸频率.TabIndex = 16;
            this.txt一般情况_呼吸频率.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // txt一般情况_脉率
            // 
            this.txt一般情况_脉率.BackColor = System.Drawing.Color.White;
            this.txt一般情况_脉率.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt一般情况_脉率.Lbl1Text = "次/分钟";
            this.txt一般情况_脉率.Location = new System.Drawing.Point(330, -3020);
            this.txt一般情况_脉率.Margin = new System.Windows.Forms.Padding(0);
            this.txt一般情况_脉率.Name = "txt一般情况_脉率";
            this.txt一般情况_脉率.Size = new System.Drawing.Size(440, 20);
            this.txt一般情况_脉率.TabIndex = 15;
            this.txt一般情况_脉率.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt一般情况_体温
            // 
            this.txt一般情况_体温.BackColor = System.Drawing.Color.White;
            this.txt一般情况_体温.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt一般情况_体温.Lbl1Text = "℃";
            this.txt一般情况_体温.Location = new System.Drawing.Point(98, -3020);
            this.txt一般情况_体温.Margin = new System.Windows.Forms.Padding(0);
            this.txt一般情况_体温.Name = "txt一般情况_体温";
            this.txt一般情况_体温.Size = new System.Drawing.Size(133, 20);
            this.txt一般情况_体温.TabIndex = 14;
            this.txt一般情况_体温.Txt1Size = new System.Drawing.Size(70, 20);
            // 
            // flow症状
            // 
            this.flow症状.BackColor = System.Drawing.Color.White;
            this.flow症状.Controls.Add(this.chk症状_无症状);
            this.flow症状.Controls.Add(this.chk症状_头痛);
            this.flow症状.Controls.Add(this.chk症状_头晕);
            this.flow症状.Controls.Add(this.chk症状_心悸);
            this.flow症状.Controls.Add(this.chk症状_胸闷);
            this.flow症状.Controls.Add(this.chk症状_胸痛);
            this.flow症状.Controls.Add(this.chk症状_慢性咳嗽);
            this.flow症状.Controls.Add(this.chk症状_咳痰);
            this.flow症状.Controls.Add(this.chk症状_呼吸困难);
            this.flow症状.Controls.Add(this.chk症状_多饮);
            this.flow症状.Controls.Add(this.chk症状_多尿);
            this.flow症状.Controls.Add(this.chk症状_体重下降);
            this.flow症状.Controls.Add(this.chk症状_乏力);
            this.flow症状.Controls.Add(this.chk症状_关节肿痛);
            this.flow症状.Controls.Add(this.chk症状_视力模糊);
            this.flow症状.Controls.Add(this.chk症状_手脚麻木);
            this.flow症状.Controls.Add(this.chk症状_尿急);
            this.flow症状.Controls.Add(this.chk症状_尿痛);
            this.flow症状.Controls.Add(this.chk症状_便秘);
            this.flow症状.Controls.Add(this.chk症状_腹泻);
            this.flow症状.Controls.Add(this.chk症状_恶心呕吐);
            this.flow症状.Controls.Add(this.chk症状_眼花);
            this.flow症状.Controls.Add(this.chk症状_耳鸣);
            this.flow症状.Controls.Add(this.chk症状_乳房胀痛);
            this.flow症状.Controls.Add(this.chk症状_其他);
            this.flow症状.Controls.Add(this.txt症状_其他);
            this.flow症状.Location = new System.Drawing.Point(13, -3126);
            this.flow症状.Name = "flow症状";
            this.flow症状.Size = new System.Drawing.Size(757, 66);
            this.flow症状.TabIndex = 13;
            // 
            // chk症状_无症状
            // 
            this.chk症状_无症状.EditValue = true;
            this.chk症状_无症状.Location = new System.Drawing.Point(3, 1);
            this.chk症状_无症状.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_无症状.Name = "chk症状_无症状";
            this.chk症状_无症状.Properties.Caption = "无症状";
            this.chk症状_无症状.Size = new System.Drawing.Size(62, 19);
            this.chk症状_无症状.TabIndex = 0;
            this.chk症状_无症状.Tag = "1";
            this.chk症状_无症状.CheckedChanged += new System.EventHandler(this.chk_CheckedChanged);
            // 
            // chk症状_头痛
            // 
            this.chk症状_头痛.Enabled = false;
            this.chk症状_头痛.Location = new System.Drawing.Point(71, 1);
            this.chk症状_头痛.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_头痛.Name = "chk症状_头痛";
            this.chk症状_头痛.Properties.Caption = "头痛";
            this.chk症状_头痛.Size = new System.Drawing.Size(56, 19);
            this.chk症状_头痛.TabIndex = 1;
            this.chk症状_头痛.Tag = "2";
            // 
            // chk症状_头晕
            // 
            this.chk症状_头晕.Enabled = false;
            this.chk症状_头晕.Location = new System.Drawing.Point(133, 1);
            this.chk症状_头晕.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_头晕.Name = "chk症状_头晕";
            this.chk症状_头晕.Properties.Caption = "头晕";
            this.chk症状_头晕.Size = new System.Drawing.Size(51, 19);
            this.chk症状_头晕.TabIndex = 2;
            this.chk症状_头晕.Tag = "3";
            // 
            // chk症状_心悸
            // 
            this.chk症状_心悸.Enabled = false;
            this.chk症状_心悸.Location = new System.Drawing.Point(190, 1);
            this.chk症状_心悸.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_心悸.Name = "chk症状_心悸";
            this.chk症状_心悸.Properties.Caption = "心悸";
            this.chk症状_心悸.Size = new System.Drawing.Size(50, 19);
            this.chk症状_心悸.TabIndex = 3;
            this.chk症状_心悸.Tag = "4";
            // 
            // chk症状_胸闷
            // 
            this.chk症状_胸闷.Enabled = false;
            this.chk症状_胸闷.Location = new System.Drawing.Point(246, 1);
            this.chk症状_胸闷.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_胸闷.Name = "chk症状_胸闷";
            this.chk症状_胸闷.Properties.Caption = "胸闷";
            this.chk症状_胸闷.Size = new System.Drawing.Size(54, 19);
            this.chk症状_胸闷.TabIndex = 4;
            this.chk症状_胸闷.Tag = "5";
            // 
            // chk症状_胸痛
            // 
            this.chk症状_胸痛.Enabled = false;
            this.chk症状_胸痛.Location = new System.Drawing.Point(306, 1);
            this.chk症状_胸痛.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_胸痛.Name = "chk症状_胸痛";
            this.chk症状_胸痛.Properties.Caption = "胸痛";
            this.chk症状_胸痛.Size = new System.Drawing.Size(50, 19);
            this.chk症状_胸痛.TabIndex = 5;
            this.chk症状_胸痛.Tag = "6";
            // 
            // chk症状_慢性咳嗽
            // 
            this.chk症状_慢性咳嗽.Enabled = false;
            this.chk症状_慢性咳嗽.Location = new System.Drawing.Point(362, 1);
            this.chk症状_慢性咳嗽.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_慢性咳嗽.Name = "chk症状_慢性咳嗽";
            this.chk症状_慢性咳嗽.Properties.Caption = "慢性咳嗽";
            this.chk症状_慢性咳嗽.Size = new System.Drawing.Size(75, 19);
            this.chk症状_慢性咳嗽.TabIndex = 6;
            this.chk症状_慢性咳嗽.Tag = "7";
            // 
            // chk症状_咳痰
            // 
            this.chk症状_咳痰.Enabled = false;
            this.chk症状_咳痰.Location = new System.Drawing.Point(443, 1);
            this.chk症状_咳痰.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_咳痰.Name = "chk症状_咳痰";
            this.chk症状_咳痰.Properties.Caption = "咳痰";
            this.chk症状_咳痰.Size = new System.Drawing.Size(54, 19);
            this.chk症状_咳痰.TabIndex = 7;
            this.chk症状_咳痰.Tag = "8";
            // 
            // chk症状_呼吸困难
            // 
            this.chk症状_呼吸困难.Enabled = false;
            this.chk症状_呼吸困难.Location = new System.Drawing.Point(503, 1);
            this.chk症状_呼吸困难.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_呼吸困难.Name = "chk症状_呼吸困难";
            this.chk症状_呼吸困难.Properties.Caption = "呼吸困难";
            this.chk症状_呼吸困难.Size = new System.Drawing.Size(75, 19);
            this.chk症状_呼吸困难.TabIndex = 8;
            this.chk症状_呼吸困难.Tag = "9";
            // 
            // chk症状_多饮
            // 
            this.chk症状_多饮.Enabled = false;
            this.chk症状_多饮.Location = new System.Drawing.Point(584, 1);
            this.chk症状_多饮.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_多饮.Name = "chk症状_多饮";
            this.chk症状_多饮.Properties.Caption = "多饮";
            this.chk症状_多饮.Size = new System.Drawing.Size(52, 19);
            this.chk症状_多饮.TabIndex = 9;
            this.chk症状_多饮.Tag = "10";
            // 
            // chk症状_多尿
            // 
            this.chk症状_多尿.Enabled = false;
            this.chk症状_多尿.Location = new System.Drawing.Point(642, 1);
            this.chk症状_多尿.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_多尿.Name = "chk症状_多尿";
            this.chk症状_多尿.Properties.Caption = "多尿";
            this.chk症状_多尿.Size = new System.Drawing.Size(62, 19);
            this.chk症状_多尿.TabIndex = 10;
            this.chk症状_多尿.Tag = "11";
            // 
            // chk症状_体重下降
            // 
            this.chk症状_体重下降.Enabled = false;
            this.chk症状_体重下降.Location = new System.Drawing.Point(3, 22);
            this.chk症状_体重下降.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_体重下降.Name = "chk症状_体重下降";
            this.chk症状_体重下降.Properties.Caption = "体重下降";
            this.chk症状_体重下降.Size = new System.Drawing.Size(75, 19);
            this.chk症状_体重下降.TabIndex = 11;
            this.chk症状_体重下降.Tag = "12";
            // 
            // chk症状_乏力
            // 
            this.chk症状_乏力.Enabled = false;
            this.chk症状_乏力.Location = new System.Drawing.Point(84, 22);
            this.chk症状_乏力.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_乏力.Name = "chk症状_乏力";
            this.chk症状_乏力.Properties.Caption = "乏力";
            this.chk症状_乏力.Size = new System.Drawing.Size(44, 19);
            this.chk症状_乏力.TabIndex = 12;
            this.chk症状_乏力.Tag = "13";
            // 
            // chk症状_关节肿痛
            // 
            this.chk症状_关节肿痛.Enabled = false;
            this.chk症状_关节肿痛.Location = new System.Drawing.Point(134, 22);
            this.chk症状_关节肿痛.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_关节肿痛.Name = "chk症状_关节肿痛";
            this.chk症状_关节肿痛.Properties.Caption = "关节肿痛";
            this.chk症状_关节肿痛.Size = new System.Drawing.Size(75, 19);
            this.chk症状_关节肿痛.TabIndex = 13;
            this.chk症状_关节肿痛.Tag = "14";
            // 
            // chk症状_视力模糊
            // 
            this.chk症状_视力模糊.Enabled = false;
            this.chk症状_视力模糊.Location = new System.Drawing.Point(215, 22);
            this.chk症状_视力模糊.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_视力模糊.Name = "chk症状_视力模糊";
            this.chk症状_视力模糊.Properties.Caption = "视力模糊";
            this.chk症状_视力模糊.Size = new System.Drawing.Size(75, 19);
            this.chk症状_视力模糊.TabIndex = 14;
            this.chk症状_视力模糊.Tag = "15";
            // 
            // chk症状_手脚麻木
            // 
            this.chk症状_手脚麻木.Enabled = false;
            this.chk症状_手脚麻木.Location = new System.Drawing.Point(296, 22);
            this.chk症状_手脚麻木.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_手脚麻木.Name = "chk症状_手脚麻木";
            this.chk症状_手脚麻木.Properties.Caption = "手脚麻木";
            this.chk症状_手脚麻木.Size = new System.Drawing.Size(75, 19);
            this.chk症状_手脚麻木.TabIndex = 15;
            this.chk症状_手脚麻木.Tag = "16";
            // 
            // chk症状_尿急
            // 
            this.chk症状_尿急.Enabled = false;
            this.chk症状_尿急.Location = new System.Drawing.Point(377, 22);
            this.chk症状_尿急.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_尿急.Name = "chk症状_尿急";
            this.chk症状_尿急.Properties.Caption = "尿急";
            this.chk症状_尿急.Size = new System.Drawing.Size(52, 19);
            this.chk症状_尿急.TabIndex = 16;
            this.chk症状_尿急.Tag = "17";
            // 
            // chk症状_尿痛
            // 
            this.chk症状_尿痛.Enabled = false;
            this.chk症状_尿痛.Location = new System.Drawing.Point(435, 22);
            this.chk症状_尿痛.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_尿痛.Name = "chk症状_尿痛";
            this.chk症状_尿痛.Properties.Caption = "尿痛";
            this.chk症状_尿痛.Size = new System.Drawing.Size(52, 19);
            this.chk症状_尿痛.TabIndex = 17;
            this.chk症状_尿痛.Tag = "18";
            // 
            // chk症状_便秘
            // 
            this.chk症状_便秘.Enabled = false;
            this.chk症状_便秘.Location = new System.Drawing.Point(493, 22);
            this.chk症状_便秘.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_便秘.Name = "chk症状_便秘";
            this.chk症状_便秘.Properties.Caption = "便秘";
            this.chk症状_便秘.Size = new System.Drawing.Size(50, 19);
            this.chk症状_便秘.TabIndex = 18;
            this.chk症状_便秘.Tag = "19";
            // 
            // chk症状_腹泻
            // 
            this.chk症状_腹泻.Enabled = false;
            this.chk症状_腹泻.Location = new System.Drawing.Point(549, 22);
            this.chk症状_腹泻.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_腹泻.Name = "chk症状_腹泻";
            this.chk症状_腹泻.Properties.Caption = "腹泻";
            this.chk症状_腹泻.Size = new System.Drawing.Size(49, 19);
            this.chk症状_腹泻.TabIndex = 19;
            this.chk症状_腹泻.Tag = "20";
            // 
            // chk症状_恶心呕吐
            // 
            this.chk症状_恶心呕吐.Enabled = false;
            this.chk症状_恶心呕吐.Location = new System.Drawing.Point(604, 22);
            this.chk症状_恶心呕吐.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_恶心呕吐.Name = "chk症状_恶心呕吐";
            this.chk症状_恶心呕吐.Properties.Caption = "恶心呕吐";
            this.chk症状_恶心呕吐.Size = new System.Drawing.Size(75, 19);
            this.chk症状_恶心呕吐.TabIndex = 20;
            this.chk症状_恶心呕吐.Tag = "21";
            // 
            // chk症状_眼花
            // 
            this.chk症状_眼花.Enabled = false;
            this.chk症状_眼花.Location = new System.Drawing.Point(685, 22);
            this.chk症状_眼花.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_眼花.Name = "chk症状_眼花";
            this.chk症状_眼花.Properties.Caption = "眼花";
            this.chk症状_眼花.Size = new System.Drawing.Size(59, 19);
            this.chk症状_眼花.TabIndex = 21;
            this.chk症状_眼花.Tag = "22";
            // 
            // chk症状_耳鸣
            // 
            this.chk症状_耳鸣.Enabled = false;
            this.chk症状_耳鸣.Location = new System.Drawing.Point(3, 43);
            this.chk症状_耳鸣.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_耳鸣.Name = "chk症状_耳鸣";
            this.chk症状_耳鸣.Properties.Caption = "耳鸣";
            this.chk症状_耳鸣.Size = new System.Drawing.Size(60, 19);
            this.chk症状_耳鸣.TabIndex = 22;
            this.chk症状_耳鸣.Tag = "23";
            // 
            // chk症状_乳房胀痛
            // 
            this.chk症状_乳房胀痛.Enabled = false;
            this.chk症状_乳房胀痛.Location = new System.Drawing.Point(69, 43);
            this.chk症状_乳房胀痛.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_乳房胀痛.Name = "chk症状_乳房胀痛";
            this.chk症状_乳房胀痛.Properties.Caption = "乳房胀痛";
            this.chk症状_乳房胀痛.Size = new System.Drawing.Size(75, 19);
            this.chk症状_乳房胀痛.TabIndex = 23;
            this.chk症状_乳房胀痛.Tag = "24";
            // 
            // chk症状_其他
            // 
            this.chk症状_其他.Enabled = false;
            this.chk症状_其他.Location = new System.Drawing.Point(150, 43);
            this.chk症状_其他.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.chk症状_其他.Name = "chk症状_其他";
            this.chk症状_其他.Properties.Caption = "其他";
            this.chk症状_其他.Size = new System.Drawing.Size(54, 19);
            this.chk症状_其他.TabIndex = 24;
            this.chk症状_其他.Tag = "99";
            this.chk症状_其他.CheckedChanged += new System.EventHandler(this.chk其他_CheckedChanged);
            // 
            // txt症状_其他
            // 
            this.txt症状_其他.Enabled = false;
            this.txt症状_其他.Location = new System.Drawing.Point(210, 43);
            this.txt症状_其他.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.txt症状_其他.Name = "txt症状_其他";
            this.txt症状_其他.Size = new System.Drawing.Size(191, 20);
            this.txt症状_其他.TabIndex = 25;
            // 
            // text责任医生
            // 
            this.text责任医生.Location = new System.Drawing.Point(408, -3178);
            this.text责任医生.Name = "text责任医生";
            this.text责任医生.Size = new System.Drawing.Size(110, 20);
            this.text责任医生.StyleController = this.layoutControl1;
            this.text责任医生.TabIndex = 12;
            // 
            // text居住地址
            // 
            this.text居住地址.Location = new System.Drawing.Point(84, -3204);
            this.text居住地址.Name = "text居住地址";
            this.text居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.text居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.text居住地址.Properties.ReadOnly = true;
            this.text居住地址.Size = new System.Drawing.Size(694, 20);
            this.text居住地址.StyleController = this.layoutControl1;
            this.text居住地址.TabIndex = 10;
            // 
            // text联系电话
            // 
            this.text联系电话.Location = new System.Drawing.Point(625, -3228);
            this.text联系电话.Name = "text联系电话";
            this.text联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.text联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.text联系电话.Properties.ReadOnly = true;
            this.text联系电话.Size = new System.Drawing.Size(153, 20);
            this.text联系电话.StyleController = this.layoutControl1;
            this.text联系电话.TabIndex = 9;
            // 
            // text出生日期
            // 
            this.text出生日期.Location = new System.Drawing.Point(346, -3228);
            this.text出生日期.Name = "text出生日期";
            this.text出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.text出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.text出生日期.Properties.ReadOnly = true;
            this.text出生日期.Size = new System.Drawing.Size(196, 20);
            this.text出生日期.StyleController = this.layoutControl1;
            this.text出生日期.TabIndex = 8;
            // 
            // text身份证号
            // 
            this.text身份证号.Location = new System.Drawing.Point(84, -3228);
            this.text身份证号.Name = "text身份证号";
            this.text身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.text身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.text身份证号.Properties.ReadOnly = true;
            this.text身份证号.Size = new System.Drawing.Size(179, 20);
            this.text身份证号.StyleController = this.layoutControl1;
            this.text身份证号.TabIndex = 7;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(625, -3252);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(153, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 6;
            // 
            // text姓名
            // 
            this.text姓名.Location = new System.Drawing.Point(346, -3252);
            this.text姓名.Name = "text姓名";
            this.text姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.text姓名.Properties.Appearance.Options.UseBackColor = true;
            this.text姓名.Properties.ReadOnly = true;
            this.text姓名.Size = new System.Drawing.Size(196, 20);
            this.text姓名.StyleController = this.layoutControl1;
            this.text姓名.TabIndex = 5;
            // 
            // text个人档案编号
            // 
            this.text个人档案编号.Location = new System.Drawing.Point(84, -3252);
            this.text个人档案编号.Name = "text个人档案编号";
            this.text个人档案编号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.text个人档案编号.Properties.Appearance.Options.UseBackColor = true;
            this.text个人档案编号.Properties.ReadOnly = true;
            this.text个人档案编号.Size = new System.Drawing.Size(179, 20);
            this.text个人档案编号.StyleController = this.layoutControl1;
            this.text个人档案编号.TabIndex = 4;
            // 
            // dte体检日期
            // 
            this.dte体检日期.EditValue = null;
            this.dte体检日期.Location = new System.Drawing.Point(84, -3178);
            this.dte体检日期.Name = "dte体检日期";
            this.dte体检日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte体检日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte体检日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte体检日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte体检日期.Size = new System.Drawing.Size(135, 20);
            this.dte体检日期.StyleController = this.layoutControl1;
            this.dte体检日期.TabIndex = 11;
            // 
            // dte创建时间
            // 
            this.dte创建时间.EditValue = null;
            this.dte创建时间.Location = new System.Drawing.Point(89, 771);
            this.dte创建时间.Name = "dte创建时间";
            this.dte创建时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte创建时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte创建时间.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.dte创建时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte创建时间.Properties.ReadOnly = true;
            this.dte创建时间.Size = new System.Drawing.Size(154, 20);
            this.dte创建时间.StyleController = this.layoutControl1;
            this.dte创建时间.TabIndex = 159;
            // 
            // dte最近更新时间
            // 
            this.dte最近更新时间.EditValue = null;
            this.dte最近更新时间.Location = new System.Drawing.Point(326, 795);
            this.dte最近更新时间.Name = "dte最近更新时间";
            this.dte最近更新时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte最近更新时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte最近更新时间.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss";
            this.dte最近更新时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte最近更新时间.Properties.ReadOnly = true;
            this.dte最近更新时间.Size = new System.Drawing.Size(150, 20);
            this.dte最近更新时间.StyleController = this.layoutControl1;
            this.dte最近更新时间.TabIndex = 160;
            // 
            // lbl血管疾病
            // 
            this.lbl血管疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl血管疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血管疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血管疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血管疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血管疾病.Control = this.flow血管疾病;
            this.lbl血管疾病.CustomizationFormText = "血管疾病";
            this.lbl血管疾病.Location = new System.Drawing.Point(0, 102);
            this.lbl血管疾病.MinSize = new System.Drawing.Size(199, 26);
            this.lbl血管疾病.Name = "lbl血管疾病";
            this.lbl血管疾病.Size = new System.Drawing.Size(761, 26);
            this.lbl血管疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血管疾病.Text = "血管疾病";
            this.lbl血管疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血管疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl血管疾病.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl体检日期,
            this.lbl责任医生,
            this.emptySpaceItem1,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.layoutControlGroup9,
            this.group辅助查体,
            this.layoutControlGroup11,
            this.layoutControlGroup12,
            this.layoutControlGroup13,
            this.layoutControlGroup14,
            this.layoutControlGroup15,
            this.group健康评价,
            this.group健康指导,
            this.emptySpaceItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -3282);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(783, 4107);
            this.layoutControlGroup1.Text = "健康体检表";
            // 
            // lbl体检日期
            // 
            this.lbl体检日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl体检日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl体检日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体检日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体检日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl体检日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl体检日期.Control = this.dte体检日期;
            this.lbl体检日期.CustomizationFormText = "体检日期";
            this.lbl体检日期.Location = new System.Drawing.Point(0, 74);
            this.lbl体检日期.Name = "lbl体检日期";
            this.lbl体检日期.Size = new System.Drawing.Size(218, 24);
            this.lbl体检日期.Text = "体检日期";
            this.lbl体检日期.TextSize = new System.Drawing.Size(76, 14);
            // 
            // lbl责任医生
            // 
            this.lbl责任医生.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl责任医生.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl责任医生.AppearanceItemCaption.Options.UseFont = true;
            this.lbl责任医生.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl责任医生.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl责任医生.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl责任医生.Control = this.text责任医生;
            this.lbl责任医生.CustomizationFormText = "责任医生";
            this.lbl责任医生.Location = new System.Drawing.Point(324, 74);
            this.lbl责任医生.Name = "lbl责任医生";
            this.lbl责任医生.Size = new System.Drawing.Size(193, 24);
            this.lbl责任医生.Text = "责任医生";
            this.lbl责任医生.TextSize = new System.Drawing.Size(76, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(517, 74);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(260, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "个人信息";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.simpleSeparator1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(9, 9, 9, 9);
            this.layoutControlGroup2.Size = new System.Drawing.Size(777, 74);
            this.layoutControlGroup2.Text = "个人信息";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.text居住地址;
            this.layoutControlItem7.CustomizationFormText = "居住地址";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(777, 24);
            this.layoutControlItem7.Text = "居住地址";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.text身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem4.Text = "身份证号";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.text个人档案编号;
            this.layoutControlItem1.CustomizationFormText = "个人档案号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem1.Text = "个人档案号";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.text姓名;
            this.layoutControlItem2.CustomizationFormText = "姓名";
            this.layoutControlItem2.Location = new System.Drawing.Point(262, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem2.Text = "姓名";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.text出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(262, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt性别;
            this.layoutControlItem3.CustomizationFormText = "性别";
            this.layoutControlItem3.Location = new System.Drawing.Point(541, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem3.Text = "性别";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.text联系电话;
            this.layoutControlItem6.CustomizationFormText = "联系电话";
            this.layoutControlItem6.Location = new System.Drawing.Point(541, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem6.Text = "联系电话";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(76, 14);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.AppearanceItemCaption.ForeColor = System.Drawing.Color.Black;
            this.simpleSeparator1.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(777, 2);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 204);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup3.Size = new System.Drawing.Size(777, 326);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup6.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup6.AppearanceGroup.Options.UseForeColor = true;
            this.layoutControlGroup6.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup6.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup6.CustomizationFormText = "一般情况";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.ExpandOnDoubleClick = true;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl体温,
            this.lbl呼吸频率,
            this.lbl身高,
            this.lbl腰围,
            this.lbl体重指数,
            this.lbl体重,
            this.simpleLabelItem4,
            this.lbl脉率,
            this.lbl血压左侧,
            this.lbl血压右侧,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.group老年人评估});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup6.Size = new System.Drawing.Size(777, 326);
            this.layoutControlGroup6.Text = "一般情况";
            // 
            // lbl体温
            // 
            this.lbl体温.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl体温.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl体温.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体温.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体温.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl体温.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl体温.Control = this.txt一般情况_体温;
            this.lbl体温.CustomizationFormText = "体温";
            this.lbl体温.Location = new System.Drawing.Point(0, 0);
            this.lbl体温.Name = "lbl体温";
            this.lbl体温.Size = new System.Drawing.Size(222, 24);
            this.lbl体温.Text = "体温";
            this.lbl体温.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl体温.TextSize = new System.Drawing.Size(80, 14);
            this.lbl体温.TextToControlDistance = 5;
            // 
            // lbl呼吸频率
            // 
            this.lbl呼吸频率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl呼吸频率.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl呼吸频率.AppearanceItemCaption.Options.UseFont = true;
            this.lbl呼吸频率.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl呼吸频率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl呼吸频率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl呼吸频率.Control = this.txt一般情况_呼吸频率;
            this.lbl呼吸频率.CustomizationFormText = "呼吸频率";
            this.lbl呼吸频率.Location = new System.Drawing.Point(0, 24);
            this.lbl呼吸频率.Name = "lbl呼吸频率";
            this.lbl呼吸频率.Size = new System.Drawing.Size(222, 48);
            this.lbl呼吸频率.Text = "呼吸频率";
            this.lbl呼吸频率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl呼吸频率.TextSize = new System.Drawing.Size(80, 14);
            this.lbl呼吸频率.TextToControlDistance = 5;
            // 
            // lbl身高
            // 
            this.lbl身高.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl身高.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl身高.AppearanceItemCaption.Options.UseFont = true;
            this.lbl身高.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl身高.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl身高.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl身高.Control = this.txt一般情况_身高;
            this.lbl身高.CustomizationFormText = "身高";
            this.lbl身高.Location = new System.Drawing.Point(0, 72);
            this.lbl身高.Name = "lbl身高";
            this.lbl身高.Size = new System.Drawing.Size(222, 24);
            this.lbl身高.Text = "身高";
            this.lbl身高.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl身高.TextSize = new System.Drawing.Size(80, 20);
            this.lbl身高.TextToControlDistance = 5;
            // 
            // lbl腰围
            // 
            this.lbl腰围.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl腰围.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl腰围.AppearanceItemCaption.Options.UseFont = true;
            this.lbl腰围.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl腰围.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl腰围.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl腰围.Control = this.txt一般情况_腰围;
            this.lbl腰围.CustomizationFormText = "腰围";
            this.lbl腰围.Location = new System.Drawing.Point(0, 96);
            this.lbl腰围.Name = "lbl腰围";
            this.lbl腰围.Size = new System.Drawing.Size(222, 24);
            this.lbl腰围.Text = "腰围";
            this.lbl腰围.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl腰围.TextSize = new System.Drawing.Size(80, 20);
            this.lbl腰围.TextToControlDistance = 5;
            // 
            // lbl体重指数
            // 
            this.lbl体重指数.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl体重指数.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl体重指数.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体重指数.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体重指数.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl体重指数.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl体重指数.Control = this.txt一般情况_体重指数;
            this.lbl体重指数.CustomizationFormText = "体重指数";
            this.lbl体重指数.Location = new System.Drawing.Point(222, 96);
            this.lbl体重指数.Name = "lbl体重指数";
            this.lbl体重指数.Size = new System.Drawing.Size(539, 24);
            this.lbl体重指数.Text = "体重指数(BMI)";
            this.lbl体重指数.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl体重指数.TextSize = new System.Drawing.Size(90, 20);
            this.lbl体重指数.TextToControlDistance = 5;
            // 
            // lbl体重
            // 
            this.lbl体重.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl体重.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl体重.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体重.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体重.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl体重.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl体重.Control = this.txt一般情况_体重;
            this.lbl体重.CustomizationFormText = "体重";
            this.lbl体重.Location = new System.Drawing.Point(222, 72);
            this.lbl体重.Name = "lbl体重";
            this.lbl体重.Size = new System.Drawing.Size(539, 24);
            this.lbl体重.Text = "体重";
            this.lbl体重.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl体重.TextSize = new System.Drawing.Size(90, 20);
            this.lbl体重.TextToControlDistance = 5;
            // 
            // simpleLabelItem4
            // 
            this.simpleLabelItem4.AllowHotTrack = false;
            this.simpleLabelItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem4.CustomizationFormText = "血压";
            this.simpleLabelItem4.Location = new System.Drawing.Point(222, 24);
            this.simpleLabelItem4.MaxSize = new System.Drawing.Size(97, 18);
            this.simpleLabelItem4.MinSize = new System.Drawing.Size(97, 18);
            this.simpleLabelItem4.Name = "simpleLabelItem4";
            this.simpleLabelItem4.Size = new System.Drawing.Size(97, 48);
            this.simpleLabelItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem4.Text = "血压";
            this.simpleLabelItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem4.TextSize = new System.Drawing.Size(90, 14);
            // 
            // lbl脉率
            // 
            this.lbl脉率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl脉率.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl脉率.AppearanceItemCaption.Options.UseFont = true;
            this.lbl脉率.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl脉率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl脉率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl脉率.Control = this.txt一般情况_脉率;
            this.lbl脉率.CustomizationFormText = "脉率";
            this.lbl脉率.Location = new System.Drawing.Point(222, 0);
            this.lbl脉率.Name = "lbl脉率";
            this.lbl脉率.Size = new System.Drawing.Size(539, 24);
            this.lbl脉率.Text = "脉率";
            this.lbl脉率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl脉率.TextSize = new System.Drawing.Size(90, 14);
            this.lbl脉率.TextToControlDistance = 5;
            // 
            // lbl血压左侧
            // 
            this.lbl血压左侧.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血压左侧.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl血压左侧.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血压左侧.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血压左侧.Control = this.txt一般情况_血压左侧;
            this.lbl血压左侧.CustomizationFormText = "左侧";
            this.lbl血压左侧.Location = new System.Drawing.Point(319, 24);
            this.lbl血压左侧.Name = "lbl血压左侧";
            this.lbl血压左侧.Size = new System.Drawing.Size(207, 24);
            this.lbl血压左侧.Text = "左侧";
            this.lbl血压左侧.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血压左侧.TextSize = new System.Drawing.Size(30, 14);
            this.lbl血压左侧.TextToControlDistance = 5;
            // 
            // lbl血压右侧
            // 
            this.lbl血压右侧.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血压右侧.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl血压右侧.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血压右侧.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血压右侧.Control = this.txt一般情况_血压右侧;
            this.lbl血压右侧.CustomizationFormText = "右侧";
            this.lbl血压右侧.Location = new System.Drawing.Point(319, 48);
            this.lbl血压右侧.Name = "lbl血压右侧";
            this.lbl血压右侧.Size = new System.Drawing.Size(207, 24);
            this.lbl血压右侧.Text = "右侧";
            this.lbl血压右侧.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血压右侧.TextSize = new System.Drawing.Size(30, 20);
            this.lbl血压右侧.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.txt一般情况_血压左侧原因;
            this.layoutControlItem16.CustomizationFormText = "左侧原因";
            this.layoutControlItem16.Location = new System.Drawing.Point(526, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem16.Text = "左侧原因";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.txt一般情况_血压右侧原因;
            this.layoutControlItem17.CustomizationFormText = "右侧原因";
            this.layoutControlItem17.Location = new System.Drawing.Point(526, 48);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem17.Text = "右侧原因";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // group老年人评估
            // 
            this.group老年人评估.CustomizationFormText = "group老年人评估";
            this.group老年人评估.GroupBordersVisible = false;
            this.group老年人评估.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem14,
            this.layoutControlItem18,
            this.layoutControlItem20,
            this.layoutControlItem128,
            this.emptySpaceItem23,
            this.emptySpaceItem25});
            this.group老年人评估.Location = new System.Drawing.Point(0, 120);
            this.group老年人评估.Name = "group老年人评估";
            this.group老年人评估.Size = new System.Drawing.Size(761, 170);
            this.group老年人评估.Text = "group老年人评估";
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem14.Control = this.radio老年人健康状态评估;
            this.layoutControlItem14.CustomizationFormText = "老年人健康状态自我评估*";
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(220, 40);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(444, 40);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "老年人健康状态自我评估*";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem18.Control = this.radio老年人生活自理能力评估;
            this.layoutControlItem18.CustomizationFormText = "老年人生活自理能力自我评估*";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(220, 50);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(444, 50);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "老年人生活自理能力自我评估*";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem20.Control = this.flow老年人认知能力;
            this.layoutControlItem20.CustomizationFormText = "老年人认知能力*";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 90);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(270, 40);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(761, 40);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "老年人认知能力*";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem128
            // 
            this.layoutControlItem128.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem128.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem128.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem128.Control = this.flow老年人情感状态;
            this.layoutControlItem128.CustomizationFormText = "老年人情感状态*";
            this.layoutControlItem128.Location = new System.Drawing.Point(0, 130);
            this.layoutControlItem128.MinSize = new System.Drawing.Size(270, 40);
            this.layoutControlItem128.Name = "layoutControlItem128";
            this.layoutControlItem128.Size = new System.Drawing.Size(761, 40);
            this.layoutControlItem128.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem128.Text = "老年人情感状态*";
            this.layoutControlItem128.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem128.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem128.TextToControlDistance = 5;
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.CustomizationFormText = "emptySpaceItem23";
            this.emptySpaceItem23.Location = new System.Drawing.Point(444, 0);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(317, 40);
            this.emptySpaceItem23.Text = "emptySpaceItem23";
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem25
            // 
            this.emptySpaceItem25.AllowHotTrack = false;
            this.emptySpaceItem25.CustomizationFormText = "emptySpaceItem25";
            this.emptySpaceItem25.Location = new System.Drawing.Point(444, 40);
            this.emptySpaceItem25.Name = "emptySpaceItem25";
            this.emptySpaceItem25.Size = new System.Drawing.Size(317, 50);
            this.emptySpaceItem25.Text = "emptySpaceItem25";
            this.emptySpaceItem25.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.group症状});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 98);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(777, 106);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // group症状
            // 
            this.group症状.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.group症状.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.group症状.AppearanceGroup.Options.UseFont = true;
            this.group症状.AppearanceGroup.Options.UseForeColor = true;
            this.group症状.AppearanceGroup.Options.UseTextOptions = true;
            this.group症状.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.group症状.CustomizationFormText = "症状";
            this.group症状.ExpandButtonVisible = true;
            this.group症状.ExpandOnDoubleClick = true;
            this.group症状.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10});
            this.group症状.Location = new System.Drawing.Point(0, 0);
            this.group症状.Name = "group症状";
            this.group症状.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.group症状.Size = new System.Drawing.Size(777, 106);
            this.group症状.Text = "症状";
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.flow症状;
            this.layoutControlItem10.CustomizationFormText = "症状";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(169, 70);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(761, 70);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "症状";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup5.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup5.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup5.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup5.CustomizationFormText = "创建人信息";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem122,
            this.layoutControlItem127,
            this.layoutControlItem123,
            this.layoutControlItem126,
            this.layoutControlItem125,
            this.layoutControlItem124});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 3998);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup5.Size = new System.Drawing.Size(777, 78);
            this.layoutControlGroup5.Text = "创建人信息";
            // 
            // layoutControlItem122
            // 
            this.layoutControlItem122.Control = this.dte创建时间;
            this.layoutControlItem122.CustomizationFormText = "创建时间: ";
            this.layoutControlItem122.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem122.Name = "layoutControlItem122";
            this.layoutControlItem122.Size = new System.Drawing.Size(237, 24);
            this.layoutControlItem122.Text = "创建时间: ";
            this.layoutControlItem122.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem127
            // 
            this.layoutControlItem127.Control = this.txt最近修改人;
            this.layoutControlItem127.CustomizationFormText = "最近修改人: ";
            this.layoutControlItem127.Location = new System.Drawing.Point(470, 24);
            this.layoutControlItem127.Name = "layoutControlItem127";
            this.layoutControlItem127.Size = new System.Drawing.Size(297, 24);
            this.layoutControlItem127.Text = "最近修改人: ";
            this.layoutControlItem127.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem123
            // 
            this.layoutControlItem123.Control = this.dte最近更新时间;
            this.layoutControlItem123.CustomizationFormText = "最近更新时间:";
            this.layoutControlItem123.Location = new System.Drawing.Point(237, 24);
            this.layoutControlItem123.Name = "layoutControlItem123";
            this.layoutControlItem123.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem123.Text = "最近更新时间:";
            this.layoutControlItem123.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem126
            // 
            this.layoutControlItem126.Control = this.txt创建人;
            this.layoutControlItem126.CustomizationFormText = "创建人:";
            this.layoutControlItem126.Location = new System.Drawing.Point(237, 0);
            this.layoutControlItem126.Name = "layoutControlItem126";
            this.layoutControlItem126.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem126.Text = "创建人:";
            this.layoutControlItem126.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem125
            // 
            this.layoutControlItem125.Control = this.txt创建机构;
            this.layoutControlItem125.CustomizationFormText = "创建机构: ";
            this.layoutControlItem125.Location = new System.Drawing.Point(470, 0);
            this.layoutControlItem125.Name = "layoutControlItem125";
            this.layoutControlItem125.Size = new System.Drawing.Size(297, 24);
            this.layoutControlItem125.Text = "创建机构: ";
            this.layoutControlItem125.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem124
            // 
            this.layoutControlItem124.Control = this.txt当前所属机构;
            this.layoutControlItem124.CustomizationFormText = "当前所属机构:";
            this.layoutControlItem124.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem124.Name = "layoutControlItem124";
            this.layoutControlItem124.Size = new System.Drawing.Size(237, 24);
            this.layoutControlItem124.Text = "当前所属机构:";
            this.layoutControlItem124.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup7.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlGroup7.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup7.AppearanceGroup.Options.UseForeColor = true;
            this.layoutControlGroup7.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup7.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup7.CustomizationFormText = "生活方式";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.ExpandOnDoubleClick = true;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl饮食习惯,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33,
            this.layoutControlItem35,
            this.layoutControlItem37,
            this.layoutControlItem36,
            this.layoutControlItem34,
            this.layoutControlGroup19,
            this.layoutControlGroup21,
            this.layoutControlGroup22,
            this.layoutControlItem23,
            this.layoutControlItem25,
            this.layoutControlItem24,
            this.layoutControlItem22,
            this.emptySpaceItem7,
            this.emptySpaceItem12,
            this.emptySpaceItem14,
            this.layoutControlGroup20,
            this.emptySpaceItem26});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 530);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup7.Size = new System.Drawing.Size(777, 501);
            this.layoutControlGroup7.Text = "生活方式";
            // 
            // lbl饮食习惯
            // 
            this.lbl饮食习惯.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl饮食习惯.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl饮食习惯.AppearanceItemCaption.Options.UseFont = true;
            this.lbl饮食习惯.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl饮食习惯.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl饮食习惯.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl饮食习惯.Control = this.flow饮食习惯;
            this.lbl饮食习惯.CustomizationFormText = "饮食习惯";
            this.lbl饮食习惯.Location = new System.Drawing.Point(0, 77);
            this.lbl饮食习惯.Name = "lbl饮食习惯";
            this.lbl饮食习惯.Size = new System.Drawing.Size(761, 28);
            this.lbl饮食习惯.Text = "饮食习惯";
            this.lbl饮食习惯.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl饮食习惯.TextSize = new System.Drawing.Size(80, 24);
            this.lbl饮食习惯.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.radio吸烟情况;
            this.layoutControlItem27.CustomizationFormText = "吸烟情况";
            this.layoutControlItem27.Location = new System.Drawing.Point(82, 105);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(320, 29);
            this.layoutControlItem27.Text = "吸烟情况";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(100, 10);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.txt日吸烟量;
            this.layoutControlItem28.CustomizationFormText = "日吸烟量";
            this.layoutControlItem28.Location = new System.Drawing.Point(82, 134);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(679, 24);
            this.layoutControlItem28.Text = "日吸烟量";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.txt开始吸烟年龄;
            this.layoutControlItem29.CustomizationFormText = "开始吸烟年龄";
            this.layoutControlItem29.Location = new System.Drawing.Point(82, 158);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(274, 24);
            this.layoutControlItem29.Text = "开始吸烟年龄";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.txt戒烟年龄;
            this.layoutControlItem30.CustomizationFormText = "戒烟年龄";
            this.layoutControlItem30.Location = new System.Drawing.Point(356, 158);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(405, 24);
            this.layoutControlItem30.Text = "戒烟年龄";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(120, 14);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.radio饮酒频率;
            this.layoutControlItem31.CustomizationFormText = "饮酒频率";
            this.layoutControlItem31.Location = new System.Drawing.Point(82, 182);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(314, 29);
            this.layoutControlItem31.Text = "饮酒频率";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.txt日饮酒量;
            this.layoutControlItem32.CustomizationFormText = "日饮酒量";
            this.layoutControlItem32.Location = new System.Drawing.Point(82, 211);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(679, 24);
            this.layoutControlItem32.Text = "日饮酒量";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.radio是否戒酒;
            this.layoutControlItem33.CustomizationFormText = "是否戒酒";
            this.layoutControlItem33.Location = new System.Drawing.Point(82, 235);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(275, 29);
            this.layoutControlItem33.Text = "是否戒酒";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.BorderColor = System.Drawing.Color.Black;
            this.layoutControlItem35.AppearanceItemCaption.Options.UseBorderColor = true;
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.Control = this.txt开始饮酒年龄;
            this.layoutControlItem35.CustomizationFormText = "开始饮酒年龄";
            this.layoutControlItem35.Location = new System.Drawing.Point(82, 264);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(275, 29);
            this.layoutControlItem35.Text = "开始饮酒年龄";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.flow饮酒种类;
            this.layoutControlItem37.CustomizationFormText = "饮酒种类";
            this.layoutControlItem37.Location = new System.Drawing.Point(82, 293);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(679, 28);
            this.layoutControlItem37.Text = "饮酒种类";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.Control = this.radio近一年内是否曾醉酒;
            this.layoutControlItem36.CustomizationFormText = "近一年内是否曾醉酒";
            this.layoutControlItem36.Location = new System.Drawing.Point(357, 264);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(214, 29);
            this.layoutControlItem36.Text = "近一年内是否曾醉酒";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(120, 14);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.txt戒酒年龄;
            this.layoutControlItem34.CustomizationFormText = "戒酒年龄";
            this.layoutControlItem34.Location = new System.Drawing.Point(357, 235);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(404, 29);
            this.layoutControlItem34.Text = "戒酒年龄";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(120, 14);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlGroup19
            // 
            this.layoutControlGroup19.CustomizationFormText = "layoutControlGroup19";
            this.layoutControlGroup19.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl锻炼频率});
            this.layoutControlGroup19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup19.Name = "layoutControlGroup19";
            this.layoutControlGroup19.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup19.Size = new System.Drawing.Size(82, 77);
            this.layoutControlGroup19.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup19.Text = "layoutControlGroup19";
            this.layoutControlGroup19.TextVisible = false;
            // 
            // lbl锻炼频率
            // 
            this.lbl锻炼频率.AllowHotTrack = false;
            this.lbl锻炼频率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl锻炼频率.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl锻炼频率.AppearanceItemCaption.Options.UseFont = true;
            this.lbl锻炼频率.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl锻炼频率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl锻炼频率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl锻炼频率.CustomizationFormText = "体育锻炼";
            this.lbl锻炼频率.Location = new System.Drawing.Point(0, 0);
            this.lbl锻炼频率.MaxSize = new System.Drawing.Size(80, 0);
            this.lbl锻炼频率.MinSize = new System.Drawing.Size(80, 10);
            this.lbl锻炼频率.Name = "lbl锻炼频率";
            this.lbl锻炼频率.Size = new System.Drawing.Size(80, 75);
            this.lbl锻炼频率.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl锻炼频率.Text = "体育锻炼";
            this.lbl锻炼频率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl锻炼频率.TextSize = new System.Drawing.Size(50, 20);
            this.lbl锻炼频率.TextVisible = true;
            // 
            // layoutControlGroup21
            // 
            this.layoutControlGroup21.CustomizationFormText = "layoutControlGroup21";
            this.layoutControlGroup21.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl吸烟状况});
            this.layoutControlGroup21.Location = new System.Drawing.Point(0, 105);
            this.layoutControlGroup21.Name = "layoutControlGroup21";
            this.layoutControlGroup21.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup21.Size = new System.Drawing.Size(82, 77);
            this.layoutControlGroup21.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup21.Text = "layoutControlGroup21";
            this.layoutControlGroup21.TextVisible = false;
            // 
            // lbl吸烟状况
            // 
            this.lbl吸烟状况.AllowHotTrack = false;
            this.lbl吸烟状况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl吸烟状况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl吸烟状况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl吸烟状况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl吸烟状况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl吸烟状况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl吸烟状况.CustomizationFormText = "吸烟情况";
            this.lbl吸烟状况.Location = new System.Drawing.Point(0, 0);
            this.lbl吸烟状况.MaxSize = new System.Drawing.Size(80, 0);
            this.lbl吸烟状况.MinSize = new System.Drawing.Size(80, 10);
            this.lbl吸烟状况.Name = "lbl吸烟状况";
            this.lbl吸烟状况.Size = new System.Drawing.Size(80, 75);
            this.lbl吸烟状况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl吸烟状况.Text = "吸烟情况";
            this.lbl吸烟状况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl吸烟状况.TextSize = new System.Drawing.Size(60, 20);
            this.lbl吸烟状况.TextVisible = true;
            // 
            // layoutControlGroup22
            // 
            this.layoutControlGroup22.CustomizationFormText = "layoutControlGroup22";
            this.layoutControlGroup22.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl饮酒情况});
            this.layoutControlGroup22.Location = new System.Drawing.Point(0, 182);
            this.layoutControlGroup22.Name = "layoutControlGroup22";
            this.layoutControlGroup22.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup22.Size = new System.Drawing.Size(82, 139);
            this.layoutControlGroup22.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup22.Text = "layoutControlGroup22";
            this.layoutControlGroup22.TextVisible = false;
            // 
            // lbl饮酒情况
            // 
            this.lbl饮酒情况.AllowHotTrack = false;
            this.lbl饮酒情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl饮酒情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl饮酒情况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl饮酒情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl饮酒情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl饮酒情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl饮酒情况.CustomizationFormText = "饮酒情况";
            this.lbl饮酒情况.Location = new System.Drawing.Point(0, 0);
            this.lbl饮酒情况.MaxSize = new System.Drawing.Size(80, 0);
            this.lbl饮酒情况.MinSize = new System.Drawing.Size(80, 10);
            this.lbl饮酒情况.Name = "lbl饮酒情况";
            this.lbl饮酒情况.Size = new System.Drawing.Size(80, 137);
            this.lbl饮酒情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl饮酒情况.Text = "饮酒情况";
            this.lbl饮酒情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl饮酒情况.TextSize = new System.Drawing.Size(60, 20);
            this.lbl饮酒情况.TextVisible = true;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt生活方式_每次锻炼时间;
            this.layoutControlItem23.CustomizationFormText = "每次锻炼时间";
            this.layoutControlItem23.Location = new System.Drawing.Point(82, 29);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(276, 24);
            this.layoutControlItem23.Text = "每次锻炼时间";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.txt生活方式_锻炼方式;
            this.layoutControlItem25.CustomizationFormText = "锻炼方式";
            this.layoutControlItem25.Location = new System.Drawing.Point(82, 53);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(679, 24);
            this.layoutControlItem25.Text = "锻炼方式";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txt生活方式_坚持锻炼时间;
            this.layoutControlItem24.CustomizationFormText = "坚持锻炼时间";
            this.layoutControlItem24.Location = new System.Drawing.Point(358, 29);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(403, 24);
            this.layoutControlItem24.Text = "坚持锻炼时间";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.radio生活方式_锻炼频率;
            this.layoutControlItem22.CustomizationFormText = "锻炼频率";
            this.layoutControlItem22.Location = new System.Drawing.Point(82, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(489, 29);
            this.layoutControlItem22.Text = "锻炼频率";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(571, 0);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(190, 29);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(402, 105);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(359, 29);
            this.emptySpaceItem12.Text = "emptySpaceItem12";
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.emptySpaceItem14.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem14.CustomizationFormText = "emptySpaceItem14";
            this.emptySpaceItem14.Location = new System.Drawing.Point(396, 182);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(365, 29);
            this.emptySpaceItem14.Text = "emptySpaceItem14";
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup20
            // 
            this.layoutControlGroup20.CustomizationFormText = "layoutControlGroup20";
            this.layoutControlGroup20.GroupBordersVisible = false;
            this.layoutControlGroup20.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl放射物质,
            this.lbl物理因素,
            this.layoutControlItem135,
            this.lbl职业病其他,
            this.layoutControlItem138,
            this.layoutControlItem136,
            this.layoutControlItem131,
            this.lbl防护措施,
            this.layoutControlItem139,
            this.layoutControlItem137,
            this.layoutControlItem133,
            this.layoutControlItem134,
            this.layoutControlItem129,
            this.lbl粉尘,
            this.layoutControlItem130,
            this.layoutControlItem132,
            this.layoutControlGroup23});
            this.layoutControlGroup20.Location = new System.Drawing.Point(0, 321);
            this.layoutControlGroup20.Name = "layoutControlGroup20";
            this.layoutControlGroup20.Size = new System.Drawing.Size(761, 144);
            this.layoutControlGroup20.Text = "layoutControlGroup20";
            // 
            // lbl放射物质
            // 
            this.lbl放射物质.Control = this.txt放射物质;
            this.lbl放射物质.CustomizationFormText = "放射物质";
            this.lbl放射物质.Location = new System.Drawing.Point(82, 48);
            this.lbl放射物质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl放射物质.MinSize = new System.Drawing.Size(177, 24);
            this.lbl放射物质.Name = "lbl放射物质";
            this.lbl放射物质.Size = new System.Drawing.Size(179, 24);
            this.lbl放射物质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl放射物质.Text = "放射物质";
            this.lbl放射物质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl放射物质.TextSize = new System.Drawing.Size(50, 20);
            this.lbl放射物质.TextToControlDistance = 5;
            // 
            // lbl物理因素
            // 
            this.lbl物理因素.Control = this.txt物理因素;
            this.lbl物理因素.CustomizationFormText = "物理因素";
            this.lbl物理因素.Location = new System.Drawing.Point(82, 72);
            this.lbl物理因素.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl物理因素.MinSize = new System.Drawing.Size(177, 24);
            this.lbl物理因素.Name = "lbl物理因素";
            this.lbl物理因素.Size = new System.Drawing.Size(179, 24);
            this.lbl物理因素.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl物理因素.Text = "物理因素";
            this.lbl物理因素.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl物理因素.TextSize = new System.Drawing.Size(50, 20);
            this.lbl物理因素.TextToControlDistance = 5;
            // 
            // layoutControlItem135
            // 
            this.layoutControlItem135.Control = this.txt化学因素;
            this.layoutControlItem135.CustomizationFormText = "化学因素";
            this.layoutControlItem135.Location = new System.Drawing.Point(82, 96);
            this.layoutControlItem135.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem135.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem135.Name = "layoutControlItem135";
            this.layoutControlItem135.Size = new System.Drawing.Size(179, 24);
            this.layoutControlItem135.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem135.Text = "化学因素";
            this.layoutControlItem135.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem135.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem135.TextToControlDistance = 5;
            // 
            // lbl职业病其他
            // 
            this.lbl职业病其他.Control = this.txt职业病其他;
            this.lbl职业病其他.CustomizationFormText = "其他";
            this.lbl职业病其他.Location = new System.Drawing.Point(82, 120);
            this.lbl职业病其他.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl职业病其他.MinSize = new System.Drawing.Size(177, 24);
            this.lbl职业病其他.Name = "lbl职业病其他";
            this.lbl职业病其他.Size = new System.Drawing.Size(179, 24);
            this.lbl职业病其他.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl职业病其他.Text = "其他";
            this.lbl职业病其他.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl职业病其他.TextSize = new System.Drawing.Size(50, 20);
            this.lbl职业病其他.TextToControlDistance = 5;
            // 
            // layoutControlItem138
            // 
            this.layoutControlItem138.Control = this.radio职业病其他防护措施有无;
            this.layoutControlItem138.CustomizationFormText = "防护措施";
            this.layoutControlItem138.Location = new System.Drawing.Point(261, 120);
            this.layoutControlItem138.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem138.Name = "layoutControlItem138";
            this.layoutControlItem138.Size = new System.Drawing.Size(179, 24);
            this.layoutControlItem138.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem138.Text = "防护措施";
            this.layoutControlItem138.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem138.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem138.TextToControlDistance = 5;
            // 
            // layoutControlItem136
            // 
            this.layoutControlItem136.Control = this.radio化学防护措施有无;
            this.layoutControlItem136.CustomizationFormText = "防护措施";
            this.layoutControlItem136.Location = new System.Drawing.Point(261, 96);
            this.layoutControlItem136.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem136.Name = "layoutControlItem136";
            this.layoutControlItem136.Size = new System.Drawing.Size(179, 24);
            this.layoutControlItem136.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem136.Text = "防护措施";
            this.layoutControlItem136.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem136.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem136.TextToControlDistance = 5;
            // 
            // layoutControlItem131
            // 
            this.layoutControlItem131.Control = this.radio物理防护措施有无;
            this.layoutControlItem131.CustomizationFormText = "防护措施";
            this.layoutControlItem131.Location = new System.Drawing.Point(261, 72);
            this.layoutControlItem131.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem131.Name = "layoutControlItem131";
            this.layoutControlItem131.Size = new System.Drawing.Size(179, 24);
            this.layoutControlItem131.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem131.Text = "防护措施";
            this.layoutControlItem131.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem131.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem131.TextToControlDistance = 5;
            // 
            // lbl防护措施
            // 
            this.lbl防护措施.Control = this.radio放射物质防护措施有无;
            this.lbl防护措施.CustomizationFormText = "防护措施";
            this.lbl防护措施.Location = new System.Drawing.Point(261, 48);
            this.lbl防护措施.MinSize = new System.Drawing.Size(177, 24);
            this.lbl防护措施.Name = "lbl防护措施";
            this.lbl防护措施.Size = new System.Drawing.Size(179, 24);
            this.lbl防护措施.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl防护措施.Text = "防护措施";
            this.lbl防护措施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl防护措施.TextSize = new System.Drawing.Size(50, 20);
            this.lbl防护措施.TextToControlDistance = 5;
            // 
            // layoutControlItem139
            // 
            this.layoutControlItem139.Control = this.txt职业病其他防护;
            this.layoutControlItem139.CustomizationFormText = "layoutControlItem139";
            this.layoutControlItem139.Location = new System.Drawing.Point(440, 120);
            this.layoutControlItem139.Name = "layoutControlItem139";
            this.layoutControlItem139.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem139.Text = "layoutControlItem139";
            this.layoutControlItem139.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem139.TextToControlDistance = 0;
            this.layoutControlItem139.TextVisible = false;
            // 
            // layoutControlItem137
            // 
            this.layoutControlItem137.Control = this.txt化学防护措施;
            this.layoutControlItem137.CustomizationFormText = "layoutControlItem137";
            this.layoutControlItem137.Location = new System.Drawing.Point(440, 96);
            this.layoutControlItem137.Name = "layoutControlItem137";
            this.layoutControlItem137.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem137.Text = "layoutControlItem137";
            this.layoutControlItem137.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem137.TextToControlDistance = 0;
            this.layoutControlItem137.TextVisible = false;
            // 
            // layoutControlItem133
            // 
            this.layoutControlItem133.Control = this.txt物理防护措施;
            this.layoutControlItem133.CustomizationFormText = "layoutControlItem133";
            this.layoutControlItem133.Location = new System.Drawing.Point(440, 72);
            this.layoutControlItem133.Name = "layoutControlItem133";
            this.layoutControlItem133.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem133.Text = "layoutControlItem133";
            this.layoutControlItem133.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem133.TextToControlDistance = 0;
            this.layoutControlItem133.TextVisible = false;
            // 
            // layoutControlItem134
            // 
            this.layoutControlItem134.Control = this.txt放射物质防护措施;
            this.layoutControlItem134.CustomizationFormText = "layoutControlItem134";
            this.layoutControlItem134.Location = new System.Drawing.Point(440, 48);
            this.layoutControlItem134.Name = "layoutControlItem134";
            this.layoutControlItem134.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem134.Text = "layoutControlItem134";
            this.layoutControlItem134.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem134.TextToControlDistance = 0;
            this.layoutControlItem134.TextVisible = false;
            // 
            // layoutControlItem129
            // 
            this.layoutControlItem129.Control = this.flowLayoutPanel1;
            this.layoutControlItem129.CustomizationFormText = "layoutControlItem129";
            this.layoutControlItem129.Location = new System.Drawing.Point(82, 0);
            this.layoutControlItem129.Name = "layoutControlItem129";
            this.layoutControlItem129.Size = new System.Drawing.Size(679, 24);
            this.layoutControlItem129.Text = "layoutControlItem129";
            this.layoutControlItem129.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem129.TextToControlDistance = 0;
            this.layoutControlItem129.TextVisible = false;
            // 
            // lbl粉尘
            // 
            this.lbl粉尘.Control = this.txt粉尘;
            this.lbl粉尘.CustomizationFormText = "粉尘";
            this.lbl粉尘.Location = new System.Drawing.Point(82, 24);
            this.lbl粉尘.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl粉尘.MinSize = new System.Drawing.Size(177, 24);
            this.lbl粉尘.Name = "lbl粉尘";
            this.lbl粉尘.Size = new System.Drawing.Size(179, 24);
            this.lbl粉尘.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl粉尘.Text = "粉尘";
            this.lbl粉尘.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl粉尘.TextSize = new System.Drawing.Size(50, 20);
            this.lbl粉尘.TextToControlDistance = 5;
            // 
            // layoutControlItem130
            // 
            this.layoutControlItem130.Control = this.radio粉尘防护措施有无;
            this.layoutControlItem130.CustomizationFormText = "防护措施";
            this.layoutControlItem130.Location = new System.Drawing.Point(261, 24);
            this.layoutControlItem130.MinSize = new System.Drawing.Size(177, 24);
            this.layoutControlItem130.Name = "layoutControlItem130";
            this.layoutControlItem130.Size = new System.Drawing.Size(179, 24);
            this.layoutControlItem130.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem130.Text = "防护措施";
            this.layoutControlItem130.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem130.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem130.TextToControlDistance = 5;
            // 
            // layoutControlItem132
            // 
            this.layoutControlItem132.Control = this.txt粉尘防护措施;
            this.layoutControlItem132.CustomizationFormText = "layoutControlItem132";
            this.layoutControlItem132.Location = new System.Drawing.Point(440, 24);
            this.layoutControlItem132.Name = "layoutControlItem132";
            this.layoutControlItem132.Size = new System.Drawing.Size(321, 24);
            this.layoutControlItem132.Text = "layoutControlItem132";
            this.layoutControlItem132.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem132.TextToControlDistance = 0;
            this.layoutControlItem132.TextVisible = false;
            // 
            // layoutControlGroup23
            // 
            this.layoutControlGroup23.CustomizationFormText = "layoutControlGroup23";
            this.layoutControlGroup23.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl职业病危害因素接触史});
            this.layoutControlGroup23.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup23.Name = "layoutControlGroup23";
            this.layoutControlGroup23.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup23.Size = new System.Drawing.Size(82, 144);
            this.layoutControlGroup23.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup23.Text = "layoutControlGroup23";
            this.layoutControlGroup23.TextVisible = false;
            // 
            // lbl职业病危害因素接触史
            // 
            this.lbl职业病危害因素接触史.AllowHotTrack = false;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl职业病危害因素接触史.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.Options.UseFont = true;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl职业病危害因素接触史.CustomizationFormText = "职业病危害因素接触史";
            this.lbl职业病危害因素接触史.Location = new System.Drawing.Point(0, 0);
            this.lbl职业病危害因素接触史.MaxSize = new System.Drawing.Size(80, 18);
            this.lbl职业病危害因素接触史.MinSize = new System.Drawing.Size(80, 18);
            this.lbl职业病危害因素接触史.Name = "lbl职业病危害因素接触史";
            this.lbl职业病危害因素接触史.Size = new System.Drawing.Size(80, 142);
            this.lbl职业病危害因素接触史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl职业病危害因素接触史.Text = "职业病危害因素接触史";
            this.lbl职业病危害因素接触史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl职业病危害因素接触史.TextSize = new System.Drawing.Size(50, 20);
            // 
            // emptySpaceItem26
            // 
            this.emptySpaceItem26.AllowHotTrack = false;
            this.emptySpaceItem26.CustomizationFormText = "emptySpaceItem26";
            this.emptySpaceItem26.Location = new System.Drawing.Point(571, 264);
            this.emptySpaceItem26.Name = "emptySpaceItem26";
            this.emptySpaceItem26.Size = new System.Drawing.Size(190, 29);
            this.emptySpaceItem26.Text = "emptySpaceItem26";
            this.emptySpaceItem26.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup8.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlGroup8.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup8.AppearanceGroup.Options.UseForeColor = true;
            this.layoutControlGroup8.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup8.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup8.CustomizationFormText = "脏器功能";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.ExpandOnDoubleClick = true;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl视力,
            this.lbl听力,
            this.lbl运动能力,
            this.layoutControlItem39,
            this.layoutControlItem38,
            this.layoutControlItem143,
            this.layoutControlItem140,
            this.layoutControlItem144,
            this.layoutControlItem146,
            this.layoutControlItem145,
            this.layoutControlItem147,
            this.layoutControlItem141,
            this.layoutControlItem148,
            this.layoutControlItem149,
            this.layoutControlItem150,
            this.layoutControlItem151,
            this.layoutControlItem152,
            this.layoutControlItem155,
            this.layoutControlItem142,
            this.layoutControlItem158,
            this.emptySpaceItem24,
            this.layoutControlGroup24,
            this.layoutControlGroup25,
            this.lbl咽部,
            this.layoutControlItem153,
            this.layoutControlItem154});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 1031);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup8.Size = new System.Drawing.Size(777, 277);
            this.layoutControlGroup8.Text = "脏器功能";
            // 
            // lbl视力
            // 
            this.lbl视力.AppearanceItemCaption.BorderColor = System.Drawing.Color.White;
            this.lbl视力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl视力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl视力.AppearanceItemCaption.Options.UseBorderColor = true;
            this.lbl视力.AppearanceItemCaption.Options.UseFont = true;
            this.lbl视力.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl视力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl视力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl视力.Control = this.flowLayoutPanel6;
            this.lbl视力.CustomizationFormText = "视力";
            this.lbl视力.Location = new System.Drawing.Point(0, 169);
            this.lbl视力.Name = "lbl视力";
            this.lbl视力.Size = new System.Drawing.Size(761, 24);
            this.lbl视力.Text = "视力";
            this.lbl视力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl视力.TextSize = new System.Drawing.Size(70, 20);
            this.lbl视力.TextToControlDistance = 5;
            // 
            // lbl听力
            // 
            this.lbl听力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl听力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl听力.AppearanceItemCaption.Options.UseFont = true;
            this.lbl听力.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl听力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl听力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl听力.Control = this.flow听力;
            this.lbl听力.CustomizationFormText = "听力";
            this.lbl听力.Location = new System.Drawing.Point(0, 193);
            this.lbl听力.Name = "lbl听力";
            this.lbl听力.Size = new System.Drawing.Size(761, 24);
            this.lbl听力.Text = "听力";
            this.lbl听力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl听力.TextSize = new System.Drawing.Size(70, 20);
            this.lbl听力.TextToControlDistance = 5;
            // 
            // lbl运动能力
            // 
            this.lbl运动能力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl运动能力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl运动能力.AppearanceItemCaption.Options.UseFont = true;
            this.lbl运动能力.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl运动能力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl运动能力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl运动能力.Control = this.flow运动功能;
            this.lbl运动能力.CustomizationFormText = "运动能力";
            this.lbl运动能力.Location = new System.Drawing.Point(0, 217);
            this.lbl运动能力.Name = "lbl运动能力";
            this.lbl运动能力.Size = new System.Drawing.Size(761, 24);
            this.lbl运动能力.Text = "运动能力";
            this.lbl运动能力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl运动能力.TextSize = new System.Drawing.Size(70, 20);
            this.lbl运动能力.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem39.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.flow口唇;
            this.layoutControlItem39.CustomizationFormText = "口唇";
            this.layoutControlItem39.Location = new System.Drawing.Point(77, 0);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(215, 26);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(684, 26);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "口唇";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(50, 14);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.chk齿列_正常;
            this.layoutControlItem38.CustomizationFormText = "layoutControlItem38";
            this.layoutControlItem38.Location = new System.Drawing.Point(134, 26);
            this.layoutControlItem38.MaxSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(60, 23);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "layoutControlItem38";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem38.TextToControlDistance = 0;
            this.layoutControlItem38.TextVisible = false;
            // 
            // layoutControlItem143
            // 
            this.layoutControlItem143.Control = this.chk齿列_义齿;
            this.layoutControlItem143.CustomizationFormText = "layoutControlItem143";
            this.layoutControlItem143.Location = new System.Drawing.Point(134, 97);
            this.layoutControlItem143.MaxSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem143.MinSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem143.Name = "layoutControlItem143";
            this.layoutControlItem143.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem143.Size = new System.Drawing.Size(60, 48);
            this.layoutControlItem143.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem143.Text = "layoutControlItem143";
            this.layoutControlItem143.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem143.TextToControlDistance = 0;
            this.layoutControlItem143.TextVisible = false;
            // 
            // layoutControlItem140
            // 
            this.layoutControlItem140.Control = this.chk齿列_缺齿;
            this.layoutControlItem140.CustomizationFormText = "layoutControlItem140";
            this.layoutControlItem140.Location = new System.Drawing.Point(134, 49);
            this.layoutControlItem140.MaxSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem140.MinSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem140.Name = "layoutControlItem140";
            this.layoutControlItem140.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem140.Size = new System.Drawing.Size(60, 48);
            this.layoutControlItem140.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem140.Text = "layoutControlItem140";
            this.layoutControlItem140.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem140.TextToControlDistance = 0;
            this.layoutControlItem140.TextVisible = false;
            // 
            // layoutControlItem144
            // 
            this.layoutControlItem144.Control = this.txt缺齿1;
            this.layoutControlItem144.CustomizationFormText = "layoutControlItem144";
            this.layoutControlItem144.Location = new System.Drawing.Point(194, 49);
            this.layoutControlItem144.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem144.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem144.Name = "layoutControlItem144";
            this.layoutControlItem144.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem144.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem144.Text = "layoutControlItem144";
            this.layoutControlItem144.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem144.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem144.TextToControlDistance = 0;
            this.layoutControlItem144.TextVisible = false;
            // 
            // layoutControlItem146
            // 
            this.layoutControlItem146.Control = this.txt缺齿2;
            this.layoutControlItem146.CustomizationFormText = "layoutControlItem146";
            this.layoutControlItem146.Location = new System.Drawing.Point(294, 49);
            this.layoutControlItem146.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem146.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem146.Name = "layoutControlItem146";
            this.layoutControlItem146.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem146.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem146.Text = "layoutControlItem146";
            this.layoutControlItem146.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem146.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem146.TextToControlDistance = 0;
            this.layoutControlItem146.TextVisible = false;
            // 
            // layoutControlItem145
            // 
            this.layoutControlItem145.Control = this.txt缺齿3;
            this.layoutControlItem145.CustomizationFormText = "layoutControlItem145";
            this.layoutControlItem145.Location = new System.Drawing.Point(194, 73);
            this.layoutControlItem145.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem145.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem145.Name = "layoutControlItem145";
            this.layoutControlItem145.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem145.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem145.Text = "layoutControlItem145";
            this.layoutControlItem145.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem145.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem145.TextToControlDistance = 0;
            this.layoutControlItem145.TextVisible = false;
            // 
            // layoutControlItem147
            // 
            this.layoutControlItem147.Control = this.txt缺齿4;
            this.layoutControlItem147.CustomizationFormText = "layoutControlItem147";
            this.layoutControlItem147.Location = new System.Drawing.Point(294, 73);
            this.layoutControlItem147.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem147.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem147.Name = "layoutControlItem147";
            this.layoutControlItem147.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem147.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem147.Text = "layoutControlItem147";
            this.layoutControlItem147.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem147.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem147.TextToControlDistance = 0;
            this.layoutControlItem147.TextVisible = false;
            // 
            // layoutControlItem141
            // 
            this.layoutControlItem141.Control = this.chk齿列_龋齿;
            this.layoutControlItem141.CustomizationFormText = "layoutControlItem141";
            this.layoutControlItem141.Location = new System.Drawing.Point(394, 49);
            this.layoutControlItem141.MaxSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem141.MinSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem141.Name = "layoutControlItem141";
            this.layoutControlItem141.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem141.Size = new System.Drawing.Size(60, 48);
            this.layoutControlItem141.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem141.Text = "layoutControlItem141";
            this.layoutControlItem141.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem141.TextToControlDistance = 0;
            this.layoutControlItem141.TextVisible = false;
            // 
            // layoutControlItem148
            // 
            this.layoutControlItem148.Control = this.txt龋齿1;
            this.layoutControlItem148.CustomizationFormText = "layoutControlItem148";
            this.layoutControlItem148.Location = new System.Drawing.Point(454, 49);
            this.layoutControlItem148.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem148.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem148.Name = "layoutControlItem148";
            this.layoutControlItem148.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem148.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem148.Text = "layoutControlItem148";
            this.layoutControlItem148.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem148.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem148.TextToControlDistance = 0;
            this.layoutControlItem148.TextVisible = false;
            // 
            // layoutControlItem149
            // 
            this.layoutControlItem149.Control = this.txt龋齿2;
            this.layoutControlItem149.CustomizationFormText = "layoutControlItem149";
            this.layoutControlItem149.Location = new System.Drawing.Point(554, 49);
            this.layoutControlItem149.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem149.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem149.Name = "layoutControlItem149";
            this.layoutControlItem149.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItem149.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem149.Text = "layoutControlItem149";
            this.layoutControlItem149.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem149.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem149.TextToControlDistance = 0;
            this.layoutControlItem149.TextVisible = false;
            // 
            // layoutControlItem150
            // 
            this.layoutControlItem150.Control = this.txt龋齿3;
            this.layoutControlItem150.CustomizationFormText = "layoutControlItem150";
            this.layoutControlItem150.Location = new System.Drawing.Point(454, 73);
            this.layoutControlItem150.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem150.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem150.Name = "layoutControlItem150";
            this.layoutControlItem150.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem150.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem150.Text = "layoutControlItem150";
            this.layoutControlItem150.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem150.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem150.TextToControlDistance = 0;
            this.layoutControlItem150.TextVisible = false;
            // 
            // layoutControlItem151
            // 
            this.layoutControlItem151.Control = this.txt龋齿4;
            this.layoutControlItem151.CustomizationFormText = "layoutControlItem151";
            this.layoutControlItem151.Location = new System.Drawing.Point(554, 73);
            this.layoutControlItem151.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem151.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem151.Name = "layoutControlItem151";
            this.layoutControlItem151.Size = new System.Drawing.Size(207, 24);
            this.layoutControlItem151.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem151.Text = "layoutControlItem151";
            this.layoutControlItem151.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem151.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem151.TextToControlDistance = 0;
            this.layoutControlItem151.TextVisible = false;
            // 
            // layoutControlItem152
            // 
            this.layoutControlItem152.Control = this.txt义齿1;
            this.layoutControlItem152.CustomizationFormText = "layoutControlItem152";
            this.layoutControlItem152.Location = new System.Drawing.Point(194, 97);
            this.layoutControlItem152.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem152.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem152.Name = "layoutControlItem152";
            this.layoutControlItem152.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem152.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem152.Text = "layoutControlItem152";
            this.layoutControlItem152.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem152.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem152.TextToControlDistance = 0;
            this.layoutControlItem152.TextVisible = false;
            // 
            // layoutControlItem155
            // 
            this.layoutControlItem155.Control = this.txt义齿4;
            this.layoutControlItem155.CustomizationFormText = "layoutControlItem155";
            this.layoutControlItem155.Location = new System.Drawing.Point(294, 121);
            this.layoutControlItem155.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem155.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem155.Name = "layoutControlItem155";
            this.layoutControlItem155.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem155.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem155.Text = "layoutControlItem155";
            this.layoutControlItem155.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem155.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem155.TextToControlDistance = 0;
            this.layoutControlItem155.TextVisible = false;
            // 
            // layoutControlItem142
            // 
            this.layoutControlItem142.Control = this.chk齿列_其他;
            this.layoutControlItem142.CustomizationFormText = "layoutControlItem142";
            this.layoutControlItem142.Location = new System.Drawing.Point(394, 97);
            this.layoutControlItem142.MaxSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem142.MinSize = new System.Drawing.Size(60, 23);
            this.layoutControlItem142.Name = "layoutControlItem142";
            this.layoutControlItem142.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem142.Size = new System.Drawing.Size(60, 48);
            this.layoutControlItem142.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem142.Text = "layoutControlItem142";
            this.layoutControlItem142.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem142.TextToControlDistance = 0;
            this.layoutControlItem142.TextVisible = false;
            // 
            // layoutControlItem158
            // 
            this.layoutControlItem158.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.layoutControlItem158.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlItem158.Control = this.txt齿列_其他;
            this.layoutControlItem158.CustomizationFormText = "layoutControlItem158";
            this.layoutControlItem158.Location = new System.Drawing.Point(454, 97);
            this.layoutControlItem158.MaxSize = new System.Drawing.Size(0, 32);
            this.layoutControlItem158.MinSize = new System.Drawing.Size(100, 32);
            this.layoutControlItem158.Name = "layoutControlItem158";
            this.layoutControlItem158.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 10, 2);
            this.layoutControlItem158.Size = new System.Drawing.Size(307, 48);
            this.layoutControlItem158.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem158.Text = "layoutControlItem158";
            this.layoutControlItem158.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem158.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem158.TextToControlDistance = 0;
            this.layoutControlItem158.TextVisible = false;
            // 
            // emptySpaceItem24
            // 
            this.emptySpaceItem24.AllowHotTrack = false;
            this.emptySpaceItem24.CustomizationFormText = "emptySpaceItem24";
            this.emptySpaceItem24.Location = new System.Drawing.Point(194, 26);
            this.emptySpaceItem24.Name = "emptySpaceItem24";
            this.emptySpaceItem24.Size = new System.Drawing.Size(567, 23);
            this.emptySpaceItem24.Text = "emptySpaceItem24";
            this.emptySpaceItem24.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup24
            // 
            this.layoutControlGroup24.CustomizationFormText = "layoutControlGroup24";
            this.layoutControlGroup24.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6});
            this.layoutControlGroup24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup24.Name = "layoutControlGroup24";
            this.layoutControlGroup24.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup24.Size = new System.Drawing.Size(77, 169);
            this.layoutControlGroup24.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup24.Text = "layoutControlGroup24";
            this.layoutControlGroup24.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem6.CustomizationFormText = "口腔";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(75, 0);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(75, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(75, 167);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "口腔";
            this.emptySpaceItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(60, 20);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // layoutControlGroup25
            // 
            this.layoutControlGroup25.CustomizationFormText = "layoutControlGroup25";
            this.layoutControlGroup25.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl齿列总});
            this.layoutControlGroup25.Location = new System.Drawing.Point(77, 26);
            this.layoutControlGroup25.Name = "layoutControlGroup25";
            this.layoutControlGroup25.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup25.Size = new System.Drawing.Size(57, 119);
            this.layoutControlGroup25.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup25.Text = "layoutControlGroup25";
            this.layoutControlGroup25.TextVisible = false;
            // 
            // lbl齿列总
            // 
            this.lbl齿列总.AllowHotTrack = false;
            this.lbl齿列总.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl齿列总.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl齿列总.AppearanceItemCaption.Options.UseFont = true;
            this.lbl齿列总.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl齿列总.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl齿列总.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl齿列总.CustomizationFormText = "齿列";
            this.lbl齿列总.Location = new System.Drawing.Point(0, 0);
            this.lbl齿列总.MaxSize = new System.Drawing.Size(55, 0);
            this.lbl齿列总.MinSize = new System.Drawing.Size(55, 10);
            this.lbl齿列总.Name = "lbl齿列总";
            this.lbl齿列总.Size = new System.Drawing.Size(55, 117);
            this.lbl齿列总.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl齿列总.Text = "齿列";
            this.lbl齿列总.TextSize = new System.Drawing.Size(76, 0);
            this.lbl齿列总.TextVisible = true;
            // 
            // lbl咽部
            // 
            this.lbl咽部.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl咽部.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl咽部.AppearanceItemCaption.Options.UseFont = true;
            this.lbl咽部.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl咽部.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl咽部.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl咽部.Control = this.flow咽部;
            this.lbl咽部.CustomizationFormText = "咽部";
            this.lbl咽部.Location = new System.Drawing.Point(77, 145);
            this.lbl咽部.Name = "lbl咽部";
            this.lbl咽部.Size = new System.Drawing.Size(684, 24);
            this.lbl咽部.Text = "咽部";
            this.lbl咽部.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl咽部.TextSize = new System.Drawing.Size(50, 20);
            this.lbl咽部.TextToControlDistance = 5;
            // 
            // layoutControlItem153
            // 
            this.layoutControlItem153.Control = this.txt义齿2;
            this.layoutControlItem153.CustomizationFormText = "layoutControlItem153";
            this.layoutControlItem153.Location = new System.Drawing.Point(294, 97);
            this.layoutControlItem153.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem153.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem153.Name = "layoutControlItem153";
            this.layoutControlItem153.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem153.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem153.Text = "layoutControlItem153";
            this.layoutControlItem153.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem153.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem153.TextToControlDistance = 0;
            this.layoutControlItem153.TextVisible = false;
            // 
            // layoutControlItem154
            // 
            this.layoutControlItem154.Control = this.txt义齿3;
            this.layoutControlItem154.CustomizationFormText = "layoutControlItem154";
            this.layoutControlItem154.Location = new System.Drawing.Point(194, 121);
            this.layoutControlItem154.MaxSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem154.MinSize = new System.Drawing.Size(100, 24);
            this.layoutControlItem154.Name = "layoutControlItem154";
            this.layoutControlItem154.Size = new System.Drawing.Size(100, 24);
            this.layoutControlItem154.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem154.Text = "layoutControlItem154";
            this.layoutControlItem154.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem154.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem154.TextToControlDistance = 0;
            this.layoutControlItem154.TextVisible = false;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup9.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlGroup9.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup9.AppearanceGroup.Options.UseForeColor = true;
            this.layoutControlGroup9.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup9.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup9.CustomizationFormText = "查体";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.ExpandOnDoubleClick = true;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem45,
            this.lbl皮肤,
            this.lbl巩膜,
            this.lbl淋巴结,
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.lbl罗音,
            this.lbl心率,
            this.layoutControlItem53,
            this.layoutControlItem54,
            this.layoutControlItem55,
            this.layoutControlItem56,
            this.layoutControlItem57,
            this.layoutControlItem58,
            this.layoutControlItem59,
            this.layoutControlItem60,
            this.layoutControlItem61,
            this.layoutControlItem62,
            this.layoutControlItem63,
            this.layoutControlItem69,
            this.emptySpaceItem19,
            this.emptySpaceItem20,
            this.group妇科,
            this.layoutControlGroup10,
            this.layoutControlGroup27,
            this.layoutControlGroup28});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 1308);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup9.Size = new System.Drawing.Size(777, 674);
            this.layoutControlGroup9.Text = "查体";
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.flowLayoutPanel9;
            this.layoutControlItem45.CustomizationFormText = "眼底";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(761, 26);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "眼底";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // lbl皮肤
            // 
            this.lbl皮肤.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl皮肤.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl皮肤.AppearanceItemCaption.Options.UseFont = true;
            this.lbl皮肤.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl皮肤.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl皮肤.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl皮肤.Control = this.flow皮肤;
            this.lbl皮肤.CustomizationFormText = "皮肤";
            this.lbl皮肤.Location = new System.Drawing.Point(0, 26);
            this.lbl皮肤.MinSize = new System.Drawing.Size(199, 26);
            this.lbl皮肤.Name = "lbl皮肤";
            this.lbl皮肤.Size = new System.Drawing.Size(761, 26);
            this.lbl皮肤.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl皮肤.Text = "皮肤";
            this.lbl皮肤.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl皮肤.TextSize = new System.Drawing.Size(90, 20);
            this.lbl皮肤.TextToControlDistance = 5;
            // 
            // lbl巩膜
            // 
            this.lbl巩膜.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.lbl巩膜.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl巩膜.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl巩膜.AppearanceItemCaption.Options.UseFont = true;
            this.lbl巩膜.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl巩膜.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl巩膜.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl巩膜.Control = this.flow巩膜;
            this.lbl巩膜.CustomizationFormText = "巩膜";
            this.lbl巩膜.Location = new System.Drawing.Point(0, 52);
            this.lbl巩膜.MinSize = new System.Drawing.Size(199, 26);
            this.lbl巩膜.Name = "lbl巩膜";
            this.lbl巩膜.Size = new System.Drawing.Size(761, 26);
            this.lbl巩膜.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl巩膜.Text = "巩膜";
            this.lbl巩膜.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl巩膜.TextSize = new System.Drawing.Size(90, 20);
            this.lbl巩膜.TextToControlDistance = 5;
            // 
            // lbl淋巴结
            // 
            this.lbl淋巴结.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl淋巴结.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl淋巴结.AppearanceItemCaption.Options.UseFont = true;
            this.lbl淋巴结.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl淋巴结.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl淋巴结.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl淋巴结.Control = this.flow淋巴结;
            this.lbl淋巴结.CustomizationFormText = "淋巴结";
            this.lbl淋巴结.Location = new System.Drawing.Point(0, 78);
            this.lbl淋巴结.MinSize = new System.Drawing.Size(199, 26);
            this.lbl淋巴结.Name = "lbl淋巴结";
            this.lbl淋巴结.Size = new System.Drawing.Size(761, 26);
            this.lbl淋巴结.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl淋巴结.Text = "淋巴结";
            this.lbl淋巴结.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl淋巴结.TextSize = new System.Drawing.Size(90, 20);
            this.lbl淋巴结.TextToControlDistance = 5;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem49.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem49.Control = this.radio桶状胸;
            this.layoutControlItem49.CustomizationFormText = "桶状胸：";
            this.layoutControlItem49.Location = new System.Drawing.Point(92, 104);
            this.layoutControlItem49.MinSize = new System.Drawing.Size(149, 26);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(203, 26);
            this.layoutControlItem49.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem49.Text = "桶状胸：";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem50.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.Control = this.flowLayoutPanel13;
            this.layoutControlItem50.CustomizationFormText = "呼吸音：";
            this.layoutControlItem50.Location = new System.Drawing.Point(92, 130);
            this.layoutControlItem50.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(669, 26);
            this.layoutControlItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem50.Text = "呼吸音：";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // lbl罗音
            // 
            this.lbl罗音.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl罗音.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl罗音.AppearanceItemCaption.Options.UseFont = true;
            this.lbl罗音.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl罗音.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl罗音.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl罗音.Control = this.flow罗音;
            this.lbl罗音.CustomizationFormText = "罗音：";
            this.lbl罗音.Location = new System.Drawing.Point(92, 156);
            this.lbl罗音.MinSize = new System.Drawing.Size(199, 26);
            this.lbl罗音.Name = "lbl罗音";
            this.lbl罗音.Size = new System.Drawing.Size(669, 26);
            this.lbl罗音.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl罗音.Text = "罗音：";
            this.lbl罗音.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl罗音.TextSize = new System.Drawing.Size(90, 20);
            this.lbl罗音.TextToControlDistance = 5;
            // 
            // lbl心率
            // 
            this.lbl心率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl心率.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl心率.AppearanceItemCaption.Options.UseFont = true;
            this.lbl心率.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl心率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl心率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl心率.Control = this.txt心率;
            this.lbl心率.CustomizationFormText = "心率：";
            this.lbl心率.Location = new System.Drawing.Point(92, 182);
            this.lbl心率.MinSize = new System.Drawing.Size(199, 26);
            this.lbl心率.Name = "lbl心率";
            this.lbl心率.Size = new System.Drawing.Size(669, 26);
            this.lbl心率.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl心率.Text = "心率：";
            this.lbl心率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl心率.TextSize = new System.Drawing.Size(90, 20);
            this.lbl心率.TextToControlDistance = 5;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem53.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem53.Control = this.radio心律;
            this.layoutControlItem53.CustomizationFormText = "心律：";
            this.layoutControlItem53.Location = new System.Drawing.Point(92, 208);
            this.layoutControlItem53.MinSize = new System.Drawing.Size(149, 26);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(315, 26);
            this.layoutControlItem53.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem53.Text = "心律：";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem54.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.Control = this.flowLayoutPanel15;
            this.layoutControlItem54.CustomizationFormText = "杂音：";
            this.layoutControlItem54.Location = new System.Drawing.Point(92, 234);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(669, 26);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Text = "杂音：";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem55.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem55.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem55.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem55.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem55.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem55.Control = this.flowLayoutPanel16;
            this.layoutControlItem55.CustomizationFormText = "压痛：";
            this.layoutControlItem55.Location = new System.Drawing.Point(92, 260);
            this.layoutControlItem55.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(669, 26);
            this.layoutControlItem55.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem55.Text = "压痛：";
            this.layoutControlItem55.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem55.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem56.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem56.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem56.Control = this.flowLayoutPanel17;
            this.layoutControlItem56.CustomizationFormText = "包块：";
            this.layoutControlItem56.Location = new System.Drawing.Point(92, 286);
            this.layoutControlItem56.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(669, 26);
            this.layoutControlItem56.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem56.Text = "包块：";
            this.layoutControlItem56.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem56.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem56.TextToControlDistance = 5;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem57.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem57.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem57.Control = this.flowLayoutPanel18;
            this.layoutControlItem57.CustomizationFormText = "肝大：";
            this.layoutControlItem57.Location = new System.Drawing.Point(92, 312);
            this.layoutControlItem57.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(669, 26);
            this.layoutControlItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem57.Text = "肝大：";
            this.layoutControlItem57.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem57.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem57.TextToControlDistance = 5;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem58.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem58.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem58.Control = this.flowLayoutPanel19;
            this.layoutControlItem58.CustomizationFormText = "脾大：";
            this.layoutControlItem58.Location = new System.Drawing.Point(92, 338);
            this.layoutControlItem58.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Size = new System.Drawing.Size(669, 26);
            this.layoutControlItem58.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem58.Text = "脾大：";
            this.layoutControlItem58.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem58.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem58.TextToControlDistance = 5;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem59.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem59.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem59.Control = this.flowLayoutPanel20;
            this.layoutControlItem59.CustomizationFormText = "移动性浊音：";
            this.layoutControlItem59.Location = new System.Drawing.Point(92, 364);
            this.layoutControlItem59.MinSize = new System.Drawing.Size(199, 26);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(669, 26);
            this.layoutControlItem59.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem59.Text = "移动性浊音：";
            this.layoutControlItem59.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem59.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem59.TextToControlDistance = 5;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem60.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem60.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem60.Control = this.radio下肢水肿;
            this.layoutControlItem60.CustomizationFormText = "下肢水肿";
            this.layoutControlItem60.Location = new System.Drawing.Point(0, 390);
            this.layoutControlItem60.MaxSize = new System.Drawing.Size(500, 0);
            this.layoutControlItem60.MinSize = new System.Drawing.Size(134, 26);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(761, 26);
            this.layoutControlItem60.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem60.Text = "下肢水肿";
            this.layoutControlItem60.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem60.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem60.TextToControlDistance = 5;
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem61.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem61.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem61.Control = this.radio足背动脉搏动;
            this.layoutControlItem61.CustomizationFormText = "足背动脉搏动";
            this.layoutControlItem61.Location = new System.Drawing.Point(0, 416);
            this.layoutControlItem61.MaxSize = new System.Drawing.Size(600, 0);
            this.layoutControlItem61.MinSize = new System.Drawing.Size(134, 26);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Size = new System.Drawing.Size(761, 26);
            this.layoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem61.Text = "足背动脉搏动";
            this.layoutControlItem61.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem61.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem61.TextToControlDistance = 5;
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem62.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem62.Control = this.flow肛门指诊;
            this.layoutControlItem62.CustomizationFormText = "肛门指诊";
            this.layoutControlItem62.Location = new System.Drawing.Point(0, 442);
            this.layoutControlItem62.MinSize = new System.Drawing.Size(184, 26);
            this.layoutControlItem62.Name = "layoutControlItem62";
            this.layoutControlItem62.Size = new System.Drawing.Size(761, 26);
            this.layoutControlItem62.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem62.Text = "肛门指诊";
            this.layoutControlItem62.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem62.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem62.TextToControlDistance = 5;
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem63.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem63.Control = this.flow乳腺;
            this.layoutControlItem63.CustomizationFormText = "乳腺";
            this.layoutControlItem63.Location = new System.Drawing.Point(0, 468);
            this.layoutControlItem63.MinSize = new System.Drawing.Size(184, 26);
            this.layoutControlItem63.Name = "layoutControlItem63";
            this.layoutControlItem63.Size = new System.Drawing.Size(761, 26);
            this.layoutControlItem63.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem63.Text = "乳腺";
            this.layoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem63.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem63.TextToControlDistance = 5;
            // 
            // layoutControlItem69
            // 
            this.layoutControlItem69.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem69.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem69.Control = this.flowLayoutPanel28;
            this.layoutControlItem69.CustomizationFormText = "其他";
            this.layoutControlItem69.Location = new System.Drawing.Point(0, 614);
            this.layoutControlItem69.MinSize = new System.Drawing.Size(184, 24);
            this.layoutControlItem69.Name = "layoutControlItem69";
            this.layoutControlItem69.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem69.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem69.Text = "其他";
            this.layoutControlItem69.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem69.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem69.TextToControlDistance = 5;
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.CustomizationFormText = "emptySpaceItem19";
            this.emptySpaceItem19.Location = new System.Drawing.Point(407, 208);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(354, 26);
            this.emptySpaceItem19.Text = "emptySpaceItem19";
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.CustomizationFormText = "emptySpaceItem20";
            this.emptySpaceItem20.Location = new System.Drawing.Point(295, 104);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(466, 26);
            this.emptySpaceItem20.Text = "emptySpaceItem20";
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(0, 0);
            // 
            // group妇科
            // 
            this.group妇科.CustomizationFormText = "group妇科";
            this.group妇科.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.group妇科.GroupBordersVisible = false;
            this.group妇科.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem64,
            this.layoutControlItem65,
            this.layoutControlItem66,
            this.layoutControlItem67,
            this.layoutControlItem68,
            this.layoutControlGroup29});
            this.group妇科.Location = new System.Drawing.Point(0, 494);
            this.group妇科.Name = "group妇科";
            this.group妇科.Size = new System.Drawing.Size(761, 120);
            this.group妇科.Text = "group妇科";
            // 
            // layoutControlItem64
            // 
            this.layoutControlItem64.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem64.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem64.Control = this.flowLayoutPanel23;
            this.layoutControlItem64.CustomizationFormText = "外阴";
            this.layoutControlItem64.Location = new System.Drawing.Point(92, 0);
            this.layoutControlItem64.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem64.Name = "layoutControlItem64";
            this.layoutControlItem64.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem64.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem64.Text = "外阴";
            this.layoutControlItem64.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem64.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem64.TextToControlDistance = 5;
            // 
            // layoutControlItem65
            // 
            this.layoutControlItem65.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem65.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem65.Control = this.flowLayoutPanel24;
            this.layoutControlItem65.CustomizationFormText = "阴道";
            this.layoutControlItem65.Location = new System.Drawing.Point(92, 24);
            this.layoutControlItem65.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem65.Name = "layoutControlItem65";
            this.layoutControlItem65.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem65.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem65.Text = "阴道";
            this.layoutControlItem65.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem65.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem65.TextToControlDistance = 5;
            // 
            // layoutControlItem66
            // 
            this.layoutControlItem66.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem66.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem66.Control = this.flowLayoutPanel25;
            this.layoutControlItem66.CustomizationFormText = "宫颈";
            this.layoutControlItem66.Location = new System.Drawing.Point(92, 48);
            this.layoutControlItem66.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem66.Name = "layoutControlItem66";
            this.layoutControlItem66.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem66.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem66.Text = "宫颈";
            this.layoutControlItem66.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem66.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem66.TextToControlDistance = 5;
            // 
            // layoutControlItem67
            // 
            this.layoutControlItem67.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem67.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem67.Control = this.flowLayoutPanel26;
            this.layoutControlItem67.CustomizationFormText = "宫体";
            this.layoutControlItem67.Location = new System.Drawing.Point(92, 72);
            this.layoutControlItem67.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem67.Name = "layoutControlItem67";
            this.layoutControlItem67.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem67.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem67.Text = "宫体";
            this.layoutControlItem67.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem67.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem67.TextToControlDistance = 5;
            // 
            // layoutControlItem68
            // 
            this.layoutControlItem68.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem68.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem68.Control = this.flowLayoutPanel27;
            this.layoutControlItem68.CustomizationFormText = "附件";
            this.layoutControlItem68.Location = new System.Drawing.Point(92, 96);
            this.layoutControlItem68.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem68.Name = "layoutControlItem68";
            this.layoutControlItem68.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem68.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem68.Text = "附件";
            this.layoutControlItem68.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem68.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem68.TextToControlDistance = 5;
            // 
            // layoutControlGroup29
            // 
            this.layoutControlGroup29.CustomizationFormText = "layoutControlGroup29";
            this.layoutControlGroup29.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem11});
            this.layoutControlGroup29.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup29.Name = "layoutControlGroup29";
            this.layoutControlGroup29.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup29.Size = new System.Drawing.Size(92, 120);
            this.layoutControlGroup29.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup29.Text = "layoutControlGroup29";
            this.layoutControlGroup29.TextVisible = false;
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem11.CustomizationFormText = "妇科";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(90, 118);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.Text = "妇科";
            this.emptySpaceItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem11.TextVisible = true;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "layoutControlGroup10";
            this.layoutControlGroup10.GroupBordersVisible = false;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup26});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 104);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(92, 78);
            this.layoutControlGroup10.Text = "layoutControlGroup10";
            // 
            // layoutControlGroup26
            // 
            this.layoutControlGroup26.CustomizationFormText = "layoutControlGroup26";
            this.layoutControlGroup26.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem8});
            this.layoutControlGroup26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup26.Name = "layoutControlGroup26";
            this.layoutControlGroup26.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup26.Size = new System.Drawing.Size(92, 78);
            this.layoutControlGroup26.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup26.Text = "layoutControlGroup26";
            this.layoutControlGroup26.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem8.CustomizationFormText = "肺";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(90, 76);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.Text = "肺";
            this.emptySpaceItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(90, 20);
            this.emptySpaceItem8.TextVisible = true;
            // 
            // layoutControlGroup27
            // 
            this.layoutControlGroup27.CustomizationFormText = "layoutControlGroup27";
            this.layoutControlGroup27.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem9});
            this.layoutControlGroup27.Location = new System.Drawing.Point(0, 182);
            this.layoutControlGroup27.Name = "layoutControlGroup27";
            this.layoutControlGroup27.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup27.Size = new System.Drawing.Size(92, 78);
            this.layoutControlGroup27.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup27.Text = "layoutControlGroup27";
            this.layoutControlGroup27.TextVisible = false;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem9.CustomizationFormText = "心脏";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(90, 76);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.Text = "心脏";
            this.emptySpaceItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(90, 20);
            this.emptySpaceItem9.TextVisible = true;
            // 
            // layoutControlGroup28
            // 
            this.layoutControlGroup28.CustomizationFormText = "layoutControlGroup28";
            this.layoutControlGroup28.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem10});
            this.layoutControlGroup28.Location = new System.Drawing.Point(0, 260);
            this.layoutControlGroup28.Name = "layoutControlGroup28";
            this.layoutControlGroup28.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup28.Size = new System.Drawing.Size(92, 130);
            this.layoutControlGroup28.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup28.Text = "layoutControlGroup28";
            this.layoutControlGroup28.TextVisible = false;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem10.CustomizationFormText = "腹部";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(90, 128);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.Text = "腹部";
            this.emptySpaceItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(90, 20);
            this.emptySpaceItem10.TextVisible = true;
            // 
            // group辅助查体
            // 
            this.group辅助查体.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.group辅助查体.AppearanceGroup.Options.UseFont = true;
            this.group辅助查体.AppearanceGroup.Options.UseTextOptions = true;
            this.group辅助查体.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.group辅助查体.CustomizationFormText = "辅助查体";
            this.group辅助查体.ExpandButtonVisible = true;
            this.group辅助查体.ExpandOnDoubleClick = true;
            this.group辅助查体.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem70,
            this.layoutControlItem72,
            this.layoutControlItem71,
            this.layoutControlItem74,
            this.layoutControlItem75,
            this.layoutControlItem76,
            this.layoutControlItem78,
            this.layoutControlItem79,
            this.layoutControlItem80,
            this.layoutControlItem81,
            this.layoutControlItem82,
            this.lbl血清谷丙转氨酶,
            this.lbl血清谷草转氨酶,
            this.layoutControlItem86,
            this.lbl总胆红素,
            this.layoutControlItem84,
            this.lbl血清肌酐,
            this.lbl血尿素氮,
            this.layoutControlItem91,
            this.layoutControlItem90,
            this.lbl总胆固醇,
            this.lbl甘油三酯,
            this.lbl血清低密度脂蛋白胆固醇,
            this.lbl血清高密度脂蛋白胆固醇,
            this.layoutControlItem96,
            this.layoutControlItem98,
            this.layoutControlItem99,
            this.layoutControlGroup30,
            this.layoutControlGroup31,
            this.layoutControlGroup32,
            this.layoutControlGroup33,
            this.layoutControlItem13,
            this.layoutControlItem15,
            this.layoutControlItem19,
            this.layoutControlItem26,
            this.layoutControlItem41,
            this.lbl腹部B超,
            this.emptySpaceItem2,
            this.lbl其他B超,
            this.emptySpaceItem3});
            this.group辅助查体.Location = new System.Drawing.Point(0, 1982);
            this.group辅助查体.Name = "group辅助查体";
            this.group辅助查体.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.group辅助查体.Size = new System.Drawing.Size(777, 826);
            this.group辅助查体.Text = "辅助查体";
            // 
            // layoutControlItem70
            // 
            this.layoutControlItem70.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem70.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem70.Control = this.txt血红蛋白;
            this.layoutControlItem70.CustomizationFormText = "血红蛋白";
            this.layoutControlItem70.Location = new System.Drawing.Point(92, 24);
            this.layoutControlItem70.Name = "layoutControlItem70";
            this.layoutControlItem70.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem70.Text = "血红蛋白";
            this.layoutControlItem70.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem70.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem70.TextToControlDistance = 5;
            // 
            // layoutControlItem72
            // 
            this.layoutControlItem72.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem72.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem72.Control = this.txt白细胞;
            this.layoutControlItem72.CustomizationFormText = "白细胞";
            this.layoutControlItem72.Location = new System.Drawing.Point(92, 48);
            this.layoutControlItem72.Name = "layoutControlItem72";
            this.layoutControlItem72.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem72.Text = "白细胞";
            this.layoutControlItem72.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem72.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem72.TextToControlDistance = 5;
            // 
            // layoutControlItem71
            // 
            this.layoutControlItem71.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem71.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem71.Control = this.txt血小板;
            this.layoutControlItem71.CustomizationFormText = "血小板";
            this.layoutControlItem71.Location = new System.Drawing.Point(92, 72);
            this.layoutControlItem71.Name = "layoutControlItem71";
            this.layoutControlItem71.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem71.Text = "血小板";
            this.layoutControlItem71.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem71.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem71.TextToControlDistance = 5;
            // 
            // layoutControlItem74
            // 
            this.layoutControlItem74.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem74.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem74.Control = this.flowLayoutPanel29;
            this.layoutControlItem74.CustomizationFormText = "其他";
            this.layoutControlItem74.Location = new System.Drawing.Point(92, 96);
            this.layoutControlItem74.Name = "layoutControlItem74";
            this.layoutControlItem74.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem74.Text = "其他";
            this.layoutControlItem74.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem74.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem74.TextToControlDistance = 5;
            // 
            // layoutControlItem75
            // 
            this.layoutControlItem75.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem75.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem75.Control = this.flowLayoutPanel30;
            this.layoutControlItem75.CustomizationFormText = "layoutControlItem75";
            this.layoutControlItem75.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem75.Name = "layoutControlItem75";
            this.layoutControlItem75.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem75.Text = "尿常规*";
            this.layoutControlItem75.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem75.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem75.TextToControlDistance = 0;
            // 
            // layoutControlItem76
            // 
            this.layoutControlItem76.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem76.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem76.Control = this.txt空腹血糖;
            this.layoutControlItem76.CustomizationFormText = "空腹血糖*";
            this.layoutControlItem76.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem76.Name = "layoutControlItem76";
            this.layoutControlItem76.Size = new System.Drawing.Size(269, 24);
            this.layoutControlItem76.Text = "空腹血糖*";
            this.layoutControlItem76.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem76.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem76.TextToControlDistance = 5;
            // 
            // layoutControlItem78
            // 
            this.layoutControlItem78.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem78.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem78.Control = this.flow心电图;
            this.layoutControlItem78.CustomizationFormText = "心电图*";
            this.layoutControlItem78.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem78.MinSize = new System.Drawing.Size(199, 50);
            this.layoutControlItem78.Name = "layoutControlItem78";
            this.layoutControlItem78.Size = new System.Drawing.Size(761, 50);
            this.layoutControlItem78.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem78.Text = "心电图*";
            this.layoutControlItem78.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem78.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem78.TextToControlDistance = 5;
            // 
            // layoutControlItem79
            // 
            this.layoutControlItem79.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem79.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem79.Control = this.txt尿微量白蛋白;
            this.layoutControlItem79.CustomizationFormText = "尿微量白蛋白*";
            this.layoutControlItem79.Location = new System.Drawing.Point(0, 242);
            this.layoutControlItem79.Name = "layoutControlItem79";
            this.layoutControlItem79.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem79.Text = "尿微量白蛋白*";
            this.layoutControlItem79.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem79.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem79.TextToControlDistance = 5;
            // 
            // layoutControlItem80
            // 
            this.layoutControlItem80.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem80.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem80.Control = this.flowLayoutPanel32;
            this.layoutControlItem80.CustomizationFormText = "大便潜血* ";
            this.layoutControlItem80.Location = new System.Drawing.Point(0, 266);
            this.layoutControlItem80.Name = "layoutControlItem80";
            this.layoutControlItem80.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem80.Text = "大便潜血* ";
            this.layoutControlItem80.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem80.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem80.TextToControlDistance = 5;
            // 
            // layoutControlItem81
            // 
            this.layoutControlItem81.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem81.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem81.Control = this.txt糖化血红蛋白;
            this.layoutControlItem81.CustomizationFormText = "糖化血红蛋白*";
            this.layoutControlItem81.Location = new System.Drawing.Point(0, 290);
            this.layoutControlItem81.Name = "layoutControlItem81";
            this.layoutControlItem81.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem81.Text = "糖化血红蛋白*";
            this.layoutControlItem81.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem81.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem81.TextToControlDistance = 5;
            // 
            // layoutControlItem82
            // 
            this.layoutControlItem82.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem82.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem82.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem82.Control = this.flowLayoutPanel33;
            this.layoutControlItem82.CustomizationFormText = "乙型肝炎表面抗原* ";
            this.layoutControlItem82.Location = new System.Drawing.Point(0, 314);
            this.layoutControlItem82.MinSize = new System.Drawing.Size(199, 32);
            this.layoutControlItem82.Name = "layoutControlItem82";
            this.layoutControlItem82.Size = new System.Drawing.Size(761, 32);
            this.layoutControlItem82.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem82.Text = "乙型肝炎表面抗原* ";
            this.layoutControlItem82.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem82.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem82.TextToControlDistance = 5;
            // 
            // lbl血清谷丙转氨酶
            // 
            this.lbl血清谷丙转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl血清谷丙转氨酶.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血清谷丙转氨酶.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血清谷丙转氨酶.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl血清谷丙转氨酶.Control = this.txt血清谷丙转氨酶;
            this.lbl血清谷丙转氨酶.CustomizationFormText = "血清谷丙转氨酶";
            this.lbl血清谷丙转氨酶.Location = new System.Drawing.Point(92, 346);
            this.lbl血清谷丙转氨酶.Name = "lbl血清谷丙转氨酶";
            this.lbl血清谷丙转氨酶.Size = new System.Drawing.Size(669, 24);
            this.lbl血清谷丙转氨酶.Text = "血清谷丙转氨酶";
            this.lbl血清谷丙转氨酶.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血清谷丙转氨酶.TextSize = new System.Drawing.Size(150, 20);
            this.lbl血清谷丙转氨酶.TextToControlDistance = 5;
            // 
            // lbl血清谷草转氨酶
            // 
            this.lbl血清谷草转氨酶.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血清谷草转氨酶.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl血清谷草转氨酶.Control = this.txt血清谷草转氨酶;
            this.lbl血清谷草转氨酶.CustomizationFormText = "血清谷草转氨酶";
            this.lbl血清谷草转氨酶.Location = new System.Drawing.Point(92, 370);
            this.lbl血清谷草转氨酶.Name = "lbl血清谷草转氨酶";
            this.lbl血清谷草转氨酶.Size = new System.Drawing.Size(669, 24);
            this.lbl血清谷草转氨酶.Text = "血清谷草转氨酶";
            this.lbl血清谷草转氨酶.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血清谷草转氨酶.TextSize = new System.Drawing.Size(150, 20);
            this.lbl血清谷草转氨酶.TextToControlDistance = 5;
            // 
            // layoutControlItem86
            // 
            this.layoutControlItem86.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem86.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem86.Control = this.txt白蛋白;
            this.layoutControlItem86.CustomizationFormText = "白蛋白";
            this.layoutControlItem86.Location = new System.Drawing.Point(92, 394);
            this.layoutControlItem86.Name = "layoutControlItem86";
            this.layoutControlItem86.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem86.Text = "白蛋白";
            this.layoutControlItem86.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem86.TextSize = new System.Drawing.Size(150, 20);
            this.layoutControlItem86.TextToControlDistance = 5;
            // 
            // lbl总胆红素
            // 
            this.lbl总胆红素.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl总胆红素.AppearanceItemCaption.Options.UseFont = true;
            this.lbl总胆红素.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl总胆红素.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl总胆红素.Control = this.txt总胆红素;
            this.lbl总胆红素.CustomizationFormText = "总胆红素";
            this.lbl总胆红素.Location = new System.Drawing.Point(92, 418);
            this.lbl总胆红素.Name = "lbl总胆红素";
            this.lbl总胆红素.Size = new System.Drawing.Size(669, 24);
            this.lbl总胆红素.Text = "总胆红素";
            this.lbl总胆红素.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl总胆红素.TextSize = new System.Drawing.Size(150, 20);
            this.lbl总胆红素.TextToControlDistance = 5;
            // 
            // layoutControlItem84
            // 
            this.layoutControlItem84.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem84.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem84.Control = this.txt结合胆红素;
            this.layoutControlItem84.CustomizationFormText = "结合胆红素";
            this.layoutControlItem84.Location = new System.Drawing.Point(92, 442);
            this.layoutControlItem84.Name = "layoutControlItem84";
            this.layoutControlItem84.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem84.Text = "结合胆红素";
            this.layoutControlItem84.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem84.TextSize = new System.Drawing.Size(150, 20);
            this.layoutControlItem84.TextToControlDistance = 5;
            // 
            // lbl血清肌酐
            // 
            this.lbl血清肌酐.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl血清肌酐.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血清肌酐.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血清肌酐.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl血清肌酐.Control = this.txt血清肌酐;
            this.lbl血清肌酐.CustomizationFormText = "血清肌酐";
            this.lbl血清肌酐.Location = new System.Drawing.Point(92, 466);
            this.lbl血清肌酐.Name = "lbl血清肌酐";
            this.lbl血清肌酐.Size = new System.Drawing.Size(669, 24);
            this.lbl血清肌酐.Text = "血清肌酐";
            this.lbl血清肌酐.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血清肌酐.TextSize = new System.Drawing.Size(150, 20);
            this.lbl血清肌酐.TextToControlDistance = 5;
            // 
            // lbl血尿素氮
            // 
            this.lbl血尿素氮.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl血尿素氮.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血尿素氮.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血尿素氮.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl血尿素氮.Control = this.txt血尿素氮;
            this.lbl血尿素氮.CustomizationFormText = "血尿素氮";
            this.lbl血尿素氮.Location = new System.Drawing.Point(92, 490);
            this.lbl血尿素氮.Name = "lbl血尿素氮";
            this.lbl血尿素氮.Size = new System.Drawing.Size(669, 24);
            this.lbl血尿素氮.Text = "血尿素氮";
            this.lbl血尿素氮.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血尿素氮.TextSize = new System.Drawing.Size(150, 20);
            this.lbl血尿素氮.TextToControlDistance = 5;
            // 
            // layoutControlItem91
            // 
            this.layoutControlItem91.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem91.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem91.Control = this.txt血钾浓度;
            this.layoutControlItem91.CustomizationFormText = "血钾浓度";
            this.layoutControlItem91.Location = new System.Drawing.Point(92, 514);
            this.layoutControlItem91.Name = "layoutControlItem91";
            this.layoutControlItem91.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem91.Text = "血钾浓度";
            this.layoutControlItem91.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem91.TextSize = new System.Drawing.Size(150, 20);
            this.layoutControlItem91.TextToControlDistance = 5;
            // 
            // layoutControlItem90
            // 
            this.layoutControlItem90.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem90.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem90.Control = this.txt血钠浓度;
            this.layoutControlItem90.CustomizationFormText = "血钠浓度";
            this.layoutControlItem90.Location = new System.Drawing.Point(92, 538);
            this.layoutControlItem90.Name = "layoutControlItem90";
            this.layoutControlItem90.Size = new System.Drawing.Size(669, 24);
            this.layoutControlItem90.Text = "血钠浓度";
            this.layoutControlItem90.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem90.TextSize = new System.Drawing.Size(150, 20);
            this.layoutControlItem90.TextToControlDistance = 5;
            // 
            // lbl总胆固醇
            // 
            this.lbl总胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl总胆固醇.AppearanceItemCaption.Options.UseFont = true;
            this.lbl总胆固醇.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl总胆固醇.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl总胆固醇.Control = this.txt总胆固醇;
            this.lbl总胆固醇.CustomizationFormText = "总胆固醇";
            this.lbl总胆固醇.Location = new System.Drawing.Point(92, 562);
            this.lbl总胆固醇.Name = "lbl总胆固醇";
            this.lbl总胆固醇.Size = new System.Drawing.Size(669, 24);
            this.lbl总胆固醇.Text = "总胆固醇";
            this.lbl总胆固醇.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl总胆固醇.TextSize = new System.Drawing.Size(150, 20);
            this.lbl总胆固醇.TextToControlDistance = 5;
            // 
            // lbl甘油三酯
            // 
            this.lbl甘油三酯.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl甘油三酯.AppearanceItemCaption.Options.UseFont = true;
            this.lbl甘油三酯.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl甘油三酯.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl甘油三酯.Control = this.txt甘油三酯;
            this.lbl甘油三酯.CustomizationFormText = "甘油三酯";
            this.lbl甘油三酯.Location = new System.Drawing.Point(92, 586);
            this.lbl甘油三酯.Name = "lbl甘油三酯";
            this.lbl甘油三酯.Size = new System.Drawing.Size(669, 24);
            this.lbl甘油三酯.Text = "甘油三酯";
            this.lbl甘油三酯.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl甘油三酯.TextSize = new System.Drawing.Size(150, 20);
            this.lbl甘油三酯.TextToControlDistance = 5;
            // 
            // lbl血清低密度脂蛋白胆固醇
            // 
            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl血清低密度脂蛋白胆固醇.Control = this.txt血清低密度脂蛋白胆固醇;
            this.lbl血清低密度脂蛋白胆固醇.CustomizationFormText = "血清低密度脂蛋白胆固醇";
            this.lbl血清低密度脂蛋白胆固醇.Location = new System.Drawing.Point(92, 610);
            this.lbl血清低密度脂蛋白胆固醇.Name = "lbl血清低密度脂蛋白胆固醇";
            this.lbl血清低密度脂蛋白胆固醇.Size = new System.Drawing.Size(669, 24);
            this.lbl血清低密度脂蛋白胆固醇.Text = "血清低密度脂蛋白胆固醇";
            this.lbl血清低密度脂蛋白胆固醇.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血清低密度脂蛋白胆固醇.TextSize = new System.Drawing.Size(150, 20);
            this.lbl血清低密度脂蛋白胆固醇.TextToControlDistance = 5;
            // 
            // lbl血清高密度脂蛋白胆固醇
            // 
            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl血清高密度脂蛋白胆固醇.Control = this.txt血清高密度脂蛋白胆固醇;
            this.lbl血清高密度脂蛋白胆固醇.CustomizationFormText = "血清高密度脂蛋白胆固醇";
            this.lbl血清高密度脂蛋白胆固醇.Location = new System.Drawing.Point(92, 634);
            this.lbl血清高密度脂蛋白胆固醇.Name = "lbl血清高密度脂蛋白胆固醇";
            this.lbl血清高密度脂蛋白胆固醇.Size = new System.Drawing.Size(669, 24);
            this.lbl血清高密度脂蛋白胆固醇.Text = "血清高密度脂蛋白胆固醇";
            this.lbl血清高密度脂蛋白胆固醇.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血清高密度脂蛋白胆固醇.TextSize = new System.Drawing.Size(150, 20);
            this.lbl血清高密度脂蛋白胆固醇.TextToControlDistance = 5;
            // 
            // layoutControlItem96
            // 
            this.layoutControlItem96.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem96.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem96.Control = this.flowLayoutPanel34;
            this.layoutControlItem96.CustomizationFormText = "胸部X线片*";
            this.layoutControlItem96.Location = new System.Drawing.Point(0, 658);
            this.layoutControlItem96.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem96.Name = "layoutControlItem96";
            this.layoutControlItem96.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem96.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem96.Text = "胸部X线片*";
            this.layoutControlItem96.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem96.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem96.TextToControlDistance = 5;
            // 
            // layoutControlItem98
            // 
            this.layoutControlItem98.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem98.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem98.Control = this.flowLayoutPanel36;
            this.layoutControlItem98.CustomizationFormText = "宫颈涂片*";
            this.layoutControlItem98.Location = new System.Drawing.Point(0, 742);
            this.layoutControlItem98.MinSize = new System.Drawing.Size(199, 24);
            this.layoutControlItem98.Name = "layoutControlItem98";
            this.layoutControlItem98.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem98.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem98.Text = "宫颈涂片*";
            this.layoutControlItem98.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem98.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem98.TextToControlDistance = 5;
            // 
            // layoutControlItem99
            // 
            this.layoutControlItem99.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem99.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem99.Control = this.txt辅助检查_其他;
            this.layoutControlItem99.CustomizationFormText = "其 他*";
            this.layoutControlItem99.Location = new System.Drawing.Point(0, 766);
            this.layoutControlItem99.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem99.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem99.Name = "layoutControlItem99";
            this.layoutControlItem99.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem99.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem99.Text = "其 他*";
            this.layoutControlItem99.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem99.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem99.TextToControlDistance = 5;
            // 
            // layoutControlGroup30
            // 
            this.layoutControlGroup30.CustomizationFormText = "layoutControlGroup30";
            this.layoutControlGroup30.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem13});
            this.layoutControlGroup30.Location = new System.Drawing.Point(0, 24);
            this.layoutControlGroup30.Name = "layoutControlGroup30";
            this.layoutControlGroup30.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup30.Size = new System.Drawing.Size(92, 96);
            this.layoutControlGroup30.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup30.Text = "layoutControlGroup30";
            this.layoutControlGroup30.TextVisible = false;
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem13.CustomizationFormText = "血常规*";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(90, 94);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.Text = "血常规*";
            this.emptySpaceItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem13.TextVisible = true;
            // 
            // layoutControlGroup31
            // 
            this.layoutControlGroup31.CustomizationFormText = "layoutControlGroup31";
            this.layoutControlGroup31.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem15});
            this.layoutControlGroup31.Location = new System.Drawing.Point(0, 346);
            this.layoutControlGroup31.Name = "layoutControlGroup31";
            this.layoutControlGroup31.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup31.Size = new System.Drawing.Size(92, 120);
            this.layoutControlGroup31.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup31.Text = "layoutControlGroup31";
            this.layoutControlGroup31.TextVisible = false;
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem15.CustomizationFormText = "肝功能*";
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem15.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem15.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(90, 118);
            this.emptySpaceItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem15.Text = "肝功能*";
            this.emptySpaceItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem15.TextVisible = true;
            // 
            // layoutControlGroup32
            // 
            this.layoutControlGroup32.CustomizationFormText = "layoutControlGroup32";
            this.layoutControlGroup32.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem16});
            this.layoutControlGroup32.Location = new System.Drawing.Point(0, 466);
            this.layoutControlGroup32.Name = "layoutControlGroup32";
            this.layoutControlGroup32.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup32.Size = new System.Drawing.Size(92, 96);
            this.layoutControlGroup32.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup32.Text = "layoutControlGroup32";
            this.layoutControlGroup32.TextVisible = false;
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem16.CustomizationFormText = "肾功能*";
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem16.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem16.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(90, 94);
            this.emptySpaceItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem16.Text = "肾功能*";
            this.emptySpaceItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem16.TextVisible = true;
            // 
            // layoutControlGroup33
            // 
            this.layoutControlGroup33.CustomizationFormText = "layoutControlGroup33";
            this.layoutControlGroup33.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem17});
            this.layoutControlGroup33.Location = new System.Drawing.Point(0, 562);
            this.layoutControlGroup33.Name = "layoutControlGroup33";
            this.layoutControlGroup33.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup33.Size = new System.Drawing.Size(92, 96);
            this.layoutControlGroup33.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup33.Text = "layoutControlGroup33";
            this.layoutControlGroup33.TextVisible = false;
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem17.CustomizationFormText = "血脂* ";
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem17.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem17.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(90, 94);
            this.emptySpaceItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem17.Text = "血脂* ";
            this.emptySpaceItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem17.TextVisible = true;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.cboABO;
            this.layoutControlItem13.CustomizationFormText = "血型";
            this.layoutControlItem13.Location = new System.Drawing.Point(93, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(198, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "ABO";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.cboRh;
            this.layoutControlItem15.CustomizationFormText = "Rh*";
            this.layoutControlItem15.Location = new System.Drawing.Point(291, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(191, 24);
            this.layoutControlItem15.Text = "Rh*";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.flowLayoutPanel2;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(482, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(279, 24);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.txt同型半胱氨酸;
            this.layoutControlItem26.CustomizationFormText = "同型半胱氨酸*";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem26.Text = "同型半胱氨酸*";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem41.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem41.Control = this.txt空腹血糖2;
            this.layoutControlItem41.CustomizationFormText = "或";
            this.layoutControlItem41.Location = new System.Drawing.Point(269, 144);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(492, 24);
            this.layoutControlItem41.Text = "或";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(30, 20);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // lbl腹部B超
            // 
            this.lbl腹部B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl腹部B超.AppearanceItemCaption.Options.UseFont = true;
            this.lbl腹部B超.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl腹部B超.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl腹部B超.Control = this.flowLayoutPanel35;
            this.lbl腹部B超.CustomizationFormText = "B 超*";
            this.lbl腹部B超.Location = new System.Drawing.Point(100, 682);
            this.lbl腹部B超.MinSize = new System.Drawing.Size(199, 24);
            this.lbl腹部B超.Name = "lbl腹部B超";
            this.lbl腹部B超.Size = new System.Drawing.Size(661, 30);
            this.lbl腹部B超.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl腹部B超.Text = "腹部 B 超";
            this.lbl腹部B超.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl腹部B超.TextSize = new System.Drawing.Size(90, 20);
            this.lbl腹部B超.TextToControlDistance = 5;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.CustomizationFormText = "B 超*";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 682);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(100, 60);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(100, 60);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "B 超*";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(100, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // lbl其他B超
            // 
            this.lbl其他B超.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl其他B超.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl其他B超.Control = this.flowLayoutPanel4;
            this.lbl其他B超.CustomizationFormText = "其他";
            this.lbl其他B超.Location = new System.Drawing.Point(100, 712);
            this.lbl其他B超.MinSize = new System.Drawing.Size(199, 25);
            this.lbl其他B超.Name = "lbl其他B超";
            this.lbl其他B超.Size = new System.Drawing.Size(661, 30);
            this.lbl其他B超.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl其他B超.Text = "其他";
            this.lbl其他B超.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl其他B超.TextSize = new System.Drawing.Size(90, 20);
            this.lbl其他B超.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem3.CustomizationFormText = "血型";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(93, 24);
            this.emptySpaceItem3.Text = "血型";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(76, 0);
            this.emptySpaceItem3.TextVisible = true;
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup11.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup11.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup11.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup11.CustomizationFormText = "中医体质辨识* ";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.Expanded = false;
            this.layoutControlGroup11.ExpandOnDoubleClick = true;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem100,
            this.layoutControlItem101,
            this.layoutControlItem102,
            this.layoutControlItem103,
            this.layoutControlItem104,
            this.layoutControlItem105,
            this.layoutControlItem106,
            this.layoutControlItem107,
            this.layoutControlItem108,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem11});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 2808);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup11.Size = new System.Drawing.Size(777, 29);
            this.layoutControlGroup11.Text = "中医体质辨识* ";
            this.layoutControlGroup11.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem100
            // 
            this.layoutControlItem100.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem100.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem100.Control = this.flowLayoutPanel37;
            this.layoutControlItem100.CustomizationFormText = "平和质";
            this.layoutControlItem100.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem100.Name = "layoutControlItem100";
            this.layoutControlItem100.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem100.Text = "平和质";
            this.layoutControlItem100.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem100.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem100.TextToControlDistance = 5;
            // 
            // layoutControlItem101
            // 
            this.layoutControlItem101.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem101.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem101.Control = this.flowLayoutPanel38;
            this.layoutControlItem101.CustomizationFormText = "气虚质";
            this.layoutControlItem101.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem101.Name = "layoutControlItem101";
            this.layoutControlItem101.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem101.Text = "气虚质";
            this.layoutControlItem101.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem101.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem101.TextToControlDistance = 5;
            // 
            // layoutControlItem102
            // 
            this.layoutControlItem102.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem102.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem102.Control = this.flowLayoutPanel39;
            this.layoutControlItem102.CustomizationFormText = "阳虚质 ";
            this.layoutControlItem102.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem102.Name = "layoutControlItem102";
            this.layoutControlItem102.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem102.Text = "阳虚质 ";
            this.layoutControlItem102.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem102.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem102.TextToControlDistance = 5;
            // 
            // layoutControlItem103
            // 
            this.layoutControlItem103.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem103.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem103.Control = this.flowLayoutPanel40;
            this.layoutControlItem103.CustomizationFormText = "阴虚质";
            this.layoutControlItem103.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem103.Name = "layoutControlItem103";
            this.layoutControlItem103.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem103.Text = "阴虚质";
            this.layoutControlItem103.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem103.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem103.TextToControlDistance = 5;
            // 
            // layoutControlItem104
            // 
            this.layoutControlItem104.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem104.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem104.Control = this.flowLayoutPanel41;
            this.layoutControlItem104.CustomizationFormText = "痰湿质";
            this.layoutControlItem104.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem104.Name = "layoutControlItem104";
            this.layoutControlItem104.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem104.Text = "痰湿质";
            this.layoutControlItem104.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem104.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem104.TextToControlDistance = 5;
            // 
            // layoutControlItem105
            // 
            this.layoutControlItem105.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem105.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem105.Control = this.flowLayoutPanel42;
            this.layoutControlItem105.CustomizationFormText = "湿热质";
            this.layoutControlItem105.Location = new System.Drawing.Point(0, 146);
            this.layoutControlItem105.Name = "layoutControlItem105";
            this.layoutControlItem105.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem105.Text = "湿热质";
            this.layoutControlItem105.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem105.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem105.TextToControlDistance = 5;
            // 
            // layoutControlItem106
            // 
            this.layoutControlItem106.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem106.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem106.Control = this.flowLayoutPanel43;
            this.layoutControlItem106.CustomizationFormText = "血瘀质";
            this.layoutControlItem106.Location = new System.Drawing.Point(0, 170);
            this.layoutControlItem106.Name = "layoutControlItem106";
            this.layoutControlItem106.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem106.Text = "血瘀质";
            this.layoutControlItem106.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem106.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem106.TextToControlDistance = 5;
            // 
            // layoutControlItem107
            // 
            this.layoutControlItem107.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem107.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem107.Control = this.flowLayoutPanel44;
            this.layoutControlItem107.CustomizationFormText = "气郁质";
            this.layoutControlItem107.Location = new System.Drawing.Point(0, 194);
            this.layoutControlItem107.Name = "layoutControlItem107";
            this.layoutControlItem107.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem107.Text = "气郁质";
            this.layoutControlItem107.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem107.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem107.TextToControlDistance = 5;
            // 
            // layoutControlItem108
            // 
            this.layoutControlItem108.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem108.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem108.Control = this.flowLayoutPanel45;
            this.layoutControlItem108.CustomizationFormText = "特秉质";
            this.layoutControlItem108.Location = new System.Drawing.Point(0, 218);
            this.layoutControlItem108.Name = "layoutControlItem108";
            this.layoutControlItem108.Size = new System.Drawing.Size(761, 24);
            this.layoutControlItem108.Text = "特秉质";
            this.layoutControlItem108.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem108.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem108.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.layoutControl2;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(210, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(551, 26);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btn清空选择;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(99, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(111, 26);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.layoutControl3;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(99, 26);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup12.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlGroup12.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup12.AppearanceGroup.Options.UseForeColor = true;
            this.layoutControlGroup12.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup12.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup12.CustomizationFormText = "现存主要健康问题";
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.ExpandOnDoubleClick = true;
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl脑血管疾病,
            this.lbl肾脏疾病,
            this.lbl心血管疾病,
            this.lbl眼部疾病,
            this.lbl神经系统疾病,
            this.lbl其他系统疾病});
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 2837);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup12.Size = new System.Drawing.Size(777, 312);
            this.layoutControlGroup12.Text = "现存主要健康问题";
            // 
            // lbl脑血管疾病
            // 
            this.lbl脑血管疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl脑血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl脑血管疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl脑血管疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl脑血管疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl脑血管疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl脑血管疾病.Control = this.flow脑血管疾病;
            this.lbl脑血管疾病.CustomizationFormText = "脑血管疾病";
            this.lbl脑血管疾病.Location = new System.Drawing.Point(0, 0);
            this.lbl脑血管疾病.MinSize = new System.Drawing.Size(199, 50);
            this.lbl脑血管疾病.Name = "lbl脑血管疾病";
            this.lbl脑血管疾病.Size = new System.Drawing.Size(761, 50);
            this.lbl脑血管疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl脑血管疾病.Text = "脑血管疾病";
            this.lbl脑血管疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl脑血管疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl脑血管疾病.TextToControlDistance = 5;
            // 
            // lbl肾脏疾病
            // 
            this.lbl肾脏疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl肾脏疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl肾脏疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl肾脏疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl肾脏疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl肾脏疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl肾脏疾病.Control = this.flow肾脏疾病;
            this.lbl肾脏疾病.CustomizationFormText = "肾脏疾病";
            this.lbl肾脏疾病.Location = new System.Drawing.Point(0, 50);
            this.lbl肾脏疾病.MinSize = new System.Drawing.Size(199, 50);
            this.lbl肾脏疾病.Name = "lbl肾脏疾病";
            this.lbl肾脏疾病.Size = new System.Drawing.Size(761, 50);
            this.lbl肾脏疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl肾脏疾病.Text = "肾脏疾病";
            this.lbl肾脏疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl肾脏疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl肾脏疾病.TextToControlDistance = 5;
            // 
            // lbl心血管疾病
            // 
            this.lbl心血管疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl心血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl心血管疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl心血管疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl心血管疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl心血管疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl心血管疾病.Control = this.flow心血管疾病;
            this.lbl心血管疾病.CustomizationFormText = "心脏疾病";
            this.lbl心血管疾病.Location = new System.Drawing.Point(0, 100);
            this.lbl心血管疾病.MinSize = new System.Drawing.Size(199, 50);
            this.lbl心血管疾病.Name = "lbl心血管疾病";
            this.lbl心血管疾病.Size = new System.Drawing.Size(761, 50);
            this.lbl心血管疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl心血管疾病.Text = "心血管疾病";
            this.lbl心血管疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl心血管疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl心血管疾病.TextToControlDistance = 5;
            // 
            // lbl眼部疾病
            // 
            this.lbl眼部疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl眼部疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl眼部疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl眼部疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl眼部疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl眼部疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl眼部疾病.Control = this.flow眼部疾病;
            this.lbl眼部疾病.CustomizationFormText = "眼部疾病";
            this.lbl眼部疾病.Location = new System.Drawing.Point(0, 150);
            this.lbl眼部疾病.MinSize = new System.Drawing.Size(199, 26);
            this.lbl眼部疾病.Name = "lbl眼部疾病";
            this.lbl眼部疾病.Size = new System.Drawing.Size(761, 26);
            this.lbl眼部疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl眼部疾病.Text = "眼部疾病";
            this.lbl眼部疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl眼部疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl眼部疾病.TextToControlDistance = 5;
            // 
            // lbl神经系统疾病
            // 
            this.lbl神经系统疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl神经系统疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl神经系统疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl神经系统疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl神经系统疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl神经系统疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl神经系统疾病.Control = this.flow神经系统疾病;
            this.lbl神经系统疾病.CustomizationFormText = "神经系统疾病";
            this.lbl神经系统疾病.Location = new System.Drawing.Point(0, 176);
            this.lbl神经系统疾病.MinSize = new System.Drawing.Size(199, 50);
            this.lbl神经系统疾病.Name = "lbl神经系统疾病";
            this.lbl神经系统疾病.Size = new System.Drawing.Size(761, 50);
            this.lbl神经系统疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl神经系统疾病.Text = "神经系统疾病";
            this.lbl神经系统疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl神经系统疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl神经系统疾病.TextToControlDistance = 5;
            // 
            // lbl其他系统疾病
            // 
            this.lbl其他系统疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl其他系统疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl其他系统疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl其他系统疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl其他系统疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl其他系统疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl其他系统疾病.Control = this.flow其他系统疾病;
            this.lbl其他系统疾病.CustomizationFormText = "其他系统疾病";
            this.lbl其他系统疾病.Location = new System.Drawing.Point(0, 226);
            this.lbl其他系统疾病.MinSize = new System.Drawing.Size(199, 50);
            this.lbl其他系统疾病.Name = "lbl其他系统疾病";
            this.lbl其他系统疾病.Size = new System.Drawing.Size(761, 50);
            this.lbl其他系统疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl其他系统疾病.Text = "其他系统疾病";
            this.lbl其他系统疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl其他系统疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl其他系统疾病.TextToControlDistance = 5;
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup13.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup13.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup13.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup13.CustomizationFormText = "住院治疗情况 ";
            this.layoutControlGroup13.ExpandButtonVisible = true;
            this.layoutControlGroup13.ExpandOnDoubleClick = true;
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lblgc住院史,
            this.lbl住院史,
            this.layoutControlItem157,
            this.layoutControlItem159,
            this.lblgc家庭病床史,
            this.layoutControlItem156,
            this.layoutControlItem160,
            this.layoutControlItem161,
            this.emptySpaceItem27,
            this.emptySpaceItem28});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 3149);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup13.Size = new System.Drawing.Size(777, 274);
            this.layoutControlGroup13.Text = "住院治疗情况 ";
            // 
            // lblgc住院史
            // 
            this.lblgc住院史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lblgc住院史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblgc住院史.Control = this.gc住院史;
            this.lblgc住院史.CustomizationFormText = " ";
            this.lblgc住院史.Location = new System.Drawing.Point(0, 29);
            this.lblgc住院史.MaxSize = new System.Drawing.Size(700, 0);
            this.lblgc住院史.MinSize = new System.Drawing.Size(600, 90);
            this.lblgc住院史.Name = "lblgc住院史";
            this.lblgc住院史.Size = new System.Drawing.Size(761, 90);
            this.lblgc住院史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblgc住院史.Text = " ";
            this.lblgc住院史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lblgc住院史.TextSize = new System.Drawing.Size(90, 90);
            this.lblgc住院史.TextToControlDistance = 5;
            this.lblgc住院史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl住院史
            // 
            this.lbl住院史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl住院史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl住院史.Control = this.radio住院史;
            this.lbl住院史.CustomizationFormText = "住院史";
            this.lbl住院史.Location = new System.Drawing.Point(0, 0);
            this.lbl住院史.Name = "lbl住院史";
            this.lbl住院史.Size = new System.Drawing.Size(190, 29);
            this.lbl住院史.Text = "住院史";
            this.lbl住院史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl住院史.TextSize = new System.Drawing.Size(90, 20);
            this.lbl住院史.TextToControlDistance = 5;
            // 
            // layoutControlItem157
            // 
            this.layoutControlItem157.Control = this.btn住院史添加;
            this.layoutControlItem157.CustomizationFormText = "layoutControlItem157";
            this.layoutControlItem157.Location = new System.Drawing.Point(190, 0);
            this.layoutControlItem157.Name = "layoutControlItem157";
            this.layoutControlItem157.Size = new System.Drawing.Size(58, 29);
            this.layoutControlItem157.Text = "layoutControlItem157";
            this.layoutControlItem157.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem157.TextToControlDistance = 0;
            this.layoutControlItem157.TextVisible = false;
            // 
            // layoutControlItem159
            // 
            this.layoutControlItem159.Control = this.btn住院史删除;
            this.layoutControlItem159.CustomizationFormText = "layoutControlItem159";
            this.layoutControlItem159.Location = new System.Drawing.Point(248, 0);
            this.layoutControlItem159.Name = "layoutControlItem159";
            this.layoutControlItem159.Size = new System.Drawing.Size(94, 29);
            this.layoutControlItem159.Text = "layoutControlItem159";
            this.layoutControlItem159.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem159.TextToControlDistance = 0;
            this.layoutControlItem159.TextVisible = false;
            // 
            // lblgc家庭病床史
            // 
            this.lblgc家庭病床史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lblgc家庭病床史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblgc家庭病床史.Control = this.gc建床史;
            this.lblgc家庭病床史.CustomizationFormText = " ";
            this.lblgc家庭病床史.Location = new System.Drawing.Point(0, 148);
            this.lblgc家庭病床史.MaxSize = new System.Drawing.Size(700, 0);
            this.lblgc家庭病床史.MinSize = new System.Drawing.Size(600, 90);
            this.lblgc家庭病床史.Name = "lblgc家庭病床史";
            this.lblgc家庭病床史.Size = new System.Drawing.Size(761, 90);
            this.lblgc家庭病床史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblgc家庭病床史.Text = " ";
            this.lblgc家庭病床史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lblgc家庭病床史.TextSize = new System.Drawing.Size(90, 90);
            this.lblgc家庭病床史.TextToControlDistance = 5;
            this.lblgc家庭病床史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem156
            // 
            this.layoutControlItem156.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem156.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem156.Control = this.radio家庭病床史;
            this.layoutControlItem156.CustomizationFormText = "家庭病床史";
            this.layoutControlItem156.Location = new System.Drawing.Point(0, 119);
            this.layoutControlItem156.Name = "layoutControlItem156";
            this.layoutControlItem156.Size = new System.Drawing.Size(190, 29);
            this.layoutControlItem156.Text = "家庭病床史";
            this.layoutControlItem156.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem156.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem156.TextToControlDistance = 5;
            // 
            // layoutControlItem160
            // 
            this.layoutControlItem160.Control = this.btn病床史添加;
            this.layoutControlItem160.CustomizationFormText = "layoutControlItem160";
            this.layoutControlItem160.Location = new System.Drawing.Point(190, 119);
            this.layoutControlItem160.Name = "layoutControlItem160";
            this.layoutControlItem160.Size = new System.Drawing.Size(58, 29);
            this.layoutControlItem160.Text = "layoutControlItem160";
            this.layoutControlItem160.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem160.TextToControlDistance = 0;
            this.layoutControlItem160.TextVisible = false;
            // 
            // layoutControlItem161
            // 
            this.layoutControlItem161.Control = this.btn病床史删除;
            this.layoutControlItem161.CustomizationFormText = "layoutControlItem161";
            this.layoutControlItem161.Location = new System.Drawing.Point(248, 119);
            this.layoutControlItem161.Name = "layoutControlItem161";
            this.layoutControlItem161.Size = new System.Drawing.Size(94, 29);
            this.layoutControlItem161.Text = "layoutControlItem161";
            this.layoutControlItem161.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem161.TextToControlDistance = 0;
            this.layoutControlItem161.TextVisible = false;
            // 
            // emptySpaceItem27
            // 
            this.emptySpaceItem27.AllowHotTrack = false;
            this.emptySpaceItem27.CustomizationFormText = "emptySpaceItem27";
            this.emptySpaceItem27.Location = new System.Drawing.Point(342, 0);
            this.emptySpaceItem27.Name = "emptySpaceItem27";
            this.emptySpaceItem27.Size = new System.Drawing.Size(419, 29);
            this.emptySpaceItem27.Text = "emptySpaceItem27";
            this.emptySpaceItem27.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem28
            // 
            this.emptySpaceItem28.AllowHotTrack = false;
            this.emptySpaceItem28.CustomizationFormText = "emptySpaceItem28";
            this.emptySpaceItem28.Location = new System.Drawing.Point(342, 119);
            this.emptySpaceItem28.Name = "emptySpaceItem28";
            this.emptySpaceItem28.Size = new System.Drawing.Size(419, 29);
            this.emptySpaceItem28.Text = "emptySpaceItem28";
            this.emptySpaceItem28.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup14.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup14.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup14.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup14.CustomizationFormText = "主要用药情况";
            this.layoutControlGroup14.ExpandButtonVisible = true;
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem40,
            this.layoutControlItem116,
            this.layoutControlItem117,
            this.emptySpaceItem29,
            this.lblgc用药情况});
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 3423);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Size = new System.Drawing.Size(777, 163);
            this.layoutControlGroup14.Text = "主要用药情况";
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.radio主要用药情况;
            this.layoutControlItem40.CustomizationFormText = "主要用药情况";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(99, 29);
            this.layoutControlItem40.Text = "主要用药情况";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem40.TextToControlDistance = 0;
            this.layoutControlItem40.TextVisible = false;
            // 
            // layoutControlItem116
            // 
            this.layoutControlItem116.Control = this.btn删除用药情况;
            this.layoutControlItem116.CustomizationFormText = "layoutControlItem116";
            this.layoutControlItem116.Location = new System.Drawing.Point(183, 0);
            this.layoutControlItem116.Name = "layoutControlItem116";
            this.layoutControlItem116.Size = new System.Drawing.Size(140, 29);
            this.layoutControlItem116.Text = "layoutControlItem116";
            this.layoutControlItem116.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem116.TextToControlDistance = 0;
            this.layoutControlItem116.TextVisible = false;
            // 
            // layoutControlItem117
            // 
            this.layoutControlItem117.Control = this.btn添加用药情况;
            this.layoutControlItem117.CustomizationFormText = "layoutControlItem117";
            this.layoutControlItem117.Location = new System.Drawing.Point(99, 0);
            this.layoutControlItem117.Name = "layoutControlItem117";
            this.layoutControlItem117.Size = new System.Drawing.Size(84, 29);
            this.layoutControlItem117.Text = "layoutControlItem117";
            this.layoutControlItem117.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem117.TextToControlDistance = 0;
            this.layoutControlItem117.TextVisible = false;
            // 
            // emptySpaceItem29
            // 
            this.emptySpaceItem29.AllowHotTrack = false;
            this.emptySpaceItem29.CustomizationFormText = "emptySpaceItem29";
            this.emptySpaceItem29.Location = new System.Drawing.Point(323, 0);
            this.emptySpaceItem29.Name = "emptySpaceItem29";
            this.emptySpaceItem29.Size = new System.Drawing.Size(430, 29);
            this.emptySpaceItem29.Text = "emptySpaceItem29";
            this.emptySpaceItem29.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lblgc用药情况
            // 
            this.lblgc用药情况.Control = this.gc用药情况;
            this.lblgc用药情况.CustomizationFormText = " ";
            this.lblgc用药情况.Location = new System.Drawing.Point(0, 29);
            this.lblgc用药情况.MaxSize = new System.Drawing.Size(700, 0);
            this.lblgc用药情况.MinSize = new System.Drawing.Size(600, 90);
            this.lblgc用药情况.Name = "lblgc用药情况";
            this.lblgc用药情况.Size = new System.Drawing.Size(753, 90);
            this.lblgc用药情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblgc用药情况.Text = " ";
            this.lblgc用药情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lblgc用药情况.TextSize = new System.Drawing.Size(90, 20);
            this.lblgc用药情况.TextToControlDistance = 5;
            this.lblgc用药情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlGroup15
            // 
            this.layoutControlGroup15.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup15.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup15.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup15.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup15.CustomizationFormText = "非免疫规划预防接种史";
            this.layoutControlGroup15.ExpandButtonVisible = true;
            this.layoutControlGroup15.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem118,
            this.layoutControlItem162,
            this.layoutControlItem163,
            this.emptySpaceItem30,
            this.lblgc接种史});
            this.layoutControlGroup15.Location = new System.Drawing.Point(0, 3586);
            this.layoutControlGroup15.Name = "layoutControlGroup15";
            this.layoutControlGroup15.Size = new System.Drawing.Size(777, 162);
            this.layoutControlGroup15.Text = "非免疫规划预防接种史";
            // 
            // layoutControlItem118
            // 
            this.layoutControlItem118.Control = this.radio预防接种史;
            this.layoutControlItem118.CustomizationFormText = "layoutControlItem118";
            this.layoutControlItem118.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem118.Name = "layoutControlItem118";
            this.layoutControlItem118.Size = new System.Drawing.Size(100, 29);
            this.layoutControlItem118.Text = "layoutControlItem118";
            this.layoutControlItem118.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem118.TextToControlDistance = 0;
            this.layoutControlItem118.TextVisible = false;
            // 
            // layoutControlItem162
            // 
            this.layoutControlItem162.Control = this.btn预防接种史添加;
            this.layoutControlItem162.CustomizationFormText = "layoutControlItem162";
            this.layoutControlItem162.Location = new System.Drawing.Point(100, 0);
            this.layoutControlItem162.Name = "layoutControlItem162";
            this.layoutControlItem162.Size = new System.Drawing.Size(77, 29);
            this.layoutControlItem162.Text = "layoutControlItem162";
            this.layoutControlItem162.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem162.TextToControlDistance = 0;
            this.layoutControlItem162.TextVisible = false;
            // 
            // layoutControlItem163
            // 
            this.layoutControlItem163.Control = this.btn预防接种史删除;
            this.layoutControlItem163.CustomizationFormText = "layoutControlItem163";
            this.layoutControlItem163.Location = new System.Drawing.Point(177, 0);
            this.layoutControlItem163.Name = "layoutControlItem163";
            this.layoutControlItem163.Size = new System.Drawing.Size(148, 29);
            this.layoutControlItem163.Text = "layoutControlItem163";
            this.layoutControlItem163.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem163.TextToControlDistance = 0;
            this.layoutControlItem163.TextVisible = false;
            // 
            // emptySpaceItem30
            // 
            this.emptySpaceItem30.AllowHotTrack = false;
            this.emptySpaceItem30.CustomizationFormText = "emptySpaceItem30";
            this.emptySpaceItem30.Location = new System.Drawing.Point(325, 0);
            this.emptySpaceItem30.Name = "emptySpaceItem30";
            this.emptySpaceItem30.Size = new System.Drawing.Size(428, 29);
            this.emptySpaceItem30.Text = "emptySpaceItem30";
            this.emptySpaceItem30.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lblgc接种史
            // 
            this.lblgc接种史.Control = this.gc接种史;
            this.lblgc接种史.CustomizationFormText = " ";
            this.lblgc接种史.Location = new System.Drawing.Point(0, 29);
            this.lblgc接种史.MaxSize = new System.Drawing.Size(700, 0);
            this.lblgc接种史.MinSize = new System.Drawing.Size(199, 90);
            this.lblgc接种史.Name = "lblgc接种史";
            this.lblgc接种史.Size = new System.Drawing.Size(753, 90);
            this.lblgc接种史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblgc接种史.Text = " ";
            this.lblgc接种史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lblgc接种史.TextSize = new System.Drawing.Size(90, 20);
            this.lblgc接种史.TextToControlDistance = 5;
            this.lblgc接种史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // group健康评价
            // 
            this.group健康评价.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.group健康评价.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.group健康评价.AppearanceGroup.Options.UseFont = true;
            this.group健康评价.AppearanceGroup.Options.UseForeColor = true;
            this.group健康评价.AppearanceGroup.Options.UseTextOptions = true;
            this.group健康评价.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.group健康评价.CustomizationFormText = "健康评价";
            this.group健康评价.ExpandButtonVisible = true;
            this.group健康评价.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem120});
            this.group健康评价.Location = new System.Drawing.Point(0, 3748);
            this.group健康评价.Name = "group健康评价";
            this.group健康评价.Size = new System.Drawing.Size(777, 104);
            this.group健康评价.Text = "健康评价";
            // 
            // layoutControlItem120
            // 
            this.layoutControlItem120.Control = this.panelControl5;
            this.layoutControlItem120.CustomizationFormText = "健康评价";
            this.layoutControlItem120.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem120.MinSize = new System.Drawing.Size(215, 60);
            this.layoutControlItem120.Name = "layoutControlItem120";
            this.layoutControlItem120.Size = new System.Drawing.Size(753, 60);
            this.layoutControlItem120.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem120.Text = "健康评价";
            this.layoutControlItem120.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem120.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem120.TextToControlDistance = 0;
            this.layoutControlItem120.TextVisible = false;
            // 
            // group健康指导
            // 
            this.group健康指导.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.group健康指导.AppearanceGroup.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.group健康指导.AppearanceGroup.Options.UseFont = true;
            this.group健康指导.AppearanceGroup.Options.UseForeColor = true;
            this.group健康指导.AppearanceGroup.Options.UseTextOptions = true;
            this.group健康指导.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.group健康指导.CustomizationFormText = "健康指导";
            this.group健康指导.ExpandButtonVisible = true;
            this.group健康指导.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem121,
            this.layoutControlItem12});
            this.group健康指导.Location = new System.Drawing.Point(0, 3852);
            this.group健康指导.Name = "group健康指导";
            this.group健康指导.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.group健康指导.Size = new System.Drawing.Size(777, 146);
            this.group健康指导.Text = "健康指导";
            // 
            // layoutControlItem121
            // 
            this.layoutControlItem121.Control = this.panelControl6;
            this.layoutControlItem121.CustomizationFormText = "健康指导";
            this.layoutControlItem121.Location = new System.Drawing.Point(136, 0);
            this.layoutControlItem121.MaxSize = new System.Drawing.Size(0, 110);
            this.layoutControlItem121.MinSize = new System.Drawing.Size(189, 110);
            this.layoutControlItem121.Name = "layoutControlItem121";
            this.layoutControlItem121.Size = new System.Drawing.Size(625, 110);
            this.layoutControlItem121.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem121.Text = "健康指导";
            this.layoutControlItem121.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem121.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem121.TextToControlDistance = 0;
            this.layoutControlItem121.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.flow健康指导;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 110);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(136, 110);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.BackColor = System.Drawing.Color.White;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(218, 74);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(106, 24);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem73
            // 
            this.layoutControlItem73.Control = this.flowLayoutPanel28;
            this.layoutControlItem73.CustomizationFormText = "其他";
            this.layoutControlItem73.Location = new System.Drawing.Point(65, 1637);
            this.layoutControlItem73.Name = "layoutControlItem69";
            this.layoutControlItem73.Size = new System.Drawing.Size(692, 24);
            this.layoutControlItem73.Text = "其他";
            this.layoutControlItem73.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem73.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem73.TextToControlDistance = 5;
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.flowLayoutPanel53);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(0, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(800, 33);
            this.panelControl7.TabIndex = 1;
            // 
            // flowLayoutPanel53
            // 
            this.flowLayoutPanel53.Controls.Add(this.btn保存);
            this.flowLayoutPanel53.Controls.Add(this.simpleButton9);
            this.flowLayoutPanel53.Controls.Add(this.btn读取化验信息);
            this.flowLayoutPanel53.Controls.Add(this.btn读取住院信息);
            this.flowLayoutPanel53.Controls.Add(this.btn读取用药信息);
            this.flowLayoutPanel53.Controls.Add(this.btn生成健康评价);
            this.flowLayoutPanel53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel53.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel53.Name = "flowLayoutPanel53";
            this.flowLayoutPanel53.Size = new System.Drawing.Size(796, 29);
            this.flowLayoutPanel53.TabIndex = 5;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(60, 24);
            this.btn保存.TabIndex = 3;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // simpleButton9
            // 
            this.simpleButton9.Location = new System.Drawing.Point(69, 3);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(60, 24);
            this.simpleButton9.TabIndex = 0;
            this.simpleButton9.Text = "填表说明";
            // 
            // btn读取化验信息
            // 
            this.btn读取化验信息.Image = ((System.Drawing.Image)(resources.GetObject("btn读取化验信息.Image")));
            this.btn读取化验信息.Location = new System.Drawing.Point(135, 3);
            this.btn读取化验信息.Name = "btn读取化验信息";
            this.btn读取化验信息.Size = new System.Drawing.Size(125, 24);
            this.btn读取化验信息.TabIndex = 139;
            this.btn读取化验信息.Text = "读取近期化验信息";
            this.btn读取化验信息.Click += new System.EventHandler(this.btn读取化验信息_Click);
            // 
            // btn读取住院信息
            // 
            this.btn读取住院信息.Image = ((System.Drawing.Image)(resources.GetObject("btn读取住院信息.Image")));
            this.btn读取住院信息.Location = new System.Drawing.Point(266, 3);
            this.btn读取住院信息.Name = "btn读取住院信息";
            this.btn读取住院信息.Size = new System.Drawing.Size(125, 24);
            this.btn读取住院信息.TabIndex = 140;
            this.btn读取住院信息.Text = "读取近期住院情况";
            this.btn读取住院信息.Click += new System.EventHandler(this.btn读取住院信息_Click);
            // 
            // btn读取用药信息
            // 
            this.btn读取用药信息.Image = ((System.Drawing.Image)(resources.GetObject("btn读取用药信息.Image")));
            this.btn读取用药信息.Location = new System.Drawing.Point(397, 3);
            this.btn读取用药信息.Name = "btn读取用药信息";
            this.btn读取用药信息.Size = new System.Drawing.Size(125, 24);
            this.btn读取用药信息.TabIndex = 141;
            this.btn读取用药信息.Text = "读取近期用药情况";
            this.btn读取用药信息.Visible = false;
            this.btn读取用药信息.Click += new System.EventHandler(this.btn读取用药信息_Click);
            // 
            // btn生成健康评价
            // 
            this.btn生成健康评价.Image = ((System.Drawing.Image)(resources.GetObject("btn生成健康评价.Image")));
            this.btn生成健康评价.Location = new System.Drawing.Point(528, 3);
            this.btn生成健康评价.Name = "btn生成健康评价";
            this.btn生成健康评价.Size = new System.Drawing.Size(108, 24);
            this.btn生成健康评价.TabIndex = 142;
            this.btn生成健康评价.Text = "生成健康评价";
            this.btn生成健康评价.Click += new System.EventHandler(this.btn生成健康评价_Click);
            // 
            // UC健康体检表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl7);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UC健康体检表";
            this.Size = new System.Drawing.Size(800, 498);
            this.Load += new System.EventHandler(this.UC健康体检表_Load);
            this.Controls.SetChildIndex(this.panelControl7, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio其他B超.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他B超异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboABO.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc接种史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv接种史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte接种日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte接种日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp服药依从性)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte用药时间.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte用药时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio预防接种史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc建床史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv建床史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte建床日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte建床日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte撤床日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte撤床日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio家庭病床史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio主要用药情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte入院日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte入院日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出院日期.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出院日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio住院史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt齿列_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_义齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_龋齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_缺齿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk齿列_正常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他防护.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学因素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理因素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio职业病其他防护措施有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio化学防护措施有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio物理防护措施有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio放射物质防护措施有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio粉尘防护措施有无.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio职业病有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt工种.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt从业时间.Properties)).EndInit();
            this.flow老年人认知能力.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人认知能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人认知能力总分.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人生活自理能力评估.Properties)).EndInit();
            this.flow老年人情感状态.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人情感状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人情感状态总分.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio老年人健康状态评估.Properties)).EndInit();
            this.flow健康指导.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk纳入慢性病管理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk建议复查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk定期随访.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk建议转诊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl6)).EndInit();
            this.panelControl6.ResumeLayout(false);
            this.panelControl6.PerformLayout();
            this.flow危险因素控制.ResumeLayout(false);
            this.flow危险因素控制.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk戒烟.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk健康饮酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk锻炼.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk减体重.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt减体重目标.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk建议疫苗接种.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk接种疫苗.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt建议疫苗接种.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk危险因素其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk低盐饮食.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt危险因素其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检异常4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检异常3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检异常2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检异常1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio健康评价.Properties)).EndInit();
            this.flow脑血管疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_缺血性卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_脑出血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_蛛网膜下腔出血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_短暂性脑缺血发作.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk脑血管疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑血管疾病_其他.Properties)).EndInit();
            this.flow其他系统疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_慢性支气管炎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_肺气肿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_恶性肿瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_老年性骨关节病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk其他系统疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他系统疾病_其他.Properties)).EndInit();
            this.flow神经系统疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_老年性痴呆.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_帕金森病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk神经系统疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt神经系统疾病_其他.Properties)).EndInit();
            this.flowLayoutPanel45.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio特秉质.Properties)).EndInit();
            this.flowLayoutPanel44.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio气郁质.Properties)).EndInit();
            this.flow眼部疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_视网膜出血或渗出.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_视乳头水肿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_白内障.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk眼部疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼部疾病_其他.Properties)).EndInit();
            this.flow血管疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk血管疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk血管疾病_夹层动脉瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk血管疾病_动脉闭塞性疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk血管疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血管疾病_其他.Properties)).EndInit();
            this.flow心血管疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_心肌梗死.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_心绞痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_冠状动脉血运重建.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_充血性心力衰竭.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_心前区疼痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_夹层动脉瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_动脉闭塞性疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心血管疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心血管疾病_其他.Properties)).EndInit();
            this.flow肾脏疾病.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_未发现.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_糖尿病肾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_肾功能衰竭.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_急性肾炎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_慢性肾炎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肾脏疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肾脏疾病_其他.Properties)).EndInit();
            this.flowLayoutPanel43.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio血瘀质.Properties)).EndInit();
            this.flowLayoutPanel42.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio湿热质.Properties)).EndInit();
            this.flowLayoutPanel41.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio痰湿质.Properties)).EndInit();
            this.flowLayoutPanel40.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio阴虚质.Properties)).EndInit();
            this.flowLayoutPanel39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio阳虚质.Properties)).EndInit();
            this.flowLayoutPanel38.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio气虚质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查_其他.Properties)).EndInit();
            this.flowLayoutPanel37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio平和质.Properties)).EndInit();
            this.flowLayoutPanel36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio宫颈涂片.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫颈涂片.Properties)).EndInit();
            this.flowLayoutPanel35.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio腹部B超.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt腹部B超异常.Properties)).EndInit();
            this.flowLayoutPanel34.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio胸部X线片.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胸部X线片.Properties)).EndInit();
            this.flowLayoutPanel33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio乙型肝炎表面抗原.Properties)).EndInit();
            this.flowLayoutPanel32.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio大便潜血.Properties)).EndInit();
            this.flow心电图.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_正常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_改变.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_心肌梗塞.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_心动过速.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_心动过缓.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_早搏.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_房颤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_房室传导阻滞.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk心电图_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电图_其他.Properties)).EndInit();
            this.flowLayoutPanel30.ResumeLayout(false);
            this.flowLayoutPanel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿蛋白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿糖.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿胴体.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿潜血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿常规其他.Properties)).EndInit();
            this.flowLayoutPanel29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt血常规_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio足背动脉搏动.Properties)).EndInit();
            this.flowLayoutPanel28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt查体_其他.Properties)).EndInit();
            this.flowLayoutPanel27.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio附件.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt附件.Properties)).EndInit();
            this.flowLayoutPanel26.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio宫体.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫体.Properties)).EndInit();
            this.flowLayoutPanel25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio宫颈.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫颈.Properties)).EndInit();
            this.flowLayoutPanel24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio阴道.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阴道.Properties)).EndInit();
            this.flowLayoutPanel23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio外阴.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外阴.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio下肢水肿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio心律.Properties)).EndInit();
            this.flowLayoutPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio移动性浊音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt移动性浊音.Properties)).EndInit();
            this.flow肛门指诊.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk肛门指诊_未见异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肛门指诊_触痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肛门指诊_包块.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肛门指诊_前列腺异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk肛门指诊_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肛门指诊_其他.Properties)).EndInit();
            this.flow乳腺.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk乳腺_未见异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk乳腺_乳房切除.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk乳腺_异常泌乳.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk乳腺_乳腺包块.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk乳腺_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳腺_其他.Properties)).EndInit();
            this.flowLayoutPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio脾大.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脾大.Properties)).EndInit();
            this.flowLayoutPanel18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio肝大.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肝大.Properties)).EndInit();
            this.flowLayoutPanel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio包块.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt包块.Properties)).EndInit();
            this.flowLayoutPanel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio压痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt压痛.Properties)).EndInit();
            this.flowLayoutPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio杂音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt杂音.Properties)).EndInit();
            this.flowLayoutPanel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio呼吸音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt呼吸音_异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio桶状胸.Properties)).EndInit();
            this.flow巩膜.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_正常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_黄染.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_充血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk巩膜_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt巩膜_其他.Properties)).EndInit();
            this.flow皮肤.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_正常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_潮红.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_苍白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_发绀.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_黄染.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_色素沉着.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk皮肤_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt皮肤_其他.Properties)).EndInit();
            this.flow罗音.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_干罗音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_湿罗音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk罗音_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt罗音_其他.Properties)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio眼底.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼底.Properties)).EndInit();
            this.flow听力.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio听力_听见.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio听力_听不清.Properties)).EndInit();
            this.flow淋巴结.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_未触及.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_锁骨上.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_腋窝.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk淋巴结_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt淋巴结_其他.Properties)).EndInit();
            this.flow运动功能.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio运动能力_可顺利完成.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio运动能力_无法完成.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt左眼视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右眼视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt矫正左眼视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt矫正右眼视力.Properties)).EndInit();
            this.flow咽部.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_无充血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_充血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_淋巴滤泡增生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio咽部_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt咽部_其他.Properties)).EndInit();
            this.flow口唇.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radio口唇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt口唇其他.Properties)).EndInit();
            this.flow饮酒种类.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_白酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_啤酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_红酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_黄酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮酒种类_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒种类_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio近一年内是否曾醉酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio是否戒酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio饮酒频率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio吸烟情况.Properties)).EndInit();
            this.flow饮食习惯.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_荤素均衡.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_荤食为主.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_素食为主.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜盐.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜油.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮食习惯_嗜糖.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt生活方式_锻炼方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio生活方式_锻炼频率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt一般情况_血压右侧原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt一般情况_血压左侧原因.Properties)).EndInit();
            this.flow症状.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_无症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_头痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_头晕.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_心悸.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_胸闷.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_胸痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_慢性咳嗽.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_咳痰.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_呼吸困难.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_多饮.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_多尿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_体重下降.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_乏力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_关节肿痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_视力模糊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_手脚麻木.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_尿急.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_尿痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_便秘.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_腹泻.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_恶心呕吐.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_眼花.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_耳鸣.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_乳房胀痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk症状_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.text责任医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.text居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.text联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.text出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.text身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.text姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.text个人档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte体检日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte创建时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血管疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体检日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl责任医生)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体温)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl呼吸频率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl身高)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl腰围)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重指数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脉率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压左侧)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压右侧)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group老年人评估)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem128)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group症状)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem127)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem123)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮食习惯)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl锻炼频率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl吸烟状况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮酒情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl放射物质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl物理因素)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem135)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病其他)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem138)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem136)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem131)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl防护措施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem139)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem137)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem133)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem134)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem129)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl粉尘)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem130)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem132)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病危害因素接触史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl视力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl听力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl运动能力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem143)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem140)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem144)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem146)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem145)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem147)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem141)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem148)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem149)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem150)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem151)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem152)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem155)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem142)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem158)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl齿列总)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl咽部)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem153)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem154)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl皮肤)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl巩膜)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl淋巴结)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl罗音)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group妇科)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group辅助查体)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清谷丙转氨酶)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清谷草转氨酶)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl总胆红素)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清肌酐)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血尿素氮)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem90)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl总胆固醇)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl甘油三酯)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清低密度脂蛋白胆固醇)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清高密度脂蛋白胆固醇)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem96)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem98)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem99)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl腹部B超)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他B超)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem100)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem101)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem102)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem103)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem104)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem105)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem106)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem107)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem108)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脑血管疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肾脏疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心血管疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl眼部疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl神经系统疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他系统疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem157)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem159)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc家庭病床史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem156)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem160)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem161)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem116)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem117)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem118)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem162)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem163)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblgc接种史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group健康评价)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem120)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group健康指导)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem121)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.flowLayoutPanel53.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit text姓名;
        private DevExpress.XtraEditors.TextEdit text个人档案编号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit text居住地址;
        private DevExpress.XtraEditors.TextEdit text联系电话;
        private DevExpress.XtraEditors.TextEdit text出生日期;
        private DevExpress.XtraEditors.TextEdit text身份证号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.TextEdit text责任医生;
        private DevExpress.XtraEditors.DateEdit dte体检日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl体检日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl责任医生;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private System.Windows.Forms.FlowLayoutPanel flow症状;
        private DevExpress.XtraEditors.CheckEdit chk症状_无症状;
        private DevExpress.XtraEditors.CheckEdit chk症状_头痛;
        private DevExpress.XtraEditors.CheckEdit chk症状_头晕;
        private DevExpress.XtraEditors.CheckEdit chk症状_心悸;
        private DevExpress.XtraEditors.CheckEdit chk症状_胸闷;
        private DevExpress.XtraEditors.CheckEdit chk症状_胸痛;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.CheckEdit chk症状_慢性咳嗽;
        private DevExpress.XtraEditors.CheckEdit chk症状_咳痰;
        private DevExpress.XtraEditors.CheckEdit chk症状_呼吸困难;
        private DevExpress.XtraEditors.CheckEdit chk症状_多饮;
        private DevExpress.XtraEditors.CheckEdit chk症状_多尿;
        private DevExpress.XtraEditors.CheckEdit chk症状_体重下降;
        private DevExpress.XtraEditors.CheckEdit chk症状_乏力;
        private DevExpress.XtraEditors.CheckEdit chk症状_关节肿痛;
        private DevExpress.XtraEditors.CheckEdit chk症状_视力模糊;
        private DevExpress.XtraEditors.CheckEdit chk症状_手脚麻木;
        private DevExpress.XtraEditors.CheckEdit chk症状_尿急;
        private DevExpress.XtraEditors.CheckEdit chk症状_尿痛;
        private DevExpress.XtraEditors.CheckEdit chk症状_便秘;
        private DevExpress.XtraEditors.CheckEdit chk症状_腹泻;
        private DevExpress.XtraEditors.CheckEdit chk症状_恶心呕吐;
        private DevExpress.XtraEditors.CheckEdit chk症状_眼花;
        private DevExpress.XtraEditors.CheckEdit chk症状_耳鸣;
        private DevExpress.XtraEditors.CheckEdit chk症状_乳房胀痛;
        private DevExpress.XtraEditors.CheckEdit chk症状_其他;
        private DevExpress.XtraEditors.TextEdit txt症状_其他;
        private UCTxtLbl txt一般情况_体温;
        private DevExpress.XtraLayout.LayoutControlItem lbl体温;
        private UCTxtLbl txt一般情况_脉率;
        private DevExpress.XtraLayout.LayoutControlItem lbl脉率;
        private UCTxtLbl txt一般情况_呼吸频率;
        private DevExpress.XtraLayout.LayoutControlItem lbl呼吸频率;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem4;
        private DevExpress.XtraEditors.TextEdit txt一般情况_血压右侧原因;
        private DevExpress.XtraEditors.TextEdit txt一般情况_血压左侧原因;
        private UCTxtLblTxtLbl txt一般情况_血压右侧;
        private UCTxtLblTxtLbl txt一般情况_血压左侧;
        private DevExpress.XtraLayout.LayoutControlItem lbl血压左侧;
        private DevExpress.XtraLayout.LayoutControlItem lbl血压右侧;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private UCTxtLbl txt一般情况_体重;
        private UCTxtLbl txt一般情况_身高;
        private DevExpress.XtraLayout.LayoutControlItem lbl身高;
        private DevExpress.XtraLayout.LayoutControlItem lbl体重;
        private UCTxtLbl txt一般情况_体重指数;
        private UCTxtLbl txt一般情况_腰围;
        private DevExpress.XtraLayout.LayoutControlItem lbl腰围;
        private DevExpress.XtraLayout.LayoutControlItem lbl体重指数;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.RadioGroup radio生活方式_锻炼频率;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.EmptySpaceItem lbl锻炼频率;
        private DevExpress.XtraEditors.TextEdit txt生活方式_锻炼方式;
        private UCTxtLbl txt生活方式_坚持锻炼时间;
        private UCTxtLbl txt生活方式_每次锻炼时间;
        private System.Windows.Forms.FlowLayoutPanel flow饮食习惯;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_荤素均衡;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_荤食为主;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_素食为主;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_嗜盐;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_嗜油;
        private DevExpress.XtraEditors.CheckEdit chk饮食习惯_嗜糖;
        private DevExpress.XtraLayout.LayoutControlItem lbl饮食习惯;
        private DevExpress.XtraLayout.EmptySpaceItem lbl吸烟状况;
        private UCLblTxtLbl txt日吸烟量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private UCTxtLbl txt开始吸烟年龄;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private UCTxtLbl txt戒烟年龄;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraEditors.RadioGroup radio吸烟情况;
        private DevExpress.XtraLayout.EmptySpaceItem lbl饮酒情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.RadioGroup radio饮酒频率;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraEditors.RadioGroup radio是否戒酒;
        private UCLblTxtLbl txt日饮酒量;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private UCTxtLbl txt戒酒年龄;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraEditors.RadioGroup radio近一年内是否曾醉酒;
        private UCTxtLbl txt开始饮酒年龄;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private System.Windows.Forms.FlowLayoutPanel flow饮酒种类;
        private DevExpress.XtraEditors.CheckEdit chk饮酒种类_白酒;
        private DevExpress.XtraEditors.CheckEdit chk饮酒种类_啤酒;
        private DevExpress.XtraEditors.CheckEdit chk饮酒种类_红酒;
        private DevExpress.XtraEditors.CheckEdit chk饮酒种类_黄酒;
        private DevExpress.XtraEditors.TextEdit txt饮酒种类_其他;
        private DevExpress.XtraEditors.CheckEdit chk饮酒种类_其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private System.Windows.Forms.FlowLayoutPanel flow口唇;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraEditors.RadioGroup radio口唇;
        private DevExpress.XtraEditors.TextEdit txt口唇其他;
        private System.Windows.Forms.FlowLayoutPanel flow咽部;
        private DevExpress.XtraEditors.CheckEdit radio咽部_无充血;
        private DevExpress.XtraEditors.CheckEdit radio咽部_充血;
        private DevExpress.XtraEditors.CheckEdit radio咽部_淋巴滤泡增生;
        private DevExpress.XtraEditors.CheckEdit radio咽部_其他;
        private DevExpress.XtraEditors.TextEdit txt咽部_其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl咽部;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraLayout.LayoutControlItem lbl视力;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txt左眼视力;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt右眼视力;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txt矫正左眼视力;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txt矫正右眼视力;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.FlowLayoutPanel flow运动功能;
        private DevExpress.XtraLayout.LayoutControlItem lbl运动能力;
        private DevExpress.XtraEditors.CheckEdit radio运动能力_可顺利完成;
        private DevExpress.XtraEditors.CheckEdit radio运动能力_无法完成;
        private System.Windows.Forms.FlowLayoutPanel flow听力;
        private DevExpress.XtraLayout.LayoutControlItem lbl听力;
        private DevExpress.XtraEditors.CheckEdit radio听力_听见;
        private DevExpress.XtraEditors.CheckEdit radio听力_听不清;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private DevExpress.XtraEditors.RadioGroup radio眼底;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraEditors.TextEdit txt眼底;
        private System.Windows.Forms.FlowLayoutPanel flow皮肤;
        private DevExpress.XtraLayout.LayoutControlItem lbl皮肤;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_正常;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_潮红;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_苍白;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_发绀;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_黄染;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_色素沉着;
        private DevExpress.XtraEditors.CheckEdit chk皮肤_其他;
        private DevExpress.XtraEditors.TextEdit txt皮肤_其他;
        private System.Windows.Forms.FlowLayoutPanel flow巩膜;
        private DevExpress.XtraLayout.LayoutControlItem lbl巩膜;
        private DevExpress.XtraEditors.CheckEdit chk巩膜_正常;
        private DevExpress.XtraEditors.CheckEdit chk巩膜_黄染;
        private DevExpress.XtraEditors.CheckEdit chk巩膜_充血;
        private DevExpress.XtraEditors.CheckEdit chk巩膜_其他;
        private DevExpress.XtraEditors.TextEdit txt巩膜_其他;
        private System.Windows.Forms.FlowLayoutPanel flow淋巴结;
        private DevExpress.XtraEditors.CheckEdit chk淋巴结_未触及;
        private DevExpress.XtraEditors.CheckEdit chk淋巴结_锁骨上;
        private DevExpress.XtraEditors.CheckEdit chk淋巴结_腋窝;
        private DevExpress.XtraEditors.CheckEdit chk淋巴结_其他;
        private DevExpress.XtraEditors.TextEdit txt淋巴结_其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl淋巴结;
        private DevExpress.XtraEditors.RadioGroup radio桶状胸;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private DevExpress.XtraEditors.RadioGroup radio呼吸音;
        private DevExpress.XtraEditors.TextEdit txt呼吸音_异常;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private System.Windows.Forms.FlowLayoutPanel flow罗音;
        private DevExpress.XtraEditors.CheckEdit chk罗音_无;
        private DevExpress.XtraEditors.CheckEdit chk罗音_干罗音;
        private DevExpress.XtraEditors.CheckEdit chk罗音_湿罗音;
        private DevExpress.XtraEditors.CheckEdit chk罗音_其他;
        private DevExpress.XtraEditors.TextEdit txt罗音_其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl罗音;
        private DevExpress.XtraEditors.RadioGroup radio心律;
        private UCTxtLbl txt心率;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlItem lbl心率;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private DevExpress.XtraEditors.RadioGroup radio杂音;
        private DevExpress.XtraEditors.TextEdit txt杂音;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private DevExpress.XtraEditors.RadioGroup radio压痛;
        private DevExpress.XtraEditors.TextEdit txt压痛;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel20;
        private DevExpress.XtraEditors.TextEdit txt移动性浊音;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel19;
        private DevExpress.XtraEditors.TextEdit txt脾大;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel18;
        private DevExpress.XtraEditors.TextEdit txt肝大;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel17;
        private DevExpress.XtraEditors.RadioGroup radio包块;
        private DevExpress.XtraEditors.TextEdit txt包块;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraEditors.RadioGroup radio下肢水肿;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraEditors.RadioGroup radio足背动脉搏动;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private System.Windows.Forms.FlowLayoutPanel flow肛门指诊;
        private DevExpress.XtraEditors.CheckEdit chk肛门指诊_未见异常;
        private DevExpress.XtraEditors.CheckEdit chk肛门指诊_触痛;
        private DevExpress.XtraEditors.CheckEdit chk肛门指诊_包块;
        private DevExpress.XtraEditors.CheckEdit chk肛门指诊_前列腺异常;
        private DevExpress.XtraEditors.CheckEdit chk肛门指诊_其他;
        private DevExpress.XtraEditors.TextEdit txt肛门指诊_其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private System.Windows.Forms.FlowLayoutPanel flow乳腺;
        private DevExpress.XtraEditors.CheckEdit chk乳腺_未见异常;
        private DevExpress.XtraEditors.CheckEdit chk乳腺_乳房切除;
        private DevExpress.XtraEditors.CheckEdit chk乳腺_异常泌乳;
        private DevExpress.XtraEditors.CheckEdit chk乳腺_乳腺包块;
        private DevExpress.XtraEditors.CheckEdit chk乳腺_其他;
        private DevExpress.XtraEditors.TextEdit txt乳腺_其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel23;
        private DevExpress.XtraEditors.RadioGroup radio外阴;
        private DevExpress.XtraEditors.TextEdit txt外阴;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem64;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel28;
        private DevExpress.XtraEditors.TextEdit txt查体_其他;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel27;
        private DevExpress.XtraEditors.TextEdit txt附件;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel26;
        private DevExpress.XtraEditors.TextEdit txt宫体;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel25;
        private DevExpress.XtraEditors.TextEdit txt宫颈;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel24;
        private DevExpress.XtraEditors.TextEdit txt阴道;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem65;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem66;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem67;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem69;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem68;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel29;
        private DevExpress.XtraEditors.TextEdit txt血常规_其他;
        private UCTxtLbl txt白细胞;
        private UCTxtLbl txt血小板;
        private UCTxtLbl txt血红蛋白;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem70;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem71;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem72;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem74;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem73;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel30;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txt尿蛋白;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txt尿糖;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txt尿胴体;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txt尿潜血;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem75;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txt尿常规其他;
        private UCTxtLbl txt尿微量白蛋白;
        private System.Windows.Forms.FlowLayoutPanel flow心电图;
        private DevExpress.XtraEditors.TextEdit txt心电图_其他;
        private UCTxtLbl txt空腹血糖;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem76;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem78;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem79;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel32;
        private DevExpress.XtraEditors.RadioGroup radio大便潜血;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem80;
        private UCTxtLbl txt血清谷草转氨酶;
        private UCTxtLbl txt白蛋白;
        private UCTxtLbl txt总胆红素;
        private UCTxtLbl txt结合胆红素;
        private UCTxtLbl txt血清谷丙转氨酶;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel33;
        private UCTxtLbl txt糖化血红蛋白;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem81;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem82;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlItem lbl血清谷丙转氨酶;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem84;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem86;
        private DevExpress.XtraLayout.LayoutControlItem lbl血清谷草转氨酶;
        private DevExpress.XtraLayout.LayoutControlItem lbl总胆红素;
        private UCTxtLbl txt血清高密度脂蛋白胆固醇;
        private UCTxtLbl txt甘油三酯;
        private UCTxtLbl txt血清低密度脂蛋白胆固醇;
        private UCTxtLbl txt总胆固醇;
        private UCTxtLbl txt血钾浓度;
        private UCTxtLbl txt血钠浓度;
        private UCTxtLbl txt血尿素氮;
        private UCTxtLbl txt血清肌酐;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.LayoutControlItem lbl血清肌酐;
        private DevExpress.XtraLayout.LayoutControlItem lbl血尿素氮;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem90;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem91;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.LayoutControlItem lbl总胆固醇;
        private DevExpress.XtraLayout.LayoutControlItem lbl血清低密度脂蛋白胆固醇;
        private DevExpress.XtraLayout.LayoutControlItem lbl甘油三酯;
        private DevExpress.XtraLayout.LayoutControlItem lbl血清高密度脂蛋白胆固醇;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel34;
        private DevExpress.XtraEditors.TextEdit txt胸部X线片;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem96;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel35;
        private DevExpress.XtraEditors.TextEdit txt腹部B超异常;
        private DevExpress.XtraLayout.LayoutControlItem lbl腹部B超;
        private DevExpress.XtraEditors.TextEdit txt辅助检查_其他;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel37;
        private DevExpress.XtraEditors.RadioGroup radio平和质;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel36;
        private DevExpress.XtraEditors.TextEdit txt宫颈涂片;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem98;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem99;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem100;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel45;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel44;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel43;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel41;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel38;
        private DevExpress.XtraEditors.RadioGroup radio气虚质;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem101;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem102;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem103;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem104;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem105;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem106;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem107;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem108;
        private System.Windows.Forms.FlowLayoutPanel flow脑血管疾病;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_缺血性卒中;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_脑出血;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_蛛网膜下腔出血;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_短暂性脑缺血发作;
        private DevExpress.XtraEditors.CheckEdit chk脑血管疾病_其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl脑血管疾病;
        private DevExpress.XtraEditors.TextEdit txt脑血管疾病_其他;
        private System.Windows.Forms.FlowLayoutPanel flow眼部疾病;
        private DevExpress.XtraEditors.CheckEdit chk眼部疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk眼部疾病_视网膜出血或渗出;
        private DevExpress.XtraEditors.CheckEdit chk眼部疾病_视乳头水肿;
        private DevExpress.XtraEditors.CheckEdit chk眼部疾病_白内障;
        private DevExpress.XtraEditors.CheckEdit chk眼部疾病_其他;
        private DevExpress.XtraEditors.TextEdit txt眼部疾病_其他;
        private System.Windows.Forms.FlowLayoutPanel flow血管疾病;
        private DevExpress.XtraEditors.CheckEdit chk血管疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk血管疾病_夹层动脉瘤;
        private DevExpress.XtraEditors.CheckEdit chk血管疾病_动脉闭塞性疾病;
        private DevExpress.XtraEditors.CheckEdit chk血管疾病_其他;
        private DevExpress.XtraEditors.TextEdit txt血管疾病_其他;
        private System.Windows.Forms.FlowLayoutPanel flow心血管疾病;
        private DevExpress.XtraEditors.CheckEdit chk心血管疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk心血管疾病_心肌梗死;
        private DevExpress.XtraEditors.CheckEdit chk心血管疾病_心绞痛;
        private DevExpress.XtraEditors.CheckEdit chk心血管疾病_冠状动脉血运重建;
        private DevExpress.XtraEditors.CheckEdit chk心血管疾病_充血性心力衰竭;
        private DevExpress.XtraEditors.CheckEdit chk心血管疾病_心前区疼痛;
        private DevExpress.XtraEditors.TextEdit txt心血管疾病_其他;
        private System.Windows.Forms.FlowLayoutPanel flow肾脏疾病;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_糖尿病肾病;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_肾功能衰竭;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_急性肾炎;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_慢性肾炎;
        private DevExpress.XtraEditors.CheckEdit chk肾脏疾病_其他;
        private DevExpress.XtraEditors.TextEdit txt肾脏疾病_其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl肾脏疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl心血管疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl血管疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl眼部疾病;
        private DevExpress.XtraEditors.CheckEdit chk心血管疾病_其他;
        private System.Windows.Forms.FlowLayoutPanel flow其他系统疾病;
        private DevExpress.XtraEditors.TextEdit txt其他系统疾病_其他;
        private System.Windows.Forms.FlowLayoutPanel flow神经系统疾病;
        private DevExpress.XtraEditors.TextEdit txt神经系统疾病_其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl神经系统疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl其他系统疾病;
        private DevExpress.XtraEditors.RadioGroup radio住院史;
        private DevExpress.XtraEditors.SimpleButton btn住院史删除;
        private DevExpress.XtraEditors.SimpleButton btn住院史添加;
        private DevExpress.XtraEditors.SimpleButton btn病床史删除;
        private DevExpress.XtraEditors.SimpleButton btn病床史添加;
        private DevExpress.XtraEditors.RadioGroup radio家庭病床史;
        private DevExpress.XtraEditors.SimpleButton btn预防接种史删除;
        private DevExpress.XtraEditors.SimpleButton btn预防接种史添加;
        private DevExpress.XtraEditors.RadioGroup radio预防接种史;
        private DevExpress.XtraEditors.SimpleButton btn删除用药情况;
        private DevExpress.XtraEditors.SimpleButton btn添加用药情况;
        private DevExpress.XtraEditors.RadioGroup radio主要用药情况;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.TextEdit txt体检异常4;
        private DevExpress.XtraEditors.LabelControl labelControl32;
        private DevExpress.XtraEditors.TextEdit txt体检异常3;
        private DevExpress.XtraEditors.LabelControl labelControl31;
        private DevExpress.XtraEditors.TextEdit txt体检异常2;
        private DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.TextEdit txt体检异常1;
        private DevExpress.XtraEditors.LabelControl labelControl26;
        private DevExpress.XtraEditors.RadioGroup radio健康评价;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem120;
        private DevExpress.XtraEditors.PanelControl panelControl6;
        private DevExpress.XtraEditors.TextEdit txt危险因素其他;
        private DevExpress.XtraEditors.TextEdit txt建议疫苗接种;
        private DevExpress.XtraEditors.TextEdit txt减体重目标;
        private DevExpress.XtraEditors.LabelControl labelControl35;
        private DevExpress.XtraEditors.LabelControl labelControl34;
        private DevExpress.XtraEditors.CheckEdit chk戒烟;
        private DevExpress.XtraEditors.CheckEdit chk健康饮酒;
        private DevExpress.XtraEditors.CheckEdit chk饮食;
        private DevExpress.XtraEditors.CheckEdit chk锻炼;
        private DevExpress.XtraEditors.CheckEdit chk减体重;
        private DevExpress.XtraEditors.CheckEdit chk建议疫苗接种;
        private DevExpress.XtraEditors.CheckEdit chk危险因素其他;
        private DevExpress.XtraEditors.LabelControl labelControl33;
        private DevExpress.XtraEditors.CheckEdit chk建议转诊;
        private DevExpress.XtraEditors.CheckEdit chk建议复查;
        private DevExpress.XtraEditors.CheckEdit chk纳入慢性病管理;
        private DevExpress.XtraEditors.CheckEdit chk定期随访;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem121;
        private DevExpress.XtraEditors.TextEdit txt最近修改人;
        private DevExpress.XtraEditors.TextEdit txt创建人;
        private DevExpress.XtraEditors.TextEdit txt创建机构;
        private DevExpress.XtraEditors.TextEdit txt当前所属机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem122;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem123;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem124;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem125;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem126;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem127;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup group辅助查体;
        private DevExpress.XtraLayout.LayoutControlGroup group症状;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup15;
        private DevExpress.XtraLayout.LayoutControlGroup group健康评价;
        private DevExpress.XtraLayout.LayoutControlGroup group健康指导;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup19;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup21;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup22;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel53;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.LayoutControlGroup group妇科;
        private DevExpress.XtraEditors.RadioGroup radio移动性浊音;
        private DevExpress.XtraEditors.RadioGroup radio脾大;
        private DevExpress.XtraEditors.RadioGroup radio肝大;
        private DevExpress.XtraEditors.RadioGroup radio附件;
        private DevExpress.XtraEditors.RadioGroup radio宫体;
        private DevExpress.XtraEditors.RadioGroup radio宫颈;
        private DevExpress.XtraEditors.RadioGroup radio阴道;
        private DevExpress.XtraEditors.RadioGroup radio乙型肝炎表面抗原;
        private DevExpress.XtraEditors.RadioGroup radio宫颈涂片;
        private DevExpress.XtraEditors.RadioGroup radio腹部B超;
        private DevExpress.XtraEditors.RadioGroup radio胸部X线片;
        private DevExpress.XtraEditors.RadioGroup radio特秉质;
        private DevExpress.XtraEditors.RadioGroup radio气郁质;
        private DevExpress.XtraEditors.RadioGroup radio血瘀质;
        private DevExpress.XtraEditors.RadioGroup radio湿热质;
        private DevExpress.XtraEditors.RadioGroup radio痰湿质;
        private DevExpress.XtraEditors.RadioGroup radio阴虚质;
        private DevExpress.XtraEditors.RadioGroup radio阳虚质;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private System.Windows.Forms.FlowLayoutPanel flow健康指导;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private System.Windows.Forms.FlowLayoutPanel flow危险因素控制;
        private DevExpress.XtraEditors.RadioGroup radio老年人健康状态评估;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.RadioGroup radio老年人生活自理能力评估;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private System.Windows.Forms.FlowLayoutPanel flow老年人认知能力;
        private DevExpress.XtraEditors.RadioGroup radio老年人认知能力;
        private DevExpress.XtraEditors.LabelControl labelControl36;
        private DevExpress.XtraEditors.TextEdit txt老年人认知能力总分;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private System.Windows.Forms.FlowLayoutPanel flow老年人情感状态;
        private DevExpress.XtraEditors.RadioGroup radio老年人情感状态;
        private DevExpress.XtraEditors.LabelControl labelControl37;
        private DevExpress.XtraEditors.TextEdit txt老年人情感状态总分;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem128;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem25;
        private DevExpress.XtraLayout.LayoutControlGroup group老年人评估;
        private DevExpress.XtraEditors.DateEdit dte创建时间;
        private DevExpress.XtraEditors.DateEdit dte最近更新时间;
        private DevExpress.XtraLayout.SimpleLabelItem lbl职业病危害因素接触史;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem129;
        private DevExpress.XtraEditors.TextEdit txt粉尘防护措施;
        private DevExpress.XtraEditors.TextEdit txt粉尘;
        private DevExpress.XtraEditors.RadioGroup radio粉尘防护措施有无;
        private DevExpress.XtraEditors.RadioGroup radio职业病有无;
        private DevExpress.XtraEditors.LabelControl labelControl38;
        private DevExpress.XtraEditors.TextEdit txt工种;
        private DevExpress.XtraEditors.LabelControl labelControl39;
        private DevExpress.XtraEditors.TextEdit txt从业时间;
        private DevExpress.XtraEditors.LabelControl labelControl40;
        private DevExpress.XtraLayout.LayoutControlItem lbl粉尘;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem132;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem130;
        private DevExpress.XtraEditors.TextEdit txt职业病其他防护;
        private DevExpress.XtraEditors.TextEdit txt职业病其他;
        private DevExpress.XtraEditors.TextEdit txt化学防护措施;
        private DevExpress.XtraEditors.TextEdit txt化学因素;
        private DevExpress.XtraEditors.TextEdit txt物理防护措施;
        private DevExpress.XtraEditors.TextEdit txt物理因素;
        private DevExpress.XtraEditors.RadioGroup radio职业病其他防护措施有无;
        private DevExpress.XtraEditors.TextEdit txt放射物质防护措施;
        private DevExpress.XtraEditors.TextEdit txt放射物质;
        private DevExpress.XtraEditors.RadioGroup radio化学防护措施有无;
        private DevExpress.XtraEditors.RadioGroup radio物理防护措施有无;
        private DevExpress.XtraEditors.RadioGroup radio放射物质防护措施有无;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem134;
        private DevExpress.XtraLayout.LayoutControlItem lbl防护措施;
        private DevExpress.XtraLayout.LayoutControlItem lbl放射物质;
        private DevExpress.XtraLayout.LayoutControlItem lbl物理因素;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem131;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem133;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem135;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem136;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem137;
        private DevExpress.XtraLayout.LayoutControlItem lbl职业病其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem138;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem139;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup20;
        private DevExpress.XtraEditors.TextEdit txt龋齿4;
        private DevExpress.XtraEditors.TextEdit txt龋齿3;
        private DevExpress.XtraEditors.TextEdit txt龋齿2;
        private DevExpress.XtraEditors.TextEdit txt龋齿1;
        private DevExpress.XtraEditors.TextEdit txt缺齿4;
        private DevExpress.XtraEditors.TextEdit txt缺齿2;
        private DevExpress.XtraEditors.TextEdit txt缺齿3;
        private DevExpress.XtraEditors.TextEdit txt缺齿1;
        private DevExpress.XtraEditors.CheckEdit chk齿列_义齿;
        private DevExpress.XtraEditors.CheckEdit chk齿列_其他;
        private DevExpress.XtraEditors.CheckEdit chk齿列_龋齿;
        private DevExpress.XtraEditors.CheckEdit chk齿列_缺齿;
        private DevExpress.XtraEditors.CheckEdit chk齿列_正常;
        private DevExpress.XtraLayout.EmptySpaceItem lbl齿列总;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem142;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem143;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem140;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem145;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem144;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem147;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem146;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem141;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem148;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem149;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem150;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem151;
        private DevExpress.XtraEditors.TextEdit txt义齿1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem152;
        private DevExpress.XtraEditors.TextEdit txt齿列_其他;
        private DevExpress.XtraEditors.TextEdit txt义齿4;
        private DevExpress.XtraEditors.TextEdit txt义齿3;
        private DevExpress.XtraEditors.TextEdit txt义齿2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem153;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem154;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem155;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem158;
        private DevExpress.XtraGrid.GridControl gc住院史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv住院史;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem26;
        private DevExpress.XtraLayout.LayoutControlItem lblgc住院史;
        private DevExpress.XtraLayout.LayoutControlItem lbl住院史;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem157;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem159;
        private DevExpress.XtraGrid.Columns.GridColumn col入院日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte入院日期;
        private DevExpress.XtraGrid.Columns.GridColumn col出院日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte出院日期;
        private DevExpress.XtraGrid.Columns.GridColumn col原因;
        private DevExpress.XtraGrid.Columns.GridColumn col医疗机构名称;
        private DevExpress.XtraGrid.Columns.GridColumn col病案号;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案编号;
        private DevExpress.XtraGrid.Columns.GridColumn col类型;
        private DevExpress.XtraGrid.GridControl gc建床史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv建床史;
        private DevExpress.XtraGrid.Columns.GridColumn col建床日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte建床日期;
        private DevExpress.XtraGrid.Columns.GridColumn col撤床日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte撤床日期;
        private DevExpress.XtraGrid.Columns.GridColumn col原因2;
        private DevExpress.XtraGrid.Columns.GridColumn col医疗机构名称2;
        private DevExpress.XtraGrid.Columns.GridColumn col病案号2;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案编号2;
        private DevExpress.XtraGrid.Columns.GridColumn col类型2;
        private DevExpress.XtraLayout.LayoutControlItem lblgc家庭病床史;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem156;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem160;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem161;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem27;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem28;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem24;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup23;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup24;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup25;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup29;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup26;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup27;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup28;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup30;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup31;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup32;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem116;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem117;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem29;
        private DevExpress.XtraGrid.GridControl gc用药情况;
        private DevExpress.XtraGrid.Views.Grid.GridView gv用药情况;
        private DevExpress.XtraGrid.Columns.GridColumn col药物名称;
        private DevExpress.XtraGrid.Columns.GridColumn col用法;
        private DevExpress.XtraGrid.Columns.GridColumn col用量;
        private DevExpress.XtraGrid.Columns.GridColumn col用药时间;
        private DevExpress.XtraGrid.Columns.GridColumn col服药依从性;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案编号3;
        private DevExpress.XtraGrid.Columns.GridColumn col创建时间;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte用药时间;
        private DevExpress.XtraLayout.LayoutControlItem lblgc用药情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem118;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem162;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem163;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem30;
        private DevExpress.XtraGrid.GridControl gc接种史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv接种史;
        private DevExpress.XtraGrid.Columns.GridColumn col接种名称;
        private DevExpress.XtraGrid.Columns.GridColumn col接种日期;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dte接种日期;
        private DevExpress.XtraGrid.Columns.GridColumn col接种机构;
        private DevExpress.XtraGrid.Columns.GridColumn col个人档案编号4;
        private DevExpress.XtraGrid.Columns.GridColumn col创建日期;
        private DevExpress.XtraLayout.LayoutControlItem lblgc接种史;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lkp服药依从性;
        private DevExpress.XtraEditors.SimpleButton btn读取化验信息;
        private DevExpress.XtraEditors.SimpleButton btn读取住院信息;
        private DevExpress.XtraEditors.SimpleButton btn读取用药信息;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraEditors.SimpleButton btn清空选择;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.CheckEdit chk接种疫苗;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit chk低盐饮食;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.ComboBoxEdit cboRh;
        private DevExpress.XtraEditors.ComboBoxEdit cboABO;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private UCTxtLbl txt同型半胱氨酸;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.RadioGroup radio其他B超;
        private DevExpress.XtraEditors.TextEdit txt其他B超异常;
        private UCTxtLbl txt空腹血糖2;
        private DevExpress.XtraEditors.CheckEdit chk心电图_正常;
        private DevExpress.XtraEditors.CheckEdit chk心电图_改变;
        private DevExpress.XtraEditors.CheckEdit chk心电图_心肌梗塞;
        private DevExpress.XtraEditors.CheckEdit chk心电图_心动过速;
        private DevExpress.XtraEditors.CheckEdit chk心电图_心动过缓;
        private DevExpress.XtraEditors.CheckEdit chk心电图_早搏;
        private DevExpress.XtraEditors.CheckEdit chk心电图_房颤;
        private DevExpress.XtraEditors.CheckEdit chk心电图_房室传导阻滞;
        private DevExpress.XtraEditors.CheckEdit chk心电图_其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem lbl其他B超;
        private DevExpress.XtraEditors.CheckEdit chk心血管疾病_高血压;
        private DevExpress.XtraEditors.CheckEdit chk心血管疾病_夹层动脉瘤;
        private DevExpress.XtraEditors.CheckEdit chk心血管疾病_动脉闭塞性疾病;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_糖尿病;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_慢性支气管炎;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_肺气肿;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_恶性肿瘤;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_老年性骨关节病;
        private DevExpress.XtraEditors.CheckEdit chk其他系统疾病_其他;
        private DevExpress.XtraEditors.CheckEdit chk神经系统疾病_未发现;
        private DevExpress.XtraEditors.CheckEdit chk神经系统疾病_老年性痴呆;
        private DevExpress.XtraEditors.CheckEdit chk神经系统疾病_帕金森病;
        private DevExpress.XtraEditors.CheckEdit chk神经系统疾病_其他;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.SimpleButton btn生成健康评价;
    }
}