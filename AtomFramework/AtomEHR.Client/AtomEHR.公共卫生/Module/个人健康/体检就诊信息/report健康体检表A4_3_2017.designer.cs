﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class report健康体检表A4_3_2017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell363 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_大便潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox辅助检查 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_糖化血红蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell393 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_乙型肝炎表面抗原 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell396 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell394 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血清谷丙转氨酶 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell395 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell397 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血清谷草转氨酶 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell401 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_白蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell399 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell400 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_总胆红素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell402 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell403 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_综合胆红素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell405 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血清肌酐 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell406 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血尿素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell409 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血钾浓度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell410 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell411 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血钠浓度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell413 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableC = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_总胆固醇 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell414 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell415 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_甘油三酯 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell416 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell417 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血清低密度脂蛋白胆固醇 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell418 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell419 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血清高密度脂蛋白胆固醇 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox心电图 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell420 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心电图其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心电图1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心电图2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心电图3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心电图4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心电图5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心电图6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell421 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell422 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_胸部X线片异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_胸部X线片 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox胸部射线 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell359 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell425 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell424 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_腹部B超异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell361 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_腹部B超 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell362 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBoxB超 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell355 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell356 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell427 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell428 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_B超其他异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell357 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_B超其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell358 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell351 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell352 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell423 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell426 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫颈涂片异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell353 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫颈涂片 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell354 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_辅助检查其他 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell347 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell348 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell349 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell350 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox主要现存问题 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell344 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell429 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell430 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_脑血管疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_脑血管疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_脑血管疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_脑血管疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_脑血管疾病4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_脑血管疾病5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell346 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell431 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell432 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_肾脏疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_肾脏疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_肾脏疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_肾脏疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_肾脏疾病4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_肾脏疾病5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell433 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心血管疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心血管疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心血管疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心血管疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心血管疾病4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_心血管疾病5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell434 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell435 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_眼部疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_眼部疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_眼部疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_眼部疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell436 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_神经系统其他疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_神经系统其他疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel85 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_神经系统其他疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_神经系统其他疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell364 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell365 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell366 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell367 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell372 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell373 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell437 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell438 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_其他系统疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell374 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_其他系统疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_其他系统疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_其他系统疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell375 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell368 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell369 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell454 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell452 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell453 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell370 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell371 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell384 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_入院日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell462 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_出院日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史原因1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史机构及科室1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史病案号1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell387 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell381 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_入院日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell464 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_出院日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史原因2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史机构及科室2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_住院史病案号2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell383 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell376 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell377 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell389 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell388 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell378 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell379 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow77 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell391 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell439 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_建床日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell467 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_撤床日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史原因1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史机构及科室1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史病案号1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell444 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow78 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell445 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell446 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_建床日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell468 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_撤床日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史原因2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史机构及科室2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_家庭病床史病案号2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell451 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.Font = new System.Drawing.Font("宋体", 9.75F);
            this.Detail.HeightF = 1169F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 51.04167F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow12,
            this.xrTableRow14,
            this.xrTableRow18,
            this.xrTableRow37,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow54,
            this.xrTableRow57,
            this.xrTableRow59,
            this.xrTableRow61,
            this.xrTableRow70,
            this.xrTableRow69,
            this.xrTableRow68,
            this.xrTableRow65,
            this.xrTableRow67,
            this.xrTableRow66,
            this.xrTableRow63,
            this.xrTableRow64,
            this.xrTableRow62,
            this.xrTableRow60,
            this.xrTableRow58,
            this.xrTableRow56,
            this.xrTableRow55,
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow71,
            this.xrTableRow73,
            this.xrTableRow72,
            this.xrTableRow76,
            this.xrTableRow75,
            this.xrTableRow74,
            this.xrTableRow77,
            this.xrTableRow78});
            this.xrTable2.SizeF = new System.Drawing.SizeF(801F, 974.9999F);
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell112,
            this.xrTableCell363,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.Weight = 0.11161843716525768D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.StylePriority.UseTextAlignment = false;
            this.xrTableCell112.Text = "  大便潜血*";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell112.Weight = 0.30993814107294138D;
            // 
            // xrTableCell363
            // 
            this.xrTableCell363.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell363.Name = "xrTableCell363";
            this.xrTableCell363.StylePriority.UseBorders = false;
            this.xrTableCell363.StylePriority.UseTextAlignment = false;
            this.xrTableCell363.Text = "  1  阴性    2  阳性";
            this.xrTableCell363.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell363.Weight = 1.4516736402579293D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_大便潜血});
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.Weight = 0.12676978150387153D;
            // 
            // xrLabel_大便潜血
            // 
            this.xrLabel_大便潜血.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_大便潜血.LocationFloat = new DevExpress.Utils.PointFloat(19.257F, 3.149592F);
            this.xrLabel_大便潜血.Name = "xrLabel_大便潜血";
            this.xrLabel_大便潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_大便潜血.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_大便潜血.StylePriority.UseBorders = false;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox辅助检查});
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.Weight = 0.19876670037244218D;
            // 
            // xrPictureBox辅助检查
            // 
            this.xrPictureBox辅助检查.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox辅助检查.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2F);
            this.xrPictureBox辅助检查.Name = "xrPictureBox辅助检查";
            this.xrPictureBox辅助检查.SizeF = new System.Drawing.SizeF(68F, 23F);
            this.xrPictureBox辅助检查.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox辅助检查.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132,
            this.xrTableCell136,
            this.xrTableCell392,
            this.xrTableCell156,
            this.xrTableCell171});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Weight = 0.11161843716525768D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.StylePriority.UseBorders = false;
            this.xrTableCell136.StylePriority.UseTextAlignment = false;
            this.xrTableCell136.Text = "糖化血红蛋白*";
            this.xrTableCell136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell136.Weight = 0.30993814107294138D;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell392.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_糖化血红蛋白});
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.StylePriority.UseBorders = false;
            this.xrTableCell392.Weight = 0.38619166578026387D;
            // 
            // xrLabel_糖化血红蛋白
            // 
            this.xrLabel_糖化血红蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_糖化血红蛋白.LocationFloat = new DevExpress.Utils.PointFloat(9.84252F, 0F);
            this.xrLabel_糖化血红蛋白.Name = "xrLabel_糖化血红蛋白";
            this.xrLabel_糖化血红蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_糖化血红蛋白.SizeF = new System.Drawing.SizeF(129.0995F, 19.66659F);
            this.xrLabel_糖化血红蛋白.StylePriority.UseBorders = false;
            this.xrLabel_糖化血红蛋白.StylePriority.UseTextAlignment = false;
            this.xrLabel_糖化血红蛋白.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.StylePriority.UseTextAlignment = false;
            this.xrTableCell156.Text = "  %";
            this.xrTableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell156.Weight = 1.1922517559815371D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.StylePriority.UseBorders = false;
            this.xrTableCell171.Weight = 0.19876670037244218D;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell172,
            this.xrTableCell248,
            this.xrTableCell249,
            this.xrTableCell250});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.StylePriority.UseBorders = false;
            this.xrTableCell172.Weight = 0.11161843716525768D;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.StylePriority.UseBorders = false;
            this.xrTableCell248.StylePriority.UseTextAlignment = false;
            this.xrTableCell248.Text = "乙型肝炎";
            this.xrTableCell248.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell248.Weight = 0.30993814107294138D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.StylePriority.UseBorders = false;
            this.xrTableCell249.Weight = 1.5784434217618009D;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseBorders = false;
            this.xrTableCell250.Weight = 0.19876670037244218D;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell251,
            this.xrTableCell252,
            this.xrTableCell393,
            this.xrTableCell253,
            this.xrTableCell254});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.StylePriority.UseBorders = false;
            this.xrTableCell251.Weight = 0.11161843716525768D;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.StylePriority.UseBorders = false;
            this.xrTableCell252.StylePriority.UseTextAlignment = false;
            this.xrTableCell252.Text = " 表面抗原*";
            this.xrTableCell252.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell252.Weight = 0.30993814107294138D;
            // 
            // xrTableCell393
            // 
            this.xrTableCell393.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell393.Name = "xrTableCell393";
            this.xrTableCell393.StylePriority.UseBorders = false;
            this.xrTableCell393.StylePriority.UseTextAlignment = false;
            this.xrTableCell393.Text = "  1  阴性    2  阳性";
            this.xrTableCell393.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell393.Weight = 1.4516739074206186D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell253.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_乙型肝炎表面抗原});
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.StylePriority.UseBorders = false;
            this.xrTableCell253.Weight = 0.12676951434118211D;
            // 
            // xrLabel_乙型肝炎表面抗原
            // 
            this.xrLabel_乙型肝炎表面抗原.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_乙型肝炎表面抗原.LocationFloat = new DevExpress.Utils.PointFloat(19.2569F, 3.149592F);
            this.xrLabel_乙型肝炎表面抗原.Name = "xrLabel_乙型肝炎表面抗原";
            this.xrLabel_乙型肝炎表面抗原.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_乙型肝炎表面抗原.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_乙型肝炎表面抗原.StylePriority.UseBorders = false;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.StylePriority.UseBorders = false;
            this.xrTableCell254.Weight = 0.19876670037244218D;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell255,
            this.xrTableCell256,
            this.xrTableCell396,
            this.xrTableCell394,
            this.xrTableCell395,
            this.xrTableCell397,
            this.xrTableCell257,
            this.xrTableCell258});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.StylePriority.UseBorders = false;
            this.xrTableCell255.Weight = 0.11161843716525768D;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.StylePriority.UseBorders = false;
            this.xrTableCell256.StylePriority.UseTextAlignment = false;
            this.xrTableCell256.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell256.Weight = 0.30993814107294138D;
            // 
            // xrTableCell396
            // 
            this.xrTableCell396.Name = "xrTableCell396";
            this.xrTableCell396.Text = "血清谷丙转氨酶";
            this.xrTableCell396.Weight = 0.325122825080712D;
            // 
            // xrTableCell394
            // 
            this.xrTableCell394.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血清谷丙转氨酶});
            this.xrTableCell394.Name = "xrTableCell394";
            this.xrTableCell394.Weight = 0.27357458564221404D;
            // 
            // xrLabel_血清谷丙转氨酶
            // 
            this.xrLabel_血清谷丙转氨酶.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血清谷丙转氨酶.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_血清谷丙转氨酶.Name = "xrLabel_血清谷丙转氨酶";
            this.xrLabel_血清谷丙转氨酶.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血清谷丙转氨酶.SizeF = new System.Drawing.SizeF(98.42528F, 19.66659F);
            this.xrLabel_血清谷丙转氨酶.StylePriority.UseBorders = false;
            this.xrLabel_血清谷丙转氨酶.StylePriority.UseTextAlignment = false;
            this.xrLabel_血清谷丙转氨酶.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell395
            // 
            this.xrTableCell395.Name = "xrTableCell395";
            this.xrTableCell395.Text = "U/L\t血清谷草转氨酶";
            this.xrTableCell395.Weight = 0.52491217702929438D;
            // 
            // xrTableCell397
            // 
            this.xrTableCell397.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血清谷草转氨酶});
            this.xrTableCell397.Name = "xrTableCell397";
            this.xrTableCell397.Weight = 0.27357459816088148D;
            // 
            // xrLabel_血清谷草转氨酶
            // 
            this.xrLabel_血清谷草转氨酶.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血清谷草转氨酶.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_血清谷草转氨酶.Name = "xrLabel_血清谷草转氨酶";
            this.xrLabel_血清谷草转氨酶.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血清谷草转氨酶.SizeF = new System.Drawing.SizeF(98.42512F, 19.66659F);
            this.xrLabel_血清谷草转氨酶.StylePriority.UseBorders = false;
            this.xrLabel_血清谷草转氨酶.StylePriority.UseTextAlignment = false;
            this.xrLabel_血清谷草转氨酶.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.StylePriority.UseBorders = false;
            this.xrTableCell257.StylePriority.UseTextAlignment = false;
            this.xrTableCell257.Text = "  U/L";
            this.xrTableCell257.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell257.Weight = 0.181259235848699D;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.StylePriority.UseBorders = false;
            this.xrTableCell258.Weight = 0.19876670037244218D;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell259,
            this.xrTableCell260,
            this.xrTableCell401,
            this.xrTableCell398,
            this.xrTableCell399,
            this.xrTableCell400,
            this.xrTableCell261,
            this.xrTableCell262});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1D;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.StylePriority.UseBorders = false;
            this.xrTableCell259.Text = "辅";
            this.xrTableCell259.Weight = 0.11161843716525768D;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.StylePriority.UseBorders = false;
            this.xrTableCell260.StylePriority.UseTextAlignment = false;
            this.xrTableCell260.Text = "  肝  功  能*";
            this.xrTableCell260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell260.Weight = 0.30993814107294138D;
            // 
            // xrTableCell401
            // 
            this.xrTableCell401.Name = "xrTableCell401";
            this.xrTableCell401.Text = "白蛋白";
            this.xrTableCell401.Weight = 0.1514029579022175D;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_白蛋白});
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.Weight = 0.447294719983398D;
            // 
            // xrLabel_白蛋白
            // 
            this.xrLabel_白蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_白蛋白.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_白蛋白.Name = "xrLabel_白蛋白";
            this.xrLabel_白蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_白蛋白.SizeF = new System.Drawing.SizeF(160.9252F, 19.66657F);
            this.xrLabel_白蛋白.StylePriority.UseBorders = false;
            this.xrLabel_白蛋白.StylePriority.UseTextAlignment = false;
            this.xrLabel_白蛋白.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell399
            // 
            this.xrTableCell399.Name = "xrTableCell399";
            this.xrTableCell399.Text = "g/L\t总胆红素";
            this.xrTableCell399.Weight = 0.39751725154814588D;
            // 
            // xrTableCell400
            // 
            this.xrTableCell400.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_总胆红素});
            this.xrTableCell400.Name = "xrTableCell400";
            this.xrTableCell400.Weight = 0.35001117942180543D;
            // 
            // xrLabel_总胆红素
            // 
            this.xrLabel_总胆红素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_总胆红素.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_总胆红素.Name = "xrLabel_总胆红素";
            this.xrLabel_总胆红素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_总胆红素.SizeF = new System.Drawing.SizeF(125.9252F, 19.66659F);
            this.xrLabel_总胆红素.StylePriority.UseBorders = false;
            this.xrLabel_总胆红素.StylePriority.UseTextAlignment = false;
            this.xrLabel_总胆红素.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseBorders = false;
            this.xrTableCell261.StylePriority.UseTextAlignment = false;
            this.xrTableCell261.Text = "  μ mol/L";
            this.xrTableCell261.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell261.Weight = 0.23221731290623418D;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.StylePriority.UseBorders = false;
            this.xrTableCell262.Weight = 0.19876670037244218D;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell402,
            this.xrTableCell403,
            this.xrTableCell265,
            this.xrTableCell266});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1D;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBorders = false;
            this.xrTableCell263.Weight = 0.11161843716525768D;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.StylePriority.UseBorders = false;
            this.xrTableCell264.StylePriority.UseTextAlignment = false;
            this.xrTableCell264.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell264.Weight = 0.30993814107294138D;
            // 
            // xrTableCell402
            // 
            this.xrTableCell402.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell402.Name = "xrTableCell402";
            this.xrTableCell402.StylePriority.UseBorders = false;
            this.xrTableCell402.Text = "综合胆红素";
            this.xrTableCell402.Weight = 0.23563455331080002D;
            // 
            // xrTableCell403
            // 
            this.xrTableCell403.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell403.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_综合胆红素});
            this.xrTableCell403.Name = "xrTableCell403";
            this.xrTableCell403.StylePriority.UseBorders = false;
            this.xrTableCell403.Weight = 0.3630631663281525D;
            // 
            // xrLabel_综合胆红素
            // 
            this.xrLabel_综合胆红素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_综合胆红素.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_综合胆红素.Name = "xrLabel_综合胆红素";
            this.xrLabel_综合胆红素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_综合胆红素.SizeF = new System.Drawing.SizeF(130.6208F, 19.66661F);
            this.xrLabel_综合胆红素.StylePriority.UseBorders = false;
            this.xrLabel_综合胆红素.StylePriority.UseTextAlignment = false;
            this.xrLabel_综合胆红素.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseBorders = false;
            this.xrTableCell265.StylePriority.UseTextAlignment = false;
            this.xrTableCell265.Text = " μ mol/L";
            this.xrTableCell265.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell265.Weight = 0.97974570212284817D;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.StylePriority.UseBorders = false;
            this.xrTableCell266.Weight = 0.19876670037244218D;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell267,
            this.xrTableCell268,
            this.xrTableCell405,
            this.xrTableCell404,
            this.xrTableCell406,
            this.xrTableCell407,
            this.xrTableCell269,
            this.xrTableCell270});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1D;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseBorders = false;
            this.xrTableCell267.Text = "助";
            this.xrTableCell267.Weight = 0.11161843716525768D;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.StylePriority.UseBorders = false;
            this.xrTableCell268.StylePriority.UseTextAlignment = false;
            this.xrTableCell268.Text = "  肾  功  能*";
            this.xrTableCell268.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell268.Weight = 0.30993814107294138D;
            // 
            // xrTableCell405
            // 
            this.xrTableCell405.Name = "xrTableCell405";
            this.xrTableCell405.StylePriority.UseTextAlignment = false;
            this.xrTableCell405.Text = "  血清肌酐";
            this.xrTableCell405.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell405.Weight = 0.18614694803558454D;
            // 
            // xrTableCell404
            // 
            this.xrTableCell404.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血清肌酐});
            this.xrTableCell404.Name = "xrTableCell404";
            this.xrTableCell404.Weight = 0.246369960000051D;
            // 
            // xrLabel_血清肌酐
            // 
            this.xrLabel_血清肌酐.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血清肌酐.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_血清肌酐.Name = "xrLabel_血清肌酐";
            this.xrLabel_血清肌酐.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血清肌酐.SizeF = new System.Drawing.SizeF(88.63764F, 19.66661F);
            this.xrLabel_血清肌酐.StylePriority.UseBorders = false;
            this.xrLabel_血清肌酐.StylePriority.UseTextAlignment = false;
            this.xrLabel_血清肌酐.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell406
            // 
            this.xrTableCell406.Name = "xrTableCell406";
            this.xrTableCell406.StylePriority.UseTextAlignment = false;
            this.xrTableCell406.Text = " μmol/L  血 尿 素";
            this.xrTableCell406.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell406.Weight = 0.38997773572429745D;
            // 
            // xrTableCell407
            // 
            this.xrTableCell407.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血尿素});
            this.xrTableCell407.Name = "xrTableCell407";
            this.xrTableCell407.Weight = 0.24637094203709944D;
            // 
            // xrLabel_血尿素
            // 
            this.xrLabel_血尿素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血尿素.LocationFloat = new DevExpress.Utils.PointFloat(1.312805F, 0.6876068F);
            this.xrLabel_血尿素.Name = "xrLabel_血尿素";
            this.xrLabel_血尿素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血尿素.SizeF = new System.Drawing.SizeF(88.43893F, 19.66661F);
            this.xrLabel_血尿素.StylePriority.UseBorders = false;
            this.xrLabel_血尿素.StylePriority.UseTextAlignment = false;
            this.xrLabel_血尿素.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.StylePriority.UseBorders = false;
            this.xrTableCell269.StylePriority.UseTextAlignment = false;
            this.xrTableCell269.Text = "  mmol/L";
            this.xrTableCell269.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell269.Weight = 0.50957783596476858D;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.StylePriority.UseBorders = false;
            this.xrTableCell270.Weight = 0.19876670037244218D;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell271,
            this.xrTableCell272,
            this.xrTableCell409,
            this.xrTableCell408,
            this.xrTableCell410,
            this.xrTableCell411,
            this.xrTableCell273,
            this.xrTableCell274});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1D;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.StylePriority.UseBorders = false;
            this.xrTableCell271.Weight = 0.11161843716525768D;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.StylePriority.UseBorders = false;
            this.xrTableCell272.StylePriority.UseTextAlignment = false;
            this.xrTableCell272.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell272.Weight = 0.30993814107294138D;
            // 
            // xrTableCell409
            // 
            this.xrTableCell409.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell409.Name = "xrTableCell409";
            this.xrTableCell409.StylePriority.UseBorders = false;
            this.xrTableCell409.StylePriority.UseTextAlignment = false;
            this.xrTableCell409.Text = "  血钾浓度";
            this.xrTableCell409.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell409.Weight = 0.18614694803558454D;
            // 
            // xrTableCell408
            // 
            this.xrTableCell408.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell408.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血钾浓度});
            this.xrTableCell408.Name = "xrTableCell408";
            this.xrTableCell408.StylePriority.UseBorders = false;
            this.xrTableCell408.Weight = 0.246369960000051D;
            // 
            // xrLabel_血钾浓度
            // 
            this.xrLabel_血钾浓度.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血钾浓度.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.2939059F);
            this.xrLabel_血钾浓度.Name = "xrLabel_血钾浓度";
            this.xrLabel_血钾浓度.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血钾浓度.SizeF = new System.Drawing.SizeF(88.63764F, 19.66661F);
            this.xrLabel_血钾浓度.StylePriority.UseBorders = false;
            this.xrLabel_血钾浓度.StylePriority.UseTextAlignment = false;
            this.xrLabel_血钾浓度.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell410
            // 
            this.xrTableCell410.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell410.Name = "xrTableCell410";
            this.xrTableCell410.StylePriority.UseBorders = false;
            this.xrTableCell410.StylePriority.UseTextAlignment = false;
            this.xrTableCell410.Text = "  mmol/L  血钠浓度";
            this.xrTableCell410.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell410.Weight = 0.38997818761496739D;
            // 
            // xrTableCell411
            // 
            this.xrTableCell411.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell411.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血钠浓度});
            this.xrTableCell411.Name = "xrTableCell411";
            this.xrTableCell411.StylePriority.UseBorders = false;
            this.xrTableCell411.Weight = 0.24637033986741663D;
            // 
            // xrLabel_血钠浓度
            // 
            this.xrLabel_血钠浓度.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血钠浓度.LocationFloat = new DevExpress.Utils.PointFloat(0.0001922367F, 0.2939059F);
            this.xrLabel_血钠浓度.Name = "xrLabel_血钠浓度";
            this.xrLabel_血钠浓度.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血钠浓度.SizeF = new System.Drawing.SizeF(88.63764F, 19.66661F);
            this.xrLabel_血钠浓度.StylePriority.UseBorders = false;
            this.xrLabel_血钠浓度.StylePriority.UseTextAlignment = false;
            this.xrLabel_血钠浓度.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.StylePriority.UseBorders = false;
            this.xrTableCell273.StylePriority.UseTextAlignment = false;
            this.xrTableCell273.Text = "  mmol/L";
            this.xrTableCell273.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell273.Weight = 0.50957798624378126D;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.StylePriority.UseBorders = false;
            this.xrTableCell274.Weight = 0.19876670037244218D;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell275,
            this.xrTableCell276,
            this.xrTableCell413,
            this.xrTableC,
            this.xrTableCell414,
            this.xrTableCell415,
            this.xrTableCell277,
            this.xrTableCell278});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1D;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.StylePriority.UseBorders = false;
            this.xrTableCell275.Text = "检";
            this.xrTableCell275.Weight = 0.11161843716525768D;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.StylePriority.UseBorders = false;
            this.xrTableCell276.StylePriority.UseTextAlignment = false;
            this.xrTableCell276.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell276.Weight = 0.30993814107294138D;
            // 
            // xrTableCell413
            // 
            this.xrTableCell413.Name = "xrTableCell413";
            this.xrTableCell413.StylePriority.UseTextAlignment = false;
            this.xrTableCell413.Text = "  总胆固醇";
            this.xrTableCell413.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell413.Weight = 0.18614694803558454D;
            // 
            // xrTableC
            // 
            this.xrTableC.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_总胆固醇});
            this.xrTableC.Name = "xrTableC";
            this.xrTableC.Weight = 0.2463696928373616D;
            // 
            // xrLabel_总胆固醇
            // 
            this.xrLabel_总胆固醇.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_总胆固醇.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.0001017252F);
            this.xrLabel_总胆固醇.Name = "xrLabel_总胆固醇";
            this.xrLabel_总胆固醇.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_总胆固醇.SizeF = new System.Drawing.SizeF(88.63764F, 19.66661F);
            this.xrLabel_总胆固醇.StylePriority.UseBorders = false;
            this.xrLabel_总胆固醇.StylePriority.UseTextAlignment = false;
            this.xrLabel_总胆固醇.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell414
            // 
            this.xrTableCell414.Name = "xrTableCell414";
            this.xrTableCell414.StylePriority.UseTextAlignment = false;
            this.xrTableCell414.Text = "  mmol/L  甘油三酯";
            this.xrTableCell414.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell414.Weight = 0.3899781876149675D;
            // 
            // xrTableCell415
            // 
            this.xrTableCell415.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_甘油三酯});
            this.xrTableCell415.Name = "xrTableCell415";
            this.xrTableCell415.Weight = 0.24637049014642931D;
            // 
            // xrLabel_甘油三酯
            // 
            this.xrLabel_甘油三酯.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_甘油三酯.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.086264E-05F);
            this.xrLabel_甘油三酯.Name = "xrLabel_甘油三酯";
            this.xrLabel_甘油三酯.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_甘油三酯.SizeF = new System.Drawing.SizeF(88.63764F, 19.66661F);
            this.xrLabel_甘油三酯.StylePriority.UseBorders = false;
            this.xrLabel_甘油三酯.StylePriority.UseTextAlignment = false;
            this.xrLabel_甘油三酯.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.StylePriority.UseBorders = false;
            this.xrTableCell277.StylePriority.UseTextAlignment = false;
            this.xrTableCell277.Text = "  mmol/L";
            this.xrTableCell277.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell277.Weight = 0.50957810312745788D;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.StylePriority.UseBorders = false;
            this.xrTableCell278.Weight = 0.19876670037244218D;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell279,
            this.xrTableCell280,
            this.xrTableCell416,
            this.xrTableCell417,
            this.xrTableCell281,
            this.xrTableCell282});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1D;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.StylePriority.UseBorders = false;
            this.xrTableCell279.Weight = 0.11161843716525768D;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.StylePriority.UseBorders = false;
            this.xrTableCell280.StylePriority.UseTextAlignment = false;
            this.xrTableCell280.Text = "  血      脂*";
            this.xrTableCell280.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell280.Weight = 0.30993814107294138D;
            // 
            // xrTableCell416
            // 
            this.xrTableCell416.Name = "xrTableCell416";
            this.xrTableCell416.StylePriority.UseTextAlignment = false;
            this.xrTableCell416.Text = "  血清低密度脂蛋白胆固醇";
            this.xrTableCell416.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell416.Weight = 0.47884238405836033D;
            // 
            // xrTableCell417
            // 
            this.xrTableCell417.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血清低密度脂蛋白胆固醇});
            this.xrTableCell417.Name = "xrTableCell417";
            this.xrTableCell417.Weight = 0.31037946876188582D;
            // 
            // xrLabel_血清低密度脂蛋白胆固醇
            // 
            this.xrLabel_血清低密度脂蛋白胆固醇.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血清低密度脂蛋白胆固醇.LocationFloat = new DevExpress.Utils.PointFloat(9.611835E-05F, 7.208878E-05F);
            this.xrLabel_血清低密度脂蛋白胆固醇.Name = "xrLabel_血清低密度脂蛋白胆固醇";
            this.xrLabel_血清低密度脂蛋白胆固醇.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血清低密度脂蛋白胆固醇.SizeF = new System.Drawing.SizeF(111.6666F, 19.66659F);
            this.xrLabel_血清低密度脂蛋白胆固醇.StylePriority.UseBorders = false;
            this.xrLabel_血清低密度脂蛋白胆固醇.StylePriority.UseTextAlignment = false;
            this.xrLabel_血清低密度脂蛋白胆固醇.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.StylePriority.UseBorders = false;
            this.xrTableCell281.StylePriority.UseTextAlignment = false;
            this.xrTableCell281.Text = "  mmol/L";
            this.xrTableCell281.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell281.Weight = 0.78922156894155471D;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.StylePriority.UseBorders = false;
            this.xrTableCell282.Weight = 0.19876670037244218D;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell283,
            this.xrTableCell284,
            this.xrTableCell418,
            this.xrTableCell419,
            this.xrTableCell285,
            this.xrTableCell286});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1D;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.StylePriority.UseBorders = false;
            this.xrTableCell283.Text = "查";
            this.xrTableCell283.Weight = 0.11161843716525768D;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.StylePriority.UseBorders = false;
            this.xrTableCell284.StylePriority.UseTextAlignment = false;
            this.xrTableCell284.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell284.Weight = 0.30993814107294138D;
            // 
            // xrTableCell418
            // 
            this.xrTableCell418.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell418.Name = "xrTableCell418";
            this.xrTableCell418.StylePriority.UseBorders = false;
            this.xrTableCell418.StylePriority.UseTextAlignment = false;
            this.xrTableCell418.Text = "  血清高密度脂蛋白胆固醇";
            this.xrTableCell418.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell418.Weight = 0.47884238405836033D;
            // 
            // xrTableCell419
            // 
            this.xrTableCell419.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell419.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血清高密度脂蛋白胆固醇});
            this.xrTableCell419.Name = "xrTableCell419";
            this.xrTableCell419.StylePriority.UseBorders = false;
            this.xrTableCell419.Weight = 0.31037946876188582D;
            // 
            // xrLabel_血清高密度脂蛋白胆固醇
            // 
            this.xrLabel_血清高密度脂蛋白胆固醇.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血清高密度脂蛋白胆固醇.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_血清高密度脂蛋白胆固醇.Name = "xrLabel_血清高密度脂蛋白胆固醇";
            this.xrLabel_血清高密度脂蛋白胆固醇.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血清高密度脂蛋白胆固醇.SizeF = new System.Drawing.SizeF(111.6666F, 19.66659F);
            this.xrLabel_血清高密度脂蛋白胆固醇.StylePriority.UseBorders = false;
            this.xrLabel_血清高密度脂蛋白胆固醇.StylePriority.UseTextAlignment = false;
            this.xrLabel_血清高密度脂蛋白胆固醇.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.StylePriority.UseBorders = false;
            this.xrTableCell285.StylePriority.UseTextAlignment = false;
            this.xrTableCell285.Text = "  mmol/L";
            this.xrTableCell285.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell285.Weight = 0.78922156894155471D;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.StylePriority.UseBorders = false;
            this.xrTableCell286.Weight = 0.19876670037244218D;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell295,
            this.xrTableCell296,
            this.xrTableCell297,
            this.xrTableCell298});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1D;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.StylePriority.UseBorders = false;
            this.xrTableCell295.Weight = 0.11161843716525768D;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.StylePriority.UseBorders = false;
            this.xrTableCell296.StylePriority.UseTextAlignment = false;
            this.xrTableCell296.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell296.Weight = 0.30993814107294138D;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.StylePriority.UseBorders = false;
            this.xrTableCell297.StylePriority.UseTextAlignment = false;
            this.xrTableCell297.Text = "  1 正常   2 ST-T改变   3 陈旧性心肌梗塞   4 窦性心动过速   5 窦性心动过缓";
            this.xrTableCell297.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell297.Weight = 1.5784434217618009D;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell298.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox心电图});
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.StylePriority.UseBorders = false;
            this.xrTableCell298.Weight = 0.19876670037244218D;
            // 
            // xrPictureBox心电图
            // 
            this.xrPictureBox心电图.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox心电图.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox心电图.Name = "xrPictureBox心电图";
            this.xrPictureBox心电图.SizeF = new System.Drawing.SizeF(68F, 25F);
            this.xrPictureBox心电图.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox心电图.StylePriority.UseBorders = false;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell307,
            this.xrTableCell308,
            this.xrTableCell420,
            this.xrTableCell309,
            this.xrTableCell310});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 1D;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.StylePriority.UseBorders = false;
            this.xrTableCell307.Weight = 0.11161843716525768D;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.StylePriority.UseBorders = false;
            this.xrTableCell308.StylePriority.UseTextAlignment = false;
            this.xrTableCell308.Text = "  心  电  图*";
            this.xrTableCell308.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell308.Weight = 0.30993814107294138D;
            // 
            // xrTableCell420
            // 
            this.xrTableCell420.Name = "xrTableCell420";
            this.xrTableCell420.StylePriority.UseTextAlignment = false;
            this.xrTableCell420.Text = "  6 早搏   7 房颤   8 房室传导阻滞   9 其他";
            this.xrTableCell420.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell420.Weight = 0.8517608897814275D;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell309.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心电图其他});
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.StylePriority.UseBorders = false;
            this.xrTableCell309.Weight = 0.72668253198037336D;
            // 
            // xrLabel_心电图其他
            // 
            this.xrLabel_心电图其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_心电图其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_心电图其他.Name = "xrLabel_心电图其他";
            this.xrLabel_心电图其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心电图其他.SizeF = new System.Drawing.SizeF(251.5994F, 19.66656F);
            this.xrLabel_心电图其他.StylePriority.UseBorders = false;
            this.xrLabel_心电图其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_心电图其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.StylePriority.UseBorders = false;
            this.xrTableCell310.Weight = 0.19876670037244218D;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell315,
            this.xrTableCell316,
            this.xrTableCell317,
            this.xrTableCell318});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 1D;
            // 
            // xrTableCell315
            // 
            this.xrTableCell315.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell315.Name = "xrTableCell315";
            this.xrTableCell315.StylePriority.UseBorders = false;
            this.xrTableCell315.Weight = 0.11161843716525768D;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.StylePriority.UseBorders = false;
            this.xrTableCell316.StylePriority.UseTextAlignment = false;
            this.xrTableCell316.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell316.Weight = 0.30993814107294138D;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell317.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心电图1,
            this.xrLabel26,
            this.xrLabel25,
            this.xrLabel_心电图2,
            this.xrLabel_心电图3,
            this.xrLabel22,
            this.xrLabel_心电图4,
            this.xrLabel20,
            this.xrLabel_心电图5,
            this.xrLabel18,
            this.xrLabel_心电图6});
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.StylePriority.UseBorders = false;
            this.xrTableCell317.Weight = 1.5784434217618009D;
            // 
            // xrLabel_心电图1
            // 
            this.xrLabel_心电图1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图1.LocationFloat = new DevExpress.Utils.PointFloat(376.4413F, 3.149618F);
            this.xrLabel_心电图1.Name = "xrLabel_心电图1";
            this.xrLabel_心电图1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心电图1.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_心电图1.StylePriority.UseBorders = false;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(392.9503F, 3.149566F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "/";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(425.9685F, 3.149618F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(16.50912F, 18.83338F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "/";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心电图2
            // 
            this.xrLabel_心电图2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图2.LocationFloat = new DevExpress.Utils.PointFloat(409.4594F, 3.149566F);
            this.xrLabel_心电图2.Name = "xrLabel_心电图2";
            this.xrLabel_心电图2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心电图2.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_心电图2.StylePriority.UseBorders = false;
            // 
            // xrLabel_心电图3
            // 
            this.xrLabel_心电图3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图3.LocationFloat = new DevExpress.Utils.PointFloat(442.4775F, 3.149618F);
            this.xrLabel_心电图3.Name = "xrLabel_心电图3";
            this.xrLabel_心电图3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心电图3.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_心电图3.StylePriority.UseBorders = false;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(458.987F, 3.149618F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "/";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心电图4
            // 
            this.xrLabel_心电图4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图4.LocationFloat = new DevExpress.Utils.PointFloat(475.4961F, 3.149618F);
            this.xrLabel_心电图4.Name = "xrLabel_心电图4";
            this.xrLabel_心电图4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心电图4.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_心电图4.StylePriority.UseBorders = false;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(492.0051F, 3.149618F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "/";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心电图5
            // 
            this.xrLabel_心电图5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图5.LocationFloat = new DevExpress.Utils.PointFloat(508.5142F, 3.149566F);
            this.xrLabel_心电图5.Name = "xrLabel_心电图5";
            this.xrLabel_心电图5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心电图5.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_心电图5.StylePriority.UseBorders = false;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(525.0232F, 3.149592F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "/";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心电图6
            // 
            this.xrLabel_心电图6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心电图6.LocationFloat = new DevExpress.Utils.PointFloat(541.5323F, 3.149606F);
            this.xrLabel_心电图6.Name = "xrLabel_心电图6";
            this.xrLabel_心电图6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心电图6.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_心电图6.StylePriority.UseBorders = false;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.StylePriority.UseBorders = false;
            this.xrTableCell318.Weight = 0.19876670037244218D;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell323,
            this.xrTableCell324,
            this.xrTableCell421,
            this.xrTableCell422,
            this.xrTableCell325,
            this.xrTableCell326});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 1D;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.StylePriority.UseBorders = false;
            this.xrTableCell323.Weight = 0.11161843716525768D;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.StylePriority.UseBorders = false;
            this.xrTableCell324.StylePriority.UseTextAlignment = false;
            this.xrTableCell324.Text = "  胸部X线片*";
            this.xrTableCell324.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell324.Weight = 0.30993814107294138D;
            // 
            // xrTableCell421
            // 
            this.xrTableCell421.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell421.Name = "xrTableCell421";
            this.xrTableCell421.StylePriority.UseBorders = false;
            this.xrTableCell421.StylePriority.UseTextAlignment = false;
            this.xrTableCell421.Text = "  1 正常  2 异常";
            this.xrTableCell421.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell421.Weight = 0.34913142522249607D;
            // 
            // xrTableCell422
            // 
            this.xrTableCell422.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell422.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_胸部X线片异常});
            this.xrTableCell422.Name = "xrTableCell422";
            this.xrTableCell422.StylePriority.UseBorders = false;
            this.xrTableCell422.Weight = 1.1025422233934341D;
            // 
            // xrLabel_胸部X线片异常
            // 
            this.xrLabel_胸部X线片异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_胸部X线片异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.7454701F);
            this.xrLabel_胸部X线片异常.Name = "xrLabel_胸部X线片异常";
            this.xrLabel_胸部X线片异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_胸部X线片异常.SizeF = new System.Drawing.SizeF(206.2952F, 19.66659F);
            this.xrLabel_胸部X线片异常.StylePriority.UseBorders = false;
            this.xrLabel_胸部X线片异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_胸部X线片异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell325.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_胸部X线片});
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.StylePriority.UseBorders = false;
            this.xrTableCell325.Weight = 0.12676977314587062D;
            // 
            // xrLabel_胸部X线片
            // 
            this.xrLabel_胸部X线片.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_胸部X线片.LocationFloat = new DevExpress.Utils.PointFloat(19.257F, 3.149606F);
            this.xrLabel_胸部X线片.Name = "xrLabel_胸部X线片";
            this.xrLabel_胸部X线片.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_胸部X线片.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_胸部X线片.StylePriority.UseBorders = false;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell326.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox胸部射线});
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.StylePriority.UseBorders = false;
            this.xrTableCell326.Weight = 0.19876670037244218D;
            // 
            // xrPictureBox胸部射线
            // 
            this.xrPictureBox胸部射线.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox胸部射线.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox胸部射线.Name = "xrPictureBox胸部射线";
            this.xrPictureBox胸部射线.SizeF = new System.Drawing.SizeF(68F, 23F);
            this.xrPictureBox胸部射线.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox胸部射线.StylePriority.UseBorders = false;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell359,
            this.xrTableCell360,
            this.xrTableCell425,
            this.xrTableCell424,
            this.xrTableCell361,
            this.xrTableCell362});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 1D;
            // 
            // xrTableCell359
            // 
            this.xrTableCell359.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell359.Name = "xrTableCell359";
            this.xrTableCell359.StylePriority.UseBorders = false;
            this.xrTableCell359.Weight = 0.11161843716525768D;
            // 
            // xrTableCell360
            // 
            this.xrTableCell360.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell360.Name = "xrTableCell360";
            this.xrTableCell360.StylePriority.UseBorders = false;
            this.xrTableCell360.StylePriority.UseTextAlignment = false;
            this.xrTableCell360.Text = "  B     超*";
            this.xrTableCell360.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell360.Weight = 0.30993814107294138D;
            // 
            // xrTableCell425
            // 
            this.xrTableCell425.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell425.Name = "xrTableCell425";
            this.xrTableCell425.StylePriority.UseBorders = false;
            this.xrTableCell425.StylePriority.UseTextAlignment = false;
            this.xrTableCell425.Text = "  腹部B超：1 正常 2 异常";
            this.xrTableCell425.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell425.Weight = 0.56369832195615155D;
            // 
            // xrTableCell424
            // 
            this.xrTableCell424.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell424.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_腹部B超异常});
            this.xrTableCell424.Name = "xrTableCell424";
            this.xrTableCell424.StylePriority.UseBorders = false;
            this.xrTableCell424.Weight = 0.88797559382246827D;
            // 
            // xrLabel_腹部B超异常
            // 
            this.xrLabel_腹部B超异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_腹部B超异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_腹部B超异常.Name = "xrLabel_腹部B超异常";
            this.xrLabel_腹部B超异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_腹部B超异常.SizeF = new System.Drawing.SizeF(129.0994F, 19.66659F);
            this.xrLabel_腹部B超异常.StylePriority.UseBorders = false;
            this.xrLabel_腹部B超异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_腹部B超异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell361
            // 
            this.xrTableCell361.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell361.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_腹部B超});
            this.xrTableCell361.Name = "xrTableCell361";
            this.xrTableCell361.StylePriority.UseBorders = false;
            this.xrTableCell361.Weight = 0.12676950598318121D;
            // 
            // xrLabel_腹部B超
            // 
            this.xrLabel_腹部B超.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_腹部B超.LocationFloat = new DevExpress.Utils.PointFloat(19.25683F, 3.149606F);
            this.xrLabel_腹部B超.Name = "xrLabel_腹部B超";
            this.xrLabel_腹部B超.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_腹部B超.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_腹部B超.StylePriority.UseBorders = false;
            // 
            // xrTableCell362
            // 
            this.xrTableCell362.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell362.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBoxB超});
            this.xrTableCell362.Name = "xrTableCell362";
            this.xrTableCell362.StylePriority.UseBorders = false;
            this.xrTableCell362.Weight = 0.19876670037244218D;
            // 
            // xrPictureBoxB超
            // 
            this.xrPictureBoxB超.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBoxB超.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBoxB超.Name = "xrPictureBoxB超";
            this.xrPictureBoxB超.SizeF = new System.Drawing.SizeF(68F, 23F);
            this.xrPictureBoxB超.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBoxB超.StylePriority.UseBorders = false;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell355,
            this.xrTableCell356,
            this.xrTableCell427,
            this.xrTableCell428,
            this.xrTableCell357,
            this.xrTableCell358});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 1D;
            // 
            // xrTableCell355
            // 
            this.xrTableCell355.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell355.Name = "xrTableCell355";
            this.xrTableCell355.StylePriority.UseBorders = false;
            this.xrTableCell355.Weight = 0.11161843716525768D;
            // 
            // xrTableCell356
            // 
            this.xrTableCell356.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell356.Name = "xrTableCell356";
            this.xrTableCell356.StylePriority.UseBorders = false;
            this.xrTableCell356.StylePriority.UseTextAlignment = false;
            this.xrTableCell356.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell356.Weight = 0.30993814107294138D;
            // 
            // xrTableCell427
            // 
            this.xrTableCell427.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell427.Name = "xrTableCell427";
            this.xrTableCell427.StylePriority.UseBorders = false;
            this.xrTableCell427.StylePriority.UseTextAlignment = false;
            this.xrTableCell427.Text = "  其他\t1 正常 2 异常";
            this.xrTableCell427.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell427.Weight = 0.56369832195615155D;
            // 
            // xrTableCell428
            // 
            this.xrTableCell428.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell428.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_B超其他异常});
            this.xrTableCell428.Name = "xrTableCell428";
            this.xrTableCell428.StylePriority.UseBorders = false;
            this.xrTableCell428.Weight = 0.88797559382246827D;
            // 
            // xrLabel_B超其他异常
            // 
            this.xrLabel_B超其他异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_B超其他异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_B超其他异常.Name = "xrLabel_B超其他异常";
            this.xrLabel_B超其他异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_B超其他异常.SizeF = new System.Drawing.SizeF(129.0994F, 19.66659F);
            this.xrLabel_B超其他异常.StylePriority.UseBorders = false;
            this.xrLabel_B超其他异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_B超其他异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell357
            // 
            this.xrTableCell357.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell357.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_B超其他});
            this.xrTableCell357.Name = "xrTableCell357";
            this.xrTableCell357.StylePriority.UseBorders = false;
            this.xrTableCell357.Weight = 0.12676950598318121D;
            // 
            // xrLabel_B超其他
            // 
            this.xrLabel_B超其他.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_B超其他.LocationFloat = new DevExpress.Utils.PointFloat(19.25683F, 3.149606F);
            this.xrLabel_B超其他.Name = "xrLabel_B超其他";
            this.xrLabel_B超其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_B超其他.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_B超其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell358
            // 
            this.xrTableCell358.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell358.Name = "xrTableCell358";
            this.xrTableCell358.StylePriority.UseBorders = false;
            this.xrTableCell358.Weight = 0.19876670037244218D;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell351,
            this.xrTableCell352,
            this.xrTableCell423,
            this.xrTableCell426,
            this.xrTableCell353,
            this.xrTableCell354});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 1D;
            // 
            // xrTableCell351
            // 
            this.xrTableCell351.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell351.Name = "xrTableCell351";
            this.xrTableCell351.StylePriority.UseBorders = false;
            this.xrTableCell351.Weight = 0.11161843716525768D;
            // 
            // xrTableCell352
            // 
            this.xrTableCell352.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell352.Name = "xrTableCell352";
            this.xrTableCell352.StylePriority.UseBorders = false;
            this.xrTableCell352.StylePriority.UseTextAlignment = false;
            this.xrTableCell352.Text = "  宫颈涂片*";
            this.xrTableCell352.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell352.Weight = 0.30993814107294138D;
            // 
            // xrTableCell423
            // 
            this.xrTableCell423.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell423.Name = "xrTableCell423";
            this.xrTableCell423.StylePriority.UseBorders = false;
            this.xrTableCell423.StylePriority.UseTextAlignment = false;
            this.xrTableCell423.Text = "  1 正常 2 异常";
            this.xrTableCell423.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell423.Weight = 0.34913142522249607D;
            // 
            // xrTableCell426
            // 
            this.xrTableCell426.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell426.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫颈涂片异常});
            this.xrTableCell426.Name = "xrTableCell426";
            this.xrTableCell426.StylePriority.UseBorders = false;
            this.xrTableCell426.Weight = 1.1025422233934341D;
            // 
            // xrLabel_宫颈涂片异常
            // 
            this.xrLabel_宫颈涂片异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_宫颈涂片异常.LocationFloat = new DevExpress.Utils.PointFloat(5.086264E-05F, 0F);
            this.xrLabel_宫颈涂片异常.Name = "xrLabel_宫颈涂片异常";
            this.xrLabel_宫颈涂片异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_宫颈涂片异常.SizeF = new System.Drawing.SizeF(206.2952F, 19.66659F);
            this.xrLabel_宫颈涂片异常.StylePriority.UseBorders = false;
            this.xrLabel_宫颈涂片异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_宫颈涂片异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell353
            // 
            this.xrTableCell353.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell353.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫颈涂片});
            this.xrTableCell353.Name = "xrTableCell353";
            this.xrTableCell353.StylePriority.UseBorders = false;
            this.xrTableCell353.Weight = 0.12676977314587062D;
            // 
            // xrLabel_宫颈涂片
            // 
            this.xrLabel_宫颈涂片.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_宫颈涂片.LocationFloat = new DevExpress.Utils.PointFloat(19.2569F, 3.149606F);
            this.xrLabel_宫颈涂片.Name = "xrLabel_宫颈涂片";
            this.xrLabel_宫颈涂片.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_宫颈涂片.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_宫颈涂片.StylePriority.UseBorders = false;
            // 
            // xrTableCell354
            // 
            this.xrTableCell354.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell354.Name = "xrTableCell354";
            this.xrTableCell354.StylePriority.UseBorders = false;
            this.xrTableCell354.Weight = 0.19876670037244218D;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell339,
            this.xrTableCell340,
            this.xrTable_辅助检查其他,
            this.xrTableCell342});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 1D;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.StylePriority.UseBorders = false;
            this.xrTableCell339.Weight = 0.11161843716525768D;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.StylePriority.UseBorders = false;
            this.xrTableCell340.StylePriority.UseTextAlignment = false;
            this.xrTableCell340.Text = "  其    他*";
            this.xrTableCell340.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell340.Weight = 0.30993814107294138D;
            // 
            // xrTable_辅助检查其他
            // 
            this.xrTable_辅助检查其他.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_辅助检查其他.Name = "xrTable_辅助检查其他";
            this.xrTable_辅助检查其他.StylePriority.UseBorders = false;
            this.xrTable_辅助检查其他.Weight = 1.5784434217618009D;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.StylePriority.UseBorders = false;
            this.xrTableCell342.Weight = 0.19876670037244218D;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell347,
            this.xrTableCell348,
            this.xrTableCell349,
            this.xrTableCell350});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 1D;
            // 
            // xrTableCell347
            // 
            this.xrTableCell347.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell347.Name = "xrTableCell347";
            this.xrTableCell347.StylePriority.UseBorders = false;
            this.xrTableCell347.Weight = 0.11161843716525768D;
            // 
            // xrTableCell348
            // 
            this.xrTableCell348.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell348.Name = "xrTableCell348";
            this.xrTableCell348.StylePriority.UseBorders = false;
            this.xrTableCell348.StylePriority.UseTextAlignment = false;
            this.xrTableCell348.Text = "脑血管疾病";
            this.xrTableCell348.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell348.Weight = 0.30993814107294138D;
            // 
            // xrTableCell349
            // 
            this.xrTableCell349.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell349.Name = "xrTableCell349";
            this.xrTableCell349.StylePriority.UseBorders = false;
            this.xrTableCell349.StylePriority.UseTextAlignment = false;
            this.xrTableCell349.Text = "  1 未发现    2 缺血性卒中    3 脑出血    4 蛛网膜下腔出血    5 短暂性脑缺血发作";
            this.xrTableCell349.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell349.Weight = 1.5784434217618009D;
            // 
            // xrTableCell350
            // 
            this.xrTableCell350.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell350.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox主要现存问题});
            this.xrTableCell350.Name = "xrTableCell350";
            this.xrTableCell350.StylePriority.UseBorders = false;
            this.xrTableCell350.Weight = 0.19876670037244218D;
            // 
            // xrPictureBox主要现存问题
            // 
            this.xrPictureBox主要现存问题.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox主要现存问题.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox主要现存问题.Name = "xrPictureBox主要现存问题";
            this.xrPictureBox主要现存问题.SizeF = new System.Drawing.SizeF(68F, 25F);
            this.xrPictureBox主要现存问题.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox主要现存问题.StylePriority.UseBorders = false;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell343,
            this.xrTableCell344,
            this.xrTableCell429,
            this.xrTableCell430,
            this.xrTableCell345,
            this.xrTableCell346});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 1D;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.StylePriority.UseBorders = false;
            this.xrTableCell343.Weight = 0.11161843716525768D;
            // 
            // xrTableCell344
            // 
            this.xrTableCell344.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell344.Name = "xrTableCell344";
            this.xrTableCell344.StylePriority.UseBorders = false;
            this.xrTableCell344.StylePriority.UseTextAlignment = false;
            this.xrTableCell344.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell344.Weight = 0.30993814107294138D;
            // 
            // xrTableCell429
            // 
            this.xrTableCell429.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell429.Name = "xrTableCell429";
            this.xrTableCell429.StylePriority.UseBorders = false;
            this.xrTableCell429.StylePriority.UseTextAlignment = false;
            this.xrTableCell429.Text = "  6 其他";
            this.xrTableCell429.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell429.Weight = 0.15140325012057609D;
            // 
            // xrTableCell430
            // 
            this.xrTableCell430.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell430.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_脑血管疾病其他});
            this.xrTableCell430.Name = "xrTableCell430";
            this.xrTableCell430.StylePriority.UseBorders = false;
            this.xrTableCell430.Weight = 0.57340167083644622D;
            // 
            // xrLabel_脑血管疾病其他
            // 
            this.xrLabel_脑血管疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_脑血管疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(5.086264E-05F, 0F);
            this.xrLabel_脑血管疾病其他.Name = "xrLabel_脑血管疾病其他";
            this.xrLabel_脑血管疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_脑血管疾病其他.SizeF = new System.Drawing.SizeF(206.2952F, 19.66659F);
            this.xrLabel_脑血管疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_脑血管疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_脑血管疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell345
            // 
            this.xrTableCell345.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell345.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel64,
            this.xrLabel_脑血管疾病1,
            this.xrLabel_脑血管疾病2,
            this.xrLabel58,
            this.xrLabel_脑血管疾病3,
            this.xrLabel54,
            this.xrLabel_脑血管疾病4,
            this.xrLabel50,
            this.xrLabel_脑血管疾病5});
            this.xrTableCell345.Name = "xrTableCell345";
            this.xrTableCell345.StylePriority.UseBorders = false;
            this.xrTableCell345.Weight = 0.85363850080477854D;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel64.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(165.2021F, 3.149643F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "/";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_脑血管疾病1
            // 
            this.xrLabel_脑血管疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脑血管疾病1.LocationFloat = new DevExpress.Utils.PointFloat(148.6374F, 3.149668F);
            this.xrLabel_脑血管疾病1.Name = "xrLabel_脑血管疾病1";
            this.xrLabel_脑血管疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_脑血管疾病1.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_脑血管疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel_脑血管疾病2
            // 
            this.xrLabel_脑血管疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脑血管疾病2.LocationFloat = new DevExpress.Utils.PointFloat(181.7112F, 3.149618F);
            this.xrLabel_脑血管疾病2.Name = "xrLabel_脑血管疾病2";
            this.xrLabel_脑血管疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_脑血管疾病2.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_脑血管疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel58
            // 
            this.xrLabel58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel58.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(198.2203F, 3.149618F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel58.StylePriority.UseBorders = false;
            this.xrLabel58.StylePriority.UseFont = false;
            this.xrLabel58.StylePriority.UseTextAlignment = false;
            this.xrLabel58.Text = "/";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_脑血管疾病3
            // 
            this.xrLabel_脑血管疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脑血管疾病3.LocationFloat = new DevExpress.Utils.PointFloat(214.7295F, 3.149618F);
            this.xrLabel_脑血管疾病3.Name = "xrLabel_脑血管疾病3";
            this.xrLabel_脑血管疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_脑血管疾病3.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_脑血管疾病3.StylePriority.UseBorders = false;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel54.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(231.2386F, 3.149618F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "/";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_脑血管疾病4
            // 
            this.xrLabel_脑血管疾病4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脑血管疾病4.LocationFloat = new DevExpress.Utils.PointFloat(247.7476F, 3.149618F);
            this.xrLabel_脑血管疾病4.Name = "xrLabel_脑血管疾病4";
            this.xrLabel_脑血管疾病4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_脑血管疾病4.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_脑血管疾病4.StylePriority.UseBorders = false;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel50.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(264.2567F, 3.149592F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel50.StylePriority.UseBorders = false;
            this.xrLabel50.StylePriority.UseFont = false;
            this.xrLabel50.StylePriority.UseTextAlignment = false;
            this.xrLabel50.Text = "/";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_脑血管疾病5
            // 
            this.xrLabel_脑血管疾病5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脑血管疾病5.LocationFloat = new DevExpress.Utils.PointFloat(280.7658F, 3.149606F);
            this.xrLabel_脑血管疾病5.Name = "xrLabel_脑血管疾病5";
            this.xrLabel_脑血管疾病5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_脑血管疾病5.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_脑血管疾病5.StylePriority.UseBorders = false;
            // 
            // xrTableCell346
            // 
            this.xrTableCell346.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell346.Name = "xrTableCell346";
            this.xrTableCell346.StylePriority.UseBorders = false;
            this.xrTableCell346.Weight = 0.19876670037244218D;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell331,
            this.xrTableCell332,
            this.xrTableCell333,
            this.xrTableCell334});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 1D;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.StylePriority.UseBorders = false;
            this.xrTableCell331.Text = "现";
            this.xrTableCell331.Weight = 0.11161843716525768D;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.StylePriority.UseBorders = false;
            this.xrTableCell332.StylePriority.UseTextAlignment = false;
            this.xrTableCell332.Text = "肾脏疾病";
            this.xrTableCell332.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell332.Weight = 0.30993814107294138D;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.StylePriority.UseBorders = false;
            this.xrTableCell333.StylePriority.UseTextAlignment = false;
            this.xrTableCell333.Text = "  1 未发现    2 糖尿病肾病    3 肾功能衰竭    4 急性肾炎    5 慢性肾炎";
            this.xrTableCell333.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell333.Weight = 1.5784434217618009D;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.StylePriority.UseBorders = false;
            this.xrTableCell334.Weight = 0.19876670037244218D;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell335,
            this.xrTableCell336,
            this.xrTableCell431,
            this.xrTableCell432,
            this.xrTableCell337,
            this.xrTableCell338});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 1D;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.StylePriority.UseBorders = false;
            this.xrTableCell335.Text = "存";
            this.xrTableCell335.Weight = 0.11161843716525768D;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.StylePriority.UseBorders = false;
            this.xrTableCell336.StylePriority.UseTextAlignment = false;
            this.xrTableCell336.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell336.Weight = 0.30993814107294138D;
            // 
            // xrTableCell431
            // 
            this.xrTableCell431.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell431.Name = "xrTableCell431";
            this.xrTableCell431.StylePriority.UseBorders = false;
            this.xrTableCell431.StylePriority.UseTextAlignment = false;
            this.xrTableCell431.Text = "  6 其他";
            this.xrTableCell431.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell431.Weight = 0.15140325012057609D;
            // 
            // xrTableCell432
            // 
            this.xrTableCell432.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell432.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_肾脏疾病其他});
            this.xrTableCell432.Name = "xrTableCell432";
            this.xrTableCell432.StylePriority.UseBorders = false;
            this.xrTableCell432.Weight = 0.57340167083644622D;
            // 
            // xrLabel_肾脏疾病其他
            // 
            this.xrLabel_肾脏疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_肾脏疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_肾脏疾病其他.Name = "xrLabel_肾脏疾病其他";
            this.xrLabel_肾脏疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_肾脏疾病其他.SizeF = new System.Drawing.SizeF(206.2952F, 19.66659F);
            this.xrLabel_肾脏疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_肾脏疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_肾脏疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell337.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel65,
            this.xrLabel_肾脏疾病1,
            this.xrLabel_肾脏疾病2,
            this.xrLabel59,
            this.xrLabel_肾脏疾病3,
            this.xrLabel55,
            this.xrLabel_肾脏疾病4,
            this.xrLabel51,
            this.xrLabel_肾脏疾病5});
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.StylePriority.UseBorders = false;
            this.xrTableCell337.Weight = 0.85363850080477854D;
            // 
            // xrLabel65
            // 
            this.xrLabel65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel65.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(165.1465F, 3.149592F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel65.StylePriority.UseBorders = false;
            this.xrLabel65.StylePriority.UseFont = false;
            this.xrLabel65.StylePriority.UseTextAlignment = false;
            this.xrLabel65.Text = "/";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_肾脏疾病1
            // 
            this.xrLabel_肾脏疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肾脏疾病1.LocationFloat = new DevExpress.Utils.PointFloat(148.6374F, 3.149592F);
            this.xrLabel_肾脏疾病1.Name = "xrLabel_肾脏疾病1";
            this.xrLabel_肾脏疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_肾脏疾病1.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_肾脏疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel_肾脏疾病2
            // 
            this.xrLabel_肾脏疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肾脏疾病2.LocationFloat = new DevExpress.Utils.PointFloat(181.7112F, 3.149618F);
            this.xrLabel_肾脏疾病2.Name = "xrLabel_肾脏疾病2";
            this.xrLabel_肾脏疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_肾脏疾病2.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_肾脏疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel59.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(198.2203F, 3.149618F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel59.StylePriority.UseBorders = false;
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = "/";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_肾脏疾病3
            // 
            this.xrLabel_肾脏疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肾脏疾病3.LocationFloat = new DevExpress.Utils.PointFloat(214.7295F, 3.149592F);
            this.xrLabel_肾脏疾病3.Name = "xrLabel_肾脏疾病3";
            this.xrLabel_肾脏疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_肾脏疾病3.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_肾脏疾病3.StylePriority.UseBorders = false;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel55.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(231.2386F, 3.149592F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel55.StylePriority.UseBorders = false;
            this.xrLabel55.StylePriority.UseFont = false;
            this.xrLabel55.StylePriority.UseTextAlignment = false;
            this.xrLabel55.Text = "/";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_肾脏疾病4
            // 
            this.xrLabel_肾脏疾病4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肾脏疾病4.LocationFloat = new DevExpress.Utils.PointFloat(247.7476F, 3.149592F);
            this.xrLabel_肾脏疾病4.Name = "xrLabel_肾脏疾病4";
            this.xrLabel_肾脏疾病4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_肾脏疾病4.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_肾脏疾病4.StylePriority.UseBorders = false;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(264.2567F, 3.149592F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "/";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_肾脏疾病5
            // 
            this.xrLabel_肾脏疾病5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肾脏疾病5.LocationFloat = new DevExpress.Utils.PointFloat(280.7657F, 3.149606F);
            this.xrLabel_肾脏疾病5.Name = "xrLabel_肾脏疾病5";
            this.xrLabel_肾脏疾病5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_肾脏疾病5.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_肾脏疾病5.StylePriority.UseBorders = false;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.StylePriority.UseBorders = false;
            this.xrTableCell338.Weight = 0.19876670037244218D;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell327,
            this.xrTableCell328,
            this.xrTableCell329,
            this.xrTableCell330});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 1D;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.StylePriority.UseBorders = false;
            this.xrTableCell327.Text = "主";
            this.xrTableCell327.Weight = 0.11161843716525768D;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.StylePriority.UseBorders = false;
            this.xrTableCell328.StylePriority.UseTextAlignment = false;
            this.xrTableCell328.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell328.Weight = 0.30993814107294138D;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.StylePriority.UseBorders = false;
            this.xrTableCell329.StylePriority.UseTextAlignment = false;
            this.xrTableCell329.Text = "  1 未发现   2 心肌梗死   3 心绞痛   4 冠状动脉血运重建   5 充血性心力衰竭    6 心";
            this.xrTableCell329.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell329.Weight = 1.5784434217618009D;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.StylePriority.UseBorders = false;
            this.xrTableCell330.Weight = 0.19876670037244218D;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell319,
            this.xrTableCell320,
            this.xrTableCell433,
            this.xrTableCell321,
            this.xrTableCell322});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 1D;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.StylePriority.UseBorders = false;
            this.xrTableCell319.Text = "要";
            this.xrTableCell319.Weight = 0.11161843716525768D;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.StylePriority.UseBorders = false;
            this.xrTableCell320.StylePriority.UseTextAlignment = false;
            this.xrTableCell320.Text = "心血管疾病";
            this.xrTableCell320.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell320.Weight = 0.30993814107294138D;
            // 
            // xrTableCell433
            // 
            this.xrTableCell433.Name = "xrTableCell433";
            this.xrTableCell433.StylePriority.UseTextAlignment = false;
            this.xrTableCell433.Text = " 前区疼痛   7 高血压   8 夹层动脉瘤   9 动脉闭塞性疾病  10 其他";
            this.xrTableCell433.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell433.Weight = 1.2107821267254424D;
            // 
            // xrTableCell321
            // 
            this.xrTableCell321.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell321.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心血管疾病其他});
            this.xrTableCell321.Name = "xrTableCell321";
            this.xrTableCell321.StylePriority.UseBorders = false;
            this.xrTableCell321.Weight = 0.36766129503635853D;
            // 
            // xrLabel_心血管疾病其他
            // 
            this.xrLabel_心血管疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_心血管疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(9.611835E-05F, 7.208878E-05F);
            this.xrLabel_心血管疾病其他.Name = "xrLabel_心血管疾病其他";
            this.xrLabel_心血管疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心血管疾病其他.SizeF = new System.Drawing.SizeF(122.4326F, 19.66659F);
            this.xrLabel_心血管疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_心血管疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_心血管疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell322
            // 
            this.xrTableCell322.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell322.Name = "xrTableCell322";
            this.xrTableCell322.StylePriority.UseBorders = false;
            this.xrTableCell322.Weight = 0.19876670037244218D;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell311,
            this.xrTableCell312,
            this.xrTableCell313,
            this.xrTableCell314});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 1D;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.StylePriority.UseBorders = false;
            this.xrTableCell311.Text = "健";
            this.xrTableCell311.Weight = 0.11161843716525768D;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.StylePriority.UseBorders = false;
            this.xrTableCell312.StylePriority.UseTextAlignment = false;
            this.xrTableCell312.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell312.Weight = 0.30993814107294138D;
            // 
            // xrTableCell313
            // 
            this.xrTableCell313.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell313.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel66,
            this.xrLabel_心血管疾病1,
            this.xrLabel_心血管疾病2,
            this.xrLabel69,
            this.xrLabel_心血管疾病3,
            this.xrLabel71,
            this.xrLabel_心血管疾病4,
            this.xrLabel73,
            this.xrLabel_心血管疾病5});
            this.xrTableCell313.Name = "xrTableCell313";
            this.xrTableCell313.StylePriority.UseBorders = false;
            this.xrTableCell313.Weight = 1.5784434217618009D;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(416.4402F, 3.149618F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel66.StylePriority.UseBorders = false;
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.StylePriority.UseTextAlignment = false;
            this.xrLabel66.Text = "/";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心血管疾病1
            // 
            this.xrLabel_心血管疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心血管疾病1.LocationFloat = new DevExpress.Utils.PointFloat(396.7551F, 3.149618F);
            this.xrLabel_心血管疾病1.Name = "xrLabel_心血管疾病1";
            this.xrLabel_心血管疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心血管疾病1.SizeF = new System.Drawing.SizeF(19.68506F, 18.83338F);
            this.xrLabel_心血管疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel_心血管疾病2
            // 
            this.xrLabel_心血管疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心血管疾病2.LocationFloat = new DevExpress.Utils.PointFloat(432.9496F, 3.149618F);
            this.xrLabel_心血管疾病2.Name = "xrLabel_心血管疾病2";
            this.xrLabel_心血管疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心血管疾病2.SizeF = new System.Drawing.SizeF(19.68503F, 18.83338F);
            this.xrLabel_心血管疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel69.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(452.6346F, 3.149618F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel69.StylePriority.UseBorders = false;
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.Text = "/";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心血管疾病3
            // 
            this.xrLabel_心血管疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心血管疾病3.LocationFloat = new DevExpress.Utils.PointFloat(469.1437F, 3.149618F);
            this.xrLabel_心血管疾病3.Name = "xrLabel_心血管疾病3";
            this.xrLabel_心血管疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心血管疾病3.SizeF = new System.Drawing.SizeF(19.68503F, 18.83338F);
            this.xrLabel_心血管疾病3.StylePriority.UseBorders = false;
            // 
            // xrLabel71
            // 
            this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel71.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(488.8287F, 3.149618F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel71.StylePriority.UseBorders = false;
            this.xrLabel71.StylePriority.UseFont = false;
            this.xrLabel71.StylePriority.UseTextAlignment = false;
            this.xrLabel71.Text = "/";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心血管疾病4
            // 
            this.xrLabel_心血管疾病4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心血管疾病4.LocationFloat = new DevExpress.Utils.PointFloat(505.3382F, 3.149606F);
            this.xrLabel_心血管疾病4.Name = "xrLabel_心血管疾病4";
            this.xrLabel_心血管疾病4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心血管疾病4.SizeF = new System.Drawing.SizeF(19.68503F, 18.83338F);
            this.xrLabel_心血管疾病4.StylePriority.UseBorders = false;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel73.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(525.0232F, 3.149626F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel73.StylePriority.UseBorders = false;
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "/";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_心血管疾病5
            // 
            this.xrLabel_心血管疾病5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心血管疾病5.LocationFloat = new DevExpress.Utils.PointFloat(541.5319F, 3.149624F);
            this.xrLabel_心血管疾病5.Name = "xrLabel_心血管疾病5";
            this.xrLabel_心血管疾病5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心血管疾病5.SizeF = new System.Drawing.SizeF(19.68506F, 18.83338F);
            this.xrLabel_心血管疾病5.StylePriority.UseBorders = false;
            this.xrLabel_心血管疾病5.StylePriority.UseTextAlignment = false;
            this.xrLabel_心血管疾病5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.StylePriority.UseBorders = false;
            this.xrTableCell314.Weight = 0.19876670037244218D;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell303,
            this.xrTableCell304,
            this.xrTableCell305,
            this.xrTableCell306});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 1D;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.StylePriority.UseBorders = false;
            this.xrTableCell303.Text = "康";
            this.xrTableCell303.Weight = 0.11161843716525768D;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.StylePriority.UseBorders = false;
            this.xrTableCell304.StylePriority.UseTextAlignment = false;
            this.xrTableCell304.Text = "眼部疾病";
            this.xrTableCell304.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell304.Weight = 0.30993814107294138D;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.StylePriority.UseBorders = false;
            this.xrTableCell305.StylePriority.UseTextAlignment = false;
            this.xrTableCell305.Text = "  1 未发现      2 视网膜出血或渗出      3 视乳头水肿      4 白内障";
            this.xrTableCell305.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell305.Weight = 1.5784434217618009D;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.StylePriority.UseBorders = false;
            this.xrTableCell306.Weight = 0.19876670037244218D;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell299,
            this.xrTableCell300,
            this.xrTableCell434,
            this.xrTableCell435,
            this.xrTableCell301,
            this.xrTableCell302});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 1D;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.StylePriority.UseBorders = false;
            this.xrTableCell299.Text = "问";
            this.xrTableCell299.Weight = 0.11161843716525768D;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.StylePriority.UseBorders = false;
            this.xrTableCell300.StylePriority.UseTextAlignment = false;
            this.xrTableCell300.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell300.Weight = 0.30993814107294138D;
            // 
            // xrTableCell434
            // 
            this.xrTableCell434.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell434.Name = "xrTableCell434";
            this.xrTableCell434.StylePriority.UseBorders = false;
            this.xrTableCell434.StylePriority.UseTextAlignment = false;
            this.xrTableCell434.Text = "  5 其他";
            this.xrTableCell434.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell434.Weight = 0.15140325012057609D;
            // 
            // xrTableCell435
            // 
            this.xrTableCell435.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell435.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_眼部疾病其他});
            this.xrTableCell435.Name = "xrTableCell435";
            this.xrTableCell435.StylePriority.UseBorders = false;
            this.xrTableCell435.Weight = 0.57340193799913552D;
            // 
            // xrLabel_眼部疾病其他
            // 
            this.xrLabel_眼部疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_眼部疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(5.086264E-05F, 0F);
            this.xrLabel_眼部疾病其他.Name = "xrLabel_眼部疾病其他";
            this.xrLabel_眼部疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_眼部疾病其他.SizeF = new System.Drawing.SizeF(206.2952F, 19.66659F);
            this.xrLabel_眼部疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_眼部疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_眼部疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell301.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_眼部疾病1,
            this.xrLabel84,
            this.xrLabel_眼部疾病2,
            this.xrLabel78,
            this.xrLabel_眼部疾病3});
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.StylePriority.UseBorders = false;
            this.xrTableCell301.Weight = 0.85363823364208924D;
            // 
            // xrLabel_眼部疾病1
            // 
            this.xrLabel_眼部疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_眼部疾病1.LocationFloat = new DevExpress.Utils.PointFloat(214.7291F, 3.149618F);
            this.xrLabel_眼部疾病1.Name = "xrLabel_眼部疾病1";
            this.xrLabel_眼部疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_眼部疾病1.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_眼部疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel84.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(231.2382F, 3.149618F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.StylePriority.UseFont = false;
            this.xrLabel84.StylePriority.UseTextAlignment = false;
            this.xrLabel84.Text = "/";
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_眼部疾病2
            // 
            this.xrLabel_眼部疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_眼部疾病2.LocationFloat = new DevExpress.Utils.PointFloat(247.7473F, 3.149566F);
            this.xrLabel_眼部疾病2.Name = "xrLabel_眼部疾病2";
            this.xrLabel_眼部疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_眼部疾病2.SizeF = new System.Drawing.SizeF(16.50911F, 18.83338F);
            this.xrLabel_眼部疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel78.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(264.2565F, 3.149618F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel78.StylePriority.UseBorders = false;
            this.xrLabel78.StylePriority.UseFont = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "/";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_眼部疾病3
            // 
            this.xrLabel_眼部疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_眼部疾病3.LocationFloat = new DevExpress.Utils.PointFloat(280.7656F, 3.149606F);
            this.xrLabel_眼部疾病3.Name = "xrLabel_眼部疾病3";
            this.xrLabel_眼部疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_眼部疾病3.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_眼部疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.StylePriority.UseBorders = false;
            this.xrTableCell302.Weight = 0.19876670037244218D;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell287,
            this.xrTableCell288,
            this.xrTableCell436,
            this.xrTableCell289,
            this.xrTableCell290});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 1D;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.StylePriority.UseBorders = false;
            this.xrTableCell287.Text = "题";
            this.xrTableCell287.Weight = 0.11161843716525768D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.StylePriority.UseBorders = false;
            this.xrTableCell288.StylePriority.UseTextAlignment = false;
            this.xrTableCell288.Text = "神经系统其他疾";
            this.xrTableCell288.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell288.Weight = 0.30993814107294138D;
            // 
            // xrTableCell436
            // 
            this.xrTableCell436.Name = "xrTableCell436";
            this.xrTableCell436.StylePriority.UseTextAlignment = false;
            this.xrTableCell436.Text = "  1 未发现  2 阿尔茨海默症（老年性痴呆）  3 帕金森症  4 其他";
            this.xrTableCell436.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell436.Weight = 1.17008954224211D;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell289.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_神经系统其他疾病其他});
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.StylePriority.UseBorders = false;
            this.xrTableCell289.Weight = 0.40835387951969065D;
            // 
            // xrLabel_神经系统其他疾病其他
            // 
            this.xrLabel_神经系统其他疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_神经系统其他疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_神经系统其他疾病其他.Name = "xrLabel_神经系统其他疾病其他";
            this.xrLabel_神经系统其他疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_神经系统其他疾病其他.SizeF = new System.Drawing.SizeF(137.0728F, 19.66659F);
            this.xrLabel_神经系统其他疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_神经系统其他疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_神经系统其他疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.StylePriority.UseBorders = false;
            this.xrTableCell290.Weight = 0.19876670037244218D;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell291,
            this.xrTableCell292,
            this.xrTableCell293,
            this.xrTableCell294});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1D;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.StylePriority.UseBorders = false;
            this.xrTableCell291.Weight = 0.11161843716525768D;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.StylePriority.UseBorders = false;
            this.xrTableCell292.StylePriority.UseTextAlignment = false;
            this.xrTableCell292.Text = "病";
            this.xrTableCell292.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell292.Weight = 0.30993814107294138D;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell293.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_神经系统其他疾病1,
            this.xrLabel85,
            this.xrLabel_神经系统其他疾病2,
            this.xrLabel79,
            this.xrLabel_神经系统其他疾病3});
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.StylePriority.UseBorders = false;
            this.xrTableCell293.Weight = 1.5784434217618009D;
            // 
            // xrLabel_神经系统其他疾病1
            // 
            this.xrLabel_神经系统其他疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_神经系统其他疾病1.LocationFloat = new DevExpress.Utils.PointFloat(475.4957F, 3.149719F);
            this.xrLabel_神经系统其他疾病1.Name = "xrLabel_神经系统其他疾病1";
            this.xrLabel_神经系统其他疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_神经系统其他疾病1.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_神经系统其他疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel85
            // 
            this.xrLabel85.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel85.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel85.LocationFloat = new DevExpress.Utils.PointFloat(492.0047F, 3.149668F);
            this.xrLabel85.Name = "xrLabel85";
            this.xrLabel85.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel85.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel85.StylePriority.UseBorders = false;
            this.xrLabel85.StylePriority.UseFont = false;
            this.xrLabel85.StylePriority.UseTextAlignment = false;
            this.xrLabel85.Text = "/";
            this.xrLabel85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_神经系统其他疾病2
            // 
            this.xrLabel_神经系统其他疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_神经系统其他疾病2.LocationFloat = new DevExpress.Utils.PointFloat(508.5138F, 3.149566F);
            this.xrLabel_神经系统其他疾病2.Name = "xrLabel_神经系统其他疾病2";
            this.xrLabel_神经系统其他疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_神经系统其他疾病2.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_神经系统其他疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel79
            // 
            this.xrLabel79.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel79.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(525.0228F, 3.149566F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel79.StylePriority.UseBorders = false;
            this.xrLabel79.StylePriority.UseFont = false;
            this.xrLabel79.StylePriority.UseTextAlignment = false;
            this.xrLabel79.Text = "/";
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_神经系统其他疾病3
            // 
            this.xrLabel_神经系统其他疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_神经系统其他疾病3.LocationFloat = new DevExpress.Utils.PointFloat(541.5319F, 3.149606F);
            this.xrLabel_神经系统其他疾病3.Name = "xrLabel_神经系统其他疾病3";
            this.xrLabel_神经系统其他疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_神经系统其他疾病3.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_神经系统其他疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.StylePriority.UseBorders = false;
            this.xrTableCell294.Weight = 0.19876670037244218D;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell364,
            this.xrTableCell365,
            this.xrTableCell366,
            this.xrTableCell367});
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Weight = 1D;
            // 
            // xrTableCell364
            // 
            this.xrTableCell364.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell364.Name = "xrTableCell364";
            this.xrTableCell364.StylePriority.UseBorders = false;
            this.xrTableCell364.Weight = 0.11161843716525768D;
            // 
            // xrTableCell365
            // 
            this.xrTableCell365.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell365.Name = "xrTableCell365";
            this.xrTableCell365.StylePriority.UseBorders = false;
            this.xrTableCell365.StylePriority.UseTextAlignment = false;
            this.xrTableCell365.Text = "其他系统疾病";
            this.xrTableCell365.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell365.Weight = 0.30993814107294138D;
            // 
            // xrTableCell366
            // 
            this.xrTableCell366.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell366.Name = "xrTableCell366";
            this.xrTableCell366.StylePriority.UseBorders = false;
            this.xrTableCell366.StylePriority.UseTextAlignment = false;
            this.xrTableCell366.Text = "  1 未发现    2 糖尿病     3 慢性支气管炎     4 慢性阻塞性肺气肿    5 恶性肿瘤";
            this.xrTableCell366.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell366.Weight = 1.5784434217618009D;
            // 
            // xrTableCell367
            // 
            this.xrTableCell367.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell367.Name = "xrTableCell367";
            this.xrTableCell367.StylePriority.UseBorders = false;
            this.xrTableCell367.Weight = 0.19876670037244218D;
            // 
            // xrTableRow73
            // 
            this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell372,
            this.xrTableCell373,
            this.xrTableCell437,
            this.xrTableCell438,
            this.xrTableCell374,
            this.xrTableCell375});
            this.xrTableRow73.Name = "xrTableRow73";
            this.xrTableRow73.Weight = 1D;
            // 
            // xrTableCell372
            // 
            this.xrTableCell372.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell372.Name = "xrTableCell372";
            this.xrTableCell372.StylePriority.UseBorders = false;
            this.xrTableCell372.Weight = 0.11161843716525768D;
            // 
            // xrTableCell373
            // 
            this.xrTableCell373.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell373.Name = "xrTableCell373";
            this.xrTableCell373.StylePriority.UseBorders = false;
            this.xrTableCell373.StylePriority.UseTextAlignment = false;
            this.xrTableCell373.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell373.Weight = 0.30993814107294138D;
            // 
            // xrTableCell437
            // 
            this.xrTableCell437.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell437.Name = "xrTableCell437";
            this.xrTableCell437.StylePriority.UseBorders = false;
            this.xrTableCell437.StylePriority.UseTextAlignment = false;
            this.xrTableCell437.Text = "  6 老年性骨关节病     7 其他";
            this.xrTableCell437.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell437.Weight = 0.5636983303141524D;
            // 
            // xrTableCell438
            // 
            this.xrTableCell438.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell438.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_其他系统疾病其他});
            this.xrTableCell438.Name = "xrTableCell438";
            this.xrTableCell438.StylePriority.UseBorders = false;
            this.xrTableCell438.Weight = 0.57340086934837786D;
            // 
            // xrLabel_其他系统疾病其他
            // 
            this.xrLabel_其他系统疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_其他系统疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.086264E-05F);
            this.xrLabel_其他系统疾病其他.Name = "xrLabel_其他系统疾病其他";
            this.xrLabel_其他系统疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_其他系统疾病其他.SizeF = new System.Drawing.SizeF(206.2952F, 19.66659F);
            this.xrLabel_其他系统疾病其他.StylePriority.UseBorders = false;
            this.xrLabel_其他系统疾病其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_其他系统疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell374
            // 
            this.xrTableCell374.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell374.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_其他系统疾病1,
            this.xrLabel86,
            this.xrLabel_其他系统疾病2,
            this.xrLabel80,
            this.xrLabel_其他系统疾病3});
            this.xrTableCell374.Name = "xrTableCell374";
            this.xrTableCell374.StylePriority.UseBorders = false;
            this.xrTableCell374.Weight = 0.44134422209927054D;
            // 
            // xrLabel_其他系统疾病1
            // 
            this.xrLabel_其他系统疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_其他系统疾病1.LocationFloat = new DevExpress.Utils.PointFloat(66.39649F, 3.149566F);
            this.xrLabel_其他系统疾病1.Name = "xrLabel_其他系统疾病1";
            this.xrLabel_其他系统疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_其他系统疾病1.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_其他系统疾病1.StylePriority.UseBorders = false;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel86.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(82.90547F, 3.149566F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel86.StylePriority.UseBorders = false;
            this.xrLabel86.StylePriority.UseFont = false;
            this.xrLabel86.StylePriority.UseTextAlignment = false;
            this.xrLabel86.Text = "/";
            this.xrLabel86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_其他系统疾病2
            // 
            this.xrLabel_其他系统疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_其他系统疾病2.LocationFloat = new DevExpress.Utils.PointFloat(99.41469F, 3.149618F);
            this.xrLabel_其他系统疾病2.Name = "xrLabel_其他系统疾病2";
            this.xrLabel_其他系统疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_其他系统疾病2.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_其他系统疾病2.StylePriority.UseBorders = false;
            // 
            // xrLabel80
            // 
            this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel80.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(115.9235F, 3.149618F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel80.StylePriority.UseBorders = false;
            this.xrLabel80.StylePriority.UseFont = false;
            this.xrLabel80.StylePriority.UseTextAlignment = false;
            this.xrLabel80.Text = "/";
            this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_其他系统疾病3
            // 
            this.xrLabel_其他系统疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_其他系统疾病3.LocationFloat = new DevExpress.Utils.PointFloat(132.4326F, 3.149606F);
            this.xrLabel_其他系统疾病3.Name = "xrLabel_其他系统疾病3";
            this.xrLabel_其他系统疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_其他系统疾病3.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_其他系统疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableCell375
            // 
            this.xrTableCell375.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell375.Name = "xrTableCell375";
            this.xrTableCell375.StylePriority.UseBorders = false;
            this.xrTableCell375.Weight = 0.19876670037244218D;
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell368,
            this.xrTableCell369,
            this.xrTableCell454,
            this.xrTableCell452,
            this.xrTableCell453,
            this.xrTableCell370,
            this.xrTableCell371});
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Weight = 1D;
            // 
            // xrTableCell368
            // 
            this.xrTableCell368.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell368.Name = "xrTableCell368";
            this.xrTableCell368.StylePriority.UseBorders = false;
            this.xrTableCell368.Weight = 0.11161843716525768D;
            // 
            // xrTableCell369
            // 
            this.xrTableCell369.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell369.Name = "xrTableCell369";
            this.xrTableCell369.StylePriority.UseBorders = false;
            this.xrTableCell369.StylePriority.UseTextAlignment = false;
            this.xrTableCell369.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell369.Weight = 0.30993814107294138D;
            // 
            // xrTableCell454
            // 
            this.xrTableCell454.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell454.Name = "xrTableCell454";
            this.xrTableCell454.StylePriority.UseBorders = false;
            this.xrTableCell454.Text = "入/出院日期";
            this.xrTableCell454.Weight = 0.4155718222398348D;
            // 
            // xrTableCell452
            // 
            this.xrTableCell452.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell452.Name = "xrTableCell452";
            this.xrTableCell452.StylePriority.UseBorders = false;
            this.xrTableCell452.Text = "原    因";
            this.xrTableCell452.Weight = 0.41997521377395236D;
            // 
            // xrTableCell453
            // 
            this.xrTableCell453.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell453.Name = "xrTableCell453";
            this.xrTableCell453.StylePriority.UseBorders = false;
            this.xrTableCell453.Text = "医疗机构及科室名称";
            this.xrTableCell453.Weight = 0.40619193064266251D;
            // 
            // xrTableCell370
            // 
            this.xrTableCell370.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell370.Name = "xrTableCell370";
            this.xrTableCell370.StylePriority.UseBorders = false;
            this.xrTableCell370.Text = "病  案  号";
            this.xrTableCell370.Weight = 0.33670445510535119D;
            // 
            // xrTableCell371
            // 
            this.xrTableCell371.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell371.Name = "xrTableCell371";
            this.xrTableCell371.StylePriority.UseBorders = false;
            this.xrTableCell371.Weight = 0.19876670037244218D;
            // 
            // xrTableRow76
            // 
            this.xrTableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell384,
            this.xrTableCell385,
            this.xrTable_入院日期1,
            this.xrTableCell462,
            this.xrTable_出院日期1,
            this.xrTable_住院史原因1,
            this.xrTable_住院史机构及科室1,
            this.xrTable_住院史病案号1,
            this.xrTableCell387});
            this.xrTableRow76.Name = "xrTableRow76";
            this.xrTableRow76.Weight = 1D;
            // 
            // xrTableCell384
            // 
            this.xrTableCell384.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell384.Name = "xrTableCell384";
            this.xrTableCell384.StylePriority.UseBorders = false;
            this.xrTableCell384.Text = "住院";
            this.xrTableCell384.Weight = 0.11161843716525768D;
            // 
            // xrTableCell385
            // 
            this.xrTableCell385.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell385.Name = "xrTableCell385";
            this.xrTableCell385.StylePriority.UseBorders = false;
            this.xrTableCell385.StylePriority.UseTextAlignment = false;
            this.xrTableCell385.Text = "住院史";
            this.xrTableCell385.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell385.Weight = 0.30993814107294138D;
            // 
            // xrTable_入院日期1
            // 
            this.xrTable_入院日期1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_入院日期1.Name = "xrTable_入院日期1";
            this.xrTable_入院日期1.StylePriority.UseBorders = false;
            this.xrTable_入院日期1.Weight = 0.19309583705996553D;
            // 
            // xrTableCell462
            // 
            this.xrTableCell462.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell462.Name = "xrTableCell462";
            this.xrTableCell462.StylePriority.UseBorders = false;
            this.xrTableCell462.Text = "/";
            this.xrTableCell462.Weight = 0.029376405757334877D;
            // 
            // xrTable_出院日期1
            // 
            this.xrTable_出院日期1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_出院日期1.Name = "xrTable_出院日期1";
            this.xrTable_出院日期1.StylePriority.UseBorders = false;
            this.xrTable_出院日期1.Weight = 0.19309989667822813D;
            // 
            // xrTable_住院史原因1
            // 
            this.xrTable_住院史原因1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史原因1.Name = "xrTable_住院史原因1";
            this.xrTable_住院史原因1.StylePriority.UseBorders = false;
            this.xrTable_住院史原因1.Weight = 0.41997489651825864D;
            // 
            // xrTable_住院史机构及科室1
            // 
            this.xrTable_住院史机构及科室1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史机构及科室1.Name = "xrTable_住院史机构及科室1";
            this.xrTable_住院史机构及科室1.StylePriority.UseBorders = false;
            this.xrTable_住院史机构及科室1.Weight = 0.40619193064266251D;
            // 
            // xrTable_住院史病案号1
            // 
            this.xrTable_住院史病案号1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史病案号1.Name = "xrTable_住院史病案号1";
            this.xrTable_住院史病案号1.StylePriority.UseBorders = false;
            this.xrTable_住院史病案号1.Weight = 0.33670445510535119D;
            // 
            // xrTableCell387
            // 
            this.xrTableCell387.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell387.Name = "xrTableCell387";
            this.xrTableCell387.StylePriority.UseBorders = false;
            this.xrTableCell387.Weight = 0.19876670037244218D;
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell380,
            this.xrTableCell381,
            this.xrTable_入院日期2,
            this.xrTableCell464,
            this.xrTable_出院日期2,
            this.xrTable_住院史原因2,
            this.xrTable_住院史机构及科室2,
            this.xrTable_住院史病案号2,
            this.xrTableCell383});
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Weight = 1D;
            // 
            // xrTableCell380
            // 
            this.xrTableCell380.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell380.Name = "xrTableCell380";
            this.xrTableCell380.StylePriority.UseBorders = false;
            this.xrTableCell380.Text = "治疗";
            this.xrTableCell380.Weight = 0.11161843716525768D;
            // 
            // xrTableCell381
            // 
            this.xrTableCell381.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell381.Name = "xrTableCell381";
            this.xrTableCell381.StylePriority.UseBorders = false;
            this.xrTableCell381.StylePriority.UseTextAlignment = false;
            this.xrTableCell381.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell381.Weight = 0.30993814107294138D;
            // 
            // xrTable_入院日期2
            // 
            this.xrTable_入院日期2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_入院日期2.Name = "xrTable_入院日期2";
            this.xrTable_入院日期2.StylePriority.UseBorders = false;
            this.xrTable_入院日期2.Weight = 0.19309583705996553D;
            // 
            // xrTableCell464
            // 
            this.xrTableCell464.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell464.Name = "xrTableCell464";
            this.xrTableCell464.StylePriority.UseBorders = false;
            this.xrTableCell464.Text = "/";
            this.xrTableCell464.Weight = 0.029376407844543397D;
            // 
            // xrTable_出院日期2
            // 
            this.xrTable_出院日期2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_出院日期2.Name = "xrTable_出院日期2";
            this.xrTable_出院日期2.StylePriority.UseBorders = false;
            this.xrTable_出院日期2.Weight = 0.19309931017263649D;
            // 
            // xrTable_住院史原因2
            // 
            this.xrTable_住院史原因2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史原因2.Name = "xrTable_住院史原因2";
            this.xrTable_住院史原因2.StylePriority.UseBorders = false;
            this.xrTable_住院史原因2.Weight = 0.41997548093664178D;
            // 
            // xrTable_住院史机构及科室2
            // 
            this.xrTable_住院史机构及科室2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史机构及科室2.Name = "xrTable_住院史机构及科室2";
            this.xrTable_住院史机构及科室2.StylePriority.UseBorders = false;
            this.xrTable_住院史机构及科室2.Weight = 0.40619193064266251D;
            // 
            // xrTable_住院史病案号2
            // 
            this.xrTable_住院史病案号2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_住院史病案号2.Name = "xrTable_住院史病案号2";
            this.xrTable_住院史病案号2.StylePriority.UseBorders = false;
            this.xrTable_住院史病案号2.Weight = 0.33670445510535119D;
            // 
            // xrTableCell383
            // 
            this.xrTableCell383.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell383.Name = "xrTableCell383";
            this.xrTableCell383.StylePriority.UseBorders = false;
            this.xrTableCell383.Weight = 0.19876670037244218D;
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell376,
            this.xrTableCell377,
            this.xrTableCell389,
            this.xrTableCell388,
            this.xrTableCell390,
            this.xrTableCell378,
            this.xrTableCell379});
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Weight = 1D;
            // 
            // xrTableCell376
            // 
            this.xrTableCell376.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell376.Name = "xrTableCell376";
            this.xrTableCell376.StylePriority.UseBorders = false;
            this.xrTableCell376.Text = "情况";
            this.xrTableCell376.Weight = 0.11161843716525768D;
            // 
            // xrTableCell377
            // 
            this.xrTableCell377.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell377.Name = "xrTableCell377";
            this.xrTableCell377.StylePriority.UseBorders = false;
            this.xrTableCell377.StylePriority.UseTextAlignment = false;
            this.xrTableCell377.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell377.Weight = 0.30993814107294138D;
            // 
            // xrTableCell389
            // 
            this.xrTableCell389.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell389.Name = "xrTableCell389";
            this.xrTableCell389.StylePriority.UseBorders = false;
            this.xrTableCell389.Text = "建/撤床日期";
            this.xrTableCell389.Weight = 0.41557155507714538D;
            // 
            // xrTableCell388
            // 
            this.xrTableCell388.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell388.Name = "xrTableCell388";
            this.xrTableCell388.StylePriority.UseBorders = false;
            this.xrTableCell388.Text = "原    因";
            this.xrTableCell388.Weight = 0.41997548093664178D;
            // 
            // xrTableCell390
            // 
            this.xrTableCell390.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell390.Name = "xrTableCell390";
            this.xrTableCell390.StylePriority.UseBorders = false;
            this.xrTableCell390.Text = "医疗机构及科室名称";
            this.xrTableCell390.Weight = 0.40619193064266251D;
            // 
            // xrTableCell378
            // 
            this.xrTableCell378.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell378.Name = "xrTableCell378";
            this.xrTableCell378.StylePriority.UseBorders = false;
            this.xrTableCell378.Text = "病  案  号";
            this.xrTableCell378.Weight = 0.33670445510535119D;
            // 
            // xrTableCell379
            // 
            this.xrTableCell379.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell379.Name = "xrTableCell379";
            this.xrTableCell379.StylePriority.UseBorders = false;
            this.xrTableCell379.Weight = 0.19876670037244218D;
            // 
            // xrTableRow77
            // 
            this.xrTableRow77.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell391,
            this.xrTableCell439,
            this.xrTable_建床日期1,
            this.xrTableCell467,
            this.xrTable_撤床日期1,
            this.xrTable_家庭病床史原因1,
            this.xrTable_家庭病床史机构及科室1,
            this.xrTable_家庭病床史病案号1,
            this.xrTableCell444});
            this.xrTableRow77.Name = "xrTableRow77";
            this.xrTableRow77.Weight = 1D;
            // 
            // xrTableCell391
            // 
            this.xrTableCell391.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell391.Name = "xrTableCell391";
            this.xrTableCell391.StylePriority.UseBorders = false;
            this.xrTableCell391.Weight = 0.11161843716525768D;
            // 
            // xrTableCell439
            // 
            this.xrTableCell439.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell439.Name = "xrTableCell439";
            this.xrTableCell439.StylePriority.UseBorders = false;
            this.xrTableCell439.Text = "家庭病床史";
            this.xrTableCell439.Weight = 0.30993814107294138D;
            // 
            // xrTable_建床日期1
            // 
            this.xrTable_建床日期1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_建床日期1.Name = "xrTable_建床日期1";
            this.xrTable_建床日期1.StylePriority.UseBorders = false;
            this.xrTable_建床日期1.Weight = 0.19309583705996553D;
            // 
            // xrTableCell467
            // 
            this.xrTableCell467.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell467.Name = "xrTableCell467";
            this.xrTableCell467.StylePriority.UseBorders = false;
            this.xrTableCell467.Text = "/";
            this.xrTableCell467.Weight = 0.029376405757334877D;
            // 
            // xrTable_撤床日期1
            // 
            this.xrTable_撤床日期1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_撤床日期1.Name = "xrTable_撤床日期1";
            this.xrTable_撤床日期1.StylePriority.UseBorders = false;
            this.xrTable_撤床日期1.Weight = 0.19309931225984497D;
            // 
            // xrTable_家庭病床史原因1
            // 
            this.xrTable_家庭病床史原因1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史原因1.Name = "xrTable_家庭病床史原因1";
            this.xrTable_家庭病床史原因1.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史原因1.Weight = 0.41997548093664178D;
            // 
            // xrTable_家庭病床史机构及科室1
            // 
            this.xrTable_家庭病床史机构及科室1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史机构及科室1.Name = "xrTable_家庭病床史机构及科室1";
            this.xrTable_家庭病床史机构及科室1.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史机构及科室1.Weight = 0.40619193064266251D;
            // 
            // xrTable_家庭病床史病案号1
            // 
            this.xrTable_家庭病床史病案号1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史病案号1.Name = "xrTable_家庭病床史病案号1";
            this.xrTable_家庭病床史病案号1.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史病案号1.Weight = 0.33670445510535119D;
            // 
            // xrTableCell444
            // 
            this.xrTableCell444.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell444.Name = "xrTableCell444";
            this.xrTableCell444.StylePriority.UseBorders = false;
            this.xrTableCell444.Weight = 0.19876670037244218D;
            // 
            // xrTableRow78
            // 
            this.xrTableRow78.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell445,
            this.xrTableCell446,
            this.xrTable_建床日期2,
            this.xrTableCell468,
            this.xrTable_撤床日期2,
            this.xrTable_家庭病床史原因2,
            this.xrTable_家庭病床史机构及科室2,
            this.xrTable_家庭病床史病案号2,
            this.xrTableCell451});
            this.xrTableRow78.Name = "xrTableRow78";
            this.xrTableRow78.Weight = 1D;
            // 
            // xrTableCell445
            // 
            this.xrTableCell445.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell445.Name = "xrTableCell445";
            this.xrTableCell445.StylePriority.UseBorders = false;
            this.xrTableCell445.Weight = 0.11161843716525768D;
            // 
            // xrTableCell446
            // 
            this.xrTableCell446.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell446.Name = "xrTableCell446";
            this.xrTableCell446.StylePriority.UseBorders = false;
            this.xrTableCell446.Weight = 0.30993814107294138D;
            // 
            // xrTable_建床日期2
            // 
            this.xrTable_建床日期2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_建床日期2.Name = "xrTable_建床日期2";
            this.xrTable_建床日期2.StylePriority.UseBorders = false;
            this.xrTable_建床日期2.Weight = 0.19309583705996553D;
            // 
            // xrTableCell468
            // 
            this.xrTableCell468.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell468.Name = "xrTableCell468";
            this.xrTableCell468.StylePriority.UseBorders = false;
            this.xrTableCell468.Text = "/";
            this.xrTableCell468.Weight = 0.029376405757334877D;
            // 
            // xrTable_撤床日期2
            // 
            this.xrTable_撤床日期2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_撤床日期2.Name = "xrTable_撤床日期2";
            this.xrTable_撤床日期2.StylePriority.UseBorders = false;
            this.xrTable_撤床日期2.Weight = 0.19309931225984497D;
            // 
            // xrTable_家庭病床史原因2
            // 
            this.xrTable_家庭病床史原因2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史原因2.Name = "xrTable_家庭病床史原因2";
            this.xrTable_家庭病床史原因2.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史原因2.Weight = 0.41997548093664178D;
            // 
            // xrTable_家庭病床史机构及科室2
            // 
            this.xrTable_家庭病床史机构及科室2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史机构及科室2.Name = "xrTable_家庭病床史机构及科室2";
            this.xrTable_家庭病床史机构及科室2.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史机构及科室2.Weight = 0.40619193064266251D;
            // 
            // xrTable_家庭病床史病案号2
            // 
            this.xrTable_家庭病床史病案号2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_家庭病床史病案号2.Name = "xrTable_家庭病床史病案号2";
            this.xrTable_家庭病床史病案号2.StylePriority.UseBorders = false;
            this.xrTable_家庭病床史病案号2.Weight = 0.33670445510535119D;
            // 
            // xrTableCell451
            // 
            this.xrTableCell451.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell451.Name = "xrTableCell451";
            this.xrTableCell451.StylePriority.UseBorders = false;
            this.xrTableCell451.Weight = 0.19876670037244218D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 12F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report健康体检表A4_3_2017
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(12, 14, 0, 12);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell363;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_大便潜血;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_糖化血红蛋白;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell393;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乙型肝炎表面抗原;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell396;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell394;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血清谷丙转氨酶;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell395;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell397;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血清谷草转氨酶;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell401;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_白蛋白;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell399;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell400;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_总胆红素;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell402;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell403;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_综合胆红素;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell405;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血清肌酐;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell406;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血尿素;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell409;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血钾浓度;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell410;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell411;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血钠浓度;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell413;
        private DevExpress.XtraReports.UI.XRTableCell xrTableC;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_总胆固醇;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell414;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell415;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_甘油三酯;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell416;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell417;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血清低密度脂蛋白胆固醇;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell418;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell419;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血清高密度脂蛋白胆固醇;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell420;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心电图6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell421;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell422;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_胸部X线片异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_胸部X线片;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell359;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell425;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell424;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_腹部B超异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell361;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_腹部B超;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell362;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell355;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell356;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell427;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell428;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_B超其他异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell357;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_B超其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell358;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell351;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell352;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell423;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell426;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫颈涂片异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell353;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫颈涂片;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell354;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_辅助检查其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell347;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell348;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell349;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell350;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell344;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell429;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell430;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脑血管疾病5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell346;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell431;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell432;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肾脏疾病5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell433;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心血管疾病5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell434;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell435;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼部疾病其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼部疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼部疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼部疾病3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell436;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_神经系统其他疾病其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_神经系统其他疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel85;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_神经系统其他疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_神经系统其他疾病3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell364;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell365;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell366;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell367;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell372;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell373;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell437;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell438;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_其他系统疾病其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell374;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_其他系统疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_其他系统疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_其他系统疾病3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell375;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell368;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell369;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell454;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell452;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell453;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell370;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell371;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell384;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_入院日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell462;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_出院日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史原因1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史机构及科室1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史病案号1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell387;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell381;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_入院日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell464;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_出院日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史原因2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史机构及科室2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_住院史病案号2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell383;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell376;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell377;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell389;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell388;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell378;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell379;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell391;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell439;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_建床日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell467;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_撤床日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史原因1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史机构及科室1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史病案号1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell444;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell445;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell446;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_建床日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell468;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_撤床日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史原因2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史机构及科室2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_家庭病床史病案号2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell451;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox心电图;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox胸部射线;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBoxB超;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox主要现存问题;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox辅助检查;
    }
}
