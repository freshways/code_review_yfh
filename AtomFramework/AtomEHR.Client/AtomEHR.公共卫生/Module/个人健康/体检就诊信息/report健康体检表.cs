﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.公共卫生;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class report健康体检表 : DevExpress.XtraReports.UI.XtraReport
    {
        DataSet ds体检;
        bll健康体检 bll = new bll健康体检();

        public report健康体检表()
        {
            InitializeComponent();
        }
        public report健康体检表(string docNo, string date)
        {
            InitializeComponent();
            ds体检 = bll.GetReportDataByKey(docNo, date);
            BindData(ds体检);
        }

        private void BindData(DataSet ds体检)
        {
            DataTable tb健康档案 = ds体检.Tables[tb_健康档案.__TableName];
            DataTable tb健康体检 = ds体检.Tables[tb_健康体检.__TableName];
            DataTable tb住院史 = ds体检.Tables["住院史"];
            DataTable tb家庭病床史 = ds体检.Tables["家庭病床史"];
            DataTable tb用药情况表 = ds体检.Tables[tb_健康体检_用药情况表.__TableName];
            DataTable tb接种史 = ds体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName];

            if (tb健康体检.Rows.Count == 1 && tb健康档案.Rows.Count == 1)
            {
                this.xrLabel姓名.Text = AtomEHR.公共卫生.util.DESEncrypt.DES解密(tb健康档案.Rows[0][tb_健康档案.姓名].ToString());

                string[] tijianriqi = tb健康体检.Rows[0][tb_健康体检.体检日期].ToString().Split('-');
                this.xrLabel体检日期年.Text = tijianriqi[0];
                this.xrLabel体检日期月.Text = tijianriqi[1];
                this.xrLabel体检日期日.Text = tijianriqi[2];

                this.xrLabel责任医生.Text = tb健康体检.Rows[0][tb_健康体检.FIELD2].ToString();
                this.xrLabel症状其他.Text = tb健康体检.Rows[0][tb_健康体检.症状其他].ToString();
                //TODO:症状 
                #region 症状

                string zhengzhuang = tb健康体检.Rows[0][tb_健康体检.症状].ToString();
                string[] zzs = zhengzhuang.Split(',');
                for (int i = 0; i < zzs.Length; i++)
                {
                    if (string.IsNullOrEmpty(zzs[i]))
                    {
                        continue;
                    }
                    string ctrName = "xrLabel症状" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(ctrName, false);
                    lbl.Text = zzs[i];
                }

                #endregion

                #region 一般状况

                this.xrLabel体温.Text = tb健康体检.Rows[0][tb_健康体检.体温].ToString();
                this.xrLabel脉率.Text = tb健康体检.Rows[0][tb_健康体检.脉搏].ToString();
                this.xrLabel呼吸频率.Text = tb健康体检.Rows[0][tb_健康体检.呼吸].ToString();
                this.xrLabel血压左侧1.Text = tb健康体检.Rows[0][tb_健康体检.血压左侧1].ToString();
                this.xrLabel血压左侧2.Text = tb健康体检.Rows[0][tb_健康体检.血压左侧2].ToString();
                this.xrLabel血压右侧1.Text = tb健康体检.Rows[0][tb_健康体检.血压右侧1].ToString();
                this.xrLabel血压右侧2.Text = tb健康体检.Rows[0][tb_健康体检.血压右侧2].ToString();
                this.xrLabel身高.Text = tb健康体检.Rows[0][tb_健康体检.身高].ToString();
                this.xrLabel体重.Text = tb健康体检.Rows[0][tb_健康体检.体重].ToString();
                this.xrLabel腰围.Text = tb健康体检.Rows[0][tb_健康体检.腰围].ToString();
                this.xrLabel体重指数.Text = tb健康体检.Rows[0][tb_健康体检.体重指数].ToString();
                this.xrLabel老年人健康状态自我评估.Text = tb健康体检.Rows[0][tb_健康体检.老年人状况评估].ToString();
                this.xrLabel老年人生活自理能力评估.Text = tb健康体检.Rows[0][tb_健康体检.老年人自理评估].ToString();
                this.xrLabel老年人认知功能.Text = tb健康体检.Rows[0][tb_健康体检.老年人认知].ToString();
                this.xrLabel老年人认知功能分.Text = tb健康体检.Rows[0][tb_健康体检.老年人认知分].ToString();
                this.xrLabel老年人情感状态.Text = tb健康体检.Rows[0][tb_健康体检.老年人情感].ToString();
                this.xrLabel老年人情感状态分.Text = tb健康体检.Rows[0][tb_健康体检.老年人情感分].ToString();

                #endregion

                #region 生活方式

                this.xrLabel锻炼频率.Text = tb健康体检.Rows[0][tb_健康体检.锻炼频率].ToString();
                this.xrLabel每次锻炼时间.Text = tb健康体检.Rows[0][tb_健康体检.每次锻炼时间].ToString();
                this.xrLabel坚持锻炼时间.Text = tb健康体检.Rows[0][tb_健康体检.坚持锻炼时间].ToString();
                this.xrLabel锻炼方式.Text = tb健康体检.Rows[0][tb_健康体检.锻炼方式].ToString();
                this.xrLabel锻炼频率.Text = tb健康体检.Rows[0][tb_健康体检.锻炼频率].ToString();
                this.xrLabel锻炼频率.Text = tb健康体检.Rows[0][tb_健康体检.锻炼频率].ToString();

                string yinshi = tb健康体检.Rows[0][tb_健康体检.饮食习惯].ToString();
                string[] yinshis = yinshi.Split(',');
                for (int i = 0; i < yinshis.Length; i++)
                {
                    if (string.IsNullOrEmpty(yinshis[i]))
                    {
                        continue;
                    }
                    string ctrName = "xrLabel饮食习惯" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(ctrName, false);
                    lbl.Text = yinshis[i];
                }

                this.xrLabel吸烟状况.Text = tb健康体检.Rows[0][tb_健康体检.吸烟状况].ToString();
                this.xrLabel日吸烟量.Text = tb健康体检.Rows[0][tb_健康体检.日吸烟量].ToString();
                this.xrLabel开始吸烟年龄.Text = tb健康体检.Rows[0][tb_健康体检.开始吸烟年龄].ToString();
                this.xrLabel戒烟年龄.Text = tb健康体检.Rows[0][tb_健康体检.戒烟年龄].ToString();

                this.xrLabel饮酒频率.Text = tb健康体检.Rows[0][tb_健康体检.饮酒频率].ToString();
                this.xrLabel日饮酒量.Text = tb健康体检.Rows[0][tb_健康体检.日饮酒量].ToString();
                this.xrLabel是否戒酒.Text = tb健康体检.Rows[0][tb_健康体检.是否戒酒].ToString();
                this.xrLabel戒酒年龄.Text = tb健康体检.Rows[0][tb_健康体检.戒酒年龄].ToString();
                this.xrLabel开始饮酒年龄.Text = tb健康体检.Rows[0][tb_健康体检.开始饮酒年龄].ToString();
                this.xrLabel近一年是否醉酒.Text = tb健康体检.Rows[0][tb_健康体检.近一年内是否曾醉酒].ToString();
                this.xrLabel饮酒种类其他.Text = tb健康体检.Rows[0][tb_健康体检.饮酒种类其它].ToString();
                string yinjiu = tb健康体检.Rows[0][tb_健康体检.饮酒种类].ToString();
                string[] yinjius = yinjiu.Split(new char[]{','},StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < yinjius.Length; i++) 
                {
                    if (string.IsNullOrEmpty(yinjius[i]) && i>=4)//控件只有4个
                    {
                        continue;
                    }
                    string ctrName = "xrLabel饮酒种类" + (i + 1);
                    if ((XRLabel)FindControl(ctrName, false) != null)
                    {
                        XRLabel lbl = (XRLabel)FindControl(ctrName, false);
                        lbl.Text = yinjius[i];
                    }
                }

                this.xrLabel职业病有无.Text = tb健康体检.Rows[0][tb_健康体检.有无职业病].ToString();
                this.xrLabel工种.Text = tb健康体检.Rows[0][tb_健康体检.具体职业].ToString();
                this.xrLabel从业时间.Text = tb健康体检.Rows[0][tb_健康体检.从业时间].ToString();
                this.xrLabel粉尘.Text = tb健康体检.Rows[0][tb_健康体检.粉尘].ToString();
                this.xrLabel粉尘有.Text = tb健康体检.Rows[0][tb_健康体检.粉尘防护措施].ToString();
                this.xrLabel粉尘防护有无.Text = tb健康体检.Rows[0][tb_健康体检.粉尘防护有无].ToString();
                this.xrLabel放射物质.Text = tb健康体检.Rows[0][tb_健康体检.放射物质].ToString();
                this.xrLabel放射物质防护措施有无.Text = tb健康体检.Rows[0][tb_健康体检.放射物质防护措施有无].ToString();
                this.xrLabel放射物质有.Text = tb健康体检.Rows[0][tb_健康体检.放射物质防护措施其他].ToString();
                this.xrLabel物理因素.Text = tb健康体检.Rows[0][tb_健康体检.物理因素].ToString();
                this.xrLabel物理因素防护措施有无.Text = tb健康体检.Rows[0][tb_健康体检.物理防护有无].ToString();
                this.xrLabel物理因素有.Text = tb健康体检.Rows[0][tb_健康体检.物理防护措施].ToString();
                this.xrLabel化学因素.Text = tb健康体检.Rows[0][tb_健康体检.化学物质].ToString();
                this.xrLabel化学因素防护措施有无.Text = tb健康体检.Rows[0][tb_健康体检.化学物质防护].ToString();
                this.xrLabel化学因素有.Text = tb健康体检.Rows[0][tb_健康体检.化学物质具体防护].ToString();
                this.xrLabel毒物其他.Text = tb健康体检.Rows[0][tb_健康体检.职业病其他].ToString();
                this.xrLabel毒物其他防护措施有无.Text = tb健康体检.Rows[0][tb_健康体检.其他防护有无].ToString();
                this.xrLabel毒物其他有.Text = tb健康体检.Rows[0][tb_健康体检.其他防护措施].ToString();

                #endregion

                #region 住院治疗情况
                if (tb住院史 != null && tb住院史.Rows.Count > 0)
                {
                    for (int i = 0; i < tb住院史.Rows.Count; i++)
                    {
                        if (i > 1) break;//只显示前两条

                        string riqi = "xrTable住院史日期" + (i + 1);
                        string yuanyin = "xrLabel住院史原因" + (i + 1);
                        string yiyuan = "xrLabel住院史医疗机构" + (i + 1);
                        string no = "xrLabel住院史病案号" + (i + 1);

                        XRTableCell lblriqi = (XRTableCell)FindControl(riqi, false);
                        XRLabel lblyuanyin = (XRLabel)FindControl(yuanyin, false);
                        XRLabel lblyiyuan = (XRLabel)FindControl(yiyuan, false);
                        XRLabel lblno = (XRLabel)FindControl(no, false);

                        lblriqi.Text = tb住院史.Rows[i][tb_健康体检_住院史.入院日期] + "/" + tb住院史.Rows[i][tb_健康体检_住院史.出院日期];
                        lblyuanyin.Text = tb住院史.Rows[i][tb_健康体检_住院史.原因].ToString();
                        lblyiyuan.Text = tb住院史.Rows[i][tb_健康体检_住院史.医疗机构名称].ToString();
                        lblno.Text = tb住院史.Rows[i][tb_健康体检_住院史.病案号].ToString();
                    }
                }
                #endregion

                #region 家庭病床史
                if (tb家庭病床史 != null && tb家庭病床史.Rows.Count > 0)
                {
                    for (int i = 0; i < tb家庭病床史.Rows.Count; i++)
                    {
                        if (i > 1) break;//只显示前两条

                        string riqi = "xrTable家庭病床史日期" + (i + 1);
                        string yuanyin = "xrTable家庭病床史原因" + (i + 1);
                        string yiyuan = "xrTable家庭病床史医疗机构" + (i + 1);
                        string no = "xrTable家庭病床史病案号" + (i + 1);

                        XRTableCell lblriqi = (XRTableCell)FindControl(riqi, false);
                        XRLabel lblyuanyin = (XRLabel)FindControl(yuanyin, false);
                        XRLabel lblyiyuan = (XRLabel)FindControl(yiyuan, false);
                        XRLabel lblno = (XRLabel)FindControl(no, false);

                        lblriqi.Text = tb家庭病床史.Rows[i][tb_健康体检_住院史.入院日期] + "/" + tb家庭病床史.Rows[i][tb_健康体检_住院史.出院日期];
                        lblyuanyin.Text = tb家庭病床史.Rows[i][tb_健康体检_住院史.原因].ToString();
                        lblyiyuan.Text = tb家庭病床史.Rows[i][tb_健康体检_住院史.医疗机构名称].ToString();
                        lblno.Text = tb家庭病床史.Rows[i][tb_健康体检_住院史.病案号].ToString();
                    }
                }
                #endregion

                #region 主要用药情况
                if (tb用药情况表 != null && tb用药情况表.Rows.Count > 0)
                {
                    for (int i = 0; i < tb用药情况表.Rows.Count; i++)
                    {
                        if (i > 6) break;//只显示前两条

                        string 用药名称 = "xrTable用药名称" + (i + 1);
                        string 用药用法 = "xrTable用药用法" + (i + 1);
                        string 用药时间 = "xrTable用药时间" + (i + 1);
                        string 用药用量 = "xrTable用药用量" + (i + 1);
                        string 用药依从性 = "xrTable用药依从性" + (i + 1);

                        XRTableCell lbl用药名称 = (XRTableCell)FindControl(用药名称, false);
                        XRLabel lbl用药用法 = (XRLabel)FindControl(用药用法, false);
                        XRLabel lbl用药时间 = (XRLabel)FindControl(用药时间, false);
                        XRLabel lbl用药依从性 = (XRLabel)FindControl(用药依从性, false);
                        XRLabel lbl用药用量 = (XRLabel)FindControl(用药用量, false);

                        lbl用药名称.Text = tb用药情况表.Rows[i][tb_健康体检_用药情况表.药物名称].ToString();
                        lbl用药用法.Text = tb用药情况表.Rows[i][tb_健康体检_用药情况表.用法].ToString();
                        lbl用药时间.Text = tb用药情况表.Rows[i][tb_健康体检_用药情况表.用药时间].ToString();
                        lbl用药用量.Text = tb用药情况表.Rows[i][tb_健康体检_用药情况表.用量].ToString();
                        lbl用药依从性.Text = tb用药情况表.Rows[i][tb_健康体检_用药情况表.服药依从性].ToString();
                    }
                }
                #endregion

                #region 接种史
                if (tb接种史 != null && tb接种史.Rows.Count > 0)
                {
                    for (int i = 0; i < tb接种史.Rows.Count; i++)
                    {
                        if (i > 1) break;//只显示前两条

                        string 接种名称 = "xrTable接种名称" + (i + 1);
                        string 接种日期 = "xrLabel接种日期" + (i + 1);
                        string 接种机构 = "xrLabel接种机构" + (i + 1);

                        XRTableCell lbl接种名称 = (XRTableCell)FindControl(接种名称, false);
                        XRLabel lbl接种日期 = (XRLabel)FindControl(接种日期, false);
                        XRLabel lbl接种机构 = (XRLabel)FindControl(接种机构, false);

                        lbl接种名称.Text = tb接种史.Rows[i][tb_健康体检_非免疫规划预防接种史.接种名称].ToString();
                        lbl接种日期.Text = tb接种史.Rows[i][tb_健康体检_非免疫规划预防接种史.接种日期].ToString();
                        lbl接种机构.Text = tb接种史.Rows[i][tb_健康体检_非免疫规划预防接种史.接种机构].ToString();
                    }
                }
                #endregion

                #region 健康评价、健康指导
                this.xrLabel体检有无异常.Text = tb健康体检.Rows[0][tb_健康体检.健康评价].ToString();
                this.xrLabel体检异常1.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常1].ToString();
                this.xrLabel体检异常2.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常2].ToString();
                this.xrLabel体检异常3.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常3].ToString();
                this.xrLabel体检异常4.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常4].ToString();

                string 健康指导 = tb健康体检.Rows[0][tb_健康体检.健康指导].ToString();
                if (!string.IsNullOrEmpty(健康指导))
                {
                    string[] ss = 健康指导.Split(',');
                    for (int i = 0; i < ss.Length; i++)
                    {
                        string ctrid = "xrLabel健康指导" + (i + 1);
                        XRLabel ctrl = (XRLabel)FindControl(ctrid, false);

                        ctrl.Text = ss[i];
                    }
                }

                string 危险因素 = tb健康体检.Rows[0][tb_健康体检.危险因素控制].ToString();
                string S_疫苗98 = "", S_其他99 = "";
                if (!string.IsNullOrEmpty(危险因素))
                {
                    string newstring = string.Empty;
                    string[] ss = 危险因素.Split(',');
                    foreach (string s in ss)
                    {
                        if (s == "5")
                            S_疫苗98 += "1.接种疫苗";
                        else if (s == "6")
                            S_疫苗98 += "2.接种流感和肺炎疫苗";
                        else if (s == "7")
                            S_其他99 += "1.低盐饮食";
                        else if (s == "8")
                            S_其他99 += "2.低脂饮食";
                        else if (s == "9")
                            S_其他99 += "3.防滑防跌倒，防骨质疏松";
                        else if (s == "10")
                            S_其他99 += "4.防意外伤害和自救";
                        else if (s == "99") { }
                        else
                            newstring += s + ",";
                    }
                    string[] wx = newstring.Split(',');
                    for (int i = 0; i < wx.Length; i++)
                    {
                        string result = string.Empty;
                        if (string.IsNullOrEmpty(wx[i])) break;
                        switch (wx[i])
                        {
                            case "97":
                                result = "5";
                                break;
                            case "98":
                                result = "6";
                                break;
                            default:
                                result = ss[i];
                                break;
                        }


                        string ctrid = "xrLabel危险控制因素" + (i + 1);
                        XRLabel ctrl = (XRLabel)FindControl(ctrid, false);

                        ctrl.Text = result;
                    }
                }

                xrLabel减体重目标.Text = tb健康体检.Rows[0][tb_健康体检.危险因素控制体重].ToString();
                xrLabel建议接种疫苗.Text = tb健康体检.Rows[0][tb_健康体检.危险因素控制疫苗].ToString() + S_疫苗98;
                xrLabel危险控制因素其他.Text = tb健康体检.Rows[0][tb_健康体检.危险因素控制其他].ToString() + S_其他99;
                #endregion
            }
        }
    }
}
