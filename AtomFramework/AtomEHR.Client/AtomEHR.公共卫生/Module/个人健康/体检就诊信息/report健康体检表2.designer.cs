﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class report健康体检表2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel尿微量白蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel大便潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel糖化血红蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel乙肝 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血清谷丙转氨酶 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血清谷草转氨酶 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel白蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel总红胆素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow72 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel结合胆红素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow73 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血清肌酐 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血尿素氮 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell285 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow74 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血钾浓度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血钠浓度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow75 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel总胆固醇 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel甘油三酯 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow76 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血清低密度 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow77 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血清高密度 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel胸部X线片异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel胸部X线片 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabelB超异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabelB超 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel宫颈涂片异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel宫颈涂片 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel辅助检查其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel平和质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel气虚质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel阳虚质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel阴虚质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel痰湿质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel湿热质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血瘀质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel气郁质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel特禀质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel脑血管疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell273 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel脑血管疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel脑血管疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel脑血管疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel脑血管疾病4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel脑血管疾病5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow78 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow79 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell313 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel肾脏疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel肾脏疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel肾脏疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel肾脏疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel肾脏疾病4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell315 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel肾脏疾病5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow80 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow81 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel心脏疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel心脏疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel心脏疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel心脏疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel心脏疾病4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel心脏疾病5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血管疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血管疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血管疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血管疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow82 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow83 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell344 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell346 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel眼部疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel眼部疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel眼部疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel眼部疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell341 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel神经系统疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel神经系统疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel其他系统疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel其他系统疾病 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel口唇 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell347 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow84 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell缺齿1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell缺齿2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow85 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell缺齿3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell缺齿4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell348 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell349 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow86 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell龋齿1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell龋齿2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow87 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell龋齿3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell龋齿4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell350 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow88 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell义齿1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell义齿2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow89 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell义齿3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell义齿4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel齿列有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel咽部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel左眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel右眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel左眼矫正 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel右眼矫正 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel听力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel运动功能 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel眼底异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel眼底 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel皮肤其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel巩膜其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel巩膜 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel淋巴结其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel淋巴结 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel桶状胸 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel呼吸音异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel呼吸音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel罗音其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel罗音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel心率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel心律 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel杂音有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel杂音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow90 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel压痛有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel压痛 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel包块有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel包块 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel肝大有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel肝大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel脾大有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel脾大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel移动性浊音有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel移动性浊音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel下肢水肿 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel足背动脉搏动 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel肛门指诊其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel肛门指诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel乳腺其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel乳腺 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel外阴异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel外阴 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel阴道异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel阴道 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel宫颈异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel宫颈 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel宫体异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel宫体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel附件异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel附件 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel查体其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血红蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel白细胞 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血小板 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel血常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel尿蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel尿糖 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel尿酮体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel尿潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel尿常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel空腹血糖1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel空腹血糖2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel心电图异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel心电图 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable7,
            this.xrTable1});
            this.Detail.Font = new System.Drawing.Font("仿宋", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Detail.HeightF = 1157.833F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable7
            // 
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(811.4583F, 81.25F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41,
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow54,
            this.xrTableRow55,
            this.xrTableRow56,
            this.xrTableRow57,
            this.xrTableRow58,
            this.xrTableRow59,
            this.xrTableRow60,
            this.xrTableRow61,
            this.xrTableRow62,
            this.xrTableRow63,
            this.xrTableRow64,
            this.xrTableRow65,
            this.xrTableRow66,
            this.xrTableRow67});
            this.xrTable7.SizeF = new System.Drawing.SizeF(762.5417F, 987F);
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell108,
            this.xrTableCell113,
            this.xrTableCell161,
            this.xrTableCell145});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.StylePriority.UseBorders = false;
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.Weight = 0.27932898095751213D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.StylePriority.UseTextAlignment = false;
            this.xrTableCell113.Text = "尿微量白蛋白*";
            this.xrTableCell113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell113.Weight = 0.51144743823639915D;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel尿微量白蛋白});
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.StylePriority.UseBorders = false;
            this.xrTableCell161.Weight = 1.4018635455781225D;
            // 
            // xrLabel尿微量白蛋白
            // 
            this.xrLabel尿微量白蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel尿微量白蛋白.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel尿微量白蛋白.Name = "xrLabel尿微量白蛋白";
            this.xrLabel尿微量白蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿微量白蛋白.SizeF = new System.Drawing.SizeF(349.9304F, 22F);
            this.xrLabel尿微量白蛋白.StylePriority.UseBorders = false;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.StylePriority.UseTextAlignment = false;
            this.xrTableCell145.Text = "mg/dL";
            this.xrTableCell145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell145.Weight = 0.80736003522796651D;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162,
            this.xrTableCell163,
            this.xrTableCell164,
            this.xrTableCell165});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.StylePriority.UseBorders = false;
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseBorders = false;
            this.xrTableCell162.Weight = 0.27932898095751213D;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Text = "大便潜血*";
            this.xrTableCell163.Weight = 0.51144743823639882D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseTextAlignment = false;
            this.xrTableCell164.Text = "1 阴性   2 阳性";
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell164.Weight = 2.0746944526998594D;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell165.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel大便潜血});
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.StylePriority.UseBorders = false;
            this.xrTableCell165.Weight = 0.13452912810622961D;
            // 
            // xrLabel大便潜血
            // 
            this.xrLabel大便潜血.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel大便潜血.CanGrow = false;
            this.xrLabel大便潜血.LocationFloat = new DevExpress.Utils.PointFloat(9.194692F, 4.000028F);
            this.xrLabel大便潜血.Name = "xrLabel大便潜血";
            this.xrLabel大便潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel大便潜血.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel大便潜血.StylePriority.UseBorders = false;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell166,
            this.xrTableCell167,
            this.xrTableCell168,
            this.xrTableCell169});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.StylePriority.UseBorders = false;
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.Weight = 0.27932898095751213D;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Text = "糖化血红蛋白*";
            this.xrTableCell167.Weight = 0.51144743823639871D;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel糖化血红蛋白});
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Weight = 0.53715700217465379D;
            // 
            // xrLabel糖化血红蛋白
            // 
            this.xrLabel糖化血红蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel糖化血红蛋白.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.052628F);
            this.xrLabel糖化血红蛋白.Name = "xrLabel糖化血红蛋白";
            this.xrLabel糖化血红蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel糖化血红蛋白.SizeF = new System.Drawing.SizeF(128.8351F, 21F);
            this.xrLabel糖化血红蛋白.StylePriority.UseBorders = false;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.StylePriority.UseBorders = false;
            this.xrTableCell169.StylePriority.UseTextAlignment = false;
            this.xrTableCell169.Text = "%";
            this.xrTableCell169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell169.Weight = 1.6720665786314353D;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell170,
            this.xrTableCell171,
            this.xrTableCell172,
            this.xrTableCell173});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.StylePriority.UseBorders = false;
            this.xrTableRow44.Weight = 2D;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.Weight = 0.27932898095751213D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Multiline = true;
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Text = "乙型肝炎\r\n表面抗原*";
            this.xrTableCell171.Weight = 0.51144767836159044D;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.StylePriority.UseTextAlignment = false;
            this.xrTableCell172.Text = "1 阴性   2 阳性";
            this.xrTableCell172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell172.Weight = 2.0746946928250507D;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell173.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel乙肝});
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.StylePriority.UseBorders = false;
            this.xrTableCell173.Weight = 0.13452864785584626D;
            // 
            // xrLabel乙肝
            // 
            this.xrLabel乙肝.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel乙肝.CanGrow = false;
            this.xrLabel乙肝.LocationFloat = new DevExpress.Utils.PointFloat(9.19458F, 18.07896F);
            this.xrLabel乙肝.Name = "xrLabel乙肝";
            this.xrLabel乙肝.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乙肝.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel乙肝.StylePriority.UseBorders = false;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell174,
            this.xrTableCell175,
            this.xrTableCell177});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.StylePriority.UseBorders = false;
            this.xrTableRow45.Weight = 3D;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 10, 100F);
            this.xrTableCell174.StylePriority.UseBorders = false;
            this.xrTableCell174.StylePriority.UsePadding = false;
            this.xrTableCell174.StylePriority.UseTextAlignment = false;
            this.xrTableCell174.Text = "辅";
            this.xrTableCell174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell174.Weight = 0.27932898095751213D;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseBorders = false;
            this.xrTableCell175.Text = "肝功能*";
            this.xrTableCell175.Weight = 0.51144791848678206D;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell177.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable9});
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.StylePriority.UseBorders = false;
            this.xrTableCell177.Weight = 2.2092231005557057D;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow70,
            this.xrTableRow71,
            this.xrTableRow72});
            this.xrTable9.SizeF = new System.Drawing.SizeF(561.5414F, 75.88159F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseTextAlignment = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell176,
            this.xrTableCell280,
            this.xrTableCell277,
            this.xrTableCell281,
            this.xrTableCell278});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 1D;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.StylePriority.UseBorders = false;
            this.xrTableCell176.Text = "血清谷丙转氨酶";
            this.xrTableCell176.Weight = 1.500001220703125D;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血清谷丙转氨酶});
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.Weight = 0.69601806640625008D;
            // 
            // xrLabel血清谷丙转氨酶
            // 
            this.xrLabel血清谷丙转氨酶.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel血清谷丙转氨酶.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.381561F);
            this.xrLabel血清谷丙转氨酶.Name = "xrLabel血清谷丙转氨酶";
            this.xrLabel血清谷丙转氨酶.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血清谷丙转氨酶.SizeF = new System.Drawing.SizeF(68.64539F, 21F);
            this.xrLabel血清谷丙转氨酶.StylePriority.UseBorders = false;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.Text = "U/L  血清谷草转氨酶";
            this.xrTableCell277.Weight = 1.98106201171875D;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血清谷草转氨酶});
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.Weight = 0.78855102539062494D;
            // 
            // xrLabel血清谷草转氨酶
            // 
            this.xrLabel血清谷草转氨酶.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel血清谷草转氨酶.LocationFloat = new DevExpress.Utils.PointFloat(4.903076F, 1.381561F);
            this.xrLabel血清谷草转氨酶.Name = "xrLabel血清谷草转氨酶";
            this.xrLabel血清谷草转氨酶.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血清谷草转氨酶.SizeF = new System.Drawing.SizeF(70.39001F, 21F);
            this.xrLabel血清谷草转氨酶.StylePriority.UseBorders = false;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.StylePriority.UseBorders = false;
            this.xrTableCell278.Text = "U/L";
            this.xrTableCell278.Weight = 0.60964477539062489D;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell282,
            this.xrTableCell283,
            this.xrTableCell284,
            this.xrTableCell286,
            this.xrTableCell287});
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Weight = 1D;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.StylePriority.UseBorders = false;
            this.xrTableCell282.Text = "白蛋白";
            this.xrTableCell282.Weight = 0.67708251953125D;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel白蛋白});
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.Weight = 1.5189367675781251D;
            // 
            // xrLabel白蛋白
            // 
            this.xrLabel白蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel白蛋白.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.381546F);
            this.xrLabel白蛋白.Name = "xrLabel白蛋白";
            this.xrLabel白蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel白蛋白.SizeF = new System.Drawing.SizeF(151.5298F, 21F);
            this.xrLabel白蛋白.StylePriority.UseBorders = false;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.Text = "g/L  总胆红素";
            this.xrTableCell284.Weight = 1.34067138671875D;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel总红胆素});
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.Weight = 1.4289404296875D;
            // 
            // xrLabel总红胆素
            // 
            this.xrLabel总红胆素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel总红胆素.LocationFloat = new DevExpress.Utils.PointFloat(0.9522095F, 1.381561F);
            this.xrLabel总红胆素.Name = "xrLabel总红胆素";
            this.xrLabel总红胆素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel总红胆素.SizeF = new System.Drawing.SizeF(142.1803F, 21F);
            this.xrLabel总红胆素.StylePriority.UseBorders = false;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.StylePriority.UseBorders = false;
            this.xrTableCell287.Text = "umol/L";
            this.xrTableCell287.Weight = 0.60964599609374981D;
            // 
            // xrTableRow72
            // 
            this.xrTableRow72.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell288,
            this.xrTableCell289,
            this.xrTableCell293});
            this.xrTableRow72.Name = "xrTableRow72";
            this.xrTableRow72.Weight = 1D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.StylePriority.UseBorders = false;
            this.xrTableCell288.Text = "结合胆红素";
            this.xrTableCell288.Weight = 1.00625D;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel结合胆红素});
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.Weight = 1.189769287109375D;
            // 
            // xrLabel结合胆红素
            // 
            this.xrLabel结合胆红素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel结合胆红素.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.381561F);
            this.xrLabel结合胆红素.Name = "xrLabel结合胆红素";
            this.xrLabel结合胆红素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel结合胆红素.SizeF = new System.Drawing.SizeF(110.3243F, 21F);
            this.xrLabel结合胆红素.StylePriority.UseBorders = false;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.StylePriority.UseBorders = false;
            this.xrTableCell293.Text = "umol/L";
            this.xrTableCell293.Weight = 3.3792578124999997D;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell178,
            this.xrTableCell179,
            this.xrTableCell181});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 2D;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.Text = "助";
            this.xrTableCell178.Weight = 0.27932898095751213D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.StylePriority.UseBorders = false;
            this.xrTableCell179.Text = "肾功能*";
            this.xrTableCell179.Weight = 0.51144791848678217D;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell181.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable10});
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.StylePriority.UseBorders = false;
            this.xrTableCell181.Weight = 2.2092231005557057D;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow73,
            this.xrTableRow74});
            this.xrTable10.SizeF = new System.Drawing.SizeF(558.4766F, 47.96051F);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseTextAlignment = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow73
            // 
            this.xrTableRow73.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell180,
            this.xrTableCell279,
            this.xrTableCell295,
            this.xrTableCell294,
            this.xrTableCell285});
            this.xrTableRow73.Name = "xrTableRow73";
            this.xrTableRow73.Weight = 1D;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.StylePriority.UseBorders = false;
            this.xrTableCell180.Text = "血清肌酐";
            this.xrTableCell180.Weight = 0.45630484476980804D;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血清肌酐});
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.Weight = 0.42267454672823357D;
            // 
            // xrLabel血清肌酐
            // 
            this.xrLabel血清肌酐.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel血清肌酐.LocationFloat = new DevExpress.Utils.PointFloat(4.080505F, 0F);
            this.xrLabel血清肌酐.Name = "xrLabel血清肌酐";
            this.xrLabel血清肌酐.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血清肌酐.SizeF = new System.Drawing.SizeF(70.39001F, 21F);
            this.xrLabel血清肌酐.StylePriority.UseBorders = false;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.Text = "umol/L    血尿素氮";
            this.xrTableCell295.Weight = 1.0167792686893287D;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血尿素氮});
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.Weight = 0.60424133981262962D;
            // 
            // xrLabel血尿素氮
            // 
            this.xrLabel血尿素氮.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel血尿素氮.LocationFloat = new DevExpress.Utils.PointFloat(1.371643F, 1.525879E-05F);
            this.xrLabel血尿素氮.Name = "xrLabel血尿素氮";
            this.xrLabel血尿素氮.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血尿素氮.SizeF = new System.Drawing.SizeF(105.8494F, 21F);
            this.xrLabel血尿素氮.StylePriority.UseBorders = false;
            // 
            // xrTableCell285
            // 
            this.xrTableCell285.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell285.Name = "xrTableCell285";
            this.xrTableCell285.StylePriority.UseBorders = false;
            this.xrTableCell285.Text = "mmol/L";
            this.xrTableCell285.Weight = 0.5D;
            // 
            // xrTableRow74
            // 
            this.xrTableRow74.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell290,
            this.xrTableCell291,
            this.xrTableCell297,
            this.xrTableCell296,
            this.xrTableCell292});
            this.xrTableRow74.Name = "xrTableRow74";
            this.xrTableRow74.Weight = 1D;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.StylePriority.UseBorders = false;
            this.xrTableCell290.Text = "血钾浓度";
            this.xrTableCell290.Weight = 0.45630484476980804D;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血钾浓度});
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.Weight = 0.42267520357640676D;
            // 
            // xrLabel血钾浓度
            // 
            this.xrLabel血钾浓度.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel血钾浓度.LocationFloat = new DevExpress.Utils.PointFloat(4.080505F, 0F);
            this.xrLabel血钾浓度.Name = "xrLabel血钾浓度";
            this.xrLabel血钾浓度.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血钾浓度.SizeF = new System.Drawing.SizeF(70.39001F, 21F);
            this.xrLabel血钾浓度.StylePriority.UseBorders = false;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.Text = "mmol/L    血钠浓度";
            this.xrTableCell297.Weight = 1.0167780781520146D;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血钠浓度});
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.Weight = 0.60424187350177039D;
            // 
            // xrLabel血钠浓度
            // 
            this.xrLabel血钠浓度.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel血钠浓度.LocationFloat = new DevExpress.Utils.PointFloat(2.324036F, 1.525879E-05F);
            this.xrLabel血钠浓度.Name = "xrLabel血钠浓度";
            this.xrLabel血钠浓度.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血钠浓度.SizeF = new System.Drawing.SizeF(101.2417F, 21F);
            this.xrLabel血钠浓度.StylePriority.UseBorders = false;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.StylePriority.UseBorders = false;
            this.xrTableCell292.Text = "mmol/L";
            this.xrTableCell292.Weight = 0.5D;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell182,
            this.xrTableCell183,
            this.xrTableCell185});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 3D;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell182.Multiline = true;
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.Text = "检\r\n\r\n查";
            this.xrTableCell182.Weight = 0.27932898095751213D;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.StylePriority.UseBorders = false;
            this.xrTableCell183.Text = "血  脂*";
            this.xrTableCell183.Weight = 0.51144815861197379D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell185.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable11});
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.StylePriority.UseBorders = false;
            this.xrTableCell185.Weight = 2.209222860430514D;
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow75,
            this.xrTableRow76,
            this.xrTableRow77});
            this.xrTable11.SizeF = new System.Drawing.SizeF(556.9911F, 73.90018F);
            this.xrTable11.StylePriority.UseBorders = false;
            this.xrTable11.StylePriority.UseTextAlignment = false;
            this.xrTable11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow75
            // 
            this.xrTableRow75.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell184,
            this.xrLabel总胆固醇,
            this.xrTableCell298,
            this.xrLabel甘油三酯,
            this.xrTableCell299});
            this.xrTableRow75.Name = "xrTableRow75";
            this.xrTableRow75.Weight = 1D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseBorders = false;
            this.xrTableCell184.Text = "总胆固醇";
            this.xrTableCell184.Weight = 0.45630488582281892D;
            // 
            // xrLabel总胆固醇
            // 
            this.xrLabel总胆固醇.Name = "xrLabel总胆固醇";
            this.xrLabel总胆固醇.Weight = 0.42267456041257068D;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.Text = "mmol/L    甘油三酯";
            this.xrTableCell298.Weight = 1.0167787076315138D;
            // 
            // xrLabel甘油三酯
            // 
            this.xrLabel甘油三酯.Name = "xrLabel甘油三酯";
            this.xrLabel甘油三酯.Weight = 0.60424184613309651D;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.StylePriority.UseBorders = false;
            this.xrTableCell299.Text = "mmol/L";
            this.xrTableCell299.Weight = 0.5D;
            // 
            // xrTableRow76
            // 
            this.xrTableRow76.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell302,
            this.xrLabel血清低密度,
            this.xrTableCell306});
            this.xrTableRow76.Name = "xrTableRow76";
            this.xrTableRow76.Weight = 1D;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.StylePriority.UseBorders = false;
            this.xrTableCell302.Text = "血清低密度脂蛋白胆固醇";
            this.xrTableCell302.Weight = 1.181655713905837D;
            // 
            // xrLabel血清低密度
            // 
            this.xrLabel血清低密度.Name = "xrLabel血清低密度";
            this.xrLabel血清低密度.Weight = 0.71410234417070761D;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.StylePriority.UseBorders = false;
            this.xrTableCell306.Text = "mmol/L";
            this.xrTableCell306.Weight = 1.1042419419234553D;
            // 
            // xrTableRow77
            // 
            this.xrTableRow77.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell307,
            this.xrLabel血清高密度,
            this.xrTableCell311});
            this.xrTableRow77.Name = "xrTableRow77";
            this.xrTableRow77.Weight = 1D;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.StylePriority.UseBorders = false;
            this.xrTableCell307.Text = "血清高密度脂蛋白胆固醇";
            this.xrTableCell307.Weight = 1.1816559191708913D;
            // 
            // xrLabel血清高密度
            // 
            this.xrLabel血清高密度.Name = "xrLabel血清高密度";
            this.xrLabel血清高密度.Weight = 0.71410205679963223D;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.StylePriority.UseBorders = false;
            this.xrTableCell311.Text = "mmol/L";
            this.xrTableCell311.Weight = 1.1042420240294768D;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell186,
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell303,
            this.xrTableCell189});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.StylePriority.UseBorders = false;
            this.xrTableRow48.Weight = 1D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseBorders = false;
            this.xrTableCell186.Weight = 0.27932898095751213D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Text = "胸部X线片*";
            this.xrTableCell187.Weight = 0.51144839873716552D;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseTextAlignment = false;
            this.xrTableCell188.Text = "1 正常   2 异常";
            this.xrTableCell188.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell188.Weight = 0.60592309406432132D;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell303.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel胸部X线片异常});
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.StylePriority.UseBorders = false;
            this.xrTableCell303.Weight = 1.4687709134034119D;
            // 
            // xrLabel胸部X线片异常
            // 
            this.xrLabel胸部X线片异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel胸部X线片异常.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 0F);
            this.xrLabel胸部X线片异常.Name = "xrLabel胸部X线片异常";
            this.xrLabel胸部X线片异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel胸部X线片异常.SizeF = new System.Drawing.SizeF(153.6141F, 21F);
            this.xrLabel胸部X线片异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell189.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel胸部X线片});
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.StylePriority.UseBorders = false;
            this.xrTableCell189.Weight = 0.13452861283758905D;
            // 
            // xrLabel胸部X线片
            // 
            this.xrLabel胸部X线片.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel胸部X线片.CanGrow = false;
            this.xrLabel胸部X线片.LocationFloat = new DevExpress.Utils.PointFloat(9.19458F, 6.092119F);
            this.xrLabel胸部X线片.Name = "xrLabel胸部X线片";
            this.xrLabel胸部X线片.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel胸部X线片.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel胸部X线片.StylePriority.UseBorders = false;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell190,
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell304,
            this.xrTableCell193});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.StylePriority.UseBorders = false;
            this.xrTableRow49.Weight = 1D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseBorders = false;
            this.xrTableCell190.Weight = 0.27932898095751213D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Text = "B   超*";
            this.xrTableCell191.Weight = 0.51144839873716541D;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.StylePriority.UseTextAlignment = false;
            this.xrTableCell192.Text = "1 正常   2 异常";
            this.xrTableCell192.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell192.Weight = 0.60592309406432143D;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell304.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelB超异常});
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.StylePriority.UseBorders = false;
            this.xrTableCell304.Weight = 1.4687718138728805D;
            // 
            // xrLabelB超异常
            // 
            this.xrLabelB超异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabelB超异常.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 0F);
            this.xrLabelB超异常.Name = "xrLabelB超异常";
            this.xrLabelB超异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelB超异常.SizeF = new System.Drawing.SizeF(153.6141F, 21F);
            this.xrLabelB超异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell193.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabelB超});
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.StylePriority.UseBorders = false;
            this.xrTableCell193.Weight = 0.13452771236812033D;
            // 
            // xrLabelB超
            // 
            this.xrLabelB超.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabelB超.CanGrow = false;
            this.xrLabelB超.LocationFloat = new DevExpress.Utils.PointFloat(9.194336F, 5.95363F);
            this.xrLabelB超.Name = "xrLabelB超";
            this.xrLabelB超.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelB超.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabelB超.StylePriority.UseBorders = false;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell194,
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell308,
            this.xrTableCell197});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.StylePriority.UseBorders = false;
            this.xrTableRow50.Weight = 1D;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.StylePriority.UseBorders = false;
            this.xrTableCell194.Weight = 0.27932898095751213D;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Text = "宫颈涂片*";
            this.xrTableCell195.Weight = 0.51144863886235725D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.StylePriority.UseTextAlignment = false;
            this.xrTableCell196.Text = "1 正常   2 异常";
            this.xrTableCell196.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell196.Weight = 0.60592333418951294D;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell308.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel宫颈涂片异常});
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.StylePriority.UseBorders = false;
            this.xrTableCell308.Weight = 1.4687713336224975D;
            // 
            // xrLabel宫颈涂片异常
            // 
            this.xrLabel宫颈涂片异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel宫颈涂片异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel宫颈涂片异常.Name = "xrLabel宫颈涂片异常";
            this.xrLabel宫颈涂片异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫颈涂片异常.SizeF = new System.Drawing.SizeF(153.6141F, 21F);
            this.xrLabel宫颈涂片异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell197.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel宫颈涂片});
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.StylePriority.UseBorders = false;
            this.xrTableCell197.Weight = 0.13452771236812033D;
            // 
            // xrLabel宫颈涂片
            // 
            this.xrLabel宫颈涂片.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel宫颈涂片.CanGrow = false;
            this.xrLabel宫颈涂片.LocationFloat = new DevExpress.Utils.PointFloat(9.194336F, 3.973675F);
            this.xrLabel宫颈涂片.Name = "xrLabel宫颈涂片";
            this.xrLabel宫颈涂片.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫颈涂片.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel宫颈涂片.StylePriority.UseBorders = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell198,
            this.xrTableCell199,
            this.xrTableCell201});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.StylePriority.UseBorders = false;
            this.xrTableRow51.Weight = 2D;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseBorders = false;
            this.xrTableCell198.Weight = 0.27932898095751213D;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Text = "其   他*";
            this.xrTableCell199.Weight = 0.51144887898754876D;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel辅助检查其他});
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.StylePriority.UseBorders = false;
            this.xrTableCell201.Weight = 2.2092221400549388D;
            // 
            // xrLabel辅助检查其他
            // 
            this.xrLabel辅助检查其他.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel辅助检查其他.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 0F);
            this.xrLabel辅助检查其他.Name = "xrLabel辅助检查其他";
            this.xrLabel辅助检查其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel辅助检查其他.SizeF = new System.Drawing.SizeF(561.5413F, 51.94739F);
            this.xrLabel辅助检查其他.StylePriority.UseBorders = false;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell202,
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell205});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.StylePriority.UseBorders = false;
            this.xrTableRow52.Weight = 1D;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.StylePriority.UseBorders = false;
            this.xrTableCell202.Weight = 0.27932898095751213D;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Text = "平和质";
            this.xrTableCell203.Weight = 0.51144935923793222D;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.StylePriority.UseTextAlignment = false;
            this.xrTableCell204.Text = "1 是   2 基本是";
            this.xrTableCell204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell204.Weight = 2.0746938523868805D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell205.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel平和质});
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.StylePriority.UseBorders = false;
            this.xrTableCell205.Weight = 0.13452780741767539D;
            // 
            // xrLabel平和质
            // 
            this.xrLabel平和质.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel平和质.CanGrow = false;
            this.xrLabel平和质.LocationFloat = new DevExpress.Utils.PointFloat(9.194336F, 5.851412F);
            this.xrLabel平和质.Name = "xrLabel平和质";
            this.xrLabel平和质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel平和质.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel平和质.StylePriority.UseBorders = false;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell206,
            this.xrTableCell207,
            this.xrTableCell208,
            this.xrTableCell209});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.StylePriority.UseBorders = false;
            this.xrTableRow53.Weight = 1D;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.StylePriority.UseBorders = false;
            this.xrTableCell206.Weight = 0.27932898095751213D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.Text = "气虚质";
            this.xrTableCell207.Weight = 0.51144767836159033D;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.StylePriority.UseTextAlignment = false;
            this.xrTableCell208.Text = "1 是   2 倾向是";
            this.xrTableCell208.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell208.Weight = 2.0746955332632218D;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell209.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel气虚质});
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.StylePriority.UseBorders = false;
            this.xrTableCell209.Weight = 0.13452780741767537D;
            // 
            // xrLabel气虚质
            // 
            this.xrLabel气虚质.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel气虚质.CanGrow = false;
            this.xrLabel气虚质.LocationFloat = new DevExpress.Utils.PointFloat(9.194336F, 3.973706F);
            this.xrLabel气虚质.Name = "xrLabel气虚质";
            this.xrLabel气虚质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel气虚质.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel气虚质.StylePriority.UseBorders = false;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell210,
            this.xrTableCell211,
            this.xrTableCell212,
            this.xrTableCell213});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.StylePriority.UseBorders = false;
            this.xrTableRow54.Weight = 1D;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.StylePriority.UseBorders = false;
            this.xrTableCell210.Weight = 0.27932898095751213D;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Text = "阳虚质";
            this.xrTableCell211.Weight = 0.51144935923793222D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.StylePriority.UseTextAlignment = false;
            this.xrTableCell212.Text = "1 是   2 倾向是";
            this.xrTableCell212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell212.Weight = 2.0746938523868805D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell213.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel阳虚质});
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.StylePriority.UseBorders = false;
            this.xrTableCell213.Weight = 0.13452780741767537D;
            // 
            // xrLabel阳虚质
            // 
            this.xrLabel阳虚质.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel阳虚质.CanGrow = false;
            this.xrLabel阳虚质.LocationFloat = new DevExpress.Utils.PointFloat(9.194336F, 9.999974F);
            this.xrLabel阳虚质.Name = "xrLabel阳虚质";
            this.xrLabel阳虚质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阳虚质.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel阳虚质.StylePriority.UseBorders = false;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell214,
            this.xrTableCell215,
            this.xrTableCell216,
            this.xrTableCell217});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.StylePriority.UseBorders = false;
            this.xrTableRow55.Weight = 1D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.StylePriority.UseBorders = false;
            this.xrTableCell214.Weight = 0.27932898095751213D;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Text = "阴虚质";
            this.xrTableCell215.Weight = 0.51144935923793222D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.StylePriority.UseTextAlignment = false;
            this.xrTableCell216.Text = "1 是   2 倾向是";
            this.xrTableCell216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell216.Weight = 2.0746938523868805D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell217.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel阴虚质});
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.StylePriority.UseBorders = false;
            this.xrTableCell217.Weight = 0.13452780741767539D;
            // 
            // xrLabel阴虚质
            // 
            this.xrLabel阴虚质.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel阴虚质.CanGrow = false;
            this.xrLabel阴虚质.LocationFloat = new DevExpress.Utils.PointFloat(9.194336F, 9.999943F);
            this.xrLabel阴虚质.Name = "xrLabel阴虚质";
            this.xrLabel阴虚质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阴虚质.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel阴虚质.StylePriority.UseBorders = false;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell218,
            this.xrTableCell219,
            this.xrTableCell220,
            this.xrTableCell221});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.StylePriority.UseBorders = false;
            this.xrTableRow56.Weight = 1D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.StylePriority.UseBorders = false;
            this.xrTableCell218.Text = "中医体";
            this.xrTableCell218.Weight = 0.27932898095751213D;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Text = "痰湿质";
            this.xrTableCell219.Weight = 0.51144767836159055D;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.StylePriority.UseTextAlignment = false;
            this.xrTableCell220.Text = "1 是   2 倾向是";
            this.xrTableCell220.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell220.Weight = 2.0746938523868805D;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell221.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel痰湿质});
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseBorders = false;
            this.xrTableCell221.Weight = 0.13452948829401706D;
            // 
            // xrLabel痰湿质
            // 
            this.xrLabel痰湿质.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel痰湿质.CanGrow = false;
            this.xrLabel痰湿质.LocationFloat = new DevExpress.Utils.PointFloat(9.194763F, 9.697501F);
            this.xrLabel痰湿质.Name = "xrLabel痰湿质";
            this.xrLabel痰湿质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel痰湿质.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel痰湿质.StylePriority.UseBorders = false;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell222,
            this.xrTableCell223,
            this.xrTableCell224,
            this.xrTableCell225});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.StylePriority.UseBorders = false;
            this.xrTableRow57.Weight = 1D;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.StylePriority.UseBorders = false;
            this.xrTableCell222.StylePriority.UseTextAlignment = false;
            this.xrTableCell222.Text = "质辨识*";
            this.xrTableCell222.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell222.Weight = 0.27932898095751207D;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Text = "湿热质";
            this.xrTableCell223.Weight = 0.51144743823639893D;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.StylePriority.UseTextAlignment = false;
            this.xrTableCell224.Text = "1 是   2 倾向是";
            this.xrTableCell224.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell224.Weight = 2.07469385238688D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell225.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel湿热质});
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseBorders = false;
            this.xrTableCell225.Weight = 0.13452972841920877D;
            // 
            // xrLabel湿热质
            // 
            this.xrLabel湿热质.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel湿热质.CanGrow = false;
            this.xrLabel湿热质.LocationFloat = new DevExpress.Utils.PointFloat(9.194824F, 5.723763F);
            this.xrLabel湿热质.Name = "xrLabel湿热质";
            this.xrLabel湿热质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel湿热质.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel湿热质.StylePriority.UseBorders = false;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell226,
            this.xrTableCell227,
            this.xrTableCell228,
            this.xrTableCell229});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.StylePriority.UseBorders = false;
            this.xrTableRow58.Weight = 1D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.StylePriority.UseBorders = false;
            this.xrTableCell226.Weight = 0.27932898095751213D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Text = "血瘀质";
            this.xrTableCell227.Weight = 0.51144935923793222D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseTextAlignment = false;
            this.xrTableCell228.Text = "1 是   2 倾向是";
            this.xrTableCell228.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell228.Weight = 2.0746938523868805D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell229.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血瘀质});
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseBorders = false;
            this.xrTableCell229.Weight = 0.13452780741767539D;
            // 
            // xrLabel血瘀质
            // 
            this.xrLabel血瘀质.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel血瘀质.CanGrow = false;
            this.xrLabel血瘀质.LocationFloat = new DevExpress.Utils.PointFloat(9.194336F, 5.698236F);
            this.xrLabel血瘀质.Name = "xrLabel血瘀质";
            this.xrLabel血瘀质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血瘀质.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel血瘀质.StylePriority.UseBorders = false;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230,
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTableCell233});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.StylePriority.UseBorders = false;
            this.xrTableRow59.Weight = 1D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.StylePriority.UseBorders = false;
            this.xrTableCell230.Weight = 0.27932898095751213D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Text = "气郁质";
            this.xrTableCell231.Weight = 0.51144935923793211D;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.StylePriority.UseTextAlignment = false;
            this.xrTableCell232.Text = "1 是   2 倾向是";
            this.xrTableCell232.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell232.Weight = 2.0746938523868805D;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell233.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel气郁质});
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.StylePriority.UseBorders = false;
            this.xrTableCell233.Weight = 0.13452780741767539D;
            // 
            // xrLabel气郁质
            // 
            this.xrLabel气郁质.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel气郁质.CanGrow = false;
            this.xrLabel气郁质.LocationFloat = new DevExpress.Utils.PointFloat(9.194336F, 3.973706F);
            this.xrLabel气郁质.Name = "xrLabel气郁质";
            this.xrLabel气郁质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel气郁质.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel气郁质.StylePriority.UseBorders = false;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell234,
            this.xrTableCell235,
            this.xrTableCell236,
            this.xrTableCell237});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.StylePriority.UseBorders = false;
            this.xrTableRow60.Weight = 1D;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Weight = 0.27932898095751213D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Text = "特秉质";
            this.xrTableCell235.Weight = 0.51144935923793211D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.StylePriority.UseTextAlignment = false;
            this.xrTableCell236.Text = "1 是   2 倾向是";
            this.xrTableCell236.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell236.Weight = 2.0746938523868805D;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell237.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel特禀质});
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.StylePriority.UseBorders = false;
            this.xrTableCell237.Weight = 0.13452780741767539D;
            // 
            // xrLabel特禀质
            // 
            this.xrLabel特禀质.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel特禀质.CanGrow = false;
            this.xrLabel特禀质.LocationFloat = new DevExpress.Utils.PointFloat(9.194336F, 9.595235F);
            this.xrLabel特禀质.Name = "xrLabel特禀质";
            this.xrLabel特禀质.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel特禀质.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel特禀质.StylePriority.UseBorders = false;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell238,
            this.xrTableCell239,
            this.xrTableCell241});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 2D;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseBorders = false;
            this.xrTableCell238.Weight = 0.27932898095751213D;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.StylePriority.UseBorders = false;
            this.xrTableCell239.Text = "脑血管疾病";
            this.xrTableCell239.Weight = 0.511449599363124D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell241.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8});
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.StylePriority.UseBorders = false;
            this.xrTableCell241.Weight = 2.2092214196793645D;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0.0001831055F, 0F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow68,
            this.xrTableRow69});
            this.xrTable8.SizeF = new System.Drawing.SizeF(561.541F, 51.94739F);
            this.xrTable8.StylePriority.UseBorders = false;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell267});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 1D;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseBorders = false;
            this.xrTableCell267.StylePriority.UseTextAlignment = false;
            this.xrTableCell267.Text = "1未发现2缺血性卒中 3脑出血 4蛛网膜下腔出血 5短暂性脑缺血";
            this.xrTableCell267.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell267.Weight = 3D;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell268,
            this.xrTableCell269,
            this.xrTableCell273,
            this.xrTableCell274,
            this.xrTableCell240,
            this.xrTableCell275,
            this.xrTableCell266,
            this.xrTableCell276,
            this.xrTableCell271,
            this.xrTableCell272,
            this.xrTableCell270});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 1D;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.StylePriority.UseBorders = false;
            this.xrTableCell268.StylePriority.UseTextAlignment = false;
            this.xrTableCell268.Text = "6其他";
            this.xrTableCell268.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell268.Weight = 0.28815218001744369D;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel脑血管疾病其他});
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.Weight = 2.0435693128179353D;
            // 
            // xrLabel脑血管疾病其他
            // 
            this.xrLabel脑血管疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel脑血管疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 0F);
            this.xrLabel脑血管疾病其他.Name = "xrLabel脑血管疾病其他";
            this.xrLabel脑血管疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脑血管疾病其他.SizeF = new System.Drawing.SizeF(366.7783F, 21F);
            this.xrLabel脑血管疾病其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell273
            // 
            this.xrTableCell273.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel脑血管疾病1});
            this.xrTableCell273.Name = "xrTableCell273";
            this.xrTableCell273.Weight = 0.085479062186303023D;
            // 
            // xrLabel脑血管疾病1
            // 
            this.xrLabel脑血管疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel脑血管疾病1.CanGrow = false;
            this.xrLabel脑血管疾病1.LocationFloat = new DevExpress.Utils.PointFloat(0.1946411F, 6.569641F);
            this.xrLabel脑血管疾病1.Name = "xrLabel脑血管疾病1";
            this.xrLabel脑血管疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脑血管疾病1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel脑血管疾病1.StylePriority.UseBorders = false;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.Text = "/";
            this.xrTableCell274.Weight = 0.027289038018621307D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel脑血管疾病2});
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Weight = 0.10246776437693679D;
            // 
            // xrLabel脑血管疾病2
            // 
            this.xrLabel脑血管疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel脑血管疾病2.CanGrow = false;
            this.xrLabel脑血管疾病2.LocationFloat = new DevExpress.Utils.PointFloat(0.1946411F, 6.569641F);
            this.xrLabel脑血管疾病2.Name = "xrLabel脑血管疾病2";
            this.xrLabel脑血管疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脑血管疾病2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel脑血管疾病2.StylePriority.UseBorders = false;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.Text = "/";
            this.xrTableCell275.Weight = 0.057700796571729718D;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel脑血管疾病3});
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.Weight = 0.09082150531417689D;
            // 
            // xrLabel脑血管疾病3
            // 
            this.xrLabel脑血管疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel脑血管疾病3.CanGrow = false;
            this.xrLabel脑血管疾病3.LocationFloat = new DevExpress.Utils.PointFloat(0.1946411F, 6.569641F);
            this.xrLabel脑血管疾病3.Name = "xrLabel脑血管疾病3";
            this.xrLabel脑血管疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脑血管疾病3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel脑血管疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.Text = "/";
            this.xrTableCell276.Weight = 0.073499654968866734D;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel脑血管疾病4});
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.Weight = 0.09745847320853844D;
            // 
            // xrLabel脑血管疾病4
            // 
            this.xrLabel脑血管疾病4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel脑血管疾病4.CanGrow = false;
            this.xrLabel脑血管疾病4.LocationFloat = new DevExpress.Utils.PointFloat(0.1946411F, 6.569641F);
            this.xrLabel脑血管疾病4.Name = "xrLabel脑血管疾病4";
            this.xrLabel脑血管疾病4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脑血管疾病4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel脑血管疾病4.StylePriority.UseBorders = false;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.Text = "/";
            this.xrTableCell272.Weight = 0.032054649848873393D;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell270.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel脑血管疾病5});
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.StylePriority.UseBorders = false;
            this.xrTableCell270.Weight = 0.10150756267057512D;
            // 
            // xrLabel脑血管疾病5
            // 
            this.xrLabel脑血管疾病5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel脑血管疾病5.CanGrow = false;
            this.xrLabel脑血管疾病5.LocationFloat = new DevExpress.Utils.PointFloat(0.0007629395F, 6.569608F);
            this.xrLabel脑血管疾病5.Name = "xrLabel脑血管疾病5";
            this.xrLabel脑血管疾病5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脑血管疾病5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel脑血管疾病5.StylePriority.UseBorders = false;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell242,
            this.xrTableCell243,
            this.xrTableCell245});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.StylePriority.UseBorders = false;
            this.xrTableRow62.Weight = 2D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.StylePriority.UseBorders = false;
            this.xrTableCell242.Weight = 0.27932898095751213D;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.StylePriority.UseBorders = false;
            this.xrTableCell243.Text = "肾脏疾病";
            this.xrTableCell243.Weight = 0.51144959936312373D;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12});
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Weight = 2.209221419679364D;
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow78,
            this.xrTableRow79});
            this.xrTable12.SizeF = new System.Drawing.SizeF(561.5409F, 50F);
            this.xrTable12.StylePriority.UseBorders = false;
            this.xrTable12.StylePriority.UseTextAlignment = false;
            this.xrTable12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow78
            // 
            this.xrTableRow78.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell309});
            this.xrTableRow78.Name = "xrTableRow78";
            this.xrTableRow78.Weight = 1D;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.StylePriority.UseBorders = false;
            this.xrTableCell309.Text = "1未发现 2糖尿病肾病 3肾功能衰竭 4急性肾炎 5慢性肾炎";
            this.xrTableCell309.Weight = 3D;
            // 
            // xrTableRow79
            // 
            this.xrTableRow79.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell312,
            this.xrTableCell313,
            this.xrTableCell319,
            this.xrTableCell316,
            this.xrTableCell317,
            this.xrTableCell200,
            this.xrTableCell320,
            this.xrTableCell318,
            this.xrTableCell244,
            this.xrTableCell315,
            this.xrTableCell314});
            this.xrTableRow79.Name = "xrTableRow79";
            this.xrTableRow79.Weight = 1D;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.StylePriority.UseBorders = false;
            this.xrTableCell312.Text = "6心前区疼痛 7其他";
            this.xrTableCell312.Weight = 1.0078976909283963D;
            // 
            // xrTableCell313
            // 
            this.xrTableCell313.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel肾脏疾病其他});
            this.xrTableCell313.Name = "xrTableCell313";
            this.xrTableCell313.Weight = 1.3238240892778694D;
            // 
            // xrLabel肾脏疾病其他
            // 
            this.xrLabel肾脏疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel肾脏疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0.0001220703F, 3.013062F);
            this.xrLabel肾脏疾病其他.Name = "xrLabel肾脏疾病其他";
            this.xrLabel肾脏疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肾脏疾病其他.SizeF = new System.Drawing.SizeF(220.6909F, 21F);
            this.xrLabel肾脏疾病其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel肾脏疾病1});
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.Weight = 0.085479081841494081D;
            // 
            // xrLabel肾脏疾病1
            // 
            this.xrLabel肾脏疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel肾脏疾病1.CanGrow = false;
            this.xrLabel肾脏疾病1.LocationFloat = new DevExpress.Utils.PointFloat(1F, 5F);
            this.xrLabel肾脏疾病1.Name = "xrLabel肾脏疾病1";
            this.xrLabel肾脏疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肾脏疾病1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel肾脏疾病1.StylePriority.UseBorders = false;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.Text = "/";
            this.xrTableCell316.Weight = 0.030110352458477617D;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel肾脏疾病2});
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.Weight = 0.0996452893510464D;
            // 
            // xrLabel肾脏疾病2
            // 
            this.xrLabel肾脏疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel肾脏疾病2.CanGrow = false;
            this.xrLabel肾脏疾病2.LocationFloat = new DevExpress.Utils.PointFloat(0.1945496F, 5F);
            this.xrLabel肾脏疾病2.Name = "xrLabel肾脏疾病2";
            this.xrLabel肾脏疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肾脏疾病2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel肾脏疾病2.StylePriority.UseBorders = false;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Text = "/";
            this.xrTableCell200.Weight = 0.032054656073513585D;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel肾脏疾病3});
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.Weight = 0.11646853928928384D;
            // 
            // xrLabel肾脏疾病3
            // 
            this.xrLabel肾脏疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel肾脏疾病3.CanGrow = false;
            this.xrLabel肾脏疾病3.LocationFloat = new DevExpress.Utils.PointFloat(4.995664F, 5F);
            this.xrLabel肾脏疾病3.Name = "xrLabel肾脏疾病3";
            this.xrLabel肾脏疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肾脏疾病3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel肾脏疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.Text = "/";
            this.xrTableCell318.Weight = 0.05876752270025308D;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel肾脏疾病4});
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Weight = 0.11219259849903018D;
            // 
            // xrLabel肾脏疾病4
            // 
            this.xrLabel肾脏疾病4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel肾脏疾病4.CanGrow = false;
            this.xrLabel肾脏疾病4.LocationFloat = new DevExpress.Utils.PointFloat(2.757772F, 5F);
            this.xrLabel肾脏疾病4.Name = "xrLabel肾脏疾病4";
            this.xrLabel肾脏疾病4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肾脏疾病4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel肾脏疾病4.StylePriority.UseBorders = false;
            // 
            // xrTableCell315
            // 
            this.xrTableCell315.Name = "xrTableCell315";
            this.xrTableCell315.Text = "/";
            this.xrTableCell315.Weight = 0.032054656414382772D;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell314.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel肾脏疾病5});
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.StylePriority.UseBorders = false;
            this.xrTableCell314.Weight = 0.10150552316625291D;
            // 
            // xrLabel肾脏疾病5
            // 
            this.xrLabel肾脏疾病5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel肾脏疾病5.CanGrow = false;
            this.xrLabel肾脏疾病5.LocationFloat = new DevExpress.Utils.PointFloat(1F, 5F);
            this.xrLabel肾脏疾病5.Name = "xrLabel肾脏疾病5";
            this.xrLabel肾脏疾病5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肾脏疾病5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel肾脏疾病5.StylePriority.UseBorders = false;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell246,
            this.xrTableCell247,
            this.xrTableCell249});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 2D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell246.Multiline = true;
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.StylePriority.UseBorders = false;
            this.xrTableCell246.Text = "现存\r\n主要";
            this.xrTableCell246.Weight = 0.27932898095751213D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.StylePriority.UseBorders = false;
            this.xrTableCell247.Text = "心脏疾病";
            this.xrTableCell247.Weight = 0.51144743823639882D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable13});
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.Weight = 2.2092235808060892D;
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow80,
            this.xrTableRow81});
            this.xrTable13.SizeF = new System.Drawing.SizeF(561.5416F, 50F);
            this.xrTable13.StylePriority.UseBorders = false;
            this.xrTable13.StylePriority.UseTextAlignment = false;
            this.xrTable13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow80
            // 
            this.xrTableRow80.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell322});
            this.xrTableRow80.Name = "xrTableRow80";
            this.xrTableRow80.Weight = 1D;
            // 
            // xrTableCell322
            // 
            this.xrTableCell322.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell322.Name = "xrTableCell322";
            this.xrTableCell322.StylePriority.UseBorders = false;
            this.xrTableCell322.Text = "1未发现 2心肌梗死 3心绞痛 4冠状动脉血运重建 5充血性心力";
            this.xrTableCell322.Weight = 3D;
            // 
            // xrTableRow81
            // 
            this.xrTableRow81.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell323,
            this.xrTableCell324,
            this.xrTableCell330,
            this.xrTableCell326,
            this.xrTableCell329,
            this.xrTableCell248,
            this.xrTableCell328,
            this.xrTableCell321,
            this.xrTableCell331,
            this.xrTableCell327,
            this.xrTableCell325});
            this.xrTableRow81.Name = "xrTableRow81";
            this.xrTableRow81.Weight = 1D;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.StylePriority.UseBorders = false;
            this.xrTableCell323.Text = "6心前区疼疼 7其他";
            this.xrTableCell323.Weight = 1D;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel心脏疾病其他});
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.Weight = 1.3317218326228748D;
            // 
            // xrLabel心脏疾病其他
            // 
            this.xrLabel心脏疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel心脏疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(1.48468F, 1.065674F);
            this.xrLabel心脏疾病其他.Name = "xrLabel心脏疾病其他";
            this.xrLabel心脏疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心脏疾病其他.SizeF = new System.Drawing.SizeF(220.6909F, 21F);
            this.xrLabel心脏疾病其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel心脏疾病1});
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.Weight = 0.094219775958798144D;
            // 
            // xrLabel心脏疾病1
            // 
            this.xrLabel心脏疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel心脏疾病1.CanGrow = false;
            this.xrLabel心脏疾病1.LocationFloat = new DevExpress.Utils.PointFloat(1F, 5F);
            this.xrLabel心脏疾病1.Name = "xrLabel心脏疾病1";
            this.xrLabel心脏疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心脏疾病1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel心脏疾病1.StylePriority.UseBorders = false;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.Text = "/";
            this.xrTableCell326.Weight = 0.022409571043199572D;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel心脏疾病2});
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.Weight = 0.098605503839215375D;
            // 
            // xrLabel心脏疾病2
            // 
            this.xrLabel心脏疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel心脏疾病2.CanGrow = false;
            this.xrLabel心脏疾病2.LocationFloat = new DevExpress.Utils.PointFloat(1F, 5F);
            this.xrLabel心脏疾病2.Name = "xrLabel心脏疾病2";
            this.xrLabel心脏疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心脏疾病2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel心脏疾病2.StylePriority.UseBorders = false;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Text = "/";
            this.xrTableCell248.Weight = 0.032054613083607755D;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel心脏疾病3});
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.Weight = 0.1164684574068611D;
            // 
            // xrLabel心脏疾病3
            // 
            this.xrLabel心脏疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel心脏疾病3.CanGrow = false;
            this.xrLabel心脏疾病3.LocationFloat = new DevExpress.Utils.PointFloat(4.9956F, 5.000051F);
            this.xrLabel心脏疾病3.Name = "xrLabel心脏疾病3";
            this.xrLabel心脏疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心脏疾病3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel心脏疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableCell321
            // 
            this.xrTableCell321.Name = "xrTableCell321";
            this.xrTableCell321.Text = "/";
            this.xrTableCell321.Weight = 0.060787813082591985D;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel心脏疾病4});
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.Weight = 0.11017078360252772D;
            // 
            // xrLabel心脏疾病4
            // 
            this.xrLabel心脏疾病4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel心脏疾病4.CanGrow = false;
            this.xrLabel心脏疾病4.LocationFloat = new DevExpress.Utils.PointFloat(1F, 5F);
            this.xrLabel心脏疾病4.Name = "xrLabel心脏疾病4";
            this.xrLabel心脏疾病4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心脏疾病4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel心脏疾病4.StylePriority.UseBorders = false;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.Text = "/";
            this.xrTableCell327.Weight = 0.032053964085979235D;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell325.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel心脏疾病5});
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.StylePriority.UseBorders = false;
            this.xrTableCell325.Weight = 0.10150768527434423D;
            // 
            // xrLabel心脏疾病5
            // 
            this.xrLabel心脏疾病5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel心脏疾病5.CanGrow = false;
            this.xrLabel心脏疾病5.LocationFloat = new DevExpress.Utils.PointFloat(1F, 5F);
            this.xrLabel心脏疾病5.Name = "xrLabel心脏疾病5";
            this.xrLabel心脏疾病5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心脏疾病5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel心脏疾病5.StylePriority.UseBorders = false;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell250,
            this.xrTableCell251,
            this.xrTableCell252,
            this.xrTableCell332,
            this.xrTableCell335,
            this.xrTableCell333,
            this.xrTableCell336,
            this.xrTableCell334,
            this.xrTableCell253});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.StylePriority.UseBorders = false;
            this.xrTableRow64.Weight = 1D;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseBorders = false;
            this.xrTableCell250.Text = "健康";
            this.xrTableCell250.Weight = 0.27932898095751213D;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.StylePriority.UseBorders = false;
            this.xrTableCell251.Text = "血管疾病";
            this.xrTableCell251.Weight = 0.51144743823639882D;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.StylePriority.UseBorders = false;
            this.xrTableCell252.Text = "1未发现 2夹层动肪瘤 3动脉闭塞性疾病 4其他";
            this.xrTableCell252.Weight = 1.6722039602567196D;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血管疾病其他});
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.Weight = 0.24512267465765952D;
            // 
            // xrLabel血管疾病其他
            // 
            this.xrLabel血管疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel血管疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel血管疾病其他.Name = "xrLabel血管疾病其他";
            this.xrLabel血管疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血管疾病其他.SizeF = new System.Drawing.SizeF(61.46509F, 21F);
            this.xrLabel血管疾病其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血管疾病1});
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.Weight = 0.067646154874305267D;
            // 
            // xrLabel血管疾病1
            // 
            this.xrLabel血管疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel血管疾病1.CanGrow = false;
            this.xrLabel血管疾病1.LocationFloat = new DevExpress.Utils.PointFloat(2F, 5F);
            this.xrLabel血管疾病1.Name = "xrLabel血管疾病1";
            this.xrLabel血管疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血管疾病1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel血管疾病1.StylePriority.UseBorders = false;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.Text = "/";
            this.xrTableCell333.Weight = 0.041023467369948D;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血管疾病2});
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.Weight = 0.084871766556712011D;
            // 
            // xrLabel血管疾病2
            // 
            this.xrLabel血管疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel血管疾病2.CanGrow = false;
            this.xrLabel血管疾病2.LocationFloat = new DevExpress.Utils.PointFloat(2F, 5F);
            this.xrLabel血管疾病2.Name = "xrLabel血管疾病2";
            this.xrLabel血管疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血管疾病2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel血管疾病2.StylePriority.UseBorders = false;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.Text = "/";
            this.xrTableCell334.Weight = 0.02360526840523576D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell253.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血管疾病3});
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.StylePriority.UseBorders = false;
            this.xrTableCell253.Weight = 0.074750288685509078D;
            // 
            // xrLabel血管疾病3
            // 
            this.xrLabel血管疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel血管疾病3.CanGrow = false;
            this.xrLabel血管疾病3.LocationFloat = new DevExpress.Utils.PointFloat(2F, 5F);
            this.xrLabel血管疾病3.Name = "xrLabel血管疾病3";
            this.xrLabel血管疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血管疾病3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel血管疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell254,
            this.xrTableCell255,
            this.xrTableCell256,
            this.xrTableCell339,
            this.xrTableCell340,
            this.xrTableCell337,
            this.xrTableCell338,
            this.xrTableCell257});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 2D;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 5, 0, 100F);
            this.xrTableCell254.StylePriority.UseBorders = false;
            this.xrTableCell254.StylePriority.UsePadding = false;
            this.xrTableCell254.StylePriority.UseTextAlignment = false;
            this.xrTableCell254.Text = "问题";
            this.xrTableCell254.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell254.Weight = 0.27932898095751213D;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.StylePriority.UseBorders = false;
            this.xrTableCell255.Text = "眼部疾病";
            this.xrTableCell255.Weight = 0.51144959936312384D;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell256.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable14});
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.StylePriority.UseBorders = false;
            this.xrTableCell256.Weight = 1.9173243262107134D;
            // 
            // xrTable14
            // 
            this.xrTable14.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(0.0001831055F, 1.811279F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow82,
            this.xrTableRow83});
            this.xrTable14.SizeF = new System.Drawing.SizeF(486.506F, 50F);
            this.xrTable14.StylePriority.UseBorders = false;
            this.xrTable14.StylePriority.UseTextAlignment = false;
            this.xrTable14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow82
            // 
            this.xrTableRow82.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell343});
            this.xrTableRow82.Name = "xrTableRow82";
            this.xrTableRow82.Weight = 1D;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.StylePriority.UseBorders = false;
            this.xrTableCell343.Text = "1未发现 2视网膜出血或渗出 3视乳头水肿 4白内障";
            this.xrTableCell343.Weight = 3D;
            // 
            // xrTableRow83
            // 
            this.xrTableRow83.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell344,
            this.xrTableCell346});
            this.xrTableRow83.Name = "xrTableRow83";
            this.xrTableRow83.Weight = 1D;
            // 
            // xrTableCell344
            // 
            this.xrTableCell344.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell344.Name = "xrTableCell344";
            this.xrTableCell344.StylePriority.UseBorders = false;
            this.xrTableCell344.Text = "5其他";
            this.xrTableCell344.Weight = 0.33238433096390552D;
            // 
            // xrTableCell346
            // 
            this.xrTableCell346.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel眼部疾病其他});
            this.xrTableCell346.Name = "xrTableCell346";
            this.xrTableCell346.Weight = 2.6676156690360946D;
            // 
            // xrLabel眼部疾病其他
            // 
            this.xrLabel眼部疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel眼部疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel眼部疾病其他.Name = "xrLabel眼部疾病其他";
            this.xrLabel眼部疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel眼部疾病其他.SizeF = new System.Drawing.SizeF(182.7809F, 21F);
            this.xrLabel眼部疾病其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell339.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel眼部疾病1});
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.StylePriority.UseBorders = false;
            this.xrTableCell339.Weight = 0.0676460079226906D;
            // 
            // xrLabel眼部疾病1
            // 
            this.xrLabel眼部疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel眼部疾病1.CanGrow = false;
            this.xrLabel眼部疾病1.LocationFloat = new DevExpress.Utils.PointFloat(0.1945801F, 23.14459F);
            this.xrLabel眼部疾病1.Name = "xrLabel眼部疾病1";
            this.xrLabel眼部疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel眼部疾病1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel眼部疾病1.StylePriority.UseBorders = false;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.StylePriority.UseBorders = false;
            this.xrTableCell340.Text = "/";
            this.xrTableCell340.Weight = 0.041815402128053493D;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell337.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel眼部疾病2});
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.StylePriority.UseBorders = false;
            this.xrTableCell337.Weight = 0.084080429922944649D;
            // 
            // xrLabel眼部疾病2
            // 
            this.xrLabel眼部疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel眼部疾病2.CanGrow = false;
            this.xrLabel眼部疾病2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 23.14459F);
            this.xrLabel眼部疾病2.Name = "xrLabel眼部疾病2";
            this.xrLabel眼部疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel眼部疾病2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel眼部疾病2.StylePriority.UseBorders = false;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.StylePriority.UseBorders = false;
            this.xrTableCell338.Text = "/";
            this.xrTableCell338.Weight = 0.023603824840118658D;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell257.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel眼部疾病3});
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.StylePriority.UseBorders = false;
            this.xrTableCell257.Weight = 0.074751428654843508D;
            // 
            // xrLabel眼部疾病3
            // 
            this.xrLabel眼部疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel眼部疾病3.CanGrow = false;
            this.xrLabel眼部疾病3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 23.14459F);
            this.xrLabel眼部疾病3.Name = "xrLabel眼部疾病3";
            this.xrLabel眼部疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel眼部疾病3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel眼部疾病3.StylePriority.UseBorders = false;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell258,
            this.xrTableCell259,
            this.xrTableCell260,
            this.xrTableCell341,
            this.xrTableCell261});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.StylePriority.UseBorders = false;
            this.xrTableRow66.Weight = 1D;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.StylePriority.UseBorders = false;
            this.xrTableCell258.Weight = 0.27932899596533661D;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.StylePriority.UseTextAlignment = false;
            this.xrTableCell259.Text = "神经系统疾病";
            this.xrTableCell259.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell259.Weight = 0.51144985449614D;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.StylePriority.UseTextAlignment = false;
            this.xrTableCell260.Text = "1未发现 2有";
            this.xrTableCell260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell260.Weight = 0.45929013550504688D;
            // 
            // xrTableCell341
            // 
            this.xrTableCell341.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell341.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel神经系统疾病其他});
            this.xrTableCell341.Name = "xrTableCell341";
            this.xrTableCell341.StylePriority.UseBorders = false;
            this.xrTableCell341.Weight = 1.6515753103037798D;
            // 
            // xrLabel神经系统疾病其他
            // 
            this.xrLabel神经系统疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel神经系统疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel神经系统疾病其他.Name = "xrLabel神经系统疾病其他";
            this.xrLabel神经系统疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel神经系统疾病其他.SizeF = new System.Drawing.SizeF(370.6039F, 20.99994F);
            this.xrLabel神经系统疾病其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell261.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel神经系统疾病});
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseBorders = false;
            this.xrTableCell261.Weight = 0.098355703729696542D;
            // 
            // xrLabel神经系统疾病
            // 
            this.xrLabel神经系统疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel神经系统疾病.CanGrow = false;
            this.xrLabel神经系统疾病.LocationFloat = new DevExpress.Utils.PointFloat(5.999756F, 5.999939F);
            this.xrLabel神经系统疾病.Name = "xrLabel神经系统疾病";
            this.xrLabel神经系统疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel神经系统疾病.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel神经系统疾病.StylePriority.UseBorders = false;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell262,
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell342,
            this.xrTableCell265});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.StylePriority.UseBorders = false;
            this.xrTableRow67.Weight = 1D;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.Weight = 0.27932898095751213D;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseTextAlignment = false;
            this.xrTableCell263.Text = "其他系统疾病";
            this.xrTableCell263.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell263.Weight = 0.51145007961350708D;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.StylePriority.UseTextAlignment = false;
            this.xrTableCell264.Text = "1未发现 2有";
            this.xrTableCell264.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell264.Weight = 0.45929040564588758D;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell342.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel其他系统疾病其他});
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.StylePriority.UseBorders = false;
            this.xrTableCell342.Weight = 1.6395848989829338D;
            // 
            // xrLabel其他系统疾病其他
            // 
            this.xrLabel其他系统疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel其他系统疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel其他系统疾病其他.Name = "xrLabel其他系统疾病其他";
            this.xrLabel其他系统疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel其他系统疾病其他.SizeF = new System.Drawing.SizeF(370.6038F, 21F);
            this.xrLabel其他系统疾病其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell265.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel其他系统疾病});
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseBorders = false;
            this.xrTableCell265.Weight = 0.11034563480015944D;
            // 
            // xrLabel其他系统疾病
            // 
            this.xrLabel其他系统疾病.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel其他系统疾病.CanGrow = false;
            this.xrLabel其他系统疾病.LocationFloat = new DevExpress.Utils.PointFloat(9.047363F, 7.72349F);
            this.xrLabel其他系统疾病.Name = "xrLabel其他系统疾病";
            this.xrLabel其他系统疾病.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel其他系统疾病.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel其他系统疾病.StylePriority.UseBorders = false;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 81.25F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3});
            this.xrTable1.SizeF = new System.Drawing.SizeF(765.625F, 987F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 6.48D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Font = new System.Drawing.Font("仿宋", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell1.Multiline = true;
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "脏\r\n器\r\n功\r\n能";
            this.xrTableCell1.Weight = 0.15102043307557397D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "xrTableCell3";
            this.xrTableCell3.Weight = 2.848979566924426D;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9});
            this.xrTable2.SizeF = new System.Drawing.SizeF(727.0833F, 162F);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell10,
            this.xrTableCell11});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Weight = 0.52702700265457814D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = "口唇 1红润 2苍白 3发绀 4皲裂 5疱疹";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell10.Weight = 2.5297293971944157D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel口唇});
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.Weight = 0.087387115763379875D;
            // 
            // xrLabel口唇
            // 
            this.xrLabel口唇.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel口唇.CanGrow = false;
            this.xrLabel口唇.LocationFloat = new DevExpress.Utils.PointFloat(2.999893F, 6.25F);
            this.xrLabel口唇.Name = "xrLabel口唇";
            this.xrLabel口唇.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel口唇.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel口唇.StylePriority.UseBorders = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell345,
            this.xrTableCell347,
            this.xrTableCell348,
            this.xrTableCell349,
            this.xrTableCell350,
            this.xrTableCell13,
            this.xrTableCell14});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.Text = "口    腔";
            this.xrTableCell12.Weight = 0.52702700265457814D;
            // 
            // xrTableCell345
            // 
            this.xrTableCell345.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell345.Name = "xrTableCell345";
            this.xrTableCell345.StylePriority.UseBorders = false;
            this.xrTableCell345.StylePriority.UseTextAlignment = false;
            this.xrTableCell345.Text = "齿列 1正常 2缺齿";
            this.xrTableCell345.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell345.Weight = 0.69932412331864724D;
            // 
            // xrTableCell347
            // 
            this.xrTableCell347.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell347.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable15});
            this.xrTableCell347.Name = "xrTableCell347";
            this.xrTableCell347.StylePriority.UseBorders = false;
            this.xrTableCell347.Weight = 0.34594587907398433D;
            // 
            // xrTable15
            // 
            this.xrTable15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.973684F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow84,
            this.xrTableRow85});
            this.xrTable15.SizeF = new System.Drawing.SizeF(80.00003F, 24.02631F);
            this.xrTable15.StylePriority.UseBorders = false;
            // 
            // xrTableRow84
            // 
            this.xrTableRow84.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell缺齿1,
            this.xrTableCell缺齿2});
            this.xrTableRow84.Name = "xrTableRow84";
            this.xrTableRow84.Weight = 1D;
            // 
            // xrTableCell缺齿1
            // 
            this.xrTableCell缺齿1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell缺齿1.Name = "xrTableCell缺齿1";
            this.xrTableCell缺齿1.StylePriority.UseBorders = false;
            this.xrTableCell缺齿1.Weight = 1.500652861346107D;
            // 
            // xrTableCell缺齿2
            // 
            this.xrTableCell缺齿2.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell缺齿2.Name = "xrTableCell缺齿2";
            this.xrTableCell缺齿2.StylePriority.UseBorders = false;
            this.xrTableCell缺齿2.Weight = 1.499347138653893D;
            // 
            // xrTableRow85
            // 
            this.xrTableRow85.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell缺齿3,
            this.xrTableCell缺齿4});
            this.xrTableRow85.Name = "xrTableRow85";
            this.xrTableRow85.Weight = 1D;
            // 
            // xrTableCell缺齿3
            // 
            this.xrTableCell缺齿3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell缺齿3.Name = "xrTableCell缺齿3";
            this.xrTableCell缺齿3.StylePriority.UseBorders = false;
            this.xrTableCell缺齿3.Weight = 1.500652861346107D;
            // 
            // xrTableCell缺齿4
            // 
            this.xrTableCell缺齿4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell缺齿4.Name = "xrTableCell缺齿4";
            this.xrTableCell缺齿4.StylePriority.UseBorders = false;
            this.xrTableCell缺齿4.Weight = 1.499347138653893D;
            // 
            // xrTableCell348
            // 
            this.xrTableCell348.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell348.Name = "xrTableCell348";
            this.xrTableCell348.StylePriority.UseBorders = false;
            this.xrTableCell348.StylePriority.UseTextAlignment = false;
            this.xrTableCell348.Text = "3龋齿";
            this.xrTableCell348.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell348.Weight = 0.26103642735286176D;
            // 
            // xrTableCell349
            // 
            this.xrTableCell349.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell349.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable16});
            this.xrTableCell349.Name = "xrTableCell349";
            this.xrTableCell349.StylePriority.UseBorders = false;
            this.xrTableCell349.StylePriority.UseTextAlignment = false;
            this.xrTableCell349.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell349.Weight = 0.34594588462070164D;
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.973716F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow86,
            this.xrTableRow87});
            this.xrTable16.SizeF = new System.Drawing.SizeF(80.00003F, 24.02631F);
            this.xrTable16.StylePriority.UseBorders = false;
            // 
            // xrTableRow86
            // 
            this.xrTableRow86.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell龋齿1,
            this.xrTableCell龋齿2});
            this.xrTableRow86.Name = "xrTableRow86";
            this.xrTableRow86.Weight = 1D;
            // 
            // xrTableCell龋齿1
            // 
            this.xrTableCell龋齿1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell龋齿1.Name = "xrTableCell龋齿1";
            this.xrTableCell龋齿1.StylePriority.UseBorders = false;
            this.xrTableCell龋齿1.Weight = 1.500652861346107D;
            // 
            // xrTableCell龋齿2
            // 
            this.xrTableCell龋齿2.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell龋齿2.Name = "xrTableCell龋齿2";
            this.xrTableCell龋齿2.StylePriority.UseBorders = false;
            this.xrTableCell龋齿2.Weight = 1.499347138653893D;
            // 
            // xrTableRow87
            // 
            this.xrTableRow87.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell龋齿3,
            this.xrTableCell龋齿4});
            this.xrTableRow87.Name = "xrTableRow87";
            this.xrTableRow87.Weight = 1D;
            // 
            // xrTableCell龋齿3
            // 
            this.xrTableCell龋齿3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell龋齿3.Name = "xrTableCell龋齿3";
            this.xrTableCell龋齿3.StylePriority.UseBorders = false;
            this.xrTableCell龋齿3.Weight = 1.500652861346107D;
            // 
            // xrTableCell龋齿4
            // 
            this.xrTableCell龋齿4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell龋齿4.Name = "xrTableCell龋齿4";
            this.xrTableCell龋齿4.StylePriority.UseBorders = false;
            this.xrTableCell龋齿4.Weight = 1.499347138653893D;
            // 
            // xrTableCell350
            // 
            this.xrTableCell350.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell350.Name = "xrTableCell350";
            this.xrTableCell350.StylePriority.UseBorders = false;
            this.xrTableCell350.Text = "4义齿（假牙)";
            this.xrTableCell350.Weight = 0.54008984234877122D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell13.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable17});
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell13.Weight = 0.33738697654366168D;
            // 
            // xrTable17
            // 
            this.xrTable17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(7.629395E-05F, 4.577637E-05F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow88,
            this.xrTableRow89});
            this.xrTable17.SizeF = new System.Drawing.SizeF(78.02075F, 26.99998F);
            this.xrTable17.StylePriority.UseBorders = false;
            // 
            // xrTableRow88
            // 
            this.xrTableRow88.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell义齿1,
            this.xrTableCell义齿2});
            this.xrTableRow88.Name = "xrTableRow88";
            this.xrTableRow88.Weight = 1D;
            // 
            // xrTableCell义齿1
            // 
            this.xrTableCell义齿1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell义齿1.Name = "xrTableCell义齿1";
            this.xrTableCell义齿1.StylePriority.UseBorders = false;
            this.xrTableCell义齿1.Weight = 1.500652861346107D;
            // 
            // xrTableCell义齿2
            // 
            this.xrTableCell义齿2.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell义齿2.Name = "xrTableCell义齿2";
            this.xrTableCell义齿2.StylePriority.UseBorders = false;
            this.xrTableCell义齿2.Weight = 1.499347138653893D;
            // 
            // xrTableRow89
            // 
            this.xrTableRow89.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell义齿3,
            this.xrTableCell义齿4});
            this.xrTableRow89.Name = "xrTableRow89";
            this.xrTableRow89.Weight = 1D;
            // 
            // xrTableCell义齿3
            // 
            this.xrTableCell义齿3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell义齿3.Name = "xrTableCell义齿3";
            this.xrTableCell义齿3.StylePriority.UseBorders = false;
            this.xrTableCell义齿3.Weight = 1.500652861346107D;
            // 
            // xrTableCell义齿4
            // 
            this.xrTableCell义齿4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell义齿4.Name = "xrTableCell义齿4";
            this.xrTableCell义齿4.StylePriority.UseBorders = false;
            this.xrTableCell义齿4.Weight = 1.499347138653893D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel齿列有无});
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.Weight = 0.087387379699167433D;
            // 
            // xrLabel齿列有无
            // 
            this.xrLabel齿列有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel齿列有无.CanGrow = false;
            this.xrLabel齿列有无.LocationFloat = new DevExpress.Utils.PointFloat(2.999893F, 4.25F);
            this.xrLabel齿列有无.Name = "xrLabel齿列有无";
            this.xrLabel齿列有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel齿列有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel齿列有无.StylePriority.UseBorders = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Weight = 0.52702700265457814D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "咽部 1无充血 2充血 3淋巴滤泡增生";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell16.Weight = 2.5297293971944157D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel咽部});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Weight = 0.087387115763379875D;
            // 
            // xrLabel咽部
            // 
            this.xrLabel咽部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel咽部.CanGrow = false;
            this.xrLabel咽部.LocationFloat = new DevExpress.Utils.PointFloat(2.999893F, 2.25F);
            this.xrLabel咽部.Name = "xrLabel咽部";
            this.xrLabel咽部.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel咽部.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel咽部.StylePriority.UseBorders = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell140,
            this.xrTableCell135,
            this.xrTableCell132,
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell134,
            this.xrTableCell143,
            this.xrTableCell20});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.StylePriority.UseTextAlignment = false;
            this.xrTableRow7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "视    力";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell18.Weight = 0.52702700265457814D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.Text = "左眼";
            this.xrTableCell19.Weight = 0.22072077828329295D;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell140.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel左眼});
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.Weight = 0.36756752973082679D;
            // 
            // xrLabel左眼
            // 
            this.xrLabel左眼.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel左眼.LocationFloat = new DevExpress.Utils.PointFloat(0.0001068115F, 1.525879E-05F);
            this.xrLabel左眼.Name = "xrLabel左眼";
            this.xrLabel左眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel左眼.SizeF = new System.Drawing.SizeF(84.99997F, 21.99994F);
            this.xrLabel左眼.StylePriority.UseBorders = false;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.StylePriority.UseBorders = false;
            this.xrTableCell135.Text = "右眼";
            this.xrTableCell135.Weight = 0.19055305522829957D;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell132.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel右眼});
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Weight = 0.36756755722413836D;
            // 
            // xrLabel右眼
            // 
            this.xrLabel右眼.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel右眼.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.25F);
            this.xrLabel右眼.Name = "xrLabel右眼";
            this.xrLabel右眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel右眼.SizeF = new System.Drawing.SizeF(84.99997F, 21.99994F);
            this.xrLabel右眼.StylePriority.UseBorders = false;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.StylePriority.UseBorders = false;
            this.xrTableCell141.StylePriority.UseTextAlignment = false;
            this.xrTableCell141.Text = "(矫正视力：左眼";
            this.xrTableCell141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell141.Weight = 0.6635006652501525D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell142.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel左眼矫正});
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.Weight = 0.26378376607511905D;
            // 
            // xrLabel左眼矫正
            // 
            this.xrLabel左眼矫正.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel左眼矫正.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.2499898F);
            this.xrLabel左眼矫正.Name = "xrLabel左眼矫正";
            this.xrLabel左眼矫正.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel左眼矫正.SizeF = new System.Drawing.SizeF(61F, 21.99994F);
            this.xrLabel左眼矫正.StylePriority.UseBorders = false;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.StylePriority.UseBorders = false;
            this.xrTableCell134.Text = "右眼";
            this.xrTableCell134.Weight = 0.19459457472510416D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell143.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel右眼矫正});
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.Weight = 0.26144105690314812D;
            // 
            // xrLabel右眼矫正
            // 
            this.xrLabel右眼矫正.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel右眼矫正.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 0.25F);
            this.xrLabel右眼矫正.Name = "xrLabel右眼矫正";
            this.xrLabel右眼矫正.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel右眼矫正.SizeF = new System.Drawing.SizeF(60.45807F, 21.99994F);
            this.xrLabel右眼矫正.StylePriority.UseBorders = false;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.Text = "）";
            this.xrTableCell20.Weight = 0.087387529537713823D;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22,
            this.xrTableCell23});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.Text = "听    力";
            this.xrTableCell21.Weight = 0.52702700265457814D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.Text = "1听见 2听不清或无法听见";
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell22.Weight = 2.5297286053870529D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel听力});
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.Weight = 0.087387907570742662D;
            // 
            // xrLabel听力
            // 
            this.xrLabel听力.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel听力.CanGrow = false;
            this.xrLabel听力.LocationFloat = new DevExpress.Utils.PointFloat(3.000076F, 10F);
            this.xrLabel听力.Name = "xrLabel听力";
            this.xrLabel听力.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel听力.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel听力.StylePriority.UseBorders = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell26});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.Text = "运动功能";
            this.xrTableCell24.Weight = 0.52702700265457814D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)));
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.Text = "1可顺利完成 2无法独立完成其中任何一个动作";
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell25.Weight = 2.5297288693228404D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel运动功能});
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Weight = 0.087387643634955214D;
            // 
            // xrLabel运动功能
            // 
            this.xrLabel运动功能.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel运动功能.CanGrow = false;
            this.xrLabel运动功能.LocationFloat = new DevExpress.Utils.PointFloat(3.000015F, 8.75F);
            this.xrLabel运动功能.Name = "xrLabel运动功能";
            this.xrLabel运动功能.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel运动功能.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel运动功能.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 24.8333349609375D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Font = new System.Drawing.Font("仿宋", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrTableCell4.Multiline = true;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.StylePriority.UseFont = false;
            this.xrTableCell4.Text = "查\r\n\r\n\r\n体";
            this.xrTableCell4.Weight = 0.15102044802295922D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "xrTableCell6";
            this.xrTableCell6.Weight = 2.8489795519770409D;
            // 
            // xrTable3
            // 
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.525879E-05F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow17,
            this.xrTableRow16,
            this.xrTableRow18,
            this.xrTableRow90,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow31,
            this.xrTableRow30,
            this.xrTableRow32});
            this.xrTable3.SizeF = new System.Drawing.SizeF(727.0833F, 620.8334F);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell27,
            this.xrTableCell29,
            this.xrTableCell28});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.Text = "眼   底*";
            this.xrTableCell5.Weight = 0.61112114789022987D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "1正常 2异常";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell27.Weight = 0.58848712490098487D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell29.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel眼底异常});
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.Weight = 2.3448938036500109D;
            // 
            // xrLabel眼底异常
            // 
            this.xrLabel眼底异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel眼底异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel眼底异常.Name = "xrLabel眼底异常";
            this.xrLabel眼底异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel眼底异常.SizeF = new System.Drawing.SizeF(196.1805F, 21.99992F);
            this.xrLabel眼底异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell28.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel眼底});
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.Weight = 0.10133135861736808D;
            // 
            // xrLabel眼底
            // 
            this.xrLabel眼底.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel眼底.CanGrow = false;
            this.xrLabel眼底.LocationFloat = new DevExpress.Utils.PointFloat(2.99995F, 6.749985F);
            this.xrLabel眼底.Name = "xrLabel眼底";
            this.xrLabel眼底.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel眼底.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel眼底.StylePriority.UseBorders = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell31,
            this.xrTableCell32,
            this.xrTableCell33});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.Text = "皮    肤";
            this.xrTableCell30.Weight = 0.61112114789022987D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.Text = "1正常 2潮红 3苍白 4 发绀 5黄染 6色素沉着 7其他";
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell31.Weight = 2.33897498416721D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell32.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel皮肤其他});
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.Weight = 0.59440563833353233D;
            // 
            // xrLabel皮肤其他
            // 
            this.xrLabel皮肤其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel皮肤其他.LocationFloat = new DevExpress.Utils.PointFloat(1.144409E-05F, 0F);
            this.xrLabel皮肤其他.Name = "xrLabel皮肤其他";
            this.xrLabel皮肤其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel皮肤其他.SizeF = new System.Drawing.SizeF(118.5413F, 21.99994F);
            this.xrLabel皮肤其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel皮肤});
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.Weight = 0.10133166466762145D;
            // 
            // xrLabel皮肤
            // 
            this.xrLabel皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel皮肤.CanGrow = false;
            this.xrLabel皮肤.LocationFloat = new DevExpress.Utils.PointFloat(3.000011F, 5.801834F);
            this.xrLabel皮肤.Name = "xrLabel皮肤";
            this.xrLabel皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel皮肤.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel皮肤.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell35,
            this.xrTableCell36,
            this.xrTableCell37});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Text = "巩    膜";
            this.xrTableCell34.Weight = 0.61112114789022987D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseBorders = false;
            this.xrTableCell35.StylePriority.UseTextAlignment = false;
            this.xrTableCell35.Text = "1正常 2黄染 3充血 4其他";
            this.xrTableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell35.Weight = 1.1903952014963091D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell36.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel巩膜其他});
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.Weight = 1.7429864921803198D;
            // 
            // xrLabel巩膜其他
            // 
            this.xrLabel巩膜其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel巩膜其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel巩膜其他.Name = "xrLabel巩膜其他";
            this.xrLabel巩膜其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel巩膜其他.SizeF = new System.Drawing.SizeF(229.0596F, 21.99994F);
            this.xrLabel巩膜其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel巩膜});
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Weight = 0.10133059349173501D;
            // 
            // xrLabel巩膜
            // 
            this.xrLabel巩膜.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel巩膜.CanGrow = false;
            this.xrLabel巩膜.LocationFloat = new DevExpress.Utils.PointFloat(2.999767F, 4.853699F);
            this.xrLabel巩膜.Name = "xrLabel巩膜";
            this.xrLabel巩膜.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel巩膜.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel巩膜.StylePriority.UseBorders = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 15, 0, 0, 100F);
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.StylePriority.UsePadding = false;
            this.xrTableCell38.Text = "淋巴结 ";
            this.xrTableCell38.Weight = 0.61112114789022987D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.Text = "1未触及 2锁骨上 3腋窝 4其他";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell39.Weight = 1.3888788521097701D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel淋巴结其他});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.Weight = 1.5445029945919853D;
            // 
            // xrLabel淋巴结其他
            // 
            this.xrLabel淋巴结其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel淋巴结其他.LocationFloat = new DevExpress.Utils.PointFloat(1.144409E-05F, 1.525879E-05F);
            this.xrLabel淋巴结其他.Name = "xrLabel淋巴结其他";
            this.xrLabel淋巴结其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel淋巴结其他.SizeF = new System.Drawing.SizeF(189.4764F, 21.99994F);
            this.xrLabel淋巴结其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell41.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel淋巴结});
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.Weight = 0.10133044046660844D;
            // 
            // xrLabel淋巴结
            // 
            this.xrLabel淋巴结.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel淋巴结.CanGrow = false;
            this.xrLabel淋巴结.LocationFloat = new DevExpress.Utils.PointFloat(2.999767F, 3.905563F);
            this.xrLabel淋巴结.Name = "xrLabel淋巴结";
            this.xrLabel淋巴结.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel淋巴结.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel淋巴结.StylePriority.UseBorders = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44,
            this.xrTableCell45});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.Weight = 0.61112114789022987D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.Text = "桶状胸：1 否    2 是";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell43.Weight = 1.3888788521097701D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.Weight = 1.5445017703909723D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell45.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel桶状胸});
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Weight = 0.10133166466762145D;
            // 
            // xrLabel桶状胸
            // 
            this.xrLabel桶状胸.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel桶状胸.CanGrow = false;
            this.xrLabel桶状胸.LocationFloat = new DevExpress.Utils.PointFloat(3.000011F, 3.948148F);
            this.xrLabel桶状胸.Name = "xrLabel桶状胸";
            this.xrLabel桶状胸.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel桶状胸.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel桶状胸.StylePriority.UseBorders = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48,
            this.xrTableCell49});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Text = "肺";
            this.xrTableCell46.Weight = 0.61112114789022987D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "呼吸音：1正常    2异常";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell47.Weight = 1.1381625611867623D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell48.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel呼吸音异常});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Weight = 1.7952180613139803D;
            // 
            // xrLabel呼吸音异常
            // 
            this.xrLabel呼吸音异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel呼吸音异常.LocationFloat = new DevExpress.Utils.PointFloat(1.144409E-05F, 2.009262F);
            this.xrLabel呼吸音异常.Name = "xrLabel呼吸音异常";
            this.xrLabel呼吸音异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel呼吸音异常.SizeF = new System.Drawing.SizeF(314.5777F, 19F);
            this.xrLabel呼吸音异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel呼吸音});
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.Weight = 0.10133166466762145D;
            // 
            // xrLabel呼吸音
            // 
            this.xrLabel呼吸音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel呼吸音.CanGrow = false;
            this.xrLabel呼吸音.LocationFloat = new DevExpress.Utils.PointFloat(3.000011F, 3.948148F);
            this.xrLabel呼吸音.Name = "xrLabel呼吸音";
            this.xrLabel呼吸音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel呼吸音.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel呼吸音.StylePriority.UseBorders = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell57});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.Weight = 0.61112114789022987D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = "罗音：1无 2干罗音 3湿罗音 4其他";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1.5722008114351302D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell56.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel罗音其他});
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseBorders = false;
            this.xrTableCell56.Weight = 1.3611810352666249D;
            // 
            // xrLabel罗音其他
            // 
            this.xrLabel罗音其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel罗音其他.LocationFloat = new DevExpress.Utils.PointFloat(7.247925E-05F, 1.061111F);
            this.xrLabel罗音其他.Name = "xrLabel罗音其他";
            this.xrLabel罗音其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel罗音其他.SizeF = new System.Drawing.SizeF(228.018F, 21.99994F);
            this.xrLabel罗音其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell57.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel罗音});
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.Weight = 0.10133044046660866D;
            // 
            // xrLabel罗音
            // 
            this.xrLabel罗音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel罗音.CanGrow = false;
            this.xrLabel罗音.LocationFloat = new DevExpress.Utils.PointFloat(2.999767F, 3.948148F);
            this.xrLabel罗音.Name = "xrLabel罗音";
            this.xrLabel罗音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel罗音.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel罗音.StylePriority.UseBorders = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52,
            this.xrTableCell144,
            this.xrTableCell53});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.Text = "心    脏";
            this.xrTableCell50.Weight = 0.61112114789022987D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseBorders = false;
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = "心率";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell51.Weight = 0.25593972937325504D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel心率});
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseBorders = false;
            this.xrTableCell52.Weight = 0.6471758941691369D;
            // 
            // xrLabel心率
            // 
            this.xrLabel心率.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel心率.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.1129608F);
            this.xrLabel心率.Name = "xrLabel心率";
            this.xrLabel心率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心率.SizeF = new System.Drawing.SizeF(129.0654F, 21.99994F);
            this.xrLabel心率.StylePriority.UseBorders = false;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.StylePriority.UseTextAlignment = false;
            this.xrTableCell144.Text = "次/分钟    心律：1齐 2不齐 3绝对不齐";
            this.xrTableCell144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell144.Weight = 2.0302649387413965D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell53.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel心律});
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseBorders = false;
            this.xrTableCell53.Weight = 0.10133172488457483D;
            // 
            // xrLabel心律
            // 
            this.xrLabel心律.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel心律.CanGrow = false;
            this.xrLabel心律.LocationFloat = new DevExpress.Utils.PointFloat(3.000011F, 3.948148F);
            this.xrLabel心律.Name = "xrLabel心律";
            this.xrLabel心律.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心律.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel心律.StylePriority.UseBorders = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTableCell60,
            this.xrTableCell61});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.Weight = 0.61112114789022987D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.StylePriority.UseTextAlignment = false;
            this.xrTableCell59.Text = "杂音：1无 2有";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell59.Weight = 0.68215786006397927D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell60.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel杂音有});
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.Weight = 2.2512239866377763D;
            // 
            // xrLabel杂音有
            // 
            this.xrLabel杂音有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel杂音有.LocationFloat = new DevExpress.Utils.PointFloat(1.144409E-05F, 1.525879E-05F);
            this.xrLabel杂音有.Name = "xrLabel杂音有";
            this.xrLabel杂音有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel杂音有.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel杂音有.StylePriority.UseBorders = false;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell61.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel杂音});
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.Weight = 0.10133044046660844D;
            // 
            // xrLabel杂音
            // 
            this.xrLabel杂音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel杂音.CanGrow = false;
            this.xrLabel杂音.LocationFloat = new DevExpress.Utils.PointFloat(2.999767F, 3.94818F);
            this.xrLabel杂音.Name = "xrLabel杂音";
            this.xrLabel杂音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel杂音.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel杂音.StylePriority.UseBorders = false;
            // 
            // xrTableRow90
            // 
            this.xrTableRow90.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell300,
            this.xrTableCell301,
            this.xrTableCell305,
            this.xrTableCell310});
            this.xrTableRow90.Name = "xrTableRow90";
            this.xrTableRow90.Weight = 1D;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.StylePriority.UseBorders = false;
            this.xrTableCell300.Weight = 0.61112114789022987D;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.StylePriority.UseBorders = false;
            this.xrTableCell301.StylePriority.UseTextAlignment = false;
            this.xrTableCell301.Text = "压痛：1无 2有";
            this.xrTableCell301.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell301.Weight = 0.68215793657654256D;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell305.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel压痛有});
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.StylePriority.UseBorders = false;
            this.xrTableCell305.Text = "xrTableCell305";
            this.xrTableCell305.Weight = 2.2512226859242D;
            // 
            // xrLabel压痛有
            // 
            this.xrLabel压痛有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel压痛有.LocationFloat = new DevExpress.Utils.PointFloat(1.144409E-05F, 0.0001220703F);
            this.xrLabel压痛有.Name = "xrLabel压痛有";
            this.xrLabel压痛有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel压痛有.SizeF = new System.Drawing.SizeF(247.1905F, 22.99997F);
            this.xrLabel压痛有.StylePriority.UseBorders = false;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell310.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel压痛});
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.StylePriority.UseBorders = false;
            this.xrTableCell310.Text = "xrTableCell310";
            this.xrTableCell310.Weight = 0.10133166466762145D;
            // 
            // xrLabel压痛
            // 
            this.xrLabel压痛.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel压痛.CanGrow = false;
            this.xrLabel压痛.LocationFloat = new DevExpress.Utils.PointFloat(3F, 2.948181F);
            this.xrLabel压痛.Name = "xrLabel压痛";
            this.xrLabel压痛.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel压痛.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel压痛.StylePriority.UseBorders = false;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.Weight = 0.61112114789022987D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "包块：1无 2有";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell63.Weight = 0.68215793657654256D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell64.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel包块有});
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.Weight = 2.2512226859242D;
            // 
            // xrLabel包块有
            // 
            this.xrLabel包块有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel包块有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel包块有.Name = "xrLabel包块有";
            this.xrLabel包块有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel包块有.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel包块有.StylePriority.UseBorders = false;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell65.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel包块});
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.Weight = 0.10133166466762145D;
            // 
            // xrLabel包块
            // 
            this.xrLabel包块.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel包块.CanGrow = false;
            this.xrLabel包块.LocationFloat = new DevExpress.Utils.PointFloat(3.000011F, 3.948116F);
            this.xrLabel包块.Name = "xrLabel包块";
            this.xrLabel包块.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel包块.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel包块.StylePriority.UseBorders = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66,
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.Text = "腹    部";
            this.xrTableCell66.Weight = 0.61112114789022987D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.StylePriority.UseTextAlignment = false;
            this.xrTableCell67.Text = "肝大：1无 2有";
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell67.Weight = 0.6821576305262893D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell68.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel肝大有});
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.Weight = 2.2512242161754661D;
            // 
            // xrLabel肝大有
            // 
            this.xrLabel肝大有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel肝大有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel肝大有.Name = "xrLabel肝大有";
            this.xrLabel肝大有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肝大有.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel肝大有.StylePriority.UseBorders = false;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell69.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel肝大});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.Weight = 0.10133044046660844D;
            // 
            // xrLabel肝大
            // 
            this.xrLabel肝大.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel肝大.CanGrow = false;
            this.xrLabel肝大.LocationFloat = new DevExpress.Utils.PointFloat(2.999767F, 3.948148F);
            this.xrLabel肝大.Name = "xrLabel肝大";
            this.xrLabel肝大.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肝大.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel肝大.StylePriority.UseBorders = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell70,
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Weight = 0.61112114789022987D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.StylePriority.UseTextAlignment = false;
            this.xrTableCell71.Text = "脾大：1无 2有";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell71.Weight = 0.6821576305262893D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell72.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel脾大有});
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.Weight = 2.2512242161754661D;
            // 
            // xrLabel脾大有
            // 
            this.xrLabel脾大有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel脾大有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel脾大有.Name = "xrLabel脾大有";
            this.xrLabel脾大有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脾大有.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel脾大有.StylePriority.UseBorders = false;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell73.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel脾大});
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.Weight = 0.10133044046660844D;
            // 
            // xrLabel脾大
            // 
            this.xrLabel脾大.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel脾大.CanGrow = false;
            this.xrLabel脾大.LocationFloat = new DevExpress.Utils.PointFloat(2.999767F, 8.820358F);
            this.xrLabel脾大.Name = "xrLabel脾大";
            this.xrLabel脾大.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel脾大.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel脾大.StylePriority.UseBorders = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell74,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.Weight = 0.61112114789022987D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.StylePriority.UseTextAlignment = false;
            this.xrTableCell75.Text = "移动性浊音：1无 2有";
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell75.Weight = 1.0115709949485112D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell76.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel移动性浊音有});
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseBorders = false;
            this.xrTableCell76.Weight = 1.9218097805773582D;
            // 
            // xrLabel移动性浊音有
            // 
            this.xrLabel移动性浊音有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel移动性浊音有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel移动性浊音有.Name = "xrLabel移动性浊音有";
            this.xrLabel移动性浊音有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel移动性浊音有.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel移动性浊音有.StylePriority.UseBorders = false;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel移动性浊音});
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.Weight = 0.10133151164249465D;
            // 
            // xrLabel移动性浊音
            // 
            this.xrLabel移动性浊音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel移动性浊音.CanGrow = false;
            this.xrLabel移动性浊音.LocationFloat = new DevExpress.Utils.PointFloat(3.000011F, 7.872208F);
            this.xrLabel移动性浊音.Name = "xrLabel移动性浊音";
            this.xrLabel移动性浊音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel移动性浊音.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel移动性浊音.StylePriority.UseBorders = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell78,
            this.xrTableCell79,
            this.xrTableCell81});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Text = "下肢水肿";
            this.xrTableCell78.Weight = 0.61112114789022987D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseBorders = false;
            this.xrTableCell79.StylePriority.UseTextAlignment = false;
            this.xrTableCell79.Text = "1无 2单侧 3双侧不对称 4双侧对称";
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell79.Weight = 2.933381501262208D;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell81.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel下肢水肿});
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.Weight = 0.10133078590615585D;
            // 
            // xrLabel下肢水肿
            // 
            this.xrLabel下肢水肿.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel下肢水肿.CanGrow = false;
            this.xrLabel下肢水肿.LocationFloat = new DevExpress.Utils.PointFloat(2.999828F, 6.924057F);
            this.xrLabel下肢水肿.Name = "xrLabel下肢水肿";
            this.xrLabel下肢水肿.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel下肢水肿.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel下肢水肿.StylePriority.UseBorders = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTableCell85});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Text = "足背动肪搏动";
            this.xrTableCell82.Weight = 0.61112114789022987D;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.StylePriority.UseTextAlignment = false;
            this.xrTableCell83.Text = "1未触及 2触及双侧对称 3触及左侧弱或消失 4触及右侧弱或消失";
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell83.Weight = 2.9333813482370812D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell85.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel足背动脉搏动});
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.Weight = 0.10133093893128255D;
            // 
            // xrLabel足背动脉搏动
            // 
            this.xrLabel足背动脉搏动.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel足背动脉搏动.CanGrow = false;
            this.xrLabel足背动脉搏动.LocationFloat = new DevExpress.Utils.PointFloat(2.999889F, 5.788437F);
            this.xrLabel足背动脉搏动.Name = "xrLabel足背动脉搏动";
            this.xrLabel足背动脉搏动.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel足背动脉搏动.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel足背动脉搏动.StylePriority.UseBorders = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell89});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Text = "肛门指诊*";
            this.xrTableCell86.Weight = 0.61112114789022987D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBorders = false;
            this.xrTableCell87.StylePriority.UseTextAlignment = false;
            this.xrTableCell87.Text = "1未见异常 2触痛 3包块 4前列腺异常 5其他";
            this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell87.Weight = 1.9947767910874426D;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell88.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel肛门指诊其他});
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.Weight = 0.93860383141329973D;
            // 
            // xrLabel肛门指诊其他
            // 
            this.xrLabel肛门指诊其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel肛门指诊其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel肛门指诊其他.Name = "xrLabel肛门指诊其他";
            this.xrLabel肛门指诊其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肛门指诊其他.SizeF = new System.Drawing.SizeF(141.732F, 22F);
            this.xrLabel肛门指诊其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel肛门指诊});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.Weight = 0.10133166466762145D;
            // 
            // xrLabel肛门指诊
            // 
            this.xrLabel肛门指诊.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel肛门指诊.CanGrow = false;
            this.xrLabel肛门指诊.LocationFloat = new DevExpress.Utils.PointFloat(3.000011F, 5.027756F);
            this.xrLabel肛门指诊.Name = "xrLabel肛门指诊";
            this.xrLabel肛门指诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel肛门指诊.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel肛门指诊.StylePriority.UseBorders = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell93});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Text = "乳    腺*";
            this.xrTableCell90.Weight = 0.61112114789022987D;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.StylePriority.UseTextAlignment = false;
            this.xrTableCell91.Text = "1未见异常 2乳房切除 3异常泌乳 4乳腺包块 5其他";
            this.xrTableCell91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell91.Weight = 2.2760342193418168D;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell92.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel乳腺其他});
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Weight = 0.65734701525943229D;
            // 
            // xrLabel乳腺其他
            // 
            this.xrLabel乳腺其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel乳腺其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel乳腺其他.Name = "xrLabel乳腺其他";
            this.xrLabel乳腺其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺其他.SizeF = new System.Drawing.SizeF(121.0939F, 22F);
            this.xrLabel乳腺其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell93.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel乳腺});
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 0.10133105256711494D;
            // 
            // xrLabel乳腺
            // 
            this.xrLabel乳腺.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel乳腺.CanGrow = false;
            this.xrLabel乳腺.LocationFloat = new DevExpress.Utils.PointFloat(2.999889F, 3.948085F);
            this.xrLabel乳腺.Name = "xrLabel乳腺";
            this.xrLabel乳腺.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel乳腺.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel乳腺.StylePriority.UseBorders = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell84,
            this.xrTableCell94,
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.Weight = 0.30556057394511493D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Text = "外阴";
            this.xrTableCell94.Weight = 0.30556057394511493D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.StylePriority.UseTextAlignment = false;
            this.xrTableCell95.Text = "1未见异常 2异常";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell95.Weight = 0.75867815113718551D;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell96.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel外阴异常});
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.StylePriority.UseBorders = false;
            this.xrTableCell96.Weight = 2.17470369556457D;
            // 
            // xrLabel外阴异常
            // 
            this.xrLabel外阴异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel外阴异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel外阴异常.Name = "xrLabel外阴异常";
            this.xrLabel外阴异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel外阴异常.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel外阴异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell97.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel外阴});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.Weight = 0.10133044046660844D;
            // 
            // xrLabel外阴
            // 
            this.xrLabel外阴.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel外阴.CanGrow = false;
            this.xrLabel外阴.LocationFloat = new DevExpress.Utils.PointFloat(2.999767F, 3.131454F);
            this.xrLabel外阴.Name = "xrLabel外阴";
            this.xrLabel外阴.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel外阴.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel外阴.StylePriority.UseBorders = false;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136,
            this.xrTableCell98,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell101});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.StylePriority.UseBorders = false;
            this.xrTableCell136.Weight = 0.30556057394511493D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Text = "阴道";
            this.xrTableCell98.Weight = 0.30556057394511493D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.StylePriority.UseTextAlignment = false;
            this.xrTableCell99.Text = "1未见异常 2异常";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell99.Weight = 0.75867815113718551D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell100.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel阴道异常});
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.Weight = 2.174702471363557D;
            // 
            // xrLabel阴道异常
            // 
            this.xrLabel阴道异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel阴道异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel阴道异常.Name = "xrLabel阴道异常";
            this.xrLabel阴道异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阴道异常.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel阴道异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell101.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel阴道});
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.Weight = 0.10133166466762145D;
            // 
            // xrLabel阴道
            // 
            this.xrLabel阴道.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel阴道.CanGrow = false;
            this.xrLabel阴道.LocationFloat = new DevExpress.Utils.PointFloat(3.000011F, 2.183304F);
            this.xrLabel阴道.Name = "xrLabel阴道";
            this.xrLabel阴道.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel阴道.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel阴道.StylePriority.UseBorders = false;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell102,
            this.xrTableCell103,
            this.xrTableCell104,
            this.xrTableCell105});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.StylePriority.UseBorders = false;
            this.xrTableCell137.Text = "妇科*";
            this.xrTableCell137.Weight = 0.30556057394511493D;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Text = "宫颈";
            this.xrTableCell102.Weight = 0.30556057394511493D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.StylePriority.UseTextAlignment = false;
            this.xrTableCell103.Text = "1未见异常 2异常";
            this.xrTableCell103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell103.Weight = 0.75867815113718551D;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell104.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel宫颈异常});
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseBorders = false;
            this.xrTableCell104.Weight = 2.17470369556457D;
            // 
            // xrLabel宫颈异常
            // 
            this.xrLabel宫颈异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel宫颈异常.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 3.178914E-05F);
            this.xrLabel宫颈异常.Name = "xrLabel宫颈异常";
            this.xrLabel宫颈异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫颈异常.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel宫颈异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell105.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel宫颈});
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.Weight = 0.10133044046660844D;
            // 
            // xrLabel宫颈
            // 
            this.xrLabel宫颈.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel宫颈.CanGrow = false;
            this.xrLabel宫颈.LocationFloat = new DevExpress.Utils.PointFloat(2.999767F, 1.235153F);
            this.xrLabel宫颈.Name = "xrLabel宫颈";
            this.xrLabel宫颈.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫颈.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel宫颈.StylePriority.UseBorders = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell138,
            this.xrTableCell111,
            this.xrTableCell112,
            this.xrTableCell114,
            this.xrTableCell115});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseBorders = false;
            this.xrTableCell138.Weight = 0.30556057394511493D;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Text = "宫体";
            this.xrTableCell111.Weight = 0.30556057394511493D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.StylePriority.UseTextAlignment = false;
            this.xrTableCell112.Text = "1未见异常 2异常";
            this.xrTableCell112.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell112.Weight = 0.75867799811205894D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell114.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel宫体异常});
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Weight = 2.1747023346340395D;
            // 
            // xrLabel宫体异常
            // 
            this.xrLabel宫体异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel宫体异常.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 0F);
            this.xrLabel宫体异常.Name = "xrLabel宫体异常";
            this.xrLabel宫体异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫体异常.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel宫体异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel宫体});
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Weight = 0.101331954422265D;
            // 
            // xrLabel宫体
            // 
            this.xrLabel宫体.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel宫体.CanGrow = false;
            this.xrLabel宫体.LocationFloat = new DevExpress.Utils.PointFloat(3.000072F, 3.948116F);
            this.xrLabel宫体.Name = "xrLabel宫体";
            this.xrLabel宫体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel宫体.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel宫体.StylePriority.UseBorders = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell139,
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell110,
            this.xrTableCell109});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.StylePriority.UseBorders = false;
            this.xrTableCell139.Weight = 0.30556057394511493D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Text = "附件";
            this.xrTableCell106.Weight = 0.30556057394511493D;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.StylePriority.UseTextAlignment = false;
            this.xrTableCell107.Text = "1未见异常 2异常";
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell107.Weight = 0.75867784508693237D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell110.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel附件异常});
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.Weight = 2.1747021816089136D;
            // 
            // xrLabel附件异常
            // 
            this.xrLabel附件异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel附件异常.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 0F);
            this.xrLabel附件异常.Name = "xrLabel附件异常";
            this.xrLabel附件异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel附件异常.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel附件异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell109.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel附件});
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.Weight = 0.1013322604725182D;
            // 
            // xrLabel附件
            // 
            this.xrLabel附件.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel附件.CanGrow = false;
            this.xrLabel附件.LocationFloat = new DevExpress.Utils.PointFloat(3.000134F, 3.948085F);
            this.xrLabel附件.Name = "xrLabel附件";
            this.xrLabel附件.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel附件.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel附件.StylePriority.UseBorders = false;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell116,
            this.xrTableCell120});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1.9259277696118016D;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell116.CanGrow = false;
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Text = "其    他*";
            this.xrTableCell116.Weight = 0.61112114789022987D;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell120.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel查体其他});
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.Weight = 3.0347122871683636D;
            // 
            // xrLabel查体其他
            // 
            this.xrLabel查体其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel查体其他.LocationFloat = new DevExpress.Utils.PointFloat(17.91672F, 4.577637E-05F);
            this.xrLabel查体其他.Name = "xrLabel查体其他";
            this.xrLabel查体其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel查体其他.SizeF = new System.Drawing.SizeF(567.0834F, 43.3728F);
            this.xrLabel查体其他.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.xrTableCell9});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 8.1666650390625D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Weight = 0.15102043805803572D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 2.8489795619419644D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 6.103516E-05F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36});
            this.xrTable4.SizeF = new System.Drawing.SizeF(727.0833F, 204.1666F);
            this.xrTable4.StylePriority.UseBorders = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell122});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 2D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Text = "血常规*";
            this.xrTableCell7.Weight = 0.50286538579707485D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5});
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "xrTableCell122";
            this.xrTableCell122.Weight = 2.4971346142029249D;
            // 
            // xrTable5
            // 
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 6.103516E-05F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37,
            this.xrTableRow38});
            this.xrTable5.SizeF = new System.Drawing.SizeF(605.2083F, 68.05548F);
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell118,
            this.xrTableCell124,
            this.xrTableCell117,
            this.xrTableCell80,
            this.xrTableCell146,
            this.xrTableCell127});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "血红蛋白";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell8.Weight = 0.40619613043231406D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell118.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血红蛋白});
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.Weight = 0.26815893237544824D;
            // 
            // xrLabel血红蛋白
            // 
            this.xrLabel血红蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel血红蛋白.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 2.027702F);
            this.xrLabel血红蛋白.Name = "xrLabel血红蛋白";
            this.xrLabel血红蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血红蛋白.SizeF = new System.Drawing.SizeF(54.09724F, 22F);
            this.xrLabel血红蛋白.StylePriority.UseBorders = false;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Text = "g/L 白细胞";
            this.xrTableCell124.Weight = 0.58297446341082382D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell117.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel白细胞});
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Weight = 0.29688747543928429D;
            // 
            // xrLabel白细胞
            // 
            this.xrLabel白细胞.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel白细胞.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.027734F);
            this.xrLabel白细胞.Name = "xrLabel白细胞";
            this.xrLabel白细胞.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel白细胞.SizeF = new System.Drawing.SizeF(59.89288F, 22F);
            this.xrLabel白细胞.StylePriority.UseBorders = false;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.Text = "x 10^9/L 血小板";
            this.xrTableCell80.Weight = 0.75800338588181015D;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell146.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血小板});
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBorders = false;
            this.xrTableCell146.Weight = 0.28791714452678391D;
            // 
            // xrLabel血小板
            // 
            this.xrLabel血小板.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel血小板.LocationFloat = new DevExpress.Utils.PointFloat(4.196167E-05F, 2.02771F);
            this.xrLabel血小板.Name = "xrLabel血小板";
            this.xrLabel血小板.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血小板.SizeF = new System.Drawing.SizeF(58.08319F, 22F);
            this.xrLabel血小板.StylePriority.UseBorders = false;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.StylePriority.UseTextAlignment = false;
            this.xrTableCell127.Text = "x 10^9/L";
            this.xrTableCell127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell127.Weight = 0.39986246793353558D;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell130,
            this.xrTableCell133});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.StylePriority.UseBorders = false;
            this.xrTableCell130.StylePriority.UseTextAlignment = false;
            this.xrTableCell130.Text = "其他";
            this.xrTableCell130.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell130.Weight = 0.253012144941781D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell133.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel血常规其他});
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.Weight = 2.7469878550582187D;
            // 
            // xrLabel血常规其他
            // 
            this.xrLabel血常规其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel血常规其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.02771F);
            this.xrLabel血常规其他.Name = "xrLabel血常规其他";
            this.xrLabel血常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel血常规其他.SizeF = new System.Drawing.SizeF(473.4998F, 21.99994F);
            this.xrLabel血常规其他.StylePriority.UseBorders = false;
            this.xrLabel血常规其他.StylePriority.UseTextAlignment = false;
            this.xrLabel血常规其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell123,
            this.xrTableCell125});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 2D;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Text = "尿常规*";
            this.xrTableCell123.Weight = 0.50286538579707485D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable6});
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 2.4971346142029249D;
            // 
            // xrTable6
            // 
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39,
            this.xrTableRow40});
            this.xrTable6.SizeF = new System.Drawing.SizeF(605.2083F, 68.05542F);
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell154,
            this.xrTableCell119,
            this.xrTableCell153,
            this.xrTableCell147,
            this.xrTableCell156,
            this.xrTableCell152,
            this.xrTableCell155,
            this.xrTableCell148});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseBorders = false;
            this.xrTableCell154.StylePriority.UseTextAlignment = false;
            this.xrTableCell154.Text = "尿蛋白";
            this.xrTableCell154.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell154.Weight = 0.40619615564477179D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell119.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel尿蛋白});
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.Weight = 0.34380402084243289D;
            // 
            // xrLabel尿蛋白
            // 
            this.xrLabel尿蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel尿蛋白.LocationFloat = new DevExpress.Utils.PointFloat(7.638897F, 5.361023F);
            this.xrLabel尿蛋白.Name = "xrLabel尿蛋白";
            this.xrLabel尿蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿蛋白.SizeF = new System.Drawing.SizeF(54.09724F, 22F);
            this.xrLabel尿蛋白.StylePriority.UseBorders = false;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.Text = "尿糖";
            this.xrTableCell153.Weight = 0.3751432319728657D;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell147.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel尿糖});
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBorders = false;
            this.xrTableCell147.Weight = 0.31547636846413679D;
            // 
            // xrLabel尿糖
            // 
            this.xrLabel尿糖.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel尿糖.LocationFloat = new DevExpress.Utils.PointFloat(0.101284F, 5.361023F);
            this.xrLabel尿糖.Name = "xrLabel尿糖";
            this.xrLabel尿糖.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿糖.SizeF = new System.Drawing.SizeF(54.09724F, 22F);
            this.xrLabel尿糖.StylePriority.UseBorders = false;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.Text = "尿酮体";
            this.xrTableCell156.Weight = 0.45905094081294845D;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell152.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel尿酮体});
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseBorders = false;
            this.xrTableCell152.Weight = 0.35032962893413933D;
            // 
            // xrLabel尿酮体
            // 
            this.xrLabel尿酮体.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel尿酮体.LocationFloat = new DevExpress.Utils.PointFloat(6.351177F, 5.361023F);
            this.xrLabel尿酮体.Name = "xrLabel尿酮体";
            this.xrLabel尿酮体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿酮体.SizeF = new System.Drawing.SizeF(54.09724F, 22F);
            this.xrLabel尿酮体.StylePriority.UseBorders = false;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseBorders = false;
            this.xrTableCell155.Text = "尿潜血";
            this.xrTableCell155.Weight = 0.35013751000556387D;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell148.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel尿潜血});
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.StylePriority.UseBorders = false;
            this.xrTableCell148.Weight = 0.39986214332314107D;
            // 
            // xrLabel尿潜血
            // 
            this.xrLabel尿潜血.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel尿潜血.LocationFloat = new DevExpress.Utils.PointFloat(15.0417F, 5.361023F);
            this.xrLabel尿潜血.Name = "xrLabel尿潜血";
            this.xrLabel尿潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿潜血.SizeF = new System.Drawing.SizeF(54.09724F, 22F);
            this.xrLabel尿潜血.StylePriority.UseBorders = false;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149,
            this.xrTableCell151});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseBorders = false;
            this.xrTableCell149.StylePriority.UseTextAlignment = false;
            this.xrTableCell149.Text = "其他";
            this.xrTableCell149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell149.Weight = 0.25301218276046761D;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell151.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel尿常规其他});
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseBorders = false;
            this.xrTableCell151.Weight = 2.7469878172395323D;
            // 
            // xrLabel尿常规其他
            // 
            this.xrLabel尿常规其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel尿常规其他.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 2.027861F);
            this.xrLabel尿常规其他.Name = "xrLabel尿常规其他";
            this.xrLabel尿常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel尿常规其他.SizeF = new System.Drawing.SizeF(502.0414F, 21.99994F);
            this.xrLabel尿常规其他.StylePriority.UseBorders = false;
            this.xrLabel尿常规其他.StylePriority.UseTextAlignment = false;
            this.xrLabel尿常规其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.xrTableCell158,
            this.xrTableCell150,
            this.xrTableCell157,
            this.xrTableCell128});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Text = "空腹血糖*";
            this.xrTableCell126.Weight = 0.50286538579707485D;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell158.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel空腹血糖1});
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.Weight = 0.62428365355073123D;
            // 
            // xrLabel空腹血糖1
            // 
            this.xrLabel空腹血糖1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel空腹血糖1.LocationFloat = new DevExpress.Utils.PointFloat(5.260499F, 2.027766F);
            this.xrLabel空腹血糖1.Name = "xrLabel空腹血糖1";
            this.xrLabel空腹血糖1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel空腹血糖1.SizeF = new System.Drawing.SizeF(136.0416F, 21.99994F);
            this.xrLabel空腹血糖1.StylePriority.UseBorders = false;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.StylePriority.UseBorders = false;
            this.xrTableCell150.Text = "mmol/L  或";
            this.xrTableCell150.Weight = 0.518565070801069D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell157.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel空腹血糖2});
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.StylePriority.UseBorders = false;
            this.xrTableCell157.Weight = 1.0214490924347568D;
            // 
            // xrLabel空腹血糖2
            // 
            this.xrLabel空腹血糖2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel空腹血糖2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.027766F);
            this.xrLabel空腹血糖2.Name = "xrLabel空腹血糖2";
            this.xrLabel空腹血糖2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel空腹血糖2.SizeF = new System.Drawing.SizeF(247.1904F, 21.99997F);
            this.xrLabel空腹血糖2.StylePriority.UseBorders = false;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseBorders = false;
            this.xrTableCell128.Text = "mg/dL";
            this.xrTableCell128.Weight = 0.33283679741636785D;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell131});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Text = "心电图*";
            this.xrTableCell129.Weight = 0.50286538579707485D;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.StylePriority.UseBorders = false;
            this.xrTableCell159.StylePriority.UseTextAlignment = false;
            this.xrTableCell159.Text = "1正常 2异常";
            this.xrTableCell159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell159.Weight = 0.48424081882844039D;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell160.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel心电图异常});
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.Weight = 1.9295118997371843D;
            // 
            // xrLabel心电图异常
            // 
            this.xrLabel心电图异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel心电图异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.277771F);
            this.xrLabel心电图异常.Name = "xrLabel心电图异常";
            this.xrLabel心电图异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心电图异常.SizeF = new System.Drawing.SizeF(424.1987F, 22F);
            this.xrLabel心电图异常.StylePriority.UseBorders = false;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell131.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel心电图});
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.Weight = 0.083381895637300252D;
            // 
            // xrLabel心电图
            // 
            this.xrLabel心电图.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel心电图.CanGrow = false;
            this.xrLabel心电图.LocationFloat = new DevExpress.Utils.PointFloat(3.000132F, 8.054256F);
            this.xrLabel心电图.Name = "xrLabel心电图";
            this.xrLabel心电图.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel心电图.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel心电图.StylePriority.UseBorders = false;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 18F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 19F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report健康体检表2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(44, 36, 18, 19);
            this.PageHeight = 1169;
            this.PageWidth = 1654;
            this.PaperKind = System.Drawing.Printing.PaperKind.A3;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel足背动脉搏动;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRLabel xrLabel口唇;
        private DevExpress.XtraReports.UI.XRLabel xrLabel齿列有无;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRLabel xrLabel咽部;
        private DevExpress.XtraReports.UI.XRLabel xrLabel听力;
        private DevExpress.XtraReports.UI.XRLabel xrLabel运动功能;
        private DevExpress.XtraReports.UI.XRLabel xrLabel眼底;
        private DevExpress.XtraReports.UI.XRLabel xrLabel皮肤;
        private DevExpress.XtraReports.UI.XRLabel xrLabel巩膜;
        private DevExpress.XtraReports.UI.XRLabel xrLabel淋巴结;
        private DevExpress.XtraReports.UI.XRLabel xrLabel桶状胸;
        private DevExpress.XtraReports.UI.XRLabel xrLabel呼吸音;
        private DevExpress.XtraReports.UI.XRLabel xrLabel罗音;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心律;
        private DevExpress.XtraReports.UI.XRLabel xrLabel杂音;
        private DevExpress.XtraReports.UI.XRLabel xrLabel包块;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肝大;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脾大;
        private DevExpress.XtraReports.UI.XRLabel xrLabel移动性浊音;
        private DevExpress.XtraReports.UI.XRLabel xrLabel下肢水肿;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肛门指诊;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺;
        private DevExpress.XtraReports.UI.XRLabel xrLabel外阴;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阴道;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫颈;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫体;
        private DevExpress.XtraReports.UI.XRLabel xrLabel附件;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心电图;
        private DevExpress.XtraReports.UI.XRLabel xrLabel左眼;
        private DevExpress.XtraReports.UI.XRLabel xrLabel右眼;
        private DevExpress.XtraReports.UI.XRLabel xrLabel左眼矫正;
        private DevExpress.XtraReports.UI.XRLabel xrLabel右眼矫正;
        private DevExpress.XtraReports.UI.XRLabel xrLabel眼底异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel皮肤其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel巩膜其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel淋巴结其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel呼吸音异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel罗音其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心率;
        private DevExpress.XtraReports.UI.XRLabel xrLabel杂音有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel包块有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肝大有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脾大有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel移动性浊音有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肛门指诊其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乳腺其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel外阴异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阴道异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫颈异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫体异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel附件异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel查体其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血红蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel白细胞;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血小板;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血常规其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿糖;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿酮体;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿潜血;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿常规其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel空腹血糖1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel空腹血糖2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心电图异常;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell273;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRLabel xrLabel大便潜血;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRLabel xrLabel乙肝;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell285;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel总胆固醇;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel甘油三酯;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel血清低密度;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrLabel血清高密度;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRLabel xrLabel胸部X线片;
        private DevExpress.XtraReports.UI.XRLabel xrLabelB超;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫颈涂片;
        private DevExpress.XtraReports.UI.XRLabel xrLabel平和质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel气虚质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阳虚质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel阴虚质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel痰湿质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel湿热质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血瘀质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel气郁质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel特禀质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脑血管疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脑血管疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脑血管疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脑血管疾病4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脑血管疾病5;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell313;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell315;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肾脏疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肾脏疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肾脏疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肾脏疾病4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肾脏疾病5;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心脏疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心脏疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心脏疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心脏疾病4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心脏疾病5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血管疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血管疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血管疾病3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRLabel xrLabel眼部疾病1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel眼部疾病2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel眼部疾病3;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell344;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell346;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell341;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRLabel xrLabel神经系统疾病;
        private DevExpress.XtraReports.UI.XRLabel xrLabel其他系统疾病;
        private DevExpress.XtraReports.UI.XRLabel xrLabel尿微量白蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel糖化血红蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血清谷丙转氨酶;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血清谷草转氨酶;
        private DevExpress.XtraReports.UI.XRLabel xrLabel白蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel总红胆素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel结合胆红素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血清肌酐;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血尿素氮;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血钾浓度;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血钠浓度;
        private DevExpress.XtraReports.UI.XRLabel xrLabel胸部X线片异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabelB超异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel宫颈涂片异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel辅助检查其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel脑血管疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel肾脏疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel心脏疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel眼部疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel神经系统疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel其他系统疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel血管疾病其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell347;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell348;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell349;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell350;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell缺齿1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell缺齿2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell缺齿3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell缺齿4;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell龋齿1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell龋齿2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell龋齿3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell龋齿4;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell义齿1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell义齿2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell义齿3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell义齿4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRLabel xrLabel压痛有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRLabel xrLabel压痛;
    }
}
