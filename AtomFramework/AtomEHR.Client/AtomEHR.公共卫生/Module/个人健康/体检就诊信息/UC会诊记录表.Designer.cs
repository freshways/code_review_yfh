﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class UC会诊记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.lbl当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl30 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.txt接诊医生 = new DevExpress.XtraEditors.TextEdit();
            this.lbl个人档案号 = new DevExpress.XtraEditors.LabelControl();
            this.lbl性别 = new DevExpress.XtraEditors.LabelControl();
            this.lbl出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.lbl姓名 = new DevExpress.XtraEditors.LabelControl();
            this.lbl身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.lbl联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.lbl居住地址 = new DevExpress.XtraEditors.LabelControl();
            this.dtp接诊时间 = new DevExpress.XtraEditors.DateEdit();
            this.txt就诊者的主观资料 = new DevExpress.XtraEditors.MemoEdit();
            this.txt就诊者的客观资料 = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.xtraScrollableControl1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp接诊时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp接诊时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt就诊者的主观资料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt就诊者的客观资料.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(600, 34);
            this.panelControl1.TabIndex = 0;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(109, 5);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "填表说明";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(5, 5);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "保存";
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Appearance.BackColor = System.Drawing.Color.White;
            this.xtraScrollableControl1.Appearance.Options.UseBackColor = true;
            this.xtraScrollableControl1.Controls.Add(this.tableLayoutPanel3);
            this.xtraScrollableControl1.Controls.Add(this.tableLayoutPanel2);
            this.xtraScrollableControl1.Controls.Add(this.tableLayoutPanel1);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 34);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(600, 425);
            this.xtraScrollableControl1.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.19366F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.55593F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.19199F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.22538F));
            this.tableLayoutPanel3.Controls.Add(this.labelControl22, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelControl23, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.labelControl24, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.lbl当前所属机构, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.lbl创建时间, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lbl创建人, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.labelControl28, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.labelControl29, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.labelControl30, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.lbl最近更新时间, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.lbl创建机构, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.lbl最近修改人, 3, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 343);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.46753F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.46753F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(600, 78);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // labelControl22
            // 
            this.labelControl22.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl22.Location = new System.Drawing.Point(1, 1);
            this.labelControl22.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(96, 24);
            this.labelControl22.TabIndex = 0;
            this.labelControl22.Text = "创建时间：";
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl23.Location = new System.Drawing.Point(1, 26);
            this.labelControl23.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(96, 24);
            this.labelControl23.TabIndex = 1;
            this.labelControl23.Text = "当前所属机构：";
            // 
            // labelControl24
            // 
            this.labelControl24.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl24.Location = new System.Drawing.Point(1, 51);
            this.labelControl24.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(96, 26);
            this.labelControl24.TabIndex = 2;
            this.labelControl24.Text = "创建人：";
            // 
            // lbl当前所属机构
            // 
            this.lbl当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl当前所属机构.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl当前所属机构.Location = new System.Drawing.Point(101, 29);
            this.lbl当前所属机构.Name = "lbl当前所属机构";
            this.lbl当前所属机构.Size = new System.Drawing.Size(193, 18);
            this.lbl当前所属机构.TabIndex = 3;
            this.lbl当前所属机构.Text = "labelControl25";
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl创建时间.Location = new System.Drawing.Point(101, 4);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(193, 18);
            this.lbl创建时间.TabIndex = 4;
            this.lbl创建时间.Text = "labelControl26";
            // 
            // lbl创建人
            // 
            this.lbl创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建人.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl创建人.Location = new System.Drawing.Point(101, 54);
            this.lbl创建人.Name = "lbl创建人";
            this.lbl创建人.Size = new System.Drawing.Size(193, 20);
            this.lbl创建人.TabIndex = 5;
            this.lbl创建人.Text = "labelControl27";
            // 
            // labelControl28
            // 
            this.labelControl28.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl28.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl28.Location = new System.Drawing.Point(298, 26);
            this.labelControl28.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(90, 24);
            this.labelControl28.TabIndex = 6;
            this.labelControl28.Text = "创建机构：";
            // 
            // labelControl29
            // 
            this.labelControl29.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl29.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl29.Location = new System.Drawing.Point(298, 1);
            this.labelControl29.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(90, 24);
            this.labelControl29.TabIndex = 7;
            this.labelControl29.Text = "最近更新时间：";
            // 
            // labelControl30
            // 
            this.labelControl30.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl30.Location = new System.Drawing.Point(298, 51);
            this.labelControl30.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl30.Name = "labelControl30";
            this.labelControl30.Size = new System.Drawing.Size(90, 26);
            this.labelControl30.TabIndex = 8;
            this.labelControl30.Text = "最近修改人：";
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近更新时间.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl最近更新时间.Location = new System.Drawing.Point(392, 4);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(204, 18);
            this.lbl最近更新时间.TabIndex = 9;
            this.lbl最近更新时间.Text = "labelControl31";
            // 
            // lbl创建机构
            // 
            this.lbl创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建机构.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl创建机构.Location = new System.Drawing.Point(392, 29);
            this.lbl创建机构.Name = "lbl创建机构";
            this.lbl创建机构.Size = new System.Drawing.Size(204, 18);
            this.lbl创建机构.TabIndex = 10;
            this.lbl创建机构.Text = "labelControl32";
            // 
            // lbl最近修改人
            // 
            this.lbl最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近修改人.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl最近修改人.Location = new System.Drawing.Point(392, 54);
            this.lbl最近修改人.Name = "lbl最近修改人";
            this.lbl最近修改人.Size = new System.Drawing.Size(204, 20);
            this.lbl最近修改人.TabIndex = 11;
            this.lbl最近修改人.Text = "labelControl33";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.labelControl2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelControl10, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelControl11, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.textEdit1, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.textEdit2, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.textEdit3, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.textEdit4, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.simpleButton3, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.simpleButton4, 4, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 265);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(600, 78);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel2.SetColumnSpan(this.labelControl2, 5);
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(1, 1);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(598, 24);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "会诊医生及其所在医疗机构";
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl10.Location = new System.Drawing.Point(1, 26);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(118, 24);
            this.labelControl10.TabIndex = 1;
            this.labelControl10.Text = "医疗机构名称";
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel2.SetColumnSpan(this.labelControl11, 3);
            this.labelControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl11.Location = new System.Drawing.Point(120, 26);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(356, 24);
            this.labelControl11.TabIndex = 2;
            this.labelControl11.Text = "会诊医生签字";
            // 
            // textEdit1
            // 
            this.textEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEdit1.Location = new System.Drawing.Point(4, 54);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(112, 20);
            this.textEdit1.TabIndex = 3;
            // 
            // textEdit2
            // 
            this.textEdit2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEdit2.Location = new System.Drawing.Point(123, 54);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(112, 20);
            this.textEdit2.TabIndex = 4;
            // 
            // textEdit3
            // 
            this.textEdit3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEdit3.Location = new System.Drawing.Point(242, 54);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(112, 20);
            this.textEdit3.TabIndex = 5;
            // 
            // textEdit4
            // 
            this.textEdit4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textEdit4.Location = new System.Drawing.Point(361, 54);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(112, 20);
            this.textEdit4.TabIndex = 6;
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(477, 26);
            this.simpleButton3.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(56, 23);
            this.simpleButton3.TabIndex = 7;
            this.simpleButton3.Text = "添加";
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(477, 51);
            this.simpleButton4.Margin = new System.Windows.Forms.Padding(0);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(56, 23);
            this.simpleButton4.TabIndex = 8;
            this.simpleButton4.Text = "查看";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.32302F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.88815F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.02838F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.05676F));
            this.tableLayoutPanel1.Controls.Add(this.labelControl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelControl4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelControl5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelControl6, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelControl7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelControl8, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.labelControl9, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.labelControl15, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelControl16, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelControl17, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.labelControl18, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.txt接诊医生, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.lbl个人档案号, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl性别, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl出生日期, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl姓名, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl身份证号, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl联系电话, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl居住地址, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.dtp接诊时间, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txt就诊者的主观资料, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txt就诊者的客观资料, 1, 7);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.469697F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.954545F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.954545F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.04545F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.81818F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(600, 265);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel1.SetColumnSpan(this.labelControl1, 4);
            this.labelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl1.Location = new System.Drawing.Point(1, 1);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(598, 24);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "会诊记录表";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl3.Location = new System.Drawing.Point(1, 26);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(96, 21);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "个人档案号";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl4.Location = new System.Drawing.Point(1, 48);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(96, 20);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "性别";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl5.Location = new System.Drawing.Point(1, 69);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(96, 20);
            this.labelControl5.TabIndex = 4;
            this.labelControl5.Text = "出生日期";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl6.Location = new System.Drawing.Point(1, 90);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(96, 21);
            this.labelControl6.TabIndex = 5;
            this.labelControl6.Text = "居住地址";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl7.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl7.Location = new System.Drawing.Point(1, 112);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(96, 23);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "会诊时间";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl8.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl8.Location = new System.Drawing.Point(1, 136);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(96, 43);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "会诊原因";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl9.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl9.Location = new System.Drawing.Point(1, 180);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(96, 84);
            this.labelControl9.TabIndex = 8;
            this.labelControl9.Text = "会诊意见";
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl15.Location = new System.Drawing.Point(294, 26);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(101, 21);
            this.labelControl15.TabIndex = 14;
            this.labelControl15.Text = "姓名";
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl16.Location = new System.Drawing.Point(294, 48);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(101, 20);
            this.labelControl16.TabIndex = 15;
            this.labelControl16.Text = "身份证号";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl17.Location = new System.Drawing.Point(294, 69);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(101, 20);
            this.labelControl17.TabIndex = 16;
            this.labelControl17.Text = "联系电话";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.labelControl18.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl18.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl18.Location = new System.Drawing.Point(294, 112);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(101, 23);
            this.labelControl18.TabIndex = 17;
            this.labelControl18.Text = "会诊医生";
            // 
            // txt接诊医生
            // 
            this.txt接诊医生.Location = new System.Drawing.Point(396, 112);
            this.txt接诊医生.Margin = new System.Windows.Forms.Padding(0);
            this.txt接诊医生.Name = "txt接诊医生";
            this.txt接诊医生.Size = new System.Drawing.Size(157, 20);
            this.txt接诊医生.TabIndex = 29;
            // 
            // lbl个人档案号
            // 
            this.lbl个人档案号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl个人档案号.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl个人档案号.Location = new System.Drawing.Point(101, 29);
            this.lbl个人档案号.Name = "lbl个人档案号";
            this.lbl个人档案号.Size = new System.Drawing.Size(189, 15);
            this.lbl个人档案号.TabIndex = 40;
            this.lbl个人档案号.Text = "labelControl22";
            // 
            // lbl性别
            // 
            this.lbl性别.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl性别.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl性别.Location = new System.Drawing.Point(101, 51);
            this.lbl性别.Name = "lbl性别";
            this.lbl性别.Size = new System.Drawing.Size(189, 14);
            this.lbl性别.TabIndex = 41;
            this.lbl性别.Text = "labelControl23";
            // 
            // lbl出生日期
            // 
            this.lbl出生日期.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl出生日期.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl出生日期.Location = new System.Drawing.Point(101, 72);
            this.lbl出生日期.Name = "lbl出生日期";
            this.lbl出生日期.Size = new System.Drawing.Size(189, 14);
            this.lbl出生日期.TabIndex = 42;
            this.lbl出生日期.Text = "labelControl24";
            // 
            // lbl姓名
            // 
            this.lbl姓名.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl姓名.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl姓名.Location = new System.Drawing.Point(399, 29);
            this.lbl姓名.Name = "lbl姓名";
            this.lbl姓名.Size = new System.Drawing.Size(197, 15);
            this.lbl姓名.TabIndex = 43;
            this.lbl姓名.Text = "labelControl25";
            // 
            // lbl身份证号
            // 
            this.lbl身份证号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl身份证号.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl身份证号.Location = new System.Drawing.Point(399, 51);
            this.lbl身份证号.Name = "lbl身份证号";
            this.lbl身份证号.Size = new System.Drawing.Size(197, 14);
            this.lbl身份证号.TabIndex = 44;
            this.lbl身份证号.Text = "labelControl26";
            // 
            // lbl联系电话
            // 
            this.lbl联系电话.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl联系电话.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl联系电话.Location = new System.Drawing.Point(399, 72);
            this.lbl联系电话.Name = "lbl联系电话";
            this.lbl联系电话.Size = new System.Drawing.Size(197, 14);
            this.lbl联系电话.TabIndex = 45;
            this.lbl联系电话.Text = "labelControl27";
            // 
            // lbl居住地址
            // 
            this.lbl居住地址.Appearance.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbl居住地址.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.tableLayoutPanel1.SetColumnSpan(this.lbl居住地址, 3);
            this.lbl居住地址.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl居住地址.Location = new System.Drawing.Point(98, 90);
            this.lbl居住地址.Margin = new System.Windows.Forms.Padding(0);
            this.lbl居住地址.Name = "lbl居住地址";
            this.lbl居住地址.Size = new System.Drawing.Size(501, 21);
            this.lbl居住地址.TabIndex = 52;
            this.lbl居住地址.Text = "labelControl22";
            // 
            // dtp接诊时间
            // 
            this.dtp接诊时间.EditValue = null;
            this.dtp接诊时间.Location = new System.Drawing.Point(98, 112);
            this.dtp接诊时间.Margin = new System.Windows.Forms.Padding(0);
            this.dtp接诊时间.Name = "dtp接诊时间";
            this.dtp接诊时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp接诊时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtp接诊时间.Properties.Mask.EditMask = "";
            this.dtp接诊时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.dtp接诊时间.Size = new System.Drawing.Size(122, 20);
            this.dtp接诊时间.TabIndex = 28;
            // 
            // txt就诊者的主观资料
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txt就诊者的主观资料, 3);
            this.txt就诊者的主观资料.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt就诊者的主观资料.Location = new System.Drawing.Point(98, 136);
            this.txt就诊者的主观资料.Margin = new System.Windows.Forms.Padding(0);
            this.txt就诊者的主观资料.Name = "txt就诊者的主观资料";
            this.txt就诊者的主观资料.Size = new System.Drawing.Size(501, 43);
            this.txt就诊者的主观资料.TabIndex = 30;
            this.txt就诊者的主观资料.UseOptimizedRendering = true;
            // 
            // txt就诊者的客观资料
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txt就诊者的客观资料, 3);
            this.txt就诊者的客观资料.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt就诊者的客观资料.Location = new System.Drawing.Point(98, 180);
            this.txt就诊者的客观资料.Margin = new System.Windows.Forms.Padding(0);
            this.txt就诊者的客观资料.Name = "txt就诊者的客观资料";
            this.txt就诊者的客观资料.Properties.Appearance.Options.UseTextOptions = true;
            this.txt就诊者的客观资料.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.txt就诊者的客观资料.Size = new System.Drawing.Size(501, 84);
            this.txt就诊者的客观资料.TabIndex = 31;
            this.txt就诊者的客观资料.UseOptimizedRendering = true;
            // 
            // UC会诊记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraScrollableControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC会诊记录表";
            this.Size = new System.Drawing.Size(600, 459);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.xtraScrollableControl1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp接诊时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtp接诊时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt就诊者的主观资料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt就诊者的客观资料.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.TextEdit txt接诊医生;
        private DevExpress.XtraEditors.LabelControl lbl个人档案号;
        private DevExpress.XtraEditors.LabelControl lbl性别;
        private DevExpress.XtraEditors.LabelControl lbl出生日期;
        private DevExpress.XtraEditors.LabelControl lbl姓名;
        private DevExpress.XtraEditors.LabelControl lbl身份证号;
        private DevExpress.XtraEditors.LabelControl lbl联系电话;
        private DevExpress.XtraEditors.LabelControl lbl居住地址;
        private DevExpress.XtraEditors.DateEdit dtp接诊时间;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private DevExpress.XtraEditors.LabelControl labelControl22;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        private DevExpress.XtraEditors.LabelControl labelControl24;
        private DevExpress.XtraEditors.LabelControl lbl当前所属机构;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraEditors.LabelControl lbl创建人;
        private DevExpress.XtraEditors.LabelControl labelControl28;
        private DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.LabelControl labelControl30;
        private DevExpress.XtraEditors.LabelControl lbl最近更新时间;
        private DevExpress.XtraEditors.LabelControl lbl创建机构;
        private DevExpress.XtraEditors.LabelControl lbl最近修改人;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.MemoEdit txt就诊者的主观资料;
        private DevExpress.XtraEditors.MemoEdit txt就诊者的客观资料;
    }
}
