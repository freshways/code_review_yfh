﻿using AtomEHR.Library.UserControls;
namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class UC健康体检表_显示
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC健康体检表_显示));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt其他B超 = new DevExpress.XtraEditors.TextEdit();
            this.txt同型半胱氨酸 = new DevExpress.XtraEditors.TextEdit();
            this.txt空腹血糖2 = new DevExpress.XtraEditors.TextEdit();
            this.txtRh = new DevExpress.XtraEditors.TextEdit();
            this.txtABO = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.txt义齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt义齿1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txt齿列其他 = new DevExpress.XtraEditors.TextEdit();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txt龋齿4 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿2 = new DevExpress.XtraEditors.TextEdit();
            this.txt龋齿1 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txt体重指数 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt腰围 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt呼吸频率 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt脉率 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt体温 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.table齿列 = new System.Windows.Forms.TableLayoutPanel();
            this.txt缺齿3 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿4 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt缺齿1 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺齿2 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txt齿列有无 = new DevExpress.XtraEditors.TextEdit();
            this.txt宫颈涂片 = new DevExpress.XtraEditors.TextEdit();
            this.txt阴道 = new DevExpress.XtraEditors.TextEdit();
            this.txt宫颈 = new DevExpress.XtraEditors.TextEdit();
            this.txt附件 = new DevExpress.XtraEditors.TextEdit();
            this.txt宫体 = new DevExpress.XtraEditors.TextEdit();
            this.txt外阴 = new DevExpress.XtraEditors.TextEdit();
            this.gc非免疫规划预防接种史 = new DevExpress.XtraGrid.GridControl();
            this.gv非免疫规划预防接种史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col接种名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col接种日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col接种机构 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gc主要用药情况 = new DevExpress.XtraGrid.GridControl();
            this.gv主要用药情况 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col药物名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用法 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用量 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col用药时间 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col服药依从性 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lkp服药依从性 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gc家庭病床史 = new DevExpress.XtraGrid.GridControl();
            this.gv家庭病床史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col建床日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col撤床日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col原因2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col医疗机构名称2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病案号2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gc住院史 = new DevExpress.XtraGrid.GridControl();
            this.gv住院史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col入院日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col出院日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col原因 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col医疗机构名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col病案号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txt肾脏疾病 = new DevExpress.XtraEditors.TextEdit();
            this.txt心血管疾病 = new DevExpress.XtraEditors.TextEdit();
            this.txt血管疾病 = new DevExpress.XtraEditors.TextEdit();
            this.txt眼部疾病 = new DevExpress.XtraEditors.TextEdit();
            this.txt神经系统疾病 = new DevExpress.XtraEditors.TextEdit();
            this.txt其他系统疾病 = new DevExpress.XtraEditors.TextEdit();
            this.txt脑血管疾病 = new DevExpress.XtraEditors.TextEdit();
            this.txt阳虚质 = new DevExpress.XtraEditors.TextEdit();
            this.txt阴虚质 = new DevExpress.XtraEditors.TextEdit();
            this.txt气虚质 = new DevExpress.XtraEditors.TextEdit();
            this.txt痰湿质 = new DevExpress.XtraEditors.TextEdit();
            this.txt湿热质 = new DevExpress.XtraEditors.TextEdit();
            this.txt血瘀质 = new DevExpress.XtraEditors.TextEdit();
            this.txt气郁质 = new DevExpress.XtraEditors.TextEdit();
            this.txt特秉质 = new DevExpress.XtraEditors.TextEdit();
            this.txt平和质 = new DevExpress.XtraEditors.TextEdit();
            this.txt腹部B超 = new DevExpress.XtraEditors.TextEdit();
            this.txt辅助检查其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt胸部X线片 = new DevExpress.XtraEditors.TextEdit();
            this.txt血清高密度脂蛋白胆固醇 = new DevExpress.XtraEditors.TextEdit();
            this.txt血清低密度脂蛋白胆固醇 = new DevExpress.XtraEditors.TextEdit();
            this.txt甘油三酯 = new DevExpress.XtraEditors.TextEdit();
            this.txt总胆固醇 = new DevExpress.XtraEditors.TextEdit();
            this.txt血钾浓度 = new DevExpress.XtraEditors.TextEdit();
            this.txt血尿素氮 = new DevExpress.XtraEditors.TextEdit();
            this.txt血钠浓度 = new DevExpress.XtraEditors.TextEdit();
            this.txt血清肌酐 = new DevExpress.XtraEditors.TextEdit();
            this.txt白蛋白 = new DevExpress.XtraEditors.TextEdit();
            this.txt总胆红素 = new DevExpress.XtraEditors.TextEdit();
            this.txt血清谷草转氨酶 = new DevExpress.XtraEditors.TextEdit();
            this.txt结合胆红素 = new DevExpress.XtraEditors.TextEdit();
            this.txt血清谷丙转氨酶 = new DevExpress.XtraEditors.TextEdit();
            this.txt糖化血红蛋白 = new DevExpress.XtraEditors.TextEdit();
            this.txt大便潜血 = new DevExpress.XtraEditors.TextEdit();
            this.txt尿微量白蛋白 = new DevExpress.XtraEditors.TextEdit();
            this.txt心电图 = new DevExpress.XtraEditors.TextEdit();
            this.txt空腹血糖 = new DevExpress.XtraEditors.TextEdit();
            this.txt尿常规 = new DevExpress.XtraEditors.TextEdit();
            this.txt血小板 = new DevExpress.XtraEditors.TextEdit();
            this.txt白细胞 = new DevExpress.XtraEditors.TextEdit();
            this.txt血常规其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt血红蛋白 = new DevExpress.XtraEditors.TextEdit();
            this.txt乳腺 = new DevExpress.XtraEditors.TextEdit();
            this.txt肛门指诊 = new DevExpress.XtraEditors.TextEdit();
            this.txt足背动脉搏动 = new DevExpress.XtraEditors.TextEdit();
            this.txt查体其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt下肢水肿 = new DevExpress.XtraEditors.TextEdit();
            this.txt脾大 = new DevExpress.XtraEditors.TextEdit();
            this.txt肝大 = new DevExpress.XtraEditors.TextEdit();
            this.txt包块 = new DevExpress.XtraEditors.TextEdit();
            this.txt移动性浊音 = new DevExpress.XtraEditors.TextEdit();
            this.txt压痛 = new DevExpress.XtraEditors.TextEdit();
            this.txt杂音 = new DevExpress.XtraEditors.TextEdit();
            this.txt心律 = new DevExpress.XtraEditors.TextEdit();
            this.txt心率 = new DevExpress.XtraEditors.TextEdit();
            this.txt呼吸音 = new DevExpress.XtraEditors.TextEdit();
            this.txt罗音 = new DevExpress.XtraEditors.TextEdit();
            this.txt桶状胸 = new DevExpress.XtraEditors.TextEdit();
            this.txt淋巴结 = new DevExpress.XtraEditors.TextEdit();
            this.txt巩膜 = new DevExpress.XtraEditors.TextEdit();
            this.txt皮肤 = new DevExpress.XtraEditors.TextEdit();
            this.txt眼底 = new DevExpress.XtraEditors.TextEdit();
            this.txt运动功能 = new DevExpress.XtraEditors.TextEdit();
            this.txt听力 = new DevExpress.XtraEditors.TextEdit();
            this.txt视力 = new DevExpress.XtraEditors.TextEdit();
            this.txt咽部 = new DevExpress.XtraEditors.TextEdit();
            this.txt口唇 = new DevExpress.XtraEditors.TextEdit();
            this.lbl工种及从业时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl毒物种类 = new DevExpress.XtraEditors.LabelControl();
            this.lbl职业病有无 = new DevExpress.XtraEditors.LabelControl();
            this.txt职业病其他防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业病其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt化学物质防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt化学物质 = new DevExpress.XtraEditors.TextEdit();
            this.txt物理因素防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt物理因素 = new DevExpress.XtraEditors.TextEdit();
            this.txt放射物质防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt放射物质 = new DevExpress.XtraEditors.TextEdit();
            this.txt粉尘防护措施 = new DevExpress.XtraEditors.TextEdit();
            this.txt粉尘 = new DevExpress.XtraEditors.TextEdit();
            this.txt饮酒种类 = new DevExpress.XtraEditors.TextEdit();
            this.txt近一年内是否曾醉酒 = new DevExpress.XtraEditors.TextEdit();
            this.txt开始饮酒年龄 = new DevExpress.XtraEditors.TextEdit();
            this.txt戒酒年龄 = new DevExpress.XtraEditors.TextEdit();
            this.txt是否戒酒 = new DevExpress.XtraEditors.TextEdit();
            this.txt日饮酒量 = new DevExpress.XtraEditors.TextEdit();
            this.txt饮酒频率 = new DevExpress.XtraEditors.TextEdit();
            this.txt戒烟年龄 = new DevExpress.XtraEditors.TextEdit();
            this.txt开始吸烟年龄 = new DevExpress.XtraEditors.TextEdit();
            this.txt日吸烟量 = new DevExpress.XtraEditors.TextEdit();
            this.txt吸烟状况 = new DevExpress.XtraEditors.TextEdit();
            this.txt饮食习惯 = new DevExpress.XtraEditors.TextEdit();
            this.txt锻炼方式 = new DevExpress.XtraEditors.TextEdit();
            this.txt坚持锻炼时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt每次锻炼时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt锻炼频率 = new DevExpress.XtraEditors.TextEdit();
            this.txt右侧原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt左侧原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt血压右侧 = new DevExpress.XtraEditors.TextEdit();
            this.txt左侧血压 = new DevExpress.XtraEditors.TextEdit();
            this.txt症状 = new DevExpress.XtraEditors.TextEdit();
            this.txt最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建人 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt责任医生 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt体检日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt乙型肝炎表面抗原 = new DevExpress.XtraEditors.MemoEdit();
            this.txt老年人健康状态自我评估 = new DevExpress.XtraEditors.MemoEdit();
            this.txt老年人生活自理能力自我评估 = new DevExpress.XtraEditors.MemoEdit();
            this.txt老年人认知能力 = new DevExpress.XtraEditors.MemoEdit();
            this.txt老年人情感状态 = new DevExpress.XtraEditors.MemoEdit();
            this.txt健康指导 = new DevExpress.XtraEditors.MemoEdit();
            this.txt危险因素控制 = new DevExpress.XtraEditors.MemoEdit();
            this.txt健康评价 = new DevExpress.XtraEditors.MemoEdit();
            this.lbl血管疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl体检日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl责任医生 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl居住地址 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl身份证号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl个人档案号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl姓名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl出生日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl性别 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl联系电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl健康评价 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl症状 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem4 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.lbl血压右侧 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血压左侧 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl锻炼频率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl饮食习惯 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl吸烟状况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl饮酒频率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.group职业病 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl粉尘 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl放射物质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl物理因素 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl化学物质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl职业病其他 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl职业病其他防护措施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl化学物质防护措施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl物理因素防护措施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl放射物质防护措施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl粉尘防护措施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.group饮酒情况 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl日饮酒量 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl是否戒酒 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl开始饮酒年龄 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl近一年内是否曾醉酒 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl戒酒年龄 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl饮酒种类 = new DevExpress.XtraLayout.LayoutControlItem();
            this.group吸烟情况 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl日吸烟量 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl开始吸烟年龄 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl戒烟年龄 = new DevExpress.XtraLayout.LayoutControlItem();
            this.group体育锻炼 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl锻炼方式 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl每次锻炼时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl坚持锻炼时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl口唇 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl咽部 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl视力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl听力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl运动功能 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl眼底 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl皮肤 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl巩膜 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl淋巴结 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl桶状胸 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl罗音 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl呼吸音 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl心率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl心律 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl杂音 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl压痛 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl移动性浊音 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl包块 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl肝大 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl脾大 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl下肢水肿 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl查体其他 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl足背动脉搏动 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl肛门指诊 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl乳腺 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血常规其他 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl白细胞 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血小板 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl尿常规 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl空腹血糖 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl心电图 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl尿微量白蛋白 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl乙型肝炎表面抗原 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl大便潜血 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl糖化血红蛋白 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血清谷丙转氨酶 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl结合胆红素 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血清谷草转氨酶 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl总胆红素 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl白蛋白 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血清肌酐 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血钠浓度 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血尿素氮 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血钾浓度 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl总胆固醇 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl甘油三酯 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血清低密度脂蛋白胆固醇 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血清高密度脂蛋白胆固醇 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl胸部X线片 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl辅助检查其他 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl腹部B超 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl平和质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl特秉质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl气郁质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血瘀质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl湿热质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl痰湿质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl气虚质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl阴虚质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl阳虚质 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl脑血管疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl其他系统疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl神经系统疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl眼部疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl心血管疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl肾脏疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl住院史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl家庭病床史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator8 = new DevExpress.XtraLayout.SimpleSeparator();
            this.group老年人 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl老年人情感状态 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl老年人认知能力 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl老年人生活自理能力自我评估 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl老年人健康状态自我评估 = new DevExpress.XtraLayout.LayoutControlItem();
            this.group妇科 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl附件 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl宫体 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl宫颈 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl阴道 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl外阴 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup20 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem22 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl宫颈涂片 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl吸烟情况 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl饮酒情况 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl职业病危害因素接触史 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup16 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup17 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem20 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup18 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem21 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup19 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup21 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup22 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup23 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup24 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup25 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup26 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator12 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup28 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl齿列正常 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl齿列总 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl体温 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl脉率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl呼吸频率 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl身高 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl体重 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl腰围 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl体重指数 = new DevExpress.XtraLayout.LayoutControlItem();
            this.group齿列 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl缺齿 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl龋齿 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl齿列其他 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl义齿 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem23 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl其他B超 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem122 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem124 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem125 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem修改人 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem126 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem修改时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl健康指导 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl危险因素控制 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator9 = new DevExpress.XtraLayout.SimpleSeparator();
            this.panelControl7 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn添加 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btn查看随访照片 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他B超.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt同型半胱氨酸.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt空腹血糖2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtABO.Properties)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿1.Properties)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt齿列其他.Properties)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿1.Properties)).BeginInit();
            this.table齿列.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt齿列有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫颈涂片.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阴道.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫颈.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt附件.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫体.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外阴.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc非免疫规划预防接种史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv非免疫规划预防接种史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc主要用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv主要用药情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp服药依从性)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc家庭病床史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv家庭病床史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肾脏疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心血管疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血管疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼部疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt神经系统疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他系统疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑血管疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阳虚质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阴虚质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt气虚质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt痰湿质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt湿热质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血瘀质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt气郁质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt特秉质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt平和质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt腹部B超.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胸部X线片.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血清高密度脂蛋白胆固醇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血清低密度脂蛋白胆固醇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt甘油三酯.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt总胆固醇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血钾浓度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血尿素氮.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血钠浓度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血清肌酐.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt白蛋白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt总胆红素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血清谷草转氨酶.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结合胆红素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血清谷丙转氨酶.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt糖化血红蛋白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt大便潜血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿微量白蛋白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电图.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt空腹血糖.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿常规.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血小板.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt白细胞.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血常规其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血红蛋白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳腺.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肛门指诊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt足背动脉搏动.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt查体其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下肢水肿.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脾大.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肝大.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt包块.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt移动性浊音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt压痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt杂音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心律.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt呼吸音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt罗音.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt桶状胸.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt淋巴结.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt巩膜.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt皮肤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼底.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt运动功能.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt听力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt视力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt咽部.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt口唇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学物质防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学物质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理因素防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理因素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘防护措施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒种类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt近一年内是否曾醉酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始饮酒年龄.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt戒酒年龄.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt是否戒酒.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt日饮酒量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒频率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt戒烟年龄.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始吸烟年龄.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt日吸烟量.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟状况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮食习惯.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt锻炼方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt坚持锻炼时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt每次锻炼时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt锻炼频率.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右侧原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt左侧原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血压右侧.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt左侧血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt责任医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乙型肝炎表面抗原.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人健康状态自我评估.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人生活自理能力自我评估.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人认知能力.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人情感状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt健康指导.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt危险因素控制.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt健康评价.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血管疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体检日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl责任医生)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl居住地址)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl身份证号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl个人档案号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl姓名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl出生日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl性别)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl联系电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl健康评价)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl症状)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压右侧)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压左侧)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl锻炼频率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮食习惯)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl吸烟状况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮酒频率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group职业病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl粉尘)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl放射物质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl物理因素)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl化学物质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病其他)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病其他防护措施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl化学物质防护措施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl物理因素防护措施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl放射物质防护措施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl粉尘防护措施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group饮酒情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl日饮酒量)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl是否戒酒)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl开始饮酒年龄)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl近一年内是否曾醉酒)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl戒酒年龄)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮酒种类)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group吸烟情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl日吸烟量)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl开始吸烟年龄)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl戒烟年龄)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group体育锻炼)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl锻炼方式)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl每次锻炼时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl坚持锻炼时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl口唇)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl咽部)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl视力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl听力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl运动功能)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl眼底)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl皮肤)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl巩膜)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl淋巴结)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl桶状胸)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl罗音)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl呼吸音)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心律)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl杂音)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl压痛)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl移动性浊音)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl包块)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肝大)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脾大)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl下肢水肿)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl查体其他)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl足背动脉搏动)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肛门指诊)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl乳腺)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血常规其他)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl白细胞)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血小板)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl尿常规)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl空腹血糖)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心电图)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl尿微量白蛋白)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl乙型肝炎表面抗原)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl大便潜血)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl糖化血红蛋白)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清谷丙转氨酶)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl结合胆红素)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清谷草转氨酶)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl总胆红素)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl白蛋白)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清肌酐)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血钠浓度)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血尿素氮)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血钾浓度)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl总胆固醇)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl甘油三酯)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清低密度脂蛋白胆固醇)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清高密度脂蛋白胆固醇)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl胸部X线片)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl辅助检查其他)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl腹部B超)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl平和质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl特秉质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl气郁质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血瘀质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl湿热质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl痰湿质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl气虚质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl阴虚质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl阳虚质)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脑血管疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他系统疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl神经系统疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl眼部疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心血管疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肾脏疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl住院史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家庭病床史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group老年人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl老年人情感状态)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl老年人认知能力)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl老年人生活自理能力自我评估)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl老年人健康状态自我评估)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group妇科)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl附件)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl宫体)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl宫颈)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl阴道)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl外阴)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl宫颈涂片)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl吸烟情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮酒情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病危害因素接触史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl齿列正常)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl齿列总)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体温)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脉率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl呼吸频率)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl身高)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl腰围)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重指数)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group齿列)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl缺齿)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl龋齿)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl齿列其他)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl义齿)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他B超)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem122)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem124)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem125)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem修改人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem126)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem修改时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl健康指导)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl危险因素控制)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).BeginInit();
            this.panelControl7.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Margin = new System.Windows.Forms.Padding(4);
            this.panelControlNavbar.Size = new System.Drawing.Size(106, 498);
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(4);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt其他B超);
            this.layoutControl1.Controls.Add(this.txt同型半胱氨酸);
            this.layoutControl1.Controls.Add(this.txt空腹血糖2);
            this.layoutControl1.Controls.Add(this.txtRh);
            this.layoutControl1.Controls.Add(this.txtABO);
            this.layoutControl1.Controls.Add(this.tableLayoutPanel4);
            this.layoutControl1.Controls.Add(this.tableLayoutPanel3);
            this.layoutControl1.Controls.Add(this.tableLayoutPanel1);
            this.layoutControl1.Controls.Add(this.txt体重指数);
            this.layoutControl1.Controls.Add(this.txt腰围);
            this.layoutControl1.Controls.Add(this.txt体重);
            this.layoutControl1.Controls.Add(this.txt身高);
            this.layoutControl1.Controls.Add(this.txt呼吸频率);
            this.layoutControl1.Controls.Add(this.txt脉率);
            this.layoutControl1.Controls.Add(this.txt体温);
            this.layoutControl1.Controls.Add(this.table齿列);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.txt齿列有无);
            this.layoutControl1.Controls.Add(this.txt宫颈涂片);
            this.layoutControl1.Controls.Add(this.txt阴道);
            this.layoutControl1.Controls.Add(this.txt宫颈);
            this.layoutControl1.Controls.Add(this.txt附件);
            this.layoutControl1.Controls.Add(this.txt宫体);
            this.layoutControl1.Controls.Add(this.txt外阴);
            this.layoutControl1.Controls.Add(this.gc非免疫规划预防接种史);
            this.layoutControl1.Controls.Add(this.gc主要用药情况);
            this.layoutControl1.Controls.Add(this.gc家庭病床史);
            this.layoutControl1.Controls.Add(this.gc住院史);
            this.layoutControl1.Controls.Add(this.txt肾脏疾病);
            this.layoutControl1.Controls.Add(this.txt心血管疾病);
            this.layoutControl1.Controls.Add(this.txt血管疾病);
            this.layoutControl1.Controls.Add(this.txt眼部疾病);
            this.layoutControl1.Controls.Add(this.txt神经系统疾病);
            this.layoutControl1.Controls.Add(this.txt其他系统疾病);
            this.layoutControl1.Controls.Add(this.txt脑血管疾病);
            this.layoutControl1.Controls.Add(this.txt阳虚质);
            this.layoutControl1.Controls.Add(this.txt阴虚质);
            this.layoutControl1.Controls.Add(this.txt气虚质);
            this.layoutControl1.Controls.Add(this.txt痰湿质);
            this.layoutControl1.Controls.Add(this.txt湿热质);
            this.layoutControl1.Controls.Add(this.txt血瘀质);
            this.layoutControl1.Controls.Add(this.txt气郁质);
            this.layoutControl1.Controls.Add(this.txt特秉质);
            this.layoutControl1.Controls.Add(this.txt平和质);
            this.layoutControl1.Controls.Add(this.txt腹部B超);
            this.layoutControl1.Controls.Add(this.txt辅助检查其他);
            this.layoutControl1.Controls.Add(this.txt胸部X线片);
            this.layoutControl1.Controls.Add(this.txt血清高密度脂蛋白胆固醇);
            this.layoutControl1.Controls.Add(this.txt血清低密度脂蛋白胆固醇);
            this.layoutControl1.Controls.Add(this.txt甘油三酯);
            this.layoutControl1.Controls.Add(this.txt总胆固醇);
            this.layoutControl1.Controls.Add(this.txt血钾浓度);
            this.layoutControl1.Controls.Add(this.txt血尿素氮);
            this.layoutControl1.Controls.Add(this.txt血钠浓度);
            this.layoutControl1.Controls.Add(this.txt血清肌酐);
            this.layoutControl1.Controls.Add(this.txt白蛋白);
            this.layoutControl1.Controls.Add(this.txt总胆红素);
            this.layoutControl1.Controls.Add(this.txt血清谷草转氨酶);
            this.layoutControl1.Controls.Add(this.txt结合胆红素);
            this.layoutControl1.Controls.Add(this.txt血清谷丙转氨酶);
            this.layoutControl1.Controls.Add(this.txt糖化血红蛋白);
            this.layoutControl1.Controls.Add(this.txt大便潜血);
            this.layoutControl1.Controls.Add(this.txt尿微量白蛋白);
            this.layoutControl1.Controls.Add(this.txt心电图);
            this.layoutControl1.Controls.Add(this.txt空腹血糖);
            this.layoutControl1.Controls.Add(this.txt尿常规);
            this.layoutControl1.Controls.Add(this.txt血小板);
            this.layoutControl1.Controls.Add(this.txt白细胞);
            this.layoutControl1.Controls.Add(this.txt血常规其他);
            this.layoutControl1.Controls.Add(this.txt血红蛋白);
            this.layoutControl1.Controls.Add(this.txt乳腺);
            this.layoutControl1.Controls.Add(this.txt肛门指诊);
            this.layoutControl1.Controls.Add(this.txt足背动脉搏动);
            this.layoutControl1.Controls.Add(this.txt查体其他);
            this.layoutControl1.Controls.Add(this.txt下肢水肿);
            this.layoutControl1.Controls.Add(this.txt脾大);
            this.layoutControl1.Controls.Add(this.txt肝大);
            this.layoutControl1.Controls.Add(this.txt包块);
            this.layoutControl1.Controls.Add(this.txt移动性浊音);
            this.layoutControl1.Controls.Add(this.txt压痛);
            this.layoutControl1.Controls.Add(this.txt杂音);
            this.layoutControl1.Controls.Add(this.txt心律);
            this.layoutControl1.Controls.Add(this.txt心率);
            this.layoutControl1.Controls.Add(this.txt呼吸音);
            this.layoutControl1.Controls.Add(this.txt罗音);
            this.layoutControl1.Controls.Add(this.txt桶状胸);
            this.layoutControl1.Controls.Add(this.txt淋巴结);
            this.layoutControl1.Controls.Add(this.txt巩膜);
            this.layoutControl1.Controls.Add(this.txt皮肤);
            this.layoutControl1.Controls.Add(this.txt眼底);
            this.layoutControl1.Controls.Add(this.txt运动功能);
            this.layoutControl1.Controls.Add(this.txt听力);
            this.layoutControl1.Controls.Add(this.txt视力);
            this.layoutControl1.Controls.Add(this.txt咽部);
            this.layoutControl1.Controls.Add(this.txt口唇);
            this.layoutControl1.Controls.Add(this.lbl工种及从业时间);
            this.layoutControl1.Controls.Add(this.lbl毒物种类);
            this.layoutControl1.Controls.Add(this.lbl职业病有无);
            this.layoutControl1.Controls.Add(this.txt职业病其他防护措施);
            this.layoutControl1.Controls.Add(this.txt职业病其他);
            this.layoutControl1.Controls.Add(this.txt化学物质防护措施);
            this.layoutControl1.Controls.Add(this.txt化学物质);
            this.layoutControl1.Controls.Add(this.txt物理因素防护措施);
            this.layoutControl1.Controls.Add(this.txt物理因素);
            this.layoutControl1.Controls.Add(this.txt放射物质防护措施);
            this.layoutControl1.Controls.Add(this.txt放射物质);
            this.layoutControl1.Controls.Add(this.txt粉尘防护措施);
            this.layoutControl1.Controls.Add(this.txt粉尘);
            this.layoutControl1.Controls.Add(this.txt饮酒种类);
            this.layoutControl1.Controls.Add(this.txt近一年内是否曾醉酒);
            this.layoutControl1.Controls.Add(this.txt开始饮酒年龄);
            this.layoutControl1.Controls.Add(this.txt戒酒年龄);
            this.layoutControl1.Controls.Add(this.txt是否戒酒);
            this.layoutControl1.Controls.Add(this.txt日饮酒量);
            this.layoutControl1.Controls.Add(this.txt饮酒频率);
            this.layoutControl1.Controls.Add(this.txt戒烟年龄);
            this.layoutControl1.Controls.Add(this.txt开始吸烟年龄);
            this.layoutControl1.Controls.Add(this.txt日吸烟量);
            this.layoutControl1.Controls.Add(this.txt吸烟状况);
            this.layoutControl1.Controls.Add(this.txt饮食习惯);
            this.layoutControl1.Controls.Add(this.txt锻炼方式);
            this.layoutControl1.Controls.Add(this.txt坚持锻炼时间);
            this.layoutControl1.Controls.Add(this.txt每次锻炼时间);
            this.layoutControl1.Controls.Add(this.txt锻炼频率);
            this.layoutControl1.Controls.Add(this.txt右侧原因);
            this.layoutControl1.Controls.Add(this.txt左侧原因);
            this.layoutControl1.Controls.Add(this.txt血压右侧);
            this.layoutControl1.Controls.Add(this.txt左侧血压);
            this.layoutControl1.Controls.Add(this.txt症状);
            this.layoutControl1.Controls.Add(this.txt最近修改人);
            this.layoutControl1.Controls.Add(this.txt创建人);
            this.layoutControl1.Controls.Add(this.txt创建机构);
            this.layoutControl1.Controls.Add(this.txt当前所属机构);
            this.layoutControl1.Controls.Add(this.txt最近更新时间);
            this.layoutControl1.Controls.Add(this.txt创建时间);
            this.layoutControl1.Controls.Add(this.txt责任医生);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt个人档案编号);
            this.layoutControl1.Controls.Add(this.txt体检日期);
            this.layoutControl1.Controls.Add(this.txt乙型肝炎表面抗原);
            this.layoutControl1.Controls.Add(this.txt老年人健康状态自我评估);
            this.layoutControl1.Controls.Add(this.txt老年人生活自理能力自我评估);
            this.layoutControl1.Controls.Add(this.txt老年人认知能力);
            this.layoutControl1.Controls.Add(this.txt老年人情感状态);
            this.layoutControl1.Controls.Add(this.txt健康指导);
            this.layoutControl1.Controls.Add(this.txt危险因素控制);
            this.layoutControl1.Controls.Add(this.txt健康评价);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl血管疾病});
            this.layoutControl1.Location = new System.Drawing.Point(106, 35);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(952, 188, 250, 350);
            this.layoutControl1.OptionsView.AutoSizeModeInLayoutControl = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(692, 463);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt其他B超
            // 
            this.txt其他B超.Location = new System.Drawing.Point(261, -342);
            this.txt其他B超.Name = "txt其他B超";
            this.txt其他B超.Size = new System.Drawing.Size(410, 20);
            this.txt其他B超.StyleController = this.layoutControl1;
            this.txt其他B超.TabIndex = 242;
            // 
            // txt同型半胱氨酸
            // 
            this.txt同型半胱氨酸.Location = new System.Drawing.Point(181, -862);
            this.txt同型半胱氨酸.Name = "txt同型半胱氨酸";
            this.txt同型半胱氨酸.Size = new System.Drawing.Size(490, 20);
            this.txt同型半胱氨酸.StyleController = this.layoutControl1;
            this.txt同型半胱氨酸.TabIndex = 241;
            // 
            // txt空腹血糖2
            // 
            this.txt空腹血糖2.Location = new System.Drawing.Point(416, -886);
            this.txt空腹血糖2.Name = "txt空腹血糖2";
            this.txt空腹血糖2.Size = new System.Drawing.Size(255, 20);
            this.txt空腹血糖2.StyleController = this.layoutControl1;
            this.txt空腹血糖2.TabIndex = 240;
            // 
            // txtRh
            // 
            this.txtRh.Location = new System.Drawing.Point(504, -1031);
            this.txtRh.Name = "txtRh";
            this.txtRh.Size = new System.Drawing.Size(167, 20);
            this.txtRh.StyleController = this.layoutControl1;
            this.txtRh.TabIndex = 239;
            // 
            // txtABO
            // 
            this.txtABO.Location = new System.Drawing.Point(284, -1031);
            this.txtABO.Name = "txtABO";
            this.txtABO.Size = new System.Drawing.Size(111, 20);
            this.txtABO.StyleController = this.layoutControl1;
            this.txtABO.TabIndex = 238;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 444F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.txt义齿4, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.txt义齿3, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.txt义齿2, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.txt义齿1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.labelControl3, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(264, -1828);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(406, 46);
            this.tableLayoutPanel4.TabIndex = 232;
            // 
            // txt义齿4
            // 
            this.txt义齿4.Dock = System.Windows.Forms.DockStyle.Right;
            this.txt义齿4.Enabled = false;
            this.txt义齿4.Location = new System.Drawing.Point(319, 23);
            this.txt义齿4.Margin = new System.Windows.Forms.Padding(0);
            this.txt义齿4.Name = "txt义齿4";
            this.txt义齿4.Size = new System.Drawing.Size(188, 20);
            this.txt义齿4.TabIndex = 9;
            // 
            // txt义齿3
            // 
            this.txt义齿3.Dock = System.Windows.Forms.DockStyle.Left;
            this.txt义齿3.Enabled = false;
            this.txt义齿3.Location = new System.Drawing.Point(37, 23);
            this.txt义齿3.Margin = new System.Windows.Forms.Padding(0);
            this.txt义齿3.MinimumSize = new System.Drawing.Size(180, 0);
            this.txt义齿3.Name = "txt义齿3";
            this.txt义齿3.Size = new System.Drawing.Size(180, 20);
            this.txt义齿3.TabIndex = 8;
            // 
            // txt义齿2
            // 
            this.txt义齿2.Dock = System.Windows.Forms.DockStyle.Right;
            this.txt义齿2.Enabled = false;
            this.txt义齿2.Location = new System.Drawing.Point(319, 1);
            this.txt义齿2.Margin = new System.Windows.Forms.Padding(0);
            this.txt义齿2.Name = "txt义齿2";
            this.txt义齿2.Size = new System.Drawing.Size(188, 20);
            this.txt义齿2.TabIndex = 7;
            // 
            // txt义齿1
            // 
            this.txt义齿1.Dock = System.Windows.Forms.DockStyle.Left;
            this.txt义齿1.Enabled = false;
            this.txt义齿1.Location = new System.Drawing.Point(37, 1);
            this.txt义齿1.Margin = new System.Windows.Forms.Padding(0);
            this.txt义齿1.MinimumSize = new System.Drawing.Size(180, 0);
            this.txt义齿1.Name = "txt义齿1";
            this.txt义齿1.Size = new System.Drawing.Size(180, 20);
            this.txt义齿1.TabIndex = 6;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl3.Location = new System.Drawing.Point(1, 1);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl3.Name = "labelControl3";
            this.tableLayoutPanel4.SetRowSpan(this.labelControl3, 2);
            this.labelControl3.Size = new System.Drawing.Size(35, 44);
            this.labelControl3.TabIndex = 5;
            this.labelControl3.Text = "义齿";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.labelControl17, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txt齿列其他, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(264, -1778);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(406, 46);
            this.tableLayoutPanel3.TabIndex = 231;
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl17.Location = new System.Drawing.Point(1, 1);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl17.Name = "labelControl17";
            this.tableLayoutPanel3.SetRowSpan(this.labelControl17, 2);
            this.labelControl17.Size = new System.Drawing.Size(35, 44);
            this.labelControl17.TabIndex = 15;
            this.labelControl17.Text = "其他";
            // 
            // txt齿列其他
            // 
            this.txt齿列其他.Enabled = false;
            this.txt齿列其他.Location = new System.Drawing.Point(37, 11);
            this.txt齿列其他.Margin = new System.Windows.Forms.Padding(0, 10, 0, 3);
            this.txt齿列其他.Name = "txt齿列其他";
            this.tableLayoutPanel3.SetRowSpan(this.txt齿列其他, 2);
            this.txt齿列其他.Size = new System.Drawing.Size(180, 20);
            this.txt齿列其他.TabIndex = 16;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 444F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿4, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿3, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt龋齿1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelControl7, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(264, -1878);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(406, 46);
            this.tableLayoutPanel1.TabIndex = 231;
            // 
            // txt龋齿4
            // 
            this.txt龋齿4.Dock = System.Windows.Forms.DockStyle.Right;
            this.txt龋齿4.Enabled = false;
            this.txt龋齿4.Location = new System.Drawing.Point(319, 23);
            this.txt龋齿4.Margin = new System.Windows.Forms.Padding(0);
            this.txt龋齿4.Name = "txt龋齿4";
            this.txt龋齿4.Size = new System.Drawing.Size(188, 20);
            this.txt龋齿4.TabIndex = 9;
            // 
            // txt龋齿3
            // 
            this.txt龋齿3.Dock = System.Windows.Forms.DockStyle.Left;
            this.txt龋齿3.Enabled = false;
            this.txt龋齿3.Location = new System.Drawing.Point(37, 23);
            this.txt龋齿3.Margin = new System.Windows.Forms.Padding(0);
            this.txt龋齿3.MinimumSize = new System.Drawing.Size(180, 0);
            this.txt龋齿3.Name = "txt龋齿3";
            this.txt龋齿3.Size = new System.Drawing.Size(180, 20);
            this.txt龋齿3.TabIndex = 8;
            // 
            // txt龋齿2
            // 
            this.txt龋齿2.Dock = System.Windows.Forms.DockStyle.Right;
            this.txt龋齿2.Enabled = false;
            this.txt龋齿2.Location = new System.Drawing.Point(319, 1);
            this.txt龋齿2.Margin = new System.Windows.Forms.Padding(0);
            this.txt龋齿2.Name = "txt龋齿2";
            this.txt龋齿2.Size = new System.Drawing.Size(188, 20);
            this.txt龋齿2.TabIndex = 7;
            // 
            // txt龋齿1
            // 
            this.txt龋齿1.Dock = System.Windows.Forms.DockStyle.Left;
            this.txt龋齿1.Enabled = false;
            this.txt龋齿1.Location = new System.Drawing.Point(37, 1);
            this.txt龋齿1.Margin = new System.Windows.Forms.Padding(0);
            this.txt龋齿1.MinimumSize = new System.Drawing.Size(180, 0);
            this.txt龋齿1.Name = "txt龋齿1";
            this.txt龋齿1.Size = new System.Drawing.Size(180, 20);
            this.txt龋齿1.TabIndex = 6;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl7.Location = new System.Drawing.Point(1, 1);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl7.Name = "labelControl7";
            this.tableLayoutPanel1.SetRowSpan(this.labelControl7, 2);
            this.labelControl7.Size = new System.Drawing.Size(35, 44);
            this.labelControl7.TabIndex = 5;
            this.labelControl7.Text = "龋齿";
            // 
            // txt体重指数
            // 
            this.txt体重指数.BackColor = System.Drawing.Color.Transparent;
            this.txt体重指数.Lbl1Size = new System.Drawing.Size(75, 18);
            this.txt体重指数.Lbl1Text = "Kg/㎡";
            this.txt体重指数.Location = new System.Drawing.Point(371, -2605);
            this.txt体重指数.Margin = new System.Windows.Forms.Padding(4);
            this.txt体重指数.Name = "txt体重指数";
            this.txt体重指数.Size = new System.Drawing.Size(300, 20);
            this.txt体重指数.TabIndex = 237;
            this.txt体重指数.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt腰围
            // 
            this.txt腰围.BackColor = System.Drawing.Color.Transparent;
            this.txt腰围.Lbl1Size = new System.Drawing.Size(30, 18);
            this.txt腰围.Lbl1Text = "CM";
            this.txt腰围.Location = new System.Drawing.Point(181, -2605);
            this.txt腰围.Margin = new System.Windows.Forms.Padding(4);
            this.txt腰围.Name = "txt腰围";
            this.txt腰围.Size = new System.Drawing.Size(91, 20);
            this.txt腰围.TabIndex = 236;
            this.txt腰围.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt体重
            // 
            this.txt体重.BackColor = System.Drawing.Color.Transparent;
            this.txt体重.Lbl1Size = new System.Drawing.Size(75, 18);
            this.txt体重.Lbl1Text = "kg";
            this.txt体重.Location = new System.Drawing.Point(371, -2629);
            this.txt体重.Margin = new System.Windows.Forms.Padding(4);
            this.txt体重.Name = "txt体重";
            this.txt体重.Size = new System.Drawing.Size(300, 20);
            this.txt体重.TabIndex = 235;
            this.txt体重.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt身高
            // 
            this.txt身高.BackColor = System.Drawing.Color.Transparent;
            this.txt身高.Lbl1Size = new System.Drawing.Size(30, 18);
            this.txt身高.Lbl1Text = "CM";
            this.txt身高.Location = new System.Drawing.Point(181, -2629);
            this.txt身高.Margin = new System.Windows.Forms.Padding(4);
            this.txt身高.Name = "txt身高";
            this.txt身高.Size = new System.Drawing.Size(91, 20);
            this.txt身高.TabIndex = 234;
            this.txt身高.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // txt呼吸频率
            // 
            this.txt呼吸频率.BackColor = System.Drawing.Color.Transparent;
            this.txt呼吸频率.Lbl1Size = new System.Drawing.Size(40, 18);
            this.txt呼吸频率.Lbl1Text = "次/分钟";
            this.txt呼吸频率.Location = new System.Drawing.Point(181, -2677);
            this.txt呼吸频率.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.txt呼吸频率.Name = "txt呼吸频率";
            this.txt呼吸频率.Size = new System.Drawing.Size(91, 44);
            this.txt呼吸频率.TabIndex = 233;
            this.txt呼吸频率.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // txt脉率
            // 
            this.txt脉率.BackColor = System.Drawing.Color.Transparent;
            this.txt脉率.Lbl1Size = new System.Drawing.Size(75, 18);
            this.txt脉率.Lbl1Text = "次/分钟";
            this.txt脉率.Location = new System.Drawing.Point(371, -2701);
            this.txt脉率.Margin = new System.Windows.Forms.Padding(4);
            this.txt脉率.Name = "txt脉率";
            this.txt脉率.Size = new System.Drawing.Size(300, 20);
            this.txt脉率.TabIndex = 232;
            this.txt脉率.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt体温
            // 
            this.txt体温.BackColor = System.Drawing.Color.Transparent;
            this.txt体温.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt体温.Lbl1Text = "℃";
            this.txt体温.Location = new System.Drawing.Point(181, -2701);
            this.txt体温.Margin = new System.Windows.Forms.Padding(4);
            this.txt体温.Name = "txt体温";
            this.txt体温.Size = new System.Drawing.Size(91, 20);
            this.txt体温.TabIndex = 231;
            this.txt体温.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // table齿列
            // 
            this.table齿列.BackColor = System.Drawing.Color.White;
            this.table齿列.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.table齿列.ColumnCount = 3;
            this.table齿列.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.table齿列.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.table齿列.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 444F));
            this.table齿列.Controls.Add(this.txt缺齿3, 0, 1);
            this.table齿列.Controls.Add(this.txt缺齿4, 0, 1);
            this.table齿列.Controls.Add(this.labelControl2, 0, 0);
            this.table齿列.Controls.Add(this.txt缺齿1, 1, 0);
            this.table齿列.Controls.Add(this.txt缺齿2, 2, 0);
            this.table齿列.Location = new System.Drawing.Point(264, -1928);
            this.table齿列.Name = "table齿列";
            this.table齿列.RowCount = 2;
            this.table齿列.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.table齿列.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.table齿列.Size = new System.Drawing.Size(406, 46);
            this.table齿列.TabIndex = 230;
            // 
            // txt缺齿3
            // 
            this.txt缺齿3.Enabled = false;
            this.txt缺齿3.Location = new System.Drawing.Point(37, 23);
            this.txt缺齿3.Margin = new System.Windows.Forms.Padding(0);
            this.txt缺齿3.MinimumSize = new System.Drawing.Size(180, 0);
            this.txt缺齿3.Name = "txt缺齿3";
            this.txt缺齿3.Size = new System.Drawing.Size(180, 20);
            this.txt缺齿3.TabIndex = 14;
            // 
            // txt缺齿4
            // 
            this.txt缺齿4.Dock = System.Windows.Forms.DockStyle.Right;
            this.txt缺齿4.Enabled = false;
            this.txt缺齿4.Location = new System.Drawing.Point(319, 23);
            this.txt缺齿4.Margin = new System.Windows.Forms.Padding(0);
            this.txt缺齿4.Name = "txt缺齿4";
            this.txt缺齿4.Size = new System.Drawing.Size(188, 20);
            this.txt缺齿4.TabIndex = 13;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.BackColor = System.Drawing.Color.White;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelControl2.Location = new System.Drawing.Point(1, 1);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl2.Name = "labelControl2";
            this.table齿列.SetRowSpan(this.labelControl2, 2);
            this.labelControl2.Size = new System.Drawing.Size(35, 44);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "缺齿";
            // 
            // txt缺齿1
            // 
            this.txt缺齿1.Enabled = false;
            this.txt缺齿1.Location = new System.Drawing.Point(37, 1);
            this.txt缺齿1.Margin = new System.Windows.Forms.Padding(0);
            this.txt缺齿1.MinimumSize = new System.Drawing.Size(180, 0);
            this.txt缺齿1.Name = "txt缺齿1";
            this.txt缺齿1.Size = new System.Drawing.Size(180, 20);
            this.txt缺齿1.TabIndex = 1;
            // 
            // txt缺齿2
            // 
            this.txt缺齿2.Dock = System.Windows.Forms.DockStyle.Right;
            this.txt缺齿2.Enabled = false;
            this.txt缺齿2.Location = new System.Drawing.Point(319, 1);
            this.txt缺齿2.Margin = new System.Windows.Forms.Padding(0);
            this.txt缺齿2.Name = "txt缺齿2";
            this.txt缺齿2.Size = new System.Drawing.Size(188, 20);
            this.txt缺齿2.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, -2862);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(70, 14);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 229;
            this.labelControl1.Text = "labelControl1";
            // 
            // txt齿列有无
            // 
            this.txt齿列有无.Location = new System.Drawing.Point(263, -1953);
            this.txt齿列有无.Name = "txt齿列有无";
            this.txt齿列有无.Size = new System.Drawing.Size(408, 20);
            this.txt齿列有无.StyleController = this.layoutControl1;
            this.txt齿列有无.TabIndex = 228;
            // 
            // txt宫颈涂片
            // 
            this.txt宫颈涂片.Location = new System.Drawing.Point(181, -316);
            this.txt宫颈涂片.Name = "txt宫颈涂片";
            this.txt宫颈涂片.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt宫颈涂片.Properties.Appearance.Options.UseBackColor = true;
            this.txt宫颈涂片.Properties.ReadOnly = true;
            this.txt宫颈涂片.Size = new System.Drawing.Size(490, 20);
            this.txt宫颈涂片.StyleController = this.layoutControl1;
            this.txt宫颈涂片.TabIndex = 227;
            // 
            // txt阴道
            // 
            this.txt阴道.Location = new System.Drawing.Point(293, -1151);
            this.txt阴道.Name = "txt阴道";
            this.txt阴道.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt阴道.Properties.Appearance.Options.UseBackColor = true;
            this.txt阴道.Properties.ReadOnly = true;
            this.txt阴道.Size = new System.Drawing.Size(378, 20);
            this.txt阴道.StyleController = this.layoutControl1;
            this.txt阴道.TabIndex = 226;
            // 
            // txt宫颈
            // 
            this.txt宫颈.Location = new System.Drawing.Point(293, -1127);
            this.txt宫颈.Name = "txt宫颈";
            this.txt宫颈.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt宫颈.Properties.Appearance.Options.UseBackColor = true;
            this.txt宫颈.Properties.ReadOnly = true;
            this.txt宫颈.Size = new System.Drawing.Size(378, 20);
            this.txt宫颈.StyleController = this.layoutControl1;
            this.txt宫颈.TabIndex = 225;
            // 
            // txt附件
            // 
            this.txt附件.Location = new System.Drawing.Point(293, -1079);
            this.txt附件.Name = "txt附件";
            this.txt附件.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt附件.Properties.Appearance.Options.UseBackColor = true;
            this.txt附件.Properties.ReadOnly = true;
            this.txt附件.Size = new System.Drawing.Size(378, 20);
            this.txt附件.StyleController = this.layoutControl1;
            this.txt附件.TabIndex = 224;
            // 
            // txt宫体
            // 
            this.txt宫体.Location = new System.Drawing.Point(293, -1103);
            this.txt宫体.Name = "txt宫体";
            this.txt宫体.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt宫体.Properties.Appearance.Options.UseBackColor = true;
            this.txt宫体.Properties.ReadOnly = true;
            this.txt宫体.Size = new System.Drawing.Size(378, 20);
            this.txt宫体.StyleController = this.layoutControl1;
            this.txt宫体.TabIndex = 223;
            // 
            // txt外阴
            // 
            this.txt外阴.Location = new System.Drawing.Point(293, -1175);
            this.txt外阴.Name = "txt外阴";
            this.txt外阴.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt外阴.Properties.Appearance.Options.UseBackColor = true;
            this.txt外阴.Properties.ReadOnly = true;
            this.txt外阴.Size = new System.Drawing.Size(378, 20);
            this.txt外阴.StyleController = this.layoutControl1;
            this.txt外阴.TabIndex = 222;
            // 
            // gc非免疫规划预防接种史
            // 
            this.gc非免疫规划预防接种史.Location = new System.Drawing.Point(89, 274);
            this.gc非免疫规划预防接种史.MainView = this.gv非免疫规划预防接种史;
            this.gc非免疫规划预防接种史.Name = "gc非免疫规划预防接种史";
            this.gc非免疫规划预防接种史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit3});
            this.gc非免疫规划预防接种史.Size = new System.Drawing.Size(582, 56);
            this.gc非免疫规划预防接种史.TabIndex = 214;
            this.gc非免疫规划预防接种史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv非免疫规划预防接种史});
            // 
            // gv非免疫规划预防接种史
            // 
            this.gv非免疫规划预防接种史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col接种名称,
            this.col接种日期,
            this.col接种机构});
            this.gv非免疫规划预防接种史.GridControl = this.gc非免疫规划预防接种史;
            this.gv非免疫规划预防接种史.Name = "gv非免疫规划预防接种史";
            this.gv非免疫规划预防接种史.OptionsView.ShowGroupPanel = false;
            // 
            // col接种名称
            // 
            this.col接种名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col接种名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col接种名称.Caption = "接种名称";
            this.col接种名称.FieldName = "接种名称";
            this.col接种名称.Name = "col接种名称";
            this.col接种名称.OptionsColumn.ReadOnly = true;
            this.col接种名称.Visible = true;
            this.col接种名称.VisibleIndex = 0;
            // 
            // col接种日期
            // 
            this.col接种日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col接种日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col接种日期.Caption = "接种日期";
            this.col接种日期.ColumnEdit = this.repositoryItemDateEdit3;
            this.col接种日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.col接种日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col接种日期.FieldName = "接种日期";
            this.col接种日期.Name = "col接种日期";
            this.col接种日期.OptionsColumn.ReadOnly = true;
            this.col接种日期.Visible = true;
            this.col接种日期.VisibleIndex = 1;
            // 
            // repositoryItemDateEdit3
            // 
            this.repositoryItemDateEdit3.AutoHeight = false;
            this.repositoryItemDateEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit3.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit3.Name = "repositoryItemDateEdit3";
            // 
            // col接种机构
            // 
            this.col接种机构.AppearanceHeader.Options.UseTextOptions = true;
            this.col接种机构.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col接种机构.Caption = "接种机构";
            this.col接种机构.FieldName = "接种机构";
            this.col接种机构.Name = "col接种机构";
            this.col接种机构.OptionsColumn.ReadOnly = true;
            this.col接种机构.Visible = true;
            this.col接种机构.VisibleIndex = 2;
            // 
            // gc主要用药情况
            // 
            this.gc主要用药情况.Location = new System.Drawing.Point(89, 212);
            this.gc主要用药情况.MainView = this.gv主要用药情况;
            this.gc主要用药情况.Name = "gc主要用药情况";
            this.gc主要用药情况.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.lkp服药依从性});
            this.gc主要用药情况.Size = new System.Drawing.Size(582, 56);
            this.gc主要用药情况.TabIndex = 213;
            this.gc主要用药情况.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv主要用药情况});
            // 
            // gv主要用药情况
            // 
            this.gv主要用药情况.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col药物名称,
            this.col用法,
            this.col用量,
            this.col用药时间,
            this.col服药依从性});
            this.gv主要用药情况.GridControl = this.gc主要用药情况;
            this.gv主要用药情况.Name = "gv主要用药情况";
            this.gv主要用药情况.OptionsView.ShowGroupPanel = false;
            // 
            // col药物名称
            // 
            this.col药物名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col药物名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col药物名称.Caption = "药物名称";
            this.col药物名称.FieldName = "药物名称";
            this.col药物名称.Name = "col药物名称";
            this.col药物名称.OptionsColumn.ReadOnly = true;
            this.col药物名称.Visible = true;
            this.col药物名称.VisibleIndex = 0;
            // 
            // col用法
            // 
            this.col用法.AppearanceHeader.Options.UseTextOptions = true;
            this.col用法.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col用法.Caption = "用法";
            this.col用法.FieldName = "用法";
            this.col用法.Name = "col用法";
            this.col用法.OptionsColumn.ReadOnly = true;
            this.col用法.Visible = true;
            this.col用法.VisibleIndex = 1;
            // 
            // col用量
            // 
            this.col用量.AppearanceHeader.Options.UseTextOptions = true;
            this.col用量.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col用量.Caption = "用量 ";
            this.col用量.FieldName = "用量";
            this.col用量.Name = "col用量";
            this.col用量.OptionsColumn.ReadOnly = true;
            this.col用量.Visible = true;
            this.col用量.VisibleIndex = 2;
            // 
            // col用药时间
            // 
            this.col用药时间.AppearanceHeader.Options.UseTextOptions = true;
            this.col用药时间.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col用药时间.Caption = "用药时间";
            this.col用药时间.FieldName = "用药时间";
            this.col用药时间.Name = "col用药时间";
            this.col用药时间.OptionsColumn.ReadOnly = true;
            this.col用药时间.Visible = true;
            this.col用药时间.VisibleIndex = 3;
            // 
            // col服药依从性
            // 
            this.col服药依从性.AppearanceHeader.Options.UseTextOptions = true;
            this.col服药依从性.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col服药依从性.Caption = "服药依从性";
            this.col服药依从性.ColumnEdit = this.lkp服药依从性;
            this.col服药依从性.FieldName = "服药依从性";
            this.col服药依从性.Name = "col服药依从性";
            this.col服药依从性.OptionsColumn.ReadOnly = true;
            this.col服药依从性.Visible = true;
            this.col服药依从性.VisibleIndex = 4;
            // 
            // lkp服药依从性
            // 
            this.lkp服药依从性.AutoHeight = false;
            this.lkp服药依从性.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp服药依从性.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_CODE", "服药依从性")});
            this.lkp服药依从性.Name = "lkp服药依从性";
            // 
            // gc家庭病床史
            // 
            this.gc家庭病床史.Location = new System.Drawing.Point(181, 152);
            this.gc家庭病床史.MainView = this.gv家庭病床史;
            this.gc家庭病床史.Name = "gc家庭病床史";
            this.gc家庭病床史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit2});
            this.gc家庭病床史.Size = new System.Drawing.Size(490, 56);
            this.gc家庭病床史.TabIndex = 212;
            this.gc家庭病床史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv家庭病床史});
            // 
            // gv家庭病床史
            // 
            this.gv家庭病床史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col建床日期,
            this.col撤床日期,
            this.col原因2,
            this.col医疗机构名称2,
            this.col病案号2});
            this.gv家庭病床史.GridControl = this.gc家庭病床史;
            this.gv家庭病床史.Name = "gv家庭病床史";
            this.gv家庭病床史.OptionsView.ShowGroupPanel = false;
            // 
            // col建床日期
            // 
            this.col建床日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col建床日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col建床日期.Caption = "建床日期";
            this.col建床日期.ColumnEdit = this.repositoryItemDateEdit2;
            this.col建床日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.col建床日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col建床日期.FieldName = "入院日期";
            this.col建床日期.Name = "col建床日期";
            this.col建床日期.OptionsColumn.ReadOnly = true;
            this.col建床日期.Visible = true;
            this.col建床日期.VisibleIndex = 0;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // col撤床日期
            // 
            this.col撤床日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col撤床日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col撤床日期.Caption = "撤床日期";
            this.col撤床日期.ColumnEdit = this.repositoryItemDateEdit2;
            this.col撤床日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.col撤床日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col撤床日期.FieldName = "出院日期";
            this.col撤床日期.Name = "col撤床日期";
            this.col撤床日期.OptionsColumn.ReadOnly = true;
            this.col撤床日期.Visible = true;
            this.col撤床日期.VisibleIndex = 1;
            // 
            // col原因2
            // 
            this.col原因2.AppearanceHeader.Options.UseTextOptions = true;
            this.col原因2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col原因2.Caption = "原 因";
            this.col原因2.FieldName = "原因";
            this.col原因2.Name = "col原因2";
            this.col原因2.OptionsColumn.ReadOnly = true;
            this.col原因2.Visible = true;
            this.col原因2.VisibleIndex = 2;
            // 
            // col医疗机构名称2
            // 
            this.col医疗机构名称2.AppearanceHeader.Options.UseTextOptions = true;
            this.col医疗机构名称2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col医疗机构名称2.Caption = "医疗机构名称 ";
            this.col医疗机构名称2.FieldName = "医疗机构名称 ";
            this.col医疗机构名称2.Name = "col医疗机构名称2";
            this.col医疗机构名称2.OptionsColumn.ReadOnly = true;
            this.col医疗机构名称2.Visible = true;
            this.col医疗机构名称2.VisibleIndex = 3;
            // 
            // col病案号2
            // 
            this.col病案号2.AppearanceHeader.Options.UseTextOptions = true;
            this.col病案号2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col病案号2.Caption = "病案号";
            this.col病案号2.FieldName = "病案号";
            this.col病案号2.Name = "col病案号2";
            this.col病案号2.OptionsColumn.ReadOnly = true;
            this.col病案号2.Visible = true;
            this.col病案号2.VisibleIndex = 4;
            // 
            // gc住院史
            // 
            this.gc住院史.Location = new System.Drawing.Point(181, 92);
            this.gc住院史.MainView = this.gv住院史;
            this.gc住院史.Name = "gc住院史";
            this.gc住院史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1});
            this.gc住院史.Size = new System.Drawing.Size(490, 56);
            this.gc住院史.TabIndex = 211;
            this.gc住院史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv住院史});
            // 
            // gv住院史
            // 
            this.gv住院史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col入院日期,
            this.col出院日期,
            this.col原因,
            this.col医疗机构名称,
            this.col病案号});
            this.gv住院史.GridControl = this.gc住院史;
            this.gv住院史.Name = "gv住院史";
            this.gv住院史.OptionsView.ShowGroupPanel = false;
            // 
            // col入院日期
            // 
            this.col入院日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col入院日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col入院日期.Caption = "入院日期";
            this.col入院日期.ColumnEdit = this.repositoryItemDateEdit1;
            this.col入院日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.col入院日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col入院日期.FieldName = "入院日期";
            this.col入院日期.Name = "col入院日期";
            this.col入院日期.OptionsColumn.ReadOnly = true;
            this.col入院日期.Visible = true;
            this.col入院日期.VisibleIndex = 0;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // col出院日期
            // 
            this.col出院日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col出院日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col出院日期.Caption = "出院日期";
            this.col出院日期.ColumnEdit = this.repositoryItemDateEdit1;
            this.col出院日期.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.col出院日期.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col出院日期.FieldName = "出院日期";
            this.col出院日期.Name = "col出院日期";
            this.col出院日期.OptionsColumn.ReadOnly = true;
            this.col出院日期.Visible = true;
            this.col出院日期.VisibleIndex = 1;
            // 
            // col原因
            // 
            this.col原因.AppearanceHeader.Options.UseTextOptions = true;
            this.col原因.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col原因.Caption = "原 因";
            this.col原因.FieldName = "原因";
            this.col原因.Name = "col原因";
            this.col原因.OptionsColumn.ReadOnly = true;
            this.col原因.Visible = true;
            this.col原因.VisibleIndex = 2;
            // 
            // col医疗机构名称
            // 
            this.col医疗机构名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col医疗机构名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col医疗机构名称.Caption = "医疗机构名称";
            this.col医疗机构名称.FieldName = "医疗机构名称";
            this.col医疗机构名称.Name = "col医疗机构名称";
            this.col医疗机构名称.OptionsColumn.ReadOnly = true;
            this.col医疗机构名称.Visible = true;
            this.col医疗机构名称.VisibleIndex = 3;
            // 
            // col病案号
            // 
            this.col病案号.AppearanceHeader.Options.UseTextOptions = true;
            this.col病案号.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col病案号.Caption = "病案号";
            this.col病案号.FieldName = "病案号";
            this.col病案号.Name = "col病案号";
            this.col病案号.OptionsColumn.ReadOnly = true;
            this.col病案号.Visible = true;
            this.col病案号.VisibleIndex = 4;
            // 
            // txt肾脏疾病
            // 
            this.txt肾脏疾病.Location = new System.Drawing.Point(181, -28);
            this.txt肾脏疾病.Name = "txt肾脏疾病";
            this.txt肾脏疾病.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt肾脏疾病.Properties.Appearance.Options.UseBackColor = true;
            this.txt肾脏疾病.Properties.ReadOnly = true;
            this.txt肾脏疾病.Size = new System.Drawing.Size(490, 20);
            this.txt肾脏疾病.StyleController = this.layoutControl1;
            this.txt肾脏疾病.TabIndex = 210;
            // 
            // txt心血管疾病
            // 
            this.txt心血管疾病.Location = new System.Drawing.Point(181, -4);
            this.txt心血管疾病.Name = "txt心血管疾病";
            this.txt心血管疾病.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt心血管疾病.Properties.Appearance.Options.UseBackColor = true;
            this.txt心血管疾病.Properties.ReadOnly = true;
            this.txt心血管疾病.Size = new System.Drawing.Size(490, 20);
            this.txt心血管疾病.StyleController = this.layoutControl1;
            this.txt心血管疾病.TabIndex = 209;
            // 
            // txt血管疾病
            // 
            this.txt血管疾病.Location = new System.Drawing.Point(181, 340);
            this.txt血管疾病.Name = "txt血管疾病";
            this.txt血管疾病.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血管疾病.Properties.Appearance.Options.UseBackColor = true;
            this.txt血管疾病.Properties.ReadOnly = true;
            this.txt血管疾病.Size = new System.Drawing.Size(490, 20);
            this.txt血管疾病.StyleController = this.layoutControl1;
            this.txt血管疾病.TabIndex = 208;
            // 
            // txt眼部疾病
            // 
            this.txt眼部疾病.Location = new System.Drawing.Point(181, 20);
            this.txt眼部疾病.Name = "txt眼部疾病";
            this.txt眼部疾病.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt眼部疾病.Properties.Appearance.Options.UseBackColor = true;
            this.txt眼部疾病.Properties.ReadOnly = true;
            this.txt眼部疾病.Size = new System.Drawing.Size(490, 20);
            this.txt眼部疾病.StyleController = this.layoutControl1;
            this.txt眼部疾病.TabIndex = 207;
            // 
            // txt神经系统疾病
            // 
            this.txt神经系统疾病.Location = new System.Drawing.Point(181, 44);
            this.txt神经系统疾病.Name = "txt神经系统疾病";
            this.txt神经系统疾病.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt神经系统疾病.Properties.Appearance.Options.UseBackColor = true;
            this.txt神经系统疾病.Properties.ReadOnly = true;
            this.txt神经系统疾病.Size = new System.Drawing.Size(490, 20);
            this.txt神经系统疾病.StyleController = this.layoutControl1;
            this.txt神经系统疾病.TabIndex = 206;
            // 
            // txt其他系统疾病
            // 
            this.txt其他系统疾病.Location = new System.Drawing.Point(181, 68);
            this.txt其他系统疾病.Name = "txt其他系统疾病";
            this.txt其他系统疾病.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt其他系统疾病.Properties.Appearance.Options.UseBackColor = true;
            this.txt其他系统疾病.Properties.ReadOnly = true;
            this.txt其他系统疾病.Size = new System.Drawing.Size(490, 20);
            this.txt其他系统疾病.StyleController = this.layoutControl1;
            this.txt其他系统疾病.TabIndex = 205;
            // 
            // txt脑血管疾病
            // 
            this.txt脑血管疾病.Location = new System.Drawing.Point(181, -52);
            this.txt脑血管疾病.Name = "txt脑血管疾病";
            this.txt脑血管疾病.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt脑血管疾病.Properties.Appearance.Options.UseBackColor = true;
            this.txt脑血管疾病.Properties.ReadOnly = true;
            this.txt脑血管疾病.Size = new System.Drawing.Size(490, 20);
            this.txt脑血管疾病.StyleController = this.layoutControl1;
            this.txt脑血管疾病.TabIndex = 204;
            // 
            // txt阳虚质
            // 
            this.txt阳虚质.Location = new System.Drawing.Point(181, -220);
            this.txt阳虚质.Name = "txt阳虚质";
            this.txt阳虚质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt阳虚质.Properties.Appearance.Options.UseBackColor = true;
            this.txt阳虚质.Properties.ReadOnly = true;
            this.txt阳虚质.Size = new System.Drawing.Size(490, 20);
            this.txt阳虚质.StyleController = this.layoutControl1;
            this.txt阳虚质.TabIndex = 203;
            // 
            // txt阴虚质
            // 
            this.txt阴虚质.Location = new System.Drawing.Point(181, -196);
            this.txt阴虚质.Name = "txt阴虚质";
            this.txt阴虚质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt阴虚质.Properties.Appearance.Options.UseBackColor = true;
            this.txt阴虚质.Properties.ReadOnly = true;
            this.txt阴虚质.Size = new System.Drawing.Size(490, 20);
            this.txt阴虚质.StyleController = this.layoutControl1;
            this.txt阴虚质.TabIndex = 202;
            // 
            // txt气虚质
            // 
            this.txt气虚质.Location = new System.Drawing.Point(181, -244);
            this.txt气虚质.Name = "txt气虚质";
            this.txt气虚质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt气虚质.Properties.Appearance.Options.UseBackColor = true;
            this.txt气虚质.Properties.ReadOnly = true;
            this.txt气虚质.Size = new System.Drawing.Size(490, 20);
            this.txt气虚质.StyleController = this.layoutControl1;
            this.txt气虚质.TabIndex = 201;
            // 
            // txt痰湿质
            // 
            this.txt痰湿质.Location = new System.Drawing.Point(181, -172);
            this.txt痰湿质.Name = "txt痰湿质";
            this.txt痰湿质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt痰湿质.Properties.Appearance.Options.UseBackColor = true;
            this.txt痰湿质.Properties.ReadOnly = true;
            this.txt痰湿质.Size = new System.Drawing.Size(490, 20);
            this.txt痰湿质.StyleController = this.layoutControl1;
            this.txt痰湿质.TabIndex = 200;
            // 
            // txt湿热质
            // 
            this.txt湿热质.Location = new System.Drawing.Point(181, -148);
            this.txt湿热质.Name = "txt湿热质";
            this.txt湿热质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt湿热质.Properties.Appearance.Options.UseBackColor = true;
            this.txt湿热质.Properties.ReadOnly = true;
            this.txt湿热质.Size = new System.Drawing.Size(490, 20);
            this.txt湿热质.StyleController = this.layoutControl1;
            this.txt湿热质.TabIndex = 199;
            // 
            // txt血瘀质
            // 
            this.txt血瘀质.Location = new System.Drawing.Point(181, -124);
            this.txt血瘀质.Name = "txt血瘀质";
            this.txt血瘀质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血瘀质.Properties.Appearance.Options.UseBackColor = true;
            this.txt血瘀质.Properties.ReadOnly = true;
            this.txt血瘀质.Size = new System.Drawing.Size(490, 20);
            this.txt血瘀质.StyleController = this.layoutControl1;
            this.txt血瘀质.TabIndex = 198;
            // 
            // txt气郁质
            // 
            this.txt气郁质.Location = new System.Drawing.Point(181, -100);
            this.txt气郁质.Name = "txt气郁质";
            this.txt气郁质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt气郁质.Properties.Appearance.Options.UseBackColor = true;
            this.txt气郁质.Properties.ReadOnly = true;
            this.txt气郁质.Size = new System.Drawing.Size(490, 20);
            this.txt气郁质.StyleController = this.layoutControl1;
            this.txt气郁质.TabIndex = 197;
            // 
            // txt特秉质
            // 
            this.txt特秉质.Location = new System.Drawing.Point(181, -76);
            this.txt特秉质.Name = "txt特秉质";
            this.txt特秉质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt特秉质.Properties.Appearance.Options.UseBackColor = true;
            this.txt特秉质.Properties.ReadOnly = true;
            this.txt特秉质.Size = new System.Drawing.Size(490, 20);
            this.txt特秉质.StyleController = this.layoutControl1;
            this.txt特秉质.TabIndex = 196;
            // 
            // txt平和质
            // 
            this.txt平和质.Location = new System.Drawing.Point(181, -268);
            this.txt平和质.Name = "txt平和质";
            this.txt平和质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt平和质.Properties.Appearance.Options.UseBackColor = true;
            this.txt平和质.Properties.ReadOnly = true;
            this.txt平和质.Size = new System.Drawing.Size(490, 20);
            this.txt平和质.StyleController = this.layoutControl1;
            this.txt平和质.TabIndex = 195;
            // 
            // txt腹部B超
            // 
            this.txt腹部B超.Location = new System.Drawing.Point(261, -366);
            this.txt腹部B超.Name = "txt腹部B超";
            this.txt腹部B超.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt腹部B超.Properties.Appearance.Options.UseBackColor = true;
            this.txt腹部B超.Properties.ReadOnly = true;
            this.txt腹部B超.Size = new System.Drawing.Size(410, 20);
            this.txt腹部B超.StyleController = this.layoutControl1;
            this.txt腹部B超.TabIndex = 194;
            // 
            // txt辅助检查其他
            // 
            this.txt辅助检查其他.Location = new System.Drawing.Point(181, -292);
            this.txt辅助检查其他.Name = "txt辅助检查其他";
            this.txt辅助检查其他.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt辅助检查其他.Properties.Appearance.Options.UseBackColor = true;
            this.txt辅助检查其他.Properties.ReadOnly = true;
            this.txt辅助检查其他.Size = new System.Drawing.Size(490, 20);
            this.txt辅助检查其他.StyleController = this.layoutControl1;
            this.txt辅助检查其他.TabIndex = 193;
            // 
            // txt胸部X线片
            // 
            this.txt胸部X线片.Location = new System.Drawing.Point(181, -390);
            this.txt胸部X线片.Name = "txt胸部X线片";
            this.txt胸部X线片.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt胸部X线片.Properties.Appearance.Options.UseBackColor = true;
            this.txt胸部X线片.Properties.ReadOnly = true;
            this.txt胸部X线片.Size = new System.Drawing.Size(490, 20);
            this.txt胸部X线片.StyleController = this.layoutControl1;
            this.txt胸部X线片.TabIndex = 192;
            // 
            // txt血清高密度脂蛋白胆固醇
            // 
            this.txt血清高密度脂蛋白胆固醇.Location = new System.Drawing.Point(333, -414);
            this.txt血清高密度脂蛋白胆固醇.Name = "txt血清高密度脂蛋白胆固醇";
            this.txt血清高密度脂蛋白胆固醇.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血清高密度脂蛋白胆固醇.Properties.Appearance.Options.UseBackColor = true;
            this.txt血清高密度脂蛋白胆固醇.Properties.ReadOnly = true;
            this.txt血清高密度脂蛋白胆固醇.Size = new System.Drawing.Size(338, 20);
            this.txt血清高密度脂蛋白胆固醇.StyleController = this.layoutControl1;
            this.txt血清高密度脂蛋白胆固醇.TabIndex = 191;
            // 
            // txt血清低密度脂蛋白胆固醇
            // 
            this.txt血清低密度脂蛋白胆固醇.Location = new System.Drawing.Point(333, -438);
            this.txt血清低密度脂蛋白胆固醇.Name = "txt血清低密度脂蛋白胆固醇";
            this.txt血清低密度脂蛋白胆固醇.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血清低密度脂蛋白胆固醇.Properties.Appearance.Options.UseBackColor = true;
            this.txt血清低密度脂蛋白胆固醇.Properties.ReadOnly = true;
            this.txt血清低密度脂蛋白胆固醇.Size = new System.Drawing.Size(338, 20);
            this.txt血清低密度脂蛋白胆固醇.StyleController = this.layoutControl1;
            this.txt血清低密度脂蛋白胆固醇.TabIndex = 190;
            // 
            // txt甘油三酯
            // 
            this.txt甘油三酯.Location = new System.Drawing.Point(333, -462);
            this.txt甘油三酯.Name = "txt甘油三酯";
            this.txt甘油三酯.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt甘油三酯.Properties.Appearance.Options.UseBackColor = true;
            this.txt甘油三酯.Properties.ReadOnly = true;
            this.txt甘油三酯.Size = new System.Drawing.Size(338, 20);
            this.txt甘油三酯.StyleController = this.layoutControl1;
            this.txt甘油三酯.TabIndex = 189;
            // 
            // txt总胆固醇
            // 
            this.txt总胆固醇.Location = new System.Drawing.Point(333, -486);
            this.txt总胆固醇.Name = "txt总胆固醇";
            this.txt总胆固醇.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt总胆固醇.Properties.Appearance.Options.UseBackColor = true;
            this.txt总胆固醇.Properties.ReadOnly = true;
            this.txt总胆固醇.Size = new System.Drawing.Size(338, 20);
            this.txt总胆固醇.StyleController = this.layoutControl1;
            this.txt总胆固醇.TabIndex = 188;
            // 
            // txt血钾浓度
            // 
            this.txt血钾浓度.Location = new System.Drawing.Point(333, -534);
            this.txt血钾浓度.Name = "txt血钾浓度";
            this.txt血钾浓度.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血钾浓度.Properties.Appearance.Options.UseBackColor = true;
            this.txt血钾浓度.Properties.ReadOnly = true;
            this.txt血钾浓度.Size = new System.Drawing.Size(338, 20);
            this.txt血钾浓度.StyleController = this.layoutControl1;
            this.txt血钾浓度.TabIndex = 187;
            // 
            // txt血尿素氮
            // 
            this.txt血尿素氮.Location = new System.Drawing.Point(333, -558);
            this.txt血尿素氮.Name = "txt血尿素氮";
            this.txt血尿素氮.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血尿素氮.Properties.Appearance.Options.UseBackColor = true;
            this.txt血尿素氮.Properties.ReadOnly = true;
            this.txt血尿素氮.Size = new System.Drawing.Size(338, 20);
            this.txt血尿素氮.StyleController = this.layoutControl1;
            this.txt血尿素氮.TabIndex = 186;
            // 
            // txt血钠浓度
            // 
            this.txt血钠浓度.Location = new System.Drawing.Point(333, -510);
            this.txt血钠浓度.Name = "txt血钠浓度";
            this.txt血钠浓度.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血钠浓度.Properties.Appearance.Options.UseBackColor = true;
            this.txt血钠浓度.Properties.ReadOnly = true;
            this.txt血钠浓度.Size = new System.Drawing.Size(338, 20);
            this.txt血钠浓度.StyleController = this.layoutControl1;
            this.txt血钠浓度.TabIndex = 185;
            // 
            // txt血清肌酐
            // 
            this.txt血清肌酐.Location = new System.Drawing.Point(333, -582);
            this.txt血清肌酐.Name = "txt血清肌酐";
            this.txt血清肌酐.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血清肌酐.Properties.Appearance.Options.UseBackColor = true;
            this.txt血清肌酐.Properties.ReadOnly = true;
            this.txt血清肌酐.Size = new System.Drawing.Size(338, 20);
            this.txt血清肌酐.StyleController = this.layoutControl1;
            this.txt血清肌酐.TabIndex = 184;
            // 
            // txt白蛋白
            // 
            this.txt白蛋白.Location = new System.Drawing.Point(333, -654);
            this.txt白蛋白.Name = "txt白蛋白";
            this.txt白蛋白.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt白蛋白.Properties.Appearance.Options.UseBackColor = true;
            this.txt白蛋白.Properties.ReadOnly = true;
            this.txt白蛋白.Size = new System.Drawing.Size(338, 20);
            this.txt白蛋白.StyleController = this.layoutControl1;
            this.txt白蛋白.TabIndex = 183;
            // 
            // txt总胆红素
            // 
            this.txt总胆红素.Location = new System.Drawing.Point(333, -630);
            this.txt总胆红素.Name = "txt总胆红素";
            this.txt总胆红素.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt总胆红素.Properties.Appearance.Options.UseBackColor = true;
            this.txt总胆红素.Properties.ReadOnly = true;
            this.txt总胆红素.Size = new System.Drawing.Size(338, 20);
            this.txt总胆红素.StyleController = this.layoutControl1;
            this.txt总胆红素.TabIndex = 182;
            // 
            // txt血清谷草转氨酶
            // 
            this.txt血清谷草转氨酶.Location = new System.Drawing.Point(333, -678);
            this.txt血清谷草转氨酶.Name = "txt血清谷草转氨酶";
            this.txt血清谷草转氨酶.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血清谷草转氨酶.Properties.Appearance.Options.UseBackColor = true;
            this.txt血清谷草转氨酶.Properties.ReadOnly = true;
            this.txt血清谷草转氨酶.Size = new System.Drawing.Size(338, 20);
            this.txt血清谷草转氨酶.StyleController = this.layoutControl1;
            this.txt血清谷草转氨酶.TabIndex = 181;
            // 
            // txt结合胆红素
            // 
            this.txt结合胆红素.Location = new System.Drawing.Point(333, -606);
            this.txt结合胆红素.Name = "txt结合胆红素";
            this.txt结合胆红素.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt结合胆红素.Properties.Appearance.Options.UseBackColor = true;
            this.txt结合胆红素.Properties.ReadOnly = true;
            this.txt结合胆红素.Size = new System.Drawing.Size(338, 20);
            this.txt结合胆红素.StyleController = this.layoutControl1;
            this.txt结合胆红素.TabIndex = 180;
            // 
            // txt血清谷丙转氨酶
            // 
            this.txt血清谷丙转氨酶.Location = new System.Drawing.Point(333, -702);
            this.txt血清谷丙转氨酶.Name = "txt血清谷丙转氨酶";
            this.txt血清谷丙转氨酶.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血清谷丙转氨酶.Properties.Appearance.Options.UseBackColor = true;
            this.txt血清谷丙转氨酶.Properties.ReadOnly = true;
            this.txt血清谷丙转氨酶.Size = new System.Drawing.Size(338, 20);
            this.txt血清谷丙转氨酶.StyleController = this.layoutControl1;
            this.txt血清谷丙转氨酶.TabIndex = 179;
            // 
            // txt糖化血红蛋白
            // 
            this.txt糖化血红蛋白.Location = new System.Drawing.Point(181, -766);
            this.txt糖化血红蛋白.Name = "txt糖化血红蛋白";
            this.txt糖化血红蛋白.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt糖化血红蛋白.Properties.Appearance.Options.UseBackColor = true;
            this.txt糖化血红蛋白.Properties.ReadOnly = true;
            this.txt糖化血红蛋白.Size = new System.Drawing.Size(490, 20);
            this.txt糖化血红蛋白.StyleController = this.layoutControl1;
            this.txt糖化血红蛋白.TabIndex = 178;
            // 
            // txt大便潜血
            // 
            this.txt大便潜血.Location = new System.Drawing.Point(181, -790);
            this.txt大便潜血.Name = "txt大便潜血";
            this.txt大便潜血.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt大便潜血.Properties.Appearance.Options.UseBackColor = true;
            this.txt大便潜血.Properties.ReadOnly = true;
            this.txt大便潜血.Size = new System.Drawing.Size(490, 20);
            this.txt大便潜血.StyleController = this.layoutControl1;
            this.txt大便潜血.TabIndex = 177;
            // 
            // txt尿微量白蛋白
            // 
            this.txt尿微量白蛋白.Location = new System.Drawing.Point(181, -814);
            this.txt尿微量白蛋白.Name = "txt尿微量白蛋白";
            this.txt尿微量白蛋白.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt尿微量白蛋白.Properties.Appearance.Options.UseBackColor = true;
            this.txt尿微量白蛋白.Properties.ReadOnly = true;
            this.txt尿微量白蛋白.Size = new System.Drawing.Size(490, 20);
            this.txt尿微量白蛋白.StyleController = this.layoutControl1;
            this.txt尿微量白蛋白.TabIndex = 175;
            // 
            // txt心电图
            // 
            this.txt心电图.Location = new System.Drawing.Point(181, -838);
            this.txt心电图.Name = "txt心电图";
            this.txt心电图.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt心电图.Properties.Appearance.Options.UseBackColor = true;
            this.txt心电图.Properties.ReadOnly = true;
            this.txt心电图.Size = new System.Drawing.Size(490, 20);
            this.txt心电图.StyleController = this.layoutControl1;
            this.txt心电图.TabIndex = 174;
            // 
            // txt空腹血糖
            // 
            this.txt空腹血糖.Location = new System.Drawing.Point(181, -886);
            this.txt空腹血糖.Name = "txt空腹血糖";
            this.txt空腹血糖.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt空腹血糖.Properties.Appearance.Options.UseBackColor = true;
            this.txt空腹血糖.Properties.ReadOnly = true;
            this.txt空腹血糖.Size = new System.Drawing.Size(196, 20);
            this.txt空腹血糖.StyleController = this.layoutControl1;
            this.txt空腹血糖.TabIndex = 172;
            // 
            // txt尿常规
            // 
            this.txt尿常规.Location = new System.Drawing.Point(181, -910);
            this.txt尿常规.Name = "txt尿常规";
            this.txt尿常规.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt尿常规.Properties.Appearance.Options.UseBackColor = true;
            this.txt尿常规.Properties.ReadOnly = true;
            this.txt尿常规.Size = new System.Drawing.Size(490, 20);
            this.txt尿常规.StyleController = this.layoutControl1;
            this.txt尿常规.TabIndex = 171;
            // 
            // txt血小板
            // 
            this.txt血小板.Location = new System.Drawing.Point(293, -958);
            this.txt血小板.Name = "txt血小板";
            this.txt血小板.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血小板.Properties.Appearance.Options.UseBackColor = true;
            this.txt血小板.Properties.ReadOnly = true;
            this.txt血小板.Size = new System.Drawing.Size(378, 20);
            this.txt血小板.StyleController = this.layoutControl1;
            this.txt血小板.TabIndex = 170;
            // 
            // txt白细胞
            // 
            this.txt白细胞.Location = new System.Drawing.Point(293, -982);
            this.txt白细胞.Name = "txt白细胞";
            this.txt白细胞.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt白细胞.Properties.Appearance.Options.UseBackColor = true;
            this.txt白细胞.Properties.ReadOnly = true;
            this.txt白细胞.Size = new System.Drawing.Size(378, 20);
            this.txt白细胞.StyleController = this.layoutControl1;
            this.txt白细胞.TabIndex = 169;
            // 
            // txt血常规其他
            // 
            this.txt血常规其他.Location = new System.Drawing.Point(293, -934);
            this.txt血常规其他.Name = "txt血常规其他";
            this.txt血常规其他.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血常规其他.Properties.Appearance.Options.UseBackColor = true;
            this.txt血常规其他.Properties.ReadOnly = true;
            this.txt血常规其他.Size = new System.Drawing.Size(378, 20);
            this.txt血常规其他.StyleController = this.layoutControl1;
            this.txt血常规其他.TabIndex = 168;
            // 
            // txt血红蛋白
            // 
            this.txt血红蛋白.Location = new System.Drawing.Point(293, -1006);
            this.txt血红蛋白.Name = "txt血红蛋白";
            this.txt血红蛋白.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血红蛋白.Properties.Appearance.Options.UseBackColor = true;
            this.txt血红蛋白.Properties.ReadOnly = true;
            this.txt血红蛋白.Size = new System.Drawing.Size(378, 20);
            this.txt血红蛋白.StyleController = this.layoutControl1;
            this.txt血红蛋白.TabIndex = 167;
            // 
            // txt乳腺
            // 
            this.txt乳腺.Location = new System.Drawing.Point(181, -1199);
            this.txt乳腺.Name = "txt乳腺";
            this.txt乳腺.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt乳腺.Properties.Appearance.Options.UseBackColor = true;
            this.txt乳腺.Properties.ReadOnly = true;
            this.txt乳腺.Size = new System.Drawing.Size(490, 20);
            this.txt乳腺.StyleController = this.layoutControl1;
            this.txt乳腺.TabIndex = 166;
            // 
            // txt肛门指诊
            // 
            this.txt肛门指诊.Location = new System.Drawing.Point(181, -1223);
            this.txt肛门指诊.Name = "txt肛门指诊";
            this.txt肛门指诊.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt肛门指诊.Properties.Appearance.Options.UseBackColor = true;
            this.txt肛门指诊.Properties.ReadOnly = true;
            this.txt肛门指诊.Size = new System.Drawing.Size(490, 20);
            this.txt肛门指诊.StyleController = this.layoutControl1;
            this.txt肛门指诊.TabIndex = 165;
            // 
            // txt足背动脉搏动
            // 
            this.txt足背动脉搏动.Location = new System.Drawing.Point(181, -1247);
            this.txt足背动脉搏动.Name = "txt足背动脉搏动";
            this.txt足背动脉搏动.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt足背动脉搏动.Properties.Appearance.Options.UseBackColor = true;
            this.txt足背动脉搏动.Properties.ReadOnly = true;
            this.txt足背动脉搏动.Size = new System.Drawing.Size(490, 20);
            this.txt足背动脉搏动.StyleController = this.layoutControl1;
            this.txt足背动脉搏动.TabIndex = 164;
            // 
            // txt查体其他
            // 
            this.txt查体其他.Location = new System.Drawing.Point(181, -1055);
            this.txt查体其他.Name = "txt查体其他";
            this.txt查体其他.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt查体其他.Properties.Appearance.Options.UseBackColor = true;
            this.txt查体其他.Properties.ReadOnly = true;
            this.txt查体其他.Size = new System.Drawing.Size(490, 20);
            this.txt查体其他.StyleController = this.layoutControl1;
            this.txt查体其他.TabIndex = 163;
            // 
            // txt下肢水肿
            // 
            this.txt下肢水肿.Location = new System.Drawing.Point(181, -1271);
            this.txt下肢水肿.Name = "txt下肢水肿";
            this.txt下肢水肿.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt下肢水肿.Properties.Appearance.Options.UseBackColor = true;
            this.txt下肢水肿.Properties.ReadOnly = true;
            this.txt下肢水肿.Size = new System.Drawing.Size(490, 20);
            this.txt下肢水肿.StyleController = this.layoutControl1;
            this.txt下肢水肿.TabIndex = 162;
            // 
            // txt脾大
            // 
            this.txt脾大.Location = new System.Drawing.Point(293, -1319);
            this.txt脾大.Name = "txt脾大";
            this.txt脾大.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt脾大.Properties.Appearance.Options.UseBackColor = true;
            this.txt脾大.Properties.ReadOnly = true;
            this.txt脾大.Size = new System.Drawing.Size(378, 20);
            this.txt脾大.StyleController = this.layoutControl1;
            this.txt脾大.TabIndex = 161;
            // 
            // txt肝大
            // 
            this.txt肝大.Location = new System.Drawing.Point(293, -1343);
            this.txt肝大.Name = "txt肝大";
            this.txt肝大.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt肝大.Properties.Appearance.Options.UseBackColor = true;
            this.txt肝大.Properties.ReadOnly = true;
            this.txt肝大.Size = new System.Drawing.Size(378, 20);
            this.txt肝大.StyleController = this.layoutControl1;
            this.txt肝大.TabIndex = 160;
            // 
            // txt包块
            // 
            this.txt包块.Location = new System.Drawing.Point(293, -1367);
            this.txt包块.Name = "txt包块";
            this.txt包块.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt包块.Properties.Appearance.Options.UseBackColor = true;
            this.txt包块.Properties.ReadOnly = true;
            this.txt包块.Size = new System.Drawing.Size(378, 20);
            this.txt包块.StyleController = this.layoutControl1;
            this.txt包块.TabIndex = 159;
            // 
            // txt移动性浊音
            // 
            this.txt移动性浊音.Location = new System.Drawing.Point(293, -1295);
            this.txt移动性浊音.Name = "txt移动性浊音";
            this.txt移动性浊音.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt移动性浊音.Properties.Appearance.Options.UseBackColor = true;
            this.txt移动性浊音.Properties.ReadOnly = true;
            this.txt移动性浊音.Size = new System.Drawing.Size(378, 20);
            this.txt移动性浊音.StyleController = this.layoutControl1;
            this.txt移动性浊音.TabIndex = 158;
            // 
            // txt压痛
            // 
            this.txt压痛.Location = new System.Drawing.Point(293, -1391);
            this.txt压痛.Name = "txt压痛";
            this.txt压痛.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt压痛.Properties.Appearance.Options.UseBackColor = true;
            this.txt压痛.Properties.ReadOnly = true;
            this.txt压痛.Size = new System.Drawing.Size(378, 20);
            this.txt压痛.StyleController = this.layoutControl1;
            this.txt压痛.TabIndex = 157;
            // 
            // txt杂音
            // 
            this.txt杂音.Location = new System.Drawing.Point(293, -1415);
            this.txt杂音.Name = "txt杂音";
            this.txt杂音.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt杂音.Properties.Appearance.Options.UseBackColor = true;
            this.txt杂音.Properties.ReadOnly = true;
            this.txt杂音.Size = new System.Drawing.Size(378, 20);
            this.txt杂音.StyleController = this.layoutControl1;
            this.txt杂音.TabIndex = 156;
            // 
            // txt心律
            // 
            this.txt心律.Location = new System.Drawing.Point(293, -1439);
            this.txt心律.Name = "txt心律";
            this.txt心律.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt心律.Properties.Appearance.Options.UseBackColor = true;
            this.txt心律.Properties.ReadOnly = true;
            this.txt心律.Size = new System.Drawing.Size(378, 20);
            this.txt心律.StyleController = this.layoutControl1;
            this.txt心律.TabIndex = 155;
            // 
            // txt心率
            // 
            this.txt心率.Location = new System.Drawing.Point(293, -1463);
            this.txt心率.Name = "txt心率";
            this.txt心率.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt心率.Properties.Appearance.Options.UseBackColor = true;
            this.txt心率.Properties.ReadOnly = true;
            this.txt心率.Size = new System.Drawing.Size(378, 20);
            this.txt心率.StyleController = this.layoutControl1;
            this.txt心率.TabIndex = 154;
            // 
            // txt呼吸音
            // 
            this.txt呼吸音.Location = new System.Drawing.Point(293, -1511);
            this.txt呼吸音.Name = "txt呼吸音";
            this.txt呼吸音.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt呼吸音.Properties.Appearance.Options.UseBackColor = true;
            this.txt呼吸音.Properties.ReadOnly = true;
            this.txt呼吸音.Size = new System.Drawing.Size(378, 20);
            this.txt呼吸音.StyleController = this.layoutControl1;
            this.txt呼吸音.TabIndex = 153;
            // 
            // txt罗音
            // 
            this.txt罗音.Location = new System.Drawing.Point(293, -1487);
            this.txt罗音.Name = "txt罗音";
            this.txt罗音.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt罗音.Properties.Appearance.Options.UseBackColor = true;
            this.txt罗音.Properties.ReadOnly = true;
            this.txt罗音.Size = new System.Drawing.Size(378, 20);
            this.txt罗音.StyleController = this.layoutControl1;
            this.txt罗音.TabIndex = 152;
            // 
            // txt桶状胸
            // 
            this.txt桶状胸.Location = new System.Drawing.Point(293, -1535);
            this.txt桶状胸.Name = "txt桶状胸";
            this.txt桶状胸.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt桶状胸.Properties.Appearance.Options.UseBackColor = true;
            this.txt桶状胸.Properties.ReadOnly = true;
            this.txt桶状胸.Size = new System.Drawing.Size(378, 20);
            this.txt桶状胸.StyleController = this.layoutControl1;
            this.txt桶状胸.TabIndex = 151;
            // 
            // txt淋巴结
            // 
            this.txt淋巴结.Location = new System.Drawing.Point(181, -1559);
            this.txt淋巴结.Name = "txt淋巴结";
            this.txt淋巴结.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt淋巴结.Properties.Appearance.Options.UseBackColor = true;
            this.txt淋巴结.Properties.ReadOnly = true;
            this.txt淋巴结.Size = new System.Drawing.Size(490, 20);
            this.txt淋巴结.StyleController = this.layoutControl1;
            this.txt淋巴结.TabIndex = 150;
            // 
            // txt巩膜
            // 
            this.txt巩膜.Location = new System.Drawing.Point(181, -1583);
            this.txt巩膜.Name = "txt巩膜";
            this.txt巩膜.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt巩膜.Properties.Appearance.Options.UseBackColor = true;
            this.txt巩膜.Properties.ReadOnly = true;
            this.txt巩膜.Size = new System.Drawing.Size(490, 20);
            this.txt巩膜.StyleController = this.layoutControl1;
            this.txt巩膜.TabIndex = 149;
            // 
            // txt皮肤
            // 
            this.txt皮肤.Location = new System.Drawing.Point(181, -1607);
            this.txt皮肤.Name = "txt皮肤";
            this.txt皮肤.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt皮肤.Properties.Appearance.Options.UseBackColor = true;
            this.txt皮肤.Properties.ReadOnly = true;
            this.txt皮肤.Size = new System.Drawing.Size(490, 20);
            this.txt皮肤.StyleController = this.layoutControl1;
            this.txt皮肤.TabIndex = 148;
            // 
            // txt眼底
            // 
            this.txt眼底.Location = new System.Drawing.Point(181, -1631);
            this.txt眼底.Name = "txt眼底";
            this.txt眼底.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt眼底.Properties.Appearance.Options.UseBackColor = true;
            this.txt眼底.Properties.ReadOnly = true;
            this.txt眼底.Size = new System.Drawing.Size(490, 20);
            this.txt眼底.StyleController = this.layoutControl1;
            this.txt眼底.TabIndex = 147;
            // 
            // txt运动功能
            // 
            this.txt运动功能.Location = new System.Drawing.Point(181, -1655);
            this.txt运动功能.Name = "txt运动功能";
            this.txt运动功能.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt运动功能.Properties.Appearance.Options.UseBackColor = true;
            this.txt运动功能.Properties.ReadOnly = true;
            this.txt运动功能.Size = new System.Drawing.Size(490, 20);
            this.txt运动功能.StyleController = this.layoutControl1;
            this.txt运动功能.TabIndex = 146;
            // 
            // txt听力
            // 
            this.txt听力.Location = new System.Drawing.Point(181, -1679);
            this.txt听力.Name = "txt听力";
            this.txt听力.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt听力.Properties.Appearance.Options.UseBackColor = true;
            this.txt听力.Properties.ReadOnly = true;
            this.txt听力.Size = new System.Drawing.Size(490, 20);
            this.txt听力.StyleController = this.layoutControl1;
            this.txt听力.TabIndex = 145;
            // 
            // txt视力
            // 
            this.txt视力.Location = new System.Drawing.Point(181, -1703);
            this.txt视力.Name = "txt视力";
            this.txt视力.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt视力.Properties.Appearance.Options.UseBackColor = true;
            this.txt视力.Properties.ReadOnly = true;
            this.txt视力.Size = new System.Drawing.Size(490, 20);
            this.txt视力.StyleController = this.layoutControl1;
            this.txt视力.TabIndex = 144;
            // 
            // txt咽部
            // 
            this.txt咽部.Location = new System.Drawing.Point(263, -1727);
            this.txt咽部.Name = "txt咽部";
            this.txt咽部.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt咽部.Properties.Appearance.Options.UseBackColor = true;
            this.txt咽部.Properties.ReadOnly = true;
            this.txt咽部.Size = new System.Drawing.Size(408, 20);
            this.txt咽部.StyleController = this.layoutControl1;
            this.txt咽部.TabIndex = 142;
            // 
            // txt口唇
            // 
            this.txt口唇.Location = new System.Drawing.Point(263, -1977);
            this.txt口唇.Name = "txt口唇";
            this.txt口唇.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt口唇.Properties.Appearance.Options.UseBackColor = true;
            this.txt口唇.Properties.ReadOnly = true;
            this.txt口唇.Size = new System.Drawing.Size(408, 20);
            this.txt口唇.StyleController = this.layoutControl1;
            this.txt口唇.TabIndex = 141;
            // 
            // lbl工种及从业时间
            // 
            this.lbl工种及从业时间.Location = new System.Drawing.Point(238, -2133);
            this.lbl工种及从业时间.Name = "lbl工种及从业时间";
            this.lbl工种及从业时间.Size = new System.Drawing.Size(433, 14);
            this.lbl工种及从业时间.StyleController = this.layoutControl1;
            this.lbl工种及从业时间.TabIndex = 140;
            // 
            // lbl毒物种类
            // 
            this.lbl毒物种类.Location = new System.Drawing.Point(188, -2115);
            this.lbl毒物种类.Name = "lbl毒物种类";
            this.lbl毒物种类.Size = new System.Drawing.Size(48, 14);
            this.lbl毒物种类.StyleController = this.layoutControl1;
            this.lbl毒物种类.TabIndex = 139;
            this.lbl毒物种类.Text = "毒物种类";
            // 
            // lbl职业病有无
            // 
            this.lbl职业病有无.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl职业病有无.Location = new System.Drawing.Point(188, -2133);
            this.lbl职业病有无.Name = "lbl职业病有无";
            this.lbl职业病有无.Size = new System.Drawing.Size(46, 14);
            this.lbl职业病有无.StyleController = this.layoutControl1;
            this.lbl职业病有无.TabIndex = 138;
            this.lbl职业病有无.Text = "有";
            // 
            // txt职业病其他防护措施
            // 
            this.txt职业病其他防护措施.Location = new System.Drawing.Point(396, -2001);
            this.txt职业病其他防护措施.Name = "txt职业病其他防护措施";
            this.txt职业病其他防护措施.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业病其他防护措施.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业病其他防护措施.Properties.ReadOnly = true;
            this.txt职业病其他防护措施.Size = new System.Drawing.Size(275, 20);
            this.txt职业病其他防护措施.StyleController = this.layoutControl1;
            this.txt职业病其他防护措施.TabIndex = 137;
            // 
            // txt职业病其他
            // 
            this.txt职业病其他.Location = new System.Drawing.Point(263, -2001);
            this.txt职业病其他.Name = "txt职业病其他";
            this.txt职业病其他.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业病其他.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业病其他.Properties.ReadOnly = true;
            this.txt职业病其他.Size = new System.Drawing.Size(54, 20);
            this.txt职业病其他.StyleController = this.layoutControl1;
            this.txt职业病其他.TabIndex = 136;
            // 
            // txt化学物质防护措施
            // 
            this.txt化学物质防护措施.Location = new System.Drawing.Point(396, -2025);
            this.txt化学物质防护措施.Name = "txt化学物质防护措施";
            this.txt化学物质防护措施.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt化学物质防护措施.Properties.Appearance.Options.UseBackColor = true;
            this.txt化学物质防护措施.Properties.ReadOnly = true;
            this.txt化学物质防护措施.Size = new System.Drawing.Size(275, 20);
            this.txt化学物质防护措施.StyleController = this.layoutControl1;
            this.txt化学物质防护措施.TabIndex = 135;
            // 
            // txt化学物质
            // 
            this.txt化学物质.Location = new System.Drawing.Point(263, -2025);
            this.txt化学物质.Name = "txt化学物质";
            this.txt化学物质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt化学物质.Properties.Appearance.Options.UseBackColor = true;
            this.txt化学物质.Properties.ReadOnly = true;
            this.txt化学物质.Size = new System.Drawing.Size(54, 20);
            this.txt化学物质.StyleController = this.layoutControl1;
            this.txt化学物质.TabIndex = 134;
            // 
            // txt物理因素防护措施
            // 
            this.txt物理因素防护措施.Location = new System.Drawing.Point(396, -2049);
            this.txt物理因素防护措施.Name = "txt物理因素防护措施";
            this.txt物理因素防护措施.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt物理因素防护措施.Properties.Appearance.Options.UseBackColor = true;
            this.txt物理因素防护措施.Properties.ReadOnly = true;
            this.txt物理因素防护措施.Size = new System.Drawing.Size(275, 20);
            this.txt物理因素防护措施.StyleController = this.layoutControl1;
            this.txt物理因素防护措施.TabIndex = 133;
            // 
            // txt物理因素
            // 
            this.txt物理因素.Location = new System.Drawing.Point(263, -2049);
            this.txt物理因素.Name = "txt物理因素";
            this.txt物理因素.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt物理因素.Properties.Appearance.Options.UseBackColor = true;
            this.txt物理因素.Properties.ReadOnly = true;
            this.txt物理因素.Size = new System.Drawing.Size(54, 20);
            this.txt物理因素.StyleController = this.layoutControl1;
            this.txt物理因素.TabIndex = 132;
            // 
            // txt放射物质防护措施
            // 
            this.txt放射物质防护措施.Location = new System.Drawing.Point(396, -2073);
            this.txt放射物质防护措施.Name = "txt放射物质防护措施";
            this.txt放射物质防护措施.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt放射物质防护措施.Properties.Appearance.Options.UseBackColor = true;
            this.txt放射物质防护措施.Properties.ReadOnly = true;
            this.txt放射物质防护措施.Size = new System.Drawing.Size(275, 20);
            this.txt放射物质防护措施.StyleController = this.layoutControl1;
            this.txt放射物质防护措施.TabIndex = 131;
            // 
            // txt放射物质
            // 
            this.txt放射物质.Location = new System.Drawing.Point(263, -2073);
            this.txt放射物质.Name = "txt放射物质";
            this.txt放射物质.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt放射物质.Properties.Appearance.Options.UseBackColor = true;
            this.txt放射物质.Properties.ReadOnly = true;
            this.txt放射物质.Size = new System.Drawing.Size(54, 20);
            this.txt放射物质.StyleController = this.layoutControl1;
            this.txt放射物质.TabIndex = 130;
            // 
            // txt粉尘防护措施
            // 
            this.txt粉尘防护措施.Location = new System.Drawing.Point(396, -2097);
            this.txt粉尘防护措施.Name = "txt粉尘防护措施";
            this.txt粉尘防护措施.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt粉尘防护措施.Properties.Appearance.Options.UseBackColor = true;
            this.txt粉尘防护措施.Properties.ReadOnly = true;
            this.txt粉尘防护措施.Size = new System.Drawing.Size(275, 20);
            this.txt粉尘防护措施.StyleController = this.layoutControl1;
            this.txt粉尘防护措施.TabIndex = 129;
            // 
            // txt粉尘
            // 
            this.txt粉尘.Location = new System.Drawing.Point(263, -2097);
            this.txt粉尘.Name = "txt粉尘";
            this.txt粉尘.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt粉尘.Properties.Appearance.Options.UseBackColor = true;
            this.txt粉尘.Properties.ReadOnly = true;
            this.txt粉尘.Size = new System.Drawing.Size(54, 20);
            this.txt粉尘.StyleController = this.layoutControl1;
            this.txt粉尘.TabIndex = 128;
            // 
            // txt饮酒种类
            // 
            this.txt饮酒种类.Location = new System.Drawing.Point(293, -2157);
            this.txt饮酒种类.Name = "txt饮酒种类";
            this.txt饮酒种类.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt饮酒种类.Properties.Appearance.Options.UseBackColor = true;
            this.txt饮酒种类.Properties.ReadOnly = true;
            this.txt饮酒种类.Size = new System.Drawing.Size(378, 20);
            this.txt饮酒种类.StyleController = this.layoutControl1;
            this.txt饮酒种类.TabIndex = 127;
            // 
            // txt近一年内是否曾醉酒
            // 
            this.txt近一年内是否曾醉酒.Location = new System.Drawing.Point(478, -2181);
            this.txt近一年内是否曾醉酒.Name = "txt近一年内是否曾醉酒";
            this.txt近一年内是否曾醉酒.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt近一年内是否曾醉酒.Properties.Appearance.Options.UseBackColor = true;
            this.txt近一年内是否曾醉酒.Properties.ReadOnly = true;
            this.txt近一年内是否曾醉酒.Size = new System.Drawing.Size(193, 20);
            this.txt近一年内是否曾醉酒.StyleController = this.layoutControl1;
            this.txt近一年内是否曾醉酒.TabIndex = 126;
            // 
            // txt开始饮酒年龄
            // 
            this.txt开始饮酒年龄.Location = new System.Drawing.Point(293, -2181);
            this.txt开始饮酒年龄.Name = "txt开始饮酒年龄";
            this.txt开始饮酒年龄.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt开始饮酒年龄.Properties.Appearance.Options.UseBackColor = true;
            this.txt开始饮酒年龄.Properties.ReadOnly = true;
            this.txt开始饮酒年龄.Size = new System.Drawing.Size(56, 20);
            this.txt开始饮酒年龄.StyleController = this.layoutControl1;
            this.txt开始饮酒年龄.TabIndex = 125;
            // 
            // txt戒酒年龄
            // 
            this.txt戒酒年龄.Location = new System.Drawing.Point(478, -2205);
            this.txt戒酒年龄.Name = "txt戒酒年龄";
            this.txt戒酒年龄.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt戒酒年龄.Properties.Appearance.Options.UseBackColor = true;
            this.txt戒酒年龄.Properties.ReadOnly = true;
            this.txt戒酒年龄.Size = new System.Drawing.Size(193, 20);
            this.txt戒酒年龄.StyleController = this.layoutControl1;
            this.txt戒酒年龄.TabIndex = 124;
            // 
            // txt是否戒酒
            // 
            this.txt是否戒酒.Location = new System.Drawing.Point(293, -2205);
            this.txt是否戒酒.Name = "txt是否戒酒";
            this.txt是否戒酒.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt是否戒酒.Properties.Appearance.Options.UseBackColor = true;
            this.txt是否戒酒.Properties.ReadOnly = true;
            this.txt是否戒酒.Size = new System.Drawing.Size(56, 20);
            this.txt是否戒酒.StyleController = this.layoutControl1;
            this.txt是否戒酒.TabIndex = 123;
            // 
            // txt日饮酒量
            // 
            this.txt日饮酒量.Location = new System.Drawing.Point(293, -2229);
            this.txt日饮酒量.Name = "txt日饮酒量";
            this.txt日饮酒量.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt日饮酒量.Properties.Appearance.Options.UseBackColor = true;
            this.txt日饮酒量.Properties.ReadOnly = true;
            this.txt日饮酒量.Size = new System.Drawing.Size(378, 20);
            this.txt日饮酒量.StyleController = this.layoutControl1;
            this.txt日饮酒量.TabIndex = 122;
            // 
            // txt饮酒频率
            // 
            this.txt饮酒频率.Location = new System.Drawing.Point(293, -2253);
            this.txt饮酒频率.Name = "txt饮酒频率";
            this.txt饮酒频率.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt饮酒频率.Properties.Appearance.Options.UseBackColor = true;
            this.txt饮酒频率.Properties.ReadOnly = true;
            this.txt饮酒频率.Size = new System.Drawing.Size(378, 20);
            this.txt饮酒频率.StyleController = this.layoutControl1;
            this.txt饮酒频率.TabIndex = 121;
            // 
            // txt戒烟年龄
            // 
            this.txt戒烟年龄.Location = new System.Drawing.Point(478, -2277);
            this.txt戒烟年龄.Name = "txt戒烟年龄";
            this.txt戒烟年龄.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt戒烟年龄.Properties.Appearance.Options.UseBackColor = true;
            this.txt戒烟年龄.Properties.ReadOnly = true;
            this.txt戒烟年龄.Size = new System.Drawing.Size(193, 20);
            this.txt戒烟年龄.StyleController = this.layoutControl1;
            this.txt戒烟年龄.TabIndex = 120;
            // 
            // txt开始吸烟年龄
            // 
            this.txt开始吸烟年龄.Location = new System.Drawing.Point(293, -2277);
            this.txt开始吸烟年龄.Name = "txt开始吸烟年龄";
            this.txt开始吸烟年龄.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt开始吸烟年龄.Properties.Appearance.Options.UseBackColor = true;
            this.txt开始吸烟年龄.Properties.ReadOnly = true;
            this.txt开始吸烟年龄.Size = new System.Drawing.Size(56, 20);
            this.txt开始吸烟年龄.StyleController = this.layoutControl1;
            this.txt开始吸烟年龄.TabIndex = 119;
            // 
            // txt日吸烟量
            // 
            this.txt日吸烟量.Location = new System.Drawing.Point(293, -2301);
            this.txt日吸烟量.Name = "txt日吸烟量";
            this.txt日吸烟量.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt日吸烟量.Properties.Appearance.Options.UseBackColor = true;
            this.txt日吸烟量.Properties.ReadOnly = true;
            this.txt日吸烟量.Size = new System.Drawing.Size(378, 20);
            this.txt日吸烟量.StyleController = this.layoutControl1;
            this.txt日吸烟量.TabIndex = 118;
            // 
            // txt吸烟状况
            // 
            this.txt吸烟状况.Location = new System.Drawing.Point(293, -2325);
            this.txt吸烟状况.Name = "txt吸烟状况";
            this.txt吸烟状况.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt吸烟状况.Properties.Appearance.Options.UseBackColor = true;
            this.txt吸烟状况.Properties.ReadOnly = true;
            this.txt吸烟状况.Size = new System.Drawing.Size(378, 20);
            this.txt吸烟状况.StyleController = this.layoutControl1;
            this.txt吸烟状况.TabIndex = 117;
            // 
            // txt饮食习惯
            // 
            this.txt饮食习惯.Location = new System.Drawing.Point(181, -2349);
            this.txt饮食习惯.Name = "txt饮食习惯";
            this.txt饮食习惯.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt饮食习惯.Properties.Appearance.Options.UseBackColor = true;
            this.txt饮食习惯.Properties.ReadOnly = true;
            this.txt饮食习惯.Size = new System.Drawing.Size(490, 20);
            this.txt饮食习惯.StyleController = this.layoutControl1;
            this.txt饮食习惯.TabIndex = 116;
            // 
            // txt锻炼方式
            // 
            this.txt锻炼方式.Location = new System.Drawing.Point(293, -2373);
            this.txt锻炼方式.Name = "txt锻炼方式";
            this.txt锻炼方式.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt锻炼方式.Properties.Appearance.Options.UseBackColor = true;
            this.txt锻炼方式.Properties.ReadOnly = true;
            this.txt锻炼方式.Size = new System.Drawing.Size(378, 20);
            this.txt锻炼方式.StyleController = this.layoutControl1;
            this.txt锻炼方式.TabIndex = 115;
            // 
            // txt坚持锻炼时间
            // 
            this.txt坚持锻炼时间.Location = new System.Drawing.Point(478, -2397);
            this.txt坚持锻炼时间.Name = "txt坚持锻炼时间";
            this.txt坚持锻炼时间.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt坚持锻炼时间.Properties.Appearance.Options.UseBackColor = true;
            this.txt坚持锻炼时间.Properties.ReadOnly = true;
            this.txt坚持锻炼时间.Size = new System.Drawing.Size(193, 20);
            this.txt坚持锻炼时间.StyleController = this.layoutControl1;
            this.txt坚持锻炼时间.TabIndex = 114;
            // 
            // txt每次锻炼时间
            // 
            this.txt每次锻炼时间.Location = new System.Drawing.Point(293, -2397);
            this.txt每次锻炼时间.Name = "txt每次锻炼时间";
            this.txt每次锻炼时间.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt每次锻炼时间.Properties.Appearance.Options.UseBackColor = true;
            this.txt每次锻炼时间.Properties.ReadOnly = true;
            this.txt每次锻炼时间.Size = new System.Drawing.Size(56, 20);
            this.txt每次锻炼时间.StyleController = this.layoutControl1;
            this.txt每次锻炼时间.TabIndex = 113;
            // 
            // txt锻炼频率
            // 
            this.txt锻炼频率.Location = new System.Drawing.Point(293, -2421);
            this.txt锻炼频率.Name = "txt锻炼频率";
            this.txt锻炼频率.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt锻炼频率.Properties.Appearance.Options.UseBackColor = true;
            this.txt锻炼频率.Properties.ReadOnly = true;
            this.txt锻炼频率.Size = new System.Drawing.Size(378, 20);
            this.txt锻炼频率.StyleController = this.layoutControl1;
            this.txt锻炼频率.TabIndex = 112;
            // 
            // txt右侧原因
            // 
            this.txt右侧原因.Location = new System.Drawing.Point(576, -2653);
            this.txt右侧原因.Name = "txt右侧原因";
            this.txt右侧原因.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt右侧原因.Properties.Appearance.Options.UseBackColor = true;
            this.txt右侧原因.Properties.ReadOnly = true;
            this.txt右侧原因.Size = new System.Drawing.Size(95, 20);
            this.txt右侧原因.StyleController = this.layoutControl1;
            this.txt右侧原因.TabIndex = 107;
            // 
            // txt左侧原因
            // 
            this.txt左侧原因.Location = new System.Drawing.Point(576, -2677);
            this.txt左侧原因.Name = "txt左侧原因";
            this.txt左侧原因.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt左侧原因.Properties.Appearance.Options.UseBackColor = true;
            this.txt左侧原因.Properties.ReadOnly = true;
            this.txt左侧原因.Size = new System.Drawing.Size(95, 20);
            this.txt左侧原因.StyleController = this.layoutControl1;
            this.txt左侧原因.TabIndex = 106;
            // 
            // txt血压右侧
            // 
            this.txt血压右侧.Location = new System.Drawing.Point(416, -2653);
            this.txt血压右侧.Name = "txt血压右侧";
            this.txt血压右侧.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血压右侧.Properties.Appearance.Options.UseBackColor = true;
            this.txt血压右侧.Properties.ReadOnly = true;
            this.txt血压右侧.Size = new System.Drawing.Size(81, 20);
            this.txt血压右侧.StyleController = this.layoutControl1;
            this.txt血压右侧.TabIndex = 105;
            // 
            // txt左侧血压
            // 
            this.txt左侧血压.Location = new System.Drawing.Point(416, -2677);
            this.txt左侧血压.Name = "txt左侧血压";
            this.txt左侧血压.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt左侧血压.Properties.Appearance.Options.UseBackColor = true;
            this.txt左侧血压.Properties.ReadOnly = true;
            this.txt左侧血压.Size = new System.Drawing.Size(81, 20);
            this.txt左侧血压.StyleController = this.layoutControl1;
            this.txt左侧血压.TabIndex = 103;
            // 
            // txt症状
            // 
            this.txt症状.Location = new System.Drawing.Point(89, -2725);
            this.txt症状.Name = "txt症状";
            this.txt症状.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt症状.Properties.Appearance.Options.UseBackColor = true;
            this.txt症状.Properties.ReadOnly = true;
            this.txt症状.Size = new System.Drawing.Size(582, 20);
            this.txt症状.StyleController = this.layoutControl1;
            this.txt症状.TabIndex = 100;
            // 
            // txt最近修改人
            // 
            this.txt最近修改人.Location = new System.Drawing.Point(483, 439);
            this.txt最近修改人.Name = "txt最近修改人";
            this.txt最近修改人.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt最近修改人.Properties.Appearance.Options.UseBackColor = true;
            this.txt最近修改人.Properties.ReadOnly = true;
            this.txt最近修改人.Size = new System.Drawing.Size(188, 20);
            this.txt最近修改人.StyleController = this.layoutControl1;
            this.txt最近修改人.TabIndex = 99;
            // 
            // txt创建人
            // 
            this.txt创建人.Location = new System.Drawing.Point(283, 415);
            this.txt创建人.Name = "txt创建人";
            this.txt创建人.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt创建人.Properties.Appearance.Options.UseBackColor = true;
            this.txt创建人.Properties.ReadOnly = true;
            this.txt创建人.Size = new System.Drawing.Size(117, 20);
            this.txt创建人.StyleController = this.layoutControl1;
            this.txt创建人.TabIndex = 98;
            // 
            // txt创建机构
            // 
            this.txt创建机构.Location = new System.Drawing.Point(83, 439);
            this.txt创建机构.Name = "txt创建机构";
            this.txt创建机构.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt创建机构.Properties.Appearance.Options.UseBackColor = true;
            this.txt创建机构.Properties.ReadOnly = true;
            this.txt创建机构.Size = new System.Drawing.Size(117, 20);
            this.txt创建机构.StyleController = this.layoutControl1;
            this.txt创建机构.TabIndex = 97;
            // 
            // txt当前所属机构
            // 
            this.txt当前所属机构.Location = new System.Drawing.Point(483, 415);
            this.txt当前所属机构.Name = "txt当前所属机构";
            this.txt当前所属机构.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt当前所属机构.Properties.Appearance.Options.UseBackColor = true;
            this.txt当前所属机构.Properties.ReadOnly = true;
            this.txt当前所属机构.Size = new System.Drawing.Size(188, 20);
            this.txt当前所属机构.StyleController = this.layoutControl1;
            this.txt当前所属机构.TabIndex = 96;
            // 
            // txt最近更新时间
            // 
            this.txt最近更新时间.Location = new System.Drawing.Point(283, 439);
            this.txt最近更新时间.Name = "txt最近更新时间";
            this.txt最近更新时间.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt最近更新时间.Properties.Appearance.Options.UseBackColor = true;
            this.txt最近更新时间.Properties.ReadOnly = true;
            this.txt最近更新时间.Size = new System.Drawing.Size(117, 20);
            this.txt最近更新时间.StyleController = this.layoutControl1;
            this.txt最近更新时间.TabIndex = 95;
            // 
            // txt创建时间
            // 
            this.txt创建时间.Location = new System.Drawing.Point(83, 415);
            this.txt创建时间.Name = "txt创建时间";
            this.txt创建时间.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt创建时间.Properties.Appearance.Options.UseBackColor = true;
            this.txt创建时间.Properties.ReadOnly = true;
            this.txt创建时间.Size = new System.Drawing.Size(117, 20);
            this.txt创建时间.StyleController = this.layoutControl1;
            this.txt创建时间.TabIndex = 94;
            // 
            // txt责任医生
            // 
            this.txt责任医生.Location = new System.Drawing.Point(288, -2772);
            this.txt责任医生.Name = "txt责任医生";
            this.txt责任医生.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt责任医生.Properties.Appearance.Options.UseBackColor = true;
            this.txt责任医生.Properties.ReadOnly = true;
            this.txt责任医生.Size = new System.Drawing.Size(111, 20);
            this.txt责任医生.StyleController = this.layoutControl1;
            this.txt责任医生.TabIndex = 12;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(88, -2796);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(584, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 10;
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(488, -2820);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Properties.ReadOnly = true;
            this.txt联系电话.Size = new System.Drawing.Size(184, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 9;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(288, -2820);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(111, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 8;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(88, -2820);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Properties.ReadOnly = true;
            this.txt身份证号.Size = new System.Drawing.Size(111, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 7;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(488, -2844);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(184, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 6;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(288, -2844);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(111, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 5;
            // 
            // txt个人档案编号
            // 
            this.txt个人档案编号.Location = new System.Drawing.Point(88, -2844);
            this.txt个人档案编号.Name = "txt个人档案编号";
            this.txt个人档案编号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt个人档案编号.Properties.Appearance.Options.UseBackColor = true;
            this.txt个人档案编号.Properties.ReadOnly = true;
            this.txt个人档案编号.Size = new System.Drawing.Size(111, 20);
            this.txt个人档案编号.StyleController = this.layoutControl1;
            this.txt个人档案编号.TabIndex = 4;
            // 
            // txt体检日期
            // 
            this.txt体检日期.EditValue = null;
            this.txt体检日期.Location = new System.Drawing.Point(88, -2772);
            this.txt体检日期.Name = "txt体检日期";
            this.txt体检日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt体检日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt体检日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt体检日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt体检日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt体检日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt体检日期.Properties.ReadOnly = true;
            this.txt体检日期.Size = new System.Drawing.Size(111, 20);
            this.txt体检日期.StyleController = this.layoutControl1;
            this.txt体检日期.TabIndex = 11;
            // 
            // txt乙型肝炎表面抗原
            // 
            this.txt乙型肝炎表面抗原.Location = new System.Drawing.Point(181, -742);
            this.txt乙型肝炎表面抗原.Name = "txt乙型肝炎表面抗原";
            this.txt乙型肝炎表面抗原.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt乙型肝炎表面抗原.Properties.Appearance.Options.UseBackColor = true;
            this.txt乙型肝炎表面抗原.Properties.ReadOnly = true;
            this.txt乙型肝炎表面抗原.Size = new System.Drawing.Size(490, 36);
            this.txt乙型肝炎表面抗原.StyleController = this.layoutControl1;
            this.txt乙型肝炎表面抗原.TabIndex = 176;
            this.txt乙型肝炎表面抗原.UseOptimizedRendering = true;
            // 
            // txt老年人健康状态自我评估
            // 
            this.txt老年人健康状态自我评估.Location = new System.Drawing.Point(181, -2581);
            this.txt老年人健康状态自我评估.Name = "txt老年人健康状态自我评估";
            this.txt老年人健康状态自我评估.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt老年人健康状态自我评估.Properties.Appearance.Options.UseBackColor = true;
            this.txt老年人健康状态自我评估.Properties.ReadOnly = true;
            this.txt老年人健康状态自我评估.Size = new System.Drawing.Size(490, 36);
            this.txt老年人健康状态自我评估.StyleController = this.layoutControl1;
            this.txt老年人健康状态自我评估.TabIndex = 218;
            this.txt老年人健康状态自我评估.UseOptimizedRendering = true;
            // 
            // txt老年人生活自理能力自我评估
            // 
            this.txt老年人生活自理能力自我评估.Location = new System.Drawing.Point(181, -2541);
            this.txt老年人生活自理能力自我评估.Name = "txt老年人生活自理能力自我评估";
            this.txt老年人生活自理能力自我评估.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt老年人生活自理能力自我评估.Properties.Appearance.Options.UseBackColor = true;
            this.txt老年人生活自理能力自我评估.Properties.ReadOnly = true;
            this.txt老年人生活自理能力自我评估.Size = new System.Drawing.Size(490, 36);
            this.txt老年人生活自理能力自我评估.StyleController = this.layoutControl1;
            this.txt老年人生活自理能力自我评估.TabIndex = 221;
            this.txt老年人生活自理能力自我评估.UseOptimizedRendering = true;
            // 
            // txt老年人认知能力
            // 
            this.txt老年人认知能力.Location = new System.Drawing.Point(181, -2501);
            this.txt老年人认知能力.Name = "txt老年人认知能力";
            this.txt老年人认知能力.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt老年人认知能力.Properties.Appearance.Options.UseBackColor = true;
            this.txt老年人认知能力.Properties.ReadOnly = true;
            this.txt老年人认知能力.Size = new System.Drawing.Size(490, 36);
            this.txt老年人认知能力.StyleController = this.layoutControl1;
            this.txt老年人认知能力.TabIndex = 220;
            this.txt老年人认知能力.UseOptimizedRendering = true;
            // 
            // txt老年人情感状态
            // 
            this.txt老年人情感状态.Location = new System.Drawing.Point(181, -2461);
            this.txt老年人情感状态.Name = "txt老年人情感状态";
            this.txt老年人情感状态.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt老年人情感状态.Properties.Appearance.Options.UseBackColor = true;
            this.txt老年人情感状态.Properties.ReadOnly = true;
            this.txt老年人情感状态.Size = new System.Drawing.Size(490, 36);
            this.txt老年人情感状态.StyleController = this.layoutControl1;
            this.txt老年人情感状态.TabIndex = 219;
            this.txt老年人情感状态.UseOptimizedRendering = true;
            // 
            // txt健康指导
            // 
            this.txt健康指导.Location = new System.Drawing.Point(88, 362);
            this.txt健康指导.Name = "txt健康指导";
            this.txt健康指导.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt健康指导.Properties.Appearance.Options.UseBackColor = true;
            this.txt健康指导.Properties.ReadOnly = true;
            this.txt健康指导.Size = new System.Drawing.Size(211, 46);
            this.txt健康指导.StyleController = this.layoutControl1;
            this.txt健康指导.TabIndex = 216;
            this.txt健康指导.UseOptimizedRendering = true;
            // 
            // txt危险因素控制
            // 
            this.txt危险因素控制.Location = new System.Drawing.Point(398, 362);
            this.txt危险因素控制.Name = "txt危险因素控制";
            this.txt危险因素控制.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt危险因素控制.Properties.Appearance.Options.UseBackColor = true;
            this.txt危险因素控制.Properties.ReadOnly = true;
            this.txt危险因素控制.Size = new System.Drawing.Size(274, 46);
            this.txt危险因素控制.StyleController = this.layoutControl1;
            this.txt危险因素控制.TabIndex = 217;
            this.txt危险因素控制.UseOptimizedRendering = true;
            // 
            // txt健康评价
            // 
            this.txt健康评价.Location = new System.Drawing.Point(88, 337);
            this.txt健康评价.Name = "txt健康评价";
            this.txt健康评价.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt健康评价.Properties.Appearance.Options.UseBackColor = true;
            this.txt健康评价.Properties.ReadOnly = true;
            this.txt健康评价.Size = new System.Drawing.Size(584, 21);
            this.txt健康评价.StyleController = this.layoutControl1;
            this.txt健康评价.TabIndex = 215;
            this.txt健康评价.UseOptimizedRendering = true;
            // 
            // lbl血管疾病
            // 
            this.lbl血管疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl血管疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血管疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血管疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血管疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血管疾病.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血管疾病.Control = this.txt血管疾病;
            this.lbl血管疾病.CustomizationFormText = "血管疾病";
            this.lbl血管疾病.Location = new System.Drawing.Point(92, 2767);
            this.lbl血管疾病.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血管疾病.MinSize = new System.Drawing.Size(133, 24);
            this.lbl血管疾病.Name = "lbl血管疾病";
            this.lbl血管疾病.Size = new System.Drawing.Size(579, 24);
            this.lbl血管疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血管疾病.Text = "血管疾病";
            this.lbl血管疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血管疾病.TextSize = new System.Drawing.Size(80, 20);
            this.lbl血管疾病.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl体检日期,
            this.lbl责任医生,
            this.emptySpaceItem1,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.lbl健康指导,
            this.lbl危险因素控制,
            this.simpleSeparator9});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -2890);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(675, 3353);
            this.layoutControlGroup1.Text = "健康体检表";
            // 
            // lbl体检日期
            // 
            this.lbl体检日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl体检日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl体检日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体检日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体检日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl体检日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl体检日期.Control = this.txt体检日期;
            this.lbl体检日期.CustomizationFormText = "体检日期";
            this.lbl体检日期.Location = new System.Drawing.Point(0, 90);
            this.lbl体检日期.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl体检日期.MinSize = new System.Drawing.Size(200, 24);
            this.lbl体检日期.Name = "lbl体检日期";
            this.lbl体检日期.Size = new System.Drawing.Size(200, 24);
            this.lbl体检日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl体检日期.Text = "体检日期";
            this.lbl体检日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl体检日期.TextSize = new System.Drawing.Size(80, 14);
            this.lbl体检日期.TextToControlDistance = 5;
            // 
            // lbl责任医生
            // 
            this.lbl责任医生.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl责任医生.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl责任医生.AppearanceItemCaption.Options.UseFont = true;
            this.lbl责任医生.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl责任医生.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl责任医生.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl责任医生.Control = this.txt责任医生;
            this.lbl责任医生.CustomizationFormText = "责任医生";
            this.lbl责任医生.Location = new System.Drawing.Point(200, 90);
            this.lbl责任医生.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl责任医生.MinSize = new System.Drawing.Size(200, 24);
            this.lbl责任医生.Name = "lbl责任医生";
            this.lbl责任医生.Size = new System.Drawing.Size(200, 24);
            this.lbl责任医生.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl责任医生.Text = "责任医生";
            this.lbl责任医生.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl责任医生.TextSize = new System.Drawing.Size(80, 14);
            this.lbl责任医生.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(400, 90);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(273, 24);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "个人信息";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl居住地址,
            this.lbl身份证号,
            this.lbl个人档案号,
            this.lbl姓名,
            this.lbl出生日期,
            this.lbl性别,
            this.lbl联系电话,
            this.layoutControlItem19});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(673, 90);
            this.layoutControlGroup2.Text = "个人信息";
            // 
            // lbl居住地址
            // 
            this.lbl居住地址.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl居住地址.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl居住地址.Control = this.txt居住地址;
            this.lbl居住地址.CustomizationFormText = "居住地址";
            this.lbl居住地址.Location = new System.Drawing.Point(0, 66);
            this.lbl居住地址.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl居住地址.MinSize = new System.Drawing.Size(165, 24);
            this.lbl居住地址.Name = "lbl居住地址";
            this.lbl居住地址.Size = new System.Drawing.Size(673, 24);
            this.lbl居住地址.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl居住地址.Text = "居住地址";
            this.lbl居住地址.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl居住地址.TextSize = new System.Drawing.Size(80, 14);
            this.lbl居住地址.TextToControlDistance = 5;
            // 
            // lbl身份证号
            // 
            this.lbl身份证号.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl身份证号.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl身份证号.Control = this.txt身份证号;
            this.lbl身份证号.CustomizationFormText = "身份证号";
            this.lbl身份证号.Location = new System.Drawing.Point(0, 42);
            this.lbl身份证号.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl身份证号.MinSize = new System.Drawing.Size(200, 24);
            this.lbl身份证号.Name = "lbl身份证号";
            this.lbl身份证号.Size = new System.Drawing.Size(200, 24);
            this.lbl身份证号.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl身份证号.Text = "身份证号";
            this.lbl身份证号.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl身份证号.TextSize = new System.Drawing.Size(80, 14);
            this.lbl身份证号.TextToControlDistance = 5;
            // 
            // lbl个人档案号
            // 
            this.lbl个人档案号.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl个人档案号.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl个人档案号.Control = this.txt个人档案编号;
            this.lbl个人档案号.CustomizationFormText = "个人档案号";
            this.lbl个人档案号.Location = new System.Drawing.Point(0, 18);
            this.lbl个人档案号.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl个人档案号.MinSize = new System.Drawing.Size(200, 24);
            this.lbl个人档案号.Name = "lbl个人档案号";
            this.lbl个人档案号.Size = new System.Drawing.Size(200, 24);
            this.lbl个人档案号.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl个人档案号.Text = "个人档案号";
            this.lbl个人档案号.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl个人档案号.TextSize = new System.Drawing.Size(80, 14);
            this.lbl个人档案号.TextToControlDistance = 5;
            // 
            // lbl姓名
            // 
            this.lbl姓名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl姓名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl姓名.Control = this.txt姓名;
            this.lbl姓名.CustomizationFormText = "姓名";
            this.lbl姓名.Location = new System.Drawing.Point(200, 18);
            this.lbl姓名.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl姓名.MinSize = new System.Drawing.Size(200, 24);
            this.lbl姓名.Name = "lbl姓名";
            this.lbl姓名.Size = new System.Drawing.Size(200, 24);
            this.lbl姓名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl姓名.Text = "姓名";
            this.lbl姓名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl姓名.TextSize = new System.Drawing.Size(80, 14);
            this.lbl姓名.TextToControlDistance = 5;
            // 
            // lbl出生日期
            // 
            this.lbl出生日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl出生日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl出生日期.Control = this.txt出生日期;
            this.lbl出生日期.CustomizationFormText = "出生日期";
            this.lbl出生日期.Location = new System.Drawing.Point(200, 42);
            this.lbl出生日期.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl出生日期.MinSize = new System.Drawing.Size(200, 24);
            this.lbl出生日期.Name = "lbl出生日期";
            this.lbl出生日期.Size = new System.Drawing.Size(200, 24);
            this.lbl出生日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl出生日期.Text = "出生日期";
            this.lbl出生日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl出生日期.TextSize = new System.Drawing.Size(80, 14);
            this.lbl出生日期.TextToControlDistance = 5;
            // 
            // lbl性别
            // 
            this.lbl性别.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl性别.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl性别.Control = this.txt性别;
            this.lbl性别.CustomizationFormText = "性别";
            this.lbl性别.Location = new System.Drawing.Point(400, 18);
            this.lbl性别.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl性别.MinSize = new System.Drawing.Size(165, 24);
            this.lbl性别.Name = "lbl性别";
            this.lbl性别.Size = new System.Drawing.Size(273, 24);
            this.lbl性别.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl性别.Text = "性别";
            this.lbl性别.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl性别.TextSize = new System.Drawing.Size(80, 14);
            this.lbl性别.TextToControlDistance = 5;
            // 
            // lbl联系电话
            // 
            this.lbl联系电话.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl联系电话.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl联系电话.Control = this.txt联系电话;
            this.lbl联系电话.CustomizationFormText = "联系电话";
            this.lbl联系电话.Location = new System.Drawing.Point(400, 42);
            this.lbl联系电话.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl联系电话.MinSize = new System.Drawing.Size(165, 24);
            this.lbl联系电话.Name = "lbl联系电话";
            this.lbl联系电话.Size = new System.Drawing.Size(273, 24);
            this.lbl联系电话.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl联系电话.Text = "联系电话";
            this.lbl联系电话.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl联系电话.TextSize = new System.Drawing.Size(80, 14);
            this.lbl联系电话.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.labelControl1;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(673, 18);
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl健康评价});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 3199);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(673, 25);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // lbl健康评价
            // 
            this.lbl健康评价.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl健康评价.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl健康评价.AppearanceItemCaption.Options.UseFont = true;
            this.lbl健康评价.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl健康评价.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl健康评价.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl健康评价.Control = this.txt健康评价;
            this.lbl健康评价.CustomizationFormText = "健康评价";
            this.lbl健康评价.Location = new System.Drawing.Point(0, 0);
            this.lbl健康评价.MinSize = new System.Drawing.Size(50, 25);
            this.lbl健康评价.Name = "lbl健康评价";
            this.lbl健康评价.Size = new System.Drawing.Size(673, 25);
            this.lbl健康评价.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl健康评价.Text = "健康评价";
            this.lbl健康评价.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl健康评价.TextSize = new System.Drawing.Size(80, 20);
            this.lbl健康评价.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem7,
            this.emptySpaceItem12,
            this.lbl症状,
            this.simpleLabelItem4,
            this.lbl血压右侧,
            this.lbl血压左侧,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.lbl锻炼频率,
            this.lbl饮食习惯,
            this.lbl吸烟状况,
            this.lbl饮酒频率,
            this.layoutControlItem26,
            this.layoutControlItem28,
            this.group职业病,
            this.group饮酒情况,
            this.group吸烟情况,
            this.group体育锻炼,
            this.lbl口唇,
            this.lbl咽部,
            this.lbl视力,
            this.lbl听力,
            this.lbl运动功能,
            this.lbl眼底,
            this.lbl皮肤,
            this.lbl巩膜,
            this.lbl淋巴结,
            this.lbl桶状胸,
            this.lbl罗音,
            this.lbl呼吸音,
            this.lbl心率,
            this.lbl心律,
            this.lbl杂音,
            this.lbl压痛,
            this.lbl移动性浊音,
            this.lbl包块,
            this.lbl肝大,
            this.lbl脾大,
            this.lbl下肢水肿,
            this.lbl查体其他,
            this.lbl足背动脉搏动,
            this.lbl肛门指诊,
            this.lbl乳腺,
            this.layoutControlItem16,
            this.lbl血常规其他,
            this.lbl白细胞,
            this.lbl血小板,
            this.lbl尿常规,
            this.lbl空腹血糖,
            this.lbl心电图,
            this.lbl尿微量白蛋白,
            this.lbl乙型肝炎表面抗原,
            this.lbl大便潜血,
            this.lbl糖化血红蛋白,
            this.lbl血清谷丙转氨酶,
            this.lbl结合胆红素,
            this.lbl血清谷草转氨酶,
            this.lbl总胆红素,
            this.lbl白蛋白,
            this.lbl血清肌酐,
            this.lbl血钠浓度,
            this.lbl血尿素氮,
            this.lbl血钾浓度,
            this.lbl总胆固醇,
            this.lbl甘油三酯,
            this.lbl血清低密度脂蛋白胆固醇,
            this.lbl血清高密度脂蛋白胆固醇,
            this.lbl胸部X线片,
            this.lbl辅助检查其他,
            this.lbl腹部B超,
            this.lbl平和质,
            this.lbl特秉质,
            this.lbl气郁质,
            this.lbl血瘀质,
            this.lbl湿热质,
            this.lbl痰湿质,
            this.lbl气虚质,
            this.lbl阴虚质,
            this.lbl阳虚质,
            this.lbl脑血管疾病,
            this.lbl其他系统疾病,
            this.lbl神经系统疾病,
            this.lbl眼部疾病,
            this.lbl心血管疾病,
            this.lbl肾脏疾病,
            this.lbl住院史,
            this.lbl家庭病床史,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.simpleSeparator8,
            this.group老年人,
            this.group妇科,
            this.lbl宫颈涂片,
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.layoutControlGroup9,
            this.layoutControlGroup10,
            this.layoutControlGroup11,
            this.layoutControlGroup12,
            this.layoutControlGroup13,
            this.layoutControlGroup14,
            this.layoutControlGroup16,
            this.layoutControlGroup17,
            this.layoutControlGroup18,
            this.layoutControlGroup19,
            this.layoutControlGroup21,
            this.layoutControlGroup22,
            this.layoutControlGroup23,
            this.layoutControlGroup24,
            this.layoutControlGroup25,
            this.layoutControlGroup26,
            this.simpleSeparator12,
            this.layoutControlGroup28,
            this.lbl齿列正常,
            this.lbl齿列总,
            this.lbl体温,
            this.lbl脉率,
            this.lbl呼吸频率,
            this.lbl身高,
            this.lbl体重,
            this.lbl腰围,
            this.lbl体重指数,
            this.group齿列,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.emptySpaceItem23,
            this.lbl其他B超});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 114);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(673, 3085);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem7.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem7.CustomizationFormText = "内   容";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(90, 22);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(90, 22);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.Text = "内   容";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(76, 0);
            this.emptySpaceItem7.TextVisible = true;
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem12.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem12.CustomizationFormText = "检查项目";
            this.emptySpaceItem12.Location = new System.Drawing.Point(90, 0);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 22);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.emptySpaceItem12.Size = new System.Drawing.Size(581, 22);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.Text = "检查项目";
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(76, 0);
            this.emptySpaceItem12.TextVisible = true;
            // 
            // lbl症状
            // 
            this.lbl症状.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl症状.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl症状.AppearanceItemCaption.Options.UseFont = true;
            this.lbl症状.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl症状.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl症状.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl症状.Control = this.txt症状;
            this.lbl症状.CustomizationFormText = "症状";
            this.lbl症状.Location = new System.Drawing.Point(0, 22);
            this.lbl症状.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl症状.MinSize = new System.Drawing.Size(165, 24);
            this.lbl症状.Name = "lbl症状";
            this.lbl症状.Size = new System.Drawing.Size(671, 24);
            this.lbl症状.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl症状.Text = "症状";
            this.lbl症状.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl症状.TextSize = new System.Drawing.Size(80, 20);
            this.lbl症状.TextToControlDistance = 5;
            // 
            // simpleLabelItem4
            // 
            this.simpleLabelItem4.AllowHotTrack = false;
            this.simpleLabelItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem4.CustomizationFormText = "血压";
            this.simpleLabelItem4.Location = new System.Drawing.Point(272, 70);
            this.simpleLabelItem4.MaxSize = new System.Drawing.Size(95, 18);
            this.simpleLabelItem4.MinSize = new System.Drawing.Size(95, 1);
            this.simpleLabelItem4.Name = "simpleLabelItem4";
            this.simpleLabelItem4.Size = new System.Drawing.Size(95, 48);
            this.simpleLabelItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem4.Text = "血压";
            this.simpleLabelItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem4.TextSize = new System.Drawing.Size(50, 14);
            // 
            // lbl血压右侧
            // 
            this.lbl血压右侧.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血压右侧.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl血压右侧.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血压右侧.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血压右侧.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血压右侧.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血压右侧.Control = this.txt血压右侧;
            this.lbl血压右侧.CustomizationFormText = "右侧";
            this.lbl血压右侧.Location = new System.Drawing.Point(367, 94);
            this.lbl血压右侧.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血压右侧.MinSize = new System.Drawing.Size(130, 24);
            this.lbl血压右侧.Name = "lbl血压右侧";
            this.lbl血压右侧.Size = new System.Drawing.Size(130, 24);
            this.lbl血压右侧.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血压右侧.Text = "右侧";
            this.lbl血压右侧.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血压右侧.TextSize = new System.Drawing.Size(40, 20);
            this.lbl血压右侧.TextToControlDistance = 5;
            // 
            // lbl血压左侧
            // 
            this.lbl血压左侧.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血压左侧.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl血压左侧.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血压左侧.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血压左侧.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血压左侧.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血压左侧.Control = this.txt左侧血压;
            this.lbl血压左侧.CustomizationFormText = "左侧";
            this.lbl血压左侧.Location = new System.Drawing.Point(367, 70);
            this.lbl血压左侧.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血压左侧.MinSize = new System.Drawing.Size(130, 24);
            this.lbl血压左侧.Name = "lbl血压左侧";
            this.lbl血压左侧.Size = new System.Drawing.Size(130, 24);
            this.lbl血压左侧.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血压左侧.Text = "左侧";
            this.lbl血压左侧.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血压左侧.TextSize = new System.Drawing.Size(40, 20);
            this.lbl血压左侧.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.txt左侧原因;
            this.layoutControlItem12.CustomizationFormText = "左侧原因";
            this.layoutControlItem12.Location = new System.Drawing.Point(497, 70);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(165, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(174, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "左侧原因";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.txt右侧原因;
            this.layoutControlItem13.CustomizationFormText = "右侧原因";
            this.layoutControlItem13.Location = new System.Drawing.Point(497, 94);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(165, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(174, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "右侧原因";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(70, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // lbl锻炼频率
            // 
            this.lbl锻炼频率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl锻炼频率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl锻炼频率.Control = this.txt锻炼频率;
            this.lbl锻炼频率.CustomizationFormText = "锻炼频率";
            this.lbl锻炼频率.Location = new System.Drawing.Point(184, 326);
            this.lbl锻炼频率.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl锻炼频率.MinSize = new System.Drawing.Size(165, 24);
            this.lbl锻炼频率.Name = "lbl锻炼频率";
            this.lbl锻炼频率.Size = new System.Drawing.Size(487, 24);
            this.lbl锻炼频率.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl锻炼频率.Text = "锻炼频率";
            this.lbl锻炼频率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl锻炼频率.TextSize = new System.Drawing.Size(100, 20);
            this.lbl锻炼频率.TextToControlDistance = 5;
            // 
            // lbl饮食习惯
            // 
            this.lbl饮食习惯.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl饮食习惯.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl饮食习惯.AppearanceItemCaption.Options.UseFont = true;
            this.lbl饮食习惯.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl饮食习惯.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl饮食习惯.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl饮食习惯.Control = this.txt饮食习惯;
            this.lbl饮食习惯.CustomizationFormText = "饮食习惯";
            this.lbl饮食习惯.Location = new System.Drawing.Point(92, 398);
            this.lbl饮食习惯.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl饮食习惯.MinSize = new System.Drawing.Size(165, 24);
            this.lbl饮食习惯.Name = "lbl饮食习惯";
            this.lbl饮食习惯.Size = new System.Drawing.Size(579, 24);
            this.lbl饮食习惯.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl饮食习惯.Text = "饮食习惯";
            this.lbl饮食习惯.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl饮食习惯.TextSize = new System.Drawing.Size(80, 20);
            this.lbl饮食习惯.TextToControlDistance = 5;
            // 
            // lbl吸烟状况
            // 
            this.lbl吸烟状况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl吸烟状况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl吸烟状况.Control = this.txt吸烟状况;
            this.lbl吸烟状况.CustomizationFormText = "吸烟状况";
            this.lbl吸烟状况.Location = new System.Drawing.Point(184, 422);
            this.lbl吸烟状况.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl吸烟状况.MinSize = new System.Drawing.Size(165, 24);
            this.lbl吸烟状况.Name = "lbl吸烟状况";
            this.lbl吸烟状况.Size = new System.Drawing.Size(487, 24);
            this.lbl吸烟状况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl吸烟状况.Text = "吸烟状况";
            this.lbl吸烟状况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl吸烟状况.TextSize = new System.Drawing.Size(100, 20);
            this.lbl吸烟状况.TextToControlDistance = 5;
            // 
            // lbl饮酒频率
            // 
            this.lbl饮酒频率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl饮酒频率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl饮酒频率.Control = this.txt饮酒频率;
            this.lbl饮酒频率.CustomizationFormText = "饮酒频率";
            this.lbl饮酒频率.Location = new System.Drawing.Point(184, 494);
            this.lbl饮酒频率.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl饮酒频率.MinSize = new System.Drawing.Size(165, 24);
            this.lbl饮酒频率.Name = "lbl饮酒频率";
            this.lbl饮酒频率.Size = new System.Drawing.Size(487, 24);
            this.lbl饮酒频率.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl饮酒频率.Text = "饮酒频率";
            this.lbl饮酒频率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl饮酒频率.TextSize = new System.Drawing.Size(100, 20);
            this.lbl饮酒频率.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.lbl职业病有无;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(184, 614);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(50, 18);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(50, 18);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(50, 18);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "layoutControlItem26";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextToControlDistance = 0;
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.lbl工种及从业时间;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(234, 614);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(437, 18);
            this.layoutControlItem28.Text = "layoutControlItem28";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextToControlDistance = 0;
            this.layoutControlItem28.TextVisible = false;
            // 
            // group职业病
            // 
            this.group职业病.CustomizationFormText = "group职业病";
            this.group职业病.GroupBordersVisible = false;
            this.group职业病.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl粉尘,
            this.lbl放射物质,
            this.lbl物理因素,
            this.lbl化学物质,
            this.lbl职业病其他,
            this.lbl职业病其他防护措施,
            this.lbl化学物质防护措施,
            this.lbl物理因素防护措施,
            this.lbl放射物质防护措施,
            this.lbl粉尘防护措施,
            this.layoutControlItem27});
            this.group职业病.Location = new System.Drawing.Point(184, 632);
            this.group职业病.Name = "group职业病";
            this.group职业病.Size = new System.Drawing.Size(487, 138);
            this.group职业病.Text = "group职业病";
            // 
            // lbl粉尘
            // 
            this.lbl粉尘.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl粉尘.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl粉尘.Control = this.txt粉尘;
            this.lbl粉尘.CustomizationFormText = "layoutControlItem16";
            this.lbl粉尘.Location = new System.Drawing.Point(0, 18);
            this.lbl粉尘.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl粉尘.MinSize = new System.Drawing.Size(133, 24);
            this.lbl粉尘.Name = "lbl粉尘";
            this.lbl粉尘.Size = new System.Drawing.Size(133, 24);
            this.lbl粉尘.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl粉尘.Text = "粉尘";
            this.lbl粉尘.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl粉尘.TextSize = new System.Drawing.Size(70, 14);
            this.lbl粉尘.TextToControlDistance = 5;
            // 
            // lbl放射物质
            // 
            this.lbl放射物质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl放射物质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl放射物质.Control = this.txt放射物质;
            this.lbl放射物质.CustomizationFormText = "layoutControlItem18";
            this.lbl放射物质.Location = new System.Drawing.Point(0, 42);
            this.lbl放射物质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl放射物质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl放射物质.Name = "lbl放射物质";
            this.lbl放射物质.Size = new System.Drawing.Size(133, 24);
            this.lbl放射物质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl放射物质.Text = "放射物质";
            this.lbl放射物质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl放射物质.TextSize = new System.Drawing.Size(70, 14);
            this.lbl放射物质.TextToControlDistance = 5;
            // 
            // lbl物理因素
            // 
            this.lbl物理因素.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl物理因素.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl物理因素.Control = this.txt物理因素;
            this.lbl物理因素.CustomizationFormText = "layoutControlItem20";
            this.lbl物理因素.Location = new System.Drawing.Point(0, 66);
            this.lbl物理因素.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl物理因素.MinSize = new System.Drawing.Size(133, 24);
            this.lbl物理因素.Name = "lbl物理因素";
            this.lbl物理因素.Size = new System.Drawing.Size(133, 24);
            this.lbl物理因素.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl物理因素.Text = "物理因素 ";
            this.lbl物理因素.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl物理因素.TextSize = new System.Drawing.Size(70, 14);
            this.lbl物理因素.TextToControlDistance = 5;
            // 
            // lbl化学物质
            // 
            this.lbl化学物质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl化学物质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl化学物质.Control = this.txt化学物质;
            this.lbl化学物质.CustomizationFormText = "layoutControlItem22";
            this.lbl化学物质.Location = new System.Drawing.Point(0, 90);
            this.lbl化学物质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl化学物质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl化学物质.Name = "lbl化学物质";
            this.lbl化学物质.Size = new System.Drawing.Size(133, 24);
            this.lbl化学物质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl化学物质.Text = "化学物质 ";
            this.lbl化学物质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl化学物质.TextSize = new System.Drawing.Size(70, 14);
            this.lbl化学物质.TextToControlDistance = 5;
            // 
            // lbl职业病其他
            // 
            this.lbl职业病其他.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl职业病其他.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl职业病其他.Control = this.txt职业病其他;
            this.lbl职业病其他.CustomizationFormText = "layoutControlItem24";
            this.lbl职业病其他.Location = new System.Drawing.Point(0, 114);
            this.lbl职业病其他.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl职业病其他.MinSize = new System.Drawing.Size(133, 24);
            this.lbl职业病其他.Name = "lbl职业病其他";
            this.lbl职业病其他.Size = new System.Drawing.Size(133, 24);
            this.lbl职业病其他.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl职业病其他.Text = "其他";
            this.lbl职业病其他.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl职业病其他.TextSize = new System.Drawing.Size(70, 14);
            this.lbl职业病其他.TextToControlDistance = 5;
            // 
            // lbl职业病其他防护措施
            // 
            this.lbl职业病其他防护措施.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl职业病其他防护措施.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl职业病其他防护措施.Control = this.txt职业病其他防护措施;
            this.lbl职业病其他防护措施.CustomizationFormText = "layoutControlItem25";
            this.lbl职业病其他防护措施.Location = new System.Drawing.Point(133, 114);
            this.lbl职业病其他防护措施.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl职业病其他防护措施.MinSize = new System.Drawing.Size(133, 24);
            this.lbl职业病其他防护措施.Name = "lbl职业病其他防护措施";
            this.lbl职业病其他防护措施.Size = new System.Drawing.Size(354, 24);
            this.lbl职业病其他防护措施.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl职业病其他防护措施.Text = "防护措施";
            this.lbl职业病其他防护措施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl职业病其他防护措施.TextSize = new System.Drawing.Size(70, 14);
            this.lbl职业病其他防护措施.TextToControlDistance = 5;
            // 
            // lbl化学物质防护措施
            // 
            this.lbl化学物质防护措施.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl化学物质防护措施.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl化学物质防护措施.Control = this.txt化学物质防护措施;
            this.lbl化学物质防护措施.CustomizationFormText = "layoutControlItem23";
            this.lbl化学物质防护措施.Location = new System.Drawing.Point(133, 90);
            this.lbl化学物质防护措施.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl化学物质防护措施.MinSize = new System.Drawing.Size(133, 24);
            this.lbl化学物质防护措施.Name = "lbl化学物质防护措施";
            this.lbl化学物质防护措施.Size = new System.Drawing.Size(354, 24);
            this.lbl化学物质防护措施.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl化学物质防护措施.Text = "防护措施";
            this.lbl化学物质防护措施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl化学物质防护措施.TextSize = new System.Drawing.Size(70, 14);
            this.lbl化学物质防护措施.TextToControlDistance = 5;
            // 
            // lbl物理因素防护措施
            // 
            this.lbl物理因素防护措施.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl物理因素防护措施.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl物理因素防护措施.Control = this.txt物理因素防护措施;
            this.lbl物理因素防护措施.CustomizationFormText = "layoutControlItem21";
            this.lbl物理因素防护措施.Location = new System.Drawing.Point(133, 66);
            this.lbl物理因素防护措施.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl物理因素防护措施.MinSize = new System.Drawing.Size(133, 24);
            this.lbl物理因素防护措施.Name = "lbl物理因素防护措施";
            this.lbl物理因素防护措施.Size = new System.Drawing.Size(354, 24);
            this.lbl物理因素防护措施.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl物理因素防护措施.Text = "防护措施";
            this.lbl物理因素防护措施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl物理因素防护措施.TextSize = new System.Drawing.Size(70, 14);
            this.lbl物理因素防护措施.TextToControlDistance = 5;
            // 
            // lbl放射物质防护措施
            // 
            this.lbl放射物质防护措施.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl放射物质防护措施.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl放射物质防护措施.Control = this.txt放射物质防护措施;
            this.lbl放射物质防护措施.CustomizationFormText = "layoutControlItem19";
            this.lbl放射物质防护措施.Location = new System.Drawing.Point(133, 42);
            this.lbl放射物质防护措施.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl放射物质防护措施.MinSize = new System.Drawing.Size(133, 24);
            this.lbl放射物质防护措施.Name = "lbl放射物质防护措施";
            this.lbl放射物质防护措施.Size = new System.Drawing.Size(354, 24);
            this.lbl放射物质防护措施.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl放射物质防护措施.Text = "防护措施";
            this.lbl放射物质防护措施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl放射物质防护措施.TextSize = new System.Drawing.Size(70, 14);
            this.lbl放射物质防护措施.TextToControlDistance = 5;
            // 
            // lbl粉尘防护措施
            // 
            this.lbl粉尘防护措施.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl粉尘防护措施.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl粉尘防护措施.Control = this.txt粉尘防护措施;
            this.lbl粉尘防护措施.CustomizationFormText = "layoutControlItem17";
            this.lbl粉尘防护措施.Location = new System.Drawing.Point(133, 18);
            this.lbl粉尘防护措施.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl粉尘防护措施.MinSize = new System.Drawing.Size(133, 24);
            this.lbl粉尘防护措施.Name = "lbl粉尘防护措施";
            this.lbl粉尘防护措施.Size = new System.Drawing.Size(354, 24);
            this.lbl粉尘防护措施.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl粉尘防护措施.Text = "防护措施";
            this.lbl粉尘防护措施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl粉尘防护措施.TextSize = new System.Drawing.Size(70, 14);
            this.lbl粉尘防护措施.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.lbl毒物种类;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(487, 18);
            this.layoutControlItem27.Text = "layoutControlItem27";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextToControlDistance = 0;
            this.layoutControlItem27.TextVisible = false;
            // 
            // group饮酒情况
            // 
            this.group饮酒情况.CustomizationFormText = "group饮酒情况";
            this.group饮酒情况.GroupBordersVisible = false;
            this.group饮酒情况.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl日饮酒量,
            this.lbl是否戒酒,
            this.lbl开始饮酒年龄,
            this.lbl近一年内是否曾醉酒,
            this.lbl戒酒年龄,
            this.lbl饮酒种类});
            this.group饮酒情况.Location = new System.Drawing.Point(184, 518);
            this.group饮酒情况.Name = "group饮酒情况";
            this.group饮酒情况.Size = new System.Drawing.Size(487, 96);
            this.group饮酒情况.Text = "group饮酒情况";
            // 
            // lbl日饮酒量
            // 
            this.lbl日饮酒量.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl日饮酒量.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl日饮酒量.Control = this.txt日饮酒量;
            this.lbl日饮酒量.CustomizationFormText = "日饮酒量 ";
            this.lbl日饮酒量.Location = new System.Drawing.Point(0, 0);
            this.lbl日饮酒量.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl日饮酒量.MinSize = new System.Drawing.Size(165, 24);
            this.lbl日饮酒量.Name = "lbl日饮酒量";
            this.lbl日饮酒量.Size = new System.Drawing.Size(487, 24);
            this.lbl日饮酒量.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl日饮酒量.Text = "日饮酒量 ";
            this.lbl日饮酒量.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl日饮酒量.TextSize = new System.Drawing.Size(100, 14);
            this.lbl日饮酒量.TextToControlDistance = 5;
            // 
            // lbl是否戒酒
            // 
            this.lbl是否戒酒.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl是否戒酒.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl是否戒酒.Control = this.txt是否戒酒;
            this.lbl是否戒酒.CustomizationFormText = "是否戒酒";
            this.lbl是否戒酒.Location = new System.Drawing.Point(0, 24);
            this.lbl是否戒酒.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl是否戒酒.MinSize = new System.Drawing.Size(165, 24);
            this.lbl是否戒酒.Name = "lbl是否戒酒";
            this.lbl是否戒酒.Size = new System.Drawing.Size(165, 24);
            this.lbl是否戒酒.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl是否戒酒.Text = "是否戒酒";
            this.lbl是否戒酒.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl是否戒酒.TextSize = new System.Drawing.Size(100, 20);
            this.lbl是否戒酒.TextToControlDistance = 5;
            // 
            // lbl开始饮酒年龄
            // 
            this.lbl开始饮酒年龄.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl开始饮酒年龄.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl开始饮酒年龄.Control = this.txt开始饮酒年龄;
            this.lbl开始饮酒年龄.CustomizationFormText = "开始饮酒年龄";
            this.lbl开始饮酒年龄.Location = new System.Drawing.Point(0, 48);
            this.lbl开始饮酒年龄.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl开始饮酒年龄.MinSize = new System.Drawing.Size(165, 24);
            this.lbl开始饮酒年龄.Name = "lbl开始饮酒年龄";
            this.lbl开始饮酒年龄.Size = new System.Drawing.Size(165, 24);
            this.lbl开始饮酒年龄.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl开始饮酒年龄.Text = "开始饮酒年龄";
            this.lbl开始饮酒年龄.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl开始饮酒年龄.TextSize = new System.Drawing.Size(100, 20);
            this.lbl开始饮酒年龄.TextToControlDistance = 5;
            // 
            // lbl近一年内是否曾醉酒
            // 
            this.lbl近一年内是否曾醉酒.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl近一年内是否曾醉酒.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl近一年内是否曾醉酒.Control = this.txt近一年内是否曾醉酒;
            this.lbl近一年内是否曾醉酒.CustomizationFormText = "近一年内是否曾醉酒";
            this.lbl近一年内是否曾醉酒.Location = new System.Drawing.Point(165, 48);
            this.lbl近一年内是否曾醉酒.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl近一年内是否曾醉酒.MinSize = new System.Drawing.Size(165, 24);
            this.lbl近一年内是否曾醉酒.Name = "lbl近一年内是否曾醉酒";
            this.lbl近一年内是否曾醉酒.Size = new System.Drawing.Size(322, 24);
            this.lbl近一年内是否曾醉酒.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl近一年内是否曾醉酒.Text = "近一年内是否曾醉酒";
            this.lbl近一年内是否曾醉酒.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl近一年内是否曾醉酒.TextSize = new System.Drawing.Size(120, 20);
            this.lbl近一年内是否曾醉酒.TextToControlDistance = 5;
            // 
            // lbl戒酒年龄
            // 
            this.lbl戒酒年龄.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl戒酒年龄.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl戒酒年龄.Control = this.txt戒酒年龄;
            this.lbl戒酒年龄.CustomizationFormText = "戒酒年龄";
            this.lbl戒酒年龄.Location = new System.Drawing.Point(165, 24);
            this.lbl戒酒年龄.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl戒酒年龄.MinSize = new System.Drawing.Size(165, 24);
            this.lbl戒酒年龄.Name = "lbl戒酒年龄";
            this.lbl戒酒年龄.Size = new System.Drawing.Size(322, 24);
            this.lbl戒酒年龄.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl戒酒年龄.Text = "戒酒年龄";
            this.lbl戒酒年龄.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl戒酒年龄.TextSize = new System.Drawing.Size(120, 20);
            this.lbl戒酒年龄.TextToControlDistance = 5;
            // 
            // lbl饮酒种类
            // 
            this.lbl饮酒种类.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl饮酒种类.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl饮酒种类.Control = this.txt饮酒种类;
            this.lbl饮酒种类.CustomizationFormText = "饮酒种类";
            this.lbl饮酒种类.Location = new System.Drawing.Point(0, 72);
            this.lbl饮酒种类.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl饮酒种类.MinSize = new System.Drawing.Size(165, 24);
            this.lbl饮酒种类.Name = "lbl饮酒种类";
            this.lbl饮酒种类.Size = new System.Drawing.Size(487, 24);
            this.lbl饮酒种类.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl饮酒种类.Text = "饮酒种类";
            this.lbl饮酒种类.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl饮酒种类.TextSize = new System.Drawing.Size(100, 20);
            this.lbl饮酒种类.TextToControlDistance = 5;
            // 
            // group吸烟情况
            // 
            this.group吸烟情况.CustomizationFormText = "group吸烟情况";
            this.group吸烟情况.GroupBordersVisible = false;
            this.group吸烟情况.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl日吸烟量,
            this.lbl开始吸烟年龄,
            this.lbl戒烟年龄});
            this.group吸烟情况.Location = new System.Drawing.Point(184, 446);
            this.group吸烟情况.Name = "group吸烟情况";
            this.group吸烟情况.Size = new System.Drawing.Size(487, 48);
            this.group吸烟情况.Text = "group吸烟情况";
            // 
            // lbl日吸烟量
            // 
            this.lbl日吸烟量.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl日吸烟量.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl日吸烟量.Control = this.txt日吸烟量;
            this.lbl日吸烟量.CustomizationFormText = "日吸烟量";
            this.lbl日吸烟量.Location = new System.Drawing.Point(0, 0);
            this.lbl日吸烟量.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl日吸烟量.MinSize = new System.Drawing.Size(165, 24);
            this.lbl日吸烟量.Name = "lbl日吸烟量";
            this.lbl日吸烟量.Size = new System.Drawing.Size(487, 24);
            this.lbl日吸烟量.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl日吸烟量.Text = "日吸烟量";
            this.lbl日吸烟量.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl日吸烟量.TextSize = new System.Drawing.Size(100, 20);
            this.lbl日吸烟量.TextToControlDistance = 5;
            // 
            // lbl开始吸烟年龄
            // 
            this.lbl开始吸烟年龄.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl开始吸烟年龄.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl开始吸烟年龄.Control = this.txt开始吸烟年龄;
            this.lbl开始吸烟年龄.CustomizationFormText = "开始吸烟年龄";
            this.lbl开始吸烟年龄.Location = new System.Drawing.Point(0, 24);
            this.lbl开始吸烟年龄.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl开始吸烟年龄.MinSize = new System.Drawing.Size(165, 24);
            this.lbl开始吸烟年龄.Name = "lbl开始吸烟年龄";
            this.lbl开始吸烟年龄.Size = new System.Drawing.Size(165, 24);
            this.lbl开始吸烟年龄.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl开始吸烟年龄.Text = "开始吸烟年龄";
            this.lbl开始吸烟年龄.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl开始吸烟年龄.TextSize = new System.Drawing.Size(100, 20);
            this.lbl开始吸烟年龄.TextToControlDistance = 5;
            // 
            // lbl戒烟年龄
            // 
            this.lbl戒烟年龄.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl戒烟年龄.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl戒烟年龄.Control = this.txt戒烟年龄;
            this.lbl戒烟年龄.CustomizationFormText = "戒烟年龄 ";
            this.lbl戒烟年龄.Location = new System.Drawing.Point(165, 24);
            this.lbl戒烟年龄.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl戒烟年龄.MinSize = new System.Drawing.Size(165, 24);
            this.lbl戒烟年龄.Name = "lbl戒烟年龄";
            this.lbl戒烟年龄.Size = new System.Drawing.Size(322, 24);
            this.lbl戒烟年龄.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl戒烟年龄.Text = "戒烟年龄 ";
            this.lbl戒烟年龄.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl戒烟年龄.TextSize = new System.Drawing.Size(120, 20);
            this.lbl戒烟年龄.TextToControlDistance = 5;
            // 
            // group体育锻炼
            // 
            this.group体育锻炼.CustomizationFormText = "group体育锻炼";
            this.group体育锻炼.GroupBordersVisible = false;
            this.group体育锻炼.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl锻炼方式,
            this.lbl每次锻炼时间,
            this.lbl坚持锻炼时间});
            this.group体育锻炼.Location = new System.Drawing.Point(184, 350);
            this.group体育锻炼.Name = "group体育锻炼";
            this.group体育锻炼.Size = new System.Drawing.Size(487, 48);
            this.group体育锻炼.Text = "group体育锻炼";
            // 
            // lbl锻炼方式
            // 
            this.lbl锻炼方式.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl锻炼方式.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl锻炼方式.Control = this.txt锻炼方式;
            this.lbl锻炼方式.CustomizationFormText = "锻炼方式";
            this.lbl锻炼方式.Location = new System.Drawing.Point(0, 24);
            this.lbl锻炼方式.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl锻炼方式.MinSize = new System.Drawing.Size(165, 24);
            this.lbl锻炼方式.Name = "lbl锻炼方式";
            this.lbl锻炼方式.Size = new System.Drawing.Size(487, 24);
            this.lbl锻炼方式.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl锻炼方式.Text = "锻炼方式";
            this.lbl锻炼方式.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl锻炼方式.TextSize = new System.Drawing.Size(100, 20);
            this.lbl锻炼方式.TextToControlDistance = 5;
            // 
            // lbl每次锻炼时间
            // 
            this.lbl每次锻炼时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl每次锻炼时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl每次锻炼时间.Control = this.txt每次锻炼时间;
            this.lbl每次锻炼时间.CustomizationFormText = "每次锻炼时间";
            this.lbl每次锻炼时间.Location = new System.Drawing.Point(0, 0);
            this.lbl每次锻炼时间.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl每次锻炼时间.MinSize = new System.Drawing.Size(165, 24);
            this.lbl每次锻炼时间.Name = "lbl每次锻炼时间";
            this.lbl每次锻炼时间.Size = new System.Drawing.Size(165, 24);
            this.lbl每次锻炼时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl每次锻炼时间.Text = "每次锻炼时间";
            this.lbl每次锻炼时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl每次锻炼时间.TextSize = new System.Drawing.Size(100, 20);
            this.lbl每次锻炼时间.TextToControlDistance = 5;
            // 
            // lbl坚持锻炼时间
            // 
            this.lbl坚持锻炼时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl坚持锻炼时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl坚持锻炼时间.Control = this.txt坚持锻炼时间;
            this.lbl坚持锻炼时间.CustomizationFormText = "坚持锻炼时间";
            this.lbl坚持锻炼时间.Location = new System.Drawing.Point(165, 0);
            this.lbl坚持锻炼时间.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl坚持锻炼时间.MinSize = new System.Drawing.Size(165, 24);
            this.lbl坚持锻炼时间.Name = "lbl坚持锻炼时间";
            this.lbl坚持锻炼时间.Size = new System.Drawing.Size(322, 24);
            this.lbl坚持锻炼时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl坚持锻炼时间.Text = "坚持锻炼时间";
            this.lbl坚持锻炼时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl坚持锻炼时间.TextSize = new System.Drawing.Size(120, 20);
            this.lbl坚持锻炼时间.TextToControlDistance = 5;
            // 
            // lbl口唇
            // 
            this.lbl口唇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl口唇.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl口唇.AppearanceItemCaption.Options.UseFont = true;
            this.lbl口唇.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl口唇.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl口唇.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl口唇.Control = this.txt口唇;
            this.lbl口唇.CustomizationFormText = "口唇";
            this.lbl口唇.Location = new System.Drawing.Point(184, 770);
            this.lbl口唇.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl口唇.MinSize = new System.Drawing.Size(133, 24);
            this.lbl口唇.Name = "lbl口唇";
            this.lbl口唇.Size = new System.Drawing.Size(487, 24);
            this.lbl口唇.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl口唇.Text = "口唇";
            this.lbl口唇.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl口唇.TextSize = new System.Drawing.Size(70, 20);
            this.lbl口唇.TextToControlDistance = 5;
            // 
            // lbl咽部
            // 
            this.lbl咽部.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl咽部.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl咽部.AppearanceItemCaption.Options.UseFont = true;
            this.lbl咽部.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl咽部.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl咽部.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl咽部.Control = this.txt咽部;
            this.lbl咽部.CustomizationFormText = "咽部";
            this.lbl咽部.Location = new System.Drawing.Point(184, 1020);
            this.lbl咽部.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl咽部.MinSize = new System.Drawing.Size(133, 24);
            this.lbl咽部.Name = "lbl咽部";
            this.lbl咽部.Size = new System.Drawing.Size(487, 24);
            this.lbl咽部.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl咽部.Text = "咽部";
            this.lbl咽部.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl咽部.TextSize = new System.Drawing.Size(70, 20);
            this.lbl咽部.TextToControlDistance = 5;
            // 
            // lbl视力
            // 
            this.lbl视力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl视力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl视力.AppearanceItemCaption.Options.UseFont = true;
            this.lbl视力.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl视力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl视力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl视力.Control = this.txt视力;
            this.lbl视力.CustomizationFormText = "视 力";
            this.lbl视力.Location = new System.Drawing.Point(92, 1044);
            this.lbl视力.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl视力.MinSize = new System.Drawing.Size(133, 24);
            this.lbl视力.Name = "lbl视力";
            this.lbl视力.Size = new System.Drawing.Size(579, 24);
            this.lbl视力.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl视力.Text = "视 力";
            this.lbl视力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl视力.TextSize = new System.Drawing.Size(80, 14);
            this.lbl视力.TextToControlDistance = 5;
            // 
            // lbl听力
            // 
            this.lbl听力.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl听力.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl听力.AppearanceItemCaption.Options.UseFont = true;
            this.lbl听力.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl听力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl听力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl听力.Control = this.txt听力;
            this.lbl听力.CustomizationFormText = "听 力";
            this.lbl听力.Location = new System.Drawing.Point(92, 1068);
            this.lbl听力.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl听力.MinSize = new System.Drawing.Size(133, 24);
            this.lbl听力.Name = "lbl听力";
            this.lbl听力.Size = new System.Drawing.Size(579, 24);
            this.lbl听力.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl听力.Text = "听 力";
            this.lbl听力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl听力.TextSize = new System.Drawing.Size(80, 20);
            this.lbl听力.TextToControlDistance = 5;
            // 
            // lbl运动功能
            // 
            this.lbl运动功能.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl运动功能.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl运动功能.AppearanceItemCaption.Options.UseFont = true;
            this.lbl运动功能.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl运动功能.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl运动功能.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl运动功能.Control = this.txt运动功能;
            this.lbl运动功能.CustomizationFormText = "运动功能";
            this.lbl运动功能.Location = new System.Drawing.Point(92, 1092);
            this.lbl运动功能.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl运动功能.MinSize = new System.Drawing.Size(133, 24);
            this.lbl运动功能.Name = "lbl运动功能";
            this.lbl运动功能.Size = new System.Drawing.Size(579, 24);
            this.lbl运动功能.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl运动功能.Text = "运动功能";
            this.lbl运动功能.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl运动功能.TextSize = new System.Drawing.Size(80, 20);
            this.lbl运动功能.TextToControlDistance = 5;
            // 
            // lbl眼底
            // 
            this.lbl眼底.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl眼底.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl眼底.Control = this.txt眼底;
            this.lbl眼底.CustomizationFormText = "眼 底*";
            this.lbl眼底.Location = new System.Drawing.Point(92, 1116);
            this.lbl眼底.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl眼底.MinSize = new System.Drawing.Size(133, 24);
            this.lbl眼底.Name = "lbl眼底";
            this.lbl眼底.Size = new System.Drawing.Size(579, 24);
            this.lbl眼底.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl眼底.Text = "眼 底*";
            this.lbl眼底.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl眼底.TextSize = new System.Drawing.Size(80, 20);
            this.lbl眼底.TextToControlDistance = 5;
            // 
            // lbl皮肤
            // 
            this.lbl皮肤.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl皮肤.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl皮肤.AppearanceItemCaption.Options.UseFont = true;
            this.lbl皮肤.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl皮肤.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl皮肤.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl皮肤.Control = this.txt皮肤;
            this.lbl皮肤.CustomizationFormText = "皮肤";
            this.lbl皮肤.Location = new System.Drawing.Point(92, 1140);
            this.lbl皮肤.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl皮肤.MinSize = new System.Drawing.Size(133, 24);
            this.lbl皮肤.Name = "lbl皮肤";
            this.lbl皮肤.Size = new System.Drawing.Size(579, 24);
            this.lbl皮肤.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl皮肤.Text = "皮肤";
            this.lbl皮肤.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl皮肤.TextSize = new System.Drawing.Size(80, 20);
            this.lbl皮肤.TextToControlDistance = 5;
            // 
            // lbl巩膜
            // 
            this.lbl巩膜.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl巩膜.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl巩膜.AppearanceItemCaption.Options.UseFont = true;
            this.lbl巩膜.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl巩膜.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl巩膜.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl巩膜.Control = this.txt巩膜;
            this.lbl巩膜.CustomizationFormText = "巩 膜";
            this.lbl巩膜.Location = new System.Drawing.Point(92, 1164);
            this.lbl巩膜.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl巩膜.MinSize = new System.Drawing.Size(133, 24);
            this.lbl巩膜.Name = "lbl巩膜";
            this.lbl巩膜.Size = new System.Drawing.Size(579, 24);
            this.lbl巩膜.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl巩膜.Text = "巩 膜";
            this.lbl巩膜.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl巩膜.TextSize = new System.Drawing.Size(80, 20);
            this.lbl巩膜.TextToControlDistance = 5;
            // 
            // lbl淋巴结
            // 
            this.lbl淋巴结.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl淋巴结.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl淋巴结.AppearanceItemCaption.Options.UseFont = true;
            this.lbl淋巴结.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl淋巴结.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl淋巴结.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl淋巴结.Control = this.txt淋巴结;
            this.lbl淋巴结.CustomizationFormText = "淋巴结";
            this.lbl淋巴结.Location = new System.Drawing.Point(92, 1188);
            this.lbl淋巴结.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl淋巴结.MinSize = new System.Drawing.Size(133, 24);
            this.lbl淋巴结.Name = "lbl淋巴结";
            this.lbl淋巴结.Size = new System.Drawing.Size(579, 24);
            this.lbl淋巴结.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl淋巴结.Text = "淋巴结";
            this.lbl淋巴结.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl淋巴结.TextSize = new System.Drawing.Size(80, 20);
            this.lbl淋巴结.TextToControlDistance = 5;
            // 
            // lbl桶状胸
            // 
            this.lbl桶状胸.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl桶状胸.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl桶状胸.AppearanceItemCaption.Options.UseFont = true;
            this.lbl桶状胸.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl桶状胸.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl桶状胸.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl桶状胸.Control = this.txt桶状胸;
            this.lbl桶状胸.CustomizationFormText = "桶状胸：";
            this.lbl桶状胸.Location = new System.Drawing.Point(184, 1212);
            this.lbl桶状胸.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl桶状胸.MinSize = new System.Drawing.Size(133, 24);
            this.lbl桶状胸.Name = "lbl桶状胸";
            this.lbl桶状胸.Size = new System.Drawing.Size(487, 24);
            this.lbl桶状胸.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl桶状胸.Text = "桶状胸：";
            this.lbl桶状胸.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl桶状胸.TextSize = new System.Drawing.Size(100, 20);
            this.lbl桶状胸.TextToControlDistance = 5;
            // 
            // lbl罗音
            // 
            this.lbl罗音.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl罗音.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl罗音.AppearanceItemCaption.Options.UseFont = true;
            this.lbl罗音.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl罗音.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl罗音.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl罗音.Control = this.txt罗音;
            this.lbl罗音.CustomizationFormText = "罗 音：";
            this.lbl罗音.Location = new System.Drawing.Point(184, 1260);
            this.lbl罗音.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl罗音.MinSize = new System.Drawing.Size(133, 24);
            this.lbl罗音.Name = "lbl罗音";
            this.lbl罗音.Size = new System.Drawing.Size(487, 24);
            this.lbl罗音.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl罗音.Text = "罗 音：";
            this.lbl罗音.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl罗音.TextSize = new System.Drawing.Size(100, 20);
            this.lbl罗音.TextToControlDistance = 5;
            // 
            // lbl呼吸音
            // 
            this.lbl呼吸音.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl呼吸音.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl呼吸音.AppearanceItemCaption.Options.UseFont = true;
            this.lbl呼吸音.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl呼吸音.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl呼吸音.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl呼吸音.Control = this.txt呼吸音;
            this.lbl呼吸音.CustomizationFormText = "layoutControlItem18";
            this.lbl呼吸音.Location = new System.Drawing.Point(184, 1236);
            this.lbl呼吸音.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl呼吸音.MinSize = new System.Drawing.Size(133, 24);
            this.lbl呼吸音.Name = "lbl呼吸音";
            this.lbl呼吸音.Size = new System.Drawing.Size(487, 24);
            this.lbl呼吸音.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl呼吸音.Text = "呼吸音：";
            this.lbl呼吸音.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl呼吸音.TextSize = new System.Drawing.Size(100, 14);
            this.lbl呼吸音.TextToControlDistance = 5;
            // 
            // lbl心率
            // 
            this.lbl心率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl心率.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl心率.AppearanceItemCaption.Options.UseFont = true;
            this.lbl心率.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl心率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl心率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl心率.Control = this.txt心率;
            this.lbl心率.CustomizationFormText = "心率：";
            this.lbl心率.Location = new System.Drawing.Point(184, 1284);
            this.lbl心率.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl心率.MinSize = new System.Drawing.Size(133, 24);
            this.lbl心率.Name = "lbl心率";
            this.lbl心率.Size = new System.Drawing.Size(487, 24);
            this.lbl心率.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl心率.Text = "心率：";
            this.lbl心率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl心率.TextSize = new System.Drawing.Size(100, 20);
            this.lbl心率.TextToControlDistance = 5;
            // 
            // lbl心律
            // 
            this.lbl心律.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl心律.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl心律.AppearanceItemCaption.Options.UseFont = true;
            this.lbl心律.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl心律.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl心律.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl心律.Control = this.txt心律;
            this.lbl心律.CustomizationFormText = "心律：";
            this.lbl心律.Location = new System.Drawing.Point(184, 1308);
            this.lbl心律.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl心律.MinSize = new System.Drawing.Size(133, 24);
            this.lbl心律.Name = "lbl心律";
            this.lbl心律.Size = new System.Drawing.Size(487, 24);
            this.lbl心律.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl心律.Text = "心律：";
            this.lbl心律.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl心律.TextSize = new System.Drawing.Size(100, 20);
            this.lbl心律.TextToControlDistance = 5;
            // 
            // lbl杂音
            // 
            this.lbl杂音.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl杂音.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl杂音.AppearanceItemCaption.Options.UseFont = true;
            this.lbl杂音.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl杂音.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl杂音.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl杂音.Control = this.txt杂音;
            this.lbl杂音.CustomizationFormText = "杂音：";
            this.lbl杂音.Location = new System.Drawing.Point(184, 1332);
            this.lbl杂音.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl杂音.MinSize = new System.Drawing.Size(133, 24);
            this.lbl杂音.Name = "lbl杂音";
            this.lbl杂音.Size = new System.Drawing.Size(487, 24);
            this.lbl杂音.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl杂音.Text = "杂音：";
            this.lbl杂音.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl杂音.TextSize = new System.Drawing.Size(100, 20);
            this.lbl杂音.TextToControlDistance = 5;
            // 
            // lbl压痛
            // 
            this.lbl压痛.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl压痛.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl压痛.AppearanceItemCaption.Options.UseFont = true;
            this.lbl压痛.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl压痛.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl压痛.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl压痛.Control = this.txt压痛;
            this.lbl压痛.CustomizationFormText = "压痛：";
            this.lbl压痛.Location = new System.Drawing.Point(184, 1356);
            this.lbl压痛.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl压痛.MinSize = new System.Drawing.Size(133, 24);
            this.lbl压痛.Name = "lbl压痛";
            this.lbl压痛.Size = new System.Drawing.Size(487, 24);
            this.lbl压痛.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl压痛.Text = "压痛：";
            this.lbl压痛.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl压痛.TextSize = new System.Drawing.Size(100, 20);
            this.lbl压痛.TextToControlDistance = 5;
            // 
            // lbl移动性浊音
            // 
            this.lbl移动性浊音.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl移动性浊音.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl移动性浊音.AppearanceItemCaption.Options.UseFont = true;
            this.lbl移动性浊音.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl移动性浊音.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl移动性浊音.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl移动性浊音.Control = this.txt移动性浊音;
            this.lbl移动性浊音.CustomizationFormText = "移动性浊音：";
            this.lbl移动性浊音.Location = new System.Drawing.Point(184, 1452);
            this.lbl移动性浊音.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl移动性浊音.MinSize = new System.Drawing.Size(133, 24);
            this.lbl移动性浊音.Name = "lbl移动性浊音";
            this.lbl移动性浊音.Size = new System.Drawing.Size(487, 24);
            this.lbl移动性浊音.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl移动性浊音.Text = "移动性浊音：";
            this.lbl移动性浊音.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl移动性浊音.TextSize = new System.Drawing.Size(100, 20);
            this.lbl移动性浊音.TextToControlDistance = 5;
            // 
            // lbl包块
            // 
            this.lbl包块.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl包块.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl包块.AppearanceItemCaption.Options.UseFont = true;
            this.lbl包块.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl包块.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl包块.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl包块.Control = this.txt包块;
            this.lbl包块.CustomizationFormText = "包块：";
            this.lbl包块.Location = new System.Drawing.Point(184, 1380);
            this.lbl包块.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl包块.MinSize = new System.Drawing.Size(133, 24);
            this.lbl包块.Name = "lbl包块";
            this.lbl包块.Size = new System.Drawing.Size(487, 24);
            this.lbl包块.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl包块.Text = "包块：";
            this.lbl包块.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl包块.TextSize = new System.Drawing.Size(100, 20);
            this.lbl包块.TextToControlDistance = 5;
            // 
            // lbl肝大
            // 
            this.lbl肝大.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl肝大.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl肝大.AppearanceItemCaption.Options.UseFont = true;
            this.lbl肝大.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl肝大.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl肝大.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl肝大.Control = this.txt肝大;
            this.lbl肝大.CustomizationFormText = "肝大：";
            this.lbl肝大.Location = new System.Drawing.Point(184, 1404);
            this.lbl肝大.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl肝大.MinSize = new System.Drawing.Size(133, 24);
            this.lbl肝大.Name = "lbl肝大";
            this.lbl肝大.Size = new System.Drawing.Size(487, 24);
            this.lbl肝大.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl肝大.Text = "肝大：";
            this.lbl肝大.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl肝大.TextSize = new System.Drawing.Size(100, 20);
            this.lbl肝大.TextToControlDistance = 5;
            // 
            // lbl脾大
            // 
            this.lbl脾大.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.False;
            this.lbl脾大.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl脾大.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl脾大.AppearanceItemCaption.Options.UseFont = true;
            this.lbl脾大.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl脾大.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl脾大.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl脾大.Control = this.txt脾大;
            this.lbl脾大.CustomizationFormText = "脾大：";
            this.lbl脾大.Location = new System.Drawing.Point(184, 1428);
            this.lbl脾大.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl脾大.MinSize = new System.Drawing.Size(133, 24);
            this.lbl脾大.Name = "lbl脾大";
            this.lbl脾大.Size = new System.Drawing.Size(487, 24);
            this.lbl脾大.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl脾大.Text = "脾大：";
            this.lbl脾大.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl脾大.TextSize = new System.Drawing.Size(100, 20);
            this.lbl脾大.TextToControlDistance = 5;
            // 
            // lbl下肢水肿
            // 
            this.lbl下肢水肿.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl下肢水肿.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl下肢水肿.AppearanceItemCaption.Options.UseFont = true;
            this.lbl下肢水肿.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl下肢水肿.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl下肢水肿.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl下肢水肿.Control = this.txt下肢水肿;
            this.lbl下肢水肿.CustomizationFormText = "下肢水肿";
            this.lbl下肢水肿.Location = new System.Drawing.Point(92, 1476);
            this.lbl下肢水肿.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl下肢水肿.MinSize = new System.Drawing.Size(133, 24);
            this.lbl下肢水肿.Name = "lbl下肢水肿";
            this.lbl下肢水肿.Size = new System.Drawing.Size(579, 24);
            this.lbl下肢水肿.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl下肢水肿.Text = "下肢水肿";
            this.lbl下肢水肿.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl下肢水肿.TextSize = new System.Drawing.Size(80, 20);
            this.lbl下肢水肿.TextToControlDistance = 5;
            // 
            // lbl查体其他
            // 
            this.lbl查体其他.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl查体其他.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl查体其他.Control = this.txt查体其他;
            this.lbl查体其他.CustomizationFormText = "查体其他* ";
            this.lbl查体其他.Location = new System.Drawing.Point(92, 1692);
            this.lbl查体其他.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl查体其他.MinSize = new System.Drawing.Size(133, 24);
            this.lbl查体其他.Name = "lbl查体其他";
            this.lbl查体其他.Size = new System.Drawing.Size(579, 24);
            this.lbl查体其他.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl查体其他.Text = "查体其他* ";
            this.lbl查体其他.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl查体其他.TextSize = new System.Drawing.Size(80, 14);
            this.lbl查体其他.TextToControlDistance = 5;
            // 
            // lbl足背动脉搏动
            // 
            this.lbl足背动脉搏动.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl足背动脉搏动.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl足背动脉搏动.AppearanceItemCaption.Options.UseFont = true;
            this.lbl足背动脉搏动.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl足背动脉搏动.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl足背动脉搏动.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl足背动脉搏动.Control = this.txt足背动脉搏动;
            this.lbl足背动脉搏动.CustomizationFormText = "足背动脉搏动";
            this.lbl足背动脉搏动.Location = new System.Drawing.Point(92, 1500);
            this.lbl足背动脉搏动.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl足背动脉搏动.MinSize = new System.Drawing.Size(133, 24);
            this.lbl足背动脉搏动.Name = "lbl足背动脉搏动";
            this.lbl足背动脉搏动.Size = new System.Drawing.Size(579, 24);
            this.lbl足背动脉搏动.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl足背动脉搏动.Text = "足背动脉搏动";
            this.lbl足背动脉搏动.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl足背动脉搏动.TextSize = new System.Drawing.Size(80, 20);
            this.lbl足背动脉搏动.TextToControlDistance = 5;
            // 
            // lbl肛门指诊
            // 
            this.lbl肛门指诊.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl肛门指诊.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl肛门指诊.Control = this.txt肛门指诊;
            this.lbl肛门指诊.CustomizationFormText = "肛门指诊*";
            this.lbl肛门指诊.Location = new System.Drawing.Point(92, 1524);
            this.lbl肛门指诊.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl肛门指诊.MinSize = new System.Drawing.Size(133, 24);
            this.lbl肛门指诊.Name = "lbl肛门指诊";
            this.lbl肛门指诊.Size = new System.Drawing.Size(579, 24);
            this.lbl肛门指诊.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl肛门指诊.Text = "肛门指诊*";
            this.lbl肛门指诊.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl肛门指诊.TextSize = new System.Drawing.Size(80, 20);
            this.lbl肛门指诊.TextToControlDistance = 5;
            // 
            // lbl乳腺
            // 
            this.lbl乳腺.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl乳腺.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl乳腺.Control = this.txt乳腺;
            this.lbl乳腺.CustomizationFormText = "乳 腺*";
            this.lbl乳腺.Location = new System.Drawing.Point(92, 1548);
            this.lbl乳腺.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl乳腺.MinSize = new System.Drawing.Size(133, 24);
            this.lbl乳腺.Name = "lbl乳腺";
            this.lbl乳腺.Size = new System.Drawing.Size(579, 24);
            this.lbl乳腺.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl乳腺.Text = "乳 腺*";
            this.lbl乳腺.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl乳腺.TextSize = new System.Drawing.Size(80, 20);
            this.lbl乳腺.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem16.Control = this.txt血红蛋白;
            this.layoutControlItem16.CustomizationFormText = "血红蛋白";
            this.layoutControlItem16.Location = new System.Drawing.Point(184, 1741);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(164, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(487, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "血红蛋白";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // lbl血常规其他
            // 
            this.lbl血常规其他.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血常规其他.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血常规其他.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血常规其他.Control = this.txt血常规其他;
            this.lbl血常规其他.CustomizationFormText = "其他";
            this.lbl血常规其他.Location = new System.Drawing.Point(184, 1813);
            this.lbl血常规其他.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血常规其他.MinSize = new System.Drawing.Size(164, 24);
            this.lbl血常规其他.Name = "lbl血常规其他";
            this.lbl血常规其他.Size = new System.Drawing.Size(487, 24);
            this.lbl血常规其他.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血常规其他.Text = "其他";
            this.lbl血常规其他.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血常规其他.TextSize = new System.Drawing.Size(100, 20);
            this.lbl血常规其他.TextToControlDistance = 5;
            // 
            // lbl白细胞
            // 
            this.lbl白细胞.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl白细胞.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl白细胞.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl白细胞.Control = this.txt白细胞;
            this.lbl白细胞.CustomizationFormText = "白细胞";
            this.lbl白细胞.Location = new System.Drawing.Point(184, 1765);
            this.lbl白细胞.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl白细胞.MinSize = new System.Drawing.Size(164, 24);
            this.lbl白细胞.Name = "lbl白细胞";
            this.lbl白细胞.Size = new System.Drawing.Size(487, 24);
            this.lbl白细胞.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl白细胞.Text = "白细胞";
            this.lbl白细胞.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl白细胞.TextSize = new System.Drawing.Size(100, 20);
            this.lbl白细胞.TextToControlDistance = 5;
            // 
            // lbl血小板
            // 
            this.lbl血小板.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血小板.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血小板.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血小板.Control = this.txt血小板;
            this.lbl血小板.CustomizationFormText = "血小板";
            this.lbl血小板.Location = new System.Drawing.Point(184, 1789);
            this.lbl血小板.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血小板.MinSize = new System.Drawing.Size(164, 24);
            this.lbl血小板.Name = "lbl血小板";
            this.lbl血小板.Size = new System.Drawing.Size(487, 24);
            this.lbl血小板.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血小板.Text = "血小板";
            this.lbl血小板.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血小板.TextSize = new System.Drawing.Size(100, 20);
            this.lbl血小板.TextToControlDistance = 5;
            // 
            // lbl尿常规
            // 
            this.lbl尿常规.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl尿常规.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl尿常规.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl尿常规.Control = this.txt尿常规;
            this.lbl尿常规.CustomizationFormText = "尿常规* ";
            this.lbl尿常规.Location = new System.Drawing.Point(92, 1837);
            this.lbl尿常规.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl尿常规.MinSize = new System.Drawing.Size(164, 24);
            this.lbl尿常规.Name = "lbl尿常规";
            this.lbl尿常规.Size = new System.Drawing.Size(579, 24);
            this.lbl尿常规.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl尿常规.Text = "尿常规* ";
            this.lbl尿常规.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl尿常规.TextSize = new System.Drawing.Size(80, 14);
            this.lbl尿常规.TextToControlDistance = 5;
            // 
            // lbl空腹血糖
            // 
            this.lbl空腹血糖.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl空腹血糖.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl空腹血糖.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl空腹血糖.Control = this.txt空腹血糖;
            this.lbl空腹血糖.CustomizationFormText = "空腹血糖*";
            this.lbl空腹血糖.Location = new System.Drawing.Point(92, 1861);
            this.lbl空腹血糖.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl空腹血糖.MinSize = new System.Drawing.Size(164, 24);
            this.lbl空腹血糖.Name = "lbl空腹血糖";
            this.lbl空腹血糖.Size = new System.Drawing.Size(285, 24);
            this.lbl空腹血糖.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl空腹血糖.Text = "空腹血糖*";
            this.lbl空腹血糖.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl空腹血糖.TextSize = new System.Drawing.Size(80, 20);
            this.lbl空腹血糖.TextToControlDistance = 5;
            // 
            // lbl心电图
            // 
            this.lbl心电图.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl心电图.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl心电图.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl心电图.Control = this.txt心电图;
            this.lbl心电图.CustomizationFormText = "心电图*";
            this.lbl心电图.Location = new System.Drawing.Point(92, 1909);
            this.lbl心电图.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl心电图.MinSize = new System.Drawing.Size(164, 24);
            this.lbl心电图.Name = "lbl心电图";
            this.lbl心电图.Size = new System.Drawing.Size(579, 24);
            this.lbl心电图.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl心电图.Text = "心电图*";
            this.lbl心电图.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl心电图.TextSize = new System.Drawing.Size(80, 20);
            this.lbl心电图.TextToControlDistance = 5;
            // 
            // lbl尿微量白蛋白
            // 
            this.lbl尿微量白蛋白.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl尿微量白蛋白.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl尿微量白蛋白.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl尿微量白蛋白.Control = this.txt尿微量白蛋白;
            this.lbl尿微量白蛋白.CustomizationFormText = "layoutControlItem18";
            this.lbl尿微量白蛋白.Location = new System.Drawing.Point(92, 1933);
            this.lbl尿微量白蛋白.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl尿微量白蛋白.MinSize = new System.Drawing.Size(164, 24);
            this.lbl尿微量白蛋白.Name = "lbl尿微量白蛋白";
            this.lbl尿微量白蛋白.Size = new System.Drawing.Size(579, 24);
            this.lbl尿微量白蛋白.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl尿微量白蛋白.Text = "尿微量白蛋白*  ";
            this.lbl尿微量白蛋白.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl尿微量白蛋白.TextSize = new System.Drawing.Size(80, 14);
            this.lbl尿微量白蛋白.TextToControlDistance = 5;
            // 
            // lbl乙型肝炎表面抗原
            // 
            this.lbl乙型肝炎表面抗原.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl乙型肝炎表面抗原.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl乙型肝炎表面抗原.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl乙型肝炎表面抗原.Control = this.txt乙型肝炎表面抗原;
            this.lbl乙型肝炎表面抗原.CustomizationFormText = "乙型肝炎表面抗原* ";
            this.lbl乙型肝炎表面抗原.Location = new System.Drawing.Point(92, 2005);
            this.lbl乙型肝炎表面抗原.MinSize = new System.Drawing.Size(50, 40);
            this.lbl乙型肝炎表面抗原.Name = "lbl乙型肝炎表面抗原";
            this.lbl乙型肝炎表面抗原.Size = new System.Drawing.Size(579, 40);
            this.lbl乙型肝炎表面抗原.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl乙型肝炎表面抗原.Text = "乙型肝炎表面抗原* ";
            this.lbl乙型肝炎表面抗原.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl乙型肝炎表面抗原.TextSize = new System.Drawing.Size(80, 14);
            this.lbl乙型肝炎表面抗原.TextToControlDistance = 5;
            // 
            // lbl大便潜血
            // 
            this.lbl大便潜血.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl大便潜血.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl大便潜血.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl大便潜血.Control = this.txt大便潜血;
            this.lbl大便潜血.CustomizationFormText = "大便潜血*";
            this.lbl大便潜血.Location = new System.Drawing.Point(92, 1957);
            this.lbl大便潜血.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl大便潜血.MinSize = new System.Drawing.Size(164, 24);
            this.lbl大便潜血.Name = "lbl大便潜血";
            this.lbl大便潜血.Size = new System.Drawing.Size(579, 24);
            this.lbl大便潜血.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl大便潜血.Text = "大便潜血*";
            this.lbl大便潜血.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl大便潜血.TextSize = new System.Drawing.Size(80, 20);
            this.lbl大便潜血.TextToControlDistance = 5;
            // 
            // lbl糖化血红蛋白
            // 
            this.lbl糖化血红蛋白.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl糖化血红蛋白.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl糖化血红蛋白.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl糖化血红蛋白.Control = this.txt糖化血红蛋白;
            this.lbl糖化血红蛋白.CustomizationFormText = "糖化血红蛋白*";
            this.lbl糖化血红蛋白.Location = new System.Drawing.Point(92, 1981);
            this.lbl糖化血红蛋白.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl糖化血红蛋白.MinSize = new System.Drawing.Size(164, 24);
            this.lbl糖化血红蛋白.Name = "lbl糖化血红蛋白";
            this.lbl糖化血红蛋白.Size = new System.Drawing.Size(579, 24);
            this.lbl糖化血红蛋白.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl糖化血红蛋白.Text = "糖化血红蛋白*";
            this.lbl糖化血红蛋白.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl糖化血红蛋白.TextSize = new System.Drawing.Size(80, 20);
            this.lbl糖化血红蛋白.TextToControlDistance = 5;
            // 
            // lbl血清谷丙转氨酶
            // 
            this.lbl血清谷丙转氨酶.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血清谷丙转氨酶.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血清谷丙转氨酶.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血清谷丙转氨酶.Control = this.txt血清谷丙转氨酶;
            this.lbl血清谷丙转氨酶.CustomizationFormText = "血清谷丙转氨酶";
            this.lbl血清谷丙转氨酶.Location = new System.Drawing.Point(184, 2045);
            this.lbl血清谷丙转氨酶.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血清谷丙转氨酶.MinSize = new System.Drawing.Size(189, 24);
            this.lbl血清谷丙转氨酶.Name = "lbl血清谷丙转氨酶";
            this.lbl血清谷丙转氨酶.Size = new System.Drawing.Size(487, 24);
            this.lbl血清谷丙转氨酶.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血清谷丙转氨酶.Text = "血清谷丙转氨酶";
            this.lbl血清谷丙转氨酶.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血清谷丙转氨酶.TextSize = new System.Drawing.Size(140, 20);
            this.lbl血清谷丙转氨酶.TextToControlDistance = 5;
            // 
            // lbl结合胆红素
            // 
            this.lbl结合胆红素.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl结合胆红素.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl结合胆红素.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl结合胆红素.Control = this.txt结合胆红素;
            this.lbl结合胆红素.CustomizationFormText = "结合胆红素 ";
            this.lbl结合胆红素.Location = new System.Drawing.Point(184, 2141);
            this.lbl结合胆红素.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl结合胆红素.MinSize = new System.Drawing.Size(189, 24);
            this.lbl结合胆红素.Name = "lbl结合胆红素";
            this.lbl结合胆红素.Size = new System.Drawing.Size(487, 24);
            this.lbl结合胆红素.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl结合胆红素.Text = "结合胆红素 ";
            this.lbl结合胆红素.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl结合胆红素.TextSize = new System.Drawing.Size(140, 14);
            this.lbl结合胆红素.TextToControlDistance = 5;
            // 
            // lbl血清谷草转氨酶
            // 
            this.lbl血清谷草转氨酶.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血清谷草转氨酶.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血清谷草转氨酶.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血清谷草转氨酶.Control = this.txt血清谷草转氨酶;
            this.lbl血清谷草转氨酶.CustomizationFormText = "血清谷草转氨酶";
            this.lbl血清谷草转氨酶.Location = new System.Drawing.Point(184, 2069);
            this.lbl血清谷草转氨酶.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血清谷草转氨酶.MinSize = new System.Drawing.Size(189, 24);
            this.lbl血清谷草转氨酶.Name = "lbl血清谷草转氨酶";
            this.lbl血清谷草转氨酶.Size = new System.Drawing.Size(487, 24);
            this.lbl血清谷草转氨酶.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血清谷草转氨酶.Text = "血清谷草转氨酶";
            this.lbl血清谷草转氨酶.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血清谷草转氨酶.TextSize = new System.Drawing.Size(140, 20);
            this.lbl血清谷草转氨酶.TextToControlDistance = 5;
            // 
            // lbl总胆红素
            // 
            this.lbl总胆红素.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl总胆红素.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl总胆红素.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl总胆红素.Control = this.txt总胆红素;
            this.lbl总胆红素.CustomizationFormText = "总胆红素";
            this.lbl总胆红素.Location = new System.Drawing.Point(184, 2117);
            this.lbl总胆红素.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl总胆红素.MinSize = new System.Drawing.Size(189, 24);
            this.lbl总胆红素.Name = "lbl总胆红素";
            this.lbl总胆红素.Size = new System.Drawing.Size(487, 24);
            this.lbl总胆红素.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl总胆红素.Text = "总胆红素";
            this.lbl总胆红素.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl总胆红素.TextSize = new System.Drawing.Size(140, 20);
            this.lbl总胆红素.TextToControlDistance = 5;
            // 
            // lbl白蛋白
            // 
            this.lbl白蛋白.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl白蛋白.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl白蛋白.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl白蛋白.Control = this.txt白蛋白;
            this.lbl白蛋白.CustomizationFormText = "白蛋白";
            this.lbl白蛋白.Location = new System.Drawing.Point(184, 2093);
            this.lbl白蛋白.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl白蛋白.MinSize = new System.Drawing.Size(189, 24);
            this.lbl白蛋白.Name = "lbl白蛋白";
            this.lbl白蛋白.Size = new System.Drawing.Size(487, 24);
            this.lbl白蛋白.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl白蛋白.Text = "白蛋白";
            this.lbl白蛋白.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl白蛋白.TextSize = new System.Drawing.Size(140, 20);
            this.lbl白蛋白.TextToControlDistance = 5;
            // 
            // lbl血清肌酐
            // 
            this.lbl血清肌酐.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血清肌酐.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血清肌酐.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血清肌酐.Control = this.txt血清肌酐;
            this.lbl血清肌酐.CustomizationFormText = "血清肌酐";
            this.lbl血清肌酐.Location = new System.Drawing.Point(184, 2165);
            this.lbl血清肌酐.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血清肌酐.MinSize = new System.Drawing.Size(189, 24);
            this.lbl血清肌酐.Name = "lbl血清肌酐";
            this.lbl血清肌酐.Size = new System.Drawing.Size(487, 24);
            this.lbl血清肌酐.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血清肌酐.Text = "血清肌酐";
            this.lbl血清肌酐.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血清肌酐.TextSize = new System.Drawing.Size(140, 20);
            this.lbl血清肌酐.TextToControlDistance = 5;
            // 
            // lbl血钠浓度
            // 
            this.lbl血钠浓度.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血钠浓度.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血钠浓度.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血钠浓度.Control = this.txt血钠浓度;
            this.lbl血钠浓度.CustomizationFormText = "血钠浓度";
            this.lbl血钠浓度.Location = new System.Drawing.Point(184, 2237);
            this.lbl血钠浓度.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血钠浓度.MinSize = new System.Drawing.Size(189, 24);
            this.lbl血钠浓度.Name = "lbl血钠浓度";
            this.lbl血钠浓度.Size = new System.Drawing.Size(487, 24);
            this.lbl血钠浓度.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血钠浓度.Text = "血钠浓度";
            this.lbl血钠浓度.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血钠浓度.TextSize = new System.Drawing.Size(140, 20);
            this.lbl血钠浓度.TextToControlDistance = 5;
            // 
            // lbl血尿素氮
            // 
            this.lbl血尿素氮.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血尿素氮.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血尿素氮.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血尿素氮.Control = this.txt血尿素氮;
            this.lbl血尿素氮.CustomizationFormText = "血尿素氮";
            this.lbl血尿素氮.Location = new System.Drawing.Point(184, 2189);
            this.lbl血尿素氮.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血尿素氮.MinSize = new System.Drawing.Size(189, 24);
            this.lbl血尿素氮.Name = "lbl血尿素氮";
            this.lbl血尿素氮.Size = new System.Drawing.Size(487, 24);
            this.lbl血尿素氮.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血尿素氮.Text = "血尿素氮";
            this.lbl血尿素氮.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血尿素氮.TextSize = new System.Drawing.Size(140, 20);
            this.lbl血尿素氮.TextToControlDistance = 5;
            // 
            // lbl血钾浓度
            // 
            this.lbl血钾浓度.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血钾浓度.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血钾浓度.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血钾浓度.Control = this.txt血钾浓度;
            this.lbl血钾浓度.CustomizationFormText = "血钾浓度";
            this.lbl血钾浓度.Location = new System.Drawing.Point(184, 2213);
            this.lbl血钾浓度.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血钾浓度.MinSize = new System.Drawing.Size(189, 24);
            this.lbl血钾浓度.Name = "lbl血钾浓度";
            this.lbl血钾浓度.Size = new System.Drawing.Size(487, 24);
            this.lbl血钾浓度.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血钾浓度.Text = "血钾浓度";
            this.lbl血钾浓度.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血钾浓度.TextSize = new System.Drawing.Size(140, 20);
            this.lbl血钾浓度.TextToControlDistance = 5;
            // 
            // lbl总胆固醇
            // 
            this.lbl总胆固醇.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl总胆固醇.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl总胆固醇.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl总胆固醇.Control = this.txt总胆固醇;
            this.lbl总胆固醇.CustomizationFormText = "总胆固醇";
            this.lbl总胆固醇.Location = new System.Drawing.Point(184, 2261);
            this.lbl总胆固醇.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl总胆固醇.MinSize = new System.Drawing.Size(189, 24);
            this.lbl总胆固醇.Name = "lbl总胆固醇";
            this.lbl总胆固醇.Size = new System.Drawing.Size(487, 24);
            this.lbl总胆固醇.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl总胆固醇.Text = "总胆固醇";
            this.lbl总胆固醇.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl总胆固醇.TextSize = new System.Drawing.Size(140, 20);
            this.lbl总胆固醇.TextToControlDistance = 5;
            // 
            // lbl甘油三酯
            // 
            this.lbl甘油三酯.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl甘油三酯.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl甘油三酯.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl甘油三酯.Control = this.txt甘油三酯;
            this.lbl甘油三酯.CustomizationFormText = "甘油三酯";
            this.lbl甘油三酯.Location = new System.Drawing.Point(184, 2285);
            this.lbl甘油三酯.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl甘油三酯.MinSize = new System.Drawing.Size(189, 24);
            this.lbl甘油三酯.Name = "lbl甘油三酯";
            this.lbl甘油三酯.Size = new System.Drawing.Size(487, 24);
            this.lbl甘油三酯.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl甘油三酯.Text = "甘油三酯";
            this.lbl甘油三酯.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl甘油三酯.TextSize = new System.Drawing.Size(140, 20);
            this.lbl甘油三酯.TextToControlDistance = 5;
            // 
            // lbl血清低密度脂蛋白胆固醇
            // 
            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血清低密度脂蛋白胆固醇.Control = this.txt血清低密度脂蛋白胆固醇;
            this.lbl血清低密度脂蛋白胆固醇.CustomizationFormText = "血清低密度脂蛋白胆固醇";
            this.lbl血清低密度脂蛋白胆固醇.Location = new System.Drawing.Point(184, 2309);
            this.lbl血清低密度脂蛋白胆固醇.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血清低密度脂蛋白胆固醇.MinSize = new System.Drawing.Size(189, 24);
            this.lbl血清低密度脂蛋白胆固醇.Name = "lbl血清低密度脂蛋白胆固醇";
            this.lbl血清低密度脂蛋白胆固醇.Size = new System.Drawing.Size(487, 24);
            this.lbl血清低密度脂蛋白胆固醇.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血清低密度脂蛋白胆固醇.Text = "血清低密度脂蛋白胆固醇";
            this.lbl血清低密度脂蛋白胆固醇.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血清低密度脂蛋白胆固醇.TextSize = new System.Drawing.Size(140, 20);
            this.lbl血清低密度脂蛋白胆固醇.TextToControlDistance = 5;
            // 
            // lbl血清高密度脂蛋白胆固醇
            // 
            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血清高密度脂蛋白胆固醇.Control = this.txt血清高密度脂蛋白胆固醇;
            this.lbl血清高密度脂蛋白胆固醇.CustomizationFormText = "血清高密度脂蛋白胆固醇";
            this.lbl血清高密度脂蛋白胆固醇.Location = new System.Drawing.Point(184, 2333);
            this.lbl血清高密度脂蛋白胆固醇.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血清高密度脂蛋白胆固醇.MinSize = new System.Drawing.Size(189, 24);
            this.lbl血清高密度脂蛋白胆固醇.Name = "lbl血清高密度脂蛋白胆固醇";
            this.lbl血清高密度脂蛋白胆固醇.Size = new System.Drawing.Size(487, 24);
            this.lbl血清高密度脂蛋白胆固醇.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血清高密度脂蛋白胆固醇.Text = "血清高密度脂蛋白胆固醇";
            this.lbl血清高密度脂蛋白胆固醇.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血清高密度脂蛋白胆固醇.TextSize = new System.Drawing.Size(140, 20);
            this.lbl血清高密度脂蛋白胆固醇.TextToControlDistance = 5;
            // 
            // lbl胸部X线片
            // 
            this.lbl胸部X线片.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl胸部X线片.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl胸部X线片.Control = this.txt胸部X线片;
            this.lbl胸部X线片.CustomizationFormText = "胸部X线片*";
            this.lbl胸部X线片.Location = new System.Drawing.Point(92, 2357);
            this.lbl胸部X线片.Name = "lbl胸部X线片";
            this.lbl胸部X线片.Size = new System.Drawing.Size(579, 24);
            this.lbl胸部X线片.Text = "胸部X线片*";
            this.lbl胸部X线片.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl胸部X线片.TextSize = new System.Drawing.Size(80, 20);
            this.lbl胸部X线片.TextToControlDistance = 5;
            // 
            // lbl辅助检查其他
            // 
            this.lbl辅助检查其他.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl辅助检查其他.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl辅助检查其他.Control = this.txt辅助检查其他;
            this.lbl辅助检查其他.CustomizationFormText = "其 他* ";
            this.lbl辅助检查其他.Location = new System.Drawing.Point(92, 2455);
            this.lbl辅助检查其他.Name = "lbl辅助检查其他";
            this.lbl辅助检查其他.Size = new System.Drawing.Size(579, 24);
            this.lbl辅助检查其他.Text = "其 他* ";
            this.lbl辅助检查其他.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl辅助检查其他.TextSize = new System.Drawing.Size(80, 20);
            this.lbl辅助检查其他.TextToControlDistance = 5;
            // 
            // lbl腹部B超
            // 
            this.lbl腹部B超.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl腹部B超.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl腹部B超.Control = this.txt腹部B超;
            this.lbl腹部B超.CustomizationFormText = "B 超* ";
            this.lbl腹部B超.Location = new System.Drawing.Point(172, 2381);
            this.lbl腹部B超.Name = "lbl腹部B超";
            this.lbl腹部B超.Size = new System.Drawing.Size(499, 24);
            this.lbl腹部B超.Text = "腹部 B超";
            this.lbl腹部B超.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl腹部B超.TextSize = new System.Drawing.Size(80, 20);
            this.lbl腹部B超.TextToControlDistance = 5;
            // 
            // lbl平和质
            // 
            this.lbl平和质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl平和质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl平和质.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl平和质.Control = this.txt平和质;
            this.lbl平和质.CustomizationFormText = "平和质";
            this.lbl平和质.Location = new System.Drawing.Point(92, 2479);
            this.lbl平和质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl平和质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl平和质.Name = "lbl平和质";
            this.lbl平和质.Size = new System.Drawing.Size(579, 24);
            this.lbl平和质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl平和质.Text = "平和质";
            this.lbl平和质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl平和质.TextSize = new System.Drawing.Size(80, 20);
            this.lbl平和质.TextToControlDistance = 5;
            this.lbl平和质.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl特秉质
            // 
            this.lbl特秉质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl特秉质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl特秉质.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl特秉质.Control = this.txt特秉质;
            this.lbl特秉质.CustomizationFormText = "特秉质";
            this.lbl特秉质.Location = new System.Drawing.Point(92, 2671);
            this.lbl特秉质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl特秉质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl特秉质.Name = "lbl特秉质";
            this.lbl特秉质.Size = new System.Drawing.Size(579, 24);
            this.lbl特秉质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl特秉质.Text = "特秉质";
            this.lbl特秉质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl特秉质.TextSize = new System.Drawing.Size(80, 20);
            this.lbl特秉质.TextToControlDistance = 5;
            this.lbl特秉质.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl气郁质
            // 
            this.lbl气郁质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl气郁质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl气郁质.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl气郁质.Control = this.txt气郁质;
            this.lbl气郁质.CustomizationFormText = "气郁质";
            this.lbl气郁质.Location = new System.Drawing.Point(92, 2647);
            this.lbl气郁质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl气郁质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl气郁质.Name = "lbl气郁质";
            this.lbl气郁质.Size = new System.Drawing.Size(579, 24);
            this.lbl气郁质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl气郁质.Text = "气郁质";
            this.lbl气郁质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl气郁质.TextSize = new System.Drawing.Size(80, 20);
            this.lbl气郁质.TextToControlDistance = 5;
            this.lbl气郁质.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl血瘀质
            // 
            this.lbl血瘀质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血瘀质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血瘀质.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl血瘀质.Control = this.txt血瘀质;
            this.lbl血瘀质.CustomizationFormText = "血瘀质";
            this.lbl血瘀质.Location = new System.Drawing.Point(92, 2623);
            this.lbl血瘀质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血瘀质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl血瘀质.Name = "lbl血瘀质";
            this.lbl血瘀质.Size = new System.Drawing.Size(579, 24);
            this.lbl血瘀质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血瘀质.Text = "血瘀质";
            this.lbl血瘀质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血瘀质.TextSize = new System.Drawing.Size(80, 20);
            this.lbl血瘀质.TextToControlDistance = 5;
            this.lbl血瘀质.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl湿热质
            // 
            this.lbl湿热质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl湿热质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl湿热质.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl湿热质.Control = this.txt湿热质;
            this.lbl湿热质.CustomizationFormText = "湿热质";
            this.lbl湿热质.Location = new System.Drawing.Point(92, 2599);
            this.lbl湿热质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl湿热质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl湿热质.Name = "lbl湿热质";
            this.lbl湿热质.Size = new System.Drawing.Size(579, 24);
            this.lbl湿热质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl湿热质.Text = "湿热质";
            this.lbl湿热质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl湿热质.TextSize = new System.Drawing.Size(80, 20);
            this.lbl湿热质.TextToControlDistance = 5;
            this.lbl湿热质.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl痰湿质
            // 
            this.lbl痰湿质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl痰湿质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl痰湿质.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl痰湿质.Control = this.txt痰湿质;
            this.lbl痰湿质.CustomizationFormText = "痰湿质";
            this.lbl痰湿质.Location = new System.Drawing.Point(92, 2575);
            this.lbl痰湿质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl痰湿质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl痰湿质.Name = "lbl痰湿质";
            this.lbl痰湿质.Size = new System.Drawing.Size(579, 24);
            this.lbl痰湿质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl痰湿质.Text = "痰湿质";
            this.lbl痰湿质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl痰湿质.TextSize = new System.Drawing.Size(80, 20);
            this.lbl痰湿质.TextToControlDistance = 5;
            this.lbl痰湿质.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl气虚质
            // 
            this.lbl气虚质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl气虚质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl气虚质.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl气虚质.Control = this.txt气虚质;
            this.lbl气虚质.CustomizationFormText = "气虚质 ";
            this.lbl气虚质.Location = new System.Drawing.Point(92, 2503);
            this.lbl气虚质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl气虚质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl气虚质.Name = "lbl气虚质";
            this.lbl气虚质.Size = new System.Drawing.Size(579, 24);
            this.lbl气虚质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl气虚质.Text = "气虚质 ";
            this.lbl气虚质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl气虚质.TextSize = new System.Drawing.Size(80, 20);
            this.lbl气虚质.TextToControlDistance = 5;
            this.lbl气虚质.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl阴虚质
            // 
            this.lbl阴虚质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl阴虚质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl阴虚质.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl阴虚质.Control = this.txt阴虚质;
            this.lbl阴虚质.CustomizationFormText = "阴虚质";
            this.lbl阴虚质.Location = new System.Drawing.Point(92, 2551);
            this.lbl阴虚质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl阴虚质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl阴虚质.Name = "lbl阴虚质";
            this.lbl阴虚质.Size = new System.Drawing.Size(579, 24);
            this.lbl阴虚质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl阴虚质.Text = "阴虚质";
            this.lbl阴虚质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl阴虚质.TextSize = new System.Drawing.Size(80, 20);
            this.lbl阴虚质.TextToControlDistance = 5;
            this.lbl阴虚质.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl阳虚质
            // 
            this.lbl阳虚质.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl阳虚质.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl阳虚质.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl阳虚质.Control = this.txt阳虚质;
            this.lbl阳虚质.CustomizationFormText = "阳虚质";
            this.lbl阳虚质.Location = new System.Drawing.Point(92, 2527);
            this.lbl阳虚质.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl阳虚质.MinSize = new System.Drawing.Size(133, 24);
            this.lbl阳虚质.Name = "lbl阳虚质";
            this.lbl阳虚质.Size = new System.Drawing.Size(579, 24);
            this.lbl阳虚质.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl阳虚质.Text = "阳虚质";
            this.lbl阳虚质.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl阳虚质.TextSize = new System.Drawing.Size(80, 20);
            this.lbl阳虚质.TextToControlDistance = 5;
            this.lbl阳虚质.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl脑血管疾病
            // 
            this.lbl脑血管疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl脑血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl脑血管疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl脑血管疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl脑血管疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl脑血管疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl脑血管疾病.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl脑血管疾病.Control = this.txt脑血管疾病;
            this.lbl脑血管疾病.CustomizationFormText = "脑血管疾病";
            this.lbl脑血管疾病.Location = new System.Drawing.Point(92, 2695);
            this.lbl脑血管疾病.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl脑血管疾病.MinSize = new System.Drawing.Size(133, 24);
            this.lbl脑血管疾病.Name = "lbl脑血管疾病";
            this.lbl脑血管疾病.Size = new System.Drawing.Size(579, 24);
            this.lbl脑血管疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl脑血管疾病.Text = "脑血管疾病";
            this.lbl脑血管疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl脑血管疾病.TextSize = new System.Drawing.Size(80, 20);
            this.lbl脑血管疾病.TextToControlDistance = 5;
            // 
            // lbl其他系统疾病
            // 
            this.lbl其他系统疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl其他系统疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl其他系统疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl其他系统疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl其他系统疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl其他系统疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl其他系统疾病.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl其他系统疾病.Control = this.txt其他系统疾病;
            this.lbl其他系统疾病.CustomizationFormText = "其他系统疾病";
            this.lbl其他系统疾病.Location = new System.Drawing.Point(92, 2815);
            this.lbl其他系统疾病.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl其他系统疾病.MinSize = new System.Drawing.Size(133, 24);
            this.lbl其他系统疾病.Name = "lbl其他系统疾病";
            this.lbl其他系统疾病.Size = new System.Drawing.Size(579, 24);
            this.lbl其他系统疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl其他系统疾病.Text = "其他系统疾病";
            this.lbl其他系统疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl其他系统疾病.TextSize = new System.Drawing.Size(80, 20);
            this.lbl其他系统疾病.TextToControlDistance = 5;
            // 
            // lbl神经系统疾病
            // 
            this.lbl神经系统疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl神经系统疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl神经系统疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl神经系统疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl神经系统疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl神经系统疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl神经系统疾病.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl神经系统疾病.Control = this.txt神经系统疾病;
            this.lbl神经系统疾病.CustomizationFormText = "神经系统疾病";
            this.lbl神经系统疾病.Location = new System.Drawing.Point(92, 2791);
            this.lbl神经系统疾病.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl神经系统疾病.MinSize = new System.Drawing.Size(133, 24);
            this.lbl神经系统疾病.Name = "lbl神经系统疾病";
            this.lbl神经系统疾病.Size = new System.Drawing.Size(579, 24);
            this.lbl神经系统疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl神经系统疾病.Text = "神经系统疾病";
            this.lbl神经系统疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl神经系统疾病.TextSize = new System.Drawing.Size(80, 20);
            this.lbl神经系统疾病.TextToControlDistance = 5;
            // 
            // lbl眼部疾病
            // 
            this.lbl眼部疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl眼部疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl眼部疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl眼部疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl眼部疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl眼部疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl眼部疾病.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl眼部疾病.Control = this.txt眼部疾病;
            this.lbl眼部疾病.CustomizationFormText = "眼部疾病";
            this.lbl眼部疾病.Location = new System.Drawing.Point(92, 2767);
            this.lbl眼部疾病.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl眼部疾病.MinSize = new System.Drawing.Size(133, 24);
            this.lbl眼部疾病.Name = "lbl眼部疾病";
            this.lbl眼部疾病.Size = new System.Drawing.Size(579, 24);
            this.lbl眼部疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl眼部疾病.Text = "眼部疾病";
            this.lbl眼部疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl眼部疾病.TextSize = new System.Drawing.Size(80, 20);
            this.lbl眼部疾病.TextToControlDistance = 5;
            // 
            // lbl心血管疾病
            // 
            this.lbl心血管疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl心血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl心血管疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl心血管疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl心血管疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl心血管疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl心血管疾病.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl心血管疾病.Control = this.txt心血管疾病;
            this.lbl心血管疾病.CustomizationFormText = "心脏疾病";
            this.lbl心血管疾病.Location = new System.Drawing.Point(92, 2743);
            this.lbl心血管疾病.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl心血管疾病.MinSize = new System.Drawing.Size(133, 24);
            this.lbl心血管疾病.Name = "lbl心血管疾病";
            this.lbl心血管疾病.Size = new System.Drawing.Size(579, 24);
            this.lbl心血管疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl心血管疾病.Text = "心血管疾病";
            this.lbl心血管疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl心血管疾病.TextSize = new System.Drawing.Size(80, 20);
            this.lbl心血管疾病.TextToControlDistance = 5;
            // 
            // lbl肾脏疾病
            // 
            this.lbl肾脏疾病.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl肾脏疾病.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl肾脏疾病.AppearanceItemCaption.Options.UseFont = true;
            this.lbl肾脏疾病.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl肾脏疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl肾脏疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl肾脏疾病.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl肾脏疾病.Control = this.txt肾脏疾病;
            this.lbl肾脏疾病.CustomizationFormText = "肾脏疾病";
            this.lbl肾脏疾病.Location = new System.Drawing.Point(92, 2719);
            this.lbl肾脏疾病.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl肾脏疾病.MinSize = new System.Drawing.Size(133, 24);
            this.lbl肾脏疾病.Name = "lbl肾脏疾病";
            this.lbl肾脏疾病.Size = new System.Drawing.Size(579, 24);
            this.lbl肾脏疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl肾脏疾病.Text = "肾脏疾病";
            this.lbl肾脏疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl肾脏疾病.TextSize = new System.Drawing.Size(80, 20);
            this.lbl肾脏疾病.TextToControlDistance = 5;
            // 
            // lbl住院史
            // 
            this.lbl住院史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl住院史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl住院史.Control = this.gc住院史;
            this.lbl住院史.CustomizationFormText = "住院史";
            this.lbl住院史.Location = new System.Drawing.Point(92, 2839);
            this.lbl住院史.MinSize = new System.Drawing.Size(183, 60);
            this.lbl住院史.Name = "lbl住院史";
            this.lbl住院史.Size = new System.Drawing.Size(579, 60);
            this.lbl住院史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl住院史.Text = "住院史";
            this.lbl住院史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl住院史.TextSize = new System.Drawing.Size(80, 20);
            this.lbl住院史.TextToControlDistance = 5;
            // 
            // lbl家庭病床史
            // 
            this.lbl家庭病床史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl家庭病床史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl家庭病床史.Control = this.gc家庭病床史;
            this.lbl家庭病床史.CustomizationFormText = "家庭病床史";
            this.lbl家庭病床史.Location = new System.Drawing.Point(92, 2899);
            this.lbl家庭病床史.MinSize = new System.Drawing.Size(183, 60);
            this.lbl家庭病床史.Name = "lbl家庭病床史";
            this.lbl家庭病床史.Size = new System.Drawing.Size(579, 60);
            this.lbl家庭病床史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl家庭病床史.Text = "家庭病床史";
            this.lbl家庭病床史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl家庭病床史.TextSize = new System.Drawing.Size(80, 20);
            this.lbl家庭病床史.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem17.Control = this.gc主要用药情况;
            this.layoutControlItem17.CustomizationFormText = "主要用药情况";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 2959);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(227, 60);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(671, 60);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "主要用药情况";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem18.Control = this.gc非免疫规划预防接种史;
            this.layoutControlItem18.CustomizationFormText = "非免疫规划预防接种史";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 3021);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(227, 60);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(671, 60);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "非免疫规划预防接种史";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // simpleSeparator8
            // 
            this.simpleSeparator8.AllowHotTrack = false;
            this.simpleSeparator8.CustomizationFormText = "simpleSeparator8";
            this.simpleSeparator8.Location = new System.Drawing.Point(0, 3081);
            this.simpleSeparator8.Name = "simpleSeparator8";
            this.simpleSeparator8.Size = new System.Drawing.Size(671, 2);
            this.simpleSeparator8.Text = "simpleSeparator8";
            // 
            // group老年人
            // 
            this.group老年人.CustomizationFormText = "group老年人";
            this.group老年人.GroupBordersVisible = false;
            this.group老年人.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl老年人情感状态,
            this.lbl老年人认知能力,
            this.lbl老年人生活自理能力自我评估,
            this.lbl老年人健康状态自我评估});
            this.group老年人.Location = new System.Drawing.Point(92, 166);
            this.group老年人.Name = "group老年人";
            this.group老年人.Size = new System.Drawing.Size(579, 160);
            this.group老年人.Text = "group老年人";
            // 
            // lbl老年人情感状态
            // 
            this.lbl老年人情感状态.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl老年人情感状态.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl老年人情感状态.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl老年人情感状态.Control = this.txt老年人情感状态;
            this.lbl老年人情感状态.CustomizationFormText = "老年人情感状态*";
            this.lbl老年人情感状态.Location = new System.Drawing.Point(0, 120);
            this.lbl老年人情感状态.MinSize = new System.Drawing.Size(50, 40);
            this.lbl老年人情感状态.Name = "lbl老年人情感状态";
            this.lbl老年人情感状态.Size = new System.Drawing.Size(579, 40);
            this.lbl老年人情感状态.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl老年人情感状态.Text = "老年人情感状态*";
            this.lbl老年人情感状态.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl老年人情感状态.TextSize = new System.Drawing.Size(80, 20);
            this.lbl老年人情感状态.TextToControlDistance = 5;
            // 
            // lbl老年人认知能力
            // 
            this.lbl老年人认知能力.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl老年人认知能力.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl老年人认知能力.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl老年人认知能力.Control = this.txt老年人认知能力;
            this.lbl老年人认知能力.CustomizationFormText = "老年人认知能力*";
            this.lbl老年人认知能力.Location = new System.Drawing.Point(0, 80);
            this.lbl老年人认知能力.MinSize = new System.Drawing.Size(50, 40);
            this.lbl老年人认知能力.Name = "lbl老年人认知能力";
            this.lbl老年人认知能力.Size = new System.Drawing.Size(579, 40);
            this.lbl老年人认知能力.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl老年人认知能力.Text = "老年人认知能力*";
            this.lbl老年人认知能力.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl老年人认知能力.TextSize = new System.Drawing.Size(80, 20);
            this.lbl老年人认知能力.TextToControlDistance = 5;
            // 
            // lbl老年人生活自理能力自我评估
            // 
            this.lbl老年人生活自理能力自我评估.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl老年人生活自理能力自我评估.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl老年人生活自理能力自我评估.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl老年人生活自理能力自我评估.Control = this.txt老年人生活自理能力自我评估;
            this.lbl老年人生活自理能力自我评估.CustomizationFormText = "老年人生活自理能力自我评估*";
            this.lbl老年人生活自理能力自我评估.Location = new System.Drawing.Point(0, 40);
            this.lbl老年人生活自理能力自我评估.MinSize = new System.Drawing.Size(50, 40);
            this.lbl老年人生活自理能力自我评估.Name = "lbl老年人生活自理能力自我评估";
            this.lbl老年人生活自理能力自我评估.Size = new System.Drawing.Size(579, 40);
            this.lbl老年人生活自理能力自我评估.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl老年人生活自理能力自我评估.Text = "老年人生活自理能力自我评估*";
            this.lbl老年人生活自理能力自我评估.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl老年人生活自理能力自我评估.TextSize = new System.Drawing.Size(80, 20);
            this.lbl老年人生活自理能力自我评估.TextToControlDistance = 5;
            // 
            // lbl老年人健康状态自我评估
            // 
            this.lbl老年人健康状态自我评估.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl老年人健康状态自我评估.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl老年人健康状态自我评估.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl老年人健康状态自我评估.Control = this.txt老年人健康状态自我评估;
            this.lbl老年人健康状态自我评估.CustomizationFormText = "老年人健康状态自我评估*";
            this.lbl老年人健康状态自我评估.Location = new System.Drawing.Point(0, 0);
            this.lbl老年人健康状态自我评估.MinSize = new System.Drawing.Size(50, 40);
            this.lbl老年人健康状态自我评估.Name = "lbl老年人健康状态自我评估";
            this.lbl老年人健康状态自我评估.Size = new System.Drawing.Size(579, 40);
            this.lbl老年人健康状态自我评估.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl老年人健康状态自我评估.Text = "老年人健康状态自我评估*";
            this.lbl老年人健康状态自我评估.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl老年人健康状态自我评估.TextSize = new System.Drawing.Size(80, 20);
            this.lbl老年人健康状态自我评估.TextToControlDistance = 5;
            // 
            // group妇科
            // 
            this.group妇科.CustomizationFormText = "group妇科";
            this.group妇科.GroupBordersVisible = false;
            this.group妇科.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl附件,
            this.lbl宫体,
            this.lbl宫颈,
            this.lbl阴道,
            this.lbl外阴,
            this.layoutControlGroup20});
            this.group妇科.Location = new System.Drawing.Point(92, 1572);
            this.group妇科.Name = "group妇科";
            this.group妇科.Size = new System.Drawing.Size(579, 120);
            this.group妇科.Text = "group妇科";
            // 
            // lbl附件
            // 
            this.lbl附件.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl附件.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl附件.Control = this.txt附件;
            this.lbl附件.CustomizationFormText = "附件";
            this.lbl附件.Location = new System.Drawing.Point(92, 96);
            this.lbl附件.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl附件.MinSize = new System.Drawing.Size(133, 24);
            this.lbl附件.Name = "lbl附件";
            this.lbl附件.Size = new System.Drawing.Size(487, 24);
            this.lbl附件.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl附件.Text = "附件";
            this.lbl附件.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl附件.TextSize = new System.Drawing.Size(100, 20);
            this.lbl附件.TextToControlDistance = 5;
            // 
            // lbl宫体
            // 
            this.lbl宫体.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl宫体.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl宫体.Control = this.txt宫体;
            this.lbl宫体.CustomizationFormText = "宫体";
            this.lbl宫体.Location = new System.Drawing.Point(92, 72);
            this.lbl宫体.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl宫体.MinSize = new System.Drawing.Size(133, 24);
            this.lbl宫体.Name = "lbl宫体";
            this.lbl宫体.Size = new System.Drawing.Size(487, 24);
            this.lbl宫体.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl宫体.Text = "宫体";
            this.lbl宫体.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl宫体.TextSize = new System.Drawing.Size(100, 20);
            this.lbl宫体.TextToControlDistance = 5;
            // 
            // lbl宫颈
            // 
            this.lbl宫颈.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl宫颈.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl宫颈.Control = this.txt宫颈;
            this.lbl宫颈.CustomizationFormText = "宫颈";
            this.lbl宫颈.Location = new System.Drawing.Point(92, 48);
            this.lbl宫颈.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl宫颈.MinSize = new System.Drawing.Size(133, 24);
            this.lbl宫颈.Name = "lbl宫颈";
            this.lbl宫颈.Size = new System.Drawing.Size(487, 24);
            this.lbl宫颈.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl宫颈.Text = "宫颈";
            this.lbl宫颈.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl宫颈.TextSize = new System.Drawing.Size(100, 20);
            this.lbl宫颈.TextToControlDistance = 5;
            // 
            // lbl阴道
            // 
            this.lbl阴道.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl阴道.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl阴道.Control = this.txt阴道;
            this.lbl阴道.CustomizationFormText = "阴道";
            this.lbl阴道.Location = new System.Drawing.Point(92, 24);
            this.lbl阴道.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl阴道.MinSize = new System.Drawing.Size(133, 24);
            this.lbl阴道.Name = "lbl阴道";
            this.lbl阴道.Size = new System.Drawing.Size(487, 24);
            this.lbl阴道.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl阴道.Text = "阴道";
            this.lbl阴道.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl阴道.TextSize = new System.Drawing.Size(100, 20);
            this.lbl阴道.TextToControlDistance = 5;
            // 
            // lbl外阴
            // 
            this.lbl外阴.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl外阴.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl外阴.Control = this.txt外阴;
            this.lbl外阴.CustomizationFormText = "外阴";
            this.lbl外阴.Location = new System.Drawing.Point(92, 0);
            this.lbl外阴.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl外阴.MinSize = new System.Drawing.Size(133, 24);
            this.lbl外阴.Name = "lbl外阴";
            this.lbl外阴.Size = new System.Drawing.Size(487, 24);
            this.lbl外阴.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl外阴.Text = "外阴";
            this.lbl外阴.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl外阴.TextSize = new System.Drawing.Size(100, 20);
            this.lbl外阴.TextToControlDistance = 5;
            // 
            // layoutControlGroup20
            // 
            this.layoutControlGroup20.CustomizationFormText = "layoutControlGroup20";
            this.layoutControlGroup20.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem22});
            this.layoutControlGroup20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup20.Name = "layoutControlGroup20";
            this.layoutControlGroup20.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup20.Size = new System.Drawing.Size(92, 120);
            this.layoutControlGroup20.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup20.Text = "layoutControlGroup20";
            this.layoutControlGroup20.TextVisible = false;
            // 
            // emptySpaceItem22
            // 
            this.emptySpaceItem22.AllowHotTrack = false;
            this.emptySpaceItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem22.CustomizationFormText = "妇科* ";
            this.emptySpaceItem22.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem22.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem22.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem22.Name = "emptySpaceItem22";
            this.emptySpaceItem22.Size = new System.Drawing.Size(90, 118);
            this.emptySpaceItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem22.Text = "妇科* ";
            this.emptySpaceItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem22.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem22.TextVisible = true;
            // 
            // lbl宫颈涂片
            // 
            this.lbl宫颈涂片.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl宫颈涂片.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl宫颈涂片.Control = this.txt宫颈涂片;
            this.lbl宫颈涂片.CustomizationFormText = "宫颈涂片* ";
            this.lbl宫颈涂片.Location = new System.Drawing.Point(92, 2431);
            this.lbl宫颈涂片.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl宫颈涂片.MinSize = new System.Drawing.Size(133, 24);
            this.lbl宫颈涂片.Name = "lbl宫颈涂片";
            this.lbl宫颈涂片.Size = new System.Drawing.Size(579, 24);
            this.lbl宫颈涂片.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl宫颈涂片.Text = "宫颈涂片* ";
            this.lbl宫颈涂片.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl宫颈涂片.TextSize = new System.Drawing.Size(80, 20);
            this.lbl宫颈涂片.TextToControlDistance = 5;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem16});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 2695);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Size = new System.Drawing.Size(92, 144);
            this.layoutControlGroup6.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            this.layoutControlGroup6.TextVisible = false;
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem16.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem16.CustomizationFormText = "现存主要健康问题 ";
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem16.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem16.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(90, 142);
            this.emptySpaceItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem16.Text = "现存主要健康问题 ";
            this.emptySpaceItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(90, 20);
            this.emptySpaceItem16.TextVisible = true;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem14});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 46);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(92, 280);
            this.layoutControlGroup7.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Text = "layoutControlGroup7";
            this.layoutControlGroup7.TextVisible = false;
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem14.CustomizationFormText = "一般情况 ";
            this.emptySpaceItem14.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem14.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem14.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(90, 278);
            this.emptySpaceItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem14.Text = "一般情况 ";
            this.emptySpaceItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem14.TextVisible = true;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "layoutControlGroup8";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem18});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 326);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup8.Size = new System.Drawing.Size(92, 444);
            this.layoutControlGroup8.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup8.Text = "layoutControlGroup8";
            this.layoutControlGroup8.TextVisible = false;
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem18.CustomizationFormText = "生活方式";
            this.emptySpaceItem18.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem18.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem18.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(90, 442);
            this.emptySpaceItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem18.Text = "生活方式";
            this.emptySpaceItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem18.TextVisible = true;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "layoutControlGroup9";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem19});
            this.layoutControlGroup9.Location = new System.Drawing.Point(92, 326);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup9.Size = new System.Drawing.Size(92, 72);
            this.layoutControlGroup9.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup9.Text = "layoutControlGroup9";
            this.layoutControlGroup9.TextVisible = false;
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem19.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem19.CustomizationFormText = "体育锻炼";
            this.emptySpaceItem19.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem19.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem19.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(90, 70);
            this.emptySpaceItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem19.Text = "体育锻炼";
            this.emptySpaceItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem19.TextVisible = true;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "layoutControlGroup10";
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl吸烟情况});
            this.layoutControlGroup10.Location = new System.Drawing.Point(92, 422);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup10.Size = new System.Drawing.Size(92, 72);
            this.layoutControlGroup10.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup10.Text = "layoutControlGroup10";
            this.layoutControlGroup10.TextVisible = false;
            // 
            // lbl吸烟情况
            // 
            this.lbl吸烟情况.AllowHotTrack = false;
            this.lbl吸烟情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl吸烟情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl吸烟情况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl吸烟情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl吸烟情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl吸烟情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl吸烟情况.CustomizationFormText = "吸烟情况";
            this.lbl吸烟情况.Location = new System.Drawing.Point(0, 0);
            this.lbl吸烟情况.MaxSize = new System.Drawing.Size(90, 0);
            this.lbl吸烟情况.MinSize = new System.Drawing.Size(90, 10);
            this.lbl吸烟情况.Name = "lbl吸烟情况";
            this.lbl吸烟情况.Size = new System.Drawing.Size(90, 70);
            this.lbl吸烟情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl吸烟情况.Text = "吸烟情况";
            this.lbl吸烟情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl吸烟情况.TextSize = new System.Drawing.Size(50, 20);
            this.lbl吸烟情况.TextVisible = true;
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "layoutControlGroup11";
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl饮酒情况});
            this.layoutControlGroup11.Location = new System.Drawing.Point(92, 494);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup11.Size = new System.Drawing.Size(92, 120);
            this.layoutControlGroup11.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup11.Text = "layoutControlGroup11";
            this.layoutControlGroup11.TextVisible = false;
            // 
            // lbl饮酒情况
            // 
            this.lbl饮酒情况.AllowHotTrack = false;
            this.lbl饮酒情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl饮酒情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl饮酒情况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl饮酒情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl饮酒情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl饮酒情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl饮酒情况.CustomizationFormText = "饮酒情况";
            this.lbl饮酒情况.Location = new System.Drawing.Point(0, 0);
            this.lbl饮酒情况.MaxSize = new System.Drawing.Size(90, 0);
            this.lbl饮酒情况.MinSize = new System.Drawing.Size(90, 10);
            this.lbl饮酒情况.Name = "lbl饮酒情况";
            this.lbl饮酒情况.Size = new System.Drawing.Size(90, 118);
            this.lbl饮酒情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl饮酒情况.Text = "饮酒情况";
            this.lbl饮酒情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl饮酒情况.TextSize = new System.Drawing.Size(50, 20);
            this.lbl饮酒情况.TextVisible = true;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.CustomizationFormText = "layoutControlGroup12";
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl职业病危害因素接触史});
            this.layoutControlGroup12.Location = new System.Drawing.Point(92, 614);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup12.Size = new System.Drawing.Size(92, 156);
            this.layoutControlGroup12.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup12.Text = "layoutControlGroup12";
            this.layoutControlGroup12.TextVisible = false;
            // 
            // lbl职业病危害因素接触史
            // 
            this.lbl职业病危害因素接触史.AllowHotTrack = false;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl职业病危害因素接触史.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.Options.UseFont = true;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl职业病危害因素接触史.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl职业病危害因素接触史.CustomizationFormText = "emptySpaceItem2";
            this.lbl职业病危害因素接触史.Location = new System.Drawing.Point(0, 0);
            this.lbl职业病危害因素接触史.MaxSize = new System.Drawing.Size(90, 0);
            this.lbl职业病危害因素接触史.MinSize = new System.Drawing.Size(90, 30);
            this.lbl职业病危害因素接触史.Name = "lbl职业病危害因素接触史";
            this.lbl职业病危害因素接触史.Size = new System.Drawing.Size(90, 154);
            this.lbl职业病危害因素接触史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl职业病危害因素接触史.Text = "职业病危害因素接触史";
            this.lbl职业病危害因素接触史.TextSize = new System.Drawing.Size(76, 0);
            this.lbl职业病危害因素接触史.TextVisible = true;
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "layoutControlGroup13";
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 770);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup13.Size = new System.Drawing.Size(92, 346);
            this.layoutControlGroup13.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup13.Text = "layoutControlGroup13";
            this.layoutControlGroup13.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.CustomizationFormText = "脏器功能";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(90, 344);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "脏器功能";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "layoutControlGroup14";
            this.layoutControlGroup14.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup14.Location = new System.Drawing.Point(92, 770);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup14.Size = new System.Drawing.Size(92, 274);
            this.layoutControlGroup14.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup14.Text = "layoutControlGroup14";
            this.layoutControlGroup14.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem3.CustomizationFormText = "口腔";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(90, 272);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "口腔";
            this.emptySpaceItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem3.TextVisible = true;
            // 
            // layoutControlGroup16
            // 
            this.layoutControlGroup16.CustomizationFormText = "layoutControlGroup16";
            this.layoutControlGroup16.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4});
            this.layoutControlGroup16.Location = new System.Drawing.Point(0, 1116);
            this.layoutControlGroup16.Name = "layoutControlGroup16";
            this.layoutControlGroup16.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup16.Size = new System.Drawing.Size(92, 647);
            this.layoutControlGroup16.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup16.Text = "layoutControlGroup16";
            this.layoutControlGroup16.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem4.CustomizationFormText = "查体 ";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(90, 645);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "查体 ";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(76, 0);
            this.emptySpaceItem4.TextVisible = true;
            // 
            // layoutControlGroup17
            // 
            this.layoutControlGroup17.CustomizationFormText = "layoutControlGroup17";
            this.layoutControlGroup17.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem20});
            this.layoutControlGroup17.Location = new System.Drawing.Point(92, 1212);
            this.layoutControlGroup17.Name = "layoutControlGroup17";
            this.layoutControlGroup17.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup17.Size = new System.Drawing.Size(92, 72);
            this.layoutControlGroup17.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup17.Text = "layoutControlGroup17";
            this.layoutControlGroup17.TextVisible = false;
            // 
            // emptySpaceItem20
            // 
            this.emptySpaceItem20.AllowHotTrack = false;
            this.emptySpaceItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem20.CustomizationFormText = "肺";
            this.emptySpaceItem20.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem20.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem20.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem20.Name = "emptySpaceItem20";
            this.emptySpaceItem20.Size = new System.Drawing.Size(90, 70);
            this.emptySpaceItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem20.Text = "肺";
            this.emptySpaceItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem20.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem20.TextVisible = true;
            // 
            // layoutControlGroup18
            // 
            this.layoutControlGroup18.CustomizationFormText = "layoutControlGroup18";
            this.layoutControlGroup18.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem21});
            this.layoutControlGroup18.Location = new System.Drawing.Point(92, 1284);
            this.layoutControlGroup18.Name = "layoutControlGroup18";
            this.layoutControlGroup18.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup18.Size = new System.Drawing.Size(92, 72);
            this.layoutControlGroup18.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup18.Text = "layoutControlGroup18";
            this.layoutControlGroup18.TextVisible = false;
            // 
            // emptySpaceItem21
            // 
            this.emptySpaceItem21.AllowHotTrack = false;
            this.emptySpaceItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem21.CustomizationFormText = "心 脏";
            this.emptySpaceItem21.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem21.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem21.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem21.Name = "emptySpaceItem21";
            this.emptySpaceItem21.Size = new System.Drawing.Size(90, 70);
            this.emptySpaceItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem21.Text = "心 脏";
            this.emptySpaceItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem21.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem21.TextVisible = true;
            // 
            // layoutControlGroup19
            // 
            this.layoutControlGroup19.CustomizationFormText = "layoutControlGroup19";
            this.layoutControlGroup19.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6});
            this.layoutControlGroup19.Location = new System.Drawing.Point(92, 1356);
            this.layoutControlGroup19.Name = "layoutControlGroup19";
            this.layoutControlGroup19.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup19.Size = new System.Drawing.Size(92, 120);
            this.layoutControlGroup19.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup19.Text = "layoutControlGroup19";
            this.layoutControlGroup19.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem6.CustomizationFormText = "腹 部";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(90, 118);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "腹 部";
            this.emptySpaceItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // layoutControlGroup21
            // 
            this.layoutControlGroup21.CustomizationFormText = "layoutControlGroup21";
            this.layoutControlGroup21.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem8});
            this.layoutControlGroup21.Location = new System.Drawing.Point(0, 1763);
            this.layoutControlGroup21.Name = "layoutControlGroup21";
            this.layoutControlGroup21.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup21.Size = new System.Drawing.Size(92, 716);
            this.layoutControlGroup21.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup21.Text = "layoutControlGroup21";
            this.layoutControlGroup21.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem8.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem8.CustomizationFormText = "辅助检查";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(90, 714);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.Text = "辅助检查";
            this.emptySpaceItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem8.TextVisible = true;
            // 
            // layoutControlGroup22
            // 
            this.layoutControlGroup22.CustomizationFormText = "layoutControlGroup22";
            this.layoutControlGroup22.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem9});
            this.layoutControlGroup22.Location = new System.Drawing.Point(92, 1741);
            this.layoutControlGroup22.Name = "layoutControlGroup22";
            this.layoutControlGroup22.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup22.Size = new System.Drawing.Size(92, 96);
            this.layoutControlGroup22.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup22.Text = "layoutControlGroup22";
            this.layoutControlGroup22.TextVisible = false;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem9.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem9.CustomizationFormText = "血常规* ";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(90, 94);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.Text = "血常规* ";
            this.emptySpaceItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem9.TextVisible = true;
            // 
            // layoutControlGroup23
            // 
            this.layoutControlGroup23.CustomizationFormText = "layoutControlGroup23";
            this.layoutControlGroup23.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem10});
            this.layoutControlGroup23.Location = new System.Drawing.Point(92, 2045);
            this.layoutControlGroup23.Name = "layoutControlGroup23";
            this.layoutControlGroup23.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup23.Size = new System.Drawing.Size(92, 120);
            this.layoutControlGroup23.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup23.Text = "layoutControlGroup23";
            this.layoutControlGroup23.TextVisible = false;
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem10.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem10.CustomizationFormText = "肝功能* ";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(90, 118);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.Text = "肝功能* ";
            this.emptySpaceItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem10.TextVisible = true;
            // 
            // layoutControlGroup24
            // 
            this.layoutControlGroup24.CustomizationFormText = "layoutControlGroup24";
            this.layoutControlGroup24.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem11});
            this.layoutControlGroup24.Location = new System.Drawing.Point(92, 2165);
            this.layoutControlGroup24.Name = "layoutControlGroup24";
            this.layoutControlGroup24.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup24.Size = new System.Drawing.Size(92, 96);
            this.layoutControlGroup24.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup24.Text = "layoutControlGroup24";
            this.layoutControlGroup24.TextVisible = false;
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem11.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem11.CustomizationFormText = "肾功能*";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(90, 94);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.Text = "肾功能*";
            this.emptySpaceItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem11.TextVisible = true;
            // 
            // layoutControlGroup25
            // 
            this.layoutControlGroup25.CustomizationFormText = "layoutControlGroup25";
            this.layoutControlGroup25.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem13});
            this.layoutControlGroup25.Location = new System.Drawing.Point(92, 2261);
            this.layoutControlGroup25.Name = "layoutControlGroup25";
            this.layoutControlGroup25.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup25.Size = new System.Drawing.Size(92, 96);
            this.layoutControlGroup25.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup25.Text = "layoutControlGroup25";
            this.layoutControlGroup25.TextLocation = DevExpress.Utils.Locations.Default;
            this.layoutControlGroup25.TextVisible = false;
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem13.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem13.CustomizationFormText = "血脂*";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem13.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem13.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(90, 94);
            this.emptySpaceItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem13.Text = "血脂*";
            this.emptySpaceItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem13.TextVisible = true;
            // 
            // layoutControlGroup26
            // 
            this.layoutControlGroup26.CustomizationFormText = "layoutControlGroup26";
            this.layoutControlGroup26.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem15});
            this.layoutControlGroup26.Location = new System.Drawing.Point(0, 2479);
            this.layoutControlGroup26.Name = "layoutControlGroup26";
            this.layoutControlGroup26.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup26.Size = new System.Drawing.Size(92, 216);
            this.layoutControlGroup26.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup26.Text = "layoutControlGroup26";
            this.layoutControlGroup26.TextVisible = false;
            this.layoutControlGroup26.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem15.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.emptySpaceItem15.CustomizationFormText = "中医体质辨识* ";
            this.emptySpaceItem15.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem15.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem15.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(90, 214);
            this.emptySpaceItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem15.Text = "中医体质辨识* ";
            this.emptySpaceItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(90, 20);
            this.emptySpaceItem15.TextVisible = true;
            // 
            // simpleSeparator12
            // 
            this.simpleSeparator12.AllowHotTrack = false;
            this.simpleSeparator12.CustomizationFormText = "simpleSeparator12";
            this.simpleSeparator12.Location = new System.Drawing.Point(0, 3019);
            this.simpleSeparator12.Name = "simpleSeparator12";
            this.simpleSeparator12.Size = new System.Drawing.Size(671, 2);
            this.simpleSeparator12.Text = "simpleSeparator12";
            // 
            // layoutControlGroup28
            // 
            this.layoutControlGroup28.CustomizationFormText = "layoutControlGroup28";
            this.layoutControlGroup28.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem17});
            this.layoutControlGroup28.Location = new System.Drawing.Point(0, 2839);
            this.layoutControlGroup28.Name = "layoutControlGroup28";
            this.layoutControlGroup28.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup28.Size = new System.Drawing.Size(92, 120);
            this.layoutControlGroup28.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup28.Text = "layoutControlGroup28";
            this.layoutControlGroup28.TextVisible = false;
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem17.CustomizationFormText = "住院治疗情况";
            this.emptySpaceItem17.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem17.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem17.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(90, 118);
            this.emptySpaceItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem17.Text = "住院治疗情况";
            this.emptySpaceItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(90, 20);
            this.emptySpaceItem17.TextVisible = true;
            // 
            // lbl齿列正常
            // 
            this.lbl齿列正常.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl齿列正常.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl齿列正常.Control = this.txt齿列有无;
            this.lbl齿列正常.CustomizationFormText = "齿列";
            this.lbl齿列正常.Location = new System.Drawing.Point(259, 794);
            this.lbl齿列正常.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl齿列正常.MinSize = new System.Drawing.Size(133, 24);
            this.lbl齿列正常.Name = "lbl齿列正常";
            this.lbl齿列正常.Size = new System.Drawing.Size(412, 24);
            this.lbl齿列正常.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl齿列正常.Text = "齿列";
            this.lbl齿列正常.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl齿列正常.TextSize = new System.Drawing.Size(0, 0);
            this.lbl齿列正常.TextToControlDistance = 0;
            this.lbl齿列正常.TextVisible = false;
            // 
            // lbl齿列总
            // 
            this.lbl齿列总.AllowHotTrack = false;
            this.lbl齿列总.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl齿列总.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl齿列总.AppearanceItemCaption.Options.UseFont = true;
            this.lbl齿列总.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl齿列总.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl齿列总.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl齿列总.CustomizationFormText = "齿列";
            this.lbl齿列总.Location = new System.Drawing.Point(184, 794);
            this.lbl齿列总.MaxSize = new System.Drawing.Size(75, 0);
            this.lbl齿列总.MinSize = new System.Drawing.Size(75, 10);
            this.lbl齿列总.Name = "lbl齿列总";
            this.lbl齿列总.Size = new System.Drawing.Size(75, 226);
            this.lbl齿列总.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl齿列总.Text = "齿列";
            this.lbl齿列总.TextSize = new System.Drawing.Size(76, 0);
            this.lbl齿列总.TextVisible = true;
            // 
            // lbl体温
            // 
            this.lbl体温.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl体温.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl体温.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体温.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体温.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl体温.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl体温.Control = this.txt体温;
            this.lbl体温.CustomizationFormText = "体温";
            this.lbl体温.Location = new System.Drawing.Point(92, 46);
            this.lbl体温.MaxSize = new System.Drawing.Size(180, 0);
            this.lbl体温.MinSize = new System.Drawing.Size(180, 24);
            this.lbl体温.Name = "lbl体温";
            this.lbl体温.Size = new System.Drawing.Size(180, 24);
            this.lbl体温.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl体温.Text = "体温";
            this.lbl体温.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl体温.TextSize = new System.Drawing.Size(80, 20);
            this.lbl体温.TextToControlDistance = 5;
            // 
            // lbl脉率
            // 
            this.lbl脉率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl脉率.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl脉率.AppearanceItemCaption.Options.UseFont = true;
            this.lbl脉率.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl脉率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl脉率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl脉率.Control = this.txt脉率;
            this.lbl脉率.CustomizationFormText = "脉率";
            this.lbl脉率.Location = new System.Drawing.Point(272, 46);
            this.lbl脉率.MinSize = new System.Drawing.Size(199, 24);
            this.lbl脉率.Name = "lbl脉率";
            this.lbl脉率.Size = new System.Drawing.Size(399, 24);
            this.lbl脉率.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl脉率.Text = "脉率";
            this.lbl脉率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl脉率.TextSize = new System.Drawing.Size(90, 20);
            this.lbl脉率.TextToControlDistance = 5;
            // 
            // lbl呼吸频率
            // 
            this.lbl呼吸频率.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl呼吸频率.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl呼吸频率.AppearanceItemCaption.Options.UseFont = true;
            this.lbl呼吸频率.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl呼吸频率.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl呼吸频率.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl呼吸频率.Control = this.txt呼吸频率;
            this.lbl呼吸频率.CustomizationFormText = "呼吸频率";
            this.lbl呼吸频率.Location = new System.Drawing.Point(92, 70);
            this.lbl呼吸频率.MaxSize = new System.Drawing.Size(180, 0);
            this.lbl呼吸频率.MinSize = new System.Drawing.Size(180, 24);
            this.lbl呼吸频率.Name = "lbl呼吸频率";
            this.lbl呼吸频率.Size = new System.Drawing.Size(180, 48);
            this.lbl呼吸频率.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl呼吸频率.Text = "呼吸频率";
            this.lbl呼吸频率.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl呼吸频率.TextSize = new System.Drawing.Size(80, 20);
            this.lbl呼吸频率.TextToControlDistance = 5;
            // 
            // lbl身高
            // 
            this.lbl身高.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl身高.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl身高.AppearanceItemCaption.Options.UseFont = true;
            this.lbl身高.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl身高.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl身高.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl身高.Control = this.txt身高;
            this.lbl身高.CustomizationFormText = "身高";
            this.lbl身高.Location = new System.Drawing.Point(92, 118);
            this.lbl身高.MaxSize = new System.Drawing.Size(180, 0);
            this.lbl身高.MinSize = new System.Drawing.Size(180, 24);
            this.lbl身高.Name = "lbl身高";
            this.lbl身高.Size = new System.Drawing.Size(180, 24);
            this.lbl身高.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl身高.Text = "身高";
            this.lbl身高.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl身高.TextSize = new System.Drawing.Size(80, 20);
            this.lbl身高.TextToControlDistance = 5;
            // 
            // lbl体重
            // 
            this.lbl体重.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl体重.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl体重.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体重.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体重.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl体重.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl体重.Control = this.txt体重;
            this.lbl体重.CustomizationFormText = "体重";
            this.lbl体重.Location = new System.Drawing.Point(272, 118);
            this.lbl体重.MinSize = new System.Drawing.Size(159, 24);
            this.lbl体重.Name = "lbl体重";
            this.lbl体重.Size = new System.Drawing.Size(399, 24);
            this.lbl体重.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl体重.Text = "体重";
            this.lbl体重.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl体重.TextSize = new System.Drawing.Size(90, 20);
            this.lbl体重.TextToControlDistance = 5;
            // 
            // lbl腰围
            // 
            this.lbl腰围.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl腰围.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl腰围.AppearanceItemCaption.Options.UseFont = true;
            this.lbl腰围.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl腰围.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl腰围.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl腰围.Control = this.txt腰围;
            this.lbl腰围.CustomizationFormText = "腰围";
            this.lbl腰围.Location = new System.Drawing.Point(92, 142);
            this.lbl腰围.MaxSize = new System.Drawing.Size(180, 0);
            this.lbl腰围.MinSize = new System.Drawing.Size(180, 24);
            this.lbl腰围.Name = "lbl腰围";
            this.lbl腰围.Size = new System.Drawing.Size(180, 24);
            this.lbl腰围.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl腰围.Text = "腰围";
            this.lbl腰围.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl腰围.TextSize = new System.Drawing.Size(80, 20);
            this.lbl腰围.TextToControlDistance = 5;
            // 
            // lbl体重指数
            // 
            this.lbl体重指数.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl体重指数.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl体重指数.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体重指数.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体重指数.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl体重指数.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl体重指数.Control = this.txt体重指数;
            this.lbl体重指数.CustomizationFormText = "体重指数(BMI)";
            this.lbl体重指数.Location = new System.Drawing.Point(272, 142);
            this.lbl体重指数.MinSize = new System.Drawing.Size(159, 24);
            this.lbl体重指数.Name = "lbl体重指数";
            this.lbl体重指数.Size = new System.Drawing.Size(399, 24);
            this.lbl体重指数.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl体重指数.Text = "体重指数(BMI)";
            this.lbl体重指数.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl体重指数.TextSize = new System.Drawing.Size(90, 20);
            this.lbl体重指数.TextToControlDistance = 5;
            // 
            // group齿列
            // 
            this.group齿列.CustomizationFormText = "group齿列";
            this.group齿列.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl缺齿,
            this.lbl龋齿,
            this.lbl齿列其他,
            this.lbl义齿});
            this.group齿列.Location = new System.Drawing.Point(259, 818);
            this.group齿列.Name = "group齿列";
            this.group齿列.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.group齿列.Size = new System.Drawing.Size(412, 202);
            this.group齿列.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.group齿列.Text = "group齿列";
            this.group齿列.TextVisible = false;
            // 
            // lbl缺齿
            // 
            this.lbl缺齿.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl缺齿.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl缺齿.Control = this.table齿列;
            this.lbl缺齿.CustomizationFormText = " ";
            this.lbl缺齿.Location = new System.Drawing.Point(0, 0);
            this.lbl缺齿.MinSize = new System.Drawing.Size(100, 50);
            this.lbl缺齿.Name = "lbl缺齿";
            this.lbl缺齿.Size = new System.Drawing.Size(410, 50);
            this.lbl缺齿.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl缺齿.Text = "齿列";
            this.lbl缺齿.TextSize = new System.Drawing.Size(0, 0);
            this.lbl缺齿.TextToControlDistance = 0;
            this.lbl缺齿.TextVisible = false;
            // 
            // lbl龋齿
            // 
            this.lbl龋齿.Control = this.tableLayoutPanel1;
            this.lbl龋齿.CustomizationFormText = "lbl龋齿";
            this.lbl龋齿.Location = new System.Drawing.Point(0, 50);
            this.lbl龋齿.MinSize = new System.Drawing.Size(100, 50);
            this.lbl龋齿.Name = "lbl龋齿";
            this.lbl龋齿.Size = new System.Drawing.Size(410, 50);
            this.lbl龋齿.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl龋齿.Text = "lbl龋齿";
            this.lbl龋齿.TextSize = new System.Drawing.Size(0, 0);
            this.lbl龋齿.TextToControlDistance = 0;
            this.lbl龋齿.TextVisible = false;
            // 
            // lbl齿列其他
            // 
            this.lbl齿列其他.Control = this.tableLayoutPanel3;
            this.lbl齿列其他.CustomizationFormText = "lbl齿列其他";
            this.lbl齿列其他.Location = new System.Drawing.Point(0, 150);
            this.lbl齿列其他.MinSize = new System.Drawing.Size(100, 50);
            this.lbl齿列其他.Name = "lbl齿列其他";
            this.lbl齿列其他.Size = new System.Drawing.Size(410, 50);
            this.lbl齿列其他.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl齿列其他.Text = "lbl齿列其他";
            this.lbl齿列其他.TextSize = new System.Drawing.Size(0, 0);
            this.lbl齿列其他.TextToControlDistance = 0;
            this.lbl齿列其他.TextVisible = false;
            // 
            // lbl义齿
            // 
            this.lbl义齿.Control = this.tableLayoutPanel4;
            this.lbl义齿.CustomizationFormText = "lbl义齿";
            this.lbl义齿.Location = new System.Drawing.Point(0, 100);
            this.lbl义齿.MinSize = new System.Drawing.Size(212, 50);
            this.lbl义齿.Name = "lbl义齿";
            this.lbl义齿.Size = new System.Drawing.Size(410, 50);
            this.lbl义齿.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl义齿.Text = "lbl义齿";
            this.lbl义齿.TextSize = new System.Drawing.Size(0, 0);
            this.lbl义齿.TextToControlDistance = 0;
            this.lbl义齿.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem5.CustomizationFormText = "血型";
            this.emptySpaceItem5.Location = new System.Drawing.Point(92, 1716);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(83, 25);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "血型";
            this.emptySpaceItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 20);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txtABO;
            this.layoutControlItem1.CustomizationFormText = "ABO";
            this.layoutControlItem1.Location = new System.Drawing.Point(175, 1716);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(220, 25);
            this.layoutControlItem1.Text = "ABO";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txtRh;
            this.layoutControlItem2.CustomizationFormText = "Rh*";
            this.layoutControlItem2.Location = new System.Drawing.Point(395, 1716);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(276, 25);
            this.layoutControlItem2.Text = "Rh*";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt空腹血糖2;
            this.layoutControlItem3.CustomizationFormText = "或";
            this.layoutControlItem3.Location = new System.Drawing.Point(377, 1861);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem3.Text = "或";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(30, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt同型半胱氨酸;
            this.layoutControlItem4.CustomizationFormText = "同型半胱氨酸*";
            this.layoutControlItem4.Location = new System.Drawing.Point(92, 1885);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(579, 24);
            this.layoutControlItem4.Text = "同型半胱氨酸*";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // emptySpaceItem23
            // 
            this.emptySpaceItem23.AllowHotTrack = false;
            this.emptySpaceItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem23.CustomizationFormText = "B 超*";
            this.emptySpaceItem23.Location = new System.Drawing.Point(92, 2381);
            this.emptySpaceItem23.MinSize = new System.Drawing.Size(10, 50);
            this.emptySpaceItem23.Name = "emptySpaceItem23";
            this.emptySpaceItem23.Size = new System.Drawing.Size(80, 50);
            this.emptySpaceItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem23.Text = "B 超*";
            this.emptySpaceItem23.TextSize = new System.Drawing.Size(76, 0);
            this.emptySpaceItem23.TextVisible = true;
            // 
            // lbl其他B超
            // 
            this.lbl其他B超.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl其他B超.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl其他B超.Control = this.txt其他B超;
            this.lbl其他B超.CustomizationFormText = "其他";
            this.lbl其他B超.Location = new System.Drawing.Point(172, 2405);
            this.lbl其他B超.Name = "lbl其他B超";
            this.lbl其他B超.Size = new System.Drawing.Size(499, 26);
            this.lbl其他B超.Text = "其他";
            this.lbl其他B超.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl其他B超.TextSize = new System.Drawing.Size(80, 14);
            this.lbl其他B超.TextToControlDistance = 5;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "创建人信息";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem122,
            this.layoutControlItem124,
            this.layoutControlItem125,
            this.layoutControlItem修改人,
            this.layoutControlItem126,
            this.layoutControlItem修改时间});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 3276);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(673, 50);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Text = "创建人信息";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem122
            // 
            this.layoutControlItem122.Control = this.txt创建时间;
            this.layoutControlItem122.CustomizationFormText = "创建时间: ";
            this.layoutControlItem122.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem122.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem122.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem122.Name = "layoutControlItem122";
            this.layoutControlItem122.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem122.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem122.Text = "创建时间: ";
            this.layoutControlItem122.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem124
            // 
            this.layoutControlItem124.Control = this.txt当前所属机构;
            this.layoutControlItem124.CustomizationFormText = "当前所属机构:";
            this.layoutControlItem124.Location = new System.Drawing.Point(400, 0);
            this.layoutControlItem124.Name = "layoutControlItem124";
            this.layoutControlItem124.Size = new System.Drawing.Size(271, 24);
            this.layoutControlItem124.Text = "当前所属机构:";
            this.layoutControlItem124.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem125
            // 
            this.layoutControlItem125.Control = this.txt创建机构;
            this.layoutControlItem125.CustomizationFormText = "创建机构: ";
            this.layoutControlItem125.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem125.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem125.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem125.Name = "layoutControlItem125";
            this.layoutControlItem125.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem125.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem125.Text = "创建机构: ";
            this.layoutControlItem125.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem修改人
            // 
            this.layoutControlItem修改人.Control = this.txt最近修改人;
            this.layoutControlItem修改人.CustomizationFormText = "最近修改人: ";
            this.layoutControlItem修改人.Location = new System.Drawing.Point(400, 24);
            this.layoutControlItem修改人.Name = "layoutControlItem修改人";
            this.layoutControlItem修改人.Size = new System.Drawing.Size(271, 24);
            this.layoutControlItem修改人.Text = "最近修改人: ";
            this.layoutControlItem修改人.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem126
            // 
            this.layoutControlItem126.Control = this.txt创建人;
            this.layoutControlItem126.CustomizationFormText = "创建人:";
            this.layoutControlItem126.Location = new System.Drawing.Point(200, 0);
            this.layoutControlItem126.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem126.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem126.Name = "layoutControlItem126";
            this.layoutControlItem126.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem126.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem126.Text = "创建人:";
            this.layoutControlItem126.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem修改时间
            // 
            this.layoutControlItem修改时间.Control = this.txt最近更新时间;
            this.layoutControlItem修改时间.CustomizationFormText = "最近更新时间:";
            this.layoutControlItem修改时间.Location = new System.Drawing.Point(200, 24);
            this.layoutControlItem修改时间.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem修改时间.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem修改时间.Name = "layoutControlItem修改时间";
            this.layoutControlItem修改时间.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem修改时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem修改时间.Text = "最近更新时间:";
            this.layoutControlItem修改时间.TextSize = new System.Drawing.Size(76, 14);
            // 
            // lbl健康指导
            // 
            this.lbl健康指导.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl健康指导.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl健康指导.AppearanceItemCaption.Options.UseFont = true;
            this.lbl健康指导.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl健康指导.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl健康指导.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl健康指导.Control = this.txt健康指导;
            this.lbl健康指导.CustomizationFormText = "健康指导";
            this.lbl健康指导.Location = new System.Drawing.Point(0, 3224);
            this.lbl健康指导.MinSize = new System.Drawing.Size(300, 50);
            this.lbl健康指导.Name = "lbl健康指导";
            this.lbl健康指导.Size = new System.Drawing.Size(300, 50);
            this.lbl健康指导.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl健康指导.Text = "健康指导";
            this.lbl健康指导.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl健康指导.TextSize = new System.Drawing.Size(80, 20);
            this.lbl健康指导.TextToControlDistance = 5;
            // 
            // lbl危险因素控制
            // 
            this.lbl危险因素控制.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl危险因素控制.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl危险因素控制.Control = this.txt危险因素控制;
            this.lbl危险因素控制.CustomizationFormText = "危险因素控制：";
            this.lbl危险因素控制.Location = new System.Drawing.Point(300, 3224);
            this.lbl危险因素控制.MinSize = new System.Drawing.Size(50, 50);
            this.lbl危险因素控制.Name = "lbl危险因素控制";
            this.lbl危险因素控制.Size = new System.Drawing.Size(373, 50);
            this.lbl危险因素控制.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl危险因素控制.Text = "危险因素控制：";
            this.lbl危险因素控制.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl危险因素控制.TextSize = new System.Drawing.Size(90, 20);
            this.lbl危险因素控制.TextToControlDistance = 5;
            // 
            // simpleSeparator9
            // 
            this.simpleSeparator9.AllowHotTrack = false;
            this.simpleSeparator9.CustomizationFormText = "simpleSeparator9";
            this.simpleSeparator9.Location = new System.Drawing.Point(0, 3274);
            this.simpleSeparator9.Name = "simpleSeparator9";
            this.simpleSeparator9.Size = new System.Drawing.Size(673, 2);
            this.simpleSeparator9.Text = "simpleSeparator9";
            // 
            // panelControl7
            // 
            this.panelControl7.Controls.Add(this.flowLayoutPanel1);
            this.panelControl7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl7.Location = new System.Drawing.Point(106, 0);
            this.panelControl7.Name = "panelControl7";
            this.panelControl7.Size = new System.Drawing.Size(692, 35);
            this.panelControl7.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn添加);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Controls.Add(this.btn查看随访照片);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(688, 31);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // btn添加
            // 
            this.btn添加.Image = ((System.Drawing.Image)(resources.GetObject("btn添加.Image")));
            this.btn添加.Location = new System.Drawing.Point(3, 3);
            this.btn添加.Name = "btn添加";
            this.btn添加.Size = new System.Drawing.Size(75, 23);
            this.btn添加.TabIndex = 3;
            this.btn添加.Text = "添加";
            this.btn添加.Click += new System.EventHandler(this.btn添加_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(84, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 2;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(165, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 0;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(246, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 4;
            this.btn导出.Text = "导出正面";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(327, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 5;
            this.simpleButton1.Text = "导出反面";
            this.simpleButton1.Click += new System.EventHandler(this.btn导出反面_Click);
            // 
            // btn查看随访照片
            // 
            this.btn查看随访照片.Image = ((System.Drawing.Image)(resources.GetObject("btn查看随访照片.Image")));
            this.btn查看随访照片.Location = new System.Drawing.Point(408, 3);
            this.btn查看随访照片.Name = "btn查看随访照片";
            this.btn查看随访照片.Size = new System.Drawing.Size(112, 23);
            this.btn查看随访照片.TabIndex = 10;
            this.btn查看随访照片.Text = "查看照片";
            this.btn查看随访照片.Click += new System.EventHandler(this.btn查看随访照片_Click);
            // 
            // UC健康体检表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl7);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "UC健康体检表_显示";
            this.Size = new System.Drawing.Size(798, 498);
            this.Load += new System.EventHandler(this.frm健康体检表2_Load);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.panelControl7, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt其他B超.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt同型半胱氨酸.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt空腹血糖2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtABO.Properties)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt义齿1.Properties)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt齿列其他.Properties)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt龋齿1.Properties)).EndInit();
            this.table齿列.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺齿2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt齿列有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫颈涂片.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阴道.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫颈.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt附件.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt宫体.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外阴.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc非免疫规划预防接种史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv非免疫规划预防接种史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc主要用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv主要用药情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp服药依从性)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc家庭病床史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv家庭病床史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肾脏疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心血管疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血管疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼部疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt神经系统疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt其他系统疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脑血管疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阳虚质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阴虚质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt气虚质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt痰湿质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt湿热质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血瘀质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt气郁质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt特秉质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt平和质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt腹部B超.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt辅助检查其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt胸部X线片.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血清高密度脂蛋白胆固醇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血清低密度脂蛋白胆固醇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt甘油三酯.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt总胆固醇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血钾浓度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血尿素氮.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血钠浓度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血清肌酐.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt白蛋白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt总胆红素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血清谷草转氨酶.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结合胆红素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血清谷丙转氨酶.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt糖化血红蛋白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt大便潜血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿微量白蛋白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心电图.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt空腹血糖.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt尿常规.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血小板.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt白细胞.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血常规其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血红蛋白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳腺.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肛门指诊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt足背动脉搏动.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt查体其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt下肢水肿.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt脾大.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt肝大.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt包块.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt移动性浊音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt压痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt杂音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心律.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt心率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt呼吸音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt罗音.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt桶状胸.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt淋巴结.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt巩膜.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt皮肤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt眼底.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt运动功能.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt听力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt视力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt咽部.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt口唇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业病其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学物质防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学物质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理因素防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt物理因素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt放射物质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘防护措施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt粉尘.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒种类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt近一年内是否曾醉酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始饮酒年龄.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt戒酒年龄.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt是否戒酒.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt日饮酒量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮酒频率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt戒烟年龄.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt开始吸烟年龄.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt日吸烟量.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt吸烟状况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮食习惯.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt锻炼方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt坚持锻炼时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt每次锻炼时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt锻炼频率.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt右侧原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt左侧原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血压右侧.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt左侧血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt责任医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乙型肝炎表面抗原.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人健康状态自我评估.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人生活自理能力自我评估.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人认知能力.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt老年人情感状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt健康指导.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt危险因素控制.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt健康评价.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血管疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体检日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl责任医生)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl居住地址)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl身份证号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl个人档案号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl姓名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl出生日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl性别)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl联系电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl健康评价)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl症状)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压右侧)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压左侧)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl锻炼频率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮食习惯)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl吸烟状况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮酒频率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group职业病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl粉尘)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl放射物质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl物理因素)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl化学物质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病其他)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病其他防护措施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl化学物质防护措施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl物理因素防护措施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl放射物质防护措施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl粉尘防护措施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group饮酒情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl日饮酒量)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl是否戒酒)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl开始饮酒年龄)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl近一年内是否曾醉酒)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl戒酒年龄)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮酒种类)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group吸烟情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl日吸烟量)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl开始吸烟年龄)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl戒烟年龄)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group体育锻炼)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl锻炼方式)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl每次锻炼时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl坚持锻炼时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl口唇)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl咽部)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl视力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl听力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl运动功能)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl眼底)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl皮肤)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl巩膜)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl淋巴结)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl桶状胸)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl罗音)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl呼吸音)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心律)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl杂音)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl压痛)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl移动性浊音)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl包块)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肝大)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脾大)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl下肢水肿)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl查体其他)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl足背动脉搏动)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肛门指诊)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl乳腺)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血常规其他)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl白细胞)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血小板)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl尿常规)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl空腹血糖)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心电图)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl尿微量白蛋白)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl乙型肝炎表面抗原)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl大便潜血)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl糖化血红蛋白)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清谷丙转氨酶)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl结合胆红素)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清谷草转氨酶)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl总胆红素)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl白蛋白)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清肌酐)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血钠浓度)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血尿素氮)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血钾浓度)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl总胆固醇)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl甘油三酯)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清低密度脂蛋白胆固醇)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血清高密度脂蛋白胆固醇)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl胸部X线片)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl辅助检查其他)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl腹部B超)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl平和质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl特秉质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl气郁质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血瘀质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl湿热质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl痰湿质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl气虚质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl阴虚质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl阳虚质)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脑血管疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他系统疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl神经系统疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl眼部疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl心血管疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肾脏疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl住院史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家庭病床史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group老年人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl老年人情感状态)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl老年人认知能力)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl老年人生活自理能力自我评估)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl老年人健康状态自我评估)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group妇科)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl附件)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl宫体)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl宫颈)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl阴道)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl外阴)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl宫颈涂片)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl吸烟情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮酒情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业病危害因素接触史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl齿列正常)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl齿列总)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体温)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl脉率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl呼吸频率)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl身高)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl腰围)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重指数)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group齿列)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl缺齿)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl龋齿)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl齿列其他)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl义齿)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他B超)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem122)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem124)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem125)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem修改人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem126)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem修改时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl健康指导)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl危险因素控制)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl7)).EndInit();
            this.panelControl7.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案编号;
        private DevExpress.XtraLayout.LayoutControlItem lbl个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem lbl姓名;
        private DevExpress.XtraLayout.LayoutControlItem lbl性别;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraLayout.LayoutControlItem lbl身份证号;
        private DevExpress.XtraLayout.LayoutControlItem lbl出生日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl联系电话;
        private DevExpress.XtraLayout.LayoutControlItem lbl居住地址;
        private DevExpress.XtraEditors.TextEdit txt责任医生;
        private DevExpress.XtraEditors.DateEdit txt体检日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl体检日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl责任医生;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.TextEdit txt最近修改人;
        private DevExpress.XtraEditors.TextEdit txt创建人;
        private DevExpress.XtraEditors.TextEdit txt创建机构;
        private DevExpress.XtraEditors.TextEdit txt当前所属机构;
        private DevExpress.XtraEditors.TextEdit txt最近更新时间;
        private DevExpress.XtraEditors.TextEdit txt创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem122;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem修改时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem124;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem125;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem126;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem修改人;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.PanelControl panelControl7;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraEditors.SimpleButton btn添加;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.TextEdit txt症状;
        private DevExpress.XtraLayout.LayoutControlItem lbl症状;
        private DevExpress.XtraEditors.TextEdit txt右侧原因;
        private DevExpress.XtraEditors.TextEdit txt左侧原因;
        private DevExpress.XtraEditors.TextEdit txt血压右侧;
        private DevExpress.XtraEditors.TextEdit txt左侧血压;
        private DevExpress.XtraLayout.LayoutControlItem lbl血压右侧;
        private DevExpress.XtraLayout.LayoutControlItem lbl血压左侧;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit txt锻炼方式;
        private DevExpress.XtraEditors.TextEdit txt坚持锻炼时间;
        private DevExpress.XtraEditors.TextEdit txt每次锻炼时间;
        private DevExpress.XtraEditors.TextEdit txt锻炼频率;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraLayout.LayoutControlItem lbl锻炼频率;
        private DevExpress.XtraLayout.LayoutControlItem lbl每次锻炼时间;
        private DevExpress.XtraLayout.LayoutControlItem lbl锻炼方式;
        private DevExpress.XtraLayout.LayoutControlItem lbl坚持锻炼时间;
        private DevExpress.XtraEditors.TextEdit txt戒烟年龄;
        private DevExpress.XtraEditors.TextEdit txt开始吸烟年龄;
        private DevExpress.XtraEditors.TextEdit txt日吸烟量;
        private DevExpress.XtraEditors.TextEdit txt吸烟状况;
        private DevExpress.XtraEditors.TextEdit txt饮食习惯;
        private DevExpress.XtraLayout.LayoutControlItem lbl饮食习惯;
        private DevExpress.XtraLayout.EmptySpaceItem lbl吸烟情况;
        private DevExpress.XtraLayout.LayoutControlItem lbl吸烟状况;
        private DevExpress.XtraLayout.LayoutControlItem lbl日吸烟量;
        private DevExpress.XtraLayout.LayoutControlItem lbl开始吸烟年龄;
        private DevExpress.XtraLayout.LayoutControlItem lbl戒烟年龄;
        private DevExpress.XtraEditors.LabelControl lbl毒物种类;
        private DevExpress.XtraEditors.LabelControl lbl职业病有无;
        private DevExpress.XtraEditors.TextEdit txt职业病其他防护措施;
        private DevExpress.XtraEditors.TextEdit txt职业病其他;
        private DevExpress.XtraEditors.TextEdit txt化学物质防护措施;
        private DevExpress.XtraEditors.TextEdit txt化学物质;
        private DevExpress.XtraEditors.TextEdit txt物理因素防护措施;
        private DevExpress.XtraEditors.TextEdit txt物理因素;
        private DevExpress.XtraEditors.TextEdit txt放射物质防护措施;
        private DevExpress.XtraEditors.TextEdit txt放射物质;
        private DevExpress.XtraEditors.TextEdit txt粉尘防护措施;
        private DevExpress.XtraEditors.TextEdit txt粉尘;
        private DevExpress.XtraEditors.TextEdit txt饮酒种类;
        private DevExpress.XtraEditors.TextEdit txt近一年内是否曾醉酒;
        private DevExpress.XtraEditors.TextEdit txt开始饮酒年龄;
        private DevExpress.XtraEditors.TextEdit txt戒酒年龄;
        private DevExpress.XtraEditors.TextEdit txt是否戒酒;
        private DevExpress.XtraEditors.TextEdit txt日饮酒量;
        private DevExpress.XtraEditors.TextEdit txt饮酒频率;
        private DevExpress.XtraLayout.EmptySpaceItem lbl饮酒情况;
        private DevExpress.XtraLayout.LayoutControlItem lbl饮酒频率;
        private DevExpress.XtraLayout.LayoutControlItem lbl日饮酒量;
        private DevExpress.XtraLayout.LayoutControlItem lbl是否戒酒;
        private DevExpress.XtraLayout.LayoutControlItem lbl戒酒年龄;
        private DevExpress.XtraLayout.LayoutControlItem lbl开始饮酒年龄;
        private DevExpress.XtraLayout.LayoutControlItem lbl近一年内是否曾醉酒;
        private DevExpress.XtraLayout.LayoutControlItem lbl饮酒种类;
        private DevExpress.XtraLayout.EmptySpaceItem lbl职业病危害因素接触史;
        private DevExpress.XtraLayout.LayoutControlItem lbl粉尘;
        private DevExpress.XtraLayout.LayoutControlItem lbl粉尘防护措施;
        private DevExpress.XtraLayout.LayoutControlItem lbl放射物质;
        private DevExpress.XtraLayout.LayoutControlItem lbl放射物质防护措施;
        private DevExpress.XtraLayout.LayoutControlItem lbl物理因素;
        private DevExpress.XtraLayout.LayoutControlItem lbl物理因素防护措施;
        private DevExpress.XtraLayout.LayoutControlItem lbl化学物质;
        private DevExpress.XtraLayout.LayoutControlItem lbl化学物质防护措施;
        private DevExpress.XtraLayout.LayoutControlItem lbl职业病其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl职业病其他防护措施;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.LabelControl lbl工种及从业时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlGroup group职业病;
        private DevExpress.XtraLayout.LayoutControlGroup group饮酒情况;
        private DevExpress.XtraLayout.LayoutControlGroup group吸烟情况;
        private DevExpress.XtraEditors.TextEdit txt咽部;
        private DevExpress.XtraEditors.TextEdit txt口唇;
        private DevExpress.XtraLayout.LayoutControlGroup group体育锻炼;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem lbl口唇;
        private DevExpress.XtraLayout.LayoutControlItem lbl咽部;
        private DevExpress.XtraEditors.TextEdit txt运动功能;
        private DevExpress.XtraEditors.TextEdit txt听力;
        private DevExpress.XtraEditors.TextEdit txt视力;
        private DevExpress.XtraLayout.LayoutControlItem lbl视力;
        private DevExpress.XtraLayout.LayoutControlItem lbl听力;
        private DevExpress.XtraLayout.LayoutControlItem lbl运动功能;
        private DevExpress.XtraEditors.TextEdit txt糖化血红蛋白;
        private DevExpress.XtraEditors.TextEdit txt大便潜血;
        private DevExpress.XtraEditors.TextEdit txt尿微量白蛋白;
        private DevExpress.XtraEditors.TextEdit txt心电图;
        private DevExpress.XtraEditors.TextEdit txt空腹血糖;
        private DevExpress.XtraEditors.TextEdit txt尿常规;
        private DevExpress.XtraEditors.TextEdit txt血小板;
        private DevExpress.XtraEditors.TextEdit txt白细胞;
        private DevExpress.XtraEditors.TextEdit txt血常规其他;
        private DevExpress.XtraEditors.TextEdit txt血红蛋白;
        private DevExpress.XtraEditors.TextEdit txt乳腺;
        private DevExpress.XtraEditors.TextEdit txt肛门指诊;
        private DevExpress.XtraEditors.TextEdit txt足背动脉搏动;
        private DevExpress.XtraEditors.TextEdit txt查体其他;
        private DevExpress.XtraEditors.TextEdit txt下肢水肿;
        private DevExpress.XtraEditors.TextEdit txt脾大;
        private DevExpress.XtraEditors.TextEdit txt肝大;
        private DevExpress.XtraEditors.TextEdit txt包块;
        private DevExpress.XtraEditors.TextEdit txt移动性浊音;
        private DevExpress.XtraEditors.TextEdit txt压痛;
        private DevExpress.XtraEditors.TextEdit txt杂音;
        private DevExpress.XtraEditors.TextEdit txt心律;
        private DevExpress.XtraEditors.TextEdit txt心率;
        private DevExpress.XtraEditors.TextEdit txt呼吸音;
        private DevExpress.XtraEditors.TextEdit txt罗音;
        private DevExpress.XtraEditors.TextEdit txt桶状胸;
        private DevExpress.XtraEditors.TextEdit txt淋巴结;
        private DevExpress.XtraEditors.TextEdit txt巩膜;
        private DevExpress.XtraEditors.TextEdit txt皮肤;
        private DevExpress.XtraEditors.TextEdit txt眼底;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem lbl眼底;
        private DevExpress.XtraLayout.LayoutControlItem lbl皮肤;
        private DevExpress.XtraLayout.LayoutControlItem lbl巩膜;
        private DevExpress.XtraLayout.LayoutControlItem lbl淋巴结;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem20;
        private DevExpress.XtraLayout.LayoutControlItem lbl桶状胸;
        private DevExpress.XtraLayout.LayoutControlItem lbl罗音;
        private DevExpress.XtraLayout.LayoutControlItem lbl呼吸音;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem21;
        private DevExpress.XtraLayout.LayoutControlItem lbl心率;
        private DevExpress.XtraLayout.LayoutControlItem lbl心律;
        private DevExpress.XtraLayout.LayoutControlItem lbl杂音;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem lbl压痛;
        private DevExpress.XtraLayout.LayoutControlItem lbl移动性浊音;
        private DevExpress.XtraLayout.LayoutControlItem lbl包块;
        private DevExpress.XtraLayout.LayoutControlItem lbl肝大;
        private DevExpress.XtraLayout.LayoutControlItem lbl脾大;
        private DevExpress.XtraLayout.LayoutControlItem lbl下肢水肿;
        private DevExpress.XtraLayout.LayoutControlItem lbl查体其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl足背动脉搏动;
        private DevExpress.XtraLayout.LayoutControlItem lbl肛门指诊;
        private DevExpress.XtraLayout.LayoutControlItem lbl乳腺;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem lbl血常规其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl白细胞;
        private DevExpress.XtraLayout.LayoutControlItem lbl血小板;
        private DevExpress.XtraLayout.LayoutControlItem lbl尿常规;
        private DevExpress.XtraLayout.LayoutControlItem lbl空腹血糖;
        private DevExpress.XtraLayout.LayoutControlItem lbl心电图;
        private DevExpress.XtraLayout.LayoutControlItem lbl尿微量白蛋白;
        private DevExpress.XtraLayout.LayoutControlItem lbl乙型肝炎表面抗原;
        private DevExpress.XtraLayout.LayoutControlItem lbl大便潜血;
        private DevExpress.XtraLayout.LayoutControlItem lbl糖化血红蛋白;
        private DevExpress.XtraEditors.TextEdit txt肾脏疾病;
        private DevExpress.XtraEditors.TextEdit txt心血管疾病;
        private DevExpress.XtraEditors.TextEdit txt血管疾病;
        private DevExpress.XtraEditors.TextEdit txt眼部疾病;
        private DevExpress.XtraEditors.TextEdit txt神经系统疾病;
        private DevExpress.XtraEditors.TextEdit txt其他系统疾病;
        private DevExpress.XtraEditors.TextEdit txt脑血管疾病;
        private DevExpress.XtraEditors.TextEdit txt阳虚质;
        private DevExpress.XtraEditors.TextEdit txt阴虚质;
        private DevExpress.XtraEditors.TextEdit txt气虚质;
        private DevExpress.XtraEditors.TextEdit txt痰湿质;
        private DevExpress.XtraEditors.TextEdit txt湿热质;
        private DevExpress.XtraEditors.TextEdit txt血瘀质;
        private DevExpress.XtraEditors.TextEdit txt气郁质;
        private DevExpress.XtraEditors.TextEdit txt特秉质;
        private DevExpress.XtraEditors.TextEdit txt平和质;
        private DevExpress.XtraEditors.TextEdit txt腹部B超;
        private DevExpress.XtraEditors.TextEdit txt辅助检查其他;
        private DevExpress.XtraEditors.TextEdit txt胸部X线片;
        private DevExpress.XtraEditors.TextEdit txt血清高密度脂蛋白胆固醇;
        private DevExpress.XtraEditors.TextEdit txt血清低密度脂蛋白胆固醇;
        private DevExpress.XtraEditors.TextEdit txt甘油三酯;
        private DevExpress.XtraEditors.TextEdit txt总胆固醇;
        private DevExpress.XtraEditors.TextEdit txt血钾浓度;
        private DevExpress.XtraEditors.TextEdit txt血尿素氮;
        private DevExpress.XtraEditors.TextEdit txt血钠浓度;
        private DevExpress.XtraEditors.TextEdit txt血清肌酐;
        private DevExpress.XtraEditors.TextEdit txt白蛋白;
        private DevExpress.XtraEditors.TextEdit txt总胆红素;
        private DevExpress.XtraEditors.TextEdit txt血清谷草转氨酶;
        private DevExpress.XtraEditors.TextEdit txt结合胆红素;
        private DevExpress.XtraEditors.TextEdit txt血清谷丙转氨酶;
        private DevExpress.XtraLayout.LayoutControlItem lbl血清谷丙转氨酶;
        private DevExpress.XtraLayout.LayoutControlItem lbl结合胆红素;
        private DevExpress.XtraLayout.LayoutControlItem lbl血清谷草转氨酶;
        private DevExpress.XtraLayout.LayoutControlItem lbl总胆红素;
        private DevExpress.XtraLayout.LayoutControlItem lbl白蛋白;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.LayoutControlItem lbl血清肌酐;
        private DevExpress.XtraLayout.LayoutControlItem lbl血钠浓度;
        private DevExpress.XtraLayout.LayoutControlItem lbl血尿素氮;
        private DevExpress.XtraLayout.LayoutControlItem lbl血钾浓度;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlItem lbl总胆固醇;
        private DevExpress.XtraLayout.LayoutControlItem lbl甘油三酯;
        private DevExpress.XtraLayout.LayoutControlItem lbl血清低密度脂蛋白胆固醇;
        private DevExpress.XtraLayout.LayoutControlItem lbl血清高密度脂蛋白胆固醇;
        private DevExpress.XtraLayout.LayoutControlItem lbl胸部X线片;
        private DevExpress.XtraLayout.LayoutControlItem lbl辅助检查其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl腹部B超;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.LayoutControlItem lbl平和质;
        private DevExpress.XtraLayout.LayoutControlItem lbl特秉质;
        private DevExpress.XtraLayout.LayoutControlItem lbl气郁质;
        private DevExpress.XtraLayout.LayoutControlItem lbl血瘀质;
        private DevExpress.XtraLayout.LayoutControlItem lbl湿热质;
        private DevExpress.XtraLayout.LayoutControlItem lbl痰湿质;
        private DevExpress.XtraLayout.LayoutControlItem lbl气虚质;
        private DevExpress.XtraLayout.LayoutControlItem lbl阴虚质;
        private DevExpress.XtraLayout.LayoutControlItem lbl阳虚质;
        private DevExpress.XtraLayout.LayoutControlItem lbl脑血管疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl其他系统疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl神经系统疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl眼部疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl血管疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl心血管疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl肾脏疾病;
        private DevExpress.XtraGrid.GridControl gc家庭病床史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv家庭病床史;
        private DevExpress.XtraGrid.GridControl gc住院史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv住院史;
        private DevExpress.XtraGrid.Columns.GridColumn col入院日期;
        private DevExpress.XtraGrid.Columns.GridColumn col出院日期;
        private DevExpress.XtraGrid.Columns.GridColumn col原因;
        private DevExpress.XtraGrid.Columns.GridColumn col医疗机构名称;
        private DevExpress.XtraGrid.Columns.GridColumn col病案号;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.LayoutControlItem lbl住院史;
        private DevExpress.XtraLayout.LayoutControlItem lbl家庭病床史;
        private DevExpress.XtraGrid.Columns.GridColumn col建床日期;
        private DevExpress.XtraGrid.Columns.GridColumn col撤床日期;
        private DevExpress.XtraGrid.Columns.GridColumn col原因2;
        private DevExpress.XtraGrid.Columns.GridColumn col医疗机构名称2;
        private DevExpress.XtraGrid.Columns.GridColumn col病案号2;
        private DevExpress.XtraGrid.GridControl gc非免疫规划预防接种史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv非免疫规划预防接种史;
        private DevExpress.XtraGrid.GridControl gc主要用药情况;
        private DevExpress.XtraGrid.Views.Grid.GridView gv主要用药情况;
        private DevExpress.XtraGrid.Columns.GridColumn col药物名称;
        private DevExpress.XtraGrid.Columns.GridColumn col用法;
        private DevExpress.XtraGrid.Columns.GridColumn col用量;
        private DevExpress.XtraGrid.Columns.GridColumn col用药时间;
        private DevExpress.XtraGrid.Columns.GridColumn col服药依从性;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraGrid.Columns.GridColumn col接种名称;
        private DevExpress.XtraGrid.Columns.GridColumn col接种日期;
        private DevExpress.XtraGrid.Columns.GridColumn col接种机构;
        private DevExpress.XtraLayout.LayoutControlItem lbl健康评价;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator8;
        private DevExpress.XtraLayout.LayoutControlItem lbl健康指导;
        private DevExpress.XtraLayout.LayoutControlItem lbl危险因素控制;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator9;
        private DevExpress.XtraEditors.TextEdit txt阴道;
        private DevExpress.XtraEditors.TextEdit txt宫颈;
        private DevExpress.XtraEditors.TextEdit txt附件;
        private DevExpress.XtraEditors.TextEdit txt宫体;
        private DevExpress.XtraEditors.TextEdit txt外阴;
        private DevExpress.XtraEditors.MemoEdit txt乙型肝炎表面抗原;
        private DevExpress.XtraEditors.MemoEdit txt老年人健康状态自我评估;
        private DevExpress.XtraEditors.MemoEdit txt老年人生活自理能力自我评估;
        private DevExpress.XtraEditors.MemoEdit txt老年人认知能力;
        private DevExpress.XtraEditors.MemoEdit txt老年人情感状态;
        private DevExpress.XtraLayout.LayoutControlGroup group老年人;
        private DevExpress.XtraLayout.LayoutControlItem lbl老年人情感状态;
        private DevExpress.XtraLayout.LayoutControlItem lbl老年人认知能力;
        private DevExpress.XtraLayout.LayoutControlItem lbl老年人生活自理能力自我评估;
        private DevExpress.XtraLayout.LayoutControlItem lbl老年人健康状态自我评估;
        private DevExpress.XtraLayout.LayoutControlGroup group妇科;
        private DevExpress.XtraLayout.LayoutControlItem lbl附件;
        private DevExpress.XtraLayout.LayoutControlItem lbl宫体;
        private DevExpress.XtraLayout.LayoutControlItem lbl宫颈;
        private DevExpress.XtraLayout.LayoutControlItem lbl阴道;
        private DevExpress.XtraLayout.LayoutControlItem lbl外阴;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem22;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.TextEdit txt宫颈涂片;
        private DevExpress.XtraLayout.LayoutControlItem lbl宫颈涂片;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup16;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup17;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup18;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup19;
        private DevExpress.XtraEditors.MemoEdit txt健康指导;
        private DevExpress.XtraEditors.MemoEdit txt危险因素控制;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup20;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup21;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup22;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup23;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup24;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup25;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup26;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup28;
        private DevExpress.XtraEditors.TextEdit txt齿列有无;
        private DevExpress.XtraLayout.LayoutControlItem lbl齿列正常;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lkp服药依从性;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private System.Windows.Forms.TableLayoutPanel table齿列;
        private DevExpress.XtraLayout.LayoutControlItem lbl缺齿;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt缺齿1;
        private DevExpress.XtraEditors.TextEdit txt缺齿2;
        private DevExpress.XtraEditors.TextEdit txt缺齿3;
        private DevExpress.XtraEditors.TextEdit txt缺齿4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit3;
        private DevExpress.XtraLayout.EmptySpaceItem lbl齿列总;
        private UCTxtLbl txt体温;
        private DevExpress.XtraLayout.LayoutControlItem lbl体温;
        private UCTxtLbl txt呼吸频率;
        private UCTxtLbl txt脉率;
        private DevExpress.XtraLayout.LayoutControlItem lbl脉率;
        private DevExpress.XtraLayout.LayoutControlItem lbl呼吸频率;
        private UCTxtLbl txt体重指数;
        private UCTxtLbl txt腰围;
        private UCTxtLbl txt体重;
        private UCTxtLbl txt身高;
        private DevExpress.XtraLayout.LayoutControlItem lbl身高;
        private DevExpress.XtraLayout.LayoutControlItem lbl体重;
        private DevExpress.XtraLayout.LayoutControlItem lbl腰围;
        private DevExpress.XtraLayout.LayoutControlItem lbl体重指数;
        private DevExpress.XtraEditors.MemoEdit txt健康评价;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txt齿列其他;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.TextEdit txt龋齿4;
        private DevExpress.XtraEditors.TextEdit txt龋齿3;
        private DevExpress.XtraEditors.TextEdit txt龋齿2;
        private DevExpress.XtraEditors.TextEdit txt龋齿1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraLayout.LayoutControlItem lbl龋齿;
        private DevExpress.XtraLayout.LayoutControlItem lbl齿列其他;
        private DevExpress.XtraLayout.LayoutControlGroup group齿列;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private DevExpress.XtraEditors.TextEdit txt义齿4;
        private DevExpress.XtraEditors.TextEdit txt义齿3;
        private DevExpress.XtraEditors.TextEdit txt义齿2;
        private DevExpress.XtraEditors.TextEdit txt义齿1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraLayout.LayoutControlItem lbl义齿;
        private DevExpress.XtraEditors.TextEdit txtRh;
        private DevExpress.XtraEditors.TextEdit txtABO;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit txt同型半胱氨酸;
        private DevExpress.XtraEditors.TextEdit txt空腹血糖2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txt其他B超;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem23;
        private DevExpress.XtraLayout.LayoutControlItem lbl其他B超;
        private DevExpress.XtraEditors.SimpleButton btn查看随访照片;
    }
}