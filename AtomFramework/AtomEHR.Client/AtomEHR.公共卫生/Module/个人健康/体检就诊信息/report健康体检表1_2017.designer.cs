﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class report健康体检表1_2017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药医师签字 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种史名称1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种机构1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种医师签字 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种史名称2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种机构2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种史名称3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种日期3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种机构3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康评价 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell341 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell344 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell346 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell347 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell349 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell350 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康评价异常1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell351 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_健康评价医师签字 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康评价异常2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康评价异常3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康评价异常4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell348 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell366 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell353 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell354 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell365 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell367 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell368 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell369 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell362 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell356 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell361 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_减重目标 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell363 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_健康指导医师签字 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell355 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell357 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell358 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_建议接种疫苗 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell359 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_建议接种疫苗补充 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_健康指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_健康指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_危险控制因素其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_危险控制因素其他补充 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell370 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell373 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell395 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell400 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell405 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell371 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell372 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell406 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_本人签字 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_家属签字 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell409 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell386 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell387 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell389 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_反馈人签字 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell382 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell375 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell383 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell381 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_反馈时间年 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell374 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell376 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_反馈时间月 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell377 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell378 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_反馈时间日 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell379 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel_编号6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体检日期年 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体检日期月 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体检日期日 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_责任医生 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_症状医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_症状其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_症状1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_症状10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体温 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_脉率 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_呼吸频率 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_左侧血压1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_左侧血压2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_右侧血压1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_右侧血压2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_身高 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体重 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_腰围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_体重指数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_老年人健康状态自我评估 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_一般状况医师签字 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_老年人生活自理能力自我评估 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_老年人认知功能总分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_老年人认知功能 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_老年人情感状态总分 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_老年人情感状态 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_锻炼频率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_每次锻炼时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_坚持锻炼时间 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_锻炼方式 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_饮食习惯1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_饮食习惯2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_饮食习惯3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_吸烟状况 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_日吸烟量 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_开始吸烟年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_戒烟年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_饮酒频率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_日饮酒量 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_戒酒年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_是否戒酒 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_开始饮酒年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_是否醉酒 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_饮酒种类其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_饮酒种类1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_饮酒种类2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_饮酒种类3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_饮酒种类4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_工种 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_从业时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_接触史有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_粉尘 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_粉尘防护措施 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_粉尘防护措施有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_放射物质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_放射物质防护措施 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_放射物质防护措施有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_物理因素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_物理因素防护措施 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_物理因素防护措施有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_化学物质 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_化学物质防护措施 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_化学物质防护措施有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_接触史其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_接触史其他防护措施 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_接触史其他防护措施有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrCrossBandLine1 = new DevExpress.XtraReports.UI.XRCrossBandLine();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3,
            this.xrLine1,
            this.xrLabel_编号6,
            this.xrLabel_编号7,
            this.xrLabel_编号8,
            this.xrLabel_编号5,
            this.xrLabel_编号4,
            this.xrLabel_编号3,
            this.xrLabel_编号2,
            this.xrLabel_编号1,
            this.xrLabel4,
            this.xrLabel_姓名,
            this.xrLabel2,
            this.xrTable2,
            this.xrLabel1});
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 2970F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(64, 64, 64, 64, 254F);
            this.Detail.StylePriority.UseBorders = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Dpi = 254F;
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(64F, 351.3667F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow47,
            this.xrTableRow46,
            this.xrTableRow45,
            this.xrTableRow44,
            this.xrTableRow43,
            this.xrTableRow42,
            this.xrTableRow41,
            this.xrTableRow40,
            this.xrTableRow39,
            this.xrTableRow50,
            this.xrTableRow53,
            this.xrTableRow55,
            this.xrTableRow56,
            this.xrTableRow54,
            this.xrTableRow52,
            this.xrTableRow51,
            this.xrTableRow58,
            this.xrTableRow61,
            this.xrTableRow60,
            this.xrTableRow59,
            this.xrTableRow57,
            this.xrTableRow70,
            this.xrTableRow71,
            this.xrTableRow62,
            this.xrTableRow67,
            this.xrTableRow68,
            this.xrTableRow69,
            this.xrTableRow66,
            this.xrTableRow65,
            this.xrTableRow64,
            this.xrTableRow63});
            this.xrTable3.SizeF = new System.Drawing.SizeF(1887.486F, 2159F);
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230,
            this.xrTableCell227,
            this.xrTableCell16,
            this.xrTableCell226,
            this.xrTableCell229,
            this.xrTableCell66,
            this.xrTableCell228,
            this.xrTableCell74});
            this.xrTableRow38.Dpi = 254F;
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell230.Dpi = 254F;
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.StylePriority.UseBorders = false;
            this.xrTableCell230.Weight = 0.15869037293521471D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell227.Dpi = 254F;
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.StylePriority.UseBorders = false;
            this.xrTableCell227.Weight = 0.054811234414819943D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell16.Dpi = 254F;
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Weight = 0.52634479657103728D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell226.Dpi = 254F;
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.StylePriority.UseBorders = false;
            this.xrTableCell226.Weight = 0.44998142626178439D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell229.Dpi = 254F;
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseBorders = false;
            this.xrTableCell229.Weight = 0.39716482711325179D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell66.Dpi = 254F;
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.Weight = 0.433388412000458D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell228.Dpi = 254F;
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseBorders = false;
            this.xrTableCell228.Text = "服药依从性";
            this.xrTableCell228.Weight = 0.64427303091311949D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell74.Dpi = 254F;
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.Weight = 0.2718740427070635D;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell303,
            this.xrTableCell304,
            this.xrTableCell305,
            this.xrTableCell306,
            this.xrTableCell307,
            this.xrTableCell308,
            this.xrTableCell309,
            this.xrTableCell310});
            this.xrTableRow48.Dpi = 254F;
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1D;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell303.Dpi = 254F;
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.StylePriority.UseBorders = false;
            this.xrTableCell303.Weight = 0.15869037293521471D;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell304.Dpi = 254F;
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.StylePriority.UseBorders = false;
            this.xrTableCell304.Weight = 0.054811234414819943D;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell305.Dpi = 254F;
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.StylePriority.UseBorders = false;
            this.xrTableCell305.Text = "药物名称";
            this.xrTableCell305.Weight = 0.52634479657103728D;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell306.Dpi = 254F;
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.StylePriority.UseBorders = false;
            this.xrTableCell306.Text = "用    法";
            this.xrTableCell306.Weight = 0.44998142626178439D;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell307.Dpi = 254F;
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.StylePriority.UseBorders = false;
            this.xrTableCell307.Text = "用    量";
            this.xrTableCell307.Weight = 0.39716482711325179D;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell308.Dpi = 254F;
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.StylePriority.UseBorders = false;
            this.xrTableCell308.Text = "用  药  时  间";
            this.xrTableCell308.Weight = 0.43338822208493433D;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell309.Dpi = 254F;
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.StylePriority.UseBorders = false;
            this.xrTableCell309.Text = " 1 规律  2 间断  3 不服药";
            this.xrTableCell309.Weight = 0.64427322082864324D;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell310.Dpi = 254F;
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.StylePriority.UseBorders = false;
            this.xrTableCell310.Weight = 0.2718740427070635D;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell311,
            this.xrTableCell312,
            this.xrTable_药物名称1,
            this.xrTable_药物用法1,
            this.xrTable_药物用量1,
            this.xrTable_用药时间1,
            this.xrTable_服药依从性1,
            this.xrTableCell318});
            this.xrTableRow49.Dpi = 254F;
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1D;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell311.Dpi = 254F;
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.StylePriority.UseBorders = false;
            this.xrTableCell311.Weight = 0.15869037293521471D;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell312.Dpi = 254F;
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.StylePriority.UseBorders = false;
            this.xrTableCell312.Text = "1";
            this.xrTableCell312.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称1
            // 
            this.xrTable_药物名称1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称1.Dpi = 254F;
            this.xrTable_药物名称1.Name = "xrTable_药物名称1";
            this.xrTable_药物名称1.StylePriority.UseBorders = false;
            this.xrTable_药物名称1.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法1
            // 
            this.xrTable_药物用法1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法1.Dpi = 254F;
            this.xrTable_药物用法1.Name = "xrTable_药物用法1";
            this.xrTable_药物用法1.StylePriority.UseBorders = false;
            this.xrTable_药物用法1.Weight = 0.44998142626178433D;
            // 
            // xrTable_药物用量1
            // 
            this.xrTable_药物用量1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量1.Dpi = 254F;
            this.xrTable_药物用量1.Name = "xrTable_药物用量1";
            this.xrTable_药物用量1.StylePriority.UseBorders = false;
            this.xrTable_药物用量1.Weight = 0.39716482711325191D;
            // 
            // xrTable_用药时间1
            // 
            this.xrTable_用药时间1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间1.Dpi = 254F;
            this.xrTable_用药时间1.Name = "xrTable_用药时间1";
            this.xrTable_用药时间1.StylePriority.UseBorders = false;
            this.xrTable_用药时间1.Weight = 0.43338860191598172D;
            // 
            // xrTable_服药依从性1
            // 
            this.xrTable_服药依从性1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性1.Dpi = 254F;
            this.xrTable_服药依从性1.Name = "xrTable_服药依从性1";
            this.xrTable_服药依从性1.StylePriority.UseBorders = false;
            this.xrTable_服药依从性1.Weight = 0.64427284099759585D;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell318.Dpi = 254F;
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.StylePriority.UseBorders = false;
            this.xrTableCell318.Weight = 0.2718740427070635D;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell295,
            this.xrTableCell296,
            this.xrTable_药物名称2,
            this.xrTable_药物用法2,
            this.xrTable_药物用量2,
            this.xrTable_用药时间2,
            this.xrTable_服药依从性2,
            this.xrTableCell302});
            this.xrTableRow47.Dpi = 254F;
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1D;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell295.Dpi = 254F;
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.StylePriority.UseBorders = false;
            this.xrTableCell295.Text = "主要";
            this.xrTableCell295.Weight = 0.15869037293521471D;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell296.Dpi = 254F;
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.StylePriority.UseBorders = false;
            this.xrTableCell296.Text = "2";
            this.xrTableCell296.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称2
            // 
            this.xrTable_药物名称2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称2.Dpi = 254F;
            this.xrTable_药物名称2.Name = "xrTable_药物名称2";
            this.xrTable_药物名称2.StylePriority.UseBorders = false;
            this.xrTable_药物名称2.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法2
            // 
            this.xrTable_药物用法2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法2.Dpi = 254F;
            this.xrTable_药物用法2.Name = "xrTable_药物用法2";
            this.xrTable_药物用法2.StylePriority.UseBorders = false;
            this.xrTable_药物用法2.Weight = 0.44998142626178439D;
            // 
            // xrTable_药物用量2
            // 
            this.xrTable_药物用量2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量2.Dpi = 254F;
            this.xrTable_药物用量2.Name = "xrTable_药物用量2";
            this.xrTable_药物用量2.StylePriority.UseBorders = false;
            this.xrTable_药物用量2.Weight = 0.39716482711325179D;
            // 
            // xrTable_用药时间2
            // 
            this.xrTable_用药时间2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间2.Dpi = 254F;
            this.xrTable_用药时间2.Name = "xrTable_用药时间2";
            this.xrTable_用药时间2.StylePriority.UseBorders = false;
            this.xrTable_用药时间2.Weight = 0.433388412000458D;
            // 
            // xrTable_服药依从性2
            // 
            this.xrTable_服药依从性2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性2.Dpi = 254F;
            this.xrTable_服药依从性2.Name = "xrTable_服药依从性2";
            this.xrTable_服药依从性2.StylePriority.UseBorders = false;
            this.xrTable_服药依从性2.Weight = 0.64427303091311949D;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell302.Dpi = 254F;
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.StylePriority.UseBorders = false;
            this.xrTableCell302.Weight = 0.2718740427070635D;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell287,
            this.xrTableCell288,
            this.xrTable_药物名称3,
            this.xrTable_药物用法3,
            this.xrTable_药物用量3,
            this.xrTable_用药时间3,
            this.xrTable_服药依从性3,
            this.xrTable_用药医师签字});
            this.xrTableRow46.Dpi = 254F;
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1D;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell287.Dpi = 254F;
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.StylePriority.UseBorders = false;
            this.xrTableCell287.Text = "用药";
            this.xrTableCell287.Weight = 0.15869037293521471D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell288.Dpi = 254F;
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.StylePriority.UseBorders = false;
            this.xrTableCell288.Text = "3";
            this.xrTableCell288.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称3
            // 
            this.xrTable_药物名称3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称3.Dpi = 254F;
            this.xrTable_药物名称3.Name = "xrTable_药物名称3";
            this.xrTable_药物名称3.StylePriority.UseBorders = false;
            this.xrTable_药物名称3.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法3
            // 
            this.xrTable_药物用法3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法3.Dpi = 254F;
            this.xrTable_药物用法3.Name = "xrTable_药物用法3";
            this.xrTable_药物用法3.StylePriority.UseBorders = false;
            this.xrTable_药物用法3.Weight = 0.44998142626178439D;
            // 
            // xrTable_药物用量3
            // 
            this.xrTable_药物用量3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量3.Dpi = 254F;
            this.xrTable_药物用量3.Name = "xrTable_药物用量3";
            this.xrTable_药物用量3.StylePriority.UseBorders = false;
            this.xrTable_药物用量3.Weight = 0.39716482711325179D;
            // 
            // xrTable_用药时间3
            // 
            this.xrTable_用药时间3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间3.Dpi = 254F;
            this.xrTable_用药时间3.Name = "xrTable_用药时间3";
            this.xrTable_用药时间3.StylePriority.UseBorders = false;
            this.xrTable_用药时间3.Weight = 0.43338822208493433D;
            // 
            // xrTable_服药依从性3
            // 
            this.xrTable_服药依从性3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性3.Dpi = 254F;
            this.xrTable_服药依从性3.Name = "xrTable_服药依从性3";
            this.xrTable_服药依从性3.StylePriority.UseBorders = false;
            this.xrTable_服药依从性3.Weight = 0.64427322082864324D;
            // 
            // xrTable_用药医师签字
            // 
            this.xrTable_用药医师签字.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_用药医师签字.Dpi = 254F;
            this.xrTable_用药医师签字.Name = "xrTable_用药医师签字";
            this.xrTable_用药医师签字.StylePriority.UseBorders = false;
            this.xrTable_用药医师签字.Weight = 0.2718740427070635D;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell279,
            this.xrTableCell280,
            this.xrTable_药物名称4,
            this.xrTable_药物用法4,
            this.xrTable_药物用量4,
            this.xrTable_用药时间4,
            this.xrTable_服药依从性4,
            this.xrTableCell286});
            this.xrTableRow45.Dpi = 254F;
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1D;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell279.Dpi = 254F;
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.StylePriority.UseBorders = false;
            this.xrTableCell279.Text = "情况";
            this.xrTableCell279.Weight = 0.15869037293521471D;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell280.Dpi = 254F;
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.StylePriority.UseBorders = false;
            this.xrTableCell280.Text = "4";
            this.xrTableCell280.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称4
            // 
            this.xrTable_药物名称4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称4.Dpi = 254F;
            this.xrTable_药物名称4.Name = "xrTable_药物名称4";
            this.xrTable_药物名称4.StylePriority.UseBorders = false;
            this.xrTable_药物名称4.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法4
            // 
            this.xrTable_药物用法4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法4.Dpi = 254F;
            this.xrTable_药物用法4.Name = "xrTable_药物用法4";
            this.xrTable_药物用法4.StylePriority.UseBorders = false;
            this.xrTable_药物用法4.Weight = 0.44998142626178439D;
            // 
            // xrTable_药物用量4
            // 
            this.xrTable_药物用量4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量4.Dpi = 254F;
            this.xrTable_药物用量4.Name = "xrTable_药物用量4";
            this.xrTable_药物用量4.StylePriority.UseBorders = false;
            this.xrTable_药物用量4.Weight = 0.39716482711325179D;
            // 
            // xrTable_用药时间4
            // 
            this.xrTable_用药时间4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间4.Dpi = 254F;
            this.xrTable_用药时间4.Name = "xrTable_用药时间4";
            this.xrTable_用药时间4.StylePriority.UseBorders = false;
            this.xrTable_用药时间4.Weight = 0.43338822208493433D;
            // 
            // xrTable_服药依从性4
            // 
            this.xrTable_服药依从性4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性4.Dpi = 254F;
            this.xrTable_服药依从性4.Name = "xrTable_服药依从性4";
            this.xrTable_服药依从性4.StylePriority.UseBorders = false;
            this.xrTable_服药依从性4.Weight = 0.64427322082864324D;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell286.Dpi = 254F;
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.StylePriority.UseBorders = false;
            this.xrTableCell286.Weight = 0.2718740427070635D;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell271,
            this.xrTableCell272,
            this.xrTable_药物名称5,
            this.xrTable_药物用法5,
            this.xrTable_药物用量5,
            this.xrTable_用药时间5,
            this.xrTable_服药依从性5,
            this.xrTableCell278});
            this.xrTableRow44.Dpi = 254F;
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1D;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell271.Dpi = 254F;
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.StylePriority.UseBorders = false;
            this.xrTableCell271.Weight = 0.15869037293521471D;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell272.Dpi = 254F;
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.StylePriority.UseBorders = false;
            this.xrTableCell272.Text = "5";
            this.xrTableCell272.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称5
            // 
            this.xrTable_药物名称5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称5.Dpi = 254F;
            this.xrTable_药物名称5.Name = "xrTable_药物名称5";
            this.xrTable_药物名称5.StylePriority.UseBorders = false;
            this.xrTable_药物名称5.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法5
            // 
            this.xrTable_药物用法5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法5.Dpi = 254F;
            this.xrTable_药物用法5.Name = "xrTable_药物用法5";
            this.xrTable_药物用法5.StylePriority.UseBorders = false;
            this.xrTable_药物用法5.Weight = 0.44998142626178439D;
            // 
            // xrTable_药物用量5
            // 
            this.xrTable_药物用量5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量5.Dpi = 254F;
            this.xrTable_药物用量5.Name = "xrTable_药物用量5";
            this.xrTable_药物用量5.StylePriority.UseBorders = false;
            this.xrTable_药物用量5.Weight = 0.39716482711325179D;
            // 
            // xrTable_用药时间5
            // 
            this.xrTable_用药时间5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间5.Dpi = 254F;
            this.xrTable_用药时间5.Name = "xrTable_用药时间5";
            this.xrTable_用药时间5.StylePriority.UseBorders = false;
            this.xrTable_用药时间5.Weight = 0.43338822208493433D;
            // 
            // xrTable_服药依从性5
            // 
            this.xrTable_服药依从性5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性5.Dpi = 254F;
            this.xrTable_服药依从性5.Name = "xrTable_服药依从性5";
            this.xrTable_服药依从性5.StylePriority.UseBorders = false;
            this.xrTable_服药依从性5.Weight = 0.64427322082864324D;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell278.Dpi = 254F;
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.StylePriority.UseBorders = false;
            this.xrTableCell278.Weight = 0.2718740427070635D;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTable_药物名称6,
            this.xrTable_药物用法6,
            this.xrTable_药物用量6,
            this.xrTable_用药时间6,
            this.xrTable_服药依从性6,
            this.xrTableCell270});
            this.xrTableRow43.Dpi = 254F;
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell263.Dpi = 254F;
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBorders = false;
            this.xrTableCell263.Weight = 0.15869037293521471D;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell264.Dpi = 254F;
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.StylePriority.UseBorders = false;
            this.xrTableCell264.Text = "6";
            this.xrTableCell264.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称6
            // 
            this.xrTable_药物名称6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称6.Dpi = 254F;
            this.xrTable_药物名称6.Name = "xrTable_药物名称6";
            this.xrTable_药物名称6.StylePriority.UseBorders = false;
            this.xrTable_药物名称6.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法6
            // 
            this.xrTable_药物用法6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法6.Dpi = 254F;
            this.xrTable_药物用法6.Name = "xrTable_药物用法6";
            this.xrTable_药物用法6.StylePriority.UseBorders = false;
            this.xrTable_药物用法6.Weight = 0.44998142626178439D;
            // 
            // xrTable_药物用量6
            // 
            this.xrTable_药物用量6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量6.Dpi = 254F;
            this.xrTable_药物用量6.Name = "xrTable_药物用量6";
            this.xrTable_药物用量6.StylePriority.UseBorders = false;
            this.xrTable_药物用量6.Weight = 0.39716482711325179D;
            // 
            // xrTable_用药时间6
            // 
            this.xrTable_用药时间6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间6.Dpi = 254F;
            this.xrTable_用药时间6.Name = "xrTable_用药时间6";
            this.xrTable_用药时间6.StylePriority.UseBorders = false;
            this.xrTable_用药时间6.Weight = 0.43338822208493433D;
            // 
            // xrTable_服药依从性6
            // 
            this.xrTable_服药依从性6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性6.Dpi = 254F;
            this.xrTable_服药依从性6.Name = "xrTable_服药依从性6";
            this.xrTable_服药依从性6.StylePriority.UseBorders = false;
            this.xrTable_服药依从性6.Weight = 0.64427322082864324D;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell270.Dpi = 254F;
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.StylePriority.UseBorders = false;
            this.xrTableCell270.Weight = 0.2718740427070635D;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell255,
            this.xrTableCell256,
            this.xrTableCell257,
            this.xrTableCell258,
            this.xrTableCell261,
            this.xrTableCell262});
            this.xrTableRow42.Dpi = 254F;
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell255.Dpi = 254F;
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.StylePriority.UseBorders = false;
            this.xrTableCell255.Text = "非免";
            this.xrTableCell255.Weight = 0.15869037293521471D;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Dpi = 254F;
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Weight = 0.054811234414819943D;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell257.Dpi = 254F;
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.StylePriority.UseBorders = false;
            this.xrTableCell257.Weight = 0.52634498648656092D;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell258.Dpi = 254F;
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.StylePriority.UseBorders = false;
            this.xrTableCell258.Weight = 0.44998095147297529D;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell261.Dpi = 254F;
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseBorders = false;
            this.xrTableCell261.Weight = 1.4748265549001149D;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell262.Dpi = 254F;
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.StylePriority.UseBorders = false;
            this.xrTableCell262.Weight = 0.2718740427070635D;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell247,
            this.xrTableCell248,
            this.xrTableCell249,
            this.xrTableCell250,
            this.xrTableCell253,
            this.xrTableCell254});
            this.xrTableRow41.Dpi = 254F;
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell247.Dpi = 254F;
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.StylePriority.UseBorders = false;
            this.xrTableCell247.Text = "疫规";
            this.xrTableCell247.Weight = 0.15869037293521471D;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell248.Dpi = 254F;
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.StylePriority.UseBorders = false;
            this.xrTableCell248.Weight = 0.054811234414819943D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell249.Dpi = 254F;
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.StylePriority.UseBorders = false;
            this.xrTableCell249.Text = "名    称";
            this.xrTableCell249.Weight = 0.52634498648656092D;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell250.Dpi = 254F;
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseBorders = false;
            this.xrTableCell250.Text = "接  种  日  期";
            this.xrTableCell250.Weight = 0.44998095147297529D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell253.Dpi = 254F;
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.StylePriority.UseBorders = false;
            this.xrTableCell253.Text = "接    种    机    构";
            this.xrTableCell253.Weight = 1.4748265549001149D;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell254.Dpi = 254F;
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.StylePriority.UseBorders = false;
            this.xrTableCell254.Weight = 0.2718740427070635D;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell239,
            this.xrTableCell240,
            this.xrTable_接种史名称1,
            this.xrTable_接种日期1,
            this.xrTable_接种机构1,
            this.xrTable_接种医师签字});
            this.xrTableRow40.Dpi = 254F;
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell239.Dpi = 254F;
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.StylePriority.UseBorders = false;
            this.xrTableCell239.Text = "划预";
            this.xrTableCell239.Weight = 0.15869037293521471D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell240.Dpi = 254F;
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.StylePriority.UseBorders = false;
            this.xrTableCell240.Text = "1";
            this.xrTableCell240.Weight = 0.054811234414819943D;
            // 
            // xrTable_接种史名称1
            // 
            this.xrTable_接种史名称1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种史名称1.Dpi = 254F;
            this.xrTable_接种史名称1.Name = "xrTable_接种史名称1";
            this.xrTable_接种史名称1.StylePriority.UseBorders = false;
            this.xrTable_接种史名称1.Weight = 0.52634498648656092D;
            // 
            // xrTable_接种日期1
            // 
            this.xrTable_接种日期1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种日期1.Dpi = 254F;
            this.xrTable_接种日期1.Name = "xrTable_接种日期1";
            this.xrTable_接种日期1.StylePriority.UseBorders = false;
            this.xrTable_接种日期1.Weight = 0.44998095147297529D;
            // 
            // xrTable_接种机构1
            // 
            this.xrTable_接种机构1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种机构1.Dpi = 254F;
            this.xrTable_接种机构1.Name = "xrTable_接种机构1";
            this.xrTable_接种机构1.StylePriority.UseBorders = false;
            this.xrTable_接种机构1.Weight = 1.4748265549001149D;
            // 
            // xrTable_接种医师签字
            // 
            this.xrTable_接种医师签字.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_接种医师签字.Dpi = 254F;
            this.xrTable_接种医师签字.Name = "xrTable_接种医师签字";
            this.xrTable_接种医师签字.StylePriority.UseBorders = false;
            this.xrTable_接种医师签字.Weight = 0.2718740427070635D;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTable_接种史名称2,
            this.xrTable_接种日期2,
            this.xrTable_接种机构2,
            this.xrTableCell238});
            this.xrTableRow39.Dpi = 254F;
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell231.Dpi = 254F;
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.StylePriority.UseBorders = false;
            this.xrTableCell231.Text = "防接";
            this.xrTableCell231.Weight = 0.15869037293521471D;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell232.Dpi = 254F;
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.StylePriority.UseBorders = false;
            this.xrTableCell232.Text = "2";
            this.xrTableCell232.Weight = 0.054811234414819943D;
            // 
            // xrTable_接种史名称2
            // 
            this.xrTable_接种史名称2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种史名称2.Dpi = 254F;
            this.xrTable_接种史名称2.Name = "xrTable_接种史名称2";
            this.xrTable_接种史名称2.StylePriority.UseBorders = false;
            this.xrTable_接种史名称2.Weight = 0.52634498648656092D;
            // 
            // xrTable_接种日期2
            // 
            this.xrTable_接种日期2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种日期2.Dpi = 254F;
            this.xrTable_接种日期2.Name = "xrTable_接种日期2";
            this.xrTable_接种日期2.StylePriority.UseBorders = false;
            this.xrTable_接种日期2.Weight = 0.44998095147297529D;
            // 
            // xrTable_接种机构2
            // 
            this.xrTable_接种机构2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种机构2.Dpi = 254F;
            this.xrTable_接种机构2.Name = "xrTable_接种机构2";
            this.xrTable_接种机构2.StylePriority.UseBorders = false;
            this.xrTable_接种机构2.Weight = 1.4748265549001149D;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell238.Dpi = 254F;
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseBorders = false;
            this.xrTableCell238.Weight = 0.2718740427070635D;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell319,
            this.xrTableCell320,
            this.xrTable_接种史名称3,
            this.xrTable_接种日期3,
            this.xrTable_接种机构3,
            this.xrTableCell326});
            this.xrTableRow50.Dpi = 254F;
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1D;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell319.Dpi = 254F;
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.StylePriority.UseBorders = false;
            this.xrTableCell319.Text = "种史";
            this.xrTableCell319.Weight = 0.15869037293521471D;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell320.Dpi = 254F;
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.StylePriority.UseBorders = false;
            this.xrTableCell320.Text = "3";
            this.xrTableCell320.Weight = 0.054811234414819943D;
            // 
            // xrTable_接种史名称3
            // 
            this.xrTable_接种史名称3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种史名称3.Dpi = 254F;
            this.xrTable_接种史名称3.Name = "xrTable_接种史名称3";
            this.xrTable_接种史名称3.StylePriority.UseBorders = false;
            this.xrTable_接种史名称3.Weight = 0.52634498648656092D;
            // 
            // xrTable_接种日期3
            // 
            this.xrTable_接种日期3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种日期3.Dpi = 254F;
            this.xrTable_接种日期3.Name = "xrTable_接种日期3";
            this.xrTable_接种日期3.StylePriority.UseBorders = false;
            this.xrTable_接种日期3.Weight = 0.44998095147297529D;
            // 
            // xrTable_接种机构3
            // 
            this.xrTable_接种机构3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种机构3.Dpi = 254F;
            this.xrTable_接种机构3.Name = "xrTable_接种机构3";
            this.xrTable_接种机构3.StylePriority.UseBorders = false;
            this.xrTable_接种机构3.Weight = 1.4748265549001149D;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell326.Dpi = 254F;
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.StylePriority.UseBorders = false;
            this.xrTableCell326.Weight = 0.2718740427070635D;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell329,
            this.xrTableCell332,
            this.xrTableCell333,
            this.xrTableCell334});
            this.xrTableRow53.Dpi = 254F;
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1D;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell329.Dpi = 254F;
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.StylePriority.UseBorders = false;
            this.xrTableCell329.Weight = 0.15869037293521471D;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.Dpi = 254F;
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.StylePriority.UseTextAlignment = false;
            this.xrTableCell332.Text = "  1  体检无异常";
            this.xrTableCell332.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell332.Weight = 2.1968891267475317D;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell333.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康评价});
            this.xrTableCell333.Dpi = 254F;
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.StylePriority.UseBorders = false;
            this.xrTableCell333.Weight = 0.30907460052693964D;
            // 
            // xrLabel_健康评价
            // 
            this.xrLabel_健康评价.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_健康评价.Dpi = 254F;
            this.xrLabel_健康评价.LocationFloat = new DevExpress.Utils.PointFloat(131.7281F, 7.996541F);
            this.xrLabel_健康评价.Name = "xrLabel_健康评价";
            this.xrLabel_健康评价.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_健康评价.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_健康评价.StylePriority.UseBorders = false;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell334.Dpi = 254F;
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.StylePriority.UseBorders = false;
            this.xrTableCell334.Weight = 0.2718740427070635D;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell341,
            this.xrTableCell344,
            this.xrTableCell345,
            this.xrTableCell346});
            this.xrTableRow55.Dpi = 254F;
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 1D;
            // 
            // xrTableCell341
            // 
            this.xrTableCell341.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell341.Dpi = 254F;
            this.xrTableCell341.Name = "xrTableCell341";
            this.xrTableCell341.StylePriority.UseBorders = false;
            this.xrTableCell341.Text = "健";
            this.xrTableCell341.Weight = 0.15869037293521471D;
            // 
            // xrTableCell344
            // 
            this.xrTableCell344.Dpi = 254F;
            this.xrTableCell344.Name = "xrTableCell344";
            this.xrTableCell344.StylePriority.UseTextAlignment = false;
            this.xrTableCell344.Text = "  2  有异常";
            this.xrTableCell344.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell344.Weight = 2.1968891267475317D;
            // 
            // xrTableCell345
            // 
            this.xrTableCell345.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell345.Dpi = 254F;
            this.xrTableCell345.Name = "xrTableCell345";
            this.xrTableCell345.StylePriority.UseBorders = false;
            this.xrTableCell345.Weight = 0.30907460052693964D;
            // 
            // xrTableCell346
            // 
            this.xrTableCell346.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell346.Dpi = 254F;
            this.xrTableCell346.Name = "xrTableCell346";
            this.xrTableCell346.StylePriority.UseBorders = false;
            this.xrTableCell346.Weight = 0.2718740427070635D;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell347,
            this.xrTableCell349,
            this.xrTableCell350,
            this.xrTableCell351,
            this.xrTable_健康评价医师签字});
            this.xrTableRow56.Dpi = 254F;
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 1D;
            // 
            // xrTableCell347
            // 
            this.xrTableCell347.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell347.Dpi = 254F;
            this.xrTableCell347.Name = "xrTableCell347";
            this.xrTableCell347.StylePriority.UseBorders = false;
            this.xrTableCell347.Text = "康";
            this.xrTableCell347.Weight = 0.15869037293521471D;
            // 
            // xrTableCell349
            // 
            this.xrTableCell349.Dpi = 254F;
            this.xrTableCell349.Name = "xrTableCell349";
            this.xrTableCell349.StylePriority.UseTextAlignment = false;
            this.xrTableCell349.Text = "  异常1";
            this.xrTableCell349.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell349.Weight = 0.23867540854152658D;
            // 
            // xrTableCell350
            // 
            this.xrTableCell350.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康评价异常1});
            this.xrTableCell350.Dpi = 254F;
            this.xrTableCell350.Name = "xrTableCell350";
            this.xrTableCell350.Weight = 1.9582137182060049D;
            // 
            // xrLabel_健康评价异常1
            // 
            this.xrLabel_健康评价异常1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_健康评价异常1.Dpi = 254F;
            this.xrLabel_健康评价异常1.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 2.91658F);
            this.xrLabel_健康评价异常1.Name = "xrLabel_健康评价异常1";
            this.xrLabel_健康评价异常1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_健康评价异常1.SizeF = new System.Drawing.SizeF(764.6459F, 49.953F);
            this.xrLabel_健康评价异常1.StylePriority.UseBorders = false;
            // 
            // xrTableCell351
            // 
            this.xrTableCell351.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell351.Dpi = 254F;
            this.xrTableCell351.Name = "xrTableCell351";
            this.xrTableCell351.StylePriority.UseBorders = false;
            this.xrTableCell351.Weight = 0.30907460052693964D;
            // 
            // xrTable_健康评价医师签字
            // 
            this.xrTable_健康评价医师签字.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_健康评价医师签字.Dpi = 254F;
            this.xrTable_健康评价医师签字.Name = "xrTable_健康评价医师签字";
            this.xrTable_健康评价医师签字.StylePriority.UseBorders = false;
            this.xrTable_健康评价医师签字.Weight = 0.2718740427070635D;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell335,
            this.xrTableCell337,
            this.xrTableCell338,
            this.xrTableCell339,
            this.xrTableCell340});
            this.xrTableRow54.Dpi = 254F;
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1D;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell335.Dpi = 254F;
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.StylePriority.UseBorders = false;
            this.xrTableCell335.Text = "评";
            this.xrTableCell335.Weight = 0.15869037293521471D;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.Dpi = 254F;
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.StylePriority.UseTextAlignment = false;
            this.xrTableCell337.Text = "  异常2";
            this.xrTableCell337.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell337.Weight = 0.23867540854152658D;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康评价异常2});
            this.xrTableCell338.Dpi = 254F;
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.Weight = 1.9582137182060049D;
            // 
            // xrLabel_健康评价异常2
            // 
            this.xrLabel_健康评价异常2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_健康评价异常2.Dpi = 254F;
            this.xrLabel_健康评价异常2.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 0F);
            this.xrLabel_健康评价异常2.Name = "xrLabel_健康评价异常2";
            this.xrLabel_健康评价异常2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_健康评价异常2.SizeF = new System.Drawing.SizeF(764.6459F, 49.95313F);
            this.xrLabel_健康评价异常2.StylePriority.UseBorders = false;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell339.Dpi = 254F;
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.StylePriority.UseBorders = false;
            this.xrTableCell339.Weight = 0.30907460052693964D;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell340.Dpi = 254F;
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.StylePriority.UseBorders = false;
            this.xrTableCell340.Weight = 0.2718740427070635D;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell259,
            this.xrTableCell323,
            this.xrTableCell324,
            this.xrTableCell327,
            this.xrTableCell328});
            this.xrTableRow52.Dpi = 254F;
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 1D;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell259.Dpi = 254F;
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.StylePriority.UseBorders = false;
            this.xrTableCell259.Text = "价";
            this.xrTableCell259.Weight = 0.15869037293521471D;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.Dpi = 254F;
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.StylePriority.UseTextAlignment = false;
            this.xrTableCell323.Text = "  异常3";
            this.xrTableCell323.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell323.Weight = 0.23867540854152658D;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康评价异常3});
            this.xrTableCell324.Dpi = 254F;
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.Weight = 1.9582137182060049D;
            // 
            // xrLabel_健康评价异常3
            // 
            this.xrLabel_健康评价异常3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_健康评价异常3.Dpi = 254F;
            this.xrLabel_健康评价异常3.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 0F);
            this.xrLabel_健康评价异常3.Name = "xrLabel_健康评价异常3";
            this.xrLabel_健康评价异常3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_健康评价异常3.SizeF = new System.Drawing.SizeF(764.6458F, 49.95313F);
            this.xrLabel_健康评价异常3.StylePriority.UseBorders = false;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell327.Dpi = 254F;
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.StylePriority.UseBorders = false;
            this.xrTableCell327.Weight = 0.30907460052693964D;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell328.Dpi = 254F;
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.StylePriority.UseBorders = false;
            this.xrTableCell328.Weight = 0.2718740427070635D;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell235,
            this.xrTableCell243,
            this.xrTableCell244,
            this.xrTableCell251,
            this.xrTableCell252});
            this.xrTableRow51.Dpi = 254F;
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell235.Dpi = 254F;
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.StylePriority.UseBorders = false;
            this.xrTableCell235.Weight = 0.15869037293521471D;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell243.Dpi = 254F;
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.StylePriority.UseBorders = false;
            this.xrTableCell243.StylePriority.UseTextAlignment = false;
            this.xrTableCell243.Text = "  异常4";
            this.xrTableCell243.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell243.Weight = 0.23867540854152658D;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell244.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康评价异常4});
            this.xrTableCell244.Dpi = 254F;
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.StylePriority.UseBorders = false;
            this.xrTableCell244.Weight = 1.9582137182060049D;
            // 
            // xrLabel_健康评价异常4
            // 
            this.xrLabel_健康评价异常4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_健康评价异常4.Dpi = 254F;
            this.xrLabel_健康评价异常4.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 0F);
            this.xrLabel_健康评价异常4.Name = "xrLabel_健康评价异常4";
            this.xrLabel_健康评价异常4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_健康评价异常4.SizeF = new System.Drawing.SizeF(764.6456F, 49.95313F);
            this.xrLabel_健康评价异常4.StylePriority.UseBorders = false;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell251.Dpi = 254F;
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.StylePriority.UseBorders = false;
            this.xrTableCell251.Weight = 0.30907460052693964D;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell252.Dpi = 254F;
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.StylePriority.UseBorders = false;
            this.xrTableCell252.Weight = 0.2718740427070635D;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell342,
            this.xrTableCell348,
            this.xrTableCell366,
            this.xrTableCell353,
            this.xrTableCell354});
            this.xrTableRow58.Dpi = 254F;
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 1D;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell342.Dpi = 254F;
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.StylePriority.UseBorders = false;
            this.xrTableCell342.Weight = 0.15869037293521471D;
            // 
            // xrTableCell348
            // 
            this.xrTableCell348.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell348.Dpi = 254F;
            this.xrTableCell348.Name = "xrTableCell348";
            this.xrTableCell348.StylePriority.UseBorders = false;
            this.xrTableCell348.StylePriority.UseTextAlignment = false;
            this.xrTableCell348.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell348.Weight = 1.1068780945547609D;
            // 
            // xrTableCell366
            // 
            this.xrTableCell366.Dpi = 254F;
            this.xrTableCell366.Name = "xrTableCell366";
            this.xrTableCell366.Text = "  危险因素控制：";
            this.xrTableCell366.Weight = 0.45256147612980274D;
            // 
            // xrTableCell353
            // 
            this.xrTableCell353.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell353.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel78,
            this.xrLabel_危险控制因素7,
            this.xrLabel_危险控制因素1,
            this.xrLabel75,
            this.xrLabel_危险控制因素2,
            this.xrLabel73,
            this.xrLabel_危险控制因素3,
            this.xrLabel71,
            this.xrLabel_危险控制因素4,
            this.xrLabel69,
            this.xrLabel_危险控制因素5,
            this.xrLabel67,
            this.xrLabel_危险控制因素6});
            this.xrTableCell353.Dpi = 254F;
            this.xrTableCell353.Name = "xrTableCell353";
            this.xrTableCell353.StylePriority.UseBorders = false;
            this.xrTableCell353.Weight = 0.94652415658990741D;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel78.Dpi = 254F;
            this.xrLabel78.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(499.5227F, 7.999964F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel78.StylePriority.UseBorders = false;
            this.xrLabel78.StylePriority.UseFont = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "/";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素7
            // 
            this.xrLabel_危险控制因素7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素7.Dpi = 254F;
            this.xrLabel_危险控制因素7.LocationFloat = new DevExpress.Utils.PointFloat(541.4558F, 7.99977F);
            this.xrLabel_危险控制因素7.Name = "xrLabel_危险控制因素7";
            this.xrLabel_危险控制因素7.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_危险控制因素7.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_危险控制因素7.StylePriority.UseBorders = false;
            // 
            // xrLabel_危险控制因素1
            // 
            this.xrLabel_危险控制因素1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素1.Dpi = 254F;
            this.xrLabel_危险控制因素1.LocationFloat = new DevExpress.Utils.PointFloat(37.12637F, 7.999705F);
            this.xrLabel_危险控制因素1.Name = "xrLabel_危险控制因素1";
            this.xrLabel_危险控制因素1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_危险控制因素1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_危险控制因素1.StylePriority.UseBorders = false;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel75.Dpi = 254F;
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(79.05948F, 7.999964F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel75.StylePriority.UseBorders = false;
            this.xrLabel75.StylePriority.UseFont = false;
            this.xrLabel75.StylePriority.UseTextAlignment = false;
            this.xrLabel75.Text = "/";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素2
            // 
            this.xrLabel_危险控制因素2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素2.Dpi = 254F;
            this.xrLabel_危险控制因素2.LocationFloat = new DevExpress.Utils.PointFloat(120.9926F, 7.999705F);
            this.xrLabel_危险控制因素2.Name = "xrLabel_危险控制因素2";
            this.xrLabel_危险控制因素2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_危险控制因素2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_危险控制因素2.StylePriority.UseBorders = false;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel73.Dpi = 254F;
            this.xrLabel73.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(162.9257F, 7.999964F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel73.StylePriority.UseBorders = false;
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "/";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素3
            // 
            this.xrLabel_危险控制因素3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素3.Dpi = 254F;
            this.xrLabel_危险控制因素3.LocationFloat = new DevExpress.Utils.PointFloat(204.8588F, 7.999964F);
            this.xrLabel_危险控制因素3.Name = "xrLabel_危险控制因素3";
            this.xrLabel_危险控制因素3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_危险控制因素3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_危险控制因素3.StylePriority.UseBorders = false;
            // 
            // xrLabel71
            // 
            this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel71.Dpi = 254F;
            this.xrLabel71.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(246.7919F, 7.999964F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel71.StylePriority.UseBorders = false;
            this.xrLabel71.StylePriority.UseFont = false;
            this.xrLabel71.StylePriority.UseTextAlignment = false;
            this.xrLabel71.Text = "/";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素4
            // 
            this.xrLabel_危险控制因素4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素4.Dpi = 254F;
            this.xrLabel_危险控制因素4.LocationFloat = new DevExpress.Utils.PointFloat(288.725F, 7.999964F);
            this.xrLabel_危险控制因素4.Name = "xrLabel_危险控制因素4";
            this.xrLabel_危险控制因素4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_危险控制因素4.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_危险控制因素4.StylePriority.UseBorders = false;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel69.Dpi = 254F;
            this.xrLabel69.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(331.7903F, 7.999964F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel69.StylePriority.UseBorders = false;
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.Text = "/";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素5
            // 
            this.xrLabel_危险控制因素5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素5.Dpi = 254F;
            this.xrLabel_危险控制因素5.LocationFloat = new DevExpress.Utils.PointFloat(373.7234F, 7.99977F);
            this.xrLabel_危险控制因素5.Name = "xrLabel_危险控制因素5";
            this.xrLabel_危险控制因素5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_危险控制因素5.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_危险控制因素5.StylePriority.UseBorders = false;
            // 
            // xrLabel67
            // 
            this.xrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel67.Dpi = 254F;
            this.xrLabel67.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(415.6565F, 7.999899F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel67.StylePriority.UseBorders = false;
            this.xrLabel67.StylePriority.UseFont = false;
            this.xrLabel67.StylePriority.UseTextAlignment = false;
            this.xrLabel67.Text = "/";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素6
            // 
            this.xrLabel_危险控制因素6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素6.Dpi = 254F;
            this.xrLabel_危险控制因素6.LocationFloat = new DevExpress.Utils.PointFloat(457.5896F, 8F);
            this.xrLabel_危险控制因素6.Name = "xrLabel_危险控制因素6";
            this.xrLabel_危险控制因素6.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_危险控制因素6.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_危险控制因素6.StylePriority.UseBorders = false;
            // 
            // xrTableCell354
            // 
            this.xrTableCell354.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell354.Dpi = 254F;
            this.xrTableCell354.Name = "xrTableCell354";
            this.xrTableCell354.StylePriority.UseBorders = false;
            this.xrTableCell354.Weight = 0.2718740427070635D;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell365,
            this.xrTableCell367,
            this.xrTableCell368,
            this.xrTableCell369});
            this.xrTableRow61.Dpi = 254F;
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 1D;
            // 
            // xrTableCell365
            // 
            this.xrTableCell365.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell365.Dpi = 254F;
            this.xrTableCell365.Name = "xrTableCell365";
            this.xrTableCell365.StylePriority.UseBorders = false;
            this.xrTableCell365.Text = "健";
            this.xrTableCell365.Weight = 0.15869037293521471D;
            // 
            // xrTableCell367
            // 
            this.xrTableCell367.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell367.Dpi = 254F;
            this.xrTableCell367.Name = "xrTableCell367";
            this.xrTableCell367.StylePriority.UseBorders = false;
            this.xrTableCell367.StylePriority.UseTextAlignment = false;
            this.xrTableCell367.Text = "    1  纳入慢性病患者健康管理";
            this.xrTableCell367.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell367.Weight = 1.1068780945547609D;
            // 
            // xrTableCell368
            // 
            this.xrTableCell368.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell368.Dpi = 254F;
            this.xrTableCell368.Name = "xrTableCell368";
            this.xrTableCell368.StylePriority.UseBorders = false;
            this.xrTableCell368.StylePriority.UseTextAlignment = false;
            this.xrTableCell368.Text = "    1  戒烟          2健康饮酒          3  饮食          4  锻炼";
            this.xrTableCell368.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell368.Weight = 1.3990856327197099D;
            // 
            // xrTableCell369
            // 
            this.xrTableCell369.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell369.Dpi = 254F;
            this.xrTableCell369.Name = "xrTableCell369";
            this.xrTableCell369.StylePriority.UseBorders = false;
            this.xrTableCell369.Weight = 0.2718740427070635D;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell360,
            this.xrTableCell362,
            this.xrTableCell356,
            this.xrTableCell361,
            this.xrTableCell363,
            this.xrTable_健康指导医师签字});
            this.xrTableRow60.Dpi = 254F;
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 1D;
            // 
            // xrTableCell360
            // 
            this.xrTableCell360.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell360.Dpi = 254F;
            this.xrTableCell360.Name = "xrTableCell360";
            this.xrTableCell360.StylePriority.UseBorders = false;
            this.xrTableCell360.Text = "康";
            this.xrTableCell360.Weight = 0.15869037293521471D;
            // 
            // xrTableCell362
            // 
            this.xrTableCell362.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell362.Dpi = 254F;
            this.xrTableCell362.Name = "xrTableCell362";
            this.xrTableCell362.StylePriority.UseBorders = false;
            this.xrTableCell362.StylePriority.UseTextAlignment = false;
            this.xrTableCell362.Text = "    2  建议复查";
            this.xrTableCell362.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell362.Weight = 1.1068780945547609D;
            // 
            // xrTableCell356
            // 
            this.xrTableCell356.Dpi = 254F;
            this.xrTableCell356.Name = "xrTableCell356";
            this.xrTableCell356.StylePriority.UseTextAlignment = false;
            this.xrTableCell356.Text = "    5  减体重（目标";
            this.xrTableCell356.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell356.Weight = 0.47231990738199747D;
            // 
            // xrTableCell361
            // 
            this.xrTableCell361.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_减重目标});
            this.xrTableCell361.Dpi = 254F;
            this.xrTableCell361.Name = "xrTableCell361";
            this.xrTableCell361.Weight = 0.49643689990255435D;
            // 
            // xrLabel_减重目标
            // 
            this.xrLabel_减重目标.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_减重目标.Dpi = 254F;
            this.xrLabel_减重目标.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 0F);
            this.xrLabel_减重目标.Name = "xrLabel_减重目标";
            this.xrLabel_减重目标.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_减重目标.SizeF = new System.Drawing.SizeF(317.9581F, 49.95313F);
            this.xrLabel_减重目标.StylePriority.UseBorders = false;
            // 
            // xrTableCell363
            // 
            this.xrTableCell363.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell363.Dpi = 254F;
            this.xrTableCell363.Name = "xrTableCell363";
            this.xrTableCell363.StylePriority.UseBorders = false;
            this.xrTableCell363.StylePriority.UseTextAlignment = false;
            this.xrTableCell363.Text = "  kg）";
            this.xrTableCell363.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell363.Weight = 0.43032882543515816D;
            // 
            // xrTable_健康指导医师签字
            // 
            this.xrTable_健康指导医师签字.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_健康指导医师签字.Dpi = 254F;
            this.xrTable_健康指导医师签字.Name = "xrTable_健康指导医师签字";
            this.xrTable_健康指导医师签字.StylePriority.UseBorders = false;
            this.xrTable_健康指导医师签字.Weight = 0.2718740427070635D;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell355,
            this.xrTableCell357,
            this.xrTableCell343,
            this.xrTableCell358,
            this.xrTableCell359});
            this.xrTableRow59.Dpi = 254F;
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 1D;
            // 
            // xrTableCell355
            // 
            this.xrTableCell355.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell355.Dpi = 254F;
            this.xrTableCell355.Name = "xrTableCell355";
            this.xrTableCell355.StylePriority.UseBorders = false;
            this.xrTableCell355.Text = "指";
            this.xrTableCell355.Weight = 0.15869037293521471D;
            // 
            // xrTableCell357
            // 
            this.xrTableCell357.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell357.Dpi = 254F;
            this.xrTableCell357.Name = "xrTableCell357";
            this.xrTableCell357.StylePriority.UseBorders = false;
            this.xrTableCell357.StylePriority.UseTextAlignment = false;
            this.xrTableCell357.Text = "    3  建议转诊";
            this.xrTableCell357.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell357.Weight = 1.1068780945547609D;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.Dpi = 254F;
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.StylePriority.UseTextAlignment = false;
            this.xrTableCell343.Text = "    6  建议接种疫苗";
            this.xrTableCell343.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell343.Weight = 0.47231990738199747D;
            // 
            // xrTableCell358
            // 
            this.xrTableCell358.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell358.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_建议接种疫苗});
            this.xrTableCell358.Dpi = 254F;
            this.xrTableCell358.Name = "xrTableCell358";
            this.xrTableCell358.StylePriority.UseBorders = false;
            this.xrTableCell358.Weight = 0.92676572533771251D;
            // 
            // xrLabel_建议接种疫苗
            // 
            this.xrLabel_建议接种疫苗.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_建议接种疫苗.Dpi = 254F;
            this.xrLabel_建议接种疫苗.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_建议接种疫苗.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_建议接种疫苗.Name = "xrLabel_建议接种疫苗";
            this.xrLabel_建议接种疫苗.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_建议接种疫苗.SizeF = new System.Drawing.SizeF(570.6891F, 49.95313F);
            this.xrLabel_建议接种疫苗.StylePriority.UseBorders = false;
            this.xrLabel_建议接种疫苗.StylePriority.UseFont = false;
            this.xrLabel_建议接种疫苗.StylePriority.UseTextAlignment = false;
            this.xrLabel_建议接种疫苗.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell359
            // 
            this.xrTableCell359.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell359.Dpi = 254F;
            this.xrTableCell359.Name = "xrTableCell359";
            this.xrTableCell359.StylePriority.UseBorders = false;
            this.xrTableCell359.Weight = 0.2718740427070635D;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell236,
            this.xrTableCell330,
            this.xrTableCell260,
            this.xrTableCell331,
            this.xrTableCell336});
            this.xrTableRow57.Dpi = 254F;
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 1D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell236.Dpi = 254F;
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.StylePriority.UseBorders = false;
            this.xrTableCell236.Text = "导";
            this.xrTableCell236.Weight = 0.15869037293521471D;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell330.Dpi = 254F;
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.StylePriority.UseBorders = false;
            this.xrTableCell330.StylePriority.UseTextAlignment = false;
            this.xrTableCell330.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell330.Weight = 1.1068780945547609D;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell260.Dpi = 254F;
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.StylePriority.UseBorders = false;
            this.xrTableCell260.StylePriority.UseTextAlignment = false;
            this.xrTableCell260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell260.Weight = 0.47231985085896477D;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell331.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_建议接种疫苗补充});
            this.xrTableCell331.Dpi = 254F;
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.StylePriority.UseBorders = false;
            this.xrTableCell331.Weight = 0.9267657818607451D;
            // 
            // xrLabel_建议接种疫苗补充
            // 
            this.xrLabel_建议接种疫苗补充.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_建议接种疫苗补充.Dpi = 254F;
            this.xrLabel_建议接种疫苗补充.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_建议接种疫苗补充.LocationFloat = new DevExpress.Utils.PointFloat(0.0001220703F, 0.6333008F);
            this.xrLabel_建议接种疫苗补充.Name = "xrLabel_建议接种疫苗补充";
            this.xrLabel_建议接种疫苗补充.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_建议接种疫苗补充.SizeF = new System.Drawing.SizeF(570.6891F, 49.95313F);
            this.xrLabel_建议接种疫苗补充.StylePriority.UseBorders = false;
            this.xrLabel_建议接种疫苗补充.StylePriority.UseFont = false;
            this.xrLabel_建议接种疫苗补充.StylePriority.UseTextAlignment = false;
            this.xrLabel_建议接种疫苗补充.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell336.Dpi = 254F;
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.StylePriority.UseBorders = false;
            this.xrTableCell336.Weight = 0.2718740427070635D;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell233,
            this.xrTableCell234,
            this.xrTableCell237,
            this.xrTableCell241,
            this.xrTableCell242});
            this.xrTableRow70.Dpi = 254F;
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 1D;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell233.Dpi = 254F;
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.StylePriority.UseBorders = false;
            this.xrTableCell233.Weight = 0.15869037293521471D;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell234.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康指导3,
            this.xrLabel34,
            this.xrLabel_健康指导2,
            this.xrLabel35,
            this.xrLabel_健康指导1});
            this.xrTableCell234.Dpi = 254F;
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.StylePriority.UseBorders = false;
            this.xrTableCell234.Weight = 1.1068780945547609D;
            // 
            // xrLabel_健康指导3
            // 
            this.xrLabel_健康指导3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_健康指导3.Dpi = 254F;
            this.xrLabel_健康指导3.LocationFloat = new DevExpress.Utils.PointFloat(644.525F, 8F);
            this.xrLabel_健康指导3.Name = "xrLabel_健康指导3";
            this.xrLabel_健康指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_健康指导3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_健康指导3.StylePriority.UseBorders = false;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.Dpi = 254F;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(602.5919F, 8.000029F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "/";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_健康指导2
            // 
            this.xrLabel_健康指导2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_健康指导2.Dpi = 254F;
            this.xrLabel_健康指导2.LocationFloat = new DevExpress.Utils.PointFloat(560.6588F, 8.000029F);
            this.xrLabel_健康指导2.Name = "xrLabel_健康指导2";
            this.xrLabel_健康指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_健康指导2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_健康指导2.StylePriority.UseBorders = false;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.Dpi = 254F;
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(518.7257F, 8.000029F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "/";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_健康指导1
            // 
            this.xrLabel_健康指导1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_健康指导1.Dpi = 254F;
            this.xrLabel_健康指导1.LocationFloat = new DevExpress.Utils.PointFloat(476.7926F, 7.999964F);
            this.xrLabel_健康指导1.Name = "xrLabel_健康指导1";
            this.xrLabel_健康指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_健康指导1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_健康指导1.StylePriority.UseBorders = false;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell237.Dpi = 254F;
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.StylePriority.UseBorders = false;
            this.xrTableCell237.StylePriority.UseTextAlignment = false;
            this.xrTableCell237.Text = "    7  其他";
            this.xrTableCell237.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell237.Weight = 0.24509709336190172D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell241.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_危险控制因素其他});
            this.xrTableCell241.Dpi = 254F;
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.StylePriority.UseBorders = false;
            this.xrTableCell241.Weight = 1.1539885393578082D;
            // 
            // xrLabel_危险控制因素其他
            // 
            this.xrLabel_危险控制因素其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_危险控制因素其他.Dpi = 254F;
            this.xrLabel_危险控制因素其他.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_危险控制因素其他.LocationFloat = new DevExpress.Utils.PointFloat(0.0003875732F, 0F);
            this.xrLabel_危险控制因素其他.Name = "xrLabel_危险控制因素其他";
            this.xrLabel_危险控制因素其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_危险控制因素其他.SizeF = new System.Drawing.SizeF(716.7386F, 49.95313F);
            this.xrLabel_危险控制因素其他.StylePriority.UseBorders = false;
            this.xrLabel_危险控制因素其他.StylePriority.UseFont = false;
            this.xrLabel_危险控制因素其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_危险控制因素其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell242.Dpi = 254F;
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.StylePriority.UseBorders = false;
            this.xrTableCell242.Weight = 0.2718740427070635D;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell245,
            this.xrTableCell246,
            this.xrTableCell265,
            this.xrTableCell266,
            this.xrTableCell267});
            this.xrTableRow71.Dpi = 254F;
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Weight = 1D;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell245.Dpi = 254F;
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.StylePriority.UseBorders = false;
            this.xrTableCell245.Weight = 0.15869037293521471D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell246.Dpi = 254F;
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.StylePriority.UseBorders = false;
            this.xrTableCell246.Weight = 1.1068780945547609D;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell265.Dpi = 254F;
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseBorders = false;
            this.xrTableCell265.Weight = 0.24509709336190172D;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell266.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_危险控制因素其他补充});
            this.xrTableCell266.Dpi = 254F;
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.StylePriority.UseBorders = false;
            this.xrTableCell266.Weight = 1.1539885393578082D;
            // 
            // xrLabel_危险控制因素其他补充
            // 
            this.xrLabel_危险控制因素其他补充.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_危险控制因素其他补充.Dpi = 254F;
            this.xrLabel_危险控制因素其他补充.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_危险控制因素其他补充.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_危险控制因素其他补充.Name = "xrLabel_危险控制因素其他补充";
            this.xrLabel_危险控制因素其他补充.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_危险控制因素其他补充.SizeF = new System.Drawing.SizeF(716.7386F, 49.95313F);
            this.xrLabel_危险控制因素其他补充.StylePriority.UseBorders = false;
            this.xrLabel_危险控制因素其他补充.StylePriority.UseFont = false;
            this.xrLabel_危险控制因素其他补充.StylePriority.UseTextAlignment = false;
            this.xrLabel_危险控制因素其他补充.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell267.Dpi = 254F;
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseBorders = false;
            this.xrTableCell267.Weight = 0.2718740427070635D;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell370,
            this.xrTableCell373});
            this.xrTableRow62.Dpi = 254F;
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 1D;
            // 
            // xrTableCell370
            // 
            this.xrTableCell370.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell370.Dpi = 254F;
            this.xrTableCell370.Name = "xrTableCell370";
            this.xrTableCell370.StylePriority.UseBorders = false;
            this.xrTableCell370.Weight = 0.15869037293521471D;
            // 
            // xrTableCell373
            // 
            this.xrTableCell373.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell373.Dpi = 254F;
            this.xrTableCell373.Name = "xrTableCell373";
            this.xrTableCell373.StylePriority.UseBorders = false;
            this.xrTableCell373.Weight = 2.7778377699815344D;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell395,
            this.xrTableCell398});
            this.xrTableRow67.Dpi = 254F;
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 1D;
            // 
            // xrTableCell395
            // 
            this.xrTableCell395.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell395.Dpi = 254F;
            this.xrTableCell395.Name = "xrTableCell395";
            this.xrTableCell395.StylePriority.UseBorders = false;
            this.xrTableCell395.Weight = 0.15869037293521471D;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell398.Dpi = 254F;
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.StylePriority.UseBorders = false;
            this.xrTableCell398.StylePriority.UseTextAlignment = false;
            this.xrTableCell398.Text = "      以上体检结果、健康评价已面对面反馈本人（家属），并已进行健康指导。";
            this.xrTableCell398.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell398.Weight = 2.7778377699815344D;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell400,
            this.xrTableCell404});
            this.xrTableRow68.Dpi = 254F;
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 1D;
            // 
            // xrTableCell400
            // 
            this.xrTableCell400.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell400.Dpi = 254F;
            this.xrTableCell400.Name = "xrTableCell400";
            this.xrTableCell400.StylePriority.UseBorders = false;
            this.xrTableCell400.Text = "结";
            this.xrTableCell400.Weight = 0.15869037293521471D;
            // 
            // xrTableCell404
            // 
            this.xrTableCell404.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell404.Dpi = 254F;
            this.xrTableCell404.Name = "xrTableCell404";
            this.xrTableCell404.StylePriority.UseBorders = false;
            this.xrTableCell404.Weight = 2.7778377699815344D;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell405,
            this.xrTableCell371,
            this.xrTableCell372,
            this.xrTableCell406,
            this.xrTableCell407,
            this.xrTableCell408,
            this.xrTableCell409});
            this.xrTableRow69.Dpi = 254F;
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 1D;
            // 
            // xrTableCell405
            // 
            this.xrTableCell405.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell405.Dpi = 254F;
            this.xrTableCell405.Name = "xrTableCell405";
            this.xrTableCell405.StylePriority.UseBorders = false;
            this.xrTableCell405.Text = "果";
            this.xrTableCell405.Weight = 0.15869037293521471D;
            // 
            // xrTableCell371
            // 
            this.xrTableCell371.Dpi = 254F;
            this.xrTableCell371.Name = "xrTableCell371";
            this.xrTableCell371.Weight = 1.1457729362402309D;
            // 
            // xrTableCell372
            // 
            this.xrTableCell372.Dpi = 254F;
            this.xrTableCell372.Name = "xrTableCell372";
            this.xrTableCell372.Text = "签字：";
            this.xrTableCell372.Weight = 0.24509676100973532D;
            // 
            // xrTableCell406
            // 
            this.xrTableCell406.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_本人签字});
            this.xrTableCell406.Dpi = 254F;
            this.xrTableCell406.Name = "xrTableCell406";
            this.xrTableCell406.Weight = 0.42204755902527008D;
            // 
            // xrLabel_本人签字
            // 
            this.xrLabel_本人签字.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_本人签字.Dpi = 254F;
            this.xrLabel_本人签字.LocationFloat = new DevExpress.Utils.PointFloat(0.0002583821F, 0F);
            this.xrLabel_本人签字.Name = "xrLabel_本人签字";
            this.xrLabel_本人签字.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_本人签字.SizeF = new System.Drawing.SizeF(270F, 49.95313F);
            this.xrLabel_本人签字.StylePriority.UseBorders = false;
            // 
            // xrTableCell407
            // 
            this.xrTableCell407.Dpi = 254F;
            this.xrTableCell407.Name = "xrTableCell407";
            this.xrTableCell407.Text = "（本人）/";
            this.xrTableCell407.Weight = 0.28843980409074227D;
            // 
            // xrTableCell408
            // 
            this.xrTableCell408.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_家属签字});
            this.xrTableCell408.Dpi = 254F;
            this.xrTableCell408.Name = "xrTableCell408";
            this.xrTableCell408.Weight = 0.40460666690849245D;
            // 
            // xrLabel_家属签字
            // 
            this.xrLabel_家属签字.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_家属签字.Dpi = 254F;
            this.xrLabel_家属签字.LocationFloat = new DevExpress.Utils.PointFloat(0.0001525879F, 0F);
            this.xrLabel_家属签字.Name = "xrLabel_家属签字";
            this.xrLabel_家属签字.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_家属签字.SizeF = new System.Drawing.SizeF(260.0653F, 49.95313F);
            this.xrLabel_家属签字.StylePriority.UseBorders = false;
            // 
            // xrTableCell409
            // 
            this.xrTableCell409.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell409.Dpi = 254F;
            this.xrTableCell409.Name = "xrTableCell409";
            this.xrTableCell409.StylePriority.UseBorders = false;
            this.xrTableCell409.Text = "（家属）";
            this.xrTableCell409.Weight = 0.2718740427070635D;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell390,
            this.xrTableCell392});
            this.xrTableRow66.Dpi = 254F;
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 1D;
            // 
            // xrTableCell390
            // 
            this.xrTableCell390.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell390.Dpi = 254F;
            this.xrTableCell390.Name = "xrTableCell390";
            this.xrTableCell390.StylePriority.UseBorders = false;
            this.xrTableCell390.Text = "反";
            this.xrTableCell390.Weight = 0.15869037293521471D;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell392.Dpi = 254F;
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.StylePriority.UseBorders = false;
            this.xrTableCell392.Weight = 2.7778377699815344D;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell385,
            this.xrTableCell386,
            this.xrTableCell387,
            this.xrTableCell389});
            this.xrTableRow65.Dpi = 254F;
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 1D;
            // 
            // xrTableCell385
            // 
            this.xrTableCell385.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell385.Dpi = 254F;
            this.xrTableCell385.Name = "xrTableCell385";
            this.xrTableCell385.StylePriority.UseBorders = false;
            this.xrTableCell385.Text = "馈";
            this.xrTableCell385.Weight = 0.15869037293521471D;
            // 
            // xrTableCell386
            // 
            this.xrTableCell386.Dpi = 254F;
            this.xrTableCell386.Name = "xrTableCell386";
            this.xrTableCell386.Weight = 1.3908699821232515D;
            // 
            // xrTableCell387
            // 
            this.xrTableCell387.Dpi = 254F;
            this.xrTableCell387.Name = "xrTableCell387";
            this.xrTableCell387.StylePriority.UseTextAlignment = false;
            this.xrTableCell387.Text = "反馈人签字：";
            this.xrTableCell387.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell387.Weight = 0.42204736910974633D;
            // 
            // xrTableCell389
            // 
            this.xrTableCell389.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell389.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_反馈人签字});
            this.xrTableCell389.Dpi = 254F;
            this.xrTableCell389.Name = "xrTableCell389";
            this.xrTableCell389.StylePriority.UseBorders = false;
            this.xrTableCell389.Weight = 0.96492041874853651D;
            // 
            // xrLabel_反馈人签字
            // 
            this.xrLabel_反馈人签字.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_反馈人签字.Dpi = 254F;
            this.xrLabel_反馈人签字.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 0F);
            this.xrLabel_反馈人签字.Name = "xrLabel_反馈人签字";
            this.xrLabel_反馈人签字.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_反馈人签字.SizeF = new System.Drawing.SizeF(246.8021F, 49.95313F);
            this.xrLabel_反馈人签字.StylePriority.UseBorders = false;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell380,
            this.xrTableCell382});
            this.xrTableRow64.Dpi = 254F;
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 1D;
            // 
            // xrTableCell380
            // 
            this.xrTableCell380.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell380.Dpi = 254F;
            this.xrTableCell380.Name = "xrTableCell380";
            this.xrTableCell380.StylePriority.UseBorders = false;
            this.xrTableCell380.Weight = 0.15869037293521471D;
            // 
            // xrTableCell382
            // 
            this.xrTableCell382.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell382.Dpi = 254F;
            this.xrTableCell382.Name = "xrTableCell382";
            this.xrTableCell382.StylePriority.UseBorders = false;
            this.xrTableCell382.Weight = 2.7778377699815344D;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell375,
            this.xrTableCell383,
            this.xrTableCell381,
            this.xrTableCell374,
            this.xrTableCell376,
            this.xrTableCell377,
            this.xrTableCell378,
            this.xrTableCell379});
            this.xrTableRow63.Dpi = 254F;
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 1D;
            // 
            // xrTableCell375
            // 
            this.xrTableCell375.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell375.Dpi = 254F;
            this.xrTableCell375.Name = "xrTableCell375";
            this.xrTableCell375.StylePriority.UseBorders = false;
            this.xrTableCell375.Weight = 0.15869037293521471D;
            // 
            // xrTableCell383
            // 
            this.xrTableCell383.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell383.Dpi = 254F;
            this.xrTableCell383.Name = "xrTableCell383";
            this.xrTableCell383.StylePriority.UseBorders = false;
            this.xrTableCell383.StylePriority.UseTextAlignment = false;
            this.xrTableCell383.Text = "反馈时间：";
            this.xrTableCell383.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell383.Weight = 1.8413303397265863D;
            // 
            // xrTableCell381
            // 
            this.xrTableCell381.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell381.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_反馈时间年});
            this.xrTableCell381.Dpi = 254F;
            this.xrTableCell381.Name = "xrTableCell381";
            this.xrTableCell381.StylePriority.UseBorders = false;
            this.xrTableCell381.Weight = 0.2343048707254527D;
            // 
            // xrLabel_反馈时间年
            // 
            this.xrLabel_反馈时间年.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_反馈时间年.Dpi = 254F;
            this.xrLabel_反馈时间年.LocationFloat = new DevExpress.Utils.PointFloat(0.0001525879F, 0F);
            this.xrLabel_反馈时间年.Name = "xrLabel_反馈时间年";
            this.xrLabel_反馈时间年.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_反馈时间年.SizeF = new System.Drawing.SizeF(149.4695F, 49.95313F);
            this.xrLabel_反馈时间年.StylePriority.UseBorders = false;
            // 
            // xrTableCell374
            // 
            this.xrTableCell374.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell374.Dpi = 254F;
            this.xrTableCell374.Name = "xrTableCell374";
            this.xrTableCell374.StylePriority.UseBorders = false;
            this.xrTableCell374.Text = "年";
            this.xrTableCell374.Weight = 0.0980013717598276D;
            // 
            // xrTableCell376
            // 
            this.xrTableCell376.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell376.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_反馈时间月});
            this.xrTableCell376.Dpi = 254F;
            this.xrTableCell376.Name = "xrTableCell376";
            this.xrTableCell376.StylePriority.UseBorders = false;
            this.xrTableCell376.Weight = 0.19778586037144808D;
            // 
            // xrLabel_反馈时间月
            // 
            this.xrLabel_反馈时间月.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_反馈时间月.Dpi = 254F;
            this.xrLabel_反馈时间月.LocationFloat = new DevExpress.Utils.PointFloat(0.0001525879F, 0F);
            this.xrLabel_反馈时间月.Name = "xrLabel_反馈时间月";
            this.xrLabel_反馈时间月.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_反馈时间月.SizeF = new System.Drawing.SizeF(127.1289F, 49.95313F);
            this.xrLabel_反馈时间月.StylePriority.UseBorders = false;
            // 
            // xrTableCell377
            // 
            this.xrTableCell377.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell377.Dpi = 254F;
            this.xrTableCell377.Name = "xrTableCell377";
            this.xrTableCell377.StylePriority.UseBorders = false;
            this.xrTableCell377.Text = "月";
            this.xrTableCell377.Weight = 0.095646965273376683D;
            // 
            // xrTableCell378
            // 
            this.xrTableCell378.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell378.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_反馈时间日});
            this.xrTableCell378.Dpi = 254F;
            this.xrTableCell378.Name = "xrTableCell378";
            this.xrTableCell378.StylePriority.UseBorders = false;
            this.xrTableCell378.Weight = 0.18049704311025416D;
            // 
            // xrLabel_反馈时间日
            // 
            this.xrLabel_反馈时间日.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_反馈时间日.Dpi = 254F;
            this.xrLabel_反馈时间日.LocationFloat = new DevExpress.Utils.PointFloat(3.051758E-05F, 0F);
            this.xrLabel_反馈时间日.Name = "xrLabel_反馈时间日";
            this.xrLabel_反馈时间日.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_反馈时间日.SizeF = new System.Drawing.SizeF(116.0165F, 49.95313F);
            this.xrLabel_反馈时间日.StylePriority.UseBorders = false;
            // 
            // xrTableCell379
            // 
            this.xrTableCell379.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell379.Dpi = 254F;
            this.xrTableCell379.Name = "xrTableCell379";
            this.xrTableCell379.StylePriority.UseBorders = false;
            this.xrTableCell379.StylePriority.UseTextAlignment = false;
            this.xrTableCell379.Text = " 日";
            this.xrTableCell379.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell379.Weight = 0.13027131901458916D;
            // 
            // xrLine1
            // 
            this.xrLine1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLine1.BorderWidth = 2F;
            this.xrLine1.Dpi = 254F;
            this.xrLine1.LineWidth = 5;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(3665.348F, 237.49F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(45.1001F, 58.42001F);
            this.xrLine1.StylePriority.UseBorders = false;
            this.xrLine1.StylePriority.UseBorderWidth = false;
            // 
            // xrLabel_编号6
            // 
            this.xrLabel_编号6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号6.BorderWidth = 2F;
            this.xrLabel_编号6.Dpi = 254F;
            this.xrLabel_编号6.LocationFloat = new DevExpress.Utils.PointFloat(3851.683F, 237.49F);
            this.xrLabel_编号6.Name = "xrLabel_编号6";
            this.xrLabel_编号6.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号6.SizeF = new System.Drawing.SizeF(47.52759F, 58.42F);
            this.xrLabel_编号6.StylePriority.UseBorders = false;
            this.xrLabel_编号6.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号6.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号7
            // 
            this.xrLabel_编号7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号7.BorderWidth = 2F;
            this.xrLabel_编号7.Dpi = 254F;
            this.xrLabel_编号7.LocationFloat = new DevExpress.Utils.PointFloat(3912.617F, 237.49F);
            this.xrLabel_编号7.Name = "xrLabel_编号7";
            this.xrLabel_编号7.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号7.SizeF = new System.Drawing.SizeF(47.52759F, 58.42F);
            this.xrLabel_编号7.StylePriority.UseBorders = false;
            this.xrLabel_编号7.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号7.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号8
            // 
            this.xrLabel_编号8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号8.BorderWidth = 2F;
            this.xrLabel_编号8.Dpi = 254F;
            this.xrLabel_编号8.LocationFloat = new DevExpress.Utils.PointFloat(3973.513F, 237.49F);
            this.xrLabel_编号8.Name = "xrLabel_编号8";
            this.xrLabel_编号8.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号8.SizeF = new System.Drawing.SizeF(47.52759F, 58.42F);
            this.xrLabel_编号8.StylePriority.UseBorders = false;
            this.xrLabel_编号8.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号8.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号5
            // 
            this.xrLabel_编号5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号5.BorderWidth = 2F;
            this.xrLabel_编号5.Dpi = 254F;
            this.xrLabel_编号5.LocationFloat = new DevExpress.Utils.PointFloat(3788.72F, 237.49F);
            this.xrLabel_编号5.Name = "xrLabel_编号5";
            this.xrLabel_编号5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号5.SizeF = new System.Drawing.SizeF(47.52759F, 58.42F);
            this.xrLabel_编号5.StylePriority.UseBorders = false;
            this.xrLabel_编号5.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号5.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号4
            // 
            this.xrLabel_编号4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号4.BorderWidth = 2F;
            this.xrLabel_编号4.Dpi = 254F;
            this.xrLabel_编号4.LocationFloat = new DevExpress.Utils.PointFloat(3721.736F, 237.49F);
            this.xrLabel_编号4.Name = "xrLabel_编号4";
            this.xrLabel_编号4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号4.SizeF = new System.Drawing.SizeF(47.52759F, 58.42F);
            this.xrLabel_编号4.StylePriority.UseBorders = false;
            this.xrLabel_编号4.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号4.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号3
            // 
            this.xrLabel_编号3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号3.BorderWidth = 2F;
            this.xrLabel_编号3.Dpi = 254F;
            this.xrLabel_编号3.LocationFloat = new DevExpress.Utils.PointFloat(3602.372F, 237.49F);
            this.xrLabel_编号3.Name = "xrLabel_编号3";
            this.xrLabel_编号3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号3.SizeF = new System.Drawing.SizeF(47.52759F, 58.42F);
            this.xrLabel_编号3.StylePriority.UseBorders = false;
            this.xrLabel_编号3.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号3.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号2
            // 
            this.xrLabel_编号2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号2.BorderWidth = 2F;
            this.xrLabel_编号2.Dpi = 254F;
            this.xrLabel_编号2.LocationFloat = new DevExpress.Utils.PointFloat(3540.094F, 237.49F);
            this.xrLabel_编号2.Name = "xrLabel_编号2";
            this.xrLabel_编号2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号2.SizeF = new System.Drawing.SizeF(47.52759F, 58.42F);
            this.xrLabel_编号2.StylePriority.UseBorders = false;
            this.xrLabel_编号2.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号2.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号1
            // 
            this.xrLabel_编号1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号1.BorderWidth = 2F;
            this.xrLabel_编号1.Dpi = 254F;
            this.xrLabel_编号1.LocationFloat = new DevExpress.Utils.PointFloat(3472.824F, 237.49F);
            this.xrLabel_编号1.Name = "xrLabel_编号1";
            this.xrLabel_编号1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_编号1.SizeF = new System.Drawing.SizeF(47.52759F, 58.42F);
            this.xrLabel_编号1.StylePriority.UseBorders = false;
            this.xrLabel_编号1.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号1.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(3358.785F, 237.49F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(103.7167F, 58.42F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "编号";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel_姓名
            // 
            this.xrLabel_姓名.Dpi = 254F;
            this.xrLabel_姓名.LocationFloat = new DevExpress.Utils.PointFloat(2395.44F, 237.49F);
            this.xrLabel_姓名.Name = "xrLabel_姓名";
            this.xrLabel_姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_姓名.SizeF = new System.Drawing.SizeF(254F, 58.42F);
            this.xrLabel_姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel_姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(2270.557F, 237.49F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(124.8833F, 58.42F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓名:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.Dpi = 254F;
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(2103.71F, 351.37F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow1,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow12,
            this.xrTableRow11,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow13,
            this.xrTableRow17,
            this.xrTableRow19,
            this.xrTableRow18,
            this.xrTableRow16,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow20,
            this.xrTableRow10,
            this.xrTableRow7,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow28,
            this.xrTableRow29,
            this.xrTableRow27,
            this.xrTableRow31,
            this.xrTableRow30,
            this.xrTableRow34,
            this.xrTableRow35,
            this.xrTableRow36,
            this.xrTableRow33,
            this.xrTableRow32,
            this.xrTableRow37});
            this.xrTable2.SizeF = new System.Drawing.SizeF(2004.484F, 2359F);
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTable_体检日期年,
            this.xrTableCell11,
            this.xrTable_体检日期月,
            this.xrTableCell23,
            this.xrTable_体检日期日,
            this.xrTableCell24,
            this.xrTableCell12,
            this.xrTableCell13,
            this.xrTable_责任医生});
            this.xrTableRow2.Dpi = 254F;
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Dpi = 254F;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.Text = "体 检 日 期";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 1.2416674996924213D;
            // 
            // xrTable_体检日期年
            // 
            this.xrTable_体检日期年.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体检日期年.Dpi = 254F;
            this.xrTable_体检日期年.Name = "xrTable_体检日期年";
            this.xrTable_体检日期年.StylePriority.UseBorders = false;
            this.xrTable_体检日期年.Weight = 0.60625019223671273D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Dpi = 254F;
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.Text = "年";
            this.xrTableCell11.Weight = 0.2953246109128938D;
            // 
            // xrTable_体检日期月
            // 
            this.xrTable_体检日期月.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体检日期月.Dpi = 254F;
            this.xrTable_体检日期月.Name = "xrTable_体检日期月";
            this.xrTable_体检日期月.StylePriority.UseBorders = false;
            this.xrTable_体检日期月.Weight = 0.53357990895669283D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell23.Dpi = 254F;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.Text = "月";
            this.xrTableCell23.Weight = 0.40455735574557089D;
            // 
            // xrTable_体检日期日
            // 
            this.xrTable_体检日期日.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_体检日期日.Dpi = 254F;
            this.xrTable_体检日期日.Name = "xrTable_体检日期日";
            this.xrTable_体检日期日.StylePriority.UseBorders = false;
            this.xrTable_体检日期日.Weight = 0.57955716350885822D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.Dpi = 254F;
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.Text = "日";
            this.xrTableCell24.Weight = 0.28411528820127963D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell12.Dpi = 254F;
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseBorders = false;
            this.xrTableCell12.Weight = 0.52994879775159942D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Dpi = 254F;
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Text = "责任医生";
            this.xrTableCell13.Weight = 1.0933645653912403D;
            // 
            // xrTable_责任医生
            // 
            this.xrTable_责任医生.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_责任医生.Dpi = 254F;
            this.xrTable_责任医生.Name = "xrTable_责任医生";
            this.xrTable_责任医生.StylePriority.UseBorders = false;
            this.xrTable_责任医生.Weight = 2.3233025017685778D;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell3,
            this.xrTableCell7});
            this.xrTableRow1.Dpi = 254F;
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Dpi = 254F;
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "内容";
            this.xrTableCell1.Weight = 0.40157480314960631D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Dpi = 254F;
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "检    查    项    目";
            this.xrTableCell3.Weight = 6.8021156130813223D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Dpi = 254F;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Text = "医师签名";
            this.xrTableCell7.Weight = 0.68797746793491688D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell20,
            this.xrTableCell21});
            this.xrTableRow3.Dpi = 254F;
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell15.Dpi = 254F;
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Weight = 0.40157480314960631D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell20.Dpi = 254F;
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "   1   无症状   2   头痛   3   头晕   4   心悸   5   胸闷   6   胸痛   7   慢性咳嗽   8   咳痰   9  " +
    " 呼吸困难   10    多饮";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableCell20.Weight = 6.8021160936731055D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell21.Dpi = 254F;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.Weight = 0.68797698734313473D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell26,
            this.xrTable_症状医师签名});
            this.xrTableRow4.Dpi = 254F;
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell2.Dpi = 254F;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Text = "症";
            this.xrTableCell2.Weight = 0.40157480314960631D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell26.Dpi = 254F;
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "   11   多尿   12   体重下降   13   乏力   14   关节肿痛   15   视力模糊   16   手脚麻木   17   尿急   " +
    "18   尿痛";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell26.Weight = 6.8021160936731055D;
            // 
            // xrTable_症状医师签名
            // 
            this.xrTable_症状医师签名.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_症状医师签名.Dpi = 254F;
            this.xrTable_症状医师签名.Name = "xrTable_症状医师签名";
            this.xrTable_症状医师签名.StylePriority.UseBorders = false;
            this.xrTable_症状医师签名.Weight = 0.68797698734313473D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell36,
            this.xrTableCell33,
            this.xrTableCell34});
            this.xrTableRow5.Dpi = 254F;
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell28.Dpi = 254F;
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.Text = "状";
            this.xrTableCell28.Weight = 0.40157480314960631D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Dpi = 254F;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "   19   便秘   20   腹泻   21   恶心呕吐   22   眼花   23   耳鸣   24   乳房胀痛   25   其他";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell36.Weight = 5.1342251995417074D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_症状其他});
            this.xrTableCell33.Dpi = 254F;
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell33.Weight = 1.6678908941313977D;
            // 
            // xrLabel_症状其他
            // 
            this.xrLabel_症状其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_症状其他.Dpi = 254F;
            this.xrLabel_症状其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_症状其他.Name = "xrLabel_症状其他";
            this.xrLabel_症状其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状其他.SizeF = new System.Drawing.SizeF(413.0605F, 49.95328F);
            this.xrLabel_症状其他.StylePriority.UseBorders = false;
            this.xrLabel_症状其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_症状其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell34.Dpi = 254F;
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Weight = 0.68797698734313473D;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow6.Dpi = 254F;
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell35.Dpi = 254F;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseBorders = false;
            this.xrTableCell35.Weight = 0.40157480314960631D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_症状1,
            this.xrLabel_症状2,
            this.xrLabel_症状3,
            this.xrLabel_症状4,
            this.xrLabel_症状5,
            this.xrLabel_症状6,
            this.xrLabel_症状7,
            this.xrLabel_症状8,
            this.xrLabel_症状9,
            this.xrLabel29,
            this.xrLabel16,
            this.xrLabel_症状10,
            this.xrLabel18,
            this.xrLabel23,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel31,
            this.xrLabel30,
            this.xrLabel20});
            this.xrTableCell40.Dpi = 254F;
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.Weight = 6.8021160936731055D;
            // 
            // xrLabel_症状1
            // 
            this.xrLabel_症状1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状1.Dpi = 254F;
            this.xrLabel_症状1.LocationFloat = new DevExpress.Utils.PointFloat(735.3862F, 0F);
            this.xrLabel_症状1.Name = "xrLabel_症状1";
            this.xrLabel_症状1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状1.SizeF = new System.Drawing.SizeF(58.396F, 47.83685F);
            this.xrLabel_症状1.StylePriority.UseBorders = false;
            // 
            // xrLabel_症状2
            // 
            this.xrLabel_症状2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状2.Dpi = 254F;
            this.xrLabel_症状2.LocationFloat = new DevExpress.Utils.PointFloat(835.7153F, 0F);
            this.xrLabel_症状2.Name = "xrLabel_症状2";
            this.xrLabel_症状2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状2.SizeF = new System.Drawing.SizeF(58.396F, 47.83685F);
            this.xrLabel_症状2.StylePriority.UseBorders = false;
            // 
            // xrLabel_症状3
            // 
            this.xrLabel_症状3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状3.Dpi = 254F;
            this.xrLabel_症状3.LocationFloat = new DevExpress.Utils.PointFloat(936.0445F, 0F);
            this.xrLabel_症状3.Name = "xrLabel_症状3";
            this.xrLabel_症状3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状3.SizeF = new System.Drawing.SizeF(58.396F, 47.83685F);
            this.xrLabel_症状3.StylePriority.UseBorders = false;
            // 
            // xrLabel_症状4
            // 
            this.xrLabel_症状4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状4.Dpi = 254F;
            this.xrLabel_症状4.LocationFloat = new DevExpress.Utils.PointFloat(1036.373F, 0F);
            this.xrLabel_症状4.Name = "xrLabel_症状4";
            this.xrLabel_症状4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状4.SizeF = new System.Drawing.SizeF(58.396F, 47.83685F);
            this.xrLabel_症状4.StylePriority.UseBorders = false;
            // 
            // xrLabel_症状5
            // 
            this.xrLabel_症状5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状5.Dpi = 254F;
            this.xrLabel_症状5.LocationFloat = new DevExpress.Utils.PointFloat(1136.703F, 0F);
            this.xrLabel_症状5.Name = "xrLabel_症状5";
            this.xrLabel_症状5.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状5.SizeF = new System.Drawing.SizeF(58.396F, 47.83685F);
            this.xrLabel_症状5.StylePriority.UseBorders = false;
            // 
            // xrLabel_症状6
            // 
            this.xrLabel_症状6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状6.Dpi = 254F;
            this.xrLabel_症状6.LocationFloat = new DevExpress.Utils.PointFloat(1237.032F, 0F);
            this.xrLabel_症状6.Name = "xrLabel_症状6";
            this.xrLabel_症状6.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状6.SizeF = new System.Drawing.SizeF(58.396F, 47.83685F);
            this.xrLabel_症状6.StylePriority.UseBorders = false;
            // 
            // xrLabel_症状7
            // 
            this.xrLabel_症状7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状7.Dpi = 254F;
            this.xrLabel_症状7.LocationFloat = new DevExpress.Utils.PointFloat(1337.361F, 0F);
            this.xrLabel_症状7.Name = "xrLabel_症状7";
            this.xrLabel_症状7.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状7.SizeF = new System.Drawing.SizeF(58.396F, 47.83685F);
            this.xrLabel_症状7.StylePriority.UseBorders = false;
            // 
            // xrLabel_症状8
            // 
            this.xrLabel_症状8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状8.Dpi = 254F;
            this.xrLabel_症状8.LocationFloat = new DevExpress.Utils.PointFloat(1437.69F, 0F);
            this.xrLabel_症状8.Name = "xrLabel_症状8";
            this.xrLabel_症状8.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状8.SizeF = new System.Drawing.SizeF(58.396F, 47.83685F);
            this.xrLabel_症状8.StylePriority.UseBorders = false;
            // 
            // xrLabel_症状9
            // 
            this.xrLabel_症状9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状9.Dpi = 254F;
            this.xrLabel_症状9.LocationFloat = new DevExpress.Utils.PointFloat(1538.019F, 0F);
            this.xrLabel_症状9.Name = "xrLabel_症状9";
            this.xrLabel_症状9.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状9.SizeF = new System.Drawing.SizeF(58.396F, 47.83685F);
            this.xrLabel_症状9.StylePriority.UseBorders = false;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel29.Dpi = 254F;
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(994.4399F, 0F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "/";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.Dpi = 254F;
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(1596.415F, 0F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "/";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_症状10
            // 
            this.xrLabel_症状10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_症状10.Dpi = 254F;
            this.xrLabel_症状10.LocationFloat = new DevExpress.Utils.PointFloat(1638.348F, 0F);
            this.xrLabel_症状10.Name = "xrLabel_症状10";
            this.xrLabel_症状10.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_症状10.SizeF = new System.Drawing.SizeF(58.396F, 47.83685F);
            this.xrLabel_症状10.StylePriority.UseBorders = false;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.Dpi = 254F;
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(1496.086F, 0F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "/";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.Dpi = 254F;
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(1395.757F, 0F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "/";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.Dpi = 254F;
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(1295.428F, 0F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "/";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.Dpi = 254F;
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(894.1114F, 0F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "/";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel31.Dpi = 254F;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(1195.099F, 0F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel31.StylePriority.UseBorders = false;
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "/";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.Dpi = 254F;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(1094.77F, 0F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "/";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.Dpi = 254F;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(793.7822F, 7.177283E-05F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "/";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell41.Dpi = 254F;
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseBorders = false;
            this.xrTableCell41.Weight = 0.68797698734313473D;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell67,
            this.xrTable_体温,
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTable_脉率,
            this.xrTableCell70,
            this.xrTableCell71});
            this.xrTableRow12.Dpi = 254F;
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell65.Dpi = 254F;
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.Weight = 0.40157480314960631D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Dpi = 254F;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.Text = "体    温";
            this.xrTableCell67.Weight = 1.1150911586491143D;
            // 
            // xrTable_体温
            // 
            this.xrTable_体温.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体温.Dpi = 254F;
            this.xrTable_体温.Name = "xrTable_体温";
            this.xrTable_体温.StylePriority.UseBorders = false;
            this.xrTable_体温.Weight = 1.5647136057455708D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell68.Dpi = 254F;
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.Text = "℃";
            this.xrTableCell68.Weight = 0.86367341289370092D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell69.Dpi = 254F;
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.Text = "脉率";
            this.xrTableCell69.Weight = 1.1240801473302167D;
            // 
            // xrTable_脉率
            // 
            this.xrTable_脉率.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_脉率.Dpi = 254F;
            this.xrTable_脉率.Name = "xrTable_脉率";
            this.xrTable_脉率.StylePriority.UseBorders = false;
            this.xrTable_脉率.Weight = 0.9768614280880904D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.Dpi = 254F;
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.StylePriority.UseTextAlignment = false;
            this.xrTableCell70.Text = "次/分钟";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell70.Weight = 1.1576963409664125D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell71.Dpi = 254F;
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.Weight = 0.68797698734313473D;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.xrTableCell59,
            this.xrTable_呼吸频率,
            this.xrTableCell61,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell64});
            this.xrTableRow11.Dpi = 254F;
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 2.0000000000000004D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell58.Dpi = 254F;
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.Weight = 0.40157480314960631D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell59.Dpi = 254F;
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.Text = "呼吸频率";
            this.xrTableCell59.Weight = 1.1150911586491141D;
            // 
            // xrTable_呼吸频率
            // 
            this.xrTable_呼吸频率.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_呼吸频率.Dpi = 254F;
            this.xrTable_呼吸频率.Name = "xrTable_呼吸频率";
            this.xrTable_呼吸频率.StylePriority.UseBorders = false;
            this.xrTable_呼吸频率.Weight = 1.5647136057455713D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell61.Dpi = 254F;
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.Text = "次/分钟";
            this.xrTableCell61.Weight = 0.86367437407726344D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell62.Dpi = 254F;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.Text = "血压";
            this.xrTableCell62.Weight = 1.1240791861466535D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell63.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.xrTableCell63.Dpi = 254F;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.Weight = 2.1456692913385824D;
            // 
            // xrTable1
            // 
            this.xrTable1.Dpi = 254F;
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.051758E-05F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8,
            this.xrTableRow9});
            this.xrTable1.SizeF = new System.Drawing.SizeF(542.1775F, 127F);
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTable_左侧血压1,
            this.xrTableCell30,
            this.xrTable_左侧血压2,
            this.xrTableCell25});
            this.xrTableRow8.Dpi = 254F;
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Dpi = 254F;
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.Text = "左侧";
            this.xrTableCell29.Weight = 0.66799939133993336D;
            // 
            // xrTable_左侧血压1
            // 
            this.xrTable_左侧血压1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_左侧血压1.Dpi = 254F;
            this.xrTable_左侧血压1.Name = "xrTable_左侧血压1";
            this.xrTable_左侧血压1.StylePriority.UseBorders = false;
            this.xrTable_左侧血压1.Weight = 0.70492435088360039D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell30.Dpi = 254F;
            this.xrTableCell30.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.StylePriority.UseFont = false;
            this.xrTableCell30.Text = "/";
            this.xrTableCell30.Weight = 0.31420795611272367D;
            // 
            // xrTable_左侧血压2
            // 
            this.xrTable_左侧血压2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_左侧血压2.Dpi = 254F;
            this.xrTable_左侧血压2.Name = "xrTable_左侧血压2";
            this.xrTable_左侧血压2.StylePriority.UseBorders = false;
            this.xrTable_左侧血压2.Weight = 0.67388944093547465D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Dpi = 254F;
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Text = "mmHg";
            this.xrTableCell25.Weight = 0.63897886072826815D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.xrTable_右侧血压1,
            this.xrTableCell42,
            this.xrTable_右侧血压2,
            this.xrTableCell44});
            this.xrTableRow9.Dpi = 254F;
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Dpi = 254F;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.Text = "右侧";
            this.xrTableCell31.Weight = 0.66799995787240785D;
            // 
            // xrTable_右侧血压1
            // 
            this.xrTable_右侧血压1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_右侧血压1.Dpi = 254F;
            this.xrTable_右侧血压1.Name = "xrTable_右侧血压1";
            this.xrTable_右侧血压1.StylePriority.UseBorders = false;
            this.xrTable_右侧血压1.Weight = 0.704924336466286D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell42.Dpi = 254F;
            this.xrTableCell42.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.StylePriority.UseFont = false;
            this.xrTableCell42.Text = "/";
            this.xrTableCell42.Weight = 0.31420797689124386D;
            // 
            // xrTable_右侧血压2
            // 
            this.xrTable_右侧血压2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_右侧血压2.Dpi = 254F;
            this.xrTable_右侧血压2.Name = "xrTable_右侧血压2";
            this.xrTable_右侧血压2.StylePriority.UseBorders = false;
            this.xrTable_右侧血压2.Weight = 0.67388886804179415D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell44.Dpi = 254F;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.Text = "mmHg";
            this.xrTableCell44.Weight = 0.63897886072826815D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell64.Dpi = 254F;
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.Weight = 0.67686546505905509D;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTable_身高,
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTable_体重,
            this.xrTableCell77,
            this.xrTableCell78});
            this.xrTableRow14.Dpi = 254F;
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1.0459300452056317D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell72.Dpi = 254F;
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.Weight = 0.40157480314960631D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell73.Dpi = 254F;
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.Text = "身      高";
            this.xrTableCell73.Weight = 1.1150911586491143D;
            // 
            // xrTable_身高
            // 
            this.xrTable_身高.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_身高.Dpi = 254F;
            this.xrTable_身高.Name = "xrTable_身高";
            this.xrTable_身高.StylePriority.UseBorders = false;
            this.xrTable_身高.Weight = 1.5647126445620079D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell75.Dpi = 254F;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.Text = "cm";
            this.xrTableCell75.Weight = 0.863674374077264D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell76.Dpi = 254F;
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseBorders = false;
            this.xrTableCell76.Text = "体      重";
            this.xrTableCell76.Weight = 1.1240782249630905D;
            // 
            // xrTable_体重
            // 
            this.xrTable_体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体重.Dpi = 254F;
            this.xrTable_体重.Name = "xrTable_体重";
            this.xrTable_体重.StylePriority.UseBorders = false;
            this.xrTable_体重.Weight = 1.3941728486789493D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell77.Dpi = 254F;
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.Text = "kg";
            this.xrTableCell77.Weight = 0.74038684274267963D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell78.Dpi = 254F;
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseBorders = false;
            this.xrTableCell78.Weight = 0.68797698734313473D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTableCell80,
            this.xrTable_腰围,
            this.xrTableCell82,
            this.xrTableCell83,
            this.xrTable_体重指数,
            this.xrTableCell84,
            this.xrTableCell85});
            this.xrTableRow15.Dpi = 254F;
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1.1666674188076553D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell79.Dpi = 254F;
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseBorders = false;
            this.xrTableCell79.StylePriority.UseTextAlignment = false;
            this.xrTableCell79.Text = "一";
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell79.Weight = 0.40157480314960631D;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell80.Dpi = 254F;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.Text = "腰      围";
            this.xrTableCell80.Weight = 1.1150911586491143D;
            // 
            // xrTable_腰围
            // 
            this.xrTable_腰围.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_腰围.Dpi = 254F;
            this.xrTable_腰围.Name = "xrTable_腰围";
            this.xrTable_腰围.StylePriority.UseBorders = false;
            this.xrTable_腰围.Weight = 1.5647116833784449D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell82.Dpi = 254F;
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseBorders = false;
            this.xrTableCell82.Text = "cm";
            this.xrTableCell82.Weight = 0.86367533526082718D;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell83.Dpi = 254F;
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.Text = "体重指数(BMI)";
            this.xrTableCell83.Weight = 1.1240802674781618D;
            // 
            // xrTable_体重指数
            // 
            this.xrTable_体重指数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_体重指数.Dpi = 254F;
            this.xrTable_体重指数.Name = "xrTable_体重指数";
            this.xrTable_体重指数.StylePriority.UseBorders = false;
            this.xrTable_体重指数.Weight = 1.394170806163878D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell84.Dpi = 254F;
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseBorders = false;
            this.xrTableCell84.Text = "kg/㎡";
            this.xrTableCell84.Weight = 0.74038684274267963D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell85.Dpi = 254F;
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.Weight = 0.68797698734313473D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell52,
            this.xrTableCell53,
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTable_一般状况医师签字});
            this.xrTableRow13.Dpi = 254F;
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1.9370078773402049D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell52.Dpi = 254F;
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell52.StylePriority.UseBorders = false;
            this.xrTableCell52.Text = "般";
            this.xrTableCell52.Weight = 0.40157480314960631D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell53.Dpi = 254F;
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseBorders = false;
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.Text = "老年人健康状态自我评估*";
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell53.Weight = 1.1150911586491143D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell56.Dpi = 254F;
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseBorders = false;
            this.xrTableCell56.StylePriority.UseTextAlignment = false;
            this.xrTableCell56.Text = "  1  满意   2  基本满意   3  说不清楚   4  不太满意    5  不满意";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell56.Weight = 4.9466379721333666D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell57.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_老年人健康状态自我评估});
            this.xrTableCell57.Dpi = 254F;
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.Weight = 0.740386962890625D;
            // 
            // xrLabel_老年人健康状态自我评估
            // 
            this.xrLabel_老年人健康状态自我评估.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_老年人健康状态自我评估.Dpi = 254F;
            this.xrLabel_老年人健康状态自我评估.LocationFloat = new DevExpress.Utils.PointFloat(115.1307F, 38.83329F);
            this.xrLabel_老年人健康状态自我评估.Name = "xrLabel_老年人健康状态自我评估";
            this.xrLabel_老年人健康状态自我评估.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_老年人健康状态自我评估.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_老年人健康状态自我评估.StylePriority.UseBorders = false;
            // 
            // xrTable_一般状况医师签字
            // 
            this.xrTable_一般状况医师签字.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_一般状况医师签字.Dpi = 254F;
            this.xrTable_一般状况医师签字.Name = "xrTable_一般状况医师签字";
            this.xrTable_一般状况医师签字.StylePriority.UseBorders = false;
            this.xrTable_一般状况医师签字.Weight = 0.68797698734313473D;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell49,
            this.xrTableCell86,
            this.xrTableCell87,
            this.xrTableCell88});
            this.xrTableRow17.Dpi = 254F;
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell48.Dpi = 254F;
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.StylePriority.UseTextAlignment = false;
            this.xrTableCell48.Text = "状";
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell48.Weight = 0.40157480314960631D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell49.Dpi = 254F;
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.Text = "老年人生活自理";
            this.xrTableCell49.Weight = 1.1150911586491143D;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Dpi = 254F;
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseTextAlignment = false;
            this.xrTableCell86.Text = "  1  可自理（0~3分）\t2   轻度依赖（4~8分）";
            this.xrTableCell86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell86.Weight = 4.946637731837475D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell87.Dpi = 254F;
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBorders = false;
            this.xrTableCell87.Weight = 0.74038720318651574D;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell88.Dpi = 254F;
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.Weight = 0.68797698734313473D;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell96,
            this.xrTableCell97,
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102});
            this.xrTableRow19.Dpi = 254F;
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell96.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell96.Dpi = 254F;
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell96.StylePriority.UseBorders = false;
            this.xrTableCell96.Text = "况";
            this.xrTableCell96.Weight = 0.40157480314960631D;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell97.Dpi = 254F;
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.Text = "能力自我评估*";
            this.xrTableCell97.Weight = 1.1150911586491143D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell100.Dpi = 254F;
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.StylePriority.UseTextAlignment = false;
            this.xrTableCell100.Text = "  3  中度依赖(9~18分)\t4  不能自理(≥19分)";
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell100.Weight = 4.946637731837475D;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell101.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_老年人生活自理能力自我评估});
            this.xrTableCell101.Dpi = 254F;
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.Weight = 0.74038720318651574D;
            // 
            // xrLabel_老年人生活自理能力自我评估
            // 
            this.xrLabel_老年人生活自理能力自我评估.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_老年人生活自理能力自我评估.Dpi = 254F;
            this.xrLabel_老年人生活自理能力自我评估.LocationFloat = new DevExpress.Utils.PointFloat(115.1304F, 0F);
            this.xrLabel_老年人生活自理能力自我评估.Name = "xrLabel_老年人生活自理能力自我评估";
            this.xrLabel_老年人生活自理能力自我评估.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_老年人生活自理能力自我评估.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_老年人生活自理能力自我评估.StylePriority.UseBorders = false;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell102.Dpi = 254F;
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseBorders = false;
            this.xrTableCell102.Weight = 0.68797698734313473D;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95});
            this.xrTableRow18.Dpi = 254F;
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.BorderDashStyle = DevExpress.XtraPrinting.BorderDashStyle.Solid;
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell89.Dpi = 254F;
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorderDashStyle = false;
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.Weight = 0.40157480314960631D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell90.Dpi = 254F;
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.Text = "老年人";
            this.xrTableCell90.Weight = 1.1150911586491143D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Dpi = 254F;
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseTextAlignment = false;
            this.xrTableCell93.Text = "  1  粗筛阴性";
            this.xrTableCell93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell93.Weight = 4.9466377318374759D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell94.Dpi = 254F;
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.Weight = 0.74038720318651574D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell95.Dpi = 254F;
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.Weight = 0.68797698734313473D;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell19,
            this.xrTableCell140,
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47});
            this.xrTableRow16.Dpi = 254F;
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell18.Dpi = 254F;
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.Weight = 0.40157480314960631D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Dpi = 254F;
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.Text = "认知功能*";
            this.xrTableCell19.Weight = 1.1150911586491143D;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell140.Dpi = 254F;
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.StylePriority.UseTextAlignment = false;
            this.xrTableCell140.Text = "  2  粗筛阳性，简易智力状态检查，总分";
            this.xrTableCell140.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell140.Weight = 2.6573381799412528D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell45.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_老年人认知功能总分});
            this.xrTableCell45.Dpi = 254F;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell45.Weight = 2.2892995518962231D;
            // 
            // xrLabel_老年人认知功能总分
            // 
            this.xrLabel_老年人认知功能总分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_老年人认知功能总分.Dpi = 254F;
            this.xrLabel_老年人认知功能总分.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.051758E-05F);
            this.xrLabel_老年人认知功能总分.Name = "xrLabel_老年人认知功能总分";
            this.xrLabel_老年人认知功能总分.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_老年人认知功能总分.SizeF = new System.Drawing.SizeF(211.9771F, 49.95325F);
            this.xrLabel_老年人认知功能总分.StylePriority.UseBorders = false;
            this.xrLabel_老年人认知功能总分.StylePriority.UseTextAlignment = false;
            this.xrLabel_老年人认知功能总分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell46.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_老年人认知功能});
            this.xrTableCell46.Dpi = 254F;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Weight = 0.74038720318651574D;
            // 
            // xrLabel_老年人认知功能
            // 
            this.xrLabel_老年人认知功能.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_老年人认知功能.Dpi = 254F;
            this.xrLabel_老年人认知功能.LocationFloat = new DevExpress.Utils.PointFloat(115.1307F, 0F);
            this.xrLabel_老年人认知功能.Name = "xrLabel_老年人认知功能";
            this.xrLabel_老年人认知功能.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_老年人认知功能.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_老年人认知功能.StylePriority.UseBorders = false;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell47.Dpi = 254F;
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.Weight = 0.68797698734313473D;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell116,
            this.xrTableCell117,
            this.xrTableCell118});
            this.xrTableRow21.Dpi = 254F;
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell112.Dpi = 254F;
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.Weight = 0.40157480314960631D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell113.Dpi = 254F;
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.StylePriority.UseBorders = false;
            this.xrTableCell113.Text = "老年人";
            this.xrTableCell113.Weight = 1.1150911586491143D;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Dpi = 254F;
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseTextAlignment = false;
            this.xrTableCell116.Text = "  1  粗筛阴性";
            this.xrTableCell116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell116.Weight = 4.9466377318374759D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell117.Dpi = 254F;
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Weight = 0.74038720318651574D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell118.Dpi = 254F;
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.Weight = 0.68797698734313473D;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119,
            this.xrTableCell120,
            this.xrTableCell141,
            this.xrTableCell123,
            this.xrTableCell124,
            this.xrTableCell125});
            this.xrTableRow22.Dpi = 254F;
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell119.Dpi = 254F;
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.Weight = 0.40157480314960631D;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell120.Dpi = 254F;
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.Text = "情感状态*";
            this.xrTableCell120.Weight = 1.1150911586491143D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell141.Dpi = 254F;
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.StylePriority.UseBorders = false;
            this.xrTableCell141.StylePriority.UseTextAlignment = false;
            this.xrTableCell141.Text = "  2  粗筛阳性，老年人抑郁评分检查，总分";
            this.xrTableCell141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell141.Weight = 2.7703964353546384D;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell123.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_老年人情感状态总分});
            this.xrTableCell123.Dpi = 254F;
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.StylePriority.UseTextAlignment = false;
            this.xrTableCell123.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell123.Weight = 2.1762412964828375D;
            // 
            // xrLabel_老年人情感状态总分
            // 
            this.xrLabel_老年人情感状态总分.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_老年人情感状态总分.Dpi = 254F;
            this.xrLabel_老年人情感状态总分.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.051758E-05F);
            this.xrLabel_老年人情感状态总分.Name = "xrLabel_老年人情感状态总分";
            this.xrLabel_老年人情感状态总分.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_老年人情感状态总分.SizeF = new System.Drawing.SizeF(209.6653F, 49.95325F);
            this.xrLabel_老年人情感状态总分.StylePriority.UseBorders = false;
            this.xrLabel_老年人情感状态总分.StylePriority.UseTextAlignment = false;
            this.xrLabel_老年人情感状态总分.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell124.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_老年人情感状态});
            this.xrTableCell124.Dpi = 254F;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Weight = 0.74038720318651574D;
            // 
            // xrLabel_老年人情感状态
            // 
            this.xrLabel_老年人情感状态.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_老年人情感状态.Dpi = 254F;
            this.xrLabel_老年人情感状态.LocationFloat = new DevExpress.Utils.PointFloat(115.1304F, 0F);
            this.xrLabel_老年人情感状态.Name = "xrLabel_老年人情感状态";
            this.xrLabel_老年人情感状态.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_老年人情感状态.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_老年人情感状态.StylePriority.UseBorders = false;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell125.Dpi = 254F;
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.StylePriority.UseBorders = false;
            this.xrTableCell125.Weight = 0.68797698734313473D;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell110,
            this.xrTableCell111});
            this.xrTableRow20.Dpi = 254F;
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell105.Dpi = 254F;
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.Weight = 0.40157480314960631D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell106.Dpi = 254F;
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.Weight = 1.1150911586491143D;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell107.Dpi = 254F;
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.StylePriority.UseTextAlignment = false;
            this.xrTableCell107.Text = "  锻炼频率";
            this.xrTableCell107.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell107.Weight = 1.287500576710138D;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell108.Dpi = 254F;
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.StylePriority.UseTextAlignment = false;
            this.xrTableCell108.Text = "  1  每天    2  每周一次以上    3  偶尔    4  不锻炼";
            this.xrTableCell108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell108.Weight = 3.659137635719119D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell110.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_锻炼频率});
            this.xrTableCell110.Dpi = 254F;
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.Weight = 0.74038672259473448D;
            // 
            // xrLabel_锻炼频率
            // 
            this.xrLabel_锻炼频率.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_锻炼频率.Dpi = 254F;
            this.xrLabel_锻炼频率.LocationFloat = new DevExpress.Utils.PointFloat(115.1304F, 7F);
            this.xrLabel_锻炼频率.Name = "xrLabel_锻炼频率";
            this.xrLabel_锻炼频率.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_锻炼频率.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_锻炼频率.StylePriority.UseBorders = false;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell111.Dpi = 254F;
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseBorders = false;
            this.xrTableCell111.Weight = 0.68797698734313473D;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell22,
            this.xrTableCell27,
            this.xrTable_每次锻炼时间,
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTable_坚持锻炼时间,
            this.xrTableCell39,
            this.xrTableCell51});
            this.xrTableRow10.Dpi = 254F;
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell14.Dpi = 254F;
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.Weight = 0.40157480314960631D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell22.Dpi = 254F;
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Text = "体育锻炼";
            this.xrTableCell22.Weight = 1.1150911586491143D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell27.Dpi = 254F;
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "  每次锻炼时间";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell27.Weight = 1.287500576710138D;
            // 
            // xrTable_每次锻炼时间
            // 
            this.xrTable_每次锻炼时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_每次锻炼时间.Dpi = 254F;
            this.xrTable_每次锻炼时间.Name = "xrTable_每次锻炼时间";
            this.xrTable_每次锻炼时间.StylePriority.UseBorders = false;
            this.xrTable_每次锻炼时间.Weight = 0.92291788416584641D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Dpi = 254F;
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Text = "分钟";
            this.xrTableCell37.Weight = 0.55997881551427164D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Dpi = 254F;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.Text = "坚持锻炼时间";
            this.xrTableCell38.Weight = 1.2487379659817912D;
            // 
            // xrTable_坚持锻炼时间
            // 
            this.xrTable_坚持锻炼时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTable_坚持锻炼时间.Dpi = 254F;
            this.xrTable_坚持锻炼时间.Name = "xrTable_坚持锻炼时间";
            this.xrTable_坚持锻炼时间.StylePriority.UseBorders = false;
            this.xrTable_坚持锻炼时间.Weight = 0.92750272976131887D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell39.Dpi = 254F;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.Text = "年";
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell39.Weight = 0.740386962890625D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell51.Dpi = 254F;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseBorders = false;
            this.xrTableCell51.Weight = 0.68797698734313473D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTable_锻炼方式,
            this.xrTableCell17});
            this.xrTableRow7.Dpi = 254F;
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell4.Dpi = 254F;
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseBorders = false;
            this.xrTableCell4.Weight = 0.40157480314960631D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Dpi = 254F;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.Weight = 1.1150911586491143D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Dpi = 254F;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "  锻炼方式";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell6.Weight = 1.287500576710138D;
            // 
            // xrTable_锻炼方式
            // 
            this.xrTable_锻炼方式.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_锻炼方式.Dpi = 254F;
            this.xrTable_锻炼方式.Name = "xrTable_锻炼方式";
            this.xrTable_锻炼方式.StylePriority.UseBorders = false;
            this.xrTable_锻炼方式.Weight = 4.3995243583138537D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell17.Dpi = 254F;
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Weight = 0.68797698734313473D;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell43,
            this.xrTableCell55,
            this.xrTableCell60,
            this.xrTableCell91});
            this.xrTableRow23.Dpi = 254F;
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell32.Dpi = 254F;
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.Weight = 0.40157480314960631D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell43.Dpi = 254F;
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseBorders = false;
            this.xrTableCell43.Text = "饮食习惯";
            this.xrTableCell43.Weight = 1.1150911586491143D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell55.Dpi = 254F;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = "  1  荤素均衡   2  荤食为主   3  素食为主   4  嗜盐   5  嗜油   6  嗜糖";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 4.2062512495386324D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell60.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_饮食习惯1,
            this.xrLabel25,
            this.xrLabel_饮食习惯2,
            this.xrLabel19,
            this.xrLabel_饮食习惯3});
            this.xrTableCell60.Dpi = 254F;
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.Weight = 1.4807736854853593D;
            // 
            // xrLabel_饮食习惯1
            // 
            this.xrLabel_饮食习惯1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_饮食习惯1.Dpi = 254F;
            this.xrLabel_饮食习惯1.LocationFloat = new DevExpress.Utils.PointFloat(134.8378F, 8F);
            this.xrLabel_饮食习惯1.Name = "xrLabel_饮食习惯1";
            this.xrLabel_饮食习惯1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_饮食习惯1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_饮食习惯1.StylePriority.UseBorders = false;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.Dpi = 254F;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(176.7706F, 8F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "/";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_饮食习惯2
            // 
            this.xrLabel_饮食习惯2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_饮食习惯2.Dpi = 254F;
            this.xrLabel_饮食习惯2.LocationFloat = new DevExpress.Utils.PointFloat(218.7034F, 8F);
            this.xrLabel_饮食习惯2.Name = "xrLabel_饮食习惯2";
            this.xrLabel_饮食习惯2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_饮食习惯2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_饮食习惯2.StylePriority.UseBorders = false;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.Dpi = 254F;
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(260.6365F, 8F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "/";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_饮食习惯3
            // 
            this.xrLabel_饮食习惯3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_饮食习惯3.Dpi = 254F;
            this.xrLabel_饮食习惯3.LocationFloat = new DevExpress.Utils.PointFloat(303.1892F, 8F);
            this.xrLabel_饮食习惯3.Name = "xrLabel_饮食习惯3";
            this.xrLabel_饮食习惯3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_饮食习惯3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_饮食习惯3.StylePriority.UseBorders = false;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell91.Dpi = 254F;
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Weight = 0.68797698734313473D;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell92,
            this.xrTableCell98,
            this.xrTableCell99,
            this.xrTableCell114,
            this.xrTableCell121,
            this.xrTableCell122});
            this.xrTableRow24.Dpi = 254F;
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell92.Dpi = 254F;
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Weight = 0.40157480314960631D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell98.Dpi = 254F;
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseBorders = false;
            this.xrTableCell98.Weight = 1.1150911586491143D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell99.Dpi = 254F;
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.StylePriority.UseTextAlignment = false;
            this.xrTableCell99.Text = "  吸烟状况";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell99.Weight = 1.287499615526575D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell114.Dpi = 254F;
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.StylePriority.UseTextAlignment = false;
            this.xrTableCell114.Text = "  1  从不吸烟\t 2  以戒烟\t \t3  吸烟";
            this.xrTableCell114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell114.Weight = 3.6591371551273379D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell121.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_吸烟状况});
            this.xrTableCell121.Dpi = 254F;
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Weight = 0.74038816437007893D;
            // 
            // xrLabel_吸烟状况
            // 
            this.xrLabel_吸烟状况.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_吸烟状况.Dpi = 254F;
            this.xrLabel_吸烟状况.LocationFloat = new DevExpress.Utils.PointFloat(115.1312F, 8F);
            this.xrLabel_吸烟状况.Name = "xrLabel_吸烟状况";
            this.xrLabel_吸烟状况.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_吸烟状况.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_吸烟状况.StylePriority.UseBorders = false;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell122.Dpi = 254F;
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.Weight = 0.68797698734313473D;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.xrTableCell127,
            this.xrTableCell128,
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell131,
            this.xrTableCell132});
            this.xrTableRow25.Dpi = 254F;
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell126.Dpi = 254F;
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseBorders = false;
            this.xrTableCell126.Weight = 0.40157480314960631D;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell127.Dpi = 254F;
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.Text = "吸烟情况";
            this.xrTableCell127.Weight = 1.1150911586491143D;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell128.Dpi = 254F;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseBorders = false;
            this.xrTableCell128.StylePriority.UseTextAlignment = false;
            this.xrTableCell128.Text = "  日吸烟量";
            this.xrTableCell128.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell128.Weight = 1.287499615526575D;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell129.Dpi = 254F;
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.Text = "平均";
            this.xrTableCell129.Weight = 0.44583538385826782D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell130.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_日吸烟量});
            this.xrTableCell130.Dpi = 254F;
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.StylePriority.UseBorders = false;
            this.xrTableCell130.Weight = 0.92400306040846458D;
            // 
            // xrLabel_日吸烟量
            // 
            this.xrLabel_日吸烟量.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_日吸烟量.Dpi = 254F;
            this.xrLabel_日吸烟量.LocationFloat = new DevExpress.Utils.PointFloat(0F, 3.051758E-05F);
            this.xrLabel_日吸烟量.Name = "xrLabel_日吸烟量";
            this.xrLabel_日吸烟量.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_日吸烟量.SizeF = new System.Drawing.SizeF(234.6968F, 49.95313F);
            this.xrLabel_日吸烟量.StylePriority.UseBorders = false;
            this.xrLabel_日吸烟量.StylePriority.UseTextAlignment = false;
            this.xrLabel_日吸烟量.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell131.Dpi = 254F;
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.StylePriority.UseTextAlignment = false;
            this.xrTableCell131.Text = "支";
            this.xrTableCell131.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell131.Weight = 3.0296868752306843D;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell132.Dpi = 254F;
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.StylePriority.UseBorders = false;
            this.xrTableCell132.Weight = 0.68797698734313473D;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell135,
            this.xrTableCell136,
            this.xrTableCell115,
            this.xrTableCell137,
            this.xrTableCell169,
            this.xrTableCell138,
            this.xrTableCell139});
            this.xrTableRow26.Dpi = 254F;
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell133.Dpi = 254F;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.Weight = 0.40157480314960631D;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell134.Dpi = 254F;
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.StylePriority.UseBorders = false;
            this.xrTableCell134.Weight = 1.1150911586491143D;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell135.Dpi = 254F;
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.StylePriority.UseBorders = false;
            this.xrTableCell135.StylePriority.UseTextAlignment = false;
            this.xrTableCell135.Text = "  开始吸烟年龄";
            this.xrTableCell135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell135.Weight = 1.287499615526575D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell136.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_开始吸烟年龄});
            this.xrTableCell136.Dpi = 254F;
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.StylePriority.UseBorders = false;
            this.xrTableCell136.Weight = 0.9645851916215552D;
            // 
            // xrLabel_开始吸烟年龄
            // 
            this.xrLabel_开始吸烟年龄.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_开始吸烟年龄.Dpi = 254F;
            this.xrLabel_开始吸烟年龄.LocationFloat = new DevExpress.Utils.PointFloat(25F, 3.051758E-05F);
            this.xrLabel_开始吸烟年龄.Name = "xrLabel_开始吸烟年龄";
            this.xrLabel_开始吸烟年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_开始吸烟年龄.SizeF = new System.Drawing.SizeF(220.0046F, 49.95313F);
            this.xrLabel_开始吸烟年龄.StylePriority.UseBorders = false;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.Dpi = 254F;
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.StylePriority.UseTextAlignment = false;
            this.xrTableCell115.Text = "岁";
            this.xrTableCell115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell115.Weight = 0.40525421382874016D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell137.Dpi = 254F;
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.StylePriority.UseBorders = false;
            this.xrTableCell137.Text = "戒烟年龄";
            this.xrTableCell137.Weight = 0.89512718380905509D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell169.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_戒烟年龄});
            this.xrTableCell169.Dpi = 254F;
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.StylePriority.UseBorders = false;
            this.xrTableCell169.Weight = 0.92423446535125475D;
            // 
            // xrLabel_戒烟年龄
            // 
            this.xrLabel_戒烟年龄.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_戒烟年龄.Dpi = 254F;
            this.xrLabel_戒烟年龄.LocationFloat = new DevExpress.Utils.PointFloat(11.01948F, 0F);
            this.xrLabel_戒烟年龄.Name = "xrLabel_戒烟年龄";
            this.xrLabel_戒烟年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_戒烟年龄.SizeF = new System.Drawing.SizeF(220.0046F, 49.95313F);
            this.xrLabel_戒烟年龄.StylePriority.UseBorders = false;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell138.Dpi = 254F;
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseBorders = false;
            this.xrTableCell138.StylePriority.UseTextAlignment = false;
            this.xrTableCell138.Text = "岁";
            this.xrTableCell138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell138.Weight = 1.2103242648868109D;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell139.Dpi = 254F;
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.StylePriority.UseBorders = false;
            this.xrTableCell139.Weight = 0.68797698734313473D;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell145,
            this.xrTableCell146,
            this.xrTableCell147});
            this.xrTableRow28.Dpi = 254F;
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell109.Dpi = 254F;
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.Text = "生";
            this.xrTableCell109.Weight = 0.40157480314960631D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell142.Dpi = 254F;
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.Weight = 1.1150911586491143D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell143.Dpi = 254F;
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.StylePriority.UseTextAlignment = false;
            this.xrTableCell143.Text = "  饮酒频率";
            this.xrTableCell143.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell143.Weight = 1.287499615526575D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell145.Dpi = 254F;
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.StylePriority.UseTextAlignment = false;
            this.xrTableCell145.Text = "  1  从不      2  偶尔      3  经常      4  每天";
            this.xrTableCell145.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell145.Weight = 3.6591381163109009D;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell146.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_饮酒频率});
            this.xrTableCell146.Dpi = 254F;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBorders = false;
            this.xrTableCell146.Weight = 0.74038720318651574D;
            // 
            // xrLabel_饮酒频率
            // 
            this.xrLabel_饮酒频率.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_饮酒频率.Dpi = 254F;
            this.xrLabel_饮酒频率.LocationFloat = new DevExpress.Utils.PointFloat(115.1304F, 8F);
            this.xrLabel_饮酒频率.Name = "xrLabel_饮酒频率";
            this.xrLabel_饮酒频率.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_饮酒频率.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_饮酒频率.StylePriority.UseBorders = false;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell147.Dpi = 254F;
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBorders = false;
            this.xrTableCell147.Weight = 0.68797698734313473D;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell148,
            this.xrTableCell149,
            this.xrTableCell150,
            this.xrTableCell151,
            this.xrTableCell152,
            this.xrTableCell153,
            this.xrTableCell154});
            this.xrTableRow29.Dpi = 254F;
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell148.Dpi = 254F;
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.StylePriority.UseBorders = false;
            this.xrTableCell148.Text = "活";
            this.xrTableCell148.Weight = 0.40157480314960631D;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell149.Dpi = 254F;
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseBorders = false;
            this.xrTableCell149.Weight = 1.1150911586491143D;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell150.Dpi = 254F;
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.StylePriority.UseBorders = false;
            this.xrTableCell150.StylePriority.UseTextAlignment = false;
            this.xrTableCell150.Text = "  日饮酒量";
            this.xrTableCell150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell150.Weight = 1.287499615526575D;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell151.Dpi = 254F;
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseBorders = false;
            this.xrTableCell151.Text = "平均";
            this.xrTableCell151.Weight = 0.44583538385826782D;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell152.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_日饮酒量});
            this.xrTableCell152.Dpi = 254F;
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseBorders = false;
            this.xrTableCell152.Weight = 0.92400378129613714D;
            // 
            // xrLabel_日饮酒量
            // 
            this.xrLabel_日饮酒量.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_日饮酒量.Dpi = 254F;
            this.xrLabel_日饮酒量.LocationFloat = new DevExpress.Utils.PointFloat(0.0002583821F, 0F);
            this.xrLabel_日饮酒量.Name = "xrLabel_日饮酒量";
            this.xrLabel_日饮酒量.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_日饮酒量.SizeF = new System.Drawing.SizeF(234.6968F, 49.95313F);
            this.xrLabel_日饮酒量.StylePriority.UseBorders = false;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell153.Dpi = 254F;
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.StylePriority.UseTextAlignment = false;
            this.xrTableCell153.Text = "两";
            this.xrTableCell153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell153.Weight = 3.0296861543430125D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell154.Dpi = 254F;
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseBorders = false;
            this.xrTableCell154.Weight = 0.68797698734313473D;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell50,
            this.xrTableCell54,
            this.xrTableCell81,
            this.xrTableCell144,
            this.xrTableCell103,
            this.xrTableCell104});
            this.xrTableRow27.Dpi = 254F;
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell9.Dpi = 254F;
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.Text = "方";
            this.xrTableCell9.Weight = 0.40157480314960631D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell10.Dpi = 254F;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Text = "饮酒情况";
            this.xrTableCell10.Weight = 1.1150911586491143D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Dpi = 254F;
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.StylePriority.UseTextAlignment = false;
            this.xrTableCell50.Text = "  是否戒酒";
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell50.Weight = 1.287499615526575D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell54.Dpi = 254F;
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.StylePriority.UseTextAlignment = false;
            this.xrTableCell54.Text = "  1  未戒酒   2  已戒酒，戒酒年龄：";
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell54.Weight = 2.2649665892593505D;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell81.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_戒酒年龄});
            this.xrTableCell81.Dpi = 254F;
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseBorders = false;
            this.xrTableCell81.Weight = 0.86883604995847685D;
            // 
            // xrLabel_戒酒年龄
            // 
            this.xrLabel_戒酒年龄.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_戒酒年龄.Dpi = 254F;
            this.xrLabel_戒酒年龄.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_戒酒年龄.Name = "xrLabel_戒酒年龄";
            this.xrLabel_戒酒年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_戒酒年龄.SizeF = new System.Drawing.SizeF(220.0046F, 49.95313F);
            this.xrLabel_戒酒年龄.StylePriority.UseBorders = false;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell144.Dpi = 254F;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.StylePriority.UseTextAlignment = false;
            this.xrTableCell144.Text = "岁";
            this.xrTableCell144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell144.Weight = 0.52533583753690949D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell103.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_是否戒酒});
            this.xrTableCell103.Dpi = 254F;
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.Weight = 0.74038684274267963D;
            // 
            // xrLabel_是否戒酒
            // 
            this.xrLabel_是否戒酒.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_是否戒酒.Dpi = 254F;
            this.xrLabel_是否戒酒.LocationFloat = new DevExpress.Utils.PointFloat(115.1304F, 8F);
            this.xrLabel_是否戒酒.Name = "xrLabel_是否戒酒";
            this.xrLabel_是否戒酒.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_是否戒酒.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_是否戒酒.StylePriority.UseBorders = false;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell104.Dpi = 254F;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseBorders = false;
            this.xrTableCell104.Weight = 0.68797698734313473D;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell162,
            this.xrTableCell163,
            this.xrTableCell164,
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell167,
            this.xrTableCell168});
            this.xrTableRow31.Dpi = 254F;
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell162.Dpi = 254F;
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseBorders = false;
            this.xrTableCell162.Text = "式";
            this.xrTableCell162.Weight = 0.40157480314960631D;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell163.Dpi = 254F;
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.StylePriority.UseBorders = false;
            this.xrTableCell163.Weight = 1.1150911586491143D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell164.Dpi = 254F;
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseBorders = false;
            this.xrTableCell164.StylePriority.UseTextAlignment = false;
            this.xrTableCell164.Text = "  开始饮酒年龄";
            this.xrTableCell164.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell164.Weight = 1.287499615526575D;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell165.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_开始饮酒年龄});
            this.xrTableCell165.Dpi = 254F;
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.StylePriority.UseBorders = false;
            this.xrTableCell165.Weight = 0.9645851916215552D;
            // 
            // xrLabel_开始饮酒年龄
            // 
            this.xrLabel_开始饮酒年龄.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_开始饮酒年龄.Dpi = 254F;
            this.xrLabel_开始饮酒年龄.LocationFloat = new DevExpress.Utils.PointFloat(25.00015F, 0F);
            this.xrLabel_开始饮酒年龄.Name = "xrLabel_开始饮酒年龄";
            this.xrLabel_开始饮酒年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_开始饮酒年龄.SizeF = new System.Drawing.SizeF(220.0046F, 49.95313F);
            this.xrLabel_开始饮酒年龄.StylePriority.UseBorders = false;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell166.Dpi = 254F;
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.StylePriority.UseTextAlignment = false;
            this.xrTableCell166.Text = "岁";
            this.xrTableCell166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell166.Weight = 0.40525325264517709D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell212.Dpi = 254F;
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.StylePriority.UseBorders = false;
            this.xrTableCell212.Text = "近一年是否醉酒";
            this.xrTableCell212.Weight = 1.1520275206077757D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell213.Dpi = 254F;
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.StylePriority.UseBorders = false;
            this.xrTableCell213.StylePriority.UseTextAlignment = false;
            this.xrTableCell213.Text = "  1  是    2  否";
            this.xrTableCell213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell213.Weight = 1.1372724518062563D;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell167.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_是否醉酒});
            this.xrTableCell167.Dpi = 254F;
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.StylePriority.UseBorders = false;
            this.xrTableCell167.Weight = 0.74038690281665231D;
            // 
            // xrLabel_是否醉酒
            // 
            this.xrLabel_是否醉酒.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_是否醉酒.Dpi = 254F;
            this.xrLabel_是否醉酒.LocationFloat = new DevExpress.Utils.PointFloat(115.1307F, 8F);
            this.xrLabel_是否醉酒.Name = "xrLabel_是否醉酒";
            this.xrLabel_是否醉酒.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_是否醉酒.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_是否醉酒.StylePriority.UseBorders = false;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell168.Dpi = 254F;
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.StylePriority.UseBorders = false;
            this.xrTableCell168.Weight = 0.68797698734313473D;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155,
            this.xrTableCell156,
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell159,
            this.xrTableCell160,
            this.xrTableCell161});
            this.xrTableRow30.Dpi = 254F;
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell155.Dpi = 254F;
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseBorders = false;
            this.xrTableCell155.Weight = 0.40157480314960631D;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell156.Dpi = 254F;
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseBorders = false;
            this.xrTableCell156.Weight = 1.1150911586491143D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell157.Dpi = 254F;
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.StylePriority.UseBorders = false;
            this.xrTableCell157.StylePriority.UseTextAlignment = false;
            this.xrTableCell157.Text = "  饮酒种类";
            this.xrTableCell157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell157.Weight = 1.287499615526575D;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell158.Dpi = 254F;
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.StylePriority.UseTextAlignment = false;
            this.xrTableCell158.Text = " 1 白酒  2 啤酒  3 红酒  4 黄酒  5 其他";
            this.xrTableCell158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell158.Weight = 2.3551986574187991D;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell159.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_饮酒种类其他});
            this.xrTableCell159.Dpi = 254F;
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.StylePriority.UseBorders = false;
            this.xrTableCell159.Weight = 0.6619774525559794D;
            // 
            // xrLabel_饮酒种类其他
            // 
            this.xrLabel_饮酒种类其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_饮酒种类其他.Dpi = 254F;
            this.xrLabel_饮酒种类其他.LocationFloat = new DevExpress.Utils.PointFloat(0.6179199F, 0.0001525879F);
            this.xrLabel_饮酒种类其他.Name = "xrLabel_饮酒种类其他";
            this.xrLabel_饮酒种类其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_饮酒种类其他.SizeF = new System.Drawing.SizeF(167.5244F, 49.95313F);
            this.xrLabel_饮酒种类其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell160.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_饮酒种类1,
            this.xrLabel45,
            this.xrLabel_饮酒种类2,
            this.xrLabel43,
            this.xrLabel_饮酒种类3,
            this.xrLabel41,
            this.xrLabel_饮酒种类4});
            this.xrTableCell160.Dpi = 254F;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.Weight = 1.3823492095226377D;
            // 
            // xrLabel_饮酒种类1
            // 
            this.xrLabel_饮酒种类1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_饮酒种类1.Dpi = 254F;
            this.xrLabel_饮酒种类1.LocationFloat = new DevExpress.Utils.PointFloat(25.97119F, 8F);
            this.xrLabel_饮酒种类1.Name = "xrLabel_饮酒种类1";
            this.xrLabel_饮酒种类1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_饮酒种类1.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_饮酒种类1.StylePriority.UseBorders = false;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel45.Dpi = 254F;
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(67.90405F, 8F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "/";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_饮酒种类2
            // 
            this.xrLabel_饮酒种类2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_饮酒种类2.Dpi = 254F;
            this.xrLabel_饮酒种类2.LocationFloat = new DevExpress.Utils.PointFloat(109.8372F, 8F);
            this.xrLabel_饮酒种类2.Name = "xrLabel_饮酒种类2";
            this.xrLabel_饮酒种类2.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_饮酒种类2.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_饮酒种类2.StylePriority.UseBorders = false;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel43.Dpi = 254F;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(151.7703F, 8F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "/";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_饮酒种类3
            // 
            this.xrLabel_饮酒种类3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_饮酒种类3.Dpi = 254F;
            this.xrLabel_饮酒种类3.LocationFloat = new DevExpress.Utils.PointFloat(193.7034F, 8F);
            this.xrLabel_饮酒种类3.Name = "xrLabel_饮酒种类3";
            this.xrLabel_饮酒种类3.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_饮酒种类3.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_饮酒种类3.StylePriority.UseBorders = false;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel41.Dpi = 254F;
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(235.6365F, 8F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel41.StylePriority.UseBorders = false;
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.StylePriority.UseTextAlignment = false;
            this.xrLabel41.Text = "/";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_饮酒种类4
            // 
            this.xrLabel_饮酒种类4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_饮酒种类4.Dpi = 254F;
            this.xrLabel_饮酒种类4.LocationFloat = new DevExpress.Utils.PointFloat(278.189F, 8F);
            this.xrLabel_饮酒种类4.Name = "xrLabel_饮酒种类4";
            this.xrLabel_饮酒种类4.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_饮酒种类4.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_饮酒种类4.StylePriority.UseBorders = false;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell161.Dpi = 254F;
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.StylePriority.UseBorders = false;
            this.xrTableCell161.Weight = 0.68797698734313473D;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell184,
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell187,
            this.xrTableCell188,
            this.xrTableCell214,
            this.xrTableCell215,
            this.xrTableCell189,
            this.xrTableCell190});
            this.xrTableRow34.Dpi = 254F;
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell184.Dpi = 254F;
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseBorders = false;
            this.xrTableCell184.Weight = 0.40157480314960631D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell185.Dpi = 254F;
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.StylePriority.UseBorders = false;
            this.xrTableCell185.Weight = 1.1150911586491143D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Dpi = 254F;
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseTextAlignment = false;
            this.xrTableCell186.Text = "  1  无   2  有（工种";
            this.xrTableCell186.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell186.Weight = 1.287500576710138D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_工种});
            this.xrTableCell187.Dpi = 254F;
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 0.79583403820127963D;
            // 
            // xrLabel_工种
            // 
            this.xrLabel_工种.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_工种.Dpi = 254F;
            this.xrLabel_工种.LocationFloat = new DevExpress.Utils.PointFloat(2.370117F, 0F);
            this.xrLabel_工种.Name = "xrLabel_工种";
            this.xrLabel_工种.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_工种.SizeF = new System.Drawing.SizeF(199.7715F, 49.95313F);
            this.xrLabel_工种.StylePriority.UseBorders = false;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Dpi = 254F;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Text = "从业时间";
            this.xrTableCell188.Weight = 0.68706266147883865D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_从业时间});
            this.xrTableCell214.Dpi = 254F;
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.Weight = 0.825452669398991D;
            // 
            // xrLabel_从业时间
            // 
            this.xrLabel_从业时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_从业时间.Dpi = 254F;
            this.xrLabel_从业时间.LocationFloat = new DevExpress.Utils.PointFloat(3.686279F, 0.0001525879F);
            this.xrLabel_从业时间.Name = "xrLabel_从业时间";
            this.xrLabel_从业时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_从业时间.SizeF = new System.Drawing.SizeF(205.9788F, 49.95313F);
            this.xrLabel_从业时间.StylePriority.UseBorders = false;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Dpi = 254F;
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.StylePriority.UseTextAlignment = false;
            this.xrTableCell215.Text = "年）";
            this.xrTableCell215.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell215.Weight = 1.3507877860482283D;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell189.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_接触史有无});
            this.xrTableCell189.Dpi = 254F;
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.StylePriority.UseBorders = false;
            this.xrTableCell189.StylePriority.UseTextAlignment = false;
            this.xrTableCell189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell189.Weight = 0.74038720318651574D;
            // 
            // xrLabel_接触史有无
            // 
            this.xrLabel_接触史有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_接触史有无.Dpi = 254F;
            this.xrLabel_接触史有无.LocationFloat = new DevExpress.Utils.PointFloat(115.1304F, 8F);
            this.xrLabel_接触史有无.Name = "xrLabel_接触史有无";
            this.xrLabel_接触史有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_接触史有无.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_接触史有无.StylePriority.UseBorders = false;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell190.Dpi = 254F;
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseBorders = false;
            this.xrTableCell190.Weight = 0.68797698734313473D;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell191,
            this.xrTableCell192,
            this.xrTableCell193,
            this.xrTableCell217,
            this.xrTableCell216,
            this.xrTableCell194,
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell197});
            this.xrTableRow35.Dpi = 254F;
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell191.Dpi = 254F;
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.StylePriority.UseBorders = false;
            this.xrTableCell191.Weight = 0.40157480314960631D;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell192.Dpi = 254F;
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.StylePriority.UseBorders = false;
            this.xrTableCell192.Weight = 1.1150911586491143D;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Dpi = 254F;
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Text = "毒物种类";
            this.xrTableCell193.Weight = 0.85000153789370092D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Dpi = 254F;
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.StylePriority.UseTextAlignment = false;
            this.xrTableCell217.Text = "粉尘";
            this.xrTableCell217.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell217.Weight = 0.310153750922736D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_粉尘});
            this.xrTableCell216.Dpi = 254F;
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.Weight = 1.4451517420490894D;
            // 
            // xrLabel_粉尘
            // 
            this.xrLabel_粉尘.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_粉尘.Dpi = 254F;
            this.xrLabel_粉尘.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.100006F);
            this.xrLabel_粉尘.Name = "xrLabel_粉尘";
            this.xrLabel_粉尘.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_粉尘.SizeF = new System.Drawing.SizeF(342.0686F, 49.95313F);
            this.xrLabel_粉尘.StylePriority.UseBorders = false;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Dpi = 254F;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.Text = "防护措施  1  无   2  有";
            this.xrTableCell194.Weight = 1.3613617664247046D;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_粉尘防护措施});
            this.xrTableCell195.Dpi = 254F;
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.Weight = 0.9799689345472441D;
            // 
            // xrLabel_粉尘防护措施
            // 
            this.xrLabel_粉尘防护措施.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_粉尘防护措施.Dpi = 254F;
            this.xrLabel_粉尘防护措施.LocationFloat = new DevExpress.Utils.PointFloat(0F, 5.100006F);
            this.xrLabel_粉尘防护措施.Name = "xrLabel_粉尘防护措施";
            this.xrLabel_粉尘防护措施.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_粉尘防护措施.SizeF = new System.Drawing.SizeF(248.9119F, 49.95313F);
            this.xrLabel_粉尘防护措施.StylePriority.UseBorders = false;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell196.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_粉尘防护措施有无});
            this.xrTableCell196.Dpi = 254F;
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.StylePriority.UseBorders = false;
            this.xrTableCell196.Weight = 0.74038720318651574D;
            // 
            // xrLabel_粉尘防护措施有无
            // 
            this.xrLabel_粉尘防护措施有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_粉尘防护措施有无.Dpi = 254F;
            this.xrLabel_粉尘防护措施有无.LocationFloat = new DevExpress.Utils.PointFloat(115.1304F, 8F);
            this.xrLabel_粉尘防护措施有无.Name = "xrLabel_粉尘防护措施有无";
            this.xrLabel_粉尘防护措施有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_粉尘防护措施有无.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_粉尘防护措施有无.StylePriority.UseBorders = false;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell197.Dpi = 254F;
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.StylePriority.UseBorders = false;
            this.xrTableCell197.Weight = 0.68797698734313473D;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell198,
            this.xrTableCell199,
            this.xrTableCell200,
            this.xrTableCell219,
            this.xrTableCell218,
            this.xrTableCell201,
            this.xrTableCell202,
            this.xrTableCell203,
            this.xrTableCell204});
            this.xrTableRow36.Dpi = 254F;
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell198.Dpi = 254F;
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseBorders = false;
            this.xrTableCell198.Weight = 0.40157480314960631D;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell199.Dpi = 254F;
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.StylePriority.UseBorders = false;
            this.xrTableCell199.Text = "职业疾病危害因";
            this.xrTableCell199.Weight = 1.1150911586491143D;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Dpi = 254F;
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Weight = 0.85000153789370092D;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Dpi = 254F;
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.StylePriority.UseTextAlignment = false;
            this.xrTableCell219.Text = "放射物质";
            this.xrTableCell219.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell219.Weight = 0.62291615403543288D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_放射物质});
            this.xrTableCell218.Dpi = 254F;
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Weight = 1.1323903001199556D;
            // 
            // xrLabel_放射物质
            // 
            this.xrLabel_放射物质.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_放射物质.Dpi = 254F;
            this.xrLabel_放射物质.LocationFloat = new DevExpress.Utils.PointFloat(0F, 4.100006F);
            this.xrLabel_放射物质.Name = "xrLabel_放射物质";
            this.xrLabel_放射物质.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_放射物质.SizeF = new System.Drawing.SizeF(262.627F, 49.95313F);
            this.xrLabel_放射物质.StylePriority.UseBorders = false;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Dpi = 254F;
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Text = "防护措施  1  无   2  有";
            this.xrTableCell201.Weight = 1.3613608052411417D;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_放射物质防护措施});
            this.xrTableCell202.Dpi = 254F;
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Weight = 0.9799689345472441D;
            // 
            // xrLabel_放射物质防护措施
            // 
            this.xrLabel_放射物质防护措施.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_放射物质防护措施.Dpi = 254F;
            this.xrLabel_放射物质防护措施.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 4.100006F);
            this.xrLabel_放射物质防护措施.Name = "xrLabel_放射物质防护措施";
            this.xrLabel_放射物质防护措施.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_放射物质防护措施.SizeF = new System.Drawing.SizeF(248.9114F, 49.95313F);
            this.xrLabel_放射物质防护措施.StylePriority.UseBorders = false;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell203.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_放射物质防护措施有无});
            this.xrTableCell203.Dpi = 254F;
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.StylePriority.UseBorders = false;
            this.xrTableCell203.Weight = 0.74038720318651574D;
            // 
            // xrLabel_放射物质防护措施有无
            // 
            this.xrLabel_放射物质防护措施有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_放射物质防护措施有无.Dpi = 254F;
            this.xrLabel_放射物质防护措施有无.LocationFloat = new DevExpress.Utils.PointFloat(115.1307F, 8F);
            this.xrLabel_放射物质防护措施有无.Name = "xrLabel_放射物质防护措施有无";
            this.xrLabel_放射物质防护措施有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_放射物质防护措施有无.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_放射物质防护措施有无.StylePriority.UseBorders = false;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell204.Dpi = 254F;
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.StylePriority.UseBorders = false;
            this.xrTableCell204.Weight = 0.68797698734313473D;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell179,
            this.xrTableCell221,
            this.xrTableCell220,
            this.xrTableCell180,
            this.xrTableCell181,
            this.xrTableCell182,
            this.xrTableCell183});
            this.xrTableRow33.Dpi = 254F;
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell177.Dpi = 254F;
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.StylePriority.UseBorders = false;
            this.xrTableCell177.Weight = 0.40157480314960631D;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell178.Dpi = 254F;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.Text = "素接触史";
            this.xrTableCell178.Weight = 1.1150911586491143D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Dpi = 254F;
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 0.85000153789370092D;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Dpi = 254F;
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseTextAlignment = false;
            this.xrTableCell221.Text = "物理因素";
            this.xrTableCell221.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell221.Weight = 0.62291615403543288D;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_物理因素});
            this.xrTableCell220.Dpi = 254F;
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Weight = 1.1323903001199556D;
            // 
            // xrLabel_物理因素
            // 
            this.xrLabel_物理因素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_物理因素.Dpi = 254F;
            this.xrLabel_物理因素.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_物理因素.Name = "xrLabel_物理因素";
            this.xrLabel_物理因素.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_物理因素.SizeF = new System.Drawing.SizeF(262.6267F, 49.95313F);
            this.xrLabel_物理因素.StylePriority.UseBorders = false;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Dpi = 254F;
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Text = "防护措施  1  无   2  有";
            this.xrTableCell180.Weight = 1.3613608052411417D;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_物理因素防护措施});
            this.xrTableCell181.Dpi = 254F;
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Weight = 0.9799689345472441D;
            // 
            // xrLabel_物理因素防护措施
            // 
            this.xrLabel_物理因素防护措施.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_物理因素防护措施.Dpi = 254F;
            this.xrLabel_物理因素防护措施.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 3.100006F);
            this.xrLabel_物理因素防护措施.Name = "xrLabel_物理因素防护措施";
            this.xrLabel_物理因素防护措施.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_物理因素防护措施.SizeF = new System.Drawing.SizeF(248.9114F, 49.95313F);
            this.xrLabel_物理因素防护措施.StylePriority.UseBorders = false;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell182.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_物理因素防护措施有无});
            this.xrTableCell182.Dpi = 254F;
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.Weight = 0.74038720318651574D;
            // 
            // xrLabel_物理因素防护措施有无
            // 
            this.xrLabel_物理因素防护措施有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_物理因素防护措施有无.Dpi = 254F;
            this.xrLabel_物理因素防护措施有无.LocationFloat = new DevExpress.Utils.PointFloat(115.1304F, 8F);
            this.xrLabel_物理因素防护措施有无.Name = "xrLabel_物理因素防护措施有无";
            this.xrLabel_物理因素防护措施有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_物理因素防护措施有无.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_物理因素防护措施有无.StylePriority.UseBorders = false;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell183.Dpi = 254F;
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.StylePriority.UseBorders = false;
            this.xrTableCell183.Weight = 0.68797698734313473D;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell170,
            this.xrTableCell171,
            this.xrTableCell172,
            this.xrTableCell223,
            this.xrTableCell222,
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell175,
            this.xrTableCell176});
            this.xrTableRow32.Dpi = 254F;
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell170.Dpi = 254F;
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.Weight = 0.40157480314960631D;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell171.Dpi = 254F;
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.StylePriority.UseBorders = false;
            this.xrTableCell171.Weight = 1.1150911586491143D;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Dpi = 254F;
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Weight = 0.85000153789370092D;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Dpi = 254F;
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.StylePriority.UseTextAlignment = false;
            this.xrTableCell223.Text = "化学物质";
            this.xrTableCell223.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell223.Weight = 0.62291615403543288D;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_化学物质});
            this.xrTableCell222.Dpi = 254F;
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Weight = 1.1323903001199556D;
            // 
            // xrLabel_化学物质
            // 
            this.xrLabel_化学物质.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_化学物质.Dpi = 254F;
            this.xrLabel_化学物质.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.100006F);
            this.xrLabel_化学物质.Name = "xrLabel_化学物质";
            this.xrLabel_化学物质.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_化学物质.SizeF = new System.Drawing.SizeF(262.6267F, 49.95313F);
            this.xrLabel_化学物质.StylePriority.UseBorders = false;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Dpi = 254F;
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Text = "防护措施  1  无   2  有";
            this.xrTableCell173.Weight = 1.3613608052411417D;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_化学物质防护措施});
            this.xrTableCell174.Dpi = 254F;
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Weight = 0.9799689345472441D;
            // 
            // xrLabel_化学物质防护措施
            // 
            this.xrLabel_化学物质防护措施.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_化学物质防护措施.Dpi = 254F;
            this.xrLabel_化学物质防护措施.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.100006F);
            this.xrLabel_化学物质防护措施.Name = "xrLabel_化学物质防护措施";
            this.xrLabel_化学物质防护措施.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_化学物质防护措施.SizeF = new System.Drawing.SizeF(248.9119F, 49.95313F);
            this.xrLabel_化学物质防护措施.StylePriority.UseBorders = false;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell175.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_化学物质防护措施有无});
            this.xrTableCell175.Dpi = 254F;
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseBorders = false;
            this.xrTableCell175.Weight = 0.74038720318651574D;
            // 
            // xrLabel_化学物质防护措施有无
            // 
            this.xrLabel_化学物质防护措施有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_化学物质防护措施有无.Dpi = 254F;
            this.xrLabel_化学物质防护措施有无.LocationFloat = new DevExpress.Utils.PointFloat(115.1304F, 8F);
            this.xrLabel_化学物质防护措施有无.Name = "xrLabel_化学物质防护措施有无";
            this.xrLabel_化学物质防护措施有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_化学物质防护措施有无.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_化学物质防护措施有无.StylePriority.UseBorders = false;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell176.Dpi = 254F;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.StylePriority.UseBorders = false;
            this.xrTableCell176.Weight = 0.68797698734313473D;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell205,
            this.xrTableCell206,
            this.xrTableCell207,
            this.xrTableCell225,
            this.xrTableCell224,
            this.xrTableCell208,
            this.xrTableCell209,
            this.xrTableCell210,
            this.xrTableCell211});
            this.xrTableRow37.Dpi = 254F;
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell205.Dpi = 254F;
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.StylePriority.UseBorders = false;
            this.xrTableCell205.Weight = 0.40157480314960631D;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell206.Dpi = 254F;
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.StylePriority.UseBorders = false;
            this.xrTableCell206.Weight = 1.1150911586491143D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell207.Dpi = 254F;
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.StylePriority.UseBorders = false;
            this.xrTableCell207.Weight = 0.85000153789370092D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell225.Dpi = 254F;
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseBorders = false;
            this.xrTableCell225.StylePriority.UseTextAlignment = false;
            this.xrTableCell225.Text = "其他";
            this.xrTableCell225.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell225.Weight = 0.31015375092273612D;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell224.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_接触史其他});
            this.xrTableCell224.Dpi = 254F;
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.StylePriority.UseBorders = false;
            this.xrTableCell224.Weight = 1.4451527032326523D;
            // 
            // xrLabel_接触史其他
            // 
            this.xrLabel_接触史其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_接触史其他.Dpi = 254F;
            this.xrLabel_接触史其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.10025F);
            this.xrLabel_接触史其他.Name = "xrLabel_接触史其他";
            this.xrLabel_接触史其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_接触史其他.SizeF = new System.Drawing.SizeF(342.0684F, 49.95288F);
            this.xrLabel_接触史其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell208.Dpi = 254F;
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.StylePriority.UseBorders = false;
            this.xrTableCell208.Text = "防护措施  1  无   2  有";
            this.xrTableCell208.Weight = 1.3613608052411417D;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell209.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_接触史其他防护措施});
            this.xrTableCell209.Dpi = 254F;
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.StylePriority.UseBorders = false;
            this.xrTableCell209.Weight = 0.9799689345472441D;
            // 
            // xrLabel_接触史其他防护措施
            // 
            this.xrLabel_接触史其他防护措施.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_接触史其他防护措施.Dpi = 254F;
            this.xrLabel_接触史其他防护措施.LocationFloat = new DevExpress.Utils.PointFloat(0.0002441406F, 1.10025F);
            this.xrLabel_接触史其他防护措施.Name = "xrLabel_接触史其他防护措施";
            this.xrLabel_接触史其他防护措施.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_接触史其他防护措施.SizeF = new System.Drawing.SizeF(248.9114F, 49.95288F);
            this.xrLabel_接触史其他防护措施.StylePriority.UseBorders = false;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell210.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_接触史其他防护措施有无});
            this.xrTableCell210.Dpi = 254F;
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.StylePriority.UseBorders = false;
            this.xrTableCell210.Weight = 0.74038720318651574D;
            // 
            // xrLabel_接触史其他防护措施有无
            // 
            this.xrLabel_接触史其他防护措施有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_接触史其他防护措施有无.Dpi = 254F;
            this.xrLabel_接触史其他防护措施有无.LocationFloat = new DevExpress.Utils.PointFloat(115.1307F, 8F);
            this.xrLabel_接触史其他防护措施有无.Name = "xrLabel_接触史其他防护措施有无";
            this.xrLabel_接触史其他防护措施有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel_接触史其他防护措施有无.SizeF = new System.Drawing.SizeF(41.93311F, 47.83679F);
            this.xrLabel_接触史其他防护措施有无.StylePriority.UseBorders = false;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell211.Dpi = 254F;
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.StylePriority.UseBorders = false;
            this.xrTableCell211.Weight = 0.68797698734313473D;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(2520.324F, 107.95F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 4, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(1051.983F, 81.70332F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "国家基本公共卫生服务项目健康体检表";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.Dpi = 254F;
            this.TopMargin.HeightF = 5F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Dpi = 254F;
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrCrossBandLine1
            // 
            this.xrCrossBandLine1.Dpi = 254F;
            this.xrCrossBandLine1.EndBand = this.Detail;
            this.xrCrossBandLine1.EndPointFloat = new DevExpress.Utils.PointFloat(2050F, 2686.166F);
            this.xrCrossBandLine1.LocationFloat = new DevExpress.Utils.PointFloat(2050F, 0F);
            this.xrCrossBandLine1.Name = "xrCrossBandLine1";
            this.xrCrossBandLine1.StartBand = this.TopMargin;
            this.xrCrossBandLine1.StartPointFloat = new DevExpress.Utils.PointFloat(2050F, 0F);
            this.xrCrossBandLine1.Visible = false;
            this.xrCrossBandLine1.WidthF = 3F;
            // 
            // report健康体检表1_2017
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.CrossBandControls.AddRange(new DevExpress.XtraReports.UI.XRCrossBandControl[] {
            this.xrCrossBandLine1});
            this.Dpi = 254F;
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(53, 34, 5, 0);
            this.PageHeight = 2970;
            this.PageWidth = 4200;
            this.PaperKind = System.Drawing.Printing.PaperKind.A3;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRCrossBandLine xrCrossBandLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_责任医生;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体检日期年;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体检日期月;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体检日期日;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_症状医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_呼吸频率;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体温;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_脉率;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_身高;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_腰围;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_一般状况医师签字;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_左侧血压1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_左侧血压2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_右侧血压1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_右侧血压2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_锻炼方式;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体重;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_体重指数;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_老年人健康状态自我评估;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_老年人生活自理能力自我评估;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_老年人认知功能;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_老年人情感状态;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_日吸烟量;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_老年人认知功能总分;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_老年人情感状态总分;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_每次锻炼时间;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_坚持锻炼时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_锻炼频率;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_饮食习惯1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_饮食习惯2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_饮食习惯3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_吸烟状况;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_开始吸烟年龄;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_戒烟年龄;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_饮酒频率;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_日饮酒量;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_戒酒年龄;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_是否戒酒;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_开始饮酒年龄;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_是否醉酒;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_饮酒种类其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_饮酒种类1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_饮酒种类2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_饮酒种类3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_饮酒种类4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_工种;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_从业时间;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_接触史有无;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_粉尘防护措施有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_放射物质防护措施有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_物理因素防护措施有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_化学物质防护措施有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_接触史其他防护措施有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_粉尘;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_粉尘防护措施;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_放射物质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_放射物质防护措施;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_物理因素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_物理因素防护措施;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_化学物质;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_化学物质防护措施;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_接触史其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_接触史其他防护措施;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药医师签字;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称5;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法5;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量5;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间5;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种史名称1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种机构1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种医师签字;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种史名称2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种机构2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种日期2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种史名称3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种日期3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种机构3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell341;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell346;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell347;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell349;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell350;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell351;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_健康评价医师签字;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell344;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康评价异常1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康评价异常2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康评价异常3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康评价异常4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康评价;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell348;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell353;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell354;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell365;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell367;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell368;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell369;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell362;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell363;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_健康指导医师签字;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell355;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell357;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell359;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell366;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell356;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell361;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell358;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康指导1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_减重目标;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_建议接种疫苗;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素其他补充;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell370;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell373;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell395;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell400;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell405;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell406;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell409;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell386;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell387;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell389;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell375;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell376;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell377;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell378;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell379;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell371;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell372;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_本人签字;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_家属签字;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_反馈人签字;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell382;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell383;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell381;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_反馈时间年;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell374;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_反馈时间月;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_反馈时间日;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_症状9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_建议接种疫苗补充;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素其他;
    }
}
