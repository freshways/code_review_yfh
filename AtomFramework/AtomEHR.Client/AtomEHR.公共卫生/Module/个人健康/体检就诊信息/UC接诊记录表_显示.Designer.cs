﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class UC接诊记录表_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC接诊记录表_显示));
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn新增 = new DevExpress.XtraEditors.SimpleButton();
            this.lab考核项 = new DevExpress.XtraEditors.LabelControl();
            this.LayoutPanel1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt接诊医生 = new DevExpress.XtraEditors.TextEdit();
            this.lbl性别 = new DevExpress.XtraEditors.LabelControl();
            this.lbl出生日期 = new DevExpress.XtraEditors.LabelControl();
            this.lbl身份证号 = new DevExpress.XtraEditors.LabelControl();
            this.lbl联系电话 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近更新时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl最近修改人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl居住地址 = new DevExpress.XtraEditors.LabelControl();
            this.txt主观资料 = new DevExpress.XtraEditors.TextEdit();
            this.txt评估 = new DevExpress.XtraEditors.TextEdit();
            this.txt客观资料 = new DevExpress.XtraEditors.TextEdit();
            this.txt处置计划 = new DevExpress.XtraEditors.TextEdit();
            this.txt接诊时间 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPanel1)).BeginInit();
            this.LayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主观资料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt评估.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt客观资料.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt处置计划.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Size = new System.Drawing.Size(123, 402);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(246, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 3;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(165, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 2;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(84, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 1;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn新增
            // 
            this.btn新增.Image = ((System.Drawing.Image)(resources.GetObject("btn新增.Image")));
            this.btn新增.Location = new System.Drawing.Point(3, 3);
            this.btn新增.Name = "btn新增";
            this.btn新增.Size = new System.Drawing.Size(75, 23);
            this.btn新增.TabIndex = 0;
            this.btn新增.Text = "添加";
            this.btn新增.Click += new System.EventHandler(this.btn新增_Click);
            // 
            // lab考核项
            // 
            this.lab考核项.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lab考核项.Location = new System.Drawing.Point(5, 30);
            this.lab考核项.Margin = new System.Windows.Forms.Padding(0);
            this.lab考核项.Name = "lab考核项";
            this.lab考核项.Size = new System.Drawing.Size(594, 26);
            this.lab考核项.StyleController = this.LayoutPanel1;
            this.lab考核项.TabIndex = 1;
            this.lab考核项.Text = "考核项：{0}    缺项：{1}     完整度：{2} % ";
            // 
            // LayoutPanel1
            // 
            this.LayoutPanel1.Controls.Add(this.txt姓名);
            this.LayoutPanel1.Controls.Add(this.txt个人档案号);
            this.LayoutPanel1.Controls.Add(this.txt接诊医生);
            this.LayoutPanel1.Controls.Add(this.lab考核项);
            this.LayoutPanel1.Controls.Add(this.lbl性别);
            this.LayoutPanel1.Controls.Add(this.lbl出生日期);
            this.LayoutPanel1.Controls.Add(this.lbl身份证号);
            this.LayoutPanel1.Controls.Add(this.lbl联系电话);
            this.LayoutPanel1.Controls.Add(this.lbl创建时间);
            this.LayoutPanel1.Controls.Add(this.lbl当前所属机构);
            this.LayoutPanel1.Controls.Add(this.lbl创建人);
            this.LayoutPanel1.Controls.Add(this.lbl最近更新时间);
            this.LayoutPanel1.Controls.Add(this.lbl创建机构);
            this.LayoutPanel1.Controls.Add(this.lbl最近修改人);
            this.LayoutPanel1.Controls.Add(this.lbl居住地址);
            this.LayoutPanel1.Controls.Add(this.txt主观资料);
            this.LayoutPanel1.Controls.Add(this.txt评估);
            this.LayoutPanel1.Controls.Add(this.txt客观资料);
            this.LayoutPanel1.Controls.Add(this.txt处置计划);
            this.LayoutPanel1.Controls.Add(this.txt接诊时间);
            this.LayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutPanel1.Location = new System.Drawing.Point(0, 28);
            this.LayoutPanel1.Name = "LayoutPanel1";
            this.LayoutPanel1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(704, 178, 250, 350);
            this.LayoutPanel1.Root = this.layoutControlGroup2;
            this.LayoutPanel1.Size = new System.Drawing.Size(604, 374);
            this.LayoutPanel1.TabIndex = 1;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(420, 60);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(179, 20);
            this.txt姓名.StyleController = this.LayoutPanel1;
            this.txt姓名.TabIndex = 56;
            // 
            // txt个人档案号
            // 
            this.txt个人档案号.Location = new System.Drawing.Point(116, 60);
            this.txt个人档案号.Name = "txt个人档案号";
            this.txt个人档案号.Size = new System.Drawing.Size(189, 20);
            this.txt个人档案号.StyleController = this.LayoutPanel1;
            this.txt个人档案号.TabIndex = 55;
            // 
            // txt接诊医生
            // 
            this.txt接诊医生.Location = new System.Drawing.Point(420, 168);
            this.txt接诊医生.Margin = new System.Windows.Forms.Padding(0);
            this.txt接诊医生.Name = "txt接诊医生";
            this.txt接诊医生.Size = new System.Drawing.Size(179, 20);
            this.txt接诊医生.StyleController = this.LayoutPanel1;
            this.txt接诊医生.TabIndex = 29;
            // 
            // lbl性别
            // 
            this.lbl性别.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl性别.Location = new System.Drawing.Point(116, 84);
            this.lbl性别.Name = "lbl性别";
            this.lbl性别.Size = new System.Drawing.Size(189, 24);
            this.lbl性别.StyleController = this.LayoutPanel1;
            this.lbl性别.TabIndex = 41;
            this.lbl性别.Text = "性别";
            // 
            // lbl出生日期
            // 
            this.lbl出生日期.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl出生日期.Location = new System.Drawing.Point(116, 112);
            this.lbl出生日期.Name = "lbl出生日期";
            this.lbl出生日期.Size = new System.Drawing.Size(189, 24);
            this.lbl出生日期.StyleController = this.LayoutPanel1;
            this.lbl出生日期.TabIndex = 42;
            this.lbl出生日期.Text = "出生日期";
            // 
            // lbl身份证号
            // 
            this.lbl身份证号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl身份证号.Location = new System.Drawing.Point(420, 84);
            this.lbl身份证号.Name = "lbl身份证号";
            this.lbl身份证号.Size = new System.Drawing.Size(179, 24);
            this.lbl身份证号.StyleController = this.LayoutPanel1;
            this.lbl身份证号.TabIndex = 44;
            this.lbl身份证号.Text = "身份证";
            // 
            // lbl联系电话
            // 
            this.lbl联系电话.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl联系电话.Location = new System.Drawing.Point(420, 112);
            this.lbl联系电话.Name = "lbl联系电话";
            this.lbl联系电话.Size = new System.Drawing.Size(179, 24);
            this.lbl联系电话.StyleController = this.LayoutPanel1;
            this.lbl联系电话.TabIndex = 45;
            this.lbl联系电话.Text = "联系电话";
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Location = new System.Drawing.Point(116, 288);
            this.lbl创建时间.Margin = new System.Windows.Forms.Padding(0);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(189, 20);
            this.lbl创建时间.StyleController = this.LayoutPanel1;
            this.lbl创建时间.TabIndex = 46;
            this.lbl创建时间.Text = "创建时间";
            // 
            // lbl当前所属机构
            // 
            this.lbl当前所属机构.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl当前所属机构.Location = new System.Drawing.Point(116, 312);
            this.lbl当前所属机构.Margin = new System.Windows.Forms.Padding(0);
            this.lbl当前所属机构.Name = "lbl当前所属机构";
            this.lbl当前所属机构.Size = new System.Drawing.Size(189, 20);
            this.lbl当前所属机构.StyleController = this.LayoutPanel1;
            this.lbl当前所属机构.TabIndex = 47;
            this.lbl当前所属机构.Text = "所属机构";
            // 
            // lbl创建人
            // 
            this.lbl创建人.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建人.Location = new System.Drawing.Point(116, 336);
            this.lbl创建人.Margin = new System.Windows.Forms.Padding(0);
            this.lbl创建人.Name = "lbl创建人";
            this.lbl创建人.Size = new System.Drawing.Size(189, 20);
            this.lbl创建人.StyleController = this.LayoutPanel1;
            this.lbl创建人.TabIndex = 48;
            this.lbl创建人.Text = "创建人";
            // 
            // lbl最近更新时间
            // 
            this.lbl最近更新时间.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl最近更新时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近更新时间.Location = new System.Drawing.Point(420, 288);
            this.lbl最近更新时间.Margin = new System.Windows.Forms.Padding(0);
            this.lbl最近更新时间.Name = "lbl最近更新时间";
            this.lbl最近更新时间.Size = new System.Drawing.Size(179, 20);
            this.lbl最近更新时间.StyleController = this.LayoutPanel1;
            this.lbl最近更新时间.TabIndex = 49;
            this.lbl最近更新时间.Text = "更新时间";
            // 
            // lbl创建机构
            // 
            this.lbl创建机构.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建机构.Location = new System.Drawing.Point(420, 312);
            this.lbl创建机构.Margin = new System.Windows.Forms.Padding(0);
            this.lbl创建机构.Name = "lbl创建机构";
            this.lbl创建机构.Size = new System.Drawing.Size(179, 20);
            this.lbl创建机构.StyleController = this.LayoutPanel1;
            this.lbl创建机构.TabIndex = 50;
            this.lbl创建机构.Text = "创建机构";
            // 
            // lbl最近修改人
            // 
            this.lbl最近修改人.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl最近修改人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl最近修改人.Location = new System.Drawing.Point(420, 336);
            this.lbl最近修改人.Margin = new System.Windows.Forms.Padding(0);
            this.lbl最近修改人.Name = "lbl最近修改人";
            this.lbl最近修改人.Size = new System.Drawing.Size(179, 20);
            this.lbl最近修改人.StyleController = this.LayoutPanel1;
            this.lbl最近修改人.TabIndex = 51;
            this.lbl最近修改人.Text = "修改人";
            // 
            // lbl居住地址
            // 
            this.lbl居住地址.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl居住地址.Location = new System.Drawing.Point(116, 140);
            this.lbl居住地址.Name = "lbl居住地址";
            this.lbl居住地址.Size = new System.Drawing.Size(483, 24);
            this.lbl居住地址.StyleController = this.LayoutPanel1;
            this.lbl居住地址.TabIndex = 52;
            this.lbl居住地址.Text = "居住地址";
            // 
            // txt主观资料
            // 
            this.txt主观资料.Location = new System.Drawing.Point(116, 192);
            this.txt主观资料.Margin = new System.Windows.Forms.Padding(0);
            this.txt主观资料.Name = "txt主观资料";
            this.txt主观资料.Size = new System.Drawing.Size(483, 20);
            this.txt主观资料.StyleController = this.LayoutPanel1;
            this.txt主观资料.TabIndex = 54;
            // 
            // txt评估
            // 
            this.txt评估.Location = new System.Drawing.Point(116, 216);
            this.txt评估.Name = "txt评估";
            this.txt评估.Size = new System.Drawing.Size(483, 20);
            this.txt评估.StyleController = this.LayoutPanel1;
            this.txt评估.TabIndex = 32;
            // 
            // txt客观资料
            // 
            this.txt客观资料.Location = new System.Drawing.Point(116, 240);
            this.txt客观资料.Margin = new System.Windows.Forms.Padding(0);
            this.txt客观资料.Name = "txt客观资料";
            this.txt客观资料.Size = new System.Drawing.Size(483, 20);
            this.txt客观资料.StyleController = this.LayoutPanel1;
            this.txt客观资料.TabIndex = 53;
            // 
            // txt处置计划
            // 
            this.txt处置计划.Location = new System.Drawing.Point(116, 264);
            this.txt处置计划.Name = "txt处置计划";
            this.txt处置计划.Size = new System.Drawing.Size(483, 20);
            this.txt处置计划.StyleController = this.LayoutPanel1;
            this.txt处置计划.TabIndex = 33;
            // 
            // txt接诊时间
            // 
            this.txt接诊时间.EditValue = null;
            this.txt接诊时间.Location = new System.Drawing.Point(116, 168);
            this.txt接诊时间.Margin = new System.Windows.Forms.Padding(0);
            this.txt接诊时间.Name = "txt接诊时间";
            this.txt接诊时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt接诊时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt接诊时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt接诊时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt接诊时间.Size = new System.Drawing.Size(189, 20);
            this.txt接诊时间.StyleController = this.LayoutPanel1;
            this.txt接诊时间.TabIndex = 28;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.CustomizationFormText = "接诊记录表";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem23,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem29,
            this.layoutControlItem32,
            this.layoutControlItem7,
            this.layoutControlItem22,
            this.layoutControlItem33,
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem6,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem27,
            this.layoutControlItem30,
            this.layoutControlItem28,
            this.layoutControlItem31,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(604, 374);
            this.layoutControlGroup2.Text = "接诊记录表";
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.lbl出生日期;
            this.layoutControlItem23.CustomizationFormText = "lbl出生日期item";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 82);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(115, 28);
            this.layoutControlItem23.Name = "lbl出生日期item";
            this.layoutControlItem23.Size = new System.Drawing.Size(304, 28);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "出生日期:";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.lbl身份证号;
            this.layoutControlItem25.CustomizationFormText = "lbl身份证号item";
            this.layoutControlItem25.Location = new System.Drawing.Point(304, 54);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(115, 28);
            this.layoutControlItem25.Name = "lbl身份证号item";
            this.layoutControlItem25.Size = new System.Drawing.Size(294, 28);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "身份证号:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.lbl联系电话;
            this.layoutControlItem26.CustomizationFormText = "lbl联系电话item";
            this.layoutControlItem26.Location = new System.Drawing.Point(304, 82);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(115, 28);
            this.layoutControlItem26.Name = "lbl联系电话item";
            this.layoutControlItem26.Size = new System.Drawing.Size(294, 28);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "联系电话:";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.lbl创建人;
            this.layoutControlItem29.CustomizationFormText = "lbl创建人item";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 306);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem29.Name = "lbl创建人item";
            this.layoutControlItem29.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "创建人:";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.lbl最近修改人;
            this.layoutControlItem32.CustomizationFormText = "lbl最近修改人item";
            this.layoutControlItem32.Location = new System.Drawing.Point(304, 306);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem32.Name = "lbl最近修改人item";
            this.layoutControlItem32.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "最近修改人:";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.lab考核项;
            this.layoutControlItem7.CustomizationFormText = "labelControl2item";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 30);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(116, 30);
            this.layoutControlItem7.Name = "labelControl2item";
            this.layoutControlItem7.Size = new System.Drawing.Size(598, 30);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "考核项";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.lbl性别;
            this.layoutControlItem22.CustomizationFormText = "lbl性别item";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 54);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(115, 28);
            this.layoutControlItem22.Name = "lbl性别item";
            this.layoutControlItem22.Size = new System.Drawing.Size(304, 28);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "性别:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.lbl居住地址;
            this.layoutControlItem33.CustomizationFormText = "lbl居住地址item";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 110);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 28);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(115, 28);
            this.layoutControlItem33.Name = "lbl居住地址item";
            this.layoutControlItem33.Size = new System.Drawing.Size(598, 28);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "居住地址:";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt接诊时间;
            this.layoutControlItem4.CustomizationFormText = "接诊时间";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 138);
            this.layoutControlItem4.Name = "dtp接诊时间item";
            this.layoutControlItem4.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem4.Tag = "check";
            this.layoutControlItem4.Text = "接诊时间:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt接诊医生;
            this.layoutControlItem1.CustomizationFormText = "接诊医生";
            this.layoutControlItem1.Location = new System.Drawing.Point(304, 138);
            this.layoutControlItem1.Name = "txt接诊医生item";
            this.layoutControlItem1.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem1.Tag = "check";
            this.layoutControlItem1.Text = "接诊医生:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt主观资料;
            this.layoutControlItem6.CustomizationFormText = "就诊者的主观资料";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 162);
            this.layoutControlItem6.Name = "memoEdit1item";
            this.layoutControlItem6.Size = new System.Drawing.Size(598, 24);
            this.layoutControlItem6.Tag = "check";
            this.layoutControlItem6.Text = "就诊者的主观资料:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt客观资料;
            this.layoutControlItem5.CustomizationFormText = "就诊者的客观资料";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 210);
            this.layoutControlItem5.Name = "txt就诊者的客观资料item";
            this.layoutControlItem5.Size = new System.Drawing.Size(598, 24);
            this.layoutControlItem5.Tag = "check";
            this.layoutControlItem5.Text = "就诊者的客观资料:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt处置计划;
            this.layoutControlItem3.CustomizationFormText = "处置计划";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 234);
            this.layoutControlItem3.Name = "txt处置计划item";
            this.layoutControlItem3.Size = new System.Drawing.Size(598, 24);
            this.layoutControlItem3.Tag = "check";
            this.layoutControlItem3.Text = "处置计划:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.lbl创建时间;
            this.layoutControlItem27.CustomizationFormText = "lbl创建时间item";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 258);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem27.Name = "lbl创建时间item";
            this.layoutControlItem27.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "创建时间:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.lbl最近更新时间;
            this.layoutControlItem30.CustomizationFormText = "lbl最近更新时间item";
            this.layoutControlItem30.Location = new System.Drawing.Point(304, 258);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem30.Name = "lbl最近更新时间item";
            this.layoutControlItem30.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "最近更新时间:";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.lbl当前所属机构;
            this.layoutControlItem28.CustomizationFormText = "lbl当前所属机构item";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 282);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem28.Name = "lbl当前所属机构item";
            this.layoutControlItem28.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "当前所属机构:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.lbl创建机构;
            this.layoutControlItem31.CustomizationFormText = "lbl创建机构item";
            this.layoutControlItem31.Location = new System.Drawing.Point(304, 282);
            this.layoutControlItem31.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem31.Name = "lbl创建机构item";
            this.layoutControlItem31.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "创建机构:";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt评估;
            this.layoutControlItem2.CustomizationFormText = "评估";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 186);
            this.layoutControlItem2.Name = "txt评估item";
            this.layoutControlItem2.Size = new System.Drawing.Size(598, 24);
            this.layoutControlItem2.Tag = "check";
            this.layoutControlItem2.Text = "评估:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt个人档案号;
            this.layoutControlItem8.CustomizationFormText = "个人档案号:";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(304, 24);
            this.layoutControlItem8.Text = "个人档案号:";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(108, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt姓名;
            this.layoutControlItem9.CustomizationFormText = "姓名:";
            this.layoutControlItem9.Location = new System.Drawing.Point(304, 30);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(294, 24);
            this.layoutControlItem9.Text = "姓名:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(108, 14);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 330);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(598, 13);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.btn新增);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(604, 28);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LayoutPanel1);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(123, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(604, 402);
            this.panel1.TabIndex = 3;
            // 
            // UC接诊记录表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.panel1);
            this.Name = "UC接诊记录表_显示";
            this.Size = new System.Drawing.Size(727, 402);
            this.Load += new System.EventHandler(this.UC接诊记录表_显示_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPanel1)).EndInit();
            this.LayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt主观资料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt评估.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt客观资料.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt处置计划.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt接诊时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn新增;
        private DevExpress.XtraEditors.LabelControl lab考核项;
        private DevExpress.XtraEditors.TextEdit txt接诊医生;
        private DevExpress.XtraEditors.LabelControl lbl性别;
        private DevExpress.XtraEditors.LabelControl lbl出生日期;
        private DevExpress.XtraEditors.LabelControl lbl身份证号;
        private DevExpress.XtraEditors.LabelControl lbl联系电话;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraEditors.LabelControl lbl当前所属机构;
        private DevExpress.XtraEditors.LabelControl lbl创建人;
        private DevExpress.XtraEditors.LabelControl lbl最近更新时间;
        private DevExpress.XtraEditors.LabelControl lbl创建机构;
        private DevExpress.XtraEditors.LabelControl lbl最近修改人;
        private DevExpress.XtraEditors.LabelControl lbl居住地址;
        private DevExpress.XtraLayout.LayoutControl LayoutPanel1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.TextEdit txt主观资料;
        private DevExpress.XtraEditors.TextEdit txt评估;
        private DevExpress.XtraEditors.TextEdit txt客观资料;
        private DevExpress.XtraEditors.TextEdit txt处置计划;
        private DevExpress.XtraEditors.DateEdit txt接诊时间;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt个人档案号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    }
}
