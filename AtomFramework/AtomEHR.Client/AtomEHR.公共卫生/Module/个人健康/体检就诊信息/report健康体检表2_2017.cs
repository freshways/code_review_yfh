﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.公共卫生;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class report健康体检表2_2017 : DevExpress.XtraReports.UI.XtraReport
    {
        DataSet ds体检;
        bll健康体检 bll = new bll健康体检();
        public report健康体检表2_2017()
        {
            InitializeComponent();
        }

        public report健康体检表2_2017(string docNo, string date)
        {
            InitializeComponent();
            ds体检 = bll.GetReportDataByKey(docNo, date);
            BindData(ds体检);
        }

        private void BindData(DataSet ds体检)
        {
            DataTable tb健康档案 = ds体检.Tables[tb_健康档案.__TableName];
            DataTable tb健康体检 = ds体检.Tables[tb_健康体检.__TableName];
            DataTable tb健康状态 = ds体检.Tables[tb_健康档案_健康状态.__TableName];
            DataTable tb住院史 = ds体检.Tables["住院史"];
            DataTable tb家庭病床史 = ds体检.Tables["家庭病床史"];

            if (tb健康体检.Rows.Count == 1 && tb健康档案.Rows.Count == 1)
            {
                //口腔
                this.xrLabel_口唇.Text = tb健康体检.Rows[0][tb_健康体检.口唇].ToString();
                //齿列
                string 齿列 = tb健康体检.Rows[0][tb_健康体检.齿列].ToString();
                string[] 齿列s = 齿列.Split(',');
                for (int i = 0; i < 齿列s.Length; i++)
                {
                    if (string.IsNullOrEmpty(齿列s[i]))
                    {
                        continue;
                    }

                    if (i > 1) break;

                    string xrName = "xrLabel_齿列" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = 齿列s[i];
                }
                if (tb健康体检.Rows[0][tb_健康体检.齿列缺齿] != null && !string.IsNullOrEmpty(tb健康体检.Rows[0][tb_健康体检.齿列缺齿].ToString()))
                {
                    string[] 缺齿 = tb健康体检.Rows[0][tb_健康体检.齿列缺齿].ToString().Split('@');

                    this.xrTable_缺齿1.Text = 缺齿[0];
                    this.xrTable_缺齿2.Text = 缺齿[1];
                    this.xrTable_缺齿3.Text = 缺齿[2];
                    this.xrTable_缺齿4.Text = 缺齿[3];
                }
                if (tb健康体检.Rows[0][tb_健康体检.齿列龋齿] != null && !string.IsNullOrEmpty(tb健康体检.Rows[0][tb_健康体检.齿列龋齿].ToString()))
                {
                    string[] 龋齿 = tb健康体检.Rows[0][tb_健康体检.齿列龋齿].ToString().Split('@');

                    this.xrTable_龋齿1.Text = 龋齿[0];
                    this.xrTable_龋齿2.Text = 龋齿[1];
                    this.xrTable_龋齿3.Text = 龋齿[2];
                    this.xrTable_龋齿4.Text = 龋齿[3];
                }
                if (tb健康体检.Rows[0][tb_健康体检.齿列义齿] != null && !string.IsNullOrEmpty(tb健康体检.Rows[0][tb_健康体检.齿列义齿].ToString()))
                {
                    string[] 义齿 = tb健康体检.Rows[0][tb_健康体检.齿列义齿].ToString().Split('@');

                    this.xrTable_义齿1.Text = 义齿[0];
                    this.xrTable_义齿2.Text = 义齿[1];
                    this.xrTable_义齿3.Text = 义齿[2];
                    this.xrTable_义齿4.Text = 义齿[3];
                }
                //咽部
                this.xrLabel_咽部.Text = tb健康体检.Rows[0][tb_健康体检.咽部].ToString();
                //视力
                this.xrLabel_视力左眼.Text = tb健康体检.Rows[0][tb_健康体检.左眼视力].ToString();
                this.xrLabel_视力右眼.Text = tb健康体检.Rows[0][tb_健康体检.右眼视力].ToString();
                this.xrLabel_矫正视力左眼.Text = tb健康体检.Rows[0][tb_健康体检.左眼矫正].ToString();
                this.xrLabel_矫正视力右眼.Text = tb健康体检.Rows[0][tb_健康体检.右眼矫正].ToString();
                //听力
                this.xrLabel_听力.Text = tb健康体检.Rows[0][tb_健康体检.听力].ToString();
                //运动能力
                this.xrLabel_运动能力.Text = tb健康体检.Rows[0][tb_健康体检.运动功能].ToString();
                //眼底
                string 眼底 = tb健康体检.Rows[0][tb_健康体检.眼底].ToString();
                this.xrLabel_眼底.Text = 眼底 == "3" ? "" : 眼底;
                this.xrLabel_眼底异常.Text = tb健康体检.Rows[0][tb_健康体检.眼底异常].ToString();
                //皮肤
                string 皮肤 = tb健康体检.Rows[0][tb_健康体检.皮肤].ToString();
                string[] 皮肤s = 皮肤.Split(',');
                this.xrLabel_皮肤.Text = 皮肤s[0] == "99" ? "7" : 皮肤s[0];
                this.xrLabel_皮肤其他.Text = tb健康体检.Rows[0][tb_健康体检.皮肤其他].ToString();
                //巩膜
                string 巩膜 = tb健康体检.Rows[0][tb_健康体检.巩膜].ToString();
                string[] 巩膜s = 巩膜.Split(',');
                this.xrLabel_巩膜.Text = 巩膜s[0] == "99" ? "4" : 巩膜s[0];
                this.xrLabel_巩膜其他.Text = tb健康体检.Rows[0][tb_健康体检.巩膜其他].ToString();
                //淋巴结
                string 淋巴结 = tb健康体检.Rows[0][tb_健康体检.淋巴结].ToString();
                string[] 淋巴结s = 淋巴结.Split(',');
                this.xrLabel_淋巴结.Text = 淋巴结s[0];
                this.xrLabel_淋巴结其他.Text = tb健康体检.Rows[0][tb_健康体检.淋巴结其他].ToString();
                //桶状胸
                this.xrLabel_桶状胸.Text = tb健康体检.Rows[0][tb_健康体检.桶状胸].ToString() == "2" ? "1" : "2";
                //呼吸音
                this.xrLabel_呼吸音.Text = tb健康体检.Rows[0][tb_健康体检.呼吸音].ToString();
                this.xrLabel_呼吸音异常.Text = tb健康体检.Rows[0][tb_健康体检.呼吸音异常].ToString();
                //罗音
                string 罗音 = tb健康体检.Rows[0][tb_健康体检.罗音].ToString();
                string[] 罗音s = 罗音.Split(',');
                this.xrLabel_罗音.Text = 罗音s[0];
                this.xrLabel_罗音其他.Text = tb健康体检.Rows[0][tb_健康体检.罗音异常].ToString();
                //心率
                this.xrLabel_心率.Text = tb健康体检.Rows[0][tb_健康体检.心率].ToString();
                //心律
                this.xrLabel_心律.Text = tb健康体检.Rows[0][tb_健康体检.心律].ToString();
                //杂音
                this.xrLabel_杂音.Text = tb健康体检.Rows[0][tb_健康体检.杂音].ToString();
                this.xrLabel_杂音有.Text = tb健康体检.Rows[0][tb_健康体检.杂音有].ToString();
                //压痛
                this.xrLabel_压痛.Text = tb健康体检.Rows[0][tb_健康体检.压痛].ToString();
                this.xrLabel_压痛有.Text = tb健康体检.Rows[0][tb_健康体检.压痛有].ToString();
                //包块
                this.xrLabel_包块.Text = tb健康体检.Rows[0][tb_健康体检.包块].ToString();
                this.xrLabel_包块有.Text = tb健康体检.Rows[0][tb_健康体检.包块有].ToString();
                //肝大
                this.xrLabel_肝大.Text = tb健康体检.Rows[0][tb_健康体检.肝大].ToString();
                this.xrLabel_肝大有.Text = tb健康体检.Rows[0][tb_健康体检.肝大有].ToString();
                //脾大
                this.xrLabel_脾大.Text = tb健康体检.Rows[0][tb_健康体检.脾大].ToString();
                this.xrLabel_脾大有.Text = tb健康体检.Rows[0][tb_健康体检.脾大有].ToString();
                //移动性浊音
                this.xrLabel_移动性浊音.Text = tb健康体检.Rows[0][tb_健康体检.浊音].ToString();
                this.xrLabel_移动性浊音有.Text = tb健康体检.Rows[0][tb_健康体检.浊音有].ToString();
                //下肢水肿
                this.xrLabel_下肢水肿.Text = tb健康体检.Rows[0][tb_健康体检.下肢水肿].ToString();
                //足背动脉搏动
                this.xrLabel_足背动脉搏动.Text = tb健康体检.Rows[0][tb_健康体检.足背动脉搏动].ToString();
                //肛门指诊
                string 肛门指诊 = tb健康体检.Rows[0][tb_健康体检.肛门指诊].ToString();
                string[] 肛门指诊s = 肛门指诊.Split(',');
                this.xrLabel_肛门指诊.Text = 肛门指诊s[0] == "99" ? "5" : 肛门指诊s[0];
                this.xrLabel_肛门指诊其他.Text = tb健康体检.Rows[0][tb_健康体检.肛门指诊异常].ToString();
                //乳腺
                string 乳腺 = tb健康体检.Rows[0][tb_健康体检.乳腺].ToString();
                string[] 乳腺s = 乳腺.Split(',');
                for (int i = 0; i < 乳腺s.Length; i++)
                {
                    if (string.IsNullOrEmpty(乳腺s[i]))
                    {
                        continue;
                    }

                    if (i > 3) break;

                    string xrName = "xrLabel_乳腺" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = 乳腺s[i] == "99" ? "5" : 乳腺s[i];
                }
                this.xrLabel_乳腺其他.Text = tb健康体检.Rows[0][tb_健康体检.乳腺其他].ToString();
                //外阴
                this.xrLabel_外阴.Text = tb健康体检.Rows[0][tb_健康体检.外阴].ToString();
                this.xrLabel_外阴异常.Text = tb健康体检.Rows[0][tb_健康体检.外阴异常].ToString();
                //阴道
                this.xrLabel_阴道.Text = tb健康体检.Rows[0][tb_健康体检.阴道].ToString();
                this.xrLabel_阴道异常.Text = tb健康体检.Rows[0][tb_健康体检.阴道异常].ToString();
                //宫颈
                this.xrLabel_宫颈.Text = tb健康体检.Rows[0][tb_健康体检.宫颈].ToString();
                this.xrLabel_宫颈异常.Text = tb健康体检.Rows[0][tb_健康体检.宫颈异常].ToString();
                //宫体
                this.xrLabel_宫体.Text = tb健康体检.Rows[0][tb_健康体检.宫体].ToString();
                this.xrLabel_宫体异常.Text = tb健康体检.Rows[0][tb_健康体检.宫体异常].ToString();
                //附件
                this.xrLabel_附件.Text = tb健康体检.Rows[0][tb_健康体检.附件].ToString();
                this.xrLabel_附件异常.Text = tb健康体检.Rows[0][tb_健康体检.附件异常].ToString();
                //查体其他
                this.xrTable_查体其他.Text = tb健康体检.Rows[0][tb_健康体检.查体其他].ToString();
                //ABO
                string 血型_ABO = tb健康状态.Rows[0][tb_健康档案_健康状态.血型].ToString();
                switch (血型_ABO)
                {
                    case "1":
                        this.xrLabel_ABO.Text = "A";
                        break;
                    case "2":
                        this.xrLabel_ABO.Text = "AB";
                        break;
                    case "3":
                        this.xrLabel_ABO.Text = "B";
                        break;
                    case "4":
                        this.xrLabel_ABO.Text = "O";
                        break;
                    case "5":
                        this.xrLabel_ABO.Text = "不祥";
                        break;
                    default:
                        this.xrLabel_ABO.Text = "";
                        break;
                }
                //Rh
                string 血型_Rh = tb健康状态.Rows[0][tb_健康档案_健康状态.RH].ToString();
                switch (血型_Rh)
                {
                    case "1":
                        this.xrLabel_Rh.Text = "RH阳性";
                        break;
                    case "2":
                        this.xrLabel_Rh.Text = "RH阴性";
                        break;
                    case "3":
                        this.xrLabel_Rh.Text = "不祥";
                        break;
                    default:
                        this.xrLabel_Rh.Text = "";
                        break;
                }
                //血红蛋白
                this.xrLabel_血红蛋白.Text = tb健康体检.Rows[0][tb_健康体检.血红蛋白].ToString();
                //白细胞
                this.xrLabel_白细胞.Text = tb健康体检.Rows[0][tb_健康体检.白细胞].ToString();
                //血小板
                this.xrLabel_血小板.Text = tb健康体检.Rows[0][tb_健康体检.血小板].ToString();
                //血常规其他
                this.xrLabel_血常规其他.Text = tb健康体检.Rows[0][tb_健康体检.血常规其他].ToString();
                //尿蛋白
                this.xrLabel_尿蛋白.Text = tb健康体检.Rows[0][tb_健康体检.尿蛋白].ToString();
                //尿糖
                this.xrLabel_尿糖.Text = tb健康体检.Rows[0][tb_健康体检.尿糖].ToString();
                //尿酮体
                this.xrLabel_尿酮体.Text = tb健康体检.Rows[0][tb_健康体检.尿酮体].ToString();
                //尿潜血
                this.xrLabel_尿潜血.Text = tb健康体检.Rows[0][tb_健康体检.尿潜血].ToString();
                //尿常规其他
                this.xrLabel_尿常规其他.Text = tb健康体检.Rows[0][tb_健康体检.尿常规其他].ToString();
                //空腹血糖
                this.xrLabel_空腹血糖1.Text = tb健康体检.Rows[0][tb_健康体检.空腹血糖].ToString();
                this.xrLabel_空腹血糖2.Text = tb健康体检.Rows[0][tb_健康体检.餐后2H血糖].ToString();
                //同型半胱氨酸
                this.xrLabel_同型半胱氨酸.Text = tb健康体检.Rows[0][tb_健康体检.同型半胱氨酸].ToString();
                //尿微量白蛋白
                this.xrLabel_尿微量白蛋白.Text = tb健康体检.Rows[0][tb_健康体检.尿微量白蛋白].ToString();
                //大便潜血
                string 大便潜血 = tb健康体检.Rows[0][tb_健康体检.大便潜血].ToString();
                this.xrLabel_大便潜血.Text = 大便潜血 == "3" ? "" : 大便潜血;
                //糖化血红蛋白
                this.xrLabel_糖化血红蛋白.Text = tb健康体检.Rows[0][tb_健康体检.糖化血红蛋白].ToString();
                //乙型肝炎表面抗原
                string 乙型肝炎表面抗原 = tb健康体检.Rows[0][tb_健康体检.乙型肝炎表面抗原].ToString();
                xrLabel_乙型肝炎表面抗原.Text = 乙型肝炎表面抗原 == "3" ? "" : 乙型肝炎表面抗原;
                //血清谷丙转氨酶
                this.xrLabel_血清谷丙转氨酶.Text = tb健康体检.Rows[0][tb_健康体检.血清谷丙转氨酶].ToString();
                //血清谷草转氨酶
                this.xrLabel_血清谷草转氨酶.Text = tb健康体检.Rows[0][tb_健康体检.血清谷草转氨酶].ToString();
                //白蛋白
                this.xrLabel_白蛋白.Text = tb健康体检.Rows[0][tb_健康体检.白蛋白].ToString();
                //总胆红素
                this.xrLabel_总胆红素.Text = tb健康体检.Rows[0][tb_健康体检.总胆红素].ToString();
                //结合胆红素
                this.xrLabel_综合胆红素.Text = tb健康体检.Rows[0][tb_健康体检.结合胆红素].ToString();
                //血清肌酐
                this.xrLabel_血清肌酐.Text = tb健康体检.Rows[0][tb_健康体检.血清肌酐].ToString();
                //血尿素
                this.xrLabel_血尿素.Text = tb健康体检.Rows[0][tb_健康体检.血尿素氮].ToString();
                //血钾浓度
                this.xrLabel_血钾浓度.Text = tb健康体检.Rows[0][tb_健康体检.血钾浓度].ToString();
                //血钠浓度
                this.xrLabel_血钠浓度.Text = tb健康体检.Rows[0][tb_健康体检.血钠浓度].ToString();
                //总胆固醇
                this.xrLabel_总胆固醇.Text = tb健康体检.Rows[0][tb_健康体检.总胆固醇].ToString();
                //甘油三酯
                this.xrLabel_甘油三酯.Text = tb健康体检.Rows[0][tb_健康体检.甘油三酯].ToString();
                //血清低密度脂蛋白胆固醇
                this.xrLabel_血清低密度脂蛋白胆固醇.Text = tb健康体检.Rows[0][tb_健康体检.血清低密度脂蛋白胆固醇].ToString();
                //血清高密度脂蛋白胆固醇
                this.xrLabel_血清高密度脂蛋白胆固醇.Text = tb健康体检.Rows[0][tb_健康体检.血清高密度脂蛋白胆固醇].ToString();
                //心电图
                string 心电图 = tb健康体检.Rows[0][tb_健康体检.心电图].ToString();
                string[] 心电图s = 心电图.Split(',');
                for (int i = 0; i < 心电图s.Length; i++)
                {
                    if (string.IsNullOrEmpty(心电图s[i]))
                    {
                        continue;
                    }

                    if (i > 5) break;

                    string result_心电图 = 心电图s[i] == "99" ? "9" : 心电图s[i];
                    string xrName = "xrLabel_心电图" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_心电图;
                }
                this.xrLabel_心电图其他.Text = tb健康体检.Rows[0][tb_健康体检.心电图异常].ToString();
                //胸部X线片
                string 胸部X线片 = tb健康体检.Rows[0][tb_健康体检.胸部X线片].ToString();
                this.xrLabel_胸部X线片.Text = 胸部X线片 == "3" ? "" : 胸部X线片;
                this.xrLabel_胸部X线片异常.Text = tb健康体检.Rows[0][tb_健康体检.胸部X线片异常].ToString();
                //B超
                string 腹部B超 = tb健康体检.Rows[0][tb_健康体检.B超].ToString();
                this.xrLabel_腹部B超.Text = 腹部B超 == "3" ? "" : 腹部B超;
                this.xrLabel_腹部B超异常.Text = tb健康体检.Rows[0][tb_健康体检.B超其他].ToString();
                string B超其他 = tb健康体检.Rows[0][tb_健康体检.其他B超].ToString();
                this.xrLabel_B超其他.Text = B超其他 == "3" ? "" : B超其他;
                this.xrLabel_B超其他异常.Text = tb健康体检.Rows[0][tb_健康体检.其他B超异常].ToString();
                //宫颈涂片
                string 宫颈涂片 = tb健康体检.Rows[0][tb_健康体检.宫颈涂片].ToString();
                this.xrLabel_宫颈涂片.Text = 宫颈涂片 == "3" ? "" : 宫颈涂片;
                this.xrLabel_宫颈涂片异常.Text = tb健康体检.Rows[0][tb_健康体检.宫颈涂片异常].ToString();
                //辅助检查其他
                this.xrTable_辅助检查其他.Text = tb健康体检.Rows[0][tb_健康体检.辅助检查其他].ToString();
                //脑血管疾病
                string 脑血管疾病 = tb健康体检.Rows[0][tb_健康体检.脑血管疾病].ToString();
                string[] 脑血管疾病s = 脑血管疾病.Split(',');
                for (int i = 0; i < 脑血管疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(脑血管疾病s[i])) continue;

                    if (i > 4) break;

                    string result_脑血管疾病 = 脑血管疾病s[i] == "99" ? "6" : 脑血管疾病s[i];
                    string xrName = "xrLabel_脑血管疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_脑血管疾病;
                }
                this.xrLabel_脑血管疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.脑血管疾病其他].ToString();
                //肾脏疾病
                string 肾脏疾病 = tb健康体检.Rows[0][tb_健康体检.肾脏疾病].ToString();
                string[] 肾脏疾病s = 肾脏疾病.Split(',');
                for (int i = 0; i < 肾脏疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(肾脏疾病s[i])) continue;

                    if (i > 4) break;

                    string result_肾脏疾病 = 肾脏疾病s[i] == "99" ? "6" : 肾脏疾病s[i];
                    string xrName = "xrLabel_肾脏疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_肾脏疾病;
                }
                this.xrLabel_肾脏疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.肾脏疾病其他].ToString();
                //心血管疾病
                string 心血管疾病 = tb健康体检.Rows[0][tb_健康体检.心脏疾病].ToString();
                string[] 心血管疾病s = 心血管疾病.Split(',');
                for (int i = 0; i < 心血管疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(心血管疾病s[i])) continue;

                    if (i > 4) break;

                    string result_心血管疾病 = 心血管疾病s[i] == "99" ? "10" : 心血管疾病s[i];
                    string xrName = "xrLabel_心血管疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_心血管疾病;
                }
                this.xrLabel_心血管疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.心脏疾病其他].ToString();
                //眼部疾病
                string 眼部疾病 = tb健康体检.Rows[0][tb_健康体检.眼部疾病].ToString();
                string[] 眼部疾病s = 眼部疾病.Split(',');
                for (int i = 0; i < 眼部疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(眼部疾病s[i])) continue;

                    if (i > 2) break;

                    string result_眼部疾病 = 眼部疾病s[i] == "99" ? "5" : 眼部疾病s[i];
                    string xrName = "xrLabel_眼部疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_眼部疾病;
                }
                this.xrLabel_眼部疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.眼部疾病其他].ToString();
                //神经系统其他疾病
                string 神经系统其他疾病 = tb健康体检.Rows[0][tb_健康体检.神经系统疾病].ToString();
                string[] 神经系统其他疾病s = 神经系统其他疾病.Split(',');
                for (int i = 0; i < 神经系统其他疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(神经系统其他疾病s[i])) continue;

                    if (i > 2) break;

                    string result_神经系统其他疾病 = 神经系统其他疾病s[i] == "99" ? "4" : 神经系统其他疾病s[i];
                    string xrName = "xrLabel_神经系统其他疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_神经系统其他疾病;
                }
                this.xrLabel_神经系统其他疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.神经系统疾病其他].ToString();
                //其他系统疾病
                string 其他系统疾病 = tb健康体检.Rows[0][tb_健康体检.其他系统疾病].ToString();
                string[] 其他系统疾病s = 其他系统疾病.Split(',');
                for (int i = 0; i < 其他系统疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(其他系统疾病s[i])) continue;

                    if (i > 2) break;

                    string result_其他系统疾病 = 其他系统疾病s[i] == "99" ? "7" : 其他系统疾病s[i];
                    string xrName = "xrLabel_其他系统疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_其他系统疾病;
                }
                this.xrLabel_其他系统疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.其他系统疾病其他].ToString();
                //住院史
                if (tb住院史 != null && tb住院史.Rows.Count>0)
                {
                    for (int i = 0; i < tb住院史.Rows.Count; i++)
                    {
                        if (i > 1) break;

                        string 入院日期 = "xrTable_入院日期" + (i + 1);
                        string 出院日期 = "xrTable_出院日期" + (i + 1);
                        string 原因 = "xrTable_住院史原因" + (i + 1);
                        string 医疗机构及科室 = "xrTable_住院史机构及科室" + (i + 1);
                        string 病案号 = "xrTable_住院史病案号" + (i + 1);

                        XRTableCell xr入院日期 = (XRTableCell)FindControl(入院日期, false);
                        XRTableCell xr出院日期 = (XRTableCell)FindControl(出院日期, false);
                        XRTableCell xr原因 = (XRTableCell)FindControl(原因, false);
                        XRTableCell xr医疗机构及科室 = (XRTableCell)FindControl(医疗机构及科室, false);
                        XRTableCell xr病案号 = (XRTableCell)FindControl(病案号, false);

                        xr入院日期.Text = tb住院史.Rows[0][tb_健康体检_住院史.入院日期].ToString().Substring(0, 10);
                        xr出院日期.Text = tb住院史.Rows[0][tb_健康体检_住院史.出院日期].ToString().Substring(0, 10);
                        xr原因.Text = tb住院史.Rows[0][tb_健康体检_住院史.原因].ToString();
                        xr医疗机构及科室.Text = tb住院史.Rows[0][tb_健康体检_住院史.医疗机构名称].ToString();
                        xr病案号.Text = tb住院史.Rows[0][tb_健康体检_住院史.病案号].ToString();
                    }
                }
                //家庭病床史
                if (tb家庭病床史 != null && tb家庭病床史.Rows.Count > 0)
                {
                    for (int i = 0; i < tb家庭病床史.Rows.Count; i++)
                    {
                        if (i > 1) break;

                        string 建床日期 = "xrTable_建床日期" + (i + 1);
                        string 撤床日期 = "xrTable_撤床日期" + (i + 1);
                        string 原因 = "xrTable_家庭病床史原因" + (i + 1);
                        string 医疗机构及科室 = "xrTable_家庭病床史机构及科室" + (i + 1);
                        string 病案号 = "xrTable_家庭病床史病案号" + (i + 1);

                        XRTableCell xr建床日期 = (XRTableCell)FindControl(建床日期, false);
                        XRTableCell xr撤床日期 = (XRTableCell)FindControl(撤床日期, false);
                        XRTableCell xr原因 = (XRTableCell)FindControl(原因, false);
                        XRTableCell xr医疗机构及科室 = (XRTableCell)FindControl(医疗机构及科室, false);
                        XRTableCell xr病案号 = (XRTableCell)FindControl(病案号, false);

                        xr建床日期.Text = tb家庭病床史.Rows[0][tb_健康体检_住院史.入院日期].ToString().Substring(0, 10);
                        xr撤床日期.Text = tb家庭病床史.Rows[0][tb_健康体检_住院史.出院日期].ToString().Substring(0, 10);
                        xr原因.Text = tb家庭病床史.Rows[0][tb_健康体检_住院史.原因].ToString();
                        xr医疗机构及科室.Text = tb家庭病床史.Rows[0][tb_健康体检_住院史.医疗机构名称].ToString();
                        xr病案号.Text = tb家庭病床史.Rows[0][tb_健康体检_住院史.病案号].ToString();
                    }
                }
            }
        }

    }
}
