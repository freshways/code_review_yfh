﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Models;
using AtomEHR.公共卫生.util;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{

    public partial class UC健康体检表 : UserControlBase
    {
        /*
     1.    症状选项对应的  code值   zz_zhengzhuang
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
                    10002498	zz_zhengzhuang 	症状	1	WZZ、MZZ	无症状		1
                    10002621	zz_zhengzhuang	症状	2	TT	头痛		1
                    10002624	zz_zhengzhuang	症状	3	TY	头晕		1
                    10002625	zz_zhengzhuang	症状	4	XJ	心悸		1
                    10002626	zz_zhengzhuang	症状	5	XM	胸闷		1
                    10002627	zz_zhengzhuang	症状	6	XT	胸痛		1
                    10002628	zz_zhengzhuang	症状	7	MXKS、MXHS	慢性咳嗽		1
                    10002629	zz_zhengzhuang	症状	8	KT、HT	咳痰		1
                    10003084	zz_zhengzhuang	症状	9	HXKN	呼吸困难		1
                    10003085	zz_zhengzhuang	症状	10	DY	多饮		1
                    10003086	zz_zhengzhuang	症状	11	DS、DN	多尿		1
                    10003087	zz_zhengzhuang	症状	12	TZXX、TCXX、TZXJ、	体重下降		1
                    10003088	zz_zhengzhuang	症状	13	FL	乏力		1
                    10003089	zz_zhengzhuang	症状	14	GJZT	关节肿痛		1
                    10003090	zz_zhengzhuang	症状	15	SLMH	视力模糊		1
                    10003091	zz_zhengzhuang	症状	16	SJMM	手脚麻木		1
                    10003092	zz_zhengzhuang	症状	17	SJ、NJ	尿急		1
                    10003093	zz_zhengzhuang	症状	18	ST、NT	尿痛		1
                    10003094	zz_zhengzhuang	症状	19	PM、BM、PL、BL、PB、	便秘		1
                    10003105	zz_zhengzhuang	症状	20	FX	腹泻		1
                    10003106	zz_zhengzhuang	症状	21	WXOT、EXOT	恶心呕吐		1
                    10003107	zz_zhengzhuang	症状	22	YH	眼花		1
                    10003108	zz_zhengzhuang	症状	23	EM	耳鸣		1
                    10003109	zz_zhengzhuang	症状	24	RFZT	乳房胀痛		1
                    10003110	zz_zhengzhuang	症状	99	QT、JT	其他		1
         * 
         * 
       2.  锻炼频率  dlpl
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003299	dlpl	锻炼频率	1		每天		1
            10003300	dlpl	锻炼频率	2		每周一次以上		1
            10003301	dlpl	锻炼频率	3		偶尔		1
            10003302	dlpl	锻炼频率	4		不锻炼		1
         * 
         * 
         *3. 饮食习惯  ysxg
         ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002888	ysxg	饮食习惯	5		嗜油		1
            10002889	ysxg	饮食习惯	6		嗜糖		1
            10002890	ysxg	饮食习惯	1		荤素均衡		1
            10002891	ysxg	饮食习惯	2		荤食为主		1
            10002892	ysxg	饮食习惯	3		素食为主		1
            10002893	ysxg	饮食习惯	4		嗜盐		1
         * 
         * 
         4.吸烟情况 
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003482	glkxyqk	管理卡吸烟情况	1		不吸烟		1
            10003483	glkxyqk	管理卡吸烟情况	2		已戒烟		1
            10003484	glkxyqk	管理卡吸烟情况	3		吸烟		1   
         * 
         5.饮酒频率  yjpl
         *ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002832	yjpl	饮酒频率	1		从不		1
            10002833	yjpl	饮酒频率	2		偶尔		1
            10002834	yjpl	饮酒频率	3		经常		1
            10002835	yjpl	饮酒频率	4		每天		1
         * 
         6.是否戒酒
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10003111	sfjj	是否戒酒	1	WJJ	未戒酒		1
            10003112	sfjj	是否戒酒	2	YJJ	已戒酒		1
         * 
         7.饮酒种类
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
        10002836	yjzl	饮酒种类	1		白酒		1
        10002837	yjzl	饮酒种类	2		啤酒		1
        10002838	yjzl	饮酒种类	3		红酒		1
        10002839	yjzl	饮酒种类	4		黄酒		1
        10002840	yjzl	饮酒种类	99		其他		1
         * 
         8.口唇
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002072	kc_houchun	口唇	1		红润		1
            10002073	kc_houchun	口唇	2		苍白		1
            10002074	kc_houchun	口唇	3		发绀		1
            10002075	kc_houchun	口唇	4		皲裂		1
            10002076	kc_houchun	口唇	5		疱疹		1
            10002077	kc_houchun	口唇	6	QT,JT	其他		1
         * 
         9.咽部
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002748	yb_yanbu	咽部	1		无充血		1
            10002749	yb_yanbu	咽部	2		充血		1
            10002750	yb_yanbu	咽部	3		淋巴滤泡增生		1
            10002751	yb_yanbu	咽部	4	QT,JT	其他		1
         * 
         10.听力
         * ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
            10002583	tingli	听力	1	TJ、TX	听见		1
            10002584	tingli	听力	2	TBQHWFTJ（EBHKZK	听不清或无法听见（耳鼻喉科专科就诊）		1
         * 
         11.运动功能
            10002894	yundong	运动功能	1	KSLWC	可顺利完成		1
            10002895	yundong	运动功能	2	WFDLWCQZRHYGDZ(	无法独立完成其中任何一个动作(上级医院就诊)		1
         * 
         12.正常异常
            10002936	zcyc	正常异常	1		正常		1
            10002937	zcyc	正常异常	2		异常		1
         * 
        13.下肢水肿
            10002739	xzsz	下肢水肿	4		双侧不对称		1
            10002740	xzsz	下肢水肿	5		双侧对称		1
            10002736	xzsz	下肢水肿	1		无		1
            10002737	xzsz	下肢水肿	2		左侧		1
            10002738	xzsz	下肢水肿	3		右侧		1
         * 
         14.
            10002932	zbdmbd	足背动脉搏动	1		未触及		1
            10002933	zbdmbd	足背动脉搏动	2		触及双侧对称		1
            10002934	zbdmbd	足背动脉搏动	3		触及左侧弱或消失		1
            10002935	zbdmbd	足背动脉搏动	4		触及右侧弱或消失		1
         * 
         15.肛门指诊
            10002492	gmzz	肛门指诊	1	WJYC、WXYC	未见异常		1
            10002493	gmzz	肛门指诊	2	CT	触痛		1
            10003051	gmzz	肛门指诊	3	BK	包块		1
            10003052	gmzz	肛门指诊	4	QLXYC	前列腺异常		1
            10003053	gmzz	肛门指诊	99	QT、JT	其他		1
         * 
         16.乳腺
            10002447	rx_ruxian	乳腺	1		未见异常		1
            10002448	rx_ruxian	乳腺	2		乳房切除		1
            10002449	rx_ruxian	乳腺	3		异常泌乳		1
            10002450	rx_ruxian	乳腺	4		乳腺包块		1
            10002451	rx_ruxian	乳腺	99		其他		1
         * 
         17.阴性阳性
            10002905	yxyx	阴性阳性	1		阴性		1
            10002906	yxyx	阴性阳性	2		阳性		1
         *  1807	        yxyx	阴性阳性	3	   未检测	1
         * 
         18.中药体质辨识
            10003047	zytzbs	中药体质辨识	1		是		1
            10003048	zytzbs	中药体质辨识	2		倾向是		1
            10003049	zytzbsphz	中药体质辨识平和质	1		是		1
            10003050	zytzbsphz	中药体质辨识平和质	2		基本是		1
         * 
         19.脑血管疾病
            10002290	nxgjb	脑血管疾病	1		未发现		1
            10002291	nxgjb	脑血管疾病	2		缺血性卒中		1
            10002292	nxgjb	脑血管疾病	3		脑出血		1
            10002293	nxgjb	脑血管疾病	4		蛛网膜下腔出血		1
            10002294	nxgjb	脑血管疾病	5		短暂性脑缺血发作		1
            10002295	nxgjb	脑血管疾病	99		其他		1
         * 
         20.肾脏疾病
            10002564	szjb	肾脏疾病	1		未发现		1
            10002565	szjb	肾脏疾病	2		糖尿病肾病		1
            10002566	szjb	肾脏疾病	3		肾功能衰竭		1
            10002567	szjb	肾脏疾病	4		急性肾炎		1
            10002568	szjb	肾脏疾病	5		慢性肾炎		1
            10002569	szjb	肾脏疾病	99		其他		1
         * 
         21.心脏疾病
            10002725	xzjb	心脏疾病	1		未发现		1
            10002726	xzjb	心脏疾病	2		心肌梗死		1
            10002727	xzjb	心脏疾病	3		心绞痛		1
            10002728	xzjb	心脏疾病	4		冠状动脉血运重建		1
            10002729	xzjb	心脏疾病	5		充血性心力衰竭		1
            10002730	xzjb	心脏疾病	6		心前区疼痛		1
            10002731	xzjb	心脏疾病	99		其他		1
         * 
         22.血管疾病
            10002678	xgjb	血管疾病	1		未发现		1
            10002679	xgjb	血管疾病	2		夹层动脉瘤		1
            10002680	xgjb	血管疾病	3		动脉闭塞性疾病		1
            10002681	xgjb	血管疾病	99		其他		1
         * 
         23.眼部疾病
            10002752	ybjb	眼部疾病	1		未发现		1
            10002753	ybjb	眼部疾病	2		视网膜出血或渗出		1
            10002754	ybjb	眼部疾病	3		视乳头水肿		1
            10002755	ybjb	眼部疾病	4		白内障		1
            10002756	ybjb	眼部疾病	99		其他		1
         * 
         24.未发现有
            10002630	wfxy	未发现有	1		未发现		1
            10002631	wfxy	未发现有	2		有		1
         * 
         25.齿列
                10003219	cl_chilei	齿列	1		正常		1
                10003220	cl_chilei	齿列	2		缺齿		1
                10003221	cl_chilei	齿列	3		龋齿		1
                10003222	cl_chilei	齿列	4		义齿(假牙)		1
                10003223	cl_chilei	齿列	5	QT,JT	其他		1
         * 
         26.体检有无异常
                10002585	tjywyc	体检有无异常	1		体检无异常		1
                10002586	tjywyc	体检有无异常	2		有异常		1
         * 
         27.体检有无异常
                10002585	tjywyc	体检有无异常	1		体检无异常		1
                10002586	tjywyc	体检有无异常	2		有异常		1
         * 
         28.健康指导
                10001896	jkzd	健康指导	1		定期随访		1
                10001897	jkzd	健康指导	2		纳入慢性病管理		1
                10001898	jkzd	健康指导	3		建议复查		1
                10001899	jkzd	健康指导	4		建议转诊		1
         * 
         29.危险因素指导
                10002653	wxyszd	危险因素指导	1		戒烟		1
                10002654	wxyszd	危险因素指导	2		健康饮酒		1
                10002655	wxyszd	危险因素指导	3		饮食		1
                10002656	wxyszd	危险因素指导	4		锻炼		1
                10002657	wxyszd	危险因素指导	97		减体重		1
                10002658	wxyszd	危险因素指导	98		建议疫苗接种		1
                10002659	wxyszd	危险因素指导	99		其他		1
         * 
         30.老年人状况评估
                10002990	zk_pg	老年人状况评估	1	NULL	满意	NULL	1
                10002991	zk_pg	老年人状况评估	2	NULL	基本满意	NULL	1
                10002992	zk_pg	老年人状况评估	3	NULL	说不清楚	NULL	1
                10002993	zk_pg	老年人状况评估	4	NULL	不太满意	NULL	1
                10002994	zk_pg	老年人状况评估	5	NULL	不满意	NULL	1
         * 
         31.老年人自理评估
                10002995	zl_pg	老年人自理评估	1	NULL	可自理(0～3分)	NULL	1
                10002996	zl_pg	老年人自理评估	2	NULL	轻度依赖(4～8分)	NULL	1
                10002997	zl_pg	老年人自理评估	3	NULL	中度依赖(9～18分)	NULL	1
                10002998	zl_pg	老年人自理评估	4	NULL	不能自理(≥19分)	NULL	1
         * 
         32.阴性阳性
                10002905	yxyx	阴性阳性	1		阴性		1
                10002906	yxyx	阴性阳性	2		阳性		1
         * 
         33.职业暴露详细
                10003131	baoluqk	职业暴露详细	1	W,M	无		1
                10003132	baoluqk	职业暴露详细	2	BX	不详		1
                10003133	baoluqk	职业暴露详细	3	Y	有		1
         * 
         34. pifu_pf  皮肤
            10002382	pifu_pf	皮肤	1		正常		1
            10002383	pifu_pf	皮肤	2		潮红		1
            10002384	pifu_pf	皮肤	3		苍白		1
            10002385	pifu_pf	皮肤	4		发绀		1
            10002386	pifu_pf	皮肤	5		黄染		1
            10002387	pifu_pf	皮肤	6		色素沉着		1
            10002388	pifu_pf	皮肤	99		其他		1
         * 
         35.
            10003492	gm_gongmo	巩膜	1		正常		1
            10003493	gm_gongmo	巩膜	2		黄染		1
            10003494	gm_gongmo	巩膜	3		充血		1
            10003495	gm_gongmo	巩膜	99		其他		1
         * 
         36.lbj2
         * 10002110  	lbj2	淋巴结	1		未触及		1
            10002111	lbj2	淋巴结	2		锁骨上		1
            10002112	lbj2	淋巴结	3		腋窝		1
            10002113	lbj2	淋巴结	4		其他		1
         * 
         37 . luoyin
         * 10002145	    luoyin	罗音	1	W、M	无		1
            10002146	luoyin	罗音	2	GLY	干罗音		1
            10002619	luoyin	罗音	3	SLY	湿罗音		1
            10002620	luoyin	罗音	4	QT、JT	其他		1
         * 
         38.心脏心律
         * 10002741 	xzxl	心脏心律	1		齐		1
            10002742	xzxl	心脏心律	2		不齐		1
            10002743	xzxl	心脏心律	3		绝对不齐		1
         * 
         39.
         * 10002667 	wy_wuyou	无有	1		无		1
            10002668	wy_wuyou	无有	2		有		1
            10002896	yw_youwu	有无	1		有		1
            10002897	yw_youwu	有无	2		无		1
         * 
        40.
         * 10002898 	ywyc	有无异常	1		未见异常		1
            10002899	ywyc	有无异常	99		异常		1
         */

        #region Fields

        bll健康体检 _Bll = new bll健康体检();
        DataSet _ds健康体检;
        DataSet _ds健康档案ForShow;
        DataSet _ds健康档案OnlyCode;
        UpdateType _UpdateType = UpdateType.None;
        frm个人健康 _frm;
        string _serverDateTime;//获取当前服务器时间
        string _docNo;
        string _date;
        #endregion

        #region Constructor
        public UC健康体检表()
        {
            InitializeComponent();
        }
        public UC健康体检表(Form frm, UpdateType _updateType)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _date = _frm._param == null ? "" : _frm._param.ToString();
            if (_updateType == UpdateType.Modify)//表示是修改
            {
                _UpdateType = _updateType;
                _ds健康体检 = _Bll.GetUserValueByKey(_docNo, _date, true);

            }
            else if (_updateType == UpdateType.Add)//新增
            {
                _UpdateType = _updateType;
                _Bll.GetBusinessByKey(_docNo, true);//下载一个空业务单据
                _Bll.NewBusiness();
                _ds健康体检 = _Bll.CurrentBusiness;
            }
            bll健康档案 _bll健康档案 = new bll健康档案();
            _ds健康档案ForShow = _bll健康档案.GetOneUserByKeyForShow(_docNo, false);
            _ds健康档案OnlyCode = _bll健康档案.GetOneUserCodeByKey(_docNo, false);
            if (_ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows.Count == 1)
            {
                this.text个人档案编号.Text = _docNo;
                //重新绑定体检表联系电话
                string str联系电话 = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人电话].ToString();
                if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
                {
                    this.text联系电话.Text = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人电话].ToString();
                }
                else
                {
                    this.text联系电话.Text = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人电话].ToString();
                }
                //this.text联系电话.Text = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人电话].ToString();
                this.text身份证号.Text = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.身份证号].ToString();
                this.txt性别.Text = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.性别].ToString();
                this.text姓名.Text = util.DESEncrypt.DES解密(_ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.姓名].ToString());
                this.text出生日期.Text = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期].ToString();
                this.text居住地址.Text = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.市].ToString() + _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.区].ToString() + _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.街道].ToString() + _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居委会].ToString() + _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住地址].ToString();
                string 出生日期 = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期].ToString();
                if (!string.IsNullOrEmpty(出生日期))
                {
                    DateTime dt = DateTime.Now;
                    if (DateTime.TryParse(出生日期, out dt))
                    {
                        if (DateTime.Now.Year - dt.Year >= 65)
                        {
                            group老年人评估.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

                            #region 如果是老年人辅助检查考核项显示蓝色
                            this.lbl血清谷丙转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清谷丙转氨酶.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            this.lbl血清谷草转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清谷草转氨酶.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            this.lbl总胆红素.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl总胆红素.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            this.lbl血清肌酐.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清肌酐.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            this.lbl血尿素氮.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血尿素氮.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            this.lbl总胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl总胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            this.lbl甘油三酯.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl甘油三酯.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            this.lbl腹部B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl腹部B超.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            //this.lbl其他B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            //this.lbl其他B超.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                            #endregion
                        }
                        else
                        {
                            group老年人评估.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                        }
                    }
                }
                string 性别 = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.性别].ToString();
                if (性别 == "男")
                    this.group妇科.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                else
                    this.group妇科.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            this.text责任医生.Text = Loginer.CurrentUser.AccountName;//责任医生默认为登录人姓名
            this.cboABO.Text = _ds健康档案ForShow.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.血型].ToString() == null ? "" : _ds健康档案ForShow.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.血型].ToString();
            this.cboRh.Text = _ds健康档案ForShow.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.RH].ToString() == null ? "" : _ds健康档案ForShow.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.RH].ToString();

        }

        #endregion

        #region Handle Events
        private void UC健康体检表_Load(object sender, EventArgs e)
        {
            _serverDateTime = _Bll.ServiceDateTime;//获取当前服务器时间
            InitView();
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t血型, cboABO);
            cboABO.Properties.Items.RemoveAt(4);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.tRH, cboRh);
            DataBinder.BindingRadioEdit(radio是否戒酒, DataDictCache.Cache.t是否戒酒, "P_CODE");
            DataBinder.BindingLookupEditDataSource(lkp服药依从性, DataDictCache.Cache.t服药依从性, "P_DESC", "P_CODE");
            this.txt一般情况_身高.Txt1.Leave += 一般情况_身高_Leave;
            this.txt一般情况_体重.Txt1.Leave += 一般情况_体重_Leave;
            this.txt一般情况_体重指数.Txt1.Enter += 一般情况_体重指数_Enter;

            if (_UpdateType == UpdateType.Modify)//修改的时候绑定已存在的数据
            {
                DobindingDatasource(_ds健康体检);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.修改时间] = _serverDateTime;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.修改人] = Loginer.CurrentUser.用户编码;
            }
            else  //新增
            {

                if (_ds健康体检.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count > 0)//存在管理卡，为高血压患者
                    this.txt危险因素其他.Text = "低盐饮食";
                else
                    this.txt危险因素其他.Text = "减盐防控高血压";

                this.dte创建时间.EditValue = _serverDateTime;
                this.txt创建人.EditValue = Loginer.CurrentUser.AccountName;
                this.dte最近更新时间.EditValue = _serverDateTime;
                this.txt创建机构.EditValue = Loginer.CurrentUser.所属机构名称;
                this.txt当前所属机构.EditValue = Loginer.CurrentUser.所属机构名称;
                this.txt最近修改人.EditValue = Loginer.CurrentUser.AccountName;

                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.个人档案编号] = _docNo;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.创建时间] = _serverDateTime;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.创建人] = Loginer.CurrentUser.用户编码;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.创建机构] = Loginer.CurrentUser.所属机构;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.修改时间] = _serverDateTime;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.修改人] = Loginer.CurrentUser.用户编码;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.所属机构] = Loginer.CurrentUser.所属机构;
            }
            this.gc住院史.DataSource = _ds健康体检.Tables["住院史"];
            this.gc建床史.DataSource = _ds健康体检.Tables["家庭病床史"];
            this.gc用药情况.DataSource = _ds健康体检.Tables[tb_健康体检_用药情况表.__TableName];
            this.gc接种史.DataSource = _ds健康体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName];
            if (_ds健康体检.Tables["住院史"].Rows.Count > 0)
            {
                this.lblgc住院史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.radio住院史.EditValue = "1";
            }
            else
            {
                this.radio住院史.EditValue = "2";
            }
            if (_ds健康体检.Tables["家庭病床史"].Rows.Count > 0)
            {
                this.lblgc家庭病床史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.radio家庭病床史.EditValue = "1";
            }
            else
            {
                this.radio家庭病床史.EditValue = "2";
            }
            if (_ds健康体检.Tables[tb_健康体检_用药情况表.__TableName].Rows.Count > 0)
            {
                this.lblgc用药情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.radio主要用药情况.EditValue = "1";
            }
            else
            {
                this.radio主要用药情况.EditValue = "2";
            }
            if (_ds健康体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows.Count > 0)
            {
                this.lblgc接种史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.radio预防接种史.EditValue = "1";
            }
            else
            {
                this.radio预防接种史.EditValue = "2";
            }


        }
        void 一般情况_体重指数_Enter(object sender, EventArgs e)
        {
            Decimal 身高 = 0;
            Decimal 体重 = 0;
            Decimal BMI = 0;
            if (string.IsNullOrEmpty(this.txt一般情况_身高.Txt1.Text) || string.IsNullOrEmpty(this.txt一般情况_体重.Txt1.Text)) return;
            if (Decimal.TryParse(this.txt一般情况_身高.Txt1.Text, out 身高) && Decimal.TryParse(this.txt一般情况_体重.Txt1.Text, out 体重))
            {
                BMI = Math.Round(体重 / ((身高 / 100) * (身高 / 100)), 2);
                this.txt一般情况_体重指数.Txt1.Text = BMI.ToString();
                if (BMI > 24)
                {
                    chk减体重.Checked = true;
                }
                else
                {
                    chk减体重.Checked = false;
                }
            }
        }
        void 一般情况_体重_Leave(object sender, EventArgs e)
        {
            一般情况_体重指数_Enter(null, null);
            //Decimal 身高 = 0;
            //Decimal 体重 = 0;
            //Decimal BMI = 0;
            //if (string.IsNullOrEmpty(this.txt一般情况_身高.Txt1.Text) || string.IsNullOrEmpty(this.txt一般情况_体重.Txt1.Text)) return;
            //if (Decimal.TryParse(this.txt一般情况_身高.Txt1.Text, out 身高) && Decimal.TryParse(this.txt一般情况_体重.Txt1.Text, out 体重))
            //{
            //    BMI = Math.Round(体重 / ((身高 / 100) * (身高 / 100)), 2);
            //    this.txt一般情况_体重指数.Txt1.Text = BMI.ToString();
            //}
        }
        void 一般情况_身高_Leave(object sender, EventArgs e)
        {
            一般情况_体重指数_Enter(null, null);
            //Decimal 身高 = 0;
            //Decimal 体重 = 0;
            //Decimal BMI = 0;
            //if (string.IsNullOrEmpty(this.txt一般情况_身高.Txt1.Text) || string.IsNullOrEmpty(this.txt一般情况_体重.Txt1.Text)) return;
            //if (Decimal.TryParse(this.txt一般情况_身高.Txt1.Text, out 身高) && Decimal.TryParse(this.txt一般情况_体重.Txt1.Text, out 体重))
            //{
            //    BMI = Math.Round(体重 / ((身高 / 100) * (身高 / 100)), 2);
            //    this.txt一般情况_体重指数.Txt1.Text = BMI.ToString();
            //}
        }
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit checkEdit = (CheckEdit)sender;
            if (checkEdit == null) return;
            SetControlEnable(checkEdit);
        }
        private void chk其他_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit checkEdit = (CheckEdit)sender;
            if (checkEdit == null) return;
            string name = checkEdit.Name;
            string target = "txt" + name.Substring(3, name.IndexOf('_') - 2) + "其他";
            Control[] list = this.Controls.Find(target, true);
            if (list.Length == 1)
            {
                if (list[0] is TextEdit)
                {
                    TextEdit txt = (TextEdit)list[0];
                    SetTextEditEnable(checkEdit, txt);
                }
            }

        }
        private void radio生活方式_锻炼频率_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio生活方式_锻炼频率.EditValue == null ? "" : this.radio生活方式_锻炼频率.EditValue.ToString();
            if (!string.IsNullOrEmpty(index) && index == "4")//不锻炼
            {
                this.txt生活方式_锻炼方式.Enabled = false;
                this.txt生活方式_坚持锻炼时间.Enabled = false;
                this.txt生活方式_每次锻炼时间.Enabled = false;
            }
            else
            {
                this.txt生活方式_锻炼方式.Enabled = true;
                this.txt生活方式_坚持锻炼时间.Enabled = true;
                this.txt生活方式_每次锻炼时间.Enabled = true;
            }
        }
        private void radio吸烟情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio吸烟情况.EditValue == null ? "" : this.radio吸烟情况.EditValue.ToString();
            if (!string.IsNullOrEmpty(index) && index == "1")//不吸烟
            {
                this.txt日吸烟量.Enabled = false;
                this.txt开始吸烟年龄.Enabled = false;
                this.txt戒烟年龄.Enabled = false;
            }
            else if (!string.IsNullOrEmpty(index) && index == "2") //已戒烟
            {
                this.txt日吸烟量.Enabled = true;
                this.txt开始吸烟年龄.Enabled = true;
                this.txt戒烟年龄.Enabled = true;
            }
            else if (!string.IsNullOrEmpty(index) && index == "3") //戒烟
            {
                this.txt日吸烟量.Enabled = true;
                this.txt开始吸烟年龄.Enabled = true;
                this.txt戒烟年龄.Enabled = false;
            }
        }
        private void radio饮酒频率_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio饮酒频率.EditValue == null ? "" : this.radio饮酒频率.EditValue.ToString();
            if (!string.IsNullOrEmpty(index) && index == "1")//从不
            {
                this.txt日饮酒量.Enabled = false;
                this.radio是否戒酒.Enabled = false;
                this.txt戒酒年龄.Enabled = false;
                this.txt开始饮酒年龄.Enabled = false;
                this.radio近一年内是否曾醉酒.Enabled = false;
                SetFlowControlEnable(flow饮酒种类, false);
            }
            else
            {
                this.txt日饮酒量.Enabled = true;
                this.radio是否戒酒.Enabled = true;
                this.txt戒酒年龄.Enabled = true;
                this.txt开始饮酒年龄.Enabled = true;
                this.radio近一年内是否曾醉酒.Enabled = true;
                SetFlowControlEnable(flow饮酒种类, true);
            }
            chk饮酒种类_其他_CheckedChanged(null, null);
        }
        private void chk饮酒种类_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt饮酒种类_其他.Enabled = chk饮酒种类_其他.Checked;
            this.txt饮酒种类_其他.Text = chk饮酒种类_其他.Checked ? this.txt饮酒种类_其他.Text : "";
        }
        private void radio口唇_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio口唇.EditValue == null ? "" : this.radio口唇.EditValue.ToString();
            this.txt口唇其他.Enabled = !string.IsNullOrEmpty(index) && index == "6" ? true : false;
            if (!string.IsNullOrEmpty(index) && index != "6") this.txt口唇其他.Text = "";
        }
        private void radio咽部_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt咽部_其他.Enabled = this.radio咽部_其他.Checked;
            if (this.radio咽部_其他.Checked == false) this.txt咽部_其他.Text = "";
        }
        private void radio眼底_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio眼底.EditValue == null ? "" : this.radio眼底.EditValue.ToString();
            this.txt眼底.Enabled = !string.IsNullOrEmpty(index) && index == "2" ? true : false;
            if (!string.IsNullOrEmpty(index) && index != "2")
            {
                this.txt眼底.Text = "";
            }
        }
        private void radio呼吸音_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio呼吸音, txt呼吸音_异常);
            //RadioGroup radio = (RadioGroup)sender;
            //if (radio == null) return;

            //string index = this.radio.EditValue.ToString();
            //this.txt呼吸音_异常.Enabled = !string.IsNullOrEmpty(index) && index == "2" ? true : false;
        }
        private void radio杂音_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio杂音, txt杂音);
        }
        private void radio压痛_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio压痛, txt压痛);
        }
        private void radio移动性浊音_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio移动性浊音, txt移动性浊音);
        }
        private void radio脾大_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio脾大, txt脾大);
        }
        private void radio肝大_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio肝大, txt肝大);
        }
        private void radio包块_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio包块, txt包块);
        }
        private void radio外阴_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio外阴, txt外阴);
        }
        private void radio阴道_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio阴道, txt阴道);
        }
        private void radio宫颈_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio宫颈, txt宫颈);
        }
        private void radio宫体_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio宫体, txt宫体);
        }
        private void radio附件_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio附件, txt附件);
        }
        //private void radio心电图_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    SetRadioGroupTxtEnable(radio心电图, txt心电图其他);
        //}
        private void radio胸部X线片_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio胸部X线片, txt胸部X线片);
        }
        private void radioB超_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio腹部B超, txt腹部B超异常);
        }
        private void radio其他B超_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio其他B超, txt其他B超异常);
        }
        private void radio宫颈涂片_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetRadioGroupTxtEnable(radio宫颈涂片, txt宫颈涂片);
        }
        //private void radio神经系统疾病_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    SetRadioGroupTxtEnable(radio神经系统疾病, txt神经系统疾病_其他);
        //}
        //private void radio其他系统疾病_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    SetRadioGroupTxtEnable(radio其他系统疾病, txt其他系统疾病_其他);
        //}
        private void radio健康评价_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio健康评价.EditValue == null ? "" : this.radio健康评价.EditValue.ToString();
            if (!string.IsNullOrEmpty(index) && index == "2")//体检有异常
            {
                this.txt体检异常1.Enabled = true;
                this.txt体检异常2.Enabled = true;
                this.txt体检异常3.Enabled = true;
                this.txt体检异常4.Enabled = true;
            }
            else
            {
                this.txt体检异常1.Enabled = false;
                this.txt体检异常2.Enabled = false;
                this.txt体检异常3.Enabled = false;
                this.txt体检异常4.Enabled = false;
            }
        }
        private void radio老年人认知能力_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio老年人认知能力.EditValue == null ? "" : this.radio老年人认知能力.EditValue.ToString();
            if (index == null) return;

            if (index == "1")//阴性
                this.txt老年人认知能力总分.Enabled = true;
            else
            {
                this.txt老年人认知能力总分.Enabled = false;
                this.txt老年人认知能力总分.Text = "";
            }
        }
        private void radio老年人情感状态_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio老年人情感状态.EditValue == null ? "" : this.radio老年人情感状态.EditValue.ToString();
            if (index == "1")//阴性
                this.txt老年人情感状态总分.Enabled = true;
            else
            {
                this.txt老年人情感状态总分.Enabled = false;
                this.txt老年人情感状态总分.Text = "";
            }
        }
        private void chk减体重_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk减体重.Checked;
            if (index)
            {
                this.txt减体重目标.Enabled = true;
                //计算符合条件的体重  BMI = 体重/身高/身高   身高单位  米
                Decimal 身高 = 0;
                Decimal 体重 = 0;
                Decimal BMI = 24.0m;
                if (string.IsNullOrEmpty(this.txt一般情况_身高.Txt1.Text) || string.IsNullOrEmpty(this.txt一般情况_体重.Txt1.Text)) return;
                if (Decimal.TryParse(this.txt一般情况_身高.Txt1.Text, out 身高) && Decimal.TryParse(this.txt一般情况_体重.Txt1.Text, out 体重))
                {
                    //体重 = Math.Floor(BMI * 身高 / 100 * 身高 / 100);
                    体重 = Math.Floor(身高 - 105);
                    this.txt减体重目标.Text = 体重.ToString() + "Kg";
                }
            }
            else
            {
                this.txt减体重目标.Enabled = false;
                this.txt减体重目标.Text = "";
            }
        }
        private void radion职业病有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio职业病有无.EditValue == null ? "" : this.radio职业病有无.EditValue.ToString();
            if (index == "3")//有职业病
            {
                Set职业病Editable(true);
            }
            else
                Set职业病Editable(false);
        }
        private void chk齿列_正常_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk齿列_正常.Checked;
            Set齿列ControlEditable(index);
        }
        private void chk齿列_缺齿_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk齿列_缺齿.Checked;
            this.txt缺齿1.Enabled = index;
            this.txt缺齿2.Enabled = index;
            this.txt缺齿3.Enabled = index;
            this.txt缺齿4.Enabled = index;
            this.txt缺齿1.Text = index ? this.txt缺齿1.Text : "";
            this.txt缺齿2.Text = index ? this.txt缺齿2.Text : "";
            this.txt缺齿3.Text = index ? this.txt缺齿3.Text : "";
            this.txt缺齿4.Text = index ? this.txt缺齿4.Text : "";
        }
        private void chk齿列_龋齿_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk齿列_龋齿.Checked;
            this.txt龋齿1.Enabled = index;
            this.txt龋齿2.Enabled = index;
            this.txt龋齿3.Enabled = index;
            this.txt龋齿4.Enabled = index;
            this.txt龋齿1.Text = index ? this.txt龋齿1.Text : "";
            this.txt龋齿2.Text = index ? this.txt龋齿2.Text : "";
            this.txt龋齿3.Text = index ? this.txt龋齿3.Text : "";
            this.txt龋齿4.Text = index ? this.txt龋齿4.Text : "";
        }
        private void chk齿列_义齿_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk齿列_义齿.Checked;
            this.txt义齿1.Enabled = index;
            this.txt义齿2.Enabled = index;
            this.txt义齿3.Enabled = index;
            this.txt义齿4.Enabled = index;
            this.txt义齿1.Text = index ? this.txt义齿1.Text : "";
            this.txt义齿2.Text = index ? this.txt义齿2.Text : "";
            this.txt义齿3.Text = index ? this.txt义齿3.Text : "";
            this.txt义齿4.Text = index ? this.txt义齿4.Text : "";
        }
        private void chk齿列_其他_CheckedChanged(object sender, EventArgs e)
        {
            bool index = this.chk齿列_其他.Checked;
            //this.txt其他.Text = index ? "" : this.txt其他.Text;
            this.txt齿列_其他.Enabled = index;
            this.txt齿列_其他.Text = index ? this.txt齿列_其他.Text : "";

        }
        private void btn住院史添加_Click(object sender, EventArgs e)
        {
            this.lblgc住院史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            DataRow dr = _ds健康体检.Tables["住院史"].Rows.Add();
            dr[tb_健康体检_住院史.个人档案编号] = _docNo;
            dr[tb_健康体检_住院史.类型] = "1";
            this.gc住院史.RefreshDataSource();
        }
        private void btn住院史删除_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("是否要删除当前行数据?")) return;
            this.gv住院史.DeleteRow(this.gv住院史.FocusedRowHandle);
            this.gc住院史.RefreshDataSource();
        }
        private void btn病床史添加_Click(object sender, EventArgs e)
        {
            this.lblgc家庭病床史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            DataRow dr = _ds健康体检.Tables["家庭病床史"].Rows.Add();
            dr[tb_健康体检_住院史.个人档案编号] = _docNo;
            dr[tb_健康体检_住院史.类型] = "2";
            this.gc建床史.RefreshDataSource();
        }
        private void btn病床史删除_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("是否要删除当前行数据?")) return;
            this.gv建床史.DeleteRow(this.gv住院史.FocusedRowHandle);
            this.gc建床史.RefreshDataSource();
        }
        private void btn添加用药情况_Click(object sender, EventArgs e)
        {
            this.lblgc用药情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            DataRow dr = _ds健康体检.Tables[tb_健康体检_用药情况表.__TableName].Rows.Add();
            dr[tb_健康体检_用药情况表.个人档案编号] = _docNo;
            this.gc用药情况.RefreshDataSource();
        }
        private void btn删除用药情况_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("是否要删除当前行数据?")) return;
            this.gv用药情况.DeleteRow(this.gv用药情况.FocusedRowHandle);
            this.gc用药情况.RefreshDataSource();
        }
        private void btn预防接种史添加_Click(object sender, EventArgs e)
        {
            this.lblgc接种史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            DataRow dr = _ds健康体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows.Add();
            dr[tb_健康体检_非免疫规划预防接种史.个人档案编号] = _docNo;
            this.gc接种史.RefreshDataSource();
        }
        private void btn预防接种史删除_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("是否要删除当前行数据?")) return;
            this.gv接种史.DeleteRow(this.gv接种史.FocusedRowHandle);
            this.gc接种史.RefreshDataSource();
        }
        private void radio粉尘防护措施有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = radio粉尘防护措施有无.EditValue == null ? "" : radio粉尘防护措施有无.EditValue.ToString();
            if (index == "3")//有
            {
                this.txt粉尘防护措施.Enabled = true;
            }
            else
            {
                this.txt粉尘防护措施.Enabled = false;
            }
        }
        private void radio放射物质防护措施有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = radio放射物质防护措施有无.EditValue == null ? "" : radio放射物质防护措施有无.EditValue.ToString();
            if (index == "3")//有
            {
                this.txt放射物质防护措施.Enabled = true;
            }
            else
            {
                this.txt放射物质防护措施.Enabled = false;
            }
        }
        private void radio物理因素防护措施有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = radio物理防护措施有无.EditValue == null ? "" : radio物理防护措施有无.EditValue.ToString();
            if (index == "3")//有
            {
                this.txt物理防护措施.Enabled = true;
            }
            else
            {
                this.txt物理防护措施.Enabled = false;
            }
        }
        private void radio化学物质防护措施有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = radio化学防护措施有无.EditValue == null ? "" : radio化学防护措施有无.EditValue.ToString();
            if (index == "3")//有
            {
                this.txt化学防护措施.Enabled = true;
            }
            else
            {
                this.txt化学防护措施.Enabled = false;
            }
        }
        private void radio职业病其他防护措施有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = radio职业病其他防护措施有无.EditValue == null ? "" : radio职业病其他防护措施有无.EditValue.ToString();
            if (index == "3")//有
            {
                this.txt职业病其他防护.Enabled = true;
            }
            else
            {
                this.txt职业病其他防护.Enabled = false;
            }
        }
        private void radio住院史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio住院史.EditValue == null ? "" : this.radio住院史.EditValue.ToString();
            if (index == "1")//有
            {
                this.btn住院史添加.Enabled = this.btn住院史删除.Enabled = true;
                this.lblgc住院史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                this.btn住院史添加.Enabled = this.btn住院史删除.Enabled = false;
                this.lblgc住院史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                //
                for (int rowi = gv住院史.DataRowCount; rowi >= 0; rowi--)
                {
                    this.gv住院史.DeleteRow(rowi);
                }
                this.gc住院史.RefreshDataSource();
            }
        }
        private void radio家庭病床史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio家庭病床史.EditValue == null ? "" : this.radio家庭病床史.EditValue.ToString();
            if (index == "1")//有
            {
                this.btn病床史添加.Enabled = this.btn病床史删除.Enabled = true;
                this.lblgc家庭病床史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                this.btn病床史添加.Enabled = this.btn病床史删除.Enabled = false;
                this.lblgc家庭病床史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                //
                for (int rowi = gv建床史.DataRowCount; rowi >= 0; rowi--)
                {
                    this.gv建床史.DeleteRow(rowi);
                }
                this.gc建床史.RefreshDataSource();
            }
        }
        private void radio预防接种史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio预防接种史.EditValue == null ? "" : this.radio预防接种史.EditValue.ToString();
            if (index == "1")//有
            {
                this.btn预防接种史添加.Enabled = this.btn预防接种史删除.Enabled = true;
                this.lblgc接种史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                this.btn预防接种史添加.Enabled = this.btn预防接种史删除.Enabled = false;
                this.lblgc接种史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                //
                for (int rowi = gv接种史.DataRowCount; rowi >= 0; rowi--)
                {
                    this.gv接种史.DeleteRow(rowi);
                }
                this.gc接种史.RefreshDataSource();
            }
        }
        private void radio主要用药情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio主要用药情况.EditValue == null ? "" : this.radio主要用药情况.EditValue.ToString();
            if (index == "1")//有
            {
                this.btn添加用药情况.Enabled = this.btn删除用药情况.Enabled = true;
                this.lblgc用药情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                this.btn添加用药情况.Enabled = this.btn删除用药情况.Enabled = false;
                this.lblgc用药情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                //
                for (int rowi = gv用药情况.DataRowCount; rowi >= 0 ; rowi--)
                {
                    this.gv用药情况.DeleteRow(rowi);
                }
                this.gc用药情况.RefreshDataSource();
            }
        }
        private void btn保存_Click(object sender, EventArgs e)
        {
            if (f保存检查())
            {
                //return;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.体检日期] = this.dte体检日期.DateTime.ToString("yyyy-MM-dd");
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.FIELD2] = this.text责任医生.Text.Trim();//责任医生
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.症状] = GetFlowLayoutResult(flow症状);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.症状其他] = this.txt症状_其他.EditValue;
                #region 一般情况
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.体温] = this.txt一般情况_体温.Txt1.Text.Trim() == "" ? DBNull.Value : this.txt一般情况_体温.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.脉搏] = this.txt一般情况_脉率.Txt1.Text.Trim() == "" ? DBNull.Value : this.txt一般情况_脉率.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.呼吸] = this.txt一般情况_呼吸频率.Txt1.Text.Trim() == "" ? DBNull.Value : this.txt一般情况_呼吸频率.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血压左侧1] = this.txt一般情况_血压左侧.Txt1.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血压左侧2] = this.txt一般情况_血压左侧.Txt2.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血压右侧1] = this.txt一般情况_血压右侧.Txt1.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血压右侧2] = this.txt一般情况_血压右侧.Txt2.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.左侧原因] = this.txt一般情况_血压左侧原因.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.右侧原因] = this.txt一般情况_血压右侧原因.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.腰围] = this.txt一般情况_腰围.Txt1.Text.Trim() == "" ? DBNull.Value : this.txt一般情况_腰围.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.身高] = this.txt一般情况_身高.Txt1.Text.Trim() == "" ? DBNull.Value : this.txt一般情况_身高.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.体重] = this.txt一般情况_体重.Txt1.Text.Trim() == "" ? DBNull.Value : this.txt一般情况_体重.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.体重指数] = this.txt一般情况_体重指数.Txt1.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.老年人状况评估] = this.radio老年人健康状态评估.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.老年人自理评估] = this.radio老年人生活自理能力评估.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.老年人情感] = this.radio老年人情感状态.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.老年人情感分] = this.txt老年人情感状态总分.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.老年人认知] = this.radio老年人认知能力.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.老年人认知分] = this.txt老年人认知能力总分.Text.Trim();
                #endregion

                #region 生活方式
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.锻炼频率] = this.radio生活方式_锻炼频率.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.每次锻炼时间] = this.txt生活方式_每次锻炼时间.Txt1.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.坚持锻炼时间] = this.txt生活方式_坚持锻炼时间.Txt1.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.锻炼方式] = this.txt生活方式_锻炼方式.Text.Trim();

                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.饮食习惯] = GetFlowLayoutResult(flow饮食习惯);

                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.吸烟状况] = this.radio吸烟情况.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.日吸烟量] = this.txt日吸烟量.Txt1.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.开始吸烟年龄] = this.txt开始吸烟年龄.Txt1.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.戒烟年龄] = this.txt戒烟年龄.Txt1.Text.Trim();

                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.饮酒频率] = this.radio饮酒频率.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.日饮酒量] = this.txt日饮酒量.Txt1.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.是否戒酒] = this.radio是否戒酒.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.戒酒年龄] = this.txt戒酒年龄.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.开始饮酒年龄] = this.txt开始饮酒年龄.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.近一年内是否曾醉酒] = this.radio近一年内是否曾醉酒.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.饮酒种类] = GetFlowLayoutResult(flow饮酒种类);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.饮酒种类其它] = this.txt饮酒种类_其他.EditValue;

                string 有无职业病 = this.radio职业病有无.EditValue == null ? "" : this.radio职业病有无.EditValue.ToString();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.有无职业病] = 有无职业病;
                if (有无职业病 == "3")//有
                {
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.具体职业] = this.txt工种.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.从业时间] = this.txt从业时间.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.粉尘] = this.txt粉尘.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.粉尘防护有无] = this.radio粉尘防护措施有无.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.粉尘防护措施] = this.txt粉尘防护措施.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.放射物质] = this.txt放射物质.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.放射物质防护措施有无] = this.radio放射物质防护措施有无.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.放射物质防护措施其他] = this.txt放射物质防护措施.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.物理因素] = this.txt物理因素.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.物理防护有无] = this.radio物理防护措施有无.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.物理防护措施] = this.txt物理防护措施.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.化学物质] = this.txt化学因素.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.化学物质防护] = this.radio化学防护措施有无.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.化学物质具体防护] = this.txt化学防护措施.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.职业病其他] = this.txt职业病其他.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.其他防护有无] = this.radio职业病其他防护措施有无.EditValue;
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.其他防护措施] = this.txt职业病其他防护.EditValue;
                }

                #endregion

                #region 脏器功能
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.口唇] = this.radio口唇.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.口唇其他] = this.txt口唇其他.EditValue;

                string 齿列 = string.Empty;
                if (chk齿列_正常.Checked) { 齿列 = "1"; }
                else
                {
                    if (chk齿列_缺齿.Checked) 齿列 += "2,";
                    if (chk齿列_龋齿.Checked) 齿列 += "3,";
                    if (chk齿列_义齿.Checked) 齿列 += "4,";
                    if (chk齿列_其他.Checked) 齿列 += "5";
                }


                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.齿列] = 齿列;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.齿列龋齿] = this.txt龋齿1.Text.Trim() + "@" + this.txt龋齿2.Text.Trim() + "@" + this.txt龋齿3.Text.Trim() + "@" + this.txt龋齿4.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.齿列缺齿] = this.txt缺齿1.Text.Trim() + "@" + this.txt缺齿2.Text.Trim() + "@" + this.txt缺齿3.Text.Trim() + "@" + this.txt缺齿4.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.齿列义齿] = this.txt义齿1.Text.Trim() + "@" + this.txt义齿2.Text.Trim() + "@" + this.txt义齿3.Text.Trim() + "@" + this.txt义齿4.Text.Trim();
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.齿列其他] = this.txt齿列_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.咽部] = GetFlowLayoutResult(flow咽部);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.咽部其他] = this.txt咽部_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.左眼视力] = (this.txt左眼视力.EditValue == null || string.IsNullOrEmpty(this.txt左眼视力.EditValue.ToString())) ? DBNull.Value : this.txt左眼视力.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.右眼视力] = (this.txt右眼视力.EditValue == null || string.IsNullOrEmpty(this.txt右眼视力.EditValue.ToString())) ? DBNull.Value : this.txt右眼视力.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.左眼矫正] = (this.txt矫正左眼视力.EditValue == null || string.IsNullOrEmpty(this.txt矫正左眼视力.EditValue.ToString())) ? DBNull.Value : this.txt矫正左眼视力.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.右眼矫正] = (this.txt矫正右眼视力.EditValue == null || string.IsNullOrEmpty(this.txt矫正右眼视力.EditValue.ToString())) ? DBNull.Value : this.txt矫正右眼视力.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.听力] = GetFlowLayoutResult(flow听力);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.运动功能] = GetFlowLayoutResult(flow运动功能);
                #endregion

                #region 查体
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.眼底] = this.radio眼底.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.眼底异常] = this.txt眼底.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.皮肤] = GetFlowLayoutResult(flow皮肤);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.皮肤其他] = this.txt皮肤_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.巩膜] = GetFlowLayoutResult(flow巩膜);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.巩膜其他] = this.txt巩膜_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.淋巴结] = GetFlowLayoutResult(flow淋巴结);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.淋巴结其他] = this.txt淋巴结_其他.EditValue;

                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.桶状胸] = this.radio桶状胸.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.呼吸音] = this.radio呼吸音.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.呼吸音异常] = this.txt呼吸音_异常.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.罗音] = GetFlowLayoutResult(flow罗音);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.罗音异常] = this.txt罗音_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心率] = this.txt心率.Txt1.EditValue == null ? DBNull.Value : txt心率.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心律] = this.radio心律.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.杂音] = this.radio杂音.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.杂音有] = this.txt杂音.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.压痛] = this.radio压痛.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.压痛有] = this.txt压痛.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.包块] = this.radio包块.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.包块有] = this.txt包块.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.肝大] = this.radio肝大.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.肝大有] = this.txt肝大.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.脾大] = this.radio脾大.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.脾大有] = this.txt脾大.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.浊音] = this.radio移动性浊音.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.浊音有] = this.txt移动性浊音.EditValue;

                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.下肢水肿] = this.radio下肢水肿.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.足背动脉搏动] = this.radio足背动脉搏动.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.肛门指诊] = GetFlowLayoutResult(flow肛门指诊);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.肛门指诊异常] = this.txt肛门指诊_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.乳腺] = GetFlowLayoutResult(flow乳腺);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.乳腺其他] = this.txt乳腺_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.外阴] = this.radio外阴.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.外阴异常] = this.txt外阴.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.阴道] = this.radio阴道.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.阴道异常] = this.txt阴道.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.宫颈] = this.radio宫颈.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.宫颈异常] = this.txt宫颈.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.宫体] = this.radio宫体.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.宫体异常] = this.txt宫体.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.附件] = this.radio附件.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.附件异常] = this.txt附件.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.查体其他] = this.txt查体_其他.EditValue;
                #endregion

                #region 辅助检查
                //_ds健康体检.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.血型] = ControlsHelper.GetComboxKey(this.cboABO);
                //_ds健康体检.Tables[tb_健康档案_健康状态.__TableName].Rows[0][tb_健康档案_健康状态.RH] = ControlsHelper.GetComboxKey(this.cboRh);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血红蛋白] = this.txt血红蛋白.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.白细胞] = this.txt白细胞.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血小板] = this.txt血小板.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血常规其他] = this.txt血常规_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.尿蛋白] = this.txt尿蛋白.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.尿糖] = this.txt尿糖.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.尿酮体] = this.txt尿胴体.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.尿潜血] = this.txt尿潜血.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.尿常规其他] = this.txt尿常规其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.空腹血糖] = this.txt空腹血糖.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.空腹血糖2] = this.txt空腹血糖2.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.同型半胱氨酸] = this.txt同型半胱氨酸.Txt1.EditValue;
                //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.餐后2H血糖] = this.txt餐后2H血糖.Txt1.EditValue;
                //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心电图] = this.radio心电图.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心电图] = GetFlowLayoutResult(flow心电图);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心电图异常] = this.txt心电图_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.尿微量白蛋白] = this.txt尿微量白蛋白.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.大便潜血] = this.radio大便潜血.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.糖化血红蛋白] = this.txt糖化血红蛋白.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.乙型肝炎表面抗原] = this.radio乙型肝炎表面抗原.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血清谷丙转氨酶] = this.txt血清谷丙转氨酶.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血清谷草转氨酶] = this.txt血清谷草转氨酶.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.白蛋白] = this.txt白蛋白.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.总胆红素] = this.txt总胆红素.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.结合胆红素] = this.txt结合胆红素.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血清肌酐] = this.txt血清肌酐.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血尿素氮] = this.txt血尿素氮.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血钾浓度] = this.txt血钾浓度.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血钠浓度] = this.txt血钠浓度.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.总胆固醇] = this.txt总胆固醇.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.甘油三酯] = this.txt甘油三酯.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血清低密度脂蛋白胆固醇] = this.txt血清低密度脂蛋白胆固醇.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血清高密度脂蛋白胆固醇] = this.txt血清高密度脂蛋白胆固醇.Txt1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.胸部X线片] = this.radio胸部X线片.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.胸部X线片异常] = this.txt胸部X线片.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.B超] = this.radio腹部B超.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.B超其他] = this.txt腹部B超异常.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.其他B超] = this.radio其他B超.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.其他B超异常] = this.txt其他B超异常.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.宫颈涂片] = this.radio宫颈涂片.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.宫颈涂片异常] = this.txt宫颈涂片.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.辅助检查其他] = this.txt辅助检查_其他.EditValue;
                #endregion

                #region 中医体质辨识
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.平和质] = this.radio平和质.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.气虚质] = this.radio气虚质.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.阳虚质] = this.radio阳虚质.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.阴虚质] = this.radio阴虚质.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.痰湿质] = this.radio痰湿质.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.湿热质] = this.radio湿热质.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血瘀质] = this.radio血瘀质.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.气郁质] = this.radio气郁质.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.特禀质] = this.radio特秉质.EditValue;
                #endregion

                #region 现存主要健康问题
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.脑血管疾病] = GetFlowLayoutResult(flow脑血管疾病);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.脑血管疾病其他] = this.txt脑血管疾病_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.肾脏疾病] = GetFlowLayoutResult(flow肾脏疾病);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.肾脏疾病其他] = this.txt肾脏疾病_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心脏疾病] = GetFlowLayoutResult(flow心血管疾病);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心脏疾病其他] = this.txt心血管疾病_其他.EditValue;
                //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血管疾病] = GetFlowLayoutResult(flow血管疾病);
                //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血管疾病其他] = this.txt血管疾病_其他.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.眼部疾病] = GetFlowLayoutResult(flow眼部疾病);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.眼部疾病其他] = this.txt眼部疾病_其他.EditValue;
                //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.神经系统疾病] = this.radio神经系统疾病.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.神经系统疾病] = GetFlowLayoutResult(flow神经系统疾病);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.神经系统疾病其他] = this.txt神经系统疾病_其他.EditValue;
                //_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.其他系统疾病] = this.radio其他系统疾病.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.其他系统疾病] = GetFlowLayoutResult(flow其他系统疾病);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.其他系统疾病其他] = this.txt其他系统疾病_其他.EditValue;
                #endregion

                #region 健康评价
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.健康评价] = this.radio健康评价.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.健康评价异常1] = this.txt体检异常1.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.健康评价异常2] = this.txt体检异常2.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.健康评价异常3] = this.txt体检异常3.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.健康评价异常4] = this.txt体检异常4.EditValue;

                #endregion

                #region 健康指导
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.健康指导] = GetFlowLayoutResult(flow健康指导);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.危险因素控制] = GetFlowLayoutResult(flow危险因素控制);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.危险因素控制体重] = this.txt减体重目标.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.危险因素控制疫苗] = this.txt建议疫苗接种.EditValue;
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.危险因素控制其他] = this.txt危险因素其他.EditValue;
                #endregion

                #region 其余需保存选项
                int i = Get缺项(_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0]);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.缺项] = i;
                int j = Get完整度(i);
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.完整度] = j;
                #endregion
                _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.健康体检] = i + "," + j;

                #region 子表  创建时间字段的更新

                if (_ds健康体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows.Count > 0)
                {
                    for (int x = 0; x < _ds健康体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows.Count; x++)
                    {
                        DataRow row = _ds健康体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName].Rows[x];
                        if (row.RowState == DataRowState.Added) row[tb_健康体检_非免疫规划预防接种史.创建日期] = this.dte创建时间.Text;
                    }
                }
                if (_ds健康体检.Tables[tb_健康体检_用药情况表.__TableName].Rows.Count > 0)
                {
                    for (int x = 0; x < _ds健康体检.Tables[tb_健康体检_用药情况表.__TableName].Rows.Count; x++)
                    {
                        DataRow row = _ds健康体检.Tables[tb_健康体检_用药情况表.__TableName].Rows[x];
                        if (row.RowState == DataRowState.Added) row[tb_健康体检_用药情况表.创建时间] = this.dte创建时间.Text;
                    }
                }
                if (_ds健康体检.Tables["住院史"].Rows.Count > 0)
                {
                    for (int x = 0; x < _ds健康体检.Tables["住院史"].Rows.Count; x++)
                    {
                        DataRow row = _ds健康体检.Tables["住院史"].Rows[x];
                        if (row.RowState == DataRowState.Added)
                        {
                            row[tb_健康体检_住院史.创建日期] = this.dte创建时间.Text;
                        }
                    }
                }
                if (_ds健康体检.Tables["家庭病床史"].Rows.Count > 0)
                {
                    for (int x = 0; x < _ds健康体检.Tables["家庭病床史"].Rows.Count; x++)
                    {
                        DataRow row = _ds健康体检.Tables["家庭病床史"].Rows[x];
                        if (row.RowState == DataRowState.Added) row[tb_健康体检_住院史.创建日期] = this.dte创建时间.Text;
                    }
                }
                #endregion

                #region 是否纳入高血压管理
                int gxyzc1 = this.txt一般情况_血压左侧.Txt1.Text.Trim() == "" ? 0 : Convert.ToInt32(this.txt一般情况_血压左侧.Txt1.Text);
                int gxyzc2 = this.txt一般情况_血压左侧.Txt2.Text.Trim() == "" ? 0 : Convert.ToInt32(this.txt一般情况_血压左侧.Txt2.Text);
                int gxyyc1 = this.txt一般情况_血压右侧.Txt1.Text.Trim() == "" ? 0 : Convert.ToInt32(this.txt一般情况_血压右侧.Txt1.Text);
                int gxyyc2 = this.txt一般情况_血压右侧.Txt2.Text.Trim() == "" ? 0 : Convert.ToInt32(this.txt一般情况_血压右侧.Txt2.Text);
                string nrgxygl = "1";
                if (gxyzc1 >= 140 || gxyzc2 >= 90 || gxyyc1 >= 140 || gxyyc2 >= 90)//符合高血压的特征
                {
                    if (_ds健康体检.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count == 0)//并且不存在高血压管理卡
                    {
                        if (Msg.AskQuestion("该居民的收缩压>=140mmHg或舒张压>=90mmHg，建议到医院确诊是否为高血压患者，若确诊为高血压患者，请点击[确定]，系统将自动纳入高血压患者管理，否则点击[取消]！"))//点击确定
                        {
                            nrgxygl = "2";
                            DataRow rowGXYGLK = _ds健康体检.Tables[tb_MXB高血压管理卡.__TableName].Rows.Add();
                            AddNewGXYGLK(rowGXYGLK);
                        }
                        else
                        {
                            nrgxygl = "3";
                        }
                    }
                }
                else if (gxyzc1 >= 140 || gxyzc2 >= 90)//符合高血压的特征
                {
                    if (_ds健康体检.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count == 0)//并且不存在高血压管理卡
                    {
                        if (Msg.AskQuestion("该居民的收缩压>=140mmHg或舒张压>=90mmHg，建议到医院确诊是否为高血压患者，若确诊为高血压患者，请点击[确定]，系统将自动纳入高血压患者管理，否则点击[取消]！"))//点击确定
                        {
                            nrgxygl = "2";
                            DataRow rowGXYGLK = _ds健康体检.Tables[tb_MXB高血压管理卡.__TableName].Rows.Add();
                            AddNewGXYGLK(rowGXYGLK);
                        }
                        else
                        {
                            nrgxygl = "3";
                        }
                    }
                }
                else if (gxyyc1 >= 140 || gxyyc2 >= 90)//符合高血压的特征
                {
                    if (_ds健康体检.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count == 0)//并且不存在高血压管理卡
                    {
                        if (Msg.AskQuestion("该居民的收缩压>=140mmHg或舒张压>=90mmHg，建议到医院确诊是否为高血压患者，若确诊为高血压患者，请点击[确定]，系统将自动纳入高血压患者管理，否则点击[取消]！"))//点击确定
                        {
                            nrgxygl = "2";
                            DataRow rowGXYGLK = _ds健康体检.Tables[tb_MXB高血压管理卡.__TableName].Rows.Add();
                            AddNewGXYGLK(rowGXYGLK);
                        }
                        else
                        {
                            nrgxygl = "3";
                        }
                    }
                }

                //临界高血压
                if (((gxyzc1 >= 130) && (gxyzc1 < 140)) || ((gxyzc2 >= 85.0D) && (gxyzc2 < 90.0D)) || ((gxyyc1 >= 130.0D) && (gxyyc1 < 140.0D)) || ((gxyyc2 >= 85.0D) && (gxyyc2 < 90.0D) && ("1".Equals(nrgxygl))))
                {
                    if (_ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] != null && _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压].ToString() != "1")
                    {
                        _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "2";
                    }
                }

                #endregion
                #region 是否纳入糖尿病管理
                string kfxt = this.txt空腹血糖.Txt1.Text.Trim();
                decimal gkfxt = string.IsNullOrEmpty(kfxt) ? 0.00m : Convert.ToDecimal(kfxt);
                string kfxt2 = this.txt空腹血糖2.Txt1.Text.Trim();
                decimal gkfxt2 = string.IsNullOrEmpty(kfxt2) ? 0.00m : Convert.ToDecimal(kfxt2);
                //string chxt = this.txt餐后2H血糖.Txt1.Text.Trim();
                //decimal gchxt = string.IsNullOrEmpty(chxt) ? 0.00m : Convert.ToDecimal(chxt);
                string nrtnbgl = "1";
                if (gkfxt < 7.0m && gkfxt >= 6.1m || (gkfxt2 < 7.0m && gkfxt2 >= 6.1m))//空腹血糖受损
                {
                    _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否空腹血糖] = "1";
                }
                else
                {
                    _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否空腹血糖] = "";
                }

                if (gkfxt >= 7.0m || gkfxt2 >= 7.0m)//符合糖尿病条件
                {
                    if (_ds健康体检.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Count == 0)//不存在糖尿病管理卡
                    {
                        if (Msg.AskQuestion("该居民的空腹血糖值>=7.0mmHg，建议到医院确诊是否为糖尿病患者，若确诊为糖尿病患者，请点击[确定]，系统将自动纳入糖尿病患者管理，否则点击[取消]！"))
                        {
                            nrtnbgl = "2";
                            DataRow row糖尿病 = _ds健康体检.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Add();
                            AddTNBGLK(row糖尿病);
                        }
                        else
                        {
                            nrtnbgl = "3";
                        }
                    }
                }

                #region 餐后2H血糖（未用）
                //if (gchxt >= 11.1m)//符合糖尿病条件
                //{
                //    if (_ds健康体检.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Count == 0)//不存在糖尿病管理卡
                //    {
                //        if (Msg.AskQuestion("该居民的空腹血糖值>=7.0mmHg，建议到医院确诊是否为糖尿病患者，若确诊为糖尿病患者，请点击[确定]，系统将自动纳入糖尿病患者管理，否则点击[取消]！"))
                //        {
                //            nrtnbgl = "2";
                //            DataRow row糖尿病 = _ds健康体检.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Add();
                //            AddTNBGLK(row糖尿病);
                //        }
                //        else
                //        {
                //            nrtnbgl = "3";
                //        }
                //    }
                //}

                //if (!string.IsNullOrEmpty(chxt))
                //{
                //    if (gchxt < 11.1m && gchxt <= 7.8m)//是否餐后血糖
                //    {
                //        _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否餐后血糖] = "1";
                //    }
                //    else if (gchxt >= 11.1m)
                //    {
                //        _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否餐后血糖] = "";
                //        _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否糖尿病] = "2";
                //    }
                //}
                #endregion

                #endregion
                #region  血脂
                string 总胆固醇 = this.txt总胆固醇.Txt1.Text.Trim();
                if (!string.IsNullOrEmpty(总胆固醇))
                {
                    decimal zdgc = 0.0m;
                    if (Decimal.TryParse(总胆固醇, out zdgc))
                    {
                        if ((zdgc >= 5.18m) && (zdgc <= 6.21m))//血脂
                        {
                            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否血脂] = "1";
                        }
                        else
                        {
                            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否血脂] = "";
                        }
                    }
                }
                string 甘油三酯 = this.txt甘油三酯.Txt1.Text.Trim();
                if (!string.IsNullOrEmpty(甘油三酯))
                {
                    decimal gysz = 0.0m;
                    if (Decimal.TryParse(甘油三酯, out gysz))
                    {
                        if ((gysz >= 1.7m) && (gysz <= 2.25m))//血脂
                        {
                            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否血脂] = "1";
                        }
                        else
                        {
                            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否血脂] = "";
                        }
                    }
                }
                #endregion
                #region  肥胖   超重且中心型肥胖
                string BMI = this.txt一般情况_体重指数.Txt1.Text.Trim();
                if (!string.IsNullOrEmpty(BMI))
                {
                    decimal _bmi = 0.0m;
                    if (Decimal.TryParse(BMI, out _bmi))
                    {
                        if (_bmi >= 28.0m)//肥胖
                        {
                            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否肥胖] = "1";
                        }
                        else
                        {
                            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否肥胖] = "";
                        }
                        //超重且中心型肥胖
                        string dXb = this.txt性别.Text.Trim();
                        string yw = this.txt一般情况_腰围.Txt1.Text.Trim();
                        decimal dYw = 0.0m;
                        if (Decimal.TryParse(yw, out dYw))
                        {
                            if (_bmi >= 24.0m && dYw >= 90m)
                            {
                                _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否超重肥胖] = "1";
                            }
                            else
                            {
                                _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否超重肥胖] = "";
                            }
                        }
                    }
                }
                #endregion
                #region  重度吸烟
                //吸烟状况   重度吸烟
                string Rxyl = this.txt日吸烟量.Txt1.Text.Trim();
                if (!string.IsNullOrEmpty(Rxyl))
                {
                    int _rxyl = 0;
                    if (Int32.TryParse(Rxyl, out _rxyl) && _rxyl >= 20)
                    {
                        _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否重度吸烟] = "1";
                    }
                    else
                    {
                        _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否重度吸烟] = "";
                    }

                }
                #endregion

                String gxytz = "0";
                String tnbtz = "0";
                if (_ds健康体检.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Count == 0 && (nrgxygl == "2" || nrtnbgl == "2"))
                {
                    DataRow row = _ds健康体检.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Add();
                    AddNew慢病基础信息表(row);

                    if (nrgxygl == "2")
                    {
                        row[tb_MXB慢病基础信息表.高血压随访时间] = "";
                    }
                    if (nrtnbgl == "2")
                    {
                        row[tb_MXB慢病基础信息表.糖尿病随访时间] = "";
                    }
                }
                else
                {
                    if ((_ds健康体检.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Count != 0) &&
        ("1900-01-01".Equals(_ds健康体检.Tables[tb_MXB慢病基础信息表.__TableName].Rows[0][tb_MXB慢病基础信息表.高血压随访时间])) && ("1".Equals(nrtnbgl)))
                    {
                        _ds健康体检.Tables[tb_MXB慢病基础信息表.__TableName].Rows[0][tb_MXB慢病基础信息表.高血压随访时间] = "";
                        gxytz = "1";
                    }
                    if ((_ds健康体检.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Count != 0) &&
        ("1900-01-01".Equals(_ds健康体检.Tables[tb_MXB慢病基础信息表.__TableName].Rows[0][tb_MXB慢病基础信息表.糖尿病随访时间])) && ("2".Equals(nrtnbgl)))
                    {
                        _ds健康体检.Tables[tb_MXB慢病基础信息表.__TableName].Rows[0][tb_MXB慢病基础信息表.糖尿病随访时间] = "";
                        tnbtz = "1";
                    }
                }
                //操作既往史 
                if (nrgxygl == "2")
                {
                    DataTable dt = _ds健康体检.Tables[tb_健康档案_既往病史.__TableName];
                    bool existsGXY = false;
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (int y = 0; y < dt.Rows.Count; y++)
                        {
                            if (dt.Rows[y][tb_健康档案_既往病史.疾病名称].ToString().IndexOf('2') != -1)
                                existsGXY = true;
                        }
                    }
                    if (!existsGXY)//不存在高血压
                    {
                        DataRow row = _ds健康体检.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
                        row[tb_健康档案_既往病史.个人档案编号] = _docNo;
                        row[tb_健康档案_既往病史.疾病类型] = "疾病";
                        row[tb_健康档案_既往病史.疾病名称] = "2";
                        row[tb_健康档案_既往病史.D_JBBM] = "高血压";
                    }
                }
                if (nrtnbgl == "2")
                {
                    DataTable dt = _ds健康体检.Tables[tb_健康档案_既往病史.__TableName];
                    bool existsTNB = false;
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (int y = 0; y < dt.Rows.Count; y++)
                        {
                            if (dt.Rows[y][tb_健康档案_既往病史.疾病名称].ToString().IndexOf('3') != -1)
                                existsTNB = true;
                        }
                    }
                    if (!existsTNB)//不存在糖尿病
                    {
                        DataRow row = _ds健康体检.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
                        row[tb_健康档案_既往病史.个人档案编号] = _docNo;
                        row[tb_健康档案_既往病史.疾病类型] = "疾病";
                        row[tb_健康档案_既往病史.疾病名称] = "3";
                        row[tb_健康档案_既往病史.D_JBBM] = "糖尿病";
                    }
                }

                DataSet ds = _Bll.CreateSaveData(_ds健康体检, _UpdateType);
                SaveResult result = _Bll.Save(ds);
                if (result.Success)
                {
                    if (_ds健康体检.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count >= 1)
                    {
                        Msg.ShowInformation("该居民为慢病患者，请一同录入随访表！");
                    }
                }
                _frm.ReLoad(this.Name, _docNo);
            }
        }
        private void AddNew慢病基础信息表(DataRow row)
        {
            row[tb_MXB慢病基础信息表.姓名] = util.DESEncrypt.DES加密(this.text姓名.Text.Trim(), false);
            row[tb_MXB慢病基础信息表.拼音简码] = util.DESEncrypt.GetChineseSpell(this.text姓名.Text.Trim());
            row[tb_MXB慢病基础信息表.身份证号] = this.text身份证号.Text.Trim();
            row[tb_MXB慢病基础信息表.出生日期] = string.IsNullOrEmpty(this.text出生日期.Text);
            row[tb_MXB慢病基础信息表.民族] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.民族];//.SelectedIndex;
            row[tb_MXB慢病基础信息表.文化程度] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.文化程度];// ControlsHelper.GetComboxKey(this.cbo文化程度);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.常住类型] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.常住类型]; //ontrolsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.工作单位] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.工作单位];// this.txt工作单位.Text.Trim();
            row[tb_MXB慢病基础信息表.婚姻状况] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.婚姻状况]; //ControlsHelper.GetComboxKey(this.cbo婚姻状况);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.职业] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.职业]; //ControlsHelper.GetComboxKey(this.cbo职业);//.SelectedIndex;
            row[tb_MXB慢病基础信息表.联系人电话] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人电话]; //this.txt联系人电话.Text.Trim();
            row[tb_MXB慢病基础信息表.省] = "37";
            row[tb_MXB慢病基础信息表.市] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.市]; //this.cbo居住地址_市.EditValue;
            row[tb_MXB慢病基础信息表.区] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.区]; //this.cbo居住地址_县.EditValue;
            row[tb_MXB慢病基础信息表.街道] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.街道]; //this.cbo居住地址_街道.EditValue;
            row[tb_MXB慢病基础信息表.居委会] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居委会]; //this.cbo居住地址_村委会.EditValue;
            row[tb_MXB慢病基础信息表.所属片区] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属片区];// this.cbo所属片区.Text.Trim();
            row[tb_MXB慢病基础信息表.居住地址] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住地址];// this.txt居住地址_详细地址.Text.Trim();
            row[tb_MXB慢病基础信息表.医疗费用支付方式] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗费支付类型];// Get医疗费用支付方式();
            //row[tb_MXB慢病基础信息表.医疗费用支付类型其他] = this.txt医疗费用支付方式_其他.Text.Trim();
            row[tb_MXB慢病基础信息表.医疗保险号] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗保险号]; //this.txt医疗保险号.Text.Trim();
            row[tb_MXB慢病基础信息表.新农合号] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.新农合号];// this.txt新农合号.Text.Trim();
            row[tb_MXB慢病基础信息表.建档机构] = Loginer.CurrentUser.所属机构;
            row[tb_MXB慢病基础信息表.建档时间] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.建档人] = Loginer.CurrentUser.用户编码;
            row[tb_MXB慢病基础信息表.创建机构] = Loginer.CurrentUser.所属机构;
            row[tb_MXB慢病基础信息表.创建时间] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.创建人] = Loginer.CurrentUser.用户编码;
            row[tb_MXB慢病基础信息表.更新时间] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.更新人] = Loginer.CurrentUser.用户编码;
            row[tb_MXB慢病基础信息表.HAPPENTIME] = this.dte创建时间.Text.Trim();
            row[tb_MXB慢病基础信息表.所属机构] = Loginer.CurrentUser.所属机构;
            row[tb_MXB慢病基础信息表.高血压随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.糖尿病随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.脑卒中随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.冠心病随访时间] = "1900-01-01";
            row[tb_MXB慢病基础信息表.家庭档案编号] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.家庭档案编号];
            row[tb_MXB慢病基础信息表.个人档案编号] = _docNo;

        }
        private void AddTNBGLK(DataRow row糖尿病)
        {
            row糖尿病[tb_MXB糖尿病管理卡.管理卡编号] = "";
            row糖尿病[tb_MXB糖尿病管理卡.个人档案编号] = _docNo;
            row糖尿病[tb_MXB糖尿病管理卡.所属机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构];
            row糖尿病[tb_MXB糖尿病管理卡.创建机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建机构];
            row糖尿病[tb_MXB糖尿病管理卡.创建时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建时间];// this.dte录入时间.Text.Trim();
            row糖尿病[tb_MXB糖尿病管理卡.创建人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建人];// Loginer.CurrentUser.用户编码;
            row糖尿病[tb_MXB糖尿病管理卡.发生时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.调查时间];
            row糖尿病[tb_MXB糖尿病管理卡.修改人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改人];// this.dte录入时间.Text.Trim();
            row糖尿病[tb_MXB糖尿病管理卡.修改时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改时间];// this.dte录入时间.Text.Trim();
            row糖尿病[tb_MXB糖尿病管理卡.缺项] = "26";
            row糖尿病[tb_MXB糖尿病管理卡.完整度] = "0";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡] = "26,0";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病随访表] = "未建";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否糖尿病] = "1";
        }
        private void AddNewGXYGLK(DataRow rowGXYGLK)
        {
            rowGXYGLK[tb_MXB高血压管理卡.管理卡编号] = "";
            rowGXYGLK[tb_MXB高血压管理卡.个人档案编号] = _docNo;
            rowGXYGLK[tb_MXB高血压管理卡.所属机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构];
            rowGXYGLK[tb_MXB高血压管理卡.创建机构] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建机构];
            rowGXYGLK[tb_MXB高血压管理卡.发生时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.调查时间];// string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
            rowGXYGLK[tb_MXB高血压管理卡.创建时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建时间];// this.dte录入时间.Text.Trim();
            rowGXYGLK[tb_MXB高血压管理卡.创建人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建人];// Loginer.CurrentUser.用户编码;
            rowGXYGLK[tb_MXB高血压管理卡.修改人] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改人];// this.dte录入时间.Text.Trim();
            rowGXYGLK[tb_MXB高血压管理卡.修改时间] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改时间];// this.dte录入时间.Text.Trim();
            //rowGXYGLK[tb_MXB高血压管理卡.终止管理] = _ds健康档案OnlyCode.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.档案状态];
            rowGXYGLK[tb_MXB高血压管理卡.缺项] = "20";
            rowGXYGLK[tb_MXB高血压管理卡.完整度] = "0";

            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压管理卡] = "20,0";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
            _ds健康体检.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "1";
        }
        #endregion

        #region Private Method
        private void Set职业病Editable(bool p)
        {
            this.txt工种.Enabled = p;
            this.txt从业时间.Enabled = p;

            this.txt粉尘.Enabled = p;
            this.radio粉尘防护措施有无.Enabled = p;
            //this.radio粉尘.EditValue = p ? "1" : "0";
            this.txt粉尘防护措施.Enabled = p;

            this.txt放射物质.Enabled = p;
            this.radio放射物质防护措施有无.Enabled = p;
            //this.radio放射.EditValue = p ? "1" : "0";
            this.txt放射物质防护措施.Enabled = p;

            this.txt物理因素.Enabled = p;
            this.radio物理防护措施有无.Enabled = p;
            //this.radio物理.EditValue = p ? "1" : "0";
            this.txt物理防护措施.Enabled = p;

            this.txt化学因素.Enabled = p;
            this.radio化学防护措施有无.Enabled = p;
            //this.radio化学.EditValue = p ? "1" : "0";
            this.txt化学防护措施.Enabled = p;

            this.txt职业病其他.Enabled = p;
            this.radio职业病其他防护措施有无.Enabled = p;
            //this.radio其他.EditValue = p ? "1" : "0";
            this.txt职业病其他防护.Enabled = p;
        }
        private bool f保存检查()
        {
            bool result = true;
            if (string.IsNullOrEmpty(this.dte体检日期.Text.Trim()))
            {
                Msg.Warning("体检日期是必填项，请输入体检日期");
                this.dte体检日期.Focus();
                return false;
                //result = false;
            }

            if (this.dte体检日期.DateTime > this.dte创建时间.DateTime)
            {
                Msg.Warning("体检日期不能大于创建时间，请重新填写体检日期");
                this.dte体检日期.Focus();
                return false;
            }

            string 出生日期 = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期].ToString();
            if (!string.IsNullOrEmpty(出生日期))
            {
                DateTime dt = DateTime.Now;
                if (DateTime.TryParse(出生日期, out dt))
                {
                    if (DateTime.Now.Year - dt.Year >= 35)
                    {
                        if (this.txt一般情况_血压左侧.Txt1.Text.Trim() == "" && this.txt一般情况_血压左侧.Txt2.Text.Trim() == "" && this.txt一般情况_血压右侧.Txt1.Text.Trim() == "" && this.txt一般情况_血压右侧.Txt2.Text.Trim() == "")
                        {
                            if (Msg.AskQuestion("此人年龄已到35岁，是否输入血压值？"))
                            {
                                this.txt一般情况_血压左侧.Txt1.Focus();
                                return false;
                            }
                        }
                    }
                    if (DateTime.Now.Year - dt.Year >= 65)
                    {
                        if (this.txt空腹血糖.Txt1.Text.Trim() == "")
                        {
                            if (Msg.AskQuestion("此人年龄已到65岁，是否输入空腹血糖？"))
                            {
                                this.txt空腹血糖.Txt1.Focus();
                                return false;
                            }
                        }
                    }
                }
            }

            //判断“血压值”是否填写完整//合理值区间提醒
            StringBuilder sbNotity = new StringBuilder();
            if ((string.IsNullOrEmpty(txt一般情况_血压左侧.Txt1.Text.Trim()) && string.IsNullOrEmpty(txt一般情况_血压左侧.Txt2.Text.Trim())) || (string.IsNullOrEmpty(txt一般情况_血压右侧.Txt1.Text.Trim()) || string.IsNullOrEmpty(txt一般情况_血压右侧.Txt2.Text.Trim())))
            {
                if (Msg.AskQuestion("血压值填写不完整，是否继续填写？"))
                {
                    this.txt一般情况_血压左侧.Txt1.Focus();
                    return false;
                }
                else
                    return true;
            }
            else
            {
                if (int.Parse(txt一般情况_血压左侧.Txt1.Text) > 140 || int.Parse(txt一般情况_血压左侧.Txt1.Text) < 90)
                {
                    sbNotity.Append("收缩压：" + txt一般情况_血压左侧.Txt1.Text + "，不在90--140之间\n");
                }

                if (int.Parse(txt一般情况_血压左侧.Txt2.Text) > 90 || int.Parse(txt一般情况_血压左侧.Txt2.Text) < 60)
                {
                    sbNotity.Append("舒张压：" + txt一般情况_血压左侧.Txt2.Text + "，不在60--90之间\n");
                }                
            }
            if (GetFlowLayoutResult(flow危险因素控制).ToString() == "")
            {
                if (Msg.AskQuestion("危险控制因素未填写，是否填写？"))
                {
                    this.chk戒烟.Focus();
                    return false;
                }
            }
            //体温
            if (string.IsNullOrEmpty(txt一般情况_体温.Txt1.Text.Trim()))
            {
                if (Msg.AskQuestion("体温值填写不完整，是否继续填写？"))
                {
                    this.txt一般情况_体温.Txt1.Focus();
                    return false;
                }
                else
                    return true;
            }
            else
            {
                if (decimal.Parse(txt一般情况_体温.Txt1.Text) > 38 || decimal.Parse(txt一般情况_体温.Txt1.Text) < 36)
                {
                    sbNotity.Append("体温：" + txt一般情况_体温.Txt1.Text + "，不在36--38之间\n");
                }    
            }
            //脉率
            if (string.IsNullOrEmpty(txt一般情况_脉率.Txt1.Text.Trim()))
            {
                if (Msg.AskQuestion("脉率值填写不完整，是否继续填写？"))
                {
                    this.txt一般情况_脉率.Txt1.Focus();
                    return false;
                }
                else
                    return true;
            }
            else
            {
                if (int.Parse(txt一般情况_脉率.Txt1.Text) > 100 || int.Parse(txt一般情况_脉率.Txt1.Text) < 60)
                {
                    sbNotity.Append("脉率：" + txt一般情况_脉率.Txt1.Text + "，不在60--100之间\n");
                }
            }
            //呼吸频率
            if (string.IsNullOrEmpty(txt一般情况_呼吸频率.Txt1.Text.Trim()))
            {
                if (Msg.AskQuestion("呼吸频率值填写不完整，是否继续填写？"))
                {
                    this.txt一般情况_呼吸频率.Txt1.Focus();
                    return false;
                }
                else
                    return true;
            }
            else
            {
                if (int.Parse(txt一般情况_呼吸频率.Txt1.Text) > 24 || int.Parse(txt一般情况_呼吸频率.Txt1.Text) < 12)
                {
                    sbNotity.Append("呼吸频率：" + txt一般情况_呼吸频率.Txt1.Text + "，不在12--24之间\n");
                }
            }
            //身高
            if (string.IsNullOrEmpty(txt一般情况_身高.Txt1.Text.Trim()))
            {
                if (Msg.AskQuestion("身高值填写不完整，是否继续填写？"))
                {
                    this.txt一般情况_身高.Txt1.Focus();
                    return false;
                }
                else
                    return true;
            }
            else
            {
                if (decimal.Parse(txt一般情况_身高.Txt1.Text) > 190 || decimal.Parse(txt一般情况_身高.Txt1.Text) < 150)
                {
                    sbNotity.Append("身高：" + txt一般情况_身高.Txt1.Text + "，不在150--190之间\n");
                }
            }
            //体重
            if (string.IsNullOrEmpty(txt一般情况_体重.Txt1.Text.Trim()))
            {
                if (Msg.AskQuestion("体重值填写不完整，是否继续填写？"))
                {
                    this.txt一般情况_体重.Txt1.Focus();
                    return false;
                }
                else
                    return true;
            }
            else
            {
                if (decimal.Parse(txt一般情况_体重.Txt1.Text) > 100 || decimal.Parse(txt一般情况_体重.Txt1.Text) < 40)
                {
                    sbNotity.Append("体重：" + txt一般情况_体重.Txt1.Text + "，不在40--100之间\n");
                }
            }
            //腰围
            if (string.IsNullOrEmpty(txt一般情况_腰围.Txt1.Text.Trim()))
            {
                if (Msg.AskQuestion("腰围值填写不完整，是否继续填写？"))
                {
                    this.txt一般情况_腰围.Txt1.Focus();
                    return false;
                }
                else
                    return true;
            }
            else
            {
                if (decimal.Parse(txt一般情况_腰围.Txt1.Text) > 100 || decimal.Parse(txt一般情况_腰围.Txt1.Text) < 50)
                {
                    sbNotity.Append("腰围：" + txt一般情况_腰围.Txt1.Text + "，不在50--100之间\n");
                }
            }
            //体重指数
            if (string.IsNullOrEmpty(txt一般情况_体重指数.Txt1.Text.Trim()))
            {
                if (Msg.AskQuestion("体重指数值填写不完整，是否继续填写？"))
                {
                    this.txt一般情况_体重指数.Txt1.Focus();
                    return false;
                }
                else
                    return true;
            }
            else
            {
                if (double.Parse(txt一般情况_体重指数.Txt1.Text) > 28 || double.Parse(txt一般情况_体重指数.Txt1.Text) < 18.5)
                {
                    sbNotity.Append("体重指数：" + txt一般情况_体重指数.Txt1.Text + "，不在18.5--28之间\n");
                }
            }
            //锻炼时间
            if (!string.IsNullOrEmpty(txt生活方式_每次锻炼时间.Txt1.Text.Trim()))
            {
                if (int.Parse(txt生活方式_每次锻炼时间.Txt1.Text) > 120 || int.Parse(txt生活方式_每次锻炼时间.Txt1.Text) < 10)
                {
                    sbNotity.Append("锻炼时间：" + txt生活方式_每次锻炼时间.Txt1.Text + "，不在10--120min之间\n");
                }
            }
            //日吸烟量
            if (!string.IsNullOrEmpty(txt日吸烟量.Txt1.Text.Trim()))
            {
                if (int.Parse(txt日吸烟量.Txt1.Text) > 20 || int.Parse(txt日吸烟量.Txt1.Text) < 0)
                {
                    sbNotity.Append("日吸烟量：" + txt日吸烟量.Txt1.Text + "，不在0--20之间\n");
                }
            }
            //日饮酒量
            if (!string.IsNullOrEmpty(txt日饮酒量.Txt1.Text.Trim()))
            {
                if (int.Parse(txt日饮酒量.Txt1.Text) > 10 || int.Parse(txt日饮酒量.Txt1.Text) < 0)
                {
                    sbNotity.Append("日饮酒量：" + txt日饮酒量.Txt1.Text + "，不在0--10之间\n");
                }
            }
            //提示超范围数据
            if (sbNotity.Length > 0)
            {
                return Msg.AskQuestion("请确认下方数据：\r\n" + sbNotity.ToString() + "确定要保存吗？");
            }
            return true;

        }
        private int Get缺项(DataRow row)
        {
            int i = 0;
            if (string.IsNullOrEmpty(row[tb_健康体检.症状].ToString())) i++;
            if (string.IsNullOrEmpty(row[tb_健康体检.体检日期].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.FIELD2].ToString())) i++;
            #region 一般状况
            if (row[tb_健康体检.体温] == DBNull.Value) i++;
            if (row[tb_健康体检.脉搏] == DBNull.Value) i++;

            if (row[tb_健康体检.呼吸] == DBNull.Value) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.血压左侧1].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.血压左侧2].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.血压右侧1].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.血压右侧2].ToString())) i++;

            if (row[tb_健康体检.身高] == DBNull.Value) i++;
            if (row[tb_健康体检.体重] == DBNull.Value) i++;
            if (row[tb_健康体检.腰围] == DBNull.Value) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.体重指数].ToString())) i++;

            #endregion

            #region 生活方式
            if (string.IsNullOrEmpty(row[tb_健康体检.锻炼频率].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.饮食习惯].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.吸烟状况].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.饮酒频率].ToString())) i++;
            if (string.IsNullOrEmpty(row[tb_健康体检.有无职业病].ToString())) i++;


            #endregion

            #region 脏器功能
            if (string.IsNullOrEmpty(row[tb_健康体检.齿列].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.咽部].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.左眼视力].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.右眼视力].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.左眼矫正].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.右眼矫正].ToString())) i++;

            #endregion

            #region 查体
            if (string.IsNullOrEmpty(row[tb_健康体检.皮肤].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.巩膜].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.淋巴结].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.罗音].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.心率].ToString())) i++;

            #endregion

            #region 现存主要健康问题
            if (string.IsNullOrEmpty(row[tb_健康体检.脑血管疾病].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.肾脏疾病].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.心脏疾病].ToString())) i++;

            //if (string.IsNullOrEmpty(row[tb_健康体检.血管疾病].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.眼部疾病].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.神经系统疾病].ToString())) i++;

            if (string.IsNullOrEmpty(row[tb_健康体检.其他系统疾病].ToString())) i++;

            #endregion

            #region 健康评价
            if (string.IsNullOrEmpty(row[tb_健康体检.健康评价].ToString())) i++;

            #endregion

            #region 健康指导
            if (string.IsNullOrEmpty(row[tb_健康体检.健康指导].ToString())) i++;
            return i;
            #endregion
        }
        /// <summary>
        /// 第一个checkbox是否选中来决定 后续的checkbook的enable状态
        /// </summary>
        /// <param name="firstChk"></param>
        public void SetControlEnable(CheckEdit firstChk)
        {
            bool flag = firstChk.Checked;
            FlowLayoutPanel container = firstChk.Parent as FlowLayoutPanel;
            if (container == null) return;
            Control.ControlCollection controlList = container.Controls;
            for (int i = 1; i < controlList.Count; i++)
            {
                if (controlList[i] is CheckEdit)
                {
                    CheckEdit item = (CheckEdit)controlList[i];
                    item.Enabled = !flag;
                    if (flag)//选中了第一个控件
                    {
                        item.Checked = false;
                    }
                }
                else
                {
                    controlList[i].Enabled = false;
                }
            }
        }
        public void SetTextEditEnable(Control control, TextEdit textEdit)
        {
            if (control is CheckEdit)//CheckEdit 类型的其他
            {
                CheckEdit check = (CheckEdit)control;
                textEdit.Enabled = check.Checked;
                textEdit.Text = check.Checked ? textEdit.Text : "";
            }
            else if (true)
            {

            }
        }
        /// <summary>
        /// 设置FlowLayoutPanel中控件的  enable属性
        /// </summary>
        /// <param name="flowLayout"></param>
        /// <param name="p"></param>
        private void SetFlowControlEnable(FlowLayoutPanel flowLayout, bool p)
        {
            if (flowLayout == null || flowLayout.Controls.Count == 0) return;
            for (int i = 0; i < flowLayout.Controls.Count; i++)
            {
                Control item = flowLayout.Controls[i];
                item.Enabled = p;
            }
        }
        private void SetRadioGroupTxtEnable(RadioGroup radio, TextEdit txt)
        {
            string index = radio.EditValue == null ? "" : radio.EditValue.ToString();
            txt.Enabled = !string.IsNullOrEmpty(index) && index == "2" ? true : false;
            txt.Text = !string.IsNullOrEmpty(index) && index == "2" ? txt.Text : "";
        }
        //private object GetFlowLayoutResult(FlowLayoutPanel flow)
        //{
        //    if (flow == null || flow.Controls.Count == 0) return "";
        //    string result = "";
        //    for (int i = 0; i < flow.Controls.Count; i++)
        //    {
        //        if (flow.Controls[i].GetType() == typeof(CheckEdit))
        //        {
        //            CheckEdit chk = (CheckEdit)flow.Controls[i];
        //            if (chk == null) continue;
        //            if (chk.Checked)
        //            {
        //                result += chk.Tag + ",";
        //            }
        //        }
        //    }
        //    result = string.IsNullOrEmpty(result) ? result : result.Remove(result.Length - 1);
        //    return result;
        //}
        private int Get完整度(int i)
        {
            int k = Convert.ToInt32((48 - i) * 100 / 48.0);
            return k;
        }
        private int Get缺项()
        {
            int i = 0;
            if (this.dte体检日期.Text.Trim() == "") i++;
            if (this.text责任医生.Text.Trim() == "") i++;
            if (GetFlowLayoutResult(flow症状) == null) i++;

            #region 一般情况  考核项 9

            if (this.txt一般情况_体温.Txt1.Text.Trim() == "") i++;
            if (this.txt一般情况_脉率.Txt1.Text.Trim() == "") i++;
            if (this.txt一般情况_呼吸频率.Txt1.Text.Trim() == "") i++;
            if (this.txt一般情况_血压左侧.Txt1.Text.Trim() == "") i++;
            if (this.txt一般情况_血压右侧.Txt1.Text.Trim() == "") i++;
            if (this.txt一般情况_身高.Txt1.Text.Trim() == "") i++;
            if (this.txt一般情况_体重.Txt1.Text.Trim() == "") i++;
            if (this.txt一般情况_腰围.Txt1.Text.Trim() == "") i++;
            if (this.txt一般情况_体重指数.Txt1.Text.Trim() == "") i++;

            #endregion

            #region 生活方式 考核项5
            if (this.radio生活方式_锻炼频率.EditValue.ToString() == "") i++;
            if (GetFlowLayoutResult(flow饮食习惯).ToString() == "") i++;
            if (this.radio吸烟情况.EditValue.ToString() == "") i++;
            if (this.radio饮酒频率.EditValue.ToString() == "") i++;

            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.有无职业病].ToString() == "") i++;
            #endregion

            #region 脏器功能 考核项6
            if (this.radio口唇.EditValue.ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.齿列].ToString() == "") i++;
            if (this.txt左眼视力.EditValue == null && this.txt右眼视力.EditValue == null && this.txt矫正左眼视力.EditValue == null && this.txt矫正右眼视力.EditValue == null) i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.听力].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.运动功能].ToString() == "") i++;
            #endregion

            #region 查体  考核项16
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.皮肤].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.巩膜].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.淋巴结].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.桶状胸].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.呼吸音].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.罗音].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心率].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心律].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.杂音].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.压痛].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.包块].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.肝大].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.脾大].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.浊音].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.下肢水肿].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.足背动脉搏动].ToString() == "") i++;

            #endregion

            #region 老年人辅助检查考核项
            string str出生日期 = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期].ToString();
            if (!string.IsNullOrEmpty(str出生日期))
            {
                DateTime dt = DateTime.Now;
                if (DateTime.TryParse(str出生日期, out dt))
                {
                    if (DateTime.Now.Year - dt.Year >= 65)
                    {
                        string str腹部B超 = _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.B超].ToString();
                        string str其他B超 = _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.其他B超].ToString();
                        if (str腹部B超 == "未检测" && str其他B超 == "未检测") i++;
                        if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血清谷丙转氨酶].ToString() == "") i++;
                        if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血清谷草转氨酶].ToString() == "") i++;
                        if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.总胆红素].ToString() == "") i++;
                        if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血清肌酐].ToString() == "") i++;
                        if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血尿素氮].ToString() == "") i++;
                        if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.总胆固醇].ToString() == "") i++;
                        if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.甘油三酯].ToString() == "") i++;
                        if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血清低密度脂蛋白胆固醇].ToString() == "") i++;
                        if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血清高密度脂蛋白胆固醇].ToString() == "") i++;
                    }
                }
            }

            #endregion

            #region 现存主要健康问题  考核项7
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.脑血管疾病].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.肾脏疾病].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心脏疾病].ToString() == "") i++;
            //if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.血管疾病].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.眼部疾病].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.神经系统疾病].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.其他系统疾病].ToString() == "") i++;
            #endregion

            #region 健康评价和健康指导  考核项2
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.健康评价].ToString() == "") i++;
            if (_ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.健康指导].ToString() == "" && _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.危险因素控制].ToString() == "") i++;
            #endregion
            return i;
        }
        private void InitView()
        {
            SetControlEnable(this.chk症状_无症状);
            SetControlEnable(this.chk皮肤_正常);
            SetControlEnable(this.chk巩膜_正常);
            SetControlEnable(this.chk淋巴结_未触及);
            SetControlEnable(this.chk肛门指诊_未见异常);
            SetControlEnable(this.chk乳腺_未见异常);
            SetControlEnable(this.chk罗音_无);

            SetControlEnable(this.chk心电图_正常);
            SetControlEnable(this.chk脑血管疾病_未发现);
            SetControlEnable(this.chk肾脏疾病_未发现);
            SetControlEnable(this.chk心血管疾病_未发现);
            SetControlEnable(this.chk血管疾病_未发现);
            SetControlEnable(this.chk眼部疾病_未发现);
            //锻炼频率
            this.radio生活方式_锻炼频率.SelectedIndex = 3;
            this.txt生活方式_锻炼方式.Enabled = false;
            this.txt生活方式_坚持锻炼时间.Enabled = false;
            this.txt生活方式_每次锻炼时间.Enabled = false;
            this.txt一般情况_体重指数.Txt1.Enabled = false;

            this.txt一般情况_体温.Txt1.Properties.Mask.EditMask = @"###.##";
            this.txt一般情况_体温.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt一般情况_体温.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt一般情况_脉率.Txt1.Properties.Mask.EditMask = @"##,###";
            this.txt一般情况_脉率.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt一般情况_脉率.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt一般情况_呼吸频率.Txt1.Properties.Mask.EditMask = @"##,###";
            this.txt一般情况_呼吸频率.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt一般情况_呼吸频率.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt一般情况_血压左侧.Txt1.Properties.Mask.EditMask = @"###";
            this.txt一般情况_血压左侧.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt一般情况_血压左侧.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt一般情况_血压左侧.Txt2.Properties.Mask.EditMask = @"###";
            this.txt一般情况_血压左侧.Txt2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt一般情况_血压左侧.Txt2.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt一般情况_血压右侧.Txt1.Properties.Mask.EditMask = @"###";
            this.txt一般情况_血压右侧.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt一般情况_血压右侧.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt一般情况_血压右侧.Txt2.Properties.Mask.EditMask = @"###";
            this.txt一般情况_血压右侧.Txt2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt一般情况_血压右侧.Txt2.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt一般情况_身高.Txt1.Properties.Mask.EditMask = @"###.##";
            this.txt一般情况_身高.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt一般情况_身高.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt一般情况_体重.Txt1.Properties.Mask.EditMask = @"###.##";
            this.txt一般情况_体重.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt一般情况_体重.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt一般情况_腰围.Txt1.Properties.Mask.EditMask = @"###.##";
            this.txt一般情况_腰围.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt一般情况_腰围.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt生活方式_每次锻炼时间.Txt1.Properties.Mask.EditMask = "n0";
            this.txt生活方式_每次锻炼时间.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt生活方式_每次锻炼时间.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt生活方式_每次锻炼时间.Txt1.Width = 60;
            this.txt生活方式_每次锻炼时间.Lbl1.Width = 50;

            this.txt生活方式_坚持锻炼时间.Txt1.Properties.Mask.EditMask = "n0";
            this.txt生活方式_坚持锻炼时间.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt生活方式_坚持锻炼时间.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt日吸烟量.Txt1.Properties.Mask.EditMask = "n0";
            this.txt日吸烟量.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt日吸烟量.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt开始吸烟年龄.Txt1.Properties.Mask.EditMask = "n0";
            this.txt开始吸烟年龄.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt开始吸烟年龄.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt戒烟年龄.Txt1.Properties.Mask.EditMask = "n0";
            this.txt戒烟年龄.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt戒烟年龄.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt开始饮酒年龄.Txt1.Properties.Mask.EditMask = "n0";
            this.txt开始饮酒年龄.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt开始饮酒年龄.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt日饮酒量.Txt1.Properties.Mask.EditMask = "n0";
            this.txt日饮酒量.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt日饮酒量.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt戒酒年龄.Txt1.Properties.Mask.EditMask = "n0";
            this.txt戒酒年龄.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt戒酒年龄.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            //this.txt左眼视力.Properties.Mask.EditMask = @"n1";
            //this.txt左眼视力.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt左眼视力.Properties.Mask.UseMaskAsDisplayFormat = true;

            //this.txt矫正左眼视力.Properties.Mask.EditMask = @"n1";
            //this.txt矫正左眼视力.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt矫正左眼视力.Properties.Mask.UseMaskAsDisplayFormat = true;

            //this.txt右眼视力.Properties.Mask.EditMask = @"n1";
            //this.txt右眼视力.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt右眼视力.Properties.Mask.UseMaskAsDisplayFormat = true;

            //this.txt矫正右眼视力.Properties.Mask.EditMask = @"n1";
            //this.txt矫正右眼视力.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt矫正右眼视力.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt心率.Txt1.Properties.Mask.EditMask = @"\d{1,5}";
            this.txt心率.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            //this.txt心率.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt血红蛋白.Txt1.Properties.Mask.EditMask = "n";
            this.txt血红蛋白.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt血红蛋白.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt白细胞.Txt1.Properties.Mask.EditMask = "n";
            this.txt白细胞.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt白细胞.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt血小板.Txt1.Properties.Mask.EditMask = "n";
            this.txt血小板.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt血小板.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt空腹血糖.Txt1.Properties.Mask.EditMask = @"(\d+\.\d{1,2}|\d+)";
            this.txt空腹血糖.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            //this.txt空腹血糖.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            //this.txt餐后2H血糖.Txt1.Properties.Mask.EditMask = @"(\d+\.\d{1,2}|\d+)"; ;
            //this.txt餐后2H血糖.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            //this.txt餐后2H血糖.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt尿微量白蛋白.Txt1.Properties.Mask.EditMask = "n";
            this.txt尿微量白蛋白.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt尿微量白蛋白.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt糖化血红蛋白.Txt1.Properties.Mask.EditMask = "n";
            this.txt糖化血红蛋白.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt糖化血红蛋白.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt血清谷丙转氨酶.Txt1.Properties.Mask.EditMask = "n";
            this.txt血清谷丙转氨酶.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt血清谷丙转氨酶.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt血清谷草转氨酶.Txt1.Properties.Mask.EditMask = "n";
            this.txt血清谷草转氨酶.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt血清谷草转氨酶.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt白蛋白.Txt1.Properties.Mask.EditMask = "n";
            this.txt白蛋白.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt白蛋白.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt总胆红素.Txt1.Properties.Mask.EditMask = "n";
            this.txt总胆红素.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt总胆红素.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt结合胆红素.Txt1.Properties.Mask.EditMask = "n";
            this.txt结合胆红素.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt结合胆红素.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt血清肌酐.Txt1.Properties.Mask.EditMask = "n";
            this.txt血清肌酐.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt血清肌酐.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt血尿素氮.Txt1.Properties.Mask.EditMask = "n";
            this.txt血尿素氮.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt血尿素氮.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt血钾浓度.Txt1.Properties.Mask.EditMask = "n";
            this.txt血钾浓度.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt血钾浓度.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt血钠浓度.Txt1.Properties.Mask.EditMask = "n";
            this.txt血钠浓度.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt血钠浓度.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt总胆固醇.Txt1.Properties.Mask.EditMask = "n";
            this.txt总胆固醇.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt总胆固醇.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt甘油三酯.Txt1.Properties.Mask.EditMask = "n";
            this.txt甘油三酯.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt甘油三酯.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt血清低密度脂蛋白胆固醇.Txt1.Properties.Mask.EditMask = "n";
            this.txt血清低密度脂蛋白胆固醇.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt血清低密度脂蛋白胆固醇.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt血清高密度脂蛋白胆固醇.Txt1.Properties.Mask.EditMask = "n";
            this.txt血清高密度脂蛋白胆固醇.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt血清高密度脂蛋白胆固醇.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            //this.txt尿蛋白.Properties.Mask.EditMask = "n";
            //this.txt尿蛋白.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt尿蛋白.Properties.Mask.UseMaskAsDisplayFormat = true;
            //this.txt尿糖.Properties.Mask.EditMask = "n";
            //this.txt尿糖.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt尿糖.Properties.Mask.UseMaskAsDisplayFormat = true;
            //this.txt鸟胴体.Properties.Mask.EditMask = "n";
            //this.txt鸟胴体.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt鸟胴体.Properties.Mask.UseMaskAsDisplayFormat = true;
            //this.txt尿潜血.Properties.Mask.EditMask = "n";
            //this.txt尿潜血.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt尿潜血.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt一般情况_体重指数.Txt1.Enabled = true;

            this.txt从业时间.Properties.Mask.EditMask = "n0";
            this.txt从业时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt从业时间.Properties.Mask.UseMaskAsDisplayFormat = true;
        }
        private void DobindingDatasource(DataSet _ds健康体检)
        {
            if (_ds健康体检 == null) return;
            DataTable dt = _ds健康体检.Tables[tb_健康体检.__TableName];
            if (dt.Rows.Count == 1)
            {
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.症状].ToString(), flow症状);
                this.txt症状_其他.EditValue = dt.Rows[0][tb_健康体检.症状其他];
                this.text责任医生.EditValue = dt.Rows[0][tb_健康体检.FIELD2];
                this.dte体检日期.EditValue = dt.Rows[0][tb_健康体检.体检日期];
                #region 一般情况
                this.txt一般情况_体温.Txt1.EditValue = dt.Rows[0][tb_健康体检.体温];
                this.txt一般情况_脉率.Txt1.EditValue = dt.Rows[0][tb_健康体检.脉搏];
                this.txt一般情况_呼吸频率.Txt1.EditValue = dt.Rows[0][tb_健康体检.呼吸];
                this.txt一般情况_血压左侧.Txt1.EditValue = dt.Rows[0][tb_健康体检.血压左侧1];
                this.txt一般情况_血压左侧.Txt2.EditValue = dt.Rows[0][tb_健康体检.血压左侧2];
                this.txt一般情况_血压右侧.Txt1.EditValue = dt.Rows[0][tb_健康体检.血压右侧1];
                this.txt一般情况_血压右侧.Txt2.EditValue = dt.Rows[0][tb_健康体检.血压右侧2];
                this.txt一般情况_血压左侧原因.EditValue = dt.Rows[0][tb_健康体检.左侧原因];
                this.txt一般情况_血压右侧原因.EditValue = dt.Rows[0][tb_健康体检.右侧原因];
                this.txt一般情况_腰围.Txt1.EditValue = dt.Rows[0][tb_健康体检.腰围];
                this.txt一般情况_身高.Txt1.EditValue = dt.Rows[0][tb_健康体检.身高];
                this.txt一般情况_体重.Txt1.EditValue = dt.Rows[0][tb_健康体检.体重];
                this.txt一般情况_体重指数.Txt1.EditValue = dt.Rows[0][tb_健康体检.体重指数];
                this.radio老年人健康状态评估.EditValue = dt.Rows[0][tb_健康体检.老年人状况评估];
                this.radio老年人生活自理能力评估.EditValue = dt.Rows[0][tb_健康体检.老年人自理评估];
                this.radio老年人情感状态.EditValue = dt.Rows[0][tb_健康体检.老年人情感];
                this.txt老年人情感状态总分.EditValue = dt.Rows[0][tb_健康体检.老年人情感分];
                this.radio老年人认知能力.EditValue = dt.Rows[0][tb_健康体检.老年人认知];
                this.txt老年人认知能力总分.EditValue = dt.Rows[0][tb_健康体检.老年人认知分];
                #endregion

                #region 生活方式
                this.radio生活方式_锻炼频率.EditValue = dt.Rows[0][tb_健康体检.锻炼频率];
                this.txt生活方式_每次锻炼时间.Txt1.EditValue = dt.Rows[0][tb_健康体检.每次锻炼时间];
                this.txt生活方式_坚持锻炼时间.Txt1.EditValue = dt.Rows[0][tb_健康体检.坚持锻炼时间];
                this.txt生活方式_锻炼方式.EditValue = dt.Rows[0][tb_健康体检.锻炼方式];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.饮食习惯].ToString(), flow饮食习惯);

                this.radio吸烟情况.EditValue = dt.Rows[0][tb_健康体检.吸烟状况];
                this.txt日吸烟量.Txt1.EditValue = dt.Rows[0][tb_健康体检.日吸烟量];
                this.txt开始吸烟年龄.Txt1.EditValue = dt.Rows[0][tb_健康体检.开始吸烟年龄];
                this.txt戒烟年龄.Txt1.EditValue = dt.Rows[0][tb_健康体检.戒烟年龄];

                this.radio饮酒频率.EditValue = dt.Rows[0][tb_健康体检.饮酒频率];
                this.txt日饮酒量.Txt1.EditValue = dt.Rows[0][tb_健康体检.日饮酒量];
                this.radio是否戒酒.EditValue = dt.Rows[0][tb_健康体检.是否戒酒];
                this.txt戒酒年龄.Txt1.EditValue = dt.Rows[0][tb_健康体检.戒酒年龄];
                this.txt开始饮酒年龄.Txt1.EditValue = dt.Rows[0][tb_健康体检.开始饮酒年龄];
                this.radio近一年内是否曾醉酒.EditValue = dt.Rows[0][tb_健康体检.近一年内是否曾醉酒];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.饮酒种类].ToString(), flow饮酒种类);
                this.txt饮酒种类_其他.EditValue = dt.Rows[0][tb_健康体检.饮酒种类其它];

                this.radio职业病有无.EditValue = dt.Rows[0][tb_健康体检.有无职业病] == null ? "1" : dt.Rows[0][tb_健康体检.有无职业病];

                this.txt工种.EditValue = dt.Rows[0][tb_健康体检.具体职业];
                this.txt从业时间.EditValue = dt.Rows[0][tb_健康体检.从业时间];
                this.txt粉尘.EditValue = dt.Rows[0][tb_健康体检.粉尘];
                this.radio粉尘防护措施有无.EditValue = dt.Rows[0][tb_健康体检.粉尘防护有无];
                this.txt粉尘防护措施.EditValue = dt.Rows[0][tb_健康体检.粉尘防护措施];
                this.txt放射物质.EditValue = dt.Rows[0][tb_健康体检.放射物质];
                this.radio放射物质防护措施有无.EditValue = dt.Rows[0][tb_健康体检.放射物质防护措施有无];
                this.txt放射物质防护措施.EditValue = dt.Rows[0][tb_健康体检.放射物质防护措施其他];
                this.txt物理因素.EditValue = dt.Rows[0][tb_健康体检.物理因素];
                this.radio物理防护措施有无.EditValue = dt.Rows[0][tb_健康体检.物理防护有无];
                this.txt物理防护措施.EditValue = dt.Rows[0][tb_健康体检.物理防护措施];
                this.txt化学因素.EditValue = dt.Rows[0][tb_健康体检.化学物质];
                this.radio化学防护措施有无.EditValue = dt.Rows[0][tb_健康体检.化学物质防护];
                this.txt化学防护措施.EditValue = dt.Rows[0][tb_健康体检.化学物质具体防护];
                this.txt职业病其他.EditValue = dt.Rows[0][tb_健康体检.职业病其他];
                this.radio职业病其他防护措施有无.EditValue = dt.Rows[0][tb_健康体检.其他防护有无];
                this.txt职业病其他防护.EditValue = dt.Rows[0][tb_健康体检.其他防护措施];

                #endregion

                #region 脏器功能
                this.radio口唇.EditValue = dt.Rows[0][tb_健康体检.口唇];
                this.txt口唇其他.EditValue = dt.Rows[0][tb_健康体检.口唇其他];
                string chilie = dt.Rows[0][tb_健康体检.齿列].ToString();
                if (!string.IsNullOrEmpty(chilie))
                {
                    this.chk齿列_正常.Checked = false;
                    string[] _strings = chilie.Split(',');
                    foreach (string item in _strings)
                    {
                        //10003219	cl_chilei	齿列	1		正常		1
                        //10003220	cl_chilei	齿列	2		缺齿		1
                        //10003221	cl_chilei	齿列	3		龋齿		1
                        //10003222	cl_chilei	齿列	4		义齿(假牙)		1
                        //10003223	cl_chilei	齿列	5	QT,JT	其他		1
                        switch (item)
                        {
                            case "1":
                                this.chk齿列_正常.Checked = true;
                                break;
                            case "2":
                                this.chk齿列_缺齿.Checked = true;
                                string quechi = dt.Rows[0][tb_健康体检.齿列缺齿].ToString();
                                if (!string.IsNullOrEmpty(quechi))
                                {
                                    string[] _queshis = quechi.Split('@');
                                    this.txt缺齿1.EditValue = _queshis[0];
                                    this.txt缺齿2.EditValue = _queshis[1];
                                    this.txt缺齿3.EditValue = _queshis[2];
                                    this.txt缺齿4.EditValue = _queshis[3];
                                }
                                break;
                            case "3":
                                this.chk齿列_龋齿.Checked = true;
                                string quchi = dt.Rows[0][tb_健康体检.齿列龋齿].ToString();
                                if (!string.IsNullOrEmpty(quchi))
                                {
                                    string[] _qushis = quchi.Split('@');
                                    this.txt龋齿1.EditValue = _qushis[0];
                                    this.txt龋齿2.EditValue = _qushis[1];
                                    this.txt龋齿3.EditValue = _qushis[2];
                                    this.txt龋齿4.EditValue = _qushis[3];
                                }
                                break;
                            case "4":
                                this.chk齿列_义齿.Checked = true;
                                string yichi = dt.Rows[0][tb_健康体检.齿列义齿].ToString();
                                if (!string.IsNullOrEmpty(yichi))
                                {
                                    string[] _yishis = yichi.Split('@');
                                    this.txt义齿1.EditValue = _yishis[0];
                                    this.txt义齿2.EditValue = _yishis[1];
                                    this.txt义齿3.EditValue = _yishis[2];
                                    this.txt义齿4.EditValue = _yishis[3];
                                }
                                break;
                            case "5":
                                this.chk齿列_其他.Checked = true;
                                this.txt齿列_其他.Text = dt.Rows[0][tb_健康体检.齿列其他].ToString();
                                break;
                            default:
                                break;
                        }
                    }
                }
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.咽部].ToString(), flow咽部);
                this.txt咽部_其他.EditValue = dt.Rows[0][tb_健康体检.咽部其他];
                this.txt左眼视力.EditValue = dt.Rows[0][tb_健康体检.左眼视力];
                this.txt右眼视力.EditValue = dt.Rows[0][tb_健康体检.右眼视力];
                this.txt矫正左眼视力.EditValue = dt.Rows[0][tb_健康体检.左眼矫正];
                this.txt矫正右眼视力.EditValue = dt.Rows[0][tb_健康体检.右眼矫正];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.听力].ToString(), flow听力);
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.运动功能].ToString(), flow运动功能);
                #endregion

                #region 查体
                this.radio眼底.EditValue = dt.Rows[0][tb_健康体检.眼底];
                this.txt眼底.EditValue = dt.Rows[0][tb_健康体检.眼底异常];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.皮肤].ToString(), flow皮肤);
                this.txt皮肤_其他.EditValue = dt.Rows[0][tb_健康体检.皮肤其他];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.巩膜].ToString(), flow巩膜);
                this.txt巩膜_其他.EditValue = dt.Rows[0][tb_健康体检.巩膜其他];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.淋巴结].ToString(), flow淋巴结);
                this.txt淋巴结_其他.EditValue = dt.Rows[0][tb_健康体检.淋巴结其他];

                this.radio桶状胸.EditValue = dt.Rows[0][tb_健康体检.桶状胸];
                this.radio呼吸音.EditValue = dt.Rows[0][tb_健康体检.呼吸音];
                this.txt呼吸音_异常.EditValue = dt.Rows[0][tb_健康体检.呼吸音异常];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.罗音].ToString(), flow罗音);
                this.txt罗音_其他.EditValue = dt.Rows[0][tb_健康体检.罗音异常];
                this.txt心率.Txt1.EditValue = dt.Rows[0][tb_健康体检.心率];
                this.radio心律.EditValue = dt.Rows[0][tb_健康体检.心律];
                this.radio杂音.EditValue = dt.Rows[0][tb_健康体检.杂音];
                this.txt杂音.EditValue = dt.Rows[0][tb_健康体检.杂音有];
                this.radio压痛.EditValue = dt.Rows[0][tb_健康体检.压痛];
                this.txt压痛.EditValue = dt.Rows[0][tb_健康体检.压痛有];
                this.radio包块.EditValue = dt.Rows[0][tb_健康体检.包块];
                this.txt包块.EditValue = dt.Rows[0][tb_健康体检.包块有];
                this.radio肝大.EditValue = dt.Rows[0][tb_健康体检.肝大];
                this.txt肝大.EditValue = dt.Rows[0][tb_健康体检.肝大有];
                this.radio脾大.EditValue = dt.Rows[0][tb_健康体检.脾大];
                this.txt脾大.EditValue = dt.Rows[0][tb_健康体检.脾大有];
                this.radio移动性浊音.EditValue = dt.Rows[0][tb_健康体检.浊音];
                this.txt移动性浊音.EditValue = dt.Rows[0][tb_健康体检.浊音有];

                this.radio下肢水肿.EditValue = dt.Rows[0][tb_健康体检.下肢水肿];
                this.radio足背动脉搏动.EditValue = dt.Rows[0][tb_健康体检.足背动脉搏动];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.肛门指诊].ToString(), flow肛门指诊);
                this.txt肛门指诊_其他.EditValue = dt.Rows[0][tb_健康体检.肛门指诊异常];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.乳腺].ToString(), flow乳腺);
                this.txt乳腺_其他.EditValue = dt.Rows[0][tb_健康体检.乳腺其他];
                this.radio外阴.EditValue = dt.Rows[0][tb_健康体检.外阴];
                this.txt外阴.EditValue = dt.Rows[0][tb_健康体检.外阴异常];
                this.radio阴道.EditValue = dt.Rows[0][tb_健康体检.阴道];
                this.txt阴道.EditValue = dt.Rows[0][tb_健康体检.阴道异常];
                this.radio宫颈.EditValue = dt.Rows[0][tb_健康体检.宫颈];
                this.txt宫颈.EditValue = dt.Rows[0][tb_健康体检.宫颈异常];
                this.radio宫体.EditValue = dt.Rows[0][tb_健康体检.宫体];
                this.txt宫体.EditValue = dt.Rows[0][tb_健康体检.宫体异常];
                this.radio附件.EditValue = dt.Rows[0][tb_健康体检.附件];
                this.txt附件.EditValue = dt.Rows[0][tb_健康体检.附件异常];
                this.txt查体_其他.EditValue = dt.Rows[0][tb_健康体检.查体其他];
                #endregion

                #region 辅助检查
                this.txt血红蛋白.Txt1.EditValue = dt.Rows[0][tb_健康体检.血红蛋白];
                this.txt白细胞.Txt1.EditValue = dt.Rows[0][tb_健康体检.白细胞];
                this.txt血小板.Txt1.EditValue = dt.Rows[0][tb_健康体检.血小板];
                this.txt血常规_其他.EditValue = dt.Rows[0][tb_健康体检.血常规其他];
                this.txt尿蛋白.EditValue = dt.Rows[0][tb_健康体检.尿蛋白];
                this.txt尿糖.EditValue = dt.Rows[0][tb_健康体检.尿糖];
                this.txt尿胴体.EditValue = dt.Rows[0][tb_健康体检.尿酮体];
                this.txt尿潜血.EditValue = dt.Rows[0][tb_健康体检.尿潜血];
                this.txt尿常规其他.EditValue = dt.Rows[0][tb_健康体检.尿常规其他];
                this.txt空腹血糖.Txt1.EditValue = dt.Rows[0][tb_健康体检.空腹血糖];
                this.txt空腹血糖2.Txt1.EditValue = dt.Rows[0][tb_健康体检.空腹血糖2];
                this.txt同型半胱氨酸.Txt1.EditValue = dt.Rows[0][tb_健康体检.同型半胱氨酸];
                //this.txt餐后2H血糖.Txt1.EditValue = dt.Rows[0][tb_健康体检.餐后2H血糖];
                //this.radio心电图.EditValue = dt.Rows[0][tb_健康体检.心电图];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.心电图].ToString(), flow心电图);
                this.txt心电图_其他.EditValue = dt.Rows[0][tb_健康体检.心电图异常];
                this.txt尿微量白蛋白.Txt1.EditValue = dt.Rows[0][tb_健康体检.尿微量白蛋白];
                this.radio大便潜血.EditValue = dt.Rows[0][tb_健康体检.大便潜血];
                this.txt糖化血红蛋白.Txt1.EditValue = dt.Rows[0][tb_健康体检.糖化血红蛋白];
                this.radio乙型肝炎表面抗原.EditValue = dt.Rows[0][tb_健康体检.乙型肝炎表面抗原];
                this.txt血清谷丙转氨酶.Txt1.EditValue = dt.Rows[0][tb_健康体检.血清谷丙转氨酶];
                this.txt血清谷草转氨酶.Txt1.EditValue = dt.Rows[0][tb_健康体检.血清谷草转氨酶];
                this.txt白蛋白.Txt1.EditValue = dt.Rows[0][tb_健康体检.白蛋白];
                this.txt总胆红素.Txt1.EditValue = dt.Rows[0][tb_健康体检.总胆红素];
                this.txt结合胆红素.Txt1.EditValue = dt.Rows[0][tb_健康体检.结合胆红素];
                this.txt血清肌酐.Txt1.EditValue = dt.Rows[0][tb_健康体检.血清肌酐];
                this.txt血尿素氮.Txt1.EditValue = dt.Rows[0][tb_健康体检.血尿素氮];
                this.txt血钾浓度.Txt1.EditValue = dt.Rows[0][tb_健康体检.血钾浓度];
                this.txt血钠浓度.Txt1.EditValue = dt.Rows[0][tb_健康体检.血钠浓度];
                this.txt总胆固醇.Txt1.EditValue = dt.Rows[0][tb_健康体检.总胆固醇];
                this.txt甘油三酯.Txt1.EditValue = dt.Rows[0][tb_健康体检.甘油三酯];
                this.txt血清低密度脂蛋白胆固醇.Txt1.EditValue = dt.Rows[0][tb_健康体检.血清低密度脂蛋白胆固醇];
                this.txt血清高密度脂蛋白胆固醇.Txt1.EditValue = dt.Rows[0][tb_健康体检.血清高密度脂蛋白胆固醇];
                this.radio胸部X线片.EditValue = dt.Rows[0][tb_健康体检.胸部X线片];
                this.txt胸部X线片.EditValue = dt.Rows[0][tb_健康体检.胸部X线片异常];
                this.radio腹部B超.EditValue = dt.Rows[0][tb_健康体检.B超];
                this.txt腹部B超异常.EditValue = dt.Rows[0][tb_健康体检.B超其他];
                this.radio其他B超.EditValue = dt.Rows[0][tb_健康体检.其他B超];
                this.txt其他B超异常.EditValue = dt.Rows[0][tb_健康体检.其他B超异常];
                this.radio宫颈涂片.EditValue = dt.Rows[0][tb_健康体检.宫颈涂片];
                this.txt宫颈涂片.EditValue = dt.Rows[0][tb_健康体检.宫颈涂片异常];
                this.txt辅助检查_其他.EditValue = dt.Rows[0][tb_健康体检.辅助检查其他];
                #endregion

                #region 中医体质辨识
                this.radio平和质.EditValue = dt.Rows[0][tb_健康体检.平和质];
                this.radio气虚质.EditValue = dt.Rows[0][tb_健康体检.气虚质];
                this.radio阳虚质.EditValue = dt.Rows[0][tb_健康体检.阳虚质];
                this.radio阴虚质.EditValue = dt.Rows[0][tb_健康体检.阴虚质];
                this.radio痰湿质.EditValue = dt.Rows[0][tb_健康体检.痰湿质];
                this.radio湿热质.EditValue = dt.Rows[0][tb_健康体检.湿热质];
                this.radio血瘀质.EditValue = dt.Rows[0][tb_健康体检.血瘀质];
                this.radio气郁质.EditValue = dt.Rows[0][tb_健康体检.气郁质];
                this.radio特秉质.EditValue = dt.Rows[0][tb_健康体检.特禀质];
                #endregion

                #region 现存主要健康问题
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.脑血管疾病].ToString(), flow脑血管疾病);
                this.txt脑血管疾病_其他.EditValue = dt.Rows[0][tb_健康体检.脑血管疾病其他];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.肾脏疾病].ToString(), flow肾脏疾病);
                this.txt肾脏疾病_其他.EditValue = dt.Rows[0][tb_健康体检.肾脏疾病其他];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.心脏疾病].ToString(), flow心血管疾病);
                this.txt心血管疾病_其他.EditValue = dt.Rows[0][tb_健康体检.心脏疾病其他];
                //SetFlowLayoutResult(dt.Rows[0][tb_健康体检.血管疾病].ToString(), flow血管疾病);
                //this.txt血管疾病_其他.EditValue = dt.Rows[0][tb_健康体检.血管疾病其他];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.眼部疾病].ToString(), flow眼部疾病);
                this.txt眼部疾病_其他.EditValue = dt.Rows[0][tb_健康体检.眼部疾病其他];
                //this.radio神经系统疾病.EditValue = dt.Rows[0][tb_健康体检.神经系统疾病];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.神经系统疾病].ToString(), flow神经系统疾病);
                this.txt神经系统疾病_其他.EditValue = dt.Rows[0][tb_健康体检.神经系统疾病其他];
                //this.radio其他系统疾病.EditValue = dt.Rows[0][tb_健康体检.其他系统疾病];
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.其他系统疾病].ToString(), flow其他系统疾病);
                this.txt其他系统疾病_其他.EditValue = dt.Rows[0][tb_健康体检.其他系统疾病其他];
                #endregion

                #region 健康评价
                this.radio健康评价.EditValue = dt.Rows[0][tb_健康体检.健康评价];
                this.txt体检异常1.EditValue = dt.Rows[0][tb_健康体检.健康评价异常1];
                this.txt体检异常2.EditValue = dt.Rows[0][tb_健康体检.健康评价异常2];
                this.txt体检异常3.EditValue = dt.Rows[0][tb_健康体检.健康评价异常3];
                this.txt体检异常4.EditValue = dt.Rows[0][tb_健康体检.健康评价异常4];

                #endregion

                #region 健康指导
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.健康指导].ToString(), flow健康指导);
                SetFlowLayoutResult(dt.Rows[0][tb_健康体检.危险因素控制].ToString(), flow危险因素控制);
                this.txt减体重目标.EditValue = dt.Rows[0][tb_健康体检.危险因素控制体重];
                this.txt建议疫苗接种.EditValue = dt.Rows[0][tb_健康体检.危险因素控制疫苗];
                this.txt危险因素其他.EditValue = dt.Rows[0][tb_健康体检.危险因素控制其他];
                #endregion

                #region 创建人信息
                this.dte创建时间.EditValue = dt.Rows[0][tb_健康体检.创建时间];
                this.txt创建人.EditValue = _bllUser.Return用户名称(dt.Rows[0][tb_健康体检.创建人].ToString());
                this.dte最近更新时间.EditValue = dt.Rows[0][tb_健康体检.修改时间];
                this.txt创建机构.EditValue = _Bll.Return机构名称(dt.Rows[0][tb_健康体检.创建机构].ToString());
                this.txt当前所属机构.EditValue = _Bll.Return机构名称(dt.Rows[0][tb_健康体检.所属机构].ToString());
                this.txt最近修改人.EditValue = _bllUser.Return用户名称(dt.Rows[0][tb_健康体检.修改人].ToString());
                #endregion

                Set考核项(dt.Rows[0]);
            }
        }
        private void Set考核项(DataRow row)
        {

            if (string.IsNullOrEmpty(row[tb_健康体检.症状].ToString()))
            {
                this.group症状.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.体检日期].ToString()))
            {
                this.lbl体检日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.FIELD2].ToString()))
            {
                this.lbl责任医生.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            #region 一般状况
            if (row[tb_健康体检.体温] == DBNull.Value)
            {
                this.lbl体温.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (row[tb_健康体检.脉搏] == DBNull.Value)
            {
                this.lbl脉率.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (row[tb_健康体检.呼吸] == DBNull.Value)
            {
                this.lbl呼吸频率.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.血压左侧1].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.血压左侧2].ToString()))
            {
                this.lbl血压左侧.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.血压右侧1].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.血压右侧2].ToString()))
            {
                this.lbl血压右侧.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (row[tb_健康体检.身高] == DBNull.Value)
            {
                this.lbl身高.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (row[tb_健康体检.体重] == DBNull.Value)
            {
                this.lbl体重.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }

            if (row[tb_健康体检.腰围] == DBNull.Value)
            {
                this.lbl腰围.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.体重指数].ToString()))
            {
                this.lbl体重指数.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            #endregion

            #region 生活方式
            if (string.IsNullOrEmpty(row[tb_健康体检.锻炼频率].ToString()))
            {
                this.lbl锻炼频率.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.饮食习惯].ToString()))
            {
                this.lbl饮食习惯.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.吸烟状况].ToString()))
            {
                this.lbl吸烟状况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }

            if (string.IsNullOrEmpty(row[tb_健康体检.饮酒频率].ToString()))
            {
                this.lbl饮酒情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }


            if (string.IsNullOrEmpty(row[tb_健康体检.有无职业病].ToString()))
            {
                this.lbl职业病危害因素接触史.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }

            #endregion

            #region 脏器功能
            if (string.IsNullOrEmpty(row[tb_健康体检.齿列].ToString()))
            {
                this.lbl齿列总.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.咽部].ToString()))
            {
                this.lbl咽部.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.左眼视力].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.右眼视力].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.左眼矫正].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.右眼矫正].ToString()))
            {
                this.lbl视力.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            #endregion

            #region 查体
            if (string.IsNullOrEmpty(row[tb_健康体检.皮肤].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.皮肤其他].ToString()))
            {
                this.lbl皮肤.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.巩膜].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.巩膜其他].ToString()))
            {
                this.lbl巩膜.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.淋巴结].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.淋巴结其他].ToString()))
            {
                this.lbl淋巴结.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.罗音].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.罗音异常].ToString()))
            {
                this.lbl罗音.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.心率].ToString()))
            {
                this.lbl心率.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            #endregion

            #region 老年人辅助检查填写考核项
            string str出生日期 = _ds健康档案ForShow.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期].ToString();
            if (!string.IsNullOrEmpty(str出生日期))
            {
                DateTime dt = DateTime.Now;
                if (DateTime.TryParse(str出生日期, out dt))
                {
                    if (DateTime.Now.Year - dt.Year >= 65)
                    {
                        if (string.IsNullOrEmpty(row[tb_健康体检.血清谷丙转氨酶].ToString()))
                        {
                            this.lbl血清谷丙转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清谷丙转氨酶.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血清谷丙转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清谷丙转氨酶.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.血清谷草转氨酶].ToString()))
                        {
                            this.lbl血清谷草转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清谷草转氨酶.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血清谷草转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清谷草转氨酶.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.总胆红素].ToString()))
                        {
                            this.lbl总胆红素.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl总胆红素.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl总胆红素.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl总胆红素.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.血清肌酐].ToString()))
                        {
                            this.lbl血清肌酐.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清肌酐.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血清肌酐.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清肌酐.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.血尿素氮].ToString()))
                        {
                            this.lbl血尿素氮.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血尿素氮.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血尿素氮.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血尿素氮.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.总胆固醇].ToString()))
                        {
                            this.lbl总胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl总胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl总胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl总胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.甘油三酯].ToString()))
                        {
                            this.lbl甘油三酯.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl甘油三酯.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl甘油三酯.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl甘油三酯.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.血清低密度脂蛋白胆固醇].ToString()))
                        {
                            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.血清高密度脂蛋白胆固醇].ToString()))
                        {
                            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (row[tb_健康体检.B超].ToString() == "3" || (row[tb_健康体检.B超].ToString() == "2" && string.IsNullOrEmpty(row[tb_健康体检.B超其他].ToString())))
                        {
                            this.lbl腹部B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl腹部B超.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl腹部B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl腹部B超.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (row[tb_健康体检.其他B超].ToString() == "2" && string.IsNullOrEmpty(row[tb_健康体检.其他B超异常].ToString()))
                        {
                            this.lbl其他B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl其他B超.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl其他B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl其他B超.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                    }
                }

            }
            #endregion

            #region 现存主要健康问题
            if (string.IsNullOrEmpty(row[tb_健康体检.脑血管疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.脑血管疾病其他].ToString()))
            {
                this.lbl脑血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.肾脏疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.肾脏疾病其他].ToString()))
            {
                this.lbl肾脏疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.心脏疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.心脏疾病其他].ToString()))
            {
                this.lbl心血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            //if (string.IsNullOrEmpty(row[tb_健康体检.血管疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.血管疾病其他].ToString()))
            //{
            //    this.lbl血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            //}
            if (string.IsNullOrEmpty(row[tb_健康体检.眼部疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.眼部疾病其他].ToString()))
            {
                this.lbl眼部疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.神经系统疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.神经系统疾病其他].ToString()))
            {
                this.lbl神经系统疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.其他系统疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.其他系统疾病其他].ToString()))
            {
                this.lbl其他系统疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            #endregion

            #region 健康评价
            if (string.IsNullOrEmpty(row[tb_健康体检.健康评价].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.健康评价异常1].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.健康评价异常2].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.健康评价异常3].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.健康评价异常4].ToString()))
            {
                this.group健康评价.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            #endregion

            #region 健康指导
            if (string.IsNullOrEmpty(row[tb_健康体检.健康指导].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.危险因素控制].ToString()))
            {
                this.group健康指导.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }

            #endregion
        }
        /// <summary>
        /// 设置控件的可用性
        /// </summary>
        /// <param name="index"></param>
        private void Set齿列ControlEditable(bool index)
        {
            //if (index)//勾选正常
            //{

            //}

            this.chk齿列_缺齿.Checked = index ? !index : index;
            this.chk齿列_缺齿.Enabled = !index;
            this.txt缺齿1.Enabled = this.chk齿列_缺齿.Enabled && this.chk齿列_缺齿.Checked;
            this.txt缺齿2.Enabled = this.chk齿列_缺齿.Enabled && this.chk齿列_缺齿.Checked;
            this.txt缺齿3.Enabled = this.chk齿列_缺齿.Enabled && this.chk齿列_缺齿.Checked;
            this.txt缺齿4.Enabled = this.chk齿列_缺齿.Enabled && this.chk齿列_缺齿.Checked;
            this.txt缺齿1.Text = index ? "" : this.txt缺齿1.Text;
            this.txt缺齿2.Text = index ? "" : this.txt缺齿2.Text;
            this.txt缺齿3.Text = index ? "" : this.txt缺齿3.Text;
            this.txt缺齿4.Text = index ? "" : this.txt缺齿4.Text;

            this.chk齿列_龋齿.Checked = index ? !index : index;
            this.chk齿列_龋齿.Enabled = !index;
            this.txt龋齿1.Enabled = this.chk齿列_龋齿.Enabled && this.chk齿列_龋齿.Checked;
            this.txt龋齿2.Enabled = this.chk齿列_龋齿.Enabled && this.chk齿列_龋齿.Checked;
            this.txt龋齿3.Enabled = this.chk齿列_龋齿.Enabled && this.chk齿列_龋齿.Checked;
            this.txt龋齿4.Enabled = this.chk齿列_龋齿.Enabled && this.chk齿列_龋齿.Checked;
            this.txt龋齿1.Text = index ? "" : this.txt龋齿1.Text;
            this.txt龋齿2.Text = index ? "" : this.txt龋齿2.Text;
            this.txt龋齿3.Text = index ? "" : this.txt龋齿3.Text;
            this.txt龋齿4.Text = index ? "" : this.txt龋齿4.Text;
            this.chk齿列_义齿.Checked = index ? !index : index;
            this.chk齿列_义齿.Enabled = !index;
            this.txt义齿1.Enabled = this.chk齿列_义齿.Enabled && this.chk齿列_义齿.Checked;
            this.txt义齿2.Enabled = this.chk齿列_义齿.Enabled && this.chk齿列_义齿.Checked;
            this.txt义齿3.Enabled = this.chk齿列_义齿.Enabled && this.chk齿列_义齿.Checked;
            this.txt义齿4.Enabled = this.chk齿列_义齿.Enabled && this.chk齿列_义齿.Checked;
            this.txt义齿1.Text = index ? "" : this.txt义齿1.Text;
            this.txt义齿2.Text = index ? "" : this.txt义齿2.Text;
            this.txt义齿3.Text = index ? "" : this.txt义齿3.Text;
            this.txt义齿4.Text = index ? "" : this.txt义齿4.Text;
            this.chk齿列_其他.Checked = index ? !index : index;
            this.chk齿列_其他.Enabled = !index;
            this.txt齿列_其他.Enabled = this.chk齿列_其他.Enabled && this.chk齿列_其他.Checked;
            this.txt齿列_其他.Text = index ? "" : this.txt齿列_其他.Text;
        }
        #endregion

        private void btn读取化验信息_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(text身份证号.Text))
            { Msg.ShowInformation("请完善身份证信息！"); return; }

            DataTable dt列表 = bll获取化验信息.Get化验信息列表(this.text身份证号.Text.ToString());
            if (dt列表 != null && dt列表.Rows.Count > 0)
            {
                DataTable dt明细 = null;
                InterFace化验结果 intf = new InterFace化验结果(dt列表);
                if (intf.ShowDialog() == DialogResult.OK)
                {
                    dt明细 = bll获取化验信息.Get化验信息(this.text身份证号.Text.ToString(), intf.s化验日期);
                }
                else
                {
                    Msg.ShowInformation("用户取消了选择！");
                    return;
                }
                //Msg.ShowInformation(dt.Rows.Count.ToString());
                foreach (DataRow dr in dt明细.Rows)
                {
                    #region 血常规
                    if (dr["fitem_code"].ToString() == "HGB" || dr["fname_j"].ToString() == "HGB")
                    {//血红蛋白
                        this.txt血红蛋白.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "WBC" || dr["fname_j"].ToString() == "WBC")
                    {//白细胞
                        this.txt白细胞.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "PLT" || dr["fname_j"].ToString() == "PLT")
                    {//血小板
                        this.txt血小板.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion

                    #region 尿常规 未做
                    if (dr["fitem_code"].ToString() == "PRO" || dr["fname_j"].ToString() == "PRO")
                    {//尿蛋白
                        this.txt尿蛋白.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "GLU1" || dr["fname_j"].ToString() == "GLU1")
                    {//尿糖
                        this.txt尿糖.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "KET" || dr["fname_j"].ToString() == "KET")
                    {//尿酮体：KET 
                        this.txt尿胴体.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "OB" || dr["fname_j"].ToString() == "OB")
                    {//尿潜血：OB--Occult Blood
                        this.txt尿潜血.Text = dr["fvalue"].ToString();
                    }
                    #endregion

                    #region 血糖/葡萄糖 空腹血糖、餐后2H血糖、尿微量白蛋白、糖化血红蛋白
                    if (dr["fitem_code"].ToString() == "GLU" || dr["fname_j"].ToString() == "GLU")
                    {//空腹血糖
                        this.txt空腹血糖.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "mAlb" || dr["fname_j"].ToString() == "mAlb")
                    {//尿微量白蛋白英文缩写:mAlb
                        this.txt尿微量白蛋白.Txt1.Text = dr["fvalue"].ToString();
                    }
                    //if (dr["fitem_code"].ToString() == "PBC2h" || dr["fname_j"].ToString() == "PBC2h")
                    //{//餐后2小时血糖(PBC2h)
                    //    this.txt餐后2H血糖.Txt1.Text = dr["fvalue"].ToString();
                    //}
                    if (dr["fitem_code"].ToString() == "HbA1c" || dr["fname_j"].ToString() == "HbA1c")
                    {//糖化血红蛋白：HbA1c
                        this.txt糖化血红蛋白.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion

                    #region 肝功能
                    if (dr["fitem_code"].ToString() == "ALT" || dr["fname_j"].ToString() == "ALT")
                    {//谷丙转氨酶
                        this.txt血清谷丙转氨酶.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "AST" || dr["fname_j"].ToString() == "AST")
                    {//谷草转氨酶
                        this.txt血清谷草转氨酶.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "ALB" || dr["fname_j"].ToString() == "ALB")
                    {//白蛋白
                        this.txt白蛋白.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "TBIL" || dr["fname_j"].ToString() == "TBIL")
                    {//总胆红素
                        this.txt总胆红素.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "DBIL" || dr["fname_j"].ToString() == "DBIL")
                    {//直接胆红素
                        this.txt结合胆红素.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion

                    #region 肾功能
                    if (dr["fitem_code"].ToString() == "CRE" || dr["fname_j"].ToString() == "CRE")
                    {//肌酐
                        this.txt血清肌酐.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "UREA" || dr["fname_j"].ToString() == "UREA")
                    {//尿素氮
                        this.txt血尿素氮.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "K+" || dr["fname_j"].ToString() == "K+")
                    {//血钾浓度
                        this.txt血钾浓度.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "Na+" || dr["fname_j"].ToString() == "Na+")
                    {//血钠浓度
                        this.txt血钠浓度.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion

                    #region 血脂
                    if (dr["fitem_code"].ToString() == "CHO" || dr["fname_j"].ToString() == "CHO")
                    {//总胆固醇
                        this.txt总胆固醇.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "TG" || dr["fname_j"].ToString() == "TG")
                    {//甘油三酯
                        this.txt甘油三酯.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "LDL-C" || dr["fname_j"].ToString() == "LDL_C" || dr["fname_j"].ToString() == "LDL-C")
                    {//低密度脂蛋白
                        this.txt血清低密度脂蛋白胆固醇.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "HDL-C" || dr["fname_j"].ToString() == "HDL_C" || dr["fname_j"].ToString() == "HDL-C")
                    {//高密度脂蛋
                        this.txt血清高密度脂蛋白胆固醇.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion
                }
            }
            else { Msg.ShowInformation("未获取到化验信息！"); }

            try
            {
                Calculate();
            }
            catch
            {
            }
        }


        private void btn生成健康评价_Click(object sender, EventArgs e)
        {
            try
            {
                Calculate();
            }
            catch
            {
            }
        }

        private void Calculate()
        {
            StringBuilder 症状temp = new StringBuilder();

            StringBuilder 危险因素temp = new StringBuilder();

            if (chk症状_头痛.Checked || chk症状_头晕.Checked || chk症状_恶心呕吐.Checked || chk症状_视力模糊.Checked || chk症状_心悸.Checked || chk症状_胸闷.Checked)
            {
                chk建议转诊.Checked = true;
            }

            #region 是否慢病
            bool _是否高血压 = false;
            bool _是否糖尿病 = false;
            bool _是否老年人 = false;

            if (_ds健康体检.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count > 0)//存在管理卡，为高血压患者
            {
                _是否高血压 = true;
            }

            if (_ds健康体检.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Count > 0)//存在管理卡，为糖尿病患者
            {
                _是否糖尿病 = true;
            }
            if (!string.IsNullOrEmpty(this.text出生日期.Text) && DateTime.Compare(DateTime.Now.AddYears(-65), Convert.ToDateTime(this.text出生日期.Text)) > 0)
            {
                _是否老年人 = true;
            }

            if (_ds健康体检.Tables[tb_健康档案_既往病史.__TableName].Rows.Count > 0)
            {//判断精神病
                foreach (DataRow dr in _ds健康体检.Tables[tb_健康档案_既往病史.__TableName].Rows)
                {
                    string 疾病 = dr["D_JBBM"].ToString();
                    if (疾病.Contains("高血压") || 疾病.Contains("糖尿病") || 疾病.Contains("重性精神疾病"))
                    {
                        chk纳入慢性病管理.Checked = true;
                        if (疾病.Contains("高血压"))
                        {
                            chk心血管疾病_未发现.Checked = false;
                            chk心血管疾病_高血压.Checked = true;
                        }
                    }
                }
            }
            #endregion

            #region BMI
            //BMI
            /*
                过轻：低于18.5
                正常：18.5-23.9
                超重：≥24
             * 24～27.9 偏胖
                肥胖：28-32
                非常肥胖, 高于32
             */
            string jianyw = "";
            if (!string.IsNullOrEmpty(this.txt一般情况_体重指数.Txt1.Text))
            {
                double bmi = double.Parse(this.txt一般情况_体重指数.Txt1.Text);
                if (bmi < 18.5)
                {
                    症状temp.Append("偏轻 ;");
                }
                else if (bmi >= 24 && bmi < 28)
                {
                    症状temp.Append("超重;");
                }
                else if (bmi >= 28 && bmi < 32)
                {
                    症状temp.Append("肥胖;");
                    jianyw = "减腰围,";
                }
                else if (bmi >= 32)
                {
                    症状temp.Append("非常肥胖;");
                    jianyw = "减腰围,";
                }
            }
            //新规范内容 2018年11月6日 yfh
            decimal yw;
            if (!string.IsNullOrEmpty(this.txt一般情况_腰围.Txt1.Text) && decimal.TryParse(this.txt一般情况_腰围.Txt1.Text, out yw))
            {
                if ((txt性别.Text == "男") && yw >= 90 || (txt性别.Text == "女" && yw >= 85))
                {
                    症状temp.Append("腹型肥胖;");
                    jianyw = "减腰围,";
                }
            }
            if (!string.IsNullOrEmpty(jianyw) && !危险因素temp.ToString().Contains("减腰围"))
            {
                危险因素temp.Append(jianyw);
            }

            #endregion

            #region 糖尿病

            if (!string.IsNullOrEmpty(this.txt空腹血糖.Txt1.Text))
            {
                string xuetang = this.txt空腹血糖.Txt1.Text.Trim();
                decimal _xuetang = 0m;
                if (Decimal.TryParse(xuetang, out _xuetang))
                {
                    if (_是否糖尿病)
                    {
                        if (_xuetang >= 7.0m) //糖尿病判断
                        {
                            症状temp.Append("血糖偏高;");
                            chk建议复查.Checked = true;
                        }
                        else if (_xuetang > 3.9m && _xuetang < 7.0m)
                        {
                            症状temp.Append("血糖正常，药物控制;");
                        }
                        else
                        {
                            症状temp.Append("低血糖;");
                        }
                    }
                    else
                    {
                        //判断血糖高低值 -正常人群
                        if (_xuetang != 0m && _xuetang > 6.10m)
                        {
                            症状temp.Append("血糖偏高;");
                            chk建议复查.Checked = true;
                        }
                        else if (_xuetang != 0m && _xuetang < 3.9m)
                        {
                            症状temp.Append("低血糖;");
                        }
                    }
                    if (_xuetang != 0m && (_xuetang >= 16.70m || _xuetang <= 3.9m))
                    {
                        chk建议转诊.Checked = true;
                    }
                }
            }

            #endregion

            #region 血压

            if (!string.IsNullOrEmpty(this.txt一般情况_血压右侧.Txt1.Text) || !string.IsNullOrEmpty(this.txt一般情况_血压左侧.Txt1.Text))
            {
                //右侧
                if (!string.IsNullOrEmpty(this.txt一般情况_血压右侧.Txt1.Text))
                {
                    string you1 = this.txt一般情况_血压右侧.Txt1.Text.Trim();
                    string you2 = this.txt一般情况_血压右侧.Txt2.Text.Trim();
                    int _you1; int _you2;
                    if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
                    {
                        if (_是否老年人) //2018年5月2日 yuhf 添加老年人判断
                        {
                            if (_you1 >= 150 || _you2 >= 90)
                            {
                                症状temp.Append("血压偏高;");
                            }
                            else if ((_you1 < 150 || _you2 < 90) && _是否高血压)
                            {
                                症状temp.Append("血压正常，药物控制;");
                            }
                        }
                        else
                        {
                            if (_you1 >= 140 || _you2 >= 90)
                            {
                                症状temp.Append("血压偏高;");
                            }
                            else if ((_you1 < 140 || _you2 < 90) && _是否高血压)
                            {
                                症状temp.Append("血压正常，药物控制;");
                            }
                        }
                        //同血糖一起添加,需要转诊判断:血压>=180 或 <=110
                        if (_you1 >= 180 || _you1 <= 110)
                        {
                            chk建议转诊.Checked = true;
                        }
                    }
                }
                ////左侧
                //else if (!string.IsNullOrEmpty(this.txt一般情况_血压左侧.Txt1.Text))
                //{
                //    string you1 = this.txt一般情况_血压左侧.Txt1.Text.Trim();
                //    string you2 = this.txt一般情况_血压左侧.Txt2.Text.Trim();
                //    int _you1; int _you2;
                //    if (int.TryParse(you1, out _you1) && int.TryParse(you2, out _you2))
                //    {
                //        if (_you1 > 140 || _you2 > 90)
                //        {
                //            症状temp.AppendLine("血压偏高;");
                //        }
                //        else if ((_you1 <= 140 || _you2 <= 90) && _是否高血压)
                //        {
                //            症状temp.Append("血压正常，药物控制;");
                //        }
                //    }
                //}
            }

            #endregion

            #region 血常规
            /*
             * 白细胞4-10；血红蛋白男性120-160，女性110-150；血小板100-300。这是统一的标准
             */
            if (!string.IsNullOrEmpty(txt白细胞.Txt1.Text) || !string.IsNullOrEmpty(txt血红蛋白.Txt1.Text) || !string.IsNullOrEmpty(txt血小板.Txt1.Text))
            {
                string baixibao = txt白细胞.Txt1.Text.Trim();
                string xuehongdanbai = txt血红蛋白.Txt1.Text.Trim();
                string xuexiaoban = txt血小板.Txt1.Text.Trim();
                decimal _baixibao;
                decimal _xuehongdanbai;
                decimal _xuexiaoban;
                string text = "血常规异常：";
                if (decimal.TryParse(baixibao, out _baixibao))
                {
                    if (_baixibao > 10m)
                    {
                        text += "白细胞:高";
                    }
                    else if (_baixibao < 4m)
                    {
                        text += "白细胞:低";
                    }
                }
                if (decimal.TryParse(xuehongdanbai, out _xuehongdanbai))
                {
                    if (_xuehongdanbai > 160m)
                    {
                        text += "血红蛋白:高";
                    }
                    else if (_xuehongdanbai < 110m)
                        text += "血红蛋白:低";
                }
                if (decimal.TryParse(xuexiaoban, out _xuexiaoban))
                {
                    if (_xuexiaoban > 300m)
                    {
                        text += "血小板:高";
                    }
                    else if (_xuexiaoban < 100m)
                        text += "血小板:低";
                }
                if (text != "血常规异常：")
                {
                    症状temp.Append(text + ";");
                    chk建议复查.Checked = true;
                }
            }

            #endregion

            #region 尿常规
            if (!string.IsNullOrEmpty(txt尿蛋白.Text) || !string.IsNullOrEmpty(txt尿胴体.Text) || !string.IsNullOrEmpty(txt尿潜血.Text) || !string.IsNullOrEmpty(txt尿糖.Text))
            {
                string text = "尿常规异常：";
                if (!string.IsNullOrEmpty(txt尿蛋白.Text) && txt尿蛋白.Text.Trim() != "-")
                {
                    text += "尿蛋白:" + txt尿蛋白.Text;
                }
                if (!string.IsNullOrEmpty(txt尿胴体.Text) && txt尿胴体.Text.Trim() != "-")
                {
                    text += "尿酮体:" + txt尿胴体.Text;
                }
                if (!string.IsNullOrEmpty(txt尿潜血.Text) && txt尿潜血.Text.Trim() != "-")
                {
                    text += "尿潜血:" + txt尿潜血.Text;
                }
                if (!string.IsNullOrEmpty(txt尿糖.Text) && txt尿糖.Text.Trim() != "-")
                {
                    text += "尿糖:" + txt尿糖.Text;
                }
                if (text != "尿常规异常：")
                {
                    症状temp.Append(text + ";");
                    chk建议复查.Checked = true;
                }
            }
            #endregion

            #region 肝功
            /*
             血清谷丙转氨酶：0-40
             血清谷草转氨酶：0-41
             总胆红素：2-20.5
             结合胆红素：0-7.0
             */
            if (!string.IsNullOrEmpty(txt血清谷丙转氨酶.Txt1.Text) || !string.IsNullOrEmpty(txt血清谷草转氨酶.Txt1.Text) || !string.IsNullOrEmpty(txt总胆红素.Txt1.Text) || !string.IsNullOrEmpty(txt结合胆红素.Txt1.Text))
            {
                string 血清谷丙转氨酶 = txt血清谷丙转氨酶.Txt1.Text.Trim();
                string 血清谷草转氨酶 = txt血清谷草转氨酶.Txt1.Text.Trim();
                string 总胆红素 = txt总胆红素.Txt1.Text.Trim();
                string 结合胆红素 = txt结合胆红素.Txt1.Text.Trim();
                decimal _血清谷丙转氨酶;
                decimal _血清谷草转氨酶;
                decimal _总胆红素;
                decimal _结合胆红素;
                string text = "肝功异常：";
                if (decimal.TryParse(血清谷丙转氨酶, out _血清谷丙转氨酶))
                {
                    if (_血清谷丙转氨酶 > 40m)
                    {
                        text += "谷丙转氨酶:高";
                    }
                    else if (_血清谷丙转氨酶 < 0m)
                        text += "谷丙转氨酶:低";
                }
                if (decimal.TryParse(血清谷草转氨酶, out _血清谷草转氨酶))
                {
                    if (_血清谷草转氨酶 > 34m)
                    {
                        text += "谷草转氨酶:高";
                    }
                    else if (_血清谷草转氨酶 < 0m)
                        text += "谷草转氨酶:低";
                }
                if (decimal.TryParse(总胆红素, out _总胆红素))
                {
                    if (_总胆红素 > 19m)
                    {
                        text += "总胆红素:高";
                    }
                    else if (_总胆红素 < 5.1m)
                        text += "总胆红素:低";
                }
                if (decimal.TryParse(结合胆红素, out _结合胆红素))
                {
                    if (_结合胆红素 > 6.8m)
                    {
                        text += "结合胆红素:高";
                    }
                    else if (_结合胆红素 < 1.7m)
                        text += "结合胆红素:低";
                }
                if (text != "肝功异常：")
                {
                    症状temp.Append(text + ";");
                    chk建议复查.Checked = true;
                }
            }
            #endregion

            #region 肾功
            /*
             血清肌酐:53-115
             血尿素氮:1.7-8.7 
             */
            if (!string.IsNullOrEmpty(txt血清肌酐.Txt1.Text) || !string.IsNullOrEmpty(txt血尿素氮.Txt1.Text))
            {
                string 血清肌酐 = txt血清肌酐.Txt1.Text.Trim();
                string 血尿素氮 = txt血尿素氮.Txt1.Text.Trim();
                decimal _血清肌酐;
                decimal _血尿素氮;
                string text = "肾功异常：";
                if (decimal.TryParse(血清肌酐, out _血清肌酐))
                {
                    if (_血清肌酐 > 115m)
                    {
                        text += "血清肌酐:高,";
                    }
                    else if (_血清肌酐 < 53m)
                        text += "血清肌酐:低,";
                }
                if (decimal.TryParse(血尿素氮, out _血尿素氮))
                {
                    if (_血尿素氮 > 8.2m)
                    {
                        text += "血尿素氮:高,";
                    }
                    else if (_血尿素氮 < 2.82m)
                        text += "血尿素氮:低,";
                }

                if (text != "肾功异常：")
                {
                    症状temp.Append(text + ";");
                    chk建议复查.Checked = true;
                }
            }
            #endregion

            #region 血脂
            /*
             总胆固醇：2.34-5.20
             甘油三酯：0.7-1.7
             高密度脂蛋白：0.77-2.25
             低密度脂蛋白：0-4.13
             */
            if (!string.IsNullOrEmpty(txt总胆固醇.Txt1.Text) || !string.IsNullOrEmpty(txt甘油三酯.Txt1.Text) || !string.IsNullOrEmpty(txt血清高密度脂蛋白胆固醇.Txt1.Text) || !string.IsNullOrEmpty(txt血清低密度脂蛋白胆固醇.Txt1.Text))
            {
                string 总胆固醇 = txt总胆固醇.Txt1.Text.Trim();
                string 甘油三酯 = txt甘油三酯.Txt1.Text.Trim();
                string 高密度脂蛋白 = txt血清高密度脂蛋白胆固醇.Txt1.Text.Trim();
                string 低密度脂蛋白 = txt血清低密度脂蛋白胆固醇.Txt1.Text.Trim();
                decimal _总胆固醇;
                decimal _甘油三酯;
                decimal _高密度脂蛋白;
                decimal _低密度脂蛋白;
                string text = "血脂异常：";
                if (decimal.TryParse(总胆固醇, out _总胆固醇))
                {
                    if (_总胆固醇 > 5.20m)
                    {
                        text += "总胆固醇:高,";
                    }
                    else if (_总胆固醇 < 2.34m)
                        text += "总胆固醇:低,";
                }
                if (decimal.TryParse(甘油三酯, out _甘油三酯))
                {
                    if (_甘油三酯 > 1.81m)
                    {
                        text += "甘油三酯:高,";
                    }
                    else if (_甘油三酯 < 0.4m)
                        text += "甘油三酯:低,";
                }
                if (decimal.TryParse(高密度脂蛋白, out _高密度脂蛋白))
                {
                    if (_高密度脂蛋白 > 1.89m)
                    {
                        text += "高密度脂蛋白:高,";
                    }
                    else if (_高密度脂蛋白 < 1.07m)
                        text += "高密度脂蛋白:低,";
                }
                if (decimal.TryParse(低密度脂蛋白, out _低密度脂蛋白))
                {
                    if (_低密度脂蛋白 > 3.1m)
                    {
                        text += "低密度脂蛋白:高,";
                    }
                    else if (_低密度脂蛋白 < 0m)
                        text += "低密度脂蛋白:低,";
                }
                if (text != "血脂异常：")
                {
                    症状temp.Append(text + ";");
                    chk建议复查.Checked = true;
                }
            }
            #endregion

            #region 心电图
            string xd = GetFlowLayoutResult(flow心电图).ToString();
            string name = SetFlowLayoutValues("jktj_xdt", xd);
            if (!string.IsNullOrEmpty(xd) && !xd.Contains("1"))
            {
                症状temp.Append("心电图异常：" + name + ";");
                if (txt心电图_其他.Text.Trim().Contains("心律不齐"))
                {
                    radio心律.EditValue = "2";
                    _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.心律] = "2";
                }
            }
            #endregion

            #region B超
            if (!string.IsNullOrEmpty(this.txt腹部B超异常.Text) && radio腹部B超.EditValue as string == "2")
            {
                症状temp.Append("B超异常：" + this.txt腹部B超异常.Text + this.txt其他B超异常.Text + ";");
            }
            #endregion

            #region 自理能力评估

            //Begin WXF 2018-11-26 | 16:06
            //自动老年人自理能力评估 

            if (_是否老年人)
            {
                if (string.IsNullOrEmpty(dte体检日期.Text))
                {
                    MessageBox.Show("请选选择体检日期!以便获取老年人评估数据");
                    return;
                }
                DataTable dt_评估 = new bll老年人随访().Get_yearData(text个人档案编号.Text, dte体检日期.Text.Substring(0, 4));

                if (dt_评估.Rows.Count > 0)
                {
                    int i_总分;
                    if (int.TryParse(dt_评估.Rows[0][tb_老年人随访.总评分].ToString(), out i_总分))
                    {
                        if (i_总分 >= 0 && i_总分 <= 3)
                        {
                            radio老年人生活自理能力评估.EditValue = "1";
                        }
                        else if (i_总分 >= 4 && i_总分 <= 8)
                        {
                            radio老年人生活自理能力评估.EditValue = "2";
                            症状temp.Append("自理能力:轻度依赖;");
                        }
                        else if (i_总分 >= 9 && i_总分 <= 18)
                        {
                            radio老年人生活自理能力评估.EditValue = "3";
                            症状temp.Append("自理能力:中度依赖;");
                        }
                        else if (i_总分 >= 19)
                        {
                            radio老年人生活自理能力评估.EditValue = "4";
                            症状temp.Append("自理能力:不能自理;");
                        }
                        else
                        {
                            MessageBox.Show("老年人们自理能力评估总评分异常:" + i_总分);
                        }
                    }
                    else
                    {
                        MessageBox.Show("老年人们自理能力评估总评分转换失败:" + dt_评估.Rows[0][tb_老年人随访.总评分].ToString());
                    }
                }
                else
                {
                    MessageBox.Show("此档案无" + dte体检日期.Text.Substring(0, 4) + "年老年人自理评估数据");
                }
            }

            //End

            #endregion

            #region 健康指导
            //健康指导
            string[] ss = 症状temp.ToString().Split(';');
            if (ss.Length > 0)
            {
                _ds健康体检.Tables[tb_健康体检.__TableName].Rows[0][tb_健康体检.健康评价] = "2";
                radio健康评价.EditValue = "2";
                txt体检异常1.Text = "";
                txt体检异常2.Text = "";
                txt体检异常3.Text = "";
                txt体检异常4.Text = "";
            }
            foreach (string s in ss)
            {
                if (string.IsNullOrEmpty(txt体检异常1.Text) || txt体检异常1.Text.Length <= 10)
                    txt体检异常1.Text += s + " ";
                else if (string.IsNullOrEmpty(txt体检异常2.Text) || txt体检异常2.Text.Length <= 10)
                    txt体检异常2.Text += s + " ";
                else if (string.IsNullOrEmpty(txt体检异常3.Text) || txt体检异常3.Text.Length <= 10)
                    txt体检异常3.Text += s + " ";
                else
                    txt体检异常4.Text += s + " ";
            }
            //危险因素控制
            if (radio生活方式_锻炼频率.EditValue != null && radio生活方式_锻炼频率.EditValue.ToString() == "4")
                chk锻炼.Checked = true;
            if (radio吸烟情况.EditValue != null && radio吸烟情况.EditValue.ToString() == "3")
                chk戒烟.Checked = true;
            if (radio饮酒频率.EditValue != null && (radio饮酒频率.EditValue.ToString() == "3" || radio饮酒频率.EditValue.ToString() == "4"))
                chk健康饮酒.Checked = true;

            if (_是否高血压)//存在管理卡，为高血压患者
            {
                危险因素temp.Append("低盐饮食,");
            }
            else
                危险因素temp.Append("减盐防控高血压,");

            if (_是否糖尿病)//存在管理卡，为糖尿病患者
            {
                危险因素temp.Append("糖尿病饮食,");
            }

            //if (_ds健康体检.Tables[tb_健康档案_既往病史.__TableName].Rows.Count > 0)
            //{//判断精神病
            //    foreach(DataRow dr in _ds健康体检.Tables[tb_健康档案_既往病史.__TableName].Rows)
            //    {
            //        string 疾病 = dr["D_JBBM"].ToString();
            //        if (疾病.Equals("高血压") || 疾病.Equals("糖尿病") || 疾病.Equals("重性精神疾病"))
            //            chk纳入慢性病管理.Checked = true;
            //    }
            //}

            if (group老年人评估.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Always)
            {
                chk建议疫苗接种.Checked = true;
                txt建议疫苗接种.Text = "流感疫苗、23价肺炎链球菌疫苗。";
                危险因素temp.Append("防跌倒，预防骨质疏松。");
            }
            if (radio主要用药情况.EditValue.Equals("1"))
            { 
                //服药的情况下判断
                DataTable dt = _ds健康体检.Tables[tb_健康体检_用药情况表.__TableName];
                if (dt != null && dt.Rows.Count >= 1)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["服药依从性"].ToString() != "1")
                        {
                            危险因素temp.Append("建议按时规律服药。");
                            break;
                        }
                    }
                }
            }
            chk定期随访.Checked = true;
            chk危险因素其他.Checked = true;
            this.txt危险因素其他.Text = 危险因素temp.ToString();
            #endregion

        }

        private void btn读取住院信息_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(text身份证号.Text))
            { Msg.ShowInformation("请完善身份证信息！"); return; }

            DataTable dt列表 = Get诊疗记录(this.text身份证号.Text.ToString(), "100");
            if (dt列表 != null && dt列表.Rows.Count > 0)
            {
                DataTable dt明细 = null;
                InterFace住院信息 intf = new InterFace住院信息(dt列表);
                if (intf.ShowDialog() == DialogResult.OK)
                {
                    DataRow[] rows = dt列表.Select("mzzyid = '" + intf.s住院id + "'");
                    if (rows.Length == 1)
                    {
                        this.radio住院史.EditValue = "1";
                        this.lblgc住院史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                        DataRow dr = _ds健康体检.Tables["住院史"].Rows.Add();
                        dr[tb_健康体检_住院史.个人档案编号] = _docNo;
                        dr[tb_健康体检_住院史.病案号] = rows[0]["mzzyid"];
                        dr[tb_健康体检_住院史.入院日期] = rows[0]["入院时间"];
                        dr[tb_健康体检_住院史.出院日期] = rows[0]["出院时间"];
                        dr[tb_健康体检_住院史.医疗机构名称] = rows[0]["医院名称"];
                        dr[tb_健康体检_住院史.原因] = rows[0]["疾病名称"];
                        dr[tb_健康体检_住院史.类型] = "1";
                        this.gc住院史.RefreshDataSource();
                    }
                }
                else
                {
                    Msg.ShowInformation("用户取消了选择！");
                    return;
                }
            }
            else
            {
                Msg.ShowInformation("未查询到相应的住院信息！");
                return;
            }
        }
        private DataTable Get诊疗记录(string cardid, string count)
        {
            DataTable dtDetaiInfo = null;
            try
            {
                string conStr = interfaceZljl.Config.hisStr;
                //string sql = "uSp就医信息检索";
                string sql = "uSp就医住院检索Contain2709";

                SqlParameter[] sqlparams = new SqlParameter[2];
                sqlparams[0] = new SqlParameter("@cardid", cardid);
                sqlparams[1] = new SqlParameter("@topnum", count);

                dtDetaiInfo = tykClass.Db.SqlHelper.ExecuteDataset(conStr, CommandType.StoredProcedure, sql, sqlparams).Tables[0];
            }
            catch (Exception ex)
            {
                dtDetaiInfo = null;
                MessageBox.Show(ex.Message, "异常提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            return dtDetaiInfo;
        }
        private void btn读取用药信息_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(text身份证号.Text))
            { Msg.ShowInformation("请完善身份证信息！"); return; }

            DataTable dt列表 = bll获取化验信息.Get化验信息列表(this.text身份证号.Text.ToString());
            if (dt列表 != null && dt列表.Rows.Count > 0)
            { }
        }

        //设置老年人中医体质辨识中选择错误时可以清空
        private void btn清空选择_Click(object sender, EventArgs e)
        {
            this.radio平和质.EditValue = "NULL";
            this.radio气虚质.EditValue = "NULL";
            this.radio阳虚质.EditValue = "NULL";
            this.radio阴虚质.EditValue = "NULL";
            this.radio痰湿质.EditValue = "NULL";
            this.radio湿热质.EditValue = "NULL";
            this.radio血瘀质.EditValue = "NULL";
            this.radio气郁质.EditValue = "NULL";
            this.radio特秉质.EditValue = "NULL";
        }

    }
}
