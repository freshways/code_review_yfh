﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class UC健康体检_B超
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC健康体检_B超));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutPanel1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt备注 = new DevExpress.XtraEditors.TextEdit();
            this.txt体检日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt超声所见 = new DevExpress.XtraEditors.MemoEdit();
            this.lbl个人档案号 = new DevExpress.XtraEditors.LabelControl();
            this.lblB超 = new DevExpress.XtraEditors.LabelControl();
            this.lbl姓名 = new DevExpress.XtraEditors.LabelControl();
            this.lblB超异常 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建时间 = new DevExpress.XtraEditors.LabelControl();
            this.lbl当前所属机构 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建人 = new DevExpress.XtraEditors.LabelControl();
            this.lbl创建机构 = new DevExpress.XtraEditors.LabelControl();
            this.txt超声提示 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btn选择模板 = new DevExpress.XtraEditors.SimpleButton();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPanel1)).BeginInit();
            this.LayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt超声所见.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt超声提示.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn选择模板);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(651, 28);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 3;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup2.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup2.CustomizationFormText = "接诊记录表";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup2.Size = new System.Drawing.Size(649, 551);
            this.layoutControlGroup2.Text = "接诊记录表";
            // 
            // LayoutPanel1
            // 
            this.LayoutPanel1.Controls.Add(this.txt备注);
            this.LayoutPanel1.Controls.Add(this.txt体检日期);
            this.LayoutPanel1.Controls.Add(this.txt超声所见);
            this.LayoutPanel1.Controls.Add(this.lbl个人档案号);
            this.LayoutPanel1.Controls.Add(this.lblB超);
            this.LayoutPanel1.Controls.Add(this.lbl姓名);
            this.LayoutPanel1.Controls.Add(this.lblB超异常);
            this.LayoutPanel1.Controls.Add(this.lbl创建时间);
            this.LayoutPanel1.Controls.Add(this.lbl当前所属机构);
            this.LayoutPanel1.Controls.Add(this.lbl创建人);
            this.LayoutPanel1.Controls.Add(this.lbl创建机构);
            this.LayoutPanel1.Controls.Add(this.txt超声提示);
            this.LayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutPanel1.Location = new System.Drawing.Point(0, 28);
            this.LayoutPanel1.Name = "LayoutPanel1";
            this.LayoutPanel1.Root = this.layoutControlGroup1;
            this.LayoutPanel1.Size = new System.Drawing.Size(651, 478);
            this.LayoutPanel1.TabIndex = 4;
            // 
            // txt备注
            // 
            this.txt备注.Location = new System.Drawing.Point(411, 78);
            this.txt备注.Margin = new System.Windows.Forms.Padding(0);
            this.txt备注.Name = "txt备注";
            this.txt备注.Size = new System.Drawing.Size(235, 20);
            this.txt备注.StyleController = this.LayoutPanel1;
            this.txt备注.TabIndex = 29;
            // 
            // txt体检日期
            // 
            this.txt体检日期.EditValue = null;
            this.txt体检日期.Location = new System.Drawing.Point(84, 78);
            this.txt体检日期.Margin = new System.Windows.Forms.Padding(0);
            this.txt体检日期.Name = "txt体检日期";
            this.txt体检日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt体检日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt体检日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt体检日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.txt体检日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt体检日期.Properties.ReadOnly = true;
            this.txt体检日期.Size = new System.Drawing.Size(244, 20);
            this.txt体检日期.StyleController = this.LayoutPanel1;
            this.txt体检日期.TabIndex = 28;
            // 
            // txt超声所见
            // 
            this.txt超声所见.Location = new System.Drawing.Point(84, 102);
            this.txt超声所见.Margin = new System.Windows.Forms.Padding(0);
            this.txt超声所见.Name = "txt超声所见";
            this.txt超声所见.Size = new System.Drawing.Size(562, 227);
            this.txt超声所见.StyleController = this.LayoutPanel1;
            this.txt超声所见.TabIndex = 53;
            this.txt超声所见.UseOptimizedRendering = true;
            // 
            // lbl个人档案号
            // 
            this.lbl个人档案号.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl个人档案号.Location = new System.Drawing.Point(84, 30);
            this.lbl个人档案号.Name = "lbl个人档案号";
            this.lbl个人档案号.Size = new System.Drawing.Size(244, 20);
            this.lbl个人档案号.StyleController = this.LayoutPanel1;
            this.lbl个人档案号.TabIndex = 40;
            this.lbl个人档案号.Text = "档案号";
            // 
            // lblB超
            // 
            this.lblB超.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblB超.Location = new System.Drawing.Point(84, 54);
            this.lblB超.Name = "lblB超";
            this.lblB超.Size = new System.Drawing.Size(244, 20);
            this.lblB超.StyleController = this.LayoutPanel1;
            this.lblB超.TabIndex = 41;
            this.lblB超.Text = "正常异常";
            // 
            // lbl姓名
            // 
            this.lbl姓名.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl姓名.Location = new System.Drawing.Point(411, 30);
            this.lbl姓名.Name = "lbl姓名";
            this.lbl姓名.Size = new System.Drawing.Size(235, 20);
            this.lbl姓名.StyleController = this.LayoutPanel1;
            this.lbl姓名.TabIndex = 43;
            this.lbl姓名.Text = "姓名";
            // 
            // lblB超异常
            // 
            this.lblB超异常.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lblB超异常.Location = new System.Drawing.Point(411, 54);
            this.lblB超异常.Name = "lblB超异常";
            this.lblB超异常.Size = new System.Drawing.Size(235, 20);
            this.lblB超异常.StyleController = this.LayoutPanel1;
            this.lblB超异常.TabIndex = 44;
            this.lblB超异常.Text = "异常";
            // 
            // lbl创建时间
            // 
            this.lbl创建时间.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl创建时间.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建时间.Location = new System.Drawing.Point(84, 429);
            this.lbl创建时间.Margin = new System.Windows.Forms.Padding(0);
            this.lbl创建时间.Name = "lbl创建时间";
            this.lbl创建时间.Size = new System.Drawing.Size(244, 20);
            this.lbl创建时间.StyleController = this.LayoutPanel1;
            this.lbl创建时间.TabIndex = 46;
            this.lbl创建时间.Text = "创建时间";
            // 
            // lbl当前所属机构
            // 
            this.lbl当前所属机构.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl当前所属机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl当前所属机构.Location = new System.Drawing.Point(84, 453);
            this.lbl当前所属机构.Margin = new System.Windows.Forms.Padding(0);
            this.lbl当前所属机构.Name = "lbl当前所属机构";
            this.lbl当前所属机构.Size = new System.Drawing.Size(244, 20);
            this.lbl当前所属机构.StyleController = this.LayoutPanel1;
            this.lbl当前所属机构.TabIndex = 47;
            this.lbl当前所属机构.Text = "所属机构";
            // 
            // lbl创建人
            // 
            this.lbl创建人.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl创建人.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建人.Location = new System.Drawing.Point(411, 453);
            this.lbl创建人.Margin = new System.Windows.Forms.Padding(0);
            this.lbl创建人.Name = "lbl创建人";
            this.lbl创建人.Size = new System.Drawing.Size(235, 20);
            this.lbl创建人.StyleController = this.LayoutPanel1;
            this.lbl创建人.TabIndex = 48;
            this.lbl创建人.Text = "创建人";
            // 
            // lbl创建机构
            // 
            this.lbl创建机构.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lbl创建机构.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl创建机构.Location = new System.Drawing.Point(411, 429);
            this.lbl创建机构.Margin = new System.Windows.Forms.Padding(0);
            this.lbl创建机构.Name = "lbl创建机构";
            this.lbl创建机构.Size = new System.Drawing.Size(235, 20);
            this.lbl创建机构.StyleController = this.LayoutPanel1;
            this.lbl创建机构.TabIndex = 50;
            this.lbl创建机构.Text = "创建机构";
            // 
            // txt超声提示
            // 
            this.txt超声提示.Location = new System.Drawing.Point(84, 333);
            this.txt超声提示.Name = "txt超声提示";
            this.txt超声提示.Size = new System.Drawing.Size(562, 92);
            this.txt超声提示.StyleController = this.LayoutPanel1;
            this.txt超声提示.TabIndex = 33;
            this.txt超声提示.UseOptimizedRendering = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "B超报告编辑";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem21,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem22,
            this.layoutControlItem4,
            this.layoutControlItem1,
            this.layoutControlItem5,
            this.layoutControlItem3,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem31,
            this.layoutControlItem29});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup2";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup1.Size = new System.Drawing.Size(651, 478);
            this.layoutControlGroup1.Text = "B超报告编辑";
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.lbl个人档案号;
            this.layoutControlItem21.CustomizationFormText = "lbl个人档案号item";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(16, 24);
            this.layoutControlItem21.Name = "lbl个人档案号item";
            this.layoutControlItem21.Size = new System.Drawing.Size(327, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "个人档案号:";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.lbl姓名;
            this.layoutControlItem24.CustomizationFormText = "lbl姓名item";
            this.layoutControlItem24.Location = new System.Drawing.Point(327, 0);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem24.Name = "lbl姓名item";
            this.layoutControlItem24.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "姓名:";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.lblB超异常;
            this.layoutControlItem25.CustomizationFormText = "lbl身份证号item";
            this.layoutControlItem25.Location = new System.Drawing.Point(327, 24);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(115, 24);
            this.layoutControlItem25.Name = "lbl身份证号item";
            this.layoutControlItem25.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "异常信息:";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.lblB超;
            this.layoutControlItem22.CustomizationFormText = "lbl性别item";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(115, 24);
            this.layoutControlItem22.Name = "lbl性别item";
            this.layoutControlItem22.Size = new System.Drawing.Size(327, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "B超:";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt体检日期;
            this.layoutControlItem4.CustomizationFormText = "接诊时间";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem4.Name = "dtp接诊时间item";
            this.layoutControlItem4.Size = new System.Drawing.Size(327, 24);
            this.layoutControlItem4.Tag = "check";
            this.layoutControlItem4.Text = "体检日期:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.txt备注;
            this.layoutControlItem1.CustomizationFormText = "接诊医生";
            this.layoutControlItem1.Location = new System.Drawing.Point(327, 48);
            this.layoutControlItem1.Name = "txt接诊医生item";
            this.layoutControlItem1.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem1.Tag = "check";
            this.layoutControlItem1.Text = "备注:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt超声所见;
            this.layoutControlItem5.CustomizationFormText = "就诊者的客观资料";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(117, 65);
            this.layoutControlItem5.Name = "txt就诊者的客观资料item";
            this.layoutControlItem5.Size = new System.Drawing.Size(645, 231);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Tag = "check";
            this.layoutControlItem5.Text = "超声所见:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.txt超声提示;
            this.layoutControlItem3.CustomizationFormText = "处置计划";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 303);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(113, 65);
            this.layoutControlItem3.Name = "txt处置计划item";
            this.layoutControlItem3.Size = new System.Drawing.Size(645, 96);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Tag = "check";
            this.layoutControlItem3.Text = "超声提示:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.lbl创建时间;
            this.layoutControlItem27.CustomizationFormText = "lbl创建时间item";
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 399);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem27.Name = "lbl创建时间item";
            this.layoutControlItem27.Size = new System.Drawing.Size(327, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "创建时间:";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.lbl当前所属机构;
            this.layoutControlItem28.CustomizationFormText = "lbl当前所属机构item";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 423);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem28.Name = "lbl当前所属机构item";
            this.layoutControlItem28.Size = new System.Drawing.Size(327, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "当前所属机构:";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.lbl创建机构;
            this.layoutControlItem31.CustomizationFormText = "lbl创建机构item";
            this.layoutControlItem31.Location = new System.Drawing.Point(327, 399);
            this.layoutControlItem31.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem31.Name = "lbl创建机构item";
            this.layoutControlItem31.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "创建机构:";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(76, 14);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.lbl创建人;
            this.layoutControlItem29.CustomizationFormText = "lbl创建人item";
            this.layoutControlItem29.Location = new System.Drawing.Point(327, 423);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(127, 24);
            this.layoutControlItem29.Name = "lbl创建人item";
            this.layoutControlItem29.Size = new System.Drawing.Size(318, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "创建人:";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(76, 14);
            // 
            // btn选择模板
            // 
            this.btn选择模板.Image = ((System.Drawing.Image)(resources.GetObject("btn选择模板.Image")));
            this.btn选择模板.Location = new System.Drawing.Point(84, 3);
            this.btn选择模板.Name = "btn选择模板";
            this.btn选择模板.Size = new System.Drawing.Size(81, 23);
            this.btn选择模板.TabIndex = 4;
            this.btn选择模板.Text = "选择模板";
            this.btn选择模板.Click += new System.EventHandler(this.btn选择模板_Click);
            // 
            // UC健康体检_B超
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UC健康体检_B超";
            this.Size = new System.Drawing.Size(651, 506);
            this.Load += new System.EventHandler(this.UC健康体检_B超_Load);
            this.Controls.SetChildIndex(this.flowLayoutPanel1, 0);
            this.Controls.SetChildIndex(this.LayoutPanel1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPanel1)).EndInit();
            this.LayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt备注.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt体检日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt超声所见.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt超声提示.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControl LayoutPanel1;
        private DevExpress.XtraEditors.TextEdit txt备注;
        private DevExpress.XtraEditors.DateEdit txt体检日期;
        private DevExpress.XtraEditors.MemoEdit txt超声所见;
        private DevExpress.XtraEditors.LabelControl lbl个人档案号;
        private DevExpress.XtraEditors.LabelControl lblB超;
        private DevExpress.XtraEditors.LabelControl lbl姓名;
        private DevExpress.XtraEditors.LabelControl lblB超异常;
        private DevExpress.XtraEditors.LabelControl lbl创建时间;
        private DevExpress.XtraEditors.LabelControl lbl当前所属机构;
        private DevExpress.XtraEditors.LabelControl lbl创建人;
        private DevExpress.XtraEditors.LabelControl lbl创建机构;
        private DevExpress.XtraEditors.MemoEdit txt超声提示;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraEditors.SimpleButton btn选择模板;

    }
}
