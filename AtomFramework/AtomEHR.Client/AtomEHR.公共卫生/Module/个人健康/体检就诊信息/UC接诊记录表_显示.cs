﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.健康档案.辖区健康档案;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class UC接诊记录表_显示 : UserControlBaseNavBar
    {

        public UC接诊记录表_显示()
        {
            InitializeComponent();
        }

        DataRow[] _dr个人档案信息 = null;
        DataRow selectRow = null;
        string _创建日期 = "";
        string _ID = "";

        public UC接诊记录表_显示(DataRow[] dr, object date)
        {
            _dr个人档案信息 = dr;
            _创建日期 = date == null ? "" : date.ToString();
            _BLL = new bll接诊记录();
            InitializeComponent();

            //默认绑定
            txt个人档案号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.lbl性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.lbl身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.lbl出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            this.lbl联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            this.lbl居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
        }

        private void UC接诊记录表_显示_Load(object sender, EventArgs e)
        {
            _dt缓存数据 = _BLL.GetBusinessByKey(txt个人档案号.Text, true).Tables[tb_接诊记录.__TableName];

            //已经有数据进行绑定
            if (dt缓存数据 != null && dt缓存数据.Rows.Count > 0)
            {
                //绑定左侧列表
                CreateNavBarButton_new(dt缓存数据, tb_接诊记录.接诊时间);

                if (_创建日期 != null && _创建日期 != "")
                {
                    selectRow = dt缓存数据.Select("创建时间='" + _创建日期 + "'")[0];
                    DoBindingSummaryEditor(dt缓存数据.Select("创建时间='" + _创建日期 + "'")[0]);
                }
                else
                {
                    selectRow = dt缓存数据.Rows[0];
                    DoBindingSummaryEditor(dt缓存数据.Rows[0]);
                }
            }
            else
            {//打开新增页面
                btn新增.PerformClick();
            }
            SetDetailEditorsAccessable(panel1, false);
        }

        private void btn新增_Click(object sender, EventArgs e)
        {
            _UpdateType = UpdateType.Add;
            _BLL.GetBusinessByKey("-", true);//下载一个空业务单据            
            _BLL.NewBusiness(); //增加一条主表记录
            _dt缓存数据 = _BLL.CurrentBusiness.Tables[tb_接诊记录.__TableName];
            _dt缓存数据.Rows[0][tb_接诊记录.个人档案编号] = _dr个人档案信息[0][tb_健康档案.__KeyName].ToString();

            UC接诊记录表 control = new UC接诊记录表(_dr个人档案信息, _UpdateType, null);
            ShowControl(control, DockStyle.Fill);
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            _UpdateType = UpdateType.Modify;
            UC接诊记录表 control = new UC接诊记录表(_dr个人档案信息, _UpdateType, _ID);
            ShowControl(control, DockStyle.Fill);
        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(Object objdataSource)
        {
            DataRow dataSource = null;
            if (objdataSource == null) return;
            if (objdataSource is DataRow)
            {
                dataSource = objdataSource as DataRow;

                _dt缓存数据 = BLL.ConvertRowsToTable(dataSource);
            }

            _ID = dataSource[tb_接诊记录.ID].ToString();
            this.txt接诊时间.Text = dataSource[tb_接诊记录.接诊时间].ToString();
            this.txt接诊医生.Text = dataSource[tb_接诊记录.接诊医生].ToString();
            this.txt主观资料.Text = dataSource[tb_接诊记录.主观资料].ToString();
            this.txt评估.Text = dataSource[tb_接诊记录.评估].ToString();
            this.txt客观资料.Text = dataSource[tb_接诊记录.客观资料].ToString();
            this.txt处置计划.Text = dataSource[tb_接诊记录.处置计划].ToString();
            //非编辑项            
            this.lbl当前所属机构.Text = BLL.Return机构名称(dataSource[tb_接诊记录.所属机构].ToString());
            this.lbl创建机构.Text = BLL.Return机构名称(dataSource[tb_接诊记录.创建机构].ToString());
            this.lbl创建人.Text = BLL.Return用户名称(dataSource[tb_接诊记录.创建人].ToString());
            this.lbl创建时间.Text = dataSource[tb_接诊记录.创建时间].ToString();
            this.lbl最近修改人.Text = BLL.Return用户名称(dataSource[tb_接诊记录.修改人].ToString());
            this.lbl最近更新时间.Text = dataSource[tb_接诊记录.修改时间].ToString();

            //设置颜色
            Set考核项颜色_new(LayoutPanel1, lab考核项);
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (Msg.AskQuestion("确认要删除？"))
            {
                if (BLL.Delete(_ID))
                {
                    this.OnLoad(e);
                }
            }
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            try
            {
                int age = System.DateTime.Now.Year - Convert.ToDateTime(lbl出生日期.Text).Year;
             
                XtraReport接诊记录单 xrpt = new XtraReport接诊记录单(selectRow, this.txt姓名.Text, lbl性别.Text, age.ToString(), lbl居住地址.Text);
                ReportPrintTool tool = new ReportPrintTool(xrpt);
                tool.ShowPreviewDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("打印失败！异常信息:\n" + ex.Message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }
}
