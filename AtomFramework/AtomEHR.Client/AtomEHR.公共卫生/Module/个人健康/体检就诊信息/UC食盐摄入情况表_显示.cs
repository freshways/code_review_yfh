﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Library;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    public partial class UC食盐摄入情况表_显示 : UserControlBaseNavBar
    {
        DataRow[] _dr个人档案信息 = null;
        string _创建日期 = "";
        string _ID = "";

        public UC食盐摄入情况表_显示()
        {
            InitializeComponent();
        }

        public UC食盐摄入情况表_显示(DataRow[] dr, object date)
        {
            _dr个人档案信息 = dr;
            _创建日期 = date == null ? "" : date.ToString();
            _BLL = new bll健康档案_食盐情况();
            InitializeComponent();

            //默认绑定
            txt个人档案号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.txt姓名.Text = dr[0][tb_健康档案.姓名].ToString();
            this.txt性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.txt身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            //绑定联系电话
            string str联系电话 = dr[0][tb_健康档案.本人电话].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话 != "无")
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            }
            else
            {
                this.txt联系电话.Text = dr[0][tb_健康档案.联系人电话].ToString();
            }
            this.txt居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();
        }

        private void UC高血压高危干预_显示_Load(object sender, EventArgs e)
        {
            _dt缓存数据 = _BLL.GetBusinessByKey(txt个人档案号.Text, true).Tables[tb_健康档案_食盐情况.__TableName];

            CreateNavBarButton_new(dt缓存数据, tb_健康档案_食盐情况.随访日期);

            //已经有数据进行绑定
            if (dt缓存数据 != null && dt缓存数据.Rows.Count > 0)
            {
                if (_创建日期 != null && _创建日期 != "")
                {
                    DoBindingSummaryEditor(dt缓存数据.Select("创建时间='" + _创建日期 + "'")[0]);                    
                }
                else
                    DoBindingSummaryEditor(dt缓存数据.Rows[0]);

                SetItemColorToRed(_ID); //设置左侧当前随访日期颜色
            }
            else
            {//打开新增页面
                btn新增.PerformClick();
            }

            SetDetailEditorsAccessable(panel1, false);

        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                _UpdateType = UpdateType.Modify;
                UC食盐摄入情况表 control = new UC食盐摄入情况表(_dr个人档案信息, _UpdateType, _ID);
                ShowControl(control);
            }
            else { Msg.ShowInformation("只能修改属于本机构的记录！"); }
        }

        private void btn新增_Click(object sender, EventArgs e)
        {
            _UpdateType = UpdateType.Add;
            UC食盐摄入情况表 control = new UC食盐摄入情况表(_dr个人档案信息, _UpdateType, "");
            ShowControl(control);
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("确认要删除？")) return;
            if (canModifyBy同一机构(_dt缓存数据.Rows[0][tb_健康档案.所属机构].ToString()))
            {
                if (BLL.Delete(_ID))
                {
                    _创建日期 = "";
                    this.OnLoad(e);
                }
            }
            else { Msg.ShowInformation("只能操作属于本机构的记录！"); }
        }
        
        protected override void DoBindingSummaryEditor(Object objdataSource)
        {
            DataTable dataSource = null;
            if (objdataSource == null) return;
            if (objdataSource is DataRow)
            {
                dataSource = BLL.ConvertRowsToTable(objdataSource as DataRow);
            }
            _ID = dataSource.Rows[0][tb_健康档案_食盐情况.ID].ToString();
            DataBinder.BindingTextEdit(txt家庭膳食1.Txt1, dataSource, tb_健康档案_食盐情况.家庭膳食1);
            DataBinder.BindingTextEdit(txt家庭膳食11.Txt1, dataSource, tb_健康档案_食盐情况.家庭膳食1_1);
            DataBinder.BindingTextEdit(txt家庭膳食2.Txt1, dataSource, tb_健康档案_食盐情况.家庭膳食2);
            DataBinder.BindingTextEdit(txt家庭膳食3.Txt1, dataSource, tb_健康档案_食盐情况.家庭膳食3);

            DataBinder.BindingTextEditDateTime(txt随访日期, dataSource, tb_健康档案_食盐情况.随访日期);
            DataBinder.BindingRadioEdit(radio随访方式, dataSource, tb_健康档案_食盐情况.随访方式);


            DataBinder.BindingTextEditDateTime(txt下次随访时间, dataSource, tb_健康档案_食盐情况.下次随访日期);
            DataBinder.BindingTextEdit(txt医生签名, dataSource, tb_健康档案_食盐情况.随访医生签名);


            //非编辑项
            this.lab当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_健康档案_食盐情况.所属机构].ToString());
            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_健康档案_食盐情况.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_健康档案_食盐情况.创建人].ToString());
            this.lbl创建时间.Text = dataSource.Rows[0][tb_健康档案_食盐情况.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_健康档案_食盐情况.修改人].ToString());
            this.lbl最近更新时间.Text = dataSource.Rows[0][tb_健康档案_食盐情况.修改时间].ToString();
        }

    }
}
