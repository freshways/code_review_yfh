﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.公共卫生;
using System.Text;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class report健康体检表1_2017 : DevExpress.XtraReports.UI.XtraReport
    {
        DataSet ds体检;
        bll健康体检 bll = new bll健康体检();
        public report健康体检表1_2017()
        {
            InitializeComponent();
        }

        //wxf 2018年7月13日 20:02:47
        //2017版体检表重绘

        public report健康体检表1_2017(string docNo,string date)
        {
            InitializeComponent();
            ds体检 = bll.GetReportDataByKey(docNo,date);
            BindData(ds体检);
        }

        private void BindData(DataSet ds体检)
        {
            DataTable tb健康档案 = ds体检.Tables[tb_健康档案.__TableName];
            DataTable tb健康体检 = ds体检.Tables[tb_健康体检.__TableName];
            DataTable tb住院史 = ds体检.Tables["住院史"];
            DataTable tb家庭病床史 = ds体检.Tables["家庭病床史"];
            DataTable tb用药情况表 = ds体检.Tables[tb_健康体检_用药情况表.__TableName];
            DataTable tb接种史 = ds体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName];

            if (tb健康体检.Rows.Count == 1 && tb健康档案.Rows.Count == 1)
            {
                //第一页
                this.xrLabel_姓名.Text = AtomEHR.公共卫生.util.DESEncrypt.DES解密(tb健康档案.Rows[0][tb_健康档案.姓名].ToString());
                //wxf 2018年7月24日 11:19:46 拆解个人档案编号后8位
                string str_个人档案编号 = tb健康档案.Rows[0][tb_健康档案.个人档案编号].ToString();
                char[] char_编号 = str_个人档案编号.Substring(str_个人档案编号.Length-8, 8).ToCharArray();
                for (int i = 0; i < char_编号.Length; i++)
                {
                    string xrName = "xrLabel_编号" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = char_编号[i].ToString();
                }

                string[] tijianriqi = tb健康体检.Rows[0][tb_健康体检.体检日期].ToString().Split('-');
                this.xrTable_体检日期年.Text = tijianriqi[0];
                this.xrTable_体检日期月.Text = tijianriqi[1];
                this.xrTable_体检日期日.Text = tijianriqi[2];

                this.xrTable_责任医生.Text = tb健康体检.Rows[0][tb_健康体检.FIELD2].ToString();
                this.xrLabel_症状其他.Text = tb健康体检.Rows[0][tb_健康体检.症状其他].ToString();

                string zhengzhuang = tb健康体检.Rows[0][tb_健康体检.症状].ToString();
                int j_zhengzhuang = 1;
                string[] zzs = zhengzhuang.Split(',');
                for (int i = 0; i < zzs.Length; i++)
                {
                    if (string.IsNullOrEmpty(zzs[i]))
                    {
                        continue;
                    }

                    string result_zhengzhuang = string.Empty;
                    switch (zzs[i])
                    {
                        case "99":
                            result_zhengzhuang = "25";
                            break;
                        default:
                            result_zhengzhuang = zzs[i];
                            break;
                    }

                    if (j_zhengzhuang  > 10)
                    {
                        break;
                    }
                    string xrName = "xrLabel_症状" + j_zhengzhuang;
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_zhengzhuang;
                    j_zhengzhuang += 1;
                }

                this.xrTable_体温.Text = tb健康体检.Rows[0][tb_健康体检.体温].ToString();
                this.xrTable_脉率.Text = tb健康体检.Rows[0][tb_健康体检.脉搏].ToString();
                this.xrTable_呼吸频率.Text = tb健康体检.Rows[0][tb_健康体检.呼吸].ToString();
                this.xrTable_左侧血压1.Text = tb健康体检.Rows[0][tb_健康体检.血压左侧1].ToString();
                this.xrTable_左侧血压2.Text = tb健康体检.Rows[0][tb_健康体检.血压左侧2].ToString();
                this.xrTable_右侧血压1.Text = tb健康体检.Rows[0][tb_健康体检.血压右侧1].ToString();
                this.xrTable_右侧血压2.Text = tb健康体检.Rows[0][tb_健康体检.血压右侧2].ToString();
                this.xrTable_身高.Text = tb健康体检.Rows[0][tb_健康体检.身高].ToString();
                this.xrTable_体重.Text = tb健康体检.Rows[0][tb_健康体检.体重].ToString();
                this.xrTable_腰围.Text = tb健康体检.Rows[0][tb_健康体检.腰围].ToString();
                this.xrTable_体重指数.Text = tb健康体检.Rows[0][tb_健康体检.体重指数].ToString();
                this.xrLabel_老年人健康状态自我评估.Text = tb健康体检.Rows[0][tb_健康体检.老年人状况评估].ToString();
                this.xrLabel_老年人生活自理能力自我评估.Text = tb健康体检.Rows[0][tb_健康体检.老年人自理评估].ToString();
                this.xrLabel_老年人认知功能.Text = tb健康体检.Rows[0][tb_健康体检.老年人认知].ToString();
                this.xrLabel_老年人认知功能总分.Text = tb健康体检.Rows[0][tb_健康体检.老年人认知分].ToString();
                this.xrLabel_老年人情感状态.Text = tb健康体检.Rows[0][tb_健康体检.老年人情感].ToString();
                this.xrLabel_老年人情感状态总分.Text = tb健康体检.Rows[0][tb_健康体检.老年人情感分].ToString();

                this.xrLabel_锻炼频率.Text = tb健康体检.Rows[0][tb_健康体检.锻炼频率].ToString();
                this.xrTable_每次锻炼时间.Text = tb健康体检.Rows[0][tb_健康体检.每次锻炼时间].ToString();
                this.xrTable_坚持锻炼时间.Text = tb健康体检.Rows[0][tb_健康体检.坚持锻炼时间].ToString();
                this.xrTable_锻炼方式.Text = tb健康体检.Rows[0][tb_健康体检.锻炼方式].ToString();

                string yinshi = tb健康体检.Rows[0][tb_健康体检.饮食习惯].ToString();
                string[] yinshis = yinshi.Split(',');
                for (int i = 0; i < yinshis.Length; i++)
                {
                    if (string.IsNullOrEmpty(yinshis[i]))
                    {
                        continue;
                    }
                    if (i<=2)
                    {
                        string xrName = "xrLabel_饮食习惯" + (i + 1);
                        XRLabel xrl = (XRLabel)FindControl(xrName, false);
                        xrl.Text = yinshis[i];
                    }
                }

                this.xrLabel_吸烟状况.Text = tb健康体检.Rows[0][tb_健康体检.吸烟状况].ToString();
                this.xrLabel_日吸烟量.Text = tb健康体检.Rows[0][tb_健康体检.日吸烟量].ToString();
                this.xrLabel_开始吸烟年龄.Text = tb健康体检.Rows[0][tb_健康体检.开始吸烟年龄].ToString();
                this.xrLabel_戒烟年龄.Text = tb健康体检.Rows[0][tb_健康体检.戒烟年龄].ToString();

                this.xrLabel_饮酒频率.Text = tb健康体检.Rows[0][tb_健康体检.饮酒频率].ToString();
                this.xrLabel_日饮酒量.Text = tb健康体检.Rows[0][tb_健康体检.日饮酒量].ToString();
                this.xrLabel_是否戒酒.Text = tb健康体检.Rows[0][tb_健康体检.是否戒酒].ToString();
                this.xrLabel_戒酒年龄.Text = tb健康体检.Rows[0][tb_健康体检.戒酒年龄].ToString();
                this.xrLabel_开始饮酒年龄.Text = tb健康体检.Rows[0][tb_健康体检.开始饮酒年龄].ToString();
                this.xrLabel_是否醉酒.Text = tb健康体检.Rows[0][tb_健康体检.近一年内是否曾醉酒].ToString();
                this.xrLabel_饮酒种类其他.Text = tb健康体检.Rows[0][tb_健康体检.饮酒种类其它].ToString();
                string yinjiu = tb健康体检.Rows[0][tb_健康体检.饮酒种类].ToString();
                int j_yinjiu = 1;
                string[] yinjius = yinjiu.Split(',');
                for (int i = 0; i < yinjius.Length; i++)
                {
                    if (string.IsNullOrEmpty(yinjius[i]))
                    {
                        continue;
                    }

                    string result_yinjiu = string.Empty;
                    switch (yinjius[i])
                    {
                        case "99":
                            result_yinjiu = "5";
                            break;
                        default:
                            result_yinjiu = yinjius[i];
                            break;
                    }

                    if (j_yinjiu > 4)
                    {
                        break;
                    }

                    string xrName = "xrLabel_饮酒种类" + j_yinjiu;
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_yinjiu;
                    j_yinjiu += 1;
                }

                this.xrLabel_接触史有无.Text = tb健康体检.Rows[0][tb_健康体检.有无职业病].ToString();
                this.xrLabel_工种.Text = tb健康体检.Rows[0][tb_健康体检.具体职业].ToString();
                this.xrLabel_从业时间.Text = tb健康体检.Rows[0][tb_健康体检.从业时间].ToString();
                this.xrLabel_粉尘.Text = tb健康体检.Rows[0][tb_健康体检.粉尘].ToString();
                this.xrLabel_粉尘防护措施.Text = tb健康体检.Rows[0][tb_健康体检.粉尘防护措施].ToString();
                this.xrLabel_粉尘防护措施有无.Text = tb健康体检.Rows[0][tb_健康体检.粉尘防护有无].ToString();
                this.xrLabel_放射物质.Text = tb健康体检.Rows[0][tb_健康体检.放射物质].ToString();
                this.xrLabel_放射物质防护措施.Text = tb健康体检.Rows[0][tb_健康体检.放射物质防护措施其他].ToString();
                this.xrLabel_放射物质防护措施有无.Text = tb健康体检.Rows[0][tb_健康体检.放射物质防护措施有无].ToString();
                this.xrLabel_物理因素.Text = tb健康体检.Rows[0][tb_健康体检.物理因素].ToString();
                this.xrLabel_物理因素防护措施.Text = tb健康体检.Rows[0][tb_健康体检.物理防护措施].ToString();
                this.xrLabel_物理因素防护措施有无.Text = tb健康体检.Rows[0][tb_健康体检.物理防护有无].ToString();
                this.xrLabel_化学物质.Text = tb健康体检.Rows[0][tb_健康体检.化学物质].ToString();
                this.xrLabel_化学物质防护措施.Text = tb健康体检.Rows[0][tb_健康体检.化学物质具体防护].ToString();
                this.xrLabel_化学物质防护措施有无.Text = tb健康体检.Rows[0][tb_健康体检.化学物质防护].ToString();
                this.xrLabel_接触史其他.Text = tb健康体检.Rows[0][tb_健康体检.职业病其他].ToString();
                this.xrLabel_接触史其他防护措施.Text = tb健康体检.Rows[0][tb_健康体检.其他防护措施].ToString();
                this.xrLabel_接触史其他防护措施有无.Text = tb健康体检.Rows[0][tb_健康体检.其他防护有无].ToString();

                //第四页
                if (tb用药情况表 != null && tb用药情况表.Rows.Count>0)
                {
                    for (int i = 0; i < tb用药情况表.Rows.Count; i++)
                    {
                        if (i >= 6) break;

                        string 用药名称 = "xrTable_药物名称" + (i + 1);
                        string 用药用法 = "xrTable_药物用法" + (i + 1);
                        string 用药用量 = "xrTable_药物用量" + (i + 1);
                        string 用药时间 = "xrTable_用药时间" + (i + 1);
                        string 用药依从性 = "xrTable_服药依从性" + (i + 1);

                        XRTableCell xr用药名称 = (XRTableCell)FindControl(用药名称, false);
                        XRTableCell xr用药用法 = (XRTableCell)FindControl(用药用法, false);
                        XRTableCell xr用药用量 = (XRTableCell)FindControl(用药用量, false);
                        XRTableCell xr用药时间 = (XRTableCell)FindControl(用药时间, false);
                        XRTableCell xr用药依从性 = (XRTableCell)FindControl(用药依从性, false);

                        xr用药名称.Text = tb用药情况表.Rows[i][tb_健康体检_用药情况表.药物名称].ToString();
                        xr用药用法.Text = tb用药情况表.Rows[i][tb_健康体检_用药情况表.用法].ToString();
                        xr用药时间.Text = tb用药情况表.Rows[i][tb_健康体检_用药情况表.用药时间].ToString();
                        xr用药用量.Text = tb用药情况表.Rows[i][tb_健康体检_用药情况表.用量].ToString();
                        xr用药依从性.Text = tb用药情况表.Rows[i][tb_健康体检_用药情况表.服药依从性].ToString();
                    }

                }

                if (tb接种史 != null && tb接种史.Rows.Count>0)
                {
                    for (int i = 0; i < tb接种史.Rows.Count; i++)
                    {
                        if (i >= 3) break;

                        string 接种名称 = "xrTable_接种史名称" + (i + 1);
                        string 接种日期 = "xrTable_接种日期" + (i + 1);
                        string 接种机构 = "xrTable_接种机构" + (i + 1);

                        XRTableCell xr接种名称 = (XRTableCell)FindControl(接种名称, false);
                        XRTableCell xr接种日期 = (XRTableCell)FindControl(接种日期, false);
                        XRTableCell xr接种机构 = (XRTableCell)FindControl(接种机构, false);

                        xr接种名称.Text = tb接种史.Rows[i][tb_健康体检_非免疫规划预防接种史.接种名称].ToString();
                        xr接种日期.Text = tb接种史.Rows[i][tb_健康体检_非免疫规划预防接种史.接种日期].ToString().Substring(0,10);
                        xr接种机构.Text = tb接种史.Rows[i][tb_健康体检_非免疫规划预防接种史.接种机构].ToString();
                    }
                    
                }

                this.xrLabel_健康评价.Text = tb健康体检.Rows[0][tb_健康体检.健康评价].ToString();
                this.xrLabel_健康评价异常1.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常1].ToString();
                this.xrLabel_健康评价异常2.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常2].ToString();
                this.xrLabel_健康评价异常3.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常3].ToString();
                this.xrLabel_健康评价异常4.Text = tb健康体检.Rows[0][tb_健康体检.健康评价异常4].ToString();

                string zhidao = tb健康体检.Rows[0][tb_健康体检.健康指导].ToString();
                int j_zhidao = 1;
                if (!string.IsNullOrEmpty(zhidao))
                {
                    string[] zhidaos = zhidao.Split(',');
                    for (int i = 0; i < zhidaos.Length; i++)
                    {
                        if (zhidaos[i] == "1")
                        {
                            continue;
                        }

                        string result_zhidao = string.Empty;
                        switch (zhidaos[i])
                        {
                            case "2":
                                result_zhidao = "1";
                                break;
                            case "3":
                                result_zhidao = "2";
                                break;
                            case "4":
                                result_zhidao = "3";
                                break;
                            default:
                                result_zhidao = zhidaos[i];
                                break;
                        }

                        string xrName = "xrLabel_健康指导" + j_zhidao;
                        XRLabel xrl = (XRLabel)FindControl(xrName, false);
                        xrl.Text = result_zhidao;
                        j_zhidao += 1;
                    }

                }

                string wxkzys = tb健康体检.Rows[0][tb_健康体检.危险因素控制].ToString();
                int j_wxkzys = 1;
                if (!string.IsNullOrEmpty(wxkzys))
                {
                    string[] wxkzyss = wxkzys.Split(',');
                    for (int i = 0; i < wxkzyss.Length; i++)
                    {
                        if (wxkzyss[i] == "5" || wxkzyss[i] == "6" || wxkzyss[i] == "7" || wxkzyss[i] == "8" || wxkzyss[i] == "9" || wxkzyss[i] == "10")
                        {
                            continue;
                        }

                        string result_wxkzys = string.Empty;
                        switch (wxkzyss[i])
                        {
                            case "97":
                                result_wxkzys = "5";
                                break;
                            case "98":
                                result_wxkzys = "6";
                                break;
                            case "99":
                                result_wxkzys = "7";
                                break;
                            default:
                                result_wxkzys = wxkzyss[i];
                                break;
                        }

                        string xrName = "xrLabel_危险控制因素" + j_wxkzys;
                        XRLabel xrl = (XRLabel)FindControl(xrName, false);
                        xrl.Text = result_wxkzys;
                        j_wxkzys += 1;
                    }
                }
                xrLabel_减重目标.Text = tb健康体检.Rows[0][tb_健康体检.危险因素控制体重].ToString();
                //xrLabel_建议接种疫苗.Text = tb健康体检.Rows[0][tb_健康体检.危险因素控制疫苗].ToString();
                //wxf 2018年7月24日 11:07:54 拼接拼接危险控制因素建议接种疫苗
                StringBuilder SB_建议接种疫苗 = new StringBuilder();
                if (!string.IsNullOrEmpty(wxkzys))
                {
                    string[] wxkzyss = wxkzys.Split(',');
                    if (Array.IndexOf(wxkzyss,"5")>=0)
                    {
                        SB_建议接种疫苗.Append("接种疫苗,");
                    }
                    if (Array.IndexOf(wxkzyss,"6")>=0)
                    {
                        SB_建议接种疫苗.Append("接种流感和肺炎疫苗,");
                    }
                }
                SB_建议接种疫苗.Append(tb健康体检.Rows[0][tb_健康体检.危险因素控制疫苗].ToString());
                string str_建议接种疫苗 = SB_建议接种疫苗.ToString();
                if (str_建议接种疫苗.Length<=15)
                {
                    xrLabel_建议接种疫苗.Text = str_建议接种疫苗;
                }
                else
                {
                    xrLabel_建议接种疫苗.Text = str_建议接种疫苗.Substring(0,15);
                    xrLabel_建议接种疫苗补充.Text = str_建议接种疫苗.Substring(15, str_建议接种疫苗.Length-15);
                }
                //xrLabel_危险控制因素其他.Text = tb健康体检.Rows[0][tb_健康体检.危险因素控制其他].ToString();
                //wxf 2018年7月24日 11:17:48 拼接拼接危险控制因素其他
                StringBuilder SB_危险控制因素其他 = new StringBuilder();
                if (!string.IsNullOrEmpty(wxkzys))
                {
                    string[] wxkzyss = wxkzys.Split(',');
                    if (Array.IndexOf(wxkzyss, "7") >= 0)
                    {
                        SB_危险控制因素其他.Append("低盐饮食,");
                    }
                    if (Array.IndexOf(wxkzyss, "8") >= 0)
                    {
                        SB_危险控制因素其他.Append("低脂饮食,");
                    }
                    if (Array.IndexOf(wxkzyss, "9") >= 0)
                    {
                        SB_危险控制因素其他.Append("防滑防跌倒，防骨质疏松,");
                    }
                    if (Array.IndexOf(wxkzyss, "10") >= 0)
                    {
                        SB_危险控制因素其他.Append("防意外伤害和自救,");
                    }
                }
                SB_危险控制因素其他.Append(tb健康体检.Rows[0][tb_健康体检.危险因素控制其他].ToString());
                string str_危险控制因素其他 = SB_危险控制因素其他.ToString();
                if (str_危险控制因素其他.Length<=20)
                {
                    xrLabel_危险控制因素其他.Text = str_危险控制因素其他;
                }
                else
                {
                    xrLabel_危险控制因素其他.Text = str_危险控制因素其他.Substring(0, 20);
                    xrLabel_危险控制因素其他补充.Text = str_危险控制因素其他.Substring(20, str_危险控制因素其他.Length - 20);
                }

                //xrLabel_本人签字.Text = tb健康体检.Rows[0][tb_健康体检.本人签字].ToString();
                //xrLabel_家属签字.Text = tb健康体检.Rows[0][tb_健康体检.家属签字].ToString();
                //xrLabel_反馈人签字.Text = tb健康体检.Rows[0][tb_健康体检.反馈人签字].ToString();

                //string[] fksj = tb健康体检.Rows[0][tb_健康体检.反馈时间].ToString().Split('-');
                //防止反馈时间为空造成的数组超界
                //if (fksj.Length == 3)
                //{
                //    this.xrLabel_反馈时间年.Text = fksj[0];
                //    this.xrLabel_反馈时间月.Text = fksj[1];
                //    this.xrLabel_反馈时间日.Text = fksj[2];
                //}

            }
        }

    }
}
