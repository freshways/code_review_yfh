﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class report健康体检表A4_2_2017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_口唇 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_缺齿1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_缺齿2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_龋齿1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_龋齿2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_义齿1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_义齿2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_缺齿3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_缺齿4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_龋齿3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_龋齿4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_义齿3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_义齿4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_齿列1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_齿列2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_咽部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_脏器功能医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox脏器功能 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_视力左眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_视力右眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_矫正视力左眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_矫正视力右眼 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_听力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_运动能力 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_眼底异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_眼底 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_眼底医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_皮肤其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_皮肤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_巩膜其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_巩膜 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_淋巴结其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_淋巴结 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_桶状胸 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_呼吸音异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_呼吸音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_罗音其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_罗音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell207 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell206 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_心律 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell209 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_杂音有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_杂音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_查体医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_压痛有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_压痛 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox一般体格检查 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_包块有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_包块 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_肝大有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_肝大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell216 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_脾大有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_脾大 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_移动性浊音有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_移动性浊音 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_下肢水肿 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_足背动脉搏动 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_肛门指诊其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_肛门指诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_肛门指诊医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_乳腺其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_乳腺2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_乳腺3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_乳腺4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_乳腺1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_乳腺医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_外阴异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_外阴 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_阴道异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_阴道 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫颈异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫颈 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable_妇科医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫体异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_宫体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_附件异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_附件 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_查体其他 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_查体其他医师签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_ABO = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_Rh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血红蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_白细胞 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血小板 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox辅助检查 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_血常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿糖 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿酮体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_空腹血糖1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_空腹血糖2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_同型半胱氨酸 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_尿微量白蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Font = new System.Drawing.Font("宋体", 9.75F);
            this.Detail.HeightF = 1082.542F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 48.95833F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6,
            this.xrTableRow1,
            this.xrTableRow5,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow4,
            this.xrTableRow3,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow13,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow19,
            this.xrTableRow32,
            this.xrTableRow35,
            this.xrTableRow34,
            this.xrTableRow33,
            this.xrTableRow20,
            this.xrTableRow22,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow31,
            this.xrTableRow30,
            this.xrTableRow29,
            this.xrTableRow28,
            this.xrTableRow27,
            this.xrTableRow24,
            this.xrTableRow23,
            this.xrTableRow21,
            this.xrTableRow39,
            this.xrTableRow44,
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow41,
            this.xrTableRow40,
            this.xrTableRow36,
            this.xrTableRow38,
            this.xrTableRow17});
            this.xrTable1.SizeF = new System.Drawing.SizeF(800F, 974.9999F);
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell1,
            this.xrTableCell36,
            this.xrTableCell17,
            this.xrTableCell18});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Weight = 0.11161843716525768D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseBorders = false;
            this.xrTableCell1.Weight = 0.30993814107294138D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseBorders = false;
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "  口唇   1  红润   2  苍白   3  发绀   4 皲裂   5  疱疹";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell36.Weight = 1.4424083213046319D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_口唇});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseBorders = false;
            this.xrTableCell17.Weight = 0.13603510045716885D;
            // 
            // xrLabel_口唇
            // 
            this.xrLabel_口唇.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_口唇.LocationFloat = new DevExpress.Utils.PointFloat(22.59033F, 3.149606F);
            this.xrLabel_口唇.Name = "xrLabel_口唇";
            this.xrLabel_口唇.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_口唇.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_口唇.StylePriority.UseBorders = false;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseBorders = false;
            this.xrTableCell18.Weight = 0.19876670037244218D;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell53,
            this.xrTable_缺齿1,
            this.xrTable_缺齿2,
            this.xrTableCell52,
            this.xrTable_龋齿1,
            this.xrTable_龋齿2,
            this.xrTableCell51,
            this.xrTable_义齿1,
            this.xrTable_义齿2,
            this.xrTableCell7,
            this.xrTableCell8});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "脏";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell2.Weight = 0.11161843716525768D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseBorders = false;
            this.xrTableCell3.Text = "口    腔";
            this.xrTableCell3.Weight = 0.30993814107294138D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Weight = 0.36733943285742759D;
            // 
            // xrTable_缺齿1
            // 
            this.xrTable_缺齿1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_缺齿1.Name = "xrTable_缺齿1";
            this.xrTable_缺齿1.StylePriority.UseBorders = false;
            this.xrTable_缺齿1.Weight = 0.075490209616103421D;
            // 
            // xrTable_缺齿2
            // 
            this.xrTable_缺齿2.Name = "xrTable_缺齿2";
            this.xrTable_缺齿2.Weight = 0.075484617986793623D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Weight = 0.13571818287749282D;
            // 
            // xrTable_龋齿1
            // 
            this.xrTable_龋齿1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_龋齿1.Name = "xrTable_龋齿1";
            this.xrTable_龋齿1.StylePriority.UseBorders = false;
            this.xrTable_龋齿1.Weight = 0.0754848183588107D;
            // 
            // xrTable_龋齿2
            // 
            this.xrTable_龋齿2.Name = "xrTable_龋齿2";
            this.xrTable_龋齿2.Weight = 0.075484415525276266D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.31355231998845645D;
            // 
            // xrTable_义齿1
            // 
            this.xrTable_义齿1.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_义齿1.Name = "xrTable_义齿1";
            this.xrTable_义齿1.StylePriority.UseBorders = false;
            this.xrTable_义齿1.Weight = 0.075484818358810657D;
            // 
            // xrTable_义齿2
            // 
            this.xrTable_义齿2.Name = "xrTable_义齿2";
            this.xrTable_义齿2.Weight = 0.075484584591457432D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseBorders = false;
            this.xrTableCell7.Weight = 0.30892002160117193D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseBorders = false;
            this.xrTableCell8.Weight = 0.19876670037244218D;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell43,
            this.xrTable_缺齿3,
            this.xrTable_缺齿4,
            this.xrTableCell41,
            this.xrTable_龋齿3,
            this.xrTable_龋齿4,
            this.xrTableCell40,
            this.xrTable_义齿3,
            this.xrTable_义齿4,
            this.xrTableCell22,
            this.xrTableCell23});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.Text = "器";
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell20.Weight = 0.11161843716525768D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseBorders = false;
            this.xrTableCell21.Weight = 0.30993814107294138D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.Text = " 齿列 1 正常 2 缺齿";
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell43.Weight = 0.36733946625276392D;
            // 
            // xrTable_缺齿3
            // 
            this.xrTable_缺齿3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable_缺齿3.Name = "xrTable_缺齿3";
            this.xrTable_缺齿3.StylePriority.UseBorders = false;
            this.xrTable_缺齿3.Weight = 0.075490092732426753D;
            // 
            // xrTable_缺齿4
            // 
            this.xrTable_缺齿4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable_缺齿4.Name = "xrTable_缺齿4";
            this.xrTable_缺齿4.StylePriority.UseBorders = false;
            this.xrTable_缺齿4.Weight = 0.075484703560050825D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            this.xrTableCell41.Text = " 3 龋齿";
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell41.Weight = 0.13571822670657974D;
            // 
            // xrTable_龋齿3
            // 
            this.xrTable_龋齿3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable_龋齿3.Name = "xrTable_龋齿3";
            this.xrTable_龋齿3.StylePriority.UseBorders = false;
            this.xrTable_龋齿3.Weight = 0.075484705649551134D;
            // 
            // xrTable_龋齿4
            // 
            this.xrTable_龋齿4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable_龋齿4.Name = "xrTable_龋齿4";
            this.xrTable_龋齿4.StylePriority.UseBorders = false;
            this.xrTable_龋齿4.Weight = 0.075484705649551065D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.Text = "  4 义齿（假牙）";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell40.Weight = 0.31355229703145443D;
            // 
            // xrTable_义齿3
            // 
            this.xrTable_义齿3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTable_义齿3.Name = "xrTable_义齿3";
            this.xrTable_义齿3.StylePriority.UseBorders = false;
            this.xrTable_义齿3.Weight = 0.075484701475134086D;
            // 
            // xrTable_义齿4
            // 
            this.xrTable_义齿4.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable_义齿4.Name = "xrTable_义齿4";
            this.xrTable_义齿4.StylePriority.UseBorders = false;
            this.xrTable_义齿4.Weight = 0.075484701475134058D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_齿列1,
            this.xrLabel4,
            this.xrLabel_齿列2});
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.Weight = 0.3089198212291549D;
            // 
            // xrLabel_齿列1
            // 
            this.xrLabel_齿列1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_齿列1.LocationFloat = new DevExpress.Utils.PointFloat(51.77165F, 0F);
            this.xrLabel_齿列1.Name = "xrLabel_齿列1";
            this.xrLabel_齿列1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_齿列1.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_齿列1.StylePriority.UseBorders = false;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(68.28075F, 0F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "/";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_齿列2
            // 
            this.xrLabel_齿列2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_齿列2.LocationFloat = new DevExpress.Utils.PointFloat(84.78984F, 0F);
            this.xrLabel_齿列2.Name = "xrLabel_齿列2";
            this.xrLabel_齿列2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_齿列2.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_齿列2.StylePriority.UseBorders = false;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseBorders = false;
            this.xrTableCell23.Weight = 0.19876670037244218D;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell25,
            this.xrTableCell55,
            this.xrTableCell26,
            this.xrTable_脏器功能医师签名});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.Text = "功";
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell24.Weight = 0.11161843716525768D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseBorders = false;
            this.xrTableCell25.Weight = 0.30993814107294138D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseBorders = false;
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.Text = " 咽部 1 无充血  2 充血  3 淋巴滤泡增生";
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell55.Weight = 1.4424081710256191D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_咽部});
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseBorders = false;
            this.xrTableCell26.Weight = 0.13603525073618172D;
            // 
            // xrLabel_咽部
            // 
            this.xrLabel_咽部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_咽部.LocationFloat = new DevExpress.Utils.PointFloat(22.59033F, 3.149606F);
            this.xrLabel_咽部.Name = "xrLabel_咽部";
            this.xrLabel_咽部.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_咽部.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_咽部.StylePriority.UseBorders = false;
            // 
            // xrTable_脏器功能医师签名
            // 
            this.xrTable_脏器功能医师签名.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_脏器功能医师签名.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox脏器功能});
            this.xrTable_脏器功能医师签名.Name = "xrTable_脏器功能医师签名";
            this.xrTable_脏器功能医师签名.StylePriority.UseBorders = false;
            this.xrTable_脏器功能医师签名.Weight = 0.19876670037244218D;
            // 
            // xrPictureBox脏器功能
            // 
            this.xrPictureBox脏器功能.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox脏器功能.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 0F);
            this.xrPictureBox脏器功能.Name = "xrPictureBox脏器功能";
            this.xrPictureBox脏器功能.SizeF = new System.Drawing.SizeF(68F, 25F);
            this.xrPictureBox脏器功能.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox脏器功能.StylePriority.UseBorders = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29,
            this.xrTableCell62,
            this.xrTableCell57,
            this.xrTableCell61,
            this.xrTableCell56,
            this.xrTableCell60,
            this.xrTableCell58,
            this.xrTableCell64,
            this.xrTableCell59,
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseBorders = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.Text = "能";
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            this.xrTableCell28.Weight = 0.11161843716525768D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.Text = "视    力";
            this.xrTableCell29.Weight = 0.30993814107294138D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseBorders = false;
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.Text = "左眼";
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell62.Weight = 0.093073540808464625D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell57.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_视力左眼});
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseBorders = false;
            this.xrTableCell57.Weight = 0.16414476056812913D;
            // 
            // xrLabel_视力左眼
            // 
            this.xrLabel_视力左眼.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_视力左眼.LocationFloat = new DevExpress.Utils.PointFloat(3.754623E-06F, 1.868697F);
            this.xrLabel_视力左眼.Name = "xrLabel_视力左眼";
            this.xrLabel_视力左眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_视力左眼.SizeF = new System.Drawing.SizeF(59.05512F, 19.66659F);
            this.xrLabel_视力左眼.StylePriority.UseBorders = false;
            this.xrLabel_视力左眼.StylePriority.UseTextAlignment = false;
            this.xrLabel_视力左眼.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseBorders = false;
            this.xrTableCell61.Text = "右眼";
            this.xrTableCell61.Weight = 0.091490159385355319D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell56.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_视力右眼});
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseBorders = false;
            this.xrTableCell56.Weight = 0.16414475221929503D;
            // 
            // xrLabel_视力右眼
            // 
            this.xrLabel_视力右眼.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_视力右眼.LocationFloat = new DevExpress.Utils.PointFloat(2.778421E-05F, 1.868697F);
            this.xrLabel_视力右眼.Name = "xrLabel_视力右眼";
            this.xrLabel_视力右眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_视力右眼.SizeF = new System.Drawing.SizeF(59.05512F, 19.66659F);
            this.xrLabel_视力右眼.StylePriority.UseBorders = false;
            this.xrLabel_视力右眼.StylePriority.UseTextAlignment = false;
            this.xrLabel_视力右眼.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.Text = "（矫正视力：左眼";
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell60.Weight = 0.33847360839952351D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell58.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_矫正视力左眼});
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseBorders = false;
            this.xrTableCell58.Weight = 0.16414476056812916D;
            // 
            // xrLabel_矫正视力左眼
            // 
            this.xrLabel_矫正视力左眼.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_矫正视力左眼.LocationFloat = new DevExpress.Utils.PointFloat(2.778421E-05F, 1.868697F);
            this.xrLabel_矫正视力左眼.Name = "xrLabel_矫正视力左眼";
            this.xrLabel_矫正视力左眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_矫正视力左眼.SizeF = new System.Drawing.SizeF(59.05512F, 19.66659F);
            this.xrLabel_矫正视力左眼.StylePriority.UseBorders = false;
            this.xrLabel_矫正视力左眼.StylePriority.UseTextAlignment = false;
            this.xrLabel_矫正视力左眼.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseBorders = false;
            this.xrTableCell64.Text = "右眼";
            this.xrTableCell64.Weight = 0.086345113176205426D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell59.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_矫正视力右眼});
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.Weight = 0.16414475847862886D;
            // 
            // xrLabel_矫正视力右眼
            // 
            this.xrLabel_矫正视力右眼.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_矫正视力右眼.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.868697F);
            this.xrLabel_矫正视力右眼.Name = "xrLabel_矫正视力右眼";
            this.xrLabel_矫正视力右眼.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_矫正视力右眼.SizeF = new System.Drawing.SizeF(59.05508F, 19.66659F);
            this.xrLabel_矫正视力右眼.StylePriority.UseBorders = false;
            this.xrLabel_矫正视力右眼.StylePriority.UseTextAlignment = false;
            this.xrLabel_矫正视力右眼.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.Text = "）";
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell30.Weight = 0.31248196815806983D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.Weight = 0.19876670037244218D;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell63,
            this.xrTableCell34,
            this.xrTableCell35});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell32.Weight = 0.11161843716525768D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseBorders = false;
            this.xrTableCell33.Text = "听    力";
            this.xrTableCell33.Weight = 0.30993814107294138D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseBorders = false;
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "  1 听见\t  2 听不清或无法听见";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell63.Weight = 1.4424081710256191D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_听力});
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.Weight = 0.13603525073618172D;
            // 
            // xrLabel_听力
            // 
            this.xrLabel_听力.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_听力.LocationFloat = new DevExpress.Utils.PointFloat(22.59023F, 3.149606F);
            this.xrLabel_听力.Name = "xrLabel_听力";
            this.xrLabel_听力.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_听力.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_听力.StylePriority.UseBorders = false;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseBorders = false;
            this.xrTableCell35.Weight = 0.19876670037244218D;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell101,
            this.xrTableCell15,
            this.xrTableCell19});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseBorders = false;
            this.xrTableCell13.Weight = 0.11161843716525768D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseBorders = false;
            this.xrTableCell14.Text = "运动能力";
            this.xrTableCell14.Weight = 0.30993814107294138D;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.StylePriority.UseBorders = false;
            this.xrTableCell101.StylePriority.UseTextAlignment = false;
            this.xrTableCell101.Text = "  1 可顺利完成  2 无法独立完成任何一个动作";
            this.xrTableCell101.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell101.Weight = 1.4424081710256191D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_运动能力});
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseBorders = false;
            this.xrTableCell15.Weight = 0.13603525073618172D;
            // 
            // xrLabel_运动能力
            // 
            this.xrLabel_运动能力.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_运动能力.LocationFloat = new DevExpress.Utils.PointFloat(22.59023F, 3.149606F);
            this.xrLabel_运动能力.Name = "xrLabel_运动能力";
            this.xrLabel_运动能力.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_运动能力.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_运动能力.StylePriority.UseBorders = false;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseBorders = false;
            this.xrTableCell19.Weight = 0.19876670037244218D;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell193,
            this.xrTableCell194,
            this.xrTableCell11,
            this.xrTable_眼底医师签名});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseBorders = false;
            this.xrTableCell9.Weight = 0.11161843716525768D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseBorders = false;
            this.xrTableCell10.Text = " 眼    底*";
            this.xrTableCell10.Weight = 0.30993814107294138D;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.StylePriority.UseBorders = false;
            this.xrTableCell193.StylePriority.UseTextAlignment = false;
            this.xrTableCell193.Text = "  1 正常  2 异常";
            this.xrTableCell193.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell193.Weight = 0.29817361532765035D;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell194.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_眼底异常});
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.StylePriority.UseBorders = false;
            this.xrTableCell194.Weight = 1.1442345640559697D;
            // 
            // xrLabel_眼底异常
            // 
            this.xrLabel_眼底异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_眼底异常.LocationFloat = new DevExpress.Utils.PointFloat(2.778421E-05F, 0F);
            this.xrLabel_眼底异常.Name = "xrLabel_眼底异常";
            this.xrLabel_眼底异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_眼底异常.SizeF = new System.Drawing.SizeF(157.4803F, 19.66661F);
            this.xrLabel_眼底异常.StylePriority.UseBorders = false;
            this.xrLabel_眼底异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_眼底异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_眼底});
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseBorders = false;
            this.xrTableCell11.Weight = 0.13603524237818082D;
            // 
            // xrLabel_眼底
            // 
            this.xrLabel_眼底.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_眼底.LocationFloat = new DevExpress.Utils.PointFloat(22.59013F, 3.149606F);
            this.xrLabel_眼底.Name = "xrLabel_眼底";
            this.xrLabel_眼底.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_眼底.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_眼底.StylePriority.UseBorders = false;
            // 
            // xrTable_眼底医师签名
            // 
            this.xrTable_眼底医师签名.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_眼底医师签名.Name = "xrTable_眼底医师签名";
            this.xrTable_眼底医师签名.StylePriority.UseBorders = false;
            this.xrTable_眼底医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell195,
            this.xrTableCell196,
            this.xrTableCell67,
            this.xrTableCell68});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseBorders = false;
            this.xrTableCell65.Weight = 0.11161843716525768D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.Text = "皮    肤";
            this.xrTableCell66.Weight = 0.30993814107294138D;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.StylePriority.UseBorders = false;
            this.xrTableCell195.StylePriority.UseTextAlignment = false;
            this.xrTableCell195.Text = "  1 正常  2 潮红  3 苍白  4 发绀  5 黄染  6 色素沉着  7 其他";
            this.xrTableCell195.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell195.Weight = 1.1806704871618643D;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell196.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_皮肤其他});
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.StylePriority.UseBorders = false;
            this.xrTableCell196.Weight = 0.26173782580310057D;
            // 
            // xrLabel_皮肤其他
            // 
            this.xrLabel_皮肤其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_皮肤其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_皮肤其他.Name = "xrLabel_皮肤其他";
            this.xrLabel_皮肤其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_皮肤其他.SizeF = new System.Drawing.SizeF(89.06693F, 19.66659F);
            this.xrLabel_皮肤其他.StylePriority.UseBorders = false;
            this.xrLabel_皮肤其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_皮肤其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell67.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_皮肤});
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.Weight = 0.13603510879683606D;
            // 
            // xrLabel_皮肤
            // 
            this.xrLabel_皮肤.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_皮肤.LocationFloat = new DevExpress.Utils.PointFloat(22.59033F, 3.149606F);
            this.xrLabel_皮肤.Name = "xrLabel_皮肤";
            this.xrLabel_皮肤.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_皮肤.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_皮肤.StylePriority.UseBorders = false;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseBorders = false;
            this.xrTableCell68.Weight = 0.19876670037244218D;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell69,
            this.xrTableCell70,
            this.xrTableCell197,
            this.xrTableCell198,
            this.xrTableCell71,
            this.xrTableCell72});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseBorders = false;
            this.xrTableCell69.Weight = 0.11161843716525768D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseBorders = false;
            this.xrTableCell70.Text = "巩    膜";
            this.xrTableCell70.Weight = 0.30993814107294138D;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.StylePriority.UseBorders = false;
            this.xrTableCell197.StylePriority.UseTextAlignment = false;
            this.xrTableCell197.Text = "  1 正常  2 黄染  3 充血  4 其他";
            this.xrTableCell197.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell197.Weight = 0.620134377946544D;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell198.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_巩膜其他});
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.StylePriority.UseBorders = false;
            this.xrTableCell198.Weight = 0.82227393501842094D;
            // 
            // xrLabel_巩膜其他
            // 
            this.xrLabel_巩膜其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_巩膜其他.LocationFloat = new DevExpress.Utils.PointFloat(2.778421E-05F, 7.208878E-05F);
            this.xrLabel_巩膜其他.Name = "xrLabel_巩膜其他";
            this.xrLabel_巩膜其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_巩膜其他.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_巩膜其他.StylePriority.UseBorders = false;
            this.xrLabel_巩膜其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_巩膜其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell71.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_巩膜});
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseBorders = false;
            this.xrTableCell71.Weight = 0.13603510879683606D;
            // 
            // xrLabel_巩膜
            // 
            this.xrLabel_巩膜.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_巩膜.LocationFloat = new DevExpress.Utils.PointFloat(22.59003F, 3.149606F);
            this.xrLabel_巩膜.Name = "xrLabel_巩膜";
            this.xrLabel_巩膜.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_巩膜.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_巩膜.StylePriority.UseBorders = false;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseBorders = false;
            this.xrTableCell72.Weight = 0.19876670037244218D;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell199,
            this.xrTableCell200,
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseBorders = false;
            this.xrTableCell77.Weight = 0.11161843716525768D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseBorders = false;
            this.xrTableCell78.Text = "淋 巴 结";
            this.xrTableCell78.Weight = 0.30993814107294138D;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.StylePriority.UseBorders = false;
            this.xrTableCell199.StylePriority.UseTextAlignment = false;
            this.xrTableCell199.Text = "  1 未触及  2 锁骨长  3 腋窝  4 其他";
            this.xrTableCell199.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell199.Weight = 0.7058361612770887D;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell200.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_淋巴结其他});
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.StylePriority.UseBorders = false;
            this.xrTableCell200.Weight = 0.73657201810653139D;
            // 
            // xrLabel_淋巴结其他
            // 
            this.xrLabel_淋巴结其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_淋巴结其他.LocationFloat = new DevExpress.Utils.PointFloat(5.086264E-05F, 5.086264E-05F);
            this.xrLabel_淋巴结其他.Name = "xrLabel_淋巴结其他";
            this.xrLabel_淋巴结其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_淋巴结其他.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_淋巴结其他.StylePriority.UseBorders = false;
            this.xrLabel_淋巴结其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_淋巴结其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_淋巴结});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseBorders = false;
            this.xrTableCell79.Weight = 0.13603524237818082D;
            // 
            // xrLabel_淋巴结
            // 
            this.xrLabel_淋巴结.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_淋巴结.LocationFloat = new DevExpress.Utils.PointFloat(22.59013F, 3.149606F);
            this.xrLabel_淋巴结.Name = "xrLabel_淋巴结";
            this.xrLabel_淋巴结.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_淋巴结.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_淋巴结.StylePriority.UseBorders = false;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseBorders = false;
            this.xrTableCell80.Weight = 0.19876670037244218D;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell86,
            this.xrTableCell12,
            this.xrTableCell87,
            this.xrTableCell88});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseBorders = false;
            this.xrTableCell85.Weight = 0.11161843716525768D;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseBorders = false;
            this.xrTableCell86.Weight = 0.30993814107294138D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "  桶状胸：1 否   2 是";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell12.Weight = 1.4424081710256191D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell87.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_桶状胸});
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseBorders = false;
            this.xrTableCell87.Weight = 0.13603525073618172D;
            // 
            // xrLabel_桶状胸
            // 
            this.xrLabel_桶状胸.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_桶状胸.LocationFloat = new DevExpress.Utils.PointFloat(22.59038F, 3.149606F);
            this.xrLabel_桶状胸.Name = "xrLabel_桶状胸";
            this.xrLabel_桶状胸.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_桶状胸.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_桶状胸.StylePriority.UseBorders = false;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.Weight = 0.19876670037244218D;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell89,
            this.xrTableCell90,
            this.xrTableCell201,
            this.xrTableCell202,
            this.xrTableCell91,
            this.xrTableCell92});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseBorders = false;
            this.xrTableCell89.Weight = 0.11161843716525768D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseBorders = false;
            this.xrTableCell90.Text = "肺";
            this.xrTableCell90.Weight = 0.30993814107294138D;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.StylePriority.UseTextAlignment = false;
            this.xrTableCell201.Text = "  呼吸音：1 正常 2 异常";
            this.xrTableCell201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell201.Weight = 0.5622278000808274D;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_呼吸音异常});
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Weight = 0.88018037930279258D;
            // 
            // xrLabel_呼吸音异常
            // 
            this.xrLabel_呼吸音异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_呼吸音异常.LocationFloat = new DevExpress.Utils.PointFloat(0.8530173F, 0F);
            this.xrLabel_呼吸音异常.Name = "xrLabel_呼吸音异常";
            this.xrLabel_呼吸音异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_呼吸音异常.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_呼吸音异常.StylePriority.UseBorders = false;
            this.xrLabel_呼吸音异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_呼吸音异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell91.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_呼吸音});
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.StylePriority.UseBorders = false;
            this.xrTableCell91.Weight = 0.13603524237818082D;
            // 
            // xrLabel_呼吸音
            // 
            this.xrLabel_呼吸音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_呼吸音.LocationFloat = new DevExpress.Utils.PointFloat(22.59038F, 3.149606F);
            this.xrLabel_呼吸音.Name = "xrLabel_呼吸音";
            this.xrLabel_呼吸音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_呼吸音.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_呼吸音.StylePriority.UseBorders = false;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.StylePriority.UseBorders = false;
            this.xrTableCell92.Weight = 0.19876670037244218D;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.xrTableCell37,
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell38,
            this.xrTableCell39});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseBorders = false;
            this.xrTableCell27.Weight = 0.11161843716525768D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseBorders = false;
            this.xrTableCell37.Weight = 0.30993814107294138D;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.StylePriority.UseBorders = false;
            this.xrTableCell203.StylePriority.UseTextAlignment = false;
            this.xrTableCell203.Text = "  罗  音：1 无   2 干罗音  3 湿罗音  4 其他";
            this.xrTableCell203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell203.Weight = 1.0023181525299041D;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell204.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_罗音其他});
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.StylePriority.UseBorders = false;
            this.xrTableCell204.Weight = 0.4400900268537159D;
            // 
            // xrLabel_罗音其他
            // 
            this.xrLabel_罗音其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_罗音其他.LocationFloat = new DevExpress.Utils.PointFloat(0.4374626F, 0F);
            this.xrLabel_罗音其他.Name = "xrLabel_罗音其他";
            this.xrLabel_罗音其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_罗音其他.SizeF = new System.Drawing.SizeF(128.3136F, 19.66656F);
            this.xrLabel_罗音其他.StylePriority.UseBorders = false;
            this.xrLabel_罗音其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_罗音其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell38.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_罗音});
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.Weight = 0.13603524237818082D;
            // 
            // xrLabel_罗音
            // 
            this.xrLabel_罗音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_罗音.LocationFloat = new DevExpress.Utils.PointFloat(22.59013F, 3.149606F);
            this.xrLabel_罗音.Name = "xrLabel_罗音";
            this.xrLabel_罗音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_罗音.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_罗音.StylePriority.UseBorders = false;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseBorders = false;
            this.xrTableCell39.Weight = 0.19876670037244218D;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell141,
            this.xrTableCell142,
            this.xrTableCell207,
            this.xrTableCell205,
            this.xrTableCell206,
            this.xrTableCell143,
            this.xrTableCell144});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.StylePriority.UseBorders = false;
            this.xrTableCell141.Weight = 0.11161843716525768D;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.StylePriority.UseBorders = false;
            this.xrTableCell142.Text = "心    脏";
            this.xrTableCell142.Weight = 0.30993814107294138D;
            // 
            // xrTableCell207
            // 
            this.xrTableCell207.Name = "xrTableCell207";
            this.xrTableCell207.StylePriority.UseTextAlignment = false;
            this.xrTableCell207.Text = "  心率";
            this.xrTableCell207.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell207.Weight = 0.13131596162269152D;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心率});
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Weight = 0.27484584106592169D;
            // 
            // xrLabel_心率
            // 
            this.xrLabel_心率.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_心率.LocationFloat = new DevExpress.Utils.PointFloat(1.525879E-05F, 4.577637E-05F);
            this.xrLabel_心率.Name = "xrLabel_心率";
            this.xrLabel_心率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心率.SizeF = new System.Drawing.SizeF(100F, 19.6666F);
            this.xrLabel_心率.StylePriority.UseBorders = false;
            this.xrLabel_心率.StylePriority.UseTextAlignment = false;
            this.xrLabel_心率.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell206
            // 
            this.xrTableCell206.Name = "xrTableCell206";
            this.xrTableCell206.StylePriority.UseTextAlignment = false;
            this.xrTableCell206.Text = "次/分钟\t心律：1 齐    2 不齐   3 绝对不齐";
            this.xrTableCell206.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell206.Weight = 1.0362466438576963D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell143.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_心律});
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.Weight = 0.13603497521549135D;
            // 
            // xrLabel_心律
            // 
            this.xrLabel_心律.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_心律.LocationFloat = new DevExpress.Utils.PointFloat(22.59028F, 3.149606F);
            this.xrLabel_心律.Name = "xrLabel_心律";
            this.xrLabel_心律.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_心律.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_心律.StylePriority.UseBorders = false;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.Weight = 0.19876670037244218D;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell153,
            this.xrTableCell154,
            this.xrTableCell208,
            this.xrTableCell209,
            this.xrTableCell155,
            this.xrTable_查体医师签名});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.StylePriority.UseBorders = false;
            this.xrTableCell153.Weight = 0.11161843716525768D;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.StylePriority.UseBorders = false;
            this.xrTableCell154.Weight = 0.30993814107294138D;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.StylePriority.UseBorders = false;
            this.xrTableCell208.StylePriority.UseTextAlignment = false;
            this.xrTableCell208.Text = "  杂音：1 无\t2 有";
            this.xrTableCell208.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell208.Weight = 0.52979989339518041D;
            // 
            // xrTableCell209
            // 
            this.xrTableCell209.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell209.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_杂音有});
            this.xrTableCell209.Name = "xrTableCell209";
            this.xrTableCell209.StylePriority.UseBorders = false;
            this.xrTableCell209.Weight = 0.91260882031381851D;
            // 
            // xrLabel_杂音有
            // 
            this.xrLabel_杂音有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_杂音有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.7454429F);
            this.xrLabel_杂音有.Name = "xrLabel_杂音有";
            this.xrLabel_杂音有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_杂音有.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_杂音有.StylePriority.UseBorders = false;
            this.xrLabel_杂音有.StylePriority.UseTextAlignment = false;
            this.xrLabel_杂音有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell155.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_杂音});
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.StylePriority.UseBorders = false;
            this.xrTableCell155.Weight = 0.13603470805280193D;
            // 
            // xrLabel_杂音
            // 
            this.xrLabel_杂音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_杂音.LocationFloat = new DevExpress.Utils.PointFloat(22.59023F, 3.149606F);
            this.xrLabel_杂音.Name = "xrLabel_杂音";
            this.xrLabel_杂音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_杂音.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_杂音.StylePriority.UseBorders = false;
            // 
            // xrTable_查体医师签名
            // 
            this.xrTable_查体医师签名.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_查体医师签名.Name = "xrTable_查体医师签名";
            this.xrTable_查体医师签名.StylePriority.UseBorders = false;
            this.xrTable_查体医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149,
            this.xrTableCell150,
            this.xrTableCell210,
            this.xrTableCell211,
            this.xrTableCell151,
            this.xrTableCell152});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.StylePriority.UseBorders = false;
            this.xrTableCell149.Weight = 0.11161843716525768D;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.StylePriority.UseBorders = false;
            this.xrTableCell150.Weight = 0.30993814107294138D;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.StylePriority.UseTextAlignment = false;
            this.xrTableCell210.Text = "  压痛：1 无\t2 有";
            this.xrTableCell210.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell210.Weight = 0.529799993581189D;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_压痛有});
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.StylePriority.UseTextAlignment = false;
            this.xrTableCell211.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell211.Weight = 0.91260912087184409D;
            // 
            // xrLabel_压痛有
            // 
            this.xrLabel_压痛有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_压痛有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_压痛有.Name = "xrLabel_压痛有";
            this.xrLabel_压痛有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_压痛有.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_压痛有.StylePriority.UseBorders = false;
            this.xrLabel_压痛有.StylePriority.UseTextAlignment = false;
            this.xrLabel_压痛有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell151.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_压痛});
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.StylePriority.UseBorders = false;
            this.xrTableCell151.Weight = 0.13603430730876776D;
            // 
            // xrLabel_压痛
            // 
            this.xrLabel_压痛.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_压痛.LocationFloat = new DevExpress.Utils.PointFloat(22.58972F, 3.149606F);
            this.xrLabel_压痛.Name = "xrLabel_压痛";
            this.xrLabel_压痛.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_压痛.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_压痛.StylePriority.UseBorders = false;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell152.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox一般体格检查});
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseBorders = false;
            this.xrTableCell152.Weight = 0.19876670037244218D;
            // 
            // xrPictureBox一般体格检查
            // 
            this.xrPictureBox一般体格检查.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox一般体格检查.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 1.525879E-05F);
            this.xrPictureBox一般体格检查.Name = "xrPictureBox一般体格检查";
            this.xrPictureBox一般体格检查.SizeF = new System.Drawing.SizeF(68F, 25F);
            this.xrPictureBox一般体格检查.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox一般体格检查.StylePriority.UseBorders = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell145,
            this.xrTableCell146,
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell147,
            this.xrTableCell148});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.StylePriority.UseBorders = false;
            this.xrTableCell145.Weight = 0.11161843716525768D;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseBorders = false;
            this.xrTableCell146.Weight = 0.30993814107294138D;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.StylePriority.UseTextAlignment = false;
            this.xrTableCell212.Text = "  包块：1 无\t2 有";
            this.xrTableCell212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell212.Weight = 0.529799993581189D;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_包块有});
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.StylePriority.UseTextAlignment = false;
            this.xrTableCell213.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell213.Weight = 0.912608185802431D;
            // 
            // xrLabel_包块有
            // 
            this.xrLabel_包块有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_包块有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_包块有.Name = "xrLabel_包块有";
            this.xrLabel_包块有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_包块有.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_包块有.StylePriority.UseBorders = false;
            this.xrLabel_包块有.StylePriority.UseTextAlignment = false;
            this.xrLabel_包块有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell147.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_包块});
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseBorders = false;
            this.xrTableCell147.Weight = 0.13603524237818082D;
            // 
            // xrLabel_包块
            // 
            this.xrLabel_包块.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_包块.LocationFloat = new DevExpress.Utils.PointFloat(22.59003F, 3.149606F);
            this.xrLabel_包块.Name = "xrLabel_包块";
            this.xrLabel_包块.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_包块.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_包块.StylePriority.UseBorders = false;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.StylePriority.UseBorders = false;
            this.xrTableCell148.Weight = 0.19876670037244218D;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell44,
            this.xrTableCell214,
            this.xrTableCell215,
            this.xrTableCell45,
            this.xrTableCell46});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseBorders = false;
            this.xrTableCell42.Text = "查";
            this.xrTableCell42.Weight = 0.11161843716525768D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseBorders = false;
            this.xrTableCell44.Text = "腹    部";
            this.xrTableCell44.Weight = 0.30993814107294138D;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.StylePriority.UseTextAlignment = false;
            this.xrTableCell214.Text = "  肝大：1 无\t2 有";
            this.xrTableCell214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell214.Weight = 0.529799993581189D;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_肝大有});
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.StylePriority.UseTextAlignment = false;
            this.xrTableCell215.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell215.Weight = 0.912608185802431D;
            // 
            // xrLabel_肝大有
            // 
            this.xrLabel_肝大有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_肝大有.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_肝大有.Name = "xrLabel_肝大有";
            this.xrLabel_肝大有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_肝大有.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_肝大有.StylePriority.UseBorders = false;
            this.xrLabel_肝大有.StylePriority.UseTextAlignment = false;
            this.xrLabel_肝大有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell45.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_肝大});
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Weight = 0.13603524237818082D;
            // 
            // xrLabel_肝大
            // 
            this.xrLabel_肝大.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肝大.LocationFloat = new DevExpress.Utils.PointFloat(22.59033F, 3.149606F);
            this.xrLabel_肝大.Name = "xrLabel_肝大";
            this.xrLabel_肝大.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_肝大.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_肝大.StylePriority.UseBorders = false;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseBorders = false;
            this.xrTableCell46.Weight = 0.19876670037244218D;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.xrTableCell102,
            this.xrTableCell216,
            this.xrTableCell217,
            this.xrTableCell103,
            this.xrTableCell104});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseBorders = false;
            this.xrTableCell54.Text = "体";
            this.xrTableCell54.Weight = 0.11161843716525768D;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseBorders = false;
            this.xrTableCell102.Weight = 0.30993814107294138D;
            // 
            // xrTableCell216
            // 
            this.xrTableCell216.Name = "xrTableCell216";
            this.xrTableCell216.StylePriority.UseTextAlignment = false;
            this.xrTableCell216.Text = "  脾大：1 无\t2 有";
            this.xrTableCell216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell216.Weight = 0.529799993581189D;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_脾大有});
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.StylePriority.UseTextAlignment = false;
            this.xrTableCell217.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell217.Weight = 0.912608185802431D;
            // 
            // xrLabel_脾大有
            // 
            this.xrLabel_脾大有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_脾大有.LocationFloat = new DevExpress.Utils.PointFloat(2.402959E-05F, 0.8844571F);
            this.xrLabel_脾大有.Name = "xrLabel_脾大有";
            this.xrLabel_脾大有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_脾大有.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_脾大有.StylePriority.UseBorders = false;
            this.xrLabel_脾大有.StylePriority.UseTextAlignment = false;
            this.xrLabel_脾大有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell103.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_脾大});
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.StylePriority.UseBorders = false;
            this.xrTableCell103.Weight = 0.13603524237818082D;
            // 
            // xrLabel_脾大
            // 
            this.xrLabel_脾大.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_脾大.LocationFloat = new DevExpress.Utils.PointFloat(22.59033F, 3.149606F);
            this.xrLabel_脾大.Name = "xrLabel_脾大";
            this.xrLabel_脾大.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_脾大.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_脾大.StylePriority.UseBorders = false;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.StylePriority.UseBorders = false;
            this.xrTableCell104.Weight = 0.19876670037244218D;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell218,
            this.xrTableCell219,
            this.xrTableCell115,
            this.xrTableCell116});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.StylePriority.UseBorders = false;
            this.xrTableCell113.Weight = 0.11161843716525768D;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.StylePriority.UseBorders = false;
            this.xrTableCell114.Weight = 0.30993814107294138D;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.StylePriority.UseBorders = false;
            this.xrTableCell218.StylePriority.UseTextAlignment = false;
            this.xrTableCell218.Text = "  移动性浊音：1 无  2 有";
            this.xrTableCell218.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell218.Weight = 0.529799993581189D;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell219.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_移动性浊音有});
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.StylePriority.UseBorders = false;
            this.xrTableCell219.StylePriority.UseTextAlignment = false;
            this.xrTableCell219.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell219.Weight = 0.912608185802431D;
            // 
            // xrLabel_移动性浊音有
            // 
            this.xrLabel_移动性浊音有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_移动性浊音有.LocationFloat = new DevExpress.Utils.PointFloat(2.778421E-05F, 0F);
            this.xrLabel_移动性浊音有.Name = "xrLabel_移动性浊音有";
            this.xrLabel_移动性浊音有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_移动性浊音有.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_移动性浊音有.StylePriority.UseBorders = false;
            this.xrLabel_移动性浊音有.StylePriority.UseTextAlignment = false;
            this.xrLabel_移动性浊音有.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell115.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_移动性浊音});
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.StylePriority.UseBorders = false;
            this.xrTableCell115.Weight = 0.13603524237818082D;
            // 
            // xrLabel_移动性浊音
            // 
            this.xrLabel_移动性浊音.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_移动性浊音.LocationFloat = new DevExpress.Utils.PointFloat(22.59033F, 3.149606F);
            this.xrLabel_移动性浊音.Name = "xrLabel_移动性浊音";
            this.xrLabel_移动性浊音.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_移动性浊音.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_移动性浊音.StylePriority.UseBorders = false;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.StylePriority.UseBorders = false;
            this.xrTableCell116.Weight = 0.19876670037244218D;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell220,
            this.xrTableCell119,
            this.xrTableCell120});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.StylePriority.UseBorders = false;
            this.xrTableCell117.Weight = 0.11161843716525768D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.StylePriority.UseBorders = false;
            this.xrTableCell118.Text = "下肢水肿";
            this.xrTableCell118.Weight = 0.30993814107294138D;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.StylePriority.UseBorders = false;
            this.xrTableCell220.StylePriority.UseTextAlignment = false;
            this.xrTableCell220.Text = "  1 无   2 单侧   3 双侧不对称   4 双侧对称";
            this.xrTableCell220.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell220.Weight = 1.4424089725136875D;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell119.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_下肢水肿});
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.StylePriority.UseBorders = false;
            this.xrTableCell119.Weight = 0.13603444924811337D;
            // 
            // xrLabel_下肢水肿
            // 
            this.xrLabel_下肢水肿.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_下肢水肿.LocationFloat = new DevExpress.Utils.PointFloat(22.59013F, 3.149606F);
            this.xrLabel_下肢水肿.Name = "xrLabel_下肢水肿";
            this.xrLabel_下肢水肿.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_下肢水肿.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_下肢水肿.StylePriority.UseBorders = false;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.StylePriority.UseBorders = false;
            this.xrTableCell120.Weight = 0.19876670037244218D;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell138,
            this.xrTableCell221,
            this.xrTableCell139,
            this.xrTableCell140});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.StylePriority.UseBorders = false;
            this.xrTableCell137.Weight = 0.11161843716525768D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseBorders = false;
            this.xrTableCell138.Text = "足背动脉搏动*";
            this.xrTableCell138.Weight = 0.30993814107294138D;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.StylePriority.UseBorders = false;
            this.xrTableCell221.StylePriority.UseTextAlignment = false;
            this.xrTableCell221.Text = "  1 未触及   2 触及双侧对称   3 触及左侧弱或消失   4 触及右侧弱或消失";
            this.xrTableCell221.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell221.Weight = 1.4424080708396108D;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell139.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_足背动脉搏动});
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.StylePriority.UseBorders = false;
            this.xrTableCell139.Weight = 0.13603535092219021D;
            // 
            // xrLabel_足背动脉搏动
            // 
            this.xrLabel_足背动脉搏动.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_足背动脉搏动.LocationFloat = new DevExpress.Utils.PointFloat(22.59003F, 3.149606F);
            this.xrLabel_足背动脉搏动.Name = "xrLabel_足背动脉搏动";
            this.xrLabel_足背动脉搏动.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_足背动脉搏动.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_足背动脉搏动.StylePriority.UseBorders = false;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.Weight = 0.19876670037244218D;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133,
            this.xrTableCell134,
            this.xrTableCell222,
            this.xrTableCell223,
            this.xrTableCell135,
            this.xrTable_肛门指诊医师签名});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.Weight = 0.11161843716525768D;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.StylePriority.UseBorders = false;
            this.xrTableCell134.Text = "  肛门指诊*";
            this.xrTableCell134.Weight = 0.30993814107294138D;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.StylePriority.UseBorders = false;
            this.xrTableCell222.StylePriority.UseTextAlignment = false;
            this.xrTableCell222.Text = "  1 未及异常  2 触痛  3 包块  4 前列腺异常  5 其他";
            this.xrTableCell222.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell222.Weight = 0.9907367835242229D;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell223.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_肛门指诊其他});
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.StylePriority.UseBorders = false;
            this.xrTableCell223.Weight = 0.451671395859397D;
            // 
            // xrLabel_肛门指诊其他
            // 
            this.xrLabel_肛门指诊其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_肛门指诊其他.LocationFloat = new DevExpress.Utils.PointFloat(2.778421E-05F, 0F);
            this.xrLabel_肛门指诊其他.Name = "xrLabel_肛门指诊其他";
            this.xrLabel_肛门指诊其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_肛门指诊其他.SizeF = new System.Drawing.SizeF(124.9002F, 19.66659F);
            this.xrLabel_肛门指诊其他.StylePriority.UseBorders = false;
            this.xrLabel_肛门指诊其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_肛门指诊其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell135.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_肛门指诊});
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.StylePriority.UseBorders = false;
            this.xrTableCell135.Weight = 0.13603524237818082D;
            // 
            // xrLabel_肛门指诊
            // 
            this.xrLabel_肛门指诊.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_肛门指诊.LocationFloat = new DevExpress.Utils.PointFloat(22.58993F, 3.149606F);
            this.xrLabel_肛门指诊.Name = "xrLabel_肛门指诊";
            this.xrLabel_肛门指诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_肛门指诊.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_肛门指诊.StylePriority.UseBorders = false;
            // 
            // xrTable_肛门指诊医师签名
            // 
            this.xrTable_肛门指诊医师签名.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_肛门指诊医师签名.Name = "xrTable_肛门指诊医师签名";
            this.xrTable_肛门指诊医师签名.StylePriority.UseBorders = false;
            this.xrTable_肛门指诊医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.xrTableCell130,
            this.xrTableCell224,
            this.xrTableCell225,
            this.xrTableCell131,
            this.xrTable_乳腺医师签名});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.StylePriority.UseBorders = false;
            this.xrTableCell129.Weight = 0.11161843716525768D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.StylePriority.UseBorders = false;
            this.xrTableCell130.Text = "  乳    腺*";
            this.xrTableCell130.Weight = 0.30993814107294138D;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.StylePriority.UseBorders = false;
            this.xrTableCell224.StylePriority.UseTextAlignment = false;
            this.xrTableCell224.Text = "  1 未见异常 2 乳房切除 3 异常泌乳 4 乳腺包块 5其他";
            this.xrTableCell224.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell224.Weight = 0.979155481309214D;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell225.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_乳腺其他});
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.StylePriority.UseBorders = false;
            this.xrTableCell225.Weight = 0.21488342609713182D;
            // 
            // xrLabel_乳腺其他
            // 
            this.xrLabel_乳腺其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_乳腺其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_乳腺其他.Name = "xrLabel_乳腺其他";
            this.xrLabel_乳腺其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_乳腺其他.SizeF = new System.Drawing.SizeF(77.30957F, 19.66659F);
            this.xrLabel_乳腺其他.StylePriority.UseBorders = false;
            this.xrLabel_乳腺其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_乳腺其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell131.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_乳腺2,
            this.xrLabel38,
            this.xrLabel_乳腺3,
            this.xrLabel36,
            this.xrLabel_乳腺4,
            this.xrLabel13,
            this.xrLabel_乳腺1});
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.StylePriority.UseBorders = false;
            this.xrTableCell131.Weight = 0.384404514355455D;
            // 
            // xrLabel_乳腺2
            // 
            this.xrLabel_乳腺2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_乳腺2.LocationFloat = new DevExpress.Utils.PointFloat(45.91047F, 3.149618F);
            this.xrLabel_乳腺2.Name = "xrLabel_乳腺2";
            this.xrLabel_乳腺2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_乳腺2.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_乳腺2.StylePriority.UseBorders = false;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(62.41953F, 3.149592F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "/";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_乳腺3
            // 
            this.xrLabel_乳腺3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_乳腺3.LocationFloat = new DevExpress.Utils.PointFloat(78.92862F, 3.149618F);
            this.xrLabel_乳腺3.Name = "xrLabel_乳腺3";
            this.xrLabel_乳腺3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_乳腺3.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_乳腺3.StylePriority.UseBorders = false;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(95.43775F, 3.149618F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "/";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_乳腺4
            // 
            this.xrLabel_乳腺4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_乳腺4.LocationFloat = new DevExpress.Utils.PointFloat(111.9469F, 3.149606F);
            this.xrLabel_乳腺4.Name = "xrLabel_乳腺4";
            this.xrLabel_乳腺4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_乳腺4.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_乳腺4.StylePriority.UseBorders = false;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(29.40134F, 3.149618F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "/";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_乳腺1
            // 
            this.xrLabel_乳腺1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_乳腺1.LocationFloat = new DevExpress.Utils.PointFloat(12.89224F, 3.149618F);
            this.xrLabel_乳腺1.Name = "xrLabel_乳腺1";
            this.xrLabel_乳腺1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_乳腺1.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_乳腺1.StylePriority.UseBorders = false;
            // 
            // xrTable_乳腺医师签名
            // 
            this.xrTable_乳腺医师签名.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_乳腺医师签名.Name = "xrTable_乳腺医师签名";
            this.xrTable_乳腺医师签名.StylePriority.UseBorders = false;
            this.xrTable_乳腺医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.xrTableCell226,
            this.xrTableCell126,
            this.xrTableCell97,
            this.xrTableCell98,
            this.xrTableCell127,
            this.xrTableCell128});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.StylePriority.UseBorders = false;
            this.xrTableCell125.Weight = 0.11161843716525768D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Weight = 0.15496907053647069D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.StylePriority.UseBorders = false;
            this.xrTableCell126.Text = "外阴";
            this.xrTableCell126.Weight = 0.15496907053647069D;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.StylePriority.UseTextAlignment = false;
            this.xrTableCell97.Text = "  1 未见异常  2 异常";
            this.xrTableCell97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell97.Weight = 0.56459880215889446D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell98.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_外阴异常});
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.StylePriority.UseBorders = false;
            this.xrTableCell98.Weight = 0.87780924364338087D;
            // 
            // xrLabel_外阴异常
            // 
            this.xrLabel_外阴异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_外阴异常.LocationFloat = new DevExpress.Utils.PointFloat(5.086264E-05F, 0F);
            this.xrLabel_外阴异常.Name = "xrLabel_外阴异常";
            this.xrLabel_外阴异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_外阴异常.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_外阴异常.StylePriority.UseBorders = false;
            this.xrLabel_外阴异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_外阴异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell127.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_外阴});
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.Weight = 0.13603537595952553D;
            // 
            // xrLabel_外阴
            // 
            this.xrLabel_外阴.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_外阴.LocationFloat = new DevExpress.Utils.PointFloat(22.59003F, 3.149606F);
            this.xrLabel_外阴.Name = "xrLabel_外阴";
            this.xrLabel_外阴.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_外阴.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_外阴.StylePriority.UseBorders = false;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.StylePriority.UseBorders = false;
            this.xrTableCell128.Weight = 0.19876670037244218D;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.xrTableCell227,
            this.xrTableCell122,
            this.xrTableCell99,
            this.xrTableCell100,
            this.xrTableCell123,
            this.xrTableCell124});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.StylePriority.UseBorders = false;
            this.xrTableCell121.Weight = 0.11161843716525768D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Weight = 0.15496907053647069D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.StylePriority.UseBorders = false;
            this.xrTableCell122.Text = "阴道";
            this.xrTableCell122.Weight = 0.15496907053647069D;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.StylePriority.UseBorders = false;
            this.xrTableCell99.StylePriority.UseTextAlignment = false;
            this.xrTableCell99.Text = "  1 未见异常  2 异常";
            this.xrTableCell99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell99.Weight = 0.56459880215889446D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell100.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_阴道异常});
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseBorders = false;
            this.xrTableCell100.Weight = 0.87780924364338087D;
            // 
            // xrLabel_阴道异常
            // 
            this.xrLabel_阴道异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_阴道异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_阴道异常.Name = "xrLabel_阴道异常";
            this.xrLabel_阴道异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阴道异常.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_阴道异常.StylePriority.UseBorders = false;
            this.xrLabel_阴道异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_阴道异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell123.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_阴道});
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.StylePriority.UseBorders = false;
            this.xrTableCell123.Weight = 0.13603537595952553D;
            // 
            // xrLabel_阴道
            // 
            this.xrLabel_阴道.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_阴道.LocationFloat = new DevExpress.Utils.PointFloat(22.59043F, 3.149606F);
            this.xrLabel_阴道.Name = "xrLabel_阴道";
            this.xrLabel_阴道.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_阴道.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_阴道.StylePriority.UseBorders = false;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.StylePriority.UseBorders = false;
            this.xrTableCell124.Weight = 0.19876670037244218D;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell228,
            this.xrTableCell110,
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTableCell111,
            this.xrTable_妇科医师签名});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.StylePriority.UseBorders = false;
            this.xrTableCell109.Weight = 0.11161843716525768D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Text = "妇科*";
            this.xrTableCell228.Weight = 0.15496907053647069D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.StylePriority.UseBorders = false;
            this.xrTableCell110.StylePriority.UseTextAlignment = false;
            this.xrTableCell110.Text = "宫颈";
            this.xrTableCell110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell110.Weight = 0.15496907053647069D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.StylePriority.UseBorders = false;
            this.xrTableCell231.StylePriority.UseTextAlignment = false;
            this.xrTableCell231.Text = "  1 未见异常  2 异常";
            this.xrTableCell231.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell231.Weight = 0.56459880215889446D;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell232.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫颈异常});
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.StylePriority.UseBorders = false;
            this.xrTableCell232.Weight = 0.87780924364338087D;
            // 
            // xrLabel_宫颈异常
            // 
            this.xrLabel_宫颈异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_宫颈异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_宫颈异常.Name = "xrLabel_宫颈异常";
            this.xrLabel_宫颈异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_宫颈异常.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_宫颈异常.StylePriority.UseBorders = false;
            this.xrLabel_宫颈异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_宫颈异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell111.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫颈});
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseBorders = false;
            this.xrTableCell111.Weight = 0.13603537595952553D;
            // 
            // xrLabel_宫颈
            // 
            this.xrLabel_宫颈.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_宫颈.LocationFloat = new DevExpress.Utils.PointFloat(22.58993F, 3.149606F);
            this.xrLabel_宫颈.Name = "xrLabel_宫颈";
            this.xrLabel_宫颈.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_宫颈.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_宫颈.StylePriority.UseBorders = false;
            // 
            // xrTable_妇科医师签名
            // 
            this.xrTable_妇科医师签名.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_妇科医师签名.Name = "xrTable_妇科医师签名";
            this.xrTable_妇科医师签名.StylePriority.UseBorders = false;
            this.xrTable_妇科医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell229,
            this.xrTableCell106,
            this.xrTableCell233,
            this.xrTableCell234,
            this.xrTableCell107,
            this.xrTableCell108});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseBorders = false;
            this.xrTableCell105.Weight = 0.11161843716525768D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.Weight = 0.15496907053647069D;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.StylePriority.UseBorders = false;
            this.xrTableCell106.Text = "宫体";
            this.xrTableCell106.Weight = 0.15496907053647069D;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.StylePriority.UseBorders = false;
            this.xrTableCell233.StylePriority.UseTextAlignment = false;
            this.xrTableCell233.Text = "  1 未见异常  2 异常";
            this.xrTableCell233.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell233.Weight = 0.56459880215889446D;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell234.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫体异常});
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.StylePriority.UseBorders = false;
            this.xrTableCell234.Weight = 0.87780924364338087D;
            // 
            // xrLabel_宫体异常
            // 
            this.xrLabel_宫体异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_宫体异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_宫体异常.Name = "xrLabel_宫体异常";
            this.xrLabel_宫体异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_宫体异常.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_宫体异常.StylePriority.UseBorders = false;
            this.xrLabel_宫体异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_宫体异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell107.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_宫体});
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.StylePriority.UseBorders = false;
            this.xrTableCell107.Weight = 0.13603537595952553D;
            // 
            // xrLabel_宫体
            // 
            this.xrLabel_宫体.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_宫体.LocationFloat = new DevExpress.Utils.PointFloat(22.58993F, 3.149606F);
            this.xrLabel_宫体.Name = "xrLabel_宫体";
            this.xrLabel_宫体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_宫体.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_宫体.StylePriority.UseBorders = false;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.StylePriority.UseBorders = false;
            this.xrTableCell108.Weight = 0.19876670037244218D;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell47,
            this.xrTableCell230,
            this.xrTableCell48,
            this.xrTableCell235,
            this.xrTableCell236,
            this.xrTableCell49,
            this.xrTableCell50});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseBorders = false;
            this.xrTableCell47.Weight = 0.11161843716525768D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.StylePriority.UseBorders = false;
            this.xrTableCell230.Weight = 0.15496907053647069D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseBorders = false;
            this.xrTableCell48.Text = "附件";
            this.xrTableCell48.Weight = 0.15496907053647069D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.StylePriority.UseBorders = false;
            this.xrTableCell235.StylePriority.UseTextAlignment = false;
            this.xrTableCell235.Text = "  1 未见异常  2 异常";
            this.xrTableCell235.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell235.Weight = 0.56459880215889446D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell236.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_附件异常});
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.StylePriority.UseBorders = false;
            this.xrTableCell236.Weight = 0.87780924364338087D;
            // 
            // xrLabel_附件异常
            // 
            this.xrLabel_附件异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_附件异常.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_附件异常.Name = "xrLabel_附件异常";
            this.xrLabel_附件异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_附件异常.SizeF = new System.Drawing.SizeF(157.4803F, 19.66659F);
            this.xrLabel_附件异常.StylePriority.UseBorders = false;
            this.xrLabel_附件异常.StylePriority.UseTextAlignment = false;
            this.xrLabel_附件异常.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell49.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_附件});
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseBorders = false;
            this.xrTableCell49.Weight = 0.13603537595952553D;
            // 
            // xrLabel_附件
            // 
            this.xrLabel_附件.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_附件.LocationFloat = new DevExpress.Utils.PointFloat(22.59043F, 3.149606F);
            this.xrLabel_附件.Name = "xrLabel_附件";
            this.xrLabel_附件.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_附件.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_附件.StylePriority.UseBorders = false;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.Weight = 0.19876670037244218D;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell169,
            this.xrTableCell170,
            this.xrTable_查体其他,
            this.xrTable_查体其他医师签名});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.StylePriority.UseBorders = false;
            this.xrTableCell169.Weight = 0.11161843716525768D;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.StylePriority.UseBorders = false;
            this.xrTableCell170.Text = "  其    他*";
            this.xrTableCell170.Weight = 0.30993814107294138D;
            // 
            // xrTable_查体其他
            // 
            this.xrTable_查体其他.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_查体其他.Name = "xrTable_查体其他";
            this.xrTable_查体其他.StylePriority.UseBorders = false;
            this.xrTable_查体其他.StylePriority.UseTextAlignment = false;
            this.xrTable_查体其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTable_查体其他.Weight = 1.5784434217618009D;
            // 
            // xrTable_查体其他医师签名
            // 
            this.xrTable_查体其他医师签名.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_查体其他医师签名.Name = "xrTable_查体其他医师签名";
            this.xrTable_查体其他医师签名.StylePriority.UseBorders = false;
            this.xrTable_查体其他医师签名.Weight = 0.19876670037244218D;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell189,
            this.xrTableCell190,
            this.xrTableCell75,
            this.xrTableCell73,
            this.xrTableCell74,
            this.xrTableCell191,
            this.xrTableCell192});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1D;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.StylePriority.UseBorders = false;
            this.xrTableCell189.Weight = 0.11161843716525768D;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.StylePriority.UseBorders = false;
            this.xrTableCell190.Text = " 血    型 ";
            this.xrTableCell190.Weight = 0.30993814107294138D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.Text = "ABO";
            this.xrTableCell75.Weight = 0.11160357342917462D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell73.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_ABO});
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.Weight = 0.45299516193904754D;
            // 
            // xrLabel_ABO
            // 
            this.xrLabel_ABO.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_ABO.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_ABO.Name = "xrLabel_ABO";
            this.xrLabel_ABO.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_ABO.SizeF = new System.Drawing.SizeF(78.74016F, 19.66659F);
            this.xrLabel_ABO.StylePriority.UseBorders = false;
            this.xrLabel_ABO.StylePriority.UseTextAlignment = false;
            this.xrLabel_ABO.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.Text = "Rh*";
            this.xrTableCell74.Weight = 0.089433653715924488D;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell191.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_Rh});
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.StylePriority.UseBorders = false;
            this.xrTableCell191.Weight = 0.92441103267765423D;
            // 
            // xrLabel_Rh
            // 
            this.xrLabel_Rh.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_Rh.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_Rh.Name = "xrLabel_Rh";
            this.xrLabel_Rh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_Rh.SizeF = new System.Drawing.SizeF(78.74016F, 19.66659F);
            this.xrLabel_Rh.StylePriority.UseBorders = false;
            this.xrLabel_Rh.StylePriority.UseTextAlignment = false;
            this.xrLabel_Rh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.StylePriority.UseBorders = false;
            this.xrTableCell192.Weight = 0.19876670037244218D;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell181,
            this.xrTableCell182,
            this.xrTableCell82,
            this.xrTableCell161,
            this.xrTableCell76,
            this.xrTableCell84,
            this.xrTableCell81,
            this.xrTableCell83,
            this.xrTableCell183,
            this.xrTableCell184});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.StylePriority.UseBorders = false;
            this.xrTableCell181.Weight = 0.11161843716525768D;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.StylePriority.UseBorders = false;
            this.xrTableCell182.Text = "  血 常 规*";
            this.xrTableCell182.Weight = 0.30993814107294138D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseTextAlignment = false;
            this.xrTableCell82.Text = "  血红蛋白";
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrTableCell82.Weight = 0.18383078109952405D;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血红蛋白});
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Weight = 0.20244520372559244D;
            // 
            // xrLabel_血红蛋白
            // 
            this.xrLabel_血红蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血红蛋白.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.6876067F);
            this.xrLabel_血红蛋白.Name = "xrLabel_血红蛋白";
            this.xrLabel_血红蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血红蛋白.SizeF = new System.Drawing.SizeF(72.83465F, 19.66659F);
            this.xrLabel_血红蛋白.StylePriority.UseBorders = false;
            this.xrLabel_血红蛋白.StylePriority.UseTextAlignment = false;
            this.xrLabel_血红蛋白.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.Text = "g/L    白细胞";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell76.Weight = 0.25238846330730713D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_白细胞});
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Weight = 0.20244520372559247D;
            // 
            // xrLabel_白细胞
            // 
            this.xrLabel_白细胞.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_白细胞.LocationFloat = new DevExpress.Utils.PointFloat(6.007398E-06F, 0.6876067F);
            this.xrLabel_白细胞.Name = "xrLabel_白细胞";
            this.xrLabel_白细胞.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_白细胞.SizeF = new System.Drawing.SizeF(72.83464F, 19.66659F);
            this.xrLabel_白细胞.StylePriority.UseBorders = false;
            this.xrLabel_白细胞.StylePriority.UseTextAlignment = false;
            this.xrLabel_白细胞.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Text = "×10^9/L    血小板";
            this.xrTableCell81.Weight = 0.33693406670398385D;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血小板});
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.20244520372559244D;
            // 
            // xrLabel_血小板
            // 
            this.xrLabel_血小板.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血小板.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0.6876067F);
            this.xrLabel_血小板.Name = "xrLabel_血小板";
            this.xrLabel_血小板.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血小板.SizeF = new System.Drawing.SizeF(72.83465F, 19.66659F);
            this.xrLabel_血小板.StylePriority.UseBorders = false;
            this.xrLabel_血小板.StylePriority.UseTextAlignment = false;
            this.xrLabel_血小板.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.StylePriority.UseBorders = false;
            this.xrTableCell183.Text = "×10^9/L";
            this.xrTableCell183.Weight = 0.19795449947420848D;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell184.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox辅助检查});
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.StylePriority.UseBorders = false;
            this.xrTableCell184.Weight = 0.19876670037244218D;
            // 
            // xrPictureBox辅助检查
            // 
            this.xrPictureBox辅助检查.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox辅助检查.LocationFloat = new DevExpress.Utils.PointFloat(0.0001220703F, 0F);
            this.xrPictureBox辅助检查.Name = "xrPictureBox辅助检查";
            this.xrPictureBox辅助检查.SizeF = new System.Drawing.SizeF(68F, 25F);
            this.xrPictureBox辅助检查.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox辅助检查.StylePriority.UseBorders = false;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell185,
            this.xrTableCell186,
            this.xrTableCell162,
            this.xrTableCell187,
            this.xrTableCell188});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.StylePriority.UseBorders = false;
            this.xrTableCell185.Weight = 0.11161843716525768D;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.StylePriority.UseBorders = false;
            this.xrTableCell186.Weight = 0.30993814107294138D;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.StylePriority.UseBorders = false;
            this.xrTableCell162.Text = "其他";
            this.xrTableCell162.Weight = 0.11160362353134568D;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell187.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_血常规其他});
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.StylePriority.UseBorders = false;
            this.xrTableCell187.Weight = 1.4668397982304553D;
            // 
            // xrLabel_血常规其他
            // 
            this.xrLabel_血常规其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_血常规其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_血常规其他.Name = "xrLabel_血常规其他";
            this.xrLabel_血常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_血常规其他.SizeF = new System.Drawing.SizeF(314.9606F, 19.66659F);
            this.xrLabel_血常规其他.StylePriority.UseBorders = false;
            this.xrLabel_血常规其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_血常规其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.StylePriority.UseBorders = false;
            this.xrTableCell188.Weight = 0.19876670037244218D;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell241,
            this.xrTableCell164,
            this.xrTableCell240,
            this.xrTableCell163,
            this.xrTableCell239,
            this.xrTableCell237,
            this.xrTableCell238,
            this.xrTableCell179,
            this.xrTableCell180});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.StylePriority.UseBorders = false;
            this.xrTableCell177.Weight = 0.11161843716525768D;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.StylePriority.UseBorders = false;
            this.xrTableCell178.Text = "  尿 常 规*";
            this.xrTableCell178.Weight = 0.30993814107294138D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.StylePriority.UseBorders = false;
            this.xrTableCell241.Text = "尿蛋白";
            this.xrTableCell241.Weight = 0.13131580094146936D;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿蛋白});
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.StylePriority.UseBorders = false;
            this.xrTableCell164.Weight = 0.26329505449898088D;
            // 
            // xrLabel_尿蛋白
            // 
            this.xrLabel_尿蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿蛋白.LocationFloat = new DevExpress.Utils.PointFloat(6.382859E-05F, 0F);
            this.xrLabel_尿蛋白.Name = "xrLabel_尿蛋白";
            this.xrLabel_尿蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_尿蛋白.SizeF = new System.Drawing.SizeF(94.72681F, 19.66659F);
            this.xrLabel_尿蛋白.StylePriority.UseBorders = false;
            this.xrLabel_尿蛋白.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿蛋白.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.StylePriority.UseBorders = false;
            this.xrTableCell240.Text = "尿糖";
            this.xrTableCell240.Weight = 0.10395834154328126D;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿糖});
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.StylePriority.UseBorders = false;
            this.xrTableCell163.Weight = 0.26329914542766286D;
            // 
            // xrLabel_尿糖
            // 
            this.xrLabel_尿糖.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿糖.LocationFloat = new DevExpress.Utils.PointFloat(0.001495842F, 0F);
            this.xrLabel_尿糖.Name = "xrLabel_尿糖";
            this.xrLabel_尿糖.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_尿糖.SizeF = new System.Drawing.SizeF(94.72681F, 19.66659F);
            this.xrLabel_尿糖.StylePriority.UseBorders = false;
            this.xrLabel_尿糖.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿糖.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.StylePriority.UseBorders = false;
            this.xrTableCell239.Text = "尿酮体";
            this.xrTableCell239.Weight = 0.1313158092903034D;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿酮体});
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.StylePriority.UseBorders = false;
            this.xrTableCell237.Weight = 0.26329912038116071D;
            // 
            // xrLabel_尿酮体
            // 
            this.xrLabel_尿酮体.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿酮体.LocationFloat = new DevExpress.Utils.PointFloat(6.007398E-06F, 0F);
            this.xrLabel_尿酮体.Name = "xrLabel_尿酮体";
            this.xrLabel_尿酮体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_尿酮体.SizeF = new System.Drawing.SizeF(94.72681F, 19.66659F);
            this.xrLabel_尿酮体.StylePriority.UseBorders = false;
            this.xrLabel_尿酮体.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿酮体.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseBorders = false;
            this.xrTableCell238.Text = "尿潜血";
            this.xrTableCell238.Weight = 0.1313158092903034D;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell179.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿潜血});
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.StylePriority.UseBorders = false;
            this.xrTableCell179.Weight = 0.29064434038863907D;
            // 
            // xrLabel_尿潜血
            // 
            this.xrLabel_尿潜血.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿潜血.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_尿潜血.Name = "xrLabel_尿潜血";
            this.xrLabel_尿潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_尿潜血.SizeF = new System.Drawing.SizeF(94.72681F, 19.66659F);
            this.xrLabel_尿潜血.StylePriority.UseBorders = false;
            this.xrLabel_尿潜血.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿潜血.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.StylePriority.UseBorders = false;
            this.xrTableCell180.Weight = 0.19876670037244218D;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell173,
            this.xrTableCell174,
            this.xrTableCell242,
            this.xrTableCell175,
            this.xrTableCell176});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.StylePriority.UseBorders = false;
            this.xrTableCell173.Weight = 0.11161843716525768D;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.StylePriority.UseBorders = false;
            this.xrTableCell174.Weight = 0.30993814107294138D;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.StylePriority.UseBorders = false;
            this.xrTableCell242.Text = "其他";
            this.xrTableCell242.Weight = 0.11160362353134568D;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell175.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿常规其他});
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseBorders = false;
            this.xrTableCell175.Weight = 1.4668397982304553D;
            // 
            // xrLabel_尿常规其他
            // 
            this.xrLabel_尿常规其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿常规其他.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_尿常规其他.Name = "xrLabel_尿常规其他";
            this.xrLabel_尿常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_尿常规其他.SizeF = new System.Drawing.SizeF(314.9606F, 19.66659F);
            this.xrLabel_尿常规其他.StylePriority.UseBorders = false;
            this.xrLabel_尿常规其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿常规其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.StylePriority.UseBorders = false;
            this.xrTableCell176.Weight = 0.19876670037244218D;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell157,
            this.xrTableCell158,
            this.xrTableCell244,
            this.xrTableCell243,
            this.xrTableCell245,
            this.xrTableCell159,
            this.xrTableCell160});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.StylePriority.UseBorders = false;
            this.xrTableCell157.Weight = 0.11161843716525768D;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.StylePriority.UseBorders = false;
            this.xrTableCell158.Text = "  空腹血糖*";
            this.xrTableCell158.Weight = 0.30993814107294138D;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell244.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_空腹血糖1});
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.StylePriority.UseBorders = false;
            this.xrTableCell244.Weight = 0.39394742369190966D;
            // 
            // xrLabel_空腹血糖1
            // 
            this.xrLabel_空腹血糖1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_空腹血糖1.LocationFloat = new DevExpress.Utils.PointFloat(11.81104F, 0F);
            this.xrLabel_空腹血糖1.Name = "xrLabel_空腹血糖1";
            this.xrLabel_空腹血糖1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_空腹血糖1.SizeF = new System.Drawing.SizeF(129.9213F, 19.66659F);
            this.xrLabel_空腹血糖1.StylePriority.UseBorders = false;
            this.xrLabel_空腹血糖1.StylePriority.UseTextAlignment = false;
            this.xrLabel_空腹血糖1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.StylePriority.UseBorders = false;
            this.xrTableCell243.StylePriority.UseTextAlignment = false;
            this.xrTableCell243.Text = "mmol/L    或";
            this.xrTableCell243.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell243.Weight = 0.2261868707662939D;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell245.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_空腹血糖2});
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.StylePriority.UseBorders = false;
            this.xrTableCell245.Weight = 0.36151375631077176D;
            // 
            // xrLabel_空腹血糖2
            // 
            this.xrLabel_空腹血糖2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_空腹血糖2.LocationFloat = new DevExpress.Utils.PointFloat(0.1422628F, 0F);
            this.xrLabel_空腹血糖2.Name = "xrLabel_空腹血糖2";
            this.xrLabel_空腹血糖2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_空腹血糖2.SizeF = new System.Drawing.SizeF(129.9213F, 19.66659F);
            this.xrLabel_空腹血糖2.StylePriority.UseBorders = false;
            this.xrLabel_空腹血糖2.StylePriority.UseTextAlignment = false;
            this.xrLabel_空腹血糖2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.StylePriority.UseBorders = false;
            this.xrTableCell159.StylePriority.UseTextAlignment = false;
            this.xrTableCell159.Text = "  mg/dL";
            this.xrTableCell159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell159.Weight = 0.59679537099282554D;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseBorders = false;
            this.xrTableCell160.Weight = 0.19876670037244218D;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell165,
            this.xrTableCell166,
            this.xrTableCell246,
            this.xrTableCell167,
            this.xrTableCell168});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.StylePriority.UseBorders = false;
            this.xrTableCell165.Weight = 0.11161843716525768D;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.Text = "同型半胱氨酸*";
            this.xrTableCell166.Weight = 0.30993814107294138D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell246.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_同型半胱氨酸});
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.StylePriority.UseBorders = false;
            this.xrTableCell246.Weight = 0.67804095581226054D;
            // 
            // xrLabel_同型半胱氨酸
            // 
            this.xrLabel_同型半胱氨酸.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_同型半胱氨酸.LocationFloat = new DevExpress.Utils.PointFloat(11.81105F, 0F);
            this.xrLabel_同型半胱氨酸.Name = "xrLabel_同型半胱氨酸";
            this.xrLabel_同型半胱氨酸.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_同型半胱氨酸.SizeF = new System.Drawing.SizeF(232.1309F, 19.66659F);
            this.xrLabel_同型半胱氨酸.StylePriority.UseBorders = false;
            this.xrLabel_同型半胱氨酸.StylePriority.UseTextAlignment = false;
            this.xrLabel_同型半胱氨酸.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.StylePriority.UseBorders = false;
            this.xrTableCell167.StylePriority.UseTextAlignment = false;
            this.xrTableCell167.Text = "  umol/L";
            this.xrTableCell167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell167.Weight = 0.90040246594954032D;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.StylePriority.UseBorders = false;
            this.xrTableCell168.Weight = 0.19876670037244218D;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell247,
            this.xrTableCell95,
            this.xrTableCell96});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.StylePriority.UseBorders = false;
            this.xrTableCell93.Weight = 0.11161843716525768D;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.StylePriority.UseBorders = false;
            this.xrTableCell94.Text = "尿微量白蛋白*";
            this.xrTableCell94.Weight = 0.30993814107294138D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell247.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_尿微量白蛋白});
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.StylePriority.UseBorders = false;
            this.xrTableCell247.Weight = 0.39394743204991062D;
            // 
            // xrLabel_尿微量白蛋白
            // 
            this.xrLabel_尿微量白蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_尿微量白蛋白.LocationFloat = new DevExpress.Utils.PointFloat(11.81105F, 0F);
            this.xrLabel_尿微量白蛋白.Name = "xrLabel_尿微量白蛋白";
            this.xrLabel_尿微量白蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_尿微量白蛋白.SizeF = new System.Drawing.SizeF(129.9213F, 19.66659F);
            this.xrLabel_尿微量白蛋白.StylePriority.UseBorders = false;
            this.xrLabel_尿微量白蛋白.StylePriority.UseTextAlignment = false;
            this.xrLabel_尿微量白蛋白.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.StylePriority.UseBorders = false;
            this.xrTableCell95.StylePriority.UseTextAlignment = false;
            this.xrTableCell95.Text = "mg/dL";
            this.xrTableCell95.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell95.Weight = 1.1844959897118903D;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.StylePriority.UseBorders = false;
            this.xrTableCell96.Weight = 0.19876670037244218D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 13F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report健康体检表A4_2_2017
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(11, 15, 0, 13);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_口唇;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_缺齿1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_缺齿2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_龋齿1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_龋齿2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_义齿1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_义齿2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_缺齿3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_缺齿4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_龋齿3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_龋齿4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_义齿3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_义齿4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_齿列1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_齿列2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_咽部;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_脏器功能医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_视力左眼;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_视力右眼;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_矫正视力左眼;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_矫正视力右眼;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_听力;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_运动能力;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼底异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_眼底;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_眼底医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_皮肤其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_皮肤;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_巩膜其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_巩膜;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_淋巴结其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_淋巴结;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_桶状胸;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_呼吸音异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_呼吸音;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_罗音其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_罗音;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell207;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心率;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell206;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_心律;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell209;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_杂音有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_杂音;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_查体医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_压痛有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_压痛;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_包块有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_包块;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肝大有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肝大;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell216;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脾大有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_脾大;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_移动性浊音有;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_移动性浊音;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_下肢水肿;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_足背动脉搏动;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肛门指诊其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_肛门指诊;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_肛门指诊医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乳腺其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乳腺2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乳腺3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乳腺4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_乳腺1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_乳腺医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_外阴异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_外阴;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴道异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_阴道;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫颈异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫颈;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_妇科医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫体异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_宫体;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_附件异常;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_附件;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_查体其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_查体其他医师签名;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_ABO;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_Rh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血红蛋白;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_白细胞;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血小板;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_血常规其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿蛋白;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿糖;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿酮体;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿潜血;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿常规其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_空腹血糖1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_空腹血糖2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_同型半胱氨酸;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_尿微量白蛋白;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox脏器功能;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox一般体格检查;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox辅助检查;
    }
}
