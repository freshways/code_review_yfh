﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Business;
using AtomEHR.Common;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class InterFace化验结果 : frmBase
    {
        DataTable dt结果 = null;
        public string s化验日期 = "";
        public InterFace化验结果()
        {
            InitializeComponent();
        }

        public InterFace化验结果(DataTable dt)
        {
            InitializeComponent();
            dt结果 = dt;//bll获取化验信息.Get化验信息(s身份证号);
        }

        private void InterFace化验结果_Load(object sender, EventArgs e)
        {
            if (dt结果 != null && dt结果.Rows.Count > 0)
                gc化验结果.DataSource = dt结果;
        }

        private void btn确认_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv化验结果.GetFocusedDataRow();
            if (row == null) return;
            s化验日期 = row["化验日期"].ToString();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
