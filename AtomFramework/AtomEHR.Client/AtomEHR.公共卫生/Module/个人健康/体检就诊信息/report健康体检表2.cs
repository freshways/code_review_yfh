﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class report健康体检表2 : DevExpress.XtraReports.UI.XtraReport
    {
        DataSet ds体检;
        bll健康体检 bll = new bll健康体检();

        public report健康体检表2()
        {
            InitializeComponent();
        }
        public report健康体检表2(string docNo, string date)
        {
            InitializeComponent();
            ds体检 = bll.GetReportDataByKey(docNo, date);
            BindData(ds体检);
        }

        private void BindData(DataSet ds体检)
        {
            DataTable tb健康档案 = ds体检.Tables[tb_健康档案.__TableName];
            DataTable tb健康体检 = ds体检.Tables[tb_健康体检.__TableName];
            //DataTable tb住院史 = ds体检.Tables["住院史"];
            //DataTable tb家庭病床史 = ds体检.Tables["家庭病床史"];
            //DataTable tb用药情况表 = ds体检.Tables[tb_健康体检_用药情况表.__TableName];
            //DataTable tb接种史 = ds体检.Tables[tb_健康体检_非免疫规划预防接种史.__TableName];

            if (tb健康体检.Rows.Count == 1 && tb健康档案.Rows.Count == 1)
            {
                this.xrLabel口唇.Text = tb健康体检.Rows[0][tb_健康体检.口唇].ToString();
                this.xrLabel齿列有无.Text = tb健康体检.Rows[0][tb_健康体检.齿列].ToString() == "1" ? "1" : "";

                if (tb健康体检.Rows[0][tb_健康体检.齿列缺齿] != null && !string.IsNullOrEmpty(tb健康体检.Rows[0][tb_健康体检.齿列缺齿].ToString()))
                {
                    string[] quchi = tb健康体检.Rows[0][tb_健康体检.齿列缺齿].ToString().Split('@');

                    this.xrTableCell缺齿1.Text = quchi[0];
                    this.xrTableCell缺齿2.Text = quchi[1];
                    this.xrTableCell缺齿3.Text = quchi[2];
                    this.xrTableCell缺齿4.Text = quchi[3];
                }
                if (tb健康体检.Rows[0][tb_健康体检.齿列义齿] != null && !string.IsNullOrEmpty(tb健康体检.Rows[0][tb_健康体检.齿列义齿].ToString()))
                {
                    string[] quchi = tb健康体检.Rows[0][tb_健康体检.齿列义齿].ToString().Split('@');

                    this.xrTableCell义齿1.Text = quchi[0];
                    this.xrTableCell义齿2.Text = quchi[1];
                    this.xrTableCell义齿3.Text = quchi[2];
                    this.xrTableCell义齿4.Text = quchi[3];
                }
                if (tb健康体检.Rows[0][tb_健康体检.齿列龋齿] != null && !string.IsNullOrEmpty(tb健康体检.Rows[0][tb_健康体检.齿列龋齿].ToString()))
                {
                    string[] quchi = tb健康体检.Rows[0][tb_健康体检.齿列龋齿].ToString().Split('@');

                    this.xrTableCell龋齿1.Text = quchi[0];
                    this.xrTableCell龋齿2.Text = quchi[1];
                    this.xrTableCell龋齿3.Text = quchi[2];
                    this.xrTableCell龋齿4.Text = quchi[3];
                }


                this.xrLabel咽部.Text = tb健康体检.Rows[0][tb_健康体检.咽部].ToString();
                this.xrLabel左眼.Text = tb健康体检.Rows[0][tb_健康体检.左眼视力].ToString();
                this.xrLabel左眼矫正.Text = tb健康体检.Rows[0][tb_健康体检.左眼矫正].ToString();
                this.xrLabel右眼.Text = tb健康体检.Rows[0][tb_健康体检.右眼视力].ToString();
                this.xrLabel右眼矫正.Text = tb健康体检.Rows[0][tb_健康体检.右眼矫正].ToString();
                this.xrLabel听力.Text = tb健康体检.Rows[0][tb_健康体检.听力].ToString();
                this.xrLabel运动功能.Text = tb健康体检.Rows[0][tb_健康体检.运动功能].ToString();

                #region  眼底
                this.xrLabel眼底.Text = tb健康体检.Rows[0][tb_健康体检.眼底].ToString();
                this.xrLabel眼底异常.Text = tb健康体检.Rows[0][tb_健康体检.眼底异常].ToString();
                this.xrLabel皮肤.Text = tb健康体检.Rows[0][tb_健康体检.皮肤].ToString();
                this.xrLabel皮肤其他.Text = tb健康体检.Rows[0][tb_健康体检.皮肤其他].ToString();
                this.xrLabel巩膜.Text = tb健康体检.Rows[0][tb_健康体检.巩膜].ToString();
                this.xrLabel巩膜其他.Text = tb健康体检.Rows[0][tb_健康体检.巩膜其他].ToString();
                this.xrLabel淋巴结.Text = tb健康体检.Rows[0][tb_健康体检.淋巴结].ToString();
                this.xrLabel淋巴结其他.Text = tb健康体检.Rows[0][tb_健康体检.淋巴结其他].ToString();
                this.xrLabel桶状胸.Text = tb健康体检.Rows[0][tb_健康体检.桶状胸].ToString() == "2" ? "1" : "2";
                this.xrLabel呼吸音.Text = tb健康体检.Rows[0][tb_健康体检.呼吸音].ToString();
                this.xrLabel呼吸音异常.Text = tb健康体检.Rows[0][tb_健康体检.呼吸音异常].ToString();
                this.xrLabel罗音.Text = tb健康体检.Rows[0][tb_健康体检.罗音].ToString();
                this.xrLabel罗音其他.Text = tb健康体检.Rows[0][tb_健康体检.罗音异常].ToString();
                this.xrLabel心率.Text = tb健康体检.Rows[0][tb_健康体检.心率].ToString();
                this.xrLabel心律.Text = tb健康体检.Rows[0][tb_健康体检.心律].ToString();
                this.xrLabel杂音.Text = tb健康体检.Rows[0][tb_健康体检.杂音].ToString();
                this.xrLabel杂音有.Text = tb健康体检.Rows[0][tb_健康体检.杂音有].ToString();
                this.xrLabel压痛.Text = tb健康体检.Rows[0][tb_健康体检.压痛].ToString();
                this.xrLabel压痛有.Text = tb健康体检.Rows[0][tb_健康体检.压痛有].ToString();

                this.xrLabel包块.Text = tb健康体检.Rows[0][tb_健康体检.包块].ToString();
                this.xrLabel包块有.Text = tb健康体检.Rows[0][tb_健康体检.包块有].ToString();
                this.xrLabel肝大.Text = tb健康体检.Rows[0][tb_健康体检.肝大].ToString();
                this.xrLabel肝大有.Text = tb健康体检.Rows[0][tb_健康体检.肝大有].ToString();

                this.xrLabel脾大.Text = tb健康体检.Rows[0][tb_健康体检.脾大].ToString();
                this.xrLabel脾大有.Text = tb健康体检.Rows[0][tb_健康体检.脾大有].ToString();
                this.xrLabel移动性浊音.Text = tb健康体检.Rows[0][tb_健康体检.浊音].ToString();
                this.xrLabel移动性浊音有.Text = tb健康体检.Rows[0][tb_健康体检.浊音有].ToString();


                this.xrLabel下肢水肿.Text = tb健康体检.Rows[0][tb_健康体检.下肢水肿].ToString();
                this.xrLabel足背动脉搏动.Text = tb健康体检.Rows[0][tb_健康体检.足背动脉搏动].ToString();
                this.xrLabel肛门指诊.Text = tb健康体检.Rows[0][tb_健康体检.肛门指诊].ToString();
                this.xrLabel肛门指诊其他.Text = tb健康体检.Rows[0][tb_健康体检.肛门指诊异常].ToString();

                this.xrLabel乳腺.Text = tb健康体检.Rows[0][tb_健康体检.乳腺].ToString();
                this.xrLabel乳腺其他.Text = tb健康体检.Rows[0][tb_健康体检.乳腺其他].ToString();

                #region 乳腺（未用）
                //string ruxian = tb健康体检.Rows[0][tb_健康体检.乳腺].ToString();
                //string[] zzs = ruxian.Split(',');
                //for (int i = 0; i < zzs.Length; i++)
                //{
                //    if (string.IsNullOrEmpty(zzs[i]))
                //    {
                //        continue;
                //    }
                //    string ctrName = "xrLabel乳腺" + (i + 1);
                //    XRLabel lbl = (XRLabel)FindControl(ctrName, false);
                //    lbl.Text = zzs[i];
                //}
                #endregion

                this.xrLabel外阴.Text = tb健康体检.Rows[0][tb_健康体检.外阴].ToString();
                this.xrLabel外阴异常.Text = tb健康体检.Rows[0][tb_健康体检.外阴异常].ToString();

                this.xrLabel阴道.Text = tb健康体检.Rows[0][tb_健康体检.阴道].ToString();
                this.xrLabel阴道异常.Text = tb健康体检.Rows[0][tb_健康体检.阴道异常].ToString();
                this.xrLabel宫颈.Text = tb健康体检.Rows[0][tb_健康体检.宫颈].ToString();
                this.xrLabel宫颈异常.Text = tb健康体检.Rows[0][tb_健康体检.宫颈异常].ToString();
                this.xrLabel宫体.Text = tb健康体检.Rows[0][tb_健康体检.宫体].ToString();
                this.xrLabel宫体异常.Text = tb健康体检.Rows[0][tb_健康体检.宫体异常].ToString();
                this.xrLabel附件.Text = tb健康体检.Rows[0][tb_健康体检.附件].ToString();
                this.xrLabel附件异常.Text = tb健康体检.Rows[0][tb_健康体检.附件异常].ToString();
                this.xrLabel查体其他.Text = tb健康体检.Rows[0][tb_健康体检.查体其他].ToString();

                #endregion

                #region 辅助检查
                this.xrLabel血红蛋白.Text = tb健康体检.Rows[0][tb_健康体检.血红蛋白].ToString();
                this.xrLabel白细胞.Text = tb健康体检.Rows[0][tb_健康体检.白细胞].ToString();
                this.xrLabel血小板.Text = tb健康体检.Rows[0][tb_健康体检.血小板].ToString();
                this.xrLabel血常规其他.Text = tb健康体检.Rows[0][tb_健康体检.血常规其他].ToString();

                this.xrLabel尿蛋白.Text = tb健康体检.Rows[0][tb_健康体检.尿蛋白].ToString();
                this.xrLabel尿糖.Text = tb健康体检.Rows[0][tb_健康体检.尿糖].ToString();
                this.xrLabel尿酮体.Text = tb健康体检.Rows[0][tb_健康体检.尿酮体].ToString();
                this.xrLabel尿潜血.Text = tb健康体检.Rows[0][tb_健康体检.尿潜血].ToString();
                this.xrLabel尿常规其他.Text = tb健康体检.Rows[0][tb_健康体检.尿常规其他].ToString();

                this.xrLabel空腹血糖1.Text = tb健康体检.Rows[0][tb_健康体检.空腹血糖].ToString();
                this.xrLabel空腹血糖2.Text = tb健康体检.Rows[0][tb_健康体检.餐后2H血糖].ToString();

                this.xrLabel心电图.Text = tb健康体检.Rows[0][tb_健康体检.心电图].ToString();
                this.xrLabel心电图异常.Text = tb健康体检.Rows[0][tb_健康体检.心电图异常].ToString();
                this.xrLabel尿微量白蛋白.Text = tb健康体检.Rows[0][tb_健康体检.尿微量白蛋白].ToString();
                this.xrLabel大便潜血.Text = tb健康体检.Rows[0][tb_健康体检.大便潜血].ToString();
                this.xrLabel糖化血红蛋白.Text = tb健康体检.Rows[0][tb_健康体检.糖化血红蛋白].ToString();
                this.xrLabel乙肝.Text = tb健康体检.Rows[0][tb_健康体检.乙型肝炎表面抗原].ToString();

                this.xrLabel血清谷丙转氨酶.Text = tb健康体检.Rows[0][tb_健康体检.血清谷丙转氨酶].ToString();
                this.xrLabel血清谷草转氨酶.Text = tb健康体检.Rows[0][tb_健康体检.血清谷草转氨酶].ToString();
                this.xrLabel白蛋白.Text = tb健康体检.Rows[0][tb_健康体检.白蛋白].ToString();
                this.xrLabel总红胆素.Text = tb健康体检.Rows[0][tb_健康体检.总胆红素].ToString();
                this.xrLabel结合胆红素.Text = tb健康体检.Rows[0][tb_健康体检.结合胆红素].ToString();
                this.xrLabel血清肌酐.Text = tb健康体检.Rows[0][tb_健康体检.血清肌酐].ToString();
                this.xrLabel血尿素氮.Text = tb健康体检.Rows[0][tb_健康体检.血尿素氮].ToString();
                this.xrLabel血钾浓度.Text = tb健康体检.Rows[0][tb_健康体检.血钾浓度].ToString();
                this.xrLabel血钠浓度.Text = tb健康体检.Rows[0][tb_健康体检.血钠浓度].ToString();
                this.xrLabel总胆固醇.Text = tb健康体检.Rows[0][tb_健康体检.总胆固醇].ToString();
                this.xrLabel甘油三酯.Text = tb健康体检.Rows[0][tb_健康体检.甘油三酯].ToString();
                this.xrLabel血清低密度.Text = tb健康体检.Rows[0][tb_健康体检.血清低密度脂蛋白胆固醇].ToString();
                this.xrLabel血清高密度.Text = tb健康体检.Rows[0][tb_健康体检.血清高密度脂蛋白胆固醇].ToString();
                this.xrLabel胸部X线片.Text = tb健康体检.Rows[0][tb_健康体检.胸部X线片].ToString();
                this.xrLabel胸部X线片异常.Text = tb健康体检.Rows[0][tb_健康体检.胸部X线片异常].ToString();
                this.xrLabelB超.Text = tb健康体检.Rows[0][tb_健康体检.B超].ToString();
                this.xrLabelB超异常.Text = tb健康体检.Rows[0][tb_健康体检.B超其他].ToString();
                this.xrLabel宫颈涂片.Text = tb健康体检.Rows[0][tb_健康体检.宫颈涂片].ToString();
                this.xrLabel宫颈涂片异常.Text = tb健康体检.Rows[0][tb_健康体检.宫颈涂片异常].ToString();
                this.xrLabel辅助检查其他.Text = tb健康体检.Rows[0][tb_健康体检.辅助检查其他].ToString();

                #endregion

                #region 中医
                this.xrLabel平和质.Text = tb健康体检.Rows[0][tb_健康体检.平和质].ToString();
                this.xrLabel气虚质.Text = tb健康体检.Rows[0][tb_健康体检.气虚质].ToString();
                this.xrLabel阳虚质.Text = tb健康体检.Rows[0][tb_健康体检.阳虚质].ToString();
                this.xrLabel阴虚质.Text = tb健康体检.Rows[0][tb_健康体检.阴虚质].ToString();
                this.xrLabel痰湿质.Text = tb健康体检.Rows[0][tb_健康体检.痰湿质].ToString();
                this.xrLabel湿热质.Text = tb健康体检.Rows[0][tb_健康体检.湿热质].ToString();
                this.xrLabel血瘀质.Text = tb健康体检.Rows[0][tb_健康体检.血瘀质].ToString();
                this.xrLabel气郁质.Text = tb健康体检.Rows[0][tb_健康体检.气郁质].ToString();
                this.xrLabel特禀质.Text = tb健康体检.Rows[0][tb_健康体检.特禀质].ToString();

                #endregion

                #region 现存主要健康问题
                string 脑血管 = tb健康体检.Rows[0][tb_健康体检.脑血管疾病].ToString();
                string[] 脑血管s = 脑血管.Split(',');
                for (int i = 0; i < 脑血管s.Length; i++)
                {
                    if (string.IsNullOrEmpty(脑血管s[i]))
                    {
                        continue;
                    }
                    string ctrName = "xrLabel脑血管疾病" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(ctrName, false);
                    lbl.Text = 脑血管s[i];
                }
                this.xrLabel脑血管疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.脑血管疾病其他].ToString();

                string 肾脏 = tb健康体检.Rows[0][tb_健康体检.肾脏疾病].ToString();
                string[] 肾脏s = 肾脏.Split(',');
                for (int i = 0; i < 肾脏s.Length; i++)
                {
                    if (string.IsNullOrEmpty(肾脏s[i]))
                    {
                        continue;
                    }
                    string ctrName = "xrLabel肾脏疾病" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(ctrName, false);
                    lbl.Text = 肾脏s[i];
                }
                this.xrLabel肾脏疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.肾脏疾病其他].ToString();

                string 心脏 = tb健康体检.Rows[0][tb_健康体检.心脏疾病].ToString();
                string[] 心脏s = 心脏.Split(',');
                for (int i = 0; i < 心脏s.Length; i++)
                {
                    if (string.IsNullOrEmpty(心脏s[i]))
                    {
                        continue;
                    }
                    string ctrName = "xrLabel心脏疾病" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(ctrName, false);
                    lbl.Text = 心脏s[i];
                }
                this.xrLabel心脏疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.心脏疾病其他].ToString();

                string 血管 = tb健康体检.Rows[0][tb_健康体检.血管疾病].ToString();
                string[] 血管s = 血管.Split(',');
                for (int i = 0; i < 血管s.Length; i++)
                {
                    if (string.IsNullOrEmpty(血管s[i]))
                    {
                        continue;
                    }
                    string ctrName = "xrLabel血管疾病" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(ctrName, false);
                    lbl.Text = 血管s[i];
                }
                this.xrLabel血管疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.血管疾病其他].ToString();

                string 眼部 = tb健康体检.Rows[0][tb_健康体检.眼部疾病].ToString();
                string[] 眼部s = 眼部.Split(',');
                for (int i = 0; i < 眼部s.Length; i++)
                {
                    if (string.IsNullOrEmpty(眼部s[i]))
                    {
                        continue;
                    }
                    string ctrName = "xrLabel眼部疾病" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(ctrName, false);
                    lbl.Text = 眼部s[i];
                }
                this.xrLabel眼部疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.眼部疾病其他].ToString();

                this.xrLabel神经系统疾病.Text = tb健康体检.Rows[0][tb_健康体检.神经系统疾病].ToString();
                this.xrLabel神经系统疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.神经系统疾病其他].ToString();

                this.xrLabel其他系统疾病.Text = tb健康体检.Rows[0][tb_健康体检.其他系统疾病].ToString();
                this.xrLabel其他系统疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.其他系统疾病其他].ToString();


                #endregion

            }
        }

    }
}
