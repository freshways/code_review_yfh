﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Library;
using AtomEHR.公共卫生.Module.健康档案.辖区健康档案;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class UC接诊记录表 : UserControlBase
    {
        public UC接诊记录表()
        {
            InitializeComponent();
        }

        DataRow[] _dr个人信息 = null;
        string _ID = "";
        public UC接诊记录表(DataRow[] dr,  UpdateType _UpdateType, object ID)
        {
            base._UpdateType = _UpdateType; //设置状态 修改/新增   
            _dr个人信息 = dr; //存个人信息    
            _ID =  ID == null ? "" : ID.ToString();     
            _BLL = new bll接诊记录();
            InitializeComponent();

            lbl个人档案号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.lbl姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            this.lbl性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.lbl身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.lbl出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            this.lbl联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            this.lbl居住地址.Text = dr[0][tb_健康档案.市].ToString() + dr[0][tb_健康档案.区].ToString() + dr[0][tb_健康档案.街道].ToString() +
                dr[0][tb_健康档案.居委会].ToString() + dr[0][tb_健康档案.居住地址].ToString();         
        }

        private void UC接诊记录表_Load(object sender, EventArgs e)
        {
            if (_UpdateType == UpdateType.Add)
            {
                _BLL.GetBusinessByKey("-", true);//下载一个空业务单据            
                _BLL.NewBusiness(); //增加一条主表记录
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                if (_ID != null && _ID != "")
                {
                    DataTable dt = ((bll接诊记录)_BLL).GetBusinessByIDEdit(_ID, true).Tables[tb_接诊记录.__TableName];
                }
                else return;
            }
            DoBindingSummaryEditor(_BLL.CurrentBusiness.Tables[tb_接诊记录.__TableName]);
            //设置颜色
            if (_UpdateType == UpdateType.Modify)
                Set考核项颜色_new(LayoutPanel1, lab考核项);
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            Set考核项颜色_new(LayoutPanel1, lab考核项);

            _BLL.DataBinder.Rows[0][tb_接诊记录.缺项] = _base缺项;
            _BLL.DataBinder.Rows[0][tb_接诊记录.完整度] = _base完整度;
            
            if (!ValidatingSummaryData()) return; //检查主表数据合法性

            if (_UpdateType == UpdateType.None) return; //操作状态是空直接返回
            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                //this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据

                ((bll接诊记录)BLL).Set个人健康特征(lbl个人档案号.Text);
                this._UpdateType = UpdateType.None;
                Msg.ShowInformation("保存成功!");

                UC接诊记录表_显示 control = new UC接诊记录表_显示(_dr个人信息, this.lbl创建时间.Text);
                ShowControl(control, DockStyle.Fill);
            }
            else
                Msg.Warning("保存失败!");
        }

        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {
            if (string.IsNullOrEmpty(ConvertEx.ToString(txt接诊时间.Text)))
            {
                Msg.Warning("接诊时间不能为空!");
                txt接诊时间.Focus();
                return false;
            }

            return true;
        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;

            dataSource.Rows[0][tb_接诊记录.个人档案编号] = lbl个人档案号.Text;

            DataBinder.BindingTextEdit(txt接诊时间, dataSource, tb_接诊记录.接诊时间);
            DataBinder.BindingTextEdit(txt接诊医生, dataSource, tb_接诊记录.接诊医生);
            DataBinder.BindingTextEdit(txt主观资料, dataSource, tb_接诊记录.主观资料);
            DataBinder.BindingTextEdit(txt评估, dataSource, tb_接诊记录.评估);
            DataBinder.BindingTextEdit(txt客观资料, dataSource, tb_接诊记录.客观资料);
            DataBinder.BindingTextEdit(txt处置计划, dataSource, tb_接诊记录.处置计划);

            //非编辑项
            this.lbl当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_接诊记录.所属机构].ToString());
            this.lbl创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_接诊记录.创建机构].ToString());
            this.lbl创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_接诊记录.创建人].ToString());
            this.lbl创建时间.Text = dataSource.Rows[0][tb_接诊记录.创建时间].ToString();
            this.lbl最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_接诊记录.修改人].ToString());
            this.lbl最近更新时间.Text = dataSource.Rows[0][tb_接诊记录.修改时间].ToString();
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            try
            {
                int age = System.DateTime.Now.Year - Convert.ToDateTime(lbl出生日期.Text).Year;
                List<string> list = new List<string>();
                //list.Add(this.lbl姓名.Text.ToString());
                //list.Add(this.lbl性别.Text.ToString());
                //list.Add(age.ToString());
                //list.Add(lbl居住地址.ToString());
                list.Add(this.txt接诊时间.Text.ToString());
                list.Add(this.txt接诊医生.Text.ToString());
                list.Add(this.txt主观资料.Text.ToString());
                list.Add(this.txt客观资料.Text.ToString());
                list.Add(this.txt评估.Text.ToString());
                list.Add(this.txt处置计划.Text.ToString());
                XtraReport接诊记录单 xrpt = new XtraReport接诊记录单(list, this.lbl姓名.Text, lbl性别.Text, age.ToString(), lbl居住地址.Text);
                ReportPrintTool tool = new ReportPrintTool(xrpt);
                tool.ShowPreviewDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("打印失败！异常信息:\n" + ex.Message, "消息", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
         
        }

    }
}
