﻿using AtomEHR.Business;
using AtomEHR.Common;
using AtomEHR.Library;
using AtomEHR.Models;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class UC健康体检表_显示 : UserControlBaseNavBar
    {
        #region Fields

        bll健康体检 _Bll = new bll健康体检();
        DataSet _ds健康体检;
        public frm个人健康 frm = null;
        string docNo;
        string familyNo;
        string date;
        string Row_ID;

        #endregion

        public UC健康体检表_显示()
        {
            InitializeComponent();
        }
        public UC健康体检表_显示(Form parentForm)
        {
            /*页面有两种打开方式
            1. 在主页面通过点击超链接 进行跳转，这时候需要从其中获取到 个人档案编号/家庭档案编号和体检时间（date）。
             * 通过体检时间来最终确认一条数据，并进行绑定
             * 
            2。打开个人健康页面后 在打开  健康体检 页面。
             * 这时候需要根据个人档案编号来查询全部的体检数据，然后进行倒叙排序，默认打开最新的一条数据（date为null）
            */
            InitializeComponent();
            //_base考核项 = "体检日期|责任医生";
            frm = (frm个人健康)parentForm;
            docNo = frm._docNo;
            familyNo = frm._familyDocNo;
            date = frm._param == null ? "" : frm._param.ToString();
            //根据个人档案编号 查询全部的  体检数据
            _ds健康体检 = _Bll.GetOneDataByKey(docNo, true);
            //DoBindingDataSource(_ds健康体检);//绑定数据
            //设置日期栏中字体颜色
            //SetItemColorToRed(_ds健康体检.Tables[0].Rows[0][tb_健康体检.ID].ToString());
        }

        #region Handle Events
        //延时加载相关变量
        BackgroundWorker bgInvoke = new BackgroundWorker();
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        private void frm健康体检表2_Load(object sender, EventArgs e)
        {
            InitView();

            bgInvoke.DoWork += bgInvoke_DoWork;
            bgInvoke.RunWorkerCompleted += bgInvoke_RunWorkerCompleted;
            int isallow = DataDictCache.Cache.IsAllow延时加载数据();
            if (isallow > 0 && !Loginer.CurrentUser.IsSubAdmin())
            {
                if (!bgInvoke.IsBusy)
                    bgInvoke.RunWorkerAsync(isallow);
                //初始化timer
                timer.Interval = (1000 * 3); //3秒执行一次
                timer.Tick += timer_Tick;
                this.layoutControlItem修改时间.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.layoutControlItem修改人.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
            else
            {
                DoBindingDataSource(_ds健康体检);
            }
        }

        #region 延时加载
        void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                _ds健康体检 = _Bll.GetOneDataByKey(docNo, true);
                //this.txt本人电话.Text = _ds.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人电话].ToString();
            }
            catch (Exception)
            {

            }
        }
        void bgInvoke_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
                DoBindingDataSource(_ds健康体检);

                //timer.Start();
                //this.OnLoad(e);
            }
            catch (Exception)
            {

            }
        }
        //后台线程
        void bgInvoke_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int iSS = Convert.ToInt32(e.Argument);
                
                DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(WaitForm1));

                for (int i = 0; i < iSS; i++)
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }
            catch (Exception)
            {

            }
        } 
        #endregion

        private void btn添加_Click(object sender, EventArgs e)
        {
            UC健康体检表 control = new UC健康体检表(frm, AtomEHR.Common.UpdateType.Add);
            ShowControl(control, DockStyle.Fill);
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_ds健康体检.Tables[0].Rows[0][tb_健康体检.所属机构].ToString()))
            {
                if (AtomEHR.Common.Msg.AskQuestion("确定要删除数据么?"))
                {
                    if (_Bll.DeleteByDocnoAndDate(docNo, date, Row_ID))
                    {
                        AtomEHR.Common.Msg.ShowInformation("删除数据成功！");
                        date = "";
                        //date = null;
                        //this.OnLoad(e);
                        //删除成功后 需 重新加载数据
                        _ds健康体检 = _Bll.GetOneDataByKey(docNo, true);
                        DoBindingDataSource(_ds健康体检);//绑定数据
                        this.layoutControl1.VerticalScroll.Value = 0;
                    }
                }
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }

        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_ds健康体检.Tables[0].Rows[0][tb_健康体检.所属机构].ToString()))
            {
                frm._param = date;
                UC健康体检表 control = new UC健康体检表(frm, AtomEHR.Common.UpdateType.Modify);
                ShowControl(control, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            //report健康体检表 report正面 = new report健康体检表(docNo, date);
            report健康体检表1_2017 report正面 = new report健康体检表1_2017(docNo, date);
            ReportPrintTool tool = new ReportPrintTool(report正面);
            tool.ShowPreviewDialog();
        }

        private void btn导出反面_Click(object sender, EventArgs e)
        {
            //report健康体检表2 report反面 = new report健康体检表2(docNo, date);
            report健康体检表2_2017 report反面 = new report健康体检表2_2017(docNo, date);
            ReportPrintTool tool = new ReportPrintTool(report反面);
            tool.ShowPreviewDialog();
        }

        #endregion

        #region Private Method
        /// <summary>
        /// 第一个checkbox是否选中来决定 后续的checkbook的enable状态
        /// </summary>
        /// <param name="firstChk"></param>
        public void SetControlEnable(CheckEdit firstChk)
        {
            bool flag = firstChk.Checked;
            FlowLayoutPanel container = firstChk.Parent as FlowLayoutPanel;
            if (container == null) return;
            Control.ControlCollection controlList = container.Controls;
            for (int i = 1; i < controlList.Count; i++)
            {
                if (controlList[i] is CheckEdit)
                {
                    CheckEdit item = (CheckEdit)controlList[i];
                    item.Enabled = !flag;
                    if (flag)//选中了第一个控件
                    {
                        item.Checked = false;
                    }
                }
                else
                {
                    controlList[i].Enabled = false;
                }
            }
        }

        public void SetTextEditEnable(Control control, TextEdit textEdit)
        {
            if (control is CheckEdit)//CheckEdit 类型的其他
            {
                CheckEdit check = (CheckEdit)control;
                textEdit.Enabled = check.Checked;
            }
            else if (true)
            {

            }
        }
        private void DoBindingDataSource(DataSet ds健康体检)
        {
            if (ds健康体检 == null) return;
            DataTable dt = ds健康体检.Tables[0];

            if (dt.Rows.Count == 0)//没有数据，则跳转到添加页面进行添加
            {
                UC健康体检表 uc = new UC健康体检表(frm, AtomEHR.Common.UpdateType.Add);
                ShowControl(uc);
                return;
            }
            //如果date为空，则默认绑定第一条体检数据
            //否则  根据date进行查找对应的 体检数据 进行绑定
            DataRow row = null;
            if (string.IsNullOrEmpty(date) && dt.Rows.Count > 0)
            {
                row = dt.Rows[0];
                Row_ID = row["ID"].ToString();
                SetItemColorToRed(Row_ID);
            }
            else
            {
                DataRow[] rows = dt.Select("创建时间 = '" + date + "' ");
                if (rows.Length == 1)
                {
                    row = rows[0];
                    Row_ID = row["ID"].ToString();
                    SetItemColorToRed(Row_ID);
                }
                else
                {
                    row = rows[0];
                    Row_ID = row["ID"].ToString();
                    SetItemColorToRed(Row_ID);
                    //return;
                }
            }

            #region 创建页面左边的导航树
            bool isallow = DataDictCache.Cache.IsAllow往年体检显示();
            if (!isallow)
            {
                // 克隆dt 的结构，包括所有 dt 架构和约束,并无数据； 
                DataTable newdt = dt.Clone();
                //把筛选出来的行添加到新表，如果开启隐藏往年体检，则用新表
                DataRow[] rows = dt.Select("体检日期 >= '2018-01-01' ");
                foreach (DataRow dr in rows)
                {
                    newdt.ImportRow(dr);
                }
                CreateNavBarButton_new(newdt, tb_健康体检.体检日期);
            }
            else
            {
                CreateNavBarButton_new(dt, tb_健康体检.体检日期);
            }
            #endregion

            // DataRow row = date == "" ? dt.Rows[0] : dt.Select("创建时间='" + date + "'")[0];

            //根据出生日期判断  老年人是否需要显示
            string 出生日期 = row["出生日期"].ToString();
            if (!string.IsNullOrEmpty(出生日期))
            {
                DateTime _dt = DateTime.Now;
                if (DateTime.TryParse(出生日期, out _dt))
                {
                    if (DateTime.Now.Year - _dt.Year >= 65)
                        group老年人.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    else
                        group老年人.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
            }
            //根据性别判断  老年人是否需要显示
            string 性别 = row["性别"].ToString();
            if (性别 == "男")
                this.group妇科.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            else
                this.group妇科.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            //绑定当前行数据
            this.DoBindingSummaryEditor(row);
        }
        protected override void DoBindingSummaryEditor(object dataSource)
        {
            DataRow row = (DataRow)dataSource;
            //date在每次的绑定中重新赋值，因为每次操作的记录都根据date进行
            date = row[tb_健康体检.创建时间].ToString();
            Row_ID = row[tb_健康体检.ID].ToString();

            this.txt个人档案编号.Text = row[tb_健康体检.个人档案编号].ToString();
            this.txt姓名.Text = row["姓名"].ToString();
            this.txt性别.Text = row["性别"].ToString();
            this.txt身份证号.Text = row["身份证号"].ToString();
            this.txt出生日期.Text = row["出生日期"].ToString();
            //修改联系电话绑定
            string str联系电话= row["本人电话"].ToString();
            if (!string.IsNullOrEmpty(str联系电话) && str联系电话!="无")
            {
                this.txt联系电话.Text = row["本人电话"].ToString();
            }
            else
            {
                this.txt联系电话.Text = row["联系人电话"].ToString();
            }
            //this.txt联系电话.Text = row["本人电话"].ToString();
            this.txt居住地址.Text = row[tb_健康档案.市].ToString().PadRight(10) + row[tb_健康档案.区].ToString().PadRight(10) + row[tb_健康档案.街道].ToString().PadRight(10) + row[tb_健康档案.居委会].ToString().PadRight(10) + row[tb_健康档案.居住地址].ToString();

            this.txt体检日期.Text = row[tb_健康体检.体检日期].ToString();
            this.txt责任医生.Text = row[tb_健康体检.FIELD2].ToString();
            this.txt症状.Text = row[tb_健康体检.症状].ToString() + row[tb_健康体检.症状其他].ToString();
            #region 一般状况
            this.txt体温.Txt1.Text = row[tb_健康体检.体温] == DBNull.Value ? "" : row[tb_健康体检.体温].ToString();
            this.txt脉率.Txt1.Text = row[tb_健康体检.脉搏] == DBNull.Value ? "" : row[tb_健康体检.脉搏].ToString();
            this.txt呼吸频率.Txt1.Text = row[tb_健康体检.呼吸] == DBNull.Value ? "" : row[tb_健康体检.呼吸].ToString();
            this.txt左侧血压.Text = row[tb_健康体检.血压左侧1].ToString() + "/" + row[tb_健康体检.血压左侧2].ToString() + "  mmHg";
            this.txt左侧原因.Text = row[tb_健康体检.左侧原因].ToString();
            this.txt血压右侧.Text = row[tb_健康体检.血压右侧1].ToString() + "/" + row[tb_健康体检.血压右侧2].ToString() + "  mmHg";
            this.txt右侧原因.Text = row[tb_健康体检.右侧原因].ToString();
            this.txt身高.Txt1.Text = row[tb_健康体检.身高] == DBNull.Value ? "" : row[tb_健康体检.身高].ToString();
            this.txt体重.Txt1.Text = row[tb_健康体检.体重] == DBNull.Value ? "" : row[tb_健康体检.体重].ToString();
            this.txt腰围.Txt1.Text = row[tb_健康体检.腰围] == DBNull.Value ? "" : row[tb_健康体检.腰围].ToString();
            this.txt体重指数.Txt1.Text = row[tb_健康体检.体重指数].ToString();
            this.txt老年人健康状态自我评估.Text = row[tb_健康体检.老年人状况评估].ToString();
            this.txt老年人生活自理能力自我评估.Text = row[tb_健康体检.老年人自理评估].ToString();
            this.txt老年人认知能力.Text = row[tb_健康体检.老年人认知].ToString().PadRight(5) + row[tb_健康体检.老年人认知分].ToString();
            this.txt老年人情感状态.Text = row[tb_健康体检.老年人情感].ToString().PadRight(5) + row[tb_健康体检.老年人情感分].ToString();
            #endregion

            #region 生活方式
            this.txt锻炼频率.Text = row[tb_健康体检.锻炼频率].ToString();
            if (this.txt锻炼频率.Text == "不锻炼")

                group体育锻炼.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            else
                group体育锻炼.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

            this.txt每次锻炼时间.Text = row[tb_健康体检.每次锻炼时间].ToString() + "分钟";
            this.txt坚持锻炼时间.Text = row[tb_健康体检.坚持锻炼时间].ToString() + "年";
            this.txt锻炼方式.Text = row[tb_健康体检.锻炼方式].ToString();

            this.txt饮食习惯.Text = row[tb_健康体检.饮食习惯].ToString();

            this.txt吸烟状况.Text = row[tb_健康体检.吸烟状况].ToString();
            if (this.txt吸烟状况.Text == "不吸烟")
                group吸烟情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            else
                group吸烟情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            this.txt日吸烟量.Text = "平均" + row[tb_健康体检.日吸烟量].ToString() + "（支）";
            this.txt开始吸烟年龄.Text = row[tb_健康体检.开始吸烟年龄].ToString() + "岁";
            this.txt戒烟年龄.Text = row[tb_健康体检.戒烟年龄].ToString() + "岁";

            this.txt饮酒频率.Text = row[tb_健康体检.饮酒频率].ToString();
            if (this.txt饮酒频率.Text == "从不")
                group饮酒情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            else
                group饮酒情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            this.txt日饮酒量.Text = "平均" + row[tb_健康体检.日饮酒量].ToString() + "（两）"; ;
            this.txt是否戒酒.Text = row[tb_健康体检.是否戒酒].ToString();
            this.txt戒酒年龄.Text = row[tb_健康体检.戒酒年龄].ToString() + "岁";
            this.txt开始饮酒年龄.Text = row[tb_健康体检.开始饮酒年龄].ToString() + "岁";
            this.txt近一年内是否曾醉酒.Text = row[tb_健康体检.近一年内是否曾醉酒].ToString();
            this.txt饮酒种类.Text = row[tb_健康体检.饮酒种类].ToString() + row[tb_健康体检.饮酒种类其它].ToString();


            string 有无职业病 = row[tb_健康体检.有无职业病].ToString();
            this.lbl职业病有无.Text = 有无职业病;
            if (有无职业病 == "有")//有
            {
                group职业病.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.lbl工种及从业时间.Text = row[tb_健康体检.具体职业].ToString().PadRight(5) + "从业时间" + row[tb_健康体检.从业时间] + "  年";
                this.txt粉尘.Text = row[tb_健康体检.粉尘].ToString();
                this.txt粉尘防护措施.Text = row[tb_健康体检.粉尘防护有无].ToString().PadRight(5) + row[tb_健康体检.粉尘防护措施];
                this.txt放射物质.Text = row[tb_健康体检.放射物质].ToString();
                this.txt放射物质防护措施.Text = row[tb_健康体检.放射物质防护措施有无].ToString().PadRight(5) + row[tb_健康体检.放射物质防护措施其他];
                this.txt物理因素.Text = row[tb_健康体检.物理因素].ToString();
                this.txt物理因素防护措施.Text = row[tb_健康体检.物理防护有无].ToString().PadRight(5) + row[tb_健康体检.物理防护措施];
                this.txt化学物质.Text = row[tb_健康体检.化学物质].ToString();
                this.txt化学物质防护措施.Text = row[tb_健康体检.化学物质防护].ToString().PadRight(5) + row[tb_健康体检.化学物质具体防护];
                this.txt职业病其他.Text = row[tb_健康体检.职业病其他].ToString();
                this.txt职业病其他防护措施.Text = row[tb_健康体检.其他防护有无].ToString().PadRight(5) + row[tb_健康体检.其他防护措施];
            }
            else
                group职业病.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            #endregion

            #region 脏器功能
            this.txt口唇.Text = row[tb_健康体检.口唇].ToString() + row[tb_健康体检.口唇其他];
            this.txt齿列有无.EditValue = row[tb_健康体检.齿列];
            if (row[tb_健康体检.齿列].ToString() != "正常,")
            {
                this.group齿列.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.lbl齿列正常.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                if (!string.IsNullOrEmpty(row[tb_健康体检.齿列缺齿].ToString()) && row[tb_健康体检.齿列缺齿].ToString() != "@@@")
                {
                    string[] quechi = row[tb_健康体检.齿列缺齿].ToString().Split('@');
                    this.txt缺齿1.Text = quechi[0];
                    this.txt缺齿2.Text = quechi[1];
                    this.txt缺齿3.Text = quechi[2];
                    this.txt缺齿4.Text = quechi[3];
                }
                else
                {
                    lbl缺齿.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
                if (!string.IsNullOrEmpty(row[tb_健康体检.齿列龋齿].ToString()) && row[tb_健康体检.齿列龋齿].ToString() != "@@@")
                {
                    string[] quchi = row[tb_健康体检.齿列龋齿].ToString().Split('@');
                    this.txt龋齿1.Text = quchi[0];
                    this.txt龋齿2.Text = quchi[1];
                    this.txt龋齿3.Text = quchi[2];
                    this.txt龋齿4.Text = quchi[3];
                }
                else
                {
                    lbl龋齿.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
                if (!string.IsNullOrEmpty(row[tb_健康体检.齿列义齿].ToString()) && row[tb_健康体检.齿列义齿].ToString() != "@@@")
                {
                    string[] yichi = row[tb_健康体检.齿列义齿].ToString().Split('@');
                    this.txt义齿1.Text = yichi[0];
                    this.txt义齿2.Text = yichi[1];
                    this.txt义齿3.Text = yichi[2];
                    this.txt义齿4.Text = yichi[3];
                }
                else
                {
                    lbl义齿.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
                if (!string.IsNullOrEmpty(row[tb_健康体检.齿列其他].ToString()))
                {
                    string qitachi = row[tb_健康体检.齿列其他].ToString();
                    this.txt齿列其他.Text = qitachi;
                }
                else
                {
                    lbl齿列其他.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
            }
            else
            {
                this.group齿列.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.lbl齿列正常.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }

            this.txt咽部.Text = row[tb_健康体检.咽部].ToString() + row[tb_健康体检.咽部其他];
            this.txt视力.Text = "左眼 " + row[tb_健康体检.左眼视力] + " 右眼 " + row[tb_健康体检.右眼视力] + "  （矫正视力：左眼 " + row[tb_健康体检.左眼矫正] + "  右眼 " + row[tb_健康体检.右眼矫正] + ")";
            this.txt听力.Text = row[tb_健康体检.听力].ToString();
            this.txt运动功能.Text = row[tb_健康体检.运动功能].ToString();
            #endregion

            #region 查体
            this.txt眼底.Text = row[tb_健康体检.眼底].ToString().PadRight(5) + row[tb_健康体检.眼底异常];
            this.txt皮肤.Text = row[tb_健康体检.皮肤].ToString().PadRight(5) + row[tb_健康体检.皮肤其他];
            this.txt巩膜.Text = row[tb_健康体检.巩膜].ToString().PadRight(5) + row[tb_健康体检.巩膜其他];
            this.txt淋巴结.Text = row[tb_健康体检.淋巴结].ToString().PadRight(5) + row[tb_健康体检.淋巴结其他];
            this.txt桶状胸.Text = row[tb_健康体检.桶状胸].ToString();
            this.txt呼吸音.Text = row[tb_健康体检.呼吸音].ToString().PadRight(5) + row[tb_健康体检.呼吸音异常];
            this.txt罗音.Text = row[tb_健康体检.罗音].ToString().PadRight(5) + row[tb_健康体检.罗音异常];
            this.txt心率.Text = row[tb_健康体检.心率].ToString() + "  次/分";
            this.txt心律.Text = row[tb_健康体检.心律].ToString();
            this.txt杂音.Text = row[tb_健康体检.杂音].ToString().PadRight(5) + row[tb_健康体检.杂音有];
            this.txt压痛.Text = row[tb_健康体检.压痛].ToString().PadRight(5) + row[tb_健康体检.压痛有];
            this.txt包块.Text = row[tb_健康体检.包块].ToString().PadRight(5) + row[tb_健康体检.包块有];
            this.txt肝大.Text = row[tb_健康体检.肝大].ToString().PadRight(5) + row[tb_健康体检.肝大有];
            this.txt脾大.Text = row[tb_健康体检.脾大].ToString().PadRight(5) + row[tb_健康体检.脾大有];
            this.txt移动性浊音.Text = row[tb_健康体检.浊音].ToString().PadRight(5) + row[tb_健康体检.浊音有];
            this.txt下肢水肿.Text = row[tb_健康体检.下肢水肿].ToString();
            this.txt足背动脉搏动.Text = row[tb_健康体检.足背动脉搏动].ToString();
            this.txt肛门指诊.Text = row[tb_健康体检.肛门指诊].ToString().PadRight(5) + row[tb_健康体检.肛门指诊异常];
            this.txt乳腺.Text = row[tb_健康体检.乳腺].ToString().PadRight(5) + row[tb_健康体检.乳腺其他];
            this.txt外阴.Text = row[tb_健康体检.外阴].ToString().PadRight(5) + row[tb_健康体检.外阴异常];
            this.txt阴道.Text = row[tb_健康体检.阴道].ToString().PadRight(5) + row[tb_健康体检.阴道异常];
            this.txt宫颈.Text = row[tb_健康体检.宫颈].ToString().PadRight(5) + row[tb_健康体检.宫颈异常];
            this.txt宫体.Text = row[tb_健康体检.宫体].ToString().PadRight(5) + row[tb_健康体检.宫体异常];
            this.txt附件.Text = row[tb_健康体检.附件].ToString().PadRight(5) + row[tb_健康体检.附件异常];
            this.txt查体其他.Text = row[tb_健康体检.查体其他].ToString();
            #endregion

            #region 血型
            this.txtABO.Text = row[tb_健康档案_健康状态.血型] == null ? "" : row[tb_健康档案_健康状态.血型].ToString();
            this.txtRh.Text = row[tb_健康档案_健康状态.RH] == null ? "" : row[tb_健康档案_健康状态.RH].ToString();
            #endregion

            #region 辅助检查
            this.txt血红蛋白.Text = row[tb_健康体检.血红蛋白].ToString() + "g/L";
            this.txt白细胞.Text = row[tb_健康体检.白细胞].ToString() + "  ×10^9/L";
            this.txt血小板.Text = row[tb_健康体检.血小板].ToString() + "  ×10^9/L";
            this.txt血常规其他.EditValue = row[tb_健康体检.血常规其他];
            this.txt尿常规.EditValue = "尿蛋白 " + row[tb_健康体检.尿蛋白] + " 尿糖" + row[tb_健康体检.尿糖] + "  尿酮体  " + row[tb_健康体检.尿酮体] + "  尿潜血  " + row[tb_健康体检.尿潜血] + "  其他  " + row[tb_健康体检.尿常规其他];
            this.txt空腹血糖.EditValue = row[tb_健康体检.空腹血糖].ToString() + "    mmol/L";
            this.txt空腹血糖2.EditValue = row[tb_健康体检.空腹血糖2].ToString() + "    mg/dL";
            this.txt同型半胱氨酸.EditValue = row[tb_健康体检.同型半胱氨酸].ToString() + "    umol/L";
            //this.txt餐后2H血糖.EditValue = row[tb_健康体检.餐后2H血糖].ToString() + "  mmol/L";
            this.txt心电图.EditValue = row[tb_健康体检.心电图].ToString().PadRight(5) + row[tb_健康体检.心电图异常];
            this.txt尿微量白蛋白.EditValue = row[tb_健康体检.尿微量白蛋白].ToString() + " mg/dL";
            this.txt大便潜血.EditValue = row[tb_健康体检.大便潜血];
            this.txt糖化血红蛋白.EditValue = row[tb_健康体检.糖化血红蛋白].ToString() + " %";
            this.txt乙型肝炎表面抗原.EditValue = row[tb_健康体检.乙型肝炎表面抗原];
            this.txt血清谷丙转氨酶.EditValue = row[tb_健康体检.血清谷丙转氨酶] + "  U/L";
            this.txt血清谷草转氨酶.EditValue = row[tb_健康体检.血清谷草转氨酶] + "  U/L";
            this.txt白蛋白.EditValue = row[tb_健康体检.白蛋白] + "  g/L";
            this.txt总胆红素.EditValue = row[tb_健康体检.总胆红素] + "  umol/L";
            this.txt结合胆红素.EditValue = row[tb_健康体检.结合胆红素] + "  umol/L";
            this.txt血清肌酐.EditValue = row[tb_健康体检.血清肌酐] + "  umol/L";
            this.txt血尿素氮.EditValue = row[tb_健康体检.血尿素氮] + "  mmol/L";
            this.txt血钾浓度.EditValue = row[tb_健康体检.血钾浓度] + "  umol/L";
            this.txt血钠浓度.EditValue = row[tb_健康体检.血钠浓度] + "  mmol/L";
            this.txt总胆固醇.EditValue = row[tb_健康体检.总胆固醇] + "  mmol/L";
            this.txt甘油三酯.EditValue = row[tb_健康体检.甘油三酯] + "  mmol/L";
            this.txt血清低密度脂蛋白胆固醇.EditValue = row[tb_健康体检.血清低密度脂蛋白胆固醇] + "  mmol/L";
            this.txt血清高密度脂蛋白胆固醇.EditValue = row[tb_健康体检.血清高密度脂蛋白胆固醇] + "  mmol/L";
            this.txt胸部X线片.EditValue = row[tb_健康体检.胸部X线片].ToString().PadRight(5) +
               row[tb_健康体检.胸部X线片异常];
            this.txt腹部B超.EditValue = row[tb_健康体检.B超].ToString().PadRight(5) +
            row[tb_健康体检.B超其他];
            this.txt其他B超.EditValue = row[tb_健康体检.其他B超].ToString().PadRight(5) +
            row[tb_健康体检.其他B超异常];
            this.txt宫颈涂片.EditValue = row[tb_健康体检.宫颈涂片].ToString().PadRight(5) +
               row[tb_健康体检.宫颈涂片异常];
            this.txt辅助检查其他.EditValue = row[tb_健康体检.辅助检查其他];
            #endregion

            #region 中医体质辨识
            this.txt平和质.EditValue = row[tb_健康体检.平和质];
            this.txt气虚质.EditValue = row[tb_健康体检.气虚质];
            this.txt阳虚质.EditValue = row[tb_健康体检.阳虚质];
            this.txt阴虚质.EditValue = row[tb_健康体检.阴虚质];
            this.txt痰湿质.EditValue = row[tb_健康体检.痰湿质];
            this.txt湿热质.EditValue = row[tb_健康体检.湿热质];
            this.txt血瘀质.EditValue = row[tb_健康体检.血瘀质];
            this.txt气郁质.EditValue = row[tb_健康体检.气郁质];
            this.txt特秉质.EditValue = row[tb_健康体检.特禀质];
            #endregion

            #region 现存主要健康问题
            this.txt脑血管疾病.EditValue = row[tb_健康体检.脑血管疾病].ToString().PadRight(5) + row[tb_健康体检.脑血管疾病其他];
            this.txt肾脏疾病.EditValue = row[tb_健康体检.肾脏疾病].ToString().PadRight(5) +
            row[tb_健康体检.肾脏疾病其他];
            this.txt心血管疾病.EditValue = row[tb_健康体检.心脏疾病].ToString().PadRight(5) + row[tb_健康体检.心脏疾病其他];
            //this.txt血管疾病.EditValue = row[tb_健康体检.血管疾病].ToString().PadRight(5) + row[tb_健康体检.血管疾病其他];
            this.txt眼部疾病.EditValue = row[tb_健康体检.眼部疾病].ToString().PadRight(5) + row[tb_健康体检.眼部疾病其他];
            this.txt神经系统疾病.EditValue = row[tb_健康体检.神经系统疾病].ToString().PadRight(5) + row[tb_健康体检.神经系统疾病其他];
            this.txt其他系统疾病.EditValue = row[tb_健康体检.其他系统疾病].ToString().PadRight(5) + row[tb_健康体检.其他系统疾病其他];
            #endregion

            #region MyRegion
            DataSet ds = _Bll.GetUserValueByKey(docNo, date, false);
            if (ds != null)
            {
                this.gc非免疫规划预防接种史.DataSource = ds.Tables[tb_健康体检_非免疫规划预防接种史.__TableName];
                this.gc家庭病床史.DataSource = ds.Tables["家庭病床史"];
                this.gc住院史.DataSource = ds.Tables["住院史"];
                this.gc主要用药情况.DataSource = ds.Tables[tb_健康体检_用药情况表.__TableName];
            }
            #endregion

            #region 健康评价
            if (row[tb_健康体检.健康评价].ToString() == "体检无异常" || string.IsNullOrEmpty(row[tb_健康体检.健康评价].ToString()))
            {
                this.txt健康评价.EditValue = "体检无异常";
            }
            else if (row[tb_健康体检.健康评价].ToString() == "有异常")
            {
                this.txt健康评价.EditValue = row[tb_健康体检.健康评价].ToString() + Environment.NewLine + "异常1：" +
              row[tb_健康体检.健康评价异常1] + Environment.NewLine + "异常2：" +
              row[tb_健康体检.健康评价异常2] + Environment.NewLine + "异常3：" +
              row[tb_健康体检.健康评价异常3] + Environment.NewLine + "异常4：" +
              row[tb_健康体检.健康评价异常4];

                this.lbl健康评价.MinSize = new System.Drawing.Size(0, 60);
            }

            #endregion

            #region 健康指导
            this.txt健康指导.EditValue = row[tb_健康体检.健康指导];
            string 危险因素控制 = row[tb_健康体检.危险因素控制].ToString();
            if (危险因素控制.IndexOf("减体重") != -1)//存在减体重
            {
                string jiantizhong = row[tb_健康体检.危险因素控制体重].ToString();
                if (!string.IsNullOrEmpty(jiantizhong))
                {
                    危险因素控制 = 危险因素控制.Insert(危险因素控制.IndexOf("减体重") + 4, "目标：" + jiantizhong + "\r\n");
                }
            }

            if (危险因素控制.IndexOf("建议疫苗接种") != -1)//存在减体重
            {
                string a = row[tb_健康体检.危险因素控制疫苗].ToString();
                if (!string.IsNullOrEmpty(a))
                {
                    危险因素控制 = 危险因素控制.Insert(危险因素控制.IndexOf("建议疫苗接种") + 7, "：" + a + "\r\n");
                }
            }
            if (危险因素控制.IndexOf("其他") != -1)//存在减体重
            {
                string a = row[tb_健康体检.危险因素控制其他].ToString();
                if (!string.IsNullOrEmpty(a))
                {
                    危险因素控制 = 危险因素控制.Insert(危险因素控制.IndexOf("其他") + 2, "：" + a);
                }
            }
            this.txt危险因素控制.Text = 危险因素控制;
            #endregion

            this.txt创建人.EditValue = _BLL.Return用户名称(row[tb_健康体检.创建人].ToString());
            this.txt创建时间.EditValue = row[tb_健康体检.创建时间];
            this.txt最近更新时间.EditValue = row[tb_健康体检.修改时间];
            this.txt最近修改人.EditValue = _BLL.Return用户名称(row[tb_健康体检.修改人].ToString());
            this.txt创建机构.EditValue = _BLL.Return机构名称(row[tb_健康体检.创建机构].ToString());
            this.txt当前所属机构.EditValue = _BLL.Return机构名称(row[tb_健康体检.所属机构].ToString());
            this.labelControl1.Text = string.Format("考核项：48  缺项：{0}     完整度：{1} % ", row[tb_健康体检.缺项], row[tb_健康体检.完整度]);

            this.layoutControl1.VerticalScroll.Value = 0;
            Set考核项(row);
        }

        private void Set考核项(DataRow row)
        {

            if (string.IsNullOrEmpty(row[tb_健康体检.症状].ToString()))
            {
                this.lbl症状.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.体检日期].ToString()))
            {
                this.lbl体检日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.FIELD2].ToString()))
            {
                this.lbl责任医生.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            #region 一般状况
            if (row[tb_健康体检.体温] == DBNull.Value)
            {
                this.lbl体温.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (row[tb_健康体检.脉搏] == DBNull.Value)
            {
                this.lbl脉率.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (row[tb_健康体检.呼吸] == DBNull.Value)
            {
                this.lbl呼吸频率.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.血压左侧1].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.血压左侧2].ToString()))
            {
                this.lbl血压左侧.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.血压右侧1].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.血压右侧2].ToString()))
            {
                this.lbl血压右侧.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (row[tb_健康体检.身高] == DBNull.Value)
            {
                this.lbl身高.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (row[tb_健康体检.体重] == DBNull.Value)
            {
                this.lbl体重.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }

            if (row[tb_健康体检.腰围] == DBNull.Value)
            {
                this.lbl腰围.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.体重指数].ToString()))
            {
                this.lbl体重指数.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            #endregion

            #region 生活方式
            if (string.IsNullOrEmpty(row[tb_健康体检.锻炼频率].ToString()))
            {
                this.lbl锻炼频率.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.饮食习惯].ToString()))
            {
                this.lbl饮食习惯.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.吸烟状况].ToString()))
            {
                this.lbl吸烟状况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }

            if (string.IsNullOrEmpty(row[tb_健康体检.饮酒频率].ToString()))
            {
                this.lbl饮酒情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }


            if (string.IsNullOrEmpty(row[tb_健康体检.有无职业病].ToString()))
            {
                this.lbl职业病危害因素接触史.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }

            #endregion

            #region 脏器功能
            if (string.IsNullOrEmpty(row[tb_健康体检.齿列].ToString()))
            {
                this.lbl齿列总.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.咽部].ToString()))
            {
                this.lbl咽部.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.左眼视力].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.右眼视力].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.左眼矫正].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.右眼矫正].ToString()))
            {
                this.lbl视力.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            #endregion

            #region 查体
            if (string.IsNullOrEmpty(row[tb_健康体检.皮肤].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.皮肤其他].ToString()))
            {
                this.lbl皮肤.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.巩膜].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.巩膜其他].ToString()))
            {
                this.lbl巩膜.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.淋巴结].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.淋巴结其他].ToString()))
            {
                this.lbl淋巴结.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.罗音].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.罗音异常].ToString()))
            {
                this.lbl罗音.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.心率].ToString()))
            {
                this.lbl心率.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }

            #endregion

            #region 老年人辅助检查填写考核项
            string str出生日期 = row["出生日期"].ToString();
            if (!string.IsNullOrEmpty(str出生日期))
            {
                DateTime dt = DateTime.Now;
                if (DateTime.TryParse(str出生日期, out dt))
                {
                    if (DateTime.Now.Year - dt.Year >= 65)
                    {
                        if (string.IsNullOrEmpty(row[tb_健康体检.血清谷丙转氨酶].ToString()))
                        {
                            this.lbl血清谷丙转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清谷丙转氨酶.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血清谷丙转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清谷丙转氨酶.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.血清谷草转氨酶].ToString()))
                        {
                            this.lbl血清谷草转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清谷草转氨酶.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血清谷草转氨酶.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清谷草转氨酶.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.总胆红素].ToString()))
                        {
                            this.lbl总胆红素.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl总胆红素.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl总胆红素.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl总胆红素.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.血清肌酐].ToString()))
                        {
                            this.lbl血清肌酐.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清肌酐.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血清肌酐.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清肌酐.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.血尿素氮].ToString()))
                        {
                            this.lbl血尿素氮.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血尿素氮.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血尿素氮.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血尿素氮.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.总胆固醇].ToString()))
                        {
                            this.lbl总胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl总胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl总胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl总胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.甘油三酯].ToString()))
                        {
                            this.lbl甘油三酯.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl甘油三酯.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl甘油三酯.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl甘油三酯.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.血清低密度脂蛋白胆固醇].ToString()))
                        {
                            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清低密度脂蛋白胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (string.IsNullOrEmpty(row[tb_健康体检.血清高密度脂蛋白胆固醇].ToString()))
                        {
                            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl血清高密度脂蛋白胆固醇.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (row[tb_健康体检.B超].ToString() == "未检测" || (row[tb_健康体检.B超].ToString() == "异常" && string.IsNullOrEmpty(row[tb_健康体检.B超其他].ToString())))
                        {
                            this.lbl腹部B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl腹部B超.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl腹部B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl腹部B超.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                        if (row[tb_健康体检.其他B超].ToString() == "异常" && string.IsNullOrEmpty(row[tb_健康体检.其他B超异常].ToString()))
                        {
                            this.lbl其他B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl其他B超.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                        }
                        else
                        {
                            this.lbl其他B超.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, FontStyle.Bold);
                            this.lbl其他B超.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
                        }
                    }
                }

            }
            #endregion

            #region 现存主要健康问题
            if (string.IsNullOrEmpty(row[tb_健康体检.脑血管疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.脑血管疾病其他].ToString()))
            {
                this.lbl脑血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.肾脏疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.肾脏疾病其他].ToString()))
            {
                this.lbl肾脏疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.心脏疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.心脏疾病其他].ToString()))
            {
                this.lbl心血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            //if (string.IsNullOrEmpty(row[tb_健康体检.血管疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.血管疾病其他].ToString()))
            //{
            //    this.lbl血管疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            //}
            if (string.IsNullOrEmpty(row[tb_健康体检.眼部疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.眼部疾病其他].ToString()))
            {
                this.lbl眼部疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.神经系统疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.神经系统疾病其他].ToString()))
            {
                this.lbl神经系统疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            if (string.IsNullOrEmpty(row[tb_健康体检.其他系统疾病].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.其他系统疾病其他].ToString()))
            {
                this.lbl其他系统疾病.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            #endregion

            #region 健康评价
            if (string.IsNullOrEmpty(row[tb_健康体检.健康评价].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.健康评价异常1].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.健康评价异常2].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.健康评价异常3].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.健康评价异常4].ToString()))
            {
                this.lbl健康评价.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                this.lbl健康评价.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            }
            #endregion

            #region 健康指导
            if (string.IsNullOrEmpty(row[tb_健康体检.健康指导].ToString()) && string.IsNullOrEmpty(row[tb_健康体检.危险因素控制].ToString()))
            {
                this.lbl健康指导.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            }

            #endregion
        }

        private void InitView()
        {
            DataBinder.BindingLookupEditDataSource(lkp服药依从性, DataDictCache.Cache.t服药依从性, "P_DESC", "P_CODE");
            this.txt呼吸频率.Txt1.Margin = new System.Windows.Forms.Padding(0, 10, 0, 3);
            this.txt呼吸频率.Lbl1.Margin = new System.Windows.Forms.Padding(0, 10, 0, 3);
            this.txt体温.Txt1.Properties.ReadOnly = true;
            this.txt体温.Txt1.BackColor = System.Drawing.Color.Empty;
            this.txt脉率.Txt1.Properties.ReadOnly = true;
            this.txt脉率.Txt1.BackColor = System.Drawing.Color.Empty;
            this.txt呼吸频率.Txt1.Properties.ReadOnly = true;
            this.txt呼吸频率.Txt1.BackColor = System.Drawing.Color.Empty;
            this.txt身高.Txt1.Properties.ReadOnly = true;
            this.txt身高.Txt1.BackColor = System.Drawing.Color.Empty;
            this.txt体重.Txt1.Properties.ReadOnly = true;
            this.txt体重.Txt1.BackColor = System.Drawing.Color.Empty;
            this.txt腰围.Txt1.Properties.ReadOnly = true;
            this.txt腰围.Txt1.BackColor = System.Drawing.Color.Empty;
            this.txt体重指数.Txt1.Properties.ReadOnly = true;
            this.txt体重指数.Txt1.BackColor = System.Drawing.Color.Empty;
        }
        #endregion

        private void btn查看随访照片_Click(object sender, EventArgs e)
        {
            DataTable _dt图像采集 = _Bll.Get_图像采集(docNo, this.txt体检日期.Text);
            if (_dt图像采集 != null && _dt图像采集.Rows.Count >= 1)
            {
                var img = _dt图像采集.Rows[0]["图像"];
                if (img != null)
                {
                    System.Drawing.Image img健康评价 = (Image)ZipTools.DecompressionObject((byte[])img);
                    AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息.Frm随访照片查看 frm = new AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息.Frm随访照片查看(img健康评价);
                    frm.ShowDialog();
                }
            }
        }

    }
}
