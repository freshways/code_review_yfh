﻿namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    partial class report健康体检表A4_4_2017
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell230 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell307 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox主要用药情况 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药医师签字 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物名称6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用法6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_药物用量6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_用药时间6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_服药依从性6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种史名称1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种日期1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种机构1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种医师签字 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种史名称2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种日期2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种机构2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种史名称3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种日期3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_接种机构3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康评价 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox健康评价 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell341 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell344 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell345 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell346 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell347 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell349 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell350 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康评价异常1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell351 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_健康评价医师签字 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell337 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康评价异常2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康评价异常3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康评价异常4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell251 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell348 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell366 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell353 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_危险控制因素6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell354 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox健康指导 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell365 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell367 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell368 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell369 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell362 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell356 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell361 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_减重目标 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell363 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable_健康指导医师签字 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell355 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell357 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell358 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_建议接种疫苗 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell359 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_建议接种疫苗补充 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow70 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_健康指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_健康指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_健康指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_危险控制因素其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow71 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_危险控制因素其他补充 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell370 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell373 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell395 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell405 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell371 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell372 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell406 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell409 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell386 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell387 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell389 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell375 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell383 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell381 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_反馈时间年 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell374 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell376 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_反馈时间月 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell377 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell378 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel_反馈时间日 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell379 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrTableCell382 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell400 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrPictureBox本人签字 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox家属签字 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox反馈人签字 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail.Font = new System.Drawing.Font("宋体", 9.75F);
            this.Detail.HeightF = 1169F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 49.62502F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow47,
            this.xrTableRow46,
            this.xrTableRow45,
            this.xrTableRow44,
            this.xrTableRow43,
            this.xrTableRow42,
            this.xrTableRow41,
            this.xrTableRow40,
            this.xrTableRow39,
            this.xrTableRow50,
            this.xrTableRow53,
            this.xrTableRow55,
            this.xrTableRow56,
            this.xrTableRow54,
            this.xrTableRow52,
            this.xrTableRow51,
            this.xrTableRow58,
            this.xrTableRow61,
            this.xrTableRow60,
            this.xrTableRow59,
            this.xrTableRow57,
            this.xrTableRow70,
            this.xrTableRow71,
            this.xrTableRow62,
            this.xrTableRow67,
            this.xrTableRow68,
            this.xrTableRow69,
            this.xrTableRow66,
            this.xrTableRow65,
            this.xrTableRow64,
            this.xrTableRow63});
            this.xrTable3.SizeF = new System.Drawing.SizeF(802.0001F, 850F);
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell230,
            this.xrTableCell227,
            this.xrTableCell16,
            this.xrTableCell226,
            this.xrTableCell229,
            this.xrTableCell66,
            this.xrTableCell228,
            this.xrTableCell74});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell230
            // 
            this.xrTableCell230.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell230.Name = "xrTableCell230";
            this.xrTableCell230.StylePriority.UseBorders = false;
            this.xrTableCell230.Weight = 0.15869037293521471D;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.StylePriority.UseBorders = false;
            this.xrTableCell227.Weight = 0.054811234414819943D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseBorders = false;
            this.xrTableCell16.Weight = 0.52634479657103728D;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.StylePriority.UseBorders = false;
            this.xrTableCell226.Weight = 0.44998142626178439D;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.StylePriority.UseBorders = false;
            this.xrTableCell229.Weight = 0.39716482711325179D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseBorders = false;
            this.xrTableCell66.Weight = 0.433388412000458D;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.StylePriority.UseBorders = false;
            this.xrTableCell228.Text = "服药依从性";
            this.xrTableCell228.Weight = 0.64427303091311949D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.Weight = 0.2718740427070635D;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell303,
            this.xrTableCell304,
            this.xrTableCell305,
            this.xrTableCell306,
            this.xrTableCell307,
            this.xrTableCell308,
            this.xrTableCell309,
            this.xrTableCell310});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1D;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.StylePriority.UseBorders = false;
            this.xrTableCell303.Weight = 0.15869037293521471D;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.StylePriority.UseBorders = false;
            this.xrTableCell304.Weight = 0.054811234414819943D;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.StylePriority.UseBorders = false;
            this.xrTableCell305.Text = "药物名称";
            this.xrTableCell305.Weight = 0.52634479657103728D;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.StylePriority.UseBorders = false;
            this.xrTableCell306.Text = "用    法";
            this.xrTableCell306.Weight = 0.44998142626178439D;
            // 
            // xrTableCell307
            // 
            this.xrTableCell307.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell307.Name = "xrTableCell307";
            this.xrTableCell307.StylePriority.UseBorders = false;
            this.xrTableCell307.Text = "用    量";
            this.xrTableCell307.Weight = 0.39716482711325179D;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.StylePriority.UseBorders = false;
            this.xrTableCell308.Text = "用  药  时  间";
            this.xrTableCell308.Weight = 0.43338822208493433D;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.StylePriority.UseBorders = false;
            this.xrTableCell309.Text = " 1 规律  2 间断  3 不服药";
            this.xrTableCell309.Weight = 0.64427322082864324D;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.StylePriority.UseBorders = false;
            this.xrTableCell310.Weight = 0.2718740427070635D;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell311,
            this.xrTableCell312,
            this.xrTable_药物名称1,
            this.xrTable_药物用法1,
            this.xrTable_药物用量1,
            this.xrTable_用药时间1,
            this.xrTable_服药依从性1,
            this.xrTableCell318});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1D;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.StylePriority.UseBorders = false;
            this.xrTableCell311.Weight = 0.15869037293521471D;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.StylePriority.UseBorders = false;
            this.xrTableCell312.Text = "1";
            this.xrTableCell312.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称1
            // 
            this.xrTable_药物名称1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称1.Name = "xrTable_药物名称1";
            this.xrTable_药物名称1.StylePriority.UseBorders = false;
            this.xrTable_药物名称1.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法1
            // 
            this.xrTable_药物用法1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法1.Name = "xrTable_药物用法1";
            this.xrTable_药物用法1.StylePriority.UseBorders = false;
            this.xrTable_药物用法1.Weight = 0.44998142626178433D;
            // 
            // xrTable_药物用量1
            // 
            this.xrTable_药物用量1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量1.Name = "xrTable_药物用量1";
            this.xrTable_药物用量1.StylePriority.UseBorders = false;
            this.xrTable_药物用量1.Weight = 0.39716482711325191D;
            // 
            // xrTable_用药时间1
            // 
            this.xrTable_用药时间1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间1.Name = "xrTable_用药时间1";
            this.xrTable_用药时间1.StylePriority.UseBorders = false;
            this.xrTable_用药时间1.Weight = 0.43338860191598172D;
            // 
            // xrTable_服药依从性1
            // 
            this.xrTable_服药依从性1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性1.Name = "xrTable_服药依从性1";
            this.xrTable_服药依从性1.StylePriority.UseBorders = false;
            this.xrTable_服药依从性1.Weight = 0.64427284099759585D;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell318.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox主要用药情况});
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.StylePriority.UseBorders = false;
            this.xrTableCell318.Weight = 0.2718740427070635D;
            // 
            // xrPictureBox主要用药情况
            // 
            this.xrPictureBox主要用药情况.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox主要用药情况.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 0F);
            this.xrPictureBox主要用药情况.Name = "xrPictureBox主要用药情况";
            this.xrPictureBox主要用药情况.SizeF = new System.Drawing.SizeF(70F, 25F);
            this.xrPictureBox主要用药情况.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox主要用药情况.StylePriority.UseBorders = false;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell295,
            this.xrTableCell296,
            this.xrTable_药物名称2,
            this.xrTable_药物用法2,
            this.xrTable_药物用量2,
            this.xrTable_用药时间2,
            this.xrTable_服药依从性2,
            this.xrTableCell302});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1D;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.StylePriority.UseBorders = false;
            this.xrTableCell295.Text = "主要";
            this.xrTableCell295.Weight = 0.15869037293521471D;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.StylePriority.UseBorders = false;
            this.xrTableCell296.Text = "2";
            this.xrTableCell296.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称2
            // 
            this.xrTable_药物名称2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称2.Name = "xrTable_药物名称2";
            this.xrTable_药物名称2.StylePriority.UseBorders = false;
            this.xrTable_药物名称2.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法2
            // 
            this.xrTable_药物用法2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法2.Name = "xrTable_药物用法2";
            this.xrTable_药物用法2.StylePriority.UseBorders = false;
            this.xrTable_药物用法2.Weight = 0.44998142626178439D;
            // 
            // xrTable_药物用量2
            // 
            this.xrTable_药物用量2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量2.Name = "xrTable_药物用量2";
            this.xrTable_药物用量2.StylePriority.UseBorders = false;
            this.xrTable_药物用量2.Weight = 0.39716482711325179D;
            // 
            // xrTable_用药时间2
            // 
            this.xrTable_用药时间2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间2.Name = "xrTable_用药时间2";
            this.xrTable_用药时间2.StylePriority.UseBorders = false;
            this.xrTable_用药时间2.Weight = 0.433388412000458D;
            // 
            // xrTable_服药依从性2
            // 
            this.xrTable_服药依从性2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性2.Name = "xrTable_服药依从性2";
            this.xrTable_服药依从性2.StylePriority.UseBorders = false;
            this.xrTable_服药依从性2.Weight = 0.64427303091311949D;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.StylePriority.UseBorders = false;
            this.xrTableCell302.Weight = 0.2718740427070635D;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell287,
            this.xrTableCell288,
            this.xrTable_药物名称3,
            this.xrTable_药物用法3,
            this.xrTable_药物用量3,
            this.xrTable_用药时间3,
            this.xrTable_服药依从性3,
            this.xrTable_用药医师签字});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1D;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.StylePriority.UseBorders = false;
            this.xrTableCell287.Text = "用药";
            this.xrTableCell287.Weight = 0.15869037293521471D;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.StylePriority.UseBorders = false;
            this.xrTableCell288.Text = "3";
            this.xrTableCell288.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称3
            // 
            this.xrTable_药物名称3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称3.Name = "xrTable_药物名称3";
            this.xrTable_药物名称3.StylePriority.UseBorders = false;
            this.xrTable_药物名称3.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法3
            // 
            this.xrTable_药物用法3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法3.Name = "xrTable_药物用法3";
            this.xrTable_药物用法3.StylePriority.UseBorders = false;
            this.xrTable_药物用法3.Weight = 0.44998142626178439D;
            // 
            // xrTable_药物用量3
            // 
            this.xrTable_药物用量3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量3.Name = "xrTable_药物用量3";
            this.xrTable_药物用量3.StylePriority.UseBorders = false;
            this.xrTable_药物用量3.Weight = 0.39716482711325179D;
            // 
            // xrTable_用药时间3
            // 
            this.xrTable_用药时间3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间3.Name = "xrTable_用药时间3";
            this.xrTable_用药时间3.StylePriority.UseBorders = false;
            this.xrTable_用药时间3.Weight = 0.43338822208493433D;
            // 
            // xrTable_服药依从性3
            // 
            this.xrTable_服药依从性3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性3.Name = "xrTable_服药依从性3";
            this.xrTable_服药依从性3.StylePriority.UseBorders = false;
            this.xrTable_服药依从性3.Weight = 0.64427322082864324D;
            // 
            // xrTable_用药医师签字
            // 
            this.xrTable_用药医师签字.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_用药医师签字.Name = "xrTable_用药医师签字";
            this.xrTable_用药医师签字.StylePriority.UseBorders = false;
            this.xrTable_用药医师签字.Weight = 0.2718740427070635D;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell279,
            this.xrTableCell280,
            this.xrTable_药物名称4,
            this.xrTable_药物用法4,
            this.xrTable_药物用量4,
            this.xrTable_用药时间4,
            this.xrTable_服药依从性4,
            this.xrTableCell286});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1D;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.StylePriority.UseBorders = false;
            this.xrTableCell279.Text = "情况";
            this.xrTableCell279.Weight = 0.15869037293521471D;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.StylePriority.UseBorders = false;
            this.xrTableCell280.Text = "4";
            this.xrTableCell280.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称4
            // 
            this.xrTable_药物名称4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称4.Name = "xrTable_药物名称4";
            this.xrTable_药物名称4.StylePriority.UseBorders = false;
            this.xrTable_药物名称4.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法4
            // 
            this.xrTable_药物用法4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法4.Name = "xrTable_药物用法4";
            this.xrTable_药物用法4.StylePriority.UseBorders = false;
            this.xrTable_药物用法4.Weight = 0.44998142626178439D;
            // 
            // xrTable_药物用量4
            // 
            this.xrTable_药物用量4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量4.Name = "xrTable_药物用量4";
            this.xrTable_药物用量4.StylePriority.UseBorders = false;
            this.xrTable_药物用量4.Weight = 0.39716482711325179D;
            // 
            // xrTable_用药时间4
            // 
            this.xrTable_用药时间4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间4.Name = "xrTable_用药时间4";
            this.xrTable_用药时间4.StylePriority.UseBorders = false;
            this.xrTable_用药时间4.Weight = 0.43338822208493433D;
            // 
            // xrTable_服药依从性4
            // 
            this.xrTable_服药依从性4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性4.Name = "xrTable_服药依从性4";
            this.xrTable_服药依从性4.StylePriority.UseBorders = false;
            this.xrTable_服药依从性4.Weight = 0.64427322082864324D;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.StylePriority.UseBorders = false;
            this.xrTableCell286.Weight = 0.2718740427070635D;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell271,
            this.xrTableCell272,
            this.xrTable_药物名称5,
            this.xrTable_药物用法5,
            this.xrTable_药物用量5,
            this.xrTable_用药时间5,
            this.xrTable_服药依从性5,
            this.xrTableCell278});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1D;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.StylePriority.UseBorders = false;
            this.xrTableCell271.Weight = 0.15869037293521471D;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.StylePriority.UseBorders = false;
            this.xrTableCell272.Text = "5";
            this.xrTableCell272.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称5
            // 
            this.xrTable_药物名称5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称5.Name = "xrTable_药物名称5";
            this.xrTable_药物名称5.StylePriority.UseBorders = false;
            this.xrTable_药物名称5.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法5
            // 
            this.xrTable_药物用法5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法5.Name = "xrTable_药物用法5";
            this.xrTable_药物用法5.StylePriority.UseBorders = false;
            this.xrTable_药物用法5.Weight = 0.44998142626178439D;
            // 
            // xrTable_药物用量5
            // 
            this.xrTable_药物用量5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量5.Name = "xrTable_药物用量5";
            this.xrTable_药物用量5.StylePriority.UseBorders = false;
            this.xrTable_药物用量5.Weight = 0.39716482711325179D;
            // 
            // xrTable_用药时间5
            // 
            this.xrTable_用药时间5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间5.Name = "xrTable_用药时间5";
            this.xrTable_用药时间5.StylePriority.UseBorders = false;
            this.xrTable_用药时间5.Weight = 0.43338822208493433D;
            // 
            // xrTable_服药依从性5
            // 
            this.xrTable_服药依从性5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性5.Name = "xrTable_服药依从性5";
            this.xrTable_服药依从性5.StylePriority.UseBorders = false;
            this.xrTable_服药依从性5.Weight = 0.64427322082864324D;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.StylePriority.UseBorders = false;
            this.xrTableCell278.Weight = 0.2718740427070635D;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTable_药物名称6,
            this.xrTable_药物用法6,
            this.xrTable_药物用量6,
            this.xrTable_用药时间6,
            this.xrTable_服药依从性6,
            this.xrTableCell270});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.StylePriority.UseBorders = false;
            this.xrTableCell263.Weight = 0.15869037293521471D;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.StylePriority.UseBorders = false;
            this.xrTableCell264.Text = "6";
            this.xrTableCell264.Weight = 0.054811234414819943D;
            // 
            // xrTable_药物名称6
            // 
            this.xrTable_药物名称6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物名称6.Name = "xrTable_药物名称6";
            this.xrTable_药物名称6.StylePriority.UseBorders = false;
            this.xrTable_药物名称6.Weight = 0.52634479657103728D;
            // 
            // xrTable_药物用法6
            // 
            this.xrTable_药物用法6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用法6.Name = "xrTable_药物用法6";
            this.xrTable_药物用法6.StylePriority.UseBorders = false;
            this.xrTable_药物用法6.Weight = 0.44998142626178439D;
            // 
            // xrTable_药物用量6
            // 
            this.xrTable_药物用量6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_药物用量6.Name = "xrTable_药物用量6";
            this.xrTable_药物用量6.StylePriority.UseBorders = false;
            this.xrTable_药物用量6.Weight = 0.39716482711325179D;
            // 
            // xrTable_用药时间6
            // 
            this.xrTable_用药时间6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_用药时间6.Name = "xrTable_用药时间6";
            this.xrTable_用药时间6.StylePriority.UseBorders = false;
            this.xrTable_用药时间6.Weight = 0.43338822208493433D;
            // 
            // xrTable_服药依从性6
            // 
            this.xrTable_服药依从性6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_服药依从性6.Name = "xrTable_服药依从性6";
            this.xrTable_服药依从性6.StylePriority.UseBorders = false;
            this.xrTable_服药依从性6.Weight = 0.64427322082864324D;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.StylePriority.UseBorders = false;
            this.xrTableCell270.Weight = 0.2718740427070635D;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell255,
            this.xrTableCell256,
            this.xrTableCell257,
            this.xrTableCell258,
            this.xrTableCell261,
            this.xrTableCell262});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.StylePriority.UseBorders = false;
            this.xrTableCell255.Text = "非免";
            this.xrTableCell255.Weight = 0.15869037293521471D;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Weight = 0.054811234414819943D;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.StylePriority.UseBorders = false;
            this.xrTableCell257.Weight = 0.52634498648656092D;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.StylePriority.UseBorders = false;
            this.xrTableCell258.Weight = 0.44998095147297529D;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.StylePriority.UseBorders = false;
            this.xrTableCell261.Weight = 1.4748265549001149D;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.StylePriority.UseBorders = false;
            this.xrTableCell262.Weight = 0.2718740427070635D;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell247,
            this.xrTableCell248,
            this.xrTableCell249,
            this.xrTableCell250,
            this.xrTableCell253,
            this.xrTableCell254});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.StylePriority.UseBorders = false;
            this.xrTableCell247.Text = "疫规";
            this.xrTableCell247.Weight = 0.15869037293521471D;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.StylePriority.UseBorders = false;
            this.xrTableCell248.Weight = 0.054811234414819943D;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.StylePriority.UseBorders = false;
            this.xrTableCell249.Text = "名    称";
            this.xrTableCell249.Weight = 0.52634498648656092D;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.StylePriority.UseBorders = false;
            this.xrTableCell250.Text = "接  种  日  期";
            this.xrTableCell250.Weight = 0.44998095147297529D;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.StylePriority.UseBorders = false;
            this.xrTableCell253.Text = "接    种    机    构";
            this.xrTableCell253.Weight = 1.4748265549001149D;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.StylePriority.UseBorders = false;
            this.xrTableCell254.Weight = 0.2718740427070635D;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell239,
            this.xrTableCell240,
            this.xrTable_接种史名称1,
            this.xrTable_接种日期1,
            this.xrTable_接种机构1,
            this.xrTable_接种医师签字});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.StylePriority.UseBorders = false;
            this.xrTableCell239.Text = "划预";
            this.xrTableCell239.Weight = 0.15869037293521471D;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.StylePriority.UseBorders = false;
            this.xrTableCell240.Text = "1";
            this.xrTableCell240.Weight = 0.054811234414819943D;
            // 
            // xrTable_接种史名称1
            // 
            this.xrTable_接种史名称1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种史名称1.Name = "xrTable_接种史名称1";
            this.xrTable_接种史名称1.StylePriority.UseBorders = false;
            this.xrTable_接种史名称1.Weight = 0.52634498648656092D;
            // 
            // xrTable_接种日期1
            // 
            this.xrTable_接种日期1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种日期1.Name = "xrTable_接种日期1";
            this.xrTable_接种日期1.StylePriority.UseBorders = false;
            this.xrTable_接种日期1.Weight = 0.44998095147297529D;
            // 
            // xrTable_接种机构1
            // 
            this.xrTable_接种机构1.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种机构1.Name = "xrTable_接种机构1";
            this.xrTable_接种机构1.StylePriority.UseBorders = false;
            this.xrTable_接种机构1.Weight = 1.4748265549001149D;
            // 
            // xrTable_接种医师签字
            // 
            this.xrTable_接种医师签字.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_接种医师签字.Name = "xrTable_接种医师签字";
            this.xrTable_接种医师签字.StylePriority.UseBorders = false;
            this.xrTable_接种医师签字.Weight = 0.2718740427070635D;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell231,
            this.xrTableCell232,
            this.xrTable_接种史名称2,
            this.xrTable_接种日期2,
            this.xrTable_接种机构2,
            this.xrTableCell238});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.StylePriority.UseBorders = false;
            this.xrTableCell231.Text = "防接";
            this.xrTableCell231.Weight = 0.15869037293521471D;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.StylePriority.UseBorders = false;
            this.xrTableCell232.Text = "2";
            this.xrTableCell232.Weight = 0.054811234414819943D;
            // 
            // xrTable_接种史名称2
            // 
            this.xrTable_接种史名称2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种史名称2.Name = "xrTable_接种史名称2";
            this.xrTable_接种史名称2.StylePriority.UseBorders = false;
            this.xrTable_接种史名称2.Weight = 0.52634498648656092D;
            // 
            // xrTable_接种日期2
            // 
            this.xrTable_接种日期2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种日期2.Name = "xrTable_接种日期2";
            this.xrTable_接种日期2.StylePriority.UseBorders = false;
            this.xrTable_接种日期2.Weight = 0.44998095147297529D;
            // 
            // xrTable_接种机构2
            // 
            this.xrTable_接种机构2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种机构2.Name = "xrTable_接种机构2";
            this.xrTable_接种机构2.StylePriority.UseBorders = false;
            this.xrTable_接种机构2.Weight = 1.4748265549001149D;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.StylePriority.UseBorders = false;
            this.xrTableCell238.Weight = 0.2718740427070635D;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell319,
            this.xrTableCell320,
            this.xrTable_接种史名称3,
            this.xrTable_接种日期3,
            this.xrTable_接种机构3,
            this.xrTableCell326});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1D;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.StylePriority.UseBorders = false;
            this.xrTableCell319.Text = "种史";
            this.xrTableCell319.Weight = 0.15869037293521471D;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.StylePriority.UseBorders = false;
            this.xrTableCell320.Text = "3";
            this.xrTableCell320.Weight = 0.054811234414819943D;
            // 
            // xrTable_接种史名称3
            // 
            this.xrTable_接种史名称3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种史名称3.Name = "xrTable_接种史名称3";
            this.xrTable_接种史名称3.StylePriority.UseBorders = false;
            this.xrTable_接种史名称3.Weight = 0.52634498648656092D;
            // 
            // xrTable_接种日期3
            // 
            this.xrTable_接种日期3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种日期3.Name = "xrTable_接种日期3";
            this.xrTable_接种日期3.StylePriority.UseBorders = false;
            this.xrTable_接种日期3.Weight = 0.44998095147297529D;
            // 
            // xrTable_接种机构3
            // 
            this.xrTable_接种机构3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_接种机构3.Name = "xrTable_接种机构3";
            this.xrTable_接种机构3.StylePriority.UseBorders = false;
            this.xrTable_接种机构3.Weight = 1.4748265549001149D;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.StylePriority.UseBorders = false;
            this.xrTableCell326.Weight = 0.2718740427070635D;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell329,
            this.xrTableCell332,
            this.xrTableCell333,
            this.xrTableCell334});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1D;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.StylePriority.UseBorders = false;
            this.xrTableCell329.Weight = 0.15869037293521471D;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.StylePriority.UseTextAlignment = false;
            this.xrTableCell332.Text = "  1  体检无异常";
            this.xrTableCell332.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell332.Weight = 2.1968891267475317D;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell333.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康评价});
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.StylePriority.UseBorders = false;
            this.xrTableCell333.Weight = 0.30907460052693964D;
            // 
            // xrLabel_健康评价
            // 
            this.xrLabel_健康评价.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_健康评价.LocationFloat = new DevExpress.Utils.PointFloat(51.86146F, 3.148244F);
            this.xrLabel_健康评价.Name = "xrLabel_健康评价";
            this.xrLabel_健康评价.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_健康评价.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_健康评价.StylePriority.UseBorders = false;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell334.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox健康评价});
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.StylePriority.UseBorders = false;
            this.xrTableCell334.Weight = 0.2718740427070635D;
            // 
            // xrPictureBox健康评价
            // 
            this.xrPictureBox健康评价.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox健康评价.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.144409E-05F);
            this.xrPictureBox健康评价.Name = "xrPictureBox健康评价";
            this.xrPictureBox健康评价.SizeF = new System.Drawing.SizeF(70F, 25F);
            this.xrPictureBox健康评价.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox健康评价.StylePriority.UseBorders = false;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell341,
            this.xrTableCell344,
            this.xrTableCell345,
            this.xrTableCell346});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 1D;
            // 
            // xrTableCell341
            // 
            this.xrTableCell341.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell341.Name = "xrTableCell341";
            this.xrTableCell341.StylePriority.UseBorders = false;
            this.xrTableCell341.Text = "健";
            this.xrTableCell341.Weight = 0.15869037293521471D;
            // 
            // xrTableCell344
            // 
            this.xrTableCell344.Name = "xrTableCell344";
            this.xrTableCell344.StylePriority.UseTextAlignment = false;
            this.xrTableCell344.Text = "  2  有异常";
            this.xrTableCell344.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell344.Weight = 2.1968891267475317D;
            // 
            // xrTableCell345
            // 
            this.xrTableCell345.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell345.Name = "xrTableCell345";
            this.xrTableCell345.StylePriority.UseBorders = false;
            this.xrTableCell345.Weight = 0.30907460052693964D;
            // 
            // xrTableCell346
            // 
            this.xrTableCell346.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell346.Name = "xrTableCell346";
            this.xrTableCell346.StylePriority.UseBorders = false;
            this.xrTableCell346.Weight = 0.2718740427070635D;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell347,
            this.xrTableCell349,
            this.xrTableCell350,
            this.xrTableCell351,
            this.xrTable_健康评价医师签字});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 1D;
            // 
            // xrTableCell347
            // 
            this.xrTableCell347.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell347.Name = "xrTableCell347";
            this.xrTableCell347.StylePriority.UseBorders = false;
            this.xrTableCell347.Text = "康";
            this.xrTableCell347.Weight = 0.15869037293521471D;
            // 
            // xrTableCell349
            // 
            this.xrTableCell349.Name = "xrTableCell349";
            this.xrTableCell349.StylePriority.UseTextAlignment = false;
            this.xrTableCell349.Text = "  异常1";
            this.xrTableCell349.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell349.Weight = 0.23867540854152658D;
            // 
            // xrTableCell350
            // 
            this.xrTableCell350.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康评价异常1});
            this.xrTableCell350.Name = "xrTableCell350";
            this.xrTableCell350.Weight = 1.9582137182060049D;
            // 
            // xrLabel_健康评价异常1
            // 
            this.xrLabel_健康评价异常1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_健康评价异常1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_健康评价异常1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.14823F);
            this.xrLabel_健康评价异常1.Name = "xrLabel_健康评价异常1";
            this.xrLabel_健康评价异常1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_健康评价异常1.SizeF = new System.Drawing.SizeF(495.5366F, 19.66653F);
            this.xrLabel_健康评价异常1.StylePriority.UseBorders = false;
            this.xrLabel_健康评价异常1.StylePriority.UseFont = false;
            this.xrLabel_健康评价异常1.StylePriority.UseTextAlignment = false;
            this.xrLabel_健康评价异常1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell351
            // 
            this.xrTableCell351.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell351.Name = "xrTableCell351";
            this.xrTableCell351.StylePriority.UseBorders = false;
            this.xrTableCell351.Weight = 0.30907460052693964D;
            // 
            // xrTable_健康评价医师签字
            // 
            this.xrTable_健康评价医师签字.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_健康评价医师签字.Name = "xrTable_健康评价医师签字";
            this.xrTable_健康评价医师签字.StylePriority.UseBorders = false;
            this.xrTable_健康评价医师签字.Weight = 0.2718740427070635D;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell335,
            this.xrTableCell337,
            this.xrTableCell338,
            this.xrTableCell339,
            this.xrTableCell340});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1D;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.StylePriority.UseBorders = false;
            this.xrTableCell335.Text = "评";
            this.xrTableCell335.Weight = 0.15869037293521471D;
            // 
            // xrTableCell337
            // 
            this.xrTableCell337.Name = "xrTableCell337";
            this.xrTableCell337.StylePriority.UseTextAlignment = false;
            this.xrTableCell337.Text = "  异常2";
            this.xrTableCell337.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell337.Weight = 0.23867540854152658D;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康评价异常2});
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.Weight = 1.9582137182060049D;
            // 
            // xrLabel_健康评价异常2
            // 
            this.xrLabel_健康评价异常2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_健康评价异常2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_健康评价异常2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_健康评价异常2.Name = "xrLabel_健康评价异常2";
            this.xrLabel_健康评价异常2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_健康评价异常2.SizeF = new System.Drawing.SizeF(495.5366F, 19.66659F);
            this.xrLabel_健康评价异常2.StylePriority.UseBorders = false;
            this.xrLabel_健康评价异常2.StylePriority.UseFont = false;
            this.xrLabel_健康评价异常2.StylePriority.UseTextAlignment = false;
            this.xrLabel_健康评价异常2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.StylePriority.UseBorders = false;
            this.xrTableCell339.Weight = 0.30907460052693964D;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.StylePriority.UseBorders = false;
            this.xrTableCell340.Weight = 0.2718740427070635D;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell259,
            this.xrTableCell323,
            this.xrTableCell324,
            this.xrTableCell327,
            this.xrTableCell328});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 1D;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.StylePriority.UseBorders = false;
            this.xrTableCell259.Text = "价";
            this.xrTableCell259.Weight = 0.15869037293521471D;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.StylePriority.UseTextAlignment = false;
            this.xrTableCell323.Text = "  异常3";
            this.xrTableCell323.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell323.Weight = 0.23867540854152658D;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康评价异常3});
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.Weight = 1.9582137182060049D;
            // 
            // xrLabel_健康评价异常3
            // 
            this.xrLabel_健康评价异常3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_健康评价异常3.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_健康评价异常3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_健康评价异常3.Name = "xrLabel_健康评价异常3";
            this.xrLabel_健康评价异常3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_健康评价异常3.SizeF = new System.Drawing.SizeF(495.5366F, 19.66659F);
            this.xrLabel_健康评价异常3.StylePriority.UseBorders = false;
            this.xrLabel_健康评价异常3.StylePriority.UseFont = false;
            this.xrLabel_健康评价异常3.StylePriority.UseTextAlignment = false;
            this.xrLabel_健康评价异常3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.StylePriority.UseBorders = false;
            this.xrTableCell327.Weight = 0.30907460052693964D;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.StylePriority.UseBorders = false;
            this.xrTableCell328.Weight = 0.2718740427070635D;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell235,
            this.xrTableCell243,
            this.xrTableCell244,
            this.xrTableCell251,
            this.xrTableCell252});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1D;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.StylePriority.UseBorders = false;
            this.xrTableCell235.Weight = 0.15869037293521471D;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.StylePriority.UseBorders = false;
            this.xrTableCell243.StylePriority.UseTextAlignment = false;
            this.xrTableCell243.Text = "  异常4";
            this.xrTableCell243.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell243.Weight = 0.23867540854152658D;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell244.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康评价异常4});
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.StylePriority.UseBorders = false;
            this.xrTableCell244.Weight = 1.9582137182060049D;
            // 
            // xrLabel_健康评价异常4
            // 
            this.xrLabel_健康评价异常4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_健康评价异常4.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_健康评价异常4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_健康评价异常4.Name = "xrLabel_健康评价异常4";
            this.xrLabel_健康评价异常4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_健康评价异常4.SizeF = new System.Drawing.SizeF(495.5366F, 19.66659F);
            this.xrLabel_健康评价异常4.StylePriority.UseBorders = false;
            this.xrLabel_健康评价异常4.StylePriority.UseFont = false;
            this.xrLabel_健康评价异常4.StylePriority.UseTextAlignment = false;
            this.xrLabel_健康评价异常4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableCell251
            // 
            this.xrTableCell251.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell251.Name = "xrTableCell251";
            this.xrTableCell251.StylePriority.UseBorders = false;
            this.xrTableCell251.Weight = 0.30907460052693964D;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.StylePriority.UseBorders = false;
            this.xrTableCell252.Weight = 0.2718740427070635D;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell342,
            this.xrTableCell348,
            this.xrTableCell366,
            this.xrTableCell353,
            this.xrTableCell354});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 1D;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.StylePriority.UseBorders = false;
            this.xrTableCell342.Weight = 0.15869037293521471D;
            // 
            // xrTableCell348
            // 
            this.xrTableCell348.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell348.Name = "xrTableCell348";
            this.xrTableCell348.StylePriority.UseBorders = false;
            this.xrTableCell348.StylePriority.UseTextAlignment = false;
            this.xrTableCell348.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell348.Weight = 1.1068780945547609D;
            // 
            // xrTableCell366
            // 
            this.xrTableCell366.Name = "xrTableCell366";
            this.xrTableCell366.Text = "  危险因素控制：";
            this.xrTableCell366.Weight = 0.45256147612980274D;
            // 
            // xrTableCell353
            // 
            this.xrTableCell353.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell353.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel78,
            this.xrLabel_危险控制因素7,
            this.xrLabel_危险控制因素1,
            this.xrLabel75,
            this.xrLabel_危险控制因素2,
            this.xrLabel73,
            this.xrLabel_危险控制因素3,
            this.xrLabel71,
            this.xrLabel_危险控制因素4,
            this.xrLabel69,
            this.xrLabel_危险控制因素5,
            this.xrLabel67,
            this.xrLabel_危险控制因素6});
            this.xrTableCell353.Name = "xrTableCell353";
            this.xrTableCell353.StylePriority.UseBorders = false;
            this.xrTableCell353.Weight = 0.94652415658990741D;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel78.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(196.6625F, 3.149592F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(16.50911F, 18.83338F);
            this.xrLabel78.StylePriority.UseBorders = false;
            this.xrLabel78.StylePriority.UseFont = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "/";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素7
            // 
            this.xrLabel_危险控制因素7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素7.LocationFloat = new DevExpress.Utils.PointFloat(213.1716F, 3.149516F);
            this.xrLabel_危险控制因素7.Name = "xrLabel_危险控制因素7";
            this.xrLabel_危险控制因素7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_危险控制因素7.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_危险控制因素7.StylePriority.UseBorders = false;
            // 
            // xrLabel_危险控制因素1
            // 
            this.xrLabel_危险控制因素1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素1.LocationFloat = new DevExpress.Utils.PointFloat(14.61668F, 3.14949F);
            this.xrLabel_危险控制因素1.Name = "xrLabel_危险控制因素1";
            this.xrLabel_危险控制因素1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_危险控制因素1.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_危险控制因素1.StylePriority.UseBorders = false;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel75.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(31.12578F, 3.149592F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel75.StylePriority.UseBorders = false;
            this.xrLabel75.StylePriority.UseFont = false;
            this.xrLabel75.StylePriority.UseTextAlignment = false;
            this.xrLabel75.Text = "/";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素2
            // 
            this.xrLabel_危险控制因素2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素2.LocationFloat = new DevExpress.Utils.PointFloat(47.63488F, 3.14949F);
            this.xrLabel_危险控制因素2.Name = "xrLabel_危险控制因素2";
            this.xrLabel_危险控制因素2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_危险控制因素2.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel_危险控制因素2.StylePriority.UseBorders = false;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel73.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(64.14398F, 3.149592F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel73.StylePriority.UseBorders = false;
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "/";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素3
            // 
            this.xrLabel_危险控制因素3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素3.LocationFloat = new DevExpress.Utils.PointFloat(80.65307F, 3.149592F);
            this.xrLabel_危险控制因素3.Name = "xrLabel_危险控制因素3";
            this.xrLabel_危险控制因素3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_危险控制因素3.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_危险控制因素3.StylePriority.UseBorders = false;
            // 
            // xrLabel71
            // 
            this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel71.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(97.16216F, 3.149592F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(16.5091F, 18.83338F);
            this.xrLabel71.StylePriority.UseBorders = false;
            this.xrLabel71.StylePriority.UseFont = false;
            this.xrLabel71.StylePriority.UseTextAlignment = false;
            this.xrLabel71.Text = "/";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素4
            // 
            this.xrLabel_危险控制因素4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素4.LocationFloat = new DevExpress.Utils.PointFloat(113.6713F, 3.149592F);
            this.xrLabel_危险控制因素4.Name = "xrLabel_危险控制因素4";
            this.xrLabel_危险控制因素4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_危险控制因素4.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_危险控制因素4.StylePriority.UseBorders = false;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel69.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(130.6261F, 3.149592F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel69.StylePriority.UseBorders = false;
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.Text = "/";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素5
            // 
            this.xrLabel_危险控制因素5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素5.LocationFloat = new DevExpress.Utils.PointFloat(147.1352F, 3.149516F);
            this.xrLabel_危险控制因素5.Name = "xrLabel_危险控制因素5";
            this.xrLabel_危险控制因素5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_危险控制因素5.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_危险控制因素5.StylePriority.UseBorders = false;
            // 
            // xrLabel67
            // 
            this.xrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel67.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(163.6443F, 3.149566F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel67.StylePriority.UseBorders = false;
            this.xrLabel67.StylePriority.UseFont = false;
            this.xrLabel67.StylePriority.UseTextAlignment = false;
            this.xrLabel67.Text = "/";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_危险控制因素6
            // 
            this.xrLabel_危险控制因素6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_危险控制因素6.LocationFloat = new DevExpress.Utils.PointFloat(180.1534F, 3.149606F);
            this.xrLabel_危险控制因素6.Name = "xrLabel_危险控制因素6";
            this.xrLabel_危险控制因素6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_危险控制因素6.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_危险控制因素6.StylePriority.UseBorders = false;
            // 
            // xrTableCell354
            // 
            this.xrTableCell354.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell354.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox健康指导});
            this.xrTableCell354.Name = "xrTableCell354";
            this.xrTableCell354.StylePriority.UseBorders = false;
            this.xrTableCell354.Weight = 0.2718740427070635D;
            // 
            // xrPictureBox健康指导
            // 
            this.xrPictureBox健康指导.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox健康指导.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox健康指导.Name = "xrPictureBox健康指导";
            this.xrPictureBox健康指导.SizeF = new System.Drawing.SizeF(70F, 25F);
            this.xrPictureBox健康指导.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox健康指导.StylePriority.UseBorders = false;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell365,
            this.xrTableCell367,
            this.xrTableCell368,
            this.xrTableCell369});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 1D;
            // 
            // xrTableCell365
            // 
            this.xrTableCell365.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell365.Name = "xrTableCell365";
            this.xrTableCell365.StylePriority.UseBorders = false;
            this.xrTableCell365.Text = "健";
            this.xrTableCell365.Weight = 0.15869037293521471D;
            // 
            // xrTableCell367
            // 
            this.xrTableCell367.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell367.Name = "xrTableCell367";
            this.xrTableCell367.StylePriority.UseBorders = false;
            this.xrTableCell367.StylePriority.UseTextAlignment = false;
            this.xrTableCell367.Text = "    1  纳入慢性病患者健康管理";
            this.xrTableCell367.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell367.Weight = 1.1068780945547609D;
            // 
            // xrTableCell368
            // 
            this.xrTableCell368.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell368.Name = "xrTableCell368";
            this.xrTableCell368.StylePriority.UseBorders = false;
            this.xrTableCell368.StylePriority.UseTextAlignment = false;
            this.xrTableCell368.Text = "    1戒烟       2健康饮酒     3饮食      4锻炼";
            this.xrTableCell368.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell368.Weight = 1.3990856327197099D;
            // 
            // xrTableCell369
            // 
            this.xrTableCell369.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell369.Name = "xrTableCell369";
            this.xrTableCell369.StylePriority.UseBorders = false;
            this.xrTableCell369.Weight = 0.2718740427070635D;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell360,
            this.xrTableCell362,
            this.xrTableCell356,
            this.xrTableCell361,
            this.xrTableCell363,
            this.xrTable_健康指导医师签字});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 1D;
            // 
            // xrTableCell360
            // 
            this.xrTableCell360.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell360.Name = "xrTableCell360";
            this.xrTableCell360.StylePriority.UseBorders = false;
            this.xrTableCell360.Text = "康";
            this.xrTableCell360.Weight = 0.15869037293521471D;
            // 
            // xrTableCell362
            // 
            this.xrTableCell362.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell362.Name = "xrTableCell362";
            this.xrTableCell362.StylePriority.UseBorders = false;
            this.xrTableCell362.StylePriority.UseTextAlignment = false;
            this.xrTableCell362.Text = "    2  建议复查";
            this.xrTableCell362.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell362.Weight = 1.1068780945547609D;
            // 
            // xrTableCell356
            // 
            this.xrTableCell356.Name = "xrTableCell356";
            this.xrTableCell356.StylePriority.UseTextAlignment = false;
            this.xrTableCell356.Text = "    5减体重（目标";
            this.xrTableCell356.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell356.Weight = 0.47231990738199747D;
            // 
            // xrTableCell361
            // 
            this.xrTableCell361.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_减重目标});
            this.xrTableCell361.Name = "xrTableCell361";
            this.xrTableCell361.Weight = 0.49643689990255435D;
            // 
            // xrLabel_减重目标
            // 
            this.xrLabel_减重目标.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_减重目标.LocationFloat = new DevExpress.Utils.PointFloat(1.20148E-05F, 0F);
            this.xrLabel_减重目标.Name = "xrLabel_减重目标";
            this.xrLabel_减重目标.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_减重目标.SizeF = new System.Drawing.SizeF(125.1804F, 19.66659F);
            this.xrLabel_减重目标.StylePriority.UseBorders = false;
            // 
            // xrTableCell363
            // 
            this.xrTableCell363.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell363.Name = "xrTableCell363";
            this.xrTableCell363.StylePriority.UseBorders = false;
            this.xrTableCell363.StylePriority.UseTextAlignment = false;
            this.xrTableCell363.Text = "  kg）";
            this.xrTableCell363.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell363.Weight = 0.43032882543515816D;
            // 
            // xrTable_健康指导医师签字
            // 
            this.xrTable_健康指导医师签字.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTable_健康指导医师签字.Name = "xrTable_健康指导医师签字";
            this.xrTable_健康指导医师签字.StylePriority.UseBorders = false;
            this.xrTable_健康指导医师签字.Weight = 0.2718740427070635D;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell355,
            this.xrTableCell357,
            this.xrTableCell343,
            this.xrTableCell358,
            this.xrTableCell359});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 1D;
            // 
            // xrTableCell355
            // 
            this.xrTableCell355.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell355.Name = "xrTableCell355";
            this.xrTableCell355.StylePriority.UseBorders = false;
            this.xrTableCell355.Text = "指";
            this.xrTableCell355.Weight = 0.15869037293521471D;
            // 
            // xrTableCell357
            // 
            this.xrTableCell357.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell357.Name = "xrTableCell357";
            this.xrTableCell357.StylePriority.UseBorders = false;
            this.xrTableCell357.StylePriority.UseTextAlignment = false;
            this.xrTableCell357.Text = "    3  建议转诊";
            this.xrTableCell357.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell357.Weight = 1.1068780945547609D;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.StylePriority.UseTextAlignment = false;
            this.xrTableCell343.Text = "    6建议接种疫苗";
            this.xrTableCell343.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell343.Weight = 0.47231990738199747D;
            // 
            // xrTableCell358
            // 
            this.xrTableCell358.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell358.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_建议接种疫苗});
            this.xrTableCell358.Name = "xrTableCell358";
            this.xrTableCell358.StylePriority.UseBorders = false;
            this.xrTableCell358.Weight = 0.92676572533771251D;
            // 
            // xrLabel_建议接种疫苗
            // 
            this.xrLabel_建议接种疫苗.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_建议接种疫苗.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_建议接种疫苗.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_建议接种疫苗.Name = "xrLabel_建议接种疫苗";
            this.xrLabel_建议接种疫苗.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_建议接种疫苗.SizeF = new System.Drawing.SizeF(224.6807F, 19.66659F);
            this.xrLabel_建议接种疫苗.StylePriority.UseBorders = false;
            this.xrLabel_建议接种疫苗.StylePriority.UseFont = false;
            this.xrLabel_建议接种疫苗.StylePriority.UseTextAlignment = false;
            this.xrLabel_建议接种疫苗.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell359
            // 
            this.xrTableCell359.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell359.Name = "xrTableCell359";
            this.xrTableCell359.StylePriority.UseBorders = false;
            this.xrTableCell359.Weight = 0.2718740427070635D;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell236,
            this.xrTableCell330,
            this.xrTableCell260,
            this.xrTableCell331,
            this.xrTableCell336});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 1D;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.StylePriority.UseBorders = false;
            this.xrTableCell236.Text = "导";
            this.xrTableCell236.Weight = 0.15869037293521471D;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.StylePriority.UseBorders = false;
            this.xrTableCell330.StylePriority.UseTextAlignment = false;
            this.xrTableCell330.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell330.Weight = 1.1068780945547609D;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.StylePriority.UseBorders = false;
            this.xrTableCell260.StylePriority.UseTextAlignment = false;
            this.xrTableCell260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell260.Weight = 0.47231985085896477D;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell331.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_建议接种疫苗补充});
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.StylePriority.UseBorders = false;
            this.xrTableCell331.Weight = 0.9267657818607451D;
            // 
            // xrLabel_建议接种疫苗补充
            // 
            this.xrLabel_建议接种疫苗补充.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_建议接种疫苗补充.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_建议接种疫苗补充.LocationFloat = new DevExpress.Utils.PointFloat(4.805917E-05F, 0.249331F);
            this.xrLabel_建议接种疫苗补充.Name = "xrLabel_建议接种疫苗补充";
            this.xrLabel_建议接种疫苗补充.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_建议接种疫苗补充.SizeF = new System.Drawing.SizeF(224.6807F, 19.66659F);
            this.xrLabel_建议接种疫苗补充.StylePriority.UseBorders = false;
            this.xrLabel_建议接种疫苗补充.StylePriority.UseFont = false;
            this.xrLabel_建议接种疫苗补充.StylePriority.UseTextAlignment = false;
            this.xrLabel_建议接种疫苗补充.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.StylePriority.UseBorders = false;
            this.xrTableCell336.Weight = 0.2718740427070635D;
            // 
            // xrTableRow70
            // 
            this.xrTableRow70.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell233,
            this.xrTableCell234,
            this.xrTableCell237,
            this.xrTableCell241,
            this.xrTableCell242});
            this.xrTableRow70.Name = "xrTableRow70";
            this.xrTableRow70.Weight = 1D;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.StylePriority.UseBorders = false;
            this.xrTableCell233.Weight = 0.15869037293521471D;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell234.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_健康指导3,
            this.xrLabel34,
            this.xrLabel_健康指导2,
            this.xrLabel35,
            this.xrLabel_健康指导1});
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.StylePriority.UseBorders = false;
            this.xrTableCell234.Weight = 1.1068780945547609D;
            // 
            // xrLabel_健康指导3
            // 
            this.xrLabel_健康指导3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_健康指导3.LocationFloat = new DevExpress.Utils.PointFloat(253.75F, 3.149606F);
            this.xrLabel_健康指导3.Name = "xrLabel_健康指导3";
            this.xrLabel_健康指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_健康指导3.SizeF = new System.Drawing.SizeF(16.50908F, 18.83338F);
            this.xrLabel_健康指导3.StylePriority.UseBorders = false;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(237.2409F, 3.149618F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(16.50911F, 18.83338F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "/";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_健康指导2
            // 
            this.xrLabel_健康指导2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_健康指导2.LocationFloat = new DevExpress.Utils.PointFloat(220.7318F, 3.149618F);
            this.xrLabel_健康指导2.Name = "xrLabel_健康指导2";
            this.xrLabel_健康指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_健康指导2.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_健康指导2.StylePriority.UseBorders = false;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(204.2227F, 3.149618F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "/";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel_健康指导1
            // 
            this.xrLabel_健康指导1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_健康指导1.LocationFloat = new DevExpress.Utils.PointFloat(187.7136F, 3.149592F);
            this.xrLabel_健康指导1.Name = "xrLabel_健康指导1";
            this.xrLabel_健康指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_健康指导1.SizeF = new System.Drawing.SizeF(16.50909F, 18.83338F);
            this.xrLabel_健康指导1.StylePriority.UseBorders = false;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.StylePriority.UseBorders = false;
            this.xrTableCell237.StylePriority.UseTextAlignment = false;
            this.xrTableCell237.Text = "    7其他";
            this.xrTableCell237.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell237.Weight = 0.24509709336190172D;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell241.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_危险控制因素其他});
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.StylePriority.UseBorders = false;
            this.xrTableCell241.Weight = 1.1539885393578082D;
            // 
            // xrLabel_危险控制因素其他
            // 
            this.xrLabel_危险控制因素其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_危险控制因素其他.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_危险控制因素其他.LocationFloat = new DevExpress.Utils.PointFloat(0.0001525879F, 0F);
            this.xrLabel_危险控制因素其他.Name = "xrLabel_危险控制因素其他";
            this.xrLabel_危险控制因素其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_危险控制因素其他.SizeF = new System.Drawing.SizeF(282.1805F, 19.66659F);
            this.xrLabel_危险控制因素其他.StylePriority.UseBorders = false;
            this.xrLabel_危险控制因素其他.StylePriority.UseFont = false;
            this.xrLabel_危险控制因素其他.StylePriority.UseTextAlignment = false;
            this.xrLabel_危险控制因素其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.StylePriority.UseBorders = false;
            this.xrTableCell242.Weight = 0.2718740427070635D;
            // 
            // xrTableRow71
            // 
            this.xrTableRow71.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell245,
            this.xrTableCell246,
            this.xrTableCell265,
            this.xrTableCell266,
            this.xrTableCell267});
            this.xrTableRow71.Name = "xrTableRow71";
            this.xrTableRow71.Weight = 1D;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.StylePriority.UseBorders = false;
            this.xrTableCell245.Weight = 0.15869037293521471D;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.StylePriority.UseBorders = false;
            this.xrTableCell246.Weight = 1.1068780945547609D;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.StylePriority.UseBorders = false;
            this.xrTableCell265.Weight = 0.24509709336190172D;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell266.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_危险控制因素其他补充});
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.StylePriority.UseBorders = false;
            this.xrTableCell266.Weight = 1.1539885393578082D;
            // 
            // xrLabel_危险控制因素其他补充
            // 
            this.xrLabel_危险控制因素其他补充.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_危险控制因素其他补充.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_危险控制因素其他补充.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel_危险控制因素其他补充.Name = "xrLabel_危险控制因素其他补充";
            this.xrLabel_危险控制因素其他补充.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_危险控制因素其他补充.SizeF = new System.Drawing.SizeF(282.1805F, 19.66659F);
            this.xrLabel_危险控制因素其他补充.StylePriority.UseBorders = false;
            this.xrLabel_危险控制因素其他补充.StylePriority.UseFont = false;
            this.xrLabel_危险控制因素其他补充.StylePriority.UseTextAlignment = false;
            this.xrLabel_危险控制因素其他补充.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.StylePriority.UseBorders = false;
            this.xrTableCell267.Weight = 0.2718740427070635D;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell370,
            this.xrTableCell373});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 1D;
            // 
            // xrTableCell370
            // 
            this.xrTableCell370.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell370.Name = "xrTableCell370";
            this.xrTableCell370.StylePriority.UseBorders = false;
            this.xrTableCell370.Weight = 0.15869037293521471D;
            // 
            // xrTableCell373
            // 
            this.xrTableCell373.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell373.Name = "xrTableCell373";
            this.xrTableCell373.StylePriority.UseBorders = false;
            this.xrTableCell373.Weight = 2.7778377699815344D;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell395,
            this.xrTableCell398});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 1D;
            // 
            // xrTableCell395
            // 
            this.xrTableCell395.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell395.Name = "xrTableCell395";
            this.xrTableCell395.StylePriority.UseBorders = false;
            this.xrTableCell395.Weight = 0.15869037293521471D;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.StylePriority.UseBorders = false;
            this.xrTableCell398.StylePriority.UseTextAlignment = false;
            this.xrTableCell398.Text = "      以上体检结果、健康评价已面对面反馈本人（家属），并已进行健康指导。";
            this.xrTableCell398.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell398.Weight = 2.7778377699815344D;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell405,
            this.xrTableCell371,
            this.xrTableCell372,
            this.xrTableCell406,
            this.xrTableCell407,
            this.xrTableCell408,
            this.xrTableCell409});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 1D;
            // 
            // xrTableCell405
            // 
            this.xrTableCell405.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell405.Multiline = true;
            this.xrTableCell405.Name = "xrTableCell405";
            this.xrTableCell405.StylePriority.UseBorders = false;
            this.xrTableCell405.StylePriority.UseFont = false;
            this.xrTableCell405.Text = "果";
            this.xrTableCell405.Weight = 0.15869037293521471D;
            // 
            // xrTableCell371
            // 
            this.xrTableCell371.Name = "xrTableCell371";
            this.xrTableCell371.Weight = 1.1457729362402309D;
            // 
            // xrTableCell372
            // 
            this.xrTableCell372.Name = "xrTableCell372";
            this.xrTableCell372.Text = "签字：";
            this.xrTableCell372.Weight = 0.24509676100973532D;
            // 
            // xrTableCell406
            // 
            this.xrTableCell406.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox本人签字});
            this.xrTableCell406.Name = "xrTableCell406";
            this.xrTableCell406.Weight = 0.42204755902527008D;
            // 
            // xrTableCell407
            // 
            this.xrTableCell407.Name = "xrTableCell407";
            this.xrTableCell407.Text = "（本人）/";
            this.xrTableCell407.Weight = 0.28843980409074227D;
            // 
            // xrTableCell408
            // 
            this.xrTableCell408.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox家属签字});
            this.xrTableCell408.Name = "xrTableCell408";
            this.xrTableCell408.Weight = 0.40460666690849245D;
            // 
            // xrTableCell409
            // 
            this.xrTableCell409.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell409.Name = "xrTableCell409";
            this.xrTableCell409.StylePriority.UseBorders = false;
            this.xrTableCell409.Text = "（家属）";
            this.xrTableCell409.Weight = 0.2718740427070635D;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell385,
            this.xrTableCell386,
            this.xrTableCell387,
            this.xrTableCell389});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.Weight = 1D;
            // 
            // xrTableCell385
            // 
            this.xrTableCell385.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell385.Multiline = true;
            this.xrTableCell385.Name = "xrTableCell385";
            this.xrTableCell385.StylePriority.UseBorders = false;
            this.xrTableCell385.Text = "馈";
            this.xrTableCell385.Weight = 0.15869037293521471D;
            // 
            // xrTableCell386
            // 
            this.xrTableCell386.Name = "xrTableCell386";
            this.xrTableCell386.Weight = 1.3908699821232515D;
            // 
            // xrTableCell387
            // 
            this.xrTableCell387.Name = "xrTableCell387";
            this.xrTableCell387.StylePriority.UseTextAlignment = false;
            this.xrTableCell387.Text = "反馈人签字：";
            this.xrTableCell387.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell387.Weight = 0.42204736910974633D;
            // 
            // xrTableCell389
            // 
            this.xrTableCell389.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell389.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPictureBox反馈人签字});
            this.xrTableCell389.Name = "xrTableCell389";
            this.xrTableCell389.StylePriority.UseBorders = false;
            this.xrTableCell389.Weight = 0.96492041874853651D;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell375,
            this.xrTableCell383,
            this.xrTableCell381,
            this.xrTableCell374,
            this.xrTableCell376,
            this.xrTableCell377,
            this.xrTableCell378,
            this.xrTableCell379});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 1D;
            // 
            // xrTableCell375
            // 
            this.xrTableCell375.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell375.Name = "xrTableCell375";
            this.xrTableCell375.StylePriority.UseBorders = false;
            this.xrTableCell375.Weight = 0.15869037293521471D;
            // 
            // xrTableCell383
            // 
            this.xrTableCell383.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell383.Name = "xrTableCell383";
            this.xrTableCell383.StylePriority.UseBorders = false;
            this.xrTableCell383.StylePriority.UseTextAlignment = false;
            this.xrTableCell383.Text = "反馈时间：";
            this.xrTableCell383.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell383.Weight = 1.8413303397265863D;
            // 
            // xrTableCell381
            // 
            this.xrTableCell381.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell381.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_反馈时间年});
            this.xrTableCell381.Name = "xrTableCell381";
            this.xrTableCell381.StylePriority.UseBorders = false;
            this.xrTableCell381.Weight = 0.2343048707254527D;
            // 
            // xrLabel_反馈时间年
            // 
            this.xrLabel_反馈时间年.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_反馈时间年.LocationFloat = new DevExpress.Utils.PointFloat(6.007398E-05F, 0F);
            this.xrLabel_反馈时间年.Name = "xrLabel_反馈时间年";
            this.xrLabel_反馈时间年.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_反馈时间年.SizeF = new System.Drawing.SizeF(58.84626F, 19.66659F);
            this.xrLabel_反馈时间年.StylePriority.UseBorders = false;
            // 
            // xrTableCell374
            // 
            this.xrTableCell374.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell374.Name = "xrTableCell374";
            this.xrTableCell374.StylePriority.UseBorders = false;
            this.xrTableCell374.Text = "年";
            this.xrTableCell374.Weight = 0.0980013717598276D;
            // 
            // xrTableCell376
            // 
            this.xrTableCell376.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell376.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_反馈时间月});
            this.xrTableCell376.Name = "xrTableCell376";
            this.xrTableCell376.StylePriority.UseBorders = false;
            this.xrTableCell376.Weight = 0.19778586037144808D;
            // 
            // xrLabel_反馈时间月
            // 
            this.xrLabel_反馈时间月.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_反馈时间月.LocationFloat = new DevExpress.Utils.PointFloat(6.007398E-05F, 0F);
            this.xrLabel_反馈时间月.Name = "xrLabel_反馈时间月";
            this.xrLabel_反馈时间月.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_反馈时间月.SizeF = new System.Drawing.SizeF(50.05075F, 19.66659F);
            this.xrLabel_反馈时间月.StylePriority.UseBorders = false;
            // 
            // xrTableCell377
            // 
            this.xrTableCell377.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell377.Name = "xrTableCell377";
            this.xrTableCell377.StylePriority.UseBorders = false;
            this.xrTableCell377.Text = "月";
            this.xrTableCell377.Weight = 0.095646965273376683D;
            // 
            // xrTableCell378
            // 
            this.xrTableCell378.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell378.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel_反馈时间日});
            this.xrTableCell378.Name = "xrTableCell378";
            this.xrTableCell378.StylePriority.UseBorders = false;
            this.xrTableCell378.Weight = 0.18049704311025416D;
            // 
            // xrLabel_反馈时间日
            // 
            this.xrLabel_反馈时间日.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_反馈时间日.LocationFloat = new DevExpress.Utils.PointFloat(1.20148E-05F, 0F);
            this.xrLabel_反馈时间日.Name = "xrLabel_反馈时间日";
            this.xrLabel_反馈时间日.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_反馈时间日.SizeF = new System.Drawing.SizeF(45.67579F, 19.66659F);
            this.xrLabel_反馈时间日.StylePriority.UseBorders = false;
            // 
            // xrTableCell379
            // 
            this.xrTableCell379.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell379.Name = "xrTableCell379";
            this.xrTableCell379.StylePriority.UseBorders = false;
            this.xrTableCell379.StylePriority.UseTextAlignment = false;
            this.xrTableCell379.Text = " 日";
            this.xrTableCell379.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrTableCell379.Weight = 0.13027131901458916D;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 12F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableCell382
            // 
            this.xrTableCell382.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell382.Name = "xrTableCell382";
            this.xrTableCell382.StylePriority.UseBorders = false;
            this.xrTableCell382.Weight = 2.7778377699815344D;
            // 
            // xrTableCell380
            // 
            this.xrTableCell380.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell380.Name = "xrTableCell380";
            this.xrTableCell380.StylePriority.UseBorders = false;
            this.xrTableCell380.Weight = 0.15869037293521471D;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell380,
            this.xrTableCell382});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 1D;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.StylePriority.UseBorders = false;
            this.xrTableCell392.Weight = 2.7778377699815344D;
            // 
            // xrTableCell390
            // 
            this.xrTableCell390.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell390.Name = "xrTableCell390";
            this.xrTableCell390.StylePriority.UseBorders = false;
            this.xrTableCell390.Text = "反";
            this.xrTableCell390.Weight = 0.15869037293521471D;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell390,
            this.xrTableCell392});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 1D;
            // 
            // xrTableCell404
            // 
            this.xrTableCell404.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell404.Name = "xrTableCell404";
            this.xrTableCell404.StylePriority.UseBorders = false;
            this.xrTableCell404.Weight = 2.7778377699815344D;
            // 
            // xrTableCell400
            // 
            this.xrTableCell400.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrTableCell400.Name = "xrTableCell400";
            this.xrTableCell400.StylePriority.UseBorders = false;
            this.xrTableCell400.Text = "结";
            this.xrTableCell400.Weight = 0.15869037293521471D;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell400,
            this.xrTableCell404});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 1D;
            // 
            // xrPictureBox本人签字
            // 
            this.xrPictureBox本人签字.LocationFloat = new DevExpress.Utils.PointFloat(6.103516E-05F, 0F);
            this.xrPictureBox本人签字.Name = "xrPictureBox本人签字";
            this.xrPictureBox本人签字.SizeF = new System.Drawing.SizeF(100F, 25.00006F);
            this.xrPictureBox本人签字.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrPictureBox家属签字
            // 
            this.xrPictureBox家属签字.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox家属签字.Name = "xrPictureBox家属签字";
            this.xrPictureBox家属签字.SizeF = new System.Drawing.SizeF(100F, 25.00006F);
            this.xrPictureBox家属签字.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrPictureBox反馈人签字
            // 
            this.xrPictureBox反馈人签字.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPictureBox反馈人签字.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox反馈人签字.Name = "xrPictureBox反馈人签字";
            this.xrPictureBox反馈人签字.SizeF = new System.Drawing.SizeF(100F, 25.00006F);
            this.xrPictureBox反馈人签字.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            this.xrPictureBox反馈人签字.StylePriority.UseBorders = false;
            // 
            // report健康体检表A4_4_2017
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(11, 10, 0, 12);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell230;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell307;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药医师签字;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间4;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称5;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法5;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量5;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间5;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物名称6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用法6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_药物用量6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_用药时间6;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_服药依从性6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种史名称1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种日期1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种机构1;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种医师签字;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种史名称2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种日期2;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种机构2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种史名称3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种日期3;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_接种机构3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康评价;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell341;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell344;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell345;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell346;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell347;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell349;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell350;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康评价异常1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell351;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_健康评价医师签字;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell337;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康评价异常2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康评价异常3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康评价异常4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell251;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell348;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell366;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell353;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell354;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell365;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell367;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell368;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell369;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell362;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell356;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell361;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_减重目标;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell363;
        private DevExpress.XtraReports.UI.XRTableCell xrTable_健康指导医师签字;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell355;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell357;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell358;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_建议接种疫苗;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell359;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_建议接种疫苗补充;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康指导3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康指导2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_健康指导1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_危险控制因素其他补充;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell370;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell373;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell395;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell405;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell371;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell372;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell406;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell409;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell386;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell387;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell389;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell375;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell383;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell381;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_反馈时间年;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell374;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell376;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_反馈时间月;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell377;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell378;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_反馈时间日;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell379;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox主要用药情况;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox健康评价;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox健康指导;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell400;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell382;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox本人签字;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox家属签字;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox反馈人签字;
    }
}
