﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;

namespace AtomEHR.公共卫生.Module.个人健康.体检就诊信息
{
    public partial class report健康体检表A4_3_2017 : DevExpress.XtraReports.UI.XtraReport
    {
        //Begin WXF 2018-11-03 | 19:00
        //健康体检表A4版界面与代码均为复制A3版稍作修改

        //End

        DataSet ds体检;
        bll健康体检 bll = new bll健康体检();
        public report健康体检表A4_3_2017()
        {
            InitializeComponent();
        }

        public report健康体检表A4_3_2017(string docNo, string date, DataSet _dsTJ = null)
        {
            InitializeComponent();
            if (_dsTJ == null)
            {
                ds体检 = bll.GetReportDataByKey(docNo, date);
            }
            else
            {
                ds体检 = _dsTJ;
            }

            BindData(ds体检);
            try
            {
                bll医生信息 bll手签 = new bll医生信息();
                //string RGID = ds体检.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构].ToString();
                DataTable dt = bll手签.GetSummaryData(false);
                if (dt.Select("c查体模块='辅助检查' ").Length > 0)
                {
                    System.Drawing.Image img辅助检查 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='辅助检查' ")[0]["s手签"]);
                    this.xrPictureBox辅助检查.Image = img辅助检查;
                }
                if (dt.Select("c查体模块='心电图' ").Length > 0)
                {
                    System.Drawing.Image img心电图 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='心电图' ")[0]["s手签"]);
                    this.xrPictureBox心电图.Image = img心电图;
                }
                if (dt.Select("c查体模块='胸部射线' ").Length > 0)
                {
                    System.Drawing.Image img胸部射线 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='胸部射线' ")[0]["s手签"]);
                    this.xrPictureBox胸部射线.Image = img胸部射线;
                }
                if (dt.Select("c查体模块='B超' ").Length > 0)
                {
                    System.Drawing.Image imgB超 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='B超' ")[0]["s手签"]);
                    this.xrPictureBoxB超.Image = imgB超;
                }
                if (dt.Select("c查体模块='主要现存问题' ").Length > 0)
                {
                    System.Drawing.Image img主要现存问题 = (Image)ZipTools.DecompressionObject((byte[])dt.Select("c查体模块='主要现存问题' ")[0]["s手签"]);
                    this.xrPictureBox主要现存问题.Image = img主要现存问题;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void BindData(DataSet ds体检)
        {
            DataTable tb健康体检 = ds体检.Tables[tb_健康体检.__TableName];
            DataTable tb住院史 = ds体检.Tables["住院史"];
            DataTable tb家庭病床史 = ds体检.Tables["家庭病床史"];

            if (tb健康体检.Rows.Count == 1)
            {
                //大便潜血
                string 大便潜血 = tb健康体检.Rows[0][tb_健康体检.大便潜血].ToString();
                this.xrLabel_大便潜血.Text = 大便潜血 == "3" ? "" : 大便潜血;
                //糖化血红蛋白
                this.xrLabel_糖化血红蛋白.Text = tb健康体检.Rows[0][tb_健康体检.糖化血红蛋白].ToString();
                //乙型肝炎表面抗原
                string 乙型肝炎表面抗原 = tb健康体检.Rows[0][tb_健康体检.乙型肝炎表面抗原].ToString();
                xrLabel_乙型肝炎表面抗原.Text = 乙型肝炎表面抗原 == "3" ? "" : 乙型肝炎表面抗原;
                //血清谷丙转氨酶
                this.xrLabel_血清谷丙转氨酶.Text = tb健康体检.Rows[0][tb_健康体检.血清谷丙转氨酶].ToString();
                //血清谷草转氨酶
                this.xrLabel_血清谷草转氨酶.Text = tb健康体检.Rows[0][tb_健康体检.血清谷草转氨酶].ToString();
                //白蛋白
                this.xrLabel_白蛋白.Text = tb健康体检.Rows[0][tb_健康体检.白蛋白].ToString();
                //总胆红素
                this.xrLabel_总胆红素.Text = tb健康体检.Rows[0][tb_健康体检.总胆红素].ToString();
                //结合胆红素
                this.xrLabel_综合胆红素.Text = tb健康体检.Rows[0][tb_健康体检.结合胆红素].ToString();
                //血清肌酐
                this.xrLabel_血清肌酐.Text = tb健康体检.Rows[0][tb_健康体检.血清肌酐].ToString();
                //血尿素
                this.xrLabel_血尿素.Text = tb健康体检.Rows[0][tb_健康体检.血尿素氮].ToString();
                //血钾浓度
                this.xrLabel_血钾浓度.Text = tb健康体检.Rows[0][tb_健康体检.血钾浓度].ToString();
                //血钠浓度
                this.xrLabel_血钠浓度.Text = tb健康体检.Rows[0][tb_健康体检.血钠浓度].ToString();
                //总胆固醇
                this.xrLabel_总胆固醇.Text = tb健康体检.Rows[0][tb_健康体检.总胆固醇].ToString();
                //甘油三酯
                this.xrLabel_甘油三酯.Text = tb健康体检.Rows[0][tb_健康体检.甘油三酯].ToString();
                //血清低密度脂蛋白胆固醇
                this.xrLabel_血清低密度脂蛋白胆固醇.Text = tb健康体检.Rows[0][tb_健康体检.血清低密度脂蛋白胆固醇].ToString();
                //血清高密度脂蛋白胆固醇
                this.xrLabel_血清高密度脂蛋白胆固醇.Text = tb健康体检.Rows[0][tb_健康体检.血清高密度脂蛋白胆固醇].ToString();
                //心电图
                string 心电图 = tb健康体检.Rows[0][tb_健康体检.心电图].ToString();
                string[] 心电图s = 心电图.Split(',');
                for (int i = 0; i < 心电图s.Length; i++)
                {
                    if (string.IsNullOrEmpty(心电图s[i]))
                    {
                        continue;
                    }

                    if (i > 5) break;

                    string result_心电图 = 心电图s[i] == "99" ? "9" : 心电图s[i];
                    string xrName = "xrLabel_心电图" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_心电图;
                }
                this.xrLabel_心电图其他.Text = tb健康体检.Rows[0][tb_健康体检.心电图异常].ToString();
                //胸部X线片
                string 胸部X线片 = tb健康体检.Rows[0][tb_健康体检.胸部X线片].ToString();
                this.xrLabel_胸部X线片.Text = 胸部X线片 == "3" ? "" : 胸部X线片;
                this.xrLabel_胸部X线片异常.Text = tb健康体检.Rows[0][tb_健康体检.胸部X线片异常].ToString();
                //B超
                string 腹部B超 = tb健康体检.Rows[0][tb_健康体检.B超].ToString();
                this.xrLabel_腹部B超.Text = 腹部B超 == "3" ? "" : 腹部B超;
                this.xrLabel_腹部B超异常.Text = tb健康体检.Rows[0][tb_健康体检.B超其他].ToString();
                string B超其他 = tb健康体检.Rows[0][tb_健康体检.其他B超].ToString();
                this.xrLabel_B超其他.Text = B超其他 == "3" ? "" : B超其他;
                this.xrLabel_B超其他异常.Text = tb健康体检.Rows[0][tb_健康体检.其他B超异常].ToString();
                //宫颈涂片
                string 宫颈涂片 = tb健康体检.Rows[0][tb_健康体检.宫颈涂片].ToString();
                this.xrLabel_宫颈涂片.Text = 宫颈涂片 == "3" ? "" : 宫颈涂片;
                this.xrLabel_宫颈涂片异常.Text = tb健康体检.Rows[0][tb_健康体检.宫颈涂片异常].ToString();
                //辅助检查其他
                this.xrTable_辅助检查其他.Text = tb健康体检.Rows[0][tb_健康体检.辅助检查其他].ToString();
                //脑血管疾病
                string 脑血管疾病 = tb健康体检.Rows[0][tb_健康体检.脑血管疾病].ToString();
                string[] 脑血管疾病s = 脑血管疾病.Split(',');
                for (int i = 0; i < 脑血管疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(脑血管疾病s[i])) continue;

                    if (i > 4) break;

                    string result_脑血管疾病 = 脑血管疾病s[i] == "99" ? "6" : 脑血管疾病s[i];
                    string xrName = "xrLabel_脑血管疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_脑血管疾病;
                }
                this.xrLabel_脑血管疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.脑血管疾病其他].ToString();
                //肾脏疾病
                string 肾脏疾病 = tb健康体检.Rows[0][tb_健康体检.肾脏疾病].ToString();
                string[] 肾脏疾病s = 肾脏疾病.Split(',');
                for (int i = 0; i < 肾脏疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(肾脏疾病s[i])) continue;

                    if (i > 4) break;

                    string result_肾脏疾病 = 肾脏疾病s[i] == "99" ? "6" : 肾脏疾病s[i];
                    string xrName = "xrLabel_肾脏疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_肾脏疾病;
                }
                this.xrLabel_肾脏疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.肾脏疾病其他].ToString();
                //心血管疾病
                string 心血管疾病 = tb健康体检.Rows[0][tb_健康体检.心脏疾病].ToString();
                string[] 心血管疾病s = 心血管疾病.Split(',');
                for (int i = 0; i < 心血管疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(心血管疾病s[i])) continue;

                    if (i > 4) break;

                    string result_心血管疾病 = 心血管疾病s[i] == "99" ? "10" : 心血管疾病s[i];
                    string xrName = "xrLabel_心血管疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_心血管疾病;
                }
                this.xrLabel_心血管疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.心脏疾病其他].ToString();
                //眼部疾病
                string 眼部疾病 = tb健康体检.Rows[0][tb_健康体检.眼部疾病].ToString();
                string[] 眼部疾病s = 眼部疾病.Split(',');
                for (int i = 0; i < 眼部疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(眼部疾病s[i])) continue;

                    if (i > 2) break;

                    string result_眼部疾病 = 眼部疾病s[i] == "99" ? "5" : 眼部疾病s[i];
                    string xrName = "xrLabel_眼部疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_眼部疾病;
                }
                this.xrLabel_眼部疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.眼部疾病其他].ToString();
                //神经系统其他疾病
                string 神经系统其他疾病 = tb健康体检.Rows[0][tb_健康体检.神经系统疾病].ToString();
                string[] 神经系统其他疾病s = 神经系统其他疾病.Split(',');
                for (int i = 0; i < 神经系统其他疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(神经系统其他疾病s[i])) continue;

                    if (i > 2) break;

                    string result_神经系统其他疾病 = 神经系统其他疾病s[i] == "99" ? "4" : 神经系统其他疾病s[i];
                    string xrName = "xrLabel_神经系统其他疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_神经系统其他疾病;
                }
                this.xrLabel_神经系统其他疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.神经系统疾病其他].ToString();
                //其他系统疾病
                string 其他系统疾病 = tb健康体检.Rows[0][tb_健康体检.其他系统疾病].ToString();
                string[] 其他系统疾病s = 其他系统疾病.Split(',');
                for (int i = 0; i < 其他系统疾病s.Length; i++)
                {
                    if (string.IsNullOrEmpty(其他系统疾病s[i])) continue;

                    if (i > 2) break;

                    string result_其他系统疾病 = 其他系统疾病s[i] == "99" ? "7" : 其他系统疾病s[i];
                    string xrName = "xrLabel_其他系统疾病" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = result_其他系统疾病;
                }
                this.xrLabel_其他系统疾病其他.Text = tb健康体检.Rows[0][tb_健康体检.其他系统疾病其他].ToString();
                //住院史
                if (tb住院史 != null && tb住院史.Rows.Count > 0)
                {
                    for (int i = 0; i < tb住院史.Rows.Count; i++)
                    {
                        if (i > 1) break;

                        string 入院日期 = "xrTable_入院日期" + (i + 1);
                        string 出院日期 = "xrTable_出院日期" + (i + 1);
                        string 原因 = "xrTable_住院史原因" + (i + 1);
                        string 医疗机构及科室 = "xrTable_住院史机构及科室" + (i + 1);
                        string 病案号 = "xrTable_住院史病案号" + (i + 1);

                        XRTableCell xr入院日期 = (XRTableCell)FindControl(入院日期, false);
                        XRTableCell xr出院日期 = (XRTableCell)FindControl(出院日期, false);
                        XRTableCell xr原因 = (XRTableCell)FindControl(原因, false);
                        XRTableCell xr医疗机构及科室 = (XRTableCell)FindControl(医疗机构及科室, false);
                        XRTableCell xr病案号 = (XRTableCell)FindControl(病案号, false);

                        object obj_入院日期 = tb住院史.Rows[0][tb_健康体检_住院史.入院日期];
                        object obj_出院日期 = tb住院史.Rows[0][tb_健康体检_住院史.出院日期];
                        object obj_原因 = tb住院史.Rows[0][tb_健康体检_住院史.原因];
                        object obj_医疗机构名称 = tb住院史.Rows[0][tb_健康体检_住院史.医疗机构名称];
                        object obj_病案号 = tb住院史.Rows[0][tb_健康体检_住院史.病案号];

                        if ((obj_入院日期 == null ? "" : obj_入院日期.ToString()).Length >= 10)
                        {
                            xr入院日期.Text = obj_入院日期.ToString().Substring(0, 10);
                        }
                        if ((obj_出院日期 == null ? "" : obj_出院日期.ToString()).Length >= 10)
                        {
                            xr出院日期.Text = obj_出院日期.ToString().Substring(0, 10);
                        }
                        xr原因.Text = (obj_原因 == null ? "" : obj_原因).ToString();
                        xr医疗机构及科室.Text = (obj_医疗机构名称 == null ? "" : obj_医疗机构名称).ToString();
                        xr病案号.Text = (obj_病案号 == null ? "" : obj_病案号).ToString();
                    }
                }
                //家庭病床史
                if (tb家庭病床史 != null && tb家庭病床史.Rows.Count > 0)
                {
                    for (int i = 0; i < tb家庭病床史.Rows.Count; i++)
                    {
                        if (i > 1) break;

                        string 建床日期 = "xrTable_建床日期" + (i + 1);
                        string 撤床日期 = "xrTable_撤床日期" + (i + 1);
                        string 原因 = "xrTable_家庭病床史原因" + (i + 1);
                        string 医疗机构及科室 = "xrTable_家庭病床史机构及科室" + (i + 1);
                        string 病案号 = "xrTable_家庭病床史病案号" + (i + 1);

                        XRTableCell xr建床日期 = (XRTableCell)FindControl(建床日期, false);
                        XRTableCell xr撤床日期 = (XRTableCell)FindControl(撤床日期, false);
                        XRTableCell xr原因 = (XRTableCell)FindControl(原因, false);
                        XRTableCell xr医疗机构及科室 = (XRTableCell)FindControl(医疗机构及科室, false);
                        XRTableCell xr病案号 = (XRTableCell)FindControl(病案号, false);

                        //wxf 2018年9月14日 17:37:10 对需要转换的数据先进行异常排除
                        object obj_建床日期 = tb家庭病床史.Rows[0][tb_健康体检_住院史.入院日期];
                        object obj_撤床日期 = tb家庭病床史.Rows[0][tb_健康体检_住院史.出院日期];
                        object obj_原因 = tb家庭病床史.Rows[0][tb_健康体检_住院史.原因];
                        object obj_医疗机构名称 = tb家庭病床史.Rows[0][tb_健康体检_住院史.医疗机构名称];
                        object obj_病案号 = tb家庭病床史.Rows[0][tb_健康体检_住院史.病案号];

                        if ((obj_建床日期 == null ? "" : obj_建床日期.ToString()).Length >= 10)
                        {
                            xr建床日期.Text = obj_建床日期.ToString().Substring(0, 10);
                        }
                        if ((obj_撤床日期 == null ? "" : obj_撤床日期.ToString()).Length >= 10)
                        {
                            xr撤床日期.Text = obj_撤床日期.ToString().Substring(0, 10);
                        }
                        xr原因.Text = (obj_原因 == null ? "" : obj_原因).ToString();
                        xr医疗机构及科室.Text = (obj_医疗机构名称 == null ? "" : obj_医疗机构名称).ToString();
                        xr病案号.Text = (obj_病案号 == null ? "" : obj_病案号).ToString();
                    }
                }
            }
        }
    }
}
