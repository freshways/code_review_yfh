﻿namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    partial class UC产后访视记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC产后访视记录表));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ch失访情况 = new DevExpress.XtraEditors.CheckEdit();
            this.txt失访原因 = new DevExpress.XtraEditors.TextEdit();
            this.txt居民或家属签名 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.date出院日期 = new DevExpress.XtraEditors.DateEdit();
            this.date分娩日期 = new DevExpress.XtraEditors.DateEdit();
            this.cbo产次 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo分类 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit分类 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo恶露 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit恶露 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo伤口 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit伤口 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo子宫 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit子宫 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo乳房 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit乳房 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit一般健康情况 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit一般心理状况 = new DevExpress.XtraEditors.TextEdit();
            this.flow转诊 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.cbo转诊有无 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.uc转诊原因 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc转诊机构 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc联系人 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc联系方式 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.label2 = new System.Windows.Forms.Label();
            this.cbo是否到位 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flow指导 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit个人卫生 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit心理 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit膳食 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit母乳喂养 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit新生儿护理与喂养 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit指导其他 = new DevExpress.XtraEditors.TextEdit();
            this.uc血压 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.uc体温 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住状态 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit孕妇姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit卡号 = new DevExpress.XtraEditors.TextEdit();
            this.dte下次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.dte随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem75 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem76 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch失访情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt失访原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民或家属签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date出院日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date出院日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date分娩日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date分娩日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo产次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit其他.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo分类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit分类.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo恶露.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit恶露.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo伤口.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit伤口.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo子宫.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit子宫.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo乳房.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit乳房.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit一般健康情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit一般心理状况.Properties)).BeginInit();
            this.flow转诊.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo转诊有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo是否到位.Properties)).BeginInit();
            this.flow指导.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit个人卫生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit心理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit膳食.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit母乳喂养.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit新生儿护理与喂养.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(748, 32);
            this.panelControl1.TabIndex = 6;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(744, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn重置
            // 
            this.btn重置.Location = new System.Drawing.Point(84, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "重置";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.ch失访情况);
            this.layoutControl1.Controls.Add(this.txt失访原因);
            this.layoutControl1.Controls.Add(this.txt居民或家属签名);
            this.layoutControl1.Controls.Add(this.layoutControl2);
            this.layoutControl1.Controls.Add(this.date出院日期);
            this.layoutControl1.Controls.Add(this.date分娩日期);
            this.layoutControl1.Controls.Add(this.cbo产次);
            this.layoutControl1.Controls.Add(this.textEdit其他);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.textEdit一般健康情况);
            this.layoutControl1.Controls.Add(this.textEdit一般心理状况);
            this.layoutControl1.Controls.Add(this.flow转诊);
            this.layoutControl1.Controls.Add(this.flow指导);
            this.layoutControl1.Controls.Add(this.uc血压);
            this.layoutControl1.Controls.Add(this.uc体温);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit创建人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近更新时间);
            this.layoutControl1.Controls.Add(this.textEdit创建时间);
            this.layoutControl1.Controls.Add(this.textEdit联系电话);
            this.layoutControl1.Controls.Add(this.textEdit居住地址);
            this.layoutControl1.Controls.Add(this.textEdit居住状态);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit孕妇姓名);
            this.layoutControl1.Controls.Add(this.textEdit档案编号);
            this.layoutControl1.Controls.Add(this.textEdit卡号);
            this.layoutControl1.Controls.Add(this.dte下次随访日期);
            this.layoutControl1.Controls.Add(this.dte随访日期);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.AllowFocusGroups = false;
            this.layoutControl1.OptionsFocus.AllowFocusReadonlyEditors = false;
            this.layoutControl1.OptionsFocus.AllowFocusTabbedGroups = false;
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(748, 409);
            this.layoutControl1.TabIndex = 7;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // ch失访情况
            // 
            this.ch失访情况.Location = new System.Drawing.Point(338, 127);
            this.ch失访情况.Name = "ch失访情况";
            this.ch失访情况.Properties.Caption = "失访";
            this.ch失访情况.Size = new System.Drawing.Size(78, 19);
            this.ch失访情况.StyleController = this.layoutControl1;
            this.ch失访情况.TabIndex = 55;
            // 
            // txt失访原因
            // 
            this.txt失访原因.Location = new System.Drawing.Point(471, 127);
            this.txt失访原因.Name = "txt失访原因";
            this.txt失访原因.Size = new System.Drawing.Size(257, 20);
            this.txt失访原因.StyleController = this.layoutControl1;
            this.txt失访原因.TabIndex = 54;
            // 
            // txt居民或家属签名
            // 
            this.txt居民或家属签名.Location = new System.Drawing.Point(553, 526);
            this.txt居民或家属签名.Name = "txt居民或家属签名";
            this.txt居民或家属签名.Size = new System.Drawing.Size(175, 20);
            this.txt居民或家属签名.StyleController = this.layoutControl1;
            this.txt居民或家属签名.TabIndex = 27;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Location = new System.Drawing.Point(513, 152);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(215, 20);
            this.layoutControl2.TabIndex = 53;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(215, 20);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // date出院日期
            // 
            this.date出院日期.EditValue = null;
            this.date出院日期.Location = new System.Drawing.Point(348, 152);
            this.date出院日期.Name = "date出院日期";
            this.date出院日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date出院日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date出院日期.Size = new System.Drawing.Size(161, 20);
            this.date出院日期.StyleController = this.layoutControl1;
            this.date出院日期.TabIndex = 12;
            // 
            // date分娩日期
            // 
            this.date分娩日期.EditValue = null;
            this.date分娩日期.Location = new System.Drawing.Point(98, 152);
            this.date分娩日期.Name = "date分娩日期";
            this.date分娩日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date分娩日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date分娩日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.date分娩日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.date分娩日期.Size = new System.Drawing.Size(151, 20);
            this.date分娩日期.StyleController = this.layoutControl1;
            this.date分娩日期.TabIndex = 11;
            // 
            // cbo产次
            // 
            this.cbo产次.Location = new System.Drawing.Point(438, 248);
            this.cbo产次.Name = "cbo产次";
            this.cbo产次.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo产次.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo产次.Size = new System.Drawing.Size(91, 20);
            this.cbo产次.StyleController = this.layoutControl1;
            this.cbo产次.TabIndex = 17;
            // 
            // textEdit其他
            // 
            this.textEdit其他.Location = new System.Drawing.Point(98, 368);
            this.textEdit其他.Name = "textEdit其他";
            this.textEdit其他.Size = new System.Drawing.Size(551, 20);
            this.textEdit其他.StyleController = this.layoutControl1;
            this.textEdit其他.TabIndex = 22;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.flowLayoutPanel6.Controls.Add(this.cbo分类);
            this.flowLayoutPanel6.Controls.Add(this.textEdit分类);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(98, 392);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(630, 20);
            this.flowLayoutPanel6.TabIndex = 23;
            // 
            // cbo分类
            // 
            this.cbo分类.Location = new System.Drawing.Point(0, 0);
            this.cbo分类.Margin = new System.Windows.Forms.Padding(0);
            this.cbo分类.Name = "cbo分类";
            this.cbo分类.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo分类.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo分类.Size = new System.Drawing.Size(100, 20);
            this.cbo分类.TabIndex = 0;
            this.cbo分类.SelectedIndexChanged += new System.EventHandler(this.cbo分类_SelectedIndexChanged);
            // 
            // textEdit分类
            // 
            this.textEdit分类.Location = new System.Drawing.Point(100, 0);
            this.textEdit分类.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit分类.Name = "textEdit分类";
            this.textEdit分类.Size = new System.Drawing.Size(459, 20);
            this.textEdit分类.TabIndex = 1;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.flowLayoutPanel3.Controls.Add(this.cbo恶露);
            this.flowLayoutPanel3.Controls.Add(this.textEdit恶露);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(98, 296);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(630, 20);
            this.flowLayoutPanel3.TabIndex = 19;
            // 
            // cbo恶露
            // 
            this.cbo恶露.Location = new System.Drawing.Point(0, 0);
            this.cbo恶露.Margin = new System.Windows.Forms.Padding(0);
            this.cbo恶露.Name = "cbo恶露";
            this.cbo恶露.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo恶露.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo恶露.Size = new System.Drawing.Size(100, 20);
            this.cbo恶露.TabIndex = 0;
            this.cbo恶露.SelectedIndexChanged += new System.EventHandler(this.cbo恶露_SelectedIndexChanged);
            // 
            // textEdit恶露
            // 
            this.textEdit恶露.Location = new System.Drawing.Point(100, 0);
            this.textEdit恶露.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit恶露.Name = "textEdit恶露";
            this.textEdit恶露.Size = new System.Drawing.Size(459, 20);
            this.textEdit恶露.TabIndex = 1;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.flowLayoutPanel5.Controls.Add(this.cbo伤口);
            this.flowLayoutPanel5.Controls.Add(this.textEdit伤口);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(98, 344);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(630, 20);
            this.flowLayoutPanel5.TabIndex = 21;
            // 
            // cbo伤口
            // 
            this.cbo伤口.Location = new System.Drawing.Point(0, 0);
            this.cbo伤口.Margin = new System.Windows.Forms.Padding(0);
            this.cbo伤口.Name = "cbo伤口";
            this.cbo伤口.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo伤口.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo伤口.Size = new System.Drawing.Size(100, 20);
            this.cbo伤口.TabIndex = 0;
            this.cbo伤口.SelectedIndexChanged += new System.EventHandler(this.cbo伤口_SelectedIndexChanged);
            // 
            // textEdit伤口
            // 
            this.textEdit伤口.Location = new System.Drawing.Point(100, 0);
            this.textEdit伤口.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit伤口.Name = "textEdit伤口";
            this.textEdit伤口.Size = new System.Drawing.Size(459, 20);
            this.textEdit伤口.TabIndex = 1;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.flowLayoutPanel4.Controls.Add(this.cbo子宫);
            this.flowLayoutPanel4.Controls.Add(this.textEdit子宫);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(98, 320);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(630, 20);
            this.flowLayoutPanel4.TabIndex = 20;
            // 
            // cbo子宫
            // 
            this.cbo子宫.Location = new System.Drawing.Point(0, 0);
            this.cbo子宫.Margin = new System.Windows.Forms.Padding(0);
            this.cbo子宫.Name = "cbo子宫";
            this.cbo子宫.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo子宫.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo子宫.Size = new System.Drawing.Size(100, 20);
            this.cbo子宫.TabIndex = 0;
            this.cbo子宫.SelectedIndexChanged += new System.EventHandler(this.cbo子宫_SelectedIndexChanged);
            // 
            // textEdit子宫
            // 
            this.textEdit子宫.Location = new System.Drawing.Point(100, 0);
            this.textEdit子宫.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit子宫.Name = "textEdit子宫";
            this.textEdit子宫.Size = new System.Drawing.Size(459, 20);
            this.textEdit子宫.TabIndex = 1;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.flowLayoutPanel2.Controls.Add(this.cbo乳房);
            this.flowLayoutPanel2.Controls.Add(this.textEdit乳房);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(98, 272);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(630, 20);
            this.flowLayoutPanel2.TabIndex = 18;
            // 
            // cbo乳房
            // 
            this.cbo乳房.Location = new System.Drawing.Point(0, 0);
            this.cbo乳房.Margin = new System.Windows.Forms.Padding(0);
            this.cbo乳房.Name = "cbo乳房";
            this.cbo乳房.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo乳房.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo乳房.Size = new System.Drawing.Size(100, 20);
            this.cbo乳房.TabIndex = 0;
            this.cbo乳房.SelectedIndexChanged += new System.EventHandler(this.cbo乳房_SelectedIndexChanged);
            // 
            // textEdit乳房
            // 
            this.textEdit乳房.Location = new System.Drawing.Point(100, 0);
            this.textEdit乳房.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit乳房.Name = "textEdit乳房";
            this.textEdit乳房.Size = new System.Drawing.Size(459, 20);
            this.textEdit乳房.TabIndex = 1;
            // 
            // textEdit一般健康情况
            // 
            this.textEdit一般健康情况.Location = new System.Drawing.Point(98, 200);
            this.textEdit一般健康情况.Name = "textEdit一般健康情况";
            this.textEdit一般健康情况.Size = new System.Drawing.Size(630, 20);
            this.textEdit一般健康情况.StyleController = this.layoutControl1;
            this.textEdit一般健康情况.TabIndex = 14;
            // 
            // textEdit一般心理状况
            // 
            this.textEdit一般心理状况.Location = new System.Drawing.Point(98, 224);
            this.textEdit一般心理状况.Name = "textEdit一般心理状况";
            this.textEdit一般心理状况.Size = new System.Drawing.Size(630, 20);
            this.textEdit一般心理状况.StyleController = this.layoutControl1;
            this.textEdit一般心理状况.TabIndex = 15;
            // 
            // flow转诊
            // 
            this.flow转诊.Controls.Add(this.label1);
            this.flow转诊.Controls.Add(this.cbo转诊有无);
            this.flow转诊.Controls.Add(this.uc转诊原因);
            this.flow转诊.Controls.Add(this.uc转诊机构);
            this.flow转诊.Controls.Add(this.uc联系人);
            this.flow转诊.Controls.Add(this.uc联系方式);
            this.flow转诊.Controls.Add(this.label2);
            this.flow转诊.Controls.Add(this.cbo是否到位);
            this.flow转诊.Location = new System.Drawing.Point(98, 466);
            this.flow转诊.Name = "flow转诊";
            this.flow转诊.Size = new System.Drawing.Size(630, 56);
            this.flow转诊.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 43;
            this.label1.Text = "有无转诊：";
            // 
            // cbo转诊有无
            // 
            this.cbo转诊有无.Location = new System.Drawing.Point(71, 0);
            this.cbo转诊有无.Margin = new System.Windows.Forms.Padding(0);
            this.cbo转诊有无.Name = "cbo转诊有无";
            this.cbo转诊有无.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo转诊有无.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo转诊有无.Size = new System.Drawing.Size(59, 20);
            this.cbo转诊有无.TabIndex = 0;
            this.cbo转诊有无.SelectedIndexChanged += new System.EventHandler(this.cbo转诊有无_SelectedIndexChanged);
            // 
            // uc转诊原因
            // 
            this.uc转诊原因.Lbl1Size = new System.Drawing.Size(40, 18);
            this.uc转诊原因.Lbl1Text = "原因:";
            this.uc转诊原因.Location = new System.Drawing.Point(130, 0);
            this.uc转诊原因.Margin = new System.Windows.Forms.Padding(0);
            this.uc转诊原因.Name = "uc转诊原因";
            this.uc转诊原因.Size = new System.Drawing.Size(166, 22);
            this.uc转诊原因.TabIndex = 1;
            this.uc转诊原因.Txt1Size = new System.Drawing.Size(120, 20);
            // 
            // uc转诊机构
            // 
            this.uc转诊机构.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc转诊机构.Lbl1Text = "机构及科室:";
            this.uc转诊机构.Location = new System.Drawing.Point(296, 0);
            this.uc转诊机构.Margin = new System.Windows.Forms.Padding(0);
            this.uc转诊机构.Name = "uc转诊机构";
            this.uc转诊机构.Size = new System.Drawing.Size(202, 22);
            this.uc转诊机构.TabIndex = 2;
            this.uc转诊机构.Txt1Size = new System.Drawing.Size(120, 20);
            // 
            // uc联系人
            // 
            this.uc联系人.Lbl1Size = new System.Drawing.Size(40, 18);
            this.uc联系人.Lbl1Text = "联系人：";
            this.uc联系人.Location = new System.Drawing.Point(3, 25);
            this.uc联系人.Name = "uc联系人";
            this.uc联系人.Size = new System.Drawing.Size(127, 22);
            this.uc联系人.TabIndex = 3;
            this.uc联系人.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // uc联系方式
            // 
            this.uc联系方式.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc联系方式.Lbl1Text = "联系方式：";
            this.uc联系方式.Location = new System.Drawing.Point(136, 25);
            this.uc联系方式.Name = "uc联系方式";
            this.uc联系方式.Size = new System.Drawing.Size(181, 22);
            this.uc联系方式.TabIndex = 4;
            this.uc联系方式.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(323, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 44;
            this.label2.Text = "结果：";
            // 
            // cbo是否到位
            // 
            this.cbo是否到位.Location = new System.Drawing.Point(370, 25);
            this.cbo是否到位.Name = "cbo是否到位";
            this.cbo是否到位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo是否到位.Size = new System.Drawing.Size(61, 20);
            this.cbo是否到位.TabIndex = 5;
            // 
            // flow指导
            // 
            this.flow指导.BackColor = System.Drawing.SystemColors.ControlLight;
            this.flow指导.Controls.Add(this.checkEdit个人卫生);
            this.flow指导.Controls.Add(this.checkEdit心理);
            this.flow指导.Controls.Add(this.checkEdit膳食);
            this.flow指导.Controls.Add(this.checkEdit母乳喂养);
            this.flow指导.Controls.Add(this.checkEdit新生儿护理与喂养);
            this.flow指导.Controls.Add(this.checkEdit其他);
            this.flow指导.Controls.Add(this.textEdit指导其他);
            this.flow指导.Location = new System.Drawing.Point(98, 416);
            this.flow指导.Name = "flow指导";
            this.flow指导.Size = new System.Drawing.Size(630, 46);
            this.flow指导.TabIndex = 23;
            // 
            // checkEdit个人卫生
            // 
            this.checkEdit个人卫生.Location = new System.Drawing.Point(0, 0);
            this.checkEdit个人卫生.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit个人卫生.Name = "checkEdit个人卫生";
            this.checkEdit个人卫生.Properties.Caption = "个人卫生";
            this.checkEdit个人卫生.Size = new System.Drawing.Size(78, 19);
            this.checkEdit个人卫生.TabIndex = 0;
            this.checkEdit个人卫生.Tag = "1";
            // 
            // checkEdit心理
            // 
            this.checkEdit心理.Location = new System.Drawing.Point(78, 0);
            this.checkEdit心理.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit心理.Name = "checkEdit心理";
            this.checkEdit心理.Properties.Caption = "心理";
            this.checkEdit心理.Size = new System.Drawing.Size(64, 19);
            this.checkEdit心理.TabIndex = 1;
            this.checkEdit心理.Tag = "2";
            // 
            // checkEdit膳食
            // 
            this.checkEdit膳食.Location = new System.Drawing.Point(142, 0);
            this.checkEdit膳食.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit膳食.Name = "checkEdit膳食";
            this.checkEdit膳食.Properties.Caption = "营养";
            this.checkEdit膳食.Size = new System.Drawing.Size(59, 19);
            this.checkEdit膳食.TabIndex = 2;
            this.checkEdit膳食.Tag = "3";
            // 
            // checkEdit母乳喂养
            // 
            this.checkEdit母乳喂养.Location = new System.Drawing.Point(201, 0);
            this.checkEdit母乳喂养.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit母乳喂养.Name = "checkEdit母乳喂养";
            this.checkEdit母乳喂养.Properties.Caption = "母乳喂养";
            this.checkEdit母乳喂养.Size = new System.Drawing.Size(73, 19);
            this.checkEdit母乳喂养.TabIndex = 3;
            this.checkEdit母乳喂养.Tag = "4";
            // 
            // checkEdit新生儿护理与喂养
            // 
            this.checkEdit新生儿护理与喂养.Location = new System.Drawing.Point(274, 0);
            this.checkEdit新生儿护理与喂养.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit新生儿护理与喂养.Name = "checkEdit新生儿护理与喂养";
            this.checkEdit新生儿护理与喂养.Properties.Caption = "新生儿护理与喂养";
            this.checkEdit新生儿护理与喂养.Size = new System.Drawing.Size(121, 19);
            this.checkEdit新生儿护理与喂养.TabIndex = 4;
            this.checkEdit新生儿护理与喂养.Tag = "5";
            // 
            // checkEdit其他
            // 
            this.checkEdit其他.Location = new System.Drawing.Point(395, 0);
            this.checkEdit其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit其他.Name = "checkEdit其他";
            this.checkEdit其他.Properties.Caption = "其他";
            this.checkEdit其他.Size = new System.Drawing.Size(54, 19);
            this.checkEdit其他.TabIndex = 5;
            this.checkEdit其他.Tag = "6";
            this.checkEdit其他.CheckedChanged += new System.EventHandler(this.checkEdit其他_CheckedChanged);
            // 
            // textEdit指导其他
            // 
            this.textEdit指导其他.Location = new System.Drawing.Point(0, 19);
            this.textEdit指导其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit指导其他.Name = "textEdit指导其他";
            this.textEdit指导其他.Size = new System.Drawing.Size(341, 20);
            this.textEdit指导其他.TabIndex = 38;
            // 
            // uc血压
            // 
            this.uc血压.Lbl1Size = new System.Drawing.Size(10, 14);
            this.uc血压.Lbl1Text = "/";
            this.uc血压.Lbl2Size = new System.Drawing.Size(40, 14);
            this.uc血压.Lbl2Text = "mmHg";
            this.uc血压.Location = new System.Drawing.Point(98, 248);
            this.uc血压.Name = "uc血压";
            this.uc血压.Size = new System.Drawing.Size(251, 20);
            this.uc血压.TabIndex = 16;
            this.uc血压.Txt1EditValue = null;
            this.uc血压.Txt1Size = new System.Drawing.Size(70, 20);
            this.uc血压.Txt2EditValue = null;
            this.uc血压.Txt2Size = new System.Drawing.Size(70, 20);
            // 
            // uc体温
            // 
            this.uc体温.Lbl1Size = new System.Drawing.Size(20, 18);
            this.uc体温.Lbl1Text = "℃ ";
            this.uc体温.Location = new System.Drawing.Point(98, 176);
            this.uc体温.Name = "uc体温";
            this.uc体温.Size = new System.Drawing.Size(630, 20);
            this.uc体温.TabIndex = 13;
            this.uc体温.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(554, 575);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit最近修改人.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(174, 20);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 33;
            // 
            // textEdit创建人
            // 
            this.textEdit创建人.Location = new System.Drawing.Point(324, 575);
            this.textEdit创建人.Name = "textEdit创建人";
            this.textEdit创建人.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建人.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建人.Properties.ReadOnly = true;
            this.textEdit创建人.Size = new System.Drawing.Size(131, 20);
            this.textEdit创建人.StyleController = this.layoutControl1;
            this.textEdit创建人.TabIndex = 32;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(98, 575);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建机构.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(137, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 31;
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(554, 551);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit当前所属机构.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(174, 20);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 30;
            // 
            // textEdit最近更新时间
            // 
            this.textEdit最近更新时间.Location = new System.Drawing.Point(324, 551);
            this.textEdit最近更新时间.Name = "textEdit最近更新时间";
            this.textEdit最近更新时间.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit最近更新时间.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit最近更新时间.Properties.ReadOnly = true;
            this.textEdit最近更新时间.Size = new System.Drawing.Size(131, 20);
            this.textEdit最近更新时间.StyleController = this.layoutControl1;
            this.textEdit最近更新时间.TabIndex = 29;
            // 
            // textEdit创建时间
            // 
            this.textEdit创建时间.Location = new System.Drawing.Point(98, 551);
            this.textEdit创建时间.Name = "textEdit创建时间";
            this.textEdit创建时间.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建时间.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建时间.Properties.ReadOnly = true;
            this.textEdit创建时间.Size = new System.Drawing.Size(137, 20);
            this.textEdit创建时间.StyleController = this.layoutControl1;
            this.textEdit创建时间.TabIndex = 28;
            // 
            // textEdit联系电话
            // 
            this.textEdit联系电话.Location = new System.Drawing.Point(598, 79);
            this.textEdit联系电话.Name = "textEdit联系电话";
            this.textEdit联系电话.Properties.ReadOnly = true;
            this.textEdit联系电话.Size = new System.Drawing.Size(130, 20);
            this.textEdit联系电话.StyleController = this.layoutControl1;
            this.textEdit联系电话.TabIndex = 7;
            // 
            // textEdit居住地址
            // 
            this.textEdit居住地址.Location = new System.Drawing.Point(338, 103);
            this.textEdit居住地址.Name = "textEdit居住地址";
            this.textEdit居住地址.Properties.ReadOnly = true;
            this.textEdit居住地址.Size = new System.Drawing.Size(390, 20);
            this.textEdit居住地址.StyleController = this.layoutControl1;
            this.textEdit居住地址.TabIndex = 9;
            // 
            // textEdit居住状态
            // 
            this.textEdit居住状态.Location = new System.Drawing.Point(98, 103);
            this.textEdit居住状态.Name = "textEdit居住状态";
            this.textEdit居住状态.Properties.ReadOnly = true;
            this.textEdit居住状态.Size = new System.Drawing.Size(151, 20);
            this.textEdit居住状态.StyleController = this.layoutControl1;
            this.textEdit居住状态.TabIndex = 8;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(323, 526);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Size = new System.Drawing.Size(131, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 26;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(338, 79);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(171, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 6;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(98, 79);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(151, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 5;
            // 
            // textEdit孕妇姓名
            // 
            this.textEdit孕妇姓名.Location = new System.Drawing.Point(598, 55);
            this.textEdit孕妇姓名.Name = "textEdit孕妇姓名";
            this.textEdit孕妇姓名.Properties.ReadOnly = true;
            this.textEdit孕妇姓名.Size = new System.Drawing.Size(130, 20);
            this.textEdit孕妇姓名.StyleController = this.layoutControl1;
            this.textEdit孕妇姓名.TabIndex = 4;
            // 
            // textEdit档案编号
            // 
            this.textEdit档案编号.Location = new System.Drawing.Point(338, 55);
            this.textEdit档案编号.Name = "textEdit档案编号";
            this.textEdit档案编号.Properties.ReadOnly = true;
            this.textEdit档案编号.Size = new System.Drawing.Size(171, 20);
            this.textEdit档案编号.StyleController = this.layoutControl1;
            this.textEdit档案编号.TabIndex = 3;
            // 
            // textEdit卡号
            // 
            this.textEdit卡号.Location = new System.Drawing.Point(98, 55);
            this.textEdit卡号.Name = "textEdit卡号";
            this.textEdit卡号.Size = new System.Drawing.Size(151, 20);
            this.textEdit卡号.StyleController = this.layoutControl1;
            this.textEdit卡号.TabIndex = 2;
            // 
            // dte下次随访日期
            // 
            this.dte下次随访日期.EditValue = null;
            this.dte下次随访日期.Location = new System.Drawing.Point(98, 526);
            this.dte下次随访日期.Name = "dte下次随访日期";
            this.dte下次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte下次随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte下次随访日期.Size = new System.Drawing.Size(136, 20);
            this.dte下次随访日期.StyleController = this.layoutControl1;
            this.dte下次随访日期.TabIndex = 25;
            // 
            // dte随访日期
            // 
            this.dte随访日期.EditValue = null;
            this.dte随访日期.Location = new System.Drawing.Point(98, 127);
            this.dte随访日期.Name = "dte随访日期";
            this.dte随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访日期.Size = new System.Drawing.Size(151, 20);
            this.dte随访日期.StyleController = this.layoutControl1;
            this.dte随访日期.TabIndex = 10;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem8,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.emptySpaceItem1,
            this.layoutControlItem12,
            this.layoutControlItem31,
            this.layoutControlItem11,
            this.layoutControlItem75,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem22,
            this.layoutControlItem44,
            this.layoutControlItem16,
            this.layoutControlItem10,
            this.layoutControlItem46,
            this.layoutControlItem9,
            this.layoutControlItem18,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem76,
            this.layoutControlItem6,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem17,
            this.layoutControlItem19,
            this.layoutControlItem32});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 598);
            this.layoutControlGroup1.Text = "产后访视记录表";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit卡号;
            this.layoutControlItem1.CustomizationFormText = "卡 号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "卡 号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.Control = this.dte下次随访日期;
            this.layoutControlItem7.CustomizationFormText = "产后休养地";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 496);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(250, 0);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(235, 25);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "下次随访日期";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit档案编号;
            this.layoutControlItem2.CustomizationFormText = "档案编号 ";
            this.layoutControlItem2.Location = new System.Drawing.Point(250, 25);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "档案编号 ";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEdit孕妇姓名;
            this.layoutControlItem3.CustomizationFormText = "孕妇姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(510, 25);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "孕妇姓名";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.Control = this.textEdit随访医生;
            this.layoutControlItem8.CustomizationFormText = "联系电话";
            this.layoutControlItem8.Location = new System.Drawing.Point(235, 496);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(220, 25);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "随访医生签名";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.textEdit创建时间;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 521);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "创建时间:";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.textEdit最近更新时间;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(236, 521);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "最近更新时间:";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.textEdit当前所属机构;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(456, 521);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 48);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(273, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "当前所属机构:";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.textEdit创建机构;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 545);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(236, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "创建机构: ";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.textEdit创建人;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(236, 545);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(220, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "创建人:";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.textEdit最近修改人;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(456, 545);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(0, 48);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(273, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "最近修改人:";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "考核项:13  缺项:0  完整度:100% ";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 25);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(729, 25);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "考核项:19  缺项:0  完整度:100% ";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(48, 0);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEdit居住地址;
            this.layoutControlItem12.CustomizationFormText = "居住地址 ";
            this.layoutControlItem12.Location = new System.Drawing.Point(250, 73);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(479, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "居住地址 ";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.Control = this.dte随访日期;
            this.layoutControlItem31.CustomizationFormText = "随访日期";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 97);
            this.layoutControlItem31.MaxSize = new System.Drawing.Size(250, 0);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(250, 25);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "随访日期";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.textEdit居住状态;
            this.layoutControlItem11.CustomizationFormText = "居住状态 ";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 73);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "居住状态 ";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem75
            // 
            this.layoutControlItem75.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem75.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem75.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem75.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem75.Control = this.flow指导;
            this.layoutControlItem75.CustomizationFormText = "保健指导";
            this.layoutControlItem75.Location = new System.Drawing.Point(0, 386);
            this.layoutControlItem75.MinSize = new System.Drawing.Size(159, 50);
            this.layoutControlItem75.Name = "layoutControlItem75";
            this.layoutControlItem75.Size = new System.Drawing.Size(729, 50);
            this.layoutControlItem75.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem75.Text = "指 导";
            this.layoutControlItem75.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem75.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem75.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证号 ";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(250, 49);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(260, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.Control = this.textEdit联系电话;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(510, 49);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "本人电话 ";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem44.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem44.Control = this.uc体温;
            this.layoutControlItem44.CustomizationFormText = "体重";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 146);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(189, 24);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "体温";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.Control = this.textEdit一般健康情况;
            this.layoutControlItem16.CustomizationFormText = "尿 蛋 白";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 170);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "一般健康情况";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.Control = this.textEdit一般心理状况;
            this.layoutControlItem10.CustomizationFormText = "胎位";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 194);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(81, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "一般心理状况";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem46.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem46.Control = this.uc血压;
            this.layoutControlItem46.CustomizationFormText = "血压";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 218);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(350, 0);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(131, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "血压";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.Control = this.flowLayoutPanel2;
            this.layoutControlItem9.CustomizationFormText = "乳 房";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 242);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "乳 房";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.Control = this.flowLayoutPanel3;
            this.layoutControlItem18.CustomizationFormText = "恶 露";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 266);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "恶 露";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem20.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem20.Control = this.flowLayoutPanel4;
            this.layoutControlItem20.CustomizationFormText = "子 宫";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 290);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "子 宫";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.Control = this.flowLayoutPanel5;
            this.layoutControlItem21.CustomizationFormText = "伤 口";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 314);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "伤 口";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.Control = this.textEdit其他;
            this.layoutControlItem23.CustomizationFormText = "其 他";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 338);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(650, 24);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "其 他";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.Control = this.flowLayoutPanel6;
            this.layoutControlItem24.CustomizationFormText = "分 类";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 362);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "分 类";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem76
            // 
            this.layoutControlItem76.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem76.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem76.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem76.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem76.Control = this.flow转诊;
            this.layoutControlItem76.CustomizationFormText = "转诊";
            this.layoutControlItem76.Location = new System.Drawing.Point(0, 436);
            this.layoutControlItem76.MinSize = new System.Drawing.Size(202, 60);
            this.layoutControlItem76.Name = "layoutControlItem76";
            this.layoutControlItem76.Size = new System.Drawing.Size(729, 60);
            this.layoutControlItem76.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem76.Text = "转诊";
            this.layoutControlItem76.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem76.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem76.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.Control = this.cbo产次;
            this.layoutControlItem6.CustomizationFormText = "产  次";
            this.layoutControlItem6.Location = new System.Drawing.Point(350, 218);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(180, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(379, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "产  次";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.Control = this.date分娩日期;
            this.layoutControlItem13.CustomizationFormText = "分娩日期";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(250, 0);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(149, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "分娩日期";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.date出院日期;
            this.layoutControlItem14.CustomizationFormText = "出院日期";
            this.layoutControlItem14.Location = new System.Drawing.Point(250, 122);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem14.Text = "出院日期";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.layoutControl2;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(510, 122);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.txt居民或家属签名;
            this.layoutControlItem17.CustomizationFormText = "居民/家属签名";
            this.layoutControlItem17.Location = new System.Drawing.Point(455, 496);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(274, 25);
            this.layoutControlItem17.Text = "居民/家属签名";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.txt失访原因;
            this.layoutControlItem19.CustomizationFormText = "失访原因";
            this.layoutControlItem19.Location = new System.Drawing.Point(417, 97);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(312, 25);
            this.layoutControlItem19.Text = "失访原因";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.ch失访情况;
            this.layoutControlItem32.CustomizationFormText = "失访情况";
            this.layoutControlItem32.Location = new System.Drawing.Point(250, 97);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(167, 25);
            this.layoutControlItem32.Text = "失访情况";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // UC产后访视记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC产后访视记录表";
            this.Size = new System.Drawing.Size(748, 441);
            this.Load += new System.EventHandler(this.UC产后访视记录表_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch失访情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt失访原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民或家属签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date出院日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date出院日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date分娩日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date分娩日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo产次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit其他.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo分类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit分类.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo恶露.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit恶露.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo伤口.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit伤口.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo子宫.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit子宫.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo乳房.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit乳房.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit一般健康情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit一般心理状况.Properties)).EndInit();
            this.flow转诊.ResumeLayout(false);
            this.flow转诊.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo转诊有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo是否到位.Properties)).EndInit();
            this.flow指导.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit个人卫生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit心理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit膳食.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit母乳喂养.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit新生儿护理与喂养.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit textEdit联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit居住地址;
        private DevExpress.XtraEditors.TextEdit textEdit居住状态;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit孕妇姓名;
        private DevExpress.XtraEditors.TextEdit textEdit档案编号;
        private DevExpress.XtraEditors.TextEdit textEdit卡号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraEditors.TextEdit textEdit创建人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近更新时间;
        private DevExpress.XtraEditors.TextEdit textEdit创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private Library.UserControls.UCTxtLblTxtLbl uc血压;
        private Library.UserControls.UCTxtLbl uc体温;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private System.Windows.Forms.FlowLayoutPanel flow转诊;
        private DevExpress.XtraEditors.ComboBoxEdit cbo转诊有无;
        private Library.UserControls.UCLblTxt uc转诊原因;
        private Library.UserControls.UCLblTxt uc转诊机构;
        private System.Windows.Forms.FlowLayoutPanel flow指导;
        private DevExpress.XtraEditors.CheckEdit checkEdit个人卫生;
        private DevExpress.XtraEditors.CheckEdit checkEdit心理;
        private DevExpress.XtraEditors.CheckEdit checkEdit膳食;
        private DevExpress.XtraEditors.CheckEdit checkEdit母乳喂养;
        private DevExpress.XtraEditors.CheckEdit checkEdit其他;
        private DevExpress.XtraEditors.TextEdit textEdit指导其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem75;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem76;
        private DevExpress.XtraEditors.DateEdit dte下次随访日期;
        private DevExpress.XtraEditors.DateEdit dte随访日期;
        private DevExpress.XtraEditors.TextEdit textEdit一般健康情况;
        private DevExpress.XtraEditors.TextEdit textEdit一般心理状况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.TextEdit textEdit其他;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.ComboBoxEdit cbo分类;
        private DevExpress.XtraEditors.TextEdit textEdit分类;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.ComboBoxEdit cbo恶露;
        private DevExpress.XtraEditors.TextEdit textEdit恶露;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.ComboBoxEdit cbo伤口;
        private DevExpress.XtraEditors.TextEdit textEdit伤口;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.ComboBoxEdit cbo子宫;
        private DevExpress.XtraEditors.TextEdit textEdit子宫;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.ComboBoxEdit cbo乳房;
        private DevExpress.XtraEditors.TextEdit textEdit乳房;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraEditors.ComboBoxEdit cbo产次;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.TextEdit txt居民或家属签名;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.DateEdit date出院日期;
        private DevExpress.XtraEditors.DateEdit date分娩日期;
        private System.Windows.Forms.Label label1;
        private Library.UserControls.UCLblTxt uc联系人;
        private Library.UserControls.UCLblTxt uc联系方式;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraEditors.CheckEdit checkEdit新生儿护理与喂养;
        private DevExpress.XtraEditors.ComboBoxEdit cbo是否到位;
        private DevExpress.XtraEditors.CheckEdit ch失访情况;
        private DevExpress.XtraEditors.TextEdit txt失访原因;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
    }
}
