﻿namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    partial class roport第2至5次产前随访服务记录表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次随访医生签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次随访医生签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访医生签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访医生签名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次下次随访日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次下次随访日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次下次随访日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次下次随访日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次转诊机构及科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次有无转诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次转诊机构及科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次有无转诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次转诊机构及科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次有无转诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次转诊机构及科室 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel74 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次有无转诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次产前随访指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次产前随访指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次产前随访指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次产前随访指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次产前随访指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次产前随访指导7 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次产前随访指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次产前随访指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次产前随访指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次产前随访指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次产前随访指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次产前随访指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次产前随访指导8 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次产前随访指导7 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次产前随访指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次产前随访指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次产前随访指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次产前随访指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次产前随访指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次产前随访指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次产前随访指导8 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次产前随访指导7 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次产前随访指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次产前随访指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次产前随访指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次产前随访指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次产前随访指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次产前随访指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次分类异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次分类 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次分类异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次分类 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次分类异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次分类 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次分类异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次分类 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次其他辅助检查 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次其他辅助检查 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次其他辅助检查 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次其他辅助检查 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次尿蛋白 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次尿蛋白 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次尿蛋白 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次尿蛋白 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次血红蛋白 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次血红蛋白 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次血红蛋白 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次血红蛋白 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次血压2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次血压1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次血压2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次血压1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次血压2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次血压1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次血压2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel87 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第5次血压1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次胎心率 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次胎心率 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次胎心率 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次胎心率 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次胎位 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次胎位 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次胎位 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次胎位 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次腹围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次腹围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次腹围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次腹围 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次宫底高度 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次宫底高度 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次宫底高度 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次宫底高度 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次主诉 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次主诉 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次主诉 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次主诉 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次体重 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次体重 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次体重 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次体重 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次孕周 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次孕周 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次孕周 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次孕周 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt第4次随访日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第5次随访日期 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt姓名,
            this.xrTable35,
            this.xrTable36,
            this.xrTable33,
            this.xrTable34,
            this.xrTable31,
            this.xrTable32,
            this.xrTable29,
            this.xrTable30,
            this.xrTable27,
            this.xrTable28,
            this.xrTable25,
            this.xrTable26,
            this.xrTable23,
            this.xrTable24,
            this.xrTable21,
            this.xrTable22,
            this.xrTable19,
            this.xrTable20,
            this.xrTable17,
            this.xrTable18,
            this.xrTable15,
            this.xrTable16,
            this.xrTable13,
            this.xrTable14,
            this.xrTable12,
            this.xrTable11,
            this.xrLabel3,
            this.xrTable9,
            this.xrTable10,
            this.xrTable7,
            this.xrTable8,
            this.xrTable5,
            this.xrTable6,
            this.xrTable3,
            this.xrTable4,
            this.xrTable2,
            this.xrTable1,
            this.xrLabel4,
            this.xrLabel5,
            this.xrLabel6,
            this.xrLabel7,
            this.xrLabel8,
            this.xrLabel9,
            this.xrLabel10,
            this.xrLabel11,
            this.xrLabel12,
            this.xrLine1,
            this.xrLabel2,
            this.xrLabel1});
            this.Detail.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.Detail.HeightF = 1350F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt姓名
            // 
            this.txt姓名.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(80.83334F, 99.99997F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.txt姓名.StylePriority.UseFont = false;
            this.txt姓名.StylePriority.UseTextAlignment = false;
            this.txt姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable35
            // 
            this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable35.LocationFloat = new DevExpress.Utils.PointFloat(444.0007F, 1083.167F);
            this.xrTable35.Name = "xrTable35";
            this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTable35.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable35.StylePriority.UseBorders = false;
            this.xrTable35.StylePriority.UseTextAlignment = false;
            this.xrTable35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次随访医生签名,
            this.txt第5次随访医生签名});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // txt第4次随访医生签名
            // 
            this.txt第4次随访医生签名.Name = "txt第4次随访医生签名";
            this.txt第4次随访医生签名.Weight = 1.55D;
            // 
            // txt第5次随访医生签名
            // 
            this.txt第5次随访医生签名.Name = "txt第5次随访医生签名";
            this.txt第5次随访医生签名.Weight = 1.55D;
            // 
            // xrTable36
            // 
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.LocationFloat = new DevExpress.Utils.PointFloat(0.0002543131F, 1083.167F);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable36.SizeF = new System.Drawing.SizeF(444F, 35F);
            this.xrTable36.StylePriority.UseBorders = false;
            this.xrTable36.StylePriority.UseTextAlignment = false;
            this.xrTable36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell88,
            this.txt第2次随访医生签名,
            this.txt第3次随访医生签名});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.StylePriority.UseTextAlignment = false;
            this.xrTableCell88.Text = "随访医生签名";
            this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell88.Weight = 0.59999996062247973D;
            // 
            // txt第2次随访医生签名
            // 
            this.txt第2次随访医生签名.Name = "txt第2次随访医生签名";
            this.txt第2次随访医生签名.Weight = 0.59999996062247984D;
            // 
            // txt第3次随访医生签名
            // 
            this.txt第3次随访医生签名.Name = "txt第3次随访医生签名";
            this.txt第3次随访医生签名.Weight = 0.59999996062248D;
            // 
            // xrTable33
            // 
            this.xrTable33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable33.LocationFloat = new DevExpress.Utils.PointFloat(444.0007F, 1048.167F);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrTable33.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable33.StylePriority.UseBorders = false;
            this.xrTable33.StylePriority.UseTextAlignment = false;
            this.xrTable33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次下次随访日期,
            this.txt第5次下次随访日期});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // txt第4次下次随访日期
            // 
            this.txt第4次下次随访日期.Name = "txt第4次下次随访日期";
            this.txt第4次下次随访日期.Weight = 1.55D;
            // 
            // txt第5次下次随访日期
            // 
            this.txt第5次下次随访日期.Name = "txt第5次下次随访日期";
            this.txt第5次下次随访日期.Weight = 1.55D;
            // 
            // xrTable34
            // 
            this.xrTable34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable34.LocationFloat = new DevExpress.Utils.PointFloat(0.0002543131F, 1048.167F);
            this.xrTable34.Name = "xrTable34";
            this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrTable34.SizeF = new System.Drawing.SizeF(444F, 35F);
            this.xrTable34.StylePriority.UseBorders = false;
            this.xrTable34.StylePriority.UseTextAlignment = false;
            this.xrTable34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.txt第2次下次随访日期,
            this.txt第3次下次随访日期});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseTextAlignment = false;
            this.xrTableCell83.Text = "下次随访日期";
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell83.Weight = 0.59999996062247973D;
            // 
            // txt第2次下次随访日期
            // 
            this.txt第2次下次随访日期.Name = "txt第2次下次随访日期";
            this.txt第2次下次随访日期.Weight = 0.59999996062247984D;
            // 
            // txt第3次下次随访日期
            // 
            this.txt第3次下次随访日期.Name = "txt第3次下次随访日期";
            this.txt第3次下次随访日期.Weight = 0.59999996062248D;
            // 
            // xrTable31
            // 
            this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable31.LocationFloat = new DevExpress.Utils.PointFloat(0.000222524F, 928.1666F);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable31.SizeF = new System.Drawing.SizeF(444F, 120F);
            this.xrTable31.StylePriority.UseBorders = false;
            this.xrTable31.StylePriority.UseTextAlignment = false;
            this.xrTable31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTableCell77,
            this.xrTableCell78});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.Text = "  转      诊";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell76.Weight = 0.59999996062247973D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次转诊机构及科室,
            this.xrLabel57,
            this.txt第2次转诊原因,
            this.xrLabel55,
            this.txt第2次有无转诊,
            this.xrLabel53});
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Weight = 0.59999996062247984D;
            // 
            // txt第2次转诊机构及科室
            // 
            this.txt第2次转诊机构及科室.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次转诊机构及科室.LocationFloat = new DevExpress.Utils.PointFloat(9.000015F, 78.99998F);
            this.txt第2次转诊机构及科室.Name = "txt第2次转诊机构及科室";
            this.txt第2次转诊机构及科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt第2次转诊机构及科室.SizeF = new System.Drawing.SizeF(130F, 25F);
            this.txt第2次转诊机构及科室.StylePriority.UseBorders = false;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(6.500128F, 56F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(90F, 23F);
            this.xrLabel57.StylePriority.UseBorders = false;
            this.xrLabel57.StylePriority.UseTextAlignment = false;
            this.xrLabel57.Text = "机构及科室：";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次转诊原因
            // 
            this.txt第2次转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(57.10813F, 32.99994F);
            this.txt第2次转诊原因.Name = "txt第2次转诊原因";
            this.txt第2次转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt第2次转诊原因.SizeF = new System.Drawing.SizeF(85F, 23F);
            this.txt第2次转诊原因.StylePriority.UseBorders = false;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(6.500117F, 32.99993F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel55.StylePriority.UseBorders = false;
            this.xrLabel55.StylePriority.UseTextAlignment = false;
            this.xrLabel55.Text = "原因：";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次有无转诊
            // 
            this.txt第2次有无转诊.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次有无转诊.CanGrow = false;
            this.txt第2次有无转诊.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次有无转诊.LocationFloat = new DevExpress.Utils.PointFloat(112.2917F, 13.99997F);
            this.txt第2次有无转诊.Name = "txt第2次有无转诊";
            this.txt第2次有无转诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次有无转诊.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次有无转诊.StylePriority.UseBorders = false;
            this.txt第2次有无转诊.StylePriority.UseFont = false;
            this.txt第2次有无转诊.StylePriority.UseTextAlignment = false;
            this.txt第2次有无转诊.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(6.500117F, 9.999974F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(97F, 23F);
            this.xrLabel53.StylePriority.UseBorders = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.Text = "1 有  2 无";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次转诊机构及科室,
            this.xrLabel60,
            this.txt第3次转诊原因,
            this.xrLabel62,
            this.txt第3次有无转诊,
            this.xrLabel64});
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.59999996062248D;
            // 
            // txt第3次转诊机构及科室
            // 
            this.txt第3次转诊机构及科室.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次转诊机构及科室.LocationFloat = new DevExpress.Utils.PointFloat(5.499991F, 79F);
            this.txt第3次转诊机构及科室.Name = "txt第3次转诊机构及科室";
            this.txt第3次转诊机构及科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次转诊机构及科室.SizeF = new System.Drawing.SizeF(130F, 25F);
            this.txt第3次转诊机构及科室.StylePriority.UseBorders = false;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(5.499985F, 56.00001F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(90F, 23F);
            this.xrLabel60.StylePriority.UseBorders = false;
            this.xrLabel60.StylePriority.UseTextAlignment = false;
            this.xrLabel60.Text = "机构及科室：";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次转诊原因
            // 
            this.txt第3次转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(58.00011F, 32.99994F);
            this.txt第3次转诊原因.Name = "txt第3次转诊原因";
            this.txt第3次转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次转诊原因.SizeF = new System.Drawing.SizeF(85F, 23F);
            this.txt第3次转诊原因.StylePriority.UseBorders = false;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(5.49996F, 33F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel62.StylePriority.UseBorders = false;
            this.xrLabel62.StylePriority.UseTextAlignment = false;
            this.xrLabel62.Text = "原因：";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次有无转诊
            // 
            this.txt第3次有无转诊.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次有无转诊.CanGrow = false;
            this.txt第3次有无转诊.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次有无转诊.LocationFloat = new DevExpress.Utils.PointFloat(119F, 13.99994F);
            this.txt第3次有无转诊.Name = "txt第3次有无转诊";
            this.txt第3次有无转诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次有无转诊.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次有无转诊.StylePriority.UseBorders = false;
            this.txt第3次有无转诊.StylePriority.UseFont = false;
            this.txt第3次有无转诊.StylePriority.UseTextAlignment = false;
            this.txt第3次有无转诊.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(6.982645F, 10.00013F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(97F, 23F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "1 有  2 无";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable32
            // 
            this.xrTable32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable32.LocationFloat = new DevExpress.Utils.PointFloat(444.0005F, 928.1666F);
            this.xrTable32.Name = "xrTable32";
            this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.xrTable32.SizeF = new System.Drawing.SizeF(296F, 120F);
            this.xrTable32.StylePriority.UseBorders = false;
            this.xrTable32.StylePriority.UseTextAlignment = false;
            this.xrTable32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次转诊机构及科室,
            this.xrLabel66,
            this.txt第4次转诊原因,
            this.xrLabel68,
            this.txt第4次有无转诊,
            this.xrLabel70});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Weight = 1.55D;
            // 
            // txt第4次转诊机构及科室
            // 
            this.txt第4次转诊机构及科室.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次转诊机构及科室.LocationFloat = new DevExpress.Utils.PointFloat(5.499908F, 78.99994F);
            this.txt第4次转诊机构及科室.Name = "txt第4次转诊机构及科室";
            this.txt第4次转诊机构及科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次转诊机构及科室.SizeF = new System.Drawing.SizeF(130F, 25F);
            this.txt第4次转诊机构及科室.StylePriority.UseBorders = false;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(5.499985F, 56.00001F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(90F, 23F);
            this.xrLabel66.StylePriority.UseBorders = false;
            this.xrLabel66.Text = "机构及科室：";
            // 
            // txt第4次转诊原因
            // 
            this.txt第4次转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(55.98471F, 32.99994F);
            this.txt第4次转诊原因.Name = "txt第4次转诊原因";
            this.txt第4次转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次转诊原因.SizeF = new System.Drawing.SizeF(85F, 23F);
            this.txt第4次转诊原因.StylePriority.UseBorders = false;
            // 
            // xrLabel68
            // 
            this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(5.500031F, 33F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel68.StylePriority.UseBorders = false;
            this.xrLabel68.Text = "原因：";
            // 
            // txt第4次有无转诊
            // 
            this.txt第4次有无转诊.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次有无转诊.CanGrow = false;
            this.txt第4次有无转诊.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次有无转诊.LocationFloat = new DevExpress.Utils.PointFloat(119.0003F, 14F);
            this.txt第4次有无转诊.Name = "txt第4次有无转诊";
            this.txt第4次有无转诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次有无转诊.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次有无转诊.StylePriority.UseBorders = false;
            this.txt第4次有无转诊.StylePriority.UseFont = false;
            this.txt第4次有无转诊.StylePriority.UseTextAlignment = false;
            this.txt第4次有无转诊.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel70
            // 
            this.xrLabel70.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(5.499985F, 10.00001F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(97F, 23F);
            this.xrLabel70.StylePriority.UseBorders = false;
            this.xrLabel70.StylePriority.UseTextAlignment = false;
            this.xrLabel70.Text = "1 有  2 无";
            this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第5次转诊机构及科室,
            this.xrLabel72,
            this.txt第5次转诊原因,
            this.xrLabel74,
            this.txt第5次有无转诊,
            this.xrLabel76});
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Weight = 1.55D;
            // 
            // txt第5次转诊机构及科室
            // 
            this.txt第5次转诊机构及科室.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第5次转诊机构及科室.LocationFloat = new DevExpress.Utils.PointFloat(5.5F, 79F);
            this.txt第5次转诊机构及科室.Name = "txt第5次转诊机构及科室";
            this.txt第5次转诊机构及科室.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次转诊机构及科室.SizeF = new System.Drawing.SizeF(130F, 25F);
            this.txt第5次转诊机构及科室.StylePriority.UseBorders = false;
            // 
            // xrLabel72
            // 
            this.xrLabel72.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(5.499985F, 56.00001F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(90F, 23F);
            this.xrLabel72.StylePriority.UseBorders = false;
            this.xrLabel72.Text = "机构及科室：";
            // 
            // txt第5次转诊原因
            // 
            this.txt第5次转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第5次转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(58.49994F, 32.99994F);
            this.txt第5次转诊原因.Name = "txt第5次转诊原因";
            this.txt第5次转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次转诊原因.SizeF = new System.Drawing.SizeF(85F, 23F);
            this.txt第5次转诊原因.StylePriority.UseBorders = false;
            // 
            // xrLabel74
            // 
            this.xrLabel74.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel74.LocationFloat = new DevExpress.Utils.PointFloat(5.5F, 33F);
            this.xrLabel74.Name = "xrLabel74";
            this.xrLabel74.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel74.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel74.StylePriority.UseBorders = false;
            this.xrLabel74.Text = "原因：";
            // 
            // txt第5次有无转诊
            // 
            this.txt第5次有无转诊.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第5次有无转诊.CanGrow = false;
            this.txt第5次有无转诊.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第5次有无转诊.LocationFloat = new DevExpress.Utils.PointFloat(115.9997F, 14.00001F);
            this.txt第5次有无转诊.Name = "txt第5次有无转诊";
            this.txt第5次有无转诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次有无转诊.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第5次有无转诊.StylePriority.UseBorders = false;
            this.txt第5次有无转诊.StylePriority.UseFont = false;
            this.txt第5次有无转诊.StylePriority.UseTextAlignment = false;
            this.txt第5次有无转诊.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(6.49999F, 9.999974F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(97F, 23F);
            this.xrLabel76.StylePriority.UseBorders = false;
            this.xrLabel76.StylePriority.UseTextAlignment = false;
            this.xrLabel76.Text = "1 有  2 无";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable29
            // 
            this.xrTable29.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable29.LocationFloat = new DevExpress.Utils.PointFloat(0.0004132589F, 717.9999F);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTable29.SizeF = new System.Drawing.SizeF(444F, 210.1667F);
            this.xrTable29.StylePriority.UseBorders = false;
            this.xrTable29.StylePriority.UseTextAlignment = false;
            this.xrTable29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseTextAlignment = false;
            this.xrTableCell71.Text = "  指      导";
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell71.Weight = 0.59999996062247973D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次产前随访指导5,
            this.txt第2次产前随访指导4,
            this.txt第2次产前随访指导3,
            this.txt第2次产前随访指导2,
            this.txt第2次产前随访指导1,
            this.txt第2次指导其他,
            this.xrLabel33,
            this.xrLabel32,
            this.xrLabel31,
            this.xrLabel30,
            this.xrLabel29});
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Weight = 0.59999996062247984D;
            // 
            // txt第2次产前随访指导5
            // 
            this.txt第2次产前随访指导5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次产前随访指导5.CanGrow = false;
            this.txt第2次产前随访指导5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次产前随访指导5.LocationFloat = new DevExpress.Utils.PointFloat(112.2915F, 84.99994F);
            this.txt第2次产前随访指导5.Name = "txt第2次产前随访指导5";
            this.txt第2次产前随访指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次产前随访指导5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次产前随访指导5.StylePriority.UseBorders = false;
            this.txt第2次产前随访指导5.StylePriority.UseFont = false;
            this.txt第2次产前随访指导5.StylePriority.UseTextAlignment = false;
            this.txt第2次产前随访指导5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次产前随访指导4
            // 
            this.txt第2次产前随访指导4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次产前随访指导4.CanGrow = false;
            this.txt第2次产前随访指导4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次产前随访指导4.LocationFloat = new DevExpress.Utils.PointFloat(112.2915F, 64.49995F);
            this.txt第2次产前随访指导4.Name = "txt第2次产前随访指导4";
            this.txt第2次产前随访指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次产前随访指导4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次产前随访指导4.StylePriority.UseBorders = false;
            this.txt第2次产前随访指导4.StylePriority.UseFont = false;
            this.txt第2次产前随访指导4.StylePriority.UseTextAlignment = false;
            this.txt第2次产前随访指导4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次产前随访指导3
            // 
            this.txt第2次产前随访指导3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次产前随访指导3.CanGrow = false;
            this.txt第2次产前随访指导3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次产前随访指导3.LocationFloat = new DevExpress.Utils.PointFloat(112.2915F, 43.99997F);
            this.txt第2次产前随访指导3.Name = "txt第2次产前随访指导3";
            this.txt第2次产前随访指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次产前随访指导3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次产前随访指导3.StylePriority.UseBorders = false;
            this.txt第2次产前随访指导3.StylePriority.UseFont = false;
            this.txt第2次产前随访指导3.StylePriority.UseTextAlignment = false;
            this.txt第2次产前随访指导3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次产前随访指导2
            // 
            this.txt第2次产前随访指导2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次产前随访指导2.CanGrow = false;
            this.txt第2次产前随访指导2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次产前随访指导2.LocationFloat = new DevExpress.Utils.PointFloat(112.2915F, 23.49999F);
            this.txt第2次产前随访指导2.Name = "txt第2次产前随访指导2";
            this.txt第2次产前随访指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次产前随访指导2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次产前随访指导2.StylePriority.UseBorders = false;
            this.txt第2次产前随访指导2.StylePriority.UseFont = false;
            this.txt第2次产前随访指导2.StylePriority.UseTextAlignment = false;
            this.txt第2次产前随访指导2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次产前随访指导1
            // 
            this.txt第2次产前随访指导1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次产前随访指导1.CanGrow = false;
            this.txt第2次产前随访指导1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次产前随访指导1.LocationFloat = new DevExpress.Utils.PointFloat(112.2915F, 3.000007F);
            this.txt第2次产前随访指导1.Name = "txt第2次产前随访指导1";
            this.txt第2次产前随访指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次产前随访指导1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次产前随访指导1.StylePriority.UseBorders = false;
            this.txt第2次产前随访指导1.StylePriority.UseFont = false;
            this.txt第2次产前随访指导1.StylePriority.UseTextAlignment = false;
            this.txt第2次产前随访指导1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次指导其他
            // 
            this.txt第2次指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次指导其他.LocationFloat = new DevExpress.Utils.PointFloat(6.500044F, 103.0001F);
            this.txt第2次指导其他.Name = "txt第2次指导其他";
            this.txt第2次指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt第2次指导其他.SizeF = new System.Drawing.SizeF(135F, 45F);
            this.txt第2次指导其他.StylePriority.UseBorders = false;
            this.txt第2次指导其他.StylePriority.UseTextAlignment = false;
            this.txt第2次指导其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(6.499907F, 80F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(53.49971F, 23.00006F);
            this.xrLabel33.StylePriority.UseBorders = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "5 其他";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(6.500053F, 59.99991F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel32.StylePriority.UseBorders = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "4 运动";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(6.49999F, 39.99984F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel31.StylePriority.UseBorders = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "3 心理";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(6.499926F, 19.99995F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "2 膳食";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(6.499926F, 6.357829E-05F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(80.9653F, 20F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "1 个人卫生";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次产前随访指导7,
            this.txt第3次产前随访指导6,
            this.txt第3次产前随访指导5,
            this.txt第3次产前随访指导4,
            this.txt第3次产前随访指导3,
            this.txt第3次产前随访指导2,
            this.txt第3次产前随访指导1,
            this.xrLabel16,
            this.xrLabel14,
            this.txt第3次指导其他,
            this.xrLabel36,
            this.xrLabel37,
            this.xrLabel38,
            this.xrLabel39,
            this.xrLabel40});
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Weight = 0.59999996062248D;
            // 
            // txt第3次产前随访指导7
            // 
            this.txt第3次产前随访指导7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次产前随访指导7.CanGrow = false;
            this.txt第3次产前随访指导7.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次产前随访指导7.LocationFloat = new DevExpress.Utils.PointFloat(118.9999F, 122.9998F);
            this.txt第3次产前随访指导7.Name = "txt第3次产前随访指导7";
            this.txt第3次产前随访指导7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次产前随访指导7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次产前随访指导7.StylePriority.UseBorders = false;
            this.txt第3次产前随访指导7.StylePriority.UseFont = false;
            this.txt第3次产前随访指导7.StylePriority.UseTextAlignment = false;
            this.txt第3次产前随访指导7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次产前随访指导6
            // 
            this.txt第3次产前随访指导6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次产前随访指导6.CanGrow = false;
            this.txt第3次产前随访指导6.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次产前随访指导6.LocationFloat = new DevExpress.Utils.PointFloat(118.9997F, 103F);
            this.txt第3次产前随访指导6.Name = "txt第3次产前随访指导6";
            this.txt第3次产前随访指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次产前随访指导6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次产前随访指导6.StylePriority.UseBorders = false;
            this.txt第3次产前随访指导6.StylePriority.UseFont = false;
            this.txt第3次产前随访指导6.StylePriority.UseTextAlignment = false;
            this.txt第3次产前随访指导6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次产前随访指导5
            // 
            this.txt第3次产前随访指导5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次产前随访指导5.CanGrow = false;
            this.txt第3次产前随访指导5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次产前随访指导5.LocationFloat = new DevExpress.Utils.PointFloat(118.9998F, 84.50008F);
            this.txt第3次产前随访指导5.Name = "txt第3次产前随访指导5";
            this.txt第3次产前随访指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次产前随访指导5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次产前随访指导5.StylePriority.UseBorders = false;
            this.txt第3次产前随访指导5.StylePriority.UseFont = false;
            this.txt第3次产前随访指导5.StylePriority.UseTextAlignment = false;
            this.txt第3次产前随访指导5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次产前随访指导4
            // 
            this.txt第3次产前随访指导4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次产前随访指导4.CanGrow = false;
            this.txt第3次产前随访指导4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次产前随访指导4.LocationFloat = new DevExpress.Utils.PointFloat(118.9998F, 64.00002F);
            this.txt第3次产前随访指导4.Name = "txt第3次产前随访指导4";
            this.txt第3次产前随访指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次产前随访指导4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次产前随访指导4.StylePriority.UseBorders = false;
            this.txt第3次产前随访指导4.StylePriority.UseFont = false;
            this.txt第3次产前随访指导4.StylePriority.UseTextAlignment = false;
            this.txt第3次产前随访指导4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次产前随访指导3
            // 
            this.txt第3次产前随访指导3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次产前随访指导3.CanGrow = false;
            this.txt第3次产前随访指导3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次产前随访指导3.LocationFloat = new DevExpress.Utils.PointFloat(118.9998F, 43.50002F);
            this.txt第3次产前随访指导3.Name = "txt第3次产前随访指导3";
            this.txt第3次产前随访指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次产前随访指导3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次产前随访指导3.StylePriority.UseBorders = false;
            this.txt第3次产前随访指导3.StylePriority.UseFont = false;
            this.txt第3次产前随访指导3.StylePriority.UseTextAlignment = false;
            this.txt第3次产前随访指导3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次产前随访指导2
            // 
            this.txt第3次产前随访指导2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次产前随访指导2.CanGrow = false;
            this.txt第3次产前随访指导2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次产前随访指导2.LocationFloat = new DevExpress.Utils.PointFloat(118.9998F, 23.00003F);
            this.txt第3次产前随访指导2.Name = "txt第3次产前随访指导2";
            this.txt第3次产前随访指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次产前随访指导2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次产前随访指导2.StylePriority.UseBorders = false;
            this.txt第3次产前随访指导2.StylePriority.UseFont = false;
            this.txt第3次产前随访指导2.StylePriority.UseTextAlignment = false;
            this.txt第3次产前随访指导2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次产前随访指导1
            // 
            this.txt第3次产前随访指导1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次产前随访指导1.CanGrow = false;
            this.txt第3次产前随访指导1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次产前随访指导1.LocationFloat = new DevExpress.Utils.PointFloat(118.9998F, 2.50004F);
            this.txt第3次产前随访指导1.Name = "txt第3次产前随访指导1";
            this.txt第3次产前随访指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次产前随访指导1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次产前随访指导1.StylePriority.UseBorders = false;
            this.txt第3次产前随访指导1.StylePriority.UseFont = false;
            this.txt第3次产前随访指导1.StylePriority.UseTextAlignment = false;
            this.txt第3次产前随访指导1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(6.499922F, 99.99994F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(77.50003F, 20F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "6 母乳喂养";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(6.982466F, 79.99994F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(77.01749F, 20F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "5 自我监护";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次指导其他
            // 
            this.txt第3次指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次指导其他.LocationFloat = new DevExpress.Utils.PointFloat(6.982497F, 142.9998F);
            this.txt第3次指导其他.Name = "txt第3次指导其他";
            this.txt第3次指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次指导其他.SizeF = new System.Drawing.SizeF(131.9655F, 45F);
            this.txt第3次指导其他.StylePriority.UseBorders = false;
            this.txt第3次指导其他.StylePriority.UseTextAlignment = false;
            this.txt第3次指导其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(6.499922F, 119.9999F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(56.53439F, 22.99994F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "7 其他";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(6.499895F, 59.99985F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel37.StylePriority.UseBorders = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "4 运动";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(6.982454F, 39.99984F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "3 心理";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(6.982454F, 19.99995F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel39.StylePriority.UseBorders = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "2 膳食";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(6.982466F, 0F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(80.48276F, 20F);
            this.xrLabel40.StylePriority.UseBorders = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "1 个人卫生";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable30
            // 
            this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable30.LocationFloat = new DevExpress.Utils.PointFloat(444.0005F, 717.9999F);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrTable30.SizeF = new System.Drawing.SizeF(296F, 210.1667F);
            this.xrTable30.StylePriority.UseBorders = false;
            this.xrTable30.StylePriority.UseTextAlignment = false;
            this.xrTable30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell74,
            this.xrTableCell75});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次产前随访指导8,
            this.txt第4次产前随访指导7,
            this.txt第4次产前随访指导6,
            this.txt第4次产前随访指导5,
            this.txt第4次产前随访指导4,
            this.txt第4次产前随访指导3,
            this.txt第4次产前随访指导2,
            this.txt第4次产前随访指导1,
            this.xrLabel23,
            this.xrLabel20,
            this.xrLabel19,
            this.xrLabel42,
            this.xrLabel43,
            this.xrLabel44,
            this.xrLabel45,
            this.xrLabel46,
            this.txt第4次指导其他});
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Weight = 1.55D;
            // 
            // txt第4次产前随访指导8
            // 
            this.txt第4次产前随访指导8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次产前随访指导8.CanGrow = false;
            this.txt第4次产前随访指导8.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次产前随访指导8.LocationFloat = new DevExpress.Utils.PointFloat(119.0002F, 140.9997F);
            this.txt第4次产前随访指导8.Name = "txt第4次产前随访指导8";
            this.txt第4次产前随访指导8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次产前随访指导8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次产前随访指导8.StylePriority.UseBorders = false;
            this.txt第4次产前随访指导8.StylePriority.UseFont = false;
            this.txt第4次产前随访指导8.StylePriority.UseTextAlignment = false;
            this.txt第4次产前随访指导8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次产前随访指导7
            // 
            this.txt第4次产前随访指导7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次产前随访指导7.CanGrow = false;
            this.txt第4次产前随访指导7.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次产前随访指导7.LocationFloat = new DevExpress.Utils.PointFloat(119.0003F, 121.2141F);
            this.txt第4次产前随访指导7.Name = "txt第4次产前随访指导7";
            this.txt第4次产前随访指导7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次产前随访指导7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次产前随访指导7.StylePriority.UseBorders = false;
            this.txt第4次产前随访指导7.StylePriority.UseFont = false;
            this.txt第4次产前随访指导7.StylePriority.UseTextAlignment = false;
            this.txt第4次产前随访指导7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次产前随访指导6
            // 
            this.txt第4次产前随访指导6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次产前随访指导6.CanGrow = false;
            this.txt第4次产前随访指导6.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次产前随访指导6.LocationFloat = new DevExpress.Utils.PointFloat(119.0002F, 101.4284F);
            this.txt第4次产前随访指导6.Name = "txt第4次产前随访指导6";
            this.txt第4次产前随访指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次产前随访指导6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次产前随访指导6.StylePriority.UseBorders = false;
            this.txt第4次产前随访指导6.StylePriority.UseFont = false;
            this.txt第4次产前随访指导6.StylePriority.UseTextAlignment = false;
            this.txt第4次产前随访指导6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次产前随访指导5
            // 
            this.txt第4次产前随访指导5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次产前随访指导5.CanGrow = false;
            this.txt第4次产前随访指导5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次产前随访指导5.LocationFloat = new DevExpress.Utils.PointFloat(119.0002F, 81.64271F);
            this.txt第4次产前随访指导5.Name = "txt第4次产前随访指导5";
            this.txt第4次产前随访指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次产前随访指导5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次产前随访指导5.StylePriority.UseBorders = false;
            this.txt第4次产前随访指导5.StylePriority.UseFont = false;
            this.txt第4次产前随访指导5.StylePriority.UseTextAlignment = false;
            this.txt第4次产前随访指导5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次产前随访指导4
            // 
            this.txt第4次产前随访指导4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次产前随访指导4.CanGrow = false;
            this.txt第4次产前随访指导4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次产前随访指导4.LocationFloat = new DevExpress.Utils.PointFloat(119.0002F, 61.85703F);
            this.txt第4次产前随访指导4.Name = "txt第4次产前随访指导4";
            this.txt第4次产前随访指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次产前随访指导4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次产前随访指导4.StylePriority.UseBorders = false;
            this.txt第4次产前随访指导4.StylePriority.UseFont = false;
            this.txt第4次产前随访指导4.StylePriority.UseTextAlignment = false;
            this.txt第4次产前随访指导4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次产前随访指导3
            // 
            this.txt第4次产前随访指导3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次产前随访指导3.CanGrow = false;
            this.txt第4次产前随访指导3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次产前随访指导3.LocationFloat = new DevExpress.Utils.PointFloat(119.0002F, 42.07136F);
            this.txt第4次产前随访指导3.Name = "txt第4次产前随访指导3";
            this.txt第4次产前随访指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次产前随访指导3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次产前随访指导3.StylePriority.UseBorders = false;
            this.txt第4次产前随访指导3.StylePriority.UseFont = false;
            this.txt第4次产前随访指导3.StylePriority.UseTextAlignment = false;
            this.txt第4次产前随访指导3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次产前随访指导2
            // 
            this.txt第4次产前随访指导2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次产前随访指导2.CanGrow = false;
            this.txt第4次产前随访指导2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次产前随访指导2.LocationFloat = new DevExpress.Utils.PointFloat(119.0002F, 22.28568F);
            this.txt第4次产前随访指导2.Name = "txt第4次产前随访指导2";
            this.txt第4次产前随访指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次产前随访指导2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次产前随访指导2.StylePriority.UseBorders = false;
            this.txt第4次产前随访指导2.StylePriority.UseFont = false;
            this.txt第4次产前随访指导2.StylePriority.UseTextAlignment = false;
            this.txt第4次产前随访指导2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次产前随访指导1
            // 
            this.txt第4次产前随访指导1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次产前随访指导1.CanGrow = false;
            this.txt第4次产前随访指导1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次产前随访指导1.LocationFloat = new DevExpress.Utils.PointFloat(119.0002F, 2.500008F);
            this.txt第4次产前随访指导1.Name = "txt第4次产前随访指导1";
            this.txt第4次产前随访指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次产前随访指导1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次产前随访指导1.StylePriority.UseBorders = false;
            this.txt第4次产前随访指导1.StylePriority.UseFont = false;
            this.txt第4次产前随访指导1.StylePriority.UseTextAlignment = false;
            this.txt第4次产前随访指导1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(6.500061F, 119.9999F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(77.50024F, 20F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "7 母乳喂养";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(6.499939F, 100F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(77.50031F, 20F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "6 分娩准备";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(6.499939F, 80F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(77.50031F, 20F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "5 自我监测";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(6.500053F, 140.0001F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(52.96924F, 23F);
            this.xrLabel42.StylePriority.UseBorders = false;
            this.xrLabel42.StylePriority.UseTextAlignment = false;
            this.xrLabel42.Text = "8 其他";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(6.499926F, 59.99994F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "4 运动";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel44
            // 
            this.xrLabel44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(6.500053F, 39.99977F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel44.StylePriority.UseBorders = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = "3 心理";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(6.499926F, 20.00004F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "2 膳食";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(6.499939F, 0.0001220703F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(80.96527F, 20F);
            this.xrLabel46.StylePriority.UseBorders = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "1 个人卫生";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第4次指导其他
            // 
            this.txt第4次指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次指导其他.LocationFloat = new DevExpress.Utils.PointFloat(6.500061F, 163.0001F);
            this.txt第4次指导其他.Name = "txt第4次指导其他";
            this.txt第4次指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次指导其他.SizeF = new System.Drawing.SizeF(133.5151F, 45F);
            this.txt第4次指导其他.StylePriority.UseBorders = false;
            this.txt第4次指导其他.StylePriority.UseTextAlignment = false;
            this.txt第4次指导其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第5次产前随访指导8,
            this.txt第5次产前随访指导7,
            this.txt第5次产前随访指导6,
            this.txt第5次产前随访指导5,
            this.txt第5次产前随访指导4,
            this.txt第5次产前随访指导3,
            this.txt第5次产前随访指导2,
            this.txt第5次产前随访指导1,
            this.xrLabel24,
            this.xrLabel27,
            this.xrLabel28,
            this.xrLabel48,
            this.xrLabel49,
            this.xrLabel50,
            this.xrLabel51,
            this.xrLabel52,
            this.txt第5次指导其他});
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Weight = 1.55D;
            // 
            // txt第5次产前随访指导8
            // 
            this.txt第5次产前随访指导8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第5次产前随访指导8.CanGrow = false;
            this.txt第5次产前随访指导8.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第5次产前随访指导8.LocationFloat = new DevExpress.Utils.PointFloat(115.9997F, 143.1427F);
            this.txt第5次产前随访指导8.Name = "txt第5次产前随访指导8";
            this.txt第5次产前随访指导8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次产前随访指导8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第5次产前随访指导8.StylePriority.UseBorders = false;
            this.txt第5次产前随访指导8.StylePriority.UseFont = false;
            this.txt第5次产前随访指导8.StylePriority.UseTextAlignment = false;
            this.txt第5次产前随访指导8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第5次产前随访指导7
            // 
            this.txt第5次产前随访指导7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第5次产前随访指导7.CanGrow = false;
            this.txt第5次产前随访指导7.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第5次产前随访指导7.LocationFloat = new DevExpress.Utils.PointFloat(115.9998F, 123.357F);
            this.txt第5次产前随访指导7.Name = "txt第5次产前随访指导7";
            this.txt第5次产前随访指导7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次产前随访指导7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第5次产前随访指导7.StylePriority.UseBorders = false;
            this.txt第5次产前随访指导7.StylePriority.UseFont = false;
            this.txt第5次产前随访指导7.StylePriority.UseTextAlignment = false;
            this.txt第5次产前随访指导7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第5次产前随访指导6
            // 
            this.txt第5次产前随访指导6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第5次产前随访指导6.CanGrow = false;
            this.txt第5次产前随访指导6.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第5次产前随访指导6.LocationFloat = new DevExpress.Utils.PointFloat(115.9997F, 103.5713F);
            this.txt第5次产前随访指导6.Name = "txt第5次产前随访指导6";
            this.txt第5次产前随访指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次产前随访指导6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第5次产前随访指导6.StylePriority.UseBorders = false;
            this.txt第5次产前随访指导6.StylePriority.UseFont = false;
            this.txt第5次产前随访指导6.StylePriority.UseTextAlignment = false;
            this.txt第5次产前随访指导6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第5次产前随访指导5
            // 
            this.txt第5次产前随访指导5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第5次产前随访指导5.CanGrow = false;
            this.txt第5次产前随访指导5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第5次产前随访指导5.LocationFloat = new DevExpress.Utils.PointFloat(115.9997F, 83.78564F);
            this.txt第5次产前随访指导5.Name = "txt第5次产前随访指导5";
            this.txt第5次产前随访指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次产前随访指导5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第5次产前随访指导5.StylePriority.UseBorders = false;
            this.txt第5次产前随访指导5.StylePriority.UseFont = false;
            this.txt第5次产前随访指导5.StylePriority.UseTextAlignment = false;
            this.txt第5次产前随访指导5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第5次产前随访指导4
            // 
            this.txt第5次产前随访指导4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第5次产前随访指导4.CanGrow = false;
            this.txt第5次产前随访指导4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第5次产前随访指导4.LocationFloat = new DevExpress.Utils.PointFloat(115.9997F, 63.99995F);
            this.txt第5次产前随访指导4.Name = "txt第5次产前随访指导4";
            this.txt第5次产前随访指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次产前随访指导4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第5次产前随访指导4.StylePriority.UseBorders = false;
            this.txt第5次产前随访指导4.StylePriority.UseFont = false;
            this.txt第5次产前随访指导4.StylePriority.UseTextAlignment = false;
            this.txt第5次产前随访指导4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第5次产前随访指导3
            // 
            this.txt第5次产前随访指导3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第5次产前随访指导3.CanGrow = false;
            this.txt第5次产前随访指导3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第5次产前随访指导3.LocationFloat = new DevExpress.Utils.PointFloat(115.9997F, 44.21426F);
            this.txt第5次产前随访指导3.Name = "txt第5次产前随访指导3";
            this.txt第5次产前随访指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次产前随访指导3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第5次产前随访指导3.StylePriority.UseBorders = false;
            this.txt第5次产前随访指导3.StylePriority.UseFont = false;
            this.txt第5次产前随访指导3.StylePriority.UseTextAlignment = false;
            this.txt第5次产前随访指导3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第5次产前随访指导2
            // 
            this.txt第5次产前随访指导2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第5次产前随访指导2.CanGrow = false;
            this.txt第5次产前随访指导2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第5次产前随访指导2.LocationFloat = new DevExpress.Utils.PointFloat(115.9997F, 24.42857F);
            this.txt第5次产前随访指导2.Name = "txt第5次产前随访指导2";
            this.txt第5次产前随访指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次产前随访指导2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第5次产前随访指导2.StylePriority.UseBorders = false;
            this.txt第5次产前随访指导2.StylePriority.UseFont = false;
            this.txt第5次产前随访指导2.StylePriority.UseTextAlignment = false;
            this.txt第5次产前随访指导2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第5次产前随访指导1
            // 
            this.txt第5次产前随访指导1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第5次产前随访指导1.CanGrow = false;
            this.txt第5次产前随访指导1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第5次产前随访指导1.LocationFloat = new DevExpress.Utils.PointFloat(115.9997F, 4.642946F);
            this.txt第5次产前随访指导1.Name = "txt第5次产前随访指导1";
            this.txt第5次产前随访指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次产前随访指导1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第5次产前随访指导1.StylePriority.UseBorders = false;
            this.txt第5次产前随访指导1.StylePriority.UseFont = false;
            this.txt第5次产前随访指导1.StylePriority.UseTextAlignment = false;
            this.txt第5次产前随访指导1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(6.5F, 119.9999F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(77.5F, 20F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "7 母乳喂养";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(6.499878F, 100F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(77.50018F, 20F);
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "6 分娩准备";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(6.499878F, 79.99994F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(77.50012F, 20F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "5 自我监测";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(6.49999F, 140.0001F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(52.96924F, 23F);
            this.xrLabel48.StylePriority.UseBorders = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.Text = "8 其他";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(6.499863F, 59.99994F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.StylePriority.UseTextAlignment = false;
            this.xrLabel49.Text = "4 运动";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(6.49999F, 39.99968F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel50.StylePriority.UseBorders = false;
            this.xrLabel50.StylePriority.UseTextAlignment = false;
            this.xrLabel50.Text = "3 心理";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(6.499863F, 19.99998F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(64F, 20F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "2 膳食";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(6.499863F, 0.0001589457F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(80.96527F, 20F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "1 个人卫生";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第5次指导其他
            // 
            this.txt第5次指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第5次指导其他.LocationFloat = new DevExpress.Utils.PointFloat(6.499878F, 163.0001F);
            this.txt第5次指导其他.Name = "txt第5次指导其他";
            this.txt第5次指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次指导其他.SizeF = new System.Drawing.SizeF(133.5153F, 45F);
            this.txt第5次指导其他.StylePriority.UseBorders = false;
            this.txt第5次指导其他.StylePriority.UseTextAlignment = false;
            this.txt第5次指导其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable27
            // 
            this.xrTable27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable27.LocationFloat = new DevExpress.Utils.PointFloat(444.0005F, 657.9999F);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTable27.SizeF = new System.Drawing.SizeF(296F, 60F);
            this.xrTable27.StylePriority.UseBorders = false;
            this.xrTable27.StylePriority.UseTextAlignment = false;
            this.xrTable27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66,
            this.xrTableCell67});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次分类异常,
            this.xrLabel26,
            this.txt第4次分类,
            this.xrLabel35});
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Weight = 1.55D;
            // 
            // txt第4次分类异常
            // 
            this.txt第4次分类异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次分类异常.LocationFloat = new DevExpress.Utils.PointFloat(66.49995F, 30.00004F);
            this.txt第4次分类异常.Name = "txt第4次分类异常";
            this.txt第4次分类异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次分类异常.SizeF = new System.Drawing.SizeF(75.00014F, 23F);
            this.txt第4次分类异常.StylePriority.UseBorders = false;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(6.499955F, 30.00004F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "2 异常";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第4次分类
            // 
            this.txt第4次分类.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次分类.CanGrow = false;
            this.txt第4次分类.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次分类.LocationFloat = new DevExpress.Utils.PointFloat(119.0002F, 8.999969F);
            this.txt第4次分类.Name = "txt第4次分类";
            this.txt第4次分类.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次分类.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次分类.StylePriority.UseBorders = false;
            this.txt第4次分类.StylePriority.UseFont = false;
            this.txt第4次分类.StylePriority.UseTextAlignment = false;
            this.txt第4次分类.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(6.499924F, 7.000051F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(80.9653F, 23F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "1 未见异常";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第5次分类异常,
            this.xrLabel47,
            this.txt第5次分类,
            this.xrLabel56});
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Weight = 1.55D;
            // 
            // txt第5次分类异常
            // 
            this.txt第5次分类异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第5次分类异常.LocationFloat = new DevExpress.Utils.PointFloat(66.49995F, 30.00004F);
            this.txt第5次分类异常.Name = "txt第5次分类异常";
            this.txt第5次分类异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次分类异常.SizeF = new System.Drawing.SizeF(75.00014F, 23F);
            this.txt第5次分类异常.StylePriority.UseBorders = false;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(6.499955F, 30.00004F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "2 异常";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第5次分类
            // 
            this.txt第5次分类.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第5次分类.CanGrow = false;
            this.txt第5次分类.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第5次分类.LocationFloat = new DevExpress.Utils.PointFloat(115.9997F, 9.000032F);
            this.txt第5次分类.Name = "txt第5次分类";
            this.txt第5次分类.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次分类.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第5次分类.StylePriority.UseBorders = false;
            this.txt第5次分类.StylePriority.UseFont = false;
            this.txt第5次分类.StylePriority.UseTextAlignment = false;
            this.txt第5次分类.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(6.499924F, 7.000051F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(80.9653F, 23F);
            this.xrLabel56.StylePriority.UseBorders = false;
            this.xrLabel56.StylePriority.UseTextAlignment = false;
            this.xrLabel56.Text = "1 未见异常";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable28
            // 
            this.xrTable28.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable28.LocationFloat = new DevExpress.Utils.PointFloat(0.0004132589F, 657.9999F);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable28.SizeF = new System.Drawing.SizeF(444F, 60F);
            this.xrTable28.StylePriority.UseBorders = false;
            this.xrTable28.StylePriority.UseTextAlignment = false;
            this.xrTable28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell70});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.Text = "  分      类";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell68.Weight = 0.59999996062247973D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次分类异常,
            this.xrLabel15,
            this.txt第2次分类,
            this.xrLabel13});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Weight = 0.59999996062247984D;
            // 
            // txt第2次分类异常
            // 
            this.txt第2次分类异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次分类异常.LocationFloat = new DevExpress.Utils.PointFloat(66.49995F, 30.00004F);
            this.txt第2次分类异常.Name = "txt第2次分类异常";
            this.txt第2次分类异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次分类异常.SizeF = new System.Drawing.SizeF(75.00014F, 23F);
            this.txt第2次分类异常.StylePriority.UseBorders = false;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(6.499955F, 30.00004F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "2 异常";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次分类
            // 
            this.txt第2次分类.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次分类.CanGrow = false;
            this.txt第2次分类.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次分类.LocationFloat = new DevExpress.Utils.PointFloat(112.2915F, 8.999969F);
            this.txt第2次分类.Name = "txt第2次分类";
            this.txt第2次分类.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次分类.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次分类.StylePriority.UseBorders = false;
            this.txt第2次分类.StylePriority.UseFont = false;
            this.txt第2次分类.StylePriority.UseTextAlignment = false;
            this.txt第2次分类.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(6.499924F, 7.000051F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(80.9653F, 23F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "1 未见异常";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次分类异常,
            this.xrLabel18,
            this.txt第3次分类,
            this.xrLabel22});
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Weight = 0.59999996062248D;
            // 
            // txt第3次分类异常
            // 
            this.txt第3次分类异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次分类异常.LocationFloat = new DevExpress.Utils.PointFloat(66.49995F, 30.00001F);
            this.txt第3次分类异常.Name = "txt第3次分类异常";
            this.txt第3次分类异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次分类异常.SizeF = new System.Drawing.SizeF(75.00014F, 23F);
            this.txt第3次分类异常.StylePriority.UseBorders = false;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(6.49994F, 30.00001F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "2 异常";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次分类
            // 
            this.txt第3次分类.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次分类.CanGrow = false;
            this.txt第3次分类.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次分类.LocationFloat = new DevExpress.Utils.PointFloat(118.9998F, 9.000032F);
            this.txt第3次分类.Name = "txt第3次分类";
            this.txt第3次分类.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次分类.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次分类.StylePriority.UseBorders = false;
            this.txt第3次分类.StylePriority.UseFont = false;
            this.txt第3次分类.StylePriority.UseTextAlignment = false;
            this.txt第3次分类.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(6.499908F, 7.000117F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(80.9653F, 23F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "1 未见异常";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable25
            // 
            this.xrTable25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable25.LocationFloat = new DevExpress.Utils.PointFloat(444.0002F, 587.9999F);
            this.xrTable25.Name = "xrTable25";
            this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTable25.SizeF = new System.Drawing.SizeF(296F, 70F);
            this.xrTable25.StylePriority.UseBorders = false;
            this.xrTable25.StylePriority.UseTextAlignment = false;
            this.xrTable25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次其他辅助检查,
            this.txt第5次其他辅助检查});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // txt第4次其他辅助检查
            // 
            this.txt第4次其他辅助检查.Name = "txt第4次其他辅助检查";
            this.txt第4次其他辅助检查.Weight = 1.55D;
            // 
            // txt第5次其他辅助检查
            // 
            this.txt第5次其他辅助检查.Name = "txt第5次其他辅助检查";
            this.txt第5次其他辅助检查.Weight = 1.55D;
            // 
            // xrTable26
            // 
            this.xrTable26.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable26.LocationFloat = new DevExpress.Utils.PointFloat(0.0004132589F, 587.9999F);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable26.SizeF = new System.Drawing.SizeF(444F, 70F);
            this.xrTable26.StylePriority.UseBorders = false;
            this.xrTable26.StylePriority.UseTextAlignment = false;
            this.xrTable26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.txt第2次其他辅助检查,
            this.txt第3次其他辅助检查});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "其他辅助检查*";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.59999996062247973D;
            // 
            // txt第2次其他辅助检查
            // 
            this.txt第2次其他辅助检查.Name = "txt第2次其他辅助检查";
            this.txt第2次其他辅助检查.Weight = 0.59999996062247984D;
            // 
            // txt第3次其他辅助检查
            // 
            this.txt第3次其他辅助检查.Name = "txt第3次其他辅助检查";
            this.txt第3次其他辅助检查.Weight = 0.59999996062248D;
            // 
            // xrTable23
            // 
            this.xrTable23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable23.LocationFloat = new DevExpress.Utils.PointFloat(443.9998F, 552.9999F);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable23.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable23.StylePriority.UseBorders = false;
            this.xrTable23.StylePriority.UseTextAlignment = false;
            this.xrTable23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次尿蛋白,
            this.txt第5次尿蛋白});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // txt第4次尿蛋白
            // 
            this.txt第4次尿蛋白.Name = "txt第4次尿蛋白";
            this.txt第4次尿蛋白.Weight = 1.55D;
            // 
            // txt第5次尿蛋白
            // 
            this.txt第5次尿蛋白.Name = "txt第5次尿蛋白";
            this.txt第5次尿蛋白.Weight = 1.55D;
            // 
            // xrTable24
            // 
            this.xrTable24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable24.LocationFloat = new DevExpress.Utils.PointFloat(0.000222524F, 552.9999F);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable24.SizeF = new System.Drawing.SizeF(444F, 35F);
            this.xrTable24.StylePriority.UseBorders = false;
            this.xrTable24.StylePriority.UseTextAlignment = false;
            this.xrTable24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.txt第2次尿蛋白,
            this.txt第3次尿蛋白});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "尿 蛋 白";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell58.Weight = 0.59999996062247973D;
            // 
            // txt第2次尿蛋白
            // 
            this.txt第2次尿蛋白.Name = "txt第2次尿蛋白";
            this.txt第2次尿蛋白.Weight = 0.59999996062247984D;
            // 
            // txt第3次尿蛋白
            // 
            this.txt第3次尿蛋白.Name = "txt第3次尿蛋白";
            this.txt第3次尿蛋白.Weight = 0.59999996062248D;
            // 
            // xrTable21
            // 
            this.xrTable21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 517.9999F);
            this.xrTable21.Name = "xrTable21";
            this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable21.SizeF = new System.Drawing.SizeF(444F, 35F);
            this.xrTable21.StylePriority.UseBorders = false;
            this.xrTable21.StylePriority.UseTextAlignment = false;
            this.xrTable21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.txt第2次血红蛋白,
            this.txt第3次血红蛋白});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = "血红蛋白(g/L)";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell51.Weight = 0.59999996062247973D;
            // 
            // txt第2次血红蛋白
            // 
            this.txt第2次血红蛋白.Name = "txt第2次血红蛋白";
            this.txt第2次血红蛋白.Weight = 0.59999996062247984D;
            // 
            // txt第3次血红蛋白
            // 
            this.txt第3次血红蛋白.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次血红蛋白.Name = "txt第3次血红蛋白";
            this.txt第3次血红蛋白.StylePriority.UseFont = false;
            this.txt第3次血红蛋白.Weight = 0.59999996062248D;
            // 
            // xrTable22
            // 
            this.xrTable22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable22.LocationFloat = new DevExpress.Utils.PointFloat(444.0001F, 517.9999F);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.xrTable22.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable22.StylePriority.UseBorders = false;
            this.xrTable22.StylePriority.UseTextAlignment = false;
            this.xrTable22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次血红蛋白,
            this.txt第5次血红蛋白});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // txt第4次血红蛋白
            // 
            this.txt第4次血红蛋白.Name = "txt第4次血红蛋白";
            this.txt第4次血红蛋白.Weight = 1.55D;
            // 
            // txt第5次血红蛋白
            // 
            this.txt第5次血红蛋白.Name = "txt第5次血红蛋白";
            this.txt第5次血红蛋白.Weight = 1.55D;
            // 
            // xrTable19
            // 
            this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable19.LocationFloat = new DevExpress.Utils.PointFloat(7.152557E-05F, 483F);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable19.SizeF = new System.Drawing.SizeF(444F, 35F);
            this.xrTable19.StylePriority.UseBorders = false;
            this.xrTable19.StylePriority.UseTextAlignment = false;
            this.xrTable19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell46,
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseTextAlignment = false;
            this.xrTableCell46.Text = "血 压(mmHg)";
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell46.Weight = 0.59999996062247973D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次血压2,
            this.xrLabel78,
            this.txt第2次血压1});
            this.xrTableCell47.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseFont = false;
            this.xrTableCell47.Weight = 0.59999996062247984D;
            // 
            // txt第2次血压2
            // 
            this.txt第2次血压2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次血压2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次血压2.LocationFloat = new DevExpress.Utils.PointFloat(80.96563F, 2.000061F);
            this.txt第2次血压2.Name = "txt第2次血压2";
            this.txt第2次血压2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次血压2.SizeF = new System.Drawing.SizeF(50F, 29.99994F);
            this.txt第2次血压2.StylePriority.UseBorders = false;
            this.txt第2次血压2.StylePriority.UseFont = false;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(60.96561F, 2F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(20.00002F, 30F);
            this.xrLabel78.StylePriority.UseBorders = false;
            this.xrLabel78.Text = "/";
            // 
            // txt第2次血压1
            // 
            this.txt第2次血压1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次血压1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次血压1.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 2.000014F);
            this.txt第2次血压1.Name = "txt第2次血压1";
            this.txt第2次血压1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt第2次血压1.SizeF = new System.Drawing.SizeF(50F, 30F);
            this.txt第2次血压1.StylePriority.UseBorders = false;
            this.txt第2次血压1.StylePriority.UseFont = false;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次血压2,
            this.xrLabel81,
            this.txt第3次血压1});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 0.59999996062248D;
            // 
            // txt第3次血压2
            // 
            this.txt第3次血压2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次血压2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次血压2.LocationFloat = new DevExpress.Utils.PointFloat(84.00024F, 2.000092F);
            this.txt第3次血压2.Name = "txt第3次血压2";
            this.txt第3次血压2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次血压2.SizeF = new System.Drawing.SizeF(50F, 29.99991F);
            this.txt第3次血压2.StylePriority.UseBorders = false;
            this.txt第3次血压2.StylePriority.UseFont = false;
            // 
            // xrLabel81
            // 
            this.xrLabel81.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel81.LocationFloat = new DevExpress.Utils.PointFloat(64.0003F, 2F);
            this.xrLabel81.Name = "xrLabel81";
            this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel81.SizeF = new System.Drawing.SizeF(20F, 30F);
            this.xrLabel81.StylePriority.UseBorders = false;
            this.xrLabel81.Text = "/";
            // 
            // txt第3次血压1
            // 
            this.txt第3次血压1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次血压1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次血压1.LocationFloat = new DevExpress.Utils.PointFloat(13.03466F, 2F);
            this.txt第3次血压1.Name = "txt第3次血压1";
            this.txt第3次血压1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次血压1.SizeF = new System.Drawing.SizeF(50F, 30F);
            this.txt第3次血压1.StylePriority.UseBorders = false;
            this.txt第3次血压1.StylePriority.UseFont = false;
            // 
            // xrTable20
            // 
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.LocationFloat = new DevExpress.Utils.PointFloat(443.9997F, 483F);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable20.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable20.StylePriority.UseBorders = false;
            this.xrTable20.StylePriority.UseTextAlignment = false;
            this.xrTable20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell50});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次血压2,
            this.xrLabel84,
            this.txt第4次血压1});
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 1.55D;
            // 
            // txt第4次血压2
            // 
            this.txt第4次血压2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次血压2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次血压2.LocationFloat = new DevExpress.Utils.PointFloat(84.0011F, 2.000092F);
            this.txt第4次血压2.Name = "txt第4次血压2";
            this.txt第4次血压2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次血压2.SizeF = new System.Drawing.SizeF(50F, 29.99991F);
            this.txt第4次血压2.StylePriority.UseBorders = false;
            this.txt第4次血压2.StylePriority.UseFont = false;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(64.00101F, 2F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(20.00003F, 30F);
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.Text = "/";
            // 
            // txt第4次血压1
            // 
            this.txt第4次血压1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次血压1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次血压1.LocationFloat = new DevExpress.Utils.PointFloat(13.03537F, 2F);
            this.txt第4次血压1.Name = "txt第4次血压1";
            this.txt第4次血压1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次血压1.SizeF = new System.Drawing.SizeF(50F, 30F);
            this.txt第4次血压1.StylePriority.UseBorders = false;
            this.txt第4次血压1.StylePriority.UseFont = false;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第5次血压2,
            this.xrLabel87,
            this.txt第5次血压1});
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Weight = 1.55D;
            // 
            // txt第5次血压2
            // 
            this.txt第5次血压2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第5次血压2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第5次血压2.LocationFloat = new DevExpress.Utils.PointFloat(84.00073F, 2.000061F);
            this.txt第5次血压2.Name = "txt第5次血压2";
            this.txt第5次血压2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次血压2.SizeF = new System.Drawing.SizeF(50F, 29.99994F);
            this.txt第5次血压2.StylePriority.UseBorders = false;
            this.txt第5次血压2.StylePriority.UseFont = false;
            // 
            // xrLabel87
            // 
            this.xrLabel87.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel87.LocationFloat = new DevExpress.Utils.PointFloat(64.00079F, 2F);
            this.xrLabel87.Name = "xrLabel87";
            this.xrLabel87.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel87.SizeF = new System.Drawing.SizeF(20F, 30F);
            this.xrLabel87.StylePriority.UseBorders = false;
            this.xrLabel87.Text = "/";
            // 
            // txt第5次血压1
            // 
            this.txt第5次血压1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第5次血压1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第5次血压1.LocationFloat = new DevExpress.Utils.PointFloat(13.03497F, 2F);
            this.txt第5次血压1.Name = "txt第5次血压1";
            this.txt第5次血压1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第5次血压1.SizeF = new System.Drawing.SizeF(50F, 30F);
            this.txt第5次血压1.StylePriority.UseBorders = false;
            this.txt第5次血压1.StylePriority.UseFont = false;
            // 
            // xrTable17
            // 
            this.xrTable17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(29.99977F, 448F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable17.SizeF = new System.Drawing.SizeF(414F, 35F);
            this.xrTable17.StylePriority.UseBorders = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.txt第2次胎心率,
            this.txt第3次胎心率});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Text = "胎心率(次/分钟)";
            this.xrTableCell41.Weight = 0.83333323160807293D;
            // 
            // txt第2次胎心率
            // 
            this.txt第2次胎心率.Name = "txt第2次胎心率";
            this.txt第2次胎心率.Weight = 1.033333231608073D;
            // 
            // txt第3次胎心率
            // 
            this.txt第3次胎心率.Name = "txt第3次胎心率";
            this.txt第3次胎心率.Weight = 1.0333333333333332D;
            // 
            // xrTable18
            // 
            this.xrTable18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(443.9997F, 448F);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable18.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable18.StylePriority.UseBorders = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次胎心率,
            this.txt第5次胎心率});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // txt第4次胎心率
            // 
            this.txt第4次胎心率.Name = "txt第4次胎心率";
            this.txt第4次胎心率.Weight = 1.4999999753890498D;
            // 
            // txt第5次胎心率
            // 
            this.txt第5次胎心率.Name = "txt第5次胎心率";
            this.txt第5次胎心率.Weight = 1.5000000246109502D;
            // 
            // xrTable15
            // 
            this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(30.00019F, 413F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable15.SizeF = new System.Drawing.SizeF(413.9993F, 35F);
            this.xrTable15.StylePriority.UseBorders = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.txt第2次胎位,
            this.txt第3次胎位});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "胎 位";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell36.Weight = 0.83332831621028225D;
            // 
            // txt第2次胎位
            // 
            this.txt第2次胎位.Name = "txt第2次胎位";
            this.txt第2次胎位.Weight = 1.033333231608073D;
            // 
            // txt第3次胎位
            // 
            this.txt第3次胎位.Name = "txt第3次胎位";
            this.txt第3次胎位.Weight = 1.0333333333333332D;
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(443.9995F, 413F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable16.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable16.StylePriority.UseBorders = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次胎位,
            this.txt第5次胎位});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // txt第4次胎位
            // 
            this.txt第4次胎位.Name = "txt第4次胎位";
            this.txt第4次胎位.Weight = 1.4999999753890498D;
            // 
            // txt第5次胎位
            // 
            this.txt第5次胎位.Name = "txt第5次胎位";
            this.txt第5次胎位.Weight = 1.5000000246109502D;
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(29.99981F, 378F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable13.SizeF = new System.Drawing.SizeF(414F, 35F);
            this.xrTable13.StylePriority.UseBorders = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell31,
            this.txt第2次腹围,
            this.txt第3次腹围});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Text = "腹 围(cm)";
            this.xrTableCell31.Weight = 0.83333323160807293D;
            // 
            // txt第2次腹围
            // 
            this.txt第2次腹围.Name = "txt第2次腹围";
            this.txt第2次腹围.Weight = 1.033333231608073D;
            // 
            // txt第3次腹围
            // 
            this.txt第3次腹围.Name = "txt第3次腹围";
            this.txt第3次腹围.Weight = 1.0333333333333332D;
            // 
            // xrTable14
            // 
            this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(443.9999F, 378F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable14.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable14.StylePriority.UseBorders = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次腹围,
            this.txt第5次腹围});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // txt第4次腹围
            // 
            this.txt第4次腹围.Name = "txt第4次腹围";
            this.txt第4次腹围.Weight = 1.4999999753890498D;
            // 
            // txt第5次腹围
            // 
            this.txt第5次腹围.Name = "txt第5次腹围";
            this.txt第5次腹围.Weight = 1.5000000246109502D;
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(444.0001F, 343F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable12.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable12.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次宫底高度,
            this.txt第5次宫底高度});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // txt第4次宫底高度
            // 
            this.txt第4次宫底高度.Name = "txt第4次宫底高度";
            this.txt第4次宫底高度.Weight = 1.4999999753890498D;
            // 
            // txt第5次宫底高度
            // 
            this.txt第5次宫底高度.Name = "txt第5次宫底高度";
            this.txt第5次宫底高度.Weight = 1.5000000246109502D;
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(30.00019F, 343F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable11.SizeF = new System.Drawing.SizeF(414F, 35F);
            this.xrTable11.StylePriority.UseBorders = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.txt第2次宫底高度,
            this.txt第3次宫底高度});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Text = "宫底高度(cm)";
            this.xrTableCell26.Weight = 0.83333323160807293D;
            // 
            // txt第2次宫底高度
            // 
            this.txt第2次宫底高度.Name = "txt第2次宫底高度";
            this.txt第2次宫底高度.Weight = 1.033333231608073D;
            // 
            // txt第3次宫底高度
            // 
            this.txt第3次宫底高度.Name = "txt第3次宫底高度";
            this.txt第3次宫底高度.Weight = 1.0333333333333332D;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 343F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(30F, 140F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "产科检查";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 228F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(444F, 80F);
            this.xrTable9.StylePriority.UseBorders = false;
            this.xrTable9.StylePriority.UseTextAlignment = false;
            this.xrTable9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.txt第2次主诉,
            this.txt第3次主诉});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "主       诉";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell21.Weight = 0.59999996062247973D;
            // 
            // txt第2次主诉
            // 
            this.txt第2次主诉.Name = "txt第2次主诉";
            this.txt第2次主诉.Weight = 0.59999996062247984D;
            // 
            // txt第3次主诉
            // 
            this.txt第3次主诉.Name = "txt第3次主诉";
            this.txt第3次主诉.Weight = 0.59999996062248D;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(443.9999F, 228F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(296F, 80F);
            this.xrTable10.StylePriority.UseBorders = false;
            this.xrTable10.StylePriority.UseTextAlignment = false;
            this.xrTable10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次主诉,
            this.txt第5次主诉});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // txt第4次主诉
            // 
            this.txt第4次主诉.Name = "txt第4次主诉";
            this.txt第4次主诉.Weight = 1.55D;
            // 
            // txt第5次主诉
            // 
            this.txt第5次主诉.Name = "txt第5次主诉";
            this.txt第5次主诉.Weight = 1.55D;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0.000166893F, 308F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(444F, 35F);
            this.xrTable7.StylePriority.UseBorders = false;
            this.xrTable7.StylePriority.UseTextAlignment = false;
            this.xrTable7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.txt第2次体重,
            this.txt第3次体重});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.Text = "体 重(kg)";
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 0.59999996062247973D;
            // 
            // txt第2次体重
            // 
            this.txt第2次体重.Name = "txt第2次体重";
            this.txt第2次体重.Weight = 0.59999996062247984D;
            // 
            // txt第3次体重
            // 
            this.txt第3次体重.Name = "txt第3次体重";
            this.txt第3次体重.Weight = 0.59999996062248D;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(444.0001F, 308F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseTextAlignment = false;
            this.xrTable8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次体重,
            this.txt第5次体重});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // txt第4次体重
            // 
            this.txt第4次体重.Name = "txt第4次体重";
            this.txt第4次体重.Weight = 1.55D;
            // 
            // txt第5次体重
            // 
            this.txt第5次体重.Name = "txt第5次体重";
            this.txt第5次体重.Weight = 1.55D;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0.0001986821F, 193F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(444F, 35F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseTextAlignment = false;
            this.xrTable5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.txt第2次孕周,
            this.txt第3次孕周});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "孕 周(周)";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.59999996062247973D;
            // 
            // txt第2次孕周
            // 
            this.txt第2次孕周.Name = "txt第2次孕周";
            this.txt第2次孕周.Weight = 0.59999996062247984D;
            // 
            // txt第3次孕周
            // 
            this.txt第3次孕周.Name = "txt第3次孕周";
            this.txt第3次孕周.Weight = 0.59999996062248D;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(444.0005F, 193F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable6.StylePriority.UseBorders = false;
            this.xrTable6.StylePriority.UseTextAlignment = false;
            this.xrTable6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次孕周,
            this.txt第5次孕周});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // txt第4次孕周
            // 
            this.txt第4次孕周.Name = "txt第4次孕周";
            this.txt第4次孕周.Weight = 1.55D;
            // 
            // txt第5次孕周
            // 
            this.txt第5次孕周.Name = "txt第5次孕周";
            this.txt第5次孕周.Weight = 1.55D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0.0001986821F, 158F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(444F, 35F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.txt第2次随访日期,
            this.txt第3次随访日期});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "随访日期";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 0.59999996062247973D;
            // 
            // txt第2次随访日期
            // 
            this.txt第2次随访日期.Name = "txt第2次随访日期";
            this.txt第2次随访日期.Weight = 0.59999996062247984D;
            // 
            // txt第3次随访日期
            // 
            this.txt第3次随访日期.Name = "txt第3次随访日期";
            this.txt第3次随访日期.Weight = 0.59999996062248D;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(444.0002F, 158F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable4.StylePriority.UseBorders = false;
            this.xrTable4.StylePriority.UseTextAlignment = false;
            this.xrTable4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt第4次随访日期,
            this.txt第5次随访日期});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // txt第4次随访日期
            // 
            this.txt第4次随访日期.Name = "txt第4次随访日期";
            this.txt第4次随访日期.Weight = 1.55D;
            // 
            // txt第5次随访日期
            // 
            this.txt第5次随访日期.Name = "txt第5次随访日期";
            this.txt第5次随访日期.Weight = 1.55D;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(444.0002F, 123F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(296F, 35F);
            this.xrTable2.StylePriority.UseBorders = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "第4次*";
            this.xrTableCell4.Weight = 1.55D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "第5次*";
            this.xrTableCell5.Weight = 1.55D;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0.0001986821F, 123F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(444F, 35F);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "项        目";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 0.59999996062247973D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "第2次";
            this.xrTableCell2.Weight = 0.59999996062247984D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "第3次";
            this.xrTableCell3.Weight = 0.59999996062248D;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(452.7922F, 99.99997F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(55.20834F, 23F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "编号：";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(510.3473F, 99.99997F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(534.0191F, 99.99997F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(605.0347F, 99.99997F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(557.691F, 99.99997F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(628.7066F, 99.99997F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(652.3786F, 99.99997F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(676.0502F, 99.99997F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(699.7221F, 99.99997F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(581.3628F, 99.99997F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(14.99994F, 15F);
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 99.99997F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(70.83334F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓名:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(217.334F, 51.00002F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(329.1667F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "第2~5次产前随访服务记录表";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 3F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // roport第2至5次产前随访服务记录表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(45, 42, 3, 0);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次随访日期;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次随访日期;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次随访日期;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次随访日期;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次孕周;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次孕周;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次孕周;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次孕周;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次主诉;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次主诉;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次主诉;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次主诉;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次体重;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次体重;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次体重;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次体重;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次宫底高度;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次宫底高度;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次宫底高度;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次宫底高度;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次胎位;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次胎位;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次胎位;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次胎位;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次腹围;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次腹围;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次腹围;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次腹围;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次胎心率;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次胎心率;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次胎心率;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次胎心率;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTable xrTable21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次血红蛋白;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次血红蛋白;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次血红蛋白;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次血红蛋白;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次尿蛋白;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次尿蛋白;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次尿蛋白;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次尿蛋白;
        private DevExpress.XtraReports.UI.XRTable xrTable25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次其他辅助检查;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次其他辅助检查;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次其他辅助检查;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次其他辅助检查;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRLabel txt第2次指导其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRLabel txt第3次指导其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel txt第4次指导其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRLabel txt第2次有无转诊;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTable xrTable32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRLabel txt第2次转诊机构及科室;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel txt第2次转诊原因;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel txt第3次转诊机构及科室;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel txt第3次转诊原因;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel txt第3次有无转诊;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel txt第4次转诊机构及科室;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel txt第4次转诊原因;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel txt第4次有无转诊;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRLabel txt第5次转诊机构及科室;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRLabel txt第5次转诊原因;
        private DevExpress.XtraReports.UI.XRLabel xrLabel74;
        private DevExpress.XtraReports.UI.XRLabel txt第5次有无转诊;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRTable xrTable35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次随访医生签名;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次随访医生签名;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次随访医生签名;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次随访医生签名;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell txt第4次下次随访日期;
        private DevExpress.XtraReports.UI.XRTableCell txt第5次下次随访日期;
        private DevExpress.XtraReports.UI.XRTable xrTable34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell txt第2次下次随访日期;
        private DevExpress.XtraReports.UI.XRTableCell txt第3次下次随访日期;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRLabel txt第2次血压2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel txt第2次血压1;
        private DevExpress.XtraReports.UI.XRLabel txt第3次血压2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel81;
        private DevExpress.XtraReports.UI.XRLabel txt第3次血压1;
        private DevExpress.XtraReports.UI.XRLabel txt第4次血压2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel txt第4次血压1;
        private DevExpress.XtraReports.UI.XRLabel txt第5次血压2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel87;
        private DevExpress.XtraReports.UI.XRLabel txt第5次血压1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel txt第4次分类异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel txt第4次分类;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel txt第5次分类异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel txt第5次分类;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel txt第2次分类异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel txt第2次分类;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel txt第3次分类异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel txt第3次分类;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel txt第5次指导其他;
        private DevExpress.XtraReports.UI.XRLabel txt第2次产前随访指导5;
        private DevExpress.XtraReports.UI.XRLabel txt第2次产前随访指导4;
        private DevExpress.XtraReports.UI.XRLabel txt第2次产前随访指导3;
        private DevExpress.XtraReports.UI.XRLabel txt第2次产前随访指导2;
        private DevExpress.XtraReports.UI.XRLabel txt第2次产前随访指导1;
        private DevExpress.XtraReports.UI.XRLabel txt第3次产前随访指导7;
        private DevExpress.XtraReports.UI.XRLabel txt第3次产前随访指导6;
        private DevExpress.XtraReports.UI.XRLabel txt第3次产前随访指导5;
        private DevExpress.XtraReports.UI.XRLabel txt第3次产前随访指导4;
        private DevExpress.XtraReports.UI.XRLabel txt第3次产前随访指导3;
        private DevExpress.XtraReports.UI.XRLabel txt第3次产前随访指导2;
        private DevExpress.XtraReports.UI.XRLabel txt第3次产前随访指导1;
        private DevExpress.XtraReports.UI.XRLabel txt第4次产前随访指导8;
        private DevExpress.XtraReports.UI.XRLabel txt第4次产前随访指导7;
        private DevExpress.XtraReports.UI.XRLabel txt第4次产前随访指导6;
        private DevExpress.XtraReports.UI.XRLabel txt第4次产前随访指导5;
        private DevExpress.XtraReports.UI.XRLabel txt第4次产前随访指导4;
        private DevExpress.XtraReports.UI.XRLabel txt第4次产前随访指导3;
        private DevExpress.XtraReports.UI.XRLabel txt第4次产前随访指导2;
        private DevExpress.XtraReports.UI.XRLabel txt第4次产前随访指导1;
        private DevExpress.XtraReports.UI.XRLabel txt第5次产前随访指导8;
        private DevExpress.XtraReports.UI.XRLabel txt第5次产前随访指导7;
        private DevExpress.XtraReports.UI.XRLabel txt第5次产前随访指导6;
        private DevExpress.XtraReports.UI.XRLabel txt第5次产前随访指导5;
        private DevExpress.XtraReports.UI.XRLabel txt第5次产前随访指导4;
        private DevExpress.XtraReports.UI.XRLabel txt第5次产前随访指导3;
        private DevExpress.XtraReports.UI.XRLabel txt第5次产前随访指导2;
        private DevExpress.XtraReports.UI.XRLabel txt第5次产前随访指导1;
    }
}
