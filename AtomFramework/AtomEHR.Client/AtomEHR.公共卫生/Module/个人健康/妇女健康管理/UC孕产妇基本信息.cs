﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using AtomEHR.Library;

namespace AtomEHR.公共卫生.Module.个人健康
{
    public partial class UC孕产妇基本信息 : UserControlBase
    {
        DataRow[] _dr个人信息 = null;
        public UC孕产妇基本信息()
        {
            InitializeComponent();
        }

        public UC孕产妇基本信息(DataRow[] dr)
        {
            _dr个人信息 = dr;
            _BLL = new bll孕妇基本信息();
            InitializeComponent();

            //默认绑定
            txt档案编号.Text = dr[0][tb_健康档案.__KeyName].ToString();
            this.lab姓名.Text = util.DESEncrypt.DES解密(dr[0][tb_健康档案.姓名].ToString());
            //this.lab性别.Text = dr[0][tb_健康档案.性别].ToString();
            this.lab身份证号.Text = dr[0][tb_健康档案.身份证号].ToString();
            this.lab出生日期.Text = dr[0][tb_健康档案.出生日期].ToString();
            this.txt年龄.Text = util.ControlsHelper.GetAge( dr[0][tb_健康档案.出生日期].ToString(),_BLL.ServiceDateTime).ToString();//dr[0][tb_健康档案.年龄].ToString();
            this.lab联系电话.Text = dr[0][tb_健康档案.本人电话].ToString();
            this.txt居住状态.Text = "常驻";
            this.lab居住地址.Text = dr[0][tb_健康档案.居住地址].ToString();
            this.lab职业.Text = dr[0][tb_健康档案.职业].ToString();
            this.txt工作单位.Text = dr[0][tb_健康档案.工作单位].ToString();                      
        }
        

        private void UC孕产妇基本信息_Load(object sender, EventArgs e)
        {
            _dt缓存数据 = _BLL.GetBusinessByKey(txt档案编号.Text, true).Tables[tb_孕妇基本信息.__TableName];

            if (_dt缓存数据 != null && _dt缓存数据.Rows.Count > 0)
            {
                DoBindingSummaryEditor(_dt缓存数据);
            }

            DataBinder.BindingLookupEditDataSource(txt丈夫文化程度, DataDictCache.Cache.t文化程度, "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt丈夫民族, DataDictCache.Cache.t民族, "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(txt丈夫职业, DataDictCache.Cache.t职业, "P_DESC", "P_CODE");
            InitButtonShow();

            //设置控件可用
            //SetDetailEditorsAccessable(panel1, false);
            Set考核项颜色_new(layoutControl1, lab考核项);
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            Set考核项颜色_new(layoutControl1, lab考核项);

            _BLL.DataBinder.Rows[0][tb_孕妇基本信息.缺项] = _base缺项;
            _BLL.DataBinder.Rows[0][tb_孕妇基本信息.完整度] = _base完整度;

            //回写个人档案本人电话和丈夫电话
            if (!_BLL.CurrentBusiness.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人电话].Equals(this.txt户口联系电话.Text) && !string.IsNullOrEmpty(this.txt户口联系电话.Text))
            {
                _BLL.CurrentBusiness.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人电话] = this.txt户口联系电话.Text;
            }

            if (!_BLL.CurrentBusiness.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人电话].Equals(this.txt丈夫电话.Text) && !string.IsNullOrEmpty(this.txt丈夫电话.Text))
            {
                _BLL.CurrentBusiness.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人电话] = this.txt丈夫电话.Text;
            }

            if (!_BLL.CurrentBusiness.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人姓名].Equals(this.txt丈夫姓名.Text) && !string.IsNullOrEmpty(this.txt丈夫姓名.Text))
            {
                _BLL.CurrentBusiness.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人姓名] = this.txt丈夫姓名.Text;
            }

            DataSet ds = new DataSet();
            dt缓存数据.Rows[0].AcceptChanges();
            ds.Tables.Add(dt缓存数据.Copy());

            if (!ValidatingSummaryData()) return; //检查主表数据合法性

            if (_UpdateType == UpdateType.None) return; //操作状态是空直接返回
            if (_UpdateType == UpdateType.Modify) _BLL.WriteLog(); //注意:只有修改状态下保存修改日志

            DataSet dsTemplate = _BLL.CreateSaveData(_BLL.CurrentBusiness, _UpdateType); //创建用于保存的临时数据

            SaveResult result = _BLL.Save(dsTemplate);//调用业务逻辑保存数据方法

            if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
            {
                //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                //this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据
                this._UpdateType = UpdateType.None;
                Msg.ShowInformation("保存成功!");
                InitButtonShow();
            }
            else
                Msg.Warning("保存失败!");            
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            _UpdateType = UpdateType.Modify;
            InitButtonShow();
        }

        private void btn重置_Click(object sender, EventArgs e)
        {
            //base.ClearContainerEditorText(panelControl3);  
        }

        /// <summary>
        /// 检查主表数据
        /// </summary>
        /// <param name="summary"></param>
        /// <returns></returns>
        private bool ValidatingSummaryData()
        {
            //if (string.IsNullOrEmpty(ConvertEx.ToString(txt接诊时间.Text)))
            //{
            //    Msg.Warning("接诊时间不能为空!");
            //    txt接诊时间.Focus();
            //    return false;
            //}

            return true;
        }

        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        protected override void DoBindingSummaryEditor(DataTable dataSource)
        {
            if (dataSource == null) return;
            
            //TextEdit
            DataBinder.BindingTextEdit(txt卡号, dataSource, tb_孕妇基本信息.卡号);
            DataBinder.BindingTextEdit(txt户口所在地, dataSource, tb_孕妇基本信息.户口所在地);
            DataBinder.BindingTextEdit(txt户口联系电话, dataSource, tb_孕妇基本信息.户口联系电话);
            DataBinder.BindingTextEdit(txt产后休养地, dataSource, tb_孕妇基本信息.产后休养地);
            DataBinder.BindingTextEdit(txt产后联系电话, dataSource, tb_孕妇基本信息.产后联系电话);
            DataBinder.BindingTextEdit(txt单位电话, dataSource, tb_孕妇基本信息.工作电话);
            DataBinder.BindingTextEdit(txt丈夫姓名, dataSource, tb_孕妇基本信息.丈夫姓名);
            DataBinder.BindingTextEdit(txt丈夫年龄, dataSource, tb_孕妇基本信息.丈夫年龄);
            DataBinder.BindingTextEdit(txt丈夫文化程度, dataSource, tb_孕妇基本信息.丈夫文化程度);
            DataBinder.BindingTextEdit(txt丈夫民族, dataSource, tb_孕妇基本信息.丈夫民族);
            DataBinder.BindingTextEdit(txt丈夫工作单位, dataSource, tb_孕妇基本信息.丈夫工作单位);
            DataBinder.BindingTextEdit(txt丈夫电话, dataSource, tb_孕妇基本信息.丈夫电话);
            DataBinder.BindingTextEdit(txt丈夫职业, dataSource, tb_孕妇基本信息.丈夫职业);
            DataBinder.BindingTextEdit(txt建档时间, dataSource, tb_孕妇基本信息.建档时间);;


            //非编辑项     
            this.lab当前所属机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_孕妇基本信息.所属机构].ToString()); 
            this.lab创建机构.Text = BLL.Return机构名称(dataSource.Rows[0][tb_孕妇基本信息.创建机构].ToString());
            this.lab创建人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_孕妇基本信息.创建人].ToString());
            this.lab创建时间.Text = dataSource.Rows[0][tb_孕妇基本信息.创建时间].ToString();
            this.lab最近修改人.Text = BLL.Return用户名称(dataSource.Rows[0][tb_孕妇基本信息.修改人].ToString());
            this.lab最近更新时间.Text = dataSource.Rows[0][tb_孕妇基本信息.修改时间].ToString();            
        }

        /// <summary>
        /// 初始化button显示
        /// </summary>
        private void InitButtonShow()
        {
            if (_UpdateType == UpdateType.Modify)
            {
                this.btn保存.Visible = true;
                this.btn重置.Visible = true;
                this.btn修改.Visible = false;
                this.btn删除.Visible = false;
                this.btn导出.Visible = false;
                this.txt卡号.Properties.ReadOnly = false;
                this.txt户口所在地.Properties.ReadOnly = false;
                this.txt户口联系电话.Properties.ReadOnly = false;
                this.txt产后休养地.Properties.ReadOnly = false;
                this.txt产后联系电话.Properties.ReadOnly = false;
                //this.txt单位电话.Properties.ReadOnly = false;
                this.txt丈夫姓名.Properties.ReadOnly = false;
                this.txt丈夫年龄.Properties.ReadOnly = false;
                this.txt丈夫文化程度.Properties.ReadOnly = false;
                this.txt丈夫民族.Properties.ReadOnly = false;
                this.txt丈夫工作单位.Properties.ReadOnly = false;
                this.txt丈夫电话.Properties.ReadOnly = false;
                this.txt丈夫职业.Properties.ReadOnly = false;
                this.txt建档时间.Properties.ReadOnly = false;                
            }
            else
            {
                this.btn保存.Visible = false;
                this.btn重置.Visible = false;
                this.btn修改.Visible = true;
                this.btn删除.Visible = true;
                this.btn导出.Visible = true;

                this.txt卡号.Properties.ReadOnly = true;
                this.txt户口所在地.Properties.ReadOnly = true;
                this.txt户口联系电话.Properties.ReadOnly = true;
                this.txt产后休养地.Properties.ReadOnly = true;
                this.txt产后联系电话.Properties.ReadOnly = true;
                //this.txt单位电话.Properties.ReadOnly = true;
                this.txt丈夫姓名.Properties.ReadOnly = true;
                this.txt丈夫年龄.Properties.ReadOnly = true;
                this.txt丈夫文化程度.Properties.ReadOnly = true;
                this.txt丈夫民族.Properties.ReadOnly = true;
                this.txt丈夫工作单位.Properties.ReadOnly = true;
                this.txt丈夫电话.Properties.ReadOnly = true;
                this.txt丈夫职业.Properties.ReadOnly = true;
                this.txt建档时间.Properties.ReadOnly = true;
            }
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            if (Msg.AskQuestion("删除此人的孕产妇基本信息将删除其所有已建立的产前/产后随访表！若选择 【确定】则一并删除，如果选择【取消】则不进行任何操作！"))
            {
                if (_BLL.Delete(txt档案编号.Text.Trim()))
                {
                    Msg.ShowInformation("删除记录成功！");
                    //删除成功后需要更新 字段
                    this.OnLoad(null);
                }
            }
        }        

    }
}
