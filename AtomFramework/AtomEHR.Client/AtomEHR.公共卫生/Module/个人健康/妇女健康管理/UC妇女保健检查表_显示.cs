﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business.Security;
using AtomEHR.Models;
using AtomEHR.Common;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class UC妇女保健检查表_显示 : UserControlBaseNavBar
    {
        #region Fields
        AtomEHR.Business.bll妇女保健检查 _Bll = new Business.bll妇女保健检查();
        bllUser _user = new bllUser();
        DataSet _ds妇女保健检查表;
        public frm个人健康 _frm = null;
        string _docNo;
        string _familyNo;
        string _id;
        DataRow _dr当前数据;
        #endregion
        public UC妇女保健检查表_显示()
        {
            InitializeComponent();
        }
        public UC妇女保健检查表_显示(Form parentForm)
        {
            InitializeComponent();
            _frm = (frm个人健康)parentForm;
            _docNo = _frm._docNo;
            _familyNo = _frm._familyDocNo;
            _id = _frm._param == null ? "" : _frm._param.ToString();
            //根据个人档案编号 查询全部的  体检数据
            _ds妇女保健检查表 = _Bll.GetAllDataByKey(_docNo);
            DoBindingDataSource(_ds妇女保健检查表);//绑定数据
            //设置日期栏中字体颜色
            SetItemColorToRed(_id);
        }
        private void DoBindingDataSource(DataSet _ds妇女保健检查表)
        {
            if (_ds妇女保健检查表 == null) return;
            DataTable dt妇女保健检查表 = _ds妇女保健检查表.Tables[1];
            DataTable dt妇女基本信息 = _ds妇女保健检查表.Tables[0];
            #region 先绑定老年人基本信息
            Bind基本信息(dt妇女基本信息);
            #endregion
            #region 创建页面左边的导航树
            CreateNavBarButton_new(dt妇女保健检查表, tb_妇女保健检查.检查日期);
            #endregion

            //如果date为空，则默认绑定第一条体检数据
            //否则  根据date进行查找对应的 体检数据 进行绑定
            DataRow row;
            if (string.IsNullOrEmpty(_id) && dt妇女保健检查表.Rows.Count > 0)
            {
                row = dt妇女保健检查表.Rows[0];
            }
            else
            {
                DataRow[] rows = dt妇女保健检查表.Select("ID='" + _id + "'");
                if (rows.Length == 1)
                {
                    row = rows[0];
                }
                else
                {
                    return;
                }
            }
            DoBindingSummaryEditor(row);
        }
        private void Bind基本信息(DataTable dt老年人基本信息)
        {
            if (dt老年人基本信息 == null || dt老年人基本信息.Rows.Count == 0) return;
            DataRow dr = dt老年人基本信息.Rows[0];
            this.txt姓名.Text = util.DESEncrypt.DES解密(dr[tb_健康档案.姓名].ToString());
            this.txt档案编号.Text = dr[tb_健康档案.个人档案编号].ToString();
            this.txt身份证号.Text = dr[tb_健康档案.身份证号].ToString();
            this.txt出生日期.Text = dr[tb_健康档案.出生日期].ToString();
            this.txt联系电话.Text = dr[tb_健康档案.本人电话].ToString();
            this.txt居住状态.Text = _Bll.ReturnDis字典显示("jzzk", dr[tb_健康档案.常住类型].ToString());
            this.txt居住地址.Text = _Bll.Return地区名称(dr[tb_健康档案.市].ToString()).PadRight(2) + _BLL.Return地区名称(dr[tb_健康档案.区].ToString()).PadRight(2) + _BLL.Return地区名称(dr[tb_健康档案.街道].ToString()).PadRight(2) + _BLL.Return地区名称(dr[tb_健康档案.居委会].ToString()).PadRight(2) + dr[tb_健康档案.居住地址].ToString();
        }
        protected override void DoBindingSummaryEditor(object dataSource)
        {
            _dr当前数据 = (DataRow)dataSource;
            if (_dr当前数据 == null) return;
            this.txt卡号.Text = _dr当前数据[tb_妇女保健检查.卡号].ToString();
            this.dte检查日期.Text = _dr当前数据[tb_妇女保健检查.检查日期].ToString();
            this.txt检查次数.Txt1.Text = _dr当前数据[tb_妇女保健检查.检查次数].ToString();
            this.txt现有症状.Text = _dr当前数据[tb_妇女保健检查.现有症状].ToString();
            this.txt曾患妇科病.Text = _dr当前数据[tb_妇女保健检查.曾患妇科病].ToString();
            this.txt曾做手术.Text = _dr当前数据[tb_妇女保健检查.曾做手术].ToString();
            string 初经 = string.Empty;
            if (_dr当前数据[tb_妇女保健检查.初经年龄] != null && _dr当前数据[tb_妇女保健检查.初经年龄].ToString() != "")
            {
                初经 += "初经年龄：" + _dr当前数据[tb_妇女保健检查.初经年龄] + "岁  ";
            }
            if (_dr当前数据[tb_妇女保健检查.持续天数] != null && _dr当前数据[tb_妇女保健检查.持续天数].ToString() != "")
            {
                初经 += "持续天数：" + _dr当前数据[tb_妇女保健检查.持续天数] + "天  ";
            }
            if (_dr当前数据[tb_妇女保健检查.间隔天数] != null && _dr当前数据[tb_妇女保健检查.间隔天数].ToString() != "")
            {
                初经 += "间隔天数：" + _dr当前数据[tb_妇女保健检查.间隔天数] + "天  ";
            }
            if (_dr当前数据[tb_妇女保健检查.绝经年龄] != null && _dr当前数据[tb_妇女保健检查.绝经年龄].ToString() != "")
            {
                初经 += "绝经年龄：" + _dr当前数据[tb_妇女保健检查.绝经年龄] + "岁";
            }

            this.txt初经.Text = 初经;
            this.txt痛经.Text = _dr当前数据[tb_妇女保健检查.痛经].ToString();
            this.txt末次月经.Text = _dr当前数据[tb_妇女保健检查.末次月经].ToString();

            if (_dr当前数据[tb_妇女保健检查.结扎对象].ToString() == "" && _dr当前数据[tb_妇女保健检查.结扎日期].ToString() == "" && _dr当前数据[tb_妇女保健检查.结扎医院].ToString() == "")
            {
                this.lbl结扎.Text = "结扎：无";
            }
            else
            {
                string 结扎 = string.Empty;
                if (_dr当前数据[tb_妇女保健检查.结扎对象] != null && _dr当前数据[tb_妇女保健检查.结扎对象].ToString()
                    != "")
                {
                    结扎 += "结扎对象：" + _dr当前数据[tb_妇女保健检查.结扎对象];
                }
                if (_dr当前数据[tb_妇女保健检查.结扎日期] != null && _dr当前数据[tb_妇女保健检查.结扎日期].ToString() != "")
                {
                    结扎 += "  结扎日期：" + _dr当前数据[tb_妇女保健检查.结扎日期];
                }
                if (_dr当前数据[tb_妇女保健检查.结扎医院] != null && _dr当前数据[tb_妇女保健检查.结扎医院].ToString() != "")
                {
                    结扎 += "  结扎医院：" + _dr当前数据[tb_妇女保健检查.结扎医院];
                }
                this.lbl结扎.Text = 结扎;
            }
            if (_dr当前数据[tb_妇女保健检查.避孕工具].ToString() == "" && _dr当前数据[tb_妇女保健检查.药物名称].ToString() == "")
            {
                this.lbl避孕.Text = "避孕工具及药物：无";
            }
            else
            {
                string 避孕 = string.Empty;
                if (_dr当前数据[tb_妇女保健检查.避孕工具] != null && _dr当前数据[tb_妇女保健检查.避孕工具].ToString() != "")
                {
                    避孕 += "避孕工具：" + _dr当前数据[tb_妇女保健检查.避孕工具];
                }
                if (_dr当前数据[tb_妇女保健检查.药物名称] != null && _dr当前数据[tb_妇女保健检查.药物名称].ToString() != "")
                {
                    避孕 += "  药物名称：" + _dr当前数据[tb_妇女保健检查.药物名称];
                }
                this.lbl避孕.Text = 避孕;
            }
            if (_dr当前数据[tb_妇女保健检查.未孕] != null && _dr当前数据[tb_妇女保健检查.未孕].ToString() == "on")
            {
                this.lbl生育史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.group生育史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.txt孕次.Txt1.Text = _dr当前数据[tb_妇女保健检查.孕次].ToString();
                this.txt产次.Txt1.Text = _dr当前数据[tb_妇女保健检查.产次].ToString();
                this.txt自然流产.Txt1.Text = _dr当前数据[tb_妇女保健检查.自然流产].ToString();
                this.txt人工流产.Txt1.Text = _dr当前数据[tb_妇女保健检查.人工流产].ToString();
                this.txt中孕引产.Txt1.Text = _dr当前数据[tb_妇女保健检查.中孕引产].ToString();
            }
            else
            {
                this.lbl生育史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.txt生育史.Text = "未孕 ";
                this.group生育史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }

            if (_dr当前数据[tb_妇女保健检查.不孕年数] != null && !string.IsNullOrEmpty(_dr当前数据["不孕年数"].ToString()) && Convert.ToInt32(_dr当前数据[tb_妇女保健检查.不孕年数]) != 0)
            {
                this.lbl不孕史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.group不孕史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.txt不孕史时间.Txt1.Text = _dr当前数据[tb_妇女保健检查.不孕年数].ToString();
                this.txt不孕史男.Text = _dr当前数据[tb_妇女保健检查.男方原因].ToString();
                this.txt不孕史女.Text = _dr当前数据[tb_妇女保健检查.女方原因].ToString();
            }
            else
            {
                this.lbl不孕史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                this.group不孕史.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.txt不孕史.Text = "无";
            }

            this.txt血压.Txt1.Text = _dr当前数据[tb_妇女保健检查.血压].ToString();
            this.txt血压.Txt2.Text = _dr当前数据[tb_妇女保健检查.血压2].ToString();
            this.txt体重.Txt1.Text = _dr当前数据[tb_妇女保健检查.体重].ToString();
            this.txt身高.Txt1.Text = _dr当前数据[tb_妇女保健检查.身高].ToString();
            this.txt痛经.Text = _dr当前数据[tb_妇女保健检查.痛经].ToString();

            this.txt乳房.Text = _dr当前数据[tb_妇女保健检查.乳房].ToString();
            this.txt结节.Text = _dr当前数据[tb_妇女保健检查.结节].ToString();
            this.txt压痛.Text = _dr当前数据[tb_妇女保健检查.压痛].ToString();
            this.txt乳房左侧.Text = _dr当前数据[tb_妇女保健检查.乳房左侧].ToString();
            this.txt乳房右侧.Text = _dr当前数据[tb_妇女保健检查.乳房右侧].ToString();

            this.txt心.Text = _dr当前数据[tb_妇女保健检查.心].ToString();
            this.txt肺.Text = _dr当前数据[tb_妇女保健检查.肺].ToString();
            this.txt透胸.Text = _dr当前数据[tb_妇女保健检查.透胸].ToString();

            string waiyin = _dr当前数据[tb_妇女保健检查.外阴].ToString();
            if (!string.IsNullOrEmpty(waiyin))
            {
                string[] a = waiyin.Split(',');
                string result = string.Empty;
                foreach (string item in a)
                {
                    result += _Bll.ReturnDis字典显示("wy_waiyin", item) + "，";
                }
                this.txt外阴.Text = result;
            }
            this.txt阴道.Text = _dr当前数据[tb_妇女保健检查.阴道].ToString();
            this.txt白带.Text = _dr当前数据[tb_妇女保健检查.白带].ToString();
            string 白带性质 = _dr当前数据[tb_妇女保健检查.白带性质].ToString();
            if (!string.IsNullOrEmpty(白带性质))
            {
                string[] a = waiyin.Split(',');
                string result = string.Empty;
                foreach (string item in a)
                {
                    result += _Bll.ReturnDis字典显示("bdxz", item) + "，";
                }
                this.txt白带性质.Text = result;
            }
            string 子宫颈 = _dr当前数据[tb_妇女保健检查.子宫颈].ToString();
            if (!string.IsNullOrEmpty(子宫颈))
            {
                string[] a = waiyin.Split(',');
                string result = string.Empty;
                foreach (string item in a)
                {
                    result += _Bll.ReturnDis字典显示("zgj", item) + "，";
                }
                this.txt子宫颈.Text = result;
            }
            this.txt子宫体活动.Text = _dr当前数据[tb_妇女保健检查.子宫体活动].ToString();
            this.txt子宫脱垂.Text = _dr当前数据[tb_妇女保健检查.子宫脱垂].ToString();
            this.txt子宫体大小.Text = _dr当前数据[tb_妇女保健检查.子宫体大小].ToString();
            this.txt左卵巢.Txt1.Text = _dr当前数据[tb_妇女保健检查.左卵巢].ToString();
            this.txt右卵巢.Txt1.Text = _dr当前数据[tb_妇女保健检查.右卵巢].ToString();
            string 附件 = _dr当前数据[tb_妇女保健检查.附件].ToString();
            if (!string.IsNullOrEmpty(附件))
            {
                string[] a = waiyin.Split(',');
                string result = string.Empty;
                foreach (string item in a)
                {
                    result += _Bll.ReturnDis字典显示("fj_fujian", item) + "，";
                }
                this.txt附件.Text = result;
            }
            this.txt阴道异常情况.Text = _dr当前数据[tb_妇女保健检查.阴道异常情况].ToString();

            this.txt白带涂片清洁度.Text = _dr当前数据[tb_妇女保健检查.白带涂片清洁度].ToString();
            this.txt酸碱度PH值.Text = _dr当前数据[tb_妇女保健检查.酸碱度PH值].ToString();
            this.txt滴虫.Text = _dr当前数据[tb_妇女保健检查.滴虫].ToString();
            this.txt霉菌.Text = _dr当前数据[tb_妇女保健检查.霉菌].ToString();
            this.txt宫颈刮片.Txt1.Text = _dr当前数据[tb_妇女保健检查.宫颈刮片].ToString();
            this.txt巴氏.Text = _dr当前数据[tb_妇女保健检查.巴氏].ToString();
            this.txt化检结果.Text = _dr当前数据[tb_妇女保健检查.化检结果].ToString();
            this.txt复查结果.Text = _dr当前数据[tb_妇女保健检查.复查结果].ToString();

            this.txt印象.Text = _dr当前数据[tb_妇女保健检查.印象].ToString();
            this.txt处理意见.Text = _dr当前数据[tb_妇女保健检查.处理意见].ToString();
            this.txt治疗情况.Text = _dr当前数据[tb_妇女保健检查.治疗情况].ToString();
            this.txt医师签名.Text = _dr当前数据[tb_妇女保健检查.医师签名].ToString();

            this.txt创建人.Text = _Bll.Return用户名称(_dr当前数据[tb_妇女保健检查.创建人].ToString());
            this.txt创建时间.Text = _dr当前数据[tb_妇女保健检查.创建时间].ToString();
            this.txt最近更新时间.Text = _dr当前数据[tb_妇女保健检查.修改时间].ToString();
            this.txt当前所属机构.Text = _Bll.Return地区名称(_dr当前数据[tb_妇女保健检查.所属机构].ToString());
            this.txt创建机构.Text = _Bll.Return地区名称(_dr当前数据[tb_妇女保健检查.创建机构].ToString());// Loginer.CurrentUser.所属机构名称;
            this.txt最近修改人.Text = _Bll.Return用户名称(_dr当前数据[tb_妇女保健检查.修改人].ToString());

        }
        private void btn添加_Click(object sender, EventArgs e)
        {
            UC妇女保健检查表 uc = new UC妇女保健检查表(_frm, AtomEHR.Common.UpdateType.Add);
            ShowControl(uc, DockStyle.Fill);
        }
        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_妇女保健检查.所属机构].ToString()))
            {
                _id = _dr当前数据["ID"].ToString();
                _frm._param = _id;
                UC妇女保健检查表 uc = new UC妇女保健检查表(_frm, AtomEHR.Common.UpdateType.Modify);
                ShowControl(uc, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }
        private void btn删除_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_妇女保健检查.所属机构].ToString()))
            {
                if (Msg.AskQuestion("您确定删除本业务表单吗？"))
                {
                    string id = _dr当前数据["ID"].ToString();
                    if (_Bll.DeleteByID(id))
                    {
                        Msg.ShowInformation("删除记录成功！");
                        DataRow rows = _ds妇女保健检查表.Tables[tb_妇女保健检查.__TableName].Select("ID = '" + id + "'")[0];
                        _ds妇女保健检查表.Tables[tb_妇女保健检查.__TableName].Rows.Remove(rows);
                        if (_ds妇女保健检查表.Tables[tb_妇女保健检查.__TableName].Rows.Count == 0)
                        {
                            UC妇女保健检查表 uc = new UC妇女保健检查表();
                            ShowControl(uc, DockStyle.Fill);
                        }
                        else
                        {
                            _dr当前数据 = _ds妇女保健检查表.Tables[tb_妇女保健检查.__TableName].Rows[0];
                        }
                        CreateNavBarButton_new(_ds妇女保健检查表.Tables[tb_妇女保健检查.__TableName], tb_妇女保健检查.检查日期);
                    }
                }
                else
                {
                    Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
                    return;
                }
            }
        }
        private void UC妇女保健检查表_显示_Load(object sender, EventArgs e)
        {
            this.txt孕次.Txt1.Properties.ReadOnly = true;
            this.txt产次.Txt1.Properties.ReadOnly = true;
            this.txt自然流产.Txt1.Properties.ReadOnly = true;
            this.txt人工流产.Txt1.Properties.ReadOnly = true;
            this.txt中孕引产.Txt1.Properties.ReadOnly = true;
            this.txt孕次.Txt1.BackColor = System.Drawing.Color.White;
            this.txt产次.Txt1.BackColor = System.Drawing.Color.White;
            this.txt自然流产.BackColor = System.Drawing.Color.White;
            this.txt人工流产.BackColor = System.Drawing.Color.White;
            this.txt中孕引产.BackColor = System.Drawing.Color.White;

            this.txt血压.Txt1.Properties.ReadOnly = true;
            this.txt血压.Txt2.Properties.ReadOnly = true;
            this.txt体重.Txt1.Properties.ReadOnly = true;
            this.txt身高.Txt1.Properties.ReadOnly = true;
            this.txt血压.Txt1.BackColor = System.Drawing.Color.White;
            this.txt血压.Txt2.BackColor = System.Drawing.Color.White;
            this.txt体重.Txt1.BackColor = System.Drawing.Color.White;
            this.txt身高.BackColor = System.Drawing.Color.White;

            this.txt左卵巢.Txt1.Properties.ReadOnly = true;
            this.txt右卵巢.Txt1.Properties.ReadOnly = true;
            this.txt左卵巢.Txt1.BackColor = System.Drawing.Color.White;
            this.txt右卵巢.Txt1.BackColor = System.Drawing.Color.White;

            this.txt宫颈刮片.Txt1.Properties.ReadOnly = true;
            this.txt宫颈刮片.Txt1.BackColor = System.Drawing.Color.White;
        }
    }
}
