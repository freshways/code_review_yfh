﻿namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    partial class report第1次产前随访服务记录表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable43 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel212 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel213 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel214 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel216 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt转诊 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable42 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt随访医生签名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable41 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt下次随访日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable39 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt保健指导6 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt保健指导其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel191 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel192 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt保健指导1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt保健指导2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt保健指导3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt保健指导4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt保健指导5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel190 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel189 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable38 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt总体评估异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel187 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt总体评估 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txtB超 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel183 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt梅毒血清学试验 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel180 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt乙型肝炎核心抗体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel178 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt乙型肝炎e抗体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel176 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt乙型肝炎e抗原 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel174 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt乙型肝炎表面抗体 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt乙型肝炎表面抗原 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel172 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel171 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阴道清洁度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt阴道分泌物3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel166 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阴道分泌物2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel168 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阴道分泌物1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阴道分泌物其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel163 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel160 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血尿素氮 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel162 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel157 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血清肌酐 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel159 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel154 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt结合胆红素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel156 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel151 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt总胆红素 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel153 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel148 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt白蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel150 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel145 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血清谷草转氨酶 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel147 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel144 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血清谷丙转氨酶 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel142 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel141 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血糖 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel139 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel138 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt血型 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel136 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt尿常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel134 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt尿潜血 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel132 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt尿酮体 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel130 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt尿糖 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt尿蛋白 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel128 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel126 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血常规其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel123 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血小板计数值 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel125 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt白细胞计数值 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel119 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血红蛋白值 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt宫颈异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt宫颈 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt子宫异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt子宫 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt外阴异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt外阴 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阴道异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt阴道 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt心脏 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt心脏异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel99 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt肺部异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt肺部 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt体质指数 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt体重 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt身高 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt出生缺陷儿 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt新生儿死亡 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt死产 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt死胎 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt流产 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt妇科手术史有 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt妇科手术史 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt个人史其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt个人史1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt个人史2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt个人史3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt个人史4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt个人史5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt家族史其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt家族史3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt既往史其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt既往史1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt既往史2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt既往史3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt既往史4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt既往史5 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt既往史6 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt既往史7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt预产期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt末次月经时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt分娩次数 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt剖宫产次数 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt孕次 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt丈夫年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt丈夫电话 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt丈夫姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt孕妇年龄 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt填表孕周 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt填表日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel114 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt附件异常 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt附件 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel184 = new DevExpress.XtraReports.UI.XRLabel();
            this.txtHIV抗体检测 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血压2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血压1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable24,
            this.xrTable43,
            this.xrTable42,
            this.xrTable41,
            this.xrTable39,
            this.xrTable38,
            this.xrTable37,
            this.xrTable35,
            this.xrTable34,
            this.xrTable33,
            this.xrTable32,
            this.xrTable31,
            this.xrTable30,
            this.xrTable29,
            this.xrTable28,
            this.xrTable27,
            this.xrTable26,
            this.xrTable25,
            this.xrTable22,
            this.xrTable21,
            this.xrTable20,
            this.xrTable19,
            this.xrTable17,
            this.xrTable16,
            this.xrTable15,
            this.xrTable14,
            this.xrTable13,
            this.xrTable12,
            this.xrTable11,
            this.xrTable10,
            this.xrTable9,
            this.xrTable8,
            this.xrTable7,
            this.xrTable6,
            this.xrTable5,
            this.xrTable4,
            this.xrTable3,
            this.xrTable2,
            this.xrTable1,
            this.xrLine1,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1,
            this.xrTable23,
            this.xrTable36,
            this.xrTable18,
            this.txt姓名});
            this.Detail.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.Detail.HeightF = 1149.333F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable24
            // 
            this.xrTable24.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable24.LocationFloat = new DevExpress.Utils.PointFloat(9.536743E-05F, 541.9586F);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable24.SizeF = new System.Drawing.SizeF(100F, 420.1664F);
            this.xrTable24.StylePriority.UseBorders = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "辅助检查";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell4.Weight = 1D;
            // 
            // xrTable43
            // 
            this.xrTable43.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable43.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1038.208F);
            this.xrTable43.Name = "xrTable43";
            this.xrTable43.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow43});
            this.xrTable43.SizeF = new System.Drawing.SizeF(740F, 55F);
            this.xrTable43.StylePriority.UseBorders = false;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel212,
            this.xrLabel213,
            this.xrLabel214,
            this.txt转诊原因,
            this.xrLabel216,
            this.txt转诊机构,
            this.txt转诊});
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseTextAlignment = false;
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell85.Weight = 7.77D;
            // 
            // xrLabel212
            // 
            this.xrLabel212.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel212.LocationFloat = new DevExpress.Utils.PointFloat(9.536743E-05F, 3.499878F);
            this.xrLabel212.Name = "xrLabel212";
            this.xrLabel212.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel212.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel212.StylePriority.UseBorders = false;
            this.xrLabel212.Text = "转诊";
            // 
            // xrLabel213
            // 
            this.xrLabel213.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel213.LocationFloat = new DevExpress.Utils.PointFloat(109.479F, 3.499858F);
            this.xrLabel213.Name = "xrLabel213";
            this.xrLabel213.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel213.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel213.StylePriority.UseBorders = false;
            this.xrLabel213.Text = "1 有  2 无";
            // 
            // xrLabel214
            // 
            this.xrLabel214.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel214.LocationFloat = new DevExpress.Utils.PointFloat(9.536743E-05F, 26.50024F);
            this.xrLabel214.Name = "xrLabel214";
            this.xrLabel214.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel214.SizeF = new System.Drawing.SizeF(109.4789F, 23F);
            this.xrLabel214.StylePriority.UseBorders = false;
            this.xrLabel214.Text = "原因：";
            // 
            // txt转诊原因
            // 
            this.txt转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(109.479F, 26.5F);
            this.txt转诊原因.Name = "txt转诊原因";
            this.txt转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt转诊原因.SizeF = new System.Drawing.SizeF(260.2431F, 23F);
            this.txt转诊原因.StylePriority.UseBorders = false;
            // 
            // xrLabel216
            // 
            this.xrLabel216.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel216.LocationFloat = new DevExpress.Utils.PointFloat(369.7222F, 26.50013F);
            this.xrLabel216.Name = "xrLabel216";
            this.xrLabel216.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel216.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel216.StylePriority.UseBorders = false;
            this.xrLabel216.Text = "机构及科室：";
            // 
            // txt转诊机构
            // 
            this.txt转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(476.3746F, 26.50007F);
            this.txt转诊机构.Name = "txt转诊机构";
            this.txt转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt转诊机构.SizeF = new System.Drawing.SizeF(230.1229F, 23F);
            this.txt转诊机构.StylePriority.UseBorders = false;
            // 
            // txt转诊
            // 
            this.txt转诊.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt转诊.CanGrow = false;
            this.txt转诊.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt转诊.LocationFloat = new DevExpress.Utils.PointFloat(707.5612F, 10.00004F);
            this.txt转诊.Name = "txt转诊";
            this.txt转诊.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt转诊.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt转诊.StylePriority.UseBorders = false;
            this.txt转诊.StylePriority.UseFont = false;
            this.txt转诊.StylePriority.UseTextAlignment = false;
            this.txt转诊.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable42
            // 
            this.xrTable42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable42.LocationFloat = new DevExpress.Utils.PointFloat(370.0001F, 1093.208F);
            this.xrTable42.Name = "xrTable42";
            this.xrTable42.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42});
            this.xrTable42.SizeF = new System.Drawing.SizeF(370F, 30F);
            this.xrTable42.StylePriority.UseBorders = false;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.xrTableCell84});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseBorders = false;
            this.xrTableCell83.StylePriority.UseTextAlignment = false;
            this.xrTableCell83.Text = "随访医生签名";
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell83.Weight = 0.80356258086205967D;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt随访医生签名});
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseTextAlignment = false;
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell84.Weight = 2.1779062765464374D;
            // 
            // txt随访医生签名
            // 
            this.txt随访医生签名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt随访医生签名.LocationFloat = new DevExpress.Utils.PointFloat(8.534114F, 3.812408F);
            this.txt随访医生签名.Name = "txt随访医生签名";
            this.txt随访医生签名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt随访医生签名.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.txt随访医生签名.StylePriority.UseBorders = false;
            this.txt随访医生签名.StylePriority.UseTextAlignment = false;
            this.txt随访医生签名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable41
            // 
            this.xrTable41.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable41.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1093.208F);
            this.xrTable41.Name = "xrTable41";
            this.xrTable41.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41});
            this.xrTable41.SizeF = new System.Drawing.SizeF(370F, 30F);
            this.xrTable41.StylePriority.UseBorders = false;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.StylePriority.UseTextAlignment = false;
            this.xrTableCell81.Text = "下次随访日期";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell81.Weight = 1.1753815118962547D;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt下次随访日期});
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.StylePriority.UseTextAlignment = false;
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell82.Weight = 3.1735301366802813D;
            // 
            // txt下次随访日期
            // 
            this.txt下次随访日期.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt下次随访日期.LocationFloat = new DevExpress.Utils.PointFloat(25.97736F, 3.812442F);
            this.txt下次随访日期.Name = "txt下次随访日期";
            this.txt下次随访日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt下次随访日期.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.txt下次随访日期.StylePriority.UseBorders = false;
            this.txt下次随访日期.StylePriority.UseTextAlignment = false;
            this.txt下次随访日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable39
            // 
            this.xrTable39.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable39.LocationFloat = new DevExpress.Utils.PointFloat(0F, 989.125F);
            this.xrTable39.Name = "xrTable39";
            this.xrTable39.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39});
            this.xrTable39.SizeF = new System.Drawing.SizeF(740F, 49.08301F);
            this.xrTable39.StylePriority.UseBorders = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell78,
            this.xrTableCell79});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.StylePriority.UseTextAlignment = false;
            this.xrTableCell78.Text = "保健指导";
            this.xrTableCell78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell78.Weight = 0.40540540989223767D;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel12,
            this.txt保健指导6,
            this.txt保健指导其他,
            this.xrLabel69,
            this.xrLabel70,
            this.xrLabel191,
            this.xrLabel192,
            this.txt保健指导1,
            this.txt保健指导2,
            this.txt保健指导3,
            this.txt保健指导4,
            this.txt保健指导5,
            this.xrLabel190,
            this.xrLabel189});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.StylePriority.UseTextAlignment = false;
            this.xrTableCell79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell79.Weight = 2.5945945901077621D;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.CanGrow = false;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(596.6669F, 24.08282F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.Text = "/";
            // 
            // txt保健指导6
            // 
            this.txt保健指导6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt保健指导6.CanGrow = false;
            this.txt保健指导6.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt保健指导6.LocationFloat = new DevExpress.Utils.PointFloat(607.0836F, 24.08285F);
            this.txt保健指导6.Name = "txt保健指导6";
            this.txt保健指导6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt保健指导6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt保健指导6.StylePriority.UseBorders = false;
            this.txt保健指导6.StylePriority.UseFont = false;
            this.txt保健指导6.StylePriority.UseTextAlignment = false;
            this.txt保健指导6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt保健指导其他
            // 
            this.txt保健指导其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt保健指导其他.LocationFloat = new DevExpress.Utils.PointFloat(218.5416F, 24.91675F);
            this.txt保健指导其他.Name = "txt保健指导其他";
            this.txt保健指导其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt保健指导其他.SizeF = new System.Drawing.SizeF(187.7359F, 21.08337F);
            this.txt保健指导其他.StylePriority.UseBorders = false;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel69.CanGrow = false;
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(567.2194F, 23.00011F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel69.StylePriority.UseBorders = false;
            this.xrLabel69.Text = "/";
            // 
            // xrLabel70
            // 
            this.xrLabel70.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel70.CanGrow = false;
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(536.9268F, 23.00011F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel70.StylePriority.UseBorders = false;
            this.xrLabel70.Text = "/";
            // 
            // xrLabel191
            // 
            this.xrLabel191.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel191.CanGrow = false;
            this.xrLabel191.LocationFloat = new DevExpress.Utils.PointFloat(506.6345F, 23.00011F);
            this.xrLabel191.Name = "xrLabel191";
            this.xrLabel191.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel191.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel191.StylePriority.UseBorders = false;
            this.xrLabel191.Text = "/";
            // 
            // xrLabel192
            // 
            this.xrLabel192.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel192.CanGrow = false;
            this.xrLabel192.LocationFloat = new DevExpress.Utils.PointFloat(476.342F, 22.99999F);
            this.xrLabel192.Name = "xrLabel192";
            this.xrLabel192.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel192.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel192.StylePriority.UseBorders = false;
            this.xrLabel192.Text = "/";
            // 
            // txt保健指导1
            // 
            this.txt保健指导1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt保健指导1.CanGrow = false;
            this.txt保健指导1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt保健指导1.LocationFloat = new DevExpress.Utils.PointFloat(457.4041F, 23.00002F);
            this.txt保健指导1.Name = "txt保健指导1";
            this.txt保健指导1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt保健指导1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt保健指导1.StylePriority.UseBorders = false;
            this.txt保健指导1.StylePriority.UseFont = false;
            this.txt保健指导1.StylePriority.UseTextAlignment = false;
            this.txt保健指导1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt保健指导2
            // 
            this.txt保健指导2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt保健指导2.CanGrow = false;
            this.txt保健指导2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt保健指导2.LocationFloat = new DevExpress.Utils.PointFloat(487.6967F, 22.99999F);
            this.txt保健指导2.Name = "txt保健指导2";
            this.txt保健指导2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt保健指导2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt保健指导2.StylePriority.UseBorders = false;
            this.txt保健指导2.StylePriority.UseFont = false;
            this.txt保健指导2.StylePriority.UseTextAlignment = false;
            this.txt保健指导2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt保健指导3
            // 
            this.txt保健指导3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt保健指导3.CanGrow = false;
            this.txt保健指导3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt保健指导3.LocationFloat = new DevExpress.Utils.PointFloat(517.989F, 22.99999F);
            this.txt保健指导3.Name = "txt保健指导3";
            this.txt保健指导3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt保健指导3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt保健指导3.StylePriority.UseBorders = false;
            this.txt保健指导3.StylePriority.UseFont = false;
            this.txt保健指导3.StylePriority.UseTextAlignment = false;
            this.txt保健指导3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt保健指导4
            // 
            this.txt保健指导4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt保健指导4.CanGrow = false;
            this.txt保健指导4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt保健指导4.LocationFloat = new DevExpress.Utils.PointFloat(548.2814F, 22.99999F);
            this.txt保健指导4.Name = "txt保健指导4";
            this.txt保健指导4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt保健指导4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt保健指导4.StylePriority.UseBorders = false;
            this.txt保健指导4.StylePriority.UseFont = false;
            this.txt保健指导4.StylePriority.UseTextAlignment = false;
            this.txt保健指导4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt保健指导5
            // 
            this.txt保健指导5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt保健指导5.CanGrow = false;
            this.txt保健指导5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt保健指导5.LocationFloat = new DevExpress.Utils.PointFloat(578.5739F, 22.99999F);
            this.txt保健指导5.Name = "txt保健指导5";
            this.txt保健指导5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt保健指导5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt保健指导5.StylePriority.UseBorders = false;
            this.txt保健指导5.StylePriority.UseFont = false;
            this.txt保健指导5.StylePriority.UseTextAlignment = false;
            this.txt保健指导5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel190
            // 
            this.xrLabel190.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel190.LocationFloat = new DevExpress.Utils.PointFloat(10.52097F, 23.00012F);
            this.xrLabel190.Name = "xrLabel190";
            this.xrLabel190.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel190.SizeF = new System.Drawing.SizeF(208.0206F, 23F);
            this.xrLabel190.StylePriority.UseBorders = false;
            this.xrLabel190.StylePriority.UseTextAlignment = false;
            this.xrLabel190.Text = "5 产前筛查宣传告知    6 其他";
            this.xrLabel190.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel189
            // 
            this.xrLabel189.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel189.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 0.0001220703F);
            this.xrLabel189.Name = "xrLabel189";
            this.xrLabel189.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel189.SizeF = new System.Drawing.SizeF(485.5581F, 23F);
            this.xrLabel189.StylePriority.UseBorders = false;
            this.xrLabel189.StylePriority.UseTextAlignment = false;
            this.xrLabel189.Text = "1 个人卫生   2 心理   3 营养   4 避免畸形因素和疾病对胚胎的不良影响";
            this.xrLabel189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable38
            // 
            this.xrTable38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable38.LocationFloat = new DevExpress.Utils.PointFloat(0F, 962.125F);
            this.xrTable38.Name = "xrTable38";
            this.xrTable38.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable38.SizeF = new System.Drawing.SizeF(739.842F, 27F);
            this.xrTable38.StylePriority.UseBorders = false;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.xrTableCell77});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.StylePriority.UseTextAlignment = false;
            this.xrTableCell76.Text = "总体评估";
            this.xrTableCell76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell76.Weight = 0.40540540989223767D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt总体评估异常,
            this.xrLabel187,
            this.txt总体评估});
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseTextAlignment = false;
            this.xrTableCell77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell77.Weight = 2.5939539684069595D;
            // 
            // txt总体评估异常
            // 
            this.txt总体评估异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt总体评估异常.LocationFloat = new DevExpress.Utils.PointFloat(162.9999F, 3.041626F);
            this.txt总体评估异常.Name = "txt总体评估异常";
            this.txt总体评估异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt总体评估异常.SizeF = new System.Drawing.SizeF(243.2776F, 19.95837F);
            this.txt总体评估异常.StylePriority.UseBorders = false;
            // 
            // xrLabel187
            // 
            this.xrLabel187.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel187.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 3.041626F);
            this.xrLabel187.Name = "xrLabel187";
            this.xrLabel187.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel187.SizeF = new System.Drawing.SizeF(149.479F, 19.95844F);
            this.xrLabel187.StylePriority.UseBorders = false;
            this.xrLabel187.StylePriority.UseTextAlignment = false;
            this.xrLabel187.Text = "1 未见异常   2 异常";
            this.xrLabel187.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt总体评估
            // 
            this.txt总体评估.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt总体评估.CanGrow = false;
            this.txt总体评估.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt总体评估.LocationFloat = new DevExpress.Utils.PointFloat(607.8109F, 3.041617F);
            this.txt总体评估.Name = "txt总体评估";
            this.txt总体评估.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt总体评估.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt总体评估.StylePriority.UseBorders = false;
            this.txt总体评估.StylePriority.UseFont = false;
            this.txt总体评估.StylePriority.UseTextAlignment = false;
            this.txt总体评估.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable37
            // 
            this.xrTable37.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable37.LocationFloat = new DevExpress.Utils.PointFloat(100.0002F, 935.125F);
            this.xrTable37.Name = "xrTable37";
            this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37});
            this.xrTable37.SizeF = new System.Drawing.SizeF(640F, 27F);
            this.xrTable37.StylePriority.UseBorders = false;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell74,
            this.xrTableCell75});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseTextAlignment = false;
            this.xrTableCell74.Text = "B超*";
            this.xrTableCell74.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell74.Weight = 1.2506244496712025D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txtB超});
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseTextAlignment = false;
            this.xrTableCell75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell75.Weight = 5.4193724985709855D;
            // 
            // txtB超
            // 
            this.txtB超.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txtB超.LocationFloat = new DevExpress.Utils.PointFloat(12.15982F, 6.357829E-05F);
            this.txtB超.Name = "txtB超";
            this.txtB超.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtB超.SizeF = new System.Drawing.SizeF(489.2427F, 23F);
            this.txtB超.StylePriority.UseBorders = false;
            this.txtB超.StylePriority.UseTextAlignment = false;
            this.txtB超.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable35
            // 
            this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable35.LocationFloat = new DevExpress.Utils.PointFloat(99.99991F, 881.125F);
            this.xrTable35.Name = "xrTable35";
            this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTable35.SizeF = new System.Drawing.SizeF(640F, 27F);
            this.xrTable35.StylePriority.UseBorders = false;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell70,
            this.xrTableCell71});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.StylePriority.UseTextAlignment = false;
            this.xrTableCell70.Text = "梅毒血清学试验*";
            this.xrTableCell70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell70.Weight = 0.56320162280499186D;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel183,
            this.txt梅毒血清学试验});
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.StylePriority.UseTextAlignment = false;
            this.xrTableCell71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell71.Weight = 2.4405403958486453D;
            // 
            // xrLabel183
            // 
            this.xrLabel183.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel183.LocationFloat = new DevExpress.Utils.PointFloat(13.3333F, 0F);
            this.xrLabel183.Name = "xrLabel183";
            this.xrLabel183.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel183.SizeF = new System.Drawing.SizeF(120F, 23F);
            this.xrLabel183.StylePriority.UseBorders = false;
            this.xrLabel183.Text = "1 阴性   2 阳性";
            // 
            // txt梅毒血清学试验
            // 
            this.txt梅毒血清学试验.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt梅毒血清学试验.CanGrow = false;
            this.txt梅毒血清学试验.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt梅毒血清学试验.LocationFloat = new DevExpress.Utils.PointFloat(487.811F, 5.000019F);
            this.txt梅毒血清学试验.Name = "txt梅毒血清学试验";
            this.txt梅毒血清学试验.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt梅毒血清学试验.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt梅毒血清学试验.StylePriority.UseBorders = false;
            this.txt梅毒血清学试验.StylePriority.UseFont = false;
            this.txt梅毒血清学试验.StylePriority.UseTextAlignment = false;
            this.txt梅毒血清学试验.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable34
            // 
            this.xrTable34.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable34.LocationFloat = new DevExpress.Utils.PointFloat(99.99991F, 831.125F);
            this.xrTable34.Name = "xrTable34";
            this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrTable34.SizeF = new System.Drawing.SizeF(640F, 50F);
            this.xrTable34.StylePriority.UseBorders = false;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.xrTableCell69});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.StylePriority.UseTextAlignment = false;
            this.xrTableCell68.Text = "乙型肝炎五项";
            this.xrTableCell68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell68.Weight = 0.56249999723177557D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel180,
            this.txt乙型肝炎核心抗体,
            this.xrLabel178,
            this.txt乙型肝炎e抗体,
            this.xrLabel176,
            this.txt乙型肝炎e抗原,
            this.xrLabel174,
            this.txt乙型肝炎表面抗体,
            this.txt乙型肝炎表面抗原,
            this.xrLabel172});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.StylePriority.UseTextAlignment = false;
            this.xrTableCell69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell69.Weight = 2.4375000027682243D;
            // 
            // xrLabel180
            // 
            this.xrLabel180.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel180.LocationFloat = new DevExpress.Utils.PointFloat(338.5142F, 23.00008F);
            this.xrLabel180.Name = "xrLabel180";
            this.xrLabel180.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel180.SizeF = new System.Drawing.SizeF(125F, 23F);
            this.xrLabel180.StylePriority.UseBorders = false;
            this.xrLabel180.Text = "乙型肝炎核心抗体";
            // 
            // txt乙型肝炎核心抗体
            // 
            this.txt乙型肝炎核心抗体.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt乙型肝炎核心抗体.LocationFloat = new DevExpress.Utils.PointFloat(463.5142F, 23.00008F);
            this.txt乙型肝炎核心抗体.Name = "txt乙型肝炎核心抗体";
            this.txt乙型肝炎核心抗体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt乙型肝炎核心抗体.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.txt乙型肝炎核心抗体.StylePriority.UseBorders = false;
            // 
            // xrLabel178
            // 
            this.xrLabel178.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel178.LocationFloat = new DevExpress.Utils.PointFloat(178.5142F, 23.00008F);
            this.xrLabel178.Name = "xrLabel178";
            this.xrLabel178.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel178.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.xrLabel178.StylePriority.UseBorders = false;
            this.xrLabel178.Text = "乙型肝炎e抗体";
            // 
            // txt乙型肝炎e抗体
            // 
            this.txt乙型肝炎e抗体.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt乙型肝炎e抗体.LocationFloat = new DevExpress.Utils.PointFloat(278.5142F, 23.00008F);
            this.txt乙型肝炎e抗体.Name = "txt乙型肝炎e抗体";
            this.txt乙型肝炎e抗体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt乙型肝炎e抗体.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.txt乙型肝炎e抗体.StylePriority.UseBorders = false;
            // 
            // xrLabel176
            // 
            this.xrLabel176.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel176.LocationFloat = new DevExpress.Utils.PointFloat(12.86163F, 23.00006F);
            this.xrLabel176.Name = "xrLabel176";
            this.xrLabel176.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel176.SizeF = new System.Drawing.SizeF(105F, 23F);
            this.xrLabel176.StylePriority.UseBorders = false;
            this.xrLabel176.Text = "乙型肝炎e抗原";
            // 
            // txt乙型肝炎e抗原
            // 
            this.txt乙型肝炎e抗原.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt乙型肝炎e抗原.LocationFloat = new DevExpress.Utils.PointFloat(117.8616F, 23.00006F);
            this.txt乙型肝炎e抗原.Name = "txt乙型肝炎e抗原";
            this.txt乙型肝炎e抗原.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt乙型肝炎e抗原.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.txt乙型肝炎e抗原.StylePriority.UseBorders = false;
            // 
            // xrLabel174
            // 
            this.xrLabel174.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel174.LocationFloat = new DevExpress.Utils.PointFloat(203.7266F, 0F);
            this.xrLabel174.Name = "xrLabel174";
            this.xrLabel174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel174.SizeF = new System.Drawing.SizeF(126F, 23.00006F);
            this.xrLabel174.StylePriority.UseBorders = false;
            this.xrLabel174.Text = "乙型肝炎表面抗体";
            // 
            // txt乙型肝炎表面抗体
            // 
            this.txt乙型肝炎表面抗体.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt乙型肝炎表面抗体.LocationFloat = new DevExpress.Utils.PointFloat(329.7266F, 6.357829E-05F);
            this.txt乙型肝炎表面抗体.Name = "txt乙型肝炎表面抗体";
            this.txt乙型肝炎表面抗体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt乙型肝炎表面抗体.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.txt乙型肝炎表面抗体.StylePriority.UseBorders = false;
            // 
            // txt乙型肝炎表面抗原
            // 
            this.txt乙型肝炎表面抗原.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt乙型肝炎表面抗原.LocationFloat = new DevExpress.Utils.PointFloat(143.7267F, 6.103516E-05F);
            this.txt乙型肝炎表面抗原.Name = "txt乙型肝炎表面抗原";
            this.txt乙型肝炎表面抗原.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt乙型肝炎表面抗原.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.txt乙型肝炎表面抗原.StylePriority.UseBorders = false;
            // 
            // xrLabel172
            // 
            this.xrLabel172.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel172.LocationFloat = new DevExpress.Utils.PointFloat(13.33318F, 6.103516E-05F);
            this.xrLabel172.Name = "xrLabel172";
            this.xrLabel172.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel172.SizeF = new System.Drawing.SizeF(130.3932F, 23F);
            this.xrLabel172.StylePriority.UseBorders = false;
            this.xrLabel172.Text = "乙型肝炎表面抗原";
            // 
            // xrTable33
            // 
            this.xrTable33.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable33.LocationFloat = new DevExpress.Utils.PointFloat(220.0002F, 804.1251F);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrTable33.SizeF = new System.Drawing.SizeF(519.9999F, 27F);
            this.xrTable33.StylePriority.UseBorders = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel171,
            this.txt阴道清洁度});
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.StylePriority.UseTextAlignment = false;
            this.xrTableCell67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell67.Weight = 3D;
            // 
            // xrLabel171
            // 
            this.xrLabel171.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel171.LocationFloat = new DevExpress.Utils.PointFloat(13.33319F, 0F);
            this.xrLabel171.Name = "xrLabel171";
            this.xrLabel171.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel171.SizeF = new System.Drawing.SizeF(303.2888F, 23F);
            this.xrLabel171.StylePriority.UseBorders = false;
            this.xrLabel171.Text = "阴道清洁度：1 Ⅰ度  2 Ⅱ度  3 Ⅲ度  4 Ⅳ度";
            // 
            // txt阴道清洁度
            // 
            this.txt阴道清洁度.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt阴道清洁度.CanGrow = false;
            this.txt阴道清洁度.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt阴道清洁度.LocationFloat = new DevExpress.Utils.PointFloat(487.8107F, 4.999956F);
            this.txt阴道清洁度.Name = "txt阴道清洁度";
            this.txt阴道清洁度.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阴道清洁度.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt阴道清洁度.StylePriority.UseBorders = false;
            this.txt阴道清洁度.StylePriority.UseFont = false;
            this.txt阴道清洁度.StylePriority.UseTextAlignment = false;
            this.txt阴道清洁度.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable32
            // 
            this.xrTable32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable32.LocationFloat = new DevExpress.Utils.PointFloat(220.0002F, 774.9584F);
            this.xrTable32.Name = "xrTable32";
            this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.xrTable32.SizeF = new System.Drawing.SizeF(519.9997F, 29.16663F);
            this.xrTable32.StylePriority.UseBorders = false;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt阴道分泌物3,
            this.xrLabel166,
            this.txt阴道分泌物2,
            this.xrLabel168,
            this.txt阴道分泌物1,
            this.txt阴道分泌物其他,
            this.xrLabel163});
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.StylePriority.UseTextAlignment = false;
            this.xrTableCell65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell65.Weight = 3D;
            // 
            // txt阴道分泌物3
            // 
            this.txt阴道分泌物3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt阴道分泌物3.CanGrow = false;
            this.txt阴道分泌物3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt阴道分泌物3.LocationFloat = new DevExpress.Utils.PointFloat(487.561F, 5.000082F);
            this.txt阴道分泌物3.Name = "txt阴道分泌物3";
            this.txt阴道分泌物3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阴道分泌物3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt阴道分泌物3.StylePriority.UseBorders = false;
            this.txt阴道分泌物3.StylePriority.UseFont = false;
            this.txt阴道分泌物3.StylePriority.UseTextAlignment = false;
            this.txt阴道分泌物3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel166
            // 
            this.xrLabel166.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel166.LocationFloat = new DevExpress.Utils.PointFloat(476.0806F, 5.000082F);
            this.xrLabel166.Name = "xrLabel166";
            this.xrLabel166.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel166.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel166.StylePriority.UseBorders = false;
            this.xrLabel166.Text = "/";
            // 
            // txt阴道分泌物2
            // 
            this.txt阴道分泌物2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt阴道分泌物2.CanGrow = false;
            this.txt阴道分泌物2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt阴道分泌物2.LocationFloat = new DevExpress.Utils.PointFloat(457.017F, 5.000082F);
            this.txt阴道分泌物2.Name = "txt阴道分泌物2";
            this.txt阴道分泌物2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阴道分泌物2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt阴道分泌物2.StylePriority.UseBorders = false;
            this.txt阴道分泌物2.StylePriority.UseFont = false;
            this.txt阴道分泌物2.StylePriority.UseTextAlignment = false;
            this.txt阴道分泌物2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel168
            // 
            this.xrLabel168.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel168.CanGrow = false;
            this.xrLabel168.LocationFloat = new DevExpress.Utils.PointFloat(445.5367F, 4.999956F);
            this.xrLabel168.Name = "xrLabel168";
            this.xrLabel168.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel168.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel168.StylePriority.UseBorders = false;
            this.xrLabel168.Text = "/";
            // 
            // txt阴道分泌物1
            // 
            this.txt阴道分泌物1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt阴道分泌物1.CanGrow = false;
            this.txt阴道分泌物1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt阴道分泌物1.LocationFloat = new DevExpress.Utils.PointFloat(426.473F, 5.000082F);
            this.txt阴道分泌物1.Name = "txt阴道分泌物1";
            this.txt阴道分泌物1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阴道分泌物1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt阴道分泌物1.StylePriority.UseBorders = false;
            this.txt阴道分泌物1.StylePriority.UseFont = false;
            this.txt阴道分泌物1.StylePriority.UseTextAlignment = false;
            this.txt阴道分泌物1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt阴道分泌物其他
            // 
            this.txt阴道分泌物其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt阴道分泌物其他.LocationFloat = new DevExpress.Utils.PointFloat(290.8375F, 3.178914E-05F);
            this.txt阴道分泌物其他.Name = "txt阴道分泌物其他";
            this.txt阴道分泌物其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阴道分泌物其他.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.txt阴道分泌物其他.StylePriority.UseBorders = false;
            // 
            // xrLabel163
            // 
            this.xrLabel163.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel163.LocationFloat = new DevExpress.Utils.PointFloat(13.33325F, 6.103516E-05F);
            this.xrLabel163.Name = "xrLabel163";
            this.xrLabel163.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel163.SizeF = new System.Drawing.SizeF(277.5041F, 23F);
            this.xrLabel163.StylePriority.UseBorders = false;
            this.xrLabel163.Text = "1 未见异常 2 滴虫 3 假丝酵母菌 4 其他";
            // 
            // xrTable31
            // 
            this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable31.LocationFloat = new DevExpress.Utils.PointFloat(100.0001F, 774.9584F);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable31.SizeF = new System.Drawing.SizeF(120F, 56F);
            this.xrTable31.StylePriority.UseBorders = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.StylePriority.UseTextAlignment = false;
            this.xrTableCell66.Text = "阴道分泌物*";
            this.xrTableCell66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell66.Weight = 2.7692307692307692D;
            // 
            // xrTable30
            // 
            this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable30.LocationFloat = new DevExpress.Utils.PointFloat(99.99991F, 747.7917F);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrTable30.SizeF = new System.Drawing.SizeF(640F, 27F);
            this.xrTable30.StylePriority.UseBorders = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell64});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.StylePriority.UseTextAlignment = false;
            this.xrTableCell63.Text = "肾功能";
            this.xrTableCell63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell63.Weight = 0.56249999723177557D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel160,
            this.txt血尿素氮,
            this.xrLabel162,
            this.xrLabel157,
            this.txt血清肌酐,
            this.xrLabel159});
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.StylePriority.UseTextAlignment = false;
            this.xrTableCell64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell64.Weight = 2.4375000027682243D;
            // 
            // xrLabel160
            // 
            this.xrLabel160.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel160.LocationFloat = new DevExpress.Utils.PointFloat(241.4137F, 0F);
            this.xrLabel160.Name = "xrLabel160";
            this.xrLabel160.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel160.SizeF = new System.Drawing.SizeF(75.20828F, 23F);
            this.xrLabel160.StylePriority.UseBorders = false;
            this.xrLabel160.Text = "血尿素氮";
            // 
            // txt血尿素氮
            // 
            this.txt血尿素氮.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血尿素氮.LocationFloat = new DevExpress.Utils.PointFloat(319.9107F, 0F);
            this.txt血尿素氮.Name = "txt血尿素氮";
            this.txt血尿素氮.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血尿素氮.SizeF = new System.Drawing.SizeF(51.89624F, 23F);
            this.txt血尿素氮.StylePriority.UseBorders = false;
            // 
            // xrLabel162
            // 
            this.xrLabel162.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel162.LocationFloat = new DevExpress.Utils.PointFloat(371.8072F, 0F);
            this.xrLabel162.Name = "xrLabel162";
            this.xrLabel162.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel162.SizeF = new System.Drawing.SizeF(50.84216F, 23F);
            this.xrLabel162.StylePriority.UseBorders = false;
            this.xrLabel162.Text = "mmol/L";
            // 
            // xrLabel157
            // 
            this.xrLabel157.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel157.LocationFloat = new DevExpress.Utils.PointFloat(13.33319F, 0F);
            this.xrLabel157.Name = "xrLabel157";
            this.xrLabel157.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel157.SizeF = new System.Drawing.SizeF(75.20828F, 23F);
            this.xrLabel157.StylePriority.UseBorders = false;
            this.xrLabel157.Text = "血清肌酐";
            // 
            // txt血清肌酐
            // 
            this.txt血清肌酐.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血清肌酐.LocationFloat = new DevExpress.Utils.PointFloat(91.83019F, 0F);
            this.txt血清肌酐.Name = "txt血清肌酐";
            this.txt血清肌酐.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血清肌酐.SizeF = new System.Drawing.SizeF(51.89624F, 23F);
            this.txt血清肌酐.StylePriority.UseBorders = false;
            // 
            // xrLabel159
            // 
            this.xrLabel159.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel159.LocationFloat = new DevExpress.Utils.PointFloat(143.7266F, 0F);
            this.xrLabel159.Name = "xrLabel159";
            this.xrLabel159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel159.SizeF = new System.Drawing.SizeF(62.315F, 23F);
            this.xrLabel159.StylePriority.UseBorders = false;
            this.xrLabel159.Text = "μmol/L";
            // 
            // xrTable29
            // 
            this.xrTable29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable29.LocationFloat = new DevExpress.Utils.PointFloat(99.99991F, 692.7917F);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTable29.SizeF = new System.Drawing.SizeF(640F, 55F);
            this.xrTable29.StylePriority.UseBorders = false;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.xrTableCell62});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.StylePriority.UseTextAlignment = false;
            this.xrTableCell61.Text = "肝功能";
            this.xrTableCell61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell61.Weight = 0.56334471340383252D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel154,
            this.txt结合胆红素,
            this.xrLabel156,
            this.xrLabel151,
            this.txt总胆红素,
            this.xrLabel153,
            this.xrLabel148,
            this.txt白蛋白,
            this.xrLabel150,
            this.xrLabel145,
            this.txt血清谷草转氨酶,
            this.xrLabel147,
            this.xrLabel144,
            this.txt血清谷丙转氨酶,
            this.xrLabel142});
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.StylePriority.UseTextAlignment = false;
            this.xrTableCell62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell62.Weight = 2.44116034179267D;
            // 
            // xrLabel154
            // 
            this.xrLabel154.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel154.LocationFloat = new DevExpress.Utils.PointFloat(334.9861F, 23F);
            this.xrLabel154.Name = "xrLabel154";
            this.xrLabel154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel154.SizeF = new System.Drawing.SizeF(77.19745F, 22.99994F);
            this.xrLabel154.StylePriority.UseBorders = false;
            this.xrLabel154.Text = "结合胆红素";
            // 
            // txt结合胆红素
            // 
            this.txt结合胆红素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt结合胆红素.LocationFloat = new DevExpress.Utils.PointFloat(413.5142F, 22.99995F);
            this.txt结合胆红素.Name = "txt结合胆红素";
            this.txt结合胆红素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt结合胆红素.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.txt结合胆红素.StylePriority.UseBorders = false;
            // 
            // xrLabel156
            // 
            this.xrLabel156.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel156.LocationFloat = new DevExpress.Utils.PointFloat(463.5141F, 23F);
            this.xrLabel156.Name = "xrLabel156";
            this.xrLabel156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel156.SizeF = new System.Drawing.SizeF(56.32794F, 22.99994F);
            this.xrLabel156.StylePriority.UseBorders = false;
            this.xrLabel156.Text = "μmol/L";
            // 
            // xrLabel151
            // 
            this.xrLabel151.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel151.LocationFloat = new DevExpress.Utils.PointFloat(152.1108F, 23.00002F);
            this.xrLabel151.Name = "xrLabel151";
            this.xrLabel151.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel151.SizeF = new System.Drawing.SizeF(75.20828F, 23F);
            this.xrLabel151.StylePriority.UseBorders = false;
            this.xrLabel151.Text = "总胆红素";
            // 
            // txt总胆红素
            // 
            this.txt总胆红素.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt总胆红素.LocationFloat = new DevExpress.Utils.PointFloat(227.3192F, 22.99999F);
            this.txt总胆红素.Name = "txt总胆红素";
            this.txt总胆红素.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt总胆红素.SizeF = new System.Drawing.SizeF(51.89624F, 23F);
            this.txt总胆红素.StylePriority.UseBorders = false;
            // 
            // xrLabel153
            // 
            this.xrLabel153.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel153.LocationFloat = new DevExpress.Utils.PointFloat(280.3724F, 22.99994F);
            this.xrLabel153.Name = "xrLabel153";
            this.xrLabel153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel153.SizeF = new System.Drawing.SizeF(54.61371F, 23F);
            this.xrLabel153.StylePriority.UseBorders = false;
            this.xrLabel153.Text = "μmol/L";
            // 
            // xrLabel148
            // 
            this.xrLabel148.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel148.LocationFloat = new DevExpress.Utils.PointFloat(12.6805F, 22.99999F);
            this.xrLabel148.Name = "xrLabel148";
            this.xrLabel148.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel148.SizeF = new System.Drawing.SizeF(57.94438F, 23F);
            this.xrLabel148.StylePriority.UseBorders = false;
            this.xrLabel148.Text = "白蛋白";
            // 
            // txt白蛋白
            // 
            this.txt白蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt白蛋白.LocationFloat = new DevExpress.Utils.PointFloat(70.62486F, 22.99999F);
            this.txt白蛋白.Name = "txt白蛋白";
            this.txt白蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt白蛋白.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.txt白蛋白.StylePriority.UseBorders = false;
            // 
            // xrLabel150
            // 
            this.xrLabel150.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel150.LocationFloat = new DevExpress.Utils.PointFloat(120.6249F, 22.99995F);
            this.xrLabel150.Name = "xrLabel150";
            this.xrLabel150.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel150.SizeF = new System.Drawing.SizeF(31.4859F, 23F);
            this.xrLabel150.StylePriority.UseBorders = false;
            this.xrLabel150.Text = "g/L";
            // 
            // xrLabel145
            // 
            this.xrLabel145.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel145.LocationFloat = new DevExpress.Utils.PointFloat(219.6108F, 0F);
            this.xrLabel145.Name = "xrLabel145";
            this.xrLabel145.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel145.SizeF = new System.Drawing.SizeF(116.2774F, 23F);
            this.xrLabel145.StylePriority.UseBorders = false;
            this.xrLabel145.Text = "血清谷草转氨酶";
            // 
            // txt血清谷草转氨酶
            // 
            this.txt血清谷草转氨酶.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血清谷草转氨酶.LocationFloat = new DevExpress.Utils.PointFloat(336.0433F, 0F);
            this.txt血清谷草转氨酶.Name = "txt血清谷草转氨酶";
            this.txt血清谷草转氨酶.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血清谷草转氨酶.SizeF = new System.Drawing.SizeF(62.47925F, 23F);
            this.txt血清谷草转氨酶.StylePriority.UseBorders = false;
            // 
            // xrLabel147
            // 
            this.xrLabel147.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel147.LocationFloat = new DevExpress.Utils.PointFloat(398.5226F, 0F);
            this.xrLabel147.Name = "xrLabel147";
            this.xrLabel147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel147.SizeF = new System.Drawing.SizeF(31.4859F, 23F);
            this.xrLabel147.StylePriority.UseBorders = false;
            this.xrLabel147.Text = "U/L";
            // 
            // xrLabel144
            // 
            this.xrLabel144.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel144.LocationFloat = new DevExpress.Utils.PointFloat(188.1249F, 0F);
            this.xrLabel144.Name = "xrLabel144";
            this.xrLabel144.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel144.SizeF = new System.Drawing.SizeF(31.4859F, 23F);
            this.xrLabel144.StylePriority.UseBorders = false;
            this.xrLabel144.Text = "U/L";
            // 
            // txt血清谷丙转氨酶
            // 
            this.txt血清谷丙转氨酶.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血清谷丙转氨酶.LocationFloat = new DevExpress.Utils.PointFloat(128.9583F, 0F);
            this.txt血清谷丙转氨酶.Name = "txt血清谷丙转氨酶";
            this.txt血清谷丙转氨酶.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt血清谷丙转氨酶.SizeF = new System.Drawing.SizeF(59.16666F, 23F);
            this.txt血清谷丙转氨酶.StylePriority.UseBorders = false;
            // 
            // xrLabel142
            // 
            this.xrLabel142.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel142.LocationFloat = new DevExpress.Utils.PointFloat(12.68082F, 0F);
            this.xrLabel142.Name = "xrLabel142";
            this.xrLabel142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel142.SizeF = new System.Drawing.SizeF(116.2774F, 23F);
            this.xrLabel142.StylePriority.UseBorders = false;
            this.xrLabel142.Text = "血清谷丙转氨酶";
            // 
            // xrTable28
            // 
            this.xrTable28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable28.LocationFloat = new DevExpress.Utils.PointFloat(100F, 664.9584F);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable28.SizeF = new System.Drawing.SizeF(640F, 27.83331F);
            this.xrTable28.StylePriority.UseBorders = false;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59,
            this.xrTableCell60});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseTextAlignment = false;
            this.xrTableCell59.Text = "血糖*";
            this.xrTableCell59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell59.Weight = 1.8112498499187204D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel141,
            this.txt血糖});
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseTextAlignment = false;
            this.xrTableCell60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell60.Weight = 7.8487495397297167D;
            // 
            // xrLabel141
            // 
            this.xrLabel141.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel141.LocationFloat = new DevExpress.Utils.PointFloat(113.5141F, 0F);
            this.xrLabel141.Name = "xrLabel141";
            this.xrLabel141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel141.SizeF = new System.Drawing.SizeF(66.84775F, 23F);
            this.xrLabel141.StylePriority.UseBorders = false;
            this.xrLabel141.Text = "mmol/L";
            // 
            // txt血糖
            // 
            this.txt血糖.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血糖.LocationFloat = new DevExpress.Utils.PointFloat(13.33319F, 0F);
            this.txt血糖.Name = "txt血糖";
            this.txt血糖.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt血糖.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.txt血糖.StylePriority.UseBorders = false;
            this.txt血糖.StylePriority.UseTextAlignment = false;
            this.txt血糖.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable27
            // 
            this.xrTable27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable27.LocationFloat = new DevExpress.Utils.PointFloat(100F, 619.9584F);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTable27.SizeF = new System.Drawing.SizeF(640F, 45F);
            this.xrTable27.StylePriority.UseBorders = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell56,
            this.xrTableCell57,
            this.xrTableCell58});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.StylePriority.UseTextAlignment = false;
            this.xrTableCell56.Text = "血型";
            this.xrTableCell56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell56.Weight = 0.32731858688911741D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel139,
            this.xrLabel138});
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.StylePriority.UseTextAlignment = false;
            this.xrTableCell57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell57.Weight = 0.23379899038912111D;
            // 
            // xrLabel139
            // 
            this.xrLabel139.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrLabel139.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 22.00006F);
            this.xrLabel139.Name = "xrLabel139";
            this.xrLabel139.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel139.SizeF = new System.Drawing.SizeF(50F, 22F);
            this.xrLabel139.StylePriority.UseBorders = false;
            this.xrLabel139.Text = "Rh*";
            // 
            // xrLabel138
            // 
            this.xrLabel138.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel138.LocationFloat = new DevExpress.Utils.PointFloat(4.999987F, 0F);
            this.xrLabel138.Name = "xrLabel138";
            this.xrLabel138.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel138.SizeF = new System.Drawing.SizeF(35F, 20F);
            this.xrLabel138.StylePriority.UseBorders = false;
            this.xrLabel138.Text = "ABO";
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt血型});
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell58.Weight = 2.4315095110829996D;
            // 
            // txt血型
            // 
            this.txt血型.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt血型.LocationFloat = new DevExpress.Utils.PointFloat(12.15996F, 0F);
            this.txt血型.Name = "txt血型";
            this.txt血型.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血型.SizeF = new System.Drawing.SizeF(490F, 40F);
            this.txt血型.StylePriority.UseBorders = false;
            this.txt血型.StylePriority.UseTextAlignment = false;
            this.txt血型.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable26
            // 
            this.xrTable26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable26.LocationFloat = new DevExpress.Utils.PointFloat(100.0001F, 589.9584F);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable26.SizeF = new System.Drawing.SizeF(640F, 30F);
            this.xrTable26.StylePriority.UseBorders = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell54,
            this.xrTableCell55});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.StylePriority.UseTextAlignment = false;
            this.xrTableCell54.Text = "尿常规";
            this.xrTableCell54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell54.Weight = 0.5625000321555782D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel136,
            this.txt尿常规其他,
            this.xrLabel134,
            this.txt尿潜血,
            this.xrLabel132,
            this.txt尿酮体,
            this.xrLabel130,
            this.txt尿糖,
            this.txt尿蛋白,
            this.xrLabel128});
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseTextAlignment = false;
            this.xrTableCell55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell55.Weight = 2.4375001712687645D;
            // 
            // xrLabel136
            // 
            this.xrLabel136.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel136.LocationFloat = new DevExpress.Utils.PointFloat(414.9036F, 0F);
            this.xrLabel136.Name = "xrLabel136";
            this.xrLabel136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel136.SizeF = new System.Drawing.SizeF(45.07782F, 23F);
            this.xrLabel136.StylePriority.UseBorders = false;
            this.xrLabel136.Text = "其他";
            // 
            // txt尿常规其他
            // 
            this.txt尿常规其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt尿常规其他.LocationFloat = new DevExpress.Utils.PointFloat(461.3538F, 0F);
            this.txt尿常规其他.Name = "txt尿常规其他";
            this.txt尿常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt尿常规其他.SizeF = new System.Drawing.SizeF(40.72748F, 23F);
            this.txt尿常规其他.StylePriority.UseBorders = false;
            // 
            // xrLabel134
            // 
            this.xrLabel134.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel134.LocationFloat = new DevExpress.Utils.PointFloat(313.625F, 0F);
            this.xrLabel134.Name = "xrLabel134";
            this.xrLabel134.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel134.SizeF = new System.Drawing.SizeF(52.8401F, 23F);
            this.xrLabel134.StylePriority.UseBorders = false;
            this.xrLabel134.Text = "尿潜血";
            // 
            // txt尿潜血
            // 
            this.txt尿潜血.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt尿潜血.LocationFloat = new DevExpress.Utils.PointFloat(366.4651F, 0F);
            this.txt尿潜血.Name = "txt尿潜血";
            this.txt尿潜血.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt尿潜血.SizeF = new System.Drawing.SizeF(46.17267F, 23F);
            this.txt尿潜血.StylePriority.UseBorders = false;
            // 
            // xrLabel132
            // 
            this.xrLabel132.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel132.LocationFloat = new DevExpress.Utils.PointFloat(209.6809F, 0F);
            this.xrLabel132.Name = "xrLabel132";
            this.xrLabel132.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel132.SizeF = new System.Drawing.SizeF(52.8401F, 23F);
            this.xrLabel132.StylePriority.UseBorders = false;
            this.xrLabel132.Text = "尿酮体";
            // 
            // txt尿酮体
            // 
            this.txt尿酮体.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt尿酮体.LocationFloat = new DevExpress.Utils.PointFloat(262.5209F, 0F);
            this.txt尿酮体.Name = "txt尿酮体";
            this.txt尿酮体.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt尿酮体.SizeF = new System.Drawing.SizeF(50.47916F, 23F);
            this.txt尿酮体.StylePriority.UseBorders = false;
            // 
            // xrLabel130
            // 
            this.xrLabel130.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel130.LocationFloat = new DevExpress.Utils.PointFloat(112.6808F, 0F);
            this.xrLabel130.Name = "xrLabel130";
            this.xrLabel130.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel130.SizeF = new System.Drawing.SizeF(45.10419F, 23F);
            this.xrLabel130.StylePriority.UseBorders = false;
            this.xrLabel130.Text = "尿糖";
            // 
            // txt尿糖
            // 
            this.txt尿糖.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt尿糖.LocationFloat = new DevExpress.Utils.PointFloat(157.785F, 0F);
            this.txt尿糖.Name = "txt尿糖";
            this.txt尿糖.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt尿糖.SizeF = new System.Drawing.SizeF(51.8959F, 23F);
            this.txt尿糖.StylePriority.UseBorders = false;
            // 
            // txt尿蛋白
            // 
            this.txt尿蛋白.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt尿蛋白.LocationFloat = new DevExpress.Utils.PointFloat(65.00002F, 0F);
            this.txt尿蛋白.Name = "txt尿蛋白";
            this.txt尿蛋白.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt尿蛋白.SizeF = new System.Drawing.SizeF(47.68088F, 23F);
            this.txt尿蛋白.StylePriority.UseBorders = false;
            // 
            // xrLabel128
            // 
            this.xrLabel128.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel128.LocationFloat = new DevExpress.Utils.PointFloat(12.68082F, 0F);
            this.xrLabel128.Name = "xrLabel128";
            this.xrLabel128.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel128.SizeF = new System.Drawing.SizeF(52.31917F, 23F);
            this.xrLabel128.StylePriority.UseBorders = false;
            this.xrLabel128.Text = "尿蛋白";
            // 
            // xrTable25
            // 
            this.xrTable25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable25.LocationFloat = new DevExpress.Utils.PointFloat(100F, 541.9584F);
            this.xrTable25.Name = "xrTable25";
            this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTable25.SizeF = new System.Drawing.SizeF(640F, 48F);
            this.xrTable25.StylePriority.UseBorders = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.xrTableCell53});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseTextAlignment = false;
            this.xrTableCell51.Text = "血常规";
            this.xrTableCell51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell51.Weight = 0.56334444970503494D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel126,
            this.txt血常规其他,
            this.xrLabel123,
            this.txt血小板计数值,
            this.xrLabel125,
            this.xrLabel120,
            this.txt白细胞计数值,
            this.xrLabel122,
            this.xrLabel119,
            this.txt血红蛋白值,
            this.xrLabel117});
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.StylePriority.UseTextAlignment = false;
            this.xrTableCell53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell53.Weight = 2.4411592324770623D;
            // 
            // xrLabel126
            // 
            this.xrLabel126.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel126.LocationFloat = new DevExpress.Utils.PointFloat(347.5428F, 23.00018F);
            this.xrLabel126.Name = "xrLabel126";
            this.xrLabel126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel126.SizeF = new System.Drawing.SizeF(51.16083F, 23.00006F);
            this.xrLabel126.StylePriority.UseBorders = false;
            this.xrLabel126.Text = "其他";
            // 
            // txt血常规其他
            // 
            this.txt血常规其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血常规其他.LocationFloat = new DevExpress.Utils.PointFloat(398.7036F, 23.00006F);
            this.txt血常规其他.Name = "txt血常规其他";
            this.txt血常规其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血常规其他.SizeF = new System.Drawing.SizeF(103.3778F, 20.99994F);
            this.txt血常规其他.StylePriority.UseBorders = false;
            // 
            // xrLabel123
            // 
            this.xrLabel123.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel123.LocationFloat = new DevExpress.Utils.PointFloat(12.86144F, 23.00005F);
            this.xrLabel123.Name = "xrLabel123";
            this.xrLabel123.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel123.SizeF = new System.Drawing.SizeF(100.0002F, 23F);
            this.xrLabel123.StylePriority.UseBorders = false;
            this.xrLabel123.Text = "血小板计数值";
            // 
            // txt血小板计数值
            // 
            this.txt血小板计数值.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血小板计数值.LocationFloat = new DevExpress.Utils.PointFloat(117.8615F, 23.00006F);
            this.txt血小板计数值.Name = "txt血小板计数值";
            this.txt血小板计数值.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血小板计数值.SizeF = new System.Drawing.SizeF(67.50024F, 20.99994F);
            this.txt血小板计数值.StylePriority.UseBorders = false;
            // 
            // xrLabel125
            // 
            this.xrLabel125.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel125.LocationFloat = new DevExpress.Utils.PointFloat(188.1248F, 23.00005F);
            this.xrLabel125.Name = "xrLabel125";
            this.xrLabel125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel125.SizeF = new System.Drawing.SizeF(40.62491F, 23F);
            this.xrLabel125.StylePriority.UseBorders = false;
            this.xrLabel125.Text = "/L";
            // 
            // xrLabel120
            // 
            this.xrLabel120.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(295.4801F, 0.000222524F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(103.2234F, 23F);
            this.xrLabel120.StylePriority.UseBorders = false;
            this.xrLabel120.Text = "白细胞计数值";
            // 
            // txt白细胞计数值
            // 
            this.txt白细胞计数值.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt白细胞计数值.LocationFloat = new DevExpress.Utils.PointFloat(398.7036F, 9.536743E-05F);
            this.txt白细胞计数值.Name = "txt白细胞计数值";
            this.txt白细胞计数值.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt白细胞计数值.SizeF = new System.Drawing.SizeF(67.50024F, 23F);
            this.txt白细胞计数值.StylePriority.UseBorders = false;
            // 
            // xrLabel122
            // 
            this.xrLabel122.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(468.834F, 6.103516E-05F);
            this.xrLabel122.Name = "xrLabel122";
            this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel122.SizeF = new System.Drawing.SizeF(32.56866F, 23F);
            this.xrLabel122.StylePriority.UseBorders = false;
            this.xrLabel122.Text = "/L";
            // 
            // xrLabel119
            // 
            this.xrLabel119.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel119.LocationFloat = new DevExpress.Utils.PointFloat(180.3619F, 3.178914E-05F);
            this.xrLabel119.Name = "xrLabel119";
            this.xrLabel119.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel119.SizeF = new System.Drawing.SizeF(40.62491F, 23F);
            this.xrLabel119.StylePriority.UseBorders = false;
            this.xrLabel119.Text = "g/L";
            // 
            // txt血红蛋白值
            // 
            this.txt血红蛋白值.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血红蛋白值.LocationFloat = new DevExpress.Utils.PointFloat(112.6808F, 3.178914E-05F);
            this.txt血红蛋白值.Name = "txt血红蛋白值";
            this.txt血红蛋白值.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血红蛋白值.SizeF = new System.Drawing.SizeF(67.50024F, 23F);
            this.txt血红蛋白值.StylePriority.UseBorders = false;
            // 
            // xrLabel117
            // 
            this.xrLabel117.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(12.86163F, 3.178914E-05F);
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(89.81911F, 23F);
            this.xrLabel117.StylePriority.UseBorders = false;
            this.xrLabel117.Text = "血红蛋白值";
            // 
            // xrTable22
            // 
            this.xrTable22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable22.LocationFloat = new DevExpress.Utils.PointFloat(100.0001F, 487.9584F);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.xrTable22.SizeF = new System.Drawing.SizeF(640F, 27F);
            this.xrTable22.StylePriority.UseBorders = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell49});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel108,
            this.txt宫颈异常,
            this.txt宫颈});
            this.xrTableCell48.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.StylePriority.UseFont = false;
            this.xrTableCell48.StylePriority.UseTextAlignment = false;
            this.xrTableCell48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell48.Weight = 1.2683560292186111D;
            // 
            // xrLabel108
            // 
            this.xrLabel108.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(9.478887F, 3.178914E-05F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(172.5746F, 23F);
            this.xrLabel108.StylePriority.UseBorders = false;
            this.xrLabel108.StylePriority.UseTextAlignment = false;
            this.xrLabel108.Text = "宫颈: 1 未见异常  2 异常";
            this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt宫颈异常
            // 
            this.txt宫颈异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt宫颈异常.LocationFloat = new DevExpress.Utils.PointFloat(187.4252F, 6.103516E-05F);
            this.txt宫颈异常.Name = "txt宫颈异常";
            this.txt宫颈异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt宫颈异常.SizeF = new System.Drawing.SizeF(57.57458F, 23.00003F);
            this.txt宫颈异常.StylePriority.UseBorders = false;
            // 
            // txt宫颈
            // 
            this.txt宫颈.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt宫颈.CanGrow = false;
            this.txt宫颈.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt宫颈.LocationFloat = new DevExpress.Utils.PointFloat(244.9998F, 5.000051F);
            this.txt宫颈.Name = "txt宫颈";
            this.txt宫颈.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt宫颈.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt宫颈.StylePriority.UseBorders = false;
            this.txt宫颈.StylePriority.UseFont = false;
            this.txt宫颈.StylePriority.UseTextAlignment = false;
            this.txt宫颈.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel111,
            this.txt子宫异常,
            this.txt子宫});
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.StylePriority.UseTextAlignment = false;
            this.xrTableCell49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell49.Weight = 1.7381174940875619D;
            // 
            // xrLabel111
            // 
            this.xrLabel111.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(38.12475F, 0F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel111.SizeF = new System.Drawing.SizeF(174.6448F, 23F);
            this.xrLabel111.StylePriority.UseBorders = false;
            this.xrLabel111.StylePriority.UseTextAlignment = false;
            this.xrLabel111.Text = "子宫: 1 未见异常  2 异常";
            this.xrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt子宫异常
            // 
            this.txt子宫异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt子宫异常.LocationFloat = new DevExpress.Utils.PointFloat(213.505F, 6.357829E-05F);
            this.txt子宫异常.Name = "txt子宫异常";
            this.txt子宫异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt子宫异常.SizeF = new System.Drawing.SizeF(73.95822F, 23F);
            this.txt子宫异常.StylePriority.UseBorders = false;
            // 
            // txt子宫
            // 
            this.txt子宫.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt子宫.CanGrow = false;
            this.txt子宫.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt子宫.LocationFloat = new DevExpress.Utils.PointFloat(337.0836F, 5.000051F);
            this.txt子宫.Name = "txt子宫";
            this.txt子宫.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt子宫.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt子宫.StylePriority.UseBorders = false;
            this.txt子宫.StylePriority.UseFont = false;
            this.txt子宫.StylePriority.UseTextAlignment = false;
            this.txt子宫.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable21
            // 
            this.xrTable21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable21.LocationFloat = new DevExpress.Utils.PointFloat(100.0001F, 460.9584F);
            this.xrTable21.Name = "xrTable21";
            this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable21.SizeF = new System.Drawing.SizeF(640F, 27F);
            this.xrTable21.StylePriority.UseBorders = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell47});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel102,
            this.txt外阴异常,
            this.txt外阴});
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseTextAlignment = false;
            this.xrTableCell45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell45.Weight = 1.2695162238184357D;
            // 
            // xrLabel102
            // 
            this.xrLabel102.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(9.478903F, 0F);
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(174.9999F, 23F);
            this.xrLabel102.StylePriority.UseBorders = false;
            this.xrLabel102.StylePriority.UseTextAlignment = false;
            this.xrLabel102.Text = "外阴: 1 未见异常  2 异常";
            this.xrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt外阴异常
            // 
            this.txt外阴异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt外阴异常.LocationFloat = new DevExpress.Utils.PointFloat(185.5209F, 0F);
            this.txt外阴异常.Name = "txt外阴异常";
            this.txt外阴异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt外阴异常.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.txt外阴异常.StylePriority.UseBorders = false;
            // 
            // txt外阴
            // 
            this.txt外阴.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt外阴.CanGrow = false;
            this.txt外阴.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt外阴.LocationFloat = new DevExpress.Utils.PointFloat(245.7263F, 4.999987F);
            this.txt外阴.Name = "txt外阴";
            this.txt外阴.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt外阴.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt外阴.StylePriority.UseBorders = false;
            this.txt外阴.StylePriority.UseFont = false;
            this.txt外阴.StylePriority.UseTextAlignment = false;
            this.txt外阴.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel105,
            this.txt阴道异常,
            this.txt阴道});
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell47.Weight = 1.7397074883395152D;
            // 
            // xrLabel105
            // 
            this.xrLabel105.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(37.76967F, 0F);
            this.xrLabel105.Name = "xrLabel105";
            this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel105.SizeF = new System.Drawing.SizeF(174.9999F, 23F);
            this.xrLabel105.StylePriority.UseBorders = false;
            this.xrLabel105.StylePriority.UseTextAlignment = false;
            this.xrLabel105.Text = "阴道: 1 未见异常  2 异常";
            this.xrLabel105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt阴道异常
            // 
            this.txt阴道异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt阴道异常.LocationFloat = new DevExpress.Utils.PointFloat(212.7698F, 0F);
            this.txt阴道异常.Name = "txt阴道异常";
            this.txt阴道异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阴道异常.SizeF = new System.Drawing.SizeF(73.95822F, 23F);
            this.txt阴道异常.StylePriority.UseBorders = false;
            // 
            // txt阴道
            // 
            this.txt阴道.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt阴道.CanGrow = false;
            this.txt阴道.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt阴道.LocationFloat = new DevExpress.Utils.PointFloat(337.0836F, 4.999987F);
            this.txt阴道.Name = "txt阴道";
            this.txt阴道.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt阴道.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt阴道.StylePriority.UseBorders = false;
            this.txt阴道.StylePriority.UseFont = false;
            this.txt阴道.StylePriority.UseTextAlignment = false;
            this.txt阴道.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable20
            // 
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.LocationFloat = new DevExpress.Utils.PointFloat(0F, 460.9584F);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable20.SizeF = new System.Drawing.SizeF(100F, 80.99994F);
            this.xrTable20.StylePriority.UseBorders = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell46});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.StylePriority.UseTextAlignment = false;
            this.xrTableCell46.Text = "妇科检查";
            this.xrTableCell46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell46.Weight = 2.7272727272727271D;
            // 
            // xrTable19
            // 
            this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 433.9584F);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable19.SizeF = new System.Drawing.SizeF(740F, 27F);
            this.xrTable19.StylePriority.UseBorders = false;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.StylePriority.UseTextAlignment = false;
            this.xrTableCell42.Text = "听诊";
            this.xrTableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell42.Weight = 0.40538943501690916D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt心脏,
            this.txt心脏异常,
            this.xrLabel96});
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.StylePriority.UseTextAlignment = false;
            this.xrTableCell43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell43.Weight = 1.0945514063481621D;
            // 
            // txt心脏
            // 
            this.txt心脏.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt心脏.CanGrow = false;
            this.txt心脏.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt心脏.LocationFloat = new DevExpress.Utils.PointFloat(244.9999F, 4.625066F);
            this.txt心脏.Name = "txt心脏";
            this.txt心脏.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt心脏.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt心脏.StylePriority.UseBorders = false;
            this.txt心脏.StylePriority.UseFont = false;
            this.txt心脏.StylePriority.UseTextAlignment = false;
            this.txt心脏.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt心脏异常
            // 
            this.txt心脏异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt心脏异常.LocationFloat = new DevExpress.Utils.PointFloat(184.9999F, 6.357829E-05F);
            this.txt心脏异常.Name = "txt心脏异常";
            this.txt心脏异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt心脏异常.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.txt心脏异常.StylePriority.UseBorders = false;
            // 
            // xrLabel96
            // 
            this.xrLabel96.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(174.9999F, 23F);
            this.xrLabel96.StylePriority.UseBorders = false;
            this.xrLabel96.StylePriority.UseTextAlignment = false;
            this.xrLabel96.Text = "心脏: 1 未见异常  2 异常";
            this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel99,
            this.txt肺部异常,
            this.txt肺部});
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.StylePriority.UseTextAlignment = false;
            this.xrTableCell44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell44.Weight = 1.4999409425123866D;
            // 
            // xrLabel99
            // 
            this.xrLabel99.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel99.LocationFloat = new DevExpress.Utils.PointFloat(37.76978F, 0F);
            this.xrLabel99.Name = "xrLabel99";
            this.xrLabel99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel99.SizeF = new System.Drawing.SizeF(174.9999F, 23F);
            this.xrLabel99.StylePriority.UseBorders = false;
            this.xrLabel99.StylePriority.UseTextAlignment = false;
            this.xrLabel99.Text = "肺部: 1 未见异常  2 异常";
            this.xrLabel99.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt肺部异常
            // 
            this.txt肺部异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt肺部异常.LocationFloat = new DevExpress.Utils.PointFloat(212.7697F, 0F);
            this.txt肺部异常.Name = "txt肺部异常";
            this.txt肺部异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt肺部异常.SizeF = new System.Drawing.SizeF(72.4137F, 23F);
            this.txt肺部异常.StylePriority.UseBorders = false;
            // 
            // txt肺部
            // 
            this.txt肺部.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt肺部.CanGrow = false;
            this.txt肺部.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt肺部.LocationFloat = new DevExpress.Utils.PointFloat(337.8109F, 4.625066F);
            this.txt肺部.Name = "txt肺部";
            this.txt肺部.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt肺部.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt肺部.StylePriority.UseBorders = false;
            this.txt肺部.StylePriority.UseFont = false;
            this.txt肺部.StylePriority.UseTextAlignment = false;
            this.txt肺部.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable17
            // 
            this.xrTable17.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(0F, 406.9584F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable17.SizeF = new System.Drawing.SizeF(370F, 27F);
            this.xrTable17.StylePriority.UseBorders = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell39});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseTextAlignment = false;
            this.xrTableCell38.Text = "体质指数";
            this.xrTableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell38.Weight = 0.82324531077775465D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt体质指数});
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.StylePriority.UseTextAlignment = false;
            this.xrTableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell39.Weight = 2.2227622500163626D;
            // 
            // txt体质指数
            // 
            this.txt体质指数.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt体质指数.LocationFloat = new DevExpress.Utils.PointFloat(9.478981F, 0F);
            this.txt体质指数.Name = "txt体质指数";
            this.txt体质指数.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt体质指数.SizeF = new System.Drawing.SizeF(100.521F, 23F);
            this.txt体质指数.StylePriority.UseBorders = false;
            this.txt体质指数.StylePriority.UseTextAlignment = false;
            this.txt体质指数.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(370F, 379.9584F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable16.SizeF = new System.Drawing.SizeF(370F, 27F);
            this.xrTable16.StylePriority.UseBorders = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.xrTableCell37});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "体重";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell36.Weight = 0.87414949126128916D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt体重,
            this.xrLabel91});
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.StylePriority.UseTextAlignment = false;
            this.xrTableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell37.Weight = 2.0944896605517256D;
            // 
            // txt体重
            // 
            this.txt体重.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt体重.LocationFloat = new DevExpress.Utils.PointFloat(15.0959F, 0F);
            this.txt体重.Name = "txt体重";
            this.txt体重.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt体重.SizeF = new System.Drawing.SizeF(100F, 22.99994F);
            this.txt体重.StylePriority.UseBorders = false;
            this.txt体重.StylePriority.UseTextAlignment = false;
            this.txt体重.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel91
            // 
            this.xrLabel91.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel91.LocationFloat = new DevExpress.Utils.PointFloat(117.3374F, 0F);
            this.xrLabel91.Name = "xrLabel91";
            this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel91.SizeF = new System.Drawing.SizeF(50.00012F, 22.99994F);
            this.xrLabel91.StylePriority.UseBorders = false;
            this.xrLabel91.StylePriority.UseTextAlignment = false;
            this.xrLabel91.Text = "Kg";
            this.xrLabel91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable15
            // 
            this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 379.9584F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable15.SizeF = new System.Drawing.SizeF(370F, 27F);
            this.xrTable15.StylePriority.UseBorders = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell35});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.Text = "身高";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell34.Weight = 1.0810810810810811D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel89,
            this.txt身高});
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Weight = 2.9189189189189184D;
            // 
            // xrLabel89
            // 
            this.xrLabel89.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel89.LocationFloat = new DevExpress.Utils.PointFloat(112.4253F, 0F);
            this.xrLabel89.Name = "xrLabel89";
            this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel89.SizeF = new System.Drawing.SizeF(50.00012F, 22.99994F);
            this.xrLabel89.StylePriority.UseBorders = false;
            this.xrLabel89.StylePriority.UseTextAlignment = false;
            this.xrLabel89.Text = "cm";
            this.xrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt身高
            // 
            this.txt身高.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt身高.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.txt身高.Name = "txt身高";
            this.txt身高.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt身高.SizeF = new System.Drawing.SizeF(100F, 22.99994F);
            this.txt身高.StylePriority.UseBorders = false;
            this.txt身高.StylePriority.UseTextAlignment = false;
            this.txt身高.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable14
            // 
            this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 352.9584F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable14.SizeF = new System.Drawing.SizeF(740F, 27F);
            this.xrTable14.StylePriority.UseBorders = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "孕产史";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell32.Weight = 0.41945933603345187D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel86,
            this.txt出生缺陷儿,
            this.xrLabel84,
            this.txt新生儿死亡,
            this.xrLabel82,
            this.txt死产,
            this.xrLabel80,
            this.txt死胎,
            this.txt流产,
            this.xrLabel78});
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.StylePriority.UseTextAlignment = false;
            this.xrTableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell33.Weight = 2.6845396789405864D;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(472.3827F, 3.178914E-05F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(98.15485F, 23F);
            this.xrLabel86.StylePriority.UseBorders = false;
            this.xrLabel86.Text = "5 出生缺陷儿";
            // 
            // txt出生缺陷儿
            // 
            this.txt出生缺陷儿.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt出生缺陷儿.LocationFloat = new DevExpress.Utils.PointFloat(570.5376F, 0.3333092F);
            this.txt出生缺陷儿.Name = "txt出生缺陷儿";
            this.txt出生缺陷儿.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt出生缺陷儿.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.txt出生缺陷儿.StylePriority.UseBorders = false;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(326.0415F, 0F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(100.8334F, 23F);
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.Text = "4 新生儿死亡";
            // 
            // txt新生儿死亡
            // 
            this.txt新生儿死亡.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt新生儿死亡.LocationFloat = new DevExpress.Utils.PointFloat(427.3827F, 3.178914E-05F);
            this.txt新生儿死亡.Name = "txt新生儿死亡";
            this.txt新生儿死亡.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt新生儿死亡.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.txt新生儿死亡.StylePriority.UseBorders = false;
            // 
            // xrLabel82
            // 
            this.xrLabel82.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(220.6249F, 0F);
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel82.SizeF = new System.Drawing.SizeF(60.4166F, 23F);
            this.xrLabel82.StylePriority.UseBorders = false;
            this.xrLabel82.Text = "3 死产";
            // 
            // txt死产
            // 
            this.txt死产.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt死产.LocationFloat = new DevExpress.Utils.PointFloat(281.0415F, 0F);
            this.txt死产.Name = "txt死产";
            this.txt死产.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt死产.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.txt死产.StylePriority.UseBorders = false;
            // 
            // xrLabel80
            // 
            this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(117.2916F, 0F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(58.33333F, 23F);
            this.xrLabel80.StylePriority.UseBorders = false;
            this.xrLabel80.Text = "2 死胎";
            // 
            // txt死胎
            // 
            this.txt死胎.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt死胎.LocationFloat = new DevExpress.Utils.PointFloat(175.6249F, 0F);
            this.txt死胎.Name = "txt死胎";
            this.txt死胎.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt死胎.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.txt死胎.StylePriority.UseBorders = false;
            // 
            // txt流产
            // 
            this.txt流产.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt流产.LocationFloat = new DevExpress.Utils.PointFloat(69.37501F, 3.178914E-05F);
            this.txt流产.Name = "txt流产";
            this.txt流产.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt流产.SizeF = new System.Drawing.SizeF(45F, 23F);
            this.txt流产.StylePriority.UseBorders = false;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0.3332825F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(59.375F, 23F);
            this.xrLabel78.StylePriority.UseBorders = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "1 流产";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 325.9584F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable13.SizeF = new System.Drawing.SizeF(740.0002F, 27F);
            this.xrTable13.StylePriority.UseBorders = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseTextAlignment = false;
            this.xrTableCell29.Text = "妇科手术史";
            this.xrTableCell29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell29.Weight = 0.41945960281065059D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt妇科手术史有,
            this.xrLabel64});
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.StylePriority.UseTextAlignment = false;
            this.xrTableCell30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell30.Weight = 1.8456222220254839D;
            // 
            // txt妇科手术史有
            // 
            this.txt妇科手术史有.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt妇科手术史有.LocationFloat = new DevExpress.Utils.PointFloat(106.8751F, 1.375F);
            this.txt妇科手术史有.Name = "txt妇科手术史有";
            this.txt妇科手术史有.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt妇科手术史有.SizeF = new System.Drawing.SizeF(263.1248F, 23F);
            this.txt妇科手术史有.StylePriority.UseBorders = false;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(9.999999F, 1.375F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(95F, 23F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "1 有 2 无";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt妇科手术史});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseTextAlignment = false;
            this.xrTableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell31.Weight = 0.838920201188512D;
            // 
            // txt妇科手术史
            // 
            this.txt妇科手术史.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt妇科手术史.CanGrow = false;
            this.txt妇科手术史.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt妇科手术史.LocationFloat = new DevExpress.Utils.PointFloat(167.0836F, 6.374995F);
            this.txt妇科手术史.Name = "txt妇科手术史";
            this.txt妇科手术史.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt妇科手术史.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt妇科手术史.StylePriority.UseBorders = false;
            this.txt妇科手术史.StylePriority.UseFont = false;
            this.txt妇科手术史.StylePriority.UseTextAlignment = false;
            this.txt妇科手术史.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(9.536743E-05F, 275.9584F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable12.SizeF = new System.Drawing.SizeF(740F, 50F);
            this.xrTable12.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.xrTableCell27,
            this.xrTableCell28});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "个人史";
            this.xrTableCell26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell26.Weight = 0.4200002695724781D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt个人史其他,
            this.xrLabel62,
            this.xrLabel61});
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell27.Weight = 1.8480012108139865D;
            // 
            // txt个人史其他
            // 
            this.txt个人史其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt个人史其他.LocationFloat = new DevExpress.Utils.PointFloat(162.4253F, 23.00002F);
            this.txt个人史其他.Name = "txt个人史其他";
            this.txt个人史其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt个人史其他.SizeF = new System.Drawing.SizeF(184.7222F, 23F);
            this.txt个人史其他.StylePriority.UseBorders = false;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(10.00004F, 23.00003F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(152.4253F, 23F);
            this.xrLabel62.StylePriority.UseBorders = false;
            this.xrLabel62.StylePriority.UseTextAlignment = false;
            this.xrLabel62.Text = "5 接触放射线   6 其他";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel61
            // 
            this.xrLabel61.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(10.00007F, 0F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(405F, 23.00003F);
            this.xrLabel61.StylePriority.UseBorders = false;
            this.xrLabel61.StylePriority.UseTextAlignment = false;
            this.xrLabel61.Text = "1 无  2 吸烟   3 饮酒   4 服用药物  5 接触有毒有害物质 ";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel65,
            this.xrLabel66,
            this.xrLabel67,
            this.xrLabel68,
            this.txt个人史1,
            this.txt个人史2,
            this.txt个人史3,
            this.txt个人史4,
            this.txt个人史5});
            this.xrTableCell28.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.StylePriority.UseFont = false;
            this.xrTableCell28.StylePriority.UseTextAlignment = false;
            this.xrTableCell28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell28.Weight = 0.840000463949315D;
            // 
            // xrLabel65
            // 
            this.xrLabel65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel65.CanGrow = false;
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(154.1995F, 10.0002F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel65.StylePriority.UseBorders = false;
            this.xrLabel65.Text = "/";
            // 
            // xrLabel66
            // 
            this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel66.CanGrow = false;
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(124.9098F, 10.0002F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel66.StylePriority.UseBorders = false;
            this.xrLabel66.Text = "/";
            // 
            // xrLabel67
            // 
            this.xrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel67.CanGrow = false;
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(95.61983F, 10.0002F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel67.StylePriority.UseBorders = false;
            this.xrLabel67.Text = "/";
            // 
            // xrLabel68
            // 
            this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel68.CanGrow = false;
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(66.33009F, 10.00001F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel68.StylePriority.UseBorders = false;
            this.xrLabel68.Text = "/";
            // 
            // txt个人史1
            // 
            this.txt个人史1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人史1.CanGrow = false;
            this.txt个人史1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt个人史1.LocationFloat = new DevExpress.Utils.PointFloat(47.8934F, 10.00001F);
            this.txt个人史1.Name = "txt个人史1";
            this.txt个人史1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt个人史1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人史1.StylePriority.UseBorders = false;
            this.txt个人史1.StylePriority.UseFont = false;
            this.txt个人史1.StylePriority.UseTextAlignment = false;
            this.txt个人史1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt个人史2
            // 
            this.txt个人史2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人史2.CanGrow = false;
            this.txt个人史2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt个人史2.LocationFloat = new DevExpress.Utils.PointFloat(77.18334F, 10.00001F);
            this.txt个人史2.Name = "txt个人史2";
            this.txt个人史2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt个人史2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人史2.StylePriority.UseBorders = false;
            this.txt个人史2.StylePriority.UseFont = false;
            this.txt个人史2.StylePriority.UseTextAlignment = false;
            this.txt个人史2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt个人史3
            // 
            this.txt个人史3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人史3.CanGrow = false;
            this.txt个人史3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt个人史3.LocationFloat = new DevExpress.Utils.PointFloat(106.473F, 10.00001F);
            this.txt个人史3.Name = "txt个人史3";
            this.txt个人史3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt个人史3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人史3.StylePriority.UseBorders = false;
            this.txt个人史3.StylePriority.UseFont = false;
            this.txt个人史3.StylePriority.UseTextAlignment = false;
            this.txt个人史3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt个人史4
            // 
            this.txt个人史4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人史4.CanGrow = false;
            this.txt个人史4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt个人史4.LocationFloat = new DevExpress.Utils.PointFloat(135.763F, 10.00001F);
            this.txt个人史4.Name = "txt个人史4";
            this.txt个人史4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt个人史4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人史4.StylePriority.UseBorders = false;
            this.txt个人史4.StylePriority.UseFont = false;
            this.txt个人史4.StylePriority.UseTextAlignment = false;
            this.txt个人史4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt个人史5
            // 
            this.txt个人史5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt个人史5.CanGrow = false;
            this.txt个人史5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt个人史5.LocationFloat = new DevExpress.Utils.PointFloat(166.5567F, 10.00023F);
            this.txt个人史5.Name = "txt个人史5";
            this.txt个人史5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt个人史5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt个人史5.StylePriority.UseBorders = false;
            this.txt个人史5.StylePriority.UseFont = false;
            this.txt个人史5.StylePriority.UseTextAlignment = false;
            this.txt个人史5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 248.9584F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable11.SizeF = new System.Drawing.SizeF(740F, 27F);
            this.xrTable11.StylePriority.UseBorders = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24,
            this.xrTableCell25});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.StylePriority.UseTextAlignment = false;
            this.xrTableCell23.Text = "家族史";
            this.xrTableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell23.Weight = 0.41878451548844037D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt家族史其他,
            this.xrLabel54});
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseTextAlignment = false;
            this.xrTableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell24.Weight = 1.8426518188017855D;
            // 
            // txt家族史其他
            // 
            this.txt家族史其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt家族史其他.LocationFloat = new DevExpress.Utils.PointFloat(329.681F, 0F);
            this.txt家族史其他.Name = "txt家族史其他";
            this.txt家族史其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt家族史其他.SizeF = new System.Drawing.SizeF(106.9412F, 23.00003F);
            this.txt家族史其他.StylePriority.UseBorders = false;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 0.3333282F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(319.681F, 23.00002F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "1 无  2 遗传性疾病史  3 精神疾病史  4 其他";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt家族史3,
            this.xrLabel57,
            this.txt家族史2,
            this.xrLabel59,
            this.txt家族史1});
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.StylePriority.UseTextAlignment = false;
            this.xrTableCell25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell25.Weight = 0.837569027958598D;
            // 
            // txt家族史3
            // 
            this.txt家族史3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史3.CanGrow = false;
            this.txt家族史3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史3.LocationFloat = new DevExpress.Utils.PointFloat(172.0001F, 4.208406F);
            this.txt家族史3.Name = "txt家族史3";
            this.txt家族史3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史3.StylePriority.UseBorders = false;
            this.txt家族史3.StylePriority.UseFont = false;
            this.txt家族史3.StylePriority.UseTextAlignment = false;
            this.txt家族史3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel57.CanGrow = false;
            this.xrLabel57.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(156.6669F, 4.208406F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel57.StylePriority.UseBorders = false;
            this.xrLabel57.StylePriority.UseFont = false;
            this.xrLabel57.Text = "/";
            // 
            // txt家族史2
            // 
            this.txt家族史2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史2.CanGrow = false;
            this.txt家族史2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史2.LocationFloat = new DevExpress.Utils.PointFloat(137.5185F, 4.208406F);
            this.txt家族史2.Name = "txt家族史2";
            this.txt家族史2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史2.StylePriority.UseBorders = false;
            this.txt家族史2.StylePriority.UseFont = false;
            this.txt家族史2.StylePriority.UseTextAlignment = false;
            this.txt家族史2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel59.CanGrow = false;
            this.xrLabel59.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(126.1639F, 4.208406F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel59.StylePriority.UseBorders = false;
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.Text = "/";
            // 
            // txt家族史1
            // 
            this.txt家族史1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史1.CanGrow = false;
            this.txt家族史1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史1.LocationFloat = new DevExpress.Utils.PointFloat(106.2793F, 4.208406F);
            this.txt家族史1.Name = "txt家族史1";
            this.txt家族史1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史1.StylePriority.UseBorders = false;
            this.txt家族史1.StylePriority.UseFont = false;
            this.txt家族史1.StylePriority.UseTextAlignment = false;
            this.txt家族史1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 203.9584F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(740F, 45F);
            this.xrTable10.StylePriority.UseBorders = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.xrTableCell22});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "既往史";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell21.Weight = 1.0500004114228791D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt既往史其他,
            this.xrLabel51,
            this.xrLabel50,
            this.xrLabel49,
            this.xrLabel48,
            this.xrLabel47,
            this.xrLabel46,
            this.txt既往史1,
            this.txt既往史2,
            this.txt既往史3,
            this.txt既往史4,
            this.txt既往史5,
            this.txt既往史6,
            this.txt既往史7,
            this.xrLabel37});
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseTextAlignment = false;
            this.xrTableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell22.Weight = 6.7200026403349327D;
            // 
            // txt既往史其他
            // 
            this.txt既往史其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt既往史其他.LocationFloat = new DevExpress.Utils.PointFloat(537.0278F, 0F);
            this.txt既往史其他.Name = "txt既往史其他";
            this.txt既往史其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt既往史其他.SizeF = new System.Drawing.SizeF(85.78302F, 21.87494F);
            this.txt既往史其他.StylePriority.UseBorders = false;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.CanGrow = false;
            this.xrLabel51.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(598.2514F, 21.87494F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.Text = "/";
            // 
            // xrLabel50
            // 
            this.xrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel50.CanGrow = false;
            this.xrLabel50.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(569.5648F, 23.00002F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel50.StylePriority.UseBorders = false;
            this.xrLabel50.StylePriority.UseFont = false;
            this.xrLabel50.Text = "/";
            // 
            // xrLabel49
            // 
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.CanGrow = false;
            this.xrLabel49.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(540.8783F, 23.00002F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.Text = "/";
            // 
            // xrLabel48
            // 
            this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel48.CanGrow = false;
            this.xrLabel48.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(512.1917F, 23.00002F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel48.StylePriority.UseBorders = false;
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.Text = "/";
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.CanGrow = false;
            this.xrLabel47.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(483.505F, 22.99996F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.Text = "/";
            // 
            // xrLabel46
            // 
            this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel46.CanGrow = false;
            this.xrLabel46.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(454.8184F, 22.99996F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(10.41666F, 15.00003F);
            this.xrLabel46.StylePriority.UseBorders = false;
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.Text = "/";
            // 
            // txt既往史1
            // 
            this.txt既往史1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt既往史1.CanGrow = false;
            this.txt既往史1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt既往史1.LocationFloat = new DevExpress.Utils.PointFloat(436.6834F, 23.00015F);
            this.txt既往史1.Name = "txt既往史1";
            this.txt既往史1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt既往史1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt既往史1.StylePriority.UseBorders = false;
            this.txt既往史1.StylePriority.UseFont = false;
            this.txt既往史1.StylePriority.UseTextAlignment = false;
            this.txt既往史1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt既往史2
            // 
            this.txt既往史2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt既往史2.CanGrow = false;
            this.txt既往史2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt既往史2.LocationFloat = new DevExpress.Utils.PointFloat(465.3701F, 22.99996F);
            this.txt既往史2.Name = "txt既往史2";
            this.txt既往史2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt既往史2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt既往史2.StylePriority.UseBorders = false;
            this.txt既往史2.StylePriority.UseFont = false;
            this.txt既往史2.StylePriority.UseTextAlignment = false;
            this.txt既往史2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt既往史3
            // 
            this.txt既往史3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt既往史3.CanGrow = false;
            this.txt既往史3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt既往史3.LocationFloat = new DevExpress.Utils.PointFloat(494.0567F, 22.99996F);
            this.txt既往史3.Name = "txt既往史3";
            this.txt既往史3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt既往史3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt既往史3.StylePriority.UseBorders = false;
            this.txt既往史3.StylePriority.UseFont = false;
            this.txt既往史3.StylePriority.UseTextAlignment = false;
            this.txt既往史3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt既往史4
            // 
            this.txt既往史4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt既往史4.CanGrow = false;
            this.txt既往史4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt既往史4.LocationFloat = new DevExpress.Utils.PointFloat(522.7433F, 22.99996F);
            this.txt既往史4.Name = "txt既往史4";
            this.txt既往史4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt既往史4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt既往史4.StylePriority.UseBorders = false;
            this.txt既往史4.StylePriority.UseFont = false;
            this.txt既往史4.StylePriority.UseTextAlignment = false;
            this.txt既往史4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt既往史5
            // 
            this.txt既往史5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt既往史5.CanGrow = false;
            this.txt既往史5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt既往史5.LocationFloat = new DevExpress.Utils.PointFloat(551.4299F, 22.99996F);
            this.txt既往史5.Name = "txt既往史5";
            this.txt既往史5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt既往史5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt既往史5.StylePriority.UseBorders = false;
            this.txt既往史5.StylePriority.UseFont = false;
            this.txt既往史5.StylePriority.UseTextAlignment = false;
            this.txt既往史5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt既往史6
            // 
            this.txt既往史6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt既往史6.CanGrow = false;
            this.txt既往史6.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt既往史6.LocationFloat = new DevExpress.Utils.PointFloat(580.1164F, 22.99996F);
            this.txt既往史6.Name = "txt既往史6";
            this.txt既往史6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt既往史6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt既往史6.StylePriority.UseBorders = false;
            this.txt既往史6.StylePriority.UseFont = false;
            this.txt既往史6.StylePriority.UseTextAlignment = false;
            this.txt既往史6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt既往史7
            // 
            this.txt既往史7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt既往史7.CanGrow = false;
            this.txt既往史7.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt既往史7.LocationFloat = new DevExpress.Utils.PointFloat(608.803F, 22.99996F);
            this.txt既往史7.Name = "txt既往史7";
            this.txt既往史7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt既往史7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt既往史7.StylePriority.UseBorders = false;
            this.txt既往史7.StylePriority.UseFont = false;
            this.txt既往史7.StylePriority.UseTextAlignment = false;
            this.txt既往史7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(526.4374F, 23F);
            this.xrLabel37.StylePriority.UseBorders = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "1 无  2 心脏病  3 肾脏疾病  4 肝脏疾病  5 高血压  6 贫血  7 糖尿病  8 其他  ";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(370F, 176.9583F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(370F, 27F);
            this.xrTable9.StylePriority.UseBorders = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.xrTableCell20});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseTextAlignment = false;
            this.xrTableCell19.Text = "预产期";
            this.xrTableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell19.Weight = 1.0838878484521828D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt预产期});
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseTextAlignment = false;
            this.xrTableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell20.Weight = 2.4073364241873634D;
            // 
            // txt预产期
            // 
            this.txt预产期.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt预产期.LocationFloat = new DevExpress.Utils.PointFloat(1.567014F, 6.357829E-05F);
            this.txt预产期.Name = "txt预产期";
            this.txt预产期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt预产期.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.txt预产期.StylePriority.UseBorders = false;
            this.txt预产期.StylePriority.UseTextAlignment = false;
            this.txt预产期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 176.9583F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(369.9998F, 27F);
            this.xrTable8.StylePriority.UseBorders = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell17,
            this.xrTableCell18});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.StylePriority.UseTextAlignment = false;
            this.xrTableCell17.Text = "末次月经";
            this.xrTableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell17.Weight = 0.8096608518564854D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel30,
            this.txt末次月经时间});
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell18.Weight = 2.1860827617251255D;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(162.4255F, 3.178914E-05F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(59.16672F, 23F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.Text = "或不详";
            // 
            // txt末次月经时间
            // 
            this.txt末次月经时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt末次月经时间.LocationFloat = new DevExpress.Utils.PointFloat(12.42545F, 3.178914E-05F);
            this.txt末次月经时间.Name = "txt末次月经时间";
            this.txt末次月经时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt末次月经时间.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.txt末次月经时间.StylePriority.UseBorders = false;
            this.txt末次月经时间.StylePriority.UseTextAlignment = false;
            this.txt末次月经时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(245.342F, 150.9583F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(494.5F, 26F);
            this.xrTable7.StylePriority.UseBorders = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell16});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "产次";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell15.Weight = 0.92299252401867393D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel20,
            this.txt分娩次数,
            this.xrLabel18,
            this.xrLabel21,
            this.txt剖宫产次数,
            this.xrLabel23});
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.StylePriority.UseTextAlignment = false;
            this.xrTableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell16.Weight = 2.7383885680946873D;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(136.2776F, 0F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(31.87488F, 23F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.Text = "次";
            // 
            // txt分娩次数
            // 
            this.txt分娩次数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt分娩次数.LocationFloat = new DevExpress.Utils.PointFloat(81.06931F, 0F);
            this.txt分娩次数.Name = "txt分娩次数";
            this.txt分娩次数.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt分娩次数.SizeF = new System.Drawing.SizeF(55.20831F, 23F);
            this.txt分娩次数.StylePriority.UseBorders = false;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(10.83349F, 0F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(70.23581F, 23F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.Text = "阴道分娩";
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(191.3556F, 0F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(64.35052F, 23F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.Text = "剖宫产";
            // 
            // txt剖宫产次数
            // 
            this.txt剖宫产次数.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt剖宫产次数.LocationFloat = new DevExpress.Utils.PointFloat(257.0157F, 0F);
            this.txt剖宫产次数.Name = "txt剖宫产次数";
            this.txt剖宫产次数.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt剖宫产次数.SizeF = new System.Drawing.SizeF(58.20154F, 23F);
            this.txt剖宫产次数.StylePriority.UseBorders = false;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(316.0434F, 3.178914E-05F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(28.15247F, 23F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.Text = "次";
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 150.9583F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(245.342F, 26F);
            this.xrTable6.StylePriority.UseBorders = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "孕次";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell13.Weight = 0.93061226108574335D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt孕次});
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell14.Weight = 1.3525703885580696D;
            // 
            // txt孕次
            // 
            this.txt孕次.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt孕次.LocationFloat = new DevExpress.Utils.PointFloat(9.478982F, 0F);
            this.txt孕次.Name = "txt孕次";
            this.txt孕次.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt孕次.SizeF = new System.Drawing.SizeF(65.58016F, 23F);
            this.txt孕次.StylePriority.UseBorders = false;
            this.txt孕次.StylePriority.UseTextAlignment = false;
            this.txt孕次.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(370F, 124.9583F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(370F, 26F);
            this.xrTable5.StylePriority.UseBorders = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt丈夫年龄});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell10.Weight = 0.99494438449439937D;
            // 
            // txt丈夫年龄
            // 
            this.txt丈夫年龄.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt丈夫年龄.LocationFloat = new DevExpress.Utils.PointFloat(11.73881F, 6.357829E-05F);
            this.txt丈夫年龄.Name = "txt丈夫年龄";
            this.txt丈夫年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt丈夫年龄.SizeF = new System.Drawing.SizeF(65.58016F, 23F);
            this.txt丈夫年龄.StylePriority.UseBorders = false;
            this.txt丈夫年龄.StylePriority.UseTextAlignment = false;
            this.txt丈夫年龄.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "丈夫电话";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell11.Weight = 0.71219119102565887D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt丈夫电话});
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell12.Weight = 1.9741586095967196D;
            // 
            // txt丈夫电话
            // 
            this.txt丈夫电话.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt丈夫电话.LocationFloat = new DevExpress.Utils.PointFloat(8.145523F, 6.357829E-05F);
            this.txt丈夫电话.Name = "txt丈夫电话";
            this.txt丈夫电话.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt丈夫电话.SizeF = new System.Drawing.SizeF(180F, 23F);
            this.txt丈夫电话.StylePriority.UseBorders = false;
            this.txt丈夫电话.StylePriority.UseTextAlignment = false;
            this.txt丈夫电话.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 124.9583F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(370F, 26F);
            this.xrTable4.StylePriority.UseBorders = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.StylePriority.UseTextAlignment = false;
            this.xrTableCell7.Text = "丈夫姓名";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell7.Weight = 0.89777755235383316D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt丈夫姓名});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.StylePriority.UseTextAlignment = false;
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell8.Weight = 1.3059081958058381D;
            // 
            // txt丈夫姓名
            // 
            this.txt丈夫姓名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt丈夫姓名.LocationFloat = new DevExpress.Utils.PointFloat(9.478967F, 6.357829E-05F);
            this.txt丈夫姓名.Name = "txt丈夫姓名";
            this.txt丈夫姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt丈夫姓名.SizeF = new System.Drawing.SizeF(120.0002F, 23F);
            this.txt丈夫姓名.StylePriority.UseBorders = false;
            this.txt丈夫姓名.StylePriority.UseTextAlignment = false;
            this.txt丈夫姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.StylePriority.UseTextAlignment = false;
            this.xrTableCell9.Text = "丈夫年龄";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell9.Weight = 1.1180911297989349D;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 98.95834F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(740F, 26F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "孕妇年龄";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell5.Weight = 0.42000027415018243D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt孕妇年龄});
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell6.Weight = 2.6880016701855971D;
            // 
            // txt孕妇年龄
            // 
            this.txt孕妇年龄.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt孕妇年龄.LocationFloat = new DevExpress.Utils.PointFloat(9.478965F, 0F);
            this.txt孕妇年龄.Name = "txt孕妇年龄";
            this.txt孕妇年龄.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt孕妇年龄.SizeF = new System.Drawing.SizeF(65.58016F, 23F);
            this.txt孕妇年龄.StylePriority.UseBorders = false;
            this.txt孕妇年龄.StylePriority.UseTextAlignment = false;
            this.txt孕妇年龄.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(370F, 72.95834F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(370F, 26F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell100});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "填表孕周";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell3.Weight = 0.85066493225920037D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt填表孕周});
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.StylePriority.UseTextAlignment = false;
            this.xrTableCell100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell100.Weight = 2.2967954216946116D;
            // 
            // txt填表孕周
            // 
            this.txt填表孕周.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt填表孕周.LocationFloat = new DevExpress.Utils.PointFloat(12.84053F, 0F);
            this.txt填表孕周.Name = "txt填表孕周";
            this.txt填表孕周.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt填表孕周.SizeF = new System.Drawing.SizeF(135.863F, 23F);
            this.txt填表孕周.StylePriority.UseBorders = false;
            this.txt填表孕周.StylePriority.UseTextAlignment = false;
            this.txt填表孕周.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 72.95834F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(370F, 26F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "填表日期";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell1.Weight = 1.1920004908552395D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt填表日期});
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell2.Weight = 3.2184012988167461D;
            // 
            // txt填表日期
            // 
            this.txt填表日期.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt填表日期.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 0F);
            this.txt填表日期.Name = "txt填表日期";
            this.txt填表日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt填表日期.SizeF = new System.Drawing.SizeF(150F, 23F);
            this.txt填表日期.StylePriority.UseBorders = false;
            this.txt填表日期.StylePriority.UseTextAlignment = false;
            this.txt填表日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(578.6578F, 51.91666F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(14.99994F, 15F);
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(697.017F, 51.91666F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(16.3739F, 15F);
            this.xrLabel11.StylePriority.UseBorders = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(673.3453F, 51.91666F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(16.3739F, 15F);
            this.xrLabel10.StylePriority.UseBorders = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(649.6734F, 51.91666F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(16.3739F, 15F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(626.0015F, 51.91666F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(16.3739F, 15F);
            this.xrLabel8.StylePriority.UseBorders = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(554.986F, 51.91666F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(16.3739F, 15F);
            this.xrLabel7.StylePriority.UseBorders = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(602.3297F, 51.91666F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(16.3739F, 15F);
            this.xrLabel6.StylePriority.UseBorders = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(531.314F, 51.91666F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(16.3739F, 15F);
            this.xrLabel5.StylePriority.UseBorders = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(509.5427F, 51.91666F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(16.3739F, 15F);
            this.xrLabel4.StylePriority.UseBorders = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(454.3343F, 48.91666F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(55.20834F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "编号：";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 48.91666F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(80F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "姓名:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(228F, 10.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(297.9166F, 32.375F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "第1次产前随访服务记录表";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable23
            // 
            this.xrTable23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable23.LocationFloat = new DevExpress.Utils.PointFloat(100F, 514.9586F);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable23.SizeF = new System.Drawing.SizeF(640F, 26.8F);
            this.xrTable23.StylePriority.UseBorders = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel114,
            this.txt附件异常,
            this.txt附件});
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseTextAlignment = false;
            this.xrTableCell50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell50.Weight = 3.0047001865315108D;
            // 
            // xrLabel114
            // 
            this.xrLabel114.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel114.LocationFloat = new DevExpress.Utils.PointFloat(9.478998F, 0F);
            this.xrLabel114.Name = "xrLabel114";
            this.xrLabel114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel114.SizeF = new System.Drawing.SizeF(174.9999F, 23F);
            this.xrLabel114.StylePriority.UseBorders = false;
            this.xrLabel114.StylePriority.UseTextAlignment = false;
            this.xrLabel114.Text = "附件: 1 未见异常  2 异常";
            this.xrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt附件异常
            // 
            this.txt附件异常.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt附件异常.LocationFloat = new DevExpress.Utils.PointFloat(186.5624F, 0F);
            this.txt附件异常.Name = "txt附件异常";
            this.txt附件异常.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt附件异常.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.txt附件异常.StylePriority.UseBorders = false;
            // 
            // txt附件
            // 
            this.txt附件.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt附件.CanGrow = false;
            this.txt附件.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt附件.LocationFloat = new DevExpress.Utils.PointFloat(607.8109F, 4.999987F);
            this.txt附件.Name = "txt附件";
            this.txt附件.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt附件.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt附件.StylePriority.UseBorders = false;
            this.txt附件.StylePriority.UseFont = false;
            this.txt附件.StylePriority.UseTextAlignment = false;
            this.txt附件.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable36
            // 
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.LocationFloat = new DevExpress.Utils.PointFloat(99.99991F, 908.125F);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable36.SizeF = new System.Drawing.SizeF(640F, 27F);
            this.xrTable36.StylePriority.UseBorders = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell72,
            this.xrTableCell73});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.StylePriority.UseTextAlignment = false;
            this.xrTableCell72.Text = "HIV抗体检测*";
            this.xrTableCell72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell72.Weight = 0.56250001039646957D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel184,
            this.txtHIV抗体检测});
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseTextAlignment = false;
            this.xrTableCell73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell73.Weight = 2.4374999896035305D;
            // 
            // xrLabel184
            // 
            this.xrLabel184.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel184.LocationFloat = new DevExpress.Utils.PointFloat(13.33331F, 0F);
            this.xrLabel184.Name = "xrLabel184";
            this.xrLabel184.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel184.SizeF = new System.Drawing.SizeF(120F, 23F);
            this.xrLabel184.StylePriority.UseBorders = false;
            this.xrLabel184.Text = "1 阴性   2 阳性";
            // 
            // txtHIV抗体检测
            // 
            this.txtHIV抗体检测.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txtHIV抗体检测.CanGrow = false;
            this.txtHIV抗体检测.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txtHIV抗体检测.LocationFloat = new DevExpress.Utils.PointFloat(488.8031F, 4.999987F);
            this.txtHIV抗体检测.Name = "txtHIV抗体检测";
            this.txtHIV抗体检测.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtHIV抗体检测.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txtHIV抗体检测.StylePriority.UseBorders = false;
            this.txtHIV抗体检测.StylePriority.UseFont = false;
            this.txtHIV抗体检测.StylePriority.UseTextAlignment = false;
            this.txtHIV抗体检测.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable18
            // 
            this.xrTable18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(370F, 406.9584F);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable18.SizeF = new System.Drawing.SizeF(370F, 27F);
            this.xrTable18.StylePriority.UseBorders = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseTextAlignment = false;
            this.xrTableCell40.Text = "血压";
            this.xrTableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell40.Weight = 0.8637786774952283D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel95,
            this.txt血压2,
            this.xrLabel93,
            this.txt血压1});
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.StylePriority.UseTextAlignment = false;
            this.xrTableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell41.Weight = 2.0884590268433461D;
            // 
            // xrLabel95
            // 
            this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(188.7191F, 6.357829E-05F);
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(55.10608F, 23F);
            this.xrLabel95.StylePriority.UseBorders = false;
            this.xrLabel95.Text = "mmHg";
            // 
            // txt血压2
            // 
            this.txt血压2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血压2.LocationFloat = new DevExpress.Utils.PointFloat(113.5847F, 2.000061F);
            this.txt血压2.Name = "txt血压2";
            this.txt血压2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血压2.SizeF = new System.Drawing.SizeF(72.43842F, 23F);
            this.txt血压2.StylePriority.UseBorders = false;
            // 
            // xrLabel93
            // 
            this.xrLabel93.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(91.33574F, 6.357829E-05F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(22.24896F, 23F);
            this.xrLabel93.StylePriority.UseBorders = false;
            this.xrLabel93.Text = "/";
            // 
            // txt血压1
            // 
            this.txt血压1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt血压1.LocationFloat = new DevExpress.Utils.PointFloat(16.34128F, 2.000061F);
            this.txt血压1.Name = "txt血压1";
            this.txt血压1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt血压1.SizeF = new System.Drawing.SizeF(74.99445F, 23F);
            this.txt血压1.StylePriority.UseBorders = false;
            // 
            // txt姓名
            // 
            this.txt姓名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt姓名.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(90.00001F, 48.91666F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(80F, 23F);
            this.txt姓名.StylePriority.UseBorders = false;
            this.txt姓名.StylePriority.UseFont = false;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 12F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report第1次产前随访服务记录表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(41, 46, 0, 12);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRLabel txt填表日期;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel txt剖宫产次数;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel txt分娩次数;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRLabel txt末次月经时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel txt既往史1;
        private DevExpress.XtraReports.UI.XRLabel txt既往史2;
        private DevExpress.XtraReports.UI.XRLabel txt既往史3;
        private DevExpress.XtraReports.UI.XRLabel txt既往史4;
        private DevExpress.XtraReports.UI.XRLabel txt既往史5;
        private DevExpress.XtraReports.UI.XRLabel txt既往史6;
        private DevExpress.XtraReports.UI.XRLabel txt既往史7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRLabel txt家族史其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel txt家族史3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel txt家族史2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel txt家族史1;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel txt个人史其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel txt个人史1;
        private DevExpress.XtraReports.UI.XRLabel txt个人史2;
        private DevExpress.XtraReports.UI.XRLabel txt个人史3;
        private DevExpress.XtraReports.UI.XRLabel txt个人史4;
        private DevExpress.XtraReports.UI.XRLabel txt个人史5;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRLabel txt妇科手术史有;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel txt妇科手术史;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRLabel txt流产;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel txt出生缺陷儿;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel txt新生儿死亡;
        private DevExpress.XtraReports.UI.XRLabel xrLabel82;
        private DevExpress.XtraReports.UI.XRLabel txt死产;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel txt死胎;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel89;
        private DevExpress.XtraReports.UI.XRLabel txt身高;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRLabel txt体重;
        private DevExpress.XtraReports.UI.XRLabel xrLabel91;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRLabel txt血压2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel txt血压1;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRLabel txt心脏;
        private DevExpress.XtraReports.UI.XRLabel txt心脏异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel99;
        private DevExpress.XtraReports.UI.XRLabel txt肺部异常;
        private DevExpress.XtraReports.UI.XRLabel txt肺部;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTable xrTable21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel txt外阴异常;
        private DevExpress.XtraReports.UI.XRLabel txt外阴;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRLabel txt宫颈异常;
        private DevExpress.XtraReports.UI.XRLabel txt宫颈;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel txt子宫异常;
        private DevExpress.XtraReports.UI.XRLabel txt子宫;
        private DevExpress.XtraReports.UI.XRLabel xrLabel105;
        private DevExpress.XtraReports.UI.XRLabel txt阴道异常;
        private DevExpress.XtraReports.UI.XRLabel txt阴道;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel114;
        private DevExpress.XtraReports.UI.XRLabel txt附件异常;
        private DevExpress.XtraReports.UI.XRLabel txt附件;
        private DevExpress.XtraReports.UI.XRTable xrTable25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel txt白细胞计数值;
        private DevExpress.XtraReports.UI.XRLabel xrLabel122;
        private DevExpress.XtraReports.UI.XRLabel xrLabel119;
        private DevExpress.XtraReports.UI.XRLabel txt血红蛋白值;
        private DevExpress.XtraReports.UI.XRLabel xrLabel117;
        private DevExpress.XtraReports.UI.XRLabel xrLabel123;
        private DevExpress.XtraReports.UI.XRLabel txt血小板计数值;
        private DevExpress.XtraReports.UI.XRLabel xrLabel125;
        private DevExpress.XtraReports.UI.XRLabel xrLabel126;
        private DevExpress.XtraReports.UI.XRLabel txt血常规其他;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRLabel txt尿蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel128;
        private DevExpress.XtraReports.UI.XRLabel xrLabel136;
        private DevExpress.XtraReports.UI.XRLabel txt尿常规其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel134;
        private DevExpress.XtraReports.UI.XRLabel txt尿潜血;
        private DevExpress.XtraReports.UI.XRLabel xrLabel132;
        private DevExpress.XtraReports.UI.XRLabel txt尿酮体;
        private DevExpress.XtraReports.UI.XRLabel xrLabel130;
        private DevExpress.XtraReports.UI.XRLabel txt尿糖;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRLabel xrLabel141;
        private DevExpress.XtraReports.UI.XRLabel txt血糖;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRLabel xrLabel144;
        private DevExpress.XtraReports.UI.XRLabel txt血清谷丙转氨酶;
        private DevExpress.XtraReports.UI.XRLabel xrLabel142;
        private DevExpress.XtraReports.UI.XRLabel xrLabel151;
        private DevExpress.XtraReports.UI.XRLabel txt总胆红素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel153;
        private DevExpress.XtraReports.UI.XRLabel xrLabel148;
        private DevExpress.XtraReports.UI.XRLabel txt白蛋白;
        private DevExpress.XtraReports.UI.XRLabel xrLabel150;
        private DevExpress.XtraReports.UI.XRLabel xrLabel145;
        private DevExpress.XtraReports.UI.XRLabel txt血清谷草转氨酶;
        private DevExpress.XtraReports.UI.XRLabel xrLabel147;
        private DevExpress.XtraReports.UI.XRLabel xrLabel154;
        private DevExpress.XtraReports.UI.XRLabel txt结合胆红素;
        private DevExpress.XtraReports.UI.XRLabel xrLabel156;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel160;
        private DevExpress.XtraReports.UI.XRLabel txt血尿素氮;
        private DevExpress.XtraReports.UI.XRLabel xrLabel162;
        private DevExpress.XtraReports.UI.XRLabel xrLabel157;
        private DevExpress.XtraReports.UI.XRLabel txt血清肌酐;
        private DevExpress.XtraReports.UI.XRLabel xrLabel159;
        private DevExpress.XtraReports.UI.XRTable xrTable32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel163;
        private DevExpress.XtraReports.UI.XRLabel txt阴道分泌物其他;
        private DevExpress.XtraReports.UI.XRLabel txt阴道分泌物3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel166;
        private DevExpress.XtraReports.UI.XRLabel txt阴道分泌物2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel168;
        private DevExpress.XtraReports.UI.XRLabel txt阴道分泌物1;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel171;
        private DevExpress.XtraReports.UI.XRLabel txt阴道清洁度;
        private DevExpress.XtraReports.UI.XRTable xrTable34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel172;
        private DevExpress.XtraReports.UI.XRLabel txt乙型肝炎表面抗原;
        private DevExpress.XtraReports.UI.XRLabel xrLabel180;
        private DevExpress.XtraReports.UI.XRLabel txt乙型肝炎核心抗体;
        private DevExpress.XtraReports.UI.XRLabel xrLabel178;
        private DevExpress.XtraReports.UI.XRLabel txt乙型肝炎e抗体;
        private DevExpress.XtraReports.UI.XRLabel xrLabel176;
        private DevExpress.XtraReports.UI.XRLabel txt乙型肝炎e抗原;
        private DevExpress.XtraReports.UI.XRLabel xrLabel174;
        private DevExpress.XtraReports.UI.XRLabel txt乙型肝炎表面抗体;
        private DevExpress.XtraReports.UI.XRTable xrTable35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel183;
        private DevExpress.XtraReports.UI.XRLabel txt梅毒血清学试验;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel184;
        private DevExpress.XtraReports.UI.XRLabel txtHIV抗体检测;
        private DevExpress.XtraReports.UI.XRTable xrTable37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTable xrTable38;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRLabel txt总体评估异常;
        private DevExpress.XtraReports.UI.XRLabel xrLabel187;
        private DevExpress.XtraReports.UI.XRLabel txt总体评估;
        private DevExpress.XtraReports.UI.XRTable xrTable39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel190;
        private DevExpress.XtraReports.UI.XRLabel xrLabel189;
        private DevExpress.XtraReports.UI.XRLabel txt保健指导其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRLabel xrLabel191;
        private DevExpress.XtraReports.UI.XRLabel xrLabel192;
        private DevExpress.XtraReports.UI.XRLabel txt保健指导1;
        private DevExpress.XtraReports.UI.XRLabel txt保健指导2;
        private DevExpress.XtraReports.UI.XRLabel txt保健指导3;
        private DevExpress.XtraReports.UI.XRLabel txt保健指导4;
        private DevExpress.XtraReports.UI.XRLabel txt保健指导5;
        private DevExpress.XtraReports.UI.XRTable xrTable41;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRLabel txt下次随访日期;
        private DevExpress.XtraReports.UI.XRTable xrTable42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTable xrTable43;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRLabel xrLabel212;
        private DevExpress.XtraReports.UI.XRLabel xrLabel213;
        private DevExpress.XtraReports.UI.XRLabel xrLabel214;
        private DevExpress.XtraReports.UI.XRLabel txt转诊原因;
        private DevExpress.XtraReports.UI.XRLabel xrLabel216;
        private DevExpress.XtraReports.UI.XRLabel txt转诊机构;
        private DevExpress.XtraReports.UI.XRLabel txt转诊;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel139;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRLabel txt孕妇年龄;
        private DevExpress.XtraReports.UI.XRLabel txt填表孕周;
        private DevExpress.XtraReports.UI.XRLabel txt丈夫年龄;
        private DevExpress.XtraReports.UI.XRLabel txt丈夫电话;
        private DevExpress.XtraReports.UI.XRLabel txt丈夫姓名;
        private DevExpress.XtraReports.UI.XRLabel txt孕次;
        private DevExpress.XtraReports.UI.XRLabel txt预产期;
        private DevExpress.XtraReports.UI.XRLabel txt体质指数;
        private DevExpress.XtraReports.UI.XRLabel txt血型;
        private DevExpress.XtraReports.UI.XRLabel txtB超;
        private DevExpress.XtraReports.UI.XRLabel txt随访医生签名;
        private DevExpress.XtraReports.UI.XRLabel txt既往史其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel txt保健指导6;
    }
}
