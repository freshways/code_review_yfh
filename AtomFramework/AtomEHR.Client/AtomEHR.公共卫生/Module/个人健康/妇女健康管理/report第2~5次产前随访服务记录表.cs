﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Business;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class roport第2至5次产前随访服务记录表 : DevExpress.XtraReports.UI.XtraReport
    {
        #region Fields
        DataSet _ds孕产妇2次;
        DataSet _ds孕产妇3次;
        DataSet _ds孕产妇4次;
        DataSet _ds孕产妇5次;
        string docNo;
        bll孕妇_产前随访2次 _bll随访2次 = new bll孕妇_产前随访2次();
        bll孕妇_产前随访3次 _bll随访3次 = new bll孕妇_产前随访3次();
        bll孕妇_产前随访4次 _bll随访4次 = new bll孕妇_产前随访4次();
        bll孕妇_产前随访5次 _bll随访5次 = new bll孕妇_产前随访5次();
        #endregion
        public roport第2至5次产前随访服务记录表()
        {
            InitializeComponent();
        }
        public roport第2至5次产前随访服务记录表(string _docNo)
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds孕产妇2次 = _bll随访2次.GetInfoSuiFang2(_docNo, true);
            _ds孕产妇3次 = _bll随访3次.GetInfoSuiFang3(_docNo, true);
            _ds孕产妇4次 = _bll随访4次.GetInfoSuiFang4(_docNo, true);
            _ds孕产妇5次 = _bll随访5次.GetInfoSuiFang5(_docNo, true);
            DoBindingDataSource(_ds孕产妇2次, _ds孕产妇3次, _ds孕产妇4次, _ds孕产妇5次);
        }

        private void DoBindingDataSource(DataSet _ds孕产妇2次,DataSet _ds孕产妇3次,DataSet _ds孕产妇4次,DataSet _ds孕产妇5次)
        {
            DataTable dt孕产妇2次 = _ds孕产妇2次.Tables[Models.tb_孕妇_产前随访2次.__TableName];

            DataTable dt孕产妇3次 = _ds孕产妇3次.Tables[Models.tb_孕妇_产前随访3次.__TableName];

            DataTable dt孕产妇4次 = _ds孕产妇4次.Tables[Models.tb_孕妇_产前随访4次.__TableName];

            DataTable dt孕产妇5次 = _ds孕产妇5次.Tables[Models.tb_孕妇_产前随访5次.__TableName];

            DataTable dt健康档案 = _ds孕产妇2次.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
          
            #region 第2次随访
            if (dt孕产妇2次 != null && dt孕产妇2次.Rows.Count >0)
            {
                this.txt第2次随访日期.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.发生时间].ToString();
                this.txt第2次孕周.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.孕周].ToString();
                this.txt第2次主诉.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.主诉].ToString();
                this.txt第2次体重.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.体重].ToString();
                this.txt第2次宫底高度.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.宫底高度].ToString();
                this.txt第2次腹围.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.腹围].ToString();
                this.txt第2次胎位.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.胎位].ToString();
                this.txt第2次胎心率.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.胎心率].ToString();
                this.txt第2次血压1.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.收缩压].ToString();//高压
                this.txt第2次血压2.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.舒张压].ToString();//低压
                this.txt第2次血红蛋白.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.血红蛋白].ToString();
                this.txt第2次尿蛋白.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.尿蛋白].ToString();
                this.txt第2次其他辅助检查.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.其他检查].ToString();
                this.txt第2次分类.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.分类].ToString();
                if (this.txt第2次分类.Text == "99") this.txt第2次分类.Text = "2";
                this.txt第2次分类异常.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.分类异常].ToString();
                
                string 第2次产前随访指导= dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.指导].ToString();
                if (!string.IsNullOrEmpty(第2次产前随访指导))
                {
                    string[] a = 第2次产前随访指导.Split(',');
                    for (int i = 0; i < a.Length;i++ )
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName="txt第2次产前随访指导"+(i+1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        lbl.Text = a[i];

                    }
                }
                
                this.txt第2次指导其他.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.指导其他].ToString();
                this.txt第2次有无转诊.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.转诊].ToString();
                this.txt第2次转诊原因.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.转诊原因].ToString();
                this.txt第2次转诊机构及科室.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.转诊机构].ToString();
                this.txt第2次下次随访日期.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.下次随访日期].ToString();
                this.txt第2次随访医生签名.Text = dt孕产妇2次.Rows[0][tb_孕妇_产前随访2次.随访医生].ToString();
            }
            #endregion

            #region 第3次随访
            if (dt孕产妇3次 != null && dt孕产妇3次.Rows.Count >0)
            {
                this.txt第3次随访日期.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.发生时间].ToString();
                this.txt第3次孕周.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.孕周].ToString();
                this.txt第3次主诉.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.主诉].ToString();
                this.txt第3次体重.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.体重].ToString();
                this.txt第3次宫底高度.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.宫底高度].ToString();
                this.txt第3次腹围.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.腹围].ToString();
                this.txt第3次胎位.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.胎位].ToString();
                this.txt第3次胎心率.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.胎心率].ToString();
                this.txt第3次血压1.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.收缩压].ToString();//高压
                this.txt第3次血压2.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.舒张压].ToString();//低压
                this.txt第3次血红蛋白.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.血红蛋白].ToString();
                this.txt第3次尿蛋白.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.尿蛋白].ToString();
                this.txt第3次其他辅助检查.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.其他检查].ToString();
                this.txt第3次分类.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.分类].ToString();
                if (this.txt第3次分类.Text == "99") this.txt第3次分类.Text = "2";
                this.txt第3次分类异常.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.分类异常].ToString();

                string 第3次产前随访指导 = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.指导].ToString();
                if (!string.IsNullOrEmpty(第3次产前随访指导))
                {
                    string[] a = 第3次产前随访指导.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt第3次产前随访指导" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        lbl.Text = a[i];

                    }
                }

                this.txt第3次指导其他.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.指导其他].ToString();
                this.txt第3次有无转诊.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.转诊].ToString();
                this.txt第3次转诊原因.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.转诊原因].ToString();
                this.txt第3次转诊机构及科室.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.转诊机构].ToString();
                this.txt第3次下次随访日期.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.下次随访日期].ToString();
                this.txt第3次随访医生签名.Text = dt孕产妇3次.Rows[0][tb_孕妇_产前随访3次.随访医生].ToString();
            }
            #endregion

            #region 第4次随访
            if (dt孕产妇4次 != null && dt孕产妇4次.Rows.Count >0)
            {
                this.txt第4次随访日期.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.发生时间].ToString();
                this.txt第4次孕周.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.孕周].ToString();
                this.txt第4次主诉.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.主诉].ToString();
                this.txt第4次体重.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.体重].ToString();
                this.txt第4次宫底高度.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.宫底高度].ToString();
                this.txt第4次腹围.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.腹围].ToString();
                this.txt第4次胎位.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.胎位].ToString();
                this.txt第4次胎心率.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.胎心率].ToString();
                this.txt第4次血压1.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.收缩压].ToString();//高压
                this.txt第4次血压2.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.舒张压].ToString();//低压
                this.txt第4次血红蛋白.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.血红蛋白].ToString();
                this.txt第4次尿蛋白.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.尿蛋白].ToString();
                this.txt第4次其他辅助检查.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.其他检查].ToString();
                this.txt第4次分类.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.分类].ToString();
                if (this.txt第4次分类.Text == "99") this.txt第4次分类.Text = "2";
                this.txt第4次分类异常.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.分类异常].ToString();

                string 第4次产前随访指导 = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.指导].ToString();
                if (!string.IsNullOrEmpty(第4次产前随访指导))
                {
                    string[] a = 第4次产前随访指导.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt第4次产前随访指导" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        lbl.Text = a[i];

                    }
                }

                this.txt第4次指导其他.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.指导其他].ToString();
                this.txt第4次有无转诊.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.转诊].ToString();
                this.txt第4次转诊原因.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.转诊原因].ToString();
                this.txt第4次转诊机构及科室.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.转诊机构].ToString();
                this.txt第4次下次随访日期.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.下次随访日期].ToString();
                this.txt第4次随访医生签名.Text = dt孕产妇4次.Rows[0][tb_孕妇_产前随访4次.随访医生].ToString();
            }
            #endregion

            #region 第5次随访
            if (dt孕产妇5次 != null && dt孕产妇5次.Rows.Count >0)
            {
                this.txt第5次随访日期.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.发生时间].ToString();
                this.txt第5次孕周.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.孕周].ToString();
                this.txt第5次主诉.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.主诉].ToString();
                this.txt第5次体重.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.体重].ToString();
                this.txt第5次宫底高度.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.宫底高度].ToString();
                this.txt第5次腹围.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.腹围].ToString();
                this.txt第5次胎位.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.胎位].ToString();
                this.txt第5次胎心率.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.胎心率].ToString();
                this.txt第5次血压1.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.收缩压].ToString();//高压
                this.txt第5次血压2.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.舒张压].ToString();//低压
                this.txt第5次血红蛋白.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.血红蛋白].ToString();
                this.txt第5次尿蛋白.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.尿蛋白].ToString();
                this.txt第5次其他辅助检查.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.其他检查].ToString();
                this.txt第5次分类.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.分类].ToString();
                if (this.txt第5次分类.Text == "99") this.txt第5次分类.Text = "2";
                this.txt第5次分类异常.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.分类异常].ToString();

                string 第5次产前随访指导 = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.指导].ToString();
                if (!string.IsNullOrEmpty(第5次产前随访指导))
                {
                    string[] a = 第5次产前随访指导.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt第5次产前随访指导" + (i + 1);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        lbl.Text = a[i];

                    }
                }

                this.txt第5次指导其他.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.指导其他].ToString();
                this.txt第5次有无转诊.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.转诊].ToString();
                this.txt第5次转诊原因.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.转诊原因].ToString();
                this.txt第5次转诊机构及科室.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.转诊机构].ToString();
                this.txt第5次下次随访日期.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.下次随访日期].ToString();
                this.txt第5次随访医生签名.Text = dt孕产妇5次.Rows[0][tb_孕妇_产前随访5次.随访医生].ToString();
            }
            #endregion

        }
    }
}
