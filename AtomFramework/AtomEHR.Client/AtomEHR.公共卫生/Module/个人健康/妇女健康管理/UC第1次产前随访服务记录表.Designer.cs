﻿namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    partial class UC第1次产前随访服务记录表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC第1次产前随访服务记录表));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.sbtnCalYZ = new DevExpress.XtraEditors.SimpleButton();
            this.txt家属签名 = new DevExpress.XtraEditors.TextEdit();
            this.dte下次随访日期 = new DevExpress.XtraEditors.DateEdit();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo建册情况 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt建册机构名称 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.cbo转诊有无 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.uc转诊原因 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc机构及科室 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc转诊联系人 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc联系方式 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.label2 = new System.Windows.Forms.Label();
            this.cbo是否到位 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flow保健指导 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk保健指导1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk保健指导2 = new DevExpress.XtraEditors.CheckEdit();
            this.chk保健指导3 = new DevExpress.XtraEditors.CheckEdit();
            this.chk保健指导4 = new DevExpress.XtraEditors.CheckEdit();
            this.chk保健指导5 = new DevExpress.XtraEditors.CheckEdit();
            this.chk保健指导6 = new DevExpress.XtraEditors.CheckEdit();
            this.txt保健指导其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo总体评估 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit总体评估异常 = new DevExpress.XtraEditors.TextEdit();
            this.textEditB超 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc乙型肝炎表面抗原 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc乙型肝炎表面抗体 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc乙型肝炎e抗原 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc乙型肝炎e抗体 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc乙型肝炎核心抗体 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo阴道分泌物 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit阴道分泌物其他 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc血清谷丙转氨酶 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血清谷草转氨酶 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc白蛋白 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc总胆红素 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc结合胆红素 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc尿蛋白 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc尿糖 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc尿酮体 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc尿潜血 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc尿常规其他 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc血红蛋白值 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc白细胞计数值 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血小板计数值 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血常规其他 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc血清肌酐 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血尿素氮 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.textEdit附件 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit子宫 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit宫颈 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit外阴 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit阴道 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit肺部 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit心脏 = new DevExpress.XtraEditors.TextEdit();
            this.uc血压 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit体重指数 = new DevExpress.XtraEditors.TextEdit();
            this.uc体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.uc身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc流产 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc死胎 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc死产 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc新生儿死亡 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc出生缺陷儿 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.textEdit妇科手术史 = new DevExpress.XtraEditors.TextEdit();
            this.cbo妇科手术史 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flow个人史 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk个人史0 = new DevExpress.XtraEditors.CheckEdit();
            this.chk个人史1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk个人史2 = new DevExpress.XtraEditors.CheckEdit();
            this.chk个人史3 = new DevExpress.XtraEditors.CheckEdit();
            this.chk个人史4 = new DevExpress.XtraEditors.CheckEdit();
            this.chk个人史5 = new DevExpress.XtraEditors.CheckEdit();
            this.chk个人史6 = new DevExpress.XtraEditors.CheckEdit();
            this.txt个人史其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow家族史 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk家族史0 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史2 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史3 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史4 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史5 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史6 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit家族史其他 = new DevExpress.XtraEditors.TextEdit();
            this.flow既往病史 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk既往病史有无 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit既往病史其他 = new DevExpress.XtraEditors.TextEdit();
            this.dte预产期 = new DevExpress.XtraEditors.DateEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk末次月经 = new DevExpress.XtraEditors.CheckEdit();
            this.dte末次月经 = new DevExpress.XtraEditors.DateEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.uc产次 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.uc填表孕周 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit丈夫电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit丈夫年龄 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit丈夫姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住状态 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit孕妇年龄 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit孕妇姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit卡号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit孕次 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo心脏 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo肺部 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo血型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cboRH = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo阴道清洁度 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo梅毒血清学试验 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cboHIV抗体检测 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo外阴 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo宫颈 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo附件 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo阴道 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo子宫 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dte填表日期 = new DevExpress.XtraEditors.DateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layout妇科手术史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem74 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem75 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem76 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem65 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem66 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem64 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem67 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem68 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem69 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem70 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem71 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem72 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem73 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn读取 = new DevExpress.XtraEditors.SimpleButton();
            this.btn填表说明 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn上传化验单图片 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt家属签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo建册情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt建册机构名称.Properties)).BeginInit();
            this.flowLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo转诊有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo是否到位.Properties)).BeginInit();
            this.flow保健指导.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt保健指导其他.Properties)).BeginInit();
            this.flowLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo总体评估.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit总体评估异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditB超.Properties)).BeginInit();
            this.flowLayoutPanel13.SuspendLayout();
            this.flowLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo阴道分泌物.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit阴道分泌物其他.Properties)).BeginInit();
            this.flowLayoutPanel10.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit附件.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit子宫.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit宫颈.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit外阴.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit阴道.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit肺部.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit心脏.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体重指数.Properties)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit妇科手术史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo妇科手术史.Properties)).BeginInit();
            this.flow个人史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史0.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人史其他.Properties)).BeginInit();
            this.flow家族史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史0.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家族史其他.Properties)).BeginInit();
            this.flow既往病史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk既往病史有无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit既往病史其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte预产期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte预产期.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk末次月经.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte末次月经.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte末次月经.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫年龄.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇年龄.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo心脏.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo肺部.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo血型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo阴道清洁度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo梅毒血清学试验.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboHIV抗体检测.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo外阴.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo宫颈.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo附件.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo阴道.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo子宫.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte填表日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte填表日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout妇科手术史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(748, 32);
            this.panelControl1.TabIndex = 6;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn读取);
            this.flowLayoutPanel1.Controls.Add(this.btn填表说明);
            this.flowLayoutPanel1.Controls.Add(this.sbtn上传化验单图片);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(744, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.sbtnCalYZ);
            this.layoutControl1.Controls.Add(this.txt家属签名);
            this.layoutControl1.Controls.Add(this.dte下次随访日期);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel16);
            this.layoutControl1.Controls.Add(this.flow保健指导);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel14);
            this.layoutControl1.Controls.Add(this.textEditB超);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel13);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel12);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel10);
            this.layoutControl1.Controls.Add(this.uc血糖);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel9);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel8);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel11);
            this.layoutControl1.Controls.Add(this.textEdit附件);
            this.layoutControl1.Controls.Add(this.textEdit子宫);
            this.layoutControl1.Controls.Add(this.textEdit宫颈);
            this.layoutControl1.Controls.Add(this.textEdit外阴);
            this.layoutControl1.Controls.Add(this.textEdit阴道);
            this.layoutControl1.Controls.Add(this.textEdit肺部);
            this.layoutControl1.Controls.Add(this.textEdit心脏);
            this.layoutControl1.Controls.Add(this.uc血压);
            this.layoutControl1.Controls.Add(this.textEdit体重指数);
            this.layoutControl1.Controls.Add(this.uc体重);
            this.layoutControl1.Controls.Add(this.uc身高);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel7);
            this.layoutControl1.Controls.Add(this.textEdit妇科手术史);
            this.layoutControl1.Controls.Add(this.cbo妇科手术史);
            this.layoutControl1.Controls.Add(this.flow个人史);
            this.layoutControl1.Controls.Add(this.flow家族史);
            this.layoutControl1.Controls.Add(this.flow既往病史);
            this.layoutControl1.Controls.Add(this.dte预产期);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.uc填表孕周);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit创建人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近更新时间);
            this.layoutControl1.Controls.Add(this.textEdit创建时间);
            this.layoutControl1.Controls.Add(this.textEdit丈夫电话);
            this.layoutControl1.Controls.Add(this.textEdit丈夫年龄);
            this.layoutControl1.Controls.Add(this.textEdit丈夫姓名);
            this.layoutControl1.Controls.Add(this.textEdit居住地址);
            this.layoutControl1.Controls.Add(this.textEdit居住状态);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.textEdit孕妇年龄);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit孕妇姓名);
            this.layoutControl1.Controls.Add(this.textEdit档案编号);
            this.layoutControl1.Controls.Add(this.textEdit卡号);
            this.layoutControl1.Controls.Add(this.textEdit孕次);
            this.layoutControl1.Controls.Add(this.cbo心脏);
            this.layoutControl1.Controls.Add(this.cbo肺部);
            this.layoutControl1.Controls.Add(this.cbo血型);
            this.layoutControl1.Controls.Add(this.cboRH);
            this.layoutControl1.Controls.Add(this.cbo阴道清洁度);
            this.layoutControl1.Controls.Add(this.cbo梅毒血清学试验);
            this.layoutControl1.Controls.Add(this.cboHIV抗体检测);
            this.layoutControl1.Controls.Add(this.cbo外阴);
            this.layoutControl1.Controls.Add(this.cbo宫颈);
            this.layoutControl1.Controls.Add(this.cbo附件);
            this.layoutControl1.Controls.Add(this.cbo阴道);
            this.layoutControl1.Controls.Add(this.cbo子宫);
            this.layoutControl1.Controls.Add(this.dte填表日期);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsFocus.AllowFocusGroups = false;
            this.layoutControl1.OptionsFocus.AllowFocusReadonlyEditors = false;
            this.layoutControl1.OptionsFocus.AllowFocusTabbedGroups = false;
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(748, 466);
            this.layoutControl1.TabIndex = 7;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // sbtnCalYZ
            // 
            this.sbtnCalYZ.Location = new System.Drawing.Point(615, 102);
            this.sbtnCalYZ.Name = "sbtnCalYZ";
            this.sbtnCalYZ.Size = new System.Drawing.Size(82, 22);
            this.sbtnCalYZ.StyleController = this.layoutControl1;
            this.sbtnCalYZ.TabIndex = 144;
            this.sbtnCalYZ.Text = "计算孕周";
            this.sbtnCalYZ.Click += new System.EventHandler(this.sbtnCalYZ_Click);
            // 
            // txt家属签名
            // 
            this.txt家属签名.Location = new System.Drawing.Point(566, 1161);
            this.txt家属签名.Name = "txt家属签名";
            this.txt家属签名.Size = new System.Drawing.Size(162, 20);
            this.txt家属签名.StyleController = this.layoutControl1;
            this.txt家属签名.TabIndex = 143;
            // 
            // dte下次随访日期
            // 
            this.dte下次随访日期.EditValue = null;
            this.dte下次随访日期.Location = new System.Drawing.Point(88, 1161);
            this.dte下次随访日期.Name = "dte下次随访日期";
            this.dte下次随访日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte下次随访日期.Size = new System.Drawing.Size(142, 20);
            this.dte下次随访日期.StyleController = this.layoutControl1;
            this.dte下次随访日期.TabIndex = 142;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.cbo建册情况);
            this.flowLayoutPanel4.Controls.Add(this.txt建册机构名称);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(88, 1072);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(640, 25);
            this.flowLayoutPanel4.TabIndex = 141;
            // 
            // cbo建册情况
            // 
            this.cbo建册情况.Location = new System.Drawing.Point(3, 3);
            this.cbo建册情况.Name = "cbo建册情况";
            this.cbo建册情况.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo建册情况.Size = new System.Drawing.Size(176, 20);
            this.cbo建册情况.TabIndex = 2;
            this.cbo建册情况.SelectedIndexChanged += new System.EventHandler(this.cbo建册情况_SelectedIndexChanged);
            // 
            // txt建册机构名称
            // 
            this.txt建册机构名称.Location = new System.Drawing.Point(185, 3);
            this.txt建册机构名称.Name = "txt建册机构名称";
            this.txt建册机构名称.Size = new System.Drawing.Size(185, 20);
            this.txt建册机构名称.TabIndex = 1;
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.Controls.Add(this.label1);
            this.flowLayoutPanel16.Controls.Add(this.cbo转诊有无);
            this.flowLayoutPanel16.Controls.Add(this.uc转诊原因);
            this.flowLayoutPanel16.Controls.Add(this.uc机构及科室);
            this.flowLayoutPanel16.Controls.Add(this.uc转诊联系人);
            this.flowLayoutPanel16.Controls.Add(this.uc联系方式);
            this.flowLayoutPanel16.Controls.Add(this.label2);
            this.flowLayoutPanel16.Controls.Add(this.cbo是否到位);
            this.flowLayoutPanel16.Location = new System.Drawing.Point(88, 1101);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(640, 56);
            this.flowLayoutPanel16.TabIndex = 129;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 140;
            this.label1.Text = "有无转诊：";
            // 
            // cbo转诊有无
            // 
            this.cbo转诊有无.Location = new System.Drawing.Point(71, 0);
            this.cbo转诊有无.Margin = new System.Windows.Forms.Padding(0);
            this.cbo转诊有无.Name = "cbo转诊有无";
            this.cbo转诊有无.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo转诊有无.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo转诊有无.Size = new System.Drawing.Size(64, 20);
            this.cbo转诊有无.TabIndex = 130;
            this.cbo转诊有无.SelectedIndexChanged += new System.EventHandler(this.cbo转诊有无_SelectedIndexChanged);
            // 
            // uc转诊原因
            // 
            this.uc转诊原因.Lbl1Size = new System.Drawing.Size(40, 18);
            this.uc转诊原因.Lbl1Text = "原因:";
            this.uc转诊原因.Location = new System.Drawing.Point(135, 0);
            this.uc转诊原因.Margin = new System.Windows.Forms.Padding(0);
            this.uc转诊原因.Name = "uc转诊原因";
            this.uc转诊原因.Size = new System.Drawing.Size(205, 22);
            this.uc转诊原因.TabIndex = 131;
            this.uc转诊原因.Txt1Size = new System.Drawing.Size(160, 20);
            // 
            // uc机构及科室
            // 
            this.uc机构及科室.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc机构及科室.Lbl1Text = " 机构及科室:";
            this.uc机构及科室.Location = new System.Drawing.Point(340, 0);
            this.uc机构及科室.Margin = new System.Windows.Forms.Padding(0);
            this.uc机构及科室.Name = "uc机构及科室";
            this.uc机构及科室.Size = new System.Drawing.Size(225, 22);
            this.uc机构及科室.TabIndex = 132;
            this.uc机构及科室.Txt1Size = new System.Drawing.Size(150, 20);
            // 
            // uc转诊联系人
            // 
            this.uc转诊联系人.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc转诊联系人.Lbl1Text = "联系人：";
            this.uc转诊联系人.Location = new System.Drawing.Point(3, 25);
            this.uc转诊联系人.Name = "uc转诊联系人";
            this.uc转诊联系人.Size = new System.Drawing.Size(145, 22);
            this.uc转诊联系人.TabIndex = 133;
            this.uc转诊联系人.Txt1Size = new System.Drawing.Size(90, 20);
            // 
            // uc联系方式
            // 
            this.uc联系方式.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc联系方式.Lbl1Text = "联系方式：";
            this.uc联系方式.Location = new System.Drawing.Point(154, 25);
            this.uc联系方式.Name = "uc联系方式";
            this.uc联系方式.Size = new System.Drawing.Size(230, 22);
            this.uc联系方式.TabIndex = 141;
            this.uc联系方式.Txt1Size = new System.Drawing.Size(150, 20);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(390, 28);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 142;
            this.label2.Text = "结果：";
            // 
            // cbo是否到位
            // 
            this.cbo是否到位.Location = new System.Drawing.Point(437, 25);
            this.cbo是否到位.Name = "cbo是否到位";
            this.cbo是否到位.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo是否到位.Size = new System.Drawing.Size(109, 20);
            this.cbo是否到位.TabIndex = 139;
            // 
            // flow保健指导
            // 
            this.flow保健指导.Controls.Add(this.chk保健指导1);
            this.flow保健指导.Controls.Add(this.chk保健指导2);
            this.flow保健指导.Controls.Add(this.chk保健指导3);
            this.flow保健指导.Controls.Add(this.chk保健指导4);
            this.flow保健指导.Controls.Add(this.chk保健指导5);
            this.flow保健指导.Controls.Add(this.chk保健指导6);
            this.flow保健指导.Controls.Add(this.txt保健指导其他);
            this.flow保健指导.Location = new System.Drawing.Point(88, 1022);
            this.flow保健指导.Name = "flow保健指导";
            this.flow保健指导.Size = new System.Drawing.Size(640, 46);
            this.flow保健指导.TabIndex = 121;
            // 
            // chk保健指导1
            // 
            this.chk保健指导1.Location = new System.Drawing.Point(0, 0);
            this.chk保健指导1.Margin = new System.Windows.Forms.Padding(0);
            this.chk保健指导1.Name = "chk保健指导1";
            this.chk保健指导1.Properties.Caption = "生活方式";
            this.chk保健指导1.Size = new System.Drawing.Size(71, 19);
            this.chk保健指导1.TabIndex = 122;
            this.chk保健指导1.Tag = "1";
            // 
            // chk保健指导2
            // 
            this.chk保健指导2.Location = new System.Drawing.Point(71, 0);
            this.chk保健指导2.Margin = new System.Windows.Forms.Padding(0);
            this.chk保健指导2.Name = "chk保健指导2";
            this.chk保健指导2.Properties.Caption = "心理";
            this.chk保健指导2.Size = new System.Drawing.Size(64, 19);
            this.chk保健指导2.TabIndex = 123;
            this.chk保健指导2.Tag = "2";
            // 
            // chk保健指导3
            // 
            this.chk保健指导3.Location = new System.Drawing.Point(135, 0);
            this.chk保健指导3.Margin = new System.Windows.Forms.Padding(0);
            this.chk保健指导3.Name = "chk保健指导3";
            this.chk保健指导3.Properties.Caption = "营养";
            this.chk保健指导3.Size = new System.Drawing.Size(59, 19);
            this.chk保健指导3.TabIndex = 124;
            this.chk保健指导3.Tag = "3";
            // 
            // chk保健指导4
            // 
            this.chk保健指导4.Location = new System.Drawing.Point(194, 0);
            this.chk保健指导4.Margin = new System.Windows.Forms.Padding(0);
            this.chk保健指导4.Name = "chk保健指导4";
            this.chk保健指导4.Properties.Caption = "避免致畸因素和疾病对胚胎的不良影响\r\n";
            this.chk保健指导4.Size = new System.Drawing.Size(232, 19);
            this.chk保健指导4.TabIndex = 125;
            this.chk保健指导4.Tag = "4";
            // 
            // chk保健指导5
            // 
            this.chk保健指导5.Location = new System.Drawing.Point(426, 0);
            this.chk保健指导5.Margin = new System.Windows.Forms.Padding(0);
            this.chk保健指导5.Name = "chk保健指导5";
            this.chk保健指导5.Properties.Caption = "产前筛查宣传告知";
            this.chk保健指导5.Size = new System.Drawing.Size(127, 19);
            this.chk保健指导5.TabIndex = 126;
            this.chk保健指导5.Tag = "5";
            // 
            // chk保健指导6
            // 
            this.chk保健指导6.Location = new System.Drawing.Point(553, 0);
            this.chk保健指导6.Margin = new System.Windows.Forms.Padding(0);
            this.chk保健指导6.Name = "chk保健指导6";
            this.chk保健指导6.Properties.Caption = "其他";
            this.chk保健指导6.Size = new System.Drawing.Size(65, 19);
            this.chk保健指导6.TabIndex = 127;
            this.chk保健指导6.Tag = "6";
            this.chk保健指导6.CheckedChanged += new System.EventHandler(this.chk保健指导6_CheckedChanged);
            // 
            // txt保健指导其他
            // 
            this.txt保健指导其他.Enabled = false;
            this.txt保健指导其他.Location = new System.Drawing.Point(0, 19);
            this.txt保健指导其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt保健指导其他.Name = "txt保健指导其他";
            this.txt保健指导其他.Size = new System.Drawing.Size(340, 20);
            this.txt保健指导其他.TabIndex = 128;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.Controls.Add(this.cbo总体评估);
            this.flowLayoutPanel14.Controls.Add(this.textEdit总体评估异常);
            this.flowLayoutPanel14.Location = new System.Drawing.Point(88, 998);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(640, 20);
            this.flowLayoutPanel14.TabIndex = 118;
            // 
            // cbo总体评估
            // 
            this.cbo总体评估.Location = new System.Drawing.Point(0, 0);
            this.cbo总体评估.Margin = new System.Windows.Forms.Padding(0);
            this.cbo总体评估.Name = "cbo总体评估";
            this.cbo总体评估.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo总体评估.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo总体评估.Size = new System.Drawing.Size(100, 20);
            this.cbo总体评估.TabIndex = 119;
            this.cbo总体评估.SelectedIndexChanged += new System.EventHandler(this.cbo总体评估_SelectedIndexChanged);
            // 
            // textEdit总体评估异常
            // 
            this.textEdit总体评估异常.Enabled = false;
            this.textEdit总体评估异常.Location = new System.Drawing.Point(100, 0);
            this.textEdit总体评估异常.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit总体评估异常.Name = "textEdit总体评估异常";
            this.textEdit总体评估异常.Size = new System.Drawing.Size(222, 20);
            this.textEdit总体评估异常.TabIndex = 120;
            // 
            // textEditB超
            // 
            this.textEditB超.Location = new System.Drawing.Point(180, 974);
            this.textEditB超.Name = "textEditB超";
            this.textEditB超.Size = new System.Drawing.Size(225, 20);
            this.textEditB超.StyleController = this.layoutControl1;
            this.textEditB超.TabIndex = 117;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.Controls.Add(this.uc乙型肝炎表面抗原);
            this.flowLayoutPanel13.Controls.Add(this.uc乙型肝炎表面抗体);
            this.flowLayoutPanel13.Controls.Add(this.uc乙型肝炎e抗原);
            this.flowLayoutPanel13.Controls.Add(this.uc乙型肝炎e抗体);
            this.flowLayoutPanel13.Controls.Add(this.uc乙型肝炎核心抗体);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(180, 841);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(548, 81);
            this.flowLayoutPanel13.TabIndex = 109;
            // 
            // uc乙型肝炎表面抗原
            // 
            this.uc乙型肝炎表面抗原.Lbl1Size = new System.Drawing.Size(100, 18);
            this.uc乙型肝炎表面抗原.Lbl1Text = "乙型肝炎表面抗原";
            this.uc乙型肝炎表面抗原.Location = new System.Drawing.Point(0, 0);
            this.uc乙型肝炎表面抗原.Margin = new System.Windows.Forms.Padding(0);
            this.uc乙型肝炎表面抗原.Name = "uc乙型肝炎表面抗原";
            this.uc乙型肝炎表面抗原.Size = new System.Drawing.Size(223, 22);
            this.uc乙型肝炎表面抗原.TabIndex = 110;
            this.uc乙型肝炎表面抗原.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc乙型肝炎表面抗体
            // 
            this.uc乙型肝炎表面抗体.Lbl1Size = new System.Drawing.Size(100, 18);
            this.uc乙型肝炎表面抗体.Lbl1Text = "乙型肝炎表面抗体";
            this.uc乙型肝炎表面抗体.Location = new System.Drawing.Point(223, 0);
            this.uc乙型肝炎表面抗体.Margin = new System.Windows.Forms.Padding(0);
            this.uc乙型肝炎表面抗体.Name = "uc乙型肝炎表面抗体";
            this.uc乙型肝炎表面抗体.Size = new System.Drawing.Size(211, 22);
            this.uc乙型肝炎表面抗体.TabIndex = 111;
            this.uc乙型肝炎表面抗体.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc乙型肝炎e抗原
            // 
            this.uc乙型肝炎e抗原.Lbl1Size = new System.Drawing.Size(100, 18);
            this.uc乙型肝炎e抗原.Lbl1Text = "乙型肝炎e抗原";
            this.uc乙型肝炎e抗原.Location = new System.Drawing.Point(0, 22);
            this.uc乙型肝炎e抗原.Margin = new System.Windows.Forms.Padding(0);
            this.uc乙型肝炎e抗原.Name = "uc乙型肝炎e抗原";
            this.uc乙型肝炎e抗原.Size = new System.Drawing.Size(223, 22);
            this.uc乙型肝炎e抗原.TabIndex = 112;
            this.uc乙型肝炎e抗原.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc乙型肝炎e抗体
            // 
            this.uc乙型肝炎e抗体.Lbl1Size = new System.Drawing.Size(100, 18);
            this.uc乙型肝炎e抗体.Lbl1Text = "乙型肝炎e抗体";
            this.uc乙型肝炎e抗体.Location = new System.Drawing.Point(223, 22);
            this.uc乙型肝炎e抗体.Margin = new System.Windows.Forms.Padding(0);
            this.uc乙型肝炎e抗体.Name = "uc乙型肝炎e抗体";
            this.uc乙型肝炎e抗体.Size = new System.Drawing.Size(217, 22);
            this.uc乙型肝炎e抗体.TabIndex = 113;
            this.uc乙型肝炎e抗体.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc乙型肝炎核心抗体
            // 
            this.uc乙型肝炎核心抗体.Lbl1Size = new System.Drawing.Size(100, 18);
            this.uc乙型肝炎核心抗体.Lbl1Text = "乙型肝炎核心抗体";
            this.uc乙型肝炎核心抗体.Location = new System.Drawing.Point(0, 44);
            this.uc乙型肝炎核心抗体.Margin = new System.Windows.Forms.Padding(0);
            this.uc乙型肝炎核心抗体.Name = "uc乙型肝炎核心抗体";
            this.uc乙型肝炎核心抗体.Size = new System.Drawing.Size(217, 22);
            this.uc乙型肝炎核心抗体.TabIndex = 114;
            this.uc乙型肝炎核心抗体.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.Controls.Add(this.cbo阴道分泌物);
            this.flowLayoutPanel12.Controls.Add(this.textEdit阴道分泌物其他);
            this.flowLayoutPanel12.Location = new System.Drawing.Point(177, 793);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(551, 20);
            this.flowLayoutPanel12.TabIndex = 105;
            // 
            // cbo阴道分泌物
            // 
            this.cbo阴道分泌物.Location = new System.Drawing.Point(0, 0);
            this.cbo阴道分泌物.Margin = new System.Windows.Forms.Padding(0);
            this.cbo阴道分泌物.Name = "cbo阴道分泌物";
            this.cbo阴道分泌物.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo阴道分泌物.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo阴道分泌物.Size = new System.Drawing.Size(100, 20);
            this.cbo阴道分泌物.TabIndex = 106;
            this.cbo阴道分泌物.SelectedIndexChanged += new System.EventHandler(this.cbo阴道分泌物_SelectedIndexChanged);
            // 
            // textEdit阴道分泌物其他
            // 
            this.textEdit阴道分泌物其他.Enabled = false;
            this.textEdit阴道分泌物其他.Location = new System.Drawing.Point(100, 0);
            this.textEdit阴道分泌物其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit阴道分泌物其他.Name = "textEdit阴道分泌物其他";
            this.textEdit阴道分泌物其他.Size = new System.Drawing.Size(164, 20);
            this.textEdit阴道分泌物其他.TabIndex = 107;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.uc血清谷丙转氨酶);
            this.flowLayoutPanel10.Controls.Add(this.uc血清谷草转氨酶);
            this.flowLayoutPanel10.Controls.Add(this.uc白蛋白);
            this.flowLayoutPanel10.Controls.Add(this.uc总胆红素);
            this.flowLayoutPanel10.Controls.Add(this.uc结合胆红素);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(180, 710);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(548, 51);
            this.flowLayoutPanel10.TabIndex = 96;
            // 
            // uc血清谷丙转氨酶
            // 
            this.uc血清谷丙转氨酶.Lbl1Size = new System.Drawing.Size(90, 18);
            this.uc血清谷丙转氨酶.Lbl1Text = "血清谷丙转氨酶";
            this.uc血清谷丙转氨酶.Lbl2Size = new System.Drawing.Size(30, 18);
            this.uc血清谷丙转氨酶.Lbl2Text = "U/L";
            this.uc血清谷丙转氨酶.Location = new System.Drawing.Point(0, 0);
            this.uc血清谷丙转氨酶.Margin = new System.Windows.Forms.Padding(0);
            this.uc血清谷丙转氨酶.Name = "uc血清谷丙转氨酶";
            this.uc血清谷丙转氨酶.Size = new System.Drawing.Size(172, 22);
            this.uc血清谷丙转氨酶.TabIndex = 97;
            this.uc血清谷丙转氨酶.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc血清谷草转氨酶
            // 
            this.uc血清谷草转氨酶.Lbl1Size = new System.Drawing.Size(90, 18);
            this.uc血清谷草转氨酶.Lbl1Text = "血清谷草转氨酶";
            this.uc血清谷草转氨酶.Lbl2Size = new System.Drawing.Size(30, 18);
            this.uc血清谷草转氨酶.Lbl2Text = "U/L";
            this.uc血清谷草转氨酶.Location = new System.Drawing.Point(172, 0);
            this.uc血清谷草转氨酶.Margin = new System.Windows.Forms.Padding(0);
            this.uc血清谷草转氨酶.Name = "uc血清谷草转氨酶";
            this.uc血清谷草转氨酶.Size = new System.Drawing.Size(177, 22);
            this.uc血清谷草转氨酶.TabIndex = 98;
            this.uc血清谷草转氨酶.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc白蛋白
            // 
            this.uc白蛋白.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc白蛋白.Lbl1Text = "白蛋白";
            this.uc白蛋白.Lbl2Size = new System.Drawing.Size(40, 18);
            this.uc白蛋白.Lbl2Text = "g/L";
            this.uc白蛋白.Location = new System.Drawing.Point(349, 0);
            this.uc白蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.uc白蛋白.Name = "uc白蛋白";
            this.uc白蛋白.Size = new System.Drawing.Size(151, 22);
            this.uc白蛋白.TabIndex = 99;
            this.uc白蛋白.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc总胆红素
            // 
            this.uc总胆红素.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc总胆红素.Lbl1Text = "总胆红素";
            this.uc总胆红素.Lbl2Size = new System.Drawing.Size(40, 18);
            this.uc总胆红素.Lbl2Text = "μmol/L";
            this.uc总胆红素.Location = new System.Drawing.Point(0, 22);
            this.uc总胆红素.Margin = new System.Windows.Forms.Padding(0);
            this.uc总胆红素.Name = "uc总胆红素";
            this.uc总胆红素.Size = new System.Drawing.Size(172, 22);
            this.uc总胆红素.TabIndex = 100;
            this.uc总胆红素.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc结合胆红素
            // 
            this.uc结合胆红素.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc结合胆红素.Lbl1Text = "结合胆红素";
            this.uc结合胆红素.Lbl2Size = new System.Drawing.Size(56, 18);
            this.uc结合胆红素.Lbl2Text = "μmol/L ";
            this.uc结合胆红素.Location = new System.Drawing.Point(172, 22);
            this.uc结合胆红素.Margin = new System.Windows.Forms.Padding(0);
            this.uc结合胆红素.Name = "uc结合胆红素";
            this.uc结合胆红素.Size = new System.Drawing.Size(177, 22);
            this.uc结合胆红素.TabIndex = 101;
            this.uc结合胆红素.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc血糖
            // 
            this.uc血糖.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc血糖.Lbl1Text = "mmol/L";
            this.uc血糖.Location = new System.Drawing.Point(180, 686);
            this.uc血糖.Name = "uc血糖";
            this.uc血糖.Size = new System.Drawing.Size(548, 20);
            this.uc血糖.TabIndex = 95;
            this.uc血糖.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Controls.Add(this.uc尿蛋白);
            this.flowLayoutPanel9.Controls.Add(this.uc尿糖);
            this.flowLayoutPanel9.Controls.Add(this.uc尿酮体);
            this.flowLayoutPanel9.Controls.Add(this.uc尿潜血);
            this.flowLayoutPanel9.Controls.Add(this.uc尿常规其他);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(180, 578);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(548, 56);
            this.flowLayoutPanel9.TabIndex = 87;
            // 
            // uc尿蛋白
            // 
            this.uc尿蛋白.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc尿蛋白.Lbl1Text = "尿蛋白";
            this.uc尿蛋白.Location = new System.Drawing.Point(0, 0);
            this.uc尿蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.uc尿蛋白.Name = "uc尿蛋白";
            this.uc尿蛋白.Size = new System.Drawing.Size(124, 22);
            this.uc尿蛋白.TabIndex = 88;
            this.uc尿蛋白.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc尿糖
            // 
            this.uc尿糖.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc尿糖.Lbl1Text = "尿糖";
            this.uc尿糖.Location = new System.Drawing.Point(124, 0);
            this.uc尿糖.Margin = new System.Windows.Forms.Padding(0);
            this.uc尿糖.Name = "uc尿糖";
            this.uc尿糖.Size = new System.Drawing.Size(127, 22);
            this.uc尿糖.TabIndex = 89;
            this.uc尿糖.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc尿酮体
            // 
            this.uc尿酮体.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc尿酮体.Lbl1Text = "尿酮体";
            this.uc尿酮体.Location = new System.Drawing.Point(251, 0);
            this.uc尿酮体.Margin = new System.Windows.Forms.Padding(0);
            this.uc尿酮体.Name = "uc尿酮体";
            this.uc尿酮体.Size = new System.Drawing.Size(122, 22);
            this.uc尿酮体.TabIndex = 90;
            this.uc尿酮体.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc尿潜血
            // 
            this.uc尿潜血.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc尿潜血.Lbl1Text = "尿潜血";
            this.uc尿潜血.Location = new System.Drawing.Point(373, 0);
            this.uc尿潜血.Margin = new System.Windows.Forms.Padding(0);
            this.uc尿潜血.Name = "uc尿潜血";
            this.uc尿潜血.Size = new System.Drawing.Size(128, 22);
            this.uc尿潜血.TabIndex = 91;
            this.uc尿潜血.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc尿常规其他
            // 
            this.uc尿常规其他.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc尿常规其他.Lbl1Text = "其他";
            this.uc尿常规其他.Location = new System.Drawing.Point(0, 22);
            this.uc尿常规其他.Margin = new System.Windows.Forms.Padding(0);
            this.uc尿常规其他.Name = "uc尿常规其他";
            this.uc尿常规其他.Size = new System.Drawing.Size(181, 22);
            this.uc尿常规其他.TabIndex = 92;
            this.uc尿常规其他.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.uc血红蛋白值);
            this.flowLayoutPanel8.Controls.Add(this.uc白细胞计数值);
            this.flowLayoutPanel8.Controls.Add(this.uc血小板计数值);
            this.flowLayoutPanel8.Controls.Add(this.uc血常规其他);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(180, 518);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(548, 56);
            this.flowLayoutPanel8.TabIndex = 82;
            // 
            // uc血红蛋白值
            // 
            this.uc血红蛋白值.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc血红蛋白值.Lbl1Text = "血红蛋白值";
            this.uc血红蛋白值.Lbl2Size = new System.Drawing.Size(20, 18);
            this.uc血红蛋白值.Lbl2Text = "g/L";
            this.uc血红蛋白值.Location = new System.Drawing.Point(0, 0);
            this.uc血红蛋白值.Margin = new System.Windows.Forms.Padding(0);
            this.uc血红蛋白值.Name = "uc血红蛋白值";
            this.uc血红蛋白值.Size = new System.Drawing.Size(217, 22);
            this.uc血红蛋白值.TabIndex = 83;
            this.uc血红蛋白值.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc白细胞计数值
            // 
            this.uc白细胞计数值.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc白细胞计数值.Lbl1Text = "白细胞计数值";
            this.uc白细胞计数值.Lbl2Size = new System.Drawing.Size(60, 18);
            this.uc白细胞计数值.Lbl2Text = "x10^9 /L";
            this.uc白细胞计数值.Location = new System.Drawing.Point(217, 0);
            this.uc白细胞计数值.Margin = new System.Windows.Forms.Padding(0);
            this.uc白细胞计数值.Name = "uc白细胞计数值";
            this.uc白细胞计数值.Size = new System.Drawing.Size(256, 22);
            this.uc白细胞计数值.TabIndex = 84;
            this.uc白细胞计数值.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc血小板计数值
            // 
            this.uc血小板计数值.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc血小板计数值.Lbl1Text = "血小板计数值";
            this.uc血小板计数值.Lbl2Size = new System.Drawing.Size(60, 18);
            this.uc血小板计数值.Lbl2Text = "x10^9 /L";
            this.uc血小板计数值.Location = new System.Drawing.Point(0, 22);
            this.uc血小板计数值.Margin = new System.Windows.Forms.Padding(0);
            this.uc血小板计数值.Name = "uc血小板计数值";
            this.uc血小板计数值.Size = new System.Drawing.Size(253, 22);
            this.uc血小板计数值.TabIndex = 85;
            this.uc血小板计数值.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc血常规其他
            // 
            this.uc血常规其他.Lbl1Size = new System.Drawing.Size(40, 18);
            this.uc血常规其他.Lbl1Text = "其他";
            this.uc血常规其他.Location = new System.Drawing.Point(253, 22);
            this.uc血常规其他.Margin = new System.Windows.Forms.Padding(0);
            this.uc血常规其他.Name = "uc血常规其他";
            this.uc血常规其他.Size = new System.Drawing.Size(181, 22);
            this.uc血常规其他.TabIndex = 86;
            this.uc血常规其他.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.uc血清肌酐);
            this.flowLayoutPanel11.Controls.Add(this.uc血尿素氮);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(180, 765);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(548, 24);
            this.flowLayoutPanel11.TabIndex = 102;
            // 
            // uc血清肌酐
            // 
            this.uc血清肌酐.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc血清肌酐.Lbl1Text = "血清肌酐";
            this.uc血清肌酐.Lbl2Size = new System.Drawing.Size(40, 18);
            this.uc血清肌酐.Lbl2Text = "μmol/L";
            this.uc血清肌酐.Location = new System.Drawing.Point(0, 0);
            this.uc血清肌酐.Margin = new System.Windows.Forms.Padding(0);
            this.uc血清肌酐.Name = "uc血清肌酐";
            this.uc血清肌酐.Size = new System.Drawing.Size(218, 22);
            this.uc血清肌酐.TabIndex = 103;
            this.uc血清肌酐.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc血尿素氮
            // 
            this.uc血尿素氮.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc血尿素氮.Lbl1Text = "血尿素氮";
            this.uc血尿素氮.Lbl2Size = new System.Drawing.Size(56, 18);
            this.uc血尿素氮.Lbl2Text = "μmol/L ";
            this.uc血尿素氮.Location = new System.Drawing.Point(218, 0);
            this.uc血尿素氮.Margin = new System.Windows.Forms.Padding(0);
            this.uc血尿素氮.Name = "uc血尿素氮";
            this.uc血尿素氮.Size = new System.Drawing.Size(259, 22);
            this.uc血尿素氮.TabIndex = 104;
            this.uc血尿素氮.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // textEdit附件
            // 
            this.textEdit附件.Enabled = false;
            this.textEdit附件.Location = new System.Drawing.Point(235, 494);
            this.textEdit附件.Name = "textEdit附件";
            this.textEdit附件.Size = new System.Drawing.Size(493, 20);
            this.textEdit附件.StyleController = this.layoutControl1;
            this.textEdit附件.TabIndex = 81;
            // 
            // textEdit子宫
            // 
            this.textEdit子宫.Enabled = false;
            this.textEdit子宫.Location = new System.Drawing.Point(581, 470);
            this.textEdit子宫.Name = "textEdit子宫";
            this.textEdit子宫.Size = new System.Drawing.Size(147, 20);
            this.textEdit子宫.StyleController = this.layoutControl1;
            this.textEdit子宫.TabIndex = 79;
            // 
            // textEdit宫颈
            // 
            this.textEdit宫颈.Enabled = false;
            this.textEdit宫颈.Location = new System.Drawing.Point(235, 470);
            this.textEdit宫颈.Name = "textEdit宫颈";
            this.textEdit宫颈.Size = new System.Drawing.Size(192, 20);
            this.textEdit宫颈.StyleController = this.layoutControl1;
            this.textEdit宫颈.TabIndex = 77;
            // 
            // textEdit外阴
            // 
            this.textEdit外阴.Enabled = false;
            this.textEdit外阴.Location = new System.Drawing.Point(235, 446);
            this.textEdit外阴.Name = "textEdit外阴";
            this.textEdit外阴.Size = new System.Drawing.Size(192, 20);
            this.textEdit外阴.StyleController = this.layoutControl1;
            this.textEdit外阴.TabIndex = 73;
            // 
            // textEdit阴道
            // 
            this.textEdit阴道.Enabled = false;
            this.textEdit阴道.Location = new System.Drawing.Point(581, 446);
            this.textEdit阴道.Name = "textEdit阴道";
            this.textEdit阴道.Size = new System.Drawing.Size(147, 20);
            this.textEdit阴道.StyleController = this.layoutControl1;
            this.textEdit阴道.TabIndex = 75;
            // 
            // textEdit肺部
            // 
            this.textEdit肺部.Enabled = false;
            this.textEdit肺部.Location = new System.Drawing.Point(581, 420);
            this.textEdit肺部.Name = "textEdit肺部";
            this.textEdit肺部.Size = new System.Drawing.Size(147, 20);
            this.textEdit肺部.StyleController = this.layoutControl1;
            this.textEdit肺部.TabIndex = 66;
            // 
            // textEdit心脏
            // 
            this.textEdit心脏.Enabled = false;
            this.textEdit心脏.Location = new System.Drawing.Point(235, 420);
            this.textEdit心脏.Name = "textEdit心脏";
            this.textEdit心脏.Size = new System.Drawing.Size(192, 20);
            this.textEdit心脏.StyleController = this.layoutControl1;
            this.textEdit心脏.TabIndex = 69;
            // 
            // uc血压
            // 
            this.uc血压.Lbl1Size = new System.Drawing.Size(18, 14);
            this.uc血压.Lbl1Text = "/";
            this.uc血压.Lbl2Size = new System.Drawing.Size(40, 14);
            this.uc血压.Lbl2Text = "mmHg";
            this.uc血压.Location = new System.Drawing.Point(389, 396);
            this.uc血压.Name = "uc血压";
            this.uc血压.Size = new System.Drawing.Size(339, 20);
            this.uc血压.TabIndex = 67;
            this.uc血压.Txt1EditValue = null;
            this.uc血压.Txt1Size = new System.Drawing.Size(100, 20);
            this.uc血压.Txt2EditValue = null;
            this.uc血压.Txt2Size = new System.Drawing.Size(100, 20);
            // 
            // textEdit体重指数
            // 
            this.textEdit体重指数.Location = new System.Drawing.Point(88, 396);
            this.textEdit体重指数.Name = "textEdit体重指数";
            this.textEdit体重指数.Size = new System.Drawing.Size(212, 20);
            this.textEdit体重指数.StyleController = this.layoutControl1;
            this.textEdit体重指数.TabIndex = 66;
            // 
            // uc体重
            // 
            this.uc体重.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc体重.Lbl1Text = "kg";
            this.uc体重.Location = new System.Drawing.Point(389, 372);
            this.uc体重.Name = "uc体重";
            this.uc体重.Size = new System.Drawing.Size(339, 20);
            this.uc体重.TabIndex = 65;
            this.uc体重.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // uc身高
            // 
            this.uc身高.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc身高.Lbl1Text = "cm";
            this.uc身高.Location = new System.Drawing.Point(88, 372);
            this.uc身高.Name = "uc身高";
            this.uc身高.Size = new System.Drawing.Size(212, 20);
            this.uc身高.TabIndex = 64;
            this.uc身高.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.uc流产);
            this.flowLayoutPanel7.Controls.Add(this.uc死胎);
            this.flowLayoutPanel7.Controls.Add(this.uc死产);
            this.flowLayoutPanel7.Controls.Add(this.uc新生儿死亡);
            this.flowLayoutPanel7.Controls.Add(this.uc出生缺陷儿);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(88, 344);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(640, 24);
            this.flowLayoutPanel7.TabIndex = 58;
            // 
            // uc流产
            // 
            this.uc流产.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc流产.Lbl1Text = "流产";
            this.uc流产.Lbl2Size = new System.Drawing.Size(15, 18);
            this.uc流产.Lbl2Text = "次";
            this.uc流产.Location = new System.Drawing.Point(0, 0);
            this.uc流产.Margin = new System.Windows.Forms.Padding(0);
            this.uc流产.Name = "uc流产";
            this.uc流产.Size = new System.Drawing.Size(120, 22);
            this.uc流产.TabIndex = 59;
            this.uc流产.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc死胎
            // 
            this.uc死胎.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc死胎.Lbl1Text = "死胎";
            this.uc死胎.Lbl2Size = new System.Drawing.Size(15, 18);
            this.uc死胎.Lbl2Text = "次";
            this.uc死胎.Location = new System.Drawing.Point(120, 0);
            this.uc死胎.Margin = new System.Windows.Forms.Padding(0);
            this.uc死胎.Name = "uc死胎";
            this.uc死胎.Size = new System.Drawing.Size(109, 22);
            this.uc死胎.TabIndex = 60;
            this.uc死胎.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc死产
            // 
            this.uc死产.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc死产.Lbl1Text = "死产";
            this.uc死产.Lbl2Size = new System.Drawing.Size(15, 18);
            this.uc死产.Lbl2Text = "次";
            this.uc死产.Location = new System.Drawing.Point(229, 0);
            this.uc死产.Margin = new System.Windows.Forms.Padding(0);
            this.uc死产.Name = "uc死产";
            this.uc死产.Size = new System.Drawing.Size(120, 22);
            this.uc死产.TabIndex = 61;
            this.uc死产.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc新生儿死亡
            // 
            this.uc新生儿死亡.Lbl1Size = new System.Drawing.Size(60, 18);
            this.uc新生儿死亡.Lbl1Text = "新生儿死亡";
            this.uc新生儿死亡.Lbl2Size = new System.Drawing.Size(15, 18);
            this.uc新生儿死亡.Lbl2Text = "次";
            this.uc新生儿死亡.Location = new System.Drawing.Point(349, 0);
            this.uc新生儿死亡.Margin = new System.Windows.Forms.Padding(0);
            this.uc新生儿死亡.Name = "uc新生儿死亡";
            this.uc新生儿死亡.Size = new System.Drawing.Size(143, 22);
            this.uc新生儿死亡.TabIndex = 62;
            this.uc新生儿死亡.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc出生缺陷儿
            // 
            this.uc出生缺陷儿.Lbl1Size = new System.Drawing.Size(60, 18);
            this.uc出生缺陷儿.Lbl1Text = "出生缺陷儿";
            this.uc出生缺陷儿.Lbl2Size = new System.Drawing.Size(15, 18);
            this.uc出生缺陷儿.Lbl2Text = "个";
            this.uc出生缺陷儿.Location = new System.Drawing.Point(492, 0);
            this.uc出生缺陷儿.Margin = new System.Windows.Forms.Padding(0);
            this.uc出生缺陷儿.Name = "uc出生缺陷儿";
            this.uc出生缺陷儿.Size = new System.Drawing.Size(145, 22);
            this.uc出生缺陷儿.TabIndex = 63;
            this.uc出生缺陷儿.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // textEdit妇科手术史
            // 
            this.textEdit妇科手术史.Enabled = false;
            this.textEdit妇科手术史.Location = new System.Drawing.Point(203, 320);
            this.textEdit妇科手术史.Name = "textEdit妇科手术史";
            this.textEdit妇科手术史.Size = new System.Drawing.Size(525, 20);
            this.textEdit妇科手术史.StyleController = this.layoutControl1;
            this.textEdit妇科手术史.TabIndex = 57;
            // 
            // cbo妇科手术史
            // 
            this.cbo妇科手术史.Location = new System.Drawing.Point(88, 320);
            this.cbo妇科手术史.Name = "cbo妇科手术史";
            this.cbo妇科手术史.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo妇科手术史.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo妇科手术史.Size = new System.Drawing.Size(111, 20);
            this.cbo妇科手术史.StyleController = this.layoutControl1;
            this.cbo妇科手术史.TabIndex = 56;
            this.cbo妇科手术史.SelectedIndexChanged += new System.EventHandler(this.cbo妇科手术史_SelectedIndexChanged);
            // 
            // flow个人史
            // 
            this.flow个人史.Controls.Add(this.chk个人史0);
            this.flow个人史.Controls.Add(this.chk个人史1);
            this.flow个人史.Controls.Add(this.chk个人史2);
            this.flow个人史.Controls.Add(this.chk个人史3);
            this.flow个人史.Controls.Add(this.chk个人史4);
            this.flow个人史.Controls.Add(this.chk个人史5);
            this.flow个人史.Controls.Add(this.chk个人史6);
            this.flow个人史.Controls.Add(this.txt个人史其他);
            this.flow个人史.Location = new System.Drawing.Point(88, 296);
            this.flow个人史.Name = "flow个人史";
            this.flow个人史.Size = new System.Drawing.Size(640, 20);
            this.flow个人史.TabIndex = 47;
            // 
            // chk个人史0
            // 
            this.chk个人史0.EditValue = true;
            this.chk个人史0.Location = new System.Drawing.Point(0, 0);
            this.chk个人史0.Margin = new System.Windows.Forms.Padding(0);
            this.chk个人史0.Name = "chk个人史0";
            this.chk个人史0.Properties.Caption = "无";
            this.chk个人史0.Size = new System.Drawing.Size(41, 19);
            this.chk个人史0.TabIndex = 48;
            this.chk个人史0.Tag = "0";
            this.chk个人史0.CheckedChanged += new System.EventHandler(this.chk个人史0_CheckedChanged);
            // 
            // chk个人史1
            // 
            this.chk个人史1.Enabled = false;
            this.chk个人史1.Location = new System.Drawing.Point(41, 0);
            this.chk个人史1.Margin = new System.Windows.Forms.Padding(0);
            this.chk个人史1.Name = "chk个人史1";
            this.chk个人史1.Properties.Caption = "吸烟";
            this.chk个人史1.Size = new System.Drawing.Size(64, 19);
            this.chk个人史1.TabIndex = 49;
            this.chk个人史1.Tag = "1";
            // 
            // chk个人史2
            // 
            this.chk个人史2.Enabled = false;
            this.chk个人史2.Location = new System.Drawing.Point(105, 0);
            this.chk个人史2.Margin = new System.Windows.Forms.Padding(0);
            this.chk个人史2.Name = "chk个人史2";
            this.chk个人史2.Properties.Caption = "饮酒";
            this.chk个人史2.Size = new System.Drawing.Size(59, 19);
            this.chk个人史2.TabIndex = 50;
            this.chk个人史2.Tag = "2";
            // 
            // chk个人史3
            // 
            this.chk个人史3.Enabled = false;
            this.chk个人史3.Location = new System.Drawing.Point(164, 0);
            this.chk个人史3.Margin = new System.Windows.Forms.Padding(0);
            this.chk个人史3.Name = "chk个人史3";
            this.chk个人史3.Properties.Caption = "服用药物";
            this.chk个人史3.Size = new System.Drawing.Size(79, 19);
            this.chk个人史3.TabIndex = 51;
            this.chk个人史3.Tag = "3";
            // 
            // chk个人史4
            // 
            this.chk个人史4.Enabled = false;
            this.chk个人史4.Location = new System.Drawing.Point(243, 0);
            this.chk个人史4.Margin = new System.Windows.Forms.Padding(0);
            this.chk个人史4.Name = "chk个人史4";
            this.chk个人史4.Properties.Caption = "接触有毒有害物质";
            this.chk个人史4.Size = new System.Drawing.Size(121, 19);
            this.chk个人史4.TabIndex = 52;
            this.chk个人史4.Tag = "4";
            // 
            // chk个人史5
            // 
            this.chk个人史5.Enabled = false;
            this.chk个人史5.Location = new System.Drawing.Point(364, 0);
            this.chk个人史5.Margin = new System.Windows.Forms.Padding(0);
            this.chk个人史5.Name = "chk个人史5";
            this.chk个人史5.Properties.Caption = "接触放射线";
            this.chk个人史5.Size = new System.Drawing.Size(84, 19);
            this.chk个人史5.TabIndex = 53;
            this.chk个人史5.Tag = "5";
            // 
            // chk个人史6
            // 
            this.chk个人史6.Enabled = false;
            this.chk个人史6.Location = new System.Drawing.Point(448, 0);
            this.chk个人史6.Margin = new System.Windows.Forms.Padding(0);
            this.chk个人史6.Name = "chk个人史6";
            this.chk个人史6.Properties.Caption = "其他";
            this.chk个人史6.Size = new System.Drawing.Size(54, 19);
            this.chk个人史6.TabIndex = 54;
            this.chk个人史6.Tag = "6";
            this.chk个人史6.CheckedChanged += new System.EventHandler(this.chk个人史6_CheckedChanged);
            // 
            // txt个人史其他
            // 
            this.txt个人史其他.Enabled = false;
            this.txt个人史其他.Location = new System.Drawing.Point(502, 0);
            this.txt个人史其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt个人史其他.Name = "txt个人史其他";
            this.txt个人史其他.Size = new System.Drawing.Size(79, 20);
            this.txt个人史其他.TabIndex = 55;
            // 
            // flow家族史
            // 
            this.flow家族史.Controls.Add(this.chk家族史0);
            this.flow家族史.Controls.Add(this.chk家族史1);
            this.flow家族史.Controls.Add(this.chk家族史2);
            this.flow家族史.Controls.Add(this.chk家族史3);
            this.flow家族史.Controls.Add(this.chk家族史4);
            this.flow家族史.Controls.Add(this.chk家族史5);
            this.flow家族史.Controls.Add(this.chk家族史6);
            this.flow家族史.Controls.Add(this.textEdit家族史其他);
            this.flow家族史.Location = new System.Drawing.Point(88, 272);
            this.flow家族史.Name = "flow家族史";
            this.flow家族史.Size = new System.Drawing.Size(640, 20);
            this.flow家族史.TabIndex = 38;
            // 
            // chk家族史0
            // 
            this.chk家族史0.EditValue = true;
            this.chk家族史0.Location = new System.Drawing.Point(0, 0);
            this.chk家族史0.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史0.Name = "chk家族史0";
            this.chk家族史0.Properties.Caption = "无";
            this.chk家族史0.Size = new System.Drawing.Size(41, 19);
            this.chk家族史0.TabIndex = 39;
            this.chk家族史0.Tag = "0";
            this.chk家族史0.CheckedChanged += new System.EventHandler(this.chk家族史0_CheckedChanged);
            // 
            // chk家族史1
            // 
            this.chk家族史1.Enabled = false;
            this.chk家族史1.Location = new System.Drawing.Point(41, 0);
            this.chk家族史1.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史1.Name = "chk家族史1";
            this.chk家族史1.Properties.Caption = "高血压";
            this.chk家族史1.Size = new System.Drawing.Size(64, 19);
            this.chk家族史1.TabIndex = 40;
            this.chk家族史1.Tag = "1";
            // 
            // chk家族史2
            // 
            this.chk家族史2.Enabled = false;
            this.chk家族史2.Location = new System.Drawing.Point(105, 0);
            this.chk家族史2.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史2.Name = "chk家族史2";
            this.chk家族史2.Properties.Caption = "遗传病";
            this.chk家族史2.Size = new System.Drawing.Size(59, 19);
            this.chk家族史2.TabIndex = 41;
            this.chk家族史2.Tag = "2";
            // 
            // chk家族史3
            // 
            this.chk家族史3.Enabled = false;
            this.chk家族史3.Location = new System.Drawing.Point(164, 0);
            this.chk家族史3.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史3.Name = "chk家族史3";
            this.chk家族史3.Properties.Caption = "糖尿病";
            this.chk家族史3.Size = new System.Drawing.Size(65, 19);
            this.chk家族史3.TabIndex = 42;
            this.chk家族史3.Tag = "3";
            // 
            // chk家族史4
            // 
            this.chk家族史4.Enabled = false;
            this.chk家族史4.Location = new System.Drawing.Point(229, 0);
            this.chk家族史4.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史4.Name = "chk家族史4";
            this.chk家族史4.Properties.Caption = "传染病";
            this.chk家族史4.Size = new System.Drawing.Size(74, 19);
            this.chk家族史4.TabIndex = 43;
            this.chk家族史4.Tag = "4";
            // 
            // chk家族史5
            // 
            this.chk家族史5.Enabled = false;
            this.chk家族史5.Location = new System.Drawing.Point(303, 0);
            this.chk家族史5.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史5.Name = "chk家族史5";
            this.chk家族史5.Properties.Caption = "精神病史";
            this.chk家族史5.Size = new System.Drawing.Size(84, 19);
            this.chk家族史5.TabIndex = 44;
            this.chk家族史5.Tag = "5";
            // 
            // chk家族史6
            // 
            this.chk家族史6.Enabled = false;
            this.chk家族史6.Location = new System.Drawing.Point(387, 0);
            this.chk家族史6.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史6.Name = "chk家族史6";
            this.chk家族史6.Properties.Caption = "其他";
            this.chk家族史6.Size = new System.Drawing.Size(54, 19);
            this.chk家族史6.TabIndex = 45;
            this.chk家族史6.Tag = "99";
            this.chk家族史6.CheckedChanged += new System.EventHandler(this.chk家族史6_CheckedChanged);
            // 
            // textEdit家族史其他
            // 
            this.textEdit家族史其他.Enabled = false;
            this.textEdit家族史其他.Location = new System.Drawing.Point(441, 0);
            this.textEdit家族史其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit家族史其他.Name = "textEdit家族史其他";
            this.textEdit家族史其他.Size = new System.Drawing.Size(122, 20);
            this.textEdit家族史其他.TabIndex = 46;
            // 
            // flow既往病史
            // 
            this.flow既往病史.Controls.Add(this.chk既往病史有无);
            this.flow既往病史.Controls.Add(this.checkEdit3);
            this.flow既往病史.Controls.Add(this.checkEdit4);
            this.flow既往病史.Controls.Add(this.checkEdit5);
            this.flow既往病史.Controls.Add(this.checkEdit6);
            this.flow既往病史.Controls.Add(this.checkEdit7);
            this.flow既往病史.Controls.Add(this.checkEdit8);
            this.flow既往病史.Controls.Add(this.checkEdit9);
            this.flow既往病史.Controls.Add(this.checkEdit10);
            this.flow既往病史.Controls.Add(this.checkEdit11);
            this.flow既往病史.Controls.Add(this.checkEdit12);
            this.flow既往病史.Controls.Add(this.checkEdit13);
            this.flow既往病史.Controls.Add(this.checkEdit14);
            this.flow既往病史.Controls.Add(this.checkEdit15);
            this.flow既往病史.Controls.Add(this.textEdit既往病史其他);
            this.flow既往病史.Location = new System.Drawing.Point(88, 224);
            this.flow既往病史.Name = "flow既往病史";
            this.flow既往病史.Size = new System.Drawing.Size(640, 44);
            this.flow既往病史.TabIndex = 22;
            // 
            // chk既往病史有无
            // 
            this.chk既往病史有无.EditValue = true;
            this.chk既往病史有无.Location = new System.Drawing.Point(0, 0);
            this.chk既往病史有无.Margin = new System.Windows.Forms.Padding(0);
            this.chk既往病史有无.Name = "chk既往病史有无";
            this.chk既往病史有无.Properties.Caption = "无";
            this.chk既往病史有无.Size = new System.Drawing.Size(41, 19);
            this.chk既往病史有无.TabIndex = 23;
            this.chk既往病史有无.Tag = "0";
            this.chk既往病史有无.CheckedChanged += new System.EventHandler(this.chk既往病史有无_CheckedChanged);
            // 
            // checkEdit3
            // 
            this.checkEdit3.Enabled = false;
            this.checkEdit3.Location = new System.Drawing.Point(41, 0);
            this.checkEdit3.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "心脏病";
            this.checkEdit3.Size = new System.Drawing.Size(64, 19);
            this.checkEdit3.TabIndex = 24;
            this.checkEdit3.Tag = "1";
            // 
            // checkEdit4
            // 
            this.checkEdit4.Enabled = false;
            this.checkEdit4.Location = new System.Drawing.Point(105, 0);
            this.checkEdit4.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "肝炎";
            this.checkEdit4.Size = new System.Drawing.Size(59, 19);
            this.checkEdit4.TabIndex = 25;
            this.checkEdit4.Tag = "2";
            // 
            // checkEdit5
            // 
            this.checkEdit5.Enabled = false;
            this.checkEdit5.Location = new System.Drawing.Point(164, 0);
            this.checkEdit5.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "肾脏病";
            this.checkEdit5.Size = new System.Drawing.Size(58, 19);
            this.checkEdit5.TabIndex = 26;
            this.checkEdit5.Tag = "3";
            // 
            // checkEdit6
            // 
            this.checkEdit6.Enabled = false;
            this.checkEdit6.Location = new System.Drawing.Point(222, 0);
            this.checkEdit6.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "慢性高血压";
            this.checkEdit6.Size = new System.Drawing.Size(90, 19);
            this.checkEdit6.TabIndex = 27;
            this.checkEdit6.Tag = "4";
            // 
            // checkEdit7
            // 
            this.checkEdit7.Enabled = false;
            this.checkEdit7.Location = new System.Drawing.Point(312, 0);
            this.checkEdit7.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "血液病";
            this.checkEdit7.Size = new System.Drawing.Size(65, 19);
            this.checkEdit7.TabIndex = 28;
            this.checkEdit7.Tag = "5";
            // 
            // checkEdit8
            // 
            this.checkEdit8.Enabled = false;
            this.checkEdit8.Location = new System.Drawing.Point(377, 0);
            this.checkEdit8.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "糖尿病";
            this.checkEdit8.Size = new System.Drawing.Size(67, 19);
            this.checkEdit8.TabIndex = 29;
            this.checkEdit8.Tag = "6";
            // 
            // checkEdit9
            // 
            this.checkEdit9.Enabled = false;
            this.checkEdit9.Location = new System.Drawing.Point(444, 0);
            this.checkEdit9.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "甲状腺疾病\r\n";
            this.checkEdit9.Size = new System.Drawing.Size(100, 19);
            this.checkEdit9.TabIndex = 30;
            this.checkEdit9.Tag = "7";
            // 
            // checkEdit10
            // 
            this.checkEdit10.Enabled = false;
            this.checkEdit10.Location = new System.Drawing.Point(544, 0);
            this.checkEdit10.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "妇科疾病";
            this.checkEdit10.Size = new System.Drawing.Size(75, 19);
            this.checkEdit10.TabIndex = 31;
            this.checkEdit10.Tag = "8";
            // 
            // checkEdit11
            // 
            this.checkEdit11.Enabled = false;
            this.checkEdit11.Location = new System.Drawing.Point(0, 19);
            this.checkEdit11.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "精神病";
            this.checkEdit11.Size = new System.Drawing.Size(70, 19);
            this.checkEdit11.TabIndex = 32;
            this.checkEdit11.Tag = "9";
            // 
            // checkEdit12
            // 
            this.checkEdit12.Enabled = false;
            this.checkEdit12.Location = new System.Drawing.Point(70, 19);
            this.checkEdit12.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "结核病";
            this.checkEdit12.Size = new System.Drawing.Size(65, 19);
            this.checkEdit12.TabIndex = 33;
            this.checkEdit12.Tag = "10";
            // 
            // checkEdit13
            // 
            this.checkEdit13.Enabled = false;
            this.checkEdit13.Location = new System.Drawing.Point(135, 19);
            this.checkEdit13.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "外伤及手术";
            this.checkEdit13.Size = new System.Drawing.Size(85, 19);
            this.checkEdit13.TabIndex = 34;
            this.checkEdit13.Tag = "11";
            // 
            // checkEdit14
            // 
            this.checkEdit14.Enabled = false;
            this.checkEdit14.Location = new System.Drawing.Point(220, 19);
            this.checkEdit14.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "药物过敏史";
            this.checkEdit14.Size = new System.Drawing.Size(88, 19);
            this.checkEdit14.TabIndex = 35;
            this.checkEdit14.Tag = "12";
            // 
            // checkEdit15
            // 
            this.checkEdit15.Enabled = false;
            this.checkEdit15.Location = new System.Drawing.Point(308, 19);
            this.checkEdit15.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Caption = "其他";
            this.checkEdit15.Size = new System.Drawing.Size(54, 19);
            this.checkEdit15.TabIndex = 36;
            this.checkEdit15.Tag = "99";
            // 
            // textEdit既往病史其他
            // 
            this.textEdit既往病史其他.Enabled = false;
            this.textEdit既往病史其他.Location = new System.Drawing.Point(362, 19);
            this.textEdit既往病史其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit既往病史其他.Name = "textEdit既往病史其他";
            this.textEdit既往病史其他.Size = new System.Drawing.Size(122, 20);
            this.textEdit既往病史其他.TabIndex = 37;
            // 
            // dte预产期
            // 
            this.dte预产期.EditValue = null;
            this.dte预产期.Location = new System.Drawing.Point(363, 200);
            this.dte预产期.Name = "dte预产期";
            this.dte预产期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte预产期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte预产期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte预产期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte预产期.Size = new System.Drawing.Size(111, 20);
            this.dte预产期.StyleController = this.layoutControl1;
            this.dte预产期.TabIndex = 21;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.chk末次月经);
            this.flowLayoutPanel3.Controls.Add(this.dte末次月经);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(88, 200);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(186, 20);
            this.flowLayoutPanel3.TabIndex = 18;
            // 
            // chk末次月经
            // 
            this.chk末次月经.Location = new System.Drawing.Point(0, 0);
            this.chk末次月经.Margin = new System.Windows.Forms.Padding(0);
            this.chk末次月经.Name = "chk末次月经";
            this.chk末次月经.Properties.Caption = "不详";
            this.chk末次月经.Size = new System.Drawing.Size(54, 19);
            this.chk末次月经.TabIndex = 19;
            this.chk末次月经.CheckedChanged += new System.EventHandler(this.chk末次月经_CheckedChanged);
            // 
            // dte末次月经
            // 
            this.dte末次月经.EditValue = null;
            this.dte末次月经.Location = new System.Drawing.Point(54, 0);
            this.dte末次月经.Margin = new System.Windows.Forms.Padding(0);
            this.dte末次月经.Name = "dte末次月经";
            this.dte末次月经.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte末次月经.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte末次月经.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte末次月经.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTimeAdvancingCaret;
            this.dte末次月经.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte末次月经.Size = new System.Drawing.Size(125, 20);
            this.dte末次月经.TabIndex = 20;
            this.dte末次月经.EditValueChanged += new System.EventHandler(this.dte末次月经_EditValueChanged);
            this.dte末次月经.Leave += new System.EventHandler(this.dte末次月经_Leave);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.labelControl1);
            this.flowLayoutPanel2.Controls.Add(this.uc产次);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(363, 176);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(365, 20);
            this.flowLayoutPanel2.TabIndex = 17;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "阴道分娩";
            // 
            // uc产次
            // 
            this.uc产次.Lbl1Size = new System.Drawing.Size(60, 14);
            this.uc产次.Lbl1Text = "次 剖宫产";
            this.uc产次.Lbl2Size = new System.Drawing.Size(18, 14);
            this.uc产次.Lbl2Text = "次";
            this.uc产次.Location = new System.Drawing.Point(54, 0);
            this.uc产次.Margin = new System.Windows.Forms.Padding(0);
            this.uc产次.Name = "uc产次";
            this.uc产次.Size = new System.Drawing.Size(307, 22);
            this.uc产次.TabIndex = 17;
            this.uc产次.Txt1EditValue = null;
            this.uc产次.Txt1Size = new System.Drawing.Size(100, 20);
            this.uc产次.Txt2EditValue = null;
            this.uc产次.Txt2Size = new System.Drawing.Size(100, 20);
            // 
            // uc填表孕周
            // 
            this.uc填表孕周.Lbl1Size = new System.Drawing.Size(18, 14);
            this.uc填表孕周.Lbl1Text = "周";
            this.uc填表孕周.Lbl2Size = new System.Drawing.Size(18, 14);
            this.uc填表孕周.Lbl2Text = "天";
            this.uc填表孕周.Location = new System.Drawing.Point(358, 102);
            this.uc填表孕周.Name = "uc填表孕周";
            this.uc填表孕周.Size = new System.Drawing.Size(253, 22);
            this.uc填表孕周.TabIndex = 11;
            this.uc填表孕周.Txt1EditValue = null;
            this.uc填表孕周.Txt1Size = new System.Drawing.Size(100, 20);
            this.uc填表孕周.Txt2EditValue = null;
            this.uc填表孕周.Txt2Size = new System.Drawing.Size(100, 20);
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(567, 1209);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(161, 20);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 140;
            // 
            // textEdit创建人
            // 
            this.textEdit创建人.Location = new System.Drawing.Point(319, 1209);
            this.textEdit创建人.Name = "textEdit创建人";
            this.textEdit创建人.Properties.ReadOnly = true;
            this.textEdit创建人.Size = new System.Drawing.Size(159, 20);
            this.textEdit创建人.StyleController = this.layoutControl1;
            this.textEdit创建人.TabIndex = 139;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(88, 1209);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(142, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 138;
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(567, 1185);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(161, 20);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 137;
            // 
            // textEdit最近更新时间
            // 
            this.textEdit最近更新时间.Location = new System.Drawing.Point(319, 1185);
            this.textEdit最近更新时间.Name = "textEdit最近更新时间";
            this.textEdit最近更新时间.Properties.ReadOnly = true;
            this.textEdit最近更新时间.Size = new System.Drawing.Size(159, 20);
            this.textEdit最近更新时间.StyleController = this.layoutControl1;
            this.textEdit最近更新时间.TabIndex = 136;
            // 
            // textEdit创建时间
            // 
            this.textEdit创建时间.Location = new System.Drawing.Point(88, 1185);
            this.textEdit创建时间.Name = "textEdit创建时间";
            this.textEdit创建时间.Properties.ReadOnly = true;
            this.textEdit创建时间.Size = new System.Drawing.Size(142, 20);
            this.textEdit创建时间.StyleController = this.layoutControl1;
            this.textEdit创建时间.TabIndex = 135;
            // 
            // textEdit丈夫电话
            // 
            this.textEdit丈夫电话.Location = new System.Drawing.Point(593, 152);
            this.textEdit丈夫电话.Name = "textEdit丈夫电话";
            this.textEdit丈夫电话.Size = new System.Drawing.Size(135, 20);
            this.textEdit丈夫电话.StyleController = this.layoutControl1;
            this.textEdit丈夫电话.TabIndex = 15;
            // 
            // textEdit丈夫年龄
            // 
            this.textEdit丈夫年龄.Location = new System.Drawing.Point(363, 152);
            this.textEdit丈夫年龄.Name = "textEdit丈夫年龄";
            this.textEdit丈夫年龄.Size = new System.Drawing.Size(141, 20);
            this.textEdit丈夫年龄.StyleController = this.layoutControl1;
            this.textEdit丈夫年龄.TabIndex = 14;
            // 
            // textEdit丈夫姓名
            // 
            this.textEdit丈夫姓名.Location = new System.Drawing.Point(88, 152);
            this.textEdit丈夫姓名.Name = "textEdit丈夫姓名";
            this.textEdit丈夫姓名.Size = new System.Drawing.Size(186, 20);
            this.textEdit丈夫姓名.StyleController = this.layoutControl1;
            this.textEdit丈夫姓名.TabIndex = 13;
            // 
            // textEdit居住地址
            // 
            this.textEdit居住地址.Location = new System.Drawing.Point(88, 78);
            this.textEdit居住地址.Name = "textEdit居住地址";
            this.textEdit居住地址.Properties.ReadOnly = true;
            this.textEdit居住地址.Size = new System.Drawing.Size(640, 20);
            this.textEdit居住地址.StyleController = this.layoutControl1;
            this.textEdit居住地址.TabIndex = 9;
            // 
            // textEdit居住状态
            // 
            this.textEdit居住状态.Location = new System.Drawing.Point(594, 54);
            this.textEdit居住状态.Name = "textEdit居住状态";
            this.textEdit居住状态.Properties.ReadOnly = true;
            this.textEdit居住状态.Size = new System.Drawing.Size(134, 20);
            this.textEdit居住状态.StyleController = this.layoutControl1;
            this.textEdit居住状态.TabIndex = 8;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(319, 1161);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Size = new System.Drawing.Size(158, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 134;
            // 
            // textEdit孕妇年龄
            // 
            this.textEdit孕妇年龄.Location = new System.Drawing.Point(88, 128);
            this.textEdit孕妇年龄.Name = "textEdit孕妇年龄";
            this.textEdit孕妇年龄.Size = new System.Drawing.Size(111, 20);
            this.textEdit孕妇年龄.StyleController = this.layoutControl1;
            this.textEdit孕妇年龄.TabIndex = 12;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(88, 54);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(182, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 6;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(359, 54);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(146, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 7;
            // 
            // textEdit孕妇姓名
            // 
            this.textEdit孕妇姓名.Location = new System.Drawing.Point(594, 30);
            this.textEdit孕妇姓名.Name = "textEdit孕妇姓名";
            this.textEdit孕妇姓名.Properties.ReadOnly = true;
            this.textEdit孕妇姓名.Size = new System.Drawing.Size(134, 20);
            this.textEdit孕妇姓名.StyleController = this.layoutControl1;
            this.textEdit孕妇姓名.TabIndex = 5;
            // 
            // textEdit档案编号
            // 
            this.textEdit档案编号.Location = new System.Drawing.Point(359, 30);
            this.textEdit档案编号.Name = "textEdit档案编号";
            this.textEdit档案编号.Properties.ReadOnly = true;
            this.textEdit档案编号.Size = new System.Drawing.Size(146, 20);
            this.textEdit档案编号.StyleController = this.layoutControl1;
            this.textEdit档案编号.TabIndex = 4;
            // 
            // textEdit卡号
            // 
            this.textEdit卡号.Location = new System.Drawing.Point(88, 30);
            this.textEdit卡号.Name = "textEdit卡号";
            this.textEdit卡号.Size = new System.Drawing.Size(182, 20);
            this.textEdit卡号.StyleController = this.layoutControl1;
            this.textEdit卡号.TabIndex = 3;
            // 
            // textEdit孕次
            // 
            this.textEdit孕次.Location = new System.Drawing.Point(88, 176);
            this.textEdit孕次.Name = "textEdit孕次";
            this.textEdit孕次.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit孕次.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.textEdit孕次.Size = new System.Drawing.Size(186, 20);
            this.textEdit孕次.StyleController = this.layoutControl1;
            this.textEdit孕次.TabIndex = 16;
            // 
            // cbo心脏
            // 
            this.cbo心脏.Location = new System.Drawing.Point(150, 420);
            this.cbo心脏.Name = "cbo心脏";
            this.cbo心脏.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo心脏.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo心脏.Size = new System.Drawing.Size(81, 20);
            this.cbo心脏.StyleController = this.layoutControl1;
            this.cbo心脏.TabIndex = 68;
            this.cbo心脏.SelectedIndexChanged += new System.EventHandler(this.cbo心脏_SelectedIndexChanged);
            // 
            // cbo肺部
            // 
            this.cbo肺部.Location = new System.Drawing.Point(486, 420);
            this.cbo肺部.Name = "cbo肺部";
            this.cbo肺部.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo肺部.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo肺部.Size = new System.Drawing.Size(91, 20);
            this.cbo肺部.StyleController = this.layoutControl1;
            this.cbo肺部.TabIndex = 71;
            this.cbo肺部.SelectedIndexChanged += new System.EventHandler(this.cbo肺部_SelectedIndexChanged);
            // 
            // cbo血型
            // 
            this.cbo血型.Location = new System.Drawing.Point(262, 638);
            this.cbo血型.Name = "cbo血型";
            this.cbo血型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo血型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo血型.Size = new System.Drawing.Size(123, 20);
            this.cbo血型.StyleController = this.layoutControl1;
            this.cbo血型.TabIndex = 93;
            // 
            // cboRH
            // 
            this.cboRH.Location = new System.Drawing.Point(262, 662);
            this.cboRH.Name = "cboRH";
            this.cboRH.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboRH.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboRH.Size = new System.Drawing.Size(123, 20);
            this.cboRH.StyleController = this.layoutControl1;
            this.cboRH.TabIndex = 94;
            // 
            // cbo阴道清洁度
            // 
            this.cbo阴道清洁度.Location = new System.Drawing.Point(240, 817);
            this.cbo阴道清洁度.Name = "cbo阴道清洁度";
            this.cbo阴道清洁度.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo阴道清洁度.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo阴道清洁度.Size = new System.Drawing.Size(133, 20);
            this.cbo阴道清洁度.StyleController = this.layoutControl1;
            this.cbo阴道清洁度.TabIndex = 108;
            // 
            // cbo梅毒血清学试验
            // 
            this.cbo梅毒血清学试验.Location = new System.Drawing.Point(180, 926);
            this.cbo梅毒血清学试验.Name = "cbo梅毒血清学试验";
            this.cbo梅毒血清学试验.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo梅毒血清学试验.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo梅毒血清学试验.Size = new System.Drawing.Size(225, 20);
            this.cbo梅毒血清学试验.StyleController = this.layoutControl1;
            this.cbo梅毒血清学试验.TabIndex = 115;
            // 
            // cboHIV抗体检测
            // 
            this.cboHIV抗体检测.Location = new System.Drawing.Point(180, 950);
            this.cboHIV抗体检测.Name = "cboHIV抗体检测";
            this.cboHIV抗体检测.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboHIV抗体检测.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboHIV抗体检测.Size = new System.Drawing.Size(225, 20);
            this.cboHIV抗体检测.StyleController = this.layoutControl1;
            this.cboHIV抗体检测.TabIndex = 116;
            // 
            // cbo外阴
            // 
            this.cbo外阴.Location = new System.Drawing.Point(150, 446);
            this.cbo外阴.Name = "cbo外阴";
            this.cbo外阴.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo外阴.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo外阴.Size = new System.Drawing.Size(81, 20);
            this.cbo外阴.StyleController = this.layoutControl1;
            this.cbo外阴.TabIndex = 72;
            this.cbo外阴.SelectedIndexChanged += new System.EventHandler(this.cbo外阴_SelectedIndexChanged);
            // 
            // cbo宫颈
            // 
            this.cbo宫颈.Location = new System.Drawing.Point(150, 470);
            this.cbo宫颈.Name = "cbo宫颈";
            this.cbo宫颈.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo宫颈.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo宫颈.Size = new System.Drawing.Size(81, 20);
            this.cbo宫颈.StyleController = this.layoutControl1;
            this.cbo宫颈.TabIndex = 76;
            this.cbo宫颈.SelectedIndexChanged += new System.EventHandler(this.cbo宫颈_SelectedIndexChanged);
            // 
            // cbo附件
            // 
            this.cbo附件.Location = new System.Drawing.Point(150, 494);
            this.cbo附件.Name = "cbo附件";
            this.cbo附件.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo附件.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo附件.Size = new System.Drawing.Size(81, 20);
            this.cbo附件.StyleController = this.layoutControl1;
            this.cbo附件.TabIndex = 80;
            this.cbo附件.SelectedIndexChanged += new System.EventHandler(this.cbo附件_SelectedIndexChanged);
            // 
            // cbo阴道
            // 
            this.cbo阴道.Location = new System.Drawing.Point(486, 446);
            this.cbo阴道.Name = "cbo阴道";
            this.cbo阴道.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo阴道.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo阴道.Size = new System.Drawing.Size(91, 20);
            this.cbo阴道.StyleController = this.layoutControl1;
            this.cbo阴道.TabIndex = 74;
            this.cbo阴道.SelectedIndexChanged += new System.EventHandler(this.cbo阴道_SelectedIndexChanged);
            // 
            // cbo子宫
            // 
            this.cbo子宫.Location = new System.Drawing.Point(486, 470);
            this.cbo子宫.Name = "cbo子宫";
            this.cbo子宫.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo子宫.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo子宫.Size = new System.Drawing.Size(91, 20);
            this.cbo子宫.StyleController = this.layoutControl1;
            this.cbo子宫.TabIndex = 78;
            this.cbo子宫.SelectedIndexChanged += new System.EventHandler(this.cbo子宫_SelectedIndexChanged);
            // 
            // dte填表日期
            // 
            this.dte填表日期.EditValue = null;
            this.dte填表日期.Location = new System.Drawing.Point(88, 102);
            this.dte填表日期.Name = "dte填表日期";
            this.dte填表日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte填表日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte填表日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte填表日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte填表日期.Size = new System.Drawing.Size(181, 20);
            this.dte填表日期.StyleController = this.layoutControl1;
            this.dte填表日期.TabIndex = 10;
            this.dte填表日期.EditValueChanged += new System.EventHandler(this.dte填表日期_EditValueChanged);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem5,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem6,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem22,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.layoutControlItem37,
            this.layoutControlItem38,
            this.layoutControlItem39,
            this.layoutControlItem40,
            this.layout妇科手术史,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.layoutControlItem51,
            this.layoutControlItem52,
            this.layoutControlItem53,
            this.layoutControlItem54,
            this.layoutControlItem55,
            this.layoutControlItem56,
            this.layoutControlItem57,
            this.layoutControlItem58,
            this.layoutControlItem59,
            this.layoutControlItem60,
            this.layoutControlItem74,
            this.layoutControlItem75,
            this.layoutControlItem76,
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlItem9,
            this.layoutControlItem7,
            this.layoutControlItem10,
            this.layoutControlItem13,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 1232);
            this.layoutControlGroup1.Text = "第1次产前随访服务记录表";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.textEdit卡号;
            this.layoutControlItem1.CustomizationFormText = "卡 号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(271, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "卡 号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.textEdit档案编号;
            this.layoutControlItem2.CustomizationFormText = "档案编号 ";
            this.layoutControlItem2.Location = new System.Drawing.Point(271, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "档案编号 ";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.textEdit孕妇姓名;
            this.layoutControlItem3.CustomizationFormText = "孕妇姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(506, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(223, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "孕妇姓名";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.textEdit身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(271, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证号 ";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit随访医生;
            this.layoutControlItem8.CustomizationFormText = "联系电话";
            this.layoutControlItem8.Location = new System.Drawing.Point(231, 1131);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(247, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "随访医师签名";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.textEdit创建时间;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 1155);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(231, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "创建时间:";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.textEdit最近更新时间;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(231, 1155);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(248, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "最近更新时间:";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.textEdit当前所属机构;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(479, 1155);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "当前所属机构:";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.textEdit创建机构;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 1179);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(231, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "创建机构: ";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.textEdit创建人;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(231, 1179);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(248, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "创建人:";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.textEdit最近修改人;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(479, 1179);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(250, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "最近修改人:";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.textEdit出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(271, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.textEdit居住状态;
            this.layoutControlItem11.CustomizationFormText = "居住状态 ";
            this.layoutControlItem11.Location = new System.Drawing.Point(506, 24);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(223, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "居住状态 ";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.textEdit居住地址;
            this.layoutControlItem12.CustomizationFormText = "居住地址 ";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "居住地址 ";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.dte填表日期;
            this.layoutControlItem31.CustomizationFormText = "填表日期";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(270, 26);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "填表日期";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.uc填表孕周;
            this.layoutControlItem32.CustomizationFormText = "填表孕周";
            this.layoutControlItem32.Location = new System.Drawing.Point(270, 72);
            this.layoutControlItem32.MaxSize = new System.Drawing.Size(342, 0);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(342, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(342, 26);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "填表孕周";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem6.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.textEdit孕妇年龄;
            this.layoutControlItem6.CustomizationFormText = "年 龄";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "孕妇年龄";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.textEdit丈夫姓名;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(275, 24);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(275, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(275, 24);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "丈夫姓名";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.textEdit丈夫年龄;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(275, 122);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(230, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "丈夫年龄";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem22.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.textEdit丈夫电话;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(505, 122);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(224, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "丈夫电话";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem33.Control = this.textEdit孕次;
            this.layoutControlItem33.CustomizationFormText = "孕次";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 146);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(275, 25);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(275, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(275, 24);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "孕次";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem34.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem34.Control = this.flowLayoutPanel2;
            this.layoutControlItem34.CustomizationFormText = "产 次";
            this.layoutControlItem34.Location = new System.Drawing.Point(275, 146);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(135, 24);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(454, 24);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "产 次";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem35.Control = this.flowLayoutPanel3;
            this.layoutControlItem35.CustomizationFormText = "末次月经";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 170);
            this.layoutControlItem35.MaxSize = new System.Drawing.Size(275, 24);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(275, 24);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(275, 24);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "末次月经";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem36.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem36.Control = this.dte预产期;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem36";
            this.layoutControlItem36.Location = new System.Drawing.Point(275, 170);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(454, 24);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "预产期";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem37.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem37.Control = this.flow既往病史;
            this.layoutControlItem37.CustomizationFormText = "既往病史";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 194);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(155, 48);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(729, 48);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Text = "既往病史";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem38.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem38.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem38.Control = this.flow家族史;
            this.layoutControlItem38.CustomizationFormText = "家 族 史";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 242);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "家 族 史";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem39.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.flow个人史;
            this.layoutControlItem39.CustomizationFormText = "个 人 史";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 266);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "个 人 史";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem40.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem40.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem40.Control = this.cbo妇科手术史;
            this.layoutControlItem40.CustomizationFormText = "妇科手术史";
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 290);
            this.layoutControlItem40.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "妇科手术史";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layout妇科手术史
            // 
            this.layout妇科手术史.Control = this.textEdit妇科手术史;
            this.layout妇科手术史.CustomizationFormText = "layoutControlItem41";
            this.layout妇科手术史.Location = new System.Drawing.Point(200, 290);
            this.layout妇科手术史.Name = "layout妇科手术史";
            this.layout妇科手术史.Size = new System.Drawing.Size(529, 24);
            this.layout妇科手术史.Text = "layout妇科手术史";
            this.layout妇科手术史.TextSize = new System.Drawing.Size(0, 0);
            this.layout妇科手术史.TextToControlDistance = 0;
            this.layout妇科手术史.TextVisible = false;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem42.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem42.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem42.Control = this.flowLayoutPanel7;
            this.layoutControlItem42.CustomizationFormText = "孕 产 史";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 314);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(189, 28);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(729, 28);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Text = "孕 产 史";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem43.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.uc身高;
            this.layoutControlItem43.CustomizationFormText = "身高";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 342);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(189, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(301, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "身高";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem44.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.uc体重;
            this.layoutControlItem44.CustomizationFormText = "体重";
            this.layoutControlItem44.Location = new System.Drawing.Point(301, 342);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(189, 24);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(428, 24);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "体重";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.textEdit体重指数;
            this.layoutControlItem45.CustomizationFormText = "体质指数";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 366);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(139, 24);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(301, 24);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "体质指数";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem46.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.uc血压;
            this.layoutControlItem46.CustomizationFormText = "血压";
            this.layoutControlItem46.Location = new System.Drawing.Point(301, 366);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(131, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(428, 24);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "血压";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.cbo心脏;
            this.layoutControlItem47.CustomizationFormText = "心脏";
            this.layoutControlItem47.Location = new System.Drawing.Point(82, 390);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(150, 26);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "心脏";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem48.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem48.Control = this.cbo肺部;
            this.layoutControlItem48.CustomizationFormText = "肺部";
            this.layoutControlItem48.Location = new System.Drawing.Point(428, 390);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(150, 26);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "肺部";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.textEdit心脏;
            this.layoutControlItem49.CustomizationFormText = "layoutControlItem49";
            this.layoutControlItem49.Location = new System.Drawing.Point(232, 390);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(196, 26);
            this.layoutControlItem49.Text = "layoutControlItem49";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem49.TextToControlDistance = 0;
            this.layoutControlItem49.TextVisible = false;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.textEdit肺部;
            this.layoutControlItem50.CustomizationFormText = "layoutControlItem50";
            this.layoutControlItem50.Location = new System.Drawing.Point(578, 390);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(151, 26);
            this.layoutControlItem50.Text = "layoutControlItem50";
            this.layoutControlItem50.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem50.TextToControlDistance = 0;
            this.layoutControlItem50.TextVisible = false;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem51.Control = this.cbo外阴;
            this.layoutControlItem51.CustomizationFormText = "外阴";
            this.layoutControlItem51.Location = new System.Drawing.Point(82, 416);
            this.layoutControlItem51.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem51.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem51.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem51.Text = "外阴";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.textEdit阴道;
            this.layoutControlItem52.CustomizationFormText = "layoutControlItem52";
            this.layoutControlItem52.Location = new System.Drawing.Point(578, 416);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(151, 24);
            this.layoutControlItem52.Text = "layoutControlItem52";
            this.layoutControlItem52.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem52.TextToControlDistance = 0;
            this.layoutControlItem52.TextVisible = false;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.textEdit外阴;
            this.layoutControlItem53.CustomizationFormText = "layoutControlItem53";
            this.layoutControlItem53.Location = new System.Drawing.Point(232, 416);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(196, 24);
            this.layoutControlItem53.Text = "layoutControlItem53";
            this.layoutControlItem53.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem53.TextToControlDistance = 0;
            this.layoutControlItem53.TextVisible = false;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.Control = this.cbo阴道;
            this.layoutControlItem54.CustomizationFormText = "阴道";
            this.layoutControlItem54.Location = new System.Drawing.Point(428, 416);
            this.layoutControlItem54.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Text = "阴道";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem55.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem55.Control = this.cbo宫颈;
            this.layoutControlItem55.CustomizationFormText = "宫颈";
            this.layoutControlItem55.Location = new System.Drawing.Point(82, 440);
            this.layoutControlItem55.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem55.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem55.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem55.Text = "宫颈";
            this.layoutControlItem55.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem55.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.Control = this.textEdit宫颈;
            this.layoutControlItem56.CustomizationFormText = "layoutControlItem56";
            this.layoutControlItem56.Location = new System.Drawing.Point(232, 440);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(196, 24);
            this.layoutControlItem56.Text = "layoutControlItem56";
            this.layoutControlItem56.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem56.TextToControlDistance = 0;
            this.layoutControlItem56.TextVisible = false;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem57.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem57.Control = this.cbo子宫;
            this.layoutControlItem57.CustomizationFormText = "子宫";
            this.layoutControlItem57.Location = new System.Drawing.Point(428, 440);
            this.layoutControlItem57.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem57.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem57.Text = "子宫";
            this.layoutControlItem57.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem57.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem57.TextToControlDistance = 5;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.Control = this.textEdit子宫;
            this.layoutControlItem58.CustomizationFormText = "layoutControlItem58";
            this.layoutControlItem58.Location = new System.Drawing.Point(578, 440);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Size = new System.Drawing.Size(151, 24);
            this.layoutControlItem58.Text = "layoutControlItem58";
            this.layoutControlItem58.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem58.TextToControlDistance = 0;
            this.layoutControlItem58.TextVisible = false;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem59.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem59.Control = this.cbo附件;
            this.layoutControlItem59.CustomizationFormText = "附件";
            this.layoutControlItem59.Location = new System.Drawing.Point(82, 464);
            this.layoutControlItem59.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem59.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem59.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem59.Text = "附件";
            this.layoutControlItem59.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem59.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem59.TextToControlDistance = 5;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.Control = this.textEdit附件;
            this.layoutControlItem60.CustomizationFormText = "layoutControlItem60";
            this.layoutControlItem60.Location = new System.Drawing.Point(232, 464);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(497, 24);
            this.layoutControlItem60.Text = "layoutControlItem60";
            this.layoutControlItem60.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem60.TextToControlDistance = 0;
            this.layoutControlItem60.TextVisible = false;
            // 
            // layoutControlItem74
            // 
            this.layoutControlItem74.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem74.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem74.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem74.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem74.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem74.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem74.Control = this.flowLayoutPanel14;
            this.layoutControlItem74.CustomizationFormText = "总体评估";
            this.layoutControlItem74.Location = new System.Drawing.Point(0, 968);
            this.layoutControlItem74.MinSize = new System.Drawing.Size(167, 24);
            this.layoutControlItem74.Name = "layoutControlItem74";
            this.layoutControlItem74.Size = new System.Drawing.Size(729, 24);
            this.layoutControlItem74.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem74.Text = "总体评估";
            this.layoutControlItem74.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem74.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem74.TextToControlDistance = 5;
            // 
            // layoutControlItem75
            // 
            this.layoutControlItem75.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem75.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem75.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem75.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem75.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem75.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem75.Control = this.flow保健指导;
            this.layoutControlItem75.CustomizationFormText = "保健指导";
            this.layoutControlItem75.Location = new System.Drawing.Point(0, 992);
            this.layoutControlItem75.MinSize = new System.Drawing.Size(159, 50);
            this.layoutControlItem75.Name = "layoutControlItem75";
            this.layoutControlItem75.Size = new System.Drawing.Size(729, 50);
            this.layoutControlItem75.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem75.Text = "保健指导";
            this.layoutControlItem75.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem75.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem75.TextToControlDistance = 5;
            // 
            // layoutControlItem76
            // 
            this.layoutControlItem76.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem76.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem76.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem76.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem76.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem76.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem76.Control = this.flowLayoutPanel16;
            this.layoutControlItem76.CustomizationFormText = "转诊";
            this.layoutControlItem76.Location = new System.Drawing.Point(0, 1071);
            this.layoutControlItem76.MinSize = new System.Drawing.Size(202, 60);
            this.layoutControlItem76.Name = "layoutControlItem76";
            this.layoutControlItem76.Size = new System.Drawing.Size(729, 60);
            this.layoutControlItem76.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem76.Text = "转诊";
            this.layoutControlItem76.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem76.TextSize = new System.Drawing.Size(80, 60);
            this.layoutControlItem76.TextToControlDistance = 5;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem62,
            this.layoutControlItem61,
            this.layoutControlItem65,
            this.layoutControlItem66,
            this.layoutControlItem63,
            this.layoutControlItem64,
            this.layoutControlItem67,
            this.layoutControlItem68,
            this.layoutControlItem69,
            this.layoutControlItem70,
            this.layoutControlItem71,
            this.layoutControlItem72,
            this.layoutControlItem73,
            this.emptySpaceItem9,
            this.emptySpaceItem8,
            this.emptySpaceItem10,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlGroup7});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 488);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(729, 480);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem62.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem62.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem62.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem62.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem62.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem62.Control = this.flowLayoutPanel9;
            this.layoutControlItem62.CustomizationFormText = "尿 常 规";
            this.layoutControlItem62.Location = new System.Drawing.Point(82, 60);
            this.layoutControlItem62.MinSize = new System.Drawing.Size(151, 60);
            this.layoutControlItem62.Name = "layoutControlItem62";
            this.layoutControlItem62.Size = new System.Drawing.Size(647, 60);
            this.layoutControlItem62.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem62.Text = "尿 常 规";
            this.layoutControlItem62.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem62.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem62.TextToControlDistance = 5;
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem61.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem61.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem61.Control = this.flowLayoutPanel8;
            this.layoutControlItem61.CustomizationFormText = "血 常 规";
            this.layoutControlItem61.Location = new System.Drawing.Point(82, 0);
            this.layoutControlItem61.MinSize = new System.Drawing.Size(151, 60);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Size = new System.Drawing.Size(647, 60);
            this.layoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem61.Text = "血 常 规";
            this.layoutControlItem61.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem61.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem61.TextToControlDistance = 5;
            // 
            // layoutControlItem65
            // 
            this.layoutControlItem65.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem65.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem65.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem65.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem65.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem65.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem65.Control = this.cbo血型;
            this.layoutControlItem65.CustomizationFormText = "ABO";
            this.layoutControlItem65.Location = new System.Drawing.Point(174, 120);
            this.layoutControlItem65.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem65.MinSize = new System.Drawing.Size(152, 24);
            this.layoutControlItem65.Name = "layoutControlItem65";
            this.layoutControlItem65.Size = new System.Drawing.Size(212, 24);
            this.layoutControlItem65.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem65.Text = "ABO";
            this.layoutControlItem65.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem65.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem65.TextToControlDistance = 5;
            // 
            // layoutControlItem66
            // 
            this.layoutControlItem66.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem66.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem66.Control = this.cboRH;
            this.layoutControlItem66.CustomizationFormText = "RH* ";
            this.layoutControlItem66.Location = new System.Drawing.Point(174, 144);
            this.layoutControlItem66.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem66.MinSize = new System.Drawing.Size(152, 24);
            this.layoutControlItem66.Name = "layoutControlItem66";
            this.layoutControlItem66.Size = new System.Drawing.Size(212, 24);
            this.layoutControlItem66.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem66.Text = "RH* ";
            this.layoutControlItem66.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem66.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem66.TextToControlDistance = 5;
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.AppearanceItemCaption.ForeColor = System.Drawing.Color.Black;
            this.layoutControlItem63.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem63.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem63.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem63.Control = this.uc血糖;
            this.layoutControlItem63.CustomizationFormText = "血 糖* ";
            this.layoutControlItem63.Location = new System.Drawing.Point(82, 168);
            this.layoutControlItem63.MinSize = new System.Drawing.Size(202, 24);
            this.layoutControlItem63.Name = "layoutControlItem63";
            this.layoutControlItem63.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem63.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem63.Text = "血 糖* ";
            this.layoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem63.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem63.TextToControlDistance = 5;
            // 
            // layoutControlItem64
            // 
            this.layoutControlItem64.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem64.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem64.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem64.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem64.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem64.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem64.Control = this.flowLayoutPanel10;
            this.layoutControlItem64.CustomizationFormText = "肝 功 能";
            this.layoutControlItem64.Location = new System.Drawing.Point(82, 192);
            this.layoutControlItem64.MinSize = new System.Drawing.Size(159, 55);
            this.layoutControlItem64.Name = "layoutControlItem64";
            this.layoutControlItem64.Size = new System.Drawing.Size(647, 55);
            this.layoutControlItem64.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem64.Text = "肝 功 能";
            this.layoutControlItem64.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem64.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem64.TextToControlDistance = 5;
            // 
            // layoutControlItem67
            // 
            this.layoutControlItem67.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem67.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem67.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem67.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem67.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem67.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem67.Control = this.flowLayoutPanel11;
            this.layoutControlItem67.CustomizationFormText = "肾 功 能";
            this.layoutControlItem67.Location = new System.Drawing.Point(82, 247);
            this.layoutControlItem67.MinSize = new System.Drawing.Size(202, 28);
            this.layoutControlItem67.Name = "layoutControlItem67";
            this.layoutControlItem67.Size = new System.Drawing.Size(647, 28);
            this.layoutControlItem67.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem67.Text = "肾 功 能";
            this.layoutControlItem67.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem67.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem67.TextToControlDistance = 5;
            // 
            // layoutControlItem68
            // 
            this.layoutControlItem68.Control = this.flowLayoutPanel12;
            this.layoutControlItem68.CustomizationFormText = "layoutControlItem68";
            this.layoutControlItem68.Location = new System.Drawing.Point(174, 275);
            this.layoutControlItem68.Name = "layoutControlItem68";
            this.layoutControlItem68.Size = new System.Drawing.Size(555, 24);
            this.layoutControlItem68.Text = "layoutControlItem68";
            this.layoutControlItem68.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem68.TextToControlDistance = 0;
            this.layoutControlItem68.TextVisible = false;
            // 
            // layoutControlItem69
            // 
            this.layoutControlItem69.Control = this.cbo阴道清洁度;
            this.layoutControlItem69.CustomizationFormText = "阴道清洁度";
            this.layoutControlItem69.Location = new System.Drawing.Point(174, 299);
            this.layoutControlItem69.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem69.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem69.Name = "layoutControlItem69";
            this.layoutControlItem69.Size = new System.Drawing.Size(555, 24);
            this.layoutControlItem69.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem69.Text = "阴道清洁度";
            this.layoutControlItem69.TextSize = new System.Drawing.Size(60, 14);
            // 
            // layoutControlItem70
            // 
            this.layoutControlItem70.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem70.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem70.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem70.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem70.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem70.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem70.Control = this.flowLayoutPanel13;
            this.layoutControlItem70.CustomizationFormText = "乙型肝炎五项";
            this.layoutControlItem70.Location = new System.Drawing.Point(82, 323);
            this.layoutControlItem70.MinSize = new System.Drawing.Size(159, 85);
            this.layoutControlItem70.Name = "layoutControlItem70";
            this.layoutControlItem70.Size = new System.Drawing.Size(647, 85);
            this.layoutControlItem70.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem70.Text = "乙型肝炎五项";
            this.layoutControlItem70.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem70.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem70.TextToControlDistance = 5;
            // 
            // layoutControlItem71
            // 
            this.layoutControlItem71.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem71.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem71.Control = this.cbo梅毒血清学试验;
            this.layoutControlItem71.CustomizationFormText = "梅毒血清学试验* ";
            this.layoutControlItem71.Location = new System.Drawing.Point(82, 408);
            this.layoutControlItem71.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem71.MinSize = new System.Drawing.Size(152, 24);
            this.layoutControlItem71.Name = "layoutControlItem71";
            this.layoutControlItem71.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem71.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem71.Text = "梅毒血清学试验* ";
            this.layoutControlItem71.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem71.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem71.TextToControlDistance = 5;
            // 
            // layoutControlItem72
            // 
            this.layoutControlItem72.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem72.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem72.Control = this.cboHIV抗体检测;
            this.layoutControlItem72.CustomizationFormText = "HIV抗体检测* ";
            this.layoutControlItem72.Location = new System.Drawing.Point(82, 432);
            this.layoutControlItem72.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem72.MinSize = new System.Drawing.Size(152, 24);
            this.layoutControlItem72.Name = "layoutControlItem72";
            this.layoutControlItem72.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem72.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem72.Text = "HIV抗体检测* ";
            this.layoutControlItem72.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem72.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem72.TextToControlDistance = 5;
            // 
            // layoutControlItem73
            // 
            this.layoutControlItem73.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem73.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem73.Control = this.textEditB超;
            this.layoutControlItem73.CustomizationFormText = "B 超* ";
            this.layoutControlItem73.Location = new System.Drawing.Point(82, 456);
            this.layoutControlItem73.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem73.MinSize = new System.Drawing.Size(152, 24);
            this.layoutControlItem73.Name = "layoutControlItem73";
            this.layoutControlItem73.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem73.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem73.Text = "B 超* ";
            this.layoutControlItem73.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem73.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem73.TextToControlDistance = 5;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(406, 408);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(323, 24);
            this.emptySpaceItem9.Text = "emptySpaceItem9";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(406, 432);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(323, 24);
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(406, 456);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(323, 24);
            this.emptySpaceItem10.Text = "emptySpaceItem10";
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(386, 120);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(343, 24);
            this.emptySpaceItem11.Text = "emptySpaceItem11";
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(386, 144);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(343, 24);
            this.emptySpaceItem12.Text = "emptySpaceItem12";
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(82, 480);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem4.CustomizationFormText = "辅助检查";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(80, 478);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "辅助检查";
            this.emptySpaceItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem4.TextVisible = true;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem5});
            this.layoutControlGroup6.Location = new System.Drawing.Point(82, 120);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Size = new System.Drawing.Size(92, 48);
            this.layoutControlGroup6.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            this.layoutControlGroup6.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem5.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem5.CustomizationFormText = "血 型 ";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(90, 46);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "血 型 ";
            this.emptySpaceItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6});
            this.layoutControlGroup7.Location = new System.Drawing.Point(82, 275);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(92, 48);
            this.layoutControlGroup7.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Text = "layoutControlGroup7";
            this.layoutControlGroup7.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem6.CustomizationFormText = "阴道分泌物* ";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(90, 46);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "阴道分泌物* ";
            this.emptySpaceItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(80, 20);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 390);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(82, 26);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem2.CustomizationFormText = "听诊";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(80, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(80, 24);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "听诊";
            this.emptySpaceItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(80, 20);
            this.emptySpaceItem2.TextVisible = true;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 416);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(82, 72);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem3.CustomizationFormText = "妇科检查";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(80, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(80, 70);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "妇科检查";
            this.emptySpaceItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem3.TextVisible = true;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.flowLayoutPanel4;
            this.layoutControlItem9.CustomizationFormText = "建册情况";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 1042);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(729, 29);
            this.layoutControlItem9.Text = "建册情况";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(80, 25);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.Control = this.dte下次随访日期;
            this.layoutControlItem7.CustomizationFormText = "下次随访日期";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 1131);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(139, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(231, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "下次随访日期";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt家属签名;
            this.layoutControlItem10.CustomizationFormText = "居民/家属签名";
            this.layoutControlItem10.Location = new System.Drawing.Point(478, 1131);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(251, 24);
            this.layoutControlItem10.Text = "居民/家属签名";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.sbtnCalYZ;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(612, 72);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(86, 0);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(86, 26);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(86, 26);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(698, 72);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(31, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn读取
            // 
            this.btn读取.Image = ((System.Drawing.Image)(resources.GetObject("btn读取.Image")));
            this.btn读取.Location = new System.Drawing.Point(84, 3);
            this.btn读取.Name = "btn读取";
            this.btn读取.Size = new System.Drawing.Size(145, 23);
            this.btn读取.TabIndex = 1;
            this.btn读取.Text = "读取近期化验结果";
            this.btn读取.Click += new System.EventHandler(this.btn读取_Click);
            // 
            // btn填表说明
            // 
            this.btn填表说明.Image = ((System.Drawing.Image)(resources.GetObject("btn填表说明.Image")));
            this.btn填表说明.Location = new System.Drawing.Point(235, 3);
            this.btn填表说明.Name = "btn填表说明";
            this.btn填表说明.Size = new System.Drawing.Size(85, 23);
            this.btn填表说明.TabIndex = 2;
            this.btn填表说明.Text = "填表说明";
            // 
            // sbtn上传化验单图片
            // 
            this.sbtn上传化验单图片.Image = ((System.Drawing.Image)(resources.GetObject("sbtn上传化验单图片.Image")));
            this.sbtn上传化验单图片.Location = new System.Drawing.Point(326, 3);
            this.sbtn上传化验单图片.Name = "sbtn上传化验单图片";
            this.sbtn上传化验单图片.Size = new System.Drawing.Size(122, 23);
            this.sbtn上传化验单图片.TabIndex = 2;
            this.sbtn上传化验单图片.Text = "上传化验单图片";
            this.sbtn上传化验单图片.Click += new System.EventHandler(this.sbtn上传化验单图片_Click);
            // 
            // UC第1次产前随访服务记录表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC第1次产前随访服务记录表";
            this.Size = new System.Drawing.Size(748, 498);
            this.Load += new System.EventHandler(this.UC第1次产前随访服务记录表_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt家属签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo建册情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt建册机构名称.Properties)).EndInit();
            this.flowLayoutPanel16.ResumeLayout(false);
            this.flowLayoutPanel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo转诊有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo是否到位.Properties)).EndInit();
            this.flow保健指导.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk保健指导6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt保健指导其他.Properties)).EndInit();
            this.flowLayoutPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo总体评估.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit总体评估异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditB超.Properties)).EndInit();
            this.flowLayoutPanel13.ResumeLayout(false);
            this.flowLayoutPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo阴道分泌物.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit阴道分泌物其他.Properties)).EndInit();
            this.flowLayoutPanel10.ResumeLayout(false);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit附件.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit子宫.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit宫颈.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit外阴.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit阴道.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit肺部.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit心脏.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体重指数.Properties)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit妇科手术史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo妇科手术史.Properties)).EndInit();
            this.flow个人史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史0.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk个人史6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人史其他.Properties)).EndInit();
            this.flow家族史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史0.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit家族史其他.Properties)).EndInit();
            this.flow既往病史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk既往病史有无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit既往病史其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte预产期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte预产期.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk末次月经.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte末次月经.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte末次月经.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫年龄.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇年龄.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo心脏.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo肺部.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo血型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo阴道清洁度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo梅毒血清学试验.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboHIV抗体检测.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo外阴.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo宫颈.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo附件.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo阴道.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo子宫.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte填表日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte填表日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout妇科手术史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn读取;
        private DevExpress.XtraEditors.SimpleButton btn填表说明;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit textEdit丈夫电话;
        private DevExpress.XtraEditors.TextEdit textEdit丈夫年龄;
        private DevExpress.XtraEditors.TextEdit textEdit丈夫姓名;
        private DevExpress.XtraEditors.TextEdit textEdit居住地址;
        private DevExpress.XtraEditors.TextEdit textEdit居住状态;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraEditors.TextEdit textEdit孕妇年龄;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit孕妇姓名;
        private DevExpress.XtraEditors.TextEdit textEdit档案编号;
        private DevExpress.XtraEditors.TextEdit textEdit卡号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraEditors.TextEdit textEdit创建人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近更新时间;
        private DevExpress.XtraEditors.TextEdit textEdit创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private Library.UserControls.UCTxtLblTxtLbl uc产次;
        private Library.UserControls.UCTxtLblTxtLbl uc填表孕周;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private Library.UserControls.UCLblTxtLbl uc流产;
        private Library.UserControls.UCLblTxtLbl uc死胎;
        private Library.UserControls.UCLblTxtLbl uc死产;
        private DevExpress.XtraEditors.TextEdit textEdit妇科手术史;
        private DevExpress.XtraEditors.ComboBoxEdit cbo妇科手术史;
        private System.Windows.Forms.FlowLayoutPanel flow个人史;
        private DevExpress.XtraEditors.CheckEdit chk个人史0;
        private DevExpress.XtraEditors.CheckEdit chk个人史1;
        private DevExpress.XtraEditors.CheckEdit chk个人史2;
        private DevExpress.XtraEditors.CheckEdit chk个人史3;
        private DevExpress.XtraEditors.CheckEdit chk个人史4;
        private DevExpress.XtraEditors.CheckEdit chk个人史5;
        private DevExpress.XtraEditors.CheckEdit chk个人史6;
        private DevExpress.XtraEditors.TextEdit txt个人史其他;
        private System.Windows.Forms.FlowLayoutPanel flow家族史;
        private DevExpress.XtraEditors.CheckEdit chk家族史0;
        private DevExpress.XtraEditors.CheckEdit chk家族史1;
        private DevExpress.XtraEditors.CheckEdit chk家族史2;
        private DevExpress.XtraEditors.CheckEdit chk家族史3;
        private DevExpress.XtraEditors.CheckEdit chk家族史4;
        private DevExpress.XtraEditors.CheckEdit chk家族史5;
        private DevExpress.XtraEditors.CheckEdit chk家族史6;
        private DevExpress.XtraEditors.TextEdit textEdit家族史其他;
        private System.Windows.Forms.FlowLayoutPanel flow既往病史;
        private DevExpress.XtraEditors.CheckEdit chk既往病史有无;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraEditors.TextEdit textEdit既往病史其他;
        private DevExpress.XtraEditors.DateEdit dte预产期;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.CheckEdit chk末次月经;
        private DevExpress.XtraEditors.DateEdit dte末次月经;
        private DevExpress.XtraEditors.ComboBoxEdit textEdit孕次;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layout妇科手术史;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private Library.UserControls.UCTxtLblTxtLbl uc血压;
        private DevExpress.XtraEditors.TextEdit textEdit体重指数;
        private Library.UserControls.UCTxtLbl uc体重;
        private Library.UserControls.UCTxtLbl uc身高;
        private Library.UserControls.UCLblTxtLbl uc新生儿死亡;
        private Library.UserControls.UCLblTxtLbl uc出生缺陷儿;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private Library.UserControls.UCTxtLbl uc血糖;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private Library.UserControls.UCLblTxtLbl uc血红蛋白值;
        private Library.UserControls.UCLblTxtLbl uc白细胞计数值;
        private Library.UserControls.UCLblTxtLbl uc血小板计数值;
        private Library.UserControls.UCLblTxt uc血常规其他;
        private DevExpress.XtraEditors.TextEdit textEdit附件;
        private DevExpress.XtraEditors.TextEdit textEdit子宫;
        private DevExpress.XtraEditors.TextEdit textEdit宫颈;
        private DevExpress.XtraEditors.TextEdit textEdit外阴;
        private DevExpress.XtraEditors.TextEdit textEdit阴道;
        private DevExpress.XtraEditors.TextEdit textEdit肺部;
        private DevExpress.XtraEditors.TextEdit textEdit心脏;
        private DevExpress.XtraEditors.ComboBoxEdit cbo心脏;
        private DevExpress.XtraEditors.ComboBoxEdit cbo肺部;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem64;
        private Library.UserControls.UCLblTxtLbl uc血清谷丙转氨酶;
        private Library.UserControls.UCLblTxtLbl uc血清谷草转氨酶;
        private Library.UserControls.UCLblTxtLbl uc白蛋白;
        private Library.UserControls.UCLblTxtLbl uc总胆红素;
        private Library.UserControls.UCLblTxtLbl uc结合胆红素;
        private Library.UserControls.UCLblTxt uc尿蛋白;
        private Library.UserControls.UCLblTxt uc尿糖;
        private Library.UserControls.UCLblTxt uc尿酮体;
        private Library.UserControls.UCLblTxt uc尿潜血;
        private Library.UserControls.UCLblTxt uc尿常规其他;
        private DevExpress.XtraEditors.ComboBoxEdit cbo血型;
        private DevExpress.XtraEditors.ComboBoxEdit cboRH;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem65;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem66;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private DevExpress.XtraEditors.ComboBoxEdit cbo阴道分泌物;
        private DevExpress.XtraEditors.TextEdit textEdit阴道分泌物其他;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private Library.UserControls.UCLblTxtLbl uc血清肌酐;
        private Library.UserControls.UCLblTxtLbl uc血尿素氮;
        private DevExpress.XtraEditors.ComboBoxEdit cbo阴道清洁度;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem67;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem68;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem69;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private Library.UserControls.UCLblTxt uc乙型肝炎表面抗原;
        private Library.UserControls.UCLblTxt uc乙型肝炎表面抗体;
        private Library.UserControls.UCLblTxt uc乙型肝炎e抗原;
        private Library.UserControls.UCLblTxt uc乙型肝炎e抗体;
        private Library.UserControls.UCLblTxt uc乙型肝炎核心抗体;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem70;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private DevExpress.XtraEditors.ComboBoxEdit cbo转诊有无;
        private Library.UserControls.UCLblTxt uc转诊原因;
        private Library.UserControls.UCLblTxt uc机构及科室;
        private System.Windows.Forms.FlowLayoutPanel flow保健指导;
        private DevExpress.XtraEditors.CheckEdit chk保健指导1;
        private DevExpress.XtraEditors.CheckEdit chk保健指导2;
        private DevExpress.XtraEditors.CheckEdit chk保健指导3;
        private DevExpress.XtraEditors.CheckEdit chk保健指导4;
        private DevExpress.XtraEditors.CheckEdit chk保健指导5;
        private DevExpress.XtraEditors.CheckEdit chk保健指导6;
        private DevExpress.XtraEditors.TextEdit txt保健指导其他;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private DevExpress.XtraEditors.ComboBoxEdit cbo总体评估;
        private DevExpress.XtraEditors.TextEdit textEdit总体评估异常;
        private DevExpress.XtraEditors.TextEdit textEditB超;
        private DevExpress.XtraEditors.ComboBoxEdit cbo梅毒血清学试验;
        private DevExpress.XtraEditors.ComboBoxEdit cboHIV抗体检测;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem71;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem72;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem73;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem74;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem75;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem76;
        private DevExpress.XtraEditors.ComboBoxEdit cbo外阴;
        private DevExpress.XtraEditors.ComboBoxEdit cbo宫颈;
        private DevExpress.XtraEditors.ComboBoxEdit cbo附件;
        private DevExpress.XtraEditors.ComboBoxEdit cbo阴道;
        private DevExpress.XtraEditors.ComboBoxEdit cbo子宫;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.DateEdit dte填表日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit txt建册机构名称;
        private Library.UserControls.UCLblTxt uc转诊联系人;
        private DevExpress.XtraEditors.ComboBoxEdit cbo是否到位;
        private DevExpress.XtraEditors.DateEdit dte下次随访日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private System.Windows.Forms.Label label1;
        private Library.UserControls.UCLblTxt uc联系方式;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txt家属签名;
        private DevExpress.XtraEditors.ComboBoxEdit cbo建册情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SimpleButton sbtnCalYZ;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton sbtn上传化验单图片;
    }
}
