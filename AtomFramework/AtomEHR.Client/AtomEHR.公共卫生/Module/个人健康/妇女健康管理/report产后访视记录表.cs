﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class report产后访视记录表 : DevExpress.XtraReports.UI.XtraReport
    {
        #region Fields
        DataSet _ds孕产妇;
        string docNo;
        bll孕妇_产后访视情况 _bll访视 = new bll孕妇_产后访视情况();
        #endregion

        public report产后访视记录表()
        {
            InitializeComponent();
        }

        public report产后访视记录表(string _docNo)
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds孕产妇 = _bll访视.GetInfoByFangshi(_docNo,true);
            DoBindingDataSource(_ds孕产妇);
        }

        private void DoBindingDataSource(DataSet _ds产后访视)
        {
            DataTable dt孕产妇=_ds孕产妇.Tables[Models.tb_孕妇_产后访视情况.__TableName];
            if (dt孕产妇 == null || dt孕产妇.Rows.Count == 0) return;

            DataTable dt健康档案=_ds孕产妇.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.txt随访日期.Text = Convert.ToDateTime(dt孕产妇.Rows[0][tb_孕妇_产后访视情况.随访日期]).ToLongDateString().ToString();//将时间格式转换为年月日
            this.txt体温.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.体温].ToString();
            this.txt一般健康情况.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.一般健康情况].ToString();
            this.txt一般心理状况.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.一般心理状况].ToString();
            this.txt血压1.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.血压1].ToString();
            this.txt血压2.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.血压2].ToString();
            this.txt乳房.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.乳房].ToString();
            this.txt乳房异常.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.乳房异常].ToString();
            this.txt恶露.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.恶露].ToString();
            this.txt恶露异常.Text=dt孕产妇.Rows[0][tb_孕妇_产后访视情况.恶露异常].ToString();
            this.txt子宫.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.子宫].ToString();
            this.txt子宫异常.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.子宫异常].ToString();
            this.txt伤口.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.伤口].ToString();
            this.txt伤口异常.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.伤口异常].ToString();
            this.txt其他.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.其他].ToString();
            this.txt分类.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.分类].ToString();
            this.txt分类异常.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.分类异常].ToString();
            string 指导 = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.指导].ToString();
            if(!string.IsNullOrEmpty(指导))
            {
                string[] a = 指导.Split(',');
                for (int i = 0; i < a.Length;i++ )
                {
                    if (string.IsNullOrEmpty(a[i]))
                    {
                        continue;
                    }
                    string strName = "txt指导" + a[i];
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    //if (a[i] == "99")
                    //    lbl.Text = "6";
                    //else
                        lbl.Text = a[i];
                }
            }
            this.txt指导其他.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.指导其他].ToString();
            this.txt转诊.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.转诊].ToString();
            this.txt转诊原因.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.转诊原因].ToString();
            this.txt转诊机构及科室.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.机构科室].ToString();
            this.txt下次随访日期.Text = Convert.ToDateTime(dt孕产妇.Rows[0][tb_孕妇_产后访视情况.下次随访日期]).ToLongDateString().ToString();
            this.txt随访医生签名.Text = dt孕产妇.Rows[0][tb_孕妇_产后访视情况.随访医生签名].ToString();

        }

    }
}
