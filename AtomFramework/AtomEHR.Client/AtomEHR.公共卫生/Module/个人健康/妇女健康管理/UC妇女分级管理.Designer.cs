﻿namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    partial class UC妇女分级管理
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC妇女分级管理));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn取消 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt病理检查 = new DevExpress.XtraEditors.TextEdit();
            this.txtTCT结果 = new DevExpress.XtraEditors.TextEdit();
            this.txt乳腺彩超 = new DevExpress.XtraEditors.TextEdit();
            this.txt钼靶检查 = new DevExpress.XtraEditors.TextEdit();
            this.txt最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建人 = new DevExpress.XtraEditors.TextEdit();
            this.txt当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt医师签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt治疗情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt处理意见 = new DevExpress.XtraEditors.MemoEdit();
            this.txt印象 = new DevExpress.XtraEditors.TextEdit();
            this.txt阴道异常情况 = new DevExpress.XtraEditors.TextEdit();
            this.flow附件 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit31 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit32 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit33 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit34 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit35 = new DevExpress.XtraEditors.CheckEdit();
            this.txt右卵巢 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt左卵巢 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt子宫体大小 = new DevExpress.XtraEditors.TextEdit();
            this.txt子宫脱垂 = new DevExpress.XtraEditors.TextEdit();
            this.txt子宫体活动 = new DevExpress.XtraEditors.TextEdit();
            this.flow子宫颈 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit22 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit23 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit24 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit25 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit26 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit27 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit28 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit29 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit30 = new DevExpress.XtraEditors.CheckEdit();
            this.flow白带性质 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk白带_水样 = new DevExpress.XtraEditors.CheckEdit();
            this.chk白带_白 = new DevExpress.XtraEditors.CheckEdit();
            this.chk白带_黄色 = new DevExpress.XtraEditors.CheckEdit();
            this.chk白带_侬性 = new DevExpress.XtraEditors.CheckEdit();
            this.chk白带_血样 = new DevExpress.XtraEditors.CheckEdit();
            this.chk白带_泡沫 = new DevExpress.XtraEditors.CheckEdit();
            this.chk白带_渣样 = new DevExpress.XtraEditors.CheckEdit();
            this.chk白带_臭味 = new DevExpress.XtraEditors.CheckEdit();
            this.chk白带性质 = new DevExpress.XtraEditors.CheckEdit();
            this.txt白带 = new DevExpress.XtraEditors.TextEdit();
            this.txt阴道 = new DevExpress.XtraEditors.TextEdit();
            this.flow外阴 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.txt乳房右侧 = new DevExpress.XtraEditors.TextEdit();
            this.txt乳房左侧 = new DevExpress.XtraEditors.TextEdit();
            this.txt压痛 = new DevExpress.XtraEditors.TextEdit();
            this.txt结节 = new DevExpress.XtraEditors.TextEdit();
            this.txt乳房 = new DevExpress.XtraEditors.TextEdit();
            this.txt不孕史女 = new DevExpress.XtraEditors.TextEdit();
            this.txt不孕史男 = new DevExpress.XtraEditors.TextEdit();
            this.txt不孕史时间 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.chk不孕史 = new DevExpress.XtraEditors.CheckEdit();
            this.txt中孕引产 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt人工流产 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt自然流产 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt产次 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt孕次 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.chk未孕 = new DevExpress.XtraEditors.CheckEdit();
            this.txt曾做手术 = new DevExpress.XtraEditors.TextEdit();
            this.txt曾患妇科病 = new DevExpress.XtraEditors.TextEdit();
            this.txt现有症状 = new DevExpress.XtraEditors.TextEdit();
            this.txt检查次数 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.txt联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住状态 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt卡号 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.dte检查日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt档案编号 = new DevExpress.XtraEditors.ButtonEdit();
            this.radio分级 = new DevExpress.XtraEditors.RadioGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem68 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem69 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl治疗情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl医师签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup13 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup14 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem77 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem75 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem76 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem74 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem73 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem72 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btn关闭 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt病理检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTCT结果.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳腺彩超.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt钼靶检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医师签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt治疗情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt处理意见.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt印象.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阴道异常情况.Properties)).BeginInit();
            this.flow附件.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt子宫体大小.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt子宫脱垂.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt子宫体活动.Properties)).BeginInit();
            this.flow子宫颈.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).BeginInit();
            this.flow白带性质.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_水样.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_黄色.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_侬性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_血样.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_泡沫.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_渣样.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_臭味.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带性质.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt白带.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阴道.Properties)).BeginInit();
            this.flow外阴.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳房右侧.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳房左侧.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt压痛.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结节.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳房.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt不孕史女.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt不孕史男.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk不孕史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk未孕.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt曾做手术.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt曾患妇科病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt现有症状.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte检查日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte检查日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio分级.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl治疗情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl医师签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btn关闭);
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(748, 32);
            this.panelControl1.TabIndex = 4;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn取消);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(744, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Image = ((System.Drawing.Image)(resources.GetObject("btn保存.Image")));
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            this.btn保存.Click += new System.EventHandler(this.btn保存_Click);
            // 
            // btn取消
            // 
            this.btn取消.Image = ((System.Drawing.Image)(resources.GetObject("btn取消.Image")));
            this.btn取消.Location = new System.Drawing.Point(84, 3);
            this.btn取消.Name = "btn取消";
            this.btn取消.Size = new System.Drawing.Size(75, 23);
            this.btn取消.TabIndex = 1;
            this.btn取消.Text = "取消";
            this.btn取消.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt病理检查);
            this.layoutControl1.Controls.Add(this.txtTCT结果);
            this.layoutControl1.Controls.Add(this.txt乳腺彩超);
            this.layoutControl1.Controls.Add(this.txt钼靶检查);
            this.layoutControl1.Controls.Add(this.txt最近修改人);
            this.layoutControl1.Controls.Add(this.txt创建人);
            this.layoutControl1.Controls.Add(this.txt当前所属机构);
            this.layoutControl1.Controls.Add(this.txt最近更新时间);
            this.layoutControl1.Controls.Add(this.txt创建机构);
            this.layoutControl1.Controls.Add(this.txt创建时间);
            this.layoutControl1.Controls.Add(this.txt医师签名);
            this.layoutControl1.Controls.Add(this.txt治疗情况);
            this.layoutControl1.Controls.Add(this.txt处理意见);
            this.layoutControl1.Controls.Add(this.txt印象);
            this.layoutControl1.Controls.Add(this.txt阴道异常情况);
            this.layoutControl1.Controls.Add(this.flow附件);
            this.layoutControl1.Controls.Add(this.txt右卵巢);
            this.layoutControl1.Controls.Add(this.txt左卵巢);
            this.layoutControl1.Controls.Add(this.txt子宫体大小);
            this.layoutControl1.Controls.Add(this.txt子宫脱垂);
            this.layoutControl1.Controls.Add(this.txt子宫体活动);
            this.layoutControl1.Controls.Add(this.flow子宫颈);
            this.layoutControl1.Controls.Add(this.flow白带性质);
            this.layoutControl1.Controls.Add(this.txt白带);
            this.layoutControl1.Controls.Add(this.txt阴道);
            this.layoutControl1.Controls.Add(this.flow外阴);
            this.layoutControl1.Controls.Add(this.txt乳房右侧);
            this.layoutControl1.Controls.Add(this.txt乳房左侧);
            this.layoutControl1.Controls.Add(this.txt压痛);
            this.layoutControl1.Controls.Add(this.txt结节);
            this.layoutControl1.Controls.Add(this.txt乳房);
            this.layoutControl1.Controls.Add(this.txt不孕史女);
            this.layoutControl1.Controls.Add(this.txt不孕史男);
            this.layoutControl1.Controls.Add(this.txt不孕史时间);
            this.layoutControl1.Controls.Add(this.chk不孕史);
            this.layoutControl1.Controls.Add(this.txt中孕引产);
            this.layoutControl1.Controls.Add(this.txt人工流产);
            this.layoutControl1.Controls.Add(this.txt自然流产);
            this.layoutControl1.Controls.Add(this.txt产次);
            this.layoutControl1.Controls.Add(this.txt孕次);
            this.layoutControl1.Controls.Add(this.chk未孕);
            this.layoutControl1.Controls.Add(this.txt曾做手术);
            this.layoutControl1.Controls.Add(this.txt曾患妇科病);
            this.layoutControl1.Controls.Add(this.txt现有症状);
            this.layoutControl1.Controls.Add(this.txt检查次数);
            this.layoutControl1.Controls.Add(this.txt联系电话);
            this.layoutControl1.Controls.Add(this.txt身份证号);
            this.layoutControl1.Controls.Add(this.txt居住状态);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt卡号);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.dte检查日期);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt档案编号);
            this.layoutControl1.Controls.Add(this.radio分级);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(748, 466);
            this.layoutControl1.TabIndex = 5;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt病理检查
            // 
            this.txt病理检查.Location = new System.Drawing.Point(180, 721);
            this.txt病理检查.Name = "txt病理检查";
            this.txt病理检查.Size = new System.Drawing.Size(548, 20);
            this.txt病理检查.StyleController = this.layoutControl1;
            this.txt病理检查.TabIndex = 81;
            // 
            // txtTCT结果
            // 
            this.txtTCT结果.Location = new System.Drawing.Point(180, 697);
            this.txtTCT结果.Name = "txtTCT结果";
            this.txtTCT结果.Size = new System.Drawing.Size(548, 20);
            this.txtTCT结果.StyleController = this.layoutControl1;
            this.txtTCT结果.TabIndex = 80;
            // 
            // txt乳腺彩超
            // 
            this.txt乳腺彩超.Location = new System.Drawing.Point(184, 425);
            this.txt乳腺彩超.Name = "txt乳腺彩超";
            this.txt乳腺彩超.Size = new System.Drawing.Size(544, 20);
            this.txt乳腺彩超.StyleController = this.layoutControl1;
            this.txt乳腺彩超.TabIndex = 79;
            // 
            // txt钼靶检查
            // 
            this.txt钼靶检查.Location = new System.Drawing.Point(184, 449);
            this.txt钼靶检查.Name = "txt钼靶检查";
            this.txt钼靶检查.Size = new System.Drawing.Size(544, 20);
            this.txt钼靶检查.StyleController = this.layoutControl1;
            this.txt钼靶检查.TabIndex = 78;
            // 
            // txt最近修改人
            // 
            this.txt最近修改人.Location = new System.Drawing.Point(539, 867);
            this.txt最近修改人.Name = "txt最近修改人";
            this.txt最近修改人.Properties.ReadOnly = true;
            this.txt最近修改人.Size = new System.Drawing.Size(186, 20);
            this.txt最近修改人.StyleController = this.layoutControl1;
            this.txt最近修改人.TabIndex = 77;
            // 
            // txt创建人
            // 
            this.txt创建人.Location = new System.Drawing.Point(324, 867);
            this.txt创建人.Name = "txt创建人";
            this.txt创建人.Properties.ReadOnly = true;
            this.txt创建人.Size = new System.Drawing.Size(112, 20);
            this.txt创建人.StyleController = this.layoutControl1;
            this.txt创建人.TabIndex = 76;
            // 
            // txt当前所属机构
            // 
            this.txt当前所属机构.Location = new System.Drawing.Point(539, 843);
            this.txt当前所属机构.Name = "txt当前所属机构";
            this.txt当前所属机构.Properties.ReadOnly = true;
            this.txt当前所属机构.Size = new System.Drawing.Size(186, 20);
            this.txt当前所属机构.StyleController = this.layoutControl1;
            this.txt当前所属机构.TabIndex = 75;
            // 
            // txt最近更新时间
            // 
            this.txt最近更新时间.Location = new System.Drawing.Point(324, 843);
            this.txt最近更新时间.Name = "txt最近更新时间";
            this.txt最近更新时间.Properties.ReadOnly = true;
            this.txt最近更新时间.Size = new System.Drawing.Size(112, 20);
            this.txt最近更新时间.StyleController = this.layoutControl1;
            this.txt最近更新时间.TabIndex = 74;
            // 
            // txt创建机构
            // 
            this.txt创建机构.Location = new System.Drawing.Point(105, 867);
            this.txt创建机构.Name = "txt创建机构";
            this.txt创建机构.Properties.ReadOnly = true;
            this.txt创建机构.Size = new System.Drawing.Size(116, 20);
            this.txt创建机构.StyleController = this.layoutControl1;
            this.txt创建机构.TabIndex = 73;
            // 
            // txt创建时间
            // 
            this.txt创建时间.Location = new System.Drawing.Point(105, 843);
            this.txt创建时间.Name = "txt创建时间";
            this.txt创建时间.Properties.ReadOnly = true;
            this.txt创建时间.Size = new System.Drawing.Size(116, 20);
            this.txt创建时间.StyleController = this.layoutControl1;
            this.txt创建时间.TabIndex = 72;
            // 
            // txt医师签名
            // 
            this.txt医师签名.Location = new System.Drawing.Point(466, 816);
            this.txt医师签名.Name = "txt医师签名";
            this.txt医师签名.Size = new System.Drawing.Size(262, 20);
            this.txt医师签名.StyleController = this.layoutControl1;
            this.txt医师签名.TabIndex = 71;
            // 
            // txt治疗情况
            // 
            this.txt治疗情况.Location = new System.Drawing.Point(83, 816);
            this.txt治疗情况.Name = "txt治疗情况";
            this.txt治疗情况.Size = new System.Drawing.Size(280, 20);
            this.txt治疗情况.StyleController = this.layoutControl1;
            this.txt治疗情况.TabIndex = 70;
            // 
            // txt处理意见
            // 
            this.txt处理意见.Location = new System.Drawing.Point(83, 776);
            this.txt处理意见.Name = "txt处理意见";
            this.txt处理意见.Size = new System.Drawing.Size(645, 36);
            this.txt处理意见.StyleController = this.layoutControl1;
            this.txt处理意见.TabIndex = 69;
            this.txt处理意见.UseOptimizedRendering = true;
            // 
            // txt印象
            // 
            this.txt印象.Location = new System.Drawing.Point(83, 747);
            this.txt印象.Name = "txt印象";
            this.txt印象.Size = new System.Drawing.Size(280, 20);
            this.txt印象.StyleController = this.layoutControl1;
            this.txt印象.TabIndex = 68;
            // 
            // txt阴道异常情况
            // 
            this.txt阴道异常情况.Location = new System.Drawing.Point(180, 673);
            this.txt阴道异常情况.Name = "txt阴道异常情况";
            this.txt阴道异常情况.Size = new System.Drawing.Size(548, 20);
            this.txt阴道异常情况.StyleController = this.layoutControl1;
            this.txt阴道异常情况.TabIndex = 59;
            // 
            // flow附件
            // 
            this.flow附件.Controls.Add(this.checkEdit31);
            this.flow附件.Controls.Add(this.checkEdit32);
            this.flow附件.Controls.Add(this.checkEdit33);
            this.flow附件.Controls.Add(this.checkEdit34);
            this.flow附件.Controls.Add(this.checkEdit35);
            this.flow附件.Location = new System.Drawing.Point(180, 649);
            this.flow附件.Name = "flow附件";
            this.flow附件.Size = new System.Drawing.Size(548, 20);
            this.flow附件.TabIndex = 53;
            // 
            // checkEdit31
            // 
            this.checkEdit31.Location = new System.Drawing.Point(0, 0);
            this.checkEdit31.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit31.Name = "checkEdit31";
            this.checkEdit31.Properties.Caption = "左侧";
            this.checkEdit31.Properties.ReadOnly = true;
            this.checkEdit31.Size = new System.Drawing.Size(53, 19);
            this.checkEdit31.TabIndex = 0;
            this.checkEdit31.Tag = "1";
            // 
            // checkEdit32
            // 
            this.checkEdit32.Location = new System.Drawing.Point(53, 0);
            this.checkEdit32.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit32.Name = "checkEdit32";
            this.checkEdit32.Properties.Caption = "右侧";
            this.checkEdit32.Properties.ReadOnly = true;
            this.checkEdit32.Size = new System.Drawing.Size(53, 19);
            this.checkEdit32.TabIndex = 1;
            this.checkEdit32.Tag = "2";
            // 
            // checkEdit33
            // 
            this.checkEdit33.Location = new System.Drawing.Point(106, 0);
            this.checkEdit33.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit33.Name = "checkEdit33";
            this.checkEdit33.Properties.Caption = "压痛";
            this.checkEdit33.Properties.ReadOnly = true;
            this.checkEdit33.Size = new System.Drawing.Size(53, 19);
            this.checkEdit33.TabIndex = 2;
            this.checkEdit33.Tag = "3";
            // 
            // checkEdit34
            // 
            this.checkEdit34.Location = new System.Drawing.Point(159, 0);
            this.checkEdit34.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit34.Name = "checkEdit34";
            this.checkEdit34.Properties.Caption = "增厚";
            this.checkEdit34.Properties.ReadOnly = true;
            this.checkEdit34.Size = new System.Drawing.Size(53, 19);
            this.checkEdit34.TabIndex = 3;
            this.checkEdit34.Tag = "4";
            // 
            // checkEdit35
            // 
            this.checkEdit35.EditValue = true;
            this.checkEdit35.Location = new System.Drawing.Point(212, 0);
            this.checkEdit35.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit35.Name = "checkEdit35";
            this.checkEdit35.Properties.Caption = "未见异常";
            this.checkEdit35.Size = new System.Drawing.Size(90, 19);
            this.checkEdit35.TabIndex = 4;
            this.checkEdit35.Tag = "99";
            this.checkEdit35.CheckedChanged += new System.EventHandler(this.checkEdit35_CheckedChanged);
            // 
            // txt右卵巢
            // 
            this.txt右卵巢.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt右卵巢.Lbl1Text = "cm";
            this.txt右卵巢.Location = new System.Drawing.Point(610, 625);
            this.txt右卵巢.Name = "txt右卵巢";
            this.txt右卵巢.Size = new System.Drawing.Size(118, 20);
            this.txt右卵巢.TabIndex = 58;
            this.txt右卵巢.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt左卵巢
            // 
            this.txt左卵巢.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt左卵巢.Lbl1Text = "cm";
            this.txt左卵巢.Location = new System.Drawing.Point(405, 625);
            this.txt左卵巢.Name = "txt左卵巢";
            this.txt左卵巢.Size = new System.Drawing.Size(136, 20);
            this.txt左卵巢.TabIndex = 57;
            this.txt左卵巢.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // txt子宫体大小
            // 
            this.txt子宫体大小.Location = new System.Drawing.Point(180, 625);
            this.txt子宫体大小.Name = "txt子宫体大小";
            this.txt子宫体大小.Size = new System.Drawing.Size(136, 20);
            this.txt子宫体大小.StyleController = this.layoutControl1;
            this.txt子宫体大小.TabIndex = 56;
            // 
            // txt子宫脱垂
            // 
            this.txt子宫脱垂.Location = new System.Drawing.Point(405, 601);
            this.txt子宫脱垂.Name = "txt子宫脱垂";
            this.txt子宫脱垂.Size = new System.Drawing.Size(323, 20);
            this.txt子宫脱垂.StyleController = this.layoutControl1;
            this.txt子宫脱垂.TabIndex = 55;
            // 
            // txt子宫体活动
            // 
            this.txt子宫体活动.Location = new System.Drawing.Point(180, 601);
            this.txt子宫体活动.Name = "txt子宫体活动";
            this.txt子宫体活动.Size = new System.Drawing.Size(136, 20);
            this.txt子宫体活动.StyleController = this.layoutControl1;
            this.txt子宫体活动.TabIndex = 54;
            // 
            // flow子宫颈
            // 
            this.flow子宫颈.Controls.Add(this.checkEdit21);
            this.flow子宫颈.Controls.Add(this.checkEdit22);
            this.flow子宫颈.Controls.Add(this.checkEdit23);
            this.flow子宫颈.Controls.Add(this.checkEdit24);
            this.flow子宫颈.Controls.Add(this.checkEdit25);
            this.flow子宫颈.Controls.Add(this.checkEdit26);
            this.flow子宫颈.Controls.Add(this.checkEdit27);
            this.flow子宫颈.Controls.Add(this.checkEdit28);
            this.flow子宫颈.Controls.Add(this.checkEdit29);
            this.flow子宫颈.Controls.Add(this.checkEdit30);
            this.flow子宫颈.Location = new System.Drawing.Point(180, 561);
            this.flow子宫颈.Name = "flow子宫颈";
            this.flow子宫颈.Size = new System.Drawing.Size(548, 36);
            this.flow子宫颈.TabIndex = 53;
            // 
            // checkEdit21
            // 
            this.checkEdit21.EditValue = true;
            this.checkEdit21.Location = new System.Drawing.Point(0, 0);
            this.checkEdit21.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "光滑";
            this.checkEdit21.Size = new System.Drawing.Size(45, 19);
            this.checkEdit21.TabIndex = 0;
            this.checkEdit21.Tag = "1";
            this.checkEdit21.CheckedChanged += new System.EventHandler(this.checkEdit21_CheckedChanged);
            // 
            // checkEdit22
            // 
            this.checkEdit22.Location = new System.Drawing.Point(45, 0);
            this.checkEdit22.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit22.Name = "checkEdit22";
            this.checkEdit22.Properties.Caption = "肥大";
            this.checkEdit22.Properties.ReadOnly = true;
            this.checkEdit22.Size = new System.Drawing.Size(45, 19);
            this.checkEdit22.TabIndex = 1;
            this.checkEdit22.Tag = "2";
            // 
            // checkEdit23
            // 
            this.checkEdit23.Location = new System.Drawing.Point(90, 0);
            this.checkEdit23.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit23.Name = "checkEdit23";
            this.checkEdit23.Properties.Caption = "充血";
            this.checkEdit23.Properties.ReadOnly = true;
            this.checkEdit23.Size = new System.Drawing.Size(45, 19);
            this.checkEdit23.TabIndex = 2;
            this.checkEdit23.Tag = "3";
            // 
            // checkEdit24
            // 
            this.checkEdit24.Location = new System.Drawing.Point(135, 0);
            this.checkEdit24.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit24.Name = "checkEdit24";
            this.checkEdit24.Properties.Caption = "上下唇糜烂";
            this.checkEdit24.Properties.ReadOnly = true;
            this.checkEdit24.Size = new System.Drawing.Size(84, 19);
            this.checkEdit24.TabIndex = 3;
            this.checkEdit24.Tag = "4";
            // 
            // checkEdit25
            // 
            this.checkEdit25.Location = new System.Drawing.Point(219, 0);
            this.checkEdit25.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit25.Name = "checkEdit25";
            this.checkEdit25.Properties.Caption = "肿瘤";
            this.checkEdit25.Properties.ReadOnly = true;
            this.checkEdit25.Size = new System.Drawing.Size(45, 19);
            this.checkEdit25.TabIndex = 4;
            this.checkEdit25.Tag = "5";
            // 
            // checkEdit26
            // 
            this.checkEdit26.Location = new System.Drawing.Point(264, 0);
            this.checkEdit26.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit26.Name = "checkEdit26";
            this.checkEdit26.Properties.Caption = "易出血";
            this.checkEdit26.Properties.ReadOnly = true;
            this.checkEdit26.Size = new System.Drawing.Size(61, 19);
            this.checkEdit26.TabIndex = 5;
            this.checkEdit26.Tag = "6";
            // 
            // checkEdit27
            // 
            this.checkEdit27.Location = new System.Drawing.Point(325, 0);
            this.checkEdit27.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit27.Name = "checkEdit27";
            this.checkEdit27.Properties.Caption = "外翻";
            this.checkEdit27.Properties.ReadOnly = true;
            this.checkEdit27.Size = new System.Drawing.Size(45, 19);
            this.checkEdit27.TabIndex = 6;
            this.checkEdit27.Tag = "7";
            // 
            // checkEdit28
            // 
            this.checkEdit28.Location = new System.Drawing.Point(370, 0);
            this.checkEdit28.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit28.Name = "checkEdit28";
            this.checkEdit28.Properties.Caption = "旧裂";
            this.checkEdit28.Properties.ReadOnly = true;
            this.checkEdit28.Size = new System.Drawing.Size(45, 19);
            this.checkEdit28.TabIndex = 7;
            this.checkEdit28.Tag = "8";
            // 
            // checkEdit29
            // 
            this.checkEdit29.Location = new System.Drawing.Point(415, 0);
            this.checkEdit29.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit29.Name = "checkEdit29";
            this.checkEdit29.Properties.Caption = "腺体囊肿";
            this.checkEdit29.Properties.ReadOnly = true;
            this.checkEdit29.Size = new System.Drawing.Size(77, 19);
            this.checkEdit29.TabIndex = 8;
            this.checkEdit29.Tag = "9";
            // 
            // checkEdit30
            // 
            this.checkEdit30.Location = new System.Drawing.Point(492, 0);
            this.checkEdit30.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit30.Name = "checkEdit30";
            this.checkEdit30.Properties.Caption = "息肉";
            this.checkEdit30.Properties.ReadOnly = true;
            this.checkEdit30.Size = new System.Drawing.Size(45, 19);
            this.checkEdit30.TabIndex = 9;
            this.checkEdit30.Tag = "10";
            // 
            // flow白带性质
            // 
            this.flow白带性质.Controls.Add(this.chk白带_水样);
            this.flow白带性质.Controls.Add(this.chk白带_白);
            this.flow白带性质.Controls.Add(this.chk白带_黄色);
            this.flow白带性质.Controls.Add(this.chk白带_侬性);
            this.flow白带性质.Controls.Add(this.chk白带_血样);
            this.flow白带性质.Controls.Add(this.chk白带_泡沫);
            this.flow白带性质.Controls.Add(this.chk白带_渣样);
            this.flow白带性质.Controls.Add(this.chk白带_臭味);
            this.flow白带性质.Controls.Add(this.chk白带性质);
            this.flow白带性质.Location = new System.Drawing.Point(180, 521);
            this.flow白带性质.Name = "flow白带性质";
            this.flow白带性质.Size = new System.Drawing.Size(548, 36);
            this.flow白带性质.TabIndex = 52;
            // 
            // chk白带_水样
            // 
            this.chk白带_水样.Location = new System.Drawing.Point(0, 0);
            this.chk白带_水样.Margin = new System.Windows.Forms.Padding(0);
            this.chk白带_水样.Name = "chk白带_水样";
            this.chk白带_水样.Properties.Caption = "水样";
            this.chk白带_水样.Properties.ReadOnly = true;
            this.chk白带_水样.Size = new System.Drawing.Size(53, 19);
            this.chk白带_水样.TabIndex = 0;
            this.chk白带_水样.Tag = "1";
            // 
            // chk白带_白
            // 
            this.chk白带_白.Location = new System.Drawing.Point(53, 0);
            this.chk白带_白.Margin = new System.Windows.Forms.Padding(0);
            this.chk白带_白.Name = "chk白带_白";
            this.chk白带_白.Properties.Caption = "白";
            this.chk白带_白.Properties.ReadOnly = true;
            this.chk白带_白.Size = new System.Drawing.Size(53, 19);
            this.chk白带_白.TabIndex = 1;
            this.chk白带_白.Tag = "2";
            // 
            // chk白带_黄色
            // 
            this.chk白带_黄色.Location = new System.Drawing.Point(106, 0);
            this.chk白带_黄色.Margin = new System.Windows.Forms.Padding(0);
            this.chk白带_黄色.Name = "chk白带_黄色";
            this.chk白带_黄色.Properties.Caption = "黄色";
            this.chk白带_黄色.Properties.ReadOnly = true;
            this.chk白带_黄色.Size = new System.Drawing.Size(53, 19);
            this.chk白带_黄色.TabIndex = 2;
            this.chk白带_黄色.Tag = "3";
            // 
            // chk白带_侬性
            // 
            this.chk白带_侬性.Location = new System.Drawing.Point(159, 0);
            this.chk白带_侬性.Margin = new System.Windows.Forms.Padding(0);
            this.chk白带_侬性.Name = "chk白带_侬性";
            this.chk白带_侬性.Properties.Caption = "脓性";
            this.chk白带_侬性.Properties.ReadOnly = true;
            this.chk白带_侬性.Size = new System.Drawing.Size(53, 19);
            this.chk白带_侬性.TabIndex = 3;
            this.chk白带_侬性.Tag = "4";
            // 
            // chk白带_血样
            // 
            this.chk白带_血样.Location = new System.Drawing.Point(212, 0);
            this.chk白带_血样.Margin = new System.Windows.Forms.Padding(0);
            this.chk白带_血样.Name = "chk白带_血样";
            this.chk白带_血样.Properties.Caption = "血样";
            this.chk白带_血样.Properties.ReadOnly = true;
            this.chk白带_血样.Size = new System.Drawing.Size(53, 19);
            this.chk白带_血样.TabIndex = 4;
            this.chk白带_血样.Tag = "5";
            // 
            // chk白带_泡沫
            // 
            this.chk白带_泡沫.Location = new System.Drawing.Point(265, 0);
            this.chk白带_泡沫.Margin = new System.Windows.Forms.Padding(0);
            this.chk白带_泡沫.Name = "chk白带_泡沫";
            this.chk白带_泡沫.Properties.Caption = "泡沫";
            this.chk白带_泡沫.Properties.ReadOnly = true;
            this.chk白带_泡沫.Size = new System.Drawing.Size(53, 19);
            this.chk白带_泡沫.TabIndex = 5;
            this.chk白带_泡沫.Tag = "6";
            // 
            // chk白带_渣样
            // 
            this.chk白带_渣样.Location = new System.Drawing.Point(318, 0);
            this.chk白带_渣样.Margin = new System.Windows.Forms.Padding(0);
            this.chk白带_渣样.Name = "chk白带_渣样";
            this.chk白带_渣样.Properties.Caption = "渣样";
            this.chk白带_渣样.Properties.ReadOnly = true;
            this.chk白带_渣样.Size = new System.Drawing.Size(53, 19);
            this.chk白带_渣样.TabIndex = 6;
            this.chk白带_渣样.Tag = "7";
            // 
            // chk白带_臭味
            // 
            this.chk白带_臭味.Location = new System.Drawing.Point(371, 0);
            this.chk白带_臭味.Margin = new System.Windows.Forms.Padding(0);
            this.chk白带_臭味.Name = "chk白带_臭味";
            this.chk白带_臭味.Properties.Caption = "臭味";
            this.chk白带_臭味.Properties.ReadOnly = true;
            this.chk白带_臭味.Size = new System.Drawing.Size(53, 19);
            this.chk白带_臭味.TabIndex = 7;
            this.chk白带_臭味.Tag = "8";
            // 
            // chk白带性质
            // 
            this.chk白带性质.EditValue = true;
            this.chk白带性质.Location = new System.Drawing.Point(424, 0);
            this.chk白带性质.Margin = new System.Windows.Forms.Padding(0);
            this.chk白带性质.Name = "chk白带性质";
            this.chk白带性质.Properties.Caption = "正常";
            this.chk白带性质.Size = new System.Drawing.Size(53, 19);
            this.chk白带性质.TabIndex = 8;
            this.chk白带性质.Tag = "99";
            this.chk白带性质.CheckedChanged += new System.EventHandler(this.chk白带性质_CheckedChanged);
            // 
            // txt白带
            // 
            this.txt白带.Location = new System.Drawing.Point(489, 497);
            this.txt白带.Name = "txt白带";
            this.txt白带.Size = new System.Drawing.Size(239, 20);
            this.txt白带.StyleController = this.layoutControl1;
            this.txt白带.TabIndex = 53;
            // 
            // txt阴道
            // 
            this.txt阴道.Location = new System.Drawing.Point(180, 497);
            this.txt阴道.Name = "txt阴道";
            this.txt阴道.Size = new System.Drawing.Size(220, 20);
            this.txt阴道.StyleController = this.layoutControl1;
            this.txt阴道.TabIndex = 52;
            // 
            // flow外阴
            // 
            this.flow外阴.Controls.Add(this.checkEdit5);
            this.flow外阴.Controls.Add(this.checkEdit6);
            this.flow外阴.Controls.Add(this.checkEdit7);
            this.flow外阴.Controls.Add(this.checkEdit8);
            this.flow外阴.Controls.Add(this.checkEdit9);
            this.flow外阴.Controls.Add(this.checkEdit10);
            this.flow外阴.Controls.Add(this.checkEdit11);
            this.flow外阴.Location = new System.Drawing.Point(180, 473);
            this.flow外阴.Name = "flow外阴";
            this.flow外阴.Size = new System.Drawing.Size(548, 20);
            this.flow外阴.TabIndex = 51;
            // 
            // checkEdit5
            // 
            this.checkEdit5.EditValue = true;
            this.checkEdit5.Location = new System.Drawing.Point(0, 0);
            this.checkEdit5.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "正常";
            this.checkEdit5.Size = new System.Drawing.Size(53, 19);
            this.checkEdit5.TabIndex = 0;
            this.checkEdit5.Tag = "1";
            this.checkEdit5.CheckedChanged += new System.EventHandler(this.checkEdit5_CheckedChanged);
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(53, 0);
            this.checkEdit6.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "炎症";
            this.checkEdit6.Size = new System.Drawing.Size(53, 19);
            this.checkEdit6.TabIndex = 1;
            this.checkEdit6.Tag = "2";
            // 
            // checkEdit7
            // 
            this.checkEdit7.Location = new System.Drawing.Point(106, 0);
            this.checkEdit7.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "萎缩";
            this.checkEdit7.Size = new System.Drawing.Size(53, 19);
            this.checkEdit7.TabIndex = 2;
            this.checkEdit7.Tag = "3";
            // 
            // checkEdit8
            // 
            this.checkEdit8.Location = new System.Drawing.Point(159, 0);
            this.checkEdit8.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "瘙痒";
            this.checkEdit8.Size = new System.Drawing.Size(53, 19);
            this.checkEdit8.TabIndex = 3;
            this.checkEdit8.Tag = "4";
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(212, 0);
            this.checkEdit9.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "白斑";
            this.checkEdit9.Size = new System.Drawing.Size(53, 19);
            this.checkEdit9.TabIndex = 4;
            this.checkEdit9.Tag = "5";
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(265, 0);
            this.checkEdit10.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "溃疡";
            this.checkEdit10.Size = new System.Drawing.Size(53, 19);
            this.checkEdit10.TabIndex = 5;
            this.checkEdit10.Tag = "6";
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(318, 0);
            this.checkEdit11.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "肿瘤";
            this.checkEdit11.Size = new System.Drawing.Size(53, 19);
            this.checkEdit11.TabIndex = 6;
            this.checkEdit11.Tag = "7";
            // 
            // txt乳房右侧
            // 
            this.txt乳房右侧.Location = new System.Drawing.Point(493, 401);
            this.txt乳房右侧.Name = "txt乳房右侧";
            this.txt乳房右侧.Size = new System.Drawing.Size(235, 20);
            this.txt乳房右侧.StyleController = this.layoutControl1;
            this.txt乳房右侧.TabIndex = 47;
            // 
            // txt乳房左侧
            // 
            this.txt乳房左侧.Location = new System.Drawing.Point(180, 401);
            this.txt乳房左侧.Name = "txt乳房左侧";
            this.txt乳房左侧.Size = new System.Drawing.Size(224, 20);
            this.txt乳房左侧.StyleController = this.layoutControl1;
            this.txt乳房左侧.TabIndex = 46;
            // 
            // txt压痛
            // 
            this.txt压痛.Location = new System.Drawing.Point(180, 377);
            this.txt压痛.Name = "txt压痛";
            this.txt压痛.Size = new System.Drawing.Size(548, 20);
            this.txt压痛.StyleController = this.layoutControl1;
            this.txt压痛.TabIndex = 45;
            // 
            // txt结节
            // 
            this.txt结节.Location = new System.Drawing.Point(503, 353);
            this.txt结节.Name = "txt结节";
            this.txt结节.Size = new System.Drawing.Size(225, 20);
            this.txt结节.StyleController = this.layoutControl1;
            this.txt结节.TabIndex = 44;
            // 
            // txt乳房
            // 
            this.txt乳房.Location = new System.Drawing.Point(180, 353);
            this.txt乳房.Name = "txt乳房";
            this.txt乳房.Size = new System.Drawing.Size(224, 20);
            this.txt乳房.StyleController = this.layoutControl1;
            this.txt乳房.TabIndex = 43;
            // 
            // txt不孕史女
            // 
            this.txt不孕史女.Location = new System.Drawing.Point(262, 329);
            this.txt不孕史女.Name = "txt不孕史女";
            this.txt不孕史女.Properties.ReadOnly = true;
            this.txt不孕史女.Size = new System.Drawing.Size(466, 20);
            this.txt不孕史女.StyleController = this.layoutControl1;
            this.txt不孕史女.TabIndex = 39;
            // 
            // txt不孕史男
            // 
            this.txt不孕史男.Location = new System.Drawing.Point(262, 305);
            this.txt不孕史男.Name = "txt不孕史男";
            this.txt不孕史男.Properties.ReadOnly = true;
            this.txt不孕史男.Size = new System.Drawing.Size(466, 20);
            this.txt不孕史男.StyleController = this.layoutControl1;
            this.txt不孕史男.TabIndex = 38;
            // 
            // txt不孕史时间
            // 
            this.txt不孕史时间.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt不孕史时间.Lbl1Text = "年";
            this.txt不孕史时间.Location = new System.Drawing.Point(180, 281);
            this.txt不孕史时间.Name = "txt不孕史时间";
            this.txt不孕史时间.Size = new System.Drawing.Size(548, 20);
            this.txt不孕史时间.TabIndex = 37;
            this.txt不孕史时间.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // chk不孕史
            // 
            this.chk不孕史.EditValue = true;
            this.chk不孕史.Location = new System.Drawing.Point(85, 258);
            this.chk不孕史.Name = "chk不孕史";
            this.chk不孕史.Properties.Caption = "无";
            this.chk不孕史.Size = new System.Drawing.Size(643, 19);
            this.chk不孕史.StyleController = this.layoutControl1;
            this.chk不孕史.TabIndex = 36;
            this.chk不孕史.CheckedChanged += new System.EventHandler(this.chk不孕史_CheckedChanged);
            // 
            // txt中孕引产
            // 
            this.txt中孕引产.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt中孕引产.Lbl1Text = "次";
            this.txt中孕引产.Location = new System.Drawing.Point(566, 234);
            this.txt中孕引产.Name = "txt中孕引产";
            this.txt中孕引产.Size = new System.Drawing.Size(162, 20);
            this.txt中孕引产.TabIndex = 35;
            this.txt中孕引产.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt人工流产
            // 
            this.txt人工流产.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt人工流产.Lbl1Text = "次";
            this.txt人工流产.Location = new System.Drawing.Point(369, 234);
            this.txt人工流产.Name = "txt人工流产";
            this.txt人工流产.Size = new System.Drawing.Size(98, 20);
            this.txt人工流产.TabIndex = 34;
            this.txt人工流产.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt自然流产
            // 
            this.txt自然流产.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt自然流产.Lbl1Text = "次";
            this.txt自然流产.Location = new System.Drawing.Point(180, 234);
            this.txt自然流产.Name = "txt自然流产";
            this.txt自然流产.Size = new System.Drawing.Size(90, 20);
            this.txt自然流产.TabIndex = 33;
            this.txt自然流产.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt产次
            // 
            this.txt产次.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt产次.Lbl1Text = "次";
            this.txt产次.Location = new System.Drawing.Point(369, 210);
            this.txt产次.Name = "txt产次";
            this.txt产次.Size = new System.Drawing.Size(359, 20);
            this.txt产次.TabIndex = 32;
            this.txt产次.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // txt孕次
            // 
            this.txt孕次.Lbl1Size = new System.Drawing.Size(20, 18);
            this.txt孕次.Lbl1Text = "次";
            this.txt孕次.Location = new System.Drawing.Point(180, 210);
            this.txt孕次.Name = "txt孕次";
            this.txt孕次.Size = new System.Drawing.Size(90, 20);
            this.txt孕次.TabIndex = 31;
            this.txt孕次.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // chk未孕
            // 
            this.chk未孕.EditValue = true;
            this.chk未孕.Location = new System.Drawing.Point(85, 187);
            this.chk未孕.Name = "chk未孕";
            this.chk未孕.Properties.Caption = "未孕 ";
            this.chk未孕.Size = new System.Drawing.Size(146, 19);
            this.chk未孕.StyleController = this.layoutControl1;
            this.chk未孕.TabIndex = 30;
            this.chk未孕.CheckedChanged += new System.EventHandler(this.chk未孕_CheckedChanged);
            // 
            // txt曾做手术
            // 
            this.txt曾做手术.Location = new System.Drawing.Point(514, 163);
            this.txt曾做手术.Name = "txt曾做手术";
            this.txt曾做手术.Size = new System.Drawing.Size(214, 20);
            this.txt曾做手术.StyleController = this.layoutControl1;
            this.txt曾做手术.TabIndex = 16;
            // 
            // txt曾患妇科病
            // 
            this.txt曾患妇科病.Location = new System.Drawing.Point(180, 163);
            this.txt曾患妇科病.Name = "txt曾患妇科病";
            this.txt曾患妇科病.Size = new System.Drawing.Size(231, 20);
            this.txt曾患妇科病.StyleController = this.layoutControl1;
            this.txt曾患妇科病.TabIndex = 15;
            // 
            // txt现有症状
            // 
            this.txt现有症状.Location = new System.Drawing.Point(180, 139);
            this.txt现有症状.Name = "txt现有症状";
            this.txt现有症状.Size = new System.Drawing.Size(548, 20);
            this.txt现有症状.StyleController = this.layoutControl1;
            this.txt现有症状.TabIndex = 14;
            // 
            // txt检查次数
            // 
            this.txt检查次数.Lbl1Size = new System.Drawing.Size(69, 18);
            this.txt检查次数.Lbl1Text = "次";
            this.txt检查次数.Location = new System.Drawing.Point(307, 109);
            this.txt检查次数.Name = "txt检查次数";
            this.txt检查次数.Size = new System.Drawing.Size(416, 21);
            this.txt检查次数.TabIndex = 13;
            this.txt检查次数.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // txt联系电话
            // 
            this.txt联系电话.Location = new System.Drawing.Point(557, 60);
            this.txt联系电话.Name = "txt联系电话";
            this.txt联系电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系电话.Size = new System.Drawing.Size(166, 20);
            this.txt联系电话.StyleController = this.layoutControl1;
            this.txt联系电话.TabIndex = 11;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Location = new System.Drawing.Point(307, 60);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt身份证号.Properties.Appearance.Options.UseBackColor = true;
            this.txt身份证号.Size = new System.Drawing.Size(147, 20);
            this.txt身份证号.StyleController = this.layoutControl1;
            this.txt身份证号.TabIndex = 10;
            // 
            // txt居住状态
            // 
            this.txt居住状态.Location = new System.Drawing.Point(107, 85);
            this.txt居住状态.Name = "txt居住状态";
            this.txt居住状态.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住状态.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住状态.Size = new System.Drawing.Size(97, 20);
            this.txt居住状态.StyleController = this.layoutControl1;
            this.txt居住状态.TabIndex = 9;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(557, 35);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Size = new System.Drawing.Size(166, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 7;
            // 
            // txt卡号
            // 
            this.txt卡号.Location = new System.Drawing.Point(107, 35);
            this.txt卡号.Name = "txt卡号";
            this.txt卡号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt卡号.Properties.Appearance.Options.UseBackColor = true;
            this.txt卡号.Size = new System.Drawing.Size(97, 20);
            this.txt卡号.StyleController = this.layoutControl1;
            this.txt卡号.TabIndex = 5;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(307, 85);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Size = new System.Drawing.Size(416, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 4;
            // 
            // dte检查日期
            // 
            this.dte检查日期.EditValue = null;
            this.dte检查日期.Location = new System.Drawing.Point(107, 109);
            this.dte检查日期.Name = "dte检查日期";
            this.dte检查日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte检查日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte检查日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte检查日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte检查日期.Size = new System.Drawing.Size(97, 20);
            this.dte检查日期.StyleController = this.layoutControl1;
            this.dte检查日期.TabIndex = 12;
            // 
            // txt出生日期
            // 
            this.txt出生日期.EditValue = null;
            this.txt出生日期.Location = new System.Drawing.Point(107, 60);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt出生日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txt出生日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.txt出生日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt出生日期.Size = new System.Drawing.Size(97, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 8;
            // 
            // txt档案编号
            // 
            this.txt档案编号.Location = new System.Drawing.Point(307, 35);
            this.txt档案编号.Name = "txt档案编号";
            this.txt档案编号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt档案编号.Properties.Appearance.Options.UseBackColor = true;
            this.txt档案编号.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.txt档案编号.Size = new System.Drawing.Size(147, 20);
            this.txt档案编号.StyleController = this.layoutControl1;
            this.txt档案编号.TabIndex = 6;
            this.txt档案编号.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.txt档案编号_ButtonClick);
            // 
            // radio分级
            // 
            this.radio分级.Location = new System.Drawing.Point(466, 747);
            this.radio分级.Name = "radio分级";
            this.radio分级.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "一级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "二级"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("3", "三级")});
            this.radio分级.Size = new System.Drawing.Size(262, 25);
            this.radio分级.StyleController = this.layoutControl1;
            this.radio分级.TabIndex = 82;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "妇女分级管理";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 893);
            this.layoutControlGroup1.Text = "妇女分级管理";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(729, 109);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.layoutControlItem3,
            this.layoutControlItem8,
            this.layoutControlItem4});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2);
            this.layoutControlGroup4.Size = new System.Drawing.Size(729, 109);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txt卡号;
            this.layoutControlItem2.CustomizationFormText = "卡 号";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(200, 25);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "卡 号";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txt出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(200, 25);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txt居住状态;
            this.layoutControlItem6.CustomizationFormText = "居住状态 ";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "居住状态 ";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.Control = this.dte检查日期;
            this.layoutControlItem9.CustomizationFormText = "检查日期";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(200, 25);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "检查日期";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt检查次数;
            this.layoutControlItem10.CustomizationFormText = "检查次数";
            this.layoutControlItem10.Location = new System.Drawing.Point(200, 74);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(179, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(519, 25);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "检查次数";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt居住地址;
            this.layoutControlItem1.CustomizationFormText = "居住地址";
            this.layoutControlItem1.Location = new System.Drawing.Point(200, 50);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(519, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "居住地址";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt身份证号;
            this.layoutControlItem7.CustomizationFormText = "身份证号";
            this.layoutControlItem7.Location = new System.Drawing.Point(200, 25);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(250, 25);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "身份证号";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txt档案编号;
            this.layoutControlItem3.CustomizationFormText = "档案编号";
            this.layoutControlItem3.Location = new System.Drawing.Point(200, 0);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(250, 25);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "档案编号";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txt联系电话;
            this.layoutControlItem8.CustomizationFormText = "联系电话";
            this.layoutControlItem8.Location = new System.Drawing.Point(450, 25);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(269, 25);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "联系电话";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt姓名;
            this.layoutControlItem4.CustomizationFormText = "姓 名 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(450, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(269, 25);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "姓 名 ";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.emptySpaceItem6,
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.layoutControlItem40,
            this.layoutControlItem42,
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem48,
            this.layoutControlItem49,
            this.layoutControlItem50,
            this.layoutControlItem51,
            this.layoutControlItem52,
            this.layoutControlItem53,
            this.layoutControlItem54,
            this.layoutControlItem55,
            this.layoutControlItem56,
            this.layoutControlItem57,
            this.layoutControlItem58,
            this.layoutControlItem59,
            this.layoutControlItem68,
            this.layoutControlItem69,
            this.lbl治疗情况,
            this.lbl医师签名,
            this.layoutControlGroup6,
            this.layoutControlGroup9,
            this.layoutControlGroup10,
            this.layoutControlGroup11,
            this.layoutControlGroup13,
            this.layoutControlGroup14,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem41,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 109);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(729, 701);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txt现有症状;
            this.layoutControlItem11.CustomizationFormText = "现有症状 ";
            this.layoutControlItem11.Location = new System.Drawing.Point(82, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "现有症状 ";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txt曾患妇科病;
            this.layoutControlItem12.CustomizationFormText = "曾患妇科病 ";
            this.layoutControlItem12.Location = new System.Drawing.Point(82, 24);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(330, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(330, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(330, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "曾患妇科病 ";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txt曾做手术;
            this.layoutControlItem13.CustomizationFormText = "曾做手术";
            this.layoutControlItem13.Location = new System.Drawing.Point(412, 24);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(317, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "曾做手术";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.chk未孕;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(82, 48);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(647, 23);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "layoutControlItem27";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextToControlDistance = 0;
            this.layoutControlItem27.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.txt孕次;
            this.layoutControlItem28.CustomizationFormText = "孕次:";
            this.layoutControlItem28.Location = new System.Drawing.Point(82, 71);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(189, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "孕次:";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.txt产次;
            this.layoutControlItem29.CustomizationFormText = "产次:";
            this.layoutControlItem29.Location = new System.Drawing.Point(271, 71);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(189, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(458, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "产次:";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.txt自然流产;
            this.layoutControlItem30.CustomizationFormText = "自然流产: ";
            this.layoutControlItem30.Location = new System.Drawing.Point(82, 95);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(189, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(189, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "自然流产: ";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.txt人工流产;
            this.layoutControlItem31.CustomizationFormText = "人工流产:";
            this.layoutControlItem31.Location = new System.Drawing.Point(271, 95);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(189, 24);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(197, 24);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "人工流产:";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.txt中孕引产;
            this.layoutControlItem32.CustomizationFormText = "中孕引产:";
            this.layoutControlItem32.Location = new System.Drawing.Point(468, 95);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(189, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(261, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "中孕引产:";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "原 因";
            this.emptySpaceItem6.Location = new System.Drawing.Point(82, 166);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(100, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(100, 48);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "原 因";
            this.emptySpaceItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(90, 20);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.txt不孕史男;
            this.layoutControlItem35.CustomizationFormText = "男";
            this.layoutControlItem35.Location = new System.Drawing.Point(182, 166);
            this.layoutControlItem35.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(547, 24);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "男";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(72, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.txt不孕史女;
            this.layoutControlItem36.CustomizationFormText = "女";
            this.layoutControlItem36.Location = new System.Drawing.Point(182, 190);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(547, 24);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "女";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(72, 20);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.txt乳房;
            this.layoutControlItem40.CustomizationFormText = "乳房:";
            this.layoutControlItem40.Location = new System.Drawing.Point(82, 214);
            this.layoutControlItem40.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(323, 24);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "乳房:";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.txt压痛;
            this.layoutControlItem42.CustomizationFormText = "压痛:";
            this.layoutControlItem42.Location = new System.Drawing.Point(82, 238);
            this.layoutControlItem42.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Text = "压痛:";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.Control = this.txt乳房左侧;
            this.layoutControlItem43.CustomizationFormText = "乳房左侧:";
            this.layoutControlItem43.Location = new System.Drawing.Point(82, 262);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(323, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "乳房左侧:";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.Control = this.txt乳房右侧;
            this.layoutControlItem44.CustomizationFormText = "乳房右侧:";
            this.layoutControlItem44.Location = new System.Drawing.Point(405, 262);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "乳房右侧:";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.flow外阴;
            this.layoutControlItem48.CustomizationFormText = "外阴:";
            this.layoutControlItem48.Location = new System.Drawing.Point(82, 334);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(196, 24);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "外阴:";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.Control = this.txt阴道;
            this.layoutControlItem49.CustomizationFormText = "阴 道:";
            this.layoutControlItem49.Location = new System.Drawing.Point(82, 358);
            this.layoutControlItem49.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem49.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(319, 24);
            this.layoutControlItem49.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem49.Text = "阴 道:";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.Control = this.txt白带;
            this.layoutControlItem50.CustomizationFormText = "白 带:";
            this.layoutControlItem50.Location = new System.Drawing.Point(401, 358);
            this.layoutControlItem50.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem50.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(328, 24);
            this.layoutControlItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem50.Text = "白 带:";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.flow白带性质;
            this.layoutControlItem51.CustomizationFormText = "白带性质:";
            this.layoutControlItem51.Location = new System.Drawing.Point(82, 382);
            this.layoutControlItem51.MinSize = new System.Drawing.Size(196, 40);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(647, 40);
            this.layoutControlItem51.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem51.Text = "白带性质:";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.Control = this.flow子宫颈;
            this.layoutControlItem52.CustomizationFormText = "子 宫 颈:";
            this.layoutControlItem52.Location = new System.Drawing.Point(82, 422);
            this.layoutControlItem52.MinSize = new System.Drawing.Size(196, 40);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(647, 40);
            this.layoutControlItem52.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem52.Text = "子 宫 颈:";
            this.layoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem52.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem52.TextToControlDistance = 5;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.Control = this.txt子宫体活动;
            this.layoutControlItem53.CustomizationFormText = "子宫体活动:";
            this.layoutControlItem53.Location = new System.Drawing.Point(82, 462);
            this.layoutControlItem53.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem53.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem53.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem53.Text = "子宫体活动:";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.Control = this.txt子宫脱垂;
            this.layoutControlItem54.CustomizationFormText = "子宫脱垂:";
            this.layoutControlItem54.Location = new System.Drawing.Point(317, 462);
            this.layoutControlItem54.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(412, 24);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Text = "子宫脱垂:";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.Control = this.txt子宫体大小;
            this.layoutControlItem55.CustomizationFormText = "子宫体大小:";
            this.layoutControlItem55.Location = new System.Drawing.Point(82, 486);
            this.layoutControlItem55.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem55.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(235, 24);
            this.layoutControlItem55.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem55.Text = "子宫体大小:";
            this.layoutControlItem55.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem55.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.Control = this.txt左卵巢;
            this.layoutControlItem56.CustomizationFormText = "左卵巢:";
            this.layoutControlItem56.Location = new System.Drawing.Point(317, 486);
            this.layoutControlItem56.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(225, 24);
            this.layoutControlItem56.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem56.Text = "左卵巢:";
            this.layoutControlItem56.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem56.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem56.TextToControlDistance = 5;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.Control = this.txt右卵巢;
            this.layoutControlItem57.CustomizationFormText = "右卵巢:";
            this.layoutControlItem57.Location = new System.Drawing.Point(542, 486);
            this.layoutControlItem57.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(187, 24);
            this.layoutControlItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem57.Text = "右卵巢:";
            this.layoutControlItem57.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem57.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem57.TextToControlDistance = 5;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.Control = this.flow附件;
            this.layoutControlItem58.CustomizationFormText = "附件: ";
            this.layoutControlItem58.Location = new System.Drawing.Point(82, 510);
            this.layoutControlItem58.MinSize = new System.Drawing.Size(196, 24);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem58.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem58.Text = "附件: ";
            this.layoutControlItem58.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem58.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem58.TextToControlDistance = 5;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.Control = this.txt阴道异常情况;
            this.layoutControlItem59.CustomizationFormText = "阴道异常情况:";
            this.layoutControlItem59.Location = new System.Drawing.Point(82, 534);
            this.layoutControlItem59.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem59.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem59.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem59.Text = "阴道异常情况:";
            this.layoutControlItem59.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem59.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem59.TextToControlDistance = 5;
            // 
            // layoutControlItem68
            // 
            this.layoutControlItem68.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem68.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem68.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem68.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem68.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem68.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem68.Control = this.txt印象;
            this.layoutControlItem68.CustomizationFormText = "印 象";
            this.layoutControlItem68.Location = new System.Drawing.Point(0, 608);
            this.layoutControlItem68.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem68.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem68.Name = "layoutControlItem68";
            this.layoutControlItem68.Size = new System.Drawing.Size(364, 29);
            this.layoutControlItem68.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem68.Text = "印 象";
            this.layoutControlItem68.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem68.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem68.TextToControlDistance = 5;
            // 
            // layoutControlItem69
            // 
            this.layoutControlItem69.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem69.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem69.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem69.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem69.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem69.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem69.Control = this.txt处理意见;
            this.layoutControlItem69.CustomizationFormText = "处理意见";
            this.layoutControlItem69.Location = new System.Drawing.Point(0, 637);
            this.layoutControlItem69.MinSize = new System.Drawing.Size(106, 40);
            this.layoutControlItem69.Name = "layoutControlItem69";
            this.layoutControlItem69.Size = new System.Drawing.Size(729, 40);
            this.layoutControlItem69.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem69.Text = "处理意见";
            this.layoutControlItem69.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem69.TextSize = new System.Drawing.Size(75, 20);
            this.layoutControlItem69.TextToControlDistance = 5;
            // 
            // lbl治疗情况
            // 
            this.lbl治疗情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl治疗情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl治疗情况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl治疗情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl治疗情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl治疗情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl治疗情况.Control = this.txt治疗情况;
            this.lbl治疗情况.CustomizationFormText = "治疗情况";
            this.lbl治疗情况.Location = new System.Drawing.Point(0, 677);
            this.lbl治疗情况.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl治疗情况.MinSize = new System.Drawing.Size(146, 24);
            this.lbl治疗情况.Name = "lbl治疗情况";
            this.lbl治疗情况.Size = new System.Drawing.Size(364, 24);
            this.lbl治疗情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl治疗情况.Text = "治疗情况";
            this.lbl治疗情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl治疗情况.TextSize = new System.Drawing.Size(75, 20);
            this.lbl治疗情况.TextToControlDistance = 5;
            // 
            // lbl医师签名
            // 
            this.lbl医师签名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl医师签名.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl医师签名.AppearanceItemCaption.Options.UseFont = true;
            this.lbl医师签名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl医师签名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl医师签名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl医师签名.Control = this.txt医师签名;
            this.lbl医师签名.CustomizationFormText = "医师签名";
            this.lbl医师签名.Location = new System.Drawing.Point(364, 677);
            this.lbl医师签名.Name = "lbl医师签名";
            this.lbl医师签名.Size = new System.Drawing.Size(365, 24);
            this.lbl医师签名.Text = "医师签名";
            this.lbl医师签名.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Size = new System.Drawing.Size(82, 48);
            this.layoutControlGroup6.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            this.layoutControlGroup6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem1.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem1.CustomizationFormText = "健康状况";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(80, 46);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "健康状况";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(96, 0);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "layoutControlGroup9";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 48);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup9.Size = new System.Drawing.Size(82, 71);
            this.layoutControlGroup9.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup9.Text = "layoutControlGroup9";
            this.layoutControlGroup9.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem4.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem4.CustomizationFormText = "生 育 史";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(80, 69);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "生 育 史";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(96, 0);
            this.emptySpaceItem4.TextVisible = true;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "layoutControlGroup10";
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem5});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 119);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup10.Size = new System.Drawing.Size(82, 95);
            this.layoutControlGroup10.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup10.Text = "layoutControlGroup10";
            this.layoutControlGroup10.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem5.CustomizationFormText = "不 孕 史";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(80, 93);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "不 孕 史";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(96, 0);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "layoutControlGroup11";
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem7});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 214);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup11.Size = new System.Drawing.Size(82, 120);
            this.layoutControlGroup11.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup11.Text = "layoutControlGroup11";
            this.layoutControlGroup11.TextVisible = false;
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem7.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem7.CustomizationFormText = "一般检查";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(80, 118);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.Text = "乳腺检查";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(96, 0);
            this.emptySpaceItem7.TextVisible = true;
            // 
            // layoutControlGroup13
            // 
            this.layoutControlGroup13.CustomizationFormText = "layoutControlGroup13";
            this.layoutControlGroup13.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem9});
            this.layoutControlGroup13.Location = new System.Drawing.Point(0, 334);
            this.layoutControlGroup13.Name = "layoutControlGroup13";
            this.layoutControlGroup13.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup13.Size = new System.Drawing.Size(82, 272);
            this.layoutControlGroup13.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup13.Text = "layoutControlGroup13";
            this.layoutControlGroup13.TextVisible = false;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.emptySpaceItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem9.CustomizationFormText = "妇科检查";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(80, 270);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.Text = "妇科检查";
            this.emptySpaceItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem9.TextVisible = true;
            // 
            // layoutControlGroup14
            // 
            this.layoutControlGroup14.CustomizationFormText = "layoutControlGroup14";
            this.layoutControlGroup14.Location = new System.Drawing.Point(0, 606);
            this.layoutControlGroup14.Name = "layoutControlGroup14";
            this.layoutControlGroup14.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup14.Size = new System.Drawing.Size(729, 2);
            this.layoutControlGroup14.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup14.Text = "layoutControlGroup14";
            this.layoutControlGroup14.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.chk不孕史;
            this.layoutControlItem33.CustomizationFormText = "layoutControlItem33";
            this.layoutControlItem33.Location = new System.Drawing.Point(82, 119);
            this.layoutControlItem33.MaxSize = new System.Drawing.Size(0, 23);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(647, 23);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "layoutControlItem33";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextToControlDistance = 0;
            this.layoutControlItem33.TextVisible = false;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.txt不孕史时间;
            this.layoutControlItem34.CustomizationFormText = "时间";
            this.layoutControlItem34.Location = new System.Drawing.Point(82, 142);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(196, 24);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "时间";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txt钼靶检查;
            this.layoutControlItem14.CustomizationFormText = "钼靶检查结果";
            this.layoutControlItem14.Location = new System.Drawing.Point(82, 310);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem14.Text = "钼靶检查结果";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.txt乳腺彩超;
            this.layoutControlItem15.CustomizationFormText = "乳腺彩超检查结果";
            this.layoutControlItem15.Location = new System.Drawing.Point(82, 286);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem15.Text = "乳腺彩超检查结果";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.txt结节;
            this.layoutControlItem41.CustomizationFormText = "结节:";
            this.layoutControlItem41.Location = new System.Drawing.Point(405, 214);
            this.layoutControlItem41.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem41.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(324, 24);
            this.layoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem41.Text = "结节:";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.txtTCT结果;
            this.layoutControlItem16.CustomizationFormText = "TCT结果";
            this.layoutControlItem16.Location = new System.Drawing.Point(82, 558);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "TCT结果";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txt病理检查;
            this.layoutControlItem17.CustomizationFormText = "病理检查结果";
            this.layoutControlItem17.Location = new System.Drawing.Point(82, 582);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(146, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(647, 24);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "病理检查结果";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.Control = this.radio分级;
            this.layoutControlItem18.CustomizationFormText = "症状分级";
            this.layoutControlItem18.Location = new System.Drawing.Point(364, 608);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(365, 29);
            this.layoutControlItem18.Text = "分级";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem77,
            this.layoutControlItem75,
            this.layoutControlItem76,
            this.layoutControlItem74,
            this.layoutControlItem73,
            this.layoutControlItem72});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 810);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(729, 54);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem77
            // 
            this.layoutControlItem77.Control = this.txt最近修改人;
            this.layoutControlItem77.CustomizationFormText = "最近修改人 ";
            this.layoutControlItem77.Location = new System.Drawing.Point(434, 24);
            this.layoutControlItem77.Name = "layoutControlItem77";
            this.layoutControlItem77.Size = new System.Drawing.Size(289, 24);
            this.layoutControlItem77.Text = "最近修改人 ";
            this.layoutControlItem77.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem75
            // 
            this.layoutControlItem75.Control = this.txt当前所属机构;
            this.layoutControlItem75.CustomizationFormText = "当前所属机构";
            this.layoutControlItem75.Location = new System.Drawing.Point(434, 0);
            this.layoutControlItem75.Name = "layoutControlItem75";
            this.layoutControlItem75.Size = new System.Drawing.Size(289, 24);
            this.layoutControlItem75.Text = "当前所属机构";
            this.layoutControlItem75.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem76
            // 
            this.layoutControlItem76.Control = this.txt创建人;
            this.layoutControlItem76.CustomizationFormText = "创建人 ";
            this.layoutControlItem76.Location = new System.Drawing.Point(219, 24);
            this.layoutControlItem76.Name = "layoutControlItem76";
            this.layoutControlItem76.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem76.Text = "创建人 ";
            this.layoutControlItem76.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem74
            // 
            this.layoutControlItem74.Control = this.txt最近更新时间;
            this.layoutControlItem74.CustomizationFormText = "最近更新时间";
            this.layoutControlItem74.Location = new System.Drawing.Point(219, 0);
            this.layoutControlItem74.Name = "layoutControlItem74";
            this.layoutControlItem74.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem74.Text = "最近更新时间";
            this.layoutControlItem74.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem73
            // 
            this.layoutControlItem73.Control = this.txt创建机构;
            this.layoutControlItem73.CustomizationFormText = "创建机构 ";
            this.layoutControlItem73.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem73.Name = "layoutControlItem73";
            this.layoutControlItem73.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem73.Text = "创建机构 ";
            this.layoutControlItem73.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem72
            // 
            this.layoutControlItem72.Control = this.txt创建时间;
            this.layoutControlItem72.CustomizationFormText = "创建时间";
            this.layoutControlItem72.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem72.Name = "layoutControlItem72";
            this.layoutControlItem72.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem72.Text = "创建时间";
            this.layoutControlItem72.TextSize = new System.Drawing.Size(96, 14);
            // 
            // btn关闭
            // 
            this.btn关闭.Dock = System.Windows.Forms.DockStyle.Right;
            this.btn关闭.Image = ((System.Drawing.Image)(resources.GetObject("btn关闭.Image")));
            this.btn关闭.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btn关闭.Location = new System.Drawing.Point(705, 2);
            this.btn关闭.Name = "btn关闭";
            this.btn关闭.Size = new System.Drawing.Size(41, 28);
            this.btn关闭.TabIndex = 1;
            this.btn关闭.Click += new System.EventHandler(this.btn取消_Click);
            // 
            // UC妇女分级管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC妇女分级管理";
            this.Size = new System.Drawing.Size(748, 498);
            this.Load += new System.EventHandler(this.UC妇女保健检查表_Load);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt病理检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTCT结果.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳腺彩超.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt钼靶检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医师签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt治疗情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt处理意见.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt印象.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阴道异常情况.Properties)).EndInit();
            this.flow附件.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt子宫体大小.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt子宫脱垂.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt子宫体活动.Properties)).EndInit();
            this.flow子宫颈.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).EndInit();
            this.flow白带性质.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_水样.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_黄色.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_侬性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_血样.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_泡沫.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_渣样.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带_臭味.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk白带性质.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt白带.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt阴道.Properties)).EndInit();
            this.flow外阴.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳房右侧.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳房左侧.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt压痛.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt结节.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt乳房.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt不孕史女.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt不孕史男.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk不孕史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk未孕.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt曾做手术.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt曾患妇科病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt现有症状.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte检查日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte检查日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio分级.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl治疗情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl医师签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn取消;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txt联系电话;
        private DevExpress.XtraEditors.TextEdit txt身份证号;
        private DevExpress.XtraEditors.TextEdit txt居住状态;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt卡号;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private Library.UserControls.UCTxtLbl txt检查次数;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.TextEdit txt曾做手术;
        private DevExpress.XtraEditors.TextEdit txt曾患妇科病;
        private DevExpress.XtraEditors.TextEdit txt现有症状;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private Library.UserControls.UCTxtLbl txt中孕引产;
        private Library.UserControls.UCTxtLbl txt人工流产;
        private Library.UserControls.UCTxtLbl txt自然流产;
        private Library.UserControls.UCTxtLbl txt产次;
        private Library.UserControls.UCTxtLbl txt孕次;
        private DevExpress.XtraEditors.CheckEdit chk未孕;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraEditors.TextEdit txt不孕史女;
        private DevExpress.XtraEditors.TextEdit txt不孕史男;
        private Library.UserControls.UCTxtLbl txt不孕史时间;
        private DevExpress.XtraEditors.CheckEdit chk不孕史;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.TextEdit txt乳房右侧;
        private DevExpress.XtraEditors.TextEdit txt乳房左侧;
        private DevExpress.XtraEditors.TextEdit txt压痛;
        private DevExpress.XtraEditors.TextEdit txt结节;
        private DevExpress.XtraEditors.TextEdit txt乳房;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private System.Windows.Forms.FlowLayoutPanel flow外阴;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private System.Windows.Forms.FlowLayoutPanel flow白带性质;
        private DevExpress.XtraEditors.CheckEdit chk白带_水样;
        private DevExpress.XtraEditors.CheckEdit chk白带_白;
        private DevExpress.XtraEditors.CheckEdit chk白带_黄色;
        private DevExpress.XtraEditors.CheckEdit chk白带_侬性;
        private DevExpress.XtraEditors.CheckEdit chk白带_血样;
        private DevExpress.XtraEditors.CheckEdit chk白带_泡沫;
        private DevExpress.XtraEditors.CheckEdit chk白带_渣样;
        private DevExpress.XtraEditors.CheckEdit chk白带_臭味;
        private DevExpress.XtraEditors.CheckEdit chk白带性质;
        private DevExpress.XtraEditors.TextEdit txt白带;
        private DevExpress.XtraEditors.TextEdit txt阴道;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private System.Windows.Forms.FlowLayoutPanel flow子宫颈;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.CheckEdit checkEdit22;
        private DevExpress.XtraEditors.CheckEdit checkEdit23;
        private DevExpress.XtraEditors.CheckEdit checkEdit24;
        private DevExpress.XtraEditors.CheckEdit checkEdit25;
        private DevExpress.XtraEditors.CheckEdit checkEdit26;
        private DevExpress.XtraEditors.CheckEdit checkEdit27;
        private DevExpress.XtraEditors.CheckEdit checkEdit28;
        private DevExpress.XtraEditors.CheckEdit checkEdit29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraEditors.CheckEdit checkEdit30;
        private Library.UserControls.UCTxtLbl txt右卵巢;
        private Library.UserControls.UCTxtLbl txt左卵巢;
        private DevExpress.XtraEditors.TextEdit txt子宫体大小;
        private DevExpress.XtraEditors.TextEdit txt子宫脱垂;
        private DevExpress.XtraEditors.TextEdit txt子宫体活动;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private System.Windows.Forms.FlowLayoutPanel flow附件;
        private DevExpress.XtraEditors.CheckEdit checkEdit31;
        private DevExpress.XtraEditors.CheckEdit checkEdit32;
        private DevExpress.XtraEditors.CheckEdit checkEdit33;
        private DevExpress.XtraEditors.CheckEdit checkEdit34;
        private DevExpress.XtraEditors.CheckEdit checkEdit35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraEditors.TextEdit txt阴道异常情况;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraEditors.TextEdit txt医师签名;
        private DevExpress.XtraEditors.TextEdit txt治疗情况;
        private DevExpress.XtraEditors.MemoEdit txt处理意见;
        private DevExpress.XtraEditors.TextEdit txt印象;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem68;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem69;
        private DevExpress.XtraLayout.LayoutControlItem lbl治疗情况;
        private DevExpress.XtraLayout.LayoutControlItem lbl医师签名;
        private DevExpress.XtraEditors.TextEdit txt最近修改人;
        private DevExpress.XtraEditors.TextEdit txt创建人;
        private DevExpress.XtraEditors.TextEdit txt当前所属机构;
        private DevExpress.XtraEditors.TextEdit txt最近更新时间;
        private DevExpress.XtraEditors.TextEdit txt创建机构;
        private DevExpress.XtraEditors.TextEdit txt创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem72;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem73;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem74;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem75;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem76;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem77;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.DateEdit dte检查日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup14;
        private DevExpress.XtraEditors.DateEdit txt出生日期;
        private DevExpress.XtraEditors.ButtonEdit txt档案编号;
        private DevExpress.XtraEditors.TextEdit txt乳腺彩超;
        private DevExpress.XtraEditors.TextEdit txt钼靶检查;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.TextEdit txt病理检查;
        private DevExpress.XtraEditors.TextEdit txtTCT结果;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.RadioGroup radio分级;
        private DevExpress.XtraEditors.SimpleButton btn关闭;
    }
}
