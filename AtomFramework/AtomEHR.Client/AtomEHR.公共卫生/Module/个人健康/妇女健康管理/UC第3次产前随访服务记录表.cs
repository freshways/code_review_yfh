﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class UC第3次产前随访服务记录表 : UserControlBase
    {
        public string m_id = "";
        private string m_grdabh = "";
        private UpdateType m_updateType = UpdateType.None;
        private string m_closeType = "";

        private DataSet m_ds孕3次 = null;
        private bll孕妇_产前随访3次 m_bll3次 = new bll孕妇_产前随访3次();
        //public UC第3次产前随访服务记录表()
        //{
        //    InitializeComponent();
        //}


        //add by wjz 填表孕周一栏，如果只填写孕周，考核项颜色也设置为蓝色 ▽
        private void textEdit孕周TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.ucTxtLblTxtLbl填表孕周.Txt1.Text))
            {
                this.ucTxtLblTxtLbl填表孕周.Tag = null;
            }
            else
            {
                this.ucTxtLblTxtLbl填表孕周.Tag = "true";
            }
        }
        //add by wjz 填表孕周一栏，如果只填写孕周，考核项颜色也设置为蓝色 △

        public UC第3次产前随访服务记录表(string grdabh, string id, UpdateType updateType, string closeType)
        {
            InitializeComponent();

            //add by wjz 填表孕周一栏，如果只填写孕周，考核项颜色也设置为蓝色 ▽
            this.ucTxtLblTxtLbl填表孕周.Txt1.TextChanged += textEdit孕周TextChanged;
            this.ucTxtLblTxtLbl填表孕周.Txt2.TextChanged += textEdit孕周TextChanged;
            //add by wjz 填表孕周一栏，如果只填写孕周，考核项颜色也设置为蓝色 △

            ucTxtLbl孕次.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            ucTxtLbl孕次.Txt1.Properties.Mask.ShowPlaceHolders = false;
            ucTxtLbl孕次.Txt1.Properties.Mask.EditMask = "\\d+";

            if(updateType == UpdateType.Add || updateType == UpdateType.Modify)
            { }
            else
            {
                throw new Exception("孕妇产前第三次随访记录更新类型异常。");
            }

            //m_id = id;
            m_grdabh = grdabh;
            m_updateType = updateType;
            if(m_updateType == UpdateType.Add)
            {
                m_id = "-1";
            }
            else if(m_updateType == UpdateType.Modify)
            {
                m_id = id;
            }
            m_closeType = closeType;
            
            //先为控件绑定数据
            BindData();

            //再执行数据查询
            GetDataFromDB();

            if (m_updateType == UpdateType.Modify)
            {
                //ucTxtLbl孕次.Txt1.Properties.ReadOnly = true;
                ucTxtLbl孕次.Txt1.Properties.ReadOnly = false;
                //ucTxtLbl孕次.Txt1.BackColor = SystemColors.Control;

                //dateEdit下次随访日期.BackColor = SystemColors.Control;
                dateEdit下次随访日期.Properties.ReadOnly = false;//下次随访日期可修改
                //dateEdit下次随访日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
                //dateEdit填表日期.BackColor = SystemColors.Control;
                dateEdit填表日期.Properties.ReadOnly = false;//填表日期可修改
                //dateEdit填表日期.Properties.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            }
            else
            {
                //新增记录的情况下，为控件设置默认值
                SetDefaultValueForComboBox();
            }
        }

        private void SetDefaultValueForComboBox()
        {
            util.ControlsHelper.SetComboxData("2", comboBoxEdit转诊);
            util.ControlsHelper.SetComboxData("1", comboBoxEdit分类);
            cbo怀孕状态.Text = "在孕";
        }

        private void GetDataFromDB()
        {
            m_ds孕3次 = m_bll3次.GetBusinessByGrdabhAndID(m_grdabh, m_id, true);

            string strServerDate = m_bll3次.ServiceDateTime;

            //changed by wjz 20151231 ▽
            //ucTxtLbl孕次.Txt1.Text = m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇_产前随访3次.孕次].ToString();

            if (m_updateType == UpdateType.Modify)
            {
                ucTxtLbl孕次.Txt1.Text = m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.孕次].ToString();
            }
            else
            {
                //changed 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 ▽
                //ucTxtLbl孕次.Txt1.Text = m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.孕次].ToString();
                ucTxtLbl孕次.Txt1.Text = m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.孕次].ToString();
                //changed 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 △
            }
            //changed by wjz 20151231 △
            
            if(m_updateType == UpdateType.Add)
            {
                m_bll3次.NewBusiness();

                textEdit创建时间.Text = strServerDate;
                textEdit创建机构.Text = Loginer.CurrentUser.所属机构名称;

                textEdit创建人.Text = Loginer.CurrentUser.AccountName;
                textEdit当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
            }
            else if(m_updateType == UpdateType.Modify)
            {
                textEdit卡号.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.卡号].ToString();
                string str填表日期 = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.发生时间].ToString();
                if(string.IsNullOrWhiteSpace(str填表日期))
                { }
                else
                {
                    dateEdit填表日期.Text = Convert.ToDateTime(str填表日期).ToString("yyyy-MM-dd");
                }

                #region 新版本添加项
                string str随访方式 = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.随访方式].ToString();
                this.radio随访方式.EditValue = str随访方式;
                if (!string.IsNullOrEmpty(str随访方式) && str随访方式 == "99")
                {
                    this.txt随访方式其他.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.随访方式其他].ToString();
                }
                else
                {
                    this.txt随访方式其他.Enabled = false;
                    this.txt随访方式其他.Text = "";
                }

                txt产前检查机构名称.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.产前检查机构名称].ToString();
                txt居民签名.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.居民签名].ToString();
                #endregion

                ucTxtLblTxtLbl填表孕周.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.孕周].ToString();
                ucTxtLblTxtLbl填表孕周.Txt2.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.孕周天].ToString();
                ucTxtLbl体重.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.体重].ToString();
                ucTxtLbl宫底高度.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.宫底高度].ToString();
                ucTxtLbl腹围.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.腹围].ToString();
                textEdit胎位.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.胎位].ToString();
                ucTxtLbl胎心率.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.胎心率].ToString();
                
                ucTxtLblTxtLbl血压.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.收缩压].ToString();
                ucTxtLblTxtLbl血压.Txt2.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.舒张压].ToString();
                ucTxtLbl血红蛋白.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.血红蛋白].ToString();
                textEdit尿蛋白.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.尿蛋白].ToString();
                textEdit其他检查.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.其他检查].ToString();

                string str分类 = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.分类].ToString();
                util.ControlsHelper.SetComboxData(str分类, comboBoxEdit分类);
                if(str分类!=null && "2".Equals(str分类))
                {
                    textEdit分类异常.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.分类异常].ToString();
                }

                string str指导Temp = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.指导].ToString();
                char[] c分隔符 = { ','};
                string[] str指导 = str指导Temp.Split(c分隔符);
                for(int index = 0; index < str指导.Length ; index++)
                {
                    switch(str指导[index])
                    {
                        case "1":
                            checkEdit指导个人卫生.Checked = true;
                            break;
                        case "2":
                            checkEdit指导膳食.Checked = true;
                            break;
                        case "3":
                            checkEdit指导心理.Checked = true;
                            break;
                        case "4":
                            checkEdit自我监护.Checked = true;
                            break;
                        case "5":
                            checkEdit母乳喂养.Checked = true;
                            break;
                        case "6":
                            checkEdit指导运动.Checked = true;
                            break;
                        case "7":
                            checkEdit指导其他.Checked = true;
                            break;
                        default:
                            break;
                    }
                }
                textEdit指导其他.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.指导其他].ToString();
                string str转诊 = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.转诊].ToString();
                util.ControlsHelper.SetComboxData(str转诊, comboBoxEdit转诊);
                if(str转诊.Equals("1"))
                {
                    ucLblTxt转诊原因.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.转诊原因].ToString();
                    ucLblTxt转诊机构.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.转诊机构].ToString();
                    uc联系人.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.联系人].ToString();
                    uc联系方式.Txt1.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.联系方式].ToString();
                    string str是否到位 = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.结果].ToString();
                    util.ControlsHelper.SetComboxData(str是否到位, cbo是否到位);
                }

                textEdit主诉.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.主诉].ToString();

                string str下次随访日期 = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.下次随访日期].ToString();
                if(string.IsNullOrWhiteSpace(str下次随访日期))
                {
                }
                else
                {
                    dateEdit下次随访日期.Text = Convert.ToDateTime(str下次随访日期).ToString("yyyy-MM-dd");
                }

                textEdit随访医生.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.随访医生].ToString();

                textEdit创建机构.Text = m_bll3次.Return机构名称(m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.创建机构].ToString());
                textEdit当前所属机构.Text = m_bll3次.Return机构名称(m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.所属机构].ToString());
                textEdit创建时间.Text = m_bll3次.Return机构名称(m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.创建时间].ToString());
                textEdit创建人.Text = m_bll3次.Return用户名称(m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.创建人].ToString());

                //add 在第2-5次产前随访服务记录表中增加“怀孕状态”--“在孕”、“流/引产：  年  月  日孕  周，因    在       终止妊娠。”，默认“在孕”。
                cbo怀孕状态.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.怀孕状态].ToString();
                if (cbo怀孕状态.Text == "流/引产")
                {
                    dte流引产.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产日期].ToString();
                    txt流引产孕周.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产孕周].ToString();
                    txt流引产原因.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产原因].ToString();
                    txt流引产机构.Text = m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产医院].ToString();
                }
                //add 在第2-5次产前随访服务记录表中增加“怀孕状态”--“在孕”、“流/引产：  年  月  日孕  周，因    在       终止妊娠。”，默认“在孕”。


                #region 设置变色
                Set考核项颜色_new(layoutControl1, null);
                #endregion
            }

            textEdit最近更新时间.Text = strServerDate;
            textEdit最近修改人.Text = Loginer.CurrentUser.AccountName;


            //changed 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 ▽
            ////显示孕妇基本信息
            //textEdit档案编号.Text = m_grdabh;
            //textEdit孕妇姓名.Text = DESEncrypt.DES解密(m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.姓名].ToString());
            //textEdit联系电话.Text = m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.联系电话].ToString();
            //textEdit出生日期.Text = m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.出生日期].ToString();
            //textEdit身份证号.Text = m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.身份证号].ToString();
            
            //string str居住状况 = m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.居住状况].ToString();
            //textEdit居住状态.Text = m_bll3次.ReturnDis字典显示("jzzk", str居住状况);

            //string str省 = m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.省].ToString());
            //string str市 = m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.市].ToString());
            //string str县 = m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.区].ToString());
            //string str街道 = m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.街道].ToString());
            //string str居委会 = m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.居委会].ToString());

            //textEdit居住地址.Text = str省 + " " + str市 + " " + str县 + " " + str街道+" " + str居委会
            //    + m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.居住地址].ToString());

            //显示孕妇基本信息
            textEdit档案编号.Text = m_grdabh;
            textEdit孕妇姓名.Text = DESEncrypt.DES解密(m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.姓名].ToString());

            string str本人电话 = m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人电话].ToString();
            textEdit联系电话.Text = string.IsNullOrWhiteSpace(str本人电话) ? str本人电话 : m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人电话].ToString(); ;

            textEdit出生日期.Text = m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期].ToString();
            textEdit身份证号.Text = m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.身份证号].ToString();

            string str居住状况 = m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.常住类型].ToString();
            textEdit居住状态.Text = m_bll3次.ReturnDis字典显示("jzzk", str居住状况);

            string str省 = m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.省].ToString());
            string str市 = m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.市].ToString());
            string str县 = m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.区].ToString());
            string str街道 = m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.街道].ToString());
            string str居委会 = m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居委会].ToString());

            textEdit居住地址.Text = str省 + " " + str市 + " " + str县 + " " + str街道 + " " + str居委会
                + m_bll3次.Return地区名称(m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住地址].ToString());
            //changed 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 △
        }

        private void BindData()
        {
            BindDataForComboBox("ywyc", comboBoxEdit分类);
            BindDataForComboBox("yw_youwu", comboBoxEdit转诊);
            BindDataForComboBox("sfdw", cbo是否到位);
        }

        private void BindDataForComboBox(string p_fun_code, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit)
        {
            DataView dv = new DataView(DataDictCache.Cache.t常用字典);
            dv.RowFilter = "P_FUN_CODE = '" + p_fun_code + "'";
            dv.Sort = "P_CODE";
            util.ControlsHelper.BindComboxDataNull(dv.ToTable(), comboBoxEdit, "P_CODE", "P_DESC");
        }

        private void checkEdit指导其他_CheckedChanged(object sender, EventArgs e)
        {
            if(checkEdit指导其他.Checked)
            {
                this.textEdit指导其他.Enabled = true;
                if (m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName] != null && m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows.Count == 1)
                {
                    if (m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.指导其他] != null && !string.IsNullOrEmpty(m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.指导其他].ToString()))
                    {
                        textEdit指导其他.Text = m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.指导其他].ToString();
                    }
                    else
                    {
                        textEdit指导其他.Text = "低盐饮食";
                    }
                }
            }
            else
            {
                textEdit指导其他.Enabled = false;
                textEdit指导其他.Text = "";
            }
        }

        private void UC第3次产前随访服务记录表_Load(object sender, EventArgs e)
        {

        }

        private void comboBoxEdit分类_TextChanged(object sender, EventArgs e)
        {
            string strTemp = util.ControlsHelper.GetComboxKey(comboBoxEdit分类);
            //非空，且等于1
            if (!(string.IsNullOrWhiteSpace(strTemp)) && ("1".Equals(strTemp)))
            {
                textEdit分类异常.Enabled = false;
                textEdit分类异常.Text = "";
            }
            else
            {
                textEdit分类异常.Enabled = true;
            }
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            if(m_updateType != UpdateType.Add && m_updateType != UpdateType.Modify)
            {
                Msg.ShowInformation("保存时出现异常。");
                return;
            }

            if (cbo怀孕状态.Text == "流/引产" &&
                (string.IsNullOrWhiteSpace(dte流引产.Text) || string.IsNullOrWhiteSpace(txt流引产机构.Text) ||
                    string.IsNullOrWhiteSpace(txt流引产原因.Text) || string.IsNullOrWhiteSpace(txt流引产孕周.Text))
               )
            {
                Msg.ShowInformation("请补充完流引产信息。");
                this.txt流引产原因.Focus();
                return;
            }

            if(string.IsNullOrWhiteSpace(dateEdit填表日期.Text) || (string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text)))
            {
                Msg.ShowInformation("请填写“随访日期”或 “下次随访日期”。");
                return;
            }
            //if (dateEdit填表日期.DateTime > Convert.ToDateTime(this.textEdit创建时间.Text))
            //{
            //    Msg.ShowInformation("随访日期不能大于填写日期");
            //    dateEdit填表日期.Focus();
            //    return;
            //}
            //if (string.IsNullOrWhiteSpace(ucTxtLbl孕次.Txt1.Text))
            //{
            //    Msg.ShowInformation("请填写孕次");
            //    ucTxtLbl孕次.Txt1.Focus();
            //    return;
            //}

            string temp转诊 = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊);
            if (temp转诊 == "1" && (string.IsNullOrWhiteSpace(ucLblTxt转诊原因.Txt1.Text) || string.IsNullOrWhiteSpace(ucLblTxt转诊机构.Txt1.Text)
                || string.IsNullOrWhiteSpace(uc联系人.Txt1.Text) || string.IsNullOrWhiteSpace(uc联系方式.Txt1.Text) || string.IsNullOrWhiteSpace(cbo是否到位.EditValue.ToString())
                ))
            {
                Msg.ShowInformation("在“转诊”是“有”的情况下，请填写“转诊原因”、“转诊机构”、“联系人”、“联系方式”、“结果”。");
                return;
            }

            if (m_updateType == UpdateType.Add)
            {
                //检查孕次
                bool isExist = m_bll3次.Check随访记录(m_grdabh, ucTxtLbl孕次.Txt1.Text.Trim());
                if (isExist)
                {
                    Msg.ShowInformation("孕次为" + ucTxtLbl孕次.Txt1.Text + "的第3次产前随访表已建立,请重新填写!");
                    return;
                }
            }
            string str个人档案孕次 = m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.孕次].ToString();
            
            try
            {
                int i填写的孕次 = Convert.ToInt32(ucTxtLbl孕次.Txt1.Text.Trim());
                int i个人档案孕次 = Convert.ToInt32(str个人档案孕次);
                if(i填写的孕次 > i个人档案孕次)
                {
                    Msg.ShowInformation("请修改孕次，孕次必须小于等于"+ str个人档案孕次);
                    ucTxtLbl孕次.Txt1.Focus();
                    return;
                }
            }
            catch
            {
                Msg.ShowInformation("请确认孕次信息， 如果还是出现此提示，请确认个人基本信息中的孕次是否已经填写。");
                ucTxtLbl孕次.Txt1.Focus();
                return;
            }
            
            if(m_updateType == UpdateType.Add)
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.创建机构] = Loginer.CurrentUser.所属机构;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.创建人] = Loginer.CurrentUser.用户编码;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.创建时间] = textEdit创建时间.Text;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.所属机构] = Loginer.CurrentUser.所属机构;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.个人档案编号] = m_grdabh;
                //m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.下次随访日期] = dateEdit下次随访日期.Text;
                //m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.发生时间] = dateEdit填表日期.Text;

                bool bret = Msg.AskQuestion("信息保存后，‘随访日期’与‘下次随访日期’将不允许修改，确认保存信息？");
                if(!bret)
                {
                    return;
                }
            }
            else
            {
                bool bret = Msg.AskQuestion("确认保存修改？");
                if (!bret)
                {
                    return;
                }
            }

            #region   新版本添加项
            string str随访方式 = this.radio随访方式.EditValue.ToString();
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.随访方式] = str随访方式;
            if (!string.IsNullOrEmpty(str随访方式) && str随访方式.Equals("99"))//其他
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.随访方式其他] = this.txt随访方式其他.Text;
            }
            else
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.随访方式其他] = null;
            }

            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.产前检查机构名称] = txt产前检查机构名称.Text;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.居民签名] = txt居民签名.Text;
            #endregion

            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.卡号] = textEdit卡号.Text;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.发生时间] = dateEdit填表日期.Text;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.下次随访日期] = dateEdit下次随访日期.Text;
            
            try
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.孕周] = Convert.ToInt32(ucTxtLblTxtLbl填表孕周.Txt1.Text);
            }
            catch
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.孕周] = DBNull.Value;
            }

            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.孕周天] = ucTxtLblTxtLbl填表孕周.Txt2.Text;
            if (string.IsNullOrWhiteSpace(ucTxtLbl体重.Txt1.Text))
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.体重] = DBNull.Value;
            }
            else
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.体重] = ucTxtLbl体重.Txt1.Text;
            }
            if (string.IsNullOrWhiteSpace(ucTxtLbl宫底高度.Txt1.Text))
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.宫底高度] = DBNull.Value;
            }
            else
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.宫底高度] = ucTxtLbl宫底高度.Txt1.Text;
            }

            if (string.IsNullOrWhiteSpace(ucTxtLbl腹围.Txt1.Text))
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.腹围] = DBNull.Value;
            }
            else
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.腹围] = ucTxtLbl腹围.Txt1.Text;
            }
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.胎位] = textEdit胎位.Text;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.胎心率] = ucTxtLbl胎心率.Txt1.Text;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.孕次] = ucTxtLbl孕次.Txt1.Text;

            if (string.IsNullOrWhiteSpace(ucTxtLblTxtLbl血压.Txt1.Text))
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.收缩压] = DBNull.Value;
            }
            else
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.收缩压] = ucTxtLblTxtLbl血压.Txt1.Text;
            }
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.舒张压] = ucTxtLblTxtLbl血压.Txt2.Text;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.血红蛋白] = ucTxtLbl血红蛋白.Txt1.Text;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.尿蛋白] = textEdit尿蛋白.Text;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.其他检查] = textEdit其他检查.Text;

            string str分类 = util.ControlsHelper.GetComboxKey(comboBoxEdit分类);
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.分类] = str分类;
            if(str分类 !=null && str分类.Equals("2"))
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.分类异常] = textEdit分类异常.Text;
            }
            else
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.分类异常] = null;
            }

            string str指导 = GetFlowLayoutResult(flow指导).ToString();
            //if(checkEdit指导个人卫生.Checked)
            //{
            //    str指导 += "1,";
            //}
            //if(checkEdit指导膳食.Checked)
            //{
            //    str指导 += "2,";
            //}
            //if(checkEdit指导心理.Checked)
            //{
            //    str指导 += "3,";
            //}
            //if (checkEdit自我监护.Checked)
            //{
            //    str指导 += "4,";
            //}
            //if (checkEdit母乳喂养.Checked)
            //{
            //    str指导 += "5,";
            //}
            //if(checkEdit指导运动.Checked)
            //{
            //    str指导 += "6,";
            //}
            //if(checkEdit指导其他.Checked)
            //{
            //    str指导 += "7,";
            //}
            //if(str指导.Length >0)
            //{
            //    m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.指导] = str指导.Substring(0, str指导.Length - 1);
            //}
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.指导] = str指导;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.指导其他] = textEdit指导其他.Text;

            string str转诊 = util.ControlsHelper.GetComboxKey(comboBoxEdit转诊);
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.转诊] = str转诊;

            if(str转诊 != null && str转诊.Equals("1"))
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.转诊原因] = ucLblTxt转诊原因.Txt1.Text;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.转诊机构] = ucLblTxt转诊机构.Txt1.Text;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.联系人] = uc联系人.Txt1.Text;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.联系方式] = uc联系方式.Txt1.Text;
                string str是否到位 = util.ControlsHelper.GetComboxKey(cbo是否到位);
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.结果] = str是否到位;
            }
            else
            {
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.转诊原因] = null;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.转诊机构] = null;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.联系人] = null;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.联系方式] = null;
                m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.结果] = null;
            }

            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.主诉] = textEdit主诉.Text;
            //m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.下次随访日期] = dateEdit下次随访日期.Text;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.随访医生] = textEdit随访医生.Text;

            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.修改时间] = textEdit最近更新时间.Text;
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.修改人] = Loginer.CurrentUser.用户编码;

            //add 在第2-5次产前随访服务记录表中增加“怀孕状态”--“在孕”、“流/引产：  年  月  日孕  周，因    在       终止妊娠。”，默认“在孕”。
            m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.怀孕状态] = cbo怀孕状态.Text;
            if (cbo怀孕状态.Text == "流/引产")
            {
                m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产日期] = dte流引产.Text;
                m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产孕周] = txt流引产孕周.Text;
                m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产原因] = txt流引产原因.Text;
                m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产医院] = txt流引产机构.Text;
            }
            else
            {
                m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产日期] = "";
                m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产孕周] = "";
                m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产原因] = "";
                m_ds孕3次.Tables[0].Rows[0][tb_孕妇_产前随访3次.流引产医院] = "";
            }
            //add 在第2-5次产前随访服务记录表中增加“怀孕状态”--“在孕”、“流/引产：  年  月  日孕  周，因    在       终止妊娠。”，默认“在孕”。


            #region 更新孕妇基本信息
            
            //获取孕妇基本信息中的孕次
            string str怀孕情况 = m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.怀孕情况].ToString();
            //string str个人档案孕次 = m_ds孕3次.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.孕次].ToString();
            string str基本信息孕次 = m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.孕次].ToString();

            if (str怀孕情况 != null && str怀孕情况.Equals("已孕未生产") && str个人档案孕次.Equals(ucTxtLbl孕次.Txt1.Text))
            {
                #region Old code
                //if (string.IsNullOrWhiteSpace(str基本信息孕次))
                //{
                //    m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.SFFLAG] = "3";
                //    m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.下次随访时间] = dateEdit下次随访日期.Text;
                //}
                //else
                //{
                //    try
                //    {
                //        int i孕次 = Convert.ToInt32(str基本信息孕次);
                //        if (i孕次 < 2)
                //        {
                //            m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.SFFLAG] = "3";
                //            m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.下次随访时间] = dateEdit下次随访日期.Text;
                //        }
                //    }
                //    catch
                //    { }
                //}
                #endregion
                string strSfflag = m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.SFFLAG].ToString();
                if (!(string.IsNullOrWhiteSpace(strSfflag)))
                {
                    try
                    {
                        int iSfflag = Convert.ToInt32(strSfflag);
                        if (iSfflag < 3)
                        {
                            m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.SFFLAG] = "3";
                            m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.下次随访时间] = dateEdit下次随访日期.Text;
                        }
                    }
                    catch
                    { }
                }
                else
                {
                    m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.SFFLAG] = "3";
                    m_ds孕3次.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.下次随访时间] = dateEdit下次随访日期.Text;
                }
            }
            #endregion

            #region 计算缺项、完整度

            int i缺项 = 0;
            int i完整度 = 0;
            if(string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                i缺项++;
            }
            //if (string.IsNullOrWhiteSpace(ucTxtLblTxtLbl填表孕周.Txt1.Text) || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl填表孕周.Txt2.Text))
            if (string.IsNullOrWhiteSpace(ucTxtLblTxtLbl填表孕周.Txt1.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(ucTxtLbl体重.Txt1.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(ucTxtLbl宫底高度.Txt1.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(ucTxtLbl腹围.Txt1.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(textEdit胎位.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(ucTxtLbl胎心率.Txt1.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(ucTxtLbl孕次.Txt1.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(ucTxtLblTxtLbl血压.Txt1.Text) || string.IsNullOrWhiteSpace(ucTxtLblTxtLbl血压.Txt2.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(ucTxtLbl血红蛋白.Txt1.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(textEdit尿蛋白.Text))
            {
                i缺项++;
            }

            if(string.IsNullOrWhiteSpace(str分类) || (str分类.Equals("99") && string.IsNullOrWhiteSpace(textEdit分类异常.Text)))
            {
                i缺项++;
            }

            if(string.IsNullOrWhiteSpace(str指导) || (str指导.Contains("5") && string.IsNullOrWhiteSpace(textEdit指导其他.Text)))
            {
                i缺项++;
            }
            
            if(string.IsNullOrWhiteSpace(str转诊) 
                || (str转诊.Equals("1") && (string.IsNullOrWhiteSpace(ucLblTxt转诊原因.Txt1.Text) || string.IsNullOrWhiteSpace(ucLblTxt转诊机构.Txt1.Text)
                || string.IsNullOrWhiteSpace(uc联系人.Txt1.Text) || string.IsNullOrWhiteSpace(uc联系方式.Txt1.Text) || string.IsNullOrWhiteSpace(cbo是否到位.EditValue.ToString())
                )))
            {
                i缺项++;
            }

            if(string.IsNullOrWhiteSpace(textEdit主诉.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(dateEdit下次随访日期.Text))
            {
                i缺项++;
            }
            if(string.IsNullOrWhiteSpace(textEdit随访医生.Text))
            {
                i缺项++;
            }
            #region 新版本添加项
            if (string.IsNullOrWhiteSpace(str随访方式) || (str随访方式.Equals("99") && string.IsNullOrWhiteSpace(txt随访方式其他.Text)))
            {
                i缺项++;
            }
            if (string.IsNullOrWhiteSpace(txt产前检查机构名称.Text))
            {
                i缺项++;
            }
            if (string.IsNullOrWhiteSpace(txt居民签名.Text))
            {
                i缺项++;
            }
            #endregion
            i完整度 = (20 - i缺项) * 100 / 20;
            //i完整度 = (17 - i缺项) * 100 / 17;

            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.缺项] = i缺项.ToString();
            m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.完整度] = i完整度.ToString();
            #endregion

            #region 更新个人体征信息
            //changed by wjz 20151231 档案中的孕次与随访记录孕次一致时，才修改个人体征信息,添加if判断 ▽
            if (str个人档案孕次.Trim() == ucTxtLbl孕次.Txt1.Text.Trim())
            {
                m_ds孕3次.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.第三次产前检查表] = i缺项.ToString() + "," + i完整度.ToString();
            }
            //changed by wjz 20151231 档案中的孕次与随访记录孕次一致时，才修改个人体征信息,添加if判断 △
            #endregion

            if (arrImageBytes != null)
            {
                if (string.IsNullOrWhiteSpace(m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.SFID].ToString()))
                {
                    m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.SFID] = Guid.NewGuid().ToString().Replace("-", "");
                    bll孕产妇Image.GetBusinessByKey("-1", true);
                    bll孕产妇Image.NewBusiness();
                    bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.档案号] = m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.个人档案编号];
                    bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICID] = m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.SFID];
                    bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICBYTE] = arrImageBytes;
                    bll孕产妇Image.Save(bll孕产妇Image.CurrentBusiness);
                }
                else
                {
                    string sfid = m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.SFID].ToString();
                    bll孕产妇Image.GetBusinessByDahAndPicid(m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.个人档案编号].ToString(), sfid, true);
                    //bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.档案号] = _dt一次随访.Rows[0][tb_孕妇_产前随访1次.个人档案编号];
                    //bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICID] = _dt一次随访.Rows[0][tb_孕妇_产前随访1次.SFID];
                    bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.createuser] = Loginer.CurrentUser.用户编码;
                    bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.createtime] = bll孕产妇Image.ServiceDateTime;
                    bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICBYTE] = arrImageBytes;
                    bll孕产妇Image.Save(bll孕产妇Image.CurrentBusiness);
                }
            }

            SaveResult result = m_bll3次.Save(m_ds孕3次);

            if (result.Success)
            {
                UC第3次产前随访服务记录表_显示 ctrl = new UC第3次产前随访服务记录表_显示(m_grdabh, null);
                ShowControl(ctrl, DockStyle.Fill);
            }
            else
            {
                Msg.ShowError("保存失败。");
            }
        }

        private void comboBoxEdit转诊_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.comboBoxEdit转诊.Text.Trim();
            if (!string.IsNullOrEmpty(index) && index == "有")
            {
                this.ucLblTxt转诊原因.Enabled = true;
                this.ucLblTxt转诊机构.Enabled = true;
                this.uc联系人.Enabled = true;
                this.uc联系方式.Enabled = true;
                this.cbo是否到位.Enabled = true;
                this.ucLblTxt转诊原因.Txt1.Properties.ReadOnly = false;
                this.ucLblTxt转诊机构.Txt1.Properties.ReadOnly = false;
                this.uc联系人.Txt1.Properties.ReadOnly = false;
                this.uc联系方式.Txt1.Properties.ReadOnly = false;
                this.cbo是否到位.Properties.ReadOnly = false;
            }
            else
            {
                this.ucLblTxt转诊原因.Txt1.Properties.ReadOnly = true;
                this.ucLblTxt转诊机构.Txt1.Properties.ReadOnly = true;
                this.uc联系人.Txt1.Properties.ReadOnly = true;
                this.uc联系方式.Txt1.Properties.ReadOnly = true;
                this.cbo是否到位.Properties.ReadOnly = true;
                this.ucLblTxt转诊原因.Txt1.Text = "";
                this.ucLblTxt转诊机构.Txt1.Text = "";
                this.uc联系人.Txt1.Text = "";
                this.uc联系方式.Txt1.Text = "";
                this.cbo是否到位.EditValue = "";
            }
        }

        private void radio随访方式_SelectedIndexChanged(object sender, EventArgs e)
        {
            string str随访方式 = this.radio随访方式.EditValue.ToString();
            if (!string.IsNullOrEmpty(str随访方式) && str随访方式 == "99")
            {
                this.txt随访方式其他.Enabled = true;
            }
            else
            {
                this.txt随访方式其他.Enabled = false;
                this.txt随访方式其他.Text = "";
            }
        }

        private void cbo怀孕状态_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo怀孕状态.Text != "流/引产")
            {
                txt流引产孕周.Enabled = false;
                dte流引产.Enabled = false;
                txt流引产原因.Enabled = false;
                txt流引产机构.Enabled = false;
            }
            else
            {
                txt流引产孕周.Enabled = true;
                dte流引产.Enabled = true;
                txt流引产原因.Enabled = true;
                txt流引产机构.Enabled = true;
            }
        }

        private byte[] arrImageBytes = null;

        bll孕产妇随访化验单 bll孕产妇Image = new bll孕产妇随访化验单();
        private void sbtn上传化验单图片_Click(object sender, EventArgs e)
        {
            FrmPIC frm = new FrmPIC();

            string sfid = m_ds孕3次.Tables[tb_孕妇_产前随访3次.__TableName].Rows[0][tb_孕妇_产前随访3次.SFID].ToString();

            if (!string.IsNullOrWhiteSpace(sfid))
            {
                DataSet dstemp = bll孕产妇Image.GetBusinessByDahAndPicid(m_grdabh, sfid, false);

                byte[] arrbyte = dstemp.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICBYTE] as byte[];

                frm.SetImage(arrbyte);
            }

            if (frm.ShowDialog() == DialogResult.OK)
            {
                arrImageBytes = frm.arrImageBytes;
            }
        }
    }
}
