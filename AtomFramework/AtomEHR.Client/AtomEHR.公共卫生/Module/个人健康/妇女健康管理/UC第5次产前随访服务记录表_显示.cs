﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.Business;
using AtomEHR.Models;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class UC第5次产前随访服务记录表_显示 : UserControlBaseNavBar
    {

        private string m_grdabh = "";
        //private UpdateType m_updateType = UpdateType.None;
        //private string m_closeType = "";
        private string m_id = "0";//当前显示的随访记录的id

        private DataSet m_ds孕5次 = null;
        private bll孕妇_产前随访5次 m_bll5次 = new bll孕妇_产前随访5次();
        //public UC第5次产前随访服务记录表_显示()
        //{
        //    InitializeComponent();
        //}


        //add by wjz 填表孕周一栏，如果只填写孕周，考核项颜色也设置为蓝色 ▽
        private void textEdit孕周TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.ucTxtLblTxtLbl孕周.Txt1.Text))
            {
                this.ucTxtLblTxtLbl孕周.Tag = null;
            }
            else
            {
                this.ucTxtLblTxtLbl孕周.Tag = "true";
            }
        }
        //add by wjz 填表孕周一栏，如果只填写孕周，考核项颜色也设置为蓝色 △


        public UC第5次产前随访服务记录表_显示(string grdabh, string id)
        {
            InitializeComponent();

            //add by wjz 填表孕周一栏，如果只填写孕周，考核项颜色也设置为蓝色 ▽
            this.ucTxtLblTxtLbl孕周.Txt1.TextChanged += textEdit孕周TextChanged;
            this.ucTxtLblTxtLbl孕周.Txt2.TextChanged += textEdit孕周TextChanged;
            //add by wjz 填表孕周一栏，如果只填写孕周，考核项颜色也设置为蓝色 △

            SetReadOnly();

            m_grdabh = grdabh;
            m_id = id;
            //m_updateType = updateType;
            //m_closeType = closeType;

            //先为控件绑定数据
            BindData();

            //再执行数据查询
            GetDataFromDB();

            ////变色
            //Set考核项颜色_new(layoutControl1, labelControl考核项);
        }

        private void SetReadOnly()
        {
            ucTxtLblTxtLbl孕周.Txt1.Properties.ReadOnly = true;
            ucTxtLblTxtLbl孕周.Txt2.Properties.ReadOnly = true;
            ucTxtLbl宫底高度.Txt1.Properties.ReadOnly = true;
            ucTxtLbl腹围.Txt1.Properties.ReadOnly = true;
            ucTxtLbl胎心率.Txt1.Properties.ReadOnly = true;
            ucTxtLbl孕次.Txt1.Properties.ReadOnly = true;
            ucTxtLblTxtLbl血压.Txt1.Properties.ReadOnly = true;
            ucTxtLblTxtLbl血压.Txt2.Properties.ReadOnly = true;
            ucTxtLbl血红蛋白.Txt1.Properties.ReadOnly = true;
            ucLblTxt转诊原因.Txt1.Properties.ReadOnly = true;
            ucLblTxt转诊机构.Txt1.Properties.ReadOnly = true;
            uc联系人.Txt1.Properties.ReadOnly = true;
            uc联系方式.Txt1.Properties.ReadOnly = true;
            uc结果.Txt1.Properties.ReadOnly = true;
        }

        private void GetDataFromDB()
        {
            m_ds孕5次 = m_bll5次.GetShowInfos(m_grdabh);
            if (m_ds孕5次 == null || m_ds孕5次.Tables.Count == 0 || m_ds孕5次.Tables[0].Rows.Count == 0)
            {
                return;
            }

            string strID = "";//首次显示时，左侧日期栏中需要变色的项目的ID
            //显示左侧的日期导航
            CreateNavBarButton_new(m_ds孕5次.Tables[tb_孕妇_产前随访5次.__TableName], "发生时间");

            if (string.IsNullOrWhiteSpace(m_id))
            {
                //if(m_ds孕5次==null || m_ds孕5次.Tables.Count ==0 || m_ds孕5次.Tables[0].Rows.Count == 0)
                //{
                //}
                //else
                //{
                strID = m_ds孕5次.Tables[tb_孕妇_产前随访5次.__TableName].Rows[0]["ID"].ToString();
                ShowDetailInfo(m_ds孕5次.Tables[tb_孕妇_产前随访5次.__TableName].Rows[0]);
                //}
            }
            else
            {
                DataRow[] DRS = m_ds孕5次.Tables[tb_孕妇_产前随访5次.__TableName].Select("ID=" + m_id);
                if (DRS.Length > 0)
                {
                    strID = m_id;
                    ShowDetailInfo(DRS[0]);
                }
                else
                {
                    strID = m_ds孕5次.Tables[tb_孕妇_产前随访5次.__TableName].Rows[0]["ID"].ToString();
                    ShowDetailInfo(m_ds孕5次.Tables[tb_孕妇_产前随访5次.__TableName].Rows[0]);
                }
            }
            SetItemColorToRed(strID);
        }

        private void ShowDetailInfo(DataRow dr)
        {
            _sfid = dr[tb_孕妇_产前随访5次.SFID].ToString();

            textEdit卡号.Text = dr[tb_孕妇_产前随访5次.卡号].ToString();
            textEdit档案编号.Text = dr[tb_孕妇_产前随访5次.个人档案编号].ToString();
            textEdit档案编号.Tag = dr[tb_孕妇_产前随访5次.ID].ToString();
            textEdit孕妇姓名.Text = DESEncrypt.DES解密(dr["姓名"].ToString());
            textEdit联系电话.Text = dr["联系电话"].ToString();
            textEdit出生日期.Text = dr["出生日期"].ToString();
            textEdit身份证号.Text = dr["身份证号"].ToString();

            //string str居住 = dr["居住状况"].ToString();
            //util.ControlsHelper.SetComboxData("", textEdit居住状态);
            textEdit居住状态.Text = dr["居住状况"].ToString();

            textEdit居住地址.Text = dr["居住地址"].ToString();
            textEdit随访日期.Text = dr[tb_孕妇_产前随访5次.发生时间].ToString();
            ucTxtLblTxtLbl孕周.Txt1.Text = dr[tb_孕妇_产前随访5次.孕周].ToString();
            ucTxtLblTxtLbl孕周.Txt2.Text = dr[tb_孕妇_产前随访5次.孕周天].ToString();

            #region 新版本添加
            string str随访方式 = m_bll5次.ReturnDis字典显示("sffs", dr[tb_孕妇_产前随访5次.随访方式].ToString());
            if (!string.IsNullOrEmpty(str随访方式) && str随访方式 == "其他")
            {
                txt随访方式.Text = str随访方式 + "：" + dr[tb_孕妇_产前随访5次.随访方式其他].ToString();
            }
            else
            {
                txt随访方式.Text = str随访方式;
            }
            txt产前检查机构名称.Text = dr[tb_孕妇_产前随访5次.产前检查机构名称].ToString();
            txt居民签名.Text = dr[tb_孕妇_产前随访5次.居民签名].ToString();
            #endregion

            ucTxtLbl体重.Txt1.Text = dr[tb_孕妇_产前随访5次.体重].ToString();
            ucTxtLbl宫底高度.Txt1.Text = dr[tb_孕妇_产前随访5次.宫底高度].ToString();
            ucTxtLbl腹围.Txt1.Text = dr[tb_孕妇_产前随访5次.腹围].ToString();
            textEdit胎位.Text = dr[tb_孕妇_产前随访5次.胎位].ToString();
            ucTxtLbl胎心率.Txt1.Text = dr[tb_孕妇_产前随访5次.胎心率].ToString();
            ucTxtLbl孕次.Txt1.Text = dr[tb_孕妇_产前随访5次.孕次].ToString();
            //ucTxtLblTxtLbl血压.Txt1.Text = dr[tb_孕妇_产前随访5次.收缩压].ToString();
            string str收缩压 = dr[tb_孕妇_产前随访5次.收缩压].ToString();
            if (string.IsNullOrWhiteSpace(str收缩压))
            {
                ucTxtLblTxtLbl血压.Txt1.Text = "";
            }
            else
            {
                try
                {
                    double d收缩压 = Convert.ToDouble(str收缩压);
                    ucTxtLblTxtLbl血压.Txt1.Text = d收缩压.ToString();
                }
                catch
                {
                    ucTxtLblTxtLbl血压.Txt1.Text = str收缩压;
                }
            }
            ucTxtLblTxtLbl血压.Txt2.Text = dr[tb_孕妇_产前随访5次.舒张压].ToString();
            ucTxtLbl血红蛋白.Txt1.Text = dr[tb_孕妇_产前随访5次.血红蛋白].ToString();
            textEdit尿蛋白.Text = dr[tb_孕妇_产前随访5次.尿蛋白].ToString();
            textEdit其他检查.Text = dr[tb_孕妇_产前随访5次.其他检查].ToString();

            textEdit分类异常.Text = "";
            string str分类 = dr[tb_孕妇_产前随访5次.分类].ToString();
            util.ControlsHelper.SetComboxData(str分类, comboBoxEdit分类);
            if (!(string.IsNullOrWhiteSpace(str分类)) || str分类.Equals("99"))
            {
                textEdit分类异常.Text = dr[tb_孕妇_产前随访5次.分类异常].ToString();
            }

            textEdit指导其他.Text = "";
            string str指导Temp = dr[tb_孕妇_产前随访5次.指导].ToString();
            char[] c分隔符 = { ',' };
            string[] str指导 = str指导Temp.Split(c分隔符);
            for (int index = 0; index < str指导.Length; index++)
            {
                switch (str指导[index])
                {
                    case "1":
                        checkEdit指导个人卫生.Checked = true;
                        break;
                    case "2":
                        checkEdit指导膳食.Checked = true;
                        break;
                    case "3":
                        checkEdit指导心理.Checked = true;
                        break;
                    case "4":
                        checkEdit分娩准备.Checked = true;
                        break;
                    case "5":
                        checkEdit母乳喂养.Checked = true;
                        break;
                    case "6":
                        checkEdit指导运动.Checked = true;
                        break;
                    case "7":
                        checkEdit自我监测.Checked = true;
                        break;
                    case "8":
                        checkEdit指导其他.Checked = true;
                        break;
                    default:
                        break;
                }
            }
            textEdit指导其他.Text = dr[tb_孕妇_产前随访5次.指导其他].ToString();

            ucLblTxt转诊原因.Text = "";
            ucLblTxt转诊机构.Text = "";

            string str转诊Temp = dr[tb_孕妇_产前随访5次.转诊].ToString();
            util.ControlsHelper.SetComboxData(str转诊Temp, comboBoxEdit转诊);
            if (str转诊Temp != null && str转诊Temp.Equals("1"))
            {
                ucLblTxt转诊原因.Txt1.Text = dr[tb_孕妇_产前随访5次.转诊原因].ToString();
                ucLblTxt转诊机构.Txt1.Text = dr[tb_孕妇_产前随访5次.转诊机构].ToString();
                uc联系人.Txt1.Text = dr[tb_孕妇_产前随访5次.联系人].ToString();
                uc联系方式.Txt1.Text = dr[tb_孕妇_产前随访5次.联系方式].ToString();
                string str结果 = dr[tb_孕妇_产前随访5次.结果].ToString();
                if (str结果 != null)
                {
                    uc结果.Txt1.Text = m_bll5次.ReturnDis字典显示("sfdw", dr[tb_孕妇_产前随访5次.结果].ToString());
                }
            }
            else
            {
                ucLblTxt转诊原因.Txt1.Text = "";
                ucLblTxt转诊机构.Txt1.Text = "";
                uc联系人.Txt1.Text = "";
                uc联系方式.Txt1.Text = "";
                uc结果.Txt1.Text = "";
            }

            textEdit主诉.Text = dr[tb_孕妇_产前随访5次.主诉].ToString();
            textEdit下次随访日期.Text = dr[tb_孕妇_产前随访5次.下次随访日期].ToString();
            textEdit随访医生.Text = dr[tb_孕妇_产前随访5次.随访医生].ToString();
            textEdit创建时间.Text = dr[tb_孕妇_产前随访5次.创建时间].ToString();
            textEdit最近更新时间.Text = dr[tb_孕妇_产前随访5次.修改时间].ToString();
            textEdit当前所属机构.Text = dr["所属机构名称"].ToString();
            textEdit当前所属机构.Tag = dr[tb_孕妇_产前随访5次.所属机构].ToString();
            textEdit创建机构.Text = dr[tb_孕妇_产前随访5次.创建机构].ToString();
            textEdit创建人.Text = dr[tb_孕妇_产前随访5次.创建人].ToString();
            textEdit最近修改人.Text = dr[tb_孕妇_产前随访5次.修改人].ToString();

            cbo怀孕状态.Text = dr[tb_孕妇_产前随访5次.怀孕状态].ToString();
            if (cbo怀孕状态.Text == "流/引产")
            {
                dte流引产.Text = dr[tb_孕妇_产前随访5次.流引产日期].ToString();
                txt流引产孕周.Text = dr[tb_孕妇_产前随访5次.流引产孕周].ToString();
                txt流引产原因.Text = dr[tb_孕妇_产前随访5次.流引产原因].ToString();
                txt流引产机构.Text = dr[tb_孕妇_产前随访5次.流引产医院].ToString();
            }

            //变色
            Set考核项颜色_new(layoutControl1, labelControl考核项);
        }

        private void BindData()
        {
            BindDataForComboBox("ywyc", comboBoxEdit分类);
            //BindDataForComboBox("jzzk", comboBoxEdit居住状态);
            BindDataForComboBox("yw_youwu", comboBoxEdit转诊);
        }

        private void BindDataForComboBox(string p_fun_code, DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit)
        {
            DataView dv = new DataView(DataDictCache.Cache.t常用字典);
            dv.RowFilter = "P_FUN_CODE = '" + p_fun_code + "'";
            dv.Sort = "P_CODE";
            util.ControlsHelper.BindComboxDataNull(dv.ToTable(), comboBoxEdit, "P_CODE", "P_DESC");
        }

        protected override void DoBindingSummaryEditor(object dataSource)
        {
            DataRow drShowing = (DataRow)dataSource;
            if (drShowing == null)
            { }
            else
            {
                ShowDetailInfo(drShowing);
            }
        }

        private void UC第5次产前随访服务记录表_显示_Load(object sender, EventArgs e)
        {
            //GetDataFromDB();
        }

        private void btn删除_Click(object sender, EventArgs e)
        {
            string strRgid = textEdit当前所属机构.Tag.ToString();
            if (strRgid != Loginer.CurrentUser.所属机构)
            {
                Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                return;
            }

            bool isDelete = Msg.AskQuestion("确定要删除当前显示的随访记录吗？");
            if (isDelete)
            {
                string delID = textEdit档案编号.Tag.ToString();
                bool bret = m_bll5次.Delete(delID);

                bll孕妇基本信息 bll孕 = new bll孕妇基本信息();
                DataSet ds孕 = bll孕.GetBusinessByGrdabh(m_grdabh);
                DataRow[] delRow = m_ds孕5次.Tables[tb_孕妇_产前随访5次.__TableName].Select("ID=" + delID);

                if (bret && delRow.Length > 0 && (ds孕 != null && ds孕.Tables.Count > 0 && ds孕.Tables[0].Rows.Count > 0))
                {

                    string str孕次 = ds孕.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.孕次].ToString();
                    string str孕次_2 = delRow[0][tb_孕妇_产前随访5次.孕次].ToString();
                    string sfflag = ds孕.Tables[tb_孕妇基本信息.__TableName].Rows[0][tb_孕妇基本信息.SFFLAG].ToString();
                    if (str孕次_2.Equals(str孕次) && !(string.IsNullOrWhiteSpace(sfflag)) && Convert.ToInt32(sfflag) >= 1)
                    {
                        if ("5".Equals(sfflag))
                        {
                            DataSet dsSecond = m_bll5次.GetNextDateByGrdabhAndYc(m_grdabh, str孕次);

                            if (dsSecond == null || dsSecond.Tables.Count != 3
                                || (dsSecond.Tables[0].Rows.Count == 0 && dsSecond.Tables[1].Rows.Count == 0
                                    && dsSecond.Tables[2].Rows.Count == 0 && dsSecond.Tables[3].Rows.Count == 0))
                            {
                                bll孕.UpdateInfoByGrdabh(m_grdabh, "", "");
                            }
                            else
                            {
                                if (dsSecond.Tables[3].Rows.Count > 0)
                                {
                                    bll孕.UpdateInfoByGrdabh(m_grdabh, "4", dsSecond.Tables[3].Rows[0][0].ToString());
                                }
                                if (dsSecond.Tables[2].Rows.Count > 0)
                                {
                                    bll孕.UpdateInfoByGrdabh(m_grdabh, "3", dsSecond.Tables[2].Rows[0][0].ToString());
                                }
                                else if (dsSecond.Tables[1].Rows.Count > 0)
                                {
                                    bll孕.UpdateInfoByGrdabh(m_grdabh, "2", dsSecond.Tables[1].Rows[0][0].ToString());
                                }
                                else //if (dsSecond.Tables[0].Rows.Count > 0)
                                {
                                    bll孕.UpdateInfoByGrdabh(m_grdabh, "1", dsSecond.Tables[0].Rows[0][0].ToString());
                                }
                            }
                        }

                        //更新个人健康体征信息表
                        m_bll5次.Update个人体征(m_grdabh);
                    }
                }
                if (bret && delRow.Length > 0)
                {
                    //删除前台左侧导航栏的记录数
                    m_ds孕5次.Tables[tb_孕妇_产前随访5次.__TableName].Rows.Remove(delRow[0]);
                    CreateNavBarButton_new(m_ds孕5次.Tables[tb_孕妇_产前随访5次.__TableName], "发生时间");
                    Msg.ShowInformation("删除成功！");
                    UC第5次产前随访服务记录表 ctrl = new UC第5次产前随访服务记录表(m_grdabh, null, UpdateType.Add, null);
                    ShowControl(ctrl, DockStyle.Fill);
                }
                else
                {
                    Msg.ShowInformation("删除失败！");
                }
            }

        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            string rgid = textEdit当前所属机构.Tag.ToString();
            string updateID = textEdit档案编号.Tag.ToString();
            if (!canModifyBy同一机构(rgid))
            {
                Msg.ShowInformation("对不起，您只能对本机构的业务数据进行此操作！");
                return;
            }

            //UC儿童健康检查_30月 ctl = new UC儿童健康检查_30月(m_grdabh, UpdateType.Modify, null);
            UC第5次产前随访服务记录表 ctl = new UC第5次产前随访服务记录表(m_grdabh, updateID, UpdateType.Modify, null);
            ctl.Dock = DockStyle.Fill;
            ShowControl(ctl);
        }

        private void btn添加_Click(object sender, EventArgs e)
        {
            UC第5次产前随访服务记录表 ctl = new UC第5次产前随访服务记录表(m_grdabh, null, UpdateType.Add, null);
            ctl.Dock = DockStyle.Fill;
            ShowControl(ctl);
        }

        private void checkEdit指导其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit指导其他.Checked)
            {
                textEdit指导其他.Enabled = true;
            }
            else
            {
                textEdit指导其他.Enabled = false;
                textEdit指导其他.Text = "";
            }
        }

        //第2~5次产前随访服务记录表（打印表）
        private void btn导出_Click(object sender, EventArgs e)
        {
            string _docNo = this.textEdit档案编号.Text.Trim();
            if(!string.IsNullOrEmpty(_docNo))
            {
                roport第2至5次产前随访服务记录表 report = new roport第2至5次产前随访服务记录表(_docNo);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }
            
        }

        private void cbo怀孕状态_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo怀孕状态.Text != "流/引产")
            {
                txt流引产孕周.Enabled = false;
                dte流引产.Enabled = false;
                txt流引产原因.Enabled = false;
                txt流引产机构.Enabled = false;
            }
            else
            {
                txt流引产孕周.Enabled = true;
                dte流引产.Enabled = true;
                txt流引产原因.Enabled = true;
                txt流引产机构.Enabled = true;
            }
        }

        AtomEHR.Business.bll孕产妇随访化验单 bll孕产妇Image = new AtomEHR.Business.bll孕产妇随访化验单();
        private string _sfid = null;
        private void sbtn查看查体化验单_Click(object sender, EventArgs e)
        {
            FrmPIC frm = new FrmPIC();

            if (!string.IsNullOrWhiteSpace(_sfid))
            {
                DataSet dstemp = bll孕产妇Image.GetBusinessByDahAndPicid(m_grdabh, _sfid, false);

                byte[] arrbyte = dstemp.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICBYTE] as byte[];

                frm.SetImage(arrbyte);
                frm.SetReadOnly();
                frm.ShowDialog();
            }
            else
            {
                Msg.ShowInformation("未上传检查化验单图片");
            }
        }
    }
}
