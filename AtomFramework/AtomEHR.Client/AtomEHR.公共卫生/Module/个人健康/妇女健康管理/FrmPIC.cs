﻿using AtomEHR.Common;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class FrmPIC : XtraForm
    {
        public byte[] arrImageBytes = null;
        public FrmPIC()
        {
            InitializeComponent();
        }

        public void SetReadOnly()
        {
            this.sbtnUploadPic.Visible = false;
            this.simpleButton1.Visible = false;
            this.simpleButton2.Visible = false;
        }

        public void SetImage(byte[] imagebytes)
        {
            try
            {
                pictureEdit1.Image = (Image)ZipTools.DecompressionObject(imagebytes);
            }
            catch
            {
            }
        }

        private void sbtnUploadPic_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "JPG图片|*.jpg|png图片|*.png";
            if(openFileDialog1.ShowDialog()== DialogResult.OK)
            {
                string filename = openFileDialog1.FileName;
                pictureEdit1.Image = Image.FromFile(filename);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (this.pictureEdit1.Image == null)
            {
                Msg.ShowInformation("未选择图片，请先选择孕产妇化验报告单图片。");
                return;
            }

            arrImageBytes = ZipTools.CompressionObject(pictureEdit1.Image);

            this.DialogResult = DialogResult.OK;
        }

        private void FrmPIC_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(this.DialogResult != System.Windows.Forms.DialogResult.OK)
            {
                this.arrImageBytes = null;
            }
        }
    }
}
