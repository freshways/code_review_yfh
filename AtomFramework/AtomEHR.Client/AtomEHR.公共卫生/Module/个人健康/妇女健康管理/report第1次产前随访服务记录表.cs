﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class report第1次产前随访服务记录表 : DevExpress.XtraReports.UI.XtraReport
    {
        #region 
        DataSet _ds孕产妇;
        string docNo;
        bll孕妇产前一次随访记录 _bll随访 = new bll孕妇产前一次随访记录();
        #endregion
        public report第1次产前随访服务记录表()
        {
            InitializeComponent();
        }
        public report第1次产前随访服务记录表(string _docNo)
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds孕产妇 = _bll随访.GetInfoSuiFang(_docNo,true);
            DoBindingDataSource(_ds孕产妇);
        }

        private void DoBindingDataSource(DataSet _ds孕产妇)
        {
            DataTable dt孕产妇 = _ds孕产妇.Tables[Models.tb_孕妇_产前随访1次.__TableName];
            if (dt孕产妇 == null || dt孕产妇.Rows.Count == 0) return;

            DataTable dt健康档案 = _ds孕产妇.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.txt填表日期.Text =Convert.ToDateTime(dt孕产妇.Rows[0][tb_孕妇_产前随访1次.创建时间]).ToLongDateString().ToString();//将时间格式转换为年月日
            this.txt填表孕周.Text=dt孕产妇.Rows[0][tb_孕妇_产前随访1次.孕周].ToString();
            this.txt孕妇年龄.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.孕妇年龄].ToString();
            this.txt丈夫姓名.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.丈夫姓名].ToString();
            this.txt丈夫年龄.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.丈夫年龄].ToString();
            this.txt丈夫电话.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.丈夫电话].ToString();
            this.txt孕次.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.孕次].ToString();
            this.txt分娩次数.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.产次].ToString();
            this.txt剖宫产次数.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.剖宫产].ToString();
            this.txt末次月经时间.Text =dt孕产妇.Rows[0][tb_孕妇_产前随访1次.末次月经].ToString();
            this.txt预产期.Text = Convert.ToDateTime(dt孕产妇.Rows[0][tb_孕妇_产前随访1次.预产期]).ToLongDateString().ToString();
            string 既往史= (Convert.ToInt32(dt孕产妇.Rows[0][tb_孕妇_产前随访1次.既往病史])+1).ToString();
            if (!string.IsNullOrEmpty(既往史))
            {
                string[] a = 既往史.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt既往史" +(i+1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = "8";
                    else lbl.Text =a[i];
                }
            }
            this.txt既往史其他.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.既往病史其他].ToString();
            string 家族史 = (Convert.ToInt32(dt孕产妇.Rows[0][tb_孕妇_产前随访1次.家族史])+1).ToString();
            if (!string.IsNullOrEmpty(家族史))
            {
                string[] a = 家族史.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt家族史" +(i+1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = "3";
                    else lbl.Text = a[i];
                }
            }
            this.txt家族史其他.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.家族史其他].ToString();
            string 个人史 = (Convert.ToInt32(dt孕产妇.Rows[0][tb_孕妇_产前随访1次.个人史])+1).ToString();
            if (!string.IsNullOrEmpty(个人史))
            {
                string[] a = 个人史.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt个人史" +(i+1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = "6";
                    else lbl.Text = a[i];
                }
            }
            this.txt个人史其他.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.个人史其他].ToString();
            this.txt妇科手术史.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科手术史].ToString();
            this.txt妇科手术史有.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科手术史有].ToString();
            this.txt流产.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.流产].ToString();
            this.txt死胎.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.死胎].ToString();
            this.txt死产.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.死产].ToString();
            this.txt新生儿死亡.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.新生儿死亡].ToString();
            this.txt出生缺陷儿.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.出生缺陷儿].ToString();
            this.txt身高.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.身高].ToString();
            this.txt体重.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.体重].ToString();
            //体质指数=体重/(身高*身高)

            this.txt血压1.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血压1].ToString();
            this.txt血压2.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血压2].ToString();
            this.txt心脏.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.心脏].ToString();
            this.txt心脏异常.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.心脏异常].ToString();
            this.txt肺部.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.肺部].ToString();
            this.txt肺部异常.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.肺部异常].ToString();
            this.txt外阴.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科外阴].ToString();
            this.txt外阴异常.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科外阴异常].ToString();
            this.txt阴道.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科阴道].ToString();
            this.txt阴道异常.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科阴道异常].ToString();
            this.txt宫颈.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科宫颈].ToString();
            this.txt宫颈异常.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科宫颈异常].ToString();
            this.txt子宫.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科子宫].ToString();
            this.txt子宫异常.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科子宫异常].ToString();
            this.txt附件.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科附件].ToString();
            this.txt附件异常.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.妇科附件异常].ToString();
            this.txt血红蛋白值.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血红蛋白].ToString();
            this.txt白细胞计数值.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.白细胞计数].ToString();
            this.txt血小板计数值.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血小板计数].ToString();
            this.txt血常规其他.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血常规其他].ToString();
            this.txt尿蛋白.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.尿蛋白].ToString();
            this.txt尿糖.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.尿糖].ToString();
            this.txt尿酮体.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.尿酮体].ToString();
            this.txt尿潜血.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.尿潜血].ToString();
            this.txt尿常规其他.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.尿常规其他].ToString();
            this.txt血型.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血型].ToString();
            this.txt血糖.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血糖].ToString();
            this.txt血清谷丙转氨酶.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血清谷丙转氨酶].ToString();
            this.txt血清谷草转氨酶.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血清谷草转氨酶].ToString();
            this.txt白蛋白.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.白蛋白].ToString();
            this.txt总胆红素.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.总胆红素].ToString();
            this.txt结合胆红素.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.结合胆红素].ToString();
            this.txt血清肌酐.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血清肌酐].ToString();
            this.txt血尿素氮.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.血尿素氮].ToString();
            string 阴道分泌物 = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.阴道分泌物].ToString();
            if (!string.IsNullOrEmpty(阴道分泌物))
            {
                string[] a = 阴道分泌物.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i]))
                    {
                        continue;
                    }
                    string strName = "txt阴道分泌物" +(i+1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    lbl.Text = a[i];
                }
            }
            this.txt阴道分泌物其他.Text= dt孕产妇.Rows[0][tb_孕妇_产前随访1次.阴道分泌物其他].ToString();
            this.txt阴道清洁度.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.阴道清洁度].ToString();
            this.txt阴道分泌物其他.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.阴道分泌物其他].ToString();
            this.txt乙型肝炎表面抗原.Text= dt孕产妇.Rows[0][tb_孕妇_产前随访1次.乙型肝炎表面抗原].ToString();
            this.txt乙型肝炎表面抗体.Text=dt孕产妇.Rows[0][tb_孕妇_产前随访1次.乙型肝炎表面抗体].ToString();
            this.txt乙型肝炎e抗原.Text=dt孕产妇.Rows[0][tb_孕妇_产前随访1次.乙型肝炎e抗原].ToString();
            this.txt乙型肝炎e抗体.Text=dt孕产妇.Rows[0][tb_孕妇_产前随访1次.乙型肝炎e抗体].ToString();
            this.txt乙型肝炎核心抗体.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.乙型肝炎核心抗体].ToString();
            this.txt梅毒血清学试验.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.梅毒血清学试验].ToString();
            this.txtHIV抗体检测.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.HIV抗体检测].ToString();
            this.txtB超.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.B超].ToString();
            this.txt总体评估.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.总体评估].ToString();
            this.txt总体评估异常.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.总体评估其他].ToString();
            string 保健指导 = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.保健指导].ToString();
            if (!string.IsNullOrEmpty(保健指导))
            {
                string[] a = 保健指导.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt保健指导" +(i+1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    lbl.Text = a[i];
                }
            }
            this.txt保健指导其他.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.保健指导其他].ToString();
            this.txt转诊.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.转诊].ToString();
            this.txt转诊原因.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.转诊原因].ToString();
            this.txt转诊机构.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.转诊机构及科室].ToString();
            this.txt下次随访日期.Text = Convert.ToDateTime(dt孕产妇.Rows[0][tb_孕妇_产前随访1次.下次随访日期]).ToLongDateString().ToString();
            this.txt随访医生签名.Text = dt孕产妇.Rows[0][tb_孕妇_产前随访1次.随访医师].ToString();

        }

    }
}
