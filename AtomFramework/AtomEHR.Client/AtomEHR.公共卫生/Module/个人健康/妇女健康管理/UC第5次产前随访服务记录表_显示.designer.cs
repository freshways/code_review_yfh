﻿namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    partial class UC第5次产前随访服务记录表_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC第5次产前随访服务记录表_显示));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn添加 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.dte流引产 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txt流引产孕周 = new DevExpress.XtraEditors.TextEdit();
            this.txt流引产机构 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txt流引产原因 = new DevExpress.XtraEditors.TextEdit();
            this.cbo怀孕状态 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txt居民签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt产前检查机构名称 = new DevExpress.XtraEditors.TextEdit();
            this.txt随访方式 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl考核项 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit分类异常 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit尿蛋白 = new DevExpress.XtraEditors.TextEdit();
            this.ucTxtLbl血红蛋白 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl孕次 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl胎心率 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit胎位 = new DevExpress.XtraEditors.TextEdit();
            this.ucTxtLbl腹围 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel16 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxEdit转诊 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ucLblTxt转诊原因 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.ucLblTxt转诊机构 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc联系人 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc联系方式 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc结果 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit指导个人卫生 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导膳食 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导心理 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit分娩准备 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit母乳喂养 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导运动 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit自我监测 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit指导其他 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit指导其他 = new DevExpress.XtraEditors.TextEdit();
            this.ucTxtLblTxtLbl血压 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.ucTxtLbl体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl宫底高度 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLblTxtLbl孕周 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit其他检查 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit孕妇姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit卡号 = new DevExpress.XtraEditors.TextEdit();
            this.comboBoxEdit分类 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit随访日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit下次随访日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住状态 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit主诉 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem75 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem76 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sbtn查看查体化验单 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte流引产.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt流引产孕周.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt流引产机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt流引产原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo怀孕状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产前检查机构名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit分类异常.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit尿蛋白.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit胎位.Properties)).BeginInit();
            this.flowLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit转诊.Properties)).BeginInit();
            this.flowLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导个人卫生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导膳食.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导心理.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit分娩准备.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit母乳喂养.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导运动.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自我监测.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit其他检查.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit分类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit主诉.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Size = new System.Drawing.Size(102, 469);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(102, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(701, 32);
            this.panelControl1.TabIndex = 6;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn添加);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Controls.Add(this.sbtn查看查体化验单);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(697, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn添加
            // 
            this.btn添加.Image = ((System.Drawing.Image)(resources.GetObject("btn添加.Image")));
            this.btn添加.Location = new System.Drawing.Point(3, 3);
            this.btn添加.Name = "btn添加";
            this.btn添加.Size = new System.Drawing.Size(75, 23);
            this.btn添加.TabIndex = 0;
            this.btn添加.Text = "添加";
            this.btn添加.Click += new System.EventHandler(this.btn添加_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(84, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 2;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(165, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 3;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(246, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 4;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.txt居民签名);
            this.layoutControl1.Controls.Add(this.txt产前检查机构名称);
            this.layoutControl1.Controls.Add(this.txt随访方式);
            this.layoutControl1.Controls.Add(this.labelControl考核项);
            this.layoutControl1.Controls.Add(this.textEdit分类异常);
            this.layoutControl1.Controls.Add(this.textEdit尿蛋白);
            this.layoutControl1.Controls.Add(this.ucTxtLbl血红蛋白);
            this.layoutControl1.Controls.Add(this.ucTxtLbl孕次);
            this.layoutControl1.Controls.Add(this.ucTxtLbl胎心率);
            this.layoutControl1.Controls.Add(this.textEdit胎位);
            this.layoutControl1.Controls.Add(this.ucTxtLbl腹围);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel16);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel15);
            this.layoutControl1.Controls.Add(this.ucTxtLblTxtLbl血压);
            this.layoutControl1.Controls.Add(this.ucTxtLbl体重);
            this.layoutControl1.Controls.Add(this.ucTxtLbl宫底高度);
            this.layoutControl1.Controls.Add(this.ucTxtLblTxtLbl孕周);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit创建人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近更新时间);
            this.layoutControl1.Controls.Add(this.textEdit创建时间);
            this.layoutControl1.Controls.Add(this.textEdit联系电话);
            this.layoutControl1.Controls.Add(this.textEdit居住地址);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.textEdit其他检查);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit孕妇姓名);
            this.layoutControl1.Controls.Add(this.textEdit档案编号);
            this.layoutControl1.Controls.Add(this.textEdit卡号);
            this.layoutControl1.Controls.Add(this.comboBoxEdit分类);
            this.layoutControl1.Controls.Add(this.textEdit随访日期);
            this.layoutControl1.Controls.Add(this.textEdit下次随访日期);
            this.layoutControl1.Controls.Add(this.textEdit居住状态);
            this.layoutControl1.Controls.Add(this.textEdit主诉);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(102, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(701, 437);
            this.layoutControl1.TabIndex = 8;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.dte流引产);
            this.panelControl2.Controls.Add(this.labelControl5);
            this.panelControl2.Controls.Add(this.txt流引产孕周);
            this.panelControl2.Controls.Add(this.txt流引产机构);
            this.panelControl2.Controls.Add(this.labelControl3);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.labelControl4);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.txt流引产原因);
            this.panelControl2.Controls.Add(this.cbo怀孕状态);
            this.panelControl2.Location = new System.Drawing.Point(103, 132);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(573, 60);
            this.panelControl2.TabIndex = 125;
            // 
            // dte流引产
            // 
            this.dte流引产.Location = new System.Drawing.Point(72, 31);
            this.dte流引产.Name = "dte流引产";
            this.dte流引产.Properties.ReadOnly = true;
            this.dte流引产.Size = new System.Drawing.Size(100, 20);
            this.dte流引产.TabIndex = 9;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(184, 34);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(12, 14);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "孕";
            // 
            // txt流引产孕周
            // 
            this.txt流引产孕周.Enabled = false;
            this.txt流引产孕周.Location = new System.Drawing.Point(202, 31);
            this.txt流引产孕周.Name = "txt流引产孕周";
            this.txt流引产孕周.Properties.Mask.EditMask = "\\d{0,2}";
            this.txt流引产孕周.Properties.Mask.IgnoreMaskBlank = false;
            this.txt流引产孕周.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txt流引产孕周.Properties.Mask.ShowPlaceHolders = false;
            this.txt流引产孕周.Properties.ReadOnly = true;
            this.txt流引产孕周.Size = new System.Drawing.Size(42, 20);
            this.txt流引产孕周.TabIndex = 7;
            // 
            // txt流引产机构
            // 
            this.txt流引产机构.Enabled = false;
            this.txt流引产机构.Location = new System.Drawing.Point(425, 31);
            this.txt流引产机构.Name = "txt流引产机构";
            this.txt流引产机构.Properties.ReadOnly = true;
            this.txt流引产机构.Size = new System.Drawing.Size(122, 20);
            this.txt流引产机构.TabIndex = 6;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(553, 34);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(48, 14);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "终止妊娠";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(407, 34);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(12, 14);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "在";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(6, 34);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(60, 14);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "流引产日期";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(250, 34);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(32, 14);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "周  因";
            // 
            // txt流引产原因
            // 
            this.txt流引产原因.Enabled = false;
            this.txt流引产原因.Location = new System.Drawing.Point(289, 31);
            this.txt流引产原因.Name = "txt流引产原因";
            this.txt流引产原因.Properties.ReadOnly = true;
            this.txt流引产原因.Size = new System.Drawing.Size(110, 20);
            this.txt流引产原因.TabIndex = 3;
            // 
            // cbo怀孕状态
            // 
            this.cbo怀孕状态.EditValue = "";
            this.cbo怀孕状态.Location = new System.Drawing.Point(5, 4);
            this.cbo怀孕状态.Name = "cbo怀孕状态";
            this.cbo怀孕状态.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo怀孕状态.Properties.Items.AddRange(new object[] {
            "",
            "在孕",
            "流/引产"});
            this.cbo怀孕状态.Properties.ReadOnly = true;
            this.cbo怀孕状态.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo怀孕状态.Size = new System.Drawing.Size(106, 20);
            this.cbo怀孕状态.TabIndex = 0;
            this.cbo怀孕状态.SelectedIndexChanged += new System.EventHandler(this.cbo怀孕状态_SelectedIndexChanged);
            // 
            // txt居民签名
            // 
            this.txt居民签名.Location = new System.Drawing.Point(556, 501);
            this.txt居民签名.Name = "txt居民签名";
            this.txt居民签名.Size = new System.Drawing.Size(120, 20);
            this.txt居民签名.StyleController = this.layoutControl1;
            this.txt居民签名.TabIndex = 88;
            // 
            // txt产前检查机构名称
            // 
            this.txt产前检查机构名称.Location = new System.Drawing.Point(448, 196);
            this.txt产前检查机构名称.Name = "txt产前检查机构名称";
            this.txt产前检查机构名称.Size = new System.Drawing.Size(228, 20);
            this.txt产前检查机构名称.StyleController = this.layoutControl1;
            this.txt产前检查机构名称.TabIndex = 87;
            // 
            // txt随访方式
            // 
            this.txt随访方式.Location = new System.Drawing.Point(103, 196);
            this.txt随访方式.Name = "txt随访方式";
            this.txt随访方式.Size = new System.Drawing.Size(234, 20);
            this.txt随访方式.StyleController = this.layoutControl1;
            this.txt随访方式.TabIndex = 86;
            // 
            // labelControl考核项
            // 
            this.labelControl考核项.Location = new System.Drawing.Point(8, 35);
            this.labelControl考核项.Name = "labelControl考核项";
            this.labelControl考核项.Size = new System.Drawing.Size(668, 20);
            this.labelControl考核项.StyleController = this.layoutControl1;
            this.labelControl考核项.TabIndex = 85;
            // 
            // textEdit分类异常
            // 
            this.textEdit分类异常.Enabled = false;
            this.textEdit分类异常.Location = new System.Drawing.Point(486, 317);
            this.textEdit分类异常.Name = "textEdit分类异常";
            this.textEdit分类异常.Properties.ReadOnly = true;
            this.textEdit分类异常.Size = new System.Drawing.Size(190, 20);
            this.textEdit分类异常.StyleController = this.layoutControl1;
            this.textEdit分类异常.TabIndex = 84;
            // 
            // textEdit尿蛋白
            // 
            this.textEdit尿蛋白.Location = new System.Drawing.Point(549, 293);
            this.textEdit尿蛋白.Name = "textEdit尿蛋白";
            this.textEdit尿蛋白.Properties.ReadOnly = true;
            this.textEdit尿蛋白.Size = new System.Drawing.Size(127, 20);
            this.textEdit尿蛋白.StyleController = this.layoutControl1;
            this.textEdit尿蛋白.TabIndex = 82;
            // 
            // ucTxtLbl血红蛋白
            // 
            this.ucTxtLbl血红蛋白.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucTxtLbl血红蛋白.Lbl1Text = "g/L";
            this.ucTxtLbl血红蛋白.Location = new System.Drawing.Point(337, 293);
            this.ucTxtLbl血红蛋白.Name = "ucTxtLbl血红蛋白";
            this.ucTxtLbl血红蛋白.Size = new System.Drawing.Size(123, 20);
            this.ucTxtLbl血红蛋白.TabIndex = 81;
            this.ucTxtLbl血红蛋白.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // ucTxtLbl孕次
            // 
            this.ucTxtLbl孕次.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl孕次.Lbl1Text = "次";
            this.ucTxtLbl孕次.Location = new System.Drawing.Point(337, 269);
            this.ucTxtLbl孕次.Name = "ucTxtLbl孕次";
            this.ucTxtLbl孕次.Size = new System.Drawing.Size(339, 20);
            this.ucTxtLbl孕次.TabIndex = 80;
            this.ucTxtLbl孕次.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl胎心率
            // 
            this.ucTxtLbl胎心率.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl胎心率.Lbl1Text = "次/分钟";
            this.ucTxtLbl胎心率.Location = new System.Drawing.Point(103, 269);
            this.ucTxtLbl胎心率.Name = "ucTxtLbl胎心率";
            this.ucTxtLbl胎心率.Size = new System.Drawing.Size(145, 20);
            this.ucTxtLbl胎心率.TabIndex = 79;
            this.ucTxtLbl胎心率.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // textEdit胎位
            // 
            this.textEdit胎位.Location = new System.Drawing.Point(549, 245);
            this.textEdit胎位.Name = "textEdit胎位";
            this.textEdit胎位.Properties.ReadOnly = true;
            this.textEdit胎位.Size = new System.Drawing.Size(127, 20);
            this.textEdit胎位.StyleController = this.layoutControl1;
            this.textEdit胎位.TabIndex = 78;
            // 
            // ucTxtLbl腹围
            // 
            this.ucTxtLbl腹围.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl腹围.Lbl1Text = "cm";
            this.ucTxtLbl腹围.Location = new System.Drawing.Point(337, 245);
            this.ucTxtLbl腹围.Name = "ucTxtLbl腹围";
            this.ucTxtLbl腹围.Size = new System.Drawing.Size(123, 20);
            this.ucTxtLbl腹围.TabIndex = 77;
            this.ucTxtLbl腹围.Txt1Size = new System.Drawing.Size(40, 20);
            // 
            // flowLayoutPanel16
            // 
            this.flowLayoutPanel16.Controls.Add(this.label1);
            this.flowLayoutPanel16.Controls.Add(this.comboBoxEdit转诊);
            this.flowLayoutPanel16.Controls.Add(this.ucLblTxt转诊原因);
            this.flowLayoutPanel16.Controls.Add(this.ucLblTxt转诊机构);
            this.flowLayoutPanel16.Controls.Add(this.uc联系人);
            this.flowLayoutPanel16.Controls.Add(this.uc联系方式);
            this.flowLayoutPanel16.Controls.Add(this.uc结果);
            this.flowLayoutPanel16.Location = new System.Drawing.Point(103, 391);
            this.flowLayoutPanel16.Name = "flowLayoutPanel16";
            this.flowLayoutPanel16.Size = new System.Drawing.Size(573, 56);
            this.flowLayoutPanel16.TabIndex = 76;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 7, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "有无转诊：";
            // 
            // comboBoxEdit转诊
            // 
            this.comboBoxEdit转诊.Location = new System.Drawing.Point(74, 3);
            this.comboBoxEdit转诊.Name = "comboBoxEdit转诊";
            this.comboBoxEdit转诊.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit转诊.Properties.ReadOnly = true;
            this.comboBoxEdit转诊.Size = new System.Drawing.Size(48, 20);
            this.comboBoxEdit转诊.TabIndex = 0;
            // 
            // ucLblTxt转诊原因
            // 
            this.ucLblTxt转诊原因.Enabled = false;
            this.ucLblTxt转诊原因.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucLblTxt转诊原因.Lbl1Text = "原因:";
            this.ucLblTxt转诊原因.Location = new System.Drawing.Point(128, 3);
            this.ucLblTxt转诊原因.Name = "ucLblTxt转诊原因";
            this.ucLblTxt转诊原因.Size = new System.Drawing.Size(153, 22);
            this.ucLblTxt转诊原因.TabIndex = 1;
            this.ucLblTxt转诊原因.Txt1Size = new System.Drawing.Size(110, 20);
            // 
            // ucLblTxt转诊机构
            // 
            this.ucLblTxt转诊机构.Enabled = false;
            this.ucLblTxt转诊机构.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucLblTxt转诊机构.Lbl1Text = "机构及科室:";
            this.ucLblTxt转诊机构.Location = new System.Drawing.Point(284, 0);
            this.ucLblTxt转诊机构.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxt转诊机构.Name = "ucLblTxt转诊机构";
            this.ucLblTxt转诊机构.Size = new System.Drawing.Size(185, 22);
            this.ucLblTxt转诊机构.TabIndex = 2;
            this.ucLblTxt转诊机构.Txt1Size = new System.Drawing.Size(110, 20);
            // 
            // uc联系人
            // 
            this.uc联系人.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc联系人.Lbl1Text = "联系人：";
            this.uc联系人.Location = new System.Drawing.Point(3, 31);
            this.uc联系人.Name = "uc联系人";
            this.uc联系人.Size = new System.Drawing.Size(119, 22);
            this.uc联系人.TabIndex = 5;
            this.uc联系人.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // uc联系方式
            // 
            this.uc联系方式.Lbl1Size = new System.Drawing.Size(60, 18);
            this.uc联系方式.Lbl1Text = "联系方式：";
            this.uc联系方式.Location = new System.Drawing.Point(128, 31);
            this.uc联系方式.Name = "uc联系方式";
            this.uc联系方式.Size = new System.Drawing.Size(176, 22);
            this.uc联系方式.TabIndex = 6;
            this.uc联系方式.Txt1Size = new System.Drawing.Size(110, 20);
            // 
            // uc结果
            // 
            this.uc结果.Lbl1Size = new System.Drawing.Size(40, 18);
            this.uc结果.Lbl1Text = "结果：";
            this.uc结果.Location = new System.Drawing.Point(310, 31);
            this.uc结果.Name = "uc结果";
            this.uc结果.Size = new System.Drawing.Size(110, 22);
            this.uc结果.TabIndex = 7;
            this.uc结果.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.Controls.Add(this.checkEdit指导个人卫生);
            this.flowLayoutPanel15.Controls.Add(this.checkEdit指导膳食);
            this.flowLayoutPanel15.Controls.Add(this.checkEdit指导心理);
            this.flowLayoutPanel15.Controls.Add(this.checkEdit分娩准备);
            this.flowLayoutPanel15.Controls.Add(this.checkEdit母乳喂养);
            this.flowLayoutPanel15.Controls.Add(this.checkEdit指导运动);
            this.flowLayoutPanel15.Controls.Add(this.checkEdit自我监测);
            this.flowLayoutPanel15.Controls.Add(this.checkEdit指导其他);
            this.flowLayoutPanel15.Controls.Add(this.textEdit指导其他);
            this.flowLayoutPanel15.Location = new System.Drawing.Point(103, 341);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(573, 46);
            this.flowLayoutPanel15.TabIndex = 41;
            // 
            // checkEdit指导个人卫生
            // 
            this.checkEdit指导个人卫生.Location = new System.Drawing.Point(0, 0);
            this.checkEdit指导个人卫生.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导个人卫生.Name = "checkEdit指导个人卫生";
            this.checkEdit指导个人卫生.Properties.Caption = "生活方式";
            this.checkEdit指导个人卫生.Properties.ReadOnly = true;
            this.checkEdit指导个人卫生.Size = new System.Drawing.Size(78, 19);
            this.checkEdit指导个人卫生.TabIndex = 31;
            // 
            // checkEdit指导膳食
            // 
            this.checkEdit指导膳食.Location = new System.Drawing.Point(78, 0);
            this.checkEdit指导膳食.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导膳食.Name = "checkEdit指导膳食";
            this.checkEdit指导膳食.Properties.Caption = "营养";
            this.checkEdit指导膳食.Properties.ReadOnly = true;
            this.checkEdit指导膳食.Size = new System.Drawing.Size(59, 19);
            this.checkEdit指导膳食.TabIndex = 32;
            // 
            // checkEdit指导心理
            // 
            this.checkEdit指导心理.Location = new System.Drawing.Point(137, 0);
            this.checkEdit指导心理.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导心理.Name = "checkEdit指导心理";
            this.checkEdit指导心理.Properties.Caption = "心理";
            this.checkEdit指导心理.Properties.ReadOnly = true;
            this.checkEdit指导心理.Size = new System.Drawing.Size(55, 19);
            this.checkEdit指导心理.TabIndex = 33;
            // 
            // checkEdit分娩准备
            // 
            this.checkEdit分娩准备.Location = new System.Drawing.Point(192, 0);
            this.checkEdit分娩准备.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit分娩准备.Name = "checkEdit分娩准备";
            this.checkEdit分娩准备.Properties.Caption = "分娩准备";
            this.checkEdit分娩准备.Properties.ReadOnly = true;
            this.checkEdit分娩准备.Size = new System.Drawing.Size(80, 19);
            this.checkEdit分娩准备.TabIndex = 34;
            // 
            // checkEdit母乳喂养
            // 
            this.checkEdit母乳喂养.Location = new System.Drawing.Point(272, 0);
            this.checkEdit母乳喂养.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit母乳喂养.Name = "checkEdit母乳喂养";
            this.checkEdit母乳喂养.Properties.Caption = "母乳喂养";
            this.checkEdit母乳喂养.Properties.ReadOnly = true;
            this.checkEdit母乳喂养.Size = new System.Drawing.Size(78, 19);
            this.checkEdit母乳喂养.TabIndex = 35;
            // 
            // checkEdit指导运动
            // 
            this.checkEdit指导运动.Location = new System.Drawing.Point(350, 0);
            this.checkEdit指导运动.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导运动.Name = "checkEdit指导运动";
            this.checkEdit指导运动.Properties.Caption = "运动";
            this.checkEdit指导运动.Properties.ReadOnly = true;
            this.checkEdit指导运动.Size = new System.Drawing.Size(58, 19);
            this.checkEdit指导运动.TabIndex = 36;
            // 
            // checkEdit自我监测
            // 
            this.checkEdit自我监测.Location = new System.Drawing.Point(408, 0);
            this.checkEdit自我监测.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit自我监测.Name = "checkEdit自我监测";
            this.checkEdit自我监测.Properties.Caption = "自我监测";
            this.checkEdit自我监测.Properties.ReadOnly = true;
            this.checkEdit自我监测.Size = new System.Drawing.Size(78, 19);
            this.checkEdit自我监测.TabIndex = 37;
            // 
            // checkEdit指导其他
            // 
            this.checkEdit指导其他.Location = new System.Drawing.Point(486, 0);
            this.checkEdit指导其他.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit指导其他.Name = "checkEdit指导其他";
            this.checkEdit指导其他.Properties.Caption = "其他";
            this.checkEdit指导其他.Properties.ReadOnly = true;
            this.checkEdit指导其他.Size = new System.Drawing.Size(54, 19);
            this.checkEdit指导其他.TabIndex = 38;
            this.checkEdit指导其他.CheckedChanged += new System.EventHandler(this.checkEdit指导其他_CheckedChanged);
            // 
            // textEdit指导其他
            // 
            this.textEdit指导其他.Enabled = false;
            this.textEdit指导其他.Location = new System.Drawing.Point(0, 19);
            this.textEdit指导其他.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit指导其他.Name = "textEdit指导其他";
            this.textEdit指导其他.Properties.ReadOnly = true;
            this.textEdit指导其他.Size = new System.Drawing.Size(110, 20);
            this.textEdit指导其他.TabIndex = 14;
            // 
            // ucTxtLblTxtLbl血压
            // 
            this.ucTxtLblTxtLbl血压.Lbl1Size = new System.Drawing.Size(10, 14);
            this.ucTxtLblTxtLbl血压.Lbl1Text = "/";
            this.ucTxtLblTxtLbl血压.Lbl2Size = new System.Drawing.Size(40, 14);
            this.ucTxtLblTxtLbl血压.Lbl2Text = "mmHg";
            this.ucTxtLblTxtLbl血压.Location = new System.Drawing.Point(103, 293);
            this.ucTxtLblTxtLbl血压.Name = "ucTxtLblTxtLbl血压";
            this.ucTxtLblTxtLbl血压.Size = new System.Drawing.Size(145, 20);
            this.ucTxtLblTxtLbl血压.TabIndex = 49;
            this.ucTxtLblTxtLbl血压.Txt1EditValue = null;
            this.ucTxtLblTxtLbl血压.Txt1Size = new System.Drawing.Size(35, 20);
            this.ucTxtLblTxtLbl血压.Txt2EditValue = null;
            this.ucTxtLblTxtLbl血压.Txt2Size = new System.Drawing.Size(35, 20);
            // 
            // ucTxtLbl体重
            // 
            this.ucTxtLbl体重.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl体重.Lbl1Text = "kg";
            this.ucTxtLbl体重.Location = new System.Drawing.Point(549, 220);
            this.ucTxtLbl体重.Name = "ucTxtLbl体重";
            this.ucTxtLbl体重.Size = new System.Drawing.Size(127, 21);
            this.ucTxtLbl体重.TabIndex = 47;
            this.ucTxtLbl体重.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // ucTxtLbl宫底高度
            // 
            this.ucTxtLbl宫底高度.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl宫底高度.Lbl1Text = "cm";
            this.ucTxtLbl宫底高度.Location = new System.Drawing.Point(103, 245);
            this.ucTxtLbl宫底高度.Name = "ucTxtLbl宫底高度";
            this.ucTxtLbl宫底高度.Size = new System.Drawing.Size(145, 20);
            this.ucTxtLbl宫底高度.TabIndex = 46;
            this.ucTxtLbl宫底高度.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // ucTxtLblTxtLbl孕周
            // 
            this.ucTxtLblTxtLbl孕周.Lbl1Size = new System.Drawing.Size(18, 14);
            this.ucTxtLblTxtLbl孕周.Lbl1Text = "周";
            this.ucTxtLblTxtLbl孕周.Lbl2Size = new System.Drawing.Size(18, 14);
            this.ucTxtLblTxtLbl孕周.Lbl2Text = "天";
            this.ucTxtLblTxtLbl孕周.Location = new System.Drawing.Point(337, 220);
            this.ucTxtLblTxtLbl孕周.Name = "ucTxtLblTxtLbl孕周";
            this.ucTxtLblTxtLbl孕周.Size = new System.Drawing.Size(123, 21);
            this.ucTxtLblTxtLbl孕周.TabIndex = 35;
            this.ucTxtLblTxtLbl孕周.Txt1EditValue = null;
            this.ucTxtLblTxtLbl孕周.Txt1Size = new System.Drawing.Size(35, 20);
            this.ucTxtLblTxtLbl孕周.Txt2EditValue = null;
            this.ucTxtLblTxtLbl孕周.Txt2Size = new System.Drawing.Size(35, 20);
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(556, 550);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit最近修改人.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(120, 20);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 33;
            // 
            // textEdit创建人
            // 
            this.textEdit创建人.Location = new System.Drawing.Point(347, 550);
            this.textEdit创建人.Name = "textEdit创建人";
            this.textEdit创建人.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建人.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建人.Properties.ReadOnly = true;
            this.textEdit创建人.Size = new System.Drawing.Size(120, 20);
            this.textEdit创建人.StyleController = this.layoutControl1;
            this.textEdit创建人.TabIndex = 32;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(103, 550);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建机构.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(155, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 31;
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(556, 526);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit当前所属机构.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(120, 20);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 30;
            // 
            // textEdit最近更新时间
            // 
            this.textEdit最近更新时间.Location = new System.Drawing.Point(347, 526);
            this.textEdit最近更新时间.Name = "textEdit最近更新时间";
            this.textEdit最近更新时间.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit最近更新时间.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit最近更新时间.Properties.ReadOnly = true;
            this.textEdit最近更新时间.Size = new System.Drawing.Size(120, 20);
            this.textEdit最近更新时间.StyleController = this.layoutControl1;
            this.textEdit最近更新时间.TabIndex = 29;
            // 
            // textEdit创建时间
            // 
            this.textEdit创建时间.Location = new System.Drawing.Point(103, 526);
            this.textEdit创建时间.Name = "textEdit创建时间";
            this.textEdit创建时间.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建时间.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建时间.Properties.ReadOnly = true;
            this.textEdit创建时间.Size = new System.Drawing.Size(155, 20);
            this.textEdit创建时间.StyleController = this.layoutControl1;
            this.textEdit创建时间.TabIndex = 28;
            // 
            // textEdit联系电话
            // 
            this.textEdit联系电话.Location = new System.Drawing.Point(103, 83);
            this.textEdit联系电话.Name = "textEdit联系电话";
            this.textEdit联系电话.Properties.ReadOnly = true;
            this.textEdit联系电话.Size = new System.Drawing.Size(145, 20);
            this.textEdit联系电话.StyleController = this.layoutControl1;
            this.textEdit联系电话.TabIndex = 25;
            // 
            // textEdit居住地址
            // 
            this.textEdit居住地址.Location = new System.Drawing.Point(337, 107);
            this.textEdit居住地址.Name = "textEdit居住地址";
            this.textEdit居住地址.Properties.ReadOnly = true;
            this.textEdit居住地址.Size = new System.Drawing.Size(339, 20);
            this.textEdit居住地址.StyleController = this.layoutControl1;
            this.textEdit居住地址.TabIndex = 15;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(347, 501);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Properties.ReadOnly = true;
            this.textEdit随访医生.Size = new System.Drawing.Size(120, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 11;
            // 
            // textEdit其他检查
            // 
            this.textEdit其他检查.Location = new System.Drawing.Point(103, 317);
            this.textEdit其他检查.Name = "textEdit其他检查";
            this.textEdit其他检查.Properties.ReadOnly = true;
            this.textEdit其他检查.Size = new System.Drawing.Size(145, 20);
            this.textEdit其他检查.StyleController = this.layoutControl1;
            this.textEdit其他检查.TabIndex = 9;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(337, 83);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(122, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 8;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(548, 83);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(128, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 7;
            // 
            // textEdit孕妇姓名
            // 
            this.textEdit孕妇姓名.Location = new System.Drawing.Point(548, 59);
            this.textEdit孕妇姓名.Name = "textEdit孕妇姓名";
            this.textEdit孕妇姓名.Properties.ReadOnly = true;
            this.textEdit孕妇姓名.Size = new System.Drawing.Size(128, 20);
            this.textEdit孕妇姓名.StyleController = this.layoutControl1;
            this.textEdit孕妇姓名.TabIndex = 6;
            // 
            // textEdit档案编号
            // 
            this.textEdit档案编号.Location = new System.Drawing.Point(337, 59);
            this.textEdit档案编号.Name = "textEdit档案编号";
            this.textEdit档案编号.Properties.ReadOnly = true;
            this.textEdit档案编号.Size = new System.Drawing.Size(122, 20);
            this.textEdit档案编号.StyleController = this.layoutControl1;
            this.textEdit档案编号.TabIndex = 5;
            // 
            // textEdit卡号
            // 
            this.textEdit卡号.Location = new System.Drawing.Point(103, 59);
            this.textEdit卡号.Name = "textEdit卡号";
            this.textEdit卡号.Properties.ReadOnly = true;
            this.textEdit卡号.Size = new System.Drawing.Size(145, 20);
            this.textEdit卡号.StyleController = this.layoutControl1;
            this.textEdit卡号.TabIndex = 4;
            // 
            // comboBoxEdit分类
            // 
            this.comboBoxEdit分类.Location = new System.Drawing.Point(337, 317);
            this.comboBoxEdit分类.Name = "comboBoxEdit分类";
            this.comboBoxEdit分类.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit分类.Properties.ReadOnly = true;
            this.comboBoxEdit分类.Size = new System.Drawing.Size(145, 20);
            this.comboBoxEdit分类.StyleController = this.layoutControl1;
            this.comboBoxEdit分类.TabIndex = 50;
            // 
            // textEdit随访日期
            // 
            this.textEdit随访日期.Location = new System.Drawing.Point(103, 220);
            this.textEdit随访日期.Name = "textEdit随访日期";
            this.textEdit随访日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.textEdit随访日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEdit随访日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.textEdit随访日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEdit随访日期.Properties.Mask.BeepOnError = true;
            this.textEdit随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.textEdit随访日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.textEdit随访日期.Properties.ReadOnly = true;
            this.textEdit随访日期.Size = new System.Drawing.Size(145, 20);
            this.textEdit随访日期.StyleController = this.layoutControl1;
            this.textEdit随访日期.TabIndex = 34;
            // 
            // textEdit下次随访日期
            // 
            this.textEdit下次随访日期.Location = new System.Drawing.Point(103, 501);
            this.textEdit下次随访日期.Name = "textEdit下次随访日期";
            this.textEdit下次随访日期.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.textEdit下次随访日期.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEdit下次随访日期.Properties.EditFormat.FormatString = "yyyy-MM-dd";
            this.textEdit下次随访日期.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.textEdit下次随访日期.Properties.ReadOnly = true;
            this.textEdit下次随访日期.Size = new System.Drawing.Size(155, 20);
            this.textEdit下次随访日期.StyleController = this.layoutControl1;
            this.textEdit下次随访日期.TabIndex = 10;
            // 
            // textEdit居住状态
            // 
            this.textEdit居住状态.Location = new System.Drawing.Point(103, 107);
            this.textEdit居住状态.Name = "textEdit居住状态";
            this.textEdit居住状态.Properties.ReadOnly = true;
            this.textEdit居住状态.Size = new System.Drawing.Size(145, 20);
            this.textEdit居住状态.StyleController = this.layoutControl1;
            this.textEdit居住状态.TabIndex = 14;
            // 
            // textEdit主诉
            // 
            this.textEdit主诉.Location = new System.Drawing.Point(103, 451);
            this.textEdit主诉.Name = "textEdit主诉";
            this.textEdit主诉.Properties.ReadOnly = true;
            this.textEdit主诉.Size = new System.Drawing.Size(498, 46);
            this.textEdit主诉.StyleController = this.layoutControl1;
            this.textEdit主诉.TabIndex = 83;
            this.textEdit主诉.UseOptimizedRendering = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem5,
            this.layoutControlItem12,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem6,
            this.layoutControlItem22,
            this.layoutControlItem11,
            this.layoutControlItem44,
            this.layoutControlItem43,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem46,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem47,
            this.layoutControlItem75,
            this.layoutControlItem19,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.emptySpaceItem2,
            this.emptySpaceItem1,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem76,
            this.layoutControlItem23,
            this.layoutControlItem24});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(684, 588);
            this.layoutControlGroup1.Text = "第5次产前随访服务记录表";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.textEdit卡号;
            this.layoutControlItem1.CustomizationFormText = "卡 号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "卡 号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem7.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.textEdit下次随访日期;
            this.layoutControlItem7.CustomizationFormText = "产后休养地";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 466);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(254, 25);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Tag = "check";
            this.layoutControlItem7.Text = "下次随访日期";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.textEdit档案编号;
            this.layoutControlItem2.CustomizationFormText = "档案编号 ";
            this.layoutControlItem2.Location = new System.Drawing.Point(244, 24);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(211, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "档案编号 ";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.textEdit孕妇姓名;
            this.layoutControlItem3.CustomizationFormText = "孕妇姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(455, 24);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(217, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "孕妇姓名";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.textEdit身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(455, 48);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(217, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证号 ";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem8.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.textEdit随访医生;
            this.layoutControlItem8.CustomizationFormText = "联系电话";
            this.layoutControlItem8.Location = new System.Drawing.Point(254, 466);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(209, 25);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Tag = "check";
            this.layoutControlItem8.Text = "随访医生签名";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.textEdit创建时间;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 491);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(254, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "创建时间";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.textEdit最近更新时间;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(254, 491);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "最近更新时间";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.textEdit当前所属机构;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(463, 491);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 48);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "当前所属机构";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.textEdit创建机构;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 515);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(254, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "创建机构";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.textEdit创建人;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(254, 515);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "创建人";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.textEdit最近修改人;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(463, 515);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(0, 48);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "最近修改人";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.textEdit出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(244, 48);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(211, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.Control = this.textEdit居住地址;
            this.layoutControlItem12.CustomizationFormText = "居住地址 ";
            this.layoutControlItem12.Location = new System.Drawing.Point(244, 72);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(428, 25);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "居住地址 ";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.textEdit随访日期;
            this.layoutControlItem31.CustomizationFormText = "随访日期";
            this.layoutControlItem31.Location = new System.Drawing.Point(0, 185);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(244, 25);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Tag = "check";
            this.layoutControlItem31.Text = "随访日期";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.ucTxtLblTxtLbl孕周;
            this.layoutControlItem32.CustomizationFormText = "填表孕周";
            this.layoutControlItem32.Location = new System.Drawing.Point(244, 185);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(155, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(212, 25);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Tag = "check";
            this.layoutControlItem32.Text = "孕周";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.textEdit其他检查;
            this.layoutControlItem6.CustomizationFormText = "年 龄";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 282);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "其他辅助检查* ";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.textEdit联系电话;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "联系电话 ";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.textEdit居住状态;
            this.layoutControlItem11.CustomizationFormText = "居住状态 ";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(244, 25);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "居住状态 ";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem44.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.ucTxtLbl体重;
            this.layoutControlItem44.CustomizationFormText = "体重";
            this.layoutControlItem44.Location = new System.Drawing.Point(456, 185);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(189, 24);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(216, 25);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Tag = "check";
            this.layoutControlItem44.Text = "体重";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem43.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.ucTxtLbl宫底高度;
            this.layoutControlItem43.CustomizationFormText = "身高";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 210);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(189, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Tag = "check";
            this.layoutControlItem43.Text = "宫底高度";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.ucTxtLbl腹围;
            this.layoutControlItem9.CustomizationFormText = "腹 围";
            this.layoutControlItem9.Location = new System.Drawing.Point(244, 210);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(212, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Tag = "check";
            this.layoutControlItem9.Text = "腹 围";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.textEdit胎位;
            this.layoutControlItem10.CustomizationFormText = "胎位";
            this.layoutControlItem10.Location = new System.Drawing.Point(456, 210);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(81, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Tag = "check";
            this.layoutControlItem10.Text = "胎位";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.Control = this.ucTxtLbl胎心率;
            this.layoutControlItem13.CustomizationFormText = "胎 心 率";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 234);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Tag = "check";
            this.layoutControlItem13.Text = "胎 心 率";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.ucTxtLbl孕次;
            this.layoutControlItem14.CustomizationFormText = "孕 次";
            this.layoutControlItem14.Location = new System.Drawing.Point(244, 234);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(428, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Tag = "check";
            this.layoutControlItem14.Text = "孕 次";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem46.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.ucTxtLblTxtLbl血压;
            this.layoutControlItem46.CustomizationFormText = "血压";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 258);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(131, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(244, 24);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Tag = "check";
            this.layoutControlItem46.Text = "血压";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.Control = this.ucTxtLbl血红蛋白;
            this.layoutControlItem15.CustomizationFormText = "血红蛋白";
            this.layoutControlItem15.Location = new System.Drawing.Point(244, 258);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(212, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Tag = "check";
            this.layoutControlItem15.Text = "血红蛋白";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.Control = this.textEdit尿蛋白;
            this.layoutControlItem16.CustomizationFormText = "尿 蛋 白";
            this.layoutControlItem16.Location = new System.Drawing.Point(456, 258);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Tag = "check";
            this.layoutControlItem16.Text = "尿 蛋 白";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem47.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem47.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem47.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.comboBoxEdit分类;
            this.layoutControlItem47.CustomizationFormText = "心脏";
            this.layoutControlItem47.Location = new System.Drawing.Point(244, 282);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(139, 24);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Tag = "check";
            this.layoutControlItem47.Text = "分 类";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem75
            // 
            this.layoutControlItem75.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem75.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem75.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem75.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem75.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem75.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem75.Control = this.flowLayoutPanel15;
            this.layoutControlItem75.CustomizationFormText = "保健指导";
            this.layoutControlItem75.Location = new System.Drawing.Point(0, 306);
            this.layoutControlItem75.MinSize = new System.Drawing.Size(159, 50);
            this.layoutControlItem75.Name = "layoutControlItem75";
            this.layoutControlItem75.Size = new System.Drawing.Size(672, 50);
            this.layoutControlItem75.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem75.Tag = "check";
            this.layoutControlItem75.Text = "指 导";
            this.layoutControlItem75.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem75.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem75.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.Control = this.textEdit主诉;
            this.layoutControlItem19.CustomizationFormText = "主 诉";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 416);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(50, 50);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(597, 50);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Tag = "check";
            this.layoutControlItem19.Text = "主 诉";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.textEdit分类异常;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(478, 282);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(194, 24);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.labelControl考核项;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(74, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(672, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 539);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(672, 10);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(597, 416);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(75, 50);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem20.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.txt随访方式;
            this.layoutControlItem20.CustomizationFormText = "随访方式";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 161);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(333, 24);
            this.layoutControlItem20.Tag = "check";
            this.layoutControlItem20.Text = "随访方式";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.Control = this.txt产前检查机构名称;
            this.layoutControlItem21.CustomizationFormText = "产前检查机构名称";
            this.layoutControlItem21.Location = new System.Drawing.Point(333, 161);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(339, 24);
            this.layoutControlItem21.Tag = "check";
            this.layoutControlItem21.Text = "产前检查机构名称";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem76
            // 
            this.layoutControlItem76.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem76.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem76.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem76.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem76.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem76.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem76.Control = this.flowLayoutPanel16;
            this.layoutControlItem76.CustomizationFormText = "转诊";
            this.layoutControlItem76.Location = new System.Drawing.Point(0, 356);
            this.layoutControlItem76.MinSize = new System.Drawing.Size(202, 60);
            this.layoutControlItem76.Name = "layoutControlItem76";
            this.layoutControlItem76.Size = new System.Drawing.Size(672, 60);
            this.layoutControlItem76.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem76.Tag = "check";
            this.layoutControlItem76.Text = "转诊";
            this.layoutControlItem76.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem76.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem76.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.Control = this.txt居民签名;
            this.layoutControlItem23.CustomizationFormText = "居民签名";
            this.layoutControlItem23.Location = new System.Drawing.Point(463, 466);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(209, 25);
            this.layoutControlItem23.Tag = "check";
            this.layoutControlItem23.Text = "居民签名";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.panelControl2;
            this.layoutControlItem24.CustomizationFormText = "怀孕状态";
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 97);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(0, 64);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(199, 64);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(672, 64);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "怀孕状态";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // sbtn查看查体化验单
            // 
            this.sbtn查看查体化验单.Image = ((System.Drawing.Image)(resources.GetObject("sbtn查看查体化验单.Image")));
            this.sbtn查看查体化验单.Location = new System.Drawing.Point(327, 3);
            this.sbtn查看查体化验单.Name = "sbtn查看查体化验单";
            this.sbtn查看查体化验单.Size = new System.Drawing.Size(120, 23);
            this.sbtn查看查体化验单.TabIndex = 6;
            this.sbtn查看查体化验单.Text = "查看查体化验单";
            this.sbtn查看查体化验单.Click += new System.EventHandler(this.sbtn查看查体化验单_Click);
            // 
            // UC第5次产前随访服务记录表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC第5次产前随访服务记录表_显示";
            this.Size = new System.Drawing.Size(803, 469);
            this.Load += new System.EventHandler(this.UC第5次产前随访服务记录表_显示_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dte流引产.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt流引产孕周.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt流引产机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt流引产原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo怀孕状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产前检查机构名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt随访方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit分类异常.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit尿蛋白.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit胎位.Properties)).EndInit();
            this.flowLayoutPanel16.ResumeLayout(false);
            this.flowLayoutPanel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit转诊.Properties)).EndInit();
            this.flowLayoutPanel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导个人卫生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导膳食.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导心理.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit分娩准备.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit母乳喂养.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导运动.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit自我监测.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit指导其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit其他检查.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit分类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit主诉.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn添加;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.LabelControl labelControl考核项;
        private DevExpress.XtraEditors.TextEdit textEdit分类异常;
        private DevExpress.XtraEditors.TextEdit textEdit尿蛋白;
        private Library.UserControls.UCTxtLbl ucTxtLbl血红蛋白;
        private Library.UserControls.UCTxtLbl ucTxtLbl孕次;
        private Library.UserControls.UCTxtLbl ucTxtLbl胎心率;
        private DevExpress.XtraEditors.TextEdit textEdit胎位;
        private Library.UserControls.UCTxtLbl ucTxtLbl腹围;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel16;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit转诊;
        private Library.UserControls.UCLblTxt ucLblTxt转诊原因;
        private Library.UserControls.UCLblTxt ucLblTxt转诊机构;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private DevExpress.XtraEditors.TextEdit textEdit指导其他;
        private Library.UserControls.UCTxtLblTxtLbl ucTxtLblTxtLbl血压;
        private Library.UserControls.UCTxtLbl ucTxtLbl体重;
        private Library.UserControls.UCTxtLbl ucTxtLbl宫底高度;
        private Library.UserControls.UCTxtLblTxtLbl ucTxtLblTxtLbl孕周;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraEditors.TextEdit textEdit创建人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近更新时间;
        private DevExpress.XtraEditors.TextEdit textEdit创建时间;
        private DevExpress.XtraEditors.TextEdit textEdit联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit居住地址;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraEditors.TextEdit textEdit其他检查;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit孕妇姓名;
        private DevExpress.XtraEditors.TextEdit textEdit档案编号;
        private DevExpress.XtraEditors.TextEdit textEdit卡号;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit分类;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem76;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem75;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit textEdit随访日期;
        private DevExpress.XtraEditors.TextEdit textEdit下次随访日期;
        private DevExpress.XtraEditors.TextEdit textEdit居住状态;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导个人卫生;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导膳食;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导心理;
        private DevExpress.XtraEditors.CheckEdit checkEdit分娩准备;
        private DevExpress.XtraEditors.CheckEdit checkEdit母乳喂养;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导运动;
        private DevExpress.XtraEditors.CheckEdit checkEdit自我监测;
        private DevExpress.XtraEditors.CheckEdit checkEdit指导其他;
        private DevExpress.XtraEditors.MemoEdit textEdit主诉;
        private DevExpress.XtraEditors.TextEdit txt产前检查机构名称;
        private DevExpress.XtraEditors.TextEdit txt随访方式;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private System.Windows.Forms.Label label1;
        private Library.UserControls.UCLblTxt uc联系人;
        private Library.UserControls.UCLblTxt uc联系方式;
        private DevExpress.XtraEditors.TextEdit txt居民签名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private Library.UserControls.UCLblTxt uc结果;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.TextEdit dte流引产;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit txt流引产孕周;
        private DevExpress.XtraEditors.TextEdit txt流引产机构;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txt流引产原因;
        private DevExpress.XtraEditors.ComboBoxEdit cbo怀孕状态;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraEditors.SimpleButton sbtn查看查体化验单;
    }
}
