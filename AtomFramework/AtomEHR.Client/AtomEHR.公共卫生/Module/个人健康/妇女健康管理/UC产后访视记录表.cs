﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business.Security;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Library;
using DevExpress.XtraEditors;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class UC产后访视记录表 : UserControlBase
    {
        #region Fields
        AtomEHR.Business.bll孕妇_产后访视情况 _Bll = new Business.bll孕妇_产后访视情况();
        DataSet _ds产后访视;
        DataTable _dt孕妇基本信息;
        DataTable _dt个人健康档案;
        DataTable _dt产后随访;
        DataTable _dt个人健康特征;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        #endregion

        public UC产后访视记录表()
        {
            InitializeComponent();
        }

        public UC产后访视记录表(Form frm, AtomEHR.Common.UpdateType updateType)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _UpdateType = updateType;
            _serverDateTime = _Bll.ServiceDateTime;
            _docNo = _frm._docNo;
            _id = _frm._param as string;
            if (_UpdateType == UpdateType.Add)//添加随访
            {
                _Bll.GetBusinessByKey(_docNo, true);
                _Bll.NewBusiness();
                _ds产后访视 = _Bll.CurrentBusiness;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                _ds产后访视 = _Bll.GetOneDataByKey(_docNo, _id, true);
                //this.dte随访日期.Properties.ReadOnly = true;
                //this.dte下次随访日期.Properties.ReadOnly = true;
            }
            DoBindingSummaryEditor(_ds产后访视);//绑定数据
        }

        private void UC产后访视记录表_Load(object sender, EventArgs e)
        {

            InitView();

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无, cbo转诊有无, false);
            this.cbo转诊有无.SelectedIndex = 2;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo乳房, false);
            this.cbo乳房.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo恶露, false);
            this.cbo恶露.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo分类, false);
            this.cbo分类.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo伤口, false);
            this.cbo伤口.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo子宫, false);
            this.cbo子宫.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t是否到位, cbo是否到位, false);

            DoBindingSummaryEditor(_ds产后访视);//绑定数据
        }
        private void DoBindingSummaryEditor(DataSet _ds产后访视)
        {
            if (_ds产后访视 == null) return;
            if (_ds产后访视.Tables.Count == 0) return;
            _dt孕妇基本信息 = _ds产后访视.Tables[tb_孕妇基本信息.__TableName];
            _dt产后随访 = _ds产后访视.Tables[tb_孕妇_产后访视情况.__TableName];
            _dt个人健康档案 = _ds产后访视.Tables[tb_健康档案.__TableName];
            _dt个人健康特征 = _ds产后访视.Tables[tb_健康档案_个人健康特征.__TableName];

            //change 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 ▽
            //if (_dt孕妇基本信息.Rows.Count >= 1) //2017-06-22 16:15:01 有多行的也取第一行判断
            //{
            //    DataRow dr = _dt孕妇基本信息.Rows[0];
            //    this.textEdit孕妇姓名.Text = util.DESEncrypt.DES解密(dr[tb_孕妇基本信息.姓名].ToString());
            //    this.textEdit档案编号.Text = dr[tb_孕妇基本信息.个人档案编号].ToString();
            //    this.textEdit身份证号.Text = dr[tb_孕妇基本信息.身份证号].ToString();
            //    this.textEdit出生日期.Text = dr[tb_孕妇基本信息.出生日期].ToString();
            //    //this.txt联系电话.Text = dr[tb_孕妇基本信息.联系电话].ToString();
            //    this.textEdit居住地址.Text = _Bll.Return地区名称(dr[tb_孕妇基本信息.省].ToString()) + _Bll.Return地区名称(dr[tb_孕妇基本信息.市].ToString()) + _Bll.Return地区名称(dr[tb_孕妇基本信息.区].ToString()) + _Bll.Return地区名称(dr[tb_孕妇基本信息.街道].ToString()) + _Bll.Return地区名称(dr[tb_孕妇基本信息.居委会].ToString()) + dr[tb_孕妇基本信息.居住地址].ToString();
            //    string chanci = dr[tb_孕妇基本信息.产次].ToString();
            //    if (!string.IsNullOrEmpty(chanci))
            //    {
            //        //给 textedit孕次控件绑定数据
            //        SetDataSourceForChanci(chanci);
            //    }
            //}
            if (_dt个人健康档案.Rows.Count >= 1) //2017-06-22 16:15:01 有多行的也取第一行判断
            {
                DataRow dr = _dt个人健康档案.Rows[0];
                this.textEdit孕妇姓名.Text = util.DESEncrypt.DES解密(dr[tb_健康档案.姓名].ToString());
                this.textEdit档案编号.Text = dr[tb_健康档案.个人档案编号].ToString();
                this.textEdit身份证号.Text = dr[tb_健康档案.身份证号].ToString();
                this.textEdit出生日期.Text = dr[tb_健康档案.出生日期].ToString();
                //this.txt联系电话.Text = dr[tb_健康档案.联系电话].ToString();
                this.textEdit联系电话.Text = string.IsNullOrWhiteSpace(dr[tb_健康档案.本人电话].ToString()) ? dr[tb_健康档案.本人电话].ToString() : dr[tb_健康档案.联系人电话].ToString();
                this.textEdit居住地址.Text = _Bll.Return地区名称(dr[tb_健康档案.省].ToString()) + _Bll.Return地区名称(dr[tb_健康档案.市].ToString()) 
                    + _Bll.Return地区名称(dr[tb_健康档案.区].ToString()) + _Bll.Return地区名称(dr[tb_健康档案.街道].ToString()) 
                    + _Bll.Return地区名称(dr[tb_健康档案.居委会].ToString()) + dr[tb_健康档案.居住地址].ToString();
                string chanci = dr[tb_健康档案.产次].ToString();
                if (!string.IsNullOrEmpty(chanci))
                {
                    //给 textedit孕次控件绑定数据
                    SetDataSourceForChanci(chanci);
                }
            }
            //change 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 △


            if (_dt产后随访.Rows.Count == 1)
            {
                BindSuiFangData(_dt产后随访);
            }
            if (_UpdateType == UpdateType.Add)
            {
                this.textEdit创建人.Text = Loginer.CurrentUser.AccountName;
                this.textEdit创建时间.Text = _serverDateTime;
                this.textEdit最近更新时间.Text = _serverDateTime;
                this.textEdit当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
                this.textEdit创建机构.Text = Loginer.CurrentUser.所属机构名称;
                this.textEdit最近修改人.Text = Loginer.CurrentUser.AccountName;
                //this.textEdit录入医生.Text = Loginer.CurrentUser.AccountName;

            }
            else if (_UpdateType == UpdateType.Modify)
            {
                this.textEdit创建人.Text = _Bll.Return用户名称(_dt产后随访.Rows[0][tb_老年人随访.创建人].ToString());
                this.textEdit创建时间.Text = _serverDateTime;
                this.textEdit最近更新时间.Text = _serverDateTime;
                this.textEdit当前所属机构.Text = _Bll.Return机构名称(_dt产后随访.Rows[0][tb_老年人随访.所属机构].ToString());
                this.textEdit创建机构.Text = _Bll.Return机构名称(_dt产后随访.Rows[0][tb_老年人随访.创建机构].ToString());// Loginer.CurrentUser.所属机构名称;
                this.textEdit最近修改人.Text = Loginer.CurrentUser.AccountName; //Loginer.CurrentUser.AccountName;
            }
        }

        private void BindSuiFangData(DataTable dataTable)
        {
            if (dataTable == null) return;

            #region  新版本添加
            DataBinder.BindingTextEditDateTime(date分娩日期, dataTable, tb_孕妇_产后访视情况.分娩日期);
            DataBinder.BindingTextEditDateTime(date出院日期, dataTable, tb_孕妇_产后访视情况.出院日期);
            DataBinder.BindingTextEdit(txt居民或家属签名, dataTable, tb_孕妇_产后访视情况.居民或家属签名);
            DataBinder.BindingTextEdit(txt失访原因, dataTable, tb_孕妇_产后访视情况.失访原因);
            if (dataTable.Rows[0][tb_孕妇_产后访视情况.失访情况] != null && !string.IsNullOrEmpty(dataTable.Rows[0][tb_孕妇_产后访视情况.失访情况].ToString()))
            {
                ch失访情况.Checked = true;
            }

            #endregion

            DataBinder.BindingTextEdit(textEdit卡号, dataTable, tb_孕妇_产后访视情况.卡号);
            DataBinder.BindingTextEditDateTime(dte随访日期, dataTable, tb_孕妇_产后访视情况.随访日期);
            DataBinder.BindingTextEdit(uc体温.Txt1, dataTable, tb_孕妇_产后访视情况.体温);
            DataBinder.BindingTextEdit(uc血压.Txt1, dataTable, tb_孕妇_产后访视情况.血压1);
            DataBinder.BindingTextEdit(uc血压.Txt2, dataTable, tb_孕妇_产后访视情况.血压2);
            DataBinder.BindingTextEdit(textEdit一般健康情况, dataTable, tb_孕妇_产后访视情况.一般健康情况);
            DataBinder.BindingTextEdit(textEdit一般心理状况, dataTable, tb_孕妇_产后访视情况.一般心理状况);
            DataBinder.BindingTextEdit(textEdit乳房, dataTable, tb_孕妇_产后访视情况.乳房异常);
            DataBinder.BindingTextEdit(textEdit伤口, dataTable, tb_孕妇_产后访视情况.伤口异常);
            DataBinder.BindingTextEdit(textEdit恶露, dataTable, tb_孕妇_产后访视情况.恶露异常);
            DataBinder.BindingTextEdit(textEdit子宫, dataTable, tb_孕妇_产后访视情况.子宫异常);
            DataBinder.BindingTextEdit(textEdit其他, dataTable, tb_孕妇_产后访视情况.其他);
            DataBinder.BindingTextEdit(textEdit分类, dataTable, tb_孕妇_产后访视情况.分类异常);
            SetFlowLayoutResult(dataTable.Rows[0][tb_孕妇_产后访视情况.指导].ToString(), flow指导);
            DataBinder.BindingTextEdit(textEdit指导其他, dataTable, tb_孕妇_产后访视情况.指导其他);

            //DataBinder.BindingTextEdit(uc转诊原因.Txt1, dataTable, tb_孕妇_产后访视情况.转诊原因);
            //DataBinder.BindingTextEdit(uc转诊机构.Txt1, dataTable, tb_孕妇_产后访视情况.机构科室);
            DataBinder.BindingTextEdit(dte下次随访日期, dataTable, tb_孕妇_产后访视情况.下次随访日期);
            DataBinder.BindingTextEdit(textEdit随访医生, dataTable, tb_孕妇_产后访视情况.随访医生签名);


            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产后访视情况.乳房].ToString(), cbo乳房);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产后访视情况.恶露].ToString(), cbo恶露);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产后访视情况.子宫].ToString(), cbo子宫);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产后访视情况.伤口].ToString(), cbo伤口);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产后访视情况.分类].ToString(), cbo分类);
            //util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产后访视情况.转诊].ToString(), cbo转诊有无);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产后访视情况.产次].ToString(), cbo产次);

            string str转诊 = dataTable.Rows[0][tb_孕妇_产后访视情况.转诊].ToString();
            util.ControlsHelper.SetComboxData(str转诊, cbo转诊有无);
            if (!string.IsNullOrEmpty(str转诊) && str转诊.Equals("1"))//有
            {
                uc转诊原因.Txt1.Text = dataTable.Rows[0][tb_孕妇_产后访视情况.转诊原因].ToString();
                uc转诊机构.Txt1.Text = dataTable.Rows[0][tb_孕妇_产后访视情况.机构科室].ToString();
                uc联系人.Txt1.Text = dataTable.Rows[0][tb_孕妇_产后访视情况.联系人].ToString();
                uc联系方式.Txt1.Text = dataTable.Rows[0][tb_孕妇_产后访视情况.联系方式].ToString();
                string str是否到位 = dataTable.Rows[0][tb_孕妇_产后访视情况.结果].ToString();
                util.ControlsHelper.SetComboxData(str是否到位, cbo是否到位);
            }
        }
        private void InitView()
        {

        }

        private void SetDataSourceForChanci(string chanci)
        {
            int yunCi = 0;
            if (Int32.TryParse(chanci, out yunCi))
            {
                DataTable table = new DataTable();
                DataColumn Code = new DataColumn("Code", System.Type.GetType("System.String"));
                DataColumn Value = new DataColumn("Value", System.Type.GetType("System.String"));

                table.Columns.Add(Code);
                table.Columns.Add(Value);

                for (int i = 1; i <= (yunCi+1); i++)
                {
                    DataRow newRow = table.NewRow();
                    newRow["Code"] = i + "次";
                    newRow["Value"] = i.ToString();
                    table.Rows.Add(newRow);
                }

                util.ControlsHelper.BindComboxData(table, cbo产次, "Value", "Code");
                util.ControlsHelper.SetComboxData(yunCi.ToString(), cbo产次);
                //this.cbo产次.EditValue = chanci;
            }
        }

        private void btn保存_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                if ((_UpdateType == UpdateType.Add && Msg.AskQuestion("信息保存后，‘随访日期’和‘下次随访时间’将不允许修改，确认保存信息？")) || _UpdateType == UpdateType.Modify)
                {
                    #region  新版本添加
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.分娩日期] = this.date分娩日期.Text;
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.出院日期] = this.date出院日期.Text;
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.转诊原因] = this.uc转诊原因.Txt1.Text;
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.机构科室] = this.uc转诊机构.Txt1.Text;
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.联系人] = this.uc联系人.Txt1.Text;
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.联系方式] = this.uc联系方式.Txt1.Text;
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.结果] = util.ControlsHelper.GetComboxKey(cbo是否到位);
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.居民或家属签名] = this.txt居民或家属签名.Text;

                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.失访情况] = this.ch失访情况.Checked ? "1" : "";
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.失访原因] = this.txt失访原因.Text.Trim();
                    #endregion

                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.随访日期] = this.dte随访日期.Text;
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.下次随访日期] = this.dte下次随访日期.Text;
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.个人档案编号] = _docNo;
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.指导] = GetFlowLayoutResult(flow指导);

                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.乳房] = util.ControlsHelper.GetComboxKey(cbo乳房);
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.恶露] = util.ControlsHelper.GetComboxKey(cbo恶露);
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.子宫] = util.ControlsHelper.GetComboxKey(cbo子宫);
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.伤口] = util.ControlsHelper.GetComboxKey(cbo伤口);
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.分类] = util.ControlsHelper.GetComboxKey(cbo分类);
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.产次] = util.ControlsHelper.GetComboxKey(cbo产次);
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.转诊] = util.ControlsHelper.GetComboxKey(cbo转诊有无);
                    int i = Get缺项();
                    int j = Get完整度(i);
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.缺项] = i;
                    _dt产后随访.Rows[0][tb_孕妇_产后访视情况.完整度] = j;
                    if (_UpdateType == UpdateType.Add)
                    {
                        _dt产后随访.Rows[0][tb_孕妇_产后访视情况.创建机构] = Loginer.CurrentUser.所属机构;
                        _dt产后随访.Rows[0][tb_孕妇_产后访视情况.所属机构] = Loginer.CurrentUser.所属机构;
                        _dt产后随访.Rows[0][tb_孕妇_产后访视情况.创建时间] = this.textEdit创建时间.Text;
                        _dt产后随访.Rows[0][tb_孕妇_产后访视情况.创建人] = Loginer.CurrentUser.用户编码;
                        _dt产后随访.Rows[0][tb_孕妇_产后访视情况.修改人] = Loginer.CurrentUser.用户编码;
                        _dt产后随访.Rows[0][tb_孕妇_产后访视情况.修改时间] = this.textEdit最近更新时间.Text;
                    }
                    else if (_UpdateType == UpdateType.Modify)
                    {
                        _dt产后随访.Rows[0][tb_孕妇_产后访视情况.修改人] = Loginer.CurrentUser.用户编码;
                        _dt产后随访.Rows[0][tb_孕妇_产后访视情况.修改时间] = this.textEdit最近更新时间.Text;
                    }

                    if ((_dt个人健康档案.Rows[0][tb_健康档案.怀孕情况] != null) && ("已孕未生产" == _dt个人健康档案.Rows[0][tb_健康档案.怀孕情况].ToString()))
                    {
                        _dt个人健康档案.Rows[0][tb_健康档案.怀孕情况] = "已生产随访期内";
                        _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.第一次产前检查表] = "";
                        _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.第二次产前检查表] = "";
                        _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.第三次产前检查表] = "";
                        _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.第四次产前检查表] = "";
                        _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.第五次产前检查表] = "";
                        _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.孕产妇产后访视] = i + "" + j;
                        _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.孕产妇产后42天] = "未建";
                        _dt孕妇基本信息.Rows[0][tb_孕妇基本信息.下次随访时间] = this.dte下次随访日期.Text;

                    }
                    if ((_dt个人健康档案.Rows[0][tb_健康档案.怀孕情况] != null) && ("已生产随访期内" == _dt个人健康档案.Rows[0][tb_健康档案.怀孕情况].ToString()))
                    {
                        _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.孕产妇产后访视] = i + "" + j;
                        _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.孕产妇产后42天] = "未建";
                        _dt孕妇基本信息.Rows[0][tb_孕妇基本信息.下次随访时间] = this.dte下次随访日期.Text;
                    }

                    //if ((_dt个人健康档案.Rows[0][tb_健康档案.怀孕情况] != null) && ("已孕未生产" == _dt个人健康档案.Rows[0][tb_健康档案.怀孕情况].ToString()) && (_dt个人健康档案.Rows[0][tb_健康档案.孕次].ToString() == util.ControlsHelper.GetComboxKey(this.textEdit孕次)))
                    //{
                    //    _dt孕妇基本信息.Rows[0][tb_孕妇基本信息.下次随访时间] = dte下次随访日期.Text;
                    //    _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.孕产妇产后访视] = i + "," + j;
                    //}

                    if (_UpdateType == UpdateType.Modify) _Bll.WriteLog();
                    DataSet dsTemplate = _Bll.CreateSaveData(_ds产后访视, _UpdateType); //创建用于保存的临时数据
                    SaveResult result = _Bll.Save(dsTemplate);//调用业务逻辑保存数据方法
                    if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
                    {
                        //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                        //this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据
                        this._UpdateType = UpdateType.None;
                        Msg.ShowInformation("保存成功!");
                        //保存成功后跳转到显示页面
                        UC产后访视记录表_显示 uc = new UC产后访视记录表_显示(_frm);
                        ShowControl(uc, DockStyle.Fill);
                    }
                    else
                        Msg.Warning("保存失败!");
                }
            }
        }

        private int Get完整度(int i)
        {
            int k = Convert.ToInt32((19 - i) * 100 / 19.0);
            return k;
        }

        private int Get缺项()
        {
            int i = 0;
            if (this.dte随访日期.Text.Trim() == "") i++;
            if (this.date分娩日期.Text.Trim() == "") i++;
            if (this.date出院日期.Text.Trim() == "") i++;
            if (this.uc体温.Txt1.Text.Trim() == "") i++;
            if (this.textEdit一般健康情况.Text.Trim() == "") i++;
            if (this.textEdit一般心理状况.Text.Trim() == "") i++;
            if (this.cbo产次.Text.Trim() == "" || this.cbo产次.Text.Trim() == "请选择") i++;
            if (this.cbo乳房.Text.Trim() == "" || this.cbo乳房.Text.Trim() == "请选择") i++;
            if (this.cbo恶露.Text.Trim() == "" || this.cbo恶露.Text.Trim() == "请选择") i++;
            if (this.cbo分类.Text.Trim() == "" || this.cbo分类.Text.Trim() == "请选择") i++;
            if (this.cbo伤口.Text.Trim() == "" || this.cbo伤口.Text.Trim() == "请选择") i++;
            if (this.cbo子宫.Text.Trim() == "" || this.cbo子宫.Text.Trim() == "请选择") i++;
            if (this.cbo转诊有无.Text.Trim() == "" || this.cbo转诊有无.Text.Trim() == "请选择") i++;

            if (this.textEdit其他.Text.Trim() == "") i++;
            if (GetFlowLayoutResult(flow指导) == "") i++;
            if (this.dte下次随访日期.Text.Trim() == "") i++;
            if (this.textEdit随访医生.Text.Trim() == "") i++;
            if (this.txt居民或家属签名.Text.Trim() == "") i++;

            return i;
        }

        private bool funCheck()
        {
            if (this.dte随访日期.Text.Trim() == "")
            {
                Msg.Warning("随访日期是必填项，请填写随访日期！");
                this.dte随访日期.Focus();
                return false;
            }
            if (this.dte下次随访日期.Text.Trim() == "")
            {
                Msg.Warning("下次随访日期是必填项，请填写下次随访日期！");
                this.dte下次随访日期.Focus();
                return false;
            }
            if (this.dte下次随访日期.DateTime < this.dte随访日期.DateTime)
            {
                Msg.Warning("下次随访日期不能小于填表日期，请修改下次随访日期！");
                this.dte下次随访日期.Focus();
                return false;
            }
            //if (dte随访日期.DateTime > Convert.ToDateTime(this.textEdit创建时间.Text))
            //{
            //    Msg.ShowInformation("随访日期不能大于填写日期");
            //    dte随访日期.Focus();
            //    return false;
            //}
            string chanci = util.ControlsHelper.GetComboxKey(cbo产次);
            if (string.IsNullOrEmpty(chanci))
            {
                Msg.Warning("请选择产次！");
                this.cbo产次.Focus();
                return false;
            }
            if (_UpdateType == UpdateType.Add && _Bll.CheckExistsByChanCi(_docNo, chanci))
            {
                Msg.Warning("产次为" + chanci + "的产后访视记录表已建立,请重新填写!");
                this.cbo产次.Focus();
                return false;
            }
            return true;
        }

        private void cbo乳房_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetControlVisble(cbo乳房, textEdit乳房);
        }

        private void SetControlVisble(ComboBoxEdit cbo, TextEdit textEdit)
        {
            string index = cbo.Text.Trim();
            if (!string.IsNullOrEmpty(index) && index == "异常")
            {
                textEdit.Visible = true;
            }
            else
            {
                textEdit.Visible = false;
                textEdit.Text = "";
            }
        }

        private void cbo恶露_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetControlVisble(cbo恶露, textEdit恶露);
        }

        private void cbo子宫_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetControlVisble(cbo子宫, textEdit子宫);
        }

        private void cbo伤口_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetControlVisble(cbo伤口, textEdit伤口);
        }

        private void cbo分类_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetControlVisble(cbo分类, textEdit分类);
        }

        private void cbo转诊有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.cbo转诊有无.Text.Trim();
            if (!string.IsNullOrEmpty(index) && index == "有")
            {
                this.uc转诊原因.Txt1.Properties.ReadOnly = false;
                this.uc转诊机构.Txt1.Properties.ReadOnly = false;
                this.uc联系人.Txt1.Properties.ReadOnly = false;
                this.uc联系方式.Txt1.Properties.ReadOnly = false;
                this.cbo是否到位.Properties.ReadOnly = false;
            }
            else
            {
                this.uc转诊原因.Txt1.Properties.ReadOnly = true;
                this.uc转诊机构.Txt1.Properties.ReadOnly = true;
                this.uc联系人.Txt1.Properties.ReadOnly = true;
                this.uc联系方式.Txt1.Properties.ReadOnly = true;
                this.cbo是否到位.Properties.ReadOnly = true;
                this.uc转诊原因.Txt1.Text = "";
                this.uc转诊机构.Txt1.Text = "";
                this.uc联系人.Txt1.Text="";
                this.uc联系方式.Txt1.Text="";
                this.cbo是否到位.EditValue = "";

            }
        }

        private void checkEdit其他_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit其他.Checked)
            {
                this.textEdit指导其他.Enabled = true;
                if (_dt产后随访 != null && _dt产后随访.Rows.Count == 1)
                {
                    if (_dt产后随访.Rows[0][tb_孕妇_产后访视情况.指导其他] != null && !string.IsNullOrEmpty(_dt产后随访.Rows[0][tb_孕妇_产后访视情况.指导其他].ToString()))
                    {
                        textEdit指导其他.Text = _dt产后随访.Rows[0][tb_孕妇_产后访视情况.指导其他].ToString();
                    }
                    else
                    {
                        textEdit指导其他.Text = "低盐饮食";
                    }
                }
            }
            else
            {
                textEdit指导其他.Enabled = false;
                textEdit指导其他.Text = "";
            }
        }


    }
}
