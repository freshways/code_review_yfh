﻿namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    partial class UC更年期保健检查表
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn保存 = new DevExpress.XtraEditors.SimpleButton();
            this.btn重置 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit65 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit64 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit63 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit62 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit61 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit60 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit59 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit58 = new DevExpress.XtraEditors.TextEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.textEdit57 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit56 = new DevExpress.XtraEditors.TextEdit();
            this.ucTxtLbl15 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl14 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl13 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl12 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl11 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl10 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit55 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit54 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit53 = new DevExpress.XtraEditors.TextEdit();
            this.ucTxtLbl9 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit52 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit51 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit50 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit49 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit48 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit47 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit46 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit45 = new DevExpress.XtraEditors.TextEdit();
            this.ucTxtLbl8 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl7 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl6 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl5 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit44 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit43 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit42 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit41 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit40 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit39 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit38 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit37 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit36 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit35 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit34 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit33 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit32 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit31 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit30 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit29 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit28 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit27 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit26 = new DevExpress.XtraEditors.TextEdit();
            this.ucTxtLblTxtLbl1 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.ucTxtLbl4 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.ucTxtLbl3 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit25 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit24 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit23 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit22 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit21 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit20 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit19 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit18 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit17 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit16 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit15 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit14 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit13 = new DevExpress.XtraEditors.TextEdit();
            this.ucTxtLbl2 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.comboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ucLblTxtLbl7 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucTxtLbl1 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.ucLblTxtLbl4 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl5 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl6 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.ucLblTxtLbl1 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl2 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.ucLblTxtLbl3 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem64 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem67 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem71 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem70 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem69 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem66 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem65 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem68 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem72 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem73 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem75 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem76 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem77 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem74 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem78 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem79 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem80 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem81 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem86 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem85 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem84 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem83 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem82 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit65.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit64.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit63.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit62.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit61.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit60.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit59.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit58.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit57.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit56.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit55.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit54.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit53.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit52.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit51.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit50.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit49.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).BeginInit();
            this.flowLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).BeginInit();
            this.flowLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).BeginInit();
            this.flowLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            this.flowLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem86)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem83)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(750, 32);
            this.panelControl1.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn保存);
            this.flowLayoutPanel1.Controls.Add(this.btn重置);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(746, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn保存
            // 
            this.btn保存.Location = new System.Drawing.Point(3, 3);
            this.btn保存.Name = "btn保存";
            this.btn保存.Size = new System.Drawing.Size(75, 23);
            this.btn保存.TabIndex = 0;
            this.btn保存.Text = "保存";
            // 
            // btn重置
            // 
            this.btn重置.Location = new System.Drawing.Point(84, 3);
            this.btn重置.Name = "btn重置";
            this.btn重置.Size = new System.Drawing.Size(75, 23);
            this.btn重置.TabIndex = 1;
            this.btn重置.Text = "重置";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.textEdit65);
            this.layoutControl1.Controls.Add(this.textEdit64);
            this.layoutControl1.Controls.Add(this.textEdit63);
            this.layoutControl1.Controls.Add(this.textEdit62);
            this.layoutControl1.Controls.Add(this.textEdit61);
            this.layoutControl1.Controls.Add(this.textEdit60);
            this.layoutControl1.Controls.Add(this.textEdit59);
            this.layoutControl1.Controls.Add(this.textEdit58);
            this.layoutControl1.Controls.Add(this.dateEdit1);
            this.layoutControl1.Controls.Add(this.textEdit57);
            this.layoutControl1.Controls.Add(this.textEdit56);
            this.layoutControl1.Controls.Add(this.ucTxtLbl15);
            this.layoutControl1.Controls.Add(this.ucTxtLbl14);
            this.layoutControl1.Controls.Add(this.ucTxtLbl13);
            this.layoutControl1.Controls.Add(this.ucTxtLbl12);
            this.layoutControl1.Controls.Add(this.ucTxtLbl11);
            this.layoutControl1.Controls.Add(this.ucTxtLbl10);
            this.layoutControl1.Controls.Add(this.textEdit55);
            this.layoutControl1.Controls.Add(this.textEdit54);
            this.layoutControl1.Controls.Add(this.textEdit53);
            this.layoutControl1.Controls.Add(this.ucTxtLbl9);
            this.layoutControl1.Controls.Add(this.textEdit52);
            this.layoutControl1.Controls.Add(this.textEdit51);
            this.layoutControl1.Controls.Add(this.textEdit50);
            this.layoutControl1.Controls.Add(this.textEdit49);
            this.layoutControl1.Controls.Add(this.textEdit48);
            this.layoutControl1.Controls.Add(this.textEdit47);
            this.layoutControl1.Controls.Add(this.textEdit46);
            this.layoutControl1.Controls.Add(this.textEdit45);
            this.layoutControl1.Controls.Add(this.ucTxtLbl8);
            this.layoutControl1.Controls.Add(this.ucTxtLbl7);
            this.layoutControl1.Controls.Add(this.ucTxtLbl6);
            this.layoutControl1.Controls.Add(this.ucTxtLbl5);
            this.layoutControl1.Controls.Add(this.textEdit44);
            this.layoutControl1.Controls.Add(this.textEdit43);
            this.layoutControl1.Controls.Add(this.textEdit42);
            this.layoutControl1.Controls.Add(this.textEdit41);
            this.layoutControl1.Controls.Add(this.textEdit40);
            this.layoutControl1.Controls.Add(this.textEdit39);
            this.layoutControl1.Controls.Add(this.textEdit38);
            this.layoutControl1.Controls.Add(this.textEdit37);
            this.layoutControl1.Controls.Add(this.textEdit36);
            this.layoutControl1.Controls.Add(this.textEdit35);
            this.layoutControl1.Controls.Add(this.textEdit34);
            this.layoutControl1.Controls.Add(this.textEdit33);
            this.layoutControl1.Controls.Add(this.textEdit32);
            this.layoutControl1.Controls.Add(this.textEdit31);
            this.layoutControl1.Controls.Add(this.textEdit30);
            this.layoutControl1.Controls.Add(this.textEdit29);
            this.layoutControl1.Controls.Add(this.textEdit28);
            this.layoutControl1.Controls.Add(this.textEdit27);
            this.layoutControl1.Controls.Add(this.textEdit26);
            this.layoutControl1.Controls.Add(this.ucTxtLblTxtLbl1);
            this.layoutControl1.Controls.Add(this.ucTxtLbl4);
            this.layoutControl1.Controls.Add(this.ucTxtLbl3);
            this.layoutControl1.Controls.Add(this.textEdit25);
            this.layoutControl1.Controls.Add(this.textEdit24);
            this.layoutControl1.Controls.Add(this.textEdit23);
            this.layoutControl1.Controls.Add(this.textEdit22);
            this.layoutControl1.Controls.Add(this.textEdit21);
            this.layoutControl1.Controls.Add(this.textEdit20);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel12);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel11);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel10);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel9);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel8);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel7);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.ucTxtLbl2);
            this.layoutControl1.Controls.Add(this.textEdit12);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.ucTxtLbl1);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.textEdit11);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.textEdit3);
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit10);
            this.layoutControl1.Controls.Add(this.textEdit9);
            this.layoutControl1.Controls.Add(this.textEdit8);
            this.layoutControl1.Controls.Add(this.textEdit7);
            this.layoutControl1.Controls.Add(this.textEdit6);
            this.layoutControl1.Controls.Add(this.textEdit5);
            this.layoutControl1.Controls.Add(this.textEdit4);
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 32);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(750, 468);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit65
            // 
            this.textEdit65.Location = new System.Drawing.Point(335, 421);
            this.textEdit65.Name = "textEdit65";
            this.textEdit65.Size = new System.Drawing.Size(167, 20);
            this.textEdit65.StyleController = this.layoutControl1;
            this.textEdit65.TabIndex = 87;
            // 
            // textEdit64
            // 
            this.textEdit64.Location = new System.Drawing.Point(589, 421);
            this.textEdit64.Name = "textEdit64";
            this.textEdit64.Size = new System.Drawing.Size(141, 20);
            this.textEdit64.StyleController = this.layoutControl1;
            this.textEdit64.TabIndex = 86;
            // 
            // textEdit63
            // 
            this.textEdit63.Location = new System.Drawing.Point(86, 445);
            this.textEdit63.Name = "textEdit63";
            this.textEdit63.Size = new System.Drawing.Size(162, 20);
            this.textEdit63.StyleController = this.layoutControl1;
            this.textEdit63.TabIndex = 85;
            // 
            // textEdit62
            // 
            this.textEdit62.Location = new System.Drawing.Point(335, 445);
            this.textEdit62.Name = "textEdit62";
            this.textEdit62.Size = new System.Drawing.Size(167, 20);
            this.textEdit62.StyleController = this.layoutControl1;
            this.textEdit62.TabIndex = 84;
            // 
            // textEdit61
            // 
            this.textEdit61.Location = new System.Drawing.Point(589, 445);
            this.textEdit61.Name = "textEdit61";
            this.textEdit61.Size = new System.Drawing.Size(141, 20);
            this.textEdit61.StyleController = this.layoutControl1;
            this.textEdit61.TabIndex = 83;
            // 
            // textEdit60
            // 
            this.textEdit60.Location = new System.Drawing.Point(86, 421);
            this.textEdit60.Name = "textEdit60";
            this.textEdit60.Size = new System.Drawing.Size(162, 20);
            this.textEdit60.StyleController = this.layoutControl1;
            this.textEdit60.TabIndex = 82;
            // 
            // textEdit59
            // 
            this.textEdit59.Location = new System.Drawing.Point(588, 394);
            this.textEdit59.Name = "textEdit59";
            this.textEdit59.Size = new System.Drawing.Size(139, 20);
            this.textEdit59.StyleController = this.layoutControl1;
            this.textEdit59.TabIndex = 81;
            // 
            // textEdit58
            // 
            this.textEdit58.Location = new System.Drawing.Point(359, 394);
            this.textEdit58.Name = "textEdit58";
            this.textEdit58.Size = new System.Drawing.Size(140, 20);
            this.textEdit58.StyleController = this.layoutControl1;
            this.textEdit58.TabIndex = 80;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.Location = new System.Drawing.Point(91, 394);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Size = new System.Drawing.Size(179, 20);
            this.dateEdit1.StyleController = this.layoutControl1;
            this.dateEdit1.TabIndex = 79;
            // 
            // textEdit57
            // 
            this.textEdit57.Location = new System.Drawing.Point(443, 370);
            this.textEdit57.Name = "textEdit57";
            this.textEdit57.Size = new System.Drawing.Size(284, 20);
            this.textEdit57.StyleController = this.layoutControl1;
            this.textEdit57.TabIndex = 78;
            // 
            // textEdit56
            // 
            this.textEdit56.Location = new System.Drawing.Point(91, 370);
            this.textEdit56.Name = "textEdit56";
            this.textEdit56.Size = new System.Drawing.Size(263, 20);
            this.textEdit56.StyleController = this.layoutControl1;
            this.textEdit56.TabIndex = 77;
            // 
            // ucTxtLbl15
            // 
            this.ucTxtLbl15.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucTxtLbl15.Lbl1Text = "ng/ml";
            this.ucTxtLbl15.Location = new System.Drawing.Point(352, 322);
            this.ucTxtLbl15.Name = "ucTxtLbl15";
            this.ucTxtLbl15.Size = new System.Drawing.Size(149, 20);
            this.ucTxtLbl15.TabIndex = 76;
            this.ucTxtLbl15.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // ucTxtLbl14
            // 
            this.ucTxtLbl14.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucTxtLbl14.Lbl1Text = "mIU/ml ";
            this.ucTxtLbl14.Location = new System.Drawing.Point(590, 322);
            this.ucTxtLbl14.Name = "ucTxtLbl14";
            this.ucTxtLbl14.Size = new System.Drawing.Size(137, 20);
            this.ucTxtLbl14.TabIndex = 75;
            this.ucTxtLbl14.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // ucTxtLbl13
            // 
            this.ucTxtLbl13.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl13.Lbl1Text = "ng/dl ";
            this.ucTxtLbl13.Location = new System.Drawing.Point(91, 346);
            this.ucTxtLbl13.Name = "ucTxtLbl13";
            this.ucTxtLbl13.Size = new System.Drawing.Size(636, 20);
            this.ucTxtLbl13.TabIndex = 74;
            this.ucTxtLbl13.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl12
            // 
            this.ucTxtLbl12.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl12.Lbl1Text = "pg/ml ";
            this.ucTxtLbl12.Location = new System.Drawing.Point(91, 322);
            this.ucTxtLbl12.Name = "ucTxtLbl12";
            this.ucTxtLbl12.Size = new System.Drawing.Size(172, 20);
            this.ucTxtLbl12.TabIndex = 73;
            this.ucTxtLbl12.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // ucTxtLbl11
            // 
            this.ucTxtLbl11.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucTxtLbl11.Lbl1Text = "mmol/L";
            this.ucTxtLbl11.Location = new System.Drawing.Point(101, 272);
            this.ucTxtLbl11.Name = "ucTxtLbl11";
            this.ucTxtLbl11.Size = new System.Drawing.Size(159, 20);
            this.ucTxtLbl11.TabIndex = 72;
            this.ucTxtLbl11.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl10
            // 
            this.ucTxtLbl10.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl10.Lbl1Text = "mmol/L";
            this.ucTxtLbl10.Location = new System.Drawing.Point(359, 272);
            this.ucTxtLbl10.Name = "ucTxtLbl10";
            this.ucTxtLbl10.Size = new System.Drawing.Size(368, 20);
            this.ucTxtLbl10.TabIndex = 71;
            this.ucTxtLbl10.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // textEdit55
            // 
            this.textEdit55.Location = new System.Drawing.Point(360, 248);
            this.textEdit55.Name = "textEdit55";
            this.textEdit55.Size = new System.Drawing.Size(139, 20);
            this.textEdit55.StyleController = this.layoutControl1;
            this.textEdit55.TabIndex = 70;
            // 
            // textEdit54
            // 
            this.textEdit54.Location = new System.Drawing.Point(598, 248);
            this.textEdit54.Name = "textEdit54";
            this.textEdit54.Size = new System.Drawing.Size(129, 20);
            this.textEdit54.StyleController = this.layoutControl1;
            this.textEdit54.TabIndex = 69;
            // 
            // textEdit53
            // 
            this.textEdit53.Location = new System.Drawing.Point(101, 248);
            this.textEdit53.Name = "textEdit53";
            this.textEdit53.Size = new System.Drawing.Size(160, 20);
            this.textEdit53.StyleController = this.layoutControl1;
            this.textEdit53.TabIndex = 68;
            // 
            // ucTxtLbl9
            // 
            this.ucTxtLbl9.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucTxtLbl9.Lbl1Text = "umol/L";
            this.ucTxtLbl9.Location = new System.Drawing.Point(360, 224);
            this.ucTxtLbl9.Name = "ucTxtLbl9";
            this.ucTxtLbl9.Size = new System.Drawing.Size(139, 20);
            this.ucTxtLbl9.TabIndex = 67;
            this.ucTxtLbl9.Txt1Size = new System.Drawing.Size(80, 20);
            // 
            // textEdit52
            // 
            this.textEdit52.Location = new System.Drawing.Point(598, 224);
            this.textEdit52.Name = "textEdit52";
            this.textEdit52.Size = new System.Drawing.Size(129, 20);
            this.textEdit52.StyleController = this.layoutControl1;
            this.textEdit52.TabIndex = 66;
            // 
            // textEdit51
            // 
            this.textEdit51.Location = new System.Drawing.Point(101, 224);
            this.textEdit51.Name = "textEdit51";
            this.textEdit51.Size = new System.Drawing.Size(160, 20);
            this.textEdit51.StyleController = this.layoutControl1;
            this.textEdit51.TabIndex = 65;
            // 
            // textEdit50
            // 
            this.textEdit50.Location = new System.Drawing.Point(361, 176);
            this.textEdit50.Name = "textEdit50";
            this.textEdit50.Size = new System.Drawing.Size(138, 20);
            this.textEdit50.StyleController = this.layoutControl1;
            this.textEdit50.TabIndex = 64;
            // 
            // textEdit49
            // 
            this.textEdit49.Location = new System.Drawing.Point(598, 176);
            this.textEdit49.Name = "textEdit49";
            this.textEdit49.Size = new System.Drawing.Size(129, 20);
            this.textEdit49.StyleController = this.layoutControl1;
            this.textEdit49.TabIndex = 63;
            // 
            // textEdit48
            // 
            this.textEdit48.Location = new System.Drawing.Point(101, 200);
            this.textEdit48.Name = "textEdit48";
            this.textEdit48.Size = new System.Drawing.Size(161, 20);
            this.textEdit48.StyleController = this.layoutControl1;
            this.textEdit48.TabIndex = 62;
            // 
            // textEdit47
            // 
            this.textEdit47.Location = new System.Drawing.Point(361, 200);
            this.textEdit47.Name = "textEdit47";
            this.textEdit47.Size = new System.Drawing.Size(138, 20);
            this.textEdit47.StyleController = this.layoutControl1;
            this.textEdit47.TabIndex = 61;
            // 
            // textEdit46
            // 
            this.textEdit46.Location = new System.Drawing.Point(101, 176);
            this.textEdit46.Name = "textEdit46";
            this.textEdit46.Size = new System.Drawing.Size(161, 20);
            this.textEdit46.StyleController = this.layoutControl1;
            this.textEdit46.TabIndex = 60;
            // 
            // textEdit45
            // 
            this.textEdit45.Location = new System.Drawing.Point(598, 200);
            this.textEdit45.Name = "textEdit45";
            this.textEdit45.Size = new System.Drawing.Size(129, 20);
            this.textEdit45.StyleController = this.layoutControl1;
            this.textEdit45.TabIndex = 59;
            // 
            // ucTxtLbl8
            // 
            this.ucTxtLbl8.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl8.Lbl1Text = "cm";
            this.ucTxtLbl8.Location = new System.Drawing.Point(597, 128);
            this.ucTxtLbl8.Name = "ucTxtLbl8";
            this.ucTxtLbl8.Size = new System.Drawing.Size(130, 44);
            this.ucTxtLbl8.TabIndex = 58;
            this.ucTxtLbl8.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl7
            // 
            this.ucTxtLbl7.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl7.Lbl1Text = "个";
            this.ucTxtLbl7.Location = new System.Drawing.Point(361, 128);
            this.ucTxtLbl7.Name = "ucTxtLbl7";
            this.ucTxtLbl7.Size = new System.Drawing.Size(137, 44);
            this.ucTxtLbl7.TabIndex = 57;
            this.ucTxtLbl7.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl6
            // 
            this.ucTxtLbl6.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl6.Lbl1Text = "mm";
            this.ucTxtLbl6.Location = new System.Drawing.Point(101, 128);
            this.ucTxtLbl6.Name = "ucTxtLbl6";
            this.ucTxtLbl6.Size = new System.Drawing.Size(161, 44);
            this.ucTxtLbl6.TabIndex = 56;
            this.ucTxtLbl6.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl5
            // 
            this.ucTxtLbl5.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl5.Lbl1Text = "mm";
            this.ucTxtLbl5.Location = new System.Drawing.Point(597, 104);
            this.ucTxtLbl5.Name = "ucTxtLbl5";
            this.ucTxtLbl5.Size = new System.Drawing.Size(130, 20);
            this.ucTxtLbl5.TabIndex = 55;
            this.ucTxtLbl5.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // textEdit44
            // 
            this.textEdit44.Location = new System.Drawing.Point(363, 80);
            this.textEdit44.Name = "textEdit44";
            this.textEdit44.Size = new System.Drawing.Size(135, 20);
            this.textEdit44.StyleController = this.layoutControl1;
            this.textEdit44.TabIndex = 54;
            // 
            // textEdit43
            // 
            this.textEdit43.Location = new System.Drawing.Point(101, 104);
            this.textEdit43.Name = "textEdit43";
            this.textEdit43.Size = new System.Drawing.Size(163, 20);
            this.textEdit43.StyleController = this.layoutControl1;
            this.textEdit43.TabIndex = 53;
            // 
            // textEdit42
            // 
            this.textEdit42.Location = new System.Drawing.Point(363, 104);
            this.textEdit42.Name = "textEdit42";
            this.textEdit42.Size = new System.Drawing.Size(135, 20);
            this.textEdit42.StyleController = this.layoutControl1;
            this.textEdit42.TabIndex = 52;
            // 
            // textEdit41
            // 
            this.textEdit41.Location = new System.Drawing.Point(597, 80);
            this.textEdit41.Name = "textEdit41";
            this.textEdit41.Size = new System.Drawing.Size(130, 20);
            this.textEdit41.StyleController = this.layoutControl1;
            this.textEdit41.TabIndex = 51;
            // 
            // textEdit40
            // 
            this.textEdit40.Location = new System.Drawing.Point(363, 32);
            this.textEdit40.Name = "textEdit40";
            this.textEdit40.Size = new System.Drawing.Size(135, 20);
            this.textEdit40.StyleController = this.layoutControl1;
            this.textEdit40.TabIndex = 50;
            // 
            // textEdit39
            // 
            this.textEdit39.Location = new System.Drawing.Point(597, 32);
            this.textEdit39.Name = "textEdit39";
            this.textEdit39.Size = new System.Drawing.Size(130, 20);
            this.textEdit39.StyleController = this.layoutControl1;
            this.textEdit39.TabIndex = 49;
            // 
            // textEdit38
            // 
            this.textEdit38.Location = new System.Drawing.Point(101, 56);
            this.textEdit38.Name = "textEdit38";
            this.textEdit38.Size = new System.Drawing.Size(163, 20);
            this.textEdit38.StyleController = this.layoutControl1;
            this.textEdit38.TabIndex = 48;
            // 
            // textEdit37
            // 
            this.textEdit37.Location = new System.Drawing.Point(363, 56);
            this.textEdit37.Name = "textEdit37";
            this.textEdit37.Size = new System.Drawing.Size(135, 20);
            this.textEdit37.StyleController = this.layoutControl1;
            this.textEdit37.TabIndex = 47;
            // 
            // textEdit36
            // 
            this.textEdit36.Location = new System.Drawing.Point(597, 56);
            this.textEdit36.Name = "textEdit36";
            this.textEdit36.Size = new System.Drawing.Size(130, 20);
            this.textEdit36.StyleController = this.layoutControl1;
            this.textEdit36.TabIndex = 46;
            // 
            // textEdit35
            // 
            this.textEdit35.Location = new System.Drawing.Point(101, 80);
            this.textEdit35.Name = "textEdit35";
            this.textEdit35.Size = new System.Drawing.Size(163, 20);
            this.textEdit35.StyleController = this.layoutControl1;
            this.textEdit35.TabIndex = 45;
            // 
            // textEdit34
            // 
            this.textEdit34.Location = new System.Drawing.Point(101, 32);
            this.textEdit34.Name = "textEdit34";
            this.textEdit34.Size = new System.Drawing.Size(163, 20);
            this.textEdit34.StyleController = this.layoutControl1;
            this.textEdit34.TabIndex = 44;
            // 
            // textEdit33
            // 
            this.textEdit33.Location = new System.Drawing.Point(313, -18);
            this.textEdit33.Name = "textEdit33";
            this.textEdit33.Size = new System.Drawing.Size(130, 20);
            this.textEdit33.StyleController = this.layoutControl1;
            this.textEdit33.TabIndex = 43;
            // 
            // textEdit32
            // 
            this.textEdit32.Location = new System.Drawing.Point(91, -18);
            this.textEdit32.Name = "textEdit32";
            this.textEdit32.Size = new System.Drawing.Size(133, 20);
            this.textEdit32.StyleController = this.layoutControl1;
            this.textEdit32.TabIndex = 42;
            // 
            // textEdit31
            // 
            this.textEdit31.Location = new System.Drawing.Point(532, -66);
            this.textEdit31.Name = "textEdit31";
            this.textEdit31.Size = new System.Drawing.Size(195, 20);
            this.textEdit31.StyleController = this.layoutControl1;
            this.textEdit31.TabIndex = 41;
            // 
            // textEdit30
            // 
            this.textEdit30.Location = new System.Drawing.Point(313, -42);
            this.textEdit30.Name = "textEdit30";
            this.textEdit30.Size = new System.Drawing.Size(130, 20);
            this.textEdit30.StyleController = this.layoutControl1;
            this.textEdit30.TabIndex = 40;
            // 
            // textEdit29
            // 
            this.textEdit29.Location = new System.Drawing.Point(91, -42);
            this.textEdit29.Name = "textEdit29";
            this.textEdit29.Size = new System.Drawing.Size(133, 20);
            this.textEdit29.StyleController = this.layoutControl1;
            this.textEdit29.TabIndex = 39;
            // 
            // textEdit28
            // 
            this.textEdit28.Location = new System.Drawing.Point(532, -42);
            this.textEdit28.Name = "textEdit28";
            this.textEdit28.Size = new System.Drawing.Size(195, 20);
            this.textEdit28.StyleController = this.layoutControl1;
            this.textEdit28.TabIndex = 38;
            // 
            // textEdit27
            // 
            this.textEdit27.Location = new System.Drawing.Point(91, -66);
            this.textEdit27.Name = "textEdit27";
            this.textEdit27.Size = new System.Drawing.Size(133, 20);
            this.textEdit27.StyleController = this.layoutControl1;
            this.textEdit27.TabIndex = 37;
            // 
            // textEdit26
            // 
            this.textEdit26.Location = new System.Drawing.Point(313, -66);
            this.textEdit26.Name = "textEdit26";
            this.textEdit26.Size = new System.Drawing.Size(130, 20);
            this.textEdit26.StyleController = this.layoutControl1;
            this.textEdit26.TabIndex = 36;
            // 
            // ucTxtLblTxtLbl1
            // 
            this.ucTxtLblTxtLbl1.Lbl1Size = new System.Drawing.Size(10, 14);
            this.ucTxtLblTxtLbl1.Lbl1Text = "/";
            this.ucTxtLblTxtLbl1.Lbl2Size = new System.Drawing.Size(40, 14);
            this.ucTxtLblTxtLbl1.Lbl2Text = "mmHg";
            this.ucTxtLblTxtLbl1.Location = new System.Drawing.Point(532, -90);
            this.ucTxtLblTxtLbl1.Name = "ucTxtLblTxtLbl1";
            this.ucTxtLblTxtLbl1.Size = new System.Drawing.Size(195, 20);
            this.ucTxtLblTxtLbl1.TabIndex = 35;
            this.ucTxtLblTxtLbl1.Txt1EditValue = null;
            this.ucTxtLblTxtLbl1.Txt1Size = new System.Drawing.Size(50, 20);
            this.ucTxtLblTxtLbl1.Txt2EditValue = null;
            this.ucTxtLblTxtLbl1.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // ucTxtLbl4
            // 
            this.ucTxtLbl4.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl4.Lbl1Text = "kg";
            this.ucTxtLbl4.Location = new System.Drawing.Point(313, -90);
            this.ucTxtLbl4.Name = "ucTxtLbl4";
            this.ucTxtLbl4.Size = new System.Drawing.Size(130, 20);
            this.ucTxtLbl4.TabIndex = 34;
            this.ucTxtLbl4.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl3
            // 
            this.ucTxtLbl3.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl3.Lbl1Text = "cm";
            this.ucTxtLbl3.Location = new System.Drawing.Point(91, -90);
            this.ucTxtLbl3.Name = "ucTxtLbl3";
            this.ucTxtLbl3.Size = new System.Drawing.Size(133, 20);
            this.ucTxtLbl3.TabIndex = 33;
            this.ucTxtLbl3.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // textEdit25
            // 
            this.textEdit25.Location = new System.Drawing.Point(579, -140);
            this.textEdit25.Name = "textEdit25";
            this.textEdit25.Size = new System.Drawing.Size(148, 20);
            this.textEdit25.StyleController = this.layoutControl1;
            this.textEdit25.TabIndex = 32;
            // 
            // textEdit24
            // 
            this.textEdit24.Location = new System.Drawing.Point(290, -140);
            this.textEdit24.Name = "textEdit24";
            this.textEdit24.Size = new System.Drawing.Size(160, 20);
            this.textEdit24.StyleController = this.layoutControl1;
            this.textEdit24.TabIndex = 31;
            // 
            // textEdit23
            // 
            this.textEdit23.Location = new System.Drawing.Point(91, -140);
            this.textEdit23.Name = "textEdit23";
            this.textEdit23.Size = new System.Drawing.Size(110, 20);
            this.textEdit23.StyleController = this.layoutControl1;
            this.textEdit23.TabIndex = 30;
            // 
            // textEdit22
            // 
            this.textEdit22.Location = new System.Drawing.Point(579, -164);
            this.textEdit22.Name = "textEdit22";
            this.textEdit22.Size = new System.Drawing.Size(148, 20);
            this.textEdit22.StyleController = this.layoutControl1;
            this.textEdit22.TabIndex = 29;
            // 
            // textEdit21
            // 
            this.textEdit21.Location = new System.Drawing.Point(290, -164);
            this.textEdit21.Name = "textEdit21";
            this.textEdit21.Size = new System.Drawing.Size(160, 20);
            this.textEdit21.StyleController = this.layoutControl1;
            this.textEdit21.TabIndex = 28;
            // 
            // textEdit20
            // 
            this.textEdit20.Location = new System.Drawing.Point(91, -164);
            this.textEdit20.Name = "textEdit20";
            this.textEdit20.Size = new System.Drawing.Size(110, 20);
            this.textEdit20.StyleController = this.layoutControl1;
            this.textEdit20.TabIndex = 27;
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.Controls.Add(this.checkEdit13);
            this.flowLayoutPanel12.Controls.Add(this.textEdit19);
            this.flowLayoutPanel12.Location = new System.Drawing.Point(459, -214);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(268, 20);
            this.flowLayoutPanel12.TabIndex = 26;
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(3, 3);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "无    有";
            this.checkEdit13.Size = new System.Drawing.Size(63, 19);
            this.checkEdit13.TabIndex = 0;
            // 
            // textEdit19
            // 
            this.textEdit19.Location = new System.Drawing.Point(69, 0);
            this.textEdit19.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit19.Name = "textEdit19";
            this.textEdit19.Size = new System.Drawing.Size(160, 20);
            this.textEdit19.TabIndex = 1;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.checkEdit12);
            this.flowLayoutPanel11.Controls.Add(this.textEdit18);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(459, -238);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(268, 20);
            this.flowLayoutPanel11.TabIndex = 25;
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(3, 3);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "无    有";
            this.checkEdit12.Size = new System.Drawing.Size(63, 19);
            this.checkEdit12.TabIndex = 0;
            // 
            // textEdit18
            // 
            this.textEdit18.Location = new System.Drawing.Point(69, 0);
            this.textEdit18.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit18.Name = "textEdit18";
            this.textEdit18.Size = new System.Drawing.Size(160, 20);
            this.textEdit18.TabIndex = 1;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.checkEdit11);
            this.flowLayoutPanel10.Controls.Add(this.textEdit17);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(459, -262);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(268, 20);
            this.flowLayoutPanel10.TabIndex = 24;
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(3, 3);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "无    有";
            this.checkEdit11.Size = new System.Drawing.Size(63, 19);
            this.checkEdit11.TabIndex = 0;
            // 
            // textEdit17
            // 
            this.textEdit17.Location = new System.Drawing.Point(69, 0);
            this.textEdit17.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit17.Name = "textEdit17";
            this.textEdit17.Size = new System.Drawing.Size(160, 20);
            this.textEdit17.TabIndex = 1;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Controls.Add(this.checkEdit5);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit6);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit7);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit8);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit9);
            this.flowLayoutPanel9.Controls.Add(this.checkEdit10);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(459, -302);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(268, 36);
            this.flowLayoutPanel9.TabIndex = 26;
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(0, 0);
            this.checkEdit5.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "无";
            this.checkEdit5.Size = new System.Drawing.Size(37, 19);
            this.checkEdit5.TabIndex = 0;
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(37, 0);
            this.checkEdit6.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "外祖母";
            this.checkEdit6.Size = new System.Drawing.Size(62, 19);
            this.checkEdit6.TabIndex = 1;
            // 
            // checkEdit7
            // 
            this.checkEdit7.Location = new System.Drawing.Point(99, 0);
            this.checkEdit7.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "祖母";
            this.checkEdit7.Size = new System.Drawing.Size(51, 19);
            this.checkEdit7.TabIndex = 2;
            // 
            // checkEdit8
            // 
            this.checkEdit8.Location = new System.Drawing.Point(150, 0);
            this.checkEdit8.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "母亲";
            this.checkEdit8.Size = new System.Drawing.Size(51, 19);
            this.checkEdit8.TabIndex = 3;
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(201, 0);
            this.checkEdit9.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "姐妹";
            this.checkEdit9.Size = new System.Drawing.Size(51, 19);
            this.checkEdit9.TabIndex = 4;
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(0, 19);
            this.checkEdit10.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "女儿";
            this.checkEdit10.Size = new System.Drawing.Size(51, 19);
            this.checkEdit10.TabIndex = 5;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.checkEdit4);
            this.flowLayoutPanel8.Controls.Add(this.textEdit16);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(91, -214);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(269, 20);
            this.flowLayoutPanel8.TabIndex = 25;
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(3, 3);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "无    有";
            this.checkEdit4.Size = new System.Drawing.Size(63, 19);
            this.checkEdit4.TabIndex = 0;
            // 
            // textEdit16
            // 
            this.textEdit16.Location = new System.Drawing.Point(69, 0);
            this.textEdit16.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit16.Name = "textEdit16";
            this.textEdit16.Size = new System.Drawing.Size(170, 20);
            this.textEdit16.TabIndex = 1;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.checkEdit3);
            this.flowLayoutPanel7.Controls.Add(this.textEdit15);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(91, -238);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(269, 20);
            this.flowLayoutPanel7.TabIndex = 24;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(3, 3);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "无    有";
            this.checkEdit3.Size = new System.Drawing.Size(63, 19);
            this.checkEdit3.TabIndex = 0;
            // 
            // textEdit15
            // 
            this.textEdit15.Location = new System.Drawing.Point(69, 0);
            this.textEdit15.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit15.Name = "textEdit15";
            this.textEdit15.Size = new System.Drawing.Size(170, 20);
            this.textEdit15.TabIndex = 1;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.checkEdit2);
            this.flowLayoutPanel6.Controls.Add(this.textEdit14);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(91, -262);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(269, 20);
            this.flowLayoutPanel6.TabIndex = 23;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(3, 3);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "无    有";
            this.checkEdit2.Size = new System.Drawing.Size(63, 19);
            this.checkEdit2.TabIndex = 0;
            // 
            // textEdit14
            // 
            this.textEdit14.Location = new System.Drawing.Point(69, 0);
            this.textEdit14.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit14.Name = "textEdit14";
            this.textEdit14.Size = new System.Drawing.Size(170, 20);
            this.textEdit14.TabIndex = 1;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.checkEdit1);
            this.flowLayoutPanel5.Controls.Add(this.textEdit13);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(91, -302);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(269, 36);
            this.flowLayoutPanel5.TabIndex = 22;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(3, 3);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "无    有";
            this.checkEdit1.Size = new System.Drawing.Size(63, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // textEdit13
            // 
            this.textEdit13.Location = new System.Drawing.Point(69, 0);
            this.textEdit13.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit13.Name = "textEdit13";
            this.textEdit13.Size = new System.Drawing.Size(170, 20);
            this.textEdit13.TabIndex = 1;
            // 
            // ucTxtLbl2
            // 
            this.ucTxtLbl2.Lbl1Size = new System.Drawing.Size(69, 18);
            this.ucTxtLbl2.Lbl1Text = "年";
            this.ucTxtLbl2.Location = new System.Drawing.Point(451, -352);
            this.ucTxtLbl2.Name = "ucTxtLbl2";
            this.ucTxtLbl2.Size = new System.Drawing.Size(276, 20);
            this.ucTxtLbl2.TabIndex = 21;
            this.ucTxtLbl2.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // textEdit12
            // 
            this.textEdit12.Location = new System.Drawing.Point(91, -352);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Size = new System.Drawing.Size(271, 20);
            this.textEdit12.StyleController = this.layoutControl1;
            this.textEdit12.TabIndex = 20;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.comboBoxEdit1);
            this.flowLayoutPanel4.Controls.Add(this.ucLblTxtLbl7);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(91, -376);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(636, 20);
            this.flowLayoutPanel4.TabIndex = 19;
            // 
            // comboBoxEdit1
            // 
            this.comboBoxEdit1.Location = new System.Drawing.Point(0, 0);
            this.comboBoxEdit1.Margin = new System.Windows.Forms.Padding(0);
            this.comboBoxEdit1.Name = "comboBoxEdit1";
            this.comboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit1.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit1.TabIndex = 0;
            // 
            // ucLblTxtLbl7
            // 
            this.ucLblTxtLbl7.Lbl1Size = new System.Drawing.Size(100, 18);
            this.ucLblTxtLbl7.Lbl1Text = "避孕药使用时间:";
            this.ucLblTxtLbl7.Lbl2Size = new System.Drawing.Size(56, 18);
            this.ucLblTxtLbl7.Lbl2Text = "年";
            this.ucLblTxtLbl7.Location = new System.Drawing.Point(100, 0);
            this.ucLblTxtLbl7.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl7.Name = "ucLblTxtLbl7";
            this.ucLblTxtLbl7.Size = new System.Drawing.Size(297, 22);
            this.ucLblTxtLbl7.TabIndex = 1;
            this.ucLblTxtLbl7.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // ucTxtLbl1
            // 
            this.ucTxtLbl1.Lbl1Size = new System.Drawing.Size(20, 18);
            this.ucTxtLbl1.Lbl1Text = "岁";
            this.ucTxtLbl1.Location = new System.Drawing.Point(538, -400);
            this.ucTxtLbl1.Name = "ucTxtLbl1";
            this.ucTxtLbl1.Size = new System.Drawing.Size(189, 20);
            this.ucTxtLbl1.TabIndex = 18;
            this.ucTxtLbl1.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.ucLblTxtLbl4);
            this.flowLayoutPanel3.Controls.Add(this.ucLblTxtLbl5);
            this.flowLayoutPanel3.Controls.Add(this.ucLblTxtLbl6);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(91, -400);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(358, 20);
            this.flowLayoutPanel3.TabIndex = 17;
            // 
            // ucLblTxtLbl4
            // 
            this.ucLblTxtLbl4.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucLblTxtLbl4.Lbl1Text = "孕次:";
            this.ucLblTxtLbl4.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl4.Lbl2Text = "次";
            this.ucLblTxtLbl4.Location = new System.Drawing.Point(0, 0);
            this.ucLblTxtLbl4.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl4.Name = "ucLblTxtLbl4";
            this.ucLblTxtLbl4.Size = new System.Drawing.Size(144, 22);
            this.ucLblTxtLbl4.TabIndex = 0;
            this.ucLblTxtLbl4.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // ucLblTxtLbl5
            // 
            this.ucLblTxtLbl5.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucLblTxtLbl5.Lbl1Text = "产次:";
            this.ucLblTxtLbl5.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl5.Lbl2Text = "次";
            this.ucLblTxtLbl5.Location = new System.Drawing.Point(144, 0);
            this.ucLblTxtLbl5.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl5.Name = "ucLblTxtLbl5";
            this.ucLblTxtLbl5.Size = new System.Drawing.Size(130, 22);
            this.ucLblTxtLbl5.TabIndex = 1;
            this.ucLblTxtLbl5.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // ucLblTxtLbl6
            // 
            this.ucLblTxtLbl6.Lbl1Size = new System.Drawing.Size(60, 18);
            this.ucLblTxtLbl6.Lbl1Text = "流产次数:";
            this.ucLblTxtLbl6.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl6.Lbl2Text = "次";
            this.ucLblTxtLbl6.Location = new System.Drawing.Point(0, 22);
            this.ucLblTxtLbl6.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl6.Name = "ucLblTxtLbl6";
            this.ucLblTxtLbl6.Size = new System.Drawing.Size(144, 22);
            this.ucLblTxtLbl6.TabIndex = 2;
            this.ucLblTxtLbl6.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // textEdit11
            // 
            this.textEdit11.Location = new System.Drawing.Point(582, -450);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Size = new System.Drawing.Size(145, 20);
            this.textEdit11.StyleController = this.layoutControl1;
            this.textEdit11.TabIndex = 17;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.ucLblTxtLbl1);
            this.flowLayoutPanel2.Controls.Add(this.ucLblTxtLbl2);
            this.flowLayoutPanel2.Controls.Add(this.ucLblTxtLbl3);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(91, -450);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(402, 20);
            this.flowLayoutPanel2.TabIndex = 16;
            // 
            // ucLblTxtLbl1
            // 
            this.ucLblTxtLbl1.Lbl1Size = new System.Drawing.Size(70, 18);
            this.ucLblTxtLbl1.Lbl1Text = "初潮年龄:";
            this.ucLblTxtLbl1.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl1.Lbl2Text = "岁";
            this.ucLblTxtLbl1.Location = new System.Drawing.Point(0, 0);
            this.ucLblTxtLbl1.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl1.Name = "ucLblTxtLbl1";
            this.ucLblTxtLbl1.Size = new System.Drawing.Size(165, 22);
            this.ucLblTxtLbl1.TabIndex = 0;
            this.ucLblTxtLbl1.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // ucLblTxtLbl2
            // 
            this.ucLblTxtLbl2.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucLblTxtLbl2.Lbl1Text = "经期:";
            this.ucLblTxtLbl2.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl2.Lbl2Text = "天";
            this.ucLblTxtLbl2.Location = new System.Drawing.Point(165, 0);
            this.ucLblTxtLbl2.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl2.Name = "ucLblTxtLbl2";
            this.ucLblTxtLbl2.Size = new System.Drawing.Size(130, 22);
            this.ucLblTxtLbl2.TabIndex = 1;
            this.ucLblTxtLbl2.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // ucLblTxtLbl3
            // 
            this.ucLblTxtLbl3.Lbl1Size = new System.Drawing.Size(40, 18);
            this.ucLblTxtLbl3.Lbl1Text = "周期:";
            this.ucLblTxtLbl3.Lbl2Size = new System.Drawing.Size(20, 18);
            this.ucLblTxtLbl3.Lbl2Text = "天";
            this.ucLblTxtLbl3.Location = new System.Drawing.Point(0, 22);
            this.ucLblTxtLbl3.Margin = new System.Windows.Forms.Padding(0);
            this.ucLblTxtLbl3.Name = "ucLblTxtLbl3";
            this.ucLblTxtLbl3.Size = new System.Drawing.Size(126, 22);
            this.ucLblTxtLbl3.TabIndex = 2;
            this.ucLblTxtLbl3.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // textEdit3
            // 
            this.textEdit3.Location = new System.Drawing.Point(402, -474);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(325, 20);
            this.textEdit3.StyleController = this.layoutControl1;
            this.textEdit3.TabIndex = 15;
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(91, -474);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(222, 20);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 14;
            // 
            // textEdit10
            // 
            this.textEdit10.Location = new System.Drawing.Point(339, -545);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Size = new System.Drawing.Size(139, 20);
            this.textEdit10.StyleController = this.layoutControl1;
            this.textEdit10.TabIndex = 13;
            // 
            // textEdit9
            // 
            this.textEdit9.Location = new System.Drawing.Point(88, -545);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Size = new System.Drawing.Size(162, 20);
            this.textEdit9.StyleController = this.layoutControl1;
            this.textEdit9.TabIndex = 12;
            // 
            // textEdit8
            // 
            this.textEdit8.Location = new System.Drawing.Point(88, -521);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Size = new System.Drawing.Size(162, 20);
            this.textEdit8.StyleController = this.layoutControl1;
            this.textEdit8.TabIndex = 11;
            // 
            // textEdit7
            // 
            this.textEdit7.Location = new System.Drawing.Point(567, -569);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Size = new System.Drawing.Size(163, 20);
            this.textEdit7.StyleController = this.layoutControl1;
            this.textEdit7.TabIndex = 10;
            // 
            // textEdit6
            // 
            this.textEdit6.Location = new System.Drawing.Point(339, -521);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Size = new System.Drawing.Size(391, 20);
            this.textEdit6.StyleController = this.layoutControl1;
            this.textEdit6.TabIndex = 9;
            // 
            // textEdit5
            // 
            this.textEdit5.Location = new System.Drawing.Point(567, -545);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Size = new System.Drawing.Size(163, 20);
            this.textEdit5.StyleController = this.layoutControl1;
            this.textEdit5.TabIndex = 8;
            // 
            // textEdit4
            // 
            this.textEdit4.Location = new System.Drawing.Point(339, -569);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Size = new System.Drawing.Size(139, 20);
            this.textEdit4.StyleController = this.layoutControl1;
            this.textEdit4.TabIndex = 7;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(88, -569);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(162, 20);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "更年期保健检查表";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.layoutControlGroup9,
            this.layoutControlGroup10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, -599);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(733, 1067);
            this.layoutControlGroup1.Text = "更年期保健检查表";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem1,
            this.layoutControlItem4,
            this.layoutControlItem10,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(731, 72);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit8;
            this.layoutControlItem8.CustomizationFormText = "居住状态 ";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(226, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(251, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "居住状态 ";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit9;
            this.layoutControlItem9.CustomizationFormText = "出生日期 ";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(226, 24);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(251, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "出生日期 ";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit1;
            this.layoutControlItem1.CustomizationFormText = "卡 号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(226, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(251, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "卡 号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit4;
            this.layoutControlItem4.CustomizationFormText = "档案编号 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(251, 0);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(226, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "档案编号 ";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.textEdit10;
            this.layoutControlItem10.CustomizationFormText = "身份证号";
            this.layoutControlItem10.Location = new System.Drawing.Point(251, 24);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(226, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "身份证号";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit5;
            this.layoutControlItem5.CustomizationFormText = "联系电话 ";
            this.layoutControlItem5.Location = new System.Drawing.Point(479, 24);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(226, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(252, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "联系电话 ";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit7;
            this.layoutControlItem7.CustomizationFormText = "姓 名 ";
            this.layoutControlItem7.Location = new System.Drawing.Point(479, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(226, 24);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(252, 24);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "姓 名 ";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit6;
            this.layoutControlItem6.CustomizationFormText = "居住地址 ";
            this.layoutControlItem6.Location = new System.Drawing.Point(251, 48);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(226, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(480, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "居住地址 ";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup3.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup3.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup3.CustomizationFormText = "前诊病史记录:";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem12});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 72);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(731, 74);
            this.layoutControlGroup3.Text = "前诊病史记录:";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.Control = this.flowLayoutPanel2;
            this.layoutControlItem11.CustomizationFormText = "经 史";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(491, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "经 史";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.Control = this.textEdit2;
            this.layoutControlItem2.CustomizationFormText = "主 诉";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(311, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "主 诉";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.Control = this.textEdit3;
            this.layoutControlItem3.CustomizationFormText = "既往病史";
            this.layoutControlItem3.Location = new System.Drawing.Point(311, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(414, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "既往病史";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.Control = this.textEdit11;
            this.layoutControlItem12.CustomizationFormText = "目前月经";
            this.layoutControlItem12.Location = new System.Drawing.Point(491, 24);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "目前月经";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup4.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup4.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup4.CustomizationFormText = "孕产史:";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem14});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 146);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(731, 98);
            this.layoutControlGroup4.Text = "孕产史:";
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.Control = this.flowLayoutPanel3;
            this.layoutControlItem13.CustomizationFormText = "孕产史";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(447, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "孕产史";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem15.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem15.Control = this.flowLayoutPanel4;
            this.layoutControlItem15.CustomizationFormText = "避孕方式";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(725, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "避孕方式";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.Control = this.textEdit12;
            this.layoutControlItem16.CustomizationFormText = "产时并发症";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(360, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "产时并发症";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem17.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem17.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem17.Control = this.ucTxtLbl2;
            this.layoutControlItem17.CustomizationFormText = "哺乳时间总和";
            this.layoutControlItem17.Location = new System.Drawing.Point(360, 48);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "哺乳时间总和";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.Control = this.ucTxtLbl1;
            this.layoutControlItem14.CustomizationFormText = "初产年龄";
            this.layoutControlItem14.Location = new System.Drawing.Point(447, 0);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(278, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "初产年龄";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup5.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup5.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup5.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup5.CustomizationFormText = "家族史: ";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem25,
            this.layoutControlItem24,
            this.layoutControlItem23,
            this.layoutControlItem22});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 244);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(731, 138);
            this.layoutControlGroup5.Text = "家族史: ";
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.Control = this.flowLayoutPanel5;
            this.layoutControlItem18.CustomizationFormText = "乳腺癌";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(276, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(358, 40);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "乳腺癌";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.Control = this.flowLayoutPanel6;
            this.layoutControlItem19.CustomizationFormText = "其它癌症史";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(276, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(358, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "其它癌症史";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem20.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem20.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem20.Control = this.flowLayoutPanel7;
            this.layoutControlItem20.CustomizationFormText = "高血压病史";
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 64);
            this.layoutControlItem20.MinSize = new System.Drawing.Size(276, 24);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(358, 24);
            this.layoutControlItem20.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem20.Text = "高血压病史";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.Control = this.flowLayoutPanel8;
            this.layoutControlItem21.CustomizationFormText = "精神病家族史";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 88);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(276, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(358, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "精神病家族史";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem25.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem25.Control = this.flowLayoutPanel12;
            this.layoutControlItem25.CustomizationFormText = "脑血管病家族史";
            this.layoutControlItem25.Location = new System.Drawing.Point(358, 88);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(276, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(367, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "脑血管病家族史";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem24.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem24.Control = this.flowLayoutPanel11;
            this.layoutControlItem24.CustomizationFormText = "冠心病家族史";
            this.layoutControlItem24.Location = new System.Drawing.Point(358, 64);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(276, 24);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(367, 24);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "冠心病家族史";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem23.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem23.Control = this.flowLayoutPanel10;
            this.layoutControlItem23.CustomizationFormText = "子宫内膜癌家族史";
            this.layoutControlItem23.Location = new System.Drawing.Point(358, 40);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(276, 24);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(367, 24);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "子宫内膜癌家族史";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem22.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem22.Control = this.flowLayoutPanel9;
            this.layoutControlItem22.CustomizationFormText = "家族一代以内是否有患乳腺癌的";
            this.layoutControlItem22.Location = new System.Drawing.Point(358, 0);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(276, 40);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(367, 40);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "家族一代以内是否有患乳腺癌的";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup6.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup6.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup6.CustomizationFormText = "个人史:";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem26,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem31});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 382);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Size = new System.Drawing.Size(731, 74);
            this.layoutControlGroup6.Text = "个人史:";
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem26.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem26.Control = this.textEdit20;
            this.layoutControlItem26.CustomizationFormText = "文化程度 ";
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(175, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(199, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "文化程度 ";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.Control = this.textEdit23;
            this.layoutControlItem29.CustomizationFormText = "收入情况";
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(175, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(199, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "收入情况";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem30.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem30.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem30.Control = this.textEdit24;
            this.layoutControlItem30.CustomizationFormText = "目前就业情况";
            this.layoutControlItem30.Location = new System.Drawing.Point(199, 24);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(175, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(249, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "目前就业情况";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem27.Control = this.textEdit21;
            this.layoutControlItem27.CustomizationFormText = "性 格";
            this.layoutControlItem27.Location = new System.Drawing.Point(199, 0);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(175, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(249, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "性 格";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem28.Control = this.textEdit22;
            this.layoutControlItem28.CustomizationFormText = "工作压力";
            this.layoutControlItem28.Location = new System.Drawing.Point(448, 0);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(175, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(277, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "工作压力";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(120, 20);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.Control = this.textEdit25;
            this.layoutControlItem31.CustomizationFormText = "目前用药情况(HRT史)";
            this.layoutControlItem31.Location = new System.Drawing.Point(448, 24);
            this.layoutControlItem31.MaxSize = new System.Drawing.Size(0, 40);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(175, 24);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(277, 24);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "目前用药情况(HRT史)";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(120, 20);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup7.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup7.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup7.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup7.CustomizationFormText = "检查记录:";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem32,
            this.layoutControlItem36,
            this.layoutControlItem38,
            this.layoutControlItem41,
            this.layoutControlItem42,
            this.layoutControlItem39,
            this.layoutControlItem35,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem40,
            this.layoutControlItem37});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 456);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(731, 122);
            this.layoutControlGroup7.Text = "检查记录:";
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.Control = this.ucTxtLbl3;
            this.layoutControlItem32.CustomizationFormText = "身高";
            this.layoutControlItem32.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(222, 24);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "身高";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem36.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem36.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem36.Control = this.textEdit27;
            this.layoutControlItem36.CustomizationFormText = "心";
            this.layoutControlItem36.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(222, 24);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "心";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem38.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem38.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem38.Control = this.textEdit29;
            this.layoutControlItem38.CustomizationFormText = "脾";
            this.layoutControlItem38.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem38.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem38.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(222, 24);
            this.layoutControlItem38.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem38.Text = "脾";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem41.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem41.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem41.Control = this.textEdit32;
            this.layoutControlItem41.CustomizationFormText = "乳 房";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem41.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem41.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(222, 24);
            this.layoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem41.Text = "乳 房";
            this.layoutControlItem41.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem41.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem41.TextToControlDistance = 5;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem42.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem42.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem42.Control = this.textEdit33;
            this.layoutControlItem42.CustomizationFormText = "乳 头";
            this.layoutControlItem42.Location = new System.Drawing.Point(222, 72);
            this.layoutControlItem42.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Text = "乳 头";
            this.layoutControlItem42.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem42.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem42.TextToControlDistance = 5;
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem39.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem39.Control = this.textEdit30;
            this.layoutControlItem39.CustomizationFormText = "四 肢";
            this.layoutControlItem39.Location = new System.Drawing.Point(222, 48);
            this.layoutControlItem39.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "四 肢";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem35.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem35.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem35.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem35.Control = this.textEdit26;
            this.layoutControlItem35.CustomizationFormText = "肝";
            this.layoutControlItem35.Location = new System.Drawing.Point(222, 24);
            this.layoutControlItem35.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "肝";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem33.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem33.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem33.Control = this.ucTxtLbl4;
            this.layoutControlItem33.CustomizationFormText = "体重";
            this.layoutControlItem33.Location = new System.Drawing.Point(222, 0);
            this.layoutControlItem33.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(219, 24);
            this.layoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem33.Text = "体重";
            this.layoutControlItem33.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem33.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem33.TextToControlDistance = 5;
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem34.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem34.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem34.Control = this.ucTxtLblTxtLbl1;
            this.layoutControlItem34.CustomizationFormText = "血压";
            this.layoutControlItem34.Location = new System.Drawing.Point(441, 0);
            this.layoutControlItem34.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(284, 24);
            this.layoutControlItem34.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem34.Text = "血压";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem40.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem40.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem40.Control = this.textEdit31;
            this.layoutControlItem40.CustomizationFormText = "肺";
            this.layoutControlItem40.Location = new System.Drawing.Point(441, 24);
            this.layoutControlItem40.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(284, 24);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "肺";
            this.layoutControlItem40.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem40.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem40.TextToControlDistance = 5;
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem37.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem37.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem37.Control = this.textEdit28;
            this.layoutControlItem37.CustomizationFormText = "脊 柱";
            this.layoutControlItem37.Location = new System.Drawing.Point(441, 48);
            this.layoutControlItem37.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem37.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(284, 48);
            this.layoutControlItem37.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem37.Text = "脊 柱";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup8.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup8.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup8.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup8.CustomizationFormText = "妇科检查: ";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem43,
            this.layoutControlItem47,
            this.layoutControlItem44,
            this.layoutControlItem52,
            this.layoutControlItem55,
            this.layoutControlItem59,
            this.layoutControlItem61,
            this.layoutControlItem64,
            this.layoutControlItem67,
            this.layoutControlItem71,
            this.layoutControlItem70,
            this.layoutControlItem69,
            this.layoutControlItem66,
            this.layoutControlItem60,
            this.layoutControlItem63,
            this.layoutControlItem56,
            this.layoutControlItem51,
            this.layoutControlItem53,
            this.layoutControlItem46,
            this.layoutControlItem49,
            this.layoutControlItem48,
            this.layoutControlItem45,
            this.layoutControlItem50,
            this.layoutControlItem54,
            this.layoutControlItem57,
            this.layoutControlItem62,
            this.layoutControlItem58,
            this.layoutControlItem65,
            this.layoutControlItem68});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 578);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup8.Size = new System.Drawing.Size(731, 290);
            this.layoutControlGroup8.Text = "妇科检查: ";
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem43.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem43.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem43.Control = this.textEdit34;
            this.layoutControlItem43.CustomizationFormText = "外 阴";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem43.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem43.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem43.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem43.Text = "外 阴";
            this.layoutControlItem43.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem43.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem43.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem47.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem47.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem47.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem47.Control = this.textEdit38;
            this.layoutControlItem47.CustomizationFormText = "宫 体";
            this.layoutControlItem47.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "宫 体";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem44.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem44.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem44.Control = this.textEdit35;
            this.layoutControlItem44.CustomizationFormText = "阴道清洁度";
            this.layoutControlItem44.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem44.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem44.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem44.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem44.Text = "阴道清洁度";
            this.layoutControlItem44.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem44.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem44.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem52.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem52.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem52.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem52.Control = this.textEdit43;
            this.layoutControlItem52.CustomizationFormText = "B超检查";
            this.layoutControlItem52.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem52.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem52.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(262, 24);
            this.layoutControlItem52.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem52.Text = "B超检查";
            this.layoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem52.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem52.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem55.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem55.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem55.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem55.Control = this.ucTxtLbl6;
            this.layoutControlItem55.CustomizationFormText = "子宫内膜厚度";
            this.layoutControlItem55.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem55.MinSize = new System.Drawing.Size(216, 24);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(260, 48);
            this.layoutControlItem55.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem55.Text = "子宫内膜厚度";
            this.layoutControlItem55.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem55.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem59.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem59.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem59.Control = this.textEdit46;
            this.layoutControlItem59.CustomizationFormText = "子宫肿块位置";
            this.layoutControlItem59.Location = new System.Drawing.Point(0, 144);
            this.layoutControlItem59.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem59.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem59.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem59.Text = "子宫肿块位置";
            this.layoutControlItem59.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem59.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem59.TextToControlDistance = 5;
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem61.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem61.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem61.Control = this.textEdit48;
            this.layoutControlItem61.CustomizationFormText = "心电图检查结果";
            this.layoutControlItem61.Location = new System.Drawing.Point(0, 168);
            this.layoutControlItem61.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem61.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Size = new System.Drawing.Size(260, 24);
            this.layoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem61.Text = "心电图检查结果";
            this.layoutControlItem61.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem61.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem61.TextToControlDistance = 5;
            // 
            // layoutControlItem64
            // 
            this.layoutControlItem64.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem64.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem64.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem64.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem64.Control = this.textEdit51;
            this.layoutControlItem64.CustomizationFormText = "乳腺母靶检查";
            this.layoutControlItem64.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem64.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem64.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem64.Name = "layoutControlItem64";
            this.layoutControlItem64.Size = new System.Drawing.Size(259, 24);
            this.layoutControlItem64.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem64.Text = "乳腺母靶检查";
            this.layoutControlItem64.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem64.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem64.TextToControlDistance = 5;
            // 
            // layoutControlItem67
            // 
            this.layoutControlItem67.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem67.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem67.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem67.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem67.Control = this.textEdit53;
            this.layoutControlItem67.CustomizationFormText = "尿 常 规";
            this.layoutControlItem67.Location = new System.Drawing.Point(0, 216);
            this.layoutControlItem67.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem67.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem67.Name = "layoutControlItem67";
            this.layoutControlItem67.Size = new System.Drawing.Size(259, 24);
            this.layoutControlItem67.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem67.Text = "尿 常 规";
            this.layoutControlItem67.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem67.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem67.TextToControlDistance = 5;
            // 
            // layoutControlItem71
            // 
            this.layoutControlItem71.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem71.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem71.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem71.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem71.Control = this.ucTxtLbl11;
            this.layoutControlItem71.CustomizationFormText = "胆 固 醇";
            this.layoutControlItem71.Location = new System.Drawing.Point(0, 240);
            this.layoutControlItem71.MinSize = new System.Drawing.Size(216, 24);
            this.layoutControlItem71.Name = "layoutControlItem71";
            this.layoutControlItem71.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem71.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem71.Text = "胆 固 醇";
            this.layoutControlItem71.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem71.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem71.TextToControlDistance = 5;
            // 
            // layoutControlItem70
            // 
            this.layoutControlItem70.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem70.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem70.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem70.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem70.Control = this.ucTxtLbl10;
            this.layoutControlItem70.CustomizationFormText = "甘 油 三 脂";
            this.layoutControlItem70.Location = new System.Drawing.Point(258, 240);
            this.layoutControlItem70.MinSize = new System.Drawing.Size(216, 24);
            this.layoutControlItem70.Name = "layoutControlItem70";
            this.layoutControlItem70.Size = new System.Drawing.Size(467, 24);
            this.layoutControlItem70.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem70.Text = "甘 油 三 脂";
            this.layoutControlItem70.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem70.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem70.TextToControlDistance = 5;
            // 
            // layoutControlItem69
            // 
            this.layoutControlItem69.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem69.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem69.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem69.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem69.Control = this.textEdit55;
            this.layoutControlItem69.CustomizationFormText = "肝功检查结果";
            this.layoutControlItem69.Location = new System.Drawing.Point(259, 216);
            this.layoutControlItem69.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem69.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem69.Name = "layoutControlItem69";
            this.layoutControlItem69.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem69.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem69.Text = "肝功检查结果";
            this.layoutControlItem69.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem69.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem69.TextToControlDistance = 5;
            // 
            // layoutControlItem66
            // 
            this.layoutControlItem66.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem66.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem66.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem66.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem66.Control = this.ucTxtLbl9;
            this.layoutControlItem66.CustomizationFormText = "血 糖";
            this.layoutControlItem66.Location = new System.Drawing.Point(259, 192);
            this.layoutControlItem66.MinSize = new System.Drawing.Size(216, 24);
            this.layoutControlItem66.Name = "layoutControlItem66";
            this.layoutControlItem66.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem66.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem66.Text = "血 糖";
            this.layoutControlItem66.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem66.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem66.TextToControlDistance = 5;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem60.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem60.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem60.Control = this.textEdit47;
            this.layoutControlItem60.CustomizationFormText = "MES检查结果";
            this.layoutControlItem60.Location = new System.Drawing.Point(260, 168);
            this.layoutControlItem60.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem60.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(237, 24);
            this.layoutControlItem60.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem60.Text = "MES检查结果";
            this.layoutControlItem60.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem60.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem60.TextToControlDistance = 5;
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem63.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem63.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem63.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem63.Control = this.textEdit50;
            this.layoutControlItem63.CustomizationFormText = "乳腺红外线检查";
            this.layoutControlItem63.Location = new System.Drawing.Point(260, 144);
            this.layoutControlItem63.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem63.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem63.Name = "layoutControlItem63";
            this.layoutControlItem63.Size = new System.Drawing.Size(237, 24);
            this.layoutControlItem63.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem63.Text = "乳腺红外线检查";
            this.layoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem63.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem63.TextToControlDistance = 5;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem56.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem56.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem56.Control = this.ucTxtLbl7;
            this.layoutControlItem56.CustomizationFormText = "子宫肿块数目";
            this.layoutControlItem56.Location = new System.Drawing.Point(260, 96);
            this.layoutControlItem56.MinSize = new System.Drawing.Size(216, 24);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(236, 48);
            this.layoutControlItem56.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem56.Text = "子宫肿块数目";
            this.layoutControlItem56.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem56.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem56.TextToControlDistance = 5;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem51.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem51.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem51.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem51.Control = this.textEdit42;
            this.layoutControlItem51.CustomizationFormText = "激素影响程度";
            this.layoutControlItem51.Location = new System.Drawing.Point(262, 72);
            this.layoutControlItem51.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem51.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem51.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem51.Text = "激素影响程度";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem53.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem53.Control = this.textEdit44;
            this.layoutControlItem53.CustomizationFormText = "阴道分泌物检查";
            this.layoutControlItem53.Location = new System.Drawing.Point(262, 48);
            this.layoutControlItem53.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem53.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem53.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem53.Text = "阴道分泌物检查";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem46.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem46.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem46.Control = this.textEdit37;
            this.layoutControlItem46.CustomizationFormText = "附 件";
            this.layoutControlItem46.Location = new System.Drawing.Point(262, 24);
            this.layoutControlItem46.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem46.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem46.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem46.Text = "附 件";
            this.layoutControlItem46.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem46.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem46.TextToControlDistance = 5;
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem49.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem49.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem49.Control = this.textEdit40;
            this.layoutControlItem49.CustomizationFormText = "阴 道";
            this.layoutControlItem49.Location = new System.Drawing.Point(262, 0);
            this.layoutControlItem49.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem49.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(234, 24);
            this.layoutControlItem49.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem49.Text = "阴 道";
            this.layoutControlItem49.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem49.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem49.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem48.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem48.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem48.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem48.Control = this.textEdit39;
            this.layoutControlItem48.CustomizationFormText = "宫 颈";
            this.layoutControlItem48.Location = new System.Drawing.Point(496, 0);
            this.layoutControlItem48.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "宫 颈";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem45.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem45.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem45.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem45.Control = this.textEdit36;
            this.layoutControlItem45.CustomizationFormText = "辅助检查";
            this.layoutControlItem45.Location = new System.Drawing.Point(496, 24);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "辅助检查";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem50.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem50.Control = this.textEdit41;
            this.layoutControlItem50.CustomizationFormText = "TCT检查结果";
            this.layoutControlItem50.Location = new System.Drawing.Point(496, 48);
            this.layoutControlItem50.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem50.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem50.Text = "TCT检查结果";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem54.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem54.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem54.Control = this.ucTxtLbl5;
            this.layoutControlItem54.CustomizationFormText = "子宫(长*宽*厚)";
            this.layoutControlItem54.Location = new System.Drawing.Point(496, 72);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(216, 24);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Text = "子宫(长*宽*厚)";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem57.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem57.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem57.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem57.Control = this.ucTxtLbl8;
            this.layoutControlItem57.CustomizationFormText = "子宫肿块(长*宽*厚)";
            this.layoutControlItem57.Location = new System.Drawing.Point(496, 96);
            this.layoutControlItem57.MinSize = new System.Drawing.Size(216, 48);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(229, 48);
            this.layoutControlItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem57.Text = "子宫肿块(长*宽*厚)";
            this.layoutControlItem57.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem57.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem57.TextToControlDistance = 5;
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem62.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem62.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem62.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem62.Control = this.textEdit49;
            this.layoutControlItem62.CustomizationFormText = "卵巢情况";
            this.layoutControlItem62.Location = new System.Drawing.Point(497, 144);
            this.layoutControlItem62.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem62.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem62.Name = "layoutControlItem62";
            this.layoutControlItem62.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem62.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem62.Text = "卵巢情况";
            this.layoutControlItem62.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem62.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem62.TextToControlDistance = 5;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem58.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem58.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem58.Control = this.textEdit45;
            this.layoutControlItem58.CustomizationFormText = "骨密度检查结果";
            this.layoutControlItem58.Location = new System.Drawing.Point(497, 168);
            this.layoutControlItem58.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem58.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem58.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem58.Text = "骨密度检查结果";
            this.layoutControlItem58.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem58.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem58.TextToControlDistance = 5;
            // 
            // layoutControlItem65
            // 
            this.layoutControlItem65.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem65.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem65.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem65.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem65.Control = this.textEdit52;
            this.layoutControlItem65.CustomizationFormText = "血 常 规";
            this.layoutControlItem65.Location = new System.Drawing.Point(497, 192);
            this.layoutControlItem65.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem65.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem65.Name = "layoutControlItem65";
            this.layoutControlItem65.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem65.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem65.Text = "血 常 规";
            this.layoutControlItem65.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem65.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem65.TextToControlDistance = 5;
            // 
            // layoutControlItem68
            // 
            this.layoutControlItem68.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem68.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem68.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem68.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem68.Control = this.textEdit54;
            this.layoutControlItem68.CustomizationFormText = "肾功检查结果";
            this.layoutControlItem68.Location = new System.Drawing.Point(497, 216);
            this.layoutControlItem68.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem68.MinSize = new System.Drawing.Size(166, 24);
            this.layoutControlItem68.Name = "layoutControlItem68";
            this.layoutControlItem68.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem68.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem68.Text = "肾功检查结果";
            this.layoutControlItem68.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem68.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem68.TextToControlDistance = 5;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup9.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup9.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup9.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup9.CustomizationFormText = "妇科内分泌检查:";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem72,
            this.layoutControlItem73,
            this.layoutControlItem75,
            this.layoutControlItem76,
            this.layoutControlItem77,
            this.layoutControlItem74,
            this.layoutControlItem78,
            this.layoutControlItem79,
            this.layoutControlItem80});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 868);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup9.Size = new System.Drawing.Size(731, 122);
            this.layoutControlGroup9.Text = "妇科内分泌检查:";
            // 
            // layoutControlItem72
            // 
            this.layoutControlItem72.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem72.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem72.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem72.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem72.Control = this.ucTxtLbl12;
            this.layoutControlItem72.CustomizationFormText = "E2";
            this.layoutControlItem72.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem72.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem72.Name = "layoutControlItem72";
            this.layoutControlItem72.Size = new System.Drawing.Size(261, 24);
            this.layoutControlItem72.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem72.Text = "E2";
            this.layoutControlItem72.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem72.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem72.TextToControlDistance = 5;
            // 
            // layoutControlItem73
            // 
            this.layoutControlItem73.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem73.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem73.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem73.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem73.Control = this.ucTxtLbl13;
            this.layoutControlItem73.CustomizationFormText = "T";
            this.layoutControlItem73.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem73.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem73.Name = "layoutControlItem73";
            this.layoutControlItem73.Size = new System.Drawing.Size(725, 24);
            this.layoutControlItem73.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem73.Text = "T";
            this.layoutControlItem73.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem73.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem73.TextToControlDistance = 5;
            // 
            // layoutControlItem75
            // 
            this.layoutControlItem75.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem75.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem75.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem75.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem75.Control = this.ucTxtLbl15;
            this.layoutControlItem75.CustomizationFormText = "P";
            this.layoutControlItem75.Location = new System.Drawing.Point(261, 0);
            this.layoutControlItem75.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem75.Name = "layoutControlItem75";
            this.layoutControlItem75.Size = new System.Drawing.Size(238, 24);
            this.layoutControlItem75.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem75.Text = "P";
            this.layoutControlItem75.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem75.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem75.TextToControlDistance = 5;
            // 
            // layoutControlItem76
            // 
            this.layoutControlItem76.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem76.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem76.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem76.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem76.Control = this.textEdit56;
            this.layoutControlItem76.CustomizationFormText = "诊 断";
            this.layoutControlItem76.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem76.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem76.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem76.Name = "layoutControlItem76";
            this.layoutControlItem76.Size = new System.Drawing.Size(352, 24);
            this.layoutControlItem76.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem76.Text = "诊 断";
            this.layoutControlItem76.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem76.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem76.TextToControlDistance = 5;
            // 
            // layoutControlItem77
            // 
            this.layoutControlItem77.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem77.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem77.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem77.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem77.Control = this.textEdit57;
            this.layoutControlItem77.CustomizationFormText = "处 治";
            this.layoutControlItem77.Location = new System.Drawing.Point(352, 48);
            this.layoutControlItem77.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem77.MinSize = new System.Drawing.Size(109, 24);
            this.layoutControlItem77.Name = "layoutControlItem77";
            this.layoutControlItem77.Size = new System.Drawing.Size(373, 24);
            this.layoutControlItem77.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem77.Text = "处 治";
            this.layoutControlItem77.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem77.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem77.TextToControlDistance = 5;
            // 
            // layoutControlItem74
            // 
            this.layoutControlItem74.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem74.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem74.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem74.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem74.Control = this.ucTxtLbl14;
            this.layoutControlItem74.CustomizationFormText = "FSH";
            this.layoutControlItem74.Location = new System.Drawing.Point(499, 0);
            this.layoutControlItem74.MinSize = new System.Drawing.Size(129, 24);
            this.layoutControlItem74.Name = "layoutControlItem74";
            this.layoutControlItem74.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem74.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem74.Text = "FSH";
            this.layoutControlItem74.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem74.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem74.TextToControlDistance = 5;
            // 
            // layoutControlItem78
            // 
            this.layoutControlItem78.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem78.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem78.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem78.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem78.Control = this.dateEdit1;
            this.layoutControlItem78.CustomizationFormText = "检查日期";
            this.layoutControlItem78.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem78.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem78.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem78.Name = "layoutControlItem78";
            this.layoutControlItem78.Size = new System.Drawing.Size(268, 24);
            this.layoutControlItem78.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem78.Text = "检查日期";
            this.layoutControlItem78.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem78.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem78.TextToControlDistance = 5;
            // 
            // layoutControlItem79
            // 
            this.layoutControlItem79.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem79.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem79.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem79.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem79.Control = this.textEdit58;
            this.layoutControlItem79.CustomizationFormText = "检查医院";
            this.layoutControlItem79.Location = new System.Drawing.Point(268, 72);
            this.layoutControlItem79.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem79.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem79.Name = "layoutControlItem79";
            this.layoutControlItem79.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem79.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem79.Text = "检查医院";
            this.layoutControlItem79.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem79.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem79.TextToControlDistance = 5;
            // 
            // layoutControlItem80
            // 
            this.layoutControlItem80.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem80.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem80.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem80.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem80.Control = this.textEdit59;
            this.layoutControlItem80.CustomizationFormText = "医师签名";
            this.layoutControlItem80.Location = new System.Drawing.Point(497, 72);
            this.layoutControlItem80.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem80.MinSize = new System.Drawing.Size(106, 24);
            this.layoutControlItem80.Name = "layoutControlItem80";
            this.layoutControlItem80.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem80.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem80.Text = "医师签名";
            this.layoutControlItem80.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem80.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem80.TextToControlDistance = 5;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "layoutControlGroup10";
            this.layoutControlGroup10.GroupBordersVisible = false;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem81,
            this.layoutControlItem86,
            this.layoutControlItem85,
            this.layoutControlItem84,
            this.layoutControlItem83,
            this.layoutControlItem82});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 990);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(731, 48);
            this.layoutControlGroup10.Text = "layoutControlGroup10";
            // 
            // layoutControlItem81
            // 
            this.layoutControlItem81.Control = this.textEdit60;
            this.layoutControlItem81.CustomizationFormText = "创建时间:";
            this.layoutControlItem81.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem81.Name = "layoutControlItem81";
            this.layoutControlItem81.Size = new System.Drawing.Size(249, 24);
            this.layoutControlItem81.Text = "创建时间:";
            this.layoutControlItem81.TextSize = new System.Drawing.Size(80, 14);
            // 
            // layoutControlItem86
            // 
            this.layoutControlItem86.Control = this.textEdit65;
            this.layoutControlItem86.CustomizationFormText = "最近更新时间: ";
            this.layoutControlItem86.Location = new System.Drawing.Point(249, 0);
            this.layoutControlItem86.Name = "layoutControlItem86";
            this.layoutControlItem86.Size = new System.Drawing.Size(254, 24);
            this.layoutControlItem86.Text = "最近更新时间: ";
            this.layoutControlItem86.TextSize = new System.Drawing.Size(80, 14);
            // 
            // layoutControlItem85
            // 
            this.layoutControlItem85.Control = this.textEdit64;
            this.layoutControlItem85.CustomizationFormText = "当前所属机构:";
            this.layoutControlItem85.Location = new System.Drawing.Point(503, 0);
            this.layoutControlItem85.Name = "layoutControlItem85";
            this.layoutControlItem85.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem85.Text = "当前所属机构:";
            this.layoutControlItem85.TextSize = new System.Drawing.Size(80, 14);
            // 
            // layoutControlItem84
            // 
            this.layoutControlItem84.Control = this.textEdit63;
            this.layoutControlItem84.CustomizationFormText = "创建机构:";
            this.layoutControlItem84.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem84.Name = "layoutControlItem84";
            this.layoutControlItem84.Size = new System.Drawing.Size(249, 24);
            this.layoutControlItem84.Text = "创建机构:";
            this.layoutControlItem84.TextSize = new System.Drawing.Size(80, 14);
            // 
            // layoutControlItem83
            // 
            this.layoutControlItem83.Control = this.textEdit62;
            this.layoutControlItem83.CustomizationFormText = "创建人: ";
            this.layoutControlItem83.Location = new System.Drawing.Point(249, 24);
            this.layoutControlItem83.Name = "layoutControlItem83";
            this.layoutControlItem83.Size = new System.Drawing.Size(254, 24);
            this.layoutControlItem83.Text = "创建人: ";
            this.layoutControlItem83.TextSize = new System.Drawing.Size(80, 14);
            // 
            // layoutControlItem82
            // 
            this.layoutControlItem82.Control = this.textEdit61;
            this.layoutControlItem82.CustomizationFormText = "最近修改人:";
            this.layoutControlItem82.Location = new System.Drawing.Point(503, 24);
            this.layoutControlItem82.Name = "layoutControlItem82";
            this.layoutControlItem82.Size = new System.Drawing.Size(228, 24);
            this.layoutControlItem82.Text = "最近修改人:";
            this.layoutControlItem82.TextSize = new System.Drawing.Size(80, 14);
            // 
            // UC更年期保健检查表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC更年期保健检查表";
            this.Size = new System.Drawing.Size(750, 500);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit65.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit64.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit63.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit62.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit61.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit60.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit59.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit58.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit57.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit56.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit55.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit54.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit53.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit52.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit51.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit50.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit49.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit48.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit47.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit46.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit45.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit44.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit43.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit42.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit41.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit40.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit39.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit38.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit37.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit36.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit35.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit34.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit33.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit32.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit20.Properties)).EndInit();
            this.flowLayoutPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit19.Properties)).EndInit();
            this.flowLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit18.Properties)).EndInit();
            this.flowLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit17.Properties)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            this.flowLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit16.Properties)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit15.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit14.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit1.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem70)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem77)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem78)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem80)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem81)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem86)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem83)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem82)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn保存;
        private DevExpress.XtraEditors.SimpleButton btn重置;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private DevExpress.XtraEditors.TextEdit textEdit9;
        private DevExpress.XtraEditors.TextEdit textEdit8;
        private DevExpress.XtraEditors.TextEdit textEdit7;
        private DevExpress.XtraEditors.TextEdit textEdit6;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        private DevExpress.XtraEditors.TextEdit textEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl4;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl5;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl6;
        private DevExpress.XtraEditors.TextEdit textEdit11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl1;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl2;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private Library.UserControls.UCTxtLbl ucTxtLbl2;
        private DevExpress.XtraEditors.TextEdit textEdit12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit1;
        private Library.UserControls.UCLblTxtLbl ucLblTxtLbl7;
        private Library.UserControls.UCTxtLbl ucTxtLbl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.TextEdit textEdit16;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit15;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.TextEdit textEdit14;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraEditors.TextEdit textEdit25;
        private DevExpress.XtraEditors.TextEdit textEdit24;
        private DevExpress.XtraEditors.TextEdit textEdit23;
        private DevExpress.XtraEditors.TextEdit textEdit22;
        private DevExpress.XtraEditors.TextEdit textEdit21;
        private DevExpress.XtraEditors.TextEdit textEdit20;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.TextEdit textEdit19;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.TextEdit textEdit18;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.TextEdit textEdit17;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraEditors.TextEdit textEdit33;
        private DevExpress.XtraEditors.TextEdit textEdit32;
        private DevExpress.XtraEditors.TextEdit textEdit31;
        private DevExpress.XtraEditors.TextEdit textEdit30;
        private DevExpress.XtraEditors.TextEdit textEdit29;
        private DevExpress.XtraEditors.TextEdit textEdit28;
        private DevExpress.XtraEditors.TextEdit textEdit27;
        private DevExpress.XtraEditors.TextEdit textEdit26;
        private Library.UserControls.UCTxtLblTxtLbl ucTxtLblTxtLbl1;
        private Library.UserControls.UCTxtLbl ucTxtLbl4;
        private Library.UserControls.UCTxtLbl ucTxtLbl3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private Library.UserControls.UCTxtLbl ucTxtLbl11;
        private Library.UserControls.UCTxtLbl ucTxtLbl10;
        private DevExpress.XtraEditors.TextEdit textEdit55;
        private DevExpress.XtraEditors.TextEdit textEdit54;
        private DevExpress.XtraEditors.TextEdit textEdit53;
        private Library.UserControls.UCTxtLbl ucTxtLbl9;
        private DevExpress.XtraEditors.TextEdit textEdit52;
        private DevExpress.XtraEditors.TextEdit textEdit51;
        private DevExpress.XtraEditors.TextEdit textEdit50;
        private DevExpress.XtraEditors.TextEdit textEdit49;
        private DevExpress.XtraEditors.TextEdit textEdit48;
        private DevExpress.XtraEditors.TextEdit textEdit47;
        private DevExpress.XtraEditors.TextEdit textEdit46;
        private DevExpress.XtraEditors.TextEdit textEdit45;
        private Library.UserControls.UCTxtLbl ucTxtLbl8;
        private Library.UserControls.UCTxtLbl ucTxtLbl7;
        private Library.UserControls.UCTxtLbl ucTxtLbl6;
        private Library.UserControls.UCTxtLbl ucTxtLbl5;
        private DevExpress.XtraEditors.TextEdit textEdit44;
        private DevExpress.XtraEditors.TextEdit textEdit43;
        private DevExpress.XtraEditors.TextEdit textEdit42;
        private DevExpress.XtraEditors.TextEdit textEdit41;
        private DevExpress.XtraEditors.TextEdit textEdit40;
        private DevExpress.XtraEditors.TextEdit textEdit39;
        private DevExpress.XtraEditors.TextEdit textEdit38;
        private DevExpress.XtraEditors.TextEdit textEdit37;
        private DevExpress.XtraEditors.TextEdit textEdit36;
        private DevExpress.XtraEditors.TextEdit textEdit35;
        private DevExpress.XtraEditors.TextEdit textEdit34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem64;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem66;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem65;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem67;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem68;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem69;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem70;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem71;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraEditors.TextEdit textEdit57;
        private DevExpress.XtraEditors.TextEdit textEdit56;
        private Library.UserControls.UCTxtLbl ucTxtLbl15;
        private Library.UserControls.UCTxtLbl ucTxtLbl14;
        private Library.UserControls.UCTxtLbl ucTxtLbl13;
        private Library.UserControls.UCTxtLbl ucTxtLbl12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem72;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem73;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem75;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem74;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem76;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem77;
        private DevExpress.XtraEditors.TextEdit textEdit59;
        private DevExpress.XtraEditors.TextEdit textEdit58;
        private DevExpress.XtraEditors.DateEdit dateEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem78;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem79;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem80;
        private DevExpress.XtraEditors.TextEdit textEdit65;
        private DevExpress.XtraEditors.TextEdit textEdit64;
        private DevExpress.XtraEditors.TextEdit textEdit63;
        private DevExpress.XtraEditors.TextEdit textEdit62;
        private DevExpress.XtraEditors.TextEdit textEdit61;
        private DevExpress.XtraEditors.TextEdit textEdit60;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem81;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem86;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem85;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem84;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem83;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem82;
    }
}
