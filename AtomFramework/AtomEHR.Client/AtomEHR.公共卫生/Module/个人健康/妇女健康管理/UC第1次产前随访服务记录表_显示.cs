﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business.Security;
using AtomEHR.Models;
using AtomEHR.Common;
using DevExpress.XtraEditors;
using AtomEHR.Library.UserControls;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class UC第1次产前随访服务记录表_显示 : UserControlBaseNavBar
    {
        #region Fields
        AtomEHR.Business.bll孕妇产前一次随访记录 _Bll = new Business.bll孕妇产前一次随访记录();
        DataSet _ds孕妇产前一次随访记录;
        public frm个人健康 _frm = null;
        string _docNo;
        string _familyNo;
        string _id;
        DataRow _dr当前数据;
        #endregion

        public UC第1次产前随访服务记录表_显示()
        {
            InitializeComponent();
        }
        public UC第1次产前随访服务记录表_显示(Form parentForm)
        {
            InitializeComponent();
            _frm = (frm个人健康)parentForm;
            _docNo = _frm._docNo;
            _familyNo = _frm._familyDocNo;
            _id = _frm._param == null ? "" : _frm._param.ToString();

        }
        private void DoBindingDataSource(DataSet _ds孕妇产前一次随访记录)
        {
            if (_ds孕妇产前一次随访记录 == null) return;
            DataTable dt孕妇产前一次随访记录 = _ds孕妇产前一次随访记录.Tables[1];
            if (dt孕妇产前一次随访记录 == null || dt孕妇产前一次随访记录.Rows.Count <= 0)
            {
                btn添加.PerformClick();
                return;
            }

            //change 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 ▽
            DataTable dt孕妇基本信息 = _ds孕妇产前一次随访记录.Tables[2];
            #region 先绑定孕妇基本信息
            Bind丈夫基本信息(dt孕妇基本信息);
            #endregion

            DataTable dt个人基本信息 = _ds孕妇产前一次随访记录.Tables[0];
            #region 先绑定孕妇基本信息
            Bind基本信息(dt个人基本信息);
            #endregion

            //change 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 △

            #region 创建页面左边的导航树
            CreateNavBarButton_new(dt孕妇产前一次随访记录, tb_孕妇_产前随访1次.填表日期);
            #endregion

            //如果date为空，则默认绑定第一条体检数据
            //否则  根据date进行查找对应的 体检数据 进行绑定
            DataRow row;
            if (string.IsNullOrEmpty(_id) && dt孕妇产前一次随访记录.Rows.Count > 0)
            {
                row = dt孕妇产前一次随访记录.Rows[0];
            }
            else
            {
                DataRow[] rows = dt孕妇产前一次随访记录.Select("ID='" + _id + "'");
                if (rows.Length == 1)
                {
                    row = rows[0];
                }
                else
                {
                    return;
                }
            }
            DoBindingSummaryEditor(row);
            //设置日期栏中字体颜色
            SetItemColorToRed(_id);
        }
        protected override void DoBindingSummaryEditor(object dataSource)
        {
            _dr当前数据 = (DataRow)dataSource;
            if (_dr当前数据 == null) return;

            _sfid = _dr当前数据[tb_孕妇_产前随访1次.SFID].ToString();

            _id = _dr当前数据[tb_孕妇_产前随访1次.ID].ToString();
            this.txt卡号.Text = _dr当前数据[tb_孕妇_产前随访1次.卡号].ToString();
            this.dte填表日期.Text = _dr当前数据[tb_孕妇_产前随访1次.填表日期].ToString();
            this.uc填表孕周.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.孕周].ToString();
            this.uc填表孕周.Txt2.Text = _dr当前数据[tb_孕妇_产前随访1次.孕周天].ToString();
            this.textEdit孕妇年龄.Text = _dr当前数据[tb_孕妇_产前随访1次.孕妇年龄].ToString();
            this.textEdit丈夫姓名.Text = _dr当前数据[tb_孕妇_产前随访1次.丈夫姓名].ToString();
            this.textEdit丈夫年龄.Text = _dr当前数据[tb_孕妇_产前随访1次.丈夫年龄].ToString();
            this.textEdit丈夫电话.Text = _dr当前数据[tb_孕妇_产前随访1次.丈夫电话].ToString();
            this.textEdit孕次.Text = _dr当前数据[tb_孕妇_产前随访1次.孕次].ToString();
            this.uc产次.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.产次].ToString();
            this.uc产次.Txt2.Text = _dr当前数据[tb_孕妇_产前随访1次.剖宫产].ToString();
            this.txt末次月经.Text = _dr当前数据[tb_孕妇_产前随访1次.末次月经].ToString() == "" ? "不详" : _dr当前数据[tb_孕妇_产前随访1次.末次月经].ToString();
            this.dte预产期.Text = _dr当前数据[tb_孕妇_产前随访1次.预产期].ToString();
            this.txt既往病史.Text = SetFlowLayoutValues("jwbs", _dr当前数据[tb_孕妇_产前随访1次.既往病史].ToString());
            this.txt家族史.Text = SetFlowLayoutValues("jzs", _dr当前数据[tb_孕妇_产前随访1次.家族史].ToString());
            this.txt个人史.Text = SetFlowLayoutValues("grsh", _dr当前数据[tb_孕妇_产前随访1次.个人史].ToString());
            if (_dr当前数据[tb_孕妇_产前随访1次.妇科手术史].ToString() == "1")//有
            {
                this.txt妇科手术史.Text = "有：" + _dr当前数据[tb_孕妇_产前随访1次.妇科手术史有].ToString();
            }
            else
            {
                this.txt妇科手术史.Text = "无";
            }
            uc流产.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.流产].ToString();
            uc死胎.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.死胎].ToString();
            uc死产.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.死产].ToString();
            uc新生儿死亡.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.新生儿死亡].ToString();
            uc出生缺陷儿.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.出生缺陷儿].ToString();

            this.uc身高.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.身高].ToString();
            this.uc体重.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.体重].ToString();
            this.textEdit体重指数.Text = CalcBMI(this.uc身高.Txt1.Text, this.uc体重.Txt1.Text);
            this.uc血压.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.血压1].ToString();
            this.uc血压.Txt2.Text = _dr当前数据[tb_孕妇_产前随访1次.血压2].ToString();
            //this.textEdit体重指数.Text = _dr当前数据[tb_孕妇_第一次产前随访记录.指数].ToString();
            if (_dr当前数据[tb_孕妇_产前随访1次.心脏] == null ||
                _dr当前数据[tb_孕妇_产前随访1次.心脏].ToString() == "1" || _dr当前数据[tb_孕妇_产前随访1次.心脏].ToString() == "")//正常
                this.textEdit心脏.Text = "未见异常";
            else
                this.textEdit心脏.Text = "异常：" + _dr当前数据[tb_孕妇_产前随访1次.心脏异常].ToString();
            if (_dr当前数据[tb_孕妇_产前随访1次.肺部] == null || _dr当前数据[tb_孕妇_产前随访1次.肺部].ToString() == "1" || _dr当前数据[tb_孕妇_产前随访1次.肺部].ToString() == "")//正常
                this.textEdit肺部.Text = "未见异常";
            else
                this.textEdit肺部.Text = "异常：" + _dr当前数据[tb_孕妇_产前随访1次.肺部异常].ToString();

            if (_dr当前数据[tb_孕妇_产前随访1次.妇科外阴] == null || _dr当前数据[tb_孕妇_产前随访1次.妇科外阴].ToString() == "1" || _dr当前数据[tb_孕妇_产前随访1次.妇科外阴].ToString() == "")//正常
                this.textEdit外阴.Text = "未见异常";
            else
                this.textEdit外阴.Text = "异常：" + _dr当前数据[tb_孕妇_产前随访1次.妇科外阴异常].ToString();

            if (_dr当前数据[tb_孕妇_产前随访1次.妇科阴道] == null || _dr当前数据[tb_孕妇_产前随访1次.妇科阴道].ToString() == "1" || _dr当前数据[tb_孕妇_产前随访1次.妇科阴道].ToString() == "")//正常
                this.textEdit阴道.Text = "未见异常";
            else
                this.textEdit阴道.Text = "异常：" + _dr当前数据[tb_孕妇_产前随访1次.妇科阴道异常].ToString();

            if (_dr当前数据[tb_孕妇_产前随访1次.妇科宫颈] == null || _dr当前数据[tb_孕妇_产前随访1次.妇科宫颈].ToString() == "1" || _dr当前数据[tb_孕妇_产前随访1次.妇科宫颈].ToString() == "")//正常
                this.textEdit宫颈.Text = "未见异常";
            else
                this.textEdit宫颈.Text = "异常：" + _dr当前数据[tb_孕妇_产前随访1次.妇科宫颈异常].ToString();

            if (_dr当前数据[tb_孕妇_产前随访1次.妇科子宫] == null || _dr当前数据[tb_孕妇_产前随访1次.妇科子宫].ToString() == "1" || _dr当前数据[tb_孕妇_产前随访1次.妇科子宫].ToString() == "")//正常
                this.textEdit子宫.Text = "未见异常";
            else
                this.textEdit子宫.Text = "异常：" + _dr当前数据[tb_孕妇_产前随访1次.妇科子宫异常].ToString();

            if (_dr当前数据[tb_孕妇_产前随访1次.妇科附件] == null || _dr当前数据[tb_孕妇_产前随访1次.妇科附件].ToString() == "1" || _dr当前数据[tb_孕妇_产前随访1次.妇科附件].ToString() == "")//正常
                this.textEdit附件.Text = "未见异常";
            else
                this.textEdit附件.Text = "异常：" + _dr当前数据[tb_孕妇_产前随访1次.妇科附件异常].ToString();

            uc血红蛋白值.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.血红蛋白].ToString();
            uc白细胞计数值.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.白细胞计数].ToString();
            uc血小板计数值.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.血小板计数].ToString();
            uc血常规其他.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.血常规其他].ToString();
            uc尿蛋白.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.尿蛋白].ToString();
            uc尿糖.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.尿糖].ToString();
            uc尿酮体.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.尿酮体].ToString();
            uc尿潜血.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.尿潜血].ToString();
            uc尿常规其他.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.尿常规其他].ToString();

            textEdit血型.Text = _Bll.ReturnDis字典显示("xuexing", _dr当前数据[tb_孕妇_产前随访1次.血型].ToString());
            textEditRH.Text = _Bll.ReturnDis字典显示("rhxx", _dr当前数据[tb_孕妇_产前随访1次.RH].ToString());

            uc血糖.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.血糖].ToString();

            uc血清谷丙转氨酶.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.血清谷丙转氨酶].ToString();
            uc血清谷草转氨酶.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.血清谷草转氨酶].ToString();
            uc白蛋白.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.白蛋白].ToString();
            uc总胆红素.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.总胆红素].ToString();
            uc结合胆红素.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.结合胆红素].ToString();
            uc血清肌酐.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.血清肌酐].ToString();
            uc血尿素氮.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.血尿素氮].ToString();

            string 阴道分泌物 = _Bll.ReturnDis字典显示("ydfmw", _dr当前数据[tb_孕妇_产前随访1次.阴道分泌物].ToString());
            if (阴道分泌物 == "其他")
            {
                阴道分泌物 += "：" + _dr当前数据[tb_孕妇_产前随访1次.阴道分泌物其他].ToString();
            }
            textEdit阴道分泌物.Text = 阴道分泌物;
            textEdit阴道清洁度.Text = _Bll.ReturnDis字典显示("ydqjdzd", _dr当前数据[tb_孕妇_产前随访1次.阴道清洁度].ToString());

            uc乙型肝炎表面抗原.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.乙型肝炎表面抗原].ToString();
            uc乙型肝炎表面抗体.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.乙型肝炎表面抗体].ToString();
            uc乙型肝炎e抗原.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.乙型肝炎e抗原].ToString();
            uc乙型肝炎e抗体.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.乙型肝炎e抗体].ToString();
            uc乙型肝炎核心抗体.Txt1.Text = _dr当前数据[tb_孕妇_产前随访1次.乙型肝炎核心抗体].ToString();
            textEdit梅毒血清学试验.Text = _Bll.ReturnDis字典显示("yxyx", _dr当前数据[tb_孕妇_产前随访1次.梅毒血清学试验].ToString());
            textEditHIV抗体检测.Text = _Bll.ReturnDis字典显示("yxyx", _dr当前数据[tb_孕妇_产前随访1次.HIV抗体检测].ToString());

            textEditB超.Text = _dr当前数据[tb_孕妇_产前随访1次.B超].ToString();
            if (_dr当前数据[tb_孕妇_产前随访1次.总体评估] == null || _dr当前数据[tb_孕妇_产前随访1次.总体评估].ToString() == "1" || _dr当前数据[tb_孕妇_产前随访1次.总体评估].ToString() == "")//正常
                this.textEdit总体评估.Text = "未见异常";
            else
                this.textEdit总体评估.Text = "异常：" + _dr当前数据[tb_孕妇_产前随访1次.总体评估其他].ToString();

            text保健指导.Text = SetFlowLayoutValues("bjzhidao", _dr当前数据[tb_孕妇_产前随访1次.保健指导].ToString()) + "  " + _dr当前数据[tb_孕妇_产前随访1次.保健指导其他].ToString();

            if (_dr当前数据[tb_孕妇_产前随访1次.建册情况] == null || _dr当前数据[tb_孕妇_产前随访1次.建册情况].ToString() == "1" || _dr当前数据[tb_孕妇_产前随访1次.建册情况].ToString() == "")//本次随访同时建册
                this.txt建册情况.Text = "本次随访同时建册";
            else
                this.txt建册情况.Text = "已在其他机构建册：" + _dr当前数据[tb_孕妇_产前随访1次.其他建册机构名称].ToString();
            
            string 转诊 = string.Empty;
            if (_dr当前数据[tb_孕妇_产前随访1次.转诊] != null &&
                _dr当前数据[tb_孕妇_产前随访1次.转诊].ToString() == "1")//有
            {
                转诊 += "有" + Environment.NewLine;
                转诊 += "原因:" + _dr当前数据[tb_孕妇_产前随访1次.转诊原因].ToString()+"。" + Environment.NewLine;
                转诊 += "机构及科室:" + _dr当前数据[tb_孕妇_产前随访1次.转诊机构及科室].ToString()+"。" + Environment.NewLine;
                转诊 += "联系人:" + _dr当前数据[tb_孕妇_产前随访1次.联系人].ToString()+"  ；";
                转诊 += "联系方式:" + _dr当前数据[tb_孕妇_产前随访1次.联系方式].ToString() +"  ；";
                string str结果=_dr当前数据[tb_孕妇_产前随访1次.结果].ToString()+"。" ;
                if (string.IsNullOrEmpty(str结果) || str结果 == "1" )
                {
                    转诊 += "结果:" + "到位";
                }
                else
                {
                    转诊 += "结果:" + "未到位";
                }
                this.lbl转诊.Height = 50;
            }
            else
            {
                转诊 += "无";
            }
            textEdit转诊.Text = 转诊;

            dte下次随访日期.Text = _dr当前数据[tb_孕妇_产前随访1次.下次随访日期].ToString();
            textEdit随访医生.Text = _dr当前数据[tb_孕妇_产前随访1次.随访医师].ToString();
            txt家属签名.Text = _dr当前数据[tb_孕妇_产前随访1次.家属签名].ToString();
            textEdit创建时间.Text = _dr当前数据[tb_孕妇_产前随访1次.创建时间].ToString();
            textEdit最近更新时间.Text = _dr当前数据[tb_孕妇_产前随访1次.修改时间].ToString();
            textEdit当前所属机构.Text = _Bll.Return机构名称(_dr当前数据[tb_孕妇_产前随访1次.所属机构].ToString());
            textEdit创建机构.Text = _Bll.Return机构名称(_dr当前数据[tb_孕妇_产前随访1次.创建机构].ToString());
            textEdit创建人.Text = _Bll.Return用户名称(_dr当前数据[tb_孕妇_产前随访1次.创建人].ToString());
            textEdit最近修改人.Text = _Bll.Return用户名称(_dr当前数据[tb_孕妇_产前随访1次.修改人].ToString());

            this.emptySpaceItem1.Text = string.Format("考核项:29  缺项:{0}  完整度:{1}%", _dr当前数据[tb_孕妇_产前随访1次.缺项].ToString(), _dr当前数据[tb_孕妇_产前随访1次.完整度].ToString());

            SetControlItemColor();

        }

        private void SetControlItemColor()
        {
            if (string.IsNullOrEmpty(this.dte填表日期.Text.Trim()))
                this.lbl填表日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.uc填表孕周.Txt1.Text.Trim()) && string.IsNullOrEmpty(this.uc填表孕周.Txt2.Text.Trim()))
                this.lbl填表孕周.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit孕妇年龄.Text.Trim()))
                this.lbl孕妇年龄.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit丈夫姓名.Text.Trim()))
                this.lbl丈夫姓名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit丈夫年龄.Text.Trim()))
                this.lbl丈夫年龄.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit丈夫电话.Text.Trim()))
                this.lbl丈夫电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit孕次.Text.Trim()))
                this.lbl孕次.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.uc产次.Txt1.Text.Trim()) && string.IsNullOrEmpty(this.uc产次.Txt2.Text.Trim()))
                this.lbl产次.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.txt既往病史.Text.Trim()))
                this.lbl既往病史.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.txt家族史.Text.Trim()))
                this.lbl家族史.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.txt个人史.Text.Trim()))
                this.lbl个人史.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.txt妇科手术史.Text.Trim()))
                this.lbl妇科手术史.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.uc流产.Txt1.Text.Trim()) && string.IsNullOrEmpty(this.uc死胎.Txt1.Text.Trim()) && string.IsNullOrEmpty(this.uc死产.Txt1.Text.Trim()) && string.IsNullOrEmpty(this.uc新生儿死亡.Txt1.Text.Trim()) && string.IsNullOrEmpty(this.uc出生缺陷儿.Txt1.Text.Trim()))
                this.lbl孕产史.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.uc身高.Txt1.Text.Trim()))
                this.lbl身高.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.uc体重.Txt1.Text.Trim()))
                this.lbl体重.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;

            if (string.IsNullOrEmpty(this.uc血压.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc血压.Txt2.Text.Trim()))
                this.lbl血压.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;

            if (string.IsNullOrEmpty(this.textEdit心脏.Text.Trim()) || string.IsNullOrEmpty(this.textEdit肺部.Text.Trim()))
                this.lbl听诊.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;

            if (string.IsNullOrEmpty(this.textEdit外阴.Text.Trim()) || string.IsNullOrEmpty(this.textEdit阴道.Text.Trim()) || string.IsNullOrEmpty(this.textEdit宫颈.Text.Trim()) || string.IsNullOrEmpty(this.textEdit子宫.Text.Trim()) || string.IsNullOrEmpty(this.textEdit附件.Text.Trim()))
                this.lbl妇科检查.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.uc血红蛋白值.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc白细胞计数值.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc血小板计数值.Txt1.Text.Trim()))
                this.lbl血常规.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;

            if (string.IsNullOrEmpty(this.uc尿蛋白.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc尿糖.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc尿酮体.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc尿潜血.Txt1.Text.Trim()))
                this.lbl尿常规.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;

            if (string.IsNullOrEmpty(this.textEdit血型.Text.Trim()))
                this.lbl血型.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;

            if (string.IsNullOrEmpty(this.uc血清谷丙转氨酶.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc血清谷草转氨酶.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc白蛋白.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc总胆红素.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc结合胆红素.Txt1.Text.Trim()))
                this.lbl肝功能.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.uc血清肌酐.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc血尿素氮.Txt1.Text.Trim()))
                this.lbl肾功能.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;

            if (string.IsNullOrEmpty(this.uc乙型肝炎表面抗原.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc乙型肝炎表面抗体.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc乙型肝炎e抗原.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc乙型肝炎e抗体.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc乙型肝炎核心抗体.Txt1.Text.Trim()))
                this.lbl乙型肝炎五项.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;

            if (string.IsNullOrEmpty(this.textEdit总体评估.Text.Trim()))
                this.lbl总体评估.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.txt建册情况.Text.Trim()))
                this.lbl建册情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.text保健指导.Text.Trim()))
                this.lbl保健指导.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit转诊.Text.Trim()))
                this.lbl转诊.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit随访医生.Text.Trim()))
                this.lbl随访医生.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.dte下次随访日期.Text.Trim()))
                this.lbl下次随访日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
        }

        private string CalcBMI(string p1, string p2)
        {
            Decimal 身高 = 0;
            Decimal 体重 = 0;
            Decimal BMI = 0;
            if (string.IsNullOrEmpty(p1) || string.IsNullOrEmpty(p2)) return "";
            if (Decimal.TryParse(p1, out 身高) && Decimal.TryParse(p2, out 体重))
            {
                if (身高 != 0)
                {
                    BMI = Math.Round(体重 / ((身高 / 100) * (身高 / 100)), 2);
                }
                return BMI.ToString();
            }
            return "";
        }
        //change 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 ▽
        private void Bind丈夫基本信息(DataTable dt孕妇基本信息)
        {
            if (dt孕妇基本信息 == null) return;
            DataRow dryf = dt孕妇基本信息.Rows[0];
            if (string.IsNullOrEmpty(textEdit丈夫姓名.Text))
            {
                this.textEdit丈夫姓名.Text = dryf[tb_孕妇基本信息.丈夫姓名].ToString();
            }
            if (string.IsNullOrEmpty(textEdit丈夫年龄.Text))
            {
                this.textEdit丈夫年龄.Text = dryf[tb_孕妇基本信息.丈夫年龄].ToString();
            }
            if (string.IsNullOrEmpty(textEdit丈夫电话.Text))
            {
                this.textEdit丈夫电话.Text = dryf[tb_孕妇基本信息.丈夫电话].ToString();
            }
            //this.textEdit档案编号.Text = dt孕妇基本信息.Rows[0][tb_孕妇基本信息.个人档案编号].ToString();
            //this.textEdit孕妇姓名.Text = util.DESEncrypt.DES解密(dt孕妇基本信息.Rows[0][tb_孕妇基本信息.姓名].ToString());
            //this.textEdit出生日期.Text = dt孕妇基本信息.Rows[0][tb_孕妇基本信息.出生日期].ToString();
            //this.textEdit身份证号.Text = dt孕妇基本信息.Rows[0][tb_孕妇基本信息.身份证号].ToString();
            //this.textEdit居住状态.Text = _Bll.ReturnDis字典显示("jzzk", dt孕妇基本信息.Rows[0][tb_孕妇基本信息.居住状况].ToString());
            //this.textEdit居住地址.Text = _Bll.Return地区名称(dt孕妇基本信息.Rows[0][tb_孕妇基本信息.区].ToString()) + _Bll.Return地区名称(dt孕妇基本信息.Rows[0][tb_孕妇基本信息.街道].ToString()) + _Bll.Return地区名称(dt孕妇基本信息.Rows[0][tb_孕妇基本信息.居委会].ToString()) + dt孕妇基本信息.Rows[0][tb_孕妇基本信息.居住地址].ToString();

        }
        private void Bind基本信息(DataTable dt个人基本信息)
        {
            if (dt个人基本信息 == null) return;

            this.textEdit档案编号.Text = dt个人基本信息.Rows[0][tb_健康档案.个人档案编号].ToString();
            this.textEdit孕妇姓名.Text = util.DESEncrypt.DES解密(dt个人基本信息.Rows[0][tb_健康档案.姓名].ToString());
            this.textEdit出生日期.Text = dt个人基本信息.Rows[0][tb_健康档案.出生日期].ToString();
            this.textEdit身份证号.Text = dt个人基本信息.Rows[0][tb_健康档案.身份证号].ToString();
            this.textEdit居住状态.Text = _Bll.ReturnDis字典显示("jzzk", dt个人基本信息.Rows[0][tb_健康档案.常住类型].ToString());
            this.textEdit居住地址.Text = _Bll.Return地区名称(dt个人基本信息.Rows[0][tb_健康档案.区].ToString())
                + _Bll.Return地区名称(dt个人基本信息.Rows[0][tb_健康档案.街道].ToString())
                + _Bll.Return地区名称(dt个人基本信息.Rows[0][tb_健康档案.居委会].ToString())
                + dt个人基本信息.Rows[0][tb_健康档案.居住地址].ToString();
            this.txt本人电话.Text = dt个人基本信息.Rows[0][tb_健康档案.本人电话].ToString();
        }
        //change 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 △
        private void btn添加_Click(object sender, EventArgs e)
        {
            UC第1次产前随访服务记录表 uc = new UC第1次产前随访服务记录表(_frm, AtomEHR.Common.UpdateType.Add);
            ShowControl(uc, DockStyle.Fill);
        }
        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_孕妇_产前随访1次.所属机构].ToString()))
            {
                _id = _dr当前数据["ID"].ToString();
                _frm._param = _id;
                UC第1次产前随访服务记录表 uc = new UC第1次产前随访服务记录表(_frm, AtomEHR.Common.UpdateType.Modify);
                ShowControl(uc, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }
        private void btn删除_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_孕妇_产前随访1次.所属机构].ToString()))
            {
                if (Msg.AskQuestion("您确定删除本业务表单吗？"))
                {
                    if (_Bll.Delete(_id))
                    {
                        Msg.ShowInformation("删除记录成功！");
                        //删除成功后需要更新 字段
                        this.OnLoad(null);
                    }
                }
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }
        private void UC第1次产前随访服务记录表_显示_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < this.layoutControl1.Controls.Count; i++)
            {
                if (this.layoutControl1.Controls[i] is TextEdit)
                {
                    TextEdit txt = layoutControl1.Controls[i] as TextEdit;
                    txt.Properties.ReadOnly = true;
                    txt.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxt)
                {
                    UCLblTxt txt = layoutControl1.Controls[i] as UCLblTxt;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCTxtLbl)
                {
                    UCTxtLbl txt = layoutControl1.Controls[i] as UCTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxtLbl)
                {
                    UCLblTxtLbl txt = layoutControl1.Controls[i] as UCLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCTxtLblTxtLbl)
                {
                    UCTxtLblTxtLbl txt = layoutControl1.Controls[i] as UCTxtLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                    txt.Txt2.Properties.ReadOnly = true;
                    txt.Txt2.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxtLbl)
                {
                    UCLblTxtLbl txt = layoutControl1.Controls[i] as UCLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is FlowLayoutPanel)
                {
                    FlowLayoutPanel flow = layoutControl1.Controls[i] as FlowLayoutPanel;
                    for (int j = 0; j < flow.Controls.Count; j++)
                    {
                        if (flow.Controls[j] is TextEdit)
                        {
                            TextEdit txt = flow.Controls[j] as TextEdit;
                            txt.Properties.ReadOnly = true;
                            txt.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCLblTxt)
                        {
                            UCLblTxt txt = flow.Controls[j] as UCLblTxt;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCTxtLbl)
                        {
                            UCTxtLbl txt = flow.Controls[j] as UCTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCLblTxtLbl)
                        {
                            UCLblTxtLbl txt = flow.Controls[j] as UCLblTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCTxtLblTxtLbl)
                        {
                            UCTxtLblTxtLbl txt = flow.Controls[j] as UCTxtLblTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                            txt.Txt2.Properties.ReadOnly = true;
                            txt.Txt2.BackColor = System.Drawing.Color.Transparent;
                        }
                    }

                }
            }

            this.uc流产.Margin = new Padding(0);
            this.uc死胎.Margin = new Padding(0);
            this.uc死产.Margin = new Padding(0);
            this.uc新生儿死亡.Margin = new Padding(0);
            this.uc出生缺陷儿.Margin = new Padding(0);
            this.uc流产.Width = 90;
            this.uc死胎.Width = 90;
            this.uc死产.Width = 90;

            //根据个人档案编号 查询全部的  体检数据

            //changed 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 ▽
            //_ds孕妇产前一次随访记录 = _Bll.GetAllDataByKey(_docNo);
            _ds孕妇产前一次随访记录 = _Bll.GetAllDataByKeyNew(_docNo);
            //changed 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 △
            DoBindingDataSource(_ds孕妇产前一次随访记录);//绑定数据
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            string _docNo = this.textEdit档案编号.Text.Trim();
            if (!string.IsNullOrEmpty(_docNo))
            {
                report第1次产前随访服务记录表 report = new report第1次产前随访服务记录表(_docNo);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }
        }


        AtomEHR.Business.bll孕产妇随访化验单 bll孕产妇Image = new AtomEHR.Business.bll孕产妇随访化验单();
        private string _sfid = null;
        private void sbtn查看查体化验单_Click(object sender, EventArgs e)
        {
            FrmPIC frm = new FrmPIC();

            if (!string.IsNullOrWhiteSpace(_sfid))
            {
                DataSet dstemp = bll孕产妇Image.GetBusinessByDahAndPicid(_docNo, _sfid, false);

                byte[] arrbyte = dstemp.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICBYTE] as byte[];

                frm.SetImage(arrbyte);
                frm.SetReadOnly();
                frm.ShowDialog();
            }
            else
            {
                Msg.ShowInformation("未上传检查化验单图片");
            }
        }
    }
}
