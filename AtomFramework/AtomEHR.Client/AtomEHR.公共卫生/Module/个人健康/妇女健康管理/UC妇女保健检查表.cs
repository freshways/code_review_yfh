﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business.Security;
using AtomEHR.Common;
using AtomEHR.Models;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class UC妇女保健检查表 : UserControlBase
    {

        #region Fields
        AtomEHR.Business.bll妇女保健检查 _Bll = new Business.bll妇女保健检查();
        DataSet _ds妇女保健检查表;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        DataTable dt妇女基本信息;

        #endregion
        public UC妇女保健检查表()
        {
            InitializeComponent();
        }
        public UC妇女保健检查表(Form frm, AtomEHR.Common.UpdateType _updateType)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _serverDateTime = _Bll.ServiceDateTime;
            _UpdateType = _updateType;
            _id = _frm._param as string;
            if (_UpdateType == UpdateType.Add)//添加随访
            {
                _Bll.GetBusinessByKey(_docNo, true);
                //_Bll.NewBusiness();
                _ds妇女保健检查表 = _Bll.CurrentBusiness;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                _ds妇女保健检查表 = _Bll.GetOneDataByKey(_docNo, _id, true);

            }
            DoBindingSummaryEditor(_ds妇女保健检查表);//绑定数据
        }
        private void DoBindingSummaryEditor(DataSet _ds妇女保健检查表)
        {
            if (_ds妇女保健检查表 == null) return;
            if (_ds妇女保健检查表.Tables.Count == 0) return;
            dt妇女基本信息 = _ds妇女保健检查表.Tables[tb_妇女基本信息.__TableName];
            DataTable dt妇女保健检查 = _ds妇女保健检查表.Tables[tb_妇女保健检查.__TableName];
            if (dt妇女基本信息.Rows.Count == 1)
            {
                DataRow dr = dt妇女基本信息.Rows[0];
                this.txt姓名.Text = util.DESEncrypt.DES解密(dr[tb_妇女基本信息.姓名].ToString());
                this.txt档案编号.Text = dr[tb_妇女基本信息.个人档案编号].ToString();
                this.txt身份证号.Text = dr[tb_妇女基本信息.身份证号].ToString();
                this.txt出生日期.Text = dr[tb_妇女基本信息.出生日期].ToString();
                this.txt联系电话.Text = dr[tb_妇女基本信息.联系电话].ToString();
                this.txt居住地址.Text = _Bll.Return地区名称(dr[tb_妇女基本信息.市].ToString()).PadRight(2) + _Bll.Return地区名称(dr[tb_妇女基本信息.区].ToString()).PadRight(2) + _Bll.Return地区名称(dr[tb_妇女基本信息.街道].ToString()).PadRight(2) + _Bll.Return地区名称(dr[tb_妇女基本信息.居委会].ToString()).PadRight(2) + dr[tb_妇女基本信息.居住地址].ToString();

            }
            if (dt妇女保健检查.Rows.Count == 1)
            {
                BindSuiFangData(dt妇女保健检查.Rows[0]);
            }
            if (_UpdateType == UpdateType.Add)
            {
                this.txt创建人.Text = Loginer.CurrentUser.AccountName;
                this.txt创建时间.Text = _serverDateTime;
                this.txt最近更新时间.Text = _serverDateTime;
                this.txt当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
                this.txt创建机构.Text = Loginer.CurrentUser.所属机构名称;
                this.txt最近修改人.Text = Loginer.CurrentUser.AccountName;

            }
            else if (_UpdateType == UpdateType.Modify)
            {
                this.txt创建人.Text = _Bll.Return用户名称(dt妇女保健检查.Rows[0][tb_妇女保健检查.创建人].ToString());
                this.txt创建时间.Text = _serverDateTime;
                this.txt最近更新时间.Text = _serverDateTime;
                this.txt当前所属机构.Text = _Bll.Return机构名称(dt妇女保健检查.Rows[0][tb_妇女保健检查.所属机构].ToString());
                this.txt创建机构.Text = _Bll.Return机构名称(dt妇女保健检查.Rows[0][tb_妇女保健检查.创建机构].ToString());// Loginer.CurrentUser.所属机构名称;
                this.txt最近修改人.Text = Loginer.CurrentUser.AccountName; //Loginer.CurrentUser.AccountName;
            }
        }
        private void BindSuiFangData(DataRow dataRow)
        {
            if (dataRow == null) return;
            this.txt卡号.Text = dataRow[tb_妇女保健检查.卡号].ToString();
            //检查日期   修改的时候不能进行 修改，添加时可以进行修改
            this.dte检查日期.Properties.ReadOnly = _UpdateType == UpdateType.Modify ? true : false;
            this.dte检查日期.Text = dataRow[tb_妇女保健检查.检查日期].ToString();
            this.txt检查次数.Txt1.Text = dataRow[tb_妇女保健检查.检查次数].ToString();
            this.txt现有症状.Text = dataRow[tb_妇女保健检查.现有症状].ToString();
            this.txt曾患妇科病.Text = dataRow[tb_妇女保健检查.曾患妇科病].ToString();
            this.txt曾做手术.Text = dataRow[tb_妇女保健检查.曾做手术].ToString();

            this.txt初经年龄.Txt1.Text = dataRow[tb_妇女保健检查.初经年龄].ToString();
            this.txt持续天数.Txt1.Text = dataRow[tb_妇女保健检查.持续天数].ToString();
            this.txt间隔天数.Txt1.Text = dataRow[tb_妇女保健检查.间隔天数].ToString();
            this.txt绝经年龄.Txt1.Text = dataRow[tb_妇女保健检查.绝经年龄].ToString();

            this.txt痛经.Text = dataRow[tb_妇女保健检查.痛经].ToString();
            this.txt末次月经.Text = dataRow[tb_妇女保健检查.末次月经].ToString();

            if (dataRow[tb_妇女保健检查.结扎对象].ToString() == "" && dataRow[tb_妇女保健检查.结扎日期].ToString() == "" && dataRow[tb_妇女保健检查.结扎医院].ToString() == "")
                this.chk结扎.Checked = true;
            else
            {
                this.chk结扎.Checked = false;
                this.txt结扎对象.Text = dataRow[tb_妇女保健检查.结扎对象].ToString();
                this.txt结扎日期.Text = dataRow[tb_妇女保健检查.结扎日期].ToString();
                this.txt结扎医院.Text = dataRow[tb_妇女保健检查.结扎医院].ToString();
            }
            if (dataRow[tb_妇女保健检查.避孕工具].ToString() == "" && dataRow[tb_妇女保健检查.药物名称].ToString() == "")
                this.chk避孕.Checked = true;
            else
            {
                this.chk避孕.Checked = false;
                this.txt避孕工具.Text = dataRow[tb_妇女保健检查.避孕工具].ToString();
                this.txt药物名称.Text = dataRow[tb_妇女保健检查.药物名称].ToString();
            }

            this.chk未孕.Checked = dataRow[tb_妇女保健检查.未孕].ToString() == "on" ? false : true;
            this.txt孕次.Txt1.Text = dataRow[tb_妇女保健检查.孕次].ToString();
            this.txt产次.Txt1.Text = dataRow[tb_妇女保健检查.产次].ToString();
            this.txt自然流产.Txt1.Text = dataRow[tb_妇女保健检查.自然流产].ToString();
            this.txt人工流产.Txt1.Text = dataRow[tb_妇女保健检查.人工流产].ToString();
            this.txt中孕引产.Txt1.Text = dataRow[tb_妇女保健检查.中孕引产].ToString();


            if (dataRow[tb_妇女保健检查.不孕年数] != null && Convert.ToInt32(dataRow[tb_妇女保健检查.不孕年数]) != 0)
            {
                this.chk不孕史.Checked = false;
                this.txt不孕史时间.Txt1.Text = dataRow[tb_妇女保健检查.不孕年数].ToString();
                this.txt不孕史男.Text = dataRow[tb_妇女保健检查.男方原因].ToString();
                this.txt不孕史女.Text = dataRow[tb_妇女保健检查.女方原因].ToString();
            }
            else
            {
                this.chk不孕史.Checked = true;
            }

            this.txt血压.Txt1.Text = dataRow[tb_妇女保健检查.血压].ToString();
            this.txt血压.Txt2.Text = dataRow[tb_妇女保健检查.血压2].ToString();
            this.txt体重.Txt1.Text = dataRow[tb_妇女保健检查.体重].ToString();
            this.txt身高.Txt1.Text = dataRow[tb_妇女保健检查.身高].ToString();
            this.txt痛经.Text = dataRow[tb_妇女保健检查.痛经].ToString();

            this.txt乳房.Text = dataRow[tb_妇女保健检查.乳房].ToString();
            this.txt结节.Text = dataRow[tb_妇女保健检查.结节].ToString();
            this.txt压痛.Text = dataRow[tb_妇女保健检查.压痛].ToString();
            this.txt乳房左侧.Text = dataRow[tb_妇女保健检查.乳房左侧].ToString();
            this.txt乳房右侧.Text = dataRow[tb_妇女保健检查.乳房右侧].ToString();

            this.txt心.Text = dataRow[tb_妇女保健检查.心].ToString();
            this.txt肺.Text = dataRow[tb_妇女保健检查.肺].ToString();
            this.txt透胸.Text = dataRow[tb_妇女保健检查.透胸].ToString();

            SetFlowLayoutResult(dataRow[tb_妇女保健检查.外阴].ToString(), flow外阴);
            this.txt阴道.Text = dataRow[tb_妇女保健检查.阴道].ToString();
            this.txt白带.Text = dataRow[tb_妇女保健检查.白带].ToString();
            SetFlowLayoutResult(dataRow[tb_妇女保健检查.白带性质].ToString(), flow白带性质);
            SetFlowLayoutResult(dataRow[tb_妇女保健检查.子宫颈].ToString(), flow子宫颈);
            this.txt子宫体活动.Text = dataRow[tb_妇女保健检查.子宫体活动].ToString();
            this.txt子宫脱垂.Text = dataRow[tb_妇女保健检查.子宫脱垂].ToString();
            this.txt子宫体大小.Text = dataRow[tb_妇女保健检查.子宫体大小].ToString();
            this.txt左卵巢.Text = dataRow[tb_妇女保健检查.左卵巢].ToString();
            this.txt右卵巢.Text = dataRow[tb_妇女保健检查.右卵巢].ToString();
            this.txt子宫体活动.Text = dataRow[tb_妇女保健检查.子宫体活动].ToString();
            SetFlowLayoutResult(dataRow[tb_妇女保健检查.附件].ToString(), flow附件);
            this.txt阴道异常情况.Text = dataRow[tb_妇女保健检查.阴道异常情况].ToString();

            this.txt白带涂片清洁度.Text = dataRow[tb_妇女保健检查.白带涂片清洁度].ToString();
            this.txt酸碱度PH值.Text = dataRow[tb_妇女保健检查.酸碱度PH值].ToString();
            this.txt滴虫.Text = dataRow[tb_妇女保健检查.滴虫].ToString();
            this.txt霉菌.Text = dataRow[tb_妇女保健检查.霉菌].ToString();
            this.txt宫颈刮片.Txt1.Text = dataRow[tb_妇女保健检查.宫颈刮片].ToString();
            this.txt巴氏.Text = dataRow[tb_妇女保健检查.巴氏].ToString();
            this.txt化检结果.Text = dataRow[tb_妇女保健检查.化检结果].ToString();
            this.txt复查结果.Text = dataRow[tb_妇女保健检查.复查结果].ToString();

            this.txt印象.Text = dataRow[tb_妇女保健检查.印象].ToString();
            this.txt处理意见.Text = dataRow[tb_妇女保健检查.处理意见].ToString();
            this.txt治疗情况.Text = dataRow[tb_妇女保健检查.治疗情况].ToString();
            this.txt医师签名.Text = dataRow[tb_妇女保健检查.医师签名].ToString();

            this.txt创建人.Text = _Bll.Return用户名称(dataRow[tb_妇女保健检查.创建人].ToString());
            this.txt创建时间.Text = dataRow[tb_妇女保健检查.创建时间].ToString();
            this.txt最近更新时间.Text = dataRow[tb_妇女保健检查.修改时间].ToString();
            this.txt当前所属机构.Text = _Bll.Return机构名称(dataRow[tb_妇女保健检查.所属机构].ToString());
            this.txt创建机构.Text = _Bll.Return机构名称(dataRow[tb_妇女保健检查.创建机构].ToString());// Loginer.CurrentUser.所属机构名称;
            this.txt最近修改人.Text = _Bll.Return用户名称(dataRow[tb_妇女保健检查.修改人].ToString());

        }

        #region Handler Events
        private void UC妇女保健检查表_Load(object sender, EventArgs e)
        {
            //this.txt孕次.Txt1.Properties.ReadOnly = true;
            //this.txt产次.Txt1.Properties.ReadOnly = true;
            //this.txt自然流产.Txt1.Properties.ReadOnly = true;
            //this.txt人工流产.Txt1.Properties.ReadOnly = true;
            //this.txt中孕引产.Txt1.Properties.ReadOnly = true;
            //this.txt不孕史时间.Txt1.Properties.ReadOnly = true;

            this.txt检查次数.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt检查次数.Txt1.Properties.Mask.EditMask = "n0";
            this.txt检查次数.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt初经年龄.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt初经年龄.Txt1.Properties.Mask.EditMask = "###";
            this.txt初经年龄.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt持续天数.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt持续天数.Txt1.Properties.Mask.EditMask = "###";
            this.txt持续天数.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt间隔天数.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt间隔天数.Txt1.Properties.Mask.EditMask = "###";
            this.txt间隔天数.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt绝经年龄.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt绝经年龄.Txt1.Properties.Mask.EditMask = "###";
            this.txt绝经年龄.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt孕次.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt孕次.Txt1.Properties.Mask.EditMask = "###";
            this.txt孕次.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt产次.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt产次.Txt1.Properties.Mask.EditMask = "###";
            this.txt产次.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt自然流产.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt自然流产.Txt1.Properties.Mask.EditMask = "###";
            this.txt自然流产.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt人工流产.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt人工流产.Txt1.Properties.Mask.EditMask = "###";
            this.txt人工流产.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt中孕引产.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt中孕引产.Txt1.Properties.Mask.EditMask = "###";
            this.txt中孕引产.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt不孕史时间.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt不孕史时间.Txt1.Properties.Mask.EditMask = "###";
            this.txt不孕史时间.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.txt血压.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt血压.Txt1.Properties.Mask.EditMask = "###";
            this.txt血压.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt血压.Txt2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt血压.Txt2.Properties.Mask.EditMask = "###";
            this.txt血压.Txt2.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt体重.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt体重.Txt1.Properties.Mask.EditMask = "###.##";
            //this.txt体重.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt身高.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt身高.Txt1.Properties.Mask.EditMask = "###.##";
            //this.txt身高.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt左卵巢.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt左卵巢.Txt1.Properties.Mask.EditMask = "###.##";
            //this.txt左卵巢.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.txt右卵巢.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txt右卵巢.Txt1.Properties.Mask.EditMask = "###.##";
            //this.txt右卵巢.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
        }
        private void chk结扎_CheckedChanged(object sender, EventArgs e)
        {
            this.txt结扎对象.Properties.ReadOnly = this.txt结扎日期.Properties.ReadOnly = this.txt结扎医院.Properties.ReadOnly = this.chk结扎.Checked;
            if (this.chk结扎.Checked)
            {
                this.txt结扎对象.Text = this.txt结扎日期.Text = this.txt结扎医院.Text = "";
            }
        }
        private void chk避孕_CheckedChanged(object sender, EventArgs e)
        {
            this.txt避孕工具.Properties.ReadOnly = this.txt药物名称.Properties.ReadOnly = this.chk避孕.Checked;
            if (this.chk避孕.Checked)
            {
                this.txt避孕工具.Text = this.txt药物名称.Text = "";
            }
        }
        private void chk未孕_CheckedChanged(object sender, EventArgs e)
        {
            this.txt孕次.Txt1.Properties.ReadOnly = this.txt产次.Txt1.Properties.ReadOnly = this.txt自然流产.Txt1.Properties.ReadOnly = this.txt人工流产.Txt1.Properties.ReadOnly = this.txt中孕引产.Txt1.Properties.ReadOnly = this.chk未孕.Checked;
            if (this.chk未孕.Checked)
            {
                this.txt孕次.Txt1.Text = this.txt产次.Txt1.Text = this.txt自然流产.Txt1.Text = this.txt人工流产.Txt1.Text = this.txt中孕引产.Txt1.Text = "";
            }
        }
        private void chk不孕史_CheckedChanged(object sender, EventArgs e)
        {
            this.txt不孕史时间.Txt1.Properties.ReadOnly = this.txt不孕史男.Properties.ReadOnly = this.txt不孕史女.Properties.ReadOnly = this.chk不孕史.Checked;
            if (this.chk不孕史.Checked)
            {
                this.txt不孕史时间.Txt1.Text = this.txt不孕史男.Text = this.txt不孕史女.Text = "";
            }
        }
        private void chk白带性质_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk白带性质.Checked)
            {
                this.chk白带_白.Properties.ReadOnly = this.chk白带_臭味.Properties.ReadOnly = this.chk白带_黄色.Properties.ReadOnly = this.chk白带_侬性.Properties.ReadOnly = this.chk白带_泡沫.Properties.ReadOnly = this.chk白带_水样.Properties.ReadOnly = this.chk白带_血样.Properties.ReadOnly = this.chk白带_渣样.Properties.ReadOnly = true;

                this.chk白带_白.Checked = this.chk白带_臭味.Checked = this.chk白带_黄色.Checked = this.chk白带_侬性.Checked = this.chk白带_泡沫.Checked = this.chk白带_水样.Checked = this.chk白带_血样.Checked = this.chk白带_渣样.Checked = false;
            }
            else
            {
                this.chk白带_白.Properties.ReadOnly = this.chk白带_臭味.Properties.ReadOnly = this.chk白带_黄色.Properties.ReadOnly = this.chk白带_侬性.Properties.ReadOnly = this.chk白带_泡沫.Properties.ReadOnly = this.chk白带_水样.Properties.ReadOnly = this.chk白带_血样.Properties.ReadOnly = this.chk白带_渣样.Properties.ReadOnly = false;
            }

        }
        private void checkEdit21_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkEdit21.Checked)
            {
                this.checkEdit22.Properties.ReadOnly = this.checkEdit23.Properties.ReadOnly = this.checkEdit24.Properties.ReadOnly = this.checkEdit25.Properties.ReadOnly = this.checkEdit26.Properties.ReadOnly = this.checkEdit27.Properties.ReadOnly = this.checkEdit28.Properties.ReadOnly = this.checkEdit29.Properties.ReadOnly = this.checkEdit30.Properties.ReadOnly = true;

                this.checkEdit22.Checked = this.checkEdit23.Checked = this.checkEdit24.Checked = this.checkEdit25.Checked = this.checkEdit26.Checked = this.checkEdit27.Checked = this.checkEdit28.Checked = this.checkEdit29.Checked = this.checkEdit30.Checked = false;
            }
            else
            {
                this.checkEdit22.Properties.ReadOnly = this.checkEdit23.Properties.ReadOnly = this.checkEdit24.Properties.ReadOnly = this.checkEdit25.Properties.ReadOnly = this.checkEdit26.Properties.ReadOnly = this.checkEdit27.Properties.ReadOnly = this.checkEdit28.Properties.ReadOnly = this.checkEdit29.Properties.ReadOnly = this.checkEdit30.Properties.ReadOnly = false;
            }
        }
        private void checkEdit35_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkEdit35.Checked)
            {
                this.checkEdit31.Properties.ReadOnly = this.checkEdit32.Properties.ReadOnly = this.checkEdit33.Properties.ReadOnly = this.checkEdit34.Properties.ReadOnly = true;

                this.checkEdit31.Checked = this.checkEdit32.Checked = this.checkEdit33.Checked = this.checkEdit34.Checked = false;
            }
            else
            {
                this.checkEdit31.Properties.ReadOnly = this.checkEdit32.Properties.ReadOnly = this.checkEdit33.Properties.ReadOnly = this.checkEdit34.Properties.ReadOnly = false;
            }
        }
        /// <summary>
        /// 外阴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkEdit5_CheckedChanged(object sender, EventArgs e)
        {
            this.checkEdit6.Enabled = this.checkEdit7.Enabled = this.checkEdit8.Enabled = this.checkEdit9.Enabled = !this.checkEdit5.Checked;
            if (this.checkEdit5.Checked)
            {
                this.checkEdit6.Checked = this.checkEdit7.Checked = this.checkEdit8.Checked = this.checkEdit9.Checked = false;
            }
        }
        private void btn保存_Click(object sender, EventArgs e)
        {
            if (Check验证())
            {
                if (_UpdateType == UpdateType.Add)//添加
                {
                    if (_Bll.CheckNoExistsByDate(_docNo, this.dte检查日期.Text))
                    {
                        Msg.Warning("已存在此检查时间，同一检查时间内不能进行多次随访，请确认！");
                        this.dte检查日期.Text = "";
                        return;
                    }
                    if (Msg.AskQuestion("信息保存后，'检查日期'将不允许修改，请确认表单信息!"))
                    {
                        var xy1 = this.txt血压.Txt1.Text.Trim();
                        var xy2 = this.txt血压.Txt2.Text.Trim();
                        if (xy1 != "" && xy2 != "")
                        {
                            int _xy1 = Convert.ToInt32(xy1);
                            int _xy2 = Convert.ToInt32(xy2);
                            if (_xy1 >= 140 || _xy2 >= 90)//符合高血压条件
                            {
                                AtomEHR.Business.bllMXB高血压管理卡 bll高血压 = new Business.bllMXB高血压管理卡();
                                if (!bll高血压.CheckNoExists(_docNo))//不存在高血压管理卡
                                {
                                    if (Msg.AskQuestion("该居民的收缩压>=140mmHg或舒张压>=90mmHg，建议到医院确诊是否为高血压患者，若确诊为高血压患者，请点击[确定]，系统将自动纳入高血压患者管理，否则点击[取消]！"))
                                    {
                                        #region 保存 tb_MXB慢病基础信息表

                                        if (_ds妇女保健检查表.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Count == 0)
                                        {
                                            DataRow row = _ds妇女保健检查表.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Add();
                                            //row[tb_MXB慢病基础信息表.户主关系] = ControlsHelper.GetComboxKey(cbo与户主关系);
                                            row[tb_MXB慢病基础信息表.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                                            row[tb_MXB慢病基础信息表.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                                            row[tb_MXB慢病基础信息表.身份证号] = dt妇女基本信息.Rows[0][tb_妇女基本信息.身份证号];
                                            row[tb_MXB慢病基础信息表.出生日期] = dt妇女基本信息.Rows[0][tb_妇女基本信息.出生日期];
                                            row[tb_MXB慢病基础信息表.民族] = dt妇女基本信息.Rows[0][tb_妇女基本信息.民族];
                                            row[tb_MXB慢病基础信息表.文化程度] = dt妇女基本信息.Rows[0][tb_妇女基本信息.文化程度];
                                            row[tb_MXB慢病基础信息表.常住类型] = dt妇女基本信息.Rows[0][tb_妇女基本信息.常住类型];
                                            row[tb_MXB慢病基础信息表.工作单位] = dt妇女基本信息.Rows[0][tb_妇女基本信息.工作单位];
                                            row[tb_MXB慢病基础信息表.婚姻状况] = dt妇女基本信息.Rows[0][tb_妇女基本信息.婚姻状况];
                                            row[tb_MXB慢病基础信息表.职业] = dt妇女基本信息.Rows[0][tb_妇女基本信息.职业];
                                            row[tb_MXB慢病基础信息表.联系人电话] = dt妇女基本信息.Rows[0][tb_妇女基本信息.联系电话];
                                            row[tb_MXB慢病基础信息表.省] = "37";
                                            row[tb_MXB慢病基础信息表.性别] = dt妇女基本信息.Rows[0][tb_妇女基本信息.性别];
                                            row[tb_MXB慢病基础信息表.市] = dt妇女基本信息.Rows[0][tb_妇女基本信息.市];
                                            row[tb_MXB慢病基础信息表.区] = dt妇女基本信息.Rows[0][tb_妇女基本信息.区];
                                            row[tb_MXB慢病基础信息表.街道] = dt妇女基本信息.Rows[0][tb_妇女基本信息.街道];
                                            row[tb_MXB慢病基础信息表.居委会] = dt妇女基本信息.Rows[0][tb_妇女基本信息.居委会];
                                            row[tb_MXB慢病基础信息表.所属片区] = dt妇女基本信息.Rows[0][tb_妇女基本信息.所属片区];
                                            row[tb_MXB慢病基础信息表.居住地址] = dt妇女基本信息.Rows[0][tb_妇女基本信息.居住地址];
                                            row[tb_MXB慢病基础信息表.医疗费用支付方式] = dt妇女基本信息.Rows[0][tb_妇女基本信息.医疗费用支付方式];
                                            //row[tb_MXB慢病基础信息表.医疗费用支付类型其他] = this.txt医疗费用支付方式_其他.Text.Trim();
                                            row[tb_MXB慢病基础信息表.医疗保险号] = dt妇女基本信息.Rows[0][tb_妇女基本信息.医疗费用支付方式];
                                            row[tb_MXB慢病基础信息表.新农合号] = dt妇女基本信息.Rows[0][tb_妇女基本信息.医疗费用支付方式];
                                            row[tb_MXB慢病基础信息表.建档机构] = Loginer.CurrentUser.所属机构;
                                            row[tb_MXB慢病基础信息表.建档时间] = _serverDateTime;
                                            row[tb_MXB慢病基础信息表.建档人] = Loginer.CurrentUser.用户编码;
                                            row[tb_MXB慢病基础信息表.创建机构] = Loginer.CurrentUser.所属机构;
                                            row[tb_MXB慢病基础信息表.创建时间] = _serverDateTime;
                                            row[tb_MXB慢病基础信息表.创建人] = Loginer.CurrentUser.用户编码;
                                            row[tb_MXB慢病基础信息表.更新时间] = _serverDateTime;
                                            row[tb_MXB慢病基础信息表.更新人] = Loginer.CurrentUser.用户编码;
                                            row[tb_MXB慢病基础信息表.HAPPENTIME] = _serverDateTime;
                                            row[tb_MXB慢病基础信息表.所属机构] = Loginer.CurrentUser.所属机构;
                                            row[tb_MXB慢病基础信息表.高血压随访时间] = "1900-01-01";
                                            row[tb_MXB慢病基础信息表.糖尿病随访时间] = "1900-01-01";
                                            row[tb_MXB慢病基础信息表.脑卒中随访时间] = "1900-01-01";
                                            row[tb_MXB慢病基础信息表.冠心病随访时间] = "1900-01-01";
                                            row[tb_MXB慢病基础信息表.家庭档案编号] = _frm._familyDocNo;
                                        }
                                        #endregion

                                        #region 操作既往史

                                        DataTable dt = _ds妇女保健检查表.Tables[tb_健康档案_既往病史.__TableName];
                                        bool existsGXY = false;
                                        if (dt != null && dt.Rows.Count > 0)
                                        {
                                            for (int y = 0; y < dt.Rows.Count; y++)
                                            {
                                                if (dt.Rows[y][tb_健康档案_既往病史.疾病名称].ToString().IndexOf('2') != -1)
                                                    existsGXY = true;
                                            }
                                        }
                                        if (!existsGXY)//不存在高血压
                                        {
                                            DataRow row = _ds妇女保健检查表.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
                                            row[tb_健康档案_既往病史.个人档案编号] = _docNo;
                                            row[tb_健康档案_既往病史.疾病类型] = "疾病";
                                            row[tb_健康档案_既往病史.疾病名称] = "2";
                                            row[tb_健康档案_既往病史.D_JBBM] = "高血压";
                                        }

                                        #endregion

                                        #region 保存高血压管理卡表
                                        DataRow rowGXYGLK = _ds妇女保健检查表.Tables[tb_MXB高血压管理卡.__TableName].Rows.Add();
                                        AddNewGXYGLK(rowGXYGLK);
                                        #endregion
                                    }
                                }
                            }
                            else
                            {
                                if ((_xy1 >= 130 && _xy1 < 140) || (_xy2 < 90 && _xy2 >= 85))//临界高血压
                                {
                                    _ds妇女保健检查表.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "2";
                                }
                            }


                        }
                        else
                        {
                            string shengao = this.txt身高.Txt1.Text.Trim();
                            string tizhong = this.txt体重.Txt1.Text.Trim();
                            decimal bmi = 0m;
                            if (!string.IsNullOrEmpty(shengao) && !string.IsNullOrEmpty(tizhong))
                            {
                                bmi = CalcBMI(Convert.ToDecimal(shengao), Convert.ToDecimal(tizhong));
                            }
                            if (bmi != 0m && bmi > 28.0m)//肥胖
                            {
                                _ds妇女保健检查表.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否肥胖] = "1";
                            }

                            #region 保存 妇女检查表
                            DataRow row妇女保健检查表 = _ds妇女保健检查表.Tables[tb_妇女保健检查.__TableName].Rows.Add();
                            row妇女保健检查表[tb_妇女保健检查.卡号] = "";
                            row妇女保健检查表[tb_妇女保健检查.个人档案编号] = _docNo;
                            row妇女保健检查表[tb_妇女保健检查.检查日期] = this.dte检查日期.DateTime.ToString("yyyy-MM-dd");
                            row妇女保健检查表[tb_妇女保健检查.检查次数] = this.txt检查次数.Txt1.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.现有症状] = this.txt现有症状.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.曾患妇科病] = this.txt曾患妇科病.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.曾做手术] = this.txt曾做手术.Text.Trim();
                            if (!string.IsNullOrEmpty(this.txt初经年龄.Txt1.Text.Trim()))
                            {
                                row妇女保健检查表[tb_妇女保健检查.初经年龄] = Convert.ToInt32(this.txt初经年龄.Txt1.Text.Trim());
                            }
                            if (!string.IsNullOrEmpty(this.txt持续天数.Txt1.Text.Trim()))
                            {
                                row妇女保健检查表[tb_妇女保健检查.持续天数] = Convert.ToInt32(this.txt持续天数.Txt1.Text.Trim());
                            }
                            if (!string.IsNullOrEmpty(this.txt间隔天数.Txt1.Text.Trim()))
                            {
                                row妇女保健检查表[tb_妇女保健检查.间隔天数] = Convert.ToInt32(this.txt间隔天数.Txt1.Text.Trim());
                            }
                            if (!string.IsNullOrEmpty(this.txt绝经年龄.Txt1.Text.Trim()))
                            {
                                row妇女保健检查表[tb_妇女保健检查.绝经年龄] = Convert.ToInt32(this.txt绝经年龄.Txt1.Text.Trim());
                            }
                            row妇女保健检查表[tb_妇女保健检查.痛经] = this.txt痛经.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.末次月经] = this.txt末次月经.Text.Trim();
                            if (!this.chk结扎.Checked)
                            {
                                row妇女保健检查表[tb_妇女保健检查.结扎对象] = this.txt结扎对象.Text.Trim();
                                row妇女保健检查表[tb_妇女保健检查.结扎日期] = this.txt结扎日期.Text.Trim();
                                row妇女保健检查表[tb_妇女保健检查.结扎医院] = this.txt结扎医院.Text.Trim();
                            }
                            if (!this.chk避孕.Checked)
                            {
                                row妇女保健检查表[tb_妇女保健检查.避孕工具] = this.txt避孕工具.Text.Trim();
                                row妇女保健检查表[tb_妇女保健检查.药物名称] = this.txt药物名称.Text.Trim();
                            }
                            if (!this.chk未孕.Checked)
                            {
                                if (!string.IsNullOrEmpty(this.txt孕次.Txt1.Text.Trim()))
                                {
                                    row妇女保健检查表[tb_妇女保健检查.孕次] = Convert.ToInt32(this.txt孕次.Txt1.Text.Trim());
                                }
                                if (!string.IsNullOrEmpty(this.txt产次.Txt1.Text.Trim()))
                                {
                                    row妇女保健检查表[tb_妇女保健检查.产次] = Convert.ToInt32(this.txt产次.Txt1.Text.Trim());
                                }
                                if (!string.IsNullOrEmpty(this.txt自然流产.Txt1.Text.Trim()))
                                {
                                    row妇女保健检查表[tb_妇女保健检查.自然流产] = Convert.ToInt32(this.txt自然流产.Txt1.Text.Trim());
                                }
                                if (!string.IsNullOrEmpty(this.txt人工流产.Txt1.Text.Trim()))
                                {
                                    row妇女保健检查表[tb_妇女保健检查.人工流产] = Convert.ToInt32(this.txt人工流产.Txt1.Text.Trim());
                                }
                                if (!string.IsNullOrEmpty(this.txt中孕引产.Txt1.Text.Trim()))
                                {
                                    row妇女保健检查表[tb_妇女保健检查.中孕引产] = Convert.ToInt32(this.txt中孕引产.Txt1.Text.Trim());
                                }
                            }
                            else
                            {
                                row妇女保健检查表[tb_妇女保健检查.未孕] = "on";
                            }
                            if (!this.chk不孕史.Checked)
                            {
                                if (!string.IsNullOrEmpty(this.txt不孕史时间.Txt1.Text.Trim()))
                                {
                                    row妇女保健检查表[tb_妇女保健检查.不孕年数] = Convert.ToInt32(this.txt不孕史时间.Txt1.Text.Trim());
                                }
                                row妇女保健检查表[tb_妇女保健检查.男方原因] = this.txt不孕史男.Text.Trim();
                                row妇女保健检查表[tb_妇女保健检查.女方原因] = this.txt不孕史女.Text.Trim();
                            }
                            row妇女保健检查表[tb_妇女保健检查.血压] = this.txt血压.Txt1.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.血压2] = this.txt血压.Txt2.Text.Trim();
                            if (!string.IsNullOrEmpty(this.txt体重.Txt1.Text.Trim()))
                            {
                                row妇女保健检查表[tb_妇女保健检查.体重] = Convert.ToDecimal(this.txt体重.Txt1.Text.Trim());
                            }
                            if (!string.IsNullOrEmpty(this.txt身高.Txt1.Text.Trim()))
                            {
                                row妇女保健检查表[tb_妇女保健检查.身高] = Convert.ToDecimal(this.txt身高.Txt1.Text.Trim());
                            }
                            row妇女保健检查表[tb_妇女保健检查.乳房] = this.txt乳房.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.结节] = this.txt结节.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.压痛] = this.txt压痛.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.乳房左侧] = this.txt乳房左侧.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.乳房右侧] = this.txt乳房右侧.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.心] = this.txt心.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.肺] = this.txt肺.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.透胸] = this.txt透胸.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.外阴] = GetFlowLayoutResult(this.flow外阴);
                            row妇女保健检查表[tb_妇女保健检查.阴道] = this.txt阴道.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.白带] = this.txt白带.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.白带性质] = GetFlowLayoutResult(this.flow白带性质);
                            row妇女保健检查表[tb_妇女保健检查.子宫颈] = GetFlowLayoutResult(this.flow子宫颈);
                            row妇女保健检查表[tb_妇女保健检查.子宫体活动] = this.txt子宫体活动.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.子宫脱垂] = this.txt子宫脱垂.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.子宫体大小] = this.txt子宫体大小.Text.Trim();
                            if (!string.IsNullOrEmpty(this.txt左卵巢.Txt1.Text.Trim()))
                            {
                                row妇女保健检查表[tb_妇女保健检查.左卵巢] = Convert.ToDecimal(this.txt左卵巢.Txt1.Text.Trim());
                            }
                            if (!string.IsNullOrEmpty(this.txt右卵巢.Txt1.Text.Trim()))
                            {
                                row妇女保健检查表[tb_妇女保健检查.右卵巢] = Convert.ToDecimal(this.txt右卵巢.Txt1.Text.Trim());
                            }
                            row妇女保健检查表[tb_妇女保健检查.附件] = GetFlowLayoutResult(this.flow附件);

                            row妇女保健检查表[tb_妇女保健检查.阴道异常情况] = this.txt阴道异常情况.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.白带涂片清洁度] = this.txt白带涂片清洁度.Text.Trim();
                            if (!string.IsNullOrEmpty(this.txt酸碱度PH值.Text.Trim()))
                            {
                                row妇女保健检查表[tb_妇女保健检查.酸碱度PH值] = Convert.ToDecimal(this.txt酸碱度PH值.Text.Trim());
                            }
                            row妇女保健检查表[tb_妇女保健检查.滴虫] = this.txt滴虫.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.霉菌] = this.txt霉菌.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.宫颈刮片] = this.txt宫颈刮片.Txt1.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.巴氏] = this.txt巴氏.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.化检结果] = this.txt化检结果.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.复查结果] = this.txt复查结果.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.印象] = this.txt印象.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.处理意见] = this.txt处理意见.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.治疗情况] = this.txt治疗情况.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.医师签名] = this.txt医师签名.Text.Trim();

                            row妇女保健检查表[tb_妇女保健检查.创建时间] = this.txt创建时间.Text;
                            row妇女保健检查表[tb_妇女保健检查.修改时间] = this.txt最近更新时间.Text.Trim();
                            row妇女保健检查表[tb_妇女保健检查.所属机构] = Loginer.CurrentUser.所属机构;
                            row妇女保健检查表[tb_妇女保健检查.创建机构] = Loginer.CurrentUser.所属机构;
                            row妇女保健检查表[tb_妇女保健检查.创建人] = Loginer.CurrentUser.用户编码;
                            row妇女保健检查表[tb_妇女保健检查.修改人] = Loginer.CurrentUser.用户编码;
                            int quexiang = Get缺项();
                            row妇女保健检查表[tb_妇女保健检查.缺项] = quexiang;
                            row妇女保健检查表[tb_妇女保健检查.完整度] = Ger完整度(quexiang);
                            #endregion
                        }
                    }
                    else return;
                }
                else//修改
                {
                    var xy1 = this.txt血压.Txt1.Text.Trim();
                    var xy2 = this.txt血压.Txt2.Text.Trim();
                    if (xy1 != "" && xy2 != "")
                    {
                        int _xy1 = Convert.ToInt32(xy1);
                        int _xy2 = Convert.ToInt32(xy2);
                        if (_xy1 >= 140 || _xy2 >= 90)//符合高血压条件
                        {
                            AtomEHR.Business.bllMXB高血压管理卡 bll高血压 = new Business.bllMXB高血压管理卡();
                            if (!bll高血压.CheckNoExists(_docNo))//不存在高血压管理卡
                            {
                                if (Msg.AskQuestion("该居民的收缩压>=140mmHg或舒张压>=90mmHg，建议到医院确诊是否为高血压患者，若确诊为高血压患者，请点击[确定]，系统将自动纳入高血压患者管理，否则点击[取消]！"))
                                {
                                    #region 保存 tb_MXB慢病基础信息表

                                    if (_ds妇女保健检查表.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Count == 0)
                                    {
                                        DataRow row = _ds妇女保健检查表.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Add();
                                        //row[tb_MXB慢病基础信息表.户主关系] = ControlsHelper.GetComboxKey(cbo与户主关系);
                                        row[tb_MXB慢病基础信息表.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                                        row[tb_MXB慢病基础信息表.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                                        row[tb_MXB慢病基础信息表.身份证号] = dt妇女基本信息.Rows[0][tb_妇女基本信息.身份证号];
                                        row[tb_MXB慢病基础信息表.出生日期] = dt妇女基本信息.Rows[0][tb_妇女基本信息.出生日期];
                                        row[tb_MXB慢病基础信息表.民族] = dt妇女基本信息.Rows[0][tb_妇女基本信息.民族];
                                        row[tb_MXB慢病基础信息表.文化程度] = dt妇女基本信息.Rows[0][tb_妇女基本信息.文化程度];
                                        row[tb_MXB慢病基础信息表.常住类型] = dt妇女基本信息.Rows[0][tb_妇女基本信息.常住类型];
                                        row[tb_MXB慢病基础信息表.工作单位] = dt妇女基本信息.Rows[0][tb_妇女基本信息.工作单位];
                                        row[tb_MXB慢病基础信息表.婚姻状况] = dt妇女基本信息.Rows[0][tb_妇女基本信息.婚姻状况];
                                        row[tb_MXB慢病基础信息表.职业] = dt妇女基本信息.Rows[0][tb_妇女基本信息.职业];
                                        row[tb_MXB慢病基础信息表.联系人电话] = dt妇女基本信息.Rows[0][tb_妇女基本信息.联系电话];
                                        row[tb_MXB慢病基础信息表.省] = "37";
                                        row[tb_MXB慢病基础信息表.性别] = dt妇女基本信息.Rows[0][tb_妇女基本信息.性别];
                                        row[tb_MXB慢病基础信息表.市] = dt妇女基本信息.Rows[0][tb_妇女基本信息.市];
                                        row[tb_MXB慢病基础信息表.区] = dt妇女基本信息.Rows[0][tb_妇女基本信息.区];
                                        row[tb_MXB慢病基础信息表.街道] = dt妇女基本信息.Rows[0][tb_妇女基本信息.街道];
                                        row[tb_MXB慢病基础信息表.居委会] = dt妇女基本信息.Rows[0][tb_妇女基本信息.居委会];
                                        row[tb_MXB慢病基础信息表.所属片区] = dt妇女基本信息.Rows[0][tb_妇女基本信息.所属片区];
                                        row[tb_MXB慢病基础信息表.居住地址] = dt妇女基本信息.Rows[0][tb_妇女基本信息.居住地址];
                                        row[tb_MXB慢病基础信息表.医疗费用支付方式] = dt妇女基本信息.Rows[0][tb_妇女基本信息.医疗费用支付方式];
                                        //row[tb_MXB慢病基础信息表.医疗费用支付类型其他] = this.txt医疗费用支付方式_其他.Text.Trim();
                                        row[tb_MXB慢病基础信息表.医疗保险号] = dt妇女基本信息.Rows[0][tb_妇女基本信息.医疗费用支付方式];
                                        row[tb_MXB慢病基础信息表.新农合号] = dt妇女基本信息.Rows[0][tb_妇女基本信息.医疗费用支付方式];
                                        row[tb_MXB慢病基础信息表.建档机构] = Loginer.CurrentUser.所属机构;
                                        row[tb_MXB慢病基础信息表.建档时间] = _serverDateTime;
                                        row[tb_MXB慢病基础信息表.建档人] = Loginer.CurrentUser.用户编码;
                                        row[tb_MXB慢病基础信息表.创建机构] = Loginer.CurrentUser.所属机构;
                                        row[tb_MXB慢病基础信息表.创建时间] = _serverDateTime;
                                        row[tb_MXB慢病基础信息表.创建人] = Loginer.CurrentUser.用户编码;
                                        row[tb_MXB慢病基础信息表.更新时间] = _serverDateTime;
                                        row[tb_MXB慢病基础信息表.更新人] = Loginer.CurrentUser.用户编码;
                                        row[tb_MXB慢病基础信息表.HAPPENTIME] = _serverDateTime;
                                        row[tb_MXB慢病基础信息表.所属机构] = Loginer.CurrentUser.所属机构;
                                        row[tb_MXB慢病基础信息表.高血压随访时间] = "1900-01-01";
                                        row[tb_MXB慢病基础信息表.糖尿病随访时间] = "1900-01-01";
                                        row[tb_MXB慢病基础信息表.脑卒中随访时间] = "1900-01-01";
                                        row[tb_MXB慢病基础信息表.冠心病随访时间] = "1900-01-01";
                                        row[tb_MXB慢病基础信息表.家庭档案编号] = _frm._familyDocNo;
                                    }
                                    #endregion

                                    #region 操作既往史

                                    DataTable dt = _ds妇女保健检查表.Tables[tb_健康档案_既往病史.__TableName];
                                    bool existsGXY = false;
                                    if (dt != null && dt.Rows.Count > 0)
                                    {
                                        for (int y = 0; y < dt.Rows.Count; y++)
                                        {
                                            if (dt.Rows[y][tb_健康档案_既往病史.疾病名称].ToString().IndexOf('2') != -1)
                                                existsGXY = true;
                                        }
                                    }
                                    if (!existsGXY)//不存在高血压
                                    {
                                        DataRow row = _ds妇女保健检查表.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
                                        row[tb_健康档案_既往病史.个人档案编号] = _docNo;
                                        row[tb_健康档案_既往病史.疾病类型] = "疾病";
                                        row[tb_健康档案_既往病史.疾病名称] = "2";
                                        row[tb_健康档案_既往病史.D_JBBM] = "高血压";
                                    }

                                    #endregion

                                    #region 保存高血压管理卡表
                                    DataRow rowGXYGLK = _ds妇女保健检查表.Tables[tb_MXB高血压管理卡.__TableName].Rows.Add();
                                    AddNewGXYGLK(rowGXYGLK);
                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            if ((_xy1 >= 130 && _xy1 < 140) || (_xy2 < 90 && _xy2 >= 85))//临界高血压
                            {
                                _ds妇女保健检查表.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "2";
                            }
                        }
                    }

                    string shengao = this.txt身高.Txt1.Text.Trim();
                    string tizhong = this.txt体重.Txt1.Text.Trim();
                    decimal bmi = 0m;
                    if (!string.IsNullOrEmpty(shengao) && !string.IsNullOrEmpty(tizhong))
                    {
                        bmi = CalcBMI(Convert.ToDecimal(shengao), Convert.ToDecimal(tizhong));
                    }
                    if (bmi != 0m && bmi > 28.0m)//肥胖
                    {
                        _ds妇女保健检查表.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否肥胖] = "1";
                    }

                    #region 保存 妇女检查表
                    DataRow row妇女保健检查表 = _ds妇女保健检查表.Tables[tb_妇女保健检查.__TableName].Rows[0];
                    row妇女保健检查表[tb_妇女保健检查.卡号] = "";
                    row妇女保健检查表[tb_妇女保健检查.个人档案编号] = _docNo;
                    row妇女保健检查表[tb_妇女保健检查.检查日期] = this.dte检查日期.DateTime.ToString("yyyy-MM-dd");
                    if (!string.IsNullOrEmpty(this.txt检查次数.Txt1.Text.Trim()))
                    {
                        row妇女保健检查表[tb_妇女保健检查.检查次数] = Convert.ToInt32(this.txt检查次数.Txt1.Text.Trim());
                    }
                    row妇女保健检查表[tb_妇女保健检查.现有症状] = this.txt现有症状.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.曾患妇科病] = this.txt曾患妇科病.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.曾做手术] = this.txt曾做手术.Text.Trim();
                    if (!string.IsNullOrEmpty(this.txt初经年龄.Txt1.Text.Trim()))
                    {
                        row妇女保健检查表[tb_妇女保健检查.初经年龄] = Convert.ToInt32(this.txt初经年龄.Txt1.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(this.txt持续天数.Txt1.Text.Trim()))
                    {
                        row妇女保健检查表[tb_妇女保健检查.持续天数] = Convert.ToInt32(this.txt持续天数.Txt1.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(this.txt间隔天数.Txt1.Text.Trim()))
                    {
                        row妇女保健检查表[tb_妇女保健检查.间隔天数] = Convert.ToInt32(this.txt间隔天数.Txt1.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(this.txt绝经年龄.Txt1.Text.Trim()))
                    {
                        row妇女保健检查表[tb_妇女保健检查.绝经年龄] = Convert.ToInt32(this.txt绝经年龄.Txt1.Text.Trim());
                    }
                    row妇女保健检查表[tb_妇女保健检查.痛经] = this.txt痛经.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.末次月经] = this.txt末次月经.Text.Trim();
                    if (!this.chk结扎.Checked)
                    {
                        row妇女保健检查表[tb_妇女保健检查.结扎对象] = this.txt结扎对象.Text.Trim();
                        row妇女保健检查表[tb_妇女保健检查.结扎日期] = this.txt结扎日期.Text.Trim();
                        row妇女保健检查表[tb_妇女保健检查.结扎医院] = this.txt结扎医院.Text.Trim();
                    }
                    if (!this.chk避孕.Checked)
                    {
                        row妇女保健检查表[tb_妇女保健检查.避孕工具] = this.txt避孕工具.Text.Trim();
                        row妇女保健检查表[tb_妇女保健检查.药物名称] = this.txt药物名称.Text.Trim();
                    }
                    if (!this.chk未孕.Checked)
                    {
                        if (!string.IsNullOrEmpty(this.txt孕次.Txt1.Text.Trim()))
                        {
                            row妇女保健检查表[tb_妇女保健检查.孕次] = Convert.ToInt32(this.txt孕次.Txt1.Text.Trim());
                        }
                        if (!string.IsNullOrEmpty(this.txt产次.Txt1.Text.Trim()))
                        {
                            row妇女保健检查表[tb_妇女保健检查.产次] = Convert.ToInt32(this.txt产次.Txt1.Text.Trim());
                        }
                        if (!string.IsNullOrEmpty(this.txt自然流产.Txt1.Text.Trim()))
                        {
                            row妇女保健检查表[tb_妇女保健检查.自然流产] = Convert.ToInt32(this.txt自然流产.Txt1.Text.Trim());
                        }
                        if (!string.IsNullOrEmpty(this.txt人工流产.Txt1.Text.Trim()))
                        {
                            row妇女保健检查表[tb_妇女保健检查.人工流产] = Convert.ToInt32(this.txt人工流产.Txt1.Text.Trim());
                        }
                        if (!string.IsNullOrEmpty(this.txt中孕引产.Txt1.Text.Trim()))
                        {
                            row妇女保健检查表[tb_妇女保健检查.中孕引产] = Convert.ToInt32(this.txt中孕引产.Txt1.Text.Trim());
                        }
                    }
                    else
                    {
                        row妇女保健检查表[tb_妇女保健检查.未孕] = "on";
                    }
                    if (!this.chk不孕史.Checked)
                    {
                        if (!string.IsNullOrEmpty(this.txt不孕史时间.Txt1.Text.Trim()))
                        {
                            row妇女保健检查表[tb_妇女保健检查.不孕年数] = Convert.ToInt32(this.txt不孕史时间.Txt1.Text.Trim());
                        }
                        row妇女保健检查表[tb_妇女保健检查.男方原因] = this.txt不孕史男.Text.Trim();
                        row妇女保健检查表[tb_妇女保健检查.女方原因] = this.txt不孕史女.Text.Trim();
                    }
                    row妇女保健检查表[tb_妇女保健检查.血压] = this.txt血压.Txt1.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.血压2] = this.txt血压.Txt2.Text.Trim();
                    if (!string.IsNullOrEmpty(this.txt体重.Txt1.Text.Trim()))
                    {
                        row妇女保健检查表[tb_妇女保健检查.体重] = Convert.ToDecimal(this.txt体重.Txt1.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(this.txt身高.Txt1.Text.Trim()))
                    {
                        row妇女保健检查表[tb_妇女保健检查.身高] = Convert.ToDecimal(this.txt身高.Txt1.Text.Trim());
                    }
                    row妇女保健检查表[tb_妇女保健检查.乳房] = this.txt乳房.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.结节] = this.txt结节.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.压痛] = this.txt压痛.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.乳房左侧] = this.txt乳房左侧.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.乳房右侧] = this.txt乳房右侧.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.心] = this.txt心.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.肺] = this.txt肺.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.透胸] = this.txt透胸.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.外阴] = GetFlowLayoutResult(this.flow外阴);
                    row妇女保健检查表[tb_妇女保健检查.阴道] = this.txt阴道.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.白带] = this.txt白带.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.白带性质] = GetFlowLayoutResult(this.flow白带性质);
                    row妇女保健检查表[tb_妇女保健检查.子宫颈] = GetFlowLayoutResult(this.flow子宫颈);
                    row妇女保健检查表[tb_妇女保健检查.子宫体活动] = this.txt子宫体活动.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.子宫脱垂] = this.txt子宫脱垂.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.子宫体大小] = this.txt子宫体大小.Text.Trim();
                    if (!string.IsNullOrEmpty(this.txt左卵巢.Txt1.Text.Trim()))
                    {
                        row妇女保健检查表[tb_妇女保健检查.左卵巢] = Convert.ToDecimal(this.txt左卵巢.Txt1.Text.Trim());
                    }
                    if (!string.IsNullOrEmpty(this.txt右卵巢.Txt1.Text.Trim()))
                    {
                        row妇女保健检查表[tb_妇女保健检查.右卵巢] = Convert.ToDecimal(this.txt右卵巢.Txt1.Text.Trim());
                    }

                    row妇女保健检查表[tb_妇女保健检查.附件] = GetFlowLayoutResult(this.flow附件);

                    row妇女保健检查表[tb_妇女保健检查.阴道异常情况] = this.txt阴道异常情况.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.白带涂片清洁度] = this.txt白带涂片清洁度.Text.Trim();
                    if (!string.IsNullOrEmpty(this.txt酸碱度PH值.Text.Trim()))
                    {
                        row妇女保健检查表[tb_妇女保健检查.酸碱度PH值] = Convert.ToDecimal(this.txt酸碱度PH值.Text.Trim());
                    }
                    row妇女保健检查表[tb_妇女保健检查.滴虫] = this.txt滴虫.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.霉菌] = this.txt霉菌.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.宫颈刮片] = this.txt宫颈刮片.Txt1.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.巴氏] = this.txt阴道.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.化检结果] = this.txt化检结果.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.复查结果] = this.txt复查结果.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.印象] = this.txt印象.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.处理意见] = this.txt处理意见.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.治疗情况] = this.txt治疗情况.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.医师签名] = this.txt医师签名.Text.Trim();

                    row妇女保健检查表[tb_妇女保健检查.修改时间] = this.txt最近更新时间.Text.Trim();
                    row妇女保健检查表[tb_妇女保健检查.所属机构] = Loginer.CurrentUser.所属机构;
                    row妇女保健检查表[tb_妇女保健检查.修改人] = Loginer.CurrentUser.用户编码;
                    int quexiang = Get缺项();
                    row妇女保健检查表[tb_妇女保健检查.缺项] = quexiang;
                    row妇女保健检查表[tb_妇女保健检查.完整度] = Ger完整度(quexiang);
                    #endregion
                }
                //_Bll.WriteLog();//TODO: 日志
                SaveResult result = _Bll.Save(_ds妇女保健检查表);
                if (result.Success)
                {
                    if (_UpdateType == UpdateType.Add)
                    {
                        _frm._param = "";
                    }
                    Msg.ShowInformation("保存数据成功！");
                    UC妇女保健检查表_显示 uc = new UC妇女保健检查表_显示(_frm);
                    ShowControl(uc);
                }
            }
        }
        #endregion

        #region Private Methods
        private int Get缺项()
        {
            return 1;
        }
        private int Ger完整度(int i)
        {
            int k = Convert.ToInt32((12 - i) * 100 / 12.0);
            return k;
        }
        private bool Check验证()
        {
            if (string.IsNullOrEmpty(this.dte检查日期.Text.Trim()))
            {
                Msg.Warning("检查日期为必填项，请填写！");
                this.dte检查日期.Focus();
                return false;
            }
            return true;
        }
        private void AddNewGXYGLK(DataRow rowGXYGLK)
        {
            rowGXYGLK[tb_MXB高血压管理卡.管理卡编号] = "";
            rowGXYGLK[tb_MXB高血压管理卡.个人档案编号] = _docNo;
            rowGXYGLK[tb_MXB高血压管理卡.所属机构] = dt妇女基本信息.Rows[0][tb_妇女基本信息.所属机构];
            rowGXYGLK[tb_MXB高血压管理卡.创建机构] = dt妇女基本信息.Rows[0][tb_妇女基本信息.创建机构];
            rowGXYGLK[tb_MXB高血压管理卡.发生时间] = this.dte检查日期.DateTime.ToString("yyyy-MM-dd");// string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
            rowGXYGLK[tb_MXB高血压管理卡.创建时间] = dt妇女基本信息.Rows[0][tb_妇女基本信息.创建时间];// this.dte录入时间.Text.Trim();
            rowGXYGLK[tb_MXB高血压管理卡.创建人] = dt妇女基本信息.Rows[0][tb_妇女基本信息.创建人];// Loginer.CurrentUser.用户编码;
            rowGXYGLK[tb_MXB高血压管理卡.修改人] = dt妇女基本信息.Rows[0][tb_妇女基本信息.修改人];// this.dte录入时间.Text.Trim();
            rowGXYGLK[tb_MXB高血压管理卡.修改时间] = dt妇女基本信息.Rows[0][tb_妇女基本信息.修改时间];// this.dte录入时间.Text.Trim();
            rowGXYGLK[tb_MXB高血压管理卡.终止管理] = dt妇女基本信息.Rows[0][tb_妇女基本信息.档案状态];
            rowGXYGLK[tb_MXB高血压管理卡.缺项] = "20";
            rowGXYGLK[tb_MXB高血压管理卡.完整度] = "0";

            _ds妇女保健检查表.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压管理卡] = "20,0";
            _ds妇女保健检查表.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
            _ds妇女保健检查表.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "1";
        }
        /// <summary>
        /// 计算BMI
        /// </summary>
        /// <param name="shengao">身高</param>
        /// <param name="tizhong">体重</param>
        /// <returns></returns>
        private decimal CalcBMI(decimal shengao, decimal tizhong)
        {
            if (shengao == 0m || tizhong == 0m) return 0m;
            return Math.Round(tizhong / ((shengao / 100) * (shengao / 100)), 2);
        }
        #endregion
    }
}
