﻿namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    partial class UC产后访视记录表_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC产后访视记录表_显示));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn添加 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txt失访原因 = new DevExpress.XtraEditors.TextEdit();
            this.checkEdit失访 = new DevExpress.XtraEditors.CheckEdit();
            this.txt居民或家属签名 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.date出院日期 = new DevExpress.XtraEditors.DateEdit();
            this.date分娩日期 = new DevExpress.XtraEditors.DateEdit();
            this.lbl考核项 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit分类 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit伤口 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit乳房 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit子宫 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit恶漏 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit转诊 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit其他 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit一般健康情况 = new DevExpress.XtraEditors.TextEdit();
            this.uc产次 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit一般心理状况 = new DevExpress.XtraEditors.TextEdit();
            this.uc血压 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.uc体温 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit联系电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住状态 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt卡号 = new DevExpress.XtraEditors.TextEdit();
            this.dte随访日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit下次随访日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit指导 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl下次随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl随访医生 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl体温 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl一般健康情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl一般心理状况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血压 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl其他 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl产次 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl指导 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl转诊 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl恶漏 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl子宫 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl乳房 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl伤口 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl分类 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl考核 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl分娩日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl出院日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl居民或家属签名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt失访原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit失访.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民或家属签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date出院日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date出院日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date分娩日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.date分娩日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit分类.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit伤口.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit乳房.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit子宫.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit恶漏.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit一般健康情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit一般心理状况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl下次随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访医生)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体温)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl一般健康情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl一般心理状况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl产次)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl指导)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl转诊)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl恶漏)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl子宫)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl乳房)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl伤口)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl分类)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl考核)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl分娩日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl出院日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl居民或家属签名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Size = new System.Drawing.Size(104, 441);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(104, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(692, 32);
            this.panelControl1.TabIndex = 6;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn添加);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(688, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn添加
            // 
            this.btn添加.Image = ((System.Drawing.Image)(resources.GetObject("btn添加.Image")));
            this.btn添加.Location = new System.Drawing.Point(3, 3);
            this.btn添加.Name = "btn添加";
            this.btn添加.Size = new System.Drawing.Size(75, 23);
            this.btn添加.TabIndex = 0;
            this.btn添加.Text = "添加";
            this.btn添加.Click += new System.EventHandler(this.btn添加_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(84, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 1;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(165, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 2;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(246, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 3;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt失访原因);
            this.layoutControl1.Controls.Add(this.checkEdit失访);
            this.layoutControl1.Controls.Add(this.txt居民或家属签名);
            this.layoutControl1.Controls.Add(this.layoutControl2);
            this.layoutControl1.Controls.Add(this.date出院日期);
            this.layoutControl1.Controls.Add(this.date分娩日期);
            this.layoutControl1.Controls.Add(this.lbl考核项);
            this.layoutControl1.Controls.Add(this.textEdit分类);
            this.layoutControl1.Controls.Add(this.textEdit伤口);
            this.layoutControl1.Controls.Add(this.textEdit乳房);
            this.layoutControl1.Controls.Add(this.textEdit子宫);
            this.layoutControl1.Controls.Add(this.textEdit恶漏);
            this.layoutControl1.Controls.Add(this.textEdit转诊);
            this.layoutControl1.Controls.Add(this.textEdit其他);
            this.layoutControl1.Controls.Add(this.textEdit一般健康情况);
            this.layoutControl1.Controls.Add(this.uc产次);
            this.layoutControl1.Controls.Add(this.textEdit一般心理状况);
            this.layoutControl1.Controls.Add(this.uc血压);
            this.layoutControl1.Controls.Add(this.uc体温);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit创建人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近更新时间);
            this.layoutControl1.Controls.Add(this.textEdit创建时间);
            this.layoutControl1.Controls.Add(this.textEdit联系电话);
            this.layoutControl1.Controls.Add(this.textEdit居住地址);
            this.layoutControl1.Controls.Add(this.textEdit居住状态);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit姓名);
            this.layoutControl1.Controls.Add(this.textEdit档案编号);
            this.layoutControl1.Controls.Add(this.txt卡号);
            this.layoutControl1.Controls.Add(this.dte随访日期);
            this.layoutControl1.Controls.Add(this.textEdit下次随访日期);
            this.layoutControl1.Controls.Add(this.textEdit指导);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(104, 32);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(692, 409);
            this.layoutControl1.TabIndex = 7;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txt失访原因
            // 
            this.txt失访原因.Location = new System.Drawing.Point(497, 127);
            this.txt失访原因.Name = "txt失访原因";
            this.txt失访原因.Size = new System.Drawing.Size(175, 20);
            this.txt失访原因.StyleController = this.layoutControl1;
            this.txt失访原因.TabIndex = 105;
            // 
            // checkEdit失访
            // 
            this.checkEdit失访.Location = new System.Drawing.Point(344, 127);
            this.checkEdit失访.Name = "checkEdit失访";
            this.checkEdit失访.Properties.Caption = "失访";
            this.checkEdit失访.Size = new System.Drawing.Size(98, 19);
            this.checkEdit失访.StyleController = this.layoutControl1;
            this.checkEdit失访.TabIndex = 104;
            // 
            // txt居民或家属签名
            // 
            this.txt居民或家属签名.Location = new System.Drawing.Point(496, 479);
            this.txt居民或家属签名.Name = "txt居民或家属签名";
            this.txt居民或家属签名.Size = new System.Drawing.Size(176, 20);
            this.txt居民或家属签名.StyleController = this.layoutControl1;
            this.txt居民或家属签名.TabIndex = 103;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Location = new System.Drawing.Point(533, 151);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.Root = this.Root;
            this.layoutControl2.Size = new System.Drawing.Size(139, 20);
            this.layoutControl2.TabIndex = 102;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // Root
            // 
            this.Root.CustomizationFormText = "Root";
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Location = new System.Drawing.Point(0, 0);
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(139, 20);
            this.Root.Text = "Root";
            this.Root.TextVisible = false;
            // 
            // date出院日期
            // 
            this.date出院日期.EditValue = null;
            this.date出院日期.Location = new System.Drawing.Point(344, 151);
            this.date出院日期.Name = "date出院日期";
            this.date出院日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date出院日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date出院日期.Size = new System.Drawing.Size(185, 20);
            this.date出院日期.StyleController = this.layoutControl1;
            this.date出院日期.TabIndex = 101;
            // 
            // date分娩日期
            // 
            this.date分娩日期.EditValue = null;
            this.date分娩日期.Location = new System.Drawing.Point(98, 151);
            this.date分娩日期.Name = "date分娩日期";
            this.date分娩日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date分娩日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.date分娩日期.Size = new System.Drawing.Size(157, 20);
            this.date分娩日期.StyleController = this.layoutControl1;
            this.date分娩日期.TabIndex = 100;
            // 
            // lbl考核项
            // 
            this.lbl考核项.Location = new System.Drawing.Point(3, 30);
            this.lbl考核项.Name = "lbl考核项";
            this.lbl考核项.Size = new System.Drawing.Size(182, 21);
            this.lbl考核项.StyleController = this.layoutControl1;
            this.lbl考核项.TabIndex = 99;
            this.lbl考核项.Text = "考核项:19  缺项:0  完整度:100% ";
            // 
            // textEdit分类
            // 
            this.textEdit分类.Location = new System.Drawing.Point(98, 391);
            this.textEdit分类.Name = "textEdit分类";
            this.textEdit分类.Size = new System.Drawing.Size(460, 20);
            this.textEdit分类.StyleController = this.layoutControl1;
            this.textEdit分类.TabIndex = 98;
            // 
            // textEdit伤口
            // 
            this.textEdit伤口.Location = new System.Drawing.Point(98, 343);
            this.textEdit伤口.Name = "textEdit伤口";
            this.textEdit伤口.Size = new System.Drawing.Size(460, 20);
            this.textEdit伤口.StyleController = this.layoutControl1;
            this.textEdit伤口.TabIndex = 97;
            // 
            // textEdit乳房
            // 
            this.textEdit乳房.Location = new System.Drawing.Point(98, 271);
            this.textEdit乳房.Name = "textEdit乳房";
            this.textEdit乳房.Size = new System.Drawing.Size(460, 20);
            this.textEdit乳房.StyleController = this.layoutControl1;
            this.textEdit乳房.TabIndex = 96;
            // 
            // textEdit子宫
            // 
            this.textEdit子宫.Location = new System.Drawing.Point(98, 319);
            this.textEdit子宫.Name = "textEdit子宫";
            this.textEdit子宫.Size = new System.Drawing.Size(460, 20);
            this.textEdit子宫.StyleController = this.layoutControl1;
            this.textEdit子宫.TabIndex = 95;
            // 
            // textEdit恶漏
            // 
            this.textEdit恶漏.Location = new System.Drawing.Point(98, 295);
            this.textEdit恶漏.Name = "textEdit恶漏";
            this.textEdit恶漏.Size = new System.Drawing.Size(460, 20);
            this.textEdit恶漏.StyleController = this.layoutControl1;
            this.textEdit恶漏.TabIndex = 94;
            // 
            // textEdit转诊
            // 
            this.textEdit转诊.Location = new System.Drawing.Point(98, 455);
            this.textEdit转诊.Name = "textEdit转诊";
            this.textEdit转诊.Size = new System.Drawing.Size(526, 20);
            this.textEdit转诊.StyleController = this.layoutControl1;
            this.textEdit转诊.TabIndex = 92;
            // 
            // textEdit其他
            // 
            this.textEdit其他.Location = new System.Drawing.Point(98, 367);
            this.textEdit其他.Name = "textEdit其他";
            this.textEdit其他.Size = new System.Drawing.Size(460, 20);
            this.textEdit其他.StyleController = this.layoutControl1;
            this.textEdit其他.TabIndex = 89;
            // 
            // textEdit一般健康情况
            // 
            this.textEdit一般健康情况.Location = new System.Drawing.Point(98, 199);
            this.textEdit一般健康情况.Name = "textEdit一般健康情况";
            this.textEdit一般健康情况.Size = new System.Drawing.Size(460, 20);
            this.textEdit一般健康情况.StyleController = this.layoutControl1;
            this.textEdit一般健康情况.TabIndex = 82;
            // 
            // uc产次
            // 
            this.uc产次.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc产次.Lbl1Text = "次";
            this.uc产次.Location = new System.Drawing.Point(424, 247);
            this.uc产次.Name = "uc产次";
            this.uc产次.Size = new System.Drawing.Size(248, 20);
            this.uc产次.TabIndex = 80;
            this.uc产次.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // textEdit一般心理状况
            // 
            this.textEdit一般心理状况.Location = new System.Drawing.Point(98, 223);
            this.textEdit一般心理状况.Name = "textEdit一般心理状况";
            this.textEdit一般心理状况.Size = new System.Drawing.Size(460, 20);
            this.textEdit一般心理状况.StyleController = this.layoutControl1;
            this.textEdit一般心理状况.TabIndex = 78;
            // 
            // uc血压
            // 
            this.uc血压.Lbl1Size = new System.Drawing.Size(10, 14);
            this.uc血压.Lbl1Text = "/";
            this.uc血压.Lbl2Size = new System.Drawing.Size(40, 14);
            this.uc血压.Lbl2Text = "mmHg";
            this.uc血压.Location = new System.Drawing.Point(98, 247);
            this.uc血压.Name = "uc血压";
            this.uc血压.Size = new System.Drawing.Size(237, 20);
            this.uc血压.TabIndex = 49;
            this.uc血压.Txt1EditValue = null;
            this.uc血压.Txt1Size = new System.Drawing.Size(70, 20);
            this.uc血压.Txt2EditValue = null;
            this.uc血压.Txt2Size = new System.Drawing.Size(70, 20);
            // 
            // uc体温
            // 
            this.uc体温.Lbl1Size = new System.Drawing.Size(20, 18);
            this.uc体温.Lbl1Text = "℃ ";
            this.uc体温.Location = new System.Drawing.Point(98, 175);
            this.uc体温.Name = "uc体温";
            this.uc体温.Size = new System.Drawing.Size(574, 20);
            this.uc体温.TabIndex = 47;
            this.uc体温.Txt1Size = new System.Drawing.Size(60, 20);
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(542, 528);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit最近修改人.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(130, 20);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 33;
            // 
            // textEdit创建人
            // 
            this.textEdit创建人.Location = new System.Drawing.Point(321, 528);
            this.textEdit创建人.Name = "textEdit创建人";
            this.textEdit创建人.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建人.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建人.Properties.ReadOnly = true;
            this.textEdit创建人.Size = new System.Drawing.Size(122, 20);
            this.textEdit创建人.StyleController = this.layoutControl1;
            this.textEdit创建人.TabIndex = 32;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(98, 528);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建机构.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(134, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 31;
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(542, 504);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit当前所属机构.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(130, 20);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 30;
            // 
            // textEdit最近更新时间
            // 
            this.textEdit最近更新时间.Location = new System.Drawing.Point(321, 504);
            this.textEdit最近更新时间.Name = "textEdit最近更新时间";
            this.textEdit最近更新时间.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit最近更新时间.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit最近更新时间.Properties.ReadOnly = true;
            this.textEdit最近更新时间.Size = new System.Drawing.Size(122, 20);
            this.textEdit最近更新时间.StyleController = this.layoutControl1;
            this.textEdit最近更新时间.TabIndex = 29;
            // 
            // textEdit创建时间
            // 
            this.textEdit创建时间.Location = new System.Drawing.Point(98, 504);
            this.textEdit创建时间.Name = "textEdit创建时间";
            this.textEdit创建时间.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit创建时间.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.textEdit创建时间.Properties.ReadOnly = true;
            this.textEdit创建时间.Size = new System.Drawing.Size(134, 20);
            this.textEdit创建时间.StyleController = this.layoutControl1;
            this.textEdit创建时间.TabIndex = 28;
            // 
            // textEdit联系电话
            // 
            this.textEdit联系电话.Location = new System.Drawing.Point(577, 79);
            this.textEdit联系电话.Name = "textEdit联系电话";
            this.textEdit联系电话.Properties.ReadOnly = true;
            this.textEdit联系电话.Size = new System.Drawing.Size(95, 20);
            this.textEdit联系电话.StyleController = this.layoutControl1;
            this.textEdit联系电话.TabIndex = 25;
            // 
            // textEdit居住地址
            // 
            this.textEdit居住地址.Location = new System.Drawing.Point(344, 103);
            this.textEdit居住地址.Name = "textEdit居住地址";
            this.textEdit居住地址.Properties.ReadOnly = true;
            this.textEdit居住地址.Size = new System.Drawing.Size(328, 20);
            this.textEdit居住地址.StyleController = this.layoutControl1;
            this.textEdit居住地址.TabIndex = 15;
            // 
            // textEdit居住状态
            // 
            this.textEdit居住状态.Location = new System.Drawing.Point(98, 103);
            this.textEdit居住状态.Name = "textEdit居住状态";
            this.textEdit居住状态.Properties.ReadOnly = true;
            this.textEdit居住状态.Size = new System.Drawing.Size(157, 20);
            this.textEdit居住状态.StyleController = this.layoutControl1;
            this.textEdit居住状态.TabIndex = 14;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(287, 479);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Size = new System.Drawing.Size(110, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 11;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(344, 79);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(144, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 8;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(98, 79);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(157, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 7;
            // 
            // textEdit姓名
            // 
            this.textEdit姓名.Location = new System.Drawing.Point(577, 55);
            this.textEdit姓名.Name = "textEdit姓名";
            this.textEdit姓名.Properties.ReadOnly = true;
            this.textEdit姓名.Size = new System.Drawing.Size(95, 20);
            this.textEdit姓名.StyleController = this.layoutControl1;
            this.textEdit姓名.TabIndex = 6;
            // 
            // textEdit档案编号
            // 
            this.textEdit档案编号.Location = new System.Drawing.Point(344, 55);
            this.textEdit档案编号.Name = "textEdit档案编号";
            this.textEdit档案编号.Properties.ReadOnly = true;
            this.textEdit档案编号.Size = new System.Drawing.Size(144, 20);
            this.textEdit档案编号.StyleController = this.layoutControl1;
            this.textEdit档案编号.TabIndex = 5;
            // 
            // txt卡号
            // 
            this.txt卡号.Location = new System.Drawing.Point(98, 55);
            this.txt卡号.Name = "txt卡号";
            this.txt卡号.Size = new System.Drawing.Size(157, 20);
            this.txt卡号.StyleController = this.layoutControl1;
            this.txt卡号.TabIndex = 4;
            // 
            // dte随访日期
            // 
            this.dte随访日期.Location = new System.Drawing.Point(98, 127);
            this.dte随访日期.MinimumSize = new System.Drawing.Size(155, 0);
            this.dte随访日期.Name = "dte随访日期";
            this.dte随访日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte随访日期.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.dte随访日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte随访日期.Size = new System.Drawing.Size(157, 20);
            this.dte随访日期.StyleController = this.layoutControl1;
            this.dte随访日期.TabIndex = 34;
            // 
            // textEdit下次随访日期
            // 
            this.textEdit下次随访日期.Location = new System.Drawing.Point(98, 479);
            this.textEdit下次随访日期.Name = "textEdit下次随访日期";
            this.textEdit下次随访日期.Size = new System.Drawing.Size(100, 20);
            this.textEdit下次随访日期.StyleController = this.layoutControl1;
            this.textEdit下次随访日期.TabIndex = 10;
            // 
            // textEdit指导
            // 
            this.textEdit指导.Location = new System.Drawing.Point(98, 415);
            this.textEdit指导.Name = "textEdit指导";
            this.textEdit指导.Size = new System.Drawing.Size(460, 36);
            this.textEdit指导.StyleController = this.layoutControl1;
            this.textEdit指导.TabIndex = 90;
            this.textEdit指导.UseOptimizedRendering = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.lbl下次随访日期,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.lbl随访医生,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.layoutControlItem12,
            this.lbl随访日期,
            this.layoutControlItem11,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem22,
            this.lbl体温,
            this.lbl一般健康情况,
            this.lbl一般心理状况,
            this.lbl血压,
            this.lbl其他,
            this.lbl产次,
            this.lbl指导,
            this.lbl转诊,
            this.lbl恶漏,
            this.lbl子宫,
            this.lbl乳房,
            this.lbl伤口,
            this.lbl分类,
            this.lbl考核,
            this.lbl分娩日期,
            this.lbl出院日期,
            this.layoutControlItem9,
            this.lbl居民或家属签名,
            this.layoutControlItem6,
            this.layoutControlItem7});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(675, 551);
            this.layoutControlGroup1.Text = "产后访视记录表";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt卡号;
            this.layoutControlItem1.CustomizationFormText = "卡 号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 25);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(256, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "卡 号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // lbl下次随访日期
            // 
            this.lbl下次随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl下次随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl下次随访日期.Control = this.textEdit下次随访日期;
            this.lbl下次随访日期.CustomizationFormText = "产后休养地";
            this.lbl下次随访日期.Location = new System.Drawing.Point(0, 449);
            this.lbl下次随访日期.MaxSize = new System.Drawing.Size(250, 0);
            this.lbl下次随访日期.MinSize = new System.Drawing.Size(50, 25);
            this.lbl下次随访日期.Name = "lbl下次随访日期";
            this.lbl下次随访日期.Size = new System.Drawing.Size(199, 25);
            this.lbl下次随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl下次随访日期.Tag = "check";
            this.lbl下次随访日期.Text = "下次随访日期";
            this.lbl下次随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl下次随访日期.TextSize = new System.Drawing.Size(90, 14);
            this.lbl下次随访日期.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit档案编号;
            this.layoutControlItem2.CustomizationFormText = "档案编号 ";
            this.layoutControlItem2.Location = new System.Drawing.Point(256, 25);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "档案编号 ";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEdit姓名;
            this.layoutControlItem3.CustomizationFormText = "孕妇姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(489, 25);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "孕妇姓名";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // lbl随访医生
            // 
            this.lbl随访医生.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl随访医生.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl随访医生.AppearanceItemCaption.Options.UseFont = true;
            this.lbl随访医生.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl随访医生.Control = this.textEdit随访医生;
            this.lbl随访医生.CustomizationFormText = "联系电话";
            this.lbl随访医生.Location = new System.Drawing.Point(199, 449);
            this.lbl随访医生.MaxSize = new System.Drawing.Size(250, 24);
            this.lbl随访医生.MinSize = new System.Drawing.Size(171, 24);
            this.lbl随访医生.Name = "lbl随访医生";
            this.lbl随访医生.Size = new System.Drawing.Size(199, 25);
            this.lbl随访医生.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl随访医生.Tag = "check";
            this.lbl随访医生.Text = "随访医师签名";
            this.lbl随访医生.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl随访医生.TextSize = new System.Drawing.Size(80, 14);
            this.lbl随访医生.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.textEdit创建时间;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 474);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "创建时间:";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.textEdit最近更新时间;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(233, 474);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(211, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "最近更新时间:";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.textEdit当前所属机构;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(444, 474);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 48);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "当前所属机构:";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.textEdit创建机构;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 498);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "创建机构: ";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.textEdit创建人;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(233, 498);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(211, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "创建人:";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.textEdit最近修改人;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(444, 498);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(0, 48);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(229, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "最近修改人:";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(90, 0);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEdit居住地址;
            this.layoutControlItem12.CustomizationFormText = "居住地址 ";
            this.layoutControlItem12.Location = new System.Drawing.Point(256, 73);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(417, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "居住地址 ";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // lbl随访日期
            // 
            this.lbl随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl随访日期.Control = this.dte随访日期;
            this.lbl随访日期.CustomizationFormText = "随访日期";
            this.lbl随访日期.Location = new System.Drawing.Point(0, 97);
            this.lbl随访日期.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl随访日期.MinSize = new System.Drawing.Size(254, 24);
            this.lbl随访日期.Name = "lbl随访日期";
            this.lbl随访日期.Size = new System.Drawing.Size(256, 24);
            this.lbl随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl随访日期.Tag = "check";
            this.lbl随访日期.Text = "随访日期";
            this.lbl随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl随访日期.TextSize = new System.Drawing.Size(90, 20);
            this.lbl随访日期.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.textEdit居住状态;
            this.layoutControlItem11.CustomizationFormText = "居住状态 ";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 73);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(256, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "居住状态 ";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(256, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证号 ";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(90, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(256, 49);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(233, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.Control = this.textEdit联系电话;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(489, 49);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "本人电话 ";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // lbl体温
            // 
            this.lbl体温.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl体温.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl体温.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体温.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体温.Control = this.uc体温;
            this.lbl体温.CustomizationFormText = "体重";
            this.lbl体温.Location = new System.Drawing.Point(0, 145);
            this.lbl体温.MinSize = new System.Drawing.Size(189, 24);
            this.lbl体温.Name = "lbl体温";
            this.lbl体温.Size = new System.Drawing.Size(673, 24);
            this.lbl体温.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl体温.Tag = "check";
            this.lbl体温.Text = "体温";
            this.lbl体温.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl体温.TextSize = new System.Drawing.Size(90, 20);
            this.lbl体温.TextToControlDistance = 5;
            // 
            // lbl一般健康情况
            // 
            this.lbl一般健康情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl一般健康情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl一般健康情况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl一般健康情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl一般健康情况.Control = this.textEdit一般健康情况;
            this.lbl一般健康情况.CustomizationFormText = "尿 蛋 白";
            this.lbl一般健康情况.Location = new System.Drawing.Point(0, 169);
            this.lbl一般健康情况.MaxSize = new System.Drawing.Size(559, 24);
            this.lbl一般健康情况.MinSize = new System.Drawing.Size(109, 24);
            this.lbl一般健康情况.Name = "lbl一般健康情况";
            this.lbl一般健康情况.Size = new System.Drawing.Size(673, 24);
            this.lbl一般健康情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl一般健康情况.Tag = "check";
            this.lbl一般健康情况.Text = "一般健康情况";
            this.lbl一般健康情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl一般健康情况.TextSize = new System.Drawing.Size(90, 20);
            this.lbl一般健康情况.TextToControlDistance = 5;
            // 
            // lbl一般心理状况
            // 
            this.lbl一般心理状况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl一般心理状况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl一般心理状况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl一般心理状况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl一般心理状况.Control = this.textEdit一般心理状况;
            this.lbl一般心理状况.CustomizationFormText = "一般心理状况";
            this.lbl一般心理状况.Location = new System.Drawing.Point(0, 193);
            this.lbl一般心理状况.MaxSize = new System.Drawing.Size(559, 24);
            this.lbl一般心理状况.MinSize = new System.Drawing.Size(81, 24);
            this.lbl一般心理状况.Name = "lbl一般心理状况";
            this.lbl一般心理状况.Size = new System.Drawing.Size(673, 24);
            this.lbl一般心理状况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl一般心理状况.Tag = "check";
            this.lbl一般心理状况.Text = "一般心理状况";
            this.lbl一般心理状况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl一般心理状况.TextSize = new System.Drawing.Size(90, 20);
            this.lbl一般心理状况.TextToControlDistance = 5;
            // 
            // lbl血压
            // 
            this.lbl血压.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血压.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl血压.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血压.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血压.Control = this.uc血压;
            this.lbl血压.CustomizationFormText = "血压";
            this.lbl血压.Location = new System.Drawing.Point(0, 217);
            this.lbl血压.MinSize = new System.Drawing.Size(131, 24);
            this.lbl血压.Name = "lbl血压";
            this.lbl血压.Size = new System.Drawing.Size(336, 24);
            this.lbl血压.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血压.Tag = "check";
            this.lbl血压.Text = "血压";
            this.lbl血压.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血压.TextSize = new System.Drawing.Size(90, 14);
            this.lbl血压.TextToControlDistance = 5;
            // 
            // lbl其他
            // 
            this.lbl其他.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl其他.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl其他.AppearanceItemCaption.Options.UseFont = true;
            this.lbl其他.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl其他.Control = this.textEdit其他;
            this.lbl其他.CustomizationFormText = "其 他";
            this.lbl其他.Location = new System.Drawing.Point(0, 337);
            this.lbl其他.MaxSize = new System.Drawing.Size(559, 24);
            this.lbl其他.MinSize = new System.Drawing.Size(109, 24);
            this.lbl其他.Name = "lbl其他";
            this.lbl其他.Size = new System.Drawing.Size(673, 24);
            this.lbl其他.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl其他.Tag = "check";
            this.lbl其他.Text = "其 他";
            this.lbl其他.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl其他.TextSize = new System.Drawing.Size(90, 20);
            this.lbl其他.TextToControlDistance = 5;
            // 
            // lbl产次
            // 
            this.lbl产次.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl产次.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl产次.AppearanceItemCaption.Options.UseFont = true;
            this.lbl产次.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl产次.Control = this.uc产次;
            this.lbl产次.CustomizationFormText = "孕 次";
            this.lbl产次.Location = new System.Drawing.Point(336, 217);
            this.lbl产次.MinSize = new System.Drawing.Size(159, 24);
            this.lbl产次.Name = "lbl产次";
            this.lbl产次.Size = new System.Drawing.Size(337, 24);
            this.lbl产次.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl产次.Tag = "check";
            this.lbl产次.Text = "产次";
            this.lbl产次.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl产次.TextSize = new System.Drawing.Size(80, 20);
            this.lbl产次.TextToControlDistance = 5;
            // 
            // lbl指导
            // 
            this.lbl指导.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl指导.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl指导.AppearanceItemCaption.Options.UseFont = true;
            this.lbl指导.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl指导.Control = this.textEdit指导;
            this.lbl指导.CustomizationFormText = "指 导";
            this.lbl指导.Location = new System.Drawing.Point(0, 385);
            this.lbl指导.MaxSize = new System.Drawing.Size(559, 0);
            this.lbl指导.MinSize = new System.Drawing.Size(50, 40);
            this.lbl指导.Name = "lbl指导";
            this.lbl指导.Size = new System.Drawing.Size(673, 40);
            this.lbl指导.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl指导.Tag = "check";
            this.lbl指导.Text = "指 导";
            this.lbl指导.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl指导.TextSize = new System.Drawing.Size(90, 20);
            this.lbl指导.TextToControlDistance = 5;
            // 
            // lbl转诊
            // 
            this.lbl转诊.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl转诊.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl转诊.AppearanceItemCaption.Options.UseFont = true;
            this.lbl转诊.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl转诊.Control = this.textEdit转诊;
            this.lbl转诊.CustomizationFormText = "转诊";
            this.lbl转诊.Location = new System.Drawing.Point(0, 425);
            this.lbl转诊.MaxSize = new System.Drawing.Size(625, 24);
            this.lbl转诊.MinSize = new System.Drawing.Size(149, 24);
            this.lbl转诊.Name = "lbl转诊";
            this.lbl转诊.Size = new System.Drawing.Size(673, 24);
            this.lbl转诊.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl转诊.Tag = "check";
            this.lbl转诊.Text = "转诊";
            this.lbl转诊.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl转诊.TextSize = new System.Drawing.Size(90, 20);
            this.lbl转诊.TextToControlDistance = 5;
            // 
            // lbl恶漏
            // 
            this.lbl恶漏.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl恶漏.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl恶漏.AppearanceItemCaption.Options.UseFont = true;
            this.lbl恶漏.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl恶漏.Control = this.textEdit恶漏;
            this.lbl恶漏.CustomizationFormText = "恶 露";
            this.lbl恶漏.Location = new System.Drawing.Point(0, 265);
            this.lbl恶漏.MaxSize = new System.Drawing.Size(559, 24);
            this.lbl恶漏.MinSize = new System.Drawing.Size(85, 24);
            this.lbl恶漏.Name = "lbl恶漏";
            this.lbl恶漏.Size = new System.Drawing.Size(673, 24);
            this.lbl恶漏.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl恶漏.Tag = "check";
            this.lbl恶漏.Text = "恶 露";
            this.lbl恶漏.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl恶漏.TextSize = new System.Drawing.Size(90, 20);
            this.lbl恶漏.TextToControlDistance = 5;
            // 
            // lbl子宫
            // 
            this.lbl子宫.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl子宫.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl子宫.AppearanceItemCaption.Options.UseFont = true;
            this.lbl子宫.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl子宫.Control = this.textEdit子宫;
            this.lbl子宫.CustomizationFormText = "子 宫";
            this.lbl子宫.Location = new System.Drawing.Point(0, 289);
            this.lbl子宫.MaxSize = new System.Drawing.Size(559, 24);
            this.lbl子宫.MinSize = new System.Drawing.Size(85, 24);
            this.lbl子宫.Name = "lbl子宫";
            this.lbl子宫.Size = new System.Drawing.Size(673, 24);
            this.lbl子宫.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl子宫.Tag = "check";
            this.lbl子宫.Text = "子 宫";
            this.lbl子宫.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl子宫.TextSize = new System.Drawing.Size(90, 20);
            this.lbl子宫.TextToControlDistance = 5;
            // 
            // lbl乳房
            // 
            this.lbl乳房.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl乳房.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl乳房.AppearanceItemCaption.Options.UseFont = true;
            this.lbl乳房.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl乳房.Control = this.textEdit乳房;
            this.lbl乳房.CustomizationFormText = "乳 房";
            this.lbl乳房.Location = new System.Drawing.Point(0, 241);
            this.lbl乳房.MaxSize = new System.Drawing.Size(559, 24);
            this.lbl乳房.MinSize = new System.Drawing.Size(85, 24);
            this.lbl乳房.Name = "lbl乳房";
            this.lbl乳房.Size = new System.Drawing.Size(673, 24);
            this.lbl乳房.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl乳房.Tag = "check";
            this.lbl乳房.Text = "乳 房";
            this.lbl乳房.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl乳房.TextSize = new System.Drawing.Size(90, 20);
            this.lbl乳房.TextToControlDistance = 5;
            // 
            // lbl伤口
            // 
            this.lbl伤口.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl伤口.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl伤口.AppearanceItemCaption.Options.UseFont = true;
            this.lbl伤口.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl伤口.Control = this.textEdit伤口;
            this.lbl伤口.CustomizationFormText = "伤 口";
            this.lbl伤口.Location = new System.Drawing.Point(0, 313);
            this.lbl伤口.MaxSize = new System.Drawing.Size(559, 24);
            this.lbl伤口.MinSize = new System.Drawing.Size(85, 24);
            this.lbl伤口.Name = "lbl伤口";
            this.lbl伤口.Size = new System.Drawing.Size(673, 24);
            this.lbl伤口.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl伤口.Tag = "check";
            this.lbl伤口.Text = "伤 口";
            this.lbl伤口.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl伤口.TextSize = new System.Drawing.Size(90, 20);
            this.lbl伤口.TextToControlDistance = 5;
            // 
            // lbl分类
            // 
            this.lbl分类.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl分类.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl分类.AppearanceItemCaption.Options.UseFont = true;
            this.lbl分类.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl分类.Control = this.textEdit分类;
            this.lbl分类.CustomizationFormText = "分 类";
            this.lbl分类.Location = new System.Drawing.Point(0, 361);
            this.lbl分类.MaxSize = new System.Drawing.Size(559, 24);
            this.lbl分类.MinSize = new System.Drawing.Size(85, 24);
            this.lbl分类.Name = "lbl分类";
            this.lbl分类.Size = new System.Drawing.Size(673, 24);
            this.lbl分类.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl分类.Tag = "check";
            this.lbl分类.Text = "分 类";
            this.lbl分类.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl分类.TextSize = new System.Drawing.Size(90, 20);
            this.lbl分类.TextToControlDistance = 5;
            // 
            // lbl考核
            // 
            this.lbl考核.Control = this.lbl考核项;
            this.lbl考核.CustomizationFormText = "layoutControlItem6";
            this.lbl考核.Location = new System.Drawing.Point(0, 0);
            this.lbl考核.MaxSize = new System.Drawing.Size(186, 25);
            this.lbl考核.MinSize = new System.Drawing.Size(186, 25);
            this.lbl考核.Name = "lbl考核";
            this.lbl考核.Size = new System.Drawing.Size(673, 25);
            this.lbl考核.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl考核.Text = "lbl考核";
            this.lbl考核.TextSize = new System.Drawing.Size(0, 0);
            this.lbl考核.TextToControlDistance = 0;
            this.lbl考核.TextVisible = false;
            // 
            // lbl分娩日期
            // 
            this.lbl分娩日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl分娩日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl分娩日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl分娩日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl分娩日期.Control = this.date分娩日期;
            this.lbl分娩日期.CustomizationFormText = "分娩日期";
            this.lbl分娩日期.Location = new System.Drawing.Point(0, 121);
            this.lbl分娩日期.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl分娩日期.MinSize = new System.Drawing.Size(149, 24);
            this.lbl分娩日期.Name = "lbl分娩日期";
            this.lbl分娩日期.Size = new System.Drawing.Size(256, 24);
            this.lbl分娩日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl分娩日期.Tag = "check";
            this.lbl分娩日期.Text = "分娩日期";
            this.lbl分娩日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl分娩日期.TextSize = new System.Drawing.Size(90, 20);
            this.lbl分娩日期.TextToControlDistance = 5;
            // 
            // lbl出院日期
            // 
            this.lbl出院日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl出院日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl出院日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl出院日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl出院日期.Control = this.date出院日期;
            this.lbl出院日期.CustomizationFormText = "出院日期";
            this.lbl出院日期.Location = new System.Drawing.Point(256, 121);
            this.lbl出院日期.Name = "lbl出院日期";
            this.lbl出院日期.Size = new System.Drawing.Size(274, 24);
            this.lbl出院日期.Tag = "check";
            this.lbl出院日期.Text = "出院日期";
            this.lbl出院日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl出院日期.TextSize = new System.Drawing.Size(80, 20);
            this.lbl出院日期.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.layoutControl2;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(530, 121);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(143, 24);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // lbl居民或家属签名
            // 
            this.lbl居民或家属签名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl居民或家属签名.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl居民或家属签名.AppearanceItemCaption.Options.UseFont = true;
            this.lbl居民或家属签名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl居民或家属签名.Control = this.txt居民或家属签名;
            this.lbl居民或家属签名.CustomizationFormText = "居民/家属签名";
            this.lbl居民或家属签名.Location = new System.Drawing.Point(398, 449);
            this.lbl居民或家属签名.Name = "lbl居民或家属签名";
            this.lbl居民或家属签名.Size = new System.Drawing.Size(275, 25);
            this.lbl居民或家属签名.Tag = "check";
            this.lbl居民或家属签名.Text = "居民/家属签名";
            this.lbl居民或家属签名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl居民或家属签名.TextSize = new System.Drawing.Size(90, 20);
            this.lbl居民或家属签名.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.checkEdit失访;
            this.layoutControlItem6.CustomizationFormText = "失访情况";
            this.layoutControlItem6.Location = new System.Drawing.Point(256, 97);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(187, 24);
            this.layoutControlItem6.Text = "失访情况";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt失访原因;
            this.layoutControlItem7.CustomizationFormText = "失访原因";
            this.layoutControlItem7.Location = new System.Drawing.Point(443, 97);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(230, 24);
            this.layoutControlItem7.Text = "失访原因";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(48, 14);
            // 
            // UC产后访视记录表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC产后访视记录表_显示";
            this.Size = new System.Drawing.Size(796, 441);
            this.Load += new System.EventHandler(this.UC产后访视记录表_显示_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt失访原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit失访.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居民或家属签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date出院日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date出院日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date分娩日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.date分娩日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit分类.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit伤口.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit乳房.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit子宫.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit恶漏.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit转诊.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit一般健康情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit一般心理状况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit联系电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit指导.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl下次随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访医生)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体温)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl一般健康情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl一般心理状况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl其他)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl产次)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl指导)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl转诊)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl恶漏)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl子宫)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl乳房)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl伤口)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl分类)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl考核)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl分娩日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl出院日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl居民或家属签名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn添加;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit textEdit联系电话;
        private DevExpress.XtraEditors.TextEdit textEdit居住地址;
        private DevExpress.XtraEditors.TextEdit textEdit居住状态;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit姓名;
        private DevExpress.XtraEditors.TextEdit textEdit档案编号;
        private DevExpress.XtraEditors.TextEdit txt卡号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem lbl下次随访日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl随访医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraEditors.TextEdit textEdit创建人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近更新时间;
        private DevExpress.XtraEditors.TextEdit textEdit创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem lbl随访日期;
        private Library.UserControls.UCTxtLblTxtLbl uc血压;
        private Library.UserControls.UCTxtLbl uc体温;
        private DevExpress.XtraLayout.LayoutControlItem lbl体温;
        private DevExpress.XtraLayout.LayoutControlItem lbl血压;
        private DevExpress.XtraEditors.TextEdit textEdit一般健康情况;
        private Library.UserControls.UCTxtLbl uc产次;
        private DevExpress.XtraEditors.TextEdit textEdit一般心理状况;
        private DevExpress.XtraLayout.LayoutControlItem lbl一般心理状况;
        private DevExpress.XtraLayout.LayoutControlItem lbl产次;
        private DevExpress.XtraLayout.LayoutControlItem lbl一般健康情况;
        private DevExpress.XtraEditors.TextEdit textEdit其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl其他;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraEditors.TextEdit dte随访日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl指导;
        private DevExpress.XtraEditors.TextEdit textEdit转诊;
        private DevExpress.XtraLayout.LayoutControlItem lbl转诊;
        private DevExpress.XtraEditors.TextEdit textEdit下次随访日期;
        private DevExpress.XtraEditors.TextEdit textEdit分类;
        private DevExpress.XtraEditors.TextEdit textEdit伤口;
        private DevExpress.XtraEditors.TextEdit textEdit乳房;
        private DevExpress.XtraEditors.TextEdit textEdit子宫;
        private DevExpress.XtraEditors.TextEdit textEdit恶漏;
        private DevExpress.XtraLayout.LayoutControlItem lbl恶漏;
        private DevExpress.XtraLayout.LayoutControlItem lbl子宫;
        private DevExpress.XtraLayout.LayoutControlItem lbl乳房;
        private DevExpress.XtraLayout.LayoutControlItem lbl伤口;
        private DevExpress.XtraLayout.LayoutControlItem lbl分类;
        private DevExpress.XtraEditors.LabelControl lbl考核项;
        private DevExpress.XtraLayout.LayoutControlItem lbl考核;
        private DevExpress.XtraEditors.MemoEdit textEdit指导;
        private DevExpress.XtraEditors.TextEdit txt居民或家属签名;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.DateEdit date出院日期;
        private DevExpress.XtraEditors.DateEdit date分娩日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl分娩日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl出院日期;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem lbl居民或家属签名;
        private DevExpress.XtraEditors.TextEdit txt失访原因;
        private DevExpress.XtraEditors.CheckEdit checkEdit失访;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
    }
}
