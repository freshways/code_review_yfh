﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Models;
using DevExpress.XtraEditors;
using AtomEHR.Library.UserControls;
using AtomEHR.Common;
using DevExpress.XtraReports.UI;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class UC产后访视记录表_显示 : UserControlBaseNavBar
    {
        #region Fields
        AtomEHR.Business.bll孕妇_产后访视情况 _Bll = new Business.bll孕妇_产后访视情况();
        DataSet _ds产后访视记录;
        public frm个人健康 _frm = null;
        string _docNo;
        string _familyNo;
        string _id;
        DataRow _dr当前数据;
        #endregion

        #region Constructor

        public UC产后访视记录表_显示()
        {
            InitializeComponent();
        }
        public UC产后访视记录表_显示(Form parentForm)
        {
            InitializeComponent();
            _frm = (frm个人健康)parentForm;
            _docNo = _frm._docNo;
            _familyNo = _frm._familyDocNo;
            _id = _frm._param == null ? "" : _frm._param.ToString();
        }

        #endregion

        #region Handler Events

        private void btn添加_Click(object sender, EventArgs e)
        {
            UC产后访视记录表 uc = new UC产后访视记录表(_frm, AtomEHR.Common.UpdateType.Add);
            ShowControl(uc, DockStyle.Fill);
        }
        private void btn修改_Click(object sender, EventArgs e)
        {
            if (canModifyBy同一机构(_dr当前数据[tb_孕妇_产后访视情况.所属机构].ToString()))
            {
                _id = _dr当前数据["ID"].ToString();
                _frm._param = _id;
                UC产后访视记录表 uc = new UC产后访视记录表(_frm, AtomEHR.Common.UpdateType.Modify);
                ShowControl(uc, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
                return;
            }
        }
        private void btn删除_Click(object sender, EventArgs e)
        {
            if (!Msg.AskQuestion("确认要删除？")) return;
            if (canModifyBy同一机构(_dr当前数据[tb_孕妇_产后访视情况.所属机构].ToString()))
            {
                //TODO : 产后42天 删除
                _id = _dr当前数据["ID"].ToString();
                _frm._param = _id;
                if (_Bll.Delete(_id))
                {
                    _id = "";
                    this.OnLoad(e);
                }
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }
        private void UC产后访视记录表_显示_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < this.layoutControl1.Controls.Count; i++)
            {
                if (this.layoutControl1.Controls[i] is TextEdit)
                {
                    TextEdit txt = layoutControl1.Controls[i] as TextEdit;
                    txt.Properties.ReadOnly = true;
                    txt.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxt)
                {
                    UCLblTxt txt = layoutControl1.Controls[i] as UCLblTxt;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCTxtLbl)
                {
                    UCTxtLbl txt = layoutControl1.Controls[i] as UCTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxtLbl)
                {
                    UCLblTxtLbl txt = layoutControl1.Controls[i] as UCLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCTxtLblTxtLbl)
                {
                    UCTxtLblTxtLbl txt = layoutControl1.Controls[i] as UCTxtLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                    txt.Txt2.Properties.ReadOnly = true;
                    txt.Txt2.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is UCLblTxtLbl)
                {
                    UCLblTxtLbl txt = layoutControl1.Controls[i] as UCLblTxtLbl;
                    txt.Txt1.Properties.ReadOnly = true;
                    txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                }
                if (this.layoutControl1.Controls[i] is FlowLayoutPanel)
                {
                    FlowLayoutPanel flow = layoutControl1.Controls[i] as FlowLayoutPanel;
                    for (int j = 0; j < flow.Controls.Count; j++)
                    {
                        if (flow.Controls[j] is TextEdit)
                        {
                            TextEdit txt = flow.Controls[j] as TextEdit;
                            txt.Properties.ReadOnly = true;
                            txt.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCLblTxt)
                        {
                            UCLblTxt txt = flow.Controls[j] as UCLblTxt;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCTxtLbl)
                        {
                            UCTxtLbl txt = flow.Controls[j] as UCTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCLblTxtLbl)
                        {
                            UCLblTxtLbl txt = flow.Controls[j] as UCLblTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                        }
                        if (flow.Controls[j] is UCTxtLblTxtLbl)
                        {
                            UCTxtLblTxtLbl txt = flow.Controls[j] as UCTxtLblTxtLbl;
                            txt.Txt1.Properties.ReadOnly = true;
                            txt.Txt1.BackColor = System.Drawing.Color.Transparent;
                            txt.Txt2.Properties.ReadOnly = true;
                            txt.Txt2.BackColor = System.Drawing.Color.Transparent;
                        }
                    }

                }
            }

            //this.uc流产.Margin = new Padding(0);
            //this.uc死胎.Margin = new Padding(0);
            //this.uc死产.Margin = new Padding(0);
            //this.uc新生儿死亡.Margin = new Padding(0);
            //this.uc出生缺陷儿.Margin = new Padding(0);
            //this.uc流产.Width = 90;
            //this.uc死胎.Width = 90;
            //this.uc死产.Width = 90;

            //根据个人档案编号 查询全部的  体检数据
            _ds产后访视记录 = _Bll.GetAllDataByKey(_docNo);
            DoBindingDataSource(_ds产后访视记录);//绑定数据
            //设置日期栏中字体颜色
            SetItemColorToRed(_id);
        }
        #endregion

        #region Private Methods
        private void DoBindingDataSource(DataSet _ds产后访视记录)
        {
            if (_ds产后访视记录 == null) return;
            DataTable dt产后访视记录 = _ds产后访视记录.Tables[1];
            if (dt产后访视记录 == null || dt产后访视记录.Rows.Count <= 0)
            {
                btn添加.PerformClick();
                return;
            }

            //DataTable dt孕妇基本信息 = _ds产后访视记录.Tables[0];
            DataTable dt孕妇基本信息 = _ds产后访视记录.Tables[tb_健康档案.__TableName];
            #region 先绑定孕妇基本信息
            Bind基本信息(dt孕妇基本信息);

            #endregion

            #region 创建页面左边的导航树
            CreateNavBarButton_new(dt产后访视记录, tb_孕妇_产后访视情况.随访日期);
            #endregion

            //如果date为空，则默认绑定第一条体检数据
            //否则  根据date进行查找对应的 体检数据 进行绑定
            DataRow row;
            if (string.IsNullOrEmpty(_id) && dt产后访视记录.Rows.Count > 0)
            {
                row = dt产后访视记录.Rows[0];
                _id = dt产后访视记录.Rows[0][tb_孕妇_产后访视情况.ID].ToString();
            }
            else
            {
                DataRow[] rows = dt产后访视记录.Select("ID='" + _id + "'");
                if (rows.Length == 1)
                {
                    row = rows[0];
                }
                else
                {
                    return;
                }
            }
            DoBindingSummaryEditor(row);
        }

        //change 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 ▽
        //private void Bind基本信息(DataTable dt孕妇基本信息)
        //{
        //    if (dt孕妇基本信息 == null) return;

        //    this.textEdit档案编号.Text = dt孕妇基本信息.Rows[0][tb_孕妇基本信息.个人档案编号].ToString();
        //    this.textEdit姓名.Text = util.DESEncrypt.DES解密(dt孕妇基本信息.Rows[0][tb_孕妇基本信息.姓名].ToString());
        //    this.textEdit出生日期.Text = dt孕妇基本信息.Rows[0][tb_孕妇基本信息.出生日期].ToString();
        //    this.textEdit身份证号.Text = dt孕妇基本信息.Rows[0][tb_孕妇基本信息.身份证号].ToString();
        //    this.textEdit居住状态.Text = _Bll.ReturnDis字典显示("jzzk", dt孕妇基本信息.Rows[0][tb_孕妇基本信息.居住状况].ToString());
        //    this.textEdit居住地址.Text = _Bll.Return地区名称(dt孕妇基本信息.Rows[0][tb_孕妇基本信息.区].ToString()) + _Bll.Return地区名称(dt孕妇基本信息.Rows[0][tb_孕妇基本信息.街道].ToString()) + _Bll.Return地区名称(dt孕妇基本信息.Rows[0][tb_孕妇基本信息.居委会].ToString()) + dt孕妇基本信息.Rows[0][tb_孕妇基本信息.居住地址].ToString();
        //}

        private void Bind基本信息(DataTable dt健康档案)
        {
            if (dt健康档案 == null) return;

            this.textEdit档案编号.Text = dt健康档案.Rows[0][tb_健康档案.个人档案编号].ToString();
            this.textEdit姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.textEdit出生日期.Text = dt健康档案.Rows[0][tb_健康档案.出生日期].ToString();
            this.textEdit身份证号.Text = dt健康档案.Rows[0][tb_健康档案.身份证号].ToString();
            this.textEdit居住状态.Text = _Bll.ReturnDis字典显示("jzzk", dt健康档案.Rows[0][tb_健康档案.常住类型].ToString());
            this.textEdit居住地址.Text = _Bll.Return地区名称(dt健康档案.Rows[0][tb_健康档案.区].ToString())
                + _Bll.Return地区名称(dt健康档案.Rows[0][tb_健康档案.街道].ToString())
                + _Bll.Return地区名称(dt健康档案.Rows[0][tb_健康档案.居委会].ToString()) + dt健康档案.Rows[0][tb_健康档案.居住地址].ToString();
            this.textEdit联系电话.Text = dt健康档案.Rows[0][tb_健康档案.本人电话].ToString();
        }

        //change 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 △
        protected override void DoBindingSummaryEditor(object dataSource)
        {
            _dr当前数据 = (DataRow)dataSource;
            if (_dr当前数据 == null) return;
            this.txt卡号.Text = _dr当前数据[tb_孕妇_产后访视情况.卡号].ToString();
            this.date分娩日期.Text = _dr当前数据[tb_孕妇_产后访视情况.分娩日期].ToString();
            if (_dr当前数据[tb_孕妇_产后访视情况.失访情况] != null && !string.IsNullOrEmpty(_dr当前数据[tb_孕妇_产后访视情况.失访情况].ToString()))
            {
                this.checkEdit失访.Checked = true;
            }
            this.txt失访原因.Text = _dr当前数据[tb_孕妇_产后访视情况.失访原因].ToString();
            this.date出院日期.Text = _dr当前数据[tb_孕妇_产后访视情况.出院日期].ToString();
            this.dte随访日期.Text = _dr当前数据[tb_孕妇_产后访视情况.随访日期].ToString();
            this.uc体温.Txt1.Text = _dr当前数据[tb_孕妇_产后访视情况.体温].ToString();
            this.uc血压.Txt1.Text = _dr当前数据[tb_孕妇_产后访视情况.血压1].ToString();
            this.uc血压.Txt2.Text = _dr当前数据[tb_孕妇_产后访视情况.血压2].ToString();
            this.textEdit一般健康情况.Text = _dr当前数据[tb_孕妇_产后访视情况.一般健康情况].ToString();
            this.textEdit一般心理状况.Text = _dr当前数据[tb_孕妇_产后访视情况.一般心理状况].ToString();
            if (_dr当前数据[tb_孕妇_产后访视情况.乳房].ToString() == "1")
                this.textEdit乳房.Text = "未见异常";
            else
                this.textEdit乳房.Text = "异常：" + _dr当前数据[tb_孕妇_产后访视情况.乳房异常].ToString();

            if (_dr当前数据[tb_孕妇_产后访视情况.恶露].ToString() == "1")
                this.textEdit恶漏.Text = "未见异常";
            else
                this.textEdit恶漏.Text = "异常：" + _dr当前数据[tb_孕妇_产后访视情况.恶露异常].ToString();

            if (_dr当前数据[tb_孕妇_产后访视情况.子宫].ToString() == "1")
                this.textEdit子宫.Text = "未见异常";
            else
                this.textEdit子宫.Text = "异常：" + _dr当前数据[tb_孕妇_产后访视情况.子宫异常].ToString();

            if (_dr当前数据[tb_孕妇_产后访视情况.伤口].ToString() == "1")
                this.textEdit伤口.Text = "未见异常";
            else
                this.textEdit伤口.Text = "异常：" + _dr当前数据[tb_孕妇_产后访视情况.伤口异常].ToString();

            if (_dr当前数据[tb_孕妇_产后访视情况.分类].ToString() == "1")
                this.textEdit分类.Text = "未见异常";
            else
                this.textEdit分类.Text = "异常：" + _dr当前数据[tb_孕妇_产后访视情况.分类异常].ToString();

            this.textEdit其他.Text = _dr当前数据[tb_孕妇_产后访视情况.其他].ToString();
            this.textEdit指导.Text = SetFlowLayoutValues("ycfzd", _dr当前数据[tb_孕妇_产后访视情况.指导].ToString());
            this.uc产次.Txt1.Text = _dr当前数据[tb_孕妇_产后访视情况.产次].ToString();

            this.uc血压.Txt1.Text = _dr当前数据[tb_孕妇_产后访视情况.血压1].ToString();
            this.uc血压.Txt2.Text = _dr当前数据[tb_孕妇_产后访视情况.血压2].ToString();

            if (_dr当前数据[tb_孕妇_产后访视情况.子宫] == null || _dr当前数据[tb_孕妇_产后访视情况.子宫].ToString() == "1" || _dr当前数据[tb_孕妇_产后访视情况.子宫].ToString() == "")//正常
                this.textEdit子宫.Text = "未见异常";
            else
                this.textEdit子宫.Text = "异常：" + _dr当前数据[tb_孕妇_产后访视情况.子宫异常].ToString();

            textEdit指导.Text = SetFlowLayoutValues("ycfzd", _dr当前数据[tb_孕妇_产后访视情况.指导].ToString()) + "  " + _dr当前数据[tb_孕妇_产后访视情况.指导其他].ToString();
            string 转诊 = string.Empty;
            if (_dr当前数据[tb_孕妇_产后访视情况.转诊] != null &&
                _dr当前数据[tb_孕妇_产后访视情况.转诊].ToString() == "1")//有
            {
                转诊 += "有" + "；"+Environment.NewLine;
                转诊 += "原因：" + _dr当前数据[tb_孕妇_产后访视情况.转诊原因].ToString() +"；"+ Environment.NewLine;
                转诊 += "机构及科室：" + _dr当前数据[tb_孕妇_产后访视情况.机构科室].ToString() +"；"+ Environment.NewLine;
                转诊 += "联系人：" + _dr当前数据[tb_孕妇_产后访视情况.联系人].ToString()+"；";
                转诊 += "联系方式：" + _dr当前数据[tb_孕妇_产后访视情况.联系方式].ToString() + "；";
                转诊 += "结果：" +  _Bll.ReturnDis字典显示("sfdw", _dr当前数据[tb_孕妇_产后访视情况.结果].ToString()) + "。";
                this.lbl一般健康情况.Height = 50;
            }
            else
            {
                转诊 += "无";
            }
            textEdit转诊.Text = 转诊;

            textEdit下次随访日期.Text = _dr当前数据[tb_孕妇_产后访视情况.下次随访日期].ToString();
            textEdit随访医生.Text = _dr当前数据[tb_孕妇_产后访视情况.随访医生签名].ToString();
            txt居民或家属签名.Text = _dr当前数据[tb_孕妇_产后访视情况.居民或家属签名].ToString();
            textEdit创建时间.Text = _dr当前数据[tb_孕妇_产后访视情况.创建时间].ToString();
            textEdit最近更新时间.Text = _dr当前数据[tb_孕妇_产后访视情况.修改时间].ToString();
            textEdit当前所属机构.Text = _Bll.Return机构名称(_dr当前数据[tb_孕妇_产后访视情况.所属机构].ToString());
            textEdit创建机构.Text = _Bll.Return机构名称(_dr当前数据[tb_孕妇_产后访视情况.创建机构].ToString());
            textEdit创建人.Text = _Bll.Return用户名称(_dr当前数据[tb_孕妇_产后访视情况.创建人].ToString());
            textEdit最近修改人.Text = _Bll.Return用户名称(_dr当前数据[tb_孕妇_产后访视情况.修改人].ToString());

            //this.emptySpaceItem1.Text = string.Format("考核项:19  缺项:{0}  完整度:{1}%", _dr当前数据[tb_孕妇_产后访视情况.缺项].ToString(), _dr当前数据[tb_孕妇_产后访视情况.完整度].ToString());

            //SetControlItemColor();

            this.lbl考核项.Text = string.Format("考核项:19  缺项:{0}  完整度:{1}%", _dr当前数据[tb_孕妇_产后访视情况.缺项].ToString(), _dr当前数据[tb_孕妇_产后访视情况.完整度].ToString());
            Set考核项颜色_new(this.layoutControl1, lbl考核项);

        }

        private void SetControlItemColor()
        {
            if (string.IsNullOrEmpty(this.dte随访日期.Text.Trim()))
                this.lbl随访日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.date分娩日期.Text.Trim()))
                this.lbl分娩日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.date出院日期.Text.Trim()))
                this.lbl出院日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.uc体温.Txt1.Text.Trim()))
                this.lbl体温.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit一般健康情况.Text.Trim()))
                this.lbl一般健康情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit一般心理状况.Text.Trim()))
                this.lbl一般心理状况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.uc血压.Txt1.Text.Trim()) || string.IsNullOrEmpty(this.uc血压.Txt2.Text.Trim()))
                this.lbl血压.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.uc产次.Txt1.Text.Trim()))
                this.lbl产次.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit乳房.Text.Trim()))
                this.lbl乳房.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit恶漏.Text.Trim()))
                this.lbl恶漏.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit子宫.Text.Trim()))
                this.lbl子宫.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit伤口.Text.Trim()))
                this.lbl伤口.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit其他.Text.Trim()))
                this.lbl其他.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit分类.Text.Trim()))
                this.lbl分类.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit指导.Text.Trim()))
                this.lbl指导.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit转诊.Text.Trim()))
                this.lbl转诊.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit下次随访日期.Text.Trim()))
                this.lbl下次随访日期.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.textEdit随访医生.Text.Trim()))
                this.lbl随访医生.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
            if (string.IsNullOrEmpty(this.txt居民或家属签名.Text.Trim()))
                this.lbl居民或家属签名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
        }

        #endregion

        private void btn导出_Click(object sender, EventArgs e)
        {
            string _docNo = this.textEdit档案编号.Text.Trim();
            if(!string.IsNullOrEmpty(_docNo))
            {
                report产后访视记录表 report = new report产后访视记录表(_docNo);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }
        }

    }
}
