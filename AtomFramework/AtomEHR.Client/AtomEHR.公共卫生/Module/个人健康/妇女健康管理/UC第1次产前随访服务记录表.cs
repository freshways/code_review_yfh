﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business.Security;
using AtomEHR.Common;
using AtomEHR.Models;
using DevExpress.XtraEditors;
using AtomEHR.Library;
using AtomEHR.Business;
using AtomEHR.公共卫生.Module.个人健康.体检就诊信息;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class UC第1次产前随访服务记录表 : UserControlBase
    {
        #region Fields
        AtomEHR.Business.bll孕妇产前一次随访记录 _Bll = new Business.bll孕妇产前一次随访记录();
        DataSet _ds孕妇产前一次随访;
        DataTable _dt孕妇基本信息;
        DataTable _dt个人健康档案;
        DataTable _dt一次随访;
        DataTable _dt个人健康特征;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        string _id;//表的主键，通过id来查询一条数据
        #endregion

        #region Construction

        public UC第1次产前随访服务记录表()
        {
            InitializeComponent();
        }
        public UC第1次产前随访服务记录表(Form frm, UpdateType _updateType)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _serverDateTime = _Bll.ServiceDateTime;
            _UpdateType = _updateType;
            _id = _frm._param as string;
            if (_UpdateType == UpdateType.Add)//添加随访
            {
                _Bll.GetBusinessByKey(_docNo, true);
                _Bll.NewBusiness();
                _ds孕妇产前一次随访 = _Bll.CurrentBusiness;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                _ds孕妇产前一次随访 = _Bll.GetOneDataByKey(_docNo, _id, true);
                //this.dte填表日期.Properties.ReadOnly = true;
                this.dte填表日期.Properties.ReadOnly = false;
                //this.dte下次随访日期.Properties.ReadOnly = true;
                this.dte下次随访日期.Properties.ReadOnly = false;
            }

        }

        #endregion

        #region Private Methods

        private void DoBindingSummaryEditor(DataSet _ds孕妇产前一次随访)
        {
            if (_ds孕妇产前一次随访 == null) return;
            if (_ds孕妇产前一次随访.Tables.Count == 0) return;
            _dt孕妇基本信息 = _ds孕妇产前一次随访.Tables[tb_孕妇基本信息.__TableName];
            _dt一次随访 = _ds孕妇产前一次随访.Tables[tb_孕妇_产前随访1次.__TableName];
            _dt个人健康档案 = _ds孕妇产前一次随访.Tables[tb_健康档案.__TableName];
            _dt个人健康特征 = _ds孕妇产前一次随访.Tables[tb_健康档案_个人健康特征.__TableName];
            if (_dt个人健康档案.Rows.Count == 1)
            {
                //changed 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 ▽
                //DataRow dr = _dt孕妇基本信息.Rows[0];
                //this.textEdit孕妇姓名.Text = util.DESEncrypt.DES解密(dr[tb_孕妇基本信息.姓名].ToString());
                //this.textEdit档案编号.Text = dr[tb_孕妇基本信息.个人档案编号].ToString();
                //this.textEdit身份证号.Text = dr[tb_孕妇基本信息.身份证号].ToString();
                //this.textEdit出生日期.Text = dr[tb_孕妇基本信息.出生日期].ToString();
                ////this.txt联系电话.Text = dr[tb_孕妇基本信息.联系电话].ToString();
                //this.textEdit居住地址.Text = _bll地区.Get名称By地区ID(dr[tb_孕妇基本信息.省].ToString()) + _bll地区.Get名称By地区ID(dr[tb_孕妇基本信息.市].ToString()) + _bll地区.Get名称By地区ID(dr[tb_孕妇基本信息.区].ToString()) + _bll地区.Get名称By地区ID(dr[tb_孕妇基本信息.街道].ToString()) + _bll地区.Get名称By地区ID(dr[tb_孕妇基本信息.居委会].ToString()) + dr[tb_孕妇基本信息.居住地址].ToString();
                //string yunci = dr[tb_孕妇基本信息.孕次].ToString();

                DataRow dr = _dt个人健康档案.Rows[0];
                this.textEdit孕妇姓名.Text = util.DESEncrypt.DES解密(dr[tb_健康档案.姓名].ToString());
                this.textEdit档案编号.Text = dr[tb_健康档案.个人档案编号].ToString();
                this.textEdit身份证号.Text = dr[tb_健康档案.身份证号].ToString();
                this.textEdit出生日期.Text = dr[tb_健康档案.出生日期].ToString();
                //this.txt联系电话.Text = dr[tb_健康档案.本人电话].ToString();
                this.textEdit居住地址.Text = _bll地区.Get名称By地区ID(dr[tb_健康档案.省].ToString()) + _bll地区.Get名称By地区ID(dr[tb_健康档案.市].ToString()) 
                    + _bll地区.Get名称By地区ID(dr[tb_健康档案.区].ToString()) + _bll地区.Get名称By地区ID(dr[tb_健康档案.街道].ToString()) 
                    + _bll地区.Get名称By地区ID(dr[tb_健康档案.居委会].ToString()) + dr[tb_健康档案.居住地址].ToString();
                string yunci = dr[tb_健康档案.孕次].ToString();
                //changed 孕产妇的电话号码只能从这一个页面进行修改，修改后其他所有显示孕产妇电话的地方 一律进行自动修改覆盖 △
                if (!string.IsNullOrEmpty(yunci))
                {
                    //给 textedit孕次控件绑定数据
                    SetDataSourceForYunci(yunci);
                }
            }
            if (_dt一次随访.Rows.Count == 1)
            {
                BindSuiFangData(_dt一次随访);
            }
            if (_UpdateType == UpdateType.Add)
            {
                this.textEdit创建人.Text = Loginer.CurrentUser.AccountName;
                this.textEdit创建时间.Text = _serverDateTime;
                this.textEdit最近更新时间.Text = _serverDateTime;
                this.textEdit当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
                this.textEdit创建机构.Text = Loginer.CurrentUser.所属机构名称;
                this.textEdit最近修改人.Text = Loginer.CurrentUser.AccountName;
                //this.textEdit录入医生.Text = Loginer.CurrentUser.AccountName;

            }
            else if (_UpdateType == UpdateType.Modify)
            {
                this.textEdit创建人.Text = _Bll.Return用户名称(_dt一次随访.Rows[0][tb_老年人随访.创建人].ToString());
                this.textEdit创建时间.Text = _serverDateTime;
                this.textEdit最近更新时间.Text = _serverDateTime;
                this.textEdit当前所属机构.Text = _Bll.Return机构名称(_dt一次随访.Rows[0][tb_老年人随访.所属机构].ToString());
                this.textEdit创建机构.Text = _Bll.Return机构名称(_dt一次随访.Rows[0][tb_老年人随访.创建机构].ToString());// Loginer.CurrentUser.所属机构名称;
                this.textEdit最近修改人.Text = Loginer.CurrentUser.AccountName; //Loginer.CurrentUser.AccountName;
            }
            //初始化完成后如果丈夫信息没有赋值，需要重新赋值
            DataRow dryf = _dt孕妇基本信息.Rows[0];
            if (string.IsNullOrEmpty(textEdit丈夫姓名.Text))
            {
                this.textEdit丈夫姓名.Text = dryf[tb_孕妇基本信息.丈夫姓名].ToString();
            }
            if (string.IsNullOrEmpty(textEdit丈夫年龄.Text))
            {
                this.textEdit丈夫年龄.Text = dryf[tb_孕妇基本信息.丈夫年龄].ToString();
            }
            if (string.IsNullOrEmpty(textEdit丈夫电话.Text))
            {
                this.textEdit丈夫电话.Text = dryf[tb_孕妇基本信息.丈夫电话].ToString();
            }
        }

        private void SetDataSourceForYunci(string yunci)
        {
            int yunCi = 0;
            if (Int32.TryParse(yunci, out yunCi))
            {
                DataTable table = new DataTable();
                DataColumn Code = new DataColumn("Code", System.Type.GetType("System.String"));
                DataColumn Value = new DataColumn("Value", System.Type.GetType("System.String"));

                table.Columns.Add(Code);
                table.Columns.Add(Value);

                for (int i = 1; i <= yunCi; i++)
                {
                    DataRow newRow = table.NewRow();
                    newRow["Code"] = i + "次";
                    newRow["Value"] = i.ToString();
                    table.Rows.Add(newRow);
                }

                util.ControlsHelper.BindComboxData(table, textEdit孕次, "Value", "Code");
                util.ControlsHelper.SetComboxData(yunci, textEdit孕次);
            }
        }
        private void BindSuiFangData(DataTable dataTable)
        {
            if (dataTable == null) return;

            DataBinder.BindingTextEdit(textEdit卡号, dataTable, tb_孕妇_产前随访1次.卡号);
            DataBinder.BindingTextEditDateTime(dte填表日期, dataTable, tb_孕妇_产前随访1次.填表日期);
            DataBinder.BindingTextEdit(uc填表孕周.Txt1, dataTable, tb_孕妇_产前随访1次.孕周);
            DataBinder.BindingTextEdit(uc填表孕周.Txt2, dataTable, tb_孕妇_产前随访1次.孕周天);
            DataBinder.BindingTextEdit(textEdit孕妇年龄, dataTable, tb_孕妇_产前随访1次.孕妇年龄);
            DataBinder.BindingTextEdit(textEdit丈夫姓名, dataTable, tb_孕妇_产前随访1次.丈夫姓名);
            DataBinder.BindingTextEdit(textEdit丈夫年龄, dataTable, tb_孕妇_产前随访1次.丈夫年龄);
            DataBinder.BindingTextEdit(textEdit丈夫电话, dataTable, tb_孕妇_产前随访1次.丈夫电话);

            DataBinder.BindingTextEdit(uc产次.Txt1, dataTable, tb_孕妇_产前随访1次.产次);
            DataBinder.BindingTextEdit(uc产次.Txt2, dataTable, tb_孕妇_产前随访1次.剖宫产);
            DataBinder.BindingTextEditDateTime(dte末次月经, dataTable, tb_孕妇_产前随访1次.末次月经);
            if (dataTable.Rows[0][tb_孕妇_产前随访1次.末次月经].ToString() == "") this.chk末次月经.Checked = true;
            DataBinder.BindingTextEditDateTime(dte预产期, dataTable, tb_孕妇_产前随访1次.预产期);
            DataBinder.BindingTextEdit(textEdit既往病史其他, dataTable, tb_孕妇_产前随访1次.既往病史其他);
            DataBinder.BindingTextEdit(textEdit家族史其他, dataTable, tb_孕妇_产前随访1次.家族史其他);
            DataBinder.BindingTextEdit(txt个人史其他, dataTable, tb_孕妇_产前随访1次.个人史其他);
            DataBinder.BindingTextEdit(textEdit妇科手术史, dataTable, tb_孕妇_产前随访1次.妇科手术史有);
            DataBinder.BindingTextEdit(uc流产.Txt1, dataTable, tb_孕妇_产前随访1次.流产);
            DataBinder.BindingTextEdit(uc死胎.Txt1, dataTable, tb_孕妇_产前随访1次.死胎);
            DataBinder.BindingTextEdit(uc死产.Txt1, dataTable, tb_孕妇_产前随访1次.死产);
            DataBinder.BindingTextEdit(uc新生儿死亡.Txt1, dataTable, tb_孕妇_产前随访1次.新生儿死亡);
            DataBinder.BindingTextEdit(uc出生缺陷儿.Txt1, dataTable, tb_孕妇_产前随访1次.出生缺陷儿);
            DataBinder.BindingTextEdit(uc身高.Txt1, dataTable, tb_孕妇_产前随访1次.身高);
            DataBinder.BindingTextEdit(uc体重.Txt1, dataTable, tb_孕妇_产前随访1次.体重);
            DataBinder.BindingTextEdit(uc血压.Txt1, dataTable, tb_孕妇_产前随访1次.血压1);
            DataBinder.BindingTextEdit(uc血压.Txt2, dataTable, tb_孕妇_产前随访1次.血压2);
            DataBinder.BindingTextEdit(textEdit心脏, dataTable, tb_孕妇_产前随访1次.心脏异常);
            DataBinder.BindingTextEdit(textEdit肺部, dataTable, tb_孕妇_产前随访1次.肺部异常);
            DataBinder.BindingTextEdit(textEdit外阴, dataTable, tb_孕妇_产前随访1次.妇科外阴异常);
            DataBinder.BindingTextEdit(textEdit阴道, dataTable, tb_孕妇_产前随访1次.妇科阴道异常);
            DataBinder.BindingTextEdit(textEdit宫颈, dataTable, tb_孕妇_产前随访1次.妇科宫颈异常);
            DataBinder.BindingTextEdit(textEdit子宫, dataTable, tb_孕妇_产前随访1次.妇科子宫异常);
            DataBinder.BindingTextEdit(textEdit附件, dataTable, tb_孕妇_产前随访1次.妇科附件异常);
            DataBinder.BindingTextEdit(uc血红蛋白值.Txt1, dataTable, tb_孕妇_产前随访1次.血红蛋白);
            DataBinder.BindingTextEdit(uc白细胞计数值.Txt1, dataTable, tb_孕妇_产前随访1次.白细胞计数);
            DataBinder.BindingTextEdit(uc血小板计数值.Txt1, dataTable, tb_孕妇_产前随访1次.血小板计数);
            DataBinder.BindingTextEdit(uc血常规其他.Txt1, dataTable, tb_孕妇_产前随访1次.血常规其他);
            DataBinder.BindingTextEdit(uc尿蛋白.Txt1, dataTable, tb_孕妇_产前随访1次.尿蛋白);
            DataBinder.BindingTextEdit(uc尿糖.Txt1, dataTable, tb_孕妇_产前随访1次.尿糖);
            DataBinder.BindingTextEdit(uc尿酮体.Txt1, dataTable, tb_孕妇_产前随访1次.尿酮体);
            DataBinder.BindingTextEdit(uc尿潜血.Txt1, dataTable, tb_孕妇_产前随访1次.尿潜血);
            DataBinder.BindingTextEdit(uc尿常规其他.Txt1, dataTable, tb_孕妇_产前随访1次.尿常规其他);
            DataBinder.BindingTextEdit(uc血糖.Txt1, dataTable, tb_孕妇_产前随访1次.血糖);
            DataBinder.BindingTextEdit(uc血清谷丙转氨酶.Txt1, dataTable, tb_孕妇_产前随访1次.血清谷丙转氨酶);
            DataBinder.BindingTextEdit(uc血清谷草转氨酶.Txt1, dataTable, tb_孕妇_产前随访1次.血清谷草转氨酶);
            DataBinder.BindingTextEdit(uc白蛋白.Txt1, dataTable, tb_孕妇_产前随访1次.白蛋白);
            DataBinder.BindingTextEdit(uc总胆红素.Txt1, dataTable, tb_孕妇_产前随访1次.总胆红素);
            DataBinder.BindingTextEdit(uc结合胆红素.Txt1, dataTable, tb_孕妇_产前随访1次.结合胆红素);
            DataBinder.BindingTextEdit(uc血清肌酐.Txt1, dataTable, tb_孕妇_产前随访1次.血清肌酐);
            DataBinder.BindingTextEdit(uc血尿素氮.Txt1, dataTable, tb_孕妇_产前随访1次.血尿素氮);

            DataBinder.BindingTextEdit(textEdit阴道分泌物其他, dataTable, tb_孕妇_产前随访1次.阴道分泌物其他);
            DataBinder.BindingTextEdit(uc乙型肝炎表面抗原.Txt1, dataTable, tb_孕妇_产前随访1次.乙型肝炎表面抗原);
            DataBinder.BindingTextEdit(uc乙型肝炎表面抗体.Txt1, dataTable, tb_孕妇_产前随访1次.乙型肝炎表面抗体);
            DataBinder.BindingTextEdit(uc乙型肝炎e抗原.Txt1, dataTable, tb_孕妇_产前随访1次.乙型肝炎e抗原);
            DataBinder.BindingTextEdit(uc乙型肝炎e抗体.Txt1, dataTable, tb_孕妇_产前随访1次.乙型肝炎e抗体);
            DataBinder.BindingTextEdit(uc乙型肝炎核心抗体.Txt1, dataTable, tb_孕妇_产前随访1次.乙型肝炎核心抗体);
            DataBinder.BindingTextEdit(textEditB超, dataTable, tb_孕妇_产前随访1次.B超);
            DataBinder.BindingTextEdit(textEdit总体评估异常, dataTable, tb_孕妇_产前随访1次.总体评估其他);
            DataBinder.BindingTextEdit(txt保健指导其他, dataTable, tb_孕妇_产前随访1次.保健指导其他);
            DataBinder.BindingTextEdit(txt建册机构名称, dataTable, tb_孕妇_产前随访1次.其他建册机构名称);

            DataBinder.BindingTextEdit(uc转诊原因.Txt1, dataTable, tb_孕妇_产前随访1次.转诊原因);
            DataBinder.BindingTextEdit(uc机构及科室.Txt1, dataTable, tb_孕妇_产前随访1次.转诊机构及科室);
            DataBinder.BindingTextEdit(uc转诊联系人.Txt1, dataTable, tb_孕妇_产前随访1次.联系人);
            DataBinder.BindingTextEdit(uc联系方式.Txt1, dataTable, tb_孕妇_产前随访1次.联系方式);

            DataBinder.BindingTextEditDateTime(dte下次随访日期, dataTable, tb_孕妇_产前随访1次.下次随访日期);
            DataBinder.BindingTextEdit(textEdit随访医生, dataTable, tb_孕妇_产前随访1次.随访医师);
            DataBinder.BindingTextEdit(txt家属签名, dataTable, tb_孕妇_产前随访1次.家属签名);
            SetFlowLayoutResult(dataTable.Rows[0][tb_孕妇_产前随访1次.保健指导].ToString(), flow保健指导);
            SetFlowLayoutResult(dataTable.Rows[0][tb_孕妇_产前随访1次.个人史].ToString(), flow个人史);
            SetFlowLayoutResult(dataTable.Rows[0][tb_孕妇_产前随访1次.家族史].ToString(), flow家族史);
            SetFlowLayoutResult(dataTable.Rows[0][tb_孕妇_产前随访1次.既往病史].ToString(), flow既往病史);

            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.妇科手术史].ToString(), cbo妇科手术史);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.孕次].ToString(), textEdit孕次);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.心脏].ToString(), cbo心脏);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.肺部].ToString(), cbo肺部);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.妇科外阴].ToString(), cbo外阴);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.妇科阴道].ToString(), cbo阴道);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.妇科宫颈].ToString(), cbo宫颈);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.妇科子宫].ToString(), cbo子宫);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.妇科附件].ToString(), cbo附件);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.血型].ToString(), cbo血型);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.RH].ToString(), cboRH);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.阴道分泌物].ToString(), cbo阴道分泌物);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.阴道清洁度].ToString(), cbo阴道清洁度);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.梅毒血清学试验].ToString(), cbo梅毒血清学试验);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.HIV抗体检测].ToString(), cboHIV抗体检测);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.总体评估].ToString(), cbo总体评估);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.转诊].ToString(), cbo转诊有无);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.建册情况].ToString(), cbo建册情况);
            util.ControlsHelper.SetComboxData(dataTable.Rows[0][tb_孕妇_产前随访1次.结果].ToString(), cbo是否到位);

        }
        private void InitView()
        {
            this.uc身高.Txt1.EditValueChanged += Txt1_EditValueChanged;
            this.uc体重.Txt1.EditValueChanged += Txt1_EditValueChanged;
            this.dte末次月经.Width = 90;
            this.txt个人史其他.Width = 80;

            this.uc转诊原因.Txt1.Enabled = false;
            this.uc机构及科室.Txt1.Enabled = false;

            this.uc血红蛋白值.Margin = new Padding(0);
            this.uc白细胞计数值.Margin = new Padding(0);
            this.uc血小板计数值.Margin = new Padding(0);
            this.uc血常规其他.Margin = new Padding(0);

            this.uc血清谷丙转氨酶.Margin = new Padding(0);
            this.uc血清谷草转氨酶.Margin = new Padding(0);
            this.uc白蛋白.Margin = new Padding(0);
            this.uc总胆红素.Margin = new Padding(0);
            this.uc结合胆红素.Margin = new Padding(0);

            this.uc尿蛋白.Margin = new Padding(0);
            this.uc尿蛋白.Txt1.Width = 80;
            this.uc尿蛋白.Width = 170;
            this.uc尿糖.Margin = new Padding(0);
            this.uc尿糖.Txt1.Width = 80;
            this.uc尿糖.Width = 170;

            this.uc尿酮体.Margin = new Padding(0);
            this.uc尿酮体.Txt1.Width = 80;
            this.uc尿酮体.Width = 170;
            this.uc尿潜血.Margin = new Padding(0);
            this.uc尿潜血.Txt1.Width = 80;
            this.uc尿潜血.Width = 170;
            this.uc尿常规其他.Margin = new Padding(0);
            this.uc尿常规其他.Txt1.Width = 80;
            this.uc尿常规其他.Width = 170;
            this.uc填表孕周.Txt1.Properties.Mask.EditMask = "n0";
            this.uc填表孕周.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.uc填表孕周.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.uc填表孕周.Txt2.Properties.Mask.EditMask = "n0";
            this.uc填表孕周.Txt2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.uc填表孕周.Txt2.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.textEdit孕妇年龄.Properties.Mask.EditMask = "n0";
            this.textEdit孕妇年龄.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.textEdit孕妇年龄.Properties.Mask.UseMaskAsDisplayFormat = true;

            this.uc产次.Txt1.Width = 50;
            this.uc产次.Txt2.Width = 50;
            this.uc流产.Txt1.Width = 50;
            this.uc死胎.Txt1.Width = 50;
            this.uc死产.Txt1.Width = 50;
            this.uc新生儿死亡.Txt1.Width = 50;
            this.uc出生缺陷儿.Txt1.Width = 50;
            this.uc流产.Width = 90;
            this.uc死胎.Width = 90;
            this.uc死产.Width = 90;
            //修改记录时，不允许修改 孕次
            if (_UpdateType == UpdateType.Modify)
            {
                this.textEdit孕次.Properties.ReadOnly = false;
            }
        }

        void Txt1_EditValueChanged(object sender, EventArgs e)
        {
            Decimal 身高 = 0;
            Decimal 体重 = 0;
            Decimal BMI = 0;
            if (string.IsNullOrEmpty(this.uc身高.Txt1.Text) || string.IsNullOrEmpty(this.uc体重.Txt1.Text)) return;
            if (Decimal.TryParse(this.uc身高.Txt1.Text, out 身高) && Decimal.TryParse(this.uc体重.Txt1.Text, out 体重))
            {
                if (身高 != 0)
                {
                    BMI = Math.Round(体重 / ((身高 / 100) * (身高 / 100)), 2);
                }
                textEdit体重指数.Text = BMI.ToString();
            }
        }
        #endregion

        #region Handler Events
        private void UC第1次产前随访服务记录表_Load(object sender, EventArgs e)
        {

            InitView();
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无, cbo妇科手术史, false);
            this.cbo妇科手术史.SelectedIndex = 2;
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无, cbo转诊有无, false);
            this.cbo转诊有无.SelectedIndex = 2;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo心脏, false);
            this.cbo心脏.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo肺部, false);
            this.cbo肺部.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo外阴, false);
            this.cbo外阴.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo阴道, false);
            this.cbo阴道.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo宫颈, false);
            this.cbo宫颈.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo子宫, false);
            this.cbo子宫.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo附件, false);
            this.cbo附件.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t有无异常, cbo总体评估, false);
            this.cbo总体评估.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t建册情况, cbo建册情况, false);
            this.cbo建册情况.SelectedIndex = 1;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t是否到位, cbo是否到位, false);


            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t血型, cbo血型, false);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.tRH, cboRH, false);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t阴道分泌物, cbo阴道分泌物, false);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t阴道清洁度, cbo阴道清洁度, false);

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t阴性阳性, cbo梅毒血清学试验, false);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(AtomEHR.Business.DataDictCache.Cache.t阴性阳性, cboHIV抗体检测, false);

            DoBindingSummaryEditor(_ds孕妇产前一次随访);//绑定数据
        }
        private void chk末次月经_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk末次月经.Checked)
            {
                this.dte末次月经.Enabled = false;
                this.dte末次月经.Text = "";
            }
            else
            {
                this.dte末次月经.Enabled = true;
            }
        }
        private void chk既往病史有无_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk既往病史有无.Checked)
            {
                this.checkEdit3.Checked = this.checkEdit4.Checked = this.checkEdit5.Checked = this.checkEdit6.Checked = this.checkEdit7.Checked = this.checkEdit8.Checked = this.checkEdit9.Checked = this.checkEdit10.Checked = this.checkEdit11.Checked = this.checkEdit12.Checked = this.checkEdit13.Checked = this.checkEdit14.Checked = this.checkEdit15.Checked = false;
                this.textEdit既往病史其他.Text = "";
            }

            this.checkEdit3.Enabled = this.checkEdit4.Enabled = this.checkEdit5.Enabled = this.checkEdit6.Enabled = this.checkEdit7.Enabled = this.checkEdit8.Enabled = this.checkEdit9.Enabled = this.checkEdit10.Enabled = this.checkEdit11.Enabled = this.checkEdit12.Enabled = this.checkEdit13.Enabled = this.checkEdit14.Enabled = this.checkEdit15.Enabled = this.textEdit既往病史其他.Enabled = !this.chk既往病史有无.Checked;
        }
        private void chk家族史6_CheckedChanged(object sender, EventArgs e)
        {
            if (chk家族史6.Checked)
            {
                this.textEdit家族史其他.Enabled = true;
                this.textEdit家族史其他.Text = "";
            }
            else
            {
                this.textEdit家族史其他.Enabled = false;
            }
        }
        private void chk家族史0_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk家族史0.Checked)
            {
                this.chk家族史1.Checked = this.chk家族史2.Checked = this.chk家族史3.Checked = this.chk家族史4.Checked = this.chk家族史5.Checked = this.chk家族史6.Checked = false;
                this.textEdit家族史其他.Text = "";
            }

            this.chk家族史1.Enabled = this.chk家族史2.Enabled = this.chk家族史3.Enabled = this.chk家族史4.Enabled = this.chk家族史5.Enabled = this.chk家族史6.Enabled = this.textEdit家族史其他.Enabled = !this.chk家族史0.Checked;
        }
        private void chk个人史6_CheckedChanged(object sender, EventArgs e)
        {
            if (chk个人史6.Checked)
            {
                this.txt个人史其他.Enabled = true;
                this.txt个人史其他.Text = "";
            }
            else
            {
                this.txt个人史其他.Enabled = false;
            }
        }
        private void chk个人史0_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk个人史0.Checked)
            {
                this.chk个人史1.Checked = this.chk个人史2.Checked = this.chk个人史3.Checked = this.chk个人史4.Checked = this.chk个人史5.Checked = this.chk个人史6.Checked = false;
                this.txt个人史其他.Text = "";
            }

            this.chk个人史1.Enabled = this.chk个人史2.Enabled = this.chk个人史3.Enabled = this.chk个人史4.Enabled = this.chk个人史5.Enabled = this.chk个人史6.Enabled = this.txt个人史其他.Enabled = !this.chk个人史0.Checked;
        }
        private void chk保健指导6_CheckedChanged(object sender, EventArgs e)
        {
            if (chk保健指导6.Checked)
            {
                this.txt保健指导其他.Enabled = true;
                if (_dt一次随访 != null && _dt一次随访.Rows.Count == 1)
                {
                    if (_dt一次随访.Rows[0][tb_孕妇_产前随访1次.保健指导其他] != null && !string.IsNullOrEmpty(_dt一次随访.Rows[0][tb_孕妇_产前随访1次.保健指导其他].ToString()))
                    {
                        txt保健指导其他.EditValue = _dt一次随访.Rows[0][tb_孕妇_产前随访1次.保健指导其他].ToString();
                    }
                    else
                    {
                        txt保健指导其他.EditValue = "低盐饮食";
                    }
                }

            }
            else
            {
                this.txt保健指导其他.EditValue = "";
                this.txt保健指导其他.Enabled = false;
            }
        }
        private void cbo妇科手术史_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbo妇科手术史.Text.Trim() == "有")
            {
                this.textEdit妇科手术史.Enabled = true;
            }
            else
            {
                this.textEdit妇科手术史.Enabled = false;
                this.textEdit妇科手术史.Text = "";

            }
        }
        private void cbo心脏_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextEditEnable(sender, textEdit心脏);
        }
        private void SetTextEditEnable(object sender, DevExpress.XtraEditors.TextEdit textEdit)
        {
            ComboBoxEdit cbo = sender as ComboBoxEdit;
            if (cbo == null) return;
            string index = cbo.Text.Trim();
            if (!string.IsNullOrEmpty(index))
            {
                if (index == "异常" || index == "已在其他机构建册")
                {
                    textEdit.Enabled = true;
                }
                else
                {
                    textEdit.Enabled = false;
                    textEdit.Text = "";
                }
            }
        }
        private void cbo肺部_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextEditEnable(sender, textEdit肺部);
        }
        private void cbo外阴_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextEditEnable(sender, textEdit外阴);
        }
        private void cbo阴道_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextEditEnable(sender, textEdit阴道);
        }
        private void cbo宫颈_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextEditEnable(sender, textEdit宫颈);
        }
        private void cbo子宫_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextEditEnable(sender, textEdit子宫);
        }
        private void cbo附件_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextEditEnable(sender, textEdit附件);
        }
        private void cbo总体评估_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextEditEnable(sender, textEdit总体评估异常);
        }
        private void cbo转诊有无_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.cbo转诊有无.Text.Trim();
            if (!string.IsNullOrEmpty(index) && index == "有")
            {
                this.uc转诊原因.Txt1.Enabled = true;
                this.uc机构及科室.Txt1.Enabled = true;
                this.uc转诊联系人.Txt1.Enabled = true;
                this.uc联系方式.Txt1.Enabled = true;
                this.uc转诊联系人.Txt1.Enabled = true;
                this.cbo是否到位.Enabled = true;
            }
            else
            {
                this.uc转诊原因.Txt1.Enabled = false;
                this.uc机构及科室.Txt1.Enabled = false;
                this.uc转诊原因.Txt1.Text = "";
                this.uc机构及科室.Txt1.Text = "";
                this.uc转诊联系人.Txt1.Enabled = false;
                this.uc联系方式.Txt1.Enabled = false;
                this.uc转诊联系人.Txt1.Enabled = false;
                this.cbo是否到位.Enabled = false;
                this.uc转诊联系人.Txt1.Text = "";
                this.uc联系方式.Txt1.Text = "";
                this.uc转诊联系人.Txt1.Text = "";
                this.cbo是否到位.Text = "";

            }
        }
        private void cbo建册情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetTextEditEnable(sender, txt建册机构名称);
        }

        #endregion

        private void btn保存_Click(object sender, EventArgs e)
        {
            if (funCheck())
            {
                if ((_UpdateType == UpdateType.Add && Msg.AskQuestion("信息保存后，‘随访日期’和‘下次随访时间’将不允许修改，确认保存信息？")) || _UpdateType == UpdateType.Modify)
                {
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.填表日期] = this.dte填表日期.Text;
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.下次随访日期] = this.dte下次随访日期.Text;
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.预产期] = this.dte预产期.Text;
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.末次月经] = this.chk末次月经.Checked ? "" : this.dte末次月经.Text;
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.个人档案编号] = _docNo;
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.保健指导] = GetFlowLayoutResult(flow保健指导);
                    if (this.chk保健指导6.Checked)//其他
                    {
                        _dt一次随访.Rows[0][tb_孕妇_产前随访1次.保健指导其他] = this.txt保健指导其他.Text.Trim();
                    }
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.个人史] = GetFlowLayoutResult(flow个人史);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.家族史] = GetFlowLayoutResult(flow家族史);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.既往病史] = GetFlowLayoutResult(flow既往病史);

                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.孕次] = util.ControlsHelper.GetComboxKey(textEdit孕次);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.妇科手术史] = util.ControlsHelper.GetComboxKey(cbo妇科手术史);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.心脏] = util.ControlsHelper.GetComboxKey(cbo心脏);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.肺部] = util.ControlsHelper.GetComboxKey(cbo肺部);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.妇科外阴] = util.ControlsHelper.GetComboxKey(cbo外阴);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.妇科阴道] = util.ControlsHelper.GetComboxKey(cbo阴道);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.妇科宫颈] = util.ControlsHelper.GetComboxKey(cbo宫颈);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.妇科子宫] = util.ControlsHelper.GetComboxKey(cbo子宫);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.妇科附件] = util.ControlsHelper.GetComboxKey(cbo附件);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.阴道分泌物] = util.ControlsHelper.GetComboxKey(cbo阴道分泌物);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.阴道清洁度] = util.ControlsHelper.GetComboxKey(cbo阴道清洁度);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.梅毒血清学试验] = util.ControlsHelper.GetComboxKey(cbo梅毒血清学试验);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.HIV抗体检测] = util.ControlsHelper.GetComboxKey(cboHIV抗体检测);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.血型] = util.ControlsHelper.GetComboxKey(cbo血型);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.RH] = util.ControlsHelper.GetComboxKey(cboRH);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.总体评估] = util.ControlsHelper.GetComboxKey(cbo总体评估);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.转诊] = util.ControlsHelper.GetComboxKey(cbo转诊有无);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.建册情况] = util.ControlsHelper.GetComboxKey(cbo建册情况);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.结果] = util.ControlsHelper.GetComboxKey(cbo是否到位);
                    int i = Get缺项();
                    int j = Get完整度(i);
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.缺项] = i;
                    _dt一次随访.Rows[0][tb_孕妇_产前随访1次.完整度] = j;
                    if (_UpdateType == UpdateType.Add)
                    {
                        _dt一次随访.Rows[0][tb_孕妇_产前随访1次.创建机构] = Loginer.CurrentUser.所属机构;
                        _dt一次随访.Rows[0][tb_孕妇_产前随访1次.所属机构] = Loginer.CurrentUser.所属机构;
                        _dt一次随访.Rows[0][tb_孕妇_产前随访1次.创建时间] = this.textEdit创建时间.Text;
                        _dt一次随访.Rows[0][tb_孕妇_产前随访1次.创建人] = Loginer.CurrentUser.用户编码;
                        _dt一次随访.Rows[0][tb_孕妇_产前随访1次.修改人] = Loginer.CurrentUser.用户编码;
                        _dt一次随访.Rows[0][tb_孕妇_产前随访1次.修改时间] = this.textEdit最近更新时间.Text;
                    }
                    else if (_UpdateType == UpdateType.Modify)
                    {
                        _dt一次随访.Rows[0][tb_孕妇_产前随访1次.修改人] = Loginer.CurrentUser.用户编码;
                        _dt一次随访.Rows[0][tb_孕妇_产前随访1次.修改时间] = this.textEdit最近更新时间.Text;
                    }
                    _dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.第一次产前检查表] = i + "," + j;

                    //TfbYunInfo tfbYunInfo = this.tfbYunInfoMapper.selectByPrimaryKey(dGrdabh);
                    if ((_dt个人健康档案.Rows[0][tb_健康档案.怀孕情况] != null) && ("已孕未生产" == _dt个人健康档案.Rows[0][tb_健康档案.怀孕情况].ToString()) && (_dt个人健康档案.Rows[0][tb_健康档案.孕次].ToString() == util.ControlsHelper.GetComboxKey(this.textEdit孕次)))
                    {
                        _dt孕妇基本信息.Rows[0][tb_孕妇基本信息.下次随访时间] = dte下次随访日期.Text;

                    }

                    if(arrImageBytes!=null)
                    {
                        if(string.IsNullOrWhiteSpace(_dt一次随访.Rows[0][tb_孕妇_产前随访1次.SFID].ToString()))
                        {
                            _dt一次随访.Rows[0][tb_孕妇_产前随访1次.SFID] = Guid.NewGuid().ToString().Replace("-","");
                            bll孕产妇Image.GetBusinessByKey("-1", true);
                            bll孕产妇Image.NewBusiness();
                            bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.档案号] = _dt一次随访.Rows[0][tb_孕妇_产前随访1次.个人档案编号];
                            bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICID] = _dt一次随访.Rows[0][tb_孕妇_产前随访1次.SFID];
                            bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICBYTE] = arrImageBytes;
                            bll孕产妇Image.Save(bll孕产妇Image.CurrentBusiness);
                        }
                        else
                        {
                            string sfid = _dt一次随访.Rows[0][tb_孕妇_产前随访1次.SFID].ToString();
                            bll孕产妇Image.GetBusinessByDahAndPicid(_dt一次随访.Rows[0][tb_孕妇_产前随访1次.个人档案编号].ToString(), sfid, true);
                            //bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.档案号] = _dt一次随访.Rows[0][tb_孕妇_产前随访1次.个人档案编号];
                            //bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICID] = _dt一次随访.Rows[0][tb_孕妇_产前随访1次.SFID];
                            bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.createuser] =Loginer.CurrentUser.用户编码;
                            bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.createtime] = bll孕产妇Image.ServiceDateTime;
                            bll孕产妇Image.CurrentBusiness.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICBYTE] = arrImageBytes;
                            bll孕产妇Image.Save(bll孕产妇Image.CurrentBusiness);
                        }
                    }

                    //changed by wjz 20151108 孕产妇第一次随访记录无法保存，改之 ▽
                    #region old code
                    if (_UpdateType == UpdateType.Modify) _Bll.WriteLog();
                    DataSet dsTemplate = _Bll.CreateSaveData(_ds孕妇产前一次随访, _UpdateType); //创建用于保存的临时数据
                    SaveResult result = _Bll.Save(dsTemplate);//调用业务逻辑保存数据方法
                    #endregion
                    //SaveResult result = _Bll.Save(_ds孕妇产前一次随访);//调用业务逻辑保存数据方法
                    //changed by wjz 20151108 孕产妇第一次随访记录无法保存，改之 △

                    if (result.Success) //保存成功, 不需要重新加载数据，更新当前的缓存数据就行．
                    {
                        //if (_UpdateType == UpdateType.Modify) _BLL.NotifyUser();//修改后通知创建人

                        //this.DoBindingSummaryEditor(_BLL.DataBinder); //重新显示数据
                        this._UpdateType = UpdateType.None;
                        Msg.ShowInformation("保存成功!");
                        //保存成功后跳转到显示页面
                        UC第1次产前随访服务记录表_显示 uc = new UC第1次产前随访服务记录表_显示(_frm);
                        ShowControl(uc, DockStyle.Fill);
                    }
                    else
                        Msg.Warning("保存失败!");
                }
            }
        }
        /// <summary>
        /// 获取缺项
        /// </summary>
        /// <returns></returns>
        private int Get缺项()
        {
            int i = 0;
            if (string.IsNullOrEmpty(dte填表日期.Text)) i++;
            if (string.IsNullOrEmpty(uc填表孕周.Txt1.Text) && string.IsNullOrEmpty(uc填表孕周.Txt2.Text)) i++;
            if (string.IsNullOrEmpty(textEdit孕妇年龄.Text)) i++;
            if (string.IsNullOrEmpty(textEdit丈夫姓名.Text)) i++;
            if (string.IsNullOrEmpty(textEdit丈夫年龄.Text)) i++;
            if (string.IsNullOrEmpty(textEdit丈夫电话.Text)) i++;
            if (string.IsNullOrEmpty(textEdit孕次.Text)) i++;
            if (string.IsNullOrEmpty(uc产次.Txt1.Text) && string.IsNullOrEmpty(uc产次.Txt2.Text)) i++;

            if (GetFlowLayoutResult(flow个人史) == "") i++;
            if (GetFlowLayoutResult(flow既往病史) == "") i++;
            if (GetFlowLayoutResult(flow家族史) == "") i++;
            if (this.cbo妇科手术史.Text == "请选择") i++;
            if (string.IsNullOrEmpty(this.uc流产.Txt1.Text) && string.IsNullOrEmpty(this.uc死胎.Txt1.Text) && string.IsNullOrEmpty(this.uc死产.Txt1.Text) && string.IsNullOrEmpty(this.uc新生儿死亡.Txt1.Text) && string.IsNullOrEmpty(this.uc出生缺陷儿.Txt1.Text)) i++;
            if (string.IsNullOrEmpty(this.uc身高.Txt1.Text)) i++;
            if (string.IsNullOrEmpty(this.uc体重.Txt1.Text)) i++;
            if (string.IsNullOrEmpty(this.uc血压.Txt1.Text) || string.IsNullOrEmpty(this.uc血压.Txt2.Text)) i++;

            if (this.cbo心脏.Text == "请选择" || this.cbo肺部.Text == "请选择") i++;
            if (this.cbo肺部.Text == "请选择" || this.cbo外阴.Text == "请选择" || this.cbo阴道.Text == "请选择"
            || this.cbo宫颈.Text == "请选择" || this.cbo子宫.Text == "请选择" || this.cbo附件.Text == "请选择") i++;

            if (string.IsNullOrEmpty(this.uc血红蛋白值.Txt1.Text) || string.IsNullOrEmpty(this.uc白细胞计数值.Txt1.Text) || string.IsNullOrEmpty(this.uc血小板计数值.Txt1.Text)) i++;

            if (string.IsNullOrEmpty(this.uc尿蛋白.Txt1.Text) || string.IsNullOrEmpty(this.uc尿糖.Txt1.Text) || string.IsNullOrEmpty(this.uc尿酮体.Txt1.Text) || string.IsNullOrEmpty(this.uc尿潜血.Txt1.Text)) i++;

            if (string.IsNullOrEmpty(this.uc血清谷丙转氨酶.Txt1.Text) || string.IsNullOrEmpty(this.uc血清谷丙转氨酶.Txt1.Text) || string.IsNullOrEmpty(this.uc白蛋白.Txt1.Text) || string.IsNullOrEmpty(this.uc总胆红素.Txt1.Text) || string.IsNullOrEmpty(this.uc结合胆红素.Txt1.Text)) i++;

            if (string.IsNullOrEmpty(this.uc血清肌酐.Txt1.Text) || string.IsNullOrEmpty(this.uc血尿素氮.Txt1.Text)) i++;

            if (string.IsNullOrEmpty(this.uc乙型肝炎表面抗原.Txt1.Text) || string.IsNullOrEmpty(this.uc乙型肝炎表面抗体.Txt1.Text) || string.IsNullOrEmpty(this.uc乙型肝炎e抗原.Txt1.Text) || string.IsNullOrEmpty(this.uc乙型肝炎e抗体.Txt1.Text) || string.IsNullOrEmpty(this.uc乙型肝炎核心抗体.Txt1.Text)) i++;

            if (this.cbo血型.Text == "请选择") i++;

            if (this.cbo总体评估.Text == "请选择") i++;
            if (GetFlowLayoutResult(flow保健指导) == "") i++;
            if (this.cbo建册情况.Text == "请选择") i++;
            if (this.cbo转诊有无.Text == "请选择") i++;
            if (this.dte下次随访日期.Text == "") i++;
            if (this.textEdit随访医生.Text.Trim() == "") i++;

            return i;
        }
        public int Get完整度(int i)
        {
            int k = Convert.ToInt32((29 - i) * 100 / 29.0);
            return k;
        }
        private bool funCheck()
        {
            if (this.dte填表日期.Text.Trim() == "")
            {
                Msg.Warning("填表日期是必填项，请填写填表日期！");
                this.dte填表日期.Focus();
                return false;
            }
            if (this.dte下次随访日期.Text.Trim() == "")
            {
                Msg.Warning("下次随访日期是必填项，请填写下次随访日期！");
                this.dte下次随访日期.Focus();
                return false;
            }
            if (this.dte下次随访日期.DateTime < this.dte填表日期.DateTime)
            {
                Msg.Warning("下次随访日期不能小于填表日期，请修改下次随访日期！");
                this.dte下次随访日期.Focus();
                return false;
            }
            if (dte填表日期.DateTime > Convert.ToDateTime(this.textEdit创建时间.Text))
            {
                Msg.ShowInformation("随访日期不能大于填写日期");
                dte填表日期.Focus();
                return false;
            }
            string ycs = util.ControlsHelper.GetComboxKey(textEdit孕次);
            //if (string.IsNullOrEmpty(ycs))
            //{
            //    Msg.Warning("请选择孕次！");
            //    this.textEdit孕次.Focus();
            //    return false;
            //}
            if (_UpdateType == UpdateType.Add && _Bll.CheckExistsByYunCi(_docNo, ycs))
            {
                Msg.Warning("孕次为" + ycs + "的第1次产前随访表已建立,请重新填写!");
                this.textEdit孕次.Focus();
                return false;
            }


            return true;
        }

        private void cbo阴道分泌物_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.cbo阴道分泌物.Text.Trim();
            if (index == "其他")
            {
                this.textEdit阴道分泌物其他.Enabled = true;
            }
            else
            {
                this.textEdit阴道分泌物其他.Enabled = false;
                this.textEdit阴道分泌物其他.Text = "";
            }
        }

        private void dte末次月经_Leave(object sender, EventArgs e)
        {
            DateTime date = this.dte末次月经.DateTime;
            DateTime yuchanqi;
            if (date.Month + 9 > 12)
            {
                yuchanqi = date.AddMonths(-3).AddYears(1).AddDays(7);
            }
            else
            {
                yuchanqi = date.AddMonths(9).AddDays(7);
            }
            this.dte预产期.DateTime = yuchanqi;

        }

        private void btn读取_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textEdit身份证号.Text))
            { Msg.ShowInformation("请完善身份证信息！"); return; }

            DataTable dt列表 = bll获取化验信息.Get化验信息列表(this.textEdit身份证号.Text.ToString());
            if (dt列表 != null && dt列表.Rows.Count > 0)
            {
                DataTable dt明细 = null;
                InterFace化验结果 intf = new InterFace化验结果(dt列表);
                if (intf.ShowDialog() == DialogResult.OK)
                {
                    dt明细 = bll获取化验信息.Get化验信息(this.textEdit身份证号.Text.ToString(), intf.s化验日期);
                }
                else
                {
                    Msg.ShowInformation("用户取消了选择！");
                    return;
                }
                //Msg.ShowInformation(dt.Rows.Count.ToString());
                foreach (DataRow dr in dt明细.Rows)
                {
                    #region 血常规
                    if (dr["fitem_code"].ToString() == "HGB" || dr["fname_j"].ToString() == "HGB")
                    {//血红蛋白
                        this.uc血红蛋白值.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "WBC" || dr["fname_j"].ToString() == "WBC")
                    {//白细胞
                        this.uc白细胞计数值.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "PLT" || dr["fname_j"].ToString() == "PLT")
                    {//血小板
                        this.uc血小板计数值.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion

                    #region 尿常规 未做
                    if (dr["fitem_code"].ToString() == "PRO" || dr["fname_j"].ToString() == "PRO")
                    {//尿蛋白
                        this.uc尿蛋白.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "GLU1" || dr["fname_j"].ToString() == "GLU1")
                    {//尿糖
                        this.uc尿糖.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "KET" || dr["fname_j"].ToString() == "KET")
                    {//尿酮体：KET 
                        this.uc尿酮体.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "OB" || dr["fname_j"].ToString() == "OB")
                    {//尿潜血：OB--Occult Blood
                        this.uc尿潜血.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion

                    #region 血糖/葡萄糖 空腹血糖、餐后2H血糖、尿微量白蛋白、糖化血红蛋白
                    if (dr["fitem_code"].ToString() == "GLU" || dr["fname_j"].ToString() == "GLU")
                    {//空腹血糖
                        this.uc血糖.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "mAlb" || dr["fname_j"].ToString() == "mAlb")
                    {//尿微量白蛋白英文缩写:mAlb
                        //this.尿微量白蛋白.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "PBC2h" || dr["fname_j"].ToString() == "PBC2h")
                    {//餐后2小时血糖(PBC2h)
                        //this.txt餐后2H血糖.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "HbA1c" || dr["fname_j"].ToString() == "HbA1c")
                    {//糖化血红蛋白：HbA1c
                        //this.txt糖化血红蛋白.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion

                    #region 肝功能
                    if (dr["fitem_code"].ToString() == "ALT" || dr["fname_j"].ToString() == "ALT")
                    {//谷丙转氨酶
                        this.uc血清谷丙转氨酶.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "AST" || dr["fname_j"].ToString() == "AST")
                    {//谷草转氨酶
                        this.uc血清谷草转氨酶.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "ALB" || dr["fname_j"].ToString() == "ALB")
                    {//白蛋白
                        this.uc白蛋白.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "TBIL" || dr["fname_j"].ToString() == "TBIL")
                    {//总胆红素
                        this.uc总胆红素.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "DBIL" || dr["fname_j"].ToString() == "DBIL")
                    {//直接胆红素
                        this.uc结合胆红素.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion

                    #region 肾功能
                    if (dr["fitem_code"].ToString() == "CRE" || dr["fname_j"].ToString() == "CRE")
                    {//肌酐
                        this.uc血清肌酐.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "UREA" || dr["fname_j"].ToString() == "UREA")
                    {//尿素氮
                        this.uc血尿素氮.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "K+" || dr["fname_j"].ToString() == "K+")
                    {//血钾浓度
                        //this.血钾浓度.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "Na+" || dr["fname_j"].ToString() == "Na+")
                    {//血钠浓度
                        //this.血钠浓度.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion

                    #region 血脂
                    if (dr["fitem_code"].ToString() == "CHO" || dr["fname_j"].ToString() == "CHO")
                    {//总胆固醇
                        //this.总胆固醇.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "TG" || dr["fname_j"].ToString() == "TG")
                    {//甘油三酯
                        //this.txt甘油三酯.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "LDL-C" || dr["fname_j"].ToString() == "LDL-C")
                    {//低密度脂蛋白
                        //this.txt血清低密度脂蛋白胆固醇.Txt1.Text = dr["fvalue"].ToString();
                    }
                    if (dr["fitem_code"].ToString() == "HDL-C" || dr["fname_j"].ToString() == "HDL-C")
                    {//高密度脂蛋
                        //this.txt血清高密度脂蛋白胆固醇.Txt1.Text = dr["fvalue"].ToString();
                    }
                    #endregion
                }
            }
            else { Msg.ShowInformation("未获取到化验信息！"); }
        }

        private void dte填表日期_EditValueChanged(object sender, EventArgs e)
        {
            Cal孕周();
        }

        private void dte末次月经_EditValueChanged(object sender, EventArgs e)
        {
            Cal孕周();
        }

        private void Cal孕周()
        {
            if (!string.IsNullOrWhiteSpace(dte填表日期.Text) && !string.IsNullOrWhiteSpace(dte末次月经.Text) && !chk末次月经.Checked)
            {
                TimeSpan ts = dte填表日期.DateTime - dte末次月经.DateTime;
                if (ts.Days > 0)
                {
                    uc填表孕周.Txt1.Text = (ts.Days / 7).ToString();
                    uc填表孕周.Txt2.Text = (ts.Days % 7).ToString();
                }
                else
                {
                    uc填表孕周.Txt1.Text = "0";
                    uc填表孕周.Txt2.Text = "0";
                }
            }
        }

        private void sbtnCalYZ_Click(object sender, EventArgs e)
        {
            if (chk末次月经.Checked)
            {
                Msg.ShowInformation("末次月经不详，请填写末次月经日期。");
                return;
            }

            if (string.IsNullOrWhiteSpace(dte填表日期.Text) || string.IsNullOrWhiteSpace(dte末次月经.Text))
            {
                Msg.ShowInformation("请填写末次月经、填表日期。");
                return;
            }

            Cal孕周();
        }

        private byte[] arrImageBytes = null;

        bll孕产妇随访化验单 bll孕产妇Image = new bll孕产妇随访化验单();
        private void sbtn上传化验单图片_Click(object sender, EventArgs e)
        {
            FrmPIC frm = new FrmPIC();

            string sfid = _dt一次随访.Rows[0][tb_孕妇_产前随访1次.SFID].ToString();

            if(!string.IsNullOrWhiteSpace(sfid))
            {
                DataSet dstemp = bll孕产妇Image.GetBusinessByDahAndPicid(_docNo, sfid,false);

                byte[] arrbyte = dstemp.Tables[tb_孕产妇随访化验单.__TableName].Rows[0][tb_孕产妇随访化验单.PICBYTE] as byte[];

                frm.SetImage(arrbyte);
            }
            
            if(frm.ShowDialog()==DialogResult.OK)
            {
                arrImageBytes = frm.arrImageBytes;
            }
        }
    }
}
