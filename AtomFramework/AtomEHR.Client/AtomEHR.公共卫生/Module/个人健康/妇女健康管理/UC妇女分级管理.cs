﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business.Security;
using AtomEHR.Common;
using AtomEHR.Models;
using AtomEHR.Library;
using DevExpress.XtraEditors;
using AtomEHR.Interfaces;

namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    public partial class UC妇女分级管理 : UserControlBase
    {

        #region Fields
        AtomEHR.Business.bll妇女分级管理 _Bll = new Business.bll妇女分级管理();
        DataSet _ds妇女分级管理表;
        //frm个人健康 _frm;
        //string _docNo;
        string _serverDateTime;
        //string _id;//表的主键，通过id来查询一条数据
        //DataTable dt妇女基本信息;

        #endregion
        public UC妇女分级管理()
        {
            InitializeComponent();
        }

        public UC妇女分级管理(AtomEHR.Common.UpdateType _updateType,string ID)
        {
            InitializeComponent();
            //_frm = (frm个人健康)frm;
            //_docNo = _frm._docNo;
            _serverDateTime = _Bll.ServiceDateTime;
            _UpdateType = _updateType;
            //_id = _frm._param as string;
            if (_UpdateType == UpdateType.Add)//添加随访
            {
                _Bll.GetBusinessByKey("-", true);
                _Bll.NewBusiness();
                _ds妇女分级管理表 = _Bll.CurrentBusiness;
                this.layoutControlGroup1.Text = "妇女分级管理-新增";
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                _ds妇女分级管理表 = _Bll.GetOneDataByKey("", ID, true);
                this.layoutControlGroup1.Text = "妇女分级管理-修改";

            }
            DoBindingSummaryEditor(_ds妇女分级管理表);//绑定数据
        }
        private void DoBindingSummaryEditor(DataSet _ds妇女分级管理表s)
        {
            if (_ds妇女分级管理表s == null) return;
            //if (_ds妇女分级管理表.Tables.Count == 0) return;

            DataTable dt妇女分级管理 = _ds妇女分级管理表s.Tables[tb_妇女分级管理.__TableName];
            
            DataBinder.BindingTextEdit(txt卡号, dt妇女分级管理, tb_妇女分级管理.卡号);
            DataBinder.BindingTextEdit(txt档案编号, dt妇女分级管理, tb_妇女分级管理.个人档案编号);
            DataBinder.BindingTextEdit(txt姓名, dt妇女分级管理, tb_妇女分级管理.姓名);
            DataBinder.BindingTextEdit(txt身份证号, dt妇女分级管理, tb_妇女分级管理.身份证号);
            DataBinder.BindingTextEdit(txt出生日期, dt妇女分级管理, tb_妇女分级管理.出生日期);
            DataBinder.BindingTextEdit(txt联系电话, dt妇女分级管理, tb_妇女分级管理.联系电话);
            DataBinder.BindingTextEdit(txt居住状态, dt妇女分级管理, tb_妇女分级管理.常住类型);
            DataBinder.BindingTextEdit(txt居住地址, dt妇女分级管理, tb_妇女分级管理.居住地址);
            DataBinder.BindingTextEditDateTime(dte检查日期, dt妇女分级管理, tb_妇女分级管理.检查日期);

            DataBinder.BindingTextEdit(txt检查次数.Txt1, dt妇女分级管理, tb_妇女分级管理.检查次数);
            DataBinder.BindingTextEdit(txt现有症状, dt妇女分级管理, tb_妇女分级管理.现有症状);
            DataBinder.BindingTextEdit(txt曾患妇科病, dt妇女分级管理, tb_妇女分级管理.曾患妇科病);
            DataBinder.BindingTextEdit(txt曾做手术, dt妇女分级管理, tb_妇女分级管理.曾做手术);

            this.chk未孕.Checked = dt妇女分级管理.Rows[0][tb_妇女分级管理.未孕].ToString() == "on" ? false : true;
            DataBinder.BindingTextEdit(txt孕次.Txt1, dt妇女分级管理, tb_妇女分级管理.孕次);
            DataBinder.BindingTextEdit(txt产次.Txt1, dt妇女分级管理, tb_妇女分级管理.产次);
            DataBinder.BindingTextEdit(txt自然流产.Txt1, dt妇女分级管理, tb_妇女分级管理.自然流产);
            DataBinder.BindingTextEdit(txt人工流产.Txt1, dt妇女分级管理, tb_妇女分级管理.人工流产);
            DataBinder.BindingTextEdit(txt中孕引产.Txt1, dt妇女分级管理, tb_妇女分级管理.中孕引产);

            if (!string.IsNullOrEmpty(dt妇女分级管理.Rows[0][tb_妇女分级管理.不孕年数].ToString()) && Convert.ToInt32(dt妇女分级管理.Rows[0][tb_妇女分级管理.不孕年数]) != 0)
            {
                this.chk不孕史.Checked = false;
            }
            else
            {
                this.chk不孕史.Checked = true;
            }

            DataBinder.BindingTextEdit(txt不孕史时间.Txt1, dt妇女分级管理, tb_妇女分级管理.不孕年数);
            DataBinder.BindingTextEdit(txt不孕史男, dt妇女分级管理, tb_妇女分级管理.男方原因);
            DataBinder.BindingTextEdit(txt不孕史女, dt妇女分级管理, tb_妇女分级管理.女方原因);
            //乳腺检查
            DataBinder.BindingTextEdit(txt乳房, dt妇女分级管理, tb_妇女分级管理.乳房);
            DataBinder.BindingTextEdit(txt结节, dt妇女分级管理, tb_妇女分级管理.结节);
            DataBinder.BindingTextEdit(txt压痛, dt妇女分级管理, tb_妇女分级管理.压痛);
            DataBinder.BindingTextEdit(txt乳房左侧, dt妇女分级管理, tb_妇女分级管理.乳房左侧);
            DataBinder.BindingTextEdit(txt乳房右侧, dt妇女分级管理, tb_妇女分级管理.乳房右侧);
            DataBinder.BindingTextEdit(txt乳腺彩超, dt妇女分级管理, tb_妇女分级管理.乳腺彩超结果);
            DataBinder.BindingTextEdit(txt钼靶检查, dt妇女分级管理, tb_妇女分级管理.钼靶检查结果);
            //妇科检查
            //flowLayoutPanel
            SetFlowLayoutResult(dt妇女分级管理.Rows[0][tb_妇女分级管理.外阴].ToString(), flow外阴);
            DataBinder.BindingTextEdit(txt阴道, dt妇女分级管理, tb_妇女分级管理.阴道);
            DataBinder.BindingTextEdit(txt白带, dt妇女分级管理, tb_妇女分级管理.白带);
            //flowLayoutPanel
            SetFlowLayoutResult(dt妇女分级管理.Rows[0][tb_妇女分级管理.白带性质].ToString(), flow白带性质);
            //flowLayoutPanel
            SetFlowLayoutResult(dt妇女分级管理.Rows[0][tb_妇女分级管理.子宫颈].ToString(), flow子宫颈);
            DataBinder.BindingTextEdit(txt子宫体活动, dt妇女分级管理, tb_妇女分级管理.子宫体活动);
            DataBinder.BindingTextEdit(txt子宫脱垂, dt妇女分级管理, tb_妇女分级管理.子宫脱垂);
            DataBinder.BindingTextEdit(txt子宫体大小, dt妇女分级管理, tb_妇女分级管理.子宫体大小);
            DataBinder.BindingTextEdit(txt左卵巢.Txt1, dt妇女分级管理, tb_妇女分级管理.左卵巢);
            DataBinder.BindingTextEdit(txt右卵巢.Txt1, dt妇女分级管理, tb_妇女分级管理.右卵巢);
            //flowLayoutPanel
            SetFlowLayoutResult(dt妇女分级管理.Rows[0][tb_妇女分级管理.附件].ToString(), flow附件);
            DataBinder.BindingTextEdit(txt阴道异常情况, dt妇女分级管理, tb_妇女分级管理.阴道异常情况);
            DataBinder.BindingTextEdit(txtTCT结果, dt妇女分级管理, tb_妇女分级管理.TCT检查结果);
            DataBinder.BindingTextEdit(txt病理检查, dt妇女分级管理, tb_妇女分级管理.病理检查结果);

            DataBinder.BindingTextEdit(txt印象, dt妇女分级管理, tb_妇女分级管理.印象);
            DataBinder.BindingRadioEdit(radio分级, dt妇女分级管理, tb_妇女分级管理.孕妇分级);
            DataBinder.BindingTextEdit(txt处理意见, dt妇女分级管理, tb_妇女分级管理.处理意见);
            DataBinder.BindingTextEdit(txt治疗情况, dt妇女分级管理, tb_妇女分级管理.治疗情况);
            DataBinder.BindingTextEdit(txt医师签名, dt妇女分级管理, tb_妇女分级管理.医师签名);
            //非编辑项
            DataBinder.BindingTextEdit(txt创建时间, dt妇女分级管理, tb_妇女分级管理.创建时间);
            DataBinder.BindingTextEdit(txt最近更新时间, dt妇女分级管理, tb_妇女分级管理.修改时间);
            DataBinder.BindingTextEdit(txt当前所属机构, dt妇女分级管理, tb_妇女分级管理.所属机构);
            DataBinder.BindingTextEdit(txt创建机构, dt妇女分级管理, tb_妇女分级管理.创建机构);
            DataBinder.BindingTextEdit(txt创建人, dt妇女分级管理, tb_妇女分级管理.创建人);
            DataBinder.BindingTextEdit(txt最近修改人, dt妇女分级管理, tb_妇女分级管理.修改人);



            //if (dt妇女分级管理.Rows.Count == 1)
            //{
            //    BindSuiFangData(dt妇女分级管理.Rows[0]);
            //}
        }

        #region Handler Events
        private void UC妇女保健检查表_Load(object sender, EventArgs e)
        {
            //this.txt检查次数.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt检查次数.Txt1.Properties.Mask.EditMask = "n0";
            //this.txt检查次数.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            
            //this.txt孕次.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt孕次.Txt1.Properties.Mask.EditMask = "###";
            //this.txt孕次.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            //this.txt产次.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt产次.Txt1.Properties.Mask.EditMask = "###";
            //this.txt产次.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            //this.txt自然流产.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt自然流产.Txt1.Properties.Mask.EditMask = "###";
            //this.txt自然流产.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            //this.txt人工流产.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt人工流产.Txt1.Properties.Mask.EditMask = "###";
            //this.txt人工流产.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;
            //this.txt中孕引产.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt中孕引产.Txt1.Properties.Mask.EditMask = "###";
            //this.txt中孕引产.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            //this.txt不孕史时间.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt不孕史时间.Txt1.Properties.Mask.EditMask = "###";
            //this.txt不孕史时间.Txt1.Properties.Mask.UseMaskAsDisplayFormat = true;

            //this.txt左卵巢.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt左卵巢.Txt1.Properties.Mask.EditMask = "###.##";
            //this.txt右卵巢.Txt1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            //this.txt右卵巢.Txt1.Properties.Mask.EditMask = "###.##";
        }
        
        private void chk未孕_CheckedChanged(object sender, EventArgs e)
        {
            this.txt孕次.Txt1.Properties.ReadOnly = this.txt产次.Txt1.Properties.ReadOnly = this.txt自然流产.Txt1.Properties.ReadOnly = this.txt人工流产.Txt1.Properties.ReadOnly = this.txt中孕引产.Txt1.Properties.ReadOnly = this.chk未孕.Checked;
            if (this.chk未孕.Checked)
            {
                this.txt孕次.Txt1.Text = this.txt产次.Txt1.Text = this.txt自然流产.Txt1.Text = this.txt人工流产.Txt1.Text = this.txt中孕引产.Txt1.Text = "";
            }
        }
        private void chk不孕史_CheckedChanged(object sender, EventArgs e)
        {
            this.txt不孕史时间.Txt1.Properties.ReadOnly = this.txt不孕史男.Properties.ReadOnly = this.txt不孕史女.Properties.ReadOnly = this.chk不孕史.Checked;
            if (this.chk不孕史.Checked)
            {
                this.txt不孕史时间.Txt1.Text = this.txt不孕史男.Text = this.txt不孕史女.Text = "";
            }
        }
        private void chk白带性质_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk白带性质.Checked)
            {
                this.chk白带_白.Properties.ReadOnly = this.chk白带_臭味.Properties.ReadOnly = this.chk白带_黄色.Properties.ReadOnly = this.chk白带_侬性.Properties.ReadOnly = this.chk白带_泡沫.Properties.ReadOnly = this.chk白带_水样.Properties.ReadOnly = this.chk白带_血样.Properties.ReadOnly = this.chk白带_渣样.Properties.ReadOnly = true;

                this.chk白带_白.Checked = this.chk白带_臭味.Checked = this.chk白带_黄色.Checked = this.chk白带_侬性.Checked = this.chk白带_泡沫.Checked = this.chk白带_水样.Checked = this.chk白带_血样.Checked = this.chk白带_渣样.Checked = false;
            }
            else
            {
                this.chk白带_白.Properties.ReadOnly = this.chk白带_臭味.Properties.ReadOnly = this.chk白带_黄色.Properties.ReadOnly = this.chk白带_侬性.Properties.ReadOnly = this.chk白带_泡沫.Properties.ReadOnly = this.chk白带_水样.Properties.ReadOnly = this.chk白带_血样.Properties.ReadOnly = this.chk白带_渣样.Properties.ReadOnly = false;
            }

        }
        private void checkEdit21_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkEdit21.Checked)
            {
                this.checkEdit22.Properties.ReadOnly = this.checkEdit23.Properties.ReadOnly = this.checkEdit24.Properties.ReadOnly = this.checkEdit25.Properties.ReadOnly = this.checkEdit26.Properties.ReadOnly = this.checkEdit27.Properties.ReadOnly = this.checkEdit28.Properties.ReadOnly = this.checkEdit29.Properties.ReadOnly = this.checkEdit30.Properties.ReadOnly = true;

                this.checkEdit22.Checked = this.checkEdit23.Checked = this.checkEdit24.Checked = this.checkEdit25.Checked = this.checkEdit26.Checked = this.checkEdit27.Checked = this.checkEdit28.Checked = this.checkEdit29.Checked = this.checkEdit30.Checked = false;
            }
            else
            {
                this.checkEdit22.Properties.ReadOnly = this.checkEdit23.Properties.ReadOnly = this.checkEdit24.Properties.ReadOnly = this.checkEdit25.Properties.ReadOnly = this.checkEdit26.Properties.ReadOnly = this.checkEdit27.Properties.ReadOnly = this.checkEdit28.Properties.ReadOnly = this.checkEdit29.Properties.ReadOnly = this.checkEdit30.Properties.ReadOnly = false;
            }
        }
        private void checkEdit35_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkEdit35.Checked)
            {
                this.checkEdit31.Properties.ReadOnly = this.checkEdit32.Properties.ReadOnly = this.checkEdit33.Properties.ReadOnly = this.checkEdit34.Properties.ReadOnly = true;

                this.checkEdit31.Checked = this.checkEdit32.Checked = this.checkEdit33.Checked = this.checkEdit34.Checked = false;
            }
            else
            {
                this.checkEdit31.Properties.ReadOnly = this.checkEdit32.Properties.ReadOnly = this.checkEdit33.Properties.ReadOnly = this.checkEdit34.Properties.ReadOnly = false;
            }
        }
        /// <summary>
        /// 外阴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkEdit5_CheckedChanged(object sender, EventArgs e)
        {
            this.checkEdit6.Enabled = this.checkEdit7.Enabled = this.checkEdit8.Enabled = this.checkEdit9.Enabled = !this.checkEdit5.Checked;
            if (this.checkEdit5.Checked)
            {
                this.checkEdit6.Checked = this.checkEdit7.Checked = this.checkEdit8.Checked = this.checkEdit9.Checked = false;
            }
        }
        private void btn保存_Click(object sender, EventArgs e)
        {
            UpdateLastControl();
            if (_UpdateType == UpdateType.None) return;
            if (Check验证())
            {
                if (_UpdateType == UpdateType.Add)//添加
                {
                    if (_Bll.CheckNoExistsByDate(txt卡号.Text, this.dte检查日期.Text))
                    {
                        Msg.Warning("已存在此检查时间，同一检查时间内不能进行多次随访，请确认！");
                        this.dte检查日期.Text = "";
                        return;
                    }                    
                }
                else//修改
                {
                    _Bll.CurrentBusiness.Tables[0].Rows[0][tb_妇女分级管理.修改时间] = this.txt最近更新时间.Text.Trim();
                    _Bll.CurrentBusiness.Tables[0].Rows[0][tb_妇女分级管理.所属机构] = Loginer.CurrentUser.所属机构;
                    _Bll.CurrentBusiness.Tables[0].Rows[0][tb_妇女分级管理.修改人] = Loginer.CurrentUser.用户编码;
                    #endregion
                }
                #region 保存 妇女检查表
                if (Msg.AskQuestion("信息保存后，'检查日期'将不允许修改，请确认表单信息!"))
                {
                    if (this.chk未孕.Checked)
                    {
                        _Bll.CurrentBusiness.Tables[0].Rows[0][tb_妇女分级管理.未孕] = "on";
                    }
                    _Bll.CurrentBusiness.Tables[0].Rows[0][tb_妇女分级管理.外阴] = GetFlowLayoutResult(this.flow外阴);
                    _Bll.CurrentBusiness.Tables[0].Rows[0][tb_妇女分级管理.白带性质] = GetFlowLayoutResult(this.flow白带性质);
                    _Bll.CurrentBusiness.Tables[0].Rows[0][tb_妇女分级管理.子宫颈] = GetFlowLayoutResult(this.flow子宫颈);

                    _Bll.CurrentBusiness.Tables[0].Rows[0][tb_妇女分级管理.附件] = GetFlowLayoutResult(this.flow附件);

                    int quexiang = Get缺项();
                    _Bll.CurrentBusiness.Tables[0].Rows[0][tb_妇女分级管理.缺项] = quexiang;
                    _Bll.CurrentBusiness.Tables[0].Rows[0][tb_妇女分级管理.完整度] = Ger完整度(quexiang);
                }
                else return;                
                #endregion
                //_Bll.WriteLog();//TODO: 日志
                //if (_UpdateType == UpdateType.Modify) _Bll.WriteLog(); //注意:只有修改状态下保存修改日志

                DataSet dsTemplate = _Bll.CreateSaveData(_Bll.CurrentBusiness, _UpdateType); //创建用于保存的临时数据
                SaveResult result = _Bll.Save(dsTemplate);
                if (result.Success)
                {
                    if (_UpdateType == UpdateType.Add)
                    {
                        //_frm._param = "";
                    }
                    Msg.ShowInformation("保存数据成功！");
                    //UC妇女保健检查表_显示 uc = new UC妇女保健检查表_显示(_frm);
                    //ShowControl(uc);
                }
            }
        }

        #region Private Methods
        private int Get缺项()
        {
            return 1;
        }
        private int Ger完整度(int i)
        {
            int k = Convert.ToInt32((12 - i) * 100 / 12.0);
            return k;
        }
        private bool Check验证()
        {
            if (string.IsNullOrEmpty(this.dte检查日期.Text.Trim()))
            {
                Msg.Warning("检查日期为必填项，请填写！");
                this.dte检查日期.Focus();
                return false;
            }
            return true;
        }


        /// <summary>
        /// 计算BMI
        /// </summary>
        /// <param name="shengao">身高</param>
        /// <param name="tizhong">体重</param>
        /// <returns></returns>
        private decimal CalcBMI(decimal shengao, decimal tizhong)
        {
            if (shengao == 0m || tizhong == 0m) return 0m;
            return Math.Round(tizhong / ((shengao / 100) * (shengao / 100)), 2);
        }
        #endregion

        delegate void CloseFrom();
        
        private void btn取消_Click(object sender, EventArgs e)
        {
            Form f = this.Parent as Form;
            if (f != null)
                f.Close();
        }

        private void txt档案编号_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //打开通用查询窗体
            frmFuzzySearch.Execute(sender as ButtonEdit, this._Bll as IFuzzySearchSupportable, this.SearchStockCallBack);
        }
        /// <summary>
        /// 选择机构资料，设置当前机构栏位的资料
        /// </summary>        
        private void SearchStockCallBack(DataRow resultRow)
        {
            if (resultRow == null) return;

            this.txt姓名.Text = ConvertEx.ToString(resultRow[tb_妇女基本信息.姓名]);
            this.txt档案编号.Text = resultRow[tb_妇女基本信息.个人档案编号].ToString();
            this.txt身份证号.Text = resultRow[tb_妇女基本信息.身份证号].ToString();
            this.txt出生日期.Text = resultRow[tb_妇女基本信息.出生日期].ToString();
            this.txt联系电话.Text = resultRow[tb_妇女基本信息.联系电话].ToString();
            this.txt居住地址.Text = resultRow[tb_妇女基本信息.居住地址].ToString();
        } 
    }
}
