﻿namespace AtomEHR.公共卫生.Module.个人健康.妇女健康管理
{
    partial class UC第1次产前随访服务记录表_显示
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC第1次产前随访服务记录表_显示));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn添加 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn删除 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.sbtn查看查体化验单 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit转诊 = new System.Windows.Forms.RichTextBox();
            this.txt家属签名 = new DevExpress.XtraEditors.TextEdit();
            this.txt建册情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt妇科手术史 = new DevExpress.XtraEditors.TextEdit();
            this.txt个人史 = new DevExpress.XtraEditors.TextEdit();
            this.txt家族史 = new DevExpress.XtraEditors.TextEdit();
            this.txt既往病史 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel14 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit总体评估 = new DevExpress.XtraEditors.TextEdit();
            this.textEditB超 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel13 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc乙型肝炎表面抗原 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc乙型肝炎表面抗体 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc乙型肝炎e抗原 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc乙型肝炎e抗体 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc乙型肝炎核心抗体 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.textEdit阴道分泌物 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc血清谷丙转氨酶 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血清谷草转氨酶 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc白蛋白 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc总胆红素 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc结合胆红素 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血糖 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc尿蛋白 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc尿糖 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc尿酮体 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc尿潜血 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.uc尿常规其他 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc血红蛋白值 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc白细胞计数值 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血小板计数值 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血常规其他 = new AtomEHR.Library.UserControls.UCLblTxt();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc血清肌酐 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血尿素氮 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc血压 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit体重指数 = new DevExpress.XtraEditors.TextEdit();
            this.uc体重 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.uc身高 = new AtomEHR.Library.UserControls.UCTxtLbl();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.uc流产 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc死胎 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc死产 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc新生儿死亡 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.uc出生缺陷儿 = new AtomEHR.Library.UserControls.UCLblTxtLbl();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt末次月经 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.uc产次 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.uc填表孕周 = new AtomEHR.Library.UserControls.UCTxtLblTxtLbl();
            this.textEdit最近修改人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建人 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit创建时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit丈夫电话 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit丈夫年龄 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit丈夫姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit居住状态 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit随访医生 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit孕妇年龄 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit孕妇姓名 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit档案编号 = new DevExpress.XtraEditors.TextEdit();
            this.txt卡号 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit孕次 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textEdit心脏 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit阴道 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit肺部 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit外阴 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit宫颈 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit子宫 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit附件 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit梅毒血清学试验 = new DevExpress.XtraEditors.TextEdit();
            this.textEditHIV抗体检测 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit血型 = new DevExpress.XtraEditors.TextEdit();
            this.textEditRH = new DevExpress.XtraEditors.TextEdit();
            this.textEdit阴道清洁度 = new DevExpress.XtraEditors.TextEdit();
            this.dte填表日期 = new DevExpress.XtraEditors.TextEdit();
            this.dte预产期 = new DevExpress.XtraEditors.TextEdit();
            this.dte下次随访日期 = new DevExpress.XtraEditors.TextEdit();
            this.text保健指导 = new DevExpress.XtraEditors.MemoEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl下次随访日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl随访医生 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl填表日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl填表孕周 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl孕妇年龄 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl丈夫姓名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl丈夫年龄 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl丈夫电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl孕次 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl产次 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl孕产史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl身高 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl体重 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血压 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl总体评估 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl尿常规 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血常规 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血型 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem66 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血糖 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl肝功能 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl肾功能 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem68 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem69 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl乙型肝炎五项 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem71 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem72 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem73 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl既往病史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl家族史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl个人史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl妇科手术史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl保健指导 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl听诊 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl妇科检查 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lbl建册情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl转诊 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txt本人电话 = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt家属签名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt建册情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt妇科手术史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家族史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt既往病史.Properties)).BeginInit();
            this.flowLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit总体评估.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditB超.Properties)).BeginInit();
            this.flowLayoutPanel13.SuspendLayout();
            this.flowLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit阴道分泌物.Properties)).BeginInit();
            this.flowLayoutPanel10.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体重指数.Properties)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt末次月经.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫年龄.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇年龄.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit心脏.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit阴道.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit肺部.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit外阴.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit宫颈.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit子宫.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit附件.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit梅毒血清学试验.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHIV抗体检测.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit血型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit阴道清洁度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte填表日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte预产期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.text保健指导.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl下次随访日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访医生)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl填表日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl填表孕周)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl孕妇年龄)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl丈夫姓名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl丈夫年龄)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl丈夫电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl孕次)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl产次)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl孕产史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl身高)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl总体评估)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl尿常规)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血常规)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血型)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血糖)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肝功能)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肾功能)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl乙型肝炎五项)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl既往病史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家族史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl个人史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl妇科手术史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl保健指导)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl听诊)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl妇科检查)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl建册情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl转诊)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本人电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlNavbar
            // 
            this.panelControlNavbar.Size = new System.Drawing.Size(111, 498);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.flowLayoutPanel1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(111, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(668, 32);
            this.panelControl1.TabIndex = 6;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn添加);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn删除);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Controls.Add(this.sbtn查看查体化验单);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(664, 28);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // btn添加
            // 
            this.btn添加.Image = ((System.Drawing.Image)(resources.GetObject("btn添加.Image")));
            this.btn添加.Location = new System.Drawing.Point(3, 3);
            this.btn添加.Name = "btn添加";
            this.btn添加.Size = new System.Drawing.Size(75, 23);
            this.btn添加.TabIndex = 0;
            this.btn添加.Text = "添加";
            this.btn添加.Click += new System.EventHandler(this.btn添加_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(84, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(75, 23);
            this.btn修改.TabIndex = 2;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn删除
            // 
            this.btn删除.Image = ((System.Drawing.Image)(resources.GetObject("btn删除.Image")));
            this.btn删除.Location = new System.Drawing.Point(165, 3);
            this.btn删除.Name = "btn删除";
            this.btn删除.Size = new System.Drawing.Size(75, 23);
            this.btn删除.TabIndex = 3;
            this.btn删除.Text = "删除";
            this.btn删除.Click += new System.EventHandler(this.btn删除_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(246, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(75, 23);
            this.btn导出.TabIndex = 4;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // sbtn查看查体化验单
            // 
            this.sbtn查看查体化验单.Image = ((System.Drawing.Image)(resources.GetObject("sbtn查看查体化验单.Image")));
            this.sbtn查看查体化验单.Location = new System.Drawing.Point(327, 3);
            this.sbtn查看查体化验单.Name = "sbtn查看查体化验单";
            this.sbtn查看查体化验单.Size = new System.Drawing.Size(120, 23);
            this.sbtn查看查体化验单.TabIndex = 4;
            this.sbtn查看查体化验单.Text = "查看查体化验单";
            this.sbtn查看查体化验单.Click += new System.EventHandler(this.sbtn查看查体化验单_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txt本人电话);
            this.layoutControl1.Controls.Add(this.textEdit转诊);
            this.layoutControl1.Controls.Add(this.txt家属签名);
            this.layoutControl1.Controls.Add(this.txt建册情况);
            this.layoutControl1.Controls.Add(this.txt妇科手术史);
            this.layoutControl1.Controls.Add(this.txt个人史);
            this.layoutControl1.Controls.Add(this.txt家族史);
            this.layoutControl1.Controls.Add(this.txt既往病史);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel14);
            this.layoutControl1.Controls.Add(this.textEditB超);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel13);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel12);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel10);
            this.layoutControl1.Controls.Add(this.uc血糖);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel9);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel8);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel11);
            this.layoutControl1.Controls.Add(this.uc血压);
            this.layoutControl1.Controls.Add(this.textEdit体重指数);
            this.layoutControl1.Controls.Add(this.uc体重);
            this.layoutControl1.Controls.Add(this.uc身高);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel7);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.uc填表孕周);
            this.layoutControl1.Controls.Add(this.textEdit最近修改人);
            this.layoutControl1.Controls.Add(this.textEdit创建人);
            this.layoutControl1.Controls.Add(this.textEdit创建机构);
            this.layoutControl1.Controls.Add(this.textEdit当前所属机构);
            this.layoutControl1.Controls.Add(this.textEdit最近更新时间);
            this.layoutControl1.Controls.Add(this.textEdit创建时间);
            this.layoutControl1.Controls.Add(this.textEdit丈夫电话);
            this.layoutControl1.Controls.Add(this.textEdit丈夫年龄);
            this.layoutControl1.Controls.Add(this.textEdit丈夫姓名);
            this.layoutControl1.Controls.Add(this.textEdit居住地址);
            this.layoutControl1.Controls.Add(this.textEdit居住状态);
            this.layoutControl1.Controls.Add(this.textEdit随访医生);
            this.layoutControl1.Controls.Add(this.textEdit孕妇年龄);
            this.layoutControl1.Controls.Add(this.textEdit出生日期);
            this.layoutControl1.Controls.Add(this.textEdit身份证号);
            this.layoutControl1.Controls.Add(this.textEdit孕妇姓名);
            this.layoutControl1.Controls.Add(this.textEdit档案编号);
            this.layoutControl1.Controls.Add(this.txt卡号);
            this.layoutControl1.Controls.Add(this.textEdit孕次);
            this.layoutControl1.Controls.Add(this.textEdit心脏);
            this.layoutControl1.Controls.Add(this.textEdit阴道);
            this.layoutControl1.Controls.Add(this.textEdit肺部);
            this.layoutControl1.Controls.Add(this.textEdit外阴);
            this.layoutControl1.Controls.Add(this.textEdit宫颈);
            this.layoutControl1.Controls.Add(this.textEdit子宫);
            this.layoutControl1.Controls.Add(this.textEdit附件);
            this.layoutControl1.Controls.Add(this.textEdit梅毒血清学试验);
            this.layoutControl1.Controls.Add(this.textEditHIV抗体检测);
            this.layoutControl1.Controls.Add(this.textEdit血型);
            this.layoutControl1.Controls.Add(this.textEditRH);
            this.layoutControl1.Controls.Add(this.textEdit阴道清洁度);
            this.layoutControl1.Controls.Add(this.dte填表日期);
            this.layoutControl1.Controls.Add(this.dte预产期);
            this.layoutControl1.Controls.Add(this.dte下次随访日期);
            this.layoutControl1.Controls.Add(this.text保健指导);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(111, 32);
            this.layoutControl1.LookAndFeel.SkinName = "Visual Studio 2013 Blue";
            this.layoutControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsView.DrawItemBorders = true;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(668, 466);
            this.layoutControl1.TabIndex = 7;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit转诊
            // 
            this.textEdit转诊.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textEdit转诊.Location = new System.Drawing.Point(88, 1100);
            this.textEdit转诊.Name = "textEdit转诊";
            this.textEdit转诊.Size = new System.Drawing.Size(560, 40);
            this.textEdit转诊.TabIndex = 85;
            this.textEdit转诊.Text = "";
            // 
            // txt家属签名
            // 
            this.txt家属签名.Location = new System.Drawing.Point(521, 1144);
            this.txt家属签名.Name = "txt家属签名";
            this.txt家属签名.Size = new System.Drawing.Size(127, 20);
            this.txt家属签名.StyleController = this.layoutControl1;
            this.txt家属签名.TabIndex = 84;
            // 
            // txt建册情况
            // 
            this.txt建册情况.Location = new System.Drawing.Point(88, 1076);
            this.txt建册情况.Name = "txt建册情况";
            this.txt建册情况.Size = new System.Drawing.Size(560, 20);
            this.txt建册情况.StyleController = this.layoutControl1;
            this.txt建册情况.TabIndex = 83;
            // 
            // txt妇科手术史
            // 
            this.txt妇科手术史.Location = new System.Drawing.Point(88, 316);
            this.txt妇科手术史.Name = "txt妇科手术史";
            this.txt妇科手术史.Size = new System.Drawing.Size(560, 20);
            this.txt妇科手术史.StyleController = this.layoutControl1;
            this.txt妇科手术史.TabIndex = 80;
            // 
            // txt个人史
            // 
            this.txt个人史.Location = new System.Drawing.Point(88, 292);
            this.txt个人史.Name = "txt个人史";
            this.txt个人史.Size = new System.Drawing.Size(560, 20);
            this.txt个人史.StyleController = this.layoutControl1;
            this.txt个人史.TabIndex = 79;
            // 
            // txt家族史
            // 
            this.txt家族史.Location = new System.Drawing.Point(88, 268);
            this.txt家族史.Name = "txt家族史";
            this.txt家族史.Size = new System.Drawing.Size(560, 20);
            this.txt家族史.StyleController = this.layoutControl1;
            this.txt家族史.TabIndex = 78;
            // 
            // txt既往病史
            // 
            this.txt既往病史.Location = new System.Drawing.Point(88, 244);
            this.txt既往病史.Name = "txt既往病史";
            this.txt既往病史.Size = new System.Drawing.Size(560, 20);
            this.txt既往病史.StyleController = this.layoutControl1;
            this.txt既往病史.TabIndex = 77;
            // 
            // flowLayoutPanel14
            // 
            this.flowLayoutPanel14.Controls.Add(this.textEdit总体评估);
            this.flowLayoutPanel14.Location = new System.Drawing.Point(88, 1012);
            this.flowLayoutPanel14.Name = "flowLayoutPanel14";
            this.flowLayoutPanel14.Size = new System.Drawing.Size(560, 20);
            this.flowLayoutPanel14.TabIndex = 75;
            // 
            // textEdit总体评估
            // 
            this.textEdit总体评估.Location = new System.Drawing.Point(0, 0);
            this.textEdit总体评估.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit总体评估.Name = "textEdit总体评估";
            this.textEdit总体评估.Size = new System.Drawing.Size(281, 20);
            this.textEdit总体评估.TabIndex = 0;
            // 
            // textEditB超
            // 
            this.textEditB超.Location = new System.Drawing.Point(180, 988);
            this.textEditB超.Name = "textEditB超";
            this.textEditB超.Size = new System.Drawing.Size(189, 20);
            this.textEditB超.StyleController = this.layoutControl1;
            this.textEditB超.TabIndex = 74;
            // 
            // flowLayoutPanel13
            // 
            this.flowLayoutPanel13.Controls.Add(this.uc乙型肝炎表面抗原);
            this.flowLayoutPanel13.Controls.Add(this.uc乙型肝炎表面抗体);
            this.flowLayoutPanel13.Controls.Add(this.uc乙型肝炎e抗原);
            this.flowLayoutPanel13.Controls.Add(this.uc乙型肝炎e抗体);
            this.flowLayoutPanel13.Controls.Add(this.uc乙型肝炎核心抗体);
            this.flowLayoutPanel13.Location = new System.Drawing.Point(180, 858);
            this.flowLayoutPanel13.Name = "flowLayoutPanel13";
            this.flowLayoutPanel13.Size = new System.Drawing.Size(468, 76);
            this.flowLayoutPanel13.TabIndex = 66;
            // 
            // uc乙型肝炎表面抗原
            // 
            this.uc乙型肝炎表面抗原.Lbl1Size = new System.Drawing.Size(100, 18);
            this.uc乙型肝炎表面抗原.Lbl1Text = "乙型肝炎表面抗原";
            this.uc乙型肝炎表面抗原.Location = new System.Drawing.Point(0, 0);
            this.uc乙型肝炎表面抗原.Margin = new System.Windows.Forms.Padding(0);
            this.uc乙型肝炎表面抗原.Name = "uc乙型肝炎表面抗原";
            this.uc乙型肝炎表面抗原.Size = new System.Drawing.Size(172, 22);
            this.uc乙型肝炎表面抗原.TabIndex = 0;
            this.uc乙型肝炎表面抗原.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc乙型肝炎表面抗体
            // 
            this.uc乙型肝炎表面抗体.Lbl1Size = new System.Drawing.Size(100, 18);
            this.uc乙型肝炎表面抗体.Lbl1Text = "乙型肝炎表面抗体";
            this.uc乙型肝炎表面抗体.Location = new System.Drawing.Point(172, 0);
            this.uc乙型肝炎表面抗体.Margin = new System.Windows.Forms.Padding(0);
            this.uc乙型肝炎表面抗体.Name = "uc乙型肝炎表面抗体";
            this.uc乙型肝炎表面抗体.Size = new System.Drawing.Size(175, 22);
            this.uc乙型肝炎表面抗体.TabIndex = 1;
            this.uc乙型肝炎表面抗体.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc乙型肝炎e抗原
            // 
            this.uc乙型肝炎e抗原.Lbl1Size = new System.Drawing.Size(100, 18);
            this.uc乙型肝炎e抗原.Lbl1Text = "乙型肝炎e抗原";
            this.uc乙型肝炎e抗原.Location = new System.Drawing.Point(0, 22);
            this.uc乙型肝炎e抗原.Margin = new System.Windows.Forms.Padding(0);
            this.uc乙型肝炎e抗原.Name = "uc乙型肝炎e抗原";
            this.uc乙型肝炎e抗原.Size = new System.Drawing.Size(172, 22);
            this.uc乙型肝炎e抗原.TabIndex = 2;
            this.uc乙型肝炎e抗原.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc乙型肝炎e抗体
            // 
            this.uc乙型肝炎e抗体.Lbl1Size = new System.Drawing.Size(100, 18);
            this.uc乙型肝炎e抗体.Lbl1Text = "乙型肝炎e抗体";
            this.uc乙型肝炎e抗体.Location = new System.Drawing.Point(172, 22);
            this.uc乙型肝炎e抗体.Margin = new System.Windows.Forms.Padding(0);
            this.uc乙型肝炎e抗体.Name = "uc乙型肝炎e抗体";
            this.uc乙型肝炎e抗体.Size = new System.Drawing.Size(175, 22);
            this.uc乙型肝炎e抗体.TabIndex = 3;
            this.uc乙型肝炎e抗体.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc乙型肝炎核心抗体
            // 
            this.uc乙型肝炎核心抗体.Lbl1Size = new System.Drawing.Size(100, 18);
            this.uc乙型肝炎核心抗体.Lbl1Text = "乙型肝炎核心抗体";
            this.uc乙型肝炎核心抗体.Location = new System.Drawing.Point(0, 44);
            this.uc乙型肝炎核心抗体.Margin = new System.Windows.Forms.Padding(0);
            this.uc乙型肝炎核心抗体.Name = "uc乙型肝炎核心抗体";
            this.uc乙型肝炎核心抗体.Size = new System.Drawing.Size(181, 22);
            this.uc乙型肝炎核心抗体.TabIndex = 4;
            this.uc乙型肝炎核心抗体.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // flowLayoutPanel12
            // 
            this.flowLayoutPanel12.Controls.Add(this.textEdit阴道分泌物);
            this.flowLayoutPanel12.Location = new System.Drawing.Point(177, 809);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(471, 20);
            this.flowLayoutPanel12.TabIndex = 70;
            // 
            // textEdit阴道分泌物
            // 
            this.textEdit阴道分泌物.Location = new System.Drawing.Point(0, 0);
            this.textEdit阴道分泌物.Margin = new System.Windows.Forms.Padding(0);
            this.textEdit阴道分泌物.Name = "textEdit阴道分泌物";
            this.textEdit阴道分泌物.Size = new System.Drawing.Size(196, 20);
            this.textEdit阴道分泌物.TabIndex = 0;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.uc血清谷丙转氨酶);
            this.flowLayoutPanel10.Controls.Add(this.uc血清谷草转氨酶);
            this.flowLayoutPanel10.Controls.Add(this.uc白蛋白);
            this.flowLayoutPanel10.Controls.Add(this.uc总胆红素);
            this.flowLayoutPanel10.Controls.Add(this.uc结合胆红素);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(180, 701);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(468, 76);
            this.flowLayoutPanel10.TabIndex = 67;
            // 
            // uc血清谷丙转氨酶
            // 
            this.uc血清谷丙转氨酶.Lbl1Size = new System.Drawing.Size(90, 18);
            this.uc血清谷丙转氨酶.Lbl1Text = "血清谷丙转氨酶";
            this.uc血清谷丙转氨酶.Lbl2Size = new System.Drawing.Size(30, 18);
            this.uc血清谷丙转氨酶.Lbl2Text = "U/L";
            this.uc血清谷丙转氨酶.Location = new System.Drawing.Point(0, 0);
            this.uc血清谷丙转氨酶.Margin = new System.Windows.Forms.Padding(0);
            this.uc血清谷丙转氨酶.Name = "uc血清谷丙转氨酶";
            this.uc血清谷丙转氨酶.Size = new System.Drawing.Size(191, 22);
            this.uc血清谷丙转氨酶.TabIndex = 0;
            this.uc血清谷丙转氨酶.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc血清谷草转氨酶
            // 
            this.uc血清谷草转氨酶.Lbl1Size = new System.Drawing.Size(90, 18);
            this.uc血清谷草转氨酶.Lbl1Text = "血清谷草转氨酶";
            this.uc血清谷草转氨酶.Lbl2Size = new System.Drawing.Size(30, 18);
            this.uc血清谷草转氨酶.Lbl2Text = "U/L";
            this.uc血清谷草转氨酶.Location = new System.Drawing.Point(191, 0);
            this.uc血清谷草转氨酶.Margin = new System.Windows.Forms.Padding(0);
            this.uc血清谷草转氨酶.Name = "uc血清谷草转氨酶";
            this.uc血清谷草转氨酶.Size = new System.Drawing.Size(197, 22);
            this.uc血清谷草转氨酶.TabIndex = 1;
            this.uc血清谷草转氨酶.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc白蛋白
            // 
            this.uc白蛋白.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc白蛋白.Lbl1Text = "白蛋白";
            this.uc白蛋白.Lbl2Size = new System.Drawing.Size(40, 18);
            this.uc白蛋白.Lbl2Text = "g/L";
            this.uc白蛋白.Location = new System.Drawing.Point(0, 22);
            this.uc白蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.uc白蛋白.Name = "uc白蛋白";
            this.uc白蛋白.Size = new System.Drawing.Size(161, 22);
            this.uc白蛋白.TabIndex = 2;
            this.uc白蛋白.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc总胆红素
            // 
            this.uc总胆红素.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc总胆红素.Lbl1Text = "总胆红素";
            this.uc总胆红素.Lbl2Size = new System.Drawing.Size(40, 18);
            this.uc总胆红素.Lbl2Text = "μmol/L";
            this.uc总胆红素.Location = new System.Drawing.Point(161, 22);
            this.uc总胆红素.Margin = new System.Windows.Forms.Padding(0);
            this.uc总胆红素.Name = "uc总胆红素";
            this.uc总胆红素.Size = new System.Drawing.Size(169, 22);
            this.uc总胆红素.TabIndex = 3;
            this.uc总胆红素.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc结合胆红素
            // 
            this.uc结合胆红素.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc结合胆红素.Lbl1Text = "结合胆红素";
            this.uc结合胆红素.Lbl2Size = new System.Drawing.Size(56, 18);
            this.uc结合胆红素.Lbl2Text = "μmol/L ";
            this.uc结合胆红素.Location = new System.Drawing.Point(0, 44);
            this.uc结合胆红素.Margin = new System.Windows.Forms.Padding(0);
            this.uc结合胆红素.Name = "uc结合胆红素";
            this.uc结合胆红素.Size = new System.Drawing.Size(191, 22);
            this.uc结合胆红素.TabIndex = 4;
            this.uc结合胆红素.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc血糖
            // 
            this.uc血糖.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc血糖.Lbl1Text = "mmol/L";
            this.uc血糖.Location = new System.Drawing.Point(180, 677);
            this.uc血糖.Name = "uc血糖";
            this.uc血糖.Size = new System.Drawing.Size(468, 20);
            this.uc血糖.TabIndex = 66;
            this.uc血糖.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Controls.Add(this.uc尿蛋白);
            this.flowLayoutPanel9.Controls.Add(this.uc尿糖);
            this.flowLayoutPanel9.Controls.Add(this.uc尿酮体);
            this.flowLayoutPanel9.Controls.Add(this.uc尿潜血);
            this.flowLayoutPanel9.Controls.Add(this.uc尿常规其他);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(180, 572);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(468, 51);
            this.flowLayoutPanel9.TabIndex = 65;
            // 
            // uc尿蛋白
            // 
            this.uc尿蛋白.Lbl1Size = new System.Drawing.Size(40, 18);
            this.uc尿蛋白.Lbl1Text = "尿蛋白";
            this.uc尿蛋白.Location = new System.Drawing.Point(0, 0);
            this.uc尿蛋白.Margin = new System.Windows.Forms.Padding(0);
            this.uc尿蛋白.Name = "uc尿蛋白";
            this.uc尿蛋白.Size = new System.Drawing.Size(114, 22);
            this.uc尿蛋白.TabIndex = 0;
            this.uc尿蛋白.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc尿糖
            // 
            this.uc尿糖.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc尿糖.Lbl1Text = "尿糖";
            this.uc尿糖.Location = new System.Drawing.Point(114, 0);
            this.uc尿糖.Margin = new System.Windows.Forms.Padding(0);
            this.uc尿糖.Name = "uc尿糖";
            this.uc尿糖.Size = new System.Drawing.Size(104, 22);
            this.uc尿糖.TabIndex = 1;
            this.uc尿糖.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc尿酮体
            // 
            this.uc尿酮体.Lbl1Size = new System.Drawing.Size(40, 18);
            this.uc尿酮体.Lbl1Text = "尿酮体";
            this.uc尿酮体.Location = new System.Drawing.Point(218, 0);
            this.uc尿酮体.Margin = new System.Windows.Forms.Padding(0);
            this.uc尿酮体.Name = "uc尿酮体";
            this.uc尿酮体.Size = new System.Drawing.Size(112, 22);
            this.uc尿酮体.TabIndex = 2;
            this.uc尿酮体.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc尿潜血
            // 
            this.uc尿潜血.Lbl1Size = new System.Drawing.Size(40, 18);
            this.uc尿潜血.Lbl1Text = "尿潜血";
            this.uc尿潜血.Location = new System.Drawing.Point(330, 0);
            this.uc尿潜血.Margin = new System.Windows.Forms.Padding(0);
            this.uc尿潜血.Name = "uc尿潜血";
            this.uc尿潜血.Size = new System.Drawing.Size(125, 22);
            this.uc尿潜血.TabIndex = 3;
            this.uc尿潜血.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc尿常规其他
            // 
            this.uc尿常规其他.Lbl1Size = new System.Drawing.Size(40, 18);
            this.uc尿常规其他.Lbl1Text = "其他";
            this.uc尿常规其他.Location = new System.Drawing.Point(0, 22);
            this.uc尿常规其他.Margin = new System.Windows.Forms.Padding(0);
            this.uc尿常规其他.Name = "uc尿常规其他";
            this.uc尿常规其他.Size = new System.Drawing.Size(114, 22);
            this.uc尿常规其他.TabIndex = 4;
            this.uc尿常规其他.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.uc血红蛋白值);
            this.flowLayoutPanel8.Controls.Add(this.uc白细胞计数值);
            this.flowLayoutPanel8.Controls.Add(this.uc血小板计数值);
            this.flowLayoutPanel8.Controls.Add(this.uc血常规其他);
            this.flowLayoutPanel8.Location = new System.Drawing.Point(180, 517);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(468, 51);
            this.flowLayoutPanel8.TabIndex = 64;
            // 
            // uc血红蛋白值
            // 
            this.uc血红蛋白值.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc血红蛋白值.Lbl1Text = "血红蛋白值";
            this.uc血红蛋白值.Lbl2Size = new System.Drawing.Size(20, 18);
            this.uc血红蛋白值.Lbl2Text = "g/L";
            this.uc血红蛋白值.Location = new System.Drawing.Point(0, 0);
            this.uc血红蛋白值.Margin = new System.Windows.Forms.Padding(0);
            this.uc血红蛋白值.Name = "uc血红蛋白值";
            this.uc血红蛋白值.Size = new System.Drawing.Size(193, 22);
            this.uc血红蛋白值.TabIndex = 0;
            this.uc血红蛋白值.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc白细胞计数值
            // 
            this.uc白细胞计数值.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc白细胞计数值.Lbl1Text = "白细胞计数值";
            this.uc白细胞计数值.Lbl2Size = new System.Drawing.Size(60, 18);
            this.uc白细胞计数值.Lbl2Text = "x10^9 /L";
            this.uc白细胞计数值.Location = new System.Drawing.Point(193, 0);
            this.uc白细胞计数值.Margin = new System.Windows.Forms.Padding(0);
            this.uc白细胞计数值.Name = "uc白细胞计数值";
            this.uc白细胞计数值.Size = new System.Drawing.Size(202, 22);
            this.uc白细胞计数值.TabIndex = 1;
            this.uc白细胞计数值.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc血小板计数值
            // 
            this.uc血小板计数值.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc血小板计数值.Lbl1Text = "血小板计数值";
            this.uc血小板计数值.Lbl2Size = new System.Drawing.Size(60, 18);
            this.uc血小板计数值.Lbl2Text = "x10^9 /L";
            this.uc血小板计数值.Location = new System.Drawing.Point(0, 22);
            this.uc血小板计数值.Margin = new System.Windows.Forms.Padding(0);
            this.uc血小板计数值.Name = "uc血小板计数值";
            this.uc血小板计数值.Size = new System.Drawing.Size(193, 22);
            this.uc血小板计数值.TabIndex = 2;
            this.uc血小板计数值.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc血常规其他
            // 
            this.uc血常规其他.Lbl1Size = new System.Drawing.Size(40, 18);
            this.uc血常规其他.Lbl1Text = "其他";
            this.uc血常规其他.Location = new System.Drawing.Point(193, 22);
            this.uc血常规其他.Margin = new System.Windows.Forms.Padding(0);
            this.uc血常规其他.Name = "uc血常规其他";
            this.uc血常规其他.Size = new System.Drawing.Size(181, 22);
            this.uc血常规其他.TabIndex = 3;
            this.uc血常规其他.Txt1Size = new System.Drawing.Size(100, 20);
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.uc血清肌酐);
            this.flowLayoutPanel11.Controls.Add(this.uc血尿素氮);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(180, 781);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(468, 24);
            this.flowLayoutPanel11.TabIndex = 68;
            // 
            // uc血清肌酐
            // 
            this.uc血清肌酐.Lbl1Size = new System.Drawing.Size(50, 18);
            this.uc血清肌酐.Lbl1Text = "血清肌酐";
            this.uc血清肌酐.Lbl2Size = new System.Drawing.Size(40, 18);
            this.uc血清肌酐.Lbl2Text = "μmol/L";
            this.uc血清肌酐.Location = new System.Drawing.Point(0, 0);
            this.uc血清肌酐.Margin = new System.Windows.Forms.Padding(0);
            this.uc血清肌酐.Name = "uc血清肌酐";
            this.uc血清肌酐.Size = new System.Drawing.Size(172, 22);
            this.uc血清肌酐.TabIndex = 3;
            this.uc血清肌酐.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc血尿素氮
            // 
            this.uc血尿素氮.Lbl1Size = new System.Drawing.Size(70, 18);
            this.uc血尿素氮.Lbl1Text = "血尿素氮";
            this.uc血尿素氮.Lbl2Size = new System.Drawing.Size(56, 18);
            this.uc血尿素氮.Lbl2Text = "μmol/L ";
            this.uc血尿素氮.Location = new System.Drawing.Point(172, 0);
            this.uc血尿素氮.Margin = new System.Windows.Forms.Padding(0);
            this.uc血尿素氮.Name = "uc血尿素氮";
            this.uc血尿素氮.Size = new System.Drawing.Size(202, 22);
            this.uc血尿素氮.TabIndex = 4;
            this.uc血尿素氮.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc血压
            // 
            this.uc血压.Lbl1Size = new System.Drawing.Size(18, 14);
            this.uc血压.Lbl1Text = "/";
            this.uc血压.Lbl2Size = new System.Drawing.Size(40, 14);
            this.uc血压.Lbl2Text = "mmHg";
            this.uc血压.Location = new System.Drawing.Point(360, 392);
            this.uc血压.Name = "uc血压";
            this.uc血压.Size = new System.Drawing.Size(288, 20);
            this.uc血压.TabIndex = 49;
            this.uc血压.Txt1EditValue = null;
            this.uc血压.Txt1Size = new System.Drawing.Size(50, 20);
            this.uc血压.Txt2EditValue = null;
            this.uc血压.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // textEdit体重指数
            // 
            this.textEdit体重指数.Location = new System.Drawing.Point(88, 392);
            this.textEdit体重指数.Name = "textEdit体重指数";
            this.textEdit体重指数.Size = new System.Drawing.Size(183, 20);
            this.textEdit体重指数.StyleController = this.layoutControl1;
            this.textEdit体重指数.TabIndex = 48;
            // 
            // uc体重
            // 
            this.uc体重.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc体重.Lbl1Text = "kg";
            this.uc体重.Location = new System.Drawing.Point(360, 368);
            this.uc体重.Name = "uc体重";
            this.uc体重.Size = new System.Drawing.Size(288, 20);
            this.uc体重.TabIndex = 47;
            this.uc体重.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // uc身高
            // 
            this.uc身高.Lbl1Size = new System.Drawing.Size(69, 18);
            this.uc身高.Lbl1Text = "cm";
            this.uc身高.Location = new System.Drawing.Point(88, 368);
            this.uc身高.Name = "uc身高";
            this.uc身高.Size = new System.Drawing.Size(183, 20);
            this.uc身高.TabIndex = 46;
            this.uc身高.Txt1Size = new System.Drawing.Size(50, 20);
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.uc流产);
            this.flowLayoutPanel7.Controls.Add(this.uc死胎);
            this.flowLayoutPanel7.Controls.Add(this.uc死产);
            this.flowLayoutPanel7.Controls.Add(this.uc新生儿死亡);
            this.flowLayoutPanel7.Controls.Add(this.uc出生缺陷儿);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(88, 340);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(560, 24);
            this.flowLayoutPanel7.TabIndex = 45;
            // 
            // uc流产
            // 
            this.uc流产.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc流产.Lbl1Text = "流产";
            this.uc流产.Lbl2Size = new System.Drawing.Size(15, 18);
            this.uc流产.Lbl2Text = "次";
            this.uc流产.Location = new System.Drawing.Point(0, 0);
            this.uc流产.Margin = new System.Windows.Forms.Padding(0);
            this.uc流产.Name = "uc流产";
            this.uc流产.Size = new System.Drawing.Size(90, 22);
            this.uc流产.TabIndex = 0;
            this.uc流产.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // uc死胎
            // 
            this.uc死胎.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc死胎.Lbl1Text = "死胎";
            this.uc死胎.Lbl2Size = new System.Drawing.Size(15, 18);
            this.uc死胎.Lbl2Text = "次";
            this.uc死胎.Location = new System.Drawing.Point(90, 0);
            this.uc死胎.Margin = new System.Windows.Forms.Padding(0);
            this.uc死胎.Name = "uc死胎";
            this.uc死胎.Size = new System.Drawing.Size(87, 22);
            this.uc死胎.TabIndex = 1;
            this.uc死胎.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // uc死产
            // 
            this.uc死产.Lbl1Size = new System.Drawing.Size(30, 18);
            this.uc死产.Lbl1Text = "死产";
            this.uc死产.Lbl2Size = new System.Drawing.Size(15, 18);
            this.uc死产.Lbl2Text = "次";
            this.uc死产.Location = new System.Drawing.Point(177, 0);
            this.uc死产.Margin = new System.Windows.Forms.Padding(0);
            this.uc死产.Name = "uc死产";
            this.uc死产.Size = new System.Drawing.Size(87, 22);
            this.uc死产.TabIndex = 2;
            this.uc死产.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // uc新生儿死亡
            // 
            this.uc新生儿死亡.Lbl1Size = new System.Drawing.Size(60, 18);
            this.uc新生儿死亡.Lbl1Text = "新生儿死亡";
            this.uc新生儿死亡.Lbl2Size = new System.Drawing.Size(15, 18);
            this.uc新生儿死亡.Lbl2Text = "次";
            this.uc新生儿死亡.Location = new System.Drawing.Point(264, 0);
            this.uc新生儿死亡.Margin = new System.Windows.Forms.Padding(0);
            this.uc新生儿死亡.Name = "uc新生儿死亡";
            this.uc新生儿死亡.Size = new System.Drawing.Size(120, 22);
            this.uc新生儿死亡.TabIndex = 3;
            this.uc新生儿死亡.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // uc出生缺陷儿
            // 
            this.uc出生缺陷儿.Lbl1Size = new System.Drawing.Size(60, 18);
            this.uc出生缺陷儿.Lbl1Text = "出生缺陷儿";
            this.uc出生缺陷儿.Lbl2Size = new System.Drawing.Size(15, 18);
            this.uc出生缺陷儿.Lbl2Text = "个";
            this.uc出生缺陷儿.Location = new System.Drawing.Point(384, 0);
            this.uc出生缺陷儿.Margin = new System.Windows.Forms.Padding(0);
            this.uc出生缺陷儿.Name = "uc出生缺陷儿";
            this.uc出生缺陷儿.Size = new System.Drawing.Size(125, 22);
            this.uc出生缺陷儿.TabIndex = 4;
            this.uc出生缺陷儿.Txt1Size = new System.Drawing.Size(30, 20);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.txt末次月经);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(88, 219);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(111, 20);
            this.flowLayoutPanel3.TabIndex = 38;
            // 
            // txt末次月经
            // 
            this.txt末次月经.Location = new System.Drawing.Point(0, 0);
            this.txt末次月经.Margin = new System.Windows.Forms.Padding(0);
            this.txt末次月经.Name = "txt末次月经";
            this.txt末次月经.Size = new System.Drawing.Size(111, 20);
            this.txt末次月经.TabIndex = 1;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.labelControl1);
            this.flowLayoutPanel2.Controls.Add(this.uc产次);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(288, 195);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(360, 20);
            this.flowLayoutPanel2.TabIndex = 37;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(3, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(48, 14);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "阴道分娩";
            // 
            // uc产次
            // 
            this.uc产次.Lbl1Size = new System.Drawing.Size(60, 14);
            this.uc产次.Lbl1Text = "次 剖宫产";
            this.uc产次.Lbl2Size = new System.Drawing.Size(18, 14);
            this.uc产次.Lbl2Text = "次";
            this.uc产次.Location = new System.Drawing.Point(54, 0);
            this.uc产次.Margin = new System.Windows.Forms.Padding(0);
            this.uc产次.Name = "uc产次";
            this.uc产次.Size = new System.Drawing.Size(235, 22);
            this.uc产次.TabIndex = 1;
            this.uc产次.Txt1EditValue = null;
            this.uc产次.Txt1Size = new System.Drawing.Size(50, 20);
            this.uc产次.Txt2EditValue = null;
            this.uc产次.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // uc填表孕周
            // 
            this.uc填表孕周.Lbl1Size = new System.Drawing.Size(18, 14);
            this.uc填表孕周.Lbl1Text = "周";
            this.uc填表孕周.Lbl2Size = new System.Drawing.Size(18, 14);
            this.uc填表孕周.Lbl2Text = "天";
            this.uc填表孕周.Location = new System.Drawing.Point(315, 122);
            this.uc填表孕周.Name = "uc填表孕周";
            this.uc填表孕周.Size = new System.Drawing.Size(333, 21);
            this.uc填表孕周.TabIndex = 35;
            this.uc填表孕周.Txt1EditValue = null;
            this.uc填表孕周.Txt1Size = new System.Drawing.Size(50, 20);
            this.uc填表孕周.Txt2EditValue = null;
            this.uc填表孕周.Txt2Size = new System.Drawing.Size(50, 20);
            // 
            // textEdit最近修改人
            // 
            this.textEdit最近修改人.Location = new System.Drawing.Point(521, 1193);
            this.textEdit最近修改人.Name = "textEdit最近修改人";
            this.textEdit最近修改人.Properties.ReadOnly = true;
            this.textEdit最近修改人.Size = new System.Drawing.Size(127, 20);
            this.textEdit最近修改人.StyleController = this.layoutControl1;
            this.textEdit最近修改人.TabIndex = 33;
            // 
            // textEdit创建人
            // 
            this.textEdit创建人.Location = new System.Drawing.Point(303, 1193);
            this.textEdit创建人.Name = "textEdit创建人";
            this.textEdit创建人.Properties.ReadOnly = true;
            this.textEdit创建人.Size = new System.Drawing.Size(129, 20);
            this.textEdit创建人.StyleController = this.layoutControl1;
            this.textEdit创建人.TabIndex = 32;
            // 
            // textEdit创建机构
            // 
            this.textEdit创建机构.Location = new System.Drawing.Point(88, 1193);
            this.textEdit创建机构.Name = "textEdit创建机构";
            this.textEdit创建机构.Properties.ReadOnly = true;
            this.textEdit创建机构.Size = new System.Drawing.Size(126, 20);
            this.textEdit创建机构.StyleController = this.layoutControl1;
            this.textEdit创建机构.TabIndex = 31;
            // 
            // textEdit当前所属机构
            // 
            this.textEdit当前所属机构.Location = new System.Drawing.Point(521, 1169);
            this.textEdit当前所属机构.Name = "textEdit当前所属机构";
            this.textEdit当前所属机构.Properties.ReadOnly = true;
            this.textEdit当前所属机构.Size = new System.Drawing.Size(127, 20);
            this.textEdit当前所属机构.StyleController = this.layoutControl1;
            this.textEdit当前所属机构.TabIndex = 30;
            // 
            // textEdit最近更新时间
            // 
            this.textEdit最近更新时间.Location = new System.Drawing.Point(303, 1169);
            this.textEdit最近更新时间.Name = "textEdit最近更新时间";
            this.textEdit最近更新时间.Properties.ReadOnly = true;
            this.textEdit最近更新时间.Size = new System.Drawing.Size(129, 20);
            this.textEdit最近更新时间.StyleController = this.layoutControl1;
            this.textEdit最近更新时间.TabIndex = 29;
            // 
            // textEdit创建时间
            // 
            this.textEdit创建时间.Location = new System.Drawing.Point(88, 1169);
            this.textEdit创建时间.Name = "textEdit创建时间";
            this.textEdit创建时间.Properties.ReadOnly = true;
            this.textEdit创建时间.Size = new System.Drawing.Size(126, 20);
            this.textEdit创建时间.StyleController = this.layoutControl1;
            this.textEdit创建时间.TabIndex = 28;
            // 
            // textEdit丈夫电话
            // 
            this.textEdit丈夫电话.Location = new System.Drawing.Point(513, 171);
            this.textEdit丈夫电话.Name = "textEdit丈夫电话";
            this.textEdit丈夫电话.Size = new System.Drawing.Size(135, 20);
            this.textEdit丈夫电话.StyleController = this.layoutControl1;
            this.textEdit丈夫电话.TabIndex = 25;
            // 
            // textEdit丈夫年龄
            // 
            this.textEdit丈夫年龄.Location = new System.Drawing.Point(288, 171);
            this.textEdit丈夫年龄.Name = "textEdit丈夫年龄";
            this.textEdit丈夫年龄.Size = new System.Drawing.Size(136, 20);
            this.textEdit丈夫年龄.StyleController = this.layoutControl1;
            this.textEdit丈夫年龄.TabIndex = 21;
            // 
            // textEdit丈夫姓名
            // 
            this.textEdit丈夫姓名.Location = new System.Drawing.Point(88, 171);
            this.textEdit丈夫姓名.Name = "textEdit丈夫姓名";
            this.textEdit丈夫姓名.Size = new System.Drawing.Size(111, 20);
            this.textEdit丈夫姓名.StyleController = this.layoutControl1;
            this.textEdit丈夫姓名.TabIndex = 20;
            // 
            // textEdit居住地址
            // 
            this.textEdit居住地址.Location = new System.Drawing.Point(88, 98);
            this.textEdit居住地址.Name = "textEdit居住地址";
            this.textEdit居住地址.Properties.ReadOnly = true;
            this.textEdit居住地址.Size = new System.Drawing.Size(560, 20);
            this.textEdit居住地址.StyleController = this.layoutControl1;
            this.textEdit居住地址.TabIndex = 15;
            // 
            // textEdit居住状态
            // 
            this.textEdit居住状态.Location = new System.Drawing.Point(545, 74);
            this.textEdit居住状态.Name = "textEdit居住状态";
            this.textEdit居住状态.Properties.ReadOnly = true;
            this.textEdit居住状态.Size = new System.Drawing.Size(103, 20);
            this.textEdit居住状态.StyleController = this.layoutControl1;
            this.textEdit居住状态.TabIndex = 14;
            // 
            // textEdit随访医生
            // 
            this.textEdit随访医生.Location = new System.Drawing.Point(302, 1144);
            this.textEdit随访医生.Name = "textEdit随访医生";
            this.textEdit随访医生.Size = new System.Drawing.Size(130, 20);
            this.textEdit随访医生.StyleController = this.layoutControl1;
            this.textEdit随访医生.TabIndex = 11;
            // 
            // textEdit孕妇年龄
            // 
            this.textEdit孕妇年龄.Location = new System.Drawing.Point(88, 147);
            this.textEdit孕妇年龄.Name = "textEdit孕妇年龄";
            this.textEdit孕妇年龄.Size = new System.Drawing.Size(111, 20);
            this.textEdit孕妇年龄.StyleController = this.layoutControl1;
            this.textEdit孕妇年龄.TabIndex = 9;
            // 
            // textEdit出生日期
            // 
            this.textEdit出生日期.Location = new System.Drawing.Point(88, 74);
            this.textEdit出生日期.Name = "textEdit出生日期";
            this.textEdit出生日期.Properties.ReadOnly = true;
            this.textEdit出生日期.Size = new System.Drawing.Size(137, 20);
            this.textEdit出生日期.StyleController = this.layoutControl1;
            this.textEdit出生日期.TabIndex = 8;
            // 
            // textEdit身份证号
            // 
            this.textEdit身份证号.Location = new System.Drawing.Point(314, 74);
            this.textEdit身份证号.Name = "textEdit身份证号";
            this.textEdit身份证号.Properties.ReadOnly = true;
            this.textEdit身份证号.Size = new System.Drawing.Size(142, 20);
            this.textEdit身份证号.StyleController = this.layoutControl1;
            this.textEdit身份证号.TabIndex = 7;
            // 
            // textEdit孕妇姓名
            // 
            this.textEdit孕妇姓名.Location = new System.Drawing.Point(545, 50);
            this.textEdit孕妇姓名.Name = "textEdit孕妇姓名";
            this.textEdit孕妇姓名.Properties.ReadOnly = true;
            this.textEdit孕妇姓名.Size = new System.Drawing.Size(103, 20);
            this.textEdit孕妇姓名.StyleController = this.layoutControl1;
            this.textEdit孕妇姓名.TabIndex = 6;
            // 
            // textEdit档案编号
            // 
            this.textEdit档案编号.Location = new System.Drawing.Point(314, 50);
            this.textEdit档案编号.Name = "textEdit档案编号";
            this.textEdit档案编号.Properties.ReadOnly = true;
            this.textEdit档案编号.Size = new System.Drawing.Size(142, 20);
            this.textEdit档案编号.StyleController = this.layoutControl1;
            this.textEdit档案编号.TabIndex = 5;
            // 
            // txt卡号
            // 
            this.txt卡号.Location = new System.Drawing.Point(88, 50);
            this.txt卡号.Name = "txt卡号";
            this.txt卡号.Size = new System.Drawing.Size(137, 20);
            this.txt卡号.StyleController = this.layoutControl1;
            this.txt卡号.TabIndex = 4;
            // 
            // textEdit孕次
            // 
            this.textEdit孕次.Location = new System.Drawing.Point(88, 195);
            this.textEdit孕次.Name = "textEdit孕次";
            this.textEdit孕次.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEdit孕次.Size = new System.Drawing.Size(111, 20);
            this.textEdit孕次.StyleController = this.layoutControl1;
            this.textEdit孕次.TabIndex = 36;
            // 
            // textEdit心脏
            // 
            this.textEdit心脏.Location = new System.Drawing.Point(150, 416);
            this.textEdit心脏.Name = "textEdit心脏";
            this.textEdit心脏.Size = new System.Drawing.Size(181, 20);
            this.textEdit心脏.StyleController = this.layoutControl1;
            this.textEdit心脏.TabIndex = 50;
            // 
            // textEdit阴道
            // 
            this.textEdit阴道.Location = new System.Drawing.Point(390, 442);
            this.textEdit阴道.Name = "textEdit阴道";
            this.textEdit阴道.Size = new System.Drawing.Size(258, 20);
            this.textEdit阴道.StyleController = this.layoutControl1;
            this.textEdit阴道.TabIndex = 57;
            // 
            // textEdit肺部
            // 
            this.textEdit肺部.Location = new System.Drawing.Point(390, 416);
            this.textEdit肺部.Name = "textEdit肺部";
            this.textEdit肺部.Size = new System.Drawing.Size(258, 20);
            this.textEdit肺部.StyleController = this.layoutControl1;
            this.textEdit肺部.TabIndex = 51;
            // 
            // textEdit外阴
            // 
            this.textEdit外阴.Location = new System.Drawing.Point(150, 442);
            this.textEdit外阴.Name = "textEdit外阴";
            this.textEdit外阴.Size = new System.Drawing.Size(181, 20);
            this.textEdit外阴.StyleController = this.layoutControl1;
            this.textEdit外阴.TabIndex = 54;
            // 
            // textEdit宫颈
            // 
            this.textEdit宫颈.Location = new System.Drawing.Point(150, 467);
            this.textEdit宫颈.Name = "textEdit宫颈";
            this.textEdit宫颈.Size = new System.Drawing.Size(181, 20);
            this.textEdit宫颈.StyleController = this.layoutControl1;
            this.textEdit宫颈.TabIndex = 58;
            // 
            // textEdit子宫
            // 
            this.textEdit子宫.Location = new System.Drawing.Point(390, 467);
            this.textEdit子宫.Name = "textEdit子宫";
            this.textEdit子宫.Size = new System.Drawing.Size(258, 20);
            this.textEdit子宫.StyleController = this.layoutControl1;
            this.textEdit子宫.TabIndex = 60;
            // 
            // textEdit附件
            // 
            this.textEdit附件.Location = new System.Drawing.Point(150, 492);
            this.textEdit附件.Name = "textEdit附件";
            this.textEdit附件.Size = new System.Drawing.Size(498, 20);
            this.textEdit附件.StyleController = this.layoutControl1;
            this.textEdit附件.TabIndex = 62;
            // 
            // textEdit梅毒血清学试验
            // 
            this.textEdit梅毒血清学试验.Location = new System.Drawing.Point(180, 938);
            this.textEdit梅毒血清学试验.Name = "textEdit梅毒血清学试验";
            this.textEdit梅毒血清学试验.Size = new System.Drawing.Size(189, 20);
            this.textEdit梅毒血清学试验.StyleController = this.layoutControl1;
            this.textEdit梅毒血清学试验.TabIndex = 72;
            // 
            // textEditHIV抗体检测
            // 
            this.textEditHIV抗体检测.Location = new System.Drawing.Point(180, 963);
            this.textEditHIV抗体检测.Name = "textEditHIV抗体检测";
            this.textEditHIV抗体检测.Size = new System.Drawing.Size(189, 20);
            this.textEditHIV抗体检测.StyleController = this.layoutControl1;
            this.textEditHIV抗体检测.TabIndex = 73;
            // 
            // textEdit血型
            // 
            this.textEdit血型.Location = new System.Drawing.Point(262, 627);
            this.textEdit血型.Name = "textEdit血型";
            this.textEdit血型.Size = new System.Drawing.Size(96, 20);
            this.textEdit血型.StyleController = this.layoutControl1;
            this.textEdit血型.TabIndex = 68;
            // 
            // textEditRH
            // 
            this.textEditRH.Location = new System.Drawing.Point(262, 652);
            this.textEditRH.Name = "textEditRH";
            this.textEditRH.Size = new System.Drawing.Size(96, 20);
            this.textEditRH.StyleController = this.layoutControl1;
            this.textEditRH.TabIndex = 69;
            // 
            // textEdit阴道清洁度
            // 
            this.textEdit阴道清洁度.Location = new System.Drawing.Point(240, 833);
            this.textEdit阴道清洁度.Name = "textEdit阴道清洁度";
            this.textEdit阴道清洁度.Size = new System.Drawing.Size(133, 20);
            this.textEdit阴道清洁度.StyleController = this.layoutControl1;
            this.textEdit阴道清洁度.TabIndex = 71;
            // 
            // dte填表日期
            // 
            this.dte填表日期.Location = new System.Drawing.Point(88, 122);
            this.dte填表日期.Name = "dte填表日期";
            this.dte填表日期.Size = new System.Drawing.Size(138, 20);
            this.dte填表日期.StyleController = this.layoutControl1;
            this.dte填表日期.TabIndex = 34;
            // 
            // dte预产期
            // 
            this.dte预产期.Location = new System.Drawing.Point(288, 219);
            this.dte预产期.Name = "dte预产期";
            this.dte预产期.Size = new System.Drawing.Size(161, 20);
            this.dte预产期.StyleController = this.layoutControl1;
            this.dte预产期.TabIndex = 39;
            // 
            // dte下次随访日期
            // 
            this.dte下次随访日期.Location = new System.Drawing.Point(88, 1144);
            this.dte下次随访日期.Name = "dte下次随访日期";
            this.dte下次随访日期.Size = new System.Drawing.Size(125, 20);
            this.dte下次随访日期.StyleController = this.layoutControl1;
            this.dte下次随访日期.TabIndex = 10;
            // 
            // text保健指导
            // 
            this.text保健指导.Location = new System.Drawing.Point(88, 1036);
            this.text保健指导.Name = "text保健指导";
            this.text保健指导.Size = new System.Drawing.Size(560, 36);
            this.text保健指导.StyleController = this.layoutControl1;
            this.text保健指导.TabIndex = 81;
            this.text保健指导.UseOptimizedRendering = true;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 13F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseBackColor = true;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.lbl下次随访日期,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.lbl随访医生,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem29,
            this.layoutControlItem30,
            this.emptySpaceItem1,
            this.layoutControlItem5,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.lbl填表日期,
            this.lbl填表孕周,
            this.lbl孕妇年龄,
            this.lbl丈夫姓名,
            this.lbl丈夫年龄,
            this.lbl丈夫电话,
            this.lbl孕次,
            this.lbl产次,
            this.layoutControlItem35,
            this.layoutControlItem36,
            this.lbl孕产史,
            this.lbl身高,
            this.lbl体重,
            this.layoutControlItem45,
            this.lbl血压,
            this.layoutControlItem47,
            this.layoutControlItem48,
            this.layoutControlItem51,
            this.layoutControlItem54,
            this.layoutControlItem55,
            this.layoutControlItem57,
            this.layoutControlItem59,
            this.lbl总体评估,
            this.layoutControlGroup2,
            this.lbl既往病史,
            this.lbl家族史,
            this.lbl个人史,
            this.lbl妇科手术史,
            this.lbl保健指导,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.lbl建册情况,
            this.layoutControlItem7,
            this.lbl转诊,
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(651, 1216);
            this.layoutControlGroup1.Text = "第1次产前随访服务记录表";
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt卡号;
            this.layoutControlItem1.CustomizationFormText = "卡 号";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 20);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "卡 号";
            this.layoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem1.TextToControlDistance = 5;
            // 
            // lbl下次随访日期
            // 
            this.lbl下次随访日期.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl下次随访日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl下次随访日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl下次随访日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl下次随访日期.Control = this.dte下次随访日期;
            this.lbl下次随访日期.CustomizationFormText = "产后休养地";
            this.lbl下次随访日期.Location = new System.Drawing.Point(0, 1114);
            this.lbl下次随访日期.MinSize = new System.Drawing.Size(50, 25);
            this.lbl下次随访日期.Name = "lbl下次随访日期";
            this.lbl下次随访日期.Size = new System.Drawing.Size(214, 25);
            this.lbl下次随访日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl下次随访日期.Text = "下次随访日期";
            this.lbl下次随访日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl下次随访日期.TextSize = new System.Drawing.Size(80, 14);
            this.lbl下次随访日期.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit档案编号;
            this.layoutControlItem2.CustomizationFormText = "档案编号 ";
            this.layoutControlItem2.Location = new System.Drawing.Point(226, 20);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(231, 24);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "档案编号 ";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.textEdit孕妇姓名;
            this.layoutControlItem3.CustomizationFormText = "孕妇姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(457, 20);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "孕妇姓名";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit身份证号;
            this.layoutControlItem4.CustomizationFormText = "身份证号 ";
            this.layoutControlItem4.Location = new System.Drawing.Point(226, 44);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(231, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "身份证号 ";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // lbl随访医生
            // 
            this.lbl随访医生.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl随访医生.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl随访医生.AppearanceItemCaption.Options.UseFont = true;
            this.lbl随访医生.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl随访医生.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl随访医生.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl随访医生.Control = this.textEdit随访医生;
            this.lbl随访医生.CustomizationFormText = "联系电话";
            this.lbl随访医生.Location = new System.Drawing.Point(214, 1114);
            this.lbl随访医生.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl随访医生.MinSize = new System.Drawing.Size(171, 24);
            this.lbl随访医生.Name = "lbl随访医生";
            this.lbl随访医生.Size = new System.Drawing.Size(219, 25);
            this.lbl随访医生.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl随访医生.Text = "随访医师签名";
            this.lbl随访医生.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl随访医生.TextSize = new System.Drawing.Size(80, 14);
            this.lbl随访医生.TextToControlDistance = 5;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.textEdit创建时间;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 1139);
            this.layoutControlItem25.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "创建时间:";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.textEdit最近更新时间;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(215, 1139);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(218, 24);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "最近更新时间:";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem27.Control = this.textEdit当前所属机构;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(433, 1139);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "当前所属机构:";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem27.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.Control = this.textEdit创建机构;
            this.layoutControlItem28.CustomizationFormText = "layoutControlItem28";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 1163);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(215, 24);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "创建机构: ";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.textEdit创建人;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(215, 1163);
            this.layoutControlItem29.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(218, 24);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "创建人:";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem30.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem30.Control = this.textEdit最近修改人;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(433, 1163);
            this.layoutControlItem30.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem30.MinSize = new System.Drawing.Size(54, 24);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(216, 24);
            this.layoutControlItem30.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem30.Text = "最近修改人:";
            this.layoutControlItem30.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem30.TextSize = new System.Drawing.Size(80, 0);
            this.layoutControlItem30.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "考核项:13  缺项:0  完整度:100% ";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 20);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(649, 20);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "考核项:29  缺项:0  完整度:0% ";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(60, 0);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 44);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(226, 24);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.textEdit居住状态;
            this.layoutControlItem11.CustomizationFormText = "居住状态 ";
            this.layoutControlItem11.Location = new System.Drawing.Point(457, 44);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(192, 24);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "居住状态 ";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.textEdit居住地址;
            this.layoutControlItem12.CustomizationFormText = "居住地址 ";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(171, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(649, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "居住地址 ";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // lbl填表日期
            // 
            this.lbl填表日期.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl填表日期.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl填表日期.AppearanceItemCaption.Options.UseFont = true;
            this.lbl填表日期.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl填表日期.Control = this.dte填表日期;
            this.lbl填表日期.CustomizationFormText = "填表日期";
            this.lbl填表日期.Location = new System.Drawing.Point(0, 92);
            this.lbl填表日期.MinSize = new System.Drawing.Size(50, 25);
            this.lbl填表日期.Name = "lbl填表日期";
            this.lbl填表日期.Size = new System.Drawing.Size(227, 25);
            this.lbl填表日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl填表日期.Text = "填表日期";
            this.lbl填表日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl填表日期.TextSize = new System.Drawing.Size(80, 20);
            this.lbl填表日期.TextToControlDistance = 5;
            // 
            // lbl填表孕周
            // 
            this.lbl填表孕周.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl填表孕周.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl填表孕周.AppearanceItemCaption.Options.UseFont = true;
            this.lbl填表孕周.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl填表孕周.Control = this.uc填表孕周;
            this.lbl填表孕周.CustomizationFormText = "填表孕周";
            this.lbl填表孕周.Location = new System.Drawing.Point(227, 92);
            this.lbl填表孕周.MinSize = new System.Drawing.Size(155, 24);
            this.lbl填表孕周.Name = "lbl填表孕周";
            this.lbl填表孕周.Size = new System.Drawing.Size(422, 25);
            this.lbl填表孕周.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl填表孕周.Text = "填表孕周";
            this.lbl填表孕周.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl填表孕周.TextSize = new System.Drawing.Size(80, 20);
            this.lbl填表孕周.TextToControlDistance = 5;
            // 
            // lbl孕妇年龄
            // 
            this.lbl孕妇年龄.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl孕妇年龄.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl孕妇年龄.AppearanceItemCaption.Options.UseFont = true;
            this.lbl孕妇年龄.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl孕妇年龄.Control = this.textEdit孕妇年龄;
            this.lbl孕妇年龄.CustomizationFormText = "年 龄";
            this.lbl孕妇年龄.Location = new System.Drawing.Point(0, 117);
            this.lbl孕妇年龄.MaxSize = new System.Drawing.Size(200, 24);
            this.lbl孕妇年龄.MinSize = new System.Drawing.Size(200, 24);
            this.lbl孕妇年龄.Name = "lbl孕妇年龄";
            this.lbl孕妇年龄.Size = new System.Drawing.Size(200, 24);
            this.lbl孕妇年龄.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl孕妇年龄.Text = "孕妇年龄";
            this.lbl孕妇年龄.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl孕妇年龄.TextSize = new System.Drawing.Size(80, 14);
            this.lbl孕妇年龄.TextToControlDistance = 5;
            // 
            // lbl丈夫姓名
            // 
            this.lbl丈夫姓名.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl丈夫姓名.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl丈夫姓名.AppearanceItemCaption.Options.UseFont = true;
            this.lbl丈夫姓名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl丈夫姓名.Control = this.textEdit丈夫姓名;
            this.lbl丈夫姓名.CustomizationFormText = "layoutControlItem17";
            this.lbl丈夫姓名.Location = new System.Drawing.Point(0, 141);
            this.lbl丈夫姓名.MaxSize = new System.Drawing.Size(200, 24);
            this.lbl丈夫姓名.MinSize = new System.Drawing.Size(200, 24);
            this.lbl丈夫姓名.Name = "lbl丈夫姓名";
            this.lbl丈夫姓名.Size = new System.Drawing.Size(200, 24);
            this.lbl丈夫姓名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl丈夫姓名.Text = "丈夫姓名";
            this.lbl丈夫姓名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl丈夫姓名.TextSize = new System.Drawing.Size(80, 14);
            this.lbl丈夫姓名.TextToControlDistance = 5;
            // 
            // lbl丈夫年龄
            // 
            this.lbl丈夫年龄.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl丈夫年龄.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl丈夫年龄.AppearanceItemCaption.Options.UseFont = true;
            this.lbl丈夫年龄.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl丈夫年龄.Control = this.textEdit丈夫年龄;
            this.lbl丈夫年龄.CustomizationFormText = "layoutControlItem18";
            this.lbl丈夫年龄.Location = new System.Drawing.Point(200, 141);
            this.lbl丈夫年龄.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl丈夫年龄.MinSize = new System.Drawing.Size(171, 24);
            this.lbl丈夫年龄.Name = "lbl丈夫年龄";
            this.lbl丈夫年龄.Size = new System.Drawing.Size(225, 24);
            this.lbl丈夫年龄.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl丈夫年龄.Text = "丈夫年龄";
            this.lbl丈夫年龄.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl丈夫年龄.TextSize = new System.Drawing.Size(80, 14);
            this.lbl丈夫年龄.TextToControlDistance = 5;
            // 
            // lbl丈夫电话
            // 
            this.lbl丈夫电话.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl丈夫电话.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl丈夫电话.AppearanceItemCaption.Options.UseFont = true;
            this.lbl丈夫电话.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl丈夫电话.Control = this.textEdit丈夫电话;
            this.lbl丈夫电话.CustomizationFormText = "layoutControlItem22";
            this.lbl丈夫电话.Location = new System.Drawing.Point(425, 141);
            this.lbl丈夫电话.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl丈夫电话.MinSize = new System.Drawing.Size(171, 24);
            this.lbl丈夫电话.Name = "lbl丈夫电话";
            this.lbl丈夫电话.Size = new System.Drawing.Size(224, 24);
            this.lbl丈夫电话.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl丈夫电话.Text = "丈夫电话";
            this.lbl丈夫电话.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl丈夫电话.TextSize = new System.Drawing.Size(80, 14);
            this.lbl丈夫电话.TextToControlDistance = 5;
            // 
            // lbl孕次
            // 
            this.lbl孕次.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl孕次.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl孕次.AppearanceItemCaption.Options.UseFont = true;
            this.lbl孕次.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl孕次.Control = this.textEdit孕次;
            this.lbl孕次.CustomizationFormText = "孕次";
            this.lbl孕次.Location = new System.Drawing.Point(0, 165);
            this.lbl孕次.MaxSize = new System.Drawing.Size(200, 24);
            this.lbl孕次.MinSize = new System.Drawing.Size(200, 24);
            this.lbl孕次.Name = "lbl孕次";
            this.lbl孕次.Size = new System.Drawing.Size(200, 24);
            this.lbl孕次.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl孕次.Text = "孕次";
            this.lbl孕次.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl孕次.TextSize = new System.Drawing.Size(80, 20);
            this.lbl孕次.TextToControlDistance = 5;
            // 
            // lbl产次
            // 
            this.lbl产次.AppearanceItemCaption.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl产次.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl产次.AppearanceItemCaption.Options.UseFont = true;
            this.lbl产次.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl产次.Control = this.flowLayoutPanel2;
            this.lbl产次.CustomizationFormText = "产 次";
            this.lbl产次.Location = new System.Drawing.Point(200, 165);
            this.lbl产次.MinSize = new System.Drawing.Size(135, 24);
            this.lbl产次.Name = "lbl产次";
            this.lbl产次.Size = new System.Drawing.Size(449, 24);
            this.lbl产次.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl产次.Text = "产 次";
            this.lbl产次.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl产次.TextSize = new System.Drawing.Size(80, 20);
            this.lbl产次.TextToControlDistance = 5;
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.flowLayoutPanel3;
            this.layoutControlItem35.CustomizationFormText = "末次月经";
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 189);
            this.layoutControlItem35.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem35.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(200, 25);
            this.layoutControlItem35.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem35.Text = "末次月经";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.dte预产期;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem36";
            this.layoutControlItem36.Location = new System.Drawing.Point(200, 189);
            this.layoutControlItem36.MaxSize = new System.Drawing.Size(250, 25);
            this.layoutControlItem36.MinSize = new System.Drawing.Size(250, 25);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(449, 25);
            this.layoutControlItem36.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem36.Text = "预产期";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // lbl孕产史
            // 
            this.lbl孕产史.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl孕产史.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl孕产史.AppearanceItemCaption.Options.UseFont = true;
            this.lbl孕产史.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl孕产史.Control = this.flowLayoutPanel7;
            this.lbl孕产史.CustomizationFormText = "孕 产 史";
            this.lbl孕产史.Location = new System.Drawing.Point(0, 310);
            this.lbl孕产史.MinSize = new System.Drawing.Size(189, 28);
            this.lbl孕产史.Name = "lbl孕产史";
            this.lbl孕产史.Size = new System.Drawing.Size(649, 28);
            this.lbl孕产史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl孕产史.Text = "孕 产 史";
            this.lbl孕产史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl孕产史.TextSize = new System.Drawing.Size(80, 20);
            this.lbl孕产史.TextToControlDistance = 5;
            // 
            // lbl身高
            // 
            this.lbl身高.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl身高.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl身高.AppearanceItemCaption.Options.UseFont = true;
            this.lbl身高.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl身高.Control = this.uc身高;
            this.lbl身高.CustomizationFormText = "身高";
            this.lbl身高.Location = new System.Drawing.Point(0, 338);
            this.lbl身高.MinSize = new System.Drawing.Size(189, 24);
            this.lbl身高.Name = "lbl身高";
            this.lbl身高.Size = new System.Drawing.Size(272, 24);
            this.lbl身高.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl身高.Text = "身高";
            this.lbl身高.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl身高.TextSize = new System.Drawing.Size(80, 20);
            this.lbl身高.TextToControlDistance = 5;
            // 
            // lbl体重
            // 
            this.lbl体重.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl体重.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl体重.AppearanceItemCaption.Options.UseFont = true;
            this.lbl体重.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl体重.Control = this.uc体重;
            this.lbl体重.CustomizationFormText = "体重";
            this.lbl体重.Location = new System.Drawing.Point(272, 338);
            this.lbl体重.MinSize = new System.Drawing.Size(189, 24);
            this.lbl体重.Name = "lbl体重";
            this.lbl体重.Size = new System.Drawing.Size(377, 24);
            this.lbl体重.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl体重.Text = "体重";
            this.lbl体重.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl体重.TextSize = new System.Drawing.Size(80, 20);
            this.lbl体重.TextToControlDistance = 5;
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.Control = this.textEdit体重指数;
            this.layoutControlItem45.CustomizationFormText = "体质指数";
            this.layoutControlItem45.Location = new System.Drawing.Point(0, 362);
            this.layoutControlItem45.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem45.MinSize = new System.Drawing.Size(139, 24);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(272, 24);
            this.layoutControlItem45.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem45.Text = "体质指数";
            this.layoutControlItem45.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem45.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem45.TextToControlDistance = 5;
            // 
            // lbl血压
            // 
            this.lbl血压.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血压.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl血压.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血压.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血压.Control = this.uc血压;
            this.lbl血压.CustomizationFormText = "血压";
            this.lbl血压.Location = new System.Drawing.Point(272, 362);
            this.lbl血压.MinSize = new System.Drawing.Size(131, 24);
            this.lbl血压.Name = "lbl血压";
            this.lbl血压.Size = new System.Drawing.Size(377, 24);
            this.lbl血压.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血压.Text = "血压";
            this.lbl血压.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血压.TextSize = new System.Drawing.Size(80, 14);
            this.lbl血压.TextToControlDistance = 5;
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.Control = this.textEdit心脏;
            this.layoutControlItem47.CustomizationFormText = "心脏";
            this.layoutControlItem47.Location = new System.Drawing.Point(82, 386);
            this.layoutControlItem47.MaxSize = new System.Drawing.Size(250, 25);
            this.layoutControlItem47.MinSize = new System.Drawing.Size(250, 25);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(250, 26);
            this.layoutControlItem47.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem47.Text = "心脏";
            this.layoutControlItem47.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem47.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem47.TextToControlDistance = 5;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.textEdit肺部;
            this.layoutControlItem48.CustomizationFormText = "肺部";
            this.layoutControlItem48.Location = new System.Drawing.Point(332, 386);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(317, 26);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "肺部";
            this.layoutControlItem48.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem48.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem48.TextToControlDistance = 5;
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.Control = this.textEdit外阴;
            this.layoutControlItem51.CustomizationFormText = "外阴";
            this.layoutControlItem51.Location = new System.Drawing.Point(82, 412);
            this.layoutControlItem51.MaxSize = new System.Drawing.Size(250, 25);
            this.layoutControlItem51.MinSize = new System.Drawing.Size(250, 25);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(250, 25);
            this.layoutControlItem51.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem51.Text = "外阴";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.Control = this.textEdit阴道;
            this.layoutControlItem54.CustomizationFormText = "阴道";
            this.layoutControlItem54.Location = new System.Drawing.Point(332, 412);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(317, 25);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Text = "阴道";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.Control = this.textEdit宫颈;
            this.layoutControlItem55.CustomizationFormText = "宫颈";
            this.layoutControlItem55.Location = new System.Drawing.Point(82, 437);
            this.layoutControlItem55.MaxSize = new System.Drawing.Size(250, 25);
            this.layoutControlItem55.MinSize = new System.Drawing.Size(250, 25);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(250, 25);
            this.layoutControlItem55.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem55.Text = "宫颈";
            this.layoutControlItem55.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem55.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem55.TextToControlDistance = 5;
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.Control = this.textEdit子宫;
            this.layoutControlItem57.CustomizationFormText = "子宫";
            this.layoutControlItem57.Location = new System.Drawing.Point(332, 437);
            this.layoutControlItem57.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(317, 25);
            this.layoutControlItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem57.Text = "子宫";
            this.layoutControlItem57.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem57.TextSize = new System.Drawing.Size(50, 20);
            this.layoutControlItem57.TextToControlDistance = 5;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.Control = this.textEdit附件;
            this.layoutControlItem59.CustomizationFormText = "附件";
            this.layoutControlItem59.Location = new System.Drawing.Point(82, 462);
            this.layoutControlItem59.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(567, 25);
            this.layoutControlItem59.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem59.Text = "附件";
            this.layoutControlItem59.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem59.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem59.TextToControlDistance = 5;
            // 
            // lbl总体评估
            // 
            this.lbl总体评估.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl总体评估.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl总体评估.AppearanceItemCaption.Options.UseFont = true;
            this.lbl总体评估.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl总体评估.Control = this.flowLayoutPanel14;
            this.lbl总体评估.CustomizationFormText = "总体评估";
            this.lbl总体评估.Location = new System.Drawing.Point(0, 982);
            this.lbl总体评估.MinSize = new System.Drawing.Size(167, 24);
            this.lbl总体评估.Name = "lbl总体评估";
            this.lbl总体评估.Size = new System.Drawing.Size(649, 24);
            this.lbl总体评估.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl总体评估.Text = "总体评估";
            this.lbl总体评估.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl总体评估.TextSize = new System.Drawing.Size(80, 20);
            this.lbl总体评估.TextToControlDistance = 5;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl尿常规,
            this.lbl血常规,
            this.lbl血型,
            this.layoutControlItem66,
            this.lbl血糖,
            this.lbl肝功能,
            this.lbl肾功能,
            this.layoutControlItem68,
            this.layoutControlItem69,
            this.lbl乙型肝炎五项,
            this.layoutControlItem71,
            this.layoutControlItem72,
            this.layoutControlItem73,
            this.emptySpaceItem9,
            this.emptySpaceItem8,
            this.emptySpaceItem10,
            this.emptySpaceItem11,
            this.emptySpaceItem12,
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.layoutControlGroup7});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 487);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(649, 495);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // lbl尿常规
            // 
            this.lbl尿常规.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl尿常规.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl尿常规.AppearanceItemCaption.Options.UseFont = true;
            this.lbl尿常规.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl尿常规.Control = this.flowLayoutPanel9;
            this.lbl尿常规.CustomizationFormText = "尿 常 规";
            this.lbl尿常规.Location = new System.Drawing.Point(82, 55);
            this.lbl尿常规.MinSize = new System.Drawing.Size(151, 55);
            this.lbl尿常规.Name = "lbl尿常规";
            this.lbl尿常规.Size = new System.Drawing.Size(567, 55);
            this.lbl尿常规.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl尿常规.Text = "尿 常 规";
            this.lbl尿常规.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl尿常规.TextSize = new System.Drawing.Size(90, 20);
            this.lbl尿常规.TextToControlDistance = 5;
            // 
            // lbl血常规
            // 
            this.lbl血常规.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血常规.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl血常规.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血常规.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血常规.Control = this.flowLayoutPanel8;
            this.lbl血常规.CustomizationFormText = "血 常 规";
            this.lbl血常规.Location = new System.Drawing.Point(82, 0);
            this.lbl血常规.MinSize = new System.Drawing.Size(151, 55);
            this.lbl血常规.Name = "lbl血常规";
            this.lbl血常规.Size = new System.Drawing.Size(567, 55);
            this.lbl血常规.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血常规.Text = "血 常 规";
            this.lbl血常规.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血常规.TextSize = new System.Drawing.Size(90, 20);
            this.lbl血常规.TextToControlDistance = 5;
            // 
            // lbl血型
            // 
            this.lbl血型.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血型.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl血型.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血型.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血型.Control = this.textEdit血型;
            this.lbl血型.CustomizationFormText = "ABO";
            this.lbl血型.Location = new System.Drawing.Point(174, 110);
            this.lbl血型.MinSize = new System.Drawing.Size(50, 25);
            this.lbl血型.Name = "lbl血型";
            this.lbl血型.Size = new System.Drawing.Size(185, 25);
            this.lbl血型.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血型.Text = "ABO";
            this.lbl血型.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血型.TextSize = new System.Drawing.Size(80, 20);
            this.lbl血型.TextToControlDistance = 5;
            // 
            // layoutControlItem66
            // 
            this.layoutControlItem66.Control = this.textEditRH;
            this.layoutControlItem66.CustomizationFormText = "RH* ";
            this.layoutControlItem66.Location = new System.Drawing.Point(174, 135);
            this.layoutControlItem66.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem66.Name = "layoutControlItem66";
            this.layoutControlItem66.Size = new System.Drawing.Size(185, 25);
            this.layoutControlItem66.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem66.Text = "RH* ";
            this.layoutControlItem66.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem66.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem66.TextToControlDistance = 5;
            // 
            // lbl血糖
            // 
            this.lbl血糖.AppearanceItemCaption.ForeColor = System.Drawing.Color.Black;
            this.lbl血糖.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血糖.Control = this.uc血糖;
            this.lbl血糖.CustomizationFormText = "血 糖* ";
            this.lbl血糖.Location = new System.Drawing.Point(82, 160);
            this.lbl血糖.MinSize = new System.Drawing.Size(202, 24);
            this.lbl血糖.Name = "lbl血糖";
            this.lbl血糖.Size = new System.Drawing.Size(567, 24);
            this.lbl血糖.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血糖.Text = "血 糖* ";
            this.lbl血糖.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血糖.TextSize = new System.Drawing.Size(90, 20);
            this.lbl血糖.TextToControlDistance = 5;
            // 
            // lbl肝功能
            // 
            this.lbl肝功能.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl肝功能.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl肝功能.AppearanceItemCaption.Options.UseFont = true;
            this.lbl肝功能.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl肝功能.Control = this.flowLayoutPanel10;
            this.lbl肝功能.CustomizationFormText = "肝 功 能";
            this.lbl肝功能.Location = new System.Drawing.Point(82, 184);
            this.lbl肝功能.MinSize = new System.Drawing.Size(159, 80);
            this.lbl肝功能.Name = "lbl肝功能";
            this.lbl肝功能.Size = new System.Drawing.Size(567, 80);
            this.lbl肝功能.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl肝功能.Text = "肝 功 能";
            this.lbl肝功能.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl肝功能.TextSize = new System.Drawing.Size(90, 20);
            this.lbl肝功能.TextToControlDistance = 5;
            // 
            // lbl肾功能
            // 
            this.lbl肾功能.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl肾功能.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl肾功能.AppearanceItemCaption.Options.UseFont = true;
            this.lbl肾功能.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl肾功能.Control = this.flowLayoutPanel11;
            this.lbl肾功能.CustomizationFormText = "肾 功 能";
            this.lbl肾功能.Location = new System.Drawing.Point(82, 264);
            this.lbl肾功能.MinSize = new System.Drawing.Size(202, 28);
            this.lbl肾功能.Name = "lbl肾功能";
            this.lbl肾功能.Size = new System.Drawing.Size(567, 28);
            this.lbl肾功能.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl肾功能.Text = "肾 功 能";
            this.lbl肾功能.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl肾功能.TextSize = new System.Drawing.Size(90, 20);
            this.lbl肾功能.TextToControlDistance = 5;
            // 
            // layoutControlItem68
            // 
            this.layoutControlItem68.Control = this.flowLayoutPanel12;
            this.layoutControlItem68.CustomizationFormText = "layoutControlItem68";
            this.layoutControlItem68.Location = new System.Drawing.Point(174, 292);
            this.layoutControlItem68.Name = "layoutControlItem68";
            this.layoutControlItem68.Size = new System.Drawing.Size(475, 24);
            this.layoutControlItem68.Text = "layoutControlItem68";
            this.layoutControlItem68.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem68.TextToControlDistance = 0;
            this.layoutControlItem68.TextVisible = false;
            // 
            // layoutControlItem69
            // 
            this.layoutControlItem69.Control = this.textEdit阴道清洁度;
            this.layoutControlItem69.CustomizationFormText = "阴道清洁度";
            this.layoutControlItem69.Location = new System.Drawing.Point(174, 316);
            this.layoutControlItem69.MaxSize = new System.Drawing.Size(200, 25);
            this.layoutControlItem69.MinSize = new System.Drawing.Size(200, 25);
            this.layoutControlItem69.Name = "layoutControlItem69";
            this.layoutControlItem69.Size = new System.Drawing.Size(475, 25);
            this.layoutControlItem69.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem69.Text = "阴道清洁度";
            this.layoutControlItem69.TextSize = new System.Drawing.Size(60, 14);
            // 
            // lbl乙型肝炎五项
            // 
            this.lbl乙型肝炎五项.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl乙型肝炎五项.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl乙型肝炎五项.AppearanceItemCaption.Options.UseFont = true;
            this.lbl乙型肝炎五项.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl乙型肝炎五项.Control = this.flowLayoutPanel13;
            this.lbl乙型肝炎五项.CustomizationFormText = "乙型肝炎五项";
            this.lbl乙型肝炎五项.Location = new System.Drawing.Point(82, 341);
            this.lbl乙型肝炎五项.MinSize = new System.Drawing.Size(159, 80);
            this.lbl乙型肝炎五项.Name = "lbl乙型肝炎五项";
            this.lbl乙型肝炎五项.Size = new System.Drawing.Size(567, 80);
            this.lbl乙型肝炎五项.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl乙型肝炎五项.Text = "乙型肝炎五项";
            this.lbl乙型肝炎五项.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl乙型肝炎五项.TextSize = new System.Drawing.Size(90, 20);
            this.lbl乙型肝炎五项.TextToControlDistance = 5;
            // 
            // layoutControlItem71
            // 
            this.layoutControlItem71.Control = this.textEdit梅毒血清学试验;
            this.layoutControlItem71.CustomizationFormText = "梅毒血清学试验* ";
            this.layoutControlItem71.Location = new System.Drawing.Point(82, 421);
            this.layoutControlItem71.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem71.Name = "layoutControlItem71";
            this.layoutControlItem71.Size = new System.Drawing.Size(288, 25);
            this.layoutControlItem71.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem71.Text = "梅毒血清学试验* ";
            this.layoutControlItem71.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem71.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem71.TextToControlDistance = 5;
            // 
            // layoutControlItem72
            // 
            this.layoutControlItem72.Control = this.textEditHIV抗体检测;
            this.layoutControlItem72.CustomizationFormText = "HIV抗体检测* ";
            this.layoutControlItem72.Location = new System.Drawing.Point(82, 446);
            this.layoutControlItem72.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem72.Name = "layoutControlItem72";
            this.layoutControlItem72.Size = new System.Drawing.Size(288, 25);
            this.layoutControlItem72.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem72.Text = "HIV抗体检测* ";
            this.layoutControlItem72.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem72.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem72.TextToControlDistance = 5;
            // 
            // layoutControlItem73
            // 
            this.layoutControlItem73.Control = this.textEditB超;
            this.layoutControlItem73.CustomizationFormText = "B 超* ";
            this.layoutControlItem73.Location = new System.Drawing.Point(82, 471);
            this.layoutControlItem73.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem73.MinSize = new System.Drawing.Size(152, 24);
            this.layoutControlItem73.Name = "layoutControlItem73";
            this.layoutControlItem73.Size = new System.Drawing.Size(288, 24);
            this.layoutControlItem73.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem73.Text = "B 超* ";
            this.layoutControlItem73.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem73.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem73.TextToControlDistance = 5;
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(370, 421);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(279, 25);
            this.emptySpaceItem9.Text = "emptySpaceItem9";
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(370, 446);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(279, 25);
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(370, 471);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(279, 24);
            this.emptySpaceItem10.Text = "emptySpaceItem10";
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(359, 110);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(290, 25);
            this.emptySpaceItem11.Text = "emptySpaceItem11";
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(359, 135);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(290, 25);
            this.emptySpaceItem12.Text = "emptySpaceItem12";
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(82, 495);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "辅助检查";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(80, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(80, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(80, 493);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "辅助检查";
            this.emptySpaceItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem4.TextVisible = true;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem5});
            this.layoutControlGroup6.Location = new System.Drawing.Point(82, 110);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Size = new System.Drawing.Size(92, 50);
            this.layoutControlGroup6.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            this.layoutControlGroup6.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "血 型 ";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(90, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(90, 48);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "血 型 ";
            this.emptySpaceItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(50, 20);
            this.emptySpaceItem5.TextVisible = true;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem6});
            this.layoutControlGroup7.Location = new System.Drawing.Point(82, 292);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(92, 49);
            this.layoutControlGroup7.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Text = "layoutControlGroup7";
            this.layoutControlGroup7.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "阴道分泌物* ";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(90, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(90, 47);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.Text = "阴道分泌物* ";
            this.emptySpaceItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(90, 20);
            this.emptySpaceItem6.TextVisible = true;
            // 
            // lbl既往病史
            // 
            this.lbl既往病史.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl既往病史.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl既往病史.AppearanceItemCaption.Options.UseFont = true;
            this.lbl既往病史.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl既往病史.Control = this.txt既往病史;
            this.lbl既往病史.CustomizationFormText = "既往病史";
            this.lbl既往病史.Location = new System.Drawing.Point(0, 214);
            this.lbl既往病史.Name = "lbl既往病史";
            this.lbl既往病史.Size = new System.Drawing.Size(649, 24);
            this.lbl既往病史.Text = "既往病史";
            this.lbl既往病史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl既往病史.TextSize = new System.Drawing.Size(80, 20);
            this.lbl既往病史.TextToControlDistance = 5;
            // 
            // lbl家族史
            // 
            this.lbl家族史.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl家族史.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl家族史.AppearanceItemCaption.Options.UseFont = true;
            this.lbl家族史.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl家族史.Control = this.txt家族史;
            this.lbl家族史.CustomizationFormText = "家 族 史";
            this.lbl家族史.Location = new System.Drawing.Point(0, 238);
            this.lbl家族史.Name = "lbl家族史";
            this.lbl家族史.Size = new System.Drawing.Size(649, 24);
            this.lbl家族史.Text = "家 族 史";
            this.lbl家族史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl家族史.TextSize = new System.Drawing.Size(80, 20);
            this.lbl家族史.TextToControlDistance = 5;
            // 
            // lbl个人史
            // 
            this.lbl个人史.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl个人史.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl个人史.AppearanceItemCaption.Options.UseFont = true;
            this.lbl个人史.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl个人史.Control = this.txt个人史;
            this.lbl个人史.CustomizationFormText = "个 人 史";
            this.lbl个人史.Location = new System.Drawing.Point(0, 262);
            this.lbl个人史.Name = "lbl个人史";
            this.lbl个人史.Size = new System.Drawing.Size(649, 24);
            this.lbl个人史.Text = "个 人 史";
            this.lbl个人史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl个人史.TextSize = new System.Drawing.Size(80, 20);
            this.lbl个人史.TextToControlDistance = 5;
            // 
            // lbl妇科手术史
            // 
            this.lbl妇科手术史.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl妇科手术史.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl妇科手术史.AppearanceItemCaption.Options.UseFont = true;
            this.lbl妇科手术史.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl妇科手术史.Control = this.txt妇科手术史;
            this.lbl妇科手术史.CustomizationFormText = "妇科手术史";
            this.lbl妇科手术史.Location = new System.Drawing.Point(0, 286);
            this.lbl妇科手术史.Name = "lbl妇科手术史";
            this.lbl妇科手术史.Size = new System.Drawing.Size(649, 24);
            this.lbl妇科手术史.Text = "妇科手术史";
            this.lbl妇科手术史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl妇科手术史.TextSize = new System.Drawing.Size(80, 20);
            this.lbl妇科手术史.TextToControlDistance = 5;
            // 
            // lbl保健指导
            // 
            this.lbl保健指导.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl保健指导.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl保健指导.AppearanceItemCaption.Options.UseFont = true;
            this.lbl保健指导.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl保健指导.Control = this.text保健指导;
            this.lbl保健指导.CustomizationFormText = "保健指导";
            this.lbl保健指导.Location = new System.Drawing.Point(0, 1006);
            this.lbl保健指导.MinSize = new System.Drawing.Size(50, 40);
            this.lbl保健指导.Name = "lbl保健指导";
            this.lbl保健指导.Size = new System.Drawing.Size(649, 40);
            this.lbl保健指导.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl保健指导.Text = "保健指导";
            this.lbl保健指导.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl保健指导.TextSize = new System.Drawing.Size(80, 20);
            this.lbl保健指导.TextToControlDistance = 5;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl听诊});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 386);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Size = new System.Drawing.Size(82, 26);
            this.layoutControlGroup3.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // lbl听诊
            // 
            this.lbl听诊.AllowHotTrack = false;
            this.lbl听诊.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl听诊.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl听诊.AppearanceItemCaption.Options.UseFont = true;
            this.lbl听诊.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl听诊.CustomizationFormText = "听诊";
            this.lbl听诊.Location = new System.Drawing.Point(0, 0);
            this.lbl听诊.MaxSize = new System.Drawing.Size(80, 0);
            this.lbl听诊.MinSize = new System.Drawing.Size(80, 24);
            this.lbl听诊.Name = "lbl听诊";
            this.lbl听诊.Size = new System.Drawing.Size(80, 24);
            this.lbl听诊.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl听诊.Text = "听诊";
            this.lbl听诊.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl听诊.TextSize = new System.Drawing.Size(80, 20);
            this.lbl听诊.TextVisible = true;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl妇科检查});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 412);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(82, 75);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // lbl妇科检查
            // 
            this.lbl妇科检查.AllowHotTrack = false;
            this.lbl妇科检查.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl妇科检查.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl妇科检查.AppearanceItemCaption.Options.UseFont = true;
            this.lbl妇科检查.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl妇科检查.CustomizationFormText = "妇科检查";
            this.lbl妇科检查.Location = new System.Drawing.Point(0, 0);
            this.lbl妇科检查.MaxSize = new System.Drawing.Size(80, 0);
            this.lbl妇科检查.MinSize = new System.Drawing.Size(80, 24);
            this.lbl妇科检查.Name = "lbl妇科检查";
            this.lbl妇科检查.Size = new System.Drawing.Size(80, 73);
            this.lbl妇科检查.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl妇科检查.Text = "妇科检查";
            this.lbl妇科检查.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl妇科检查.TextSize = new System.Drawing.Size(50, 20);
            this.lbl妇科检查.TextVisible = true;
            // 
            // lbl建册情况
            // 
            this.lbl建册情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl建册情况.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl建册情况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl建册情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl建册情况.Control = this.txt建册情况;
            this.lbl建册情况.CustomizationFormText = "建册情况";
            this.lbl建册情况.Location = new System.Drawing.Point(0, 1046);
            this.lbl建册情况.Name = "lbl建册情况";
            this.lbl建册情况.Size = new System.Drawing.Size(649, 24);
            this.lbl建册情况.Text = "建册情况";
            this.lbl建册情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl建册情况.TextSize = new System.Drawing.Size(80, 20);
            this.lbl建册情况.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt家属签名;
            this.layoutControlItem7.CustomizationFormText = "居民/家属签名";
            this.layoutControlItem7.Location = new System.Drawing.Point(433, 1114);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(216, 25);
            this.layoutControlItem7.Text = "居民/家属签名";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(80, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // lbl转诊
            // 
            this.lbl转诊.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl转诊.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl转诊.AppearanceItemCaption.Options.UseFont = true;
            this.lbl转诊.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl转诊.Control = this.textEdit转诊;
            this.lbl转诊.CustomizationFormText = "转诊";
            this.lbl转诊.Location = new System.Drawing.Point(0, 1070);
            this.lbl转诊.Name = "lbl转诊";
            this.lbl转诊.Size = new System.Drawing.Size(649, 44);
            this.lbl转诊.Text = "转诊";
            this.lbl转诊.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl转诊.TextSize = new System.Drawing.Size(80, 40);
            this.lbl转诊.TextToControlDistance = 5;
            // 
            // txt本人电话
            // 
            this.txt本人电话.Location = new System.Drawing.Point(266, 147);
            this.txt本人电话.Name = "txt本人电话";
            this.txt本人电话.Size = new System.Drawing.Size(382, 20);
            this.txt本人电话.StyleController = this.layoutControl1;
            this.txt本人电话.TabIndex = 86;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txt本人电话;
            this.layoutControlItem6.CustomizationFormText = "本人电话";
            this.layoutControlItem6.Location = new System.Drawing.Point(200, 117);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(449, 24);
            this.layoutControlItem6.Text = "本人电话";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(60, 14);
            // 
            // UC第1次产前随访服务记录表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl1);
            this.Name = "UC第1次产前随访服务记录表_显示";
            this.Size = new System.Drawing.Size(779, 498);
            this.Load += new System.EventHandler(this.UC第1次产前随访服务记录表_显示_Load);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            this.Controls.SetChildIndex(this.panelControlNavbar, 0);
            this.Controls.SetChildIndex(this.panelControl1, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.panelControlNavbar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt家属签名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt建册情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt妇科手术史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt个人史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家族史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt既往病史.Properties)).EndInit();
            this.flowLayoutPanel14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit总体评估.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditB超.Properties)).EndInit();
            this.flowLayoutPanel13.ResumeLayout(false);
            this.flowLayoutPanel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit阴道分泌物.Properties)).EndInit();
            this.flowLayoutPanel10.ResumeLayout(false);
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit体重指数.Properties)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt末次月经.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近修改人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit创建时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫年龄.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit丈夫姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居住状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit随访医生.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇年龄.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕妇姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit档案编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit孕次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit心脏.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit阴道.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit肺部.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit外阴.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit宫颈.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit子宫.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit附件.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit梅毒血清学试验.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditHIV抗体检测.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit血型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditRH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit阴道清洁度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte填表日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte预产期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte下次随访日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.text保健指导.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl下次随访日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl随访医生)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl填表日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl填表孕周)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl孕妇年龄)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl丈夫姓名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl丈夫年龄)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl丈夫电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl孕次)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl产次)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl孕产史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl身高)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl体重)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血压)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl总体评估)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl尿常规)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血常规)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血型)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血糖)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肝功能)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl肾功能)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl乙型肝炎五项)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl既往病史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家族史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl个人史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl妇科手术史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl保健指导)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl听诊)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl妇科检查)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl建册情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl转诊)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本人电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraEditors.SimpleButton btn添加;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn删除;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit textEdit丈夫电话;
        private DevExpress.XtraEditors.TextEdit textEdit丈夫年龄;
        private DevExpress.XtraEditors.TextEdit textEdit丈夫姓名;
        private DevExpress.XtraEditors.TextEdit textEdit居住地址;
        private DevExpress.XtraEditors.TextEdit textEdit居住状态;
        private DevExpress.XtraEditors.TextEdit textEdit随访医生;
        private DevExpress.XtraEditors.TextEdit textEdit孕妇年龄;
        private DevExpress.XtraEditors.TextEdit textEdit出生日期;
        private DevExpress.XtraEditors.TextEdit textEdit身份证号;
        private DevExpress.XtraEditors.TextEdit textEdit孕妇姓名;
        private DevExpress.XtraEditors.TextEdit textEdit档案编号;
        private DevExpress.XtraEditors.TextEdit txt卡号;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem lbl孕妇年龄;
        private DevExpress.XtraLayout.LayoutControlItem lbl下次随访日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl随访医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem lbl丈夫姓名;
        private DevExpress.XtraLayout.LayoutControlItem lbl丈夫年龄;
        private DevExpress.XtraLayout.LayoutControlItem lbl丈夫电话;
        private DevExpress.XtraEditors.TextEdit textEdit最近修改人;
        private DevExpress.XtraEditors.TextEdit textEdit创建人;
        private DevExpress.XtraEditors.TextEdit textEdit创建机构;
        private DevExpress.XtraEditors.TextEdit textEdit当前所属机构;
        private DevExpress.XtraEditors.TextEdit textEdit最近更新时间;
        private DevExpress.XtraEditors.TextEdit textEdit创建时间;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private Library.UserControls.UCTxtLblTxtLbl uc产次;
        private Library.UserControls.UCTxtLblTxtLbl uc填表孕周;
        private DevExpress.XtraLayout.LayoutControlItem lbl填表日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl填表孕周;
        private DevExpress.XtraLayout.LayoutControlItem lbl孕次;
        private DevExpress.XtraLayout.LayoutControlItem lbl产次;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private Library.UserControls.UCLblTxtLbl uc流产;
        private Library.UserControls.UCLblTxtLbl uc死胎;
        private Library.UserControls.UCLblTxtLbl uc死产;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.ComboBoxEdit textEdit孕次;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem lbl孕产史;
        private Library.UserControls.UCTxtLblTxtLbl uc血压;
        private DevExpress.XtraEditors.TextEdit textEdit体重指数;
        private Library.UserControls.UCTxtLbl uc体重;
        private Library.UserControls.UCTxtLbl uc身高;
        private Library.UserControls.UCLblTxtLbl uc新生儿死亡;
        private Library.UserControls.UCLblTxtLbl uc出生缺陷儿;
        private DevExpress.XtraLayout.LayoutControlItem lbl身高;
        private DevExpress.XtraLayout.LayoutControlItem lbl体重;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem lbl血压;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private Library.UserControls.UCTxtLbl uc血糖;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private Library.UserControls.UCLblTxtLbl uc血红蛋白值;
        private Library.UserControls.UCLblTxtLbl uc白细胞计数值;
        private Library.UserControls.UCLblTxtLbl uc血小板计数值;
        private Library.UserControls.UCLblTxt uc血常规其他;
        private DevExpress.XtraLayout.EmptySpaceItem lbl听诊;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraLayout.EmptySpaceItem lbl妇科检查;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem lbl血常规;
        private DevExpress.XtraLayout.LayoutControlItem lbl尿常规;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem lbl血糖;
        private DevExpress.XtraLayout.LayoutControlItem lbl肝功能;
        private Library.UserControls.UCLblTxtLbl uc血清谷丙转氨酶;
        private Library.UserControls.UCLblTxtLbl uc血清谷草转氨酶;
        private Library.UserControls.UCLblTxtLbl uc白蛋白;
        private Library.UserControls.UCLblTxtLbl uc总胆红素;
        private Library.UserControls.UCLblTxtLbl uc结合胆红素;
        private Library.UserControls.UCLblTxt uc尿蛋白;
        private Library.UserControls.UCLblTxt uc尿糖;
        private Library.UserControls.UCLblTxt uc尿酮体;
        private Library.UserControls.UCLblTxt uc尿潜血;
        private Library.UserControls.UCLblTxt uc尿常规其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl血型;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem66;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private Library.UserControls.UCLblTxtLbl uc血清肌酐;
        private Library.UserControls.UCLblTxtLbl uc血尿素氮;
        private DevExpress.XtraLayout.LayoutControlItem lbl肾功能;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem68;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem69;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel13;
        private Library.UserControls.UCLblTxt uc乙型肝炎表面抗原;
        private Library.UserControls.UCLblTxt uc乙型肝炎表面抗体;
        private Library.UserControls.UCLblTxt uc乙型肝炎e抗原;
        private Library.UserControls.UCLblTxt uc乙型肝炎e抗体;
        private Library.UserControls.UCLblTxt uc乙型肝炎核心抗体;
        private DevExpress.XtraLayout.LayoutControlItem lbl乙型肝炎五项;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel14;
        private DevExpress.XtraEditors.TextEdit textEditB超;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem71;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem72;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem73;
        private DevExpress.XtraLayout.LayoutControlItem lbl总体评估;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.TextEdit txt妇科手术史;
        private DevExpress.XtraEditors.TextEdit txt个人史;
        private DevExpress.XtraEditors.TextEdit txt家族史;
        private DevExpress.XtraEditors.TextEdit txt既往病史;
        private DevExpress.XtraEditors.TextEdit txt末次月经;
        private DevExpress.XtraLayout.LayoutControlItem lbl既往病史;
        private DevExpress.XtraLayout.LayoutControlItem lbl家族史;
        private DevExpress.XtraLayout.LayoutControlItem lbl个人史;
        private DevExpress.XtraLayout.LayoutControlItem lbl妇科手术史;
        private DevExpress.XtraEditors.TextEdit textEdit心脏;
        private DevExpress.XtraEditors.TextEdit textEdit阴道;
        private DevExpress.XtraEditors.TextEdit textEdit肺部;
        private DevExpress.XtraEditors.TextEdit textEdit外阴;
        private DevExpress.XtraEditors.TextEdit textEdit宫颈;
        private DevExpress.XtraEditors.TextEdit textEdit子宫;
        private DevExpress.XtraEditors.TextEdit textEdit附件;
        private DevExpress.XtraEditors.TextEdit textEdit总体评估;
        private DevExpress.XtraEditors.TextEdit textEdit梅毒血清学试验;
        private DevExpress.XtraEditors.TextEdit textEditHIV抗体检测;
        private DevExpress.XtraLayout.LayoutControlItem lbl保健指导;
        private DevExpress.XtraEditors.TextEdit textEdit血型;
        private DevExpress.XtraEditors.TextEdit textEditRH;
        private DevExpress.XtraEditors.TextEdit textEdit阴道分泌物;
        private DevExpress.XtraEditors.TextEdit textEdit阴道清洁度;
        private DevExpress.XtraEditors.TextEdit dte填表日期;
        private DevExpress.XtraEditors.TextEdit dte预产期;
        private DevExpress.XtraEditors.TextEdit dte下次随访日期;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.MemoEdit text保健指导;
        private DevExpress.XtraEditors.TextEdit txt建册情况;
        private DevExpress.XtraLayout.LayoutControlItem lbl建册情况;
        private System.Windows.Forms.RichTextBox textEdit转诊;
        private DevExpress.XtraEditors.TextEdit txt家属签名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem lbl转诊;
        private DevExpress.XtraEditors.SimpleButton sbtn查看查体化验单;
        private DevExpress.XtraEditors.TextEdit txt本人电话;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}
