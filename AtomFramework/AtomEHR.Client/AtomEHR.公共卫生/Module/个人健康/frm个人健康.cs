﻿using AtomEHR.Business;
using AtomEHR.Business.BLL_Base;
using AtomEHR.Business.BLL_Business;
using AtomEHR.Library;
using AtomEHR.Models;
using AtomEHR.公共卫生.Module.个人健康.儿童健康信息;
using AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息;
using AtomEHR.公共卫生.Module.个人健康.家庭健康信息;
using AtomEHR.公共卫生.util;
using DevExpress.XtraNavBar;
using DevExpress.XtraSplashScreen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Common;
using AtomEHR.公共卫生.Module.个人健康.体检就诊信息;
using AtomEHR.公共卫生.Module.个人健康.老年人健康管理;
using AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息;
using AtomEHR.公共卫生.Module.个人健康.妇女健康管理;
using AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理;

namespace AtomEHR.公共卫生.Module.个人健康
{
    public partial class frm个人健康 : frmBase
    {
        #region 变量
        //public static int countSum = 0;
        public string _familyDocNo;      //家庭档案编号，方便页面调用
        public string _docNo;         //当前被操作的人的档案编号
        public DataSet _currentBusiness;    //保存整个家庭档案的全部成员基本数据
        public bll健康档案 _bll健康档案 = new bll健康档案();
        public bll健康档案_个人健康特征 _bll健康档案特征 = new bll健康档案_个人健康特征();
        public DataSet _家庭全部成员健康档案;
        public DataSet _个人健康特征;
        public object _param;
        Control ctrlExists = null;
        //add by wjz 20151109
        public util.LayoutControlScrollHelper _helper;
        #endregion

        #region 构造函数
        public frm个人健康()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 此页面的显示方式
        /// </summary>
        /// <param name="isPop">页面的显示方式，如果是点击修改按钮进来的话，则导航栏隐藏，否则显示</param>
        /// <param name="docNo">档案编号，家庭档案/个人档案</param>
        public frm个人健康(bool isMoidfy, string pageName, string familydocNo, string docNo, object parms)
        {
            InitializeComponent();
            //countSum++;
            _familyDocNo = familydocNo;
            _docNo = docNo;
            _param = parms;
            Common.dno = _docNo;

            _家庭全部成员健康档案 = _bll健康档案.GetAllUserByKey(_familyDocNo, _docNo, false);//获取健康档案的基本数据,包括当前人的基本数据，所有家庭成员的数据
            _个人健康特征 = _bll健康档案特征.GetBusinessByKey(docNo, false);//获取个人的健康数据
            Common.dt个人档案 = _家庭全部成员健康档案.Tables[tb_健康档案.__TableName];
            Common.dt个人健康特征 = _个人健康特征.Tables[tb_健康档案_个人健康特征.__TableName];
            Common.frm个人健康 = this;
            Bind健康干预提醒();
            //点击修改按钮进来的
            //if (isMoidfy)
            //{
            //    this.dockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;//.navBarControl1.Visible = !isMoidfy;      //隐藏左边的导航栏
            //}
            //else
            //{
            //    this.dockPanel1.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;//.navBarControl1.Visible = !isMoidfy;      //隐藏左边的导航栏
            //}

            Control ctrl = null;
            switch (pageName)
            {
                case "frm个人健康档案":       //个人健康档案
                    if (isMoidfy)//点击修改按钮进来的
                        ctrl = new UC个人基本信息表(this, UpdateType.Modify);
                    else
                        ctrl = new UC个人基本信息表_显示(this);
                    break;
                case "frm家庭健康档案":       //家庭健康档案
                    ctrl = new UC家庭基本信息(this);
                    break;
                case "frm健康体检表":       //健康体检表
                    ctrl = new UC健康体检表_显示(this);
                    break;
                #region 儿童
                case "frm儿童基本信息":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童基本信息(_docNo, Common.dt个人档案, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童基本信息_显示(this, _docNo);
                    }
                    break;
                case "frm新生儿家庭访视记录表":
                    if (isMoidfy)
                    {
                        ctrl = new UC新生儿家庭访视记录表(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC新生儿家庭访视记录表_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_满月":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_满月(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_满月_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_3月龄":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_3月(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_3月_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_6月龄":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_6月(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_6月_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_8月龄":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_8月(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_8月_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_12月龄":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_12月(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_12月_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_18月龄":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_18月(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_18月_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_24月龄":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_24月(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_24月_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_30月龄":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_30月(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_30月_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_3岁":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_3岁(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_3岁_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_4岁":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_4岁(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_4岁_显示(_docNo);
                    }
                    break;

                case "frm儿童健康检查记录表_5岁":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_5岁(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_5岁_显示(_docNo);
                    }
                    break;
                case "frm儿童健康检查记录表_6岁":
                    if (isMoidfy)
                    {
                        ctrl = new UC儿童健康检查_6岁(_docNo, UpdateType.Modify, "SaveAndClose");
                    }
                    else
                    {
                        ctrl = new UC儿童健康检查_6岁_显示(_docNo);
                    }
                    break;

                #endregion
                #region 妇女

                case "frm妇女保健检查表":
                    ctrl = new UC妇女保健检查表_显示(this);
                    break;
                #endregion
                #region 孕妇
                case "frm孕产妇基本信息":
                    ctrl = new UC孕产妇基本信息(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"));
                    break;
                case "frm第一次产前随访记录":
                    ctrl = new UC第1次产前随访服务记录表_显示(this);
                    break;
                case "frm第二次产前随访记录":
                    //通过构造函数的parms参数传递ID,
                    object obj2次ID = _param;
                    string str2次ID = null;
                    if (obj2次ID != null)
                    {
                        str2次ID = obj2次ID.ToString();
                    }
                    if (isMoidfy)
                    {
                        if (string.IsNullOrWhiteSpace(str2次ID))
                        {
                            ctrl = null;
                        }
                        else
                        {
                            ctrl = new UC第2次产前随访服务记录表(_docNo, str2次ID, UpdateType.Modify, "SaveAndClose");
                        }
                    }
                    else
                    {
                        ctrl = new UC第2次产前随访服务记录表_显示(_docNo, str2次ID);
                    }
                    break;
                case "frm第三次产前随访记录":
                    //通过构造函数的parms参数传递ID,
                    object obj3次ID = _param;
                    string str3次ID = null;
                    if (obj3次ID != null)
                    {
                        str3次ID = obj3次ID.ToString();
                    }
                    if (isMoidfy)
                    {
                        if (string.IsNullOrWhiteSpace(str3次ID))
                        {
                            ctrl = null;
                        }
                        else
                        {
                            ctrl = new UC第3次产前随访服务记录表(_docNo, str3次ID, UpdateType.Modify, "SaveAndClose");
                        }
                    }
                    else
                    {
                        ctrl = new UC第3次产前随访服务记录表_显示(_docNo, str3次ID);
                    }
                    break;
                case "frm第四次产前随访记录":
                    //通过构造函数的parms参数传递ID,
                    object obj4次ID = _param;
                    string str4次ID = null;
                    if (obj4次ID != null)
                    {
                        str4次ID = obj4次ID.ToString();
                    }
                    if (isMoidfy)
                    {
                        if (string.IsNullOrWhiteSpace(str4次ID))
                        {
                            ctrl = null;
                        }
                        else
                        {
                            ctrl = new UC第4次产前随访服务记录表(_docNo, str4次ID, UpdateType.Modify, "SaveAndClose");
                        }
                    }
                    else
                    {
                        ctrl = new UC第4次产前随访服务记录表_显示(_docNo, str4次ID);
                    }
                    break;
                case "frm第五次产前随访记录":
                    //通过构造函数的parms参数传递ID,
                    object obj5次ID = _param;
                    string str5次ID = null;
                    if (obj5次ID != null)
                    {
                        str5次ID = obj5次ID.ToString();
                    }
                    if (isMoidfy)
                    {
                        if (string.IsNullOrWhiteSpace(str5次ID))
                        {
                            ctrl = null;
                        }
                        else
                        {
                            ctrl = new UC第5次产前随访服务记录表(_docNo, str5次ID, UpdateType.Modify, "SaveAndClose");
                        }
                    }
                    else
                    {
                        ctrl = new UC第5次产前随访服务记录表_显示(_docNo, str5次ID);
                    }
                    break;
                case "frm产后访视情况":
                    ctrl = new UC产后访视记录表_显示(this);
                    break;
                case "frm产后42天检查":
                    ctrl = new UC产后42天健康检查记录表_显示(this);
                    break;
                #endregion
                #region MyRegion
                case "frm残疾人随访服务表":
                    ctrl = new Module.个人健康.残疾人健康信息.UC残疾人康复服务随访记录表_显示(this);
                    break;
                #endregion
                case "frm接诊记录":       //接诊记录表
                    ctrl = new UC接诊记录表_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"), _param);
                    break;
                #region 转档

                case "frm转档申请":       //转档申请
                    ctrl = new UC个人基本信息表_显示(this);
                    break;
                case "frm转档查询":       //转档查询
                    ctrl = new UC个人基本信息表_显示(this);
                    break;

                #endregion
                #region 老年人

                case "frm老年人随访服务记录表":       //老年人随访
                    ctrl = new UC老年人生活自理能力评估表_显示(this);
                    break;
                case "frm老年人中医药健康管理":       //老年人中医药健康管理
                    if (_param != null && (_param as string) != "")
                    {
                        //ctrl = new UC老年人中医药健康管理服务记录表_显示(this);
                        ctrl = new UC老年人中医药健康管理服务记录表列表(this);//显示中医药管理随访表列表
                    }
                    else
                    {
                        //ctrl = new UC老年人中医药健康管理服务记录表列表(this);
                        ctrl = new UC老年人中医药健康管理服务记录表_显示(this);
                    }
                    break;
                #endregion
                #region 慢性病
                case "frm高血压患者管理卡":       //高血压患者管理卡表
                    ctrl = new UC高血压患者管理卡_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"));
                    break;
                case "frm高血压患者随访记录":       //高血压患者随访记录
                    ctrl = new UC高血压患者随访记录表_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"), _param);
                    break;
                case "frm糖尿病患者管理卡":       //糖尿病患者管理卡表
                    ctrl = new UC糖尿病患者管理卡_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"));
                    break;
                case "frm糖尿病患者随访记录":       //糖尿病患者随访记录
                    ctrl = new UC糖尿病患者随访记录表_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"), _param);
                    break;
                case "frm冠心病患者管理卡":       //冠心病患者管理卡表
                    ctrl = new UC冠心病患者管理卡_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"));
                    break;
                case "frm冠心病患者随访记录":       //冠心病患者随访记录
                    ctrl = new UC冠心病患者随访服务记录表_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"), _param);
                    break;
                case "frm脑卒中患者管理卡":       //脑卒中患者管理卡表
                    ctrl = new UC脑卒中患者管理卡_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"));
                    break;
                case "frm脑卒中患者随访记录":       //脑卒中患者随访记录
                    ctrl = new UC脑卒中患者随访记录表_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"), _param);
                    break;
                case "frm高血压高危干预":       //高血压高危干预随访记录
                    ctrl = new UC高血压高危干预_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"), _param);
                    break;
                #endregion
                #region 精神病
                case "frm精神病患者基本信息表":
                    ctrl = new UC重性精神疾病患者个人信息补充表_显示(this);
                    break;
                case "frm精神病患者随访服务表":
                    ctrl = new UC重性精神疾病患者随访服务记录表_显示(_docNo, _param.ToString());
                    break;
                #endregion
                case "frm肺结核第一次随访":
                    ctrl = new 肺结核.UC肺结核第一次随访_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"), _param);
                    break;
                case "frm肺结核后续随访":
                    ctrl = new 肺结核.UC肺结核后续随访_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"), _param);
                    break;
                case "frm个案随访表":
                    ctrl = new 艾滋病.UC个案随访表_显示(Common.dt个人档案.Select("个人档案编号='" + _docNo + "'"), _param);
                    break;
                default:
                    if (isMoidfy)//点击修改按钮进来的
                        ctrl = new UC个人基本信息表(this, UpdateType.Modify);
                    else
                        ctrl = new UC个人基本信息表_显示(this);
                    break;
            }
            if (isMoidfy)
            {
                this.Width = this.panelControl2.Size.Width;
                this.Height = 550;
            }
            else        //加载左边导航树的家庭成员信息
            {
                this.Size = new Size(1024, 550);
                LoadFamilyTree(_家庭全部成员健康档案.Tables[0]);
            }
            _param = null;
            ShowControl(ctrl);//显示对应的用户控件
        }
        #endregion

        #region Handle Events
        private void navBarControl1_MouseClick(object sender, MouseEventArgs e)
        {
            DevExpress.XtraNavBar.NavBarHitInfo hitInfo = this.navBarControl1.CalcHitInfo(new Point(e.X, e.Y));
            if (hitInfo != null && hitInfo.InGroupCaption && !hitInfo.InGroupButton)
            {
                hitInfo.Group.Expanded = !hitInfo.Group.Expanded;
                Control ctrl;
                Control exitCtrl = this.panelControl2.Controls.Count == 1 ? this.panelControl2.Controls[0] : null;
                if (hitInfo.Group.Name == "nav家庭健康信息")
                {
                    //判断是否存在家庭档案编号， 不存在家庭档案编号
                    if (string.IsNullOrEmpty(_familyDocNo))
                    {
                        Msg.ShowInformation("此人不在任何家庭中，不能进行此操作！");
                        return;
                    }
                    if (exitCtrl is UC家庭基本信息) return;
                    else ctrl = new UC家庭基本信息(this);
                }
                else
                {
                    ctrl = null;
                }

                if (ctrl != null)
                {
                    ShowControl(ctrl);
                }
            }
        }

        BackgroundWorker bgInvoke = new BackgroundWorker();
        private void frm个人健康_Load(object sender, EventArgs e)
        {
            this.panelControl2.Dock = DockStyle.Fill;
            this.panelControl3.SendToBack();
            this.panelControl1.BringToFront();
            
            bgInvoke.DoWork += bgInvoke_DoWork;
            bgInvoke.RunWorkerCompleted += bgInvoke_RunWorkerCompleted;

            //该功能暂未使用，先不显示
            barItem会诊记录表.Visible = false;
            //if (!bgInvoke.IsBusy)
            //    bgInvoke.RunWorkerAsync();
        }

        void bgInvoke_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();

            //this.OnLoad(e);
        }

        void bgInvoke_DoWork(object sender, DoWorkEventArgs e)
        {
            DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(WaitForm1));

            for (int i = 0; i < 10; i++)
            {
                System.Threading.Thread.Sleep(1000);
            }
        }
        #endregion

        #region 所有NavBarItem的点击事件
        /// <summary>
        /// 点击每个家庭成员姓名 所执行的事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void item_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            NavBarItem item = (NavBarItem)sender;

            //add by wjz 20151028 人名变色▽
            for (int index = 0; index < nav家庭成员信息.ItemLinks.Count; index++)
            {
                nav家庭成员信息.ItemLinks[index].Item.Appearance.ForeColor = Color.Black;
            }
            item.Appearance.ForeColor = Color.Red;
            //add by wjz 20151028 人名变色△

            if (item == null) return;
            if (item.Tag == null) return;
            _docNo = item.Tag as string;           //给全局变量赋值
            _param = null;
            Common.dno = _docNo;
            if (string.IsNullOrEmpty(_docNo)) return;
            item.AppearancePressed.BackColor = System.Drawing.Color.DarkRed;
            //判断个人健康档案是否建档
            _个人健康特征 = _bll健康档案特征.GetBusinessByKey(_docNo, false);
            Bind健康干预提醒();
            //DataTable table = _个人健康.Tables[0];
            Control frm;
            if (_个人健康特征.Tables[tb_健康档案_个人健康特征.__TableName].Rows.Count == 1 && _个人健康特征.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.个人基本信息表] != null && !string.IsNullOrEmpty(Convert.ToString(_个人健康特征.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.个人基本信息表])))
            {
                frm = new UC个人基本信息表_显示(this);
            }
            else
            {
                frm = new UC个人基本信息表(this, UpdateType.AddPeople);
            }
            ShowControl(frm);
            SetNavBarShow(_家庭全部成员健康档案.Tables[0]);
        }
        private void navBarControl1_Click(object sender, EventArgs e)
        {
            //NavBarItem barItem = (NavBarItem)sender;
            //if (barItem == null) return;
            //ShowControl(barItem.Name);
        }

        #region 儿童相关信息
        //儿童基本信息

        private void navBarItem6_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            //ShowControl(sender);
            if (string.IsNullOrWhiteSpace(_docNo))
            {
                return;
            }

            Control ctl = null;
            Control exitctl = this.panelControl2.Controls.Count == 1 ? this.panelControl2.Controls[0] : null;
            try
            {
                //从数据库中获取儿童基本信息
                DataSet ds = new bll儿童基本信息().Get儿童基本信息ByKey(_docNo);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    if (exitctl is UC儿童基本信息_显示) return;
                    //ctl = new UC儿童基本信息_显示(this,ds.Tables[0]);
                    else ctl = new UC儿童基本信息_显示(this, _docNo);
                }
                else
                {
                    if (Msg.AskQuestion("是否继续添加儿童基本信息表！"))
                    {
                        if (exitctl is UC儿童基本信息) return;
                        //儿童基本信息表添加画面显示
                        else ctl = new UC儿童基本信息(_docNo, Common.dt个人档案, UpdateType.Add, null);
                    }
                    else
                    {
                        if (exitctl is UC个人基本信息表_显示) return;
                        //显示个人基本信息表
                        else ctl = new UC个人基本信息表_显示(this);
                    }
                }
                ShowControl(ctl);
            }
            catch (Exception ex)
            {
                Msg.ShowException(ex);
            }
        }

        private void navBar新生儿访视_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            DataSet ds新生儿 = new bll儿童新生儿访视记录().GetInfo(_docNo);
            Control exitctl = this.panelControl2.Controls.Count == 1 ? this.panelControl2.Controls[0] : null;
            string str儿童信息数量 = ds新生儿.Tables[0].Rows[0][0].ToString();
            string str新生儿数量 = ds新生儿.Tables[0].Rows[1][0].ToString();
            if (str儿童信息数量 != "0" && str新生儿数量 != "0")
            {
                if (exitctl is UC新生儿家庭访视记录表_显示) return;
                else
                {
                    Control uc新生儿 = new UC新生儿家庭访视记录表_显示(_docNo);
                    ShowControl(uc新生儿);
                }
            }
            else if (str儿童信息数量 != "0" && str新生儿数量 == "0")
            {
                if (exitctl is UC新生儿家庭访视记录表) return;
                else
                {
                    Control uc新生儿 = new UC新生儿家庭访视记录表(_docNo, UpdateType.Add, null);
                    ShowControl(uc新生儿);
                }
            }
            else if (str儿童信息数量 == "0")
            {
                Msg.ShowInformation("如果需要创建新生儿访视记录，请先添加此人的儿童基本信息。");
            }
        }
        #endregion
        private void barItem_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            this._param = "";
            ShowControl(sender);
        }
        #endregion

        #region Private Method
        /// <summary>
        ///    点击NavBarItem打开页面的统一方法
        /// </summary>
        /// <param name="sender"></param>
        private void ShowControl(object sender)
        {
            Control ctrl = null;
            try
            {
                NavBarItem barItem = (NavBarItem)sender;
                if (barItem == null) return;
                splashScreenManager1.ShowWaitForm();

                ctrl = AtomEHR.公共卫生.Module.个人健康.Common.ShowControl(barItem.Name, this);
                Bind健康干预提醒();//加载完以后  重新绑定 健康干预提醒
                if (ctrl != null)
                {
                    for (int i = 0; i < panelControl2.Controls.Count - 1; i++)
                    {
                        panelControl2.Controls[i].Dispose();
                    }
                    this.panelControl2.Controls.Clear();
                    SetLayoutControlMouseWheel(ctrl);
                    //add by wjz 20151109 作用：通过“添加”、“删除”进入新画面后，允许滚动页面 △
                    this.panelControl2.Controls.Add(ctrl);
                    ctrl.Dock = DockStyle.Fill;

                }
                else
                {
                    this.splashScreenManager1.CloseWaitForm();
                    return;
                }
                this.splashScreenManager1.CloseWaitForm();
            }
            catch
            {
                if (this.splashScreenManager1.IsSplashFormVisible)
                {
                    this.splashScreenManager1.CloseWaitForm();
                }
            }
            finally
            {
                if (this.splashScreenManager1.IsSplashFormVisible)
                {
                    this.splashScreenManager1.CloseWaitForm();
                }
                if (ctrlExists != null)
                {
                    ctrlExists.Dispose();
                }
                ctrlExists = ctrl;
            }
        }
        private void ShowControl(Control ctrl)
        {
            try
            {
                splashScreenManager1.ShowWaitForm();
                if (ctrl != null)
                {
                    for (int i = 0; i < panelControl2.Controls.Count - 1; i++)
                    {
                        panelControl2.Controls[i].Dispose();
                    }
                    this.panelControl2.Controls.Clear();
                    SetLayoutControlMouseWheel(ctrl);
                    //add by wjz 20151109 作用：通过“添加”、“删除”进入新画面后，允许滚动页面 △

                    this.panelControl2.Controls.Add(ctrl);

                    //add by wjz 20150909 调整显示效果
                    ctrl.Dock = DockStyle.Fill;
                }
                else
                {
                    this.splashScreenManager1.CloseWaitForm();
                    return;
                }
                this.splashScreenManager1.CloseWaitForm();
            }
            catch
            {
                if (this.splashScreenManager1.IsSplashFormVisible)
                {
                    this.splashScreenManager1.CloseWaitForm();
                }

            }
            finally
            {
                if (this.splashScreenManager1.IsSplashFormVisible)
                {
                    this.splashScreenManager1.CloseWaitForm();
                }
                if (ctrlExists != null)
                {
                    ctrlExists.Dispose();
                }
                ctrlExists = ctrl;
            }
        }
        /// <summary>
        /// 根据名称获取 页面控件
        /// </summary>
        /// <param name="ctrlName"></param>
        private void ShowControl(string ctrlName)
        {
            Control ctrl = null;
            try
            {
                ctrl = AtomEHR.公共卫生.Module.个人健康.Common.ShowControl(ctrlName, this);
                if (ctrl != null)
                {
                    this.panelControl2.Controls.Clear();
                    SetLayoutControlMouseWheel(ctrl);
                    //add by wjz 20151109 作用：通过“添加”、“删除”进入新画面后，允许滚动页面 △
                    this.panelControl2.Controls.Add(ctrl);
                    ctrl.Dock = DockStyle.Fill;
                }
                else
                {
                    return;
                }
            }
            catch
            {
                if (this.splashScreenManager1.IsSplashFormVisible)
                {
                    this.splashScreenManager1.CloseWaitForm();
                }
            }
            finally
            {
                if (ctrlExists != null)
                {
                    ctrlExists.Dispose();
                }
                ctrlExists = ctrl;
            }
        }
        public void LoadFamilyTree(DataTable table)
        {
            NavBarItem item = null;
            nav家庭成员信息.ItemLinks.Clear();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                item = new NavBarItem();

                item.Name = table.Rows[i]["个人档案编号"] as string;
                item.Caption = table.Rows[i]["姓名"].ToString() + "(" + table.Rows[i]["与户主关系"] + ")";
                item.Tag = table.Rows[i]["个人档案编号"] as string;
                if (table.Rows[i]["个人档案编号"] as string == _docNo)
                {
                    //设置背景色，突出显示被操作人
                    item.Appearance.ForeColor = System.Drawing.Color.DarkRed;
                }
                item.LinkClicked -= item_LinkClicked;
                item.LinkClicked += item_LinkClicked;
                nav家庭成员信息.ItemLinks.Add(item);
            }

            //add by wjz 20151028 人名变色▽
            for (int index = 0; index < nav家庭成员信息.ItemLinks.Count; index++)
            {
                nav家庭成员信息.ItemLinks[index].Item.Appearance.ForeColor = Color.Black;
                if (nav家庭成员信息.ItemLinks[index].Item.Tag.ToString() == _docNo)
                {
                    nav家庭成员信息.ItemLinks[index].Item.Appearance.ForeColor = Color.Red;
                }
            }
            SetNavBarShow(table);

        }
        /// <summary>
        /// 根据家庭档案编号 来查询
        /// </summary>
        /// <param name="familyNo"></param>
        public void LoadFamilyTree(string familyNo)
        {
            _家庭全部成员健康档案 = _bll健康档案.GetAllUserByKey(familyNo, "", false);
            DataTable table = _家庭全部成员健康档案.Tables[tb_健康档案.__TableName];
            NavBarItem item = null;
            nav家庭成员信息.ItemLinks.Clear();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                item = new NavBarItem();
                item.Name = table.Rows[i]["个人档案编号"] as string;
                item.Caption = util.DESEncrypt.DES解密(table.Rows[i]["姓名"].ToString()) + "(" + table.Rows[i]["与户主关系"] + ")";
                item.Tag = table.Rows[i]["个人档案编号"] as string;
                item.LinkClicked -= item_LinkClicked;
                item.LinkClicked += item_LinkClicked;
                nav家庭成员信息.ItemLinks.Add(item);
            }
            //add by wjz 20151028 人名变色▽
            for (int index = 0; index < nav家庭成员信息.ItemLinks.Count; index++)
            {
                nav家庭成员信息.ItemLinks[index].Item.Appearance.ForeColor = Color.Black;
                if (nav家庭成员信息.ItemLinks[index].Item.Tag.ToString() == _docNo)
                {
                    nav家庭成员信息.ItemLinks[index].Item.Appearance.ForeColor = Color.Red;
                }
            }
            SetNavBarShow(table);
        }
        private void SetNavBarShow(DataTable table)
        {
            //获取当前被操作用户的数据
            DataRow[] Users = table.Select("个人档案编号='" + _docNo + "'");
            DataRow currentUser;
            if (Users.Length != 1)
            {
                return;
            }
            currentUser = Users[0];
            int age = GetAge(currentUser["出生日期"].ToString());
            if (age >= 65)//老年人
            {
                this.nav老年人健康管理.Visible = true;
                //在此判断   老年人  是否存在老年人基本信息表
                bll老年人基本信息 bll老年人 = new bll老年人基本信息();
                if (!bll老年人.CheckNoExists(_docNo))
                {
                    DataSet ds老年人 = bll老年人.GetBusinessByKey("", true);
                    bll老年人.NewBusiness();
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.个人档案编号] = _docNo;
                    //row老年人[tb_老年人基本信息.卡号] = "";
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.家庭档案编号] = _familyDocNo;
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.姓名] = currentUser["姓名"];
                    //ds老年人.Tables[0].Rows[0][tb_老年人基本信息.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.身份证号] = currentUser["身份证号"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.工作单位] = currentUser["工作单位"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.联系电话] = currentUser["联系人电话"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.省] = "37";
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.市] = currentUser["市编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.区] = currentUser["区编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.街道] = currentUser["街道编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.居委会] = currentUser["居委会编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.居住地址] = currentUser["居住地址"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.新农合号] = currentUser["新农合号"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.所属片区] = currentUser["所属片区"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.常住类型] = currentUser["常住类型编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.性别] = currentUser["性别编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.出生日期] = currentUser["出生日期"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.民族] = currentUser["民族编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.文化程度] = currentUser["文化程度编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.职业] = currentUser["职业编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.婚姻状况] = currentUser["婚姻状况编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.医疗费支付类型] = currentUser["医疗费支付类编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.发生时间] = bll老年人.ServiceDateTime.Substring(0, 10);
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.创建时间] = bll老年人.ServiceDateTime;
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.修改时间] = bll老年人.ServiceDateTime;
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.创建人] = currentUser["创建人"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.修改人] = currentUser["修改人"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.创建机构] = currentUser["创建机构编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.档案状态] = currentUser["档案状态编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.所属机构] = currentUser["所属机构编码"];
                    ds老年人.Tables[0].Rows[0][tb_老年人基本信息.与户主关系] = currentUser["与户主关系编码"];
                    SaveResult result = bll老年人.Save(ds老年人);
                    if (result.Success)
                    {

                    }
                }
            }
            else
            {
                this.nav老年人健康管理.Visible = false;
            }
            string gender = currentUser["性别"].ToString();
            if (gender == "男")
            {
                this.nav妇女健康管理.Visible = false;
                this.nav孕产妇健康管理.Visible = false;
            }
            if (gender == "女" && age >= 15)
            {
                this.nav妇女健康管理.Visible = true;
                this.nav孕产妇健康管理.Visible = true;
            }
            else
            {
                this.nav孕产妇健康管理.Visible = false;
                this.nav妇女健康管理.Visible = false;
            }
        }
        /// <summary>
        /// 重新加载页面
        /// familyNo 默认设置为null，主要是用在 注销了一个用户以后 重新修改保存此用户时用到
        /// </summary>
        public void ReLoad(string pageName, string _newDocNo, string familyNo = null)
        {
            _docNo = _newDocNo;
            if (!string.IsNullOrEmpty(familyNo)) _familyDocNo = familyNo;
            _家庭全部成员健康档案 = _bll健康档案.GetAllUserByKey(_familyDocNo, _docNo, false);//获取健康档案的基本数据,包括当前人的基本数据，所有家庭成员的数据
            _个人健康特征 = _bll健康档案特征.GetBusinessByKey(_docNo, false);//获取个人的健康数据
            Common.dno = _docNo;
            Common.dt个人档案 = _家庭全部成员健康档案.Tables[tb_健康档案.__TableName];
            Common.dt个人健康特征 = _个人健康特征.Tables[tb_健康档案_个人健康特征.__TableName];
            Common.frm个人健康 = this;
            Control ctrl = null;
            switch (pageName)
            {
                case "UC个人基本信息表":       //个人健康档案
                    ctrl = new UC个人基本信息表_显示(this);
                    break;
                case "UC健康体检表":       //个人健康档案
                    ctrl = new UC健康体检表_显示(this);
                    break;
                case "UC家庭基本信息":       //家庭基本信息  转档成功
                    ctrl = new UC个人基本信息表_显示(this);
                    break;
                default:
                    break;
            }
            LoadFamilyTree(_家庭全部成员健康档案.Tables[0]);
            ShowControl(ctrl);//显示对应的用户控件
            Bind健康干预提醒();
        }
        private int GetAge(String strBirthday)
        {
            int returnAge = 0;
            int birthYear = 0;
            int birthMonth = 0;
            int birthDay = 0;
            String[] strBirthdayArr = strBirthday.Split('-');
            if (strBirthdayArr.Length == 3)
            {
                birthYear = Int32.Parse(strBirthdayArr[0]);
                birthMonth = Int32.Parse(strBirthdayArr[1]);
                birthDay = Int32.Parse(strBirthdayArr[2]);
            }

            String date = _bll健康档案.ServiceDateTime;
            int nowYear = Int32.Parse(date.Substring(0, 4));
            int nowMonth = Int32.Parse(date.Substring(5, 2));
            int nowDay = Int32.Parse(date.Substring(8, 2));
            if (nowYear == birthYear)
            {
                returnAge = 0;
            }
            else
            {
                int ageDiff = nowYear - birthYear;//根据出生年份判断年龄

                #region 根据出生月份和日期判断年龄(未用)
                //if (ageDiff > 0)
                //{
                //    if (nowMonth == birthMonth)
                //    {
                //        int dayDiff = nowDay - birthDay;
                //        if (dayDiff < 0)
                //            returnAge = ageDiff - 1;
                //        else
                //            returnAge = ageDiff;
                //    }
                //    else
                //    {
                //        int monthDiff = nowMonth - birthMonth;
                //        if (monthDiff < 0)
                //            returnAge = ageDiff - 1;
                //        else
                //            returnAge = ageDiff;
                //    }
                //}
                //else
                //{
                //    returnAge = -1;
                //}
                #endregion

                returnAge = ageDiff;
            }

            return returnAge;
        }
        #endregion

        private void frm个人健康_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Dispose();
        }

        private void panelControl1_MouseEnter(object sender, EventArgs e)
        {
            this.panelControl1.SendToBack();
            this.panelControl3.Visible = true;
            this.panelControl3.BringToFront();
        }

        private void panelControl3_MouseLeave(object sender, EventArgs e)
        {
            this.panelControl3.SendToBack();
            this.panelControl1.BringToFront();
        }

        /// <summary>
        /// 绑定健康干预提醒数据
        /// </summary>
        /// <param name="grjktz"></param>
        public void Bind健康干预提醒()
        {
            if (_个人健康特征 == null || _个人健康特征.Tables[tb_健康档案_个人健康特征.__TableName].Rows.Count != 1) return;
            DataTable table = _个人健康特征.Tables[tb_健康档案_个人健康特征.__TableName];
            DataTable newTable = new DataTable("健康干预");
            DataColumn col业务表单 = newTable.Columns.Add("业务表单", System.Type.GetType("System.String"));
            DataColumn col缺项 = newTable.Columns.Add("缺项", System.Type.GetType("System.String"));
            DataColumn col完整度 = newTable.Columns.Add("完整度", System.Type.GetType("System.String"));

            DataRow[] Users = _家庭全部成员健康档案.Tables[tb_健康档案.__TableName].Select("个人档案编号='" + _docNo + "'");
            DataRow currentUser;
            if (Users.Length != 1)
            {
                return;
            }
            currentUser = Users[0];
            int age = GetAge(currentUser["出生日期"].ToString());

            //健康档案
            if (table.Rows[0][tb_健康档案_个人健康特征.个人基本信息表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.个人基本信息表].ToString()))
            {

                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.个人基本信息表);
            }
            //健康体检
            if (table.Rows[0][tb_健康档案_个人健康特征.健康体检] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.健康体检].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.健康体检);
            }
            //家庭档案
            if (table.Rows[0][tb_健康档案_个人健康特征.家庭档案] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.家庭档案].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.家庭档案);
            }
            //转诊记录
            if (table.Rows[0][tb_健康档案_个人健康特征.接诊记录] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.接诊记录].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.接诊记录);
            }
            //会诊记录
            if (table.Rows[0][tb_健康档案_个人健康特征.会诊记录] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.会诊记录].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.会诊记录);
            }
            //儿童基本信息
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童基本信息);
            }
            //新生儿随访记录
            if (table.Rows[0][tb_健康档案_个人健康特征.新生儿随访记录] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.新生儿随访记录].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.新生儿随访记录);
            }
            //儿童满月
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表满月] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表满月].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表满月);
            }
            //儿童3月
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表3月] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表3月].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表3月);
            }
            //儿童6月
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表6月] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表6月].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表6月);
            }
            //儿童9月6月
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表8月] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表8月].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表8月);
            }
            //儿童12月6月
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表12月] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表12月].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表12月);
            }
            //儿童18月
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表18月] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表18月].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表18月);
            }
            //儿童2岁
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表2岁] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表2岁].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表2岁);
            }
            //儿童2岁半
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表2岁半] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表2岁半].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表2岁半);
            }
            //儿童3岁
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表3岁] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表3岁].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表3岁);
            }
            //儿童4岁
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表4岁] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表4岁].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表4岁);
            }
            //儿童5岁
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表5岁] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表5岁].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表5岁);
            }
            //儿童6岁
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表6岁] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表6岁].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童健康检查记录表6岁);
            }
            //儿童入托
            if (table.Rows[0][tb_健康档案_个人健康特征.儿童入托信息表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.儿童入托信息表].ToString()) && !"未建".Equals(table.Rows[0][tb_健康档案_个人健康特征.儿童基本信息].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.儿童入托信息表);
            }
            //妇女保健检查
            if (table.Rows[0][tb_健康档案_个人健康特征.妇女保健检查] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.妇女保健检查].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.妇女保健检查);
            }
            //妇女更年期保健检查
            if (table.Rows[0][tb_健康档案_个人健康特征.妇女更年期保健检查] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.妇女更年期保健检查].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.妇女更年期保健检查);
            }
            // del 删除孕产妇基本信息，由个人基本信息表代替，把个人基本信息表的编辑权限放给妇幼人员，不再是使用孕产妇所在卫生室的账号编辑。 ▽
            //孕产妇信息
            //if (table.Rows[0][tb_健康档案_个人健康特征.孕产妇基本信息表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.孕产妇基本信息表].ToString()))
            //{
            //    Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.孕产妇基本信息表);
            //}
            // del 删除孕产妇基本信息，由个人基本信息表代替，把个人基本信息表的编辑权限放给妇幼人员，不再是使用孕产妇所在卫生室的账号编辑。 △
            //孕产妇产前检测记录第一次
            if (table.Rows[0][tb_健康档案_个人健康特征.第一次产前检查表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.第一次产前检查表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.第一次产前检查表);
            }
            //孕产妇产前检测记录第二次
            if (table.Rows[0][tb_健康档案_个人健康特征.第二次产前检查表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.第二次产前检查表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.第二次产前检查表);
            }
            //孕产妇产前记录第三次
            if (table.Rows[0][tb_健康档案_个人健康特征.第三次产前检查表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.第三次产前检查表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.第三次产前检查表);
            }
            //孕产妇产前记录第四次
            if (table.Rows[0][tb_健康档案_个人健康特征.第四次产前检查表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.第四次产前检查表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.第四次产前检查表);
            }
            //孕产妇产前检测记录第五次
            if (table.Rows[0][tb_健康档案_个人健康特征.第五次产前检查表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.第五次产前检查表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.第五次产前检查表);
            }
            //孕产妇产后访视
            if (table.Rows[0][tb_健康档案_个人健康特征.孕产妇产后访视] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.孕产妇产后访视].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.孕产妇产后访视);
            }
            //孕产妇产后42
            if (table.Rows[0][tb_健康档案_个人健康特征.孕产妇产后42天] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.孕产妇产后42天].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.孕产妇产后42天);
            }
            //老年人随访
            if (table.Rows[0][tb_健康档案_个人健康特征.老年人随访] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.老年人随访].ToString()))
            {
                //年龄符合条件的 显示
                if (age >= 65)
                {
                    Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.老年人随访);
                }
            }
            //高血压管理卡
            if (table.Rows[0][tb_健康档案_个人健康特征.是否高血压] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.是否高血压].ToString()) && table.Rows[0][tb_健康档案_个人健康特征.高血压管理卡] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.高血压管理卡].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.高血压管理卡);
                //高血压管理卡
                if (table.Rows[0][tb_健康档案_个人健康特征.高血压随访表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.高血压随访表].ToString()))
                {
                    Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.高血压随访表);
                }
            }
            //糖尿病管理卡
            if (table.Rows[0][tb_健康档案_个人健康特征.是否糖尿病] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.是否糖尿病].ToString()) && table.Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.糖尿病管理卡);
                //高血压管理卡
                if (table.Rows[0][tb_健康档案_个人健康特征.糖尿病随访表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.糖尿病随访表].ToString()))
                {
                    Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.糖尿病随访表);
                }
            }
            //脑卒中管理卡
            if (table.Rows[0][tb_健康档案_个人健康特征.是否脑卒中] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.是否脑卒中].ToString()) && table.Rows[0][tb_健康档案_个人健康特征.脑卒中管理卡] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.脑卒中管理卡].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.脑卒中管理卡);
                if (table.Rows[0][tb_健康档案_个人健康特征.脑卒中随访表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.脑卒中随访表].ToString()))
                {
                    Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.脑卒中随访表);
                }
            }
            //冠心病
            if (table.Rows[0][tb_健康档案_个人健康特征.是否冠心病] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.是否冠心病].ToString()) && table.Rows[0][tb_健康档案_个人健康特征.冠心病管理卡] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.冠心病管理卡].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.冠心病管理卡);
                if (table.Rows[0][tb_健康档案_个人健康特征.冠心病随访表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.冠心病随访表].ToString()))
                {
                    Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.冠心病随访表);
                }
            }
            //残疾人精神疾病信息补充表
            if (table.Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.精神疾病信息补充表);
            }
            //残疾人_精神
            if (table.Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.精神疾病随访表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.精神疾病随访表);
            }
            //残疾人_听力
            if (table.Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.听力言语残疾随访表);
            }
            //残疾人_肢体
            if (table.Rows[0][tb_健康档案_个人健康特征.肢体残疾随访表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.肢体残疾随访表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.肢体残疾随访表);
            }
            //残疾人_智力
            if (table.Rows[0][tb_健康档案_个人健康特征.智力残疾随访表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.智力残疾随访表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.智力残疾随访表);
            }
            //残疾人_视力
            if (table.Rows[0][tb_健康档案_个人健康特征.视力残疾随访表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.视力残疾随访表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.视力残疾随访表);
            }
            //残疾人康复服务
            if (table.Rows[0][tb_健康档案_个人健康特征.残疾人康复服务随访记录表] != null && !string.IsNullOrEmpty(table.Rows[0][tb_健康档案_个人健康特征.残疾人康复服务随访记录表].ToString()))
            {
                Bind干预提醒Row(newTable, tb_健康档案_个人健康特征.残疾人康复服务随访记录表);
            }
            this.gc干预提醒.DataSource = newTable;
        }

        private void Bind干预提醒Row(DataTable newTable, DataColumn dataColumn)
        {
            throw new NotImplementedException();
        }

        private void Bind干预提醒Row(DataTable newTable, string fieldName)
        {
            DataRow row;
            string[] array = _个人健康特征.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][fieldName].ToString().Split(',');
            row = newTable.NewRow();
            row["业务表单"] = fieldName;
            if (array.Length == 2)
            {
                row["缺项"] = array[0];
                row["完整度"] = array[1] + "%";
            }
            else
            {
                row["缺项"] = "未建";
                row["完整度"] = "未建";
            }
            newTable.Rows.Add(row);
        }

        private void pictureEdit1_MouseMove(object sender, MouseEventArgs e)
        {
            this.panelControl1.SendToBack();
            this.panelControl3.Visible = true;
            this.panelControl3.BringToFront();

        }
        /// <summary>
        /// 业务表单 点击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            string view = this.gv干预提醒.GetFocusedValue().ToString();
            if (string.IsNullOrEmpty(view)) return;
            JumpToShowView(view);//跳转到显示页面
        }

        private void JumpToShowView(string view)
        {
            Control ctrl = null;
            switch (view)
            {
                case "个人基本信息表":
                    ctrl = new UC个人基本信息表_显示(this);
                    break;
                case "家庭档案":
                    if (string.IsNullOrEmpty(_familyDocNo))
                    {
                        Msg.ShowInformation("此人不在任何家庭中，不能进行此操作！");
                        return;
                    }
                    ctrl = new UC家庭基本信息(this);
                    break;
                case "健康体检":
                    ShowControl(this.barItem健康体检表.Name);
                    break;
                case "接诊记录":
                    ShowControl(this.barItem接诊记录表.Name);
                    break;
                case "会诊记录":
                    ShowControl(this.barItem会诊记录表.Name);
                    break;
                case "儿童基本信息":
                    navBarItem6_LinkClicked(null, null);
                    break;
                case "新生儿随访记录":
                    navBar新生儿访视_LinkClicked(null, null);
                    break;
                case "儿童健康检查记录表满月":
                    ShowControl(this.barItem儿童健康检查记录表_满月.Name);
                    break;
                case "儿童健康检查记录表3月":
                    ShowControl(this.barItem儿童健康检查记录表_3月.Name);
                    break;
                case "儿童健康检查记录表6月":
                    ShowControl(this.barItem儿童健康检查记录表_6月.Name);
                    break;
                case "儿童健康检查记录表8月":
                    ShowControl(this.barItem儿童健康检查记录表_8月.Name);
                    break;
                case "儿童健康检查记录表12月":
                    ShowControl(this.barItem儿童健康检查记录表_12月.Name);
                    break;
                case "儿童健康检查记录表18月":
                    ShowControl(this.barItem儿童健康检查记录表_18月.Name);
                    break;
                case "儿童健康检查记录表2岁":
                    ShowControl(this.barItem儿童健康检查记录表_24月.Name);
                    break;
                case "儿童健康检查记录表2岁半":
                    ShowControl(this.barItem儿童健康检查记录表_30月.Name);
                    break;
                case "儿童健康检查记录表3岁":
                    ShowControl(this.barItem儿童健康检查记录表_3岁.Name);
                    break;
                case "儿童健康检查记录表4岁":
                    ShowControl(this.barItem儿童健康检查记录表_4岁.Name);
                    break;
                case "儿童健康检查记录表5岁":
                    ShowControl(this.barItem儿童健康检查记录表_5岁.Name);
                    break;
                case "儿童健康检查记录表6岁":
                    ShowControl(this.barItem儿童健康检查记录表_6岁.Name);
                    break;
                case "儿童入托信息表":
                    ShowControl(this.barItem儿童入托信息表.Name);
                    break;

                case "妇女保健检查":
                    ShowControl(this.barItem妇女保健检查表.Name);
                    break;
                case "妇女更年期保健检查":
                    ShowControl(this.barItem更年期保健检查表.Name);
                    break;
                case "孕产妇基本信息表":
                    ShowControl(this.barItem孕妇基本信息.Name);
                    break;
                case "第一次产前检查表":
                    ShowControl(this.barItem产前一次随访.Name);
                    break;
                case "第二次产前检查表":
                    ShowControl(this.barItem产前二次随访.Name);
                    break;
                case "第三次产前检查表":
                    ShowControl(this.barItem产前三次随访.Name);
                    break;
                case "第四次产前检查表":
                    ShowControl(this.barItem产前四次随访.Name);
                    break;
                case "第五次产前检查表":
                    ShowControl(this.barItem产前五次随访.Name);
                    break;
                case "孕产妇产后访视":
                    ShowControl(this.barItem产后访视记录表.Name);
                    break;
                case "孕产妇产后42天":
                    ShowControl(this.barItem产后42天健康检查记录表.Name);
                    break;


                case "老年人随访":
                    ShowControl(this.barItem老年人生活自理能力评估表.Name);
                    break;
                case "高血压管理卡":
                    ShowControl(this.barItem高血压患者管理.Name);
                    break;
                case "高血压随访表":
                    ShowControl(this.barItem高血压患者随访服务记录表.Name);
                    break;
                case "糖尿病管理卡":
                    ShowControl(this.barItem糖尿病患者管理卡.Name);
                    break;
                case "糖尿病随访表":
                    ShowControl(this.barItem糖尿病患者随访服务记录表.Name);
                    break;
                case "脑卒中管理卡":
                    ShowControl(this.barItem脑卒中患者管理卡.Name);
                    break;
                case "脑卒中随访表":
                    ShowControl(this.BarItem脑卒中患者随访服务记录表.Name);
                    break;
                case "冠心病管理卡":
                    ShowControl(this.barItem冠心病患者管理卡.Name);
                    break;
                case "冠心病随访表":
                    ShowControl(this.BarItem冠心病患者随访服务记录表.Name);
                    break;
                case "精神疾病信息补充表":
                    ShowControl(this.barItem精神疾病患者补充表.Name);
                    break;
                case "精神疾病随访表":
                    ShowControl(this.barItem精神疾病患者随访表.Name);
                    break;
                case "听力言语残疾随访表":
                    ShowControl(this.barItem听力语言残疾随访表.Name);
                    break;
                case "肢体残疾随访表":
                    ShowControl(this.barItem肢体残疾随访表.Name);
                    break;
                case "智力残疾随访表":
                    ShowControl(this.barItem智力残疾随访表.Name);
                    break;
                case "视力残疾随访表":
                    ShowControl(this.barItem视力残疾随访表.Name);
                    break;
                case "残疾人康复服务随访记录表":
                    ShowControl(this.barItem残疾人康复服务随访记录表.Name);
                    break;

                default:
                    break;
            }
            if (ctrl != null)
            {
                ShowControl(ctrl);
            }
        }

        private void repositoryItemHyperLinkEdit2_Click(object sender, EventArgs e)
        {

        }

        private void repositoryItemHyperLinkEdit3_Click(object sender, EventArgs e)
        {

        }

        private void gc干预提醒_MouseLeave(object sender, EventArgs e)
        {
            //this.panelControl3.SendToBack();
            //this.panelControl1.BringToFront();
        }

        private void gc干预提醒_Leave(object sender, EventArgs e)
        {
            this.panelControl3.SendToBack();
            this.panelControl1.BringToFront();
        }

        private void gv干预提醒_MouseLeave(object sender, EventArgs e)
        {
            //this.panelControl3.SendToBack();
            //this.panelControl1.BringToFront();
        }

        //add by wjz 20151109 作用：通过“添加”、“删除”进入新画面后，允许滚动页面 ▽
        public void SetLayoutControlMouseWheel(DevExpress.XtraLayout.LayoutControl lc)
        {
            _helper = new util.LayoutControlScrollHelper(lc);
            _helper.EnableScrollOnMouseWheel();
        }

        public void SetLayoutControlMouseWheel(Control uc)
        {
            for (int index = 0; index < uc.Controls.Count; index++)
            {
                Type type = uc.Controls[index].GetType();
                if (type.Name == "LayoutControl")
                {
                    _helper = new util.LayoutControlScrollHelper(uc.Controls[index] as DevExpress.XtraLayout.LayoutControl);
                    _helper.EnableScrollOnMouseWheelAllTime();
                    //_helper.EnableScrollOnMouseWheel();
                    break;
                }
            }
        }
        //add by wjz 20151109 作用：通过“添加”、“删除”进入新画面后，允许滚动页面 △
    }
}
