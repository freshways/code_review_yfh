﻿using AtomEHR.公共卫生.Module.个人健康.儿童健康信息;
using AtomEHR.公共卫生.Module.个人健康.老年人健康管理;
using AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息;
using AtomEHR.公共卫生.Module.个人健康.体检就诊信息;
using AtomEHR.Models;
using AtomEHR.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.公共卫生.Module.个人健康.家庭健康信息;
using AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息;
using AtomEHR.Business;
using AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理;
using AtomEHR.公共卫生.Module.个人健康.艾滋病;

namespace AtomEHR.公共卫生.Module.个人健康
{
    public static class Common
    {
        public static DataTable dt个人档案;
        public static DataTable dt个人健康特征;
        public static Form frm个人健康;
        public static string dno;//个人档案编号

        /// <summary>
        /// 点击个人健康页面左侧的页面导航栏时，
        /// 根据不同的BarItem来显示不同的控件
        /// </summary>
        /// <param name="ctrlName">控件名称</param>
        /// <param name="userInfo">当前被操作人的信息，用于进行一些相关的判断</param>
        /// <returns></returns>
        public static Control ShowControl(string ctrlName, System.Windows.Forms.Form row)
        {
            if (string.IsNullOrWhiteSpace(ctrlName)) return null;
            Control ctrl = null;
            Control existCtrl = null;
            Control _ctrlPanel = row.Controls["panelControl2"];
            if (_ctrlPanel != null)
            {
                existCtrl = _ctrlPanel.Controls.Count == 1 ? _ctrlPanel.Controls[0] : null;
            }
            DataRow[] currentRow = dt个人档案.Select("个人档案编号='" + dno + "'");
            switch (ctrlName)
            {

                #region 家庭健康信息
                case "barItem家庭健康":
                    if (existCtrl is UC家庭基本信息) ctrl = null;
                    else ctrl = new UC家庭基本信息(row);
                    break;
                #endregion

                #region 儿童相关页面
                //case "barItem儿童基本信息":
                //    ctrl = new UC儿童基本信息_显示(null, null);
                //    break;
                //case "barItem新生儿家庭访视记录表":
                //    ctrl = new UC新生儿家庭访视记录表_显示(dno);
                //    break;
                case "barItem儿童健康检查记录表_满月":
                    //ctrl = new UC儿童健康检查_满月_显示();
                    AtomEHR.Business.bll儿童_健康检查_满月 bll满月 = new bll儿童_健康检查_满月();
                    DataSet ds满月 = bll满月.GetInfo(dno);

                    //
                    if (ds满月.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（满月）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds满月.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(满月)记录的情况下
                            //判断年龄是否小于28天，若是，提示不能创建记录
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll满月.ServiceDateTime;

                            try
                            {
                                TimeSpan ts = Convert.ToDateTime(strCurrentServerDate) - Convert.ToDateTime(strBirth);

                                if (ts.TotalDays < 28)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_满月) ctrl = null;
                                    else ctrl = new UC儿童健康检查_满月(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common81:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_满月_显示(dno);
                        }
                    }
                    //ctrl = new UC儿童健康检查_满月(null);
                    break;
                case "barItem儿童健康检查记录表_3月":
                    //ctrl = new UC儿童健康检查_3月_显示();
                    AtomEHR.Business.bll儿童_健康检查_3月 bll3月 = new bll儿童_健康检查_3月();
                    DataSet ds3月 = bll3月.GetInfo(dno);

                    //
                    if (ds3月.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（3月）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds3月.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(3月)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll3月.ServiceDateTime;

                            try
                            {
                                DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                DateTime DtBirth = Convert.ToDateTime(strBirth);
                                int month = (DtNow.Year - DtBirth.Year) * 12 + DtNow.Month - DtBirth.Month;

                                if (month < 3)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_3月) ctrl = null;
                                    else ctrl = new UC儿童健康检查_3月(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common3月:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_3月_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童健康检查记录表_6月":
                    //ctrl = new UC儿童健康检查_6月_显示();
                    AtomEHR.Business.bll儿童_健康检查_6月 bll6月 = new bll儿童_健康检查_6月();
                    DataSet ds6月 = bll6月.GetInfo(dno);

                    //
                    if (ds6月.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（6月）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds6月.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(6月)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll6月.ServiceDateTime;

                            try
                            {
                                DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                DateTime DtBirth = Convert.ToDateTime(strBirth);
                                int month = (DtNow.Year - DtBirth.Year) * 12 + DtNow.Month - DtBirth.Month;

                                if (month < 6)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_6月) ctrl = null;
                                    else ctrl = new UC儿童健康检查_6月(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common6月:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_6月_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童健康检查记录表_8月":
                    //ctrl = new UC儿童健康检查_8月_显示();
                    AtomEHR.Business.bll儿童_健康检查_8月 bll8月 = new bll儿童_健康检查_8月();
                    DataSet ds8月 = bll8月.GetInfo(dno);

                    //
                    if (ds8月.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（8月）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds8月.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(8月)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll8月.ServiceDateTime;

                            try
                            {
                                DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                DateTime DtBirth = Convert.ToDateTime(strBirth);
                                int month = (DtNow.Year - DtBirth.Year) * 12 + DtNow.Month - DtBirth.Month;

                                if (month < 8)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_8月) ctrl = null;
                                    else ctrl = new UC儿童健康检查_8月(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common8月:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_8月_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童健康检查记录表_12月":
                    //ctrl = new UC儿童健康检查_12月_显示();
                    AtomEHR.Business.bll儿童_健康检查_12月 bll12月 = new bll儿童_健康检查_12月();
                    DataSet ds12月 = bll12月.GetInfo(dno);

                    //
                    if (ds12月.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（12月）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds12月.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(12月)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll12月.ServiceDateTime;

                            try
                            {
                                DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                DateTime DtBirth = Convert.ToDateTime(strBirth);
                                int month = (DtNow.Year - DtBirth.Year) * 12 + DtNow.Month - DtBirth.Month;

                                if (month < 12)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_12月) ctrl = null;
                                    else ctrl = new UC儿童健康检查_12月(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common12月:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_12月_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童健康检查记录表_18月":
                    //ctrl = new UC儿童健康检查_18月_显示();
                    AtomEHR.Business.bll儿童_健康检查_18月 bll18月 = new bll儿童_健康检查_18月();
                    DataSet ds18月 = bll18月.GetInfo(dno);

                    //
                    if (ds18月.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（18月）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds18月.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(18月)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll18月.ServiceDateTime;

                            try
                            {
                                DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                DateTime DtBirth = Convert.ToDateTime(strBirth);
                                int month = (DtNow.Year - DtBirth.Year) * 12 + DtNow.Month - DtBirth.Month;

                                if (month < 18)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_18月) ctrl = null;
                                    else ctrl = new UC儿童健康检查_18月(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common18月:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_18月_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童健康检查记录表_24月":
                    //ctrl = new UC儿童健康检查_24月_显示();
                    AtomEHR.Business.bll儿童_健康检查_24月 bll24月 = new bll儿童_健康检查_24月();
                    DataSet ds24月 = bll24月.GetInfo(dno);

                    //
                    if (ds24月.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（24月）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds24月.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(24月)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll24月.ServiceDateTime;

                            try
                            {
                                DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                DateTime DtBirth = Convert.ToDateTime(strBirth);
                                int month = (DtNow.Year - DtBirth.Year) * 12 + DtNow.Month - DtBirth.Month;

                                if (month < 24)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_24月) ctrl = null;
                                    else ctrl = new UC儿童健康检查_24月(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common24月:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_24月_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童健康检查记录表_30月":
                    //ctrl = new UC儿童健康检查_30月_显示();
                    AtomEHR.Business.bll儿童_健康检查_30月 bll30月 = new bll儿童_健康检查_30月();
                    DataSet ds30月 = bll30月.GetInfo(dno);

                    //
                    if (ds30月.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（30月）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds30月.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(30月)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll30月.ServiceDateTime;

                            try
                            {
                                DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                DateTime DtBirth = Convert.ToDateTime(strBirth);
                                int month = (DtNow.Year - DtBirth.Year) * 12 + DtNow.Month - DtBirth.Month;

                                if (month < 30)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_30月) ctrl = null;
                                    else ctrl = new UC儿童健康检查_30月(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common30月:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_30月_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童健康检查记录表_3岁":
                    //ctrl = new UC儿童健康检查_3岁_显示();
                    AtomEHR.Business.bll儿童_健康检查_3岁 bll3岁 = new bll儿童_健康检查_3岁();
                    DataSet ds3岁 = bll3岁.GetInfo(dno);

                    //
                    if (ds3岁.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（3岁）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds3岁.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(3岁)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll3岁.ServiceDateTime;

                            try
                            {
                                //DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                //DateTime DtBirth = Convert.ToDateTime(strBirth);
                                //int month =  (DtNow.Year-DtBirth.Year)* 12+ DtNow.Month-DtBirth.Month;
                                int age = new bllCom().GetAgeByBirthDay(strBirth, strCurrentServerDate);

                                //if (month < 36)
                                if (age < 3)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_3岁) ctrl = null;
                                    else ctrl = new UC儿童健康检查_3岁(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common3岁:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_3岁_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童健康检查记录表_4岁":
                    //ctrl = new UC儿童健康检查_4岁_显示();
                    AtomEHR.Business.bll儿童_健康检查_4岁 bll4岁 = new bll儿童_健康检查_4岁();
                    DataSet ds4岁 = bll4岁.GetInfo(dno);

                    //
                    if (ds4岁.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（4岁）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds4岁.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(4岁)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll4岁.ServiceDateTime;

                            try
                            {
                                //DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                //DateTime DtBirth = Convert.ToDateTime(strBirth);
                                //int month =  (DtNow.Year-DtBirth.Year)* 12+ DtNow.Month-DtBirth.Month;
                                int age = new bllCom().GetAgeByBirthDay(strBirth, strCurrentServerDate);

                                //if (month < 36)
                                if (age < 4)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_4岁) ctrl = null;
                                    else ctrl = new UC儿童健康检查_4岁(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common4岁:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_4岁_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童健康检查记录表_5岁":
                    //ctrl = new UC儿童健康检查_5岁_显示();
                    AtomEHR.Business.bll儿童_健康检查_5岁 bll5岁 = new bll儿童_健康检查_5岁();
                    DataSet ds5岁 = bll5岁.GetInfo(dno);

                    //
                    if (ds5岁.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（5岁）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds5岁.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(5岁)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll5岁.ServiceDateTime;

                            try
                            {
                                //DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                //DateTime DtBirth = Convert.ToDateTime(strBirth);
                                //int month =  (DtNow.Year-DtBirth.Year)* 12+ DtNow.Month-DtBirth.Month;
                                int age = new bllCom().GetAgeByBirthDay(strBirth, strCurrentServerDate);

                                //if (month < 36)
                                if (age < 4)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_5岁) ctrl = null;
                                    else ctrl = new UC儿童健康检查_5岁(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common5岁:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_5岁_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童健康检查记录表_6岁":
                    //ctrl = new UC儿童健康检查_6岁_显示();
                    AtomEHR.Business.bll儿童_健康检查_6岁 bll6岁 = new bll儿童_健康检查_6岁();
                    DataSet ds6岁 = bll6岁.GetInfo(dno);

                    //
                    if (ds6岁.Tables[0].Rows[0][0].ToString() == "0")
                    {
                        //没有儿童基本信息的情况下
                        Msg.ShowInformation("未找到此人的儿童基本信息，如果需要为其添加健康检查（6岁）,请先添加儿童基本信息。");
                        ctrl = null;
                    }
                    else
                    {
                        if (ds6岁.Tables[0].Rows[1][0].ToString() == "0")
                        {
                            //有儿童基本信息，没有健康检查(6岁)记录的情况下
                            DataRow[] dr个人 = dt个人档案.Select("个人档案编号='" + dno + "'");
                            string strBirth = dr个人[0]["出生日期"].ToString();
                            string strCurrentServerDate = bll6岁.ServiceDateTime;

                            try
                            {
                                //DateTime DtNow = Convert.ToDateTime(strCurrentServerDate);
                                //DateTime DtBirth = Convert.ToDateTime(strBirth);
                                //int month =  (DtNow.Year-DtBirth.Year)* 12+ DtNow.Month-DtBirth.Month;
                                int age = new bllCom().GetAgeByBirthDay(strBirth, strCurrentServerDate);

                                //if (month < 36)
                                if (age < 4)
                                {
                                    Msg.ShowInformation("此儿童未达到本次随访的年龄，不能进行此次随访！");
                                    ctrl = null;
                                }
                                else
                                {
                                    if (existCtrl is UC儿童健康检查_6岁) ctrl = null;
                                    else ctrl = new UC儿童健康检查_6岁(dno, UpdateType.Add, null);
                                }
                            }
                            catch (Exception ex)
                            {
                                Msg.ShowError("Common6岁:" + ex.Message);
                            }
                        }
                        else
                        {
                            ctrl = new UC儿童健康检查_6岁_显示(dno);
                        }
                    }
                    break;
                case "barItem儿童入托信息表":                   //暂时还未开发页面   todo
                    ctrl = null;
                    break;
                case "barItem生长发育监测图":
                    DataRow[] dr个人信息 = dt个人档案.Select("个人档案编号='" + dno + "'");
                    string strName = dr个人信息[0]["姓名"].ToString();
                    string strSex = dr个人信息[0]["性别"].ToString();
                    Frm儿童生长发育图 frm = new Frm儿童生长发育图(strName, dno, strSex);
                    frm.Show();
                    ctrl = null;
                    break;
                #endregion

                #region 妇女 && 孕妇

                case "barItem妇女保健检查表":
                    bll妇女保健检查 _bll妇女 = new bll妇女保健检查();
                    if (_bll妇女.CheckNoExists(dno))//存在数据
                    {
                        ctrl = new 个人健康.妇女健康管理.UC妇女保健检查表_显示(row);
                    }
                    else
                    {
                        ctrl = new 个人健康.妇女健康管理.UC妇女保健检查表(row, UpdateType.Add);
                    }
                    break;

                case "barItem孕妇基本信息":
                    AtomEHR.Business.bll孕妇基本信息 bll孕妇基本信息 = new Business.bll孕妇基本信息();
                    DataTable dt孕妇基本信息 = bll孕妇基本信息.GetBusinessByKey(dno, true).Tables[tb_孕妇基本信息.__TableName];
                    if (dt孕妇基本信息 != null && dt孕妇基本信息.Rows.Count > 0)
                    {
                        ctrl = new UC孕产妇基本信息(currentRow);
                    }
                    else
                    {
                        if (Msg.AskQuestion("此居民不是孕产妇，若您要对其进行孕产妇管理请先修改个人基本系信息表中的[孕产情况]为管理期状态！"))
                        {
                            ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }

                    break;
                case "barItem产前一次随访":
                    if (currentRow[0][tb_健康档案.怀孕情况] == null || currentRow[0][tb_健康档案.怀孕情况].ToString() == "未孕")
                    {
                        if (Msg.AskQuestion("此居民不是孕产妇，若您要对其进行孕产妇管理请先修改个人基本系信息表中的[孕产情况]为管理期状态！"))
                        {
                            ctrl = new UC个人基本信息表(row, UpdateType.Modify);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        bll孕妇产前一次随访记录 _bll孕妇产前一次检查 = new bll孕妇产前一次随访记录();
                        if (_bll孕妇产前一次检查.CheckNoExists(dno))//存在数据
                        {
                            ctrl = new 个人健康.妇女健康管理.UC第1次产前随访服务记录表_显示(row);
                        }
                        else
                        {
                            ctrl = new 个人健康.妇女健康管理.UC第1次产前随访服务记录表(row, UpdateType.Add);
                        }
                    }

                    break;
                case "barItem产前二次随访":
                    if (currentRow[0][tb_健康档案.怀孕情况] == null || currentRow[0][tb_健康档案.怀孕情况].ToString() == "未孕")
                    {
                        if (Msg.AskQuestion("此居民不是孕产妇，若您要对其进行孕产妇管理请先修改个人基本系信息表中的[孕产情况]为管理期状态！"))
                        {
                            ctrl = new UC个人基本信息表(row, UpdateType.Modify);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        bll孕妇_产前随访2次 _bll孕妇2次 = new bll孕妇_产前随访2次();
                        if (_bll孕妇2次.Check记录(dno))
                        {
                            ctrl = new 妇女健康管理.UC第2次产前随访服务记录表_显示(dno, null);
                        }
                        else
                        {
                            ctrl = new 妇女健康管理.UC第2次产前随访服务记录表(dno, null, UpdateType.Add, null);
                        }
                    }
                    break;
                case "barItem产前三次随访":
                    if (currentRow[0][tb_健康档案.怀孕情况] == null || currentRow[0][tb_健康档案.怀孕情况].ToString() == "未孕")
                    {
                        if (Msg.AskQuestion("此居民不是孕产妇，若您要对其进行孕产妇管理请先修改个人基本系信息表中的[孕产情况]为管理期状态！"))
                        {
                            ctrl = new UC个人基本信息表(row, UpdateType.Modify);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        bll孕妇_产前随访3次 _bll孕妇3次 = new bll孕妇_产前随访3次();
                        if (_bll孕妇3次.Check记录(dno))
                        {
                            ctrl = new 妇女健康管理.UC第3次产前随访服务记录表_显示(dno, null);
                        }
                        else
                        {
                            ctrl = new 妇女健康管理.UC第3次产前随访服务记录表(dno, null, UpdateType.Add, null);
                        }
                    }
                    break;
                case "barItem产前四次随访":
                    if (currentRow[0][tb_健康档案.怀孕情况] == null || currentRow[0][tb_健康档案.怀孕情况].ToString() == "未孕")
                    {
                        if (Msg.AskQuestion("此居民不是孕产妇，若您要对其进行孕产妇管理请先修改个人基本系信息表中的[孕产情况]为管理期状态！"))
                        {
                            ctrl = new UC个人基本信息表(row, UpdateType.Modify);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        bll孕妇_产前随访4次 _bll孕妇4次 = new bll孕妇_产前随访4次();
                        if (_bll孕妇4次.Check记录(dno))
                        {
                            ctrl = new 妇女健康管理.UC第4次产前随访服务记录表_显示(dno, null);
                        }
                        else
                        {
                            ctrl = new 妇女健康管理.UC第4次产前随访服务记录表(dno, null, UpdateType.Add, null);
                        }
                    }
                    break;
                case "barItem产前五次随访":
                    if (currentRow[0][tb_健康档案.怀孕情况] == null || currentRow[0][tb_健康档案.怀孕情况].ToString() == "未孕")
                    {
                        if (Msg.AskQuestion("此居民不是孕产妇，若您要对其进行孕产妇管理请先修改个人基本系信息表中的[孕产情况]为管理期状态！"))
                        {
                            ctrl = new UC个人基本信息表(row, UpdateType.Modify);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        bll孕妇_产前随访5次 _bll孕妇5次 = new bll孕妇_产前随访5次();
                        if (_bll孕妇5次.Check记录(dno))
                        {
                            ctrl = new 妇女健康管理.UC第5次产前随访服务记录表_显示(dno, null);
                        }
                        else
                        {
                            ctrl = new 妇女健康管理.UC第5次产前随访服务记录表(dno, null, UpdateType.Add, null);
                        }
                    }
                    break;
                case "barItem产后访视记录表":
                    #region 产后访视记录表

                    if (currentRow[0][tb_健康档案.怀孕情况] == null || currentRow[0][tb_健康档案.怀孕情况].ToString() == "未孕")
                    {
                        if (Msg.AskQuestion("此居民不是孕产妇，若您要对其进行孕产妇管理请先修改个人基本系信息表中的[孕产情况]为管理期状态！"))
                        {
                            ctrl = new UC个人基本信息表(row, UpdateType.Modify);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else if (currentRow[0][tb_健康档案.产次] == null || currentRow[0][tb_健康档案.产次].ToString() == "" || currentRow[0][tb_健康档案.产次].ToString() == "0")
                    {
                        if (Msg.AskQuestion("此居民的【产次】为空或为0,请先修改个人基本信息中的【产次】再进行此操作！"))
                        {
                            ctrl = new UC个人基本信息表(row, UpdateType.Modify);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        bll孕妇_产后访视情况 _bll产后访视情况 = new bll孕妇_产后访视情况();
                        if (_bll产后访视情况.CheckNoExists(dno))//存在数据
                        {
                            ctrl = new 个人健康.妇女健康管理.UC产后访视记录表_显示(row);
                        }
                        else
                        {
                            ctrl = new 个人健康.妇女健康管理.UC产后访视记录表(row, UpdateType.Add);
                        }
                    }

                    #endregion
                    break;
                case "barItem产后42天健康检查记录表":
                    #region 产后42天健康检查记录表
                    if (currentRow[0][tb_健康档案.怀孕情况] == null || currentRow[0][tb_健康档案.怀孕情况].ToString() == "未孕")
                    {
                        if (Msg.AskQuestion("此居民不是孕产妇，若您要对其进行孕产妇管理请先修改个人基本系信息表中的[孕产情况]为管理期状态！"))
                        {
                            ctrl = new UC个人基本信息表(row, UpdateType.Modify);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else if (currentRow[0][tb_健康档案.产次] == null || currentRow[0][tb_健康档案.产次].ToString() == "" || currentRow[0][tb_健康档案.产次].ToString() == "0")
                    {
                        if (Msg.AskQuestion("此居民的【产次】为空或为0,请先修改个人基本信息中的【产次】再进行此操作！"))
                        {
                            ctrl = new UC个人基本信息表(row, UpdateType.Modify);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        bll孕妇_产后42天检查记录 _bll产后42天检查记录 = new bll孕妇_产后42天检查记录();
                        if (_bll产后42天检查记录.CheckNoExists(dno))//存在数据
                        {
                            ctrl = new 个人健康.妇女健康管理.UC产后42天健康检查记录表_显示(row);
                        }
                        else
                        {
                            ctrl = new 个人健康.妇女健康管理.UC产后42天健康检查记录表(row, UpdateType.Add);
                        }
                    }

                    #endregion
                    break;

                #endregion

                #region 老年人
                case "barItem老年人生活自理能力评估表":
                    AtomEHR.Business.bll老年人随访 _bll老年人随访 = new AtomEHR.Business.bll老年人随访();
                    if (_bll老年人随访.CheckNoExists(dno))
                    //if (_bll老年人随访.GetBusinessByKey(dno, false).Tables[0].Rows.Count == 0)
                    {
                        if (existCtrl is UC老年人生活自理能力评估表_显示) ctrl = null;
                        else ctrl = new UC老年人生活自理能力评估表_显示(row);
                    }
                    else
                    {
                        if (existCtrl is UC老年人生活自理能力评估表) ctrl = null;
                        else ctrl = new UC老年人生活自理能力评估表(row, UpdateType.Add);
                    }
                    break;
                case "barItem老年人中医药健康管理":
                    AtomEHR.Business.bll老年人中医药特征管理 _bll老年人中医药管理 = new AtomEHR.Business.bll老年人中医药特征管理();
                    DataSet ds = _bll老年人中医药管理.GetAllDataByKey(dno);
                    if (ds.Tables[tb_老年人中医药特征管理.__TableName].Rows.Count == 0)
                    {
                        if (existCtrl is UC老年人中医药健康管理服务记录表) ctrl = null;
                        else ctrl = new UC老年人中医药健康管理服务记录表(row, UpdateType.Add);
                    }
                    else if (ds.Tables[tb_老年人中医药特征管理.__TableName].Rows.Count == 1)
                    {
                        if (existCtrl is UC老年人中医药健康管理服务记录表_显示) ctrl = null;
                        else ctrl = new UC老年人中医药健康管理服务记录表_显示(row);
                    }
                    else
                    {
                        if (existCtrl is UC老年人中医药健康管理服务记录表列表) ctrl = null;
                        else ctrl = new UC老年人中医药健康管理服务记录表列表(row);
                    }
                    break;
                #endregion

                #region 体检就诊信息
                case "barItem接诊记录表":
                    if (dt个人档案 != null)
                        if (existCtrl is UC接诊记录表_显示) ctrl = null;
                        else ctrl = new UC接诊记录表_显示(dt个人档案.Select("个人档案编号='" + dno + "'"), null);
                    break;
                case "barItem健康体检表":
                    AtomEHR.Business.bll健康体检 BLL = new AtomEHR.Business.bll健康体检();
                    if (BLL.GetOneDataByKey(dno, false).Tables[0].Rows.Count == 0)
                    {
                        if (existCtrl is UC健康体检表) ctrl = null;
                        else ctrl = new UC健康体检表(row, AtomEHR.Common.UpdateType.Add);
                    }
                    else
                    {
                        if (existCtrl is UC健康体检表_显示) ctrl = null;
                        else ctrl = new UC健康体检表_显示(row);
                    }
                    break;
                case "barItem会诊记录表":
                    if (existCtrl is UC会诊记录表_显示) ctrl = null;
                    else ctrl = new UC会诊记录表_显示();
                    break;
                case "barItem食盐摄入情况":
                    if (existCtrl is UC食盐摄入情况表_显示) ctrl = null;
                    else ctrl = new UC食盐摄入情况表_显示(dt个人档案.Select("个人档案编号='" + dno + "'"), null);
                    break;
                #endregion

                #region 慢性疾病
                #region 高血压
                case "barItem高血压患者管理":
                    AtomEHR.Business.bllMXB高血压管理卡 bll高血压管理卡 = new Business.bllMXB高血压管理卡();
                    DataTable dt高血压管理卡 = bll高血压管理卡.GetBusinessByKey(dno, true).Tables[tb_MXB高血压管理卡.__TableName];
                    if (dt高血压管理卡 != null && dt高血压管理卡.Rows.Count > 0)
                    {
                        if (existCtrl is UC高血压患者管理卡_显示) ctrl = null;
                        else ctrl = new UC高血压患者管理卡_显示(dt个人档案.Select("个人档案编号='" + dno + "'"));
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “高血压” 患者，若您要对其进行高血压管理，请先修改个人基本信息表中的[既往史 疾病]为 “高血压” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表) ctrl = null;
                            else ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }
                    break;
                case "barItem高血压患者随访服务记录表":
                    AtomEHR.Business.bllMXB高血压管理卡 bll高血压随访 = new Business.bllMXB高血压管理卡();
                    DataTable dt高血压随访 = bll高血压随访.GetBusinessByKey(dno, true).Tables[tb_MXB高血压管理卡.__TableName];
                    if (dt高血压随访 != null && dt高血压随访.Rows.Count > 0)
                    {
                        if (existCtrl is UC高血压患者随访记录表_显示) ctrl = null;
                        else ctrl = new UC高血压患者随访记录表_显示(dt个人档案.Select("个人档案编号='" + dno + "'"), null);
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “高血压” 患者，若您要对其进行高血压管理，请先修改个人基本信息表中的[既往史 疾病]为 “高血压” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表) ctrl = null;
                            else ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }
                    break;
                case "barItem高血压患者随访服务记录表2":
                    AtomEHR.Business.bllMXB高血压管理卡 bll高血压随访2 = new Business.bllMXB高血压管理卡();
                    DataTable dt高血压随访2 = bll高血压随访2.GetBusinessByKey(dno, true).Tables[tb_MXB高血压管理卡.__TableName];
                    if (dt高血压随访2 != null && dt高血压随访2.Rows.Count > 0)
                    {
                        if (existCtrl is UC高血压患者随访记录表二_显示) ctrl = null;
                        else ctrl = new UC高血压患者随访记录表二_显示(dt个人档案.Select("个人档案编号='" + dno + "'"), null);
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “高血压” 患者，若您要对其进行高血压管理，请先修改个人基本信息表中的[既往史 疾病]为 “高血压” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表) ctrl = null;
                            else ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }
                    break;
                #endregion
                #region 糖尿病
                case "barItem糖尿病患者管理卡":
                    AtomEHR.Business.bllMXB糖尿病管理卡 tnbbll = new Business.bllMXB糖尿病管理卡();
                    DataTable tnbdt = tnbbll.GetBusinessByKey(dno, true).Tables[tb_MXB糖尿病管理卡.__TableName];
                    if (tnbdt != null && tnbdt.Rows.Count > 0)
                    {
                        if (existCtrl is UC糖尿病患者管理卡_显示) ctrl = null;
                        else ctrl = new UC糖尿病患者管理卡_显示(dt个人档案.Select("个人档案编号='" + dno + "'"));
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “糖尿病” 患者，若您要对其进行糖尿病管理，请先修改个人基本信息表中的[既往史 疾病]为 “糖尿病” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表) ctrl = null;
                            else ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }
                    //ctrl = new UC糖尿病患者管理卡_显示(dt个人档案.Select("个人档案编号='" + dno + "'"));
                    break;
                case "barItem糖尿病患者随访服务记录表":
                    AtomEHR.Business.bllMXB糖尿病管理卡 tnbsfbll = new Business.bllMXB糖尿病管理卡();
                    DataTable tnbsfdt = tnbsfbll.GetBusinessByKey(dno, true).Tables[tb_MXB糖尿病管理卡.__TableName];
                    if (tnbsfdt != null && tnbsfdt.Rows.Count > 0)
                    {
                        if (existCtrl is UC糖尿病患者随访记录表_显示) ctrl = null;
                        else ctrl = new UC糖尿病患者随访记录表_显示(dt个人档案.Select("个人档案编号='" + dno + "'"), null);
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “糖尿病” 患者，若您要对其进行糖尿病管理，请先修改个人基本信息表中的[既往史 疾病]为 “糖尿病” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表) ctrl = null;
                            else ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }
                    break;
                #endregion
                #region 脑卒中
                case "barItem脑卒中患者管理卡":
                    AtomEHR.Business.bllMXB脑卒中管理卡 nczbll = new Business.bllMXB脑卒中管理卡();
                    DataTable nczdt = nczbll.GetBusinessByKey(dno, true).Tables[tb_MXB脑卒中管理卡.__TableName];
                    if (nczdt != null && nczdt.Rows.Count > 0)
                    {
                        if (existCtrl is UC脑卒中患者管理卡_显示) ctrl = null;
                        else ctrl = new UC脑卒中患者管理卡_显示(dt个人档案.Select("个人档案编号='" + dno + "'"));
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “脑卒中” 患者，若您要对其进行脑卒中管理，请先修改个人基本信息表中的[既往史 疾病]为 “脑卒中” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表) ctrl = null;
                            else ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }
                    break;
                case "BarItem脑卒中患者随访服务记录表":
                    AtomEHR.Business.bllMXB脑卒中管理卡 nczsfbll = new Business.bllMXB脑卒中管理卡();
                    DataTable nczsfdt = nczsfbll.GetBusinessByKey(dno, true).Tables[tb_MXB脑卒中管理卡.__TableName];
                    if (nczsfdt != null && nczsfdt.Rows.Count > 0)
                    {
                        if (existCtrl is UC脑卒中患者随访记录表_显示) ctrl = null;
                        else ctrl = new UC脑卒中患者随访记录表_显示(dt个人档案.Select("个人档案编号='" + dno + "'"), null);
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “脑卒中” 患者，若您要对其进行脑卒中管理，请先修改个人基本信息表中的[既往史 疾病]为 “脑卒中” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表) ctrl = null;
                            else ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }
                    break;
                #endregion
                #region 冠心病
                case "barItem冠心病患者管理卡":
                    AtomEHR.Business.bllMXB冠心病管理卡 gxbbll = new Business.bllMXB冠心病管理卡();
                    DataTable gxbdt = gxbbll.GetBusinessByKey(dno, true).Tables[tb_MXB冠心病管理卡.__TableName];
                    if (gxbdt != null && gxbdt.Rows.Count > 0)
                    {
                        if (existCtrl is UC冠心病患者管理卡_显示) ctrl = null;
                        else ctrl = new UC冠心病患者管理卡_显示(dt个人档案.Select("个人档案编号='" + dno + "'"));
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “冠心病” 患者，若您要对其进行冠心病管理，请先修改个人基本信息表中的[既往史 疾病]为 “冠心病” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表) ctrl = null;
                            else ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }
                    break;
                case "BarItem冠心病患者随访服务记录表":
                    AtomEHR.Business.bllMXB冠心病管理卡 gxbsfbll = new Business.bllMXB冠心病管理卡();
                    DataTable gxbsfdt = gxbsfbll.GetBusinessByKey(dno, true).Tables[tb_MXB冠心病管理卡.__TableName];
                    if (gxbsfdt != null && gxbsfdt.Rows.Count > 0)
                    {
                        if (existCtrl is UC冠心病患者随访服务记录表_显示) ctrl = null;
                        else ctrl = new UC冠心病患者随访服务记录表_显示(dt个人档案.Select("个人档案编号='" + dno + "'"), null);
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “冠心病” 患者，若您要对其进行冠心病管理，请先修改个人基本信息表中的[既往史 疾病]为 “冠心病” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表) ctrl = null;
                            else ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }
                    break;
                #endregion
                #region 高危人群干预随访 //2019年6月12日 yfh调整，所有高危人群只要不是高血压的，都可以录入高危随访
                case "BarItem高血压高危人群干预随访表":
                    AtomEHR.Business.bllMXB高血压管理卡 bll高危人群 = new Business.bllMXB高血压管理卡();
                    DataTable dt高危人群 = bll高危人群.GetBusinessByKey(dno, true).Tables[tb_健康档案_个人健康特征.__TableName];
                    if (dt高危人群 != null && (dt高危人群.Rows[0][tb_健康档案_个人健康特征.是否高血压].Equals("2")
                        || dt高危人群.Rows[0][tb_健康档案_个人健康特征.是否血脂].Equals("1")
                        || dt高危人群.Rows[0][tb_健康档案_个人健康特征.是否空腹血糖].Equals("1")
                        || dt高危人群.Rows[0][tb_健康档案_个人健康特征.是否肥胖].Equals("1")
                        || dt高危人群.Rows[0][tb_健康档案_个人健康特征.是否重度吸烟].Equals("1")
                        || dt高危人群.Rows[0][tb_健康档案_个人健康特征.是否超重肥胖].Equals("1")))
                    {
                        if (existCtrl is UC高血压高危干预_显示) ctrl = null;
                        else ctrl = new UC高血压高危干预_显示(dt个人档案.Select("个人档案编号='" + dno + "'"), null);
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “高危人群” ！高危人群的标准是收缩压130-139，舒张压是85-89！等"))
                        {
                            //if (existCtrl is UC个人基本信息表) ctrl = null;
                            //else ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                        }
                    }
                    break;
                #endregion
                #endregion

                #region 预防接种信息
                case "barItem预防接种记录":
                    if (existCtrl is AtomEHR.公共卫生.Module.个人健康.预防接种信息.UC预防接种记录) ctrl = null;
                    else ctrl = new AtomEHR.公共卫生.Module.个人健康.预防接种信息.UC预防接种记录();
                    break;
                #endregion

                #region 重性精神疾病患者管理
                case "barItem精神疾病患者补充表":
                    AtomEHR.Business.bll精神疾病信息补充表 bll精神疾病 = new bll精神疾病信息补充表();
                    DataSet ds精神疾病 = bll精神疾病.GetBusinessByKey(dno, true);
                    if (ds精神疾病.Tables[tb_精神疾病信息补充表.__TableName].Rows.Count == 1)
                    {
                        if (existCtrl is AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理.UC重性精神疾病患者个人信息补充表_显示) ctrl = null;
                        else ctrl = new AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理.UC重性精神疾病患者个人信息补充表_显示(ds精神疾病, row);
                    }
                    else
                    {
                        Msg.ShowInformation("该服务对象不是精神病人，请修改个人基本信息的既往病史为重型精神疾病！");
                        if (existCtrl is UC个人基本信息表_显示) ctrl = null;
                        else ctrl = new UC个人基本信息表_显示(row);
                    }
                    break;
                case "barItem精神疾病患者随访表":
                    AtomEHR.Business.bll精神病随访记录 bll精神病随访 = new bll精神病随访记录();
                    DataSet ds精神病随访 = bll精神病随访.Get基本概要(dno);
                    if (ds精神病随访.Tables[0].Rows.Count > 0)
                    {
                        //2018年4月10日 yufh 调整：去掉原来判断条件，验证补充表的下次随访时间
                        string count随访 = ds精神病随访.Tables[1].Rows[0][0].ToString();
                        if (Convert.ToInt32(count随访) > 0)
                        {
                            if (existCtrl is AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理.UC重性精神疾病患者随访服务记录表_显示) ctrl = null;
                            else ctrl = new AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理.UC重性精神疾病患者随访服务记录表_显示(dno, null);
                        }
                        else
                        {
                            if (existCtrl is AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理.UC重性精神疾病患者随访服务记录表) ctrl = null;
                            else ctrl = new AtomEHR.公共卫生.Module.个人健康.重性精神疾病患者管理.UC重性精神疾病患者随访服务记录表(dno, null, UpdateType.Add);
                        }
                    }
                    else
                    {
                        Msg.ShowInformation("该服务对象不是精神病人，请修改个人基本信息的既往病史为重型精神疾病！");
                    }

                    break;
                #endregion

                #region 残疾人信息健康管理
                case "barItem听力语言残疾随访表":
                    if (!string.IsNullOrEmpty(dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表].ToString()))
                    {
                        if (Convert.ToString(dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表]) != "未建")//存在随访表
                        {
                            if (existCtrl is AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC听力言语残疾人健康管理随访表_显示) ctrl = null;
                            else ctrl = new AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC听力言语残疾人健康管理随访表_显示(row);
                        }
                        else
                        {
                            if (existCtrl is AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC听力言语残疾人健康管理随访表) ctrl = null;
                            else ctrl = new AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC听力言语残疾人健康管理随访表(row, UpdateType.Add);
                        }
                    }
                    else
                    {
                        Msg.ShowInformation("该服务对象不是听力言语残疾，请修改个人基本信息的残疾情况为听力残疾或者言语残疾！");
                        if (existCtrl is UC个人基本信息表_显示) ctrl = null;
                        else ctrl = new UC个人基本信息表_显示(row);
                    }
                    break;
                case "barItem肢体残疾随访表":
                    if (!string.IsNullOrEmpty(dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.肢体残疾随访表].ToString()))
                    {
                        if (Convert.ToString(dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.肢体残疾随访表]) != "未建")//存在随访表
                        {
                            if (existCtrl is AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC肢体残疾人健康管理随访表_显示) ctrl = null;
                            else ctrl = new AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC肢体残疾人健康管理随访表_显示(row);
                        }
                        else
                        {
                            if (existCtrl is AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC肢体残疾人健康管理随访表) ctrl = null;
                            else ctrl = new AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC肢体残疾人健康管理随访表(row, UpdateType.Add);
                        }
                    }
                    else
                    {
                        Msg.ShowInformation("该服务对象不是肢体残疾，请修改个人基本信息的残疾情况为肢体残疾！");
                        if (existCtrl is UC个人基本信息表_显示) ctrl = null;
                        else ctrl = new UC个人基本信息表_显示(row);
                    }
                    break;
                case "barItem智力残疾随访表":
                    if (existCtrl is AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC智力残疾人健康管理随访表) ctrl = null;
                    else ctrl = new AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC智力残疾人健康管理随访表();
                    break;
                case "barItem视力残疾随访表":
                    if (existCtrl is AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC视力残疾人健康管理随访表) ctrl = null;
                    else ctrl = new AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC视力残疾人健康管理随访表();
                    break;

                case "barItem残疾人康复服务随访记录表":
                    AtomEHR.Business.bll残疾人康复服务随访记录表 bll残疾人 = new bll残疾人康复服务随访记录表();
                    DataTable dt残疾人 = bll残疾人.GetBusinessByKey(dno, true).Tables[tb_残疾人_基本信息.__TableName];
                    //if (!string.IsNullOrEmpty(dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.残疾人康复服务随访记录表].ToString()))
                    //{
                    //if (!string.IsNullOrEmpty(dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.残疾人康复服务随访记录表].ToString()) &&
                    //    Convert.ToString(dt个人健康特征.Rows[0][tb_健康档案_个人健康特征.残疾人康复服务随访记录表]) != "未建")//存在随访表
                    //{
                    if (dt残疾人!=null && dt残疾人.Rows.Count > 0)
                    {
                        if (bll残疾人.CheckNoExists(dno))
                        {
                            if (existCtrl is AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC残疾人康复服务随访记录表_显示) ctrl = null;
                            else ctrl = new AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC残疾人康复服务随访记录表_显示(row);
                        }
                        else
                        {
                            if (existCtrl is AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC残疾人康复服务随访记录表) ctrl = null;
                            else ctrl = new AtomEHR.公共卫生.Module.个人健康.残疾人健康信息.UC残疾人康复服务随访记录表(row, UpdateType.Add);
                        }
                    }
                    else
                    {
                        Msg.ShowInformation("该服务对象不是残疾人，请修改个人基本信息的残疾情况为任意一种或多种残疾！");
                        if (existCtrl is UC个人基本信息表) ctrl = null;
                        else ctrl = new UC个人基本信息表(row,UpdateType.Modify);
                    }

                    break;
                #endregion

                #region  肺结核
                case "navBarItem肺结核第一次":
                    //判断既往史中是否有结核病
                    //若无，提示修改既往史
                    //若有，判断是否已录入第一次随访记录
                    //若存在第一次随访记录，则显示；否则，显示添加界面
                    AtomEHR.Business.bll肺结核第一次随访 bll肺结核 = new bll肺结核第一次随访();

                    //判断既往史中是否有“结核病”
                    //string str个人档案号 = dt个人档案.Rows[0][tb_健康档案.个人档案编号].ToString();
                    bool bisExist = bll肺结核.Exists既往病史(dno);
                    if(bisExist)
                    {
                        DataTable data肺结核 = bll肺结核.GetBusinessByKey(dno, false).Tables[tb_MXB肺结核第一次随访.__TableName];
                        if(data肺结核!=null && data肺结核.Rows.Count > 0)
                        {
                            ctrl = new AtomEHR.公共卫生.Module.个人健康.肺结核.UC肺结核第一次随访_显示(dt个人档案.Select("个人档案编号='" + dno + "'"),null);
                        }
                        else
                        {
                            ctrl = new AtomEHR.公共卫生.Module.个人健康.肺结核.UC肺结核第一次随访(dt个人档案.Select("个人档案编号='" + dno + "'"), AtomEHR.Common.UpdateType.Add, "");
                        }
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “结核病” 患者，若您要对其进行结核病管理，请先修改个人基本信息表中的[既往史 疾病]为 “结核病” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表)
                            {
                                ctrl = null;
                            }
                            else
                            {
                                ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                            }
                        }
                    }
                    break;
                case "navBarItem肺结核后续随访":
                    AtomEHR.Business.bllMXB肺结核后续随访 bll肺后续 = new bllMXB肺结核后续随访();
                    AtomEHR.Business.bll肺结核第一次随访 bll肺结核2 = new bll肺结核第一次随访();
                    bool bisExist2 = bll肺结核2.Exists既往病史(dno);

                    //是肺结核患者的情况下
                    if(bisExist2)
                    {
                        //取出肺结核患者的非第一次随访记录
                        DataTable data后续 = bll肺后续.GetBusinessByKey(dno, false).Tables[tb_MXB肺结核后续随访.__TableName];
                        if (data后续 != null && data后续.Rows.Count > 0)
                        {
                            ctrl = new AtomEHR.公共卫生.Module.个人健康.肺结核.UC肺结核后续随访_显示(dt个人档案.Select("个人档案编号='" + dno + "'"), null);
                        }
                        else
                        {
                            //没有后续随访时，直接进行添加操作
                            int i首次 = 0;
                            int i后续 = 0;
                            bool bret = bll肺后续.Get两种随访次数(dno, out i首次, out i后续);
                            if (bret)
                            {
                                if (i首次 <= i后续)
                                {
                                    DialogResult reult = MessageBox.Show("肺结核患病期间可能缺少第一次入户随访记录。是否继续进行本次随访操作？\n“是”：继续进行操作；“否”：停止到期操作。"
                                       , "确认", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button2);
                                    if (reult == DialogResult.Yes)
                                    {
                                        ctrl = new AtomEHR.公共卫生.Module.个人健康.肺结核.UC肺结核后续随访(dt个人档案.Select("个人档案编号='" + dno + "'"), AtomEHR.Common.UpdateType.Add, "");
                                    }
                                    else
                                    {
                                        ctrl = null;
                                    }
                                }
                                else
                                {
                                    ctrl = new AtomEHR.公共卫生.Module.个人健康.肺结核.UC肺结核后续随访(dt个人档案.Select("个人档案编号='" + dno + "'"), AtomEHR.Common.UpdateType.Add, "");
                                }
                            }
                            else
                            {
                                ctrl = null;
                                Msg.ShowInformation("网络可能出现问题，请检查。");
                            }
                        }
                    }
                    else
                    {
                        if (Msg.AskQuestion("此人不是 “结核病” 患者，若您要对其进行结核病管理，请先修改个人基本信息表中的[既往史 疾病]为 “结核病” 状态！"))
                        {
                            if (existCtrl is UC个人基本信息表)
                            {
                                ctrl = null;
                            }
                            else
                            {
                                ctrl = new UC个人基本信息表(row, AtomEHR.Common.UpdateType.Modify);
                            }
                        }
                    }
                    break;
                #endregion

                #region 艾滋病
                case "navBarItem个案随访表":
                    AtomEHR.Business.bll个案随访表 bll艾滋病 = new AtomEHR.Business.bll个案随访表();
                    DataTable dt艾滋病 = bll艾滋病.GetBusinessByKey(dno, false).Tables[tb_个案随访表.__TableName];
                    if (dt艾滋病 != null && dt艾滋病.Rows.Count > 0)
                    {
                        if (existCtrl is UC个案随访表_显示) ctrl = null;
                        else ctrl = new UC个案随访表_显示(dt个人档案.Select("个人档案编号='" + dno + "'"), null);
                    }
                    else
                    {
                        if (existCtrl is UC个案随访表) ctrl = null;
                        else ctrl = new UC个案随访表(dt个人档案.Select("个人档案编号='" + dno + "'"), AtomEHR.Common.UpdateType.Add, "");
                    }
                        
                    break;
                #endregion
                case "barTest":
                    ctrl = new AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息.UC个人基本信息表_显示();
                    break;

                default:
                    break;
            }
            return ctrl;
        }
    }
}
