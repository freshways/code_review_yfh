﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Library;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Common;
using DevExpress.XtraEditors;
using AtomEHR.公共卫生.util;
using System.Text.RegularExpressions;
using DevExpress.XtraGrid.Views.Grid;

namespace AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息
{
    public partial class UC个人基本信息表 : UserControlBase
    {
        #region Fields
        bll健康档案 _Bll = new bll健康档案();
        DataSet _ds健康档案;
        frm个人健康 _frm;
        string _docNo;
        string _serverDateTime;
        DataRow _户主档案;
        DataTable _Old健康档案;
        #endregion

        #region Constructor
        public UC个人基本信息表(Form frm, UpdateType _updateType)
        {
            InitializeComponent();
            _frm = (frm个人健康)frm;
            _docNo = _frm._docNo;
            _serverDateTime = _Bll.ServiceDateTime;
            _户主档案 = _frm._家庭全部成员健康档案.Tables[tb_健康档案.__TableName].Select("与户主关系 = '户主'").Length == 0 ? null : _frm._家庭全部成员健康档案.Tables[tb_健康档案.__TableName].Select("与户主关系 = '户主'")[0];
            _UpdateType = _updateType;
            if (_updateType == UpdateType.Modify)//表示是修改
            {
                _ds健康档案 = _Bll.GetOneUserCodeByKey(_docNo, true);
            }
            else if (_updateType == UpdateType.AddPeople)//新增
            {
                _Bll.GetNewUserByKey("-", true);//下载一个空业务单据
                _Bll.NewUser(_frm._familyDocNo);
                _ds健康档案 = _Bll.CurrentBusiness;
            }
            else if (_updateType == UpdateType.Add)//只能添加户主
            {
                _ds健康档案 = _Bll.GetBusinessByKey(_docNo, true);
            }

        }

        #endregion

        #region Handler Events
        private void frm个人基本信息表_Load(object sender, EventArgs e)
        {
            this.chk孕产1.Width = 50;
            this.chk孕产2.Width = 80;
            this.chk孕产3.Width = 120;
            this.chk孕产4.Width = 120;
            this.cbo产次.Width = 50;
            this.cbo孕次.Width = 50;
            this.txt证件编号.ImeMode = System.Windows.Forms.ImeMode.Off;

            cbo居住地址_市.EditValueChanged += cbo居住地址_市_EditValueChanged;
            cbo居住地址_县.EditValueChanged += cbo居住地址_县_EditValueChanged;
            cbo居住地址_街道.EditValueChanged += cbo居住地址_街道_EditValueChanged;

            DataBinder.BindingLookupEditDataSource(lkp家族关系, DataDictCache.Cache.t家族史成员, "P_DESC", "P_CODE");
            DataBinder.BindingLookupEditDataSource(cbo居住地址_村委会, DataDictCache.Cache.t地区信息, "地区名称", "地区编码");

            DataBinder.BindingLookupEditDataSource(cbo居住地址_街道, DataDictCache.Cache.t地区信息, "地区名称", "地区编码");

            DataBinder.BindingLookupEditDataSource(cbo居住地址_县, DataDictCache.Cache.t地区信息, "地区名称", "地区编码");

            //绑定相关缓存数据
            DataView dv市 = new DataView(DataDictCache.Cache.t地区信息);
            dv市.RowFilter = "上级编码=37";
            DataBinder.BindingLookupEditDataSource(cbo居住地址_市, dv市.ToTable(), "地区名称", "地区编码");

            cbo居住地址_市.EditValue = "3713";

            cbo居住地址_县.EditValue = Loginer.CurrentUser.单位代码;
            this.cbo与户主关系.SelectedIndex = 0;

            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t性别, cbo性别);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.tRH, cboRH阴性);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t常住类型, cbo常住类型);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t档案类别, cbo档案类别);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t婚姻状况, cbo婚姻状况);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t劳动强度, cbo劳动程度);
            if (_UpdateType == UpdateType.Add)
            {
                AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t与户主关系_户主, cbo与户主关系, "P_CODE", "P_DESC");
            }
            else if (_UpdateType == UpdateType.AddPeople)
            {
                AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t与户主关系_非户主, cbo与户主关系, "P_CODE", "P_DESC");
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t与户主关系, cbo与户主关系, "P_CODE", "P_DESC");
            }
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t民族, cbo民族);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t文化程度, cbo文化程度);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t血型, cbo血型);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t证件类型, cbo证件类型);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t职业, cbo职业);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t有无, cbo过敏史);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t有无, cbo暴露史);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t有无, cbo遗传病史);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t有无, cbo残疾情况);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t家族史成员, cbo家族史关系);
            AtomEHR.公共卫生.util.ControlsHelper.BindComboxData(DataDictCache.Cache.t家族史成员, rpcbo家族关系);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("2", cbo过敏史);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("2", cbo暴露史);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("2", cbo遗传病史);
            AtomEHR.公共卫生.util.ControlsHelper.SetComboxData("2", cbo残疾情况);
            this.cbo与户主关系.SelectedIndex = 0;
            this.radio档案状态.SelectedIndex = 0;


            this.radio疾病.EditValue = this.radio手术.EditValue = this.radio输血.EditValue = this.radio外伤.EditValue = "2";
            DoBindingSummaryEditor(_ds健康档案);
            _Old健康档案 = _ds健康档案.Tables[tb_健康档案.__TableName].Copy();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (f保存检查())
            {
                bool tnb = false;  //糖尿病
                bool nzz = false; //脑卒中
                bool gxb = false; //冠心病
                bool gxy = false; //高血压
                bool jsb = false; //精神病
                bool mz = false;//慢阻肺
                bool zl = false;//肿瘤

                if (string.IsNullOrEmpty(_frm._familyDocNo))//不存在家庭档案编号，则说明此人未加入任何家庭
                {
                    DataTable dt家庭档案 = _ds健康档案.Tables[tb_家庭档案.__TableName];//获取一个 家庭档案的Datatable
                    if (dt家庭档案 != null) // yufh修改，这地方是个bug，没有家庭档案的可以忽略跳过，进行后续修改
                    {
                        DataRow row家庭档案 = dt家庭档案.Rows.Add();//家庭档案中 在添加一条数据
                        string _jiedao = this.cbo居住地址_村委会.EditValue == null ? "" : Convert.ToString(this.cbo居住地址_村委会.EditValue);
                        #region tb_家庭档案

                        row家庭档案[tb_家庭档案.街道] = _jiedao;
                        row家庭档案[tb_家庭档案.所属机构] = Loginer.CurrentUser.所属机构;
                        row家庭档案[tb_家庭档案.创建机构] = Loginer.CurrentUser.所属机构;
                        row家庭档案[tb_家庭档案.创建人] = Loginer.CurrentUser.用户编码;
                        row家庭档案[tb_家庭档案.创建时间] = _serverDateTime;
                        row家庭档案[tb_家庭档案.修改人] = Loginer.CurrentUser.用户编码;
                        row家庭档案[tb_家庭档案.修改时间] = _serverDateTime;
                    }

                    #endregion
                }

                #region tb_健康档案 保存

                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.与户主关系] = ControlsHelper.GetComboxKey(cbo与户主关系);
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.档案状态] = Get档案状态();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.档案位置] = this.txt档案位置.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(), false);
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.证件类型] = ControlsHelper.GetComboxKey(this.cbo证件类型);//.SelectedIndex;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.身份证号] = this.txt证件编号.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人电话] = this.txt本人电话.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.工作单位] = this.txt工作单位.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人电话] = this.txt联系人电话.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.联系人姓名] = this.txt联系人姓名.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.劳动强度] = ControlsHelper.GetComboxKey(this.cbo劳动程度);//.SelectedIndex;

                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.是否贫困人口] = this.chk是否贫困人口.Checked ? "1" : "";
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.是否流动人口] = this.chk是否流动人口.Checked ? "1" : "";
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.是否签约] = this.chk是否签约.Checked ? "1" : "";

                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.省] = "37";
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.市] = this.cbo居住地址_市.EditValue;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.区] = this.cbo居住地址_县.EditValue;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.街道] = this.cbo居住地址_街道.EditValue;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居委会] = this.cbo居住地址_村委会.EditValue;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.民族] = ControlsHelper.GetComboxKey(this.cbo民族);//.SelectedIndex;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.职业] = ControlsHelper.GetComboxKey(this.cbo职业);//.SelectedIndex;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);//.SelectedIndex;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);//.SelectedIndex;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗费支付类型] = Get医疗费用支付方式();
                #region    新版本添加
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗保险号] = this.textEdit职工医疗保险卡号.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.新农合号] = this.textEdit居民医疗保险卡号.Text.Trim();
                #endregion
                //_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.职工医疗保险卡号] = this.textEdit职工医疗保险卡号.Text.Trim();
                //_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居民医疗保险卡号] = this.textEdit居民医疗保险卡号.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.贫困救助卡号] = this.textEdit贫困救助卡号.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗费用支付类型其他] = this.txt医疗费用支付方式_其他.Text.Trim();
                //_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.医疗保险号] = this.txt医疗保险号.Text.Trim();
                //_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.新农合号] = this.txt新农合号.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属片区] = this.txt所属片区.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.档案类别] = ControlsHelper.GetComboxKey(this.cbo档案类别);//.SelectedIndex;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.复核备注] = this.txt复核备注.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.复核标记] = ch复核标记.Checked ? "1" : "";
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.二次复核] = ch二次复核.Checked ? "1" : "";
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.二次复核时间] = _serverDateTime;
                
                //孕产情况  如果孕产情况显示，则表示其为孕妇，需要进行保存孕产情况
                string 孕产情况 = string.Empty;
                bool isYunFu = false;
                if (this.layout孕产情况.Visibility == DevExpress.XtraLayout.Utils.LayoutVisibility.Always)
                {
                    if (this.chk孕产1.Checked)
                        孕产情况 = "未孕";
                    else if (this.chk孕产2.Checked)
                    {
                        孕产情况 = "已孕未生产";
                        isYunFu = true;
                    }
                    else if (this.chk孕产3.Checked)
                    {
                        孕产情况 = "已生产随访期内";
                        isYunFu = true;
                    }
                    else if (this.chk孕产4.Checked)
                    {
                        孕产情况 = "已生产随访期外";
                        isYunFu = false;
                    }
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.怀孕情况] = 孕产情况;
                    if (!string.IsNullOrEmpty(孕产情况) && 孕产情况 != "未孕")
                    {
                        if (this.cbo孕次.Text.Trim() == "" || this.cbo产次.Text.Trim() == "")
                        {
                            Msg.Warning("请选择孕次及产次！");
                            this.cbo孕次.Focus();
                            return;
                        }
                    }
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.孕次] = this.cbo孕次.EditValue;
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.产次] = this.cbo产次.EditValue;
                }

                int i = Get缺项();//缺项
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.缺项] = i;//.SelectedIndex;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.完整度] = Get完整度(i);//.SelectedIndex;

                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.厨房排气设施] = Get厨房排气设施();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.燃料类型] = Get燃料类型();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.饮水] = Get饮水();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.厕所] = Get厕所();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.禽畜栏] = Get禽畜栏();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.外出情况] = GetFlowLayoutResult(flow外出);
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.外出地址] = this.txt外出地址.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.调查时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");

                #endregion

                #region tb_健康档案_个人健康特征
                if (_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows.Count == 0)
                {
                    DataRow row个人健康特征 = _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows.Add();
                    row个人健康特征[tb_健康档案_个人健康特征.个人档案编号] = _docNo; 
                }
                #endregion

                #region tb_健康档案_健康状态
                string ywjws = "";//有无既往史
                if (this.gv既往史.RowCount > 0)
                {
                    ywjws = "1";
                }
                //判断是否存在既往史，如果没有则重新添加既往史表，如果有则跳过。
                if (_ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows.Count == 0)
                {
                    DataRow row健康状态 = _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows.Add();
                    row健康状态[tb_健康档案_健康状态.个人档案编号] = _docNo; 
                    row健康状态[tb_健康档案_健康状态.暴露史] = ControlsHelper.GetComboxKey(this.cbo暴露史);
                    row健康状态[tb_健康档案_健康状态.暴露史毒物] = this.txt暴露史_毒物.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.暴露史化学品] = this.txt暴露史_化学品.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.暴露史射线] = this.txt暴露史_射线.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.遗传病史有无] = ControlsHelper.GetComboxKey(this.cbo遗传病史);
                    row健康状态[tb_健康档案_健康状态.遗传病史] = this.txt遗传病史_疾病名称.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.有无残疾] = ControlsHelper.GetComboxKey(cbo残疾情况);
                    row健康状态[tb_健康档案_健康状态.有无疾病] = this.radio疾病.EditValue;
                    row健康状态[tb_健康档案_健康状态.有无既往史] = ywjws;
                    row健康状态[tb_健康档案_健康状态.残疾情况] = Get残疾情况();
                    row健康状态[tb_健康档案_健康状态.残疾其他] = this.txt残疾情况_其他残疾.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.过敏史有无] = ControlsHelper.GetComboxKey(this.cbo过敏史);
                    row健康状态[tb_健康档案_健康状态.药物过敏史] = Get过敏史();
                    row健康状态[tb_健康档案_健康状态.过敏史其他] = this.txt过敏史_其他.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.血型] = ControlsHelper.GetComboxKey(this.cbo血型);
                    row健康状态[tb_健康档案_健康状态.RH] = ControlsHelper.GetComboxKey(this.cboRH阴性);
                }
                else
                {
                    DataRow row健康状态 = _ds健康档案.Tables[tb_健康档案_健康状态.__TableName].Rows[0];
                    row健康状态[tb_健康档案_健康状态.个人档案编号] = _docNo; 
                    row健康状态[tb_健康档案_健康状态.暴露史] = ControlsHelper.GetComboxKey(this.cbo暴露史);
                    row健康状态[tb_健康档案_健康状态.暴露史毒物] = this.txt暴露史_毒物.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.暴露史化学品] = this.txt暴露史_化学品.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.暴露史射线] = this.txt暴露史_射线.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.遗传病史有无] = ControlsHelper.GetComboxKey(this.cbo遗传病史);
                    row健康状态[tb_健康档案_健康状态.遗传病史] = this.txt遗传病史_疾病名称.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.有无残疾] = ControlsHelper.GetComboxKey(cbo残疾情况);
                    row健康状态[tb_健康档案_健康状态.有无疾病] = this.radio疾病.EditValue;
                    row健康状态[tb_健康档案_健康状态.有无既往史] = ywjws;
                    row健康状态[tb_健康档案_健康状态.残疾情况] = Get残疾情况();
                    row健康状态[tb_健康档案_健康状态.残疾其他] = this.txt残疾情况_其他残疾.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.过敏史有无] = ControlsHelper.GetComboxKey(this.cbo过敏史);
                    row健康状态[tb_健康档案_健康状态.药物过敏史] = Get过敏史();
                    row健康状态[tb_健康档案_健康状态.过敏史其他] = this.txt过敏史_其他.Text.Trim();
                    row健康状态[tb_健康档案_健康状态.血型] = ControlsHelper.GetComboxKey(this.cbo血型);
                    row健康状态[tb_健康档案_健康状态.RH] = ControlsHelper.GetComboxKey(this.cboRH阴性);
                }

                #endregion
                int Age = GetAge(this.dte出生日期.Text.Trim());
                #region 6岁以下儿童需要保存  儿童基本信息表
                if (Age > 6)//6岁以上儿童不需要创建儿童信息表
                {
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童基本信息] = "未建";
                }
                else
                {
                    if (_ds健康档案.Tables[tb_儿童基本信息.__TableName].Rows.Count == 0)//不存在儿童信息
                    {
                        DataRow row儿童基本信息 = _ds健康档案.Tables[tb_儿童基本信息.__TableName].Rows.Add();
                        row儿童基本信息[tb_儿童基本信息.个人档案编号] = "";
                        row儿童基本信息[tb_儿童基本信息.卡号] = "";
                        row儿童基本信息[tb_儿童基本信息.家庭档案编号] = "";
                        row儿童基本信息[tb_儿童基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                        row儿童基本信息[tb_儿童基本信息.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                        row儿童基本信息[tb_儿童基本信息.身份证号] = this.txt证件编号.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.工作单位] = this.txt工作单位.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.省] = "37";
                        row儿童基本信息[tb_儿童基本信息.市] = this.cbo居住地址_市.EditValue;
                        row儿童基本信息[tb_儿童基本信息.区] = this.cbo居住地址_县.EditValue;
                        row儿童基本信息[tb_儿童基本信息.街道] = this.cbo居住地址_街道.EditValue;
                        row儿童基本信息[tb_儿童基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                        row儿童基本信息[tb_儿童基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.所属片区] = this.txt所属片区.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.居住状况] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                        row儿童基本信息[tb_儿童基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row儿童基本信息[tb_儿童基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row儿童基本信息[tb_儿童基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row儿童基本信息[tb_儿童基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row儿童基本信息[tb_儿童基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row儿童基本信息[tb_儿童基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row儿童基本信息[tb_儿童基本信息.医疗费支付类型] = GetFlowLayoutResult(flow医疗费用支付方式);
                        row儿童基本信息[tb_儿童基本信息.新农合号] = this.txt新农合号.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                        row儿童基本信息[tb_儿童基本信息.HAPPENTIME] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row儿童基本信息[tb_儿童基本信息.创建日期] = _serverDateTime;
                        row儿童基本信息[tb_儿童基本信息.修改日期] = this.dte最近更新时间.Text;
                        row儿童基本信息[tb_儿童基本信息.创建人] = Loginer.CurrentUser.用户编码;
                        row儿童基本信息[tb_儿童基本信息.修改人] = Loginer.CurrentUser.用户编码;
                        row儿童基本信息[tb_儿童基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                        row儿童基本信息[tb_儿童基本信息.档案状态] = this.radio档案状态.EditValue;
                        row儿童基本信息[tb_儿童基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                        row儿童基本信息[tb_儿童基本信息.缺项] = 15;
                        row儿童基本信息[tb_儿童基本信息.完整度] = 0;
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童基本信息] = string.Format("{0},{1}", "15", "0");
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.新生儿随访记录] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表满月] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表3月] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表6月] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表8月] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表12月] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表18月] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表2岁] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表2岁半] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表3岁] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表4岁] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表5岁] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童健康检查记录表6岁] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童入托信息表] = "未建";
                    }
                    else if (_ds健康档案.Tables[tb_儿童基本信息.__TableName].Rows.Count == 1)
                    {
                        DataRow row儿童基本信息 = _ds健康档案.Tables[tb_儿童基本信息.__TableName].Rows[0];
                        //row儿童基本信息[tb_儿童基本信息.个人档案编号] = "";
                        //row儿童基本信息[tb_儿童基本信息.卡号] = "";
                        //row儿童基本信息[tb_儿童基本信息.家庭档案编号] = "";
                        row儿童基本信息[tb_儿童基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                        row儿童基本信息[tb_儿童基本信息.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                        row儿童基本信息[tb_儿童基本信息.身份证号] = this.txt证件编号.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.工作单位] = this.txt工作单位.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.省] = "37";
                        row儿童基本信息[tb_儿童基本信息.市] = this.cbo居住地址_市.EditValue;
                        row儿童基本信息[tb_儿童基本信息.区] = this.cbo居住地址_县.EditValue;
                        row儿童基本信息[tb_儿童基本信息.街道] = this.cbo居住地址_街道.EditValue;
                        row儿童基本信息[tb_儿童基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                        row儿童基本信息[tb_儿童基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.所属片区] = this.txt所属片区.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.居住状况] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                        row儿童基本信息[tb_儿童基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row儿童基本信息[tb_儿童基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row儿童基本信息[tb_儿童基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row儿童基本信息[tb_儿童基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row儿童基本信息[tb_儿童基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row儿童基本信息[tb_儿童基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row儿童基本信息[tb_儿童基本信息.医疗费支付类型] = GetFlowLayoutResult(flow医疗费用支付方式);
                        row儿童基本信息[tb_儿童基本信息.新农合号] = this.txt新农合号.Text.Trim();
                        row儿童基本信息[tb_儿童基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                        row儿童基本信息[tb_儿童基本信息.HAPPENTIME] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row儿童基本信息[tb_儿童基本信息.修改日期] = this.dte最近更新时间.Text;
                        row儿童基本信息[tb_儿童基本信息.修改人] = Loginer.CurrentUser.用户编码;
                        row儿童基本信息[tb_儿童基本信息.档案状态] = this.radio档案状态.EditValue;
                        row儿童基本信息[tb_儿童基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                        row儿童基本信息[tb_儿童基本信息.缺项] = 15;
                        row儿童基本信息[tb_儿童基本信息.完整度] = 0;
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.儿童基本信息] = string.Format("{0},{1}", 15, 0);
                    }
                }

                #endregion
                #region 老年人信息保存
                if (Age >= 65)
                {
                    if (_ds健康档案.Tables[tb_老年人基本信息.__TableName].Rows.Count == 0)//不存在老年人信息
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.老年人随访] = "未建";
                        DataRow row老年人 = _ds健康档案.Tables[tb_老年人基本信息.__TableName].Rows.Add();
                        row老年人[tb_老年人基本信息.个人档案编号] = _docNo;
                        //row老年人[tb_老年人基本信息.卡号] = "";
                        row老年人[tb_老年人基本信息.家庭档案编号] = _frm._familyDocNo;
                        row老年人[tb_老年人基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                        row老年人[tb_老年人基本信息.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                        row老年人[tb_老年人基本信息.身份证号] = this.txt证件编号.Text.Trim();
                        row老年人[tb_老年人基本信息.工作单位] = this.txt工作单位.Text.Trim();
                        row老年人[tb_老年人基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                        row老年人[tb_老年人基本信息.省] = "37";
                        row老年人[tb_老年人基本信息.市] = this.cbo居住地址_市.EditValue;
                        row老年人[tb_老年人基本信息.区] = this.cbo居住地址_县.EditValue;
                        row老年人[tb_老年人基本信息.街道] = this.cbo居住地址_街道.EditValue;
                        row老年人[tb_老年人基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                        row老年人[tb_老年人基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row老年人[tb_老年人基本信息.新农合号] = this.txt医疗保险号.Text.Trim();
                        row老年人[tb_老年人基本信息.所属片区] = this.txt所属片区.Text.Trim();
                        row老年人[tb_老年人基本信息.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                        row老年人[tb_老年人基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row老年人[tb_老年人基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row老年人[tb_老年人基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row老年人[tb_老年人基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row老年人[tb_老年人基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row老年人[tb_老年人基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row老年人[tb_老年人基本信息.医疗费支付类型] = GetFlowLayoutResult(flow医疗费用支付方式);
                        row老年人[tb_老年人基本信息.新农合号] = this.txt新农合号.Text.Trim();
                        row老年人[tb_老年人基本信息.所属片区] = Loginer.CurrentUser.所属机构;
                        row老年人[tb_老年人基本信息.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row老年人[tb_老年人基本信息.创建时间] = _serverDateTime;
                        row老年人[tb_老年人基本信息.修改时间] = _serverDateTime;
                        row老年人[tb_老年人基本信息.创建人] = Loginer.CurrentUser.用户编码;
                        row老年人[tb_老年人基本信息.修改人] = Loginer.CurrentUser.用户编码;
                        row老年人[tb_老年人基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                        row老年人[tb_老年人基本信息.档案状态] = this.radio档案状态.EditValue;
                        row老年人[tb_老年人基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);

                    }
                    else
                    {
                        DataRow row老年人 = _ds健康档案.Tables[tb_老年人基本信息.__TableName].Rows[0];
                        row老年人[tb_老年人基本信息.个人档案编号] = _docNo;
                        //row老年人[tb_老年人基本信息.卡号] = "";
                        row老年人[tb_老年人基本信息.家庭档案编号] = _frm._familyDocNo;
                        row老年人[tb_老年人基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                        row老年人[tb_老年人基本信息.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                        row老年人[tb_老年人基本信息.身份证号] = this.txt证件编号.Text.Trim();
                        row老年人[tb_老年人基本信息.工作单位] = this.txt工作单位.Text.Trim();
                        row老年人[tb_老年人基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                        row老年人[tb_老年人基本信息.省] = "37";
                        row老年人[tb_老年人基本信息.市] = this.cbo居住地址_市.EditValue;
                        row老年人[tb_老年人基本信息.区] = this.cbo居住地址_县.EditValue;
                        row老年人[tb_老年人基本信息.街道] = this.cbo居住地址_街道.EditValue;
                        row老年人[tb_老年人基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                        row老年人[tb_老年人基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row老年人[tb_老年人基本信息.新农合号] = this.txt医疗保险号.Text.Trim();
                        row老年人[tb_老年人基本信息.所属片区] = this.txt所属片区.Text.Trim();
                        row老年人[tb_老年人基本信息.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                        row老年人[tb_老年人基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row老年人[tb_老年人基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row老年人[tb_老年人基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row老年人[tb_老年人基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row老年人[tb_老年人基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row老年人[tb_老年人基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row老年人[tb_老年人基本信息.医疗费支付类型] = GetFlowLayoutResult(flow医疗费用支付方式);
                        row老年人[tb_老年人基本信息.新农合号] = this.txt新农合号.Text.Trim();
                        row老年人[tb_老年人基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                        row老年人[tb_老年人基本信息.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row老年人[tb_老年人基本信息.修改时间] = _serverDateTime;
                        row老年人[tb_老年人基本信息.修改人] = Loginer.CurrentUser.用户编码;
                        row老年人[tb_老年人基本信息.档案状态] = this.radio档案状态.EditValue;
                        row老年人[tb_老年人基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                        //row老年人[tb_老年人基本信息.所属机构] = ControlsHelper.GetComboxKey(this.cbo与户主关系);

                        //row老年人.SetModified();
                    }
                }
                #endregion
                #region 妇女
                if (Age >= 15 && ControlsHelper.GetComboxKey(cbo性别) == "2")
                {
                    if (_ds健康档案.Tables[tb_妇女基本信息.__TableName].Rows.Count == 0)//不存在妇女信息
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.妇女保健检查] = "未建";
                        DataRow row妇女 = _ds健康档案.Tables[tb_妇女基本信息.__TableName].Rows.Add();
                        row妇女[tb_妇女基本信息.个人档案编号] = "";
                        //row老年人[tb_老年人基本信息.卡号] = "";
                        row妇女[tb_妇女基本信息.家庭档案编号] = "";
                        row妇女[tb_妇女基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                        row妇女[tb_妇女基本信息.身份证号] = this.txt证件编号.Text.Trim();
                        row妇女[tb_妇女基本信息.工作单位] = this.txt工作单位.Text.Trim();
                        row妇女[tb_妇女基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                        row妇女[tb_妇女基本信息.省] = "37";
                        row妇女[tb_妇女基本信息.市] = this.cbo居住地址_市.EditValue;
                        row妇女[tb_妇女基本信息.区] = this.cbo居住地址_县.EditValue;
                        row妇女[tb_妇女基本信息.街道] = this.cbo居住地址_街道.EditValue;
                        row妇女[tb_妇女基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                        row妇女[tb_妇女基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row妇女[tb_妇女基本信息.新农合号] = this.txt医疗保险号.Text.Trim();
                        row妇女[tb_妇女基本信息.所属片区] = this.txt所属片区.Text.Trim();
                        row妇女[tb_妇女基本信息.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                        row妇女[tb_妇女基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row妇女[tb_妇女基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row妇女[tb_妇女基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row妇女[tb_妇女基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row妇女[tb_妇女基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row妇女[tb_妇女基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row妇女[tb_妇女基本信息.医疗费用支付方式] = GetFlowLayoutResult(flow医疗费用支付方式);
                        row妇女[tb_妇女基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                        row妇女[tb_妇女基本信息.新农合号] = this.txt新农合号.Text.Trim();
                        row妇女[tb_妇女基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                        row妇女[tb_妇女基本信息.建档时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row妇女[tb_妇女基本信息.创建时间] = this.dte录入时间.Text;
                        row妇女[tb_妇女基本信息.修改时间] = this.dte最近更新时间.Text;
                        row妇女[tb_妇女基本信息.创建人] = Loginer.CurrentUser.用户编码;
                        row妇女[tb_妇女基本信息.修改人] = Loginer.CurrentUser.用户编码;
                        row妇女[tb_妇女基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                        row妇女[tb_妇女基本信息.档案状态] = this.radio档案状态.EditValue;
                        row妇女[tb_妇女基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                    }
                    else
                    {
                        DataRow row妇女 = _ds健康档案.Tables[tb_妇女基本信息.__TableName].Rows[0];
                        row妇女[tb_妇女基本信息.个人档案编号] = _docNo;
                        //row老年人[tb_老年人基本信息.卡号] = "";
                        row妇女[tb_妇女基本信息.家庭档案编号] = _frm._familyDocNo;
                        row妇女[tb_妇女基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                        row妇女[tb_妇女基本信息.身份证号] = this.txt证件编号.Text.Trim();
                        row妇女[tb_妇女基本信息.工作单位] = this.txt工作单位.Text.Trim();
                        row妇女[tb_妇女基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                        row妇女[tb_妇女基本信息.省] = "37";
                        row妇女[tb_妇女基本信息.市] = this.cbo居住地址_市.EditValue;
                        row妇女[tb_妇女基本信息.区] = this.cbo居住地址_县.EditValue;
                        row妇女[tb_妇女基本信息.街道] = this.cbo居住地址_街道.EditValue;
                        row妇女[tb_妇女基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                        row妇女[tb_妇女基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row妇女[tb_妇女基本信息.新农合号] = this.txt医疗保险号.Text.Trim();
                        row妇女[tb_妇女基本信息.所属片区] = this.txt所属片区.Text.Trim();
                        row妇女[tb_妇女基本信息.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                        row妇女[tb_妇女基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row妇女[tb_妇女基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row妇女[tb_妇女基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row妇女[tb_妇女基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row妇女[tb_妇女基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row妇女[tb_妇女基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row妇女[tb_妇女基本信息.医疗费用支付方式] = GetFlowLayoutResult(flow医疗费用支付方式);
                        row妇女[tb_妇女基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                        row妇女[tb_妇女基本信息.新农合号] = this.txt新农合号.Text.Trim();
                        row妇女[tb_妇女基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                        row妇女[tb_妇女基本信息.建档时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row妇女[tb_妇女基本信息.修改时间] = this.dte最近更新时间.Text;
                        row妇女[tb_妇女基本信息.修改人] = Loginer.CurrentUser.用户编码;
                        row妇女[tb_妇女基本信息.档案状态] = this.radio档案状态.EditValue;
                        row妇女[tb_妇女基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                    }
                }
                #endregion
                #region 孕妇基本信息表保存
                if (isYunFu)//是孕妇
                {
                    if (_ds健康档案.Tables[tb_孕妇基本信息.__TableName].Rows.Count == 0)//不存在妇女信息
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.孕产妇基本信息表] = "12,0";
                        DataRow row孕妇 = _ds健康档案.Tables[tb_孕妇基本信息.__TableName].Rows.Add();
                        row孕妇[tb_孕妇基本信息.个人档案编号] = "";
                        //row老年人[tb_老年人基本信息.卡号] = "";
                        row孕妇[tb_孕妇基本信息.家庭档案编号] = "";
                        row孕妇[tb_孕妇基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(), false);
                        row孕妇[tb_孕妇基本信息.身份证号] = this.txt证件编号.Text.Trim();
                        row孕妇[tb_孕妇基本信息.工作单位] = this.txt工作单位.Text.Trim();
                        row孕妇[tb_孕妇基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                        row孕妇[tb_孕妇基本信息.省] = "37";
                        row孕妇[tb_孕妇基本信息.市] = this.cbo居住地址_市.EditValue;
                        row孕妇[tb_孕妇基本信息.区] = this.cbo居住地址_县.EditValue;
                        row孕妇[tb_孕妇基本信息.街道] = this.cbo居住地址_街道.EditValue;
                        row孕妇[tb_孕妇基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                        row孕妇[tb_孕妇基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row孕妇[tb_孕妇基本信息.新农合号] = this.txt医疗保险号.Text.Trim();
                        row孕妇[tb_孕妇基本信息.所属片区] = this.txt所属片区.Text.Trim();
                        row孕妇[tb_孕妇基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row孕妇[tb_孕妇基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row孕妇[tb_孕妇基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row孕妇[tb_孕妇基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row孕妇[tb_孕妇基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row孕妇[tb_孕妇基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row孕妇[tb_孕妇基本信息.医疗费用支付方式] = GetFlowLayoutResult(flow医疗费用支付方式);
                        row孕妇[tb_孕妇基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                        row孕妇[tb_孕妇基本信息.新农合号] = this.txt新农合号.Text.Trim();
                        row孕妇[tb_孕妇基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                        row孕妇[tb_孕妇基本信息.建档时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row孕妇[tb_孕妇基本信息.创建时间] = this.dte录入时间.Text;
                        row孕妇[tb_孕妇基本信息.修改时间] = this.dte最近更新时间.Text;
                        row孕妇[tb_孕妇基本信息.创建人] = Loginer.CurrentUser.用户编码;
                        row孕妇[tb_孕妇基本信息.修改人] = Loginer.CurrentUser.用户编码;
                        row孕妇[tb_孕妇基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                        row孕妇[tb_孕妇基本信息.档案状态] = this.radio档案状态.EditValue;
                        row孕妇[tb_孕妇基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                        row孕妇[tb_孕妇基本信息.缺项] = "12";
                        row孕妇[tb_孕妇基本信息.完整度] = "0";
                        row孕妇[tb_孕妇基本信息.下次随访时间] = "1900-01-01";
                        row孕妇[tb_孕妇基本信息.孕次] = this.cbo孕次.EditValue.ToString();
                        row孕妇[tb_孕妇基本信息.产次] = this.cbo产次.EditValue.ToString();
                    }
                    else
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.孕产妇基本信息表] = "12,0";
                        DataRow row孕妇 = _ds健康档案.Tables[tb_孕妇基本信息.__TableName].Rows[0];
                        row孕妇[tb_孕妇基本信息.个人档案编号] = _docNo;
                        //row老年人[tb_老年人基本信息.卡号] = "";
                        row孕妇[tb_孕妇基本信息.家庭档案编号] = _frm._familyDocNo;
                        row孕妇[tb_孕妇基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(), false);
                        row孕妇[tb_孕妇基本信息.身份证号] = this.txt证件编号.Text.Trim();
                        row孕妇[tb_孕妇基本信息.工作单位] = this.txt工作单位.Text.Trim();
                        row孕妇[tb_孕妇基本信息.联系电话] = this.txt联系人电话.Text.Trim();
                        row孕妇[tb_孕妇基本信息.省] = "37";
                        row孕妇[tb_孕妇基本信息.市] = this.cbo居住地址_市.EditValue;
                        row孕妇[tb_孕妇基本信息.区] = this.cbo居住地址_县.EditValue;
                        row孕妇[tb_孕妇基本信息.街道] = this.cbo居住地址_街道.EditValue;
                        row孕妇[tb_孕妇基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                        row孕妇[tb_孕妇基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row孕妇[tb_孕妇基本信息.新农合号] = this.txt医疗保险号.Text.Trim();
                        row孕妇[tb_孕妇基本信息.所属片区] = this.txt所属片区.Text.Trim();
                        row孕妇[tb_孕妇基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row孕妇[tb_孕妇基本信息.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row孕妇[tb_孕妇基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row孕妇[tb_孕妇基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row孕妇[tb_孕妇基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row孕妇[tb_孕妇基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row孕妇[tb_孕妇基本信息.医疗费用支付方式] = GetFlowLayoutResult(flow医疗费用支付方式);
                        row孕妇[tb_孕妇基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                        row孕妇[tb_孕妇基本信息.新农合号] = this.txt新农合号.Text.Trim();
                        row孕妇[tb_孕妇基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                        row孕妇[tb_孕妇基本信息.修改时间] = this.dte最近更新时间.Text;
                        row孕妇[tb_孕妇基本信息.修改人] = Loginer.CurrentUser.用户编码;
                        row孕妇[tb_孕妇基本信息.档案状态] = this.radio档案状态.EditValue;
                        row孕妇[tb_孕妇基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                        row孕妇[tb_孕妇基本信息.孕次] = this.cbo孕次.EditValue.ToString();
                        row孕妇[tb_孕妇基本信息.产次] = this.cbo产次.EditValue.ToString();

                    }
                }
                else
                {
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.孕产妇基本信息表] = DBNull.Value;
                }
                #endregion
                #region 疾病  高血压 糖尿病 脑卒中 冠心病  精神病 等
                if (ywjws == "1")//存在疾病
                {
                    DataRow[] rows = _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Select(" 疾病类型 = '疾病'");
                    for (int j = 0; j < rows.Length; j++)
                    {
                        string jibingmingcheng = rows[j]["疾病名称"].ToString();
                        string[] a = jibingmingcheng.Split(',');
                        for (int k = 0; k < a.Length; k++)
                        {
                            #region 高血压

                            if (a[k].Trim() == "2")//高血压
                            {
                                gxy = true;

                            }
                            #endregion
                            #region 糖尿病

                            if (a[k].Trim() == "3")//糖尿病
                            {
                                tnb = true;

                            }
                            #endregion
                            #region 精神病

                            if (a[k].Trim() == "8")//精神病
                            {
                                jsb = true;
                                ////TODO: 设置糖尿病管理卡的值 , 暂时先从 [T_BS_KHX考核项] 统计一下 赋值 
                                //_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡] = string.Format("{0},{1}", 26, "0");
                                //_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病随访表] = "未建";
                                //_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否糖尿病] = "1";
                            }
                            #endregion
                            #region 脑卒中

                            if ((a[k].Trim() == "7"))//脑卒中
                            {
                                nzz = true;

                            }
                            #endregion
                            #region 冠心病

                            if ((a[k].Trim() == "4"))//冠心病
                            {
                                gxb = true;
                            }
                            #endregion
                            #region 肿瘤

                            if ((a[k].Trim() == "6"))//肿瘤
                            {
                                zl = true;
                                _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否肿瘤] = "1";
                            }
                            #endregion
                            #region 慢阻肺

                            if ((a[k].Trim() == "5"))//慢阻肺
                            {
                                mz = true;
                                _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否慢阻肺] = "1";
                            }
                            #endregion
                        }
                    }
                }
                #region tb_MXB慢病基础信息表

                if ((gxy) || (tnb) || (nzz) || (gxb))
                {
                    if (_ds健康档案.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Count == 0)
                    {
                        DataRow row = _ds健康档案.Tables[tb_MXB慢病基础信息表.__TableName].Rows.Add();
                        //row[tb_MXB慢病基础信息表.户主关系] = ControlsHelper.GetComboxKey(cbo与户主关系);
                        row[tb_MXB慢病基础信息表.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                        row[tb_MXB慢病基础信息表.拼音简码] = util.DESEncrypt.GetChineseSpell(this.txt姓名.Text.Trim());
                        row[tb_MXB慢病基础信息表.身份证号] = this.txt证件编号.Text.Trim();
                        row[tb_MXB慢病基础信息表.出生日期] = string.IsNullOrEmpty(this.dte出生日期.Text) ? "" : this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row[tb_MXB慢病基础信息表.民族] = ControlsHelper.GetComboxKey(this.cbo民族);//.SelectedIndex;
                        row[tb_MXB慢病基础信息表.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);//.SelectedIndex;
                        row[tb_MXB慢病基础信息表.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);//.SelectedIndex;
                        row[tb_MXB慢病基础信息表.工作单位] = this.txt工作单位.Text.Trim();
                        row[tb_MXB慢病基础信息表.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);//.SelectedIndex;
                        row[tb_MXB慢病基础信息表.职业] = ControlsHelper.GetComboxKey(this.cbo职业);//.SelectedIndex;
                        row[tb_MXB慢病基础信息表.联系人电话] = this.txt联系人电话.Text.Trim();
                        row[tb_MXB慢病基础信息表.省] = "37";
                        row[tb_MXB慢病基础信息表.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row[tb_MXB慢病基础信息表.市] = this.cbo居住地址_市.EditValue;
                        row[tb_MXB慢病基础信息表.区] = this.cbo居住地址_县.EditValue;
                        row[tb_MXB慢病基础信息表.街道] = this.cbo居住地址_街道.EditValue;
                        row[tb_MXB慢病基础信息表.居委会] = this.cbo居住地址_村委会.EditValue;
                        row[tb_MXB慢病基础信息表.所属片区] = this.txt所属片区.Text.Trim();
                        row[tb_MXB慢病基础信息表.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row[tb_MXB慢病基础信息表.医疗费用支付方式] = Get医疗费用支付方式();
                        //row[tb_MXB慢病基础信息表.医疗费用支付类型其他] = this.txt医疗费用支付方式_其他.Text.Trim();
                        row[tb_MXB慢病基础信息表.医疗保险号] = this.txt医疗保险号.Text.Trim();
                        row[tb_MXB慢病基础信息表.新农合号] = this.txt新农合号.Text.Trim();
                        row[tb_MXB慢病基础信息表.建档机构] = Loginer.CurrentUser.所属机构;
                        row[tb_MXB慢病基础信息表.建档时间] = _serverDateTime;
                        row[tb_MXB慢病基础信息表.建档人] = Loginer.CurrentUser.用户编码;
                        row[tb_MXB慢病基础信息表.创建机构] = Loginer.CurrentUser.所属机构;
                        row[tb_MXB慢病基础信息表.创建时间] = _serverDateTime;
                        row[tb_MXB慢病基础信息表.创建人] = Loginer.CurrentUser.用户编码;
                        row[tb_MXB慢病基础信息表.更新时间] = _serverDateTime;
                        row[tb_MXB慢病基础信息表.更新人] = Loginer.CurrentUser.用户编码;
                        row[tb_MXB慢病基础信息表.HAPPENTIME] = _serverDateTime.Substring(0, 10);
                        row[tb_MXB慢病基础信息表.所属机构] = Loginer.CurrentUser.所属机构;
                        row[tb_MXB慢病基础信息表.高血压随访时间] = "";
                        row[tb_MXB慢病基础信息表.糖尿病随访时间] = "1900-01-01";
                        row[tb_MXB慢病基础信息表.脑卒中随访时间] = "1900-01-01";
                        row[tb_MXB慢病基础信息表.冠心病随访时间] = "1900-01-01";
                        row[tb_MXB慢病基础信息表.家庭档案编号] = _frm._familyDocNo;


                    }
                }
                #endregion
                #region 保存高血压管理卡表

                if (gxy) //保存高血压管理卡表
                {
                    if (_ds健康档案.Tables[tb_MXB高血压管理卡.__TableName].Rows.Count == 0)
                    {
                        DataRow row高血压 = _ds健康档案.Tables[tb_MXB高血压管理卡.__TableName].Rows.Add();
                        row高血压[tb_MXB高血压管理卡.管理卡编号] = "";
                        row高血压[tb_MXB高血压管理卡.创建机构] = Loginer.CurrentUser.所属机构;
                        row高血压[tb_MXB高血压管理卡.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row高血压[tb_MXB高血压管理卡.创建时间] = _serverDateTime;
                        row高血压[tb_MXB高血压管理卡.创建人] = Loginer.CurrentUser.用户编码;
                        row高血压[tb_MXB高血压管理卡.所属机构] = Loginer.CurrentUser.所属机构;
                        row高血压[tb_MXB高血压管理卡.修改人] = Loginer.CurrentUser.用户编码;
                        row高血压[tb_MXB高血压管理卡.修改时间] = _serverDateTime;
                        row高血压[tb_MXB高血压管理卡.缺项] = "20";
                        row高血压[tb_MXB高血压管理卡.完整度] = "0";

                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压管理卡] = string.Format("{0},{1}", "21", "0");
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "1";
                    }
                    else
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压管理卡] = string.Format("{0},{1}", _ds健康档案.Tables[tb_MXB高血压管理卡.__TableName].Rows[0][tb_MXB高血压管理卡.缺项], _ds健康档案.Tables[tb_MXB高血压管理卡.__TableName].Rows[0][tb_MXB高血压管理卡.完整度]);
                        //_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "1";
                    }
                }
                else
                {
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压管理卡] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否高血压] = "";
                }
                #endregion
                #region 保存糖尿病管理卡表

                if (tnb) //保存糖尿病管理卡表
                {
                    if (_ds健康档案.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Count == 0)
                    {
                        DataRow row糖尿病 = _ds健康档案.Tables[tb_MXB糖尿病管理卡.__TableName].Rows.Add();
                        row糖尿病[tb_MXB糖尿病管理卡.管理卡编号] = "";
                        row糖尿病[tb_MXB糖尿病管理卡.创建机构] = Loginer.CurrentUser.所属机构;
                        row糖尿病[tb_MXB糖尿病管理卡.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row糖尿病[tb_MXB糖尿病管理卡.创建时间] = _serverDateTime;
                        row糖尿病[tb_MXB糖尿病管理卡.创建人] = Loginer.CurrentUser.用户编码;
                        row糖尿病[tb_MXB糖尿病管理卡.所属机构] = Loginer.CurrentUser.所属机构;
                        row糖尿病[tb_MXB糖尿病管理卡.修改人] = Loginer.CurrentUser.用户编码;
                        row糖尿病[tb_MXB糖尿病管理卡.修改时间] = _serverDateTime;
                        row糖尿病[tb_MXB糖尿病管理卡.缺项] = "26";
                        row糖尿病[tb_MXB糖尿病管理卡.完整度] = "0";

                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡] = string.Format("{0},{1}", "26", "0");
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病随访表] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否糖尿病] = "1";
                    }
                    else
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡] = string.Format("{0},{1}", _ds健康档案.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0][tb_MXB糖尿病管理卡.缺项], _ds健康档案.Tables[tb_MXB糖尿病管理卡.__TableName].Rows[0][tb_MXB糖尿病管理卡.完整度]);
                        //_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否糖尿病] = "1";
                    }
                }
                else
                {
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病管理卡] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.糖尿病随访表] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否糖尿病] = "";
                }
                #endregion
                #region 保存脑卒中管理卡表

                if (nzz) //保存脑卒中管理卡表
                {
                    if (_ds健康档案.Tables[tb_MXB脑卒中管理卡.__TableName].Rows.Count == 0)
                    {
                        DataRow row脑卒中 = _ds健康档案.Tables[tb_MXB脑卒中管理卡.__TableName].Rows.Add();
                        row脑卒中[tb_MXB脑卒中管理卡.管理卡编号] = "";
                        row脑卒中[tb_MXB脑卒中管理卡.创建机构] = Loginer.CurrentUser.所属机构;
                        row脑卒中[tb_MXB脑卒中管理卡.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row脑卒中[tb_MXB脑卒中管理卡.创建时间] = _serverDateTime;
                        row脑卒中[tb_MXB脑卒中管理卡.创建人] = Loginer.CurrentUser.用户编码;
                        row脑卒中[tb_MXB脑卒中管理卡.所属机构] = Loginer.CurrentUser.所属机构;
                        row脑卒中[tb_MXB脑卒中管理卡.修改人] = Loginer.CurrentUser.用户编码;
                        row脑卒中[tb_MXB脑卒中管理卡.修改时间] = _serverDateTime;
                        row脑卒中[tb_MXB脑卒中管理卡.缺项] = "17";
                        row脑卒中[tb_MXB脑卒中管理卡.完整度] = "0";

                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.脑卒中管理卡] = string.Format("{0},{1}", "17", "0");
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.脑卒中随访表] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否脑卒中] = "1";
                    }
                    else
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.脑卒中管理卡] = string.Format("{0},{1}", _ds健康档案.Tables[tb_MXB脑卒中管理卡.__TableName].Rows[0][tb_MXB脑卒中管理卡.缺项], _ds健康档案.Tables[tb_MXB脑卒中管理卡.__TableName].Rows[0][tb_MXB脑卒中管理卡.完整度]);
                        //_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否脑卒中] = "1";
                    }
                }
                else
                {
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.脑卒中管理卡] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.脑卒中随访表] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否脑卒中] = "";
                }
                #endregion
                #region 保存冠心病管理卡表
                if (gxb) //保存冠心病管理卡表
                {
                    if (_ds健康档案.Tables[tb_MXB冠心病管理卡.__TableName].Rows.Count == 0)
                    {

                        DataRow row冠心病 = _ds健康档案.Tables[tb_MXB冠心病管理卡.__TableName].Rows.Add();

                        row冠心病[tb_MXB冠心病管理卡.管理卡编号] = "";
                        row冠心病[tb_MXB冠心病管理卡.创建机构] = Loginer.CurrentUser.所属机构;
                        row冠心病[tb_MXB冠心病管理卡.发生时间] = string.IsNullOrEmpty(this.dte调查时间.Text) ? "" : this.dte调查时间.DateTime.ToString("yyyy-MM-dd");
                        row冠心病[tb_MXB冠心病管理卡.创建时间] = this.dte录入时间.Text.Trim();
                        row冠心病[tb_MXB冠心病管理卡.创建人] = Loginer.CurrentUser.用户编码;
                        row冠心病[tb_MXB冠心病管理卡.所属机构] = Loginer.CurrentUser.所属机构;
                        row冠心病[tb_MXB冠心病管理卡.修改人] = Loginer.CurrentUser.用户编码;
                        row冠心病[tb_MXB冠心病管理卡.修改时间] = this.dte录入时间.Text.Trim();
                        row冠心病[tb_MXB冠心病管理卡.缺项] = "17";
                        row冠心病[tb_MXB冠心病管理卡.完整度] = "0";

                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.冠心病管理卡] = string.Format("{0},{1}", "17", "0");
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.冠心病随访表] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否冠心病] = "1";
                    }
                    else
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.冠心病管理卡] = string.Format("{0},{1}", _ds健康档案.Tables[tb_MXB冠心病管理卡.__TableName].Rows[0][tb_MXB冠心病管理卡.缺项], _ds健康档案.Tables[tb_MXB冠心病管理卡.__TableName].Rows[0][tb_MXB冠心病管理卡.完整度]);
                        //_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.高血压随访表] = "未建";
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否冠心病] = "1";
                    }
                }
                else
                {
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.冠心病管理卡] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.冠心病随访表] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.是否冠心病] = "";
                }


                #endregion
                #region tb_精神疾病信息补充表
                if (jsb)//  tb_精神疾病信息补充表
                {
                    if (_ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows.Count == 0)
                    {
                        DataRow row精神病 = _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows.Add();
                        row精神病[tb_精神疾病信息补充表.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(), false);
                        row精神病[tb_精神疾病信息补充表.与患者关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                        row精神病[tb_精神疾病信息补充表.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row精神病[tb_精神疾病信息补充表.身份证号] = this.txt证件编号.Text.Trim();
                        row精神病[tb_精神疾病信息补充表.出生日期] = this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row精神病[tb_精神疾病信息补充表.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row精神病[tb_精神疾病信息补充表.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row精神病[tb_精神疾病信息补充表.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);
                        row精神病[tb_精神疾病信息补充表.工作地址] = this.txt工作单位.Text.Trim();
                        row精神病[tb_精神疾病信息补充表.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row精神病[tb_精神疾病信息补充表.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row精神病[tb_精神疾病信息补充表.联系电话] = this.txt本人电话.Text.Trim();
                        row精神病[tb_精神疾病信息补充表.省] = "37";
                        row精神病[tb_精神疾病信息补充表.市] = this.cbo居住地址_市.EditValue;
                        row精神病[tb_精神疾病信息补充表.区] = this.cbo居住地址_县.EditValue;
                        row精神病[tb_精神疾病信息补充表.街道] = this.cbo居住地址_街道.EditValue;
                        row精神病[tb_精神疾病信息补充表.居委会] = this.cbo居住地址_村委会.EditValue;
                        row精神病[tb_精神疾病信息补充表.所属片区] = this.txt所属片区.Text.Trim();
                        row精神病[tb_精神疾病信息补充表.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row精神病[tb_精神疾病信息补充表.医疗支付类型] = Get医疗费用支付方式();
                        row精神病[tb_精神疾病信息补充表.新农合号] = this.txt新农合号.Text.Trim();
                        row精神病[tb_精神疾病信息补充表.创建机构] = Loginer.CurrentUser.所属机构;
                        row精神病[tb_精神疾病信息补充表.创建时间] = this.dte录入时间.Text.Trim();
                        row精神病[tb_精神疾病信息补充表.创建人] = Loginer.CurrentUser.用户编码;
                        row精神病[tb_精神疾病信息补充表.修改时间] = this.dte录入时间.Text.Trim();
                        row精神病[tb_精神疾病信息补充表.修改人] = Loginer.CurrentUser.用户编码;
                        row精神病[tb_精神疾病信息补充表.检查日期] = this.dte录入时间.Text.Trim();
                        row精神病[tb_精神疾病信息补充表.所属机构] = Loginer.CurrentUser.所属机构;
                        row精神病[tb_精神疾病信息补充表.档案状态] = this.radio档案状态.EditValue.ToString();
                        row精神病[tb_精神疾病信息补充表.完整度] = "11";
                        row精神病[tb_精神疾病信息补充表.缺项] = "16";
                        //jsbinfo.setdJtdabh(tdaJkdaRkxzl.getdJtdabh());

                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = string.Format("{0},{1}", "16", "11");
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                    }
                    #region (未用)
                    //else if (_ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows.Count == 1)
                    //{
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.与患者关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.身份证号] = this.txt证件编号.Text.Trim();
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.出生日期] = this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.工作地址] = this.txt工作单位.Text.Trim();
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.联系电话] = this.txt本人电话.Text.Trim();
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.省] = "37";
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.市] = this.cbo居住地址_市.EditValue;
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.区] = this.cbo居住地址_县.EditValue;
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.街道] = this.cbo居住地址_街道.EditValue;
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.居委会] = this.cbo居住地址_村委会.EditValue;
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.所属片区] = this.txt所属片区.Text.Trim();
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.医疗支付类型] = Get医疗费用支付方式();
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.新农合号] = this.txt新农合号.Text.Trim();
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.创建机构] = Loginer.CurrentUser.所属机构;
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.创建时间] = this.dte录入时间.Text.Trim();
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.创建人] = Loginer.CurrentUser.用户编码;
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.修改时间] = this.dte录入时间.Text.Trim();
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.修改人] = Loginer.CurrentUser.用户编码;
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.检查日期] = this.dte录入时间.Text.Trim();
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.所属机构] = Loginer.CurrentUser.所属机构;
                    //    _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.档案状态] = this.radio档案状态.EditValue.ToString();

                    //    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = string.Format("{0},{1}", "16", "11");
                    //    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                    //}
                    //}
                    #endregion
                    else
                    {
                        _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = string.Format("{0},{1}", _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.缺项], _ds健康档案.Tables[tb_精神疾病信息补充表.__TableName].Rows[0][tb_精神疾病信息补充表.完整度]);
                        //_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = "";
                    }
                }
                else
                {
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = "";
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = "";
                }
                #endregion
                #region 残疾人基本信息表
                if (this.cbo残疾情况.Text == "有")
                {
                    if (_ds健康档案.Tables[tb_残疾人_基本信息.__TableName].Rows.Count == 0)
                    {
                        DataRow row残疾人 = _ds健康档案.Tables[tb_残疾人_基本信息.__TableName].Rows.Add();
                        row残疾人[tb_残疾人_基本信息.个人档案编号] = _docNo;//添加个人档案编号
                        row残疾人[tb_残疾人_基本信息.姓名] = util.DESEncrypt.DES加密(this.txt姓名.Text.Trim(),false);
                        row残疾人[tb_残疾人_基本信息.与户主关系] = ControlsHelper.GetComboxKey(this.cbo与户主关系);
                        row残疾人[tb_残疾人_基本信息.性别] = ControlsHelper.GetComboxKey(this.cbo性别);
                        row残疾人[tb_残疾人_基本信息.身份证号] = this.txt证件编号.Text.Trim();
                        row残疾人[tb_残疾人_基本信息.出生日期] = this.dte出生日期.DateTime.ToString("yyyy-MM-dd");
                        row残疾人[tb_残疾人_基本信息.民族] = ControlsHelper.GetComboxKey(this.cbo民族);
                        row残疾人[tb_残疾人_基本信息.文化程度] = ControlsHelper.GetComboxKey(this.cbo文化程度);
                        row残疾人[tb_残疾人_基本信息.常住类型] = ControlsHelper.GetComboxKey(this.cbo常住类型);
                        row残疾人[tb_残疾人_基本信息.工作地址] = this.txt工作单位.Text.Trim();
                        row残疾人[tb_残疾人_基本信息.婚姻状况] = ControlsHelper.GetComboxKey(this.cbo婚姻状况);
                        row残疾人[tb_残疾人_基本信息.职业] = ControlsHelper.GetComboxKey(this.cbo职业);
                        row残疾人[tb_残疾人_基本信息.联系电话] = this.txt本人电话.Text.Trim();
                        row残疾人[tb_残疾人_基本信息.省] = "37";
                        row残疾人[tb_残疾人_基本信息.市] = this.cbo居住地址_市.EditValue;
                        row残疾人[tb_残疾人_基本信息.区] = this.cbo居住地址_县.EditValue;
                        row残疾人[tb_残疾人_基本信息.街道] = this.cbo居住地址_街道.EditValue;
                        row残疾人[tb_残疾人_基本信息.居委会] = this.cbo居住地址_村委会.EditValue;
                        row残疾人[tb_残疾人_基本信息.所属片区] = this.txt所属片区.Text.Trim();
                        row残疾人[tb_残疾人_基本信息.居住地址] = this.txt居住地址_详细地址.Text.Trim();
                        row残疾人[tb_残疾人_基本信息.医疗费用支付类型] = Get医疗费用支付方式();
                        row残疾人[tb_残疾人_基本信息.医疗保险号] = this.txt医疗保险号.Text.Trim();
                        row残疾人[tb_残疾人_基本信息.新农合号] = this.txt新农合号.Text.Trim();
                        row残疾人[tb_残疾人_基本信息.创建机构] = Loginer.CurrentUser.所属机构;
                        row残疾人[tb_残疾人_基本信息.创建时间] = this.dte录入时间.Text.Trim();
                        row残疾人[tb_残疾人_基本信息.创建人] = Loginer.CurrentUser.用户编码;
                        row残疾人[tb_残疾人_基本信息.修改时间] = this.dte录入时间.Text.Trim();
                        row残疾人[tb_残疾人_基本信息.修改人] = Loginer.CurrentUser.用户编码;
                        row残疾人[tb_残疾人_基本信息.发生时间] = this.dte录入时间.Text.Trim();
                        row残疾人[tb_残疾人_基本信息.所属机构] = Loginer.CurrentUser.所属机构;
                        row残疾人[tb_残疾人_基本信息.档案状态] = this.radio档案状态.EditValue.ToString();
                        if (this.chk残疾情况_听力残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表] = "未建";
                            row残疾人[tb_残疾人_基本信息.听力残下次随访时间] = "";
                        }
                        if (this.chk残疾情况_言语残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表] = "未建";
                            row残疾人[tb_残疾人_基本信息.听力残下次随访时间] = "";
                        }
                        if (this.chk残疾情况_肢体残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.肢体残疾随访表] = "未建";
                            row残疾人[tb_残疾人_基本信息.肢体残下次随访时间] = "";
                        }
                        if (this.chk残疾情况_智力残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.智力残疾随访表] = "未建";
                            row残疾人[tb_残疾人_基本信息.智力残下次随访时间] = "";
                        }
                        if (this.chk残疾情况_视力残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.视力残疾随访表] = "未建";
                            row残疾人[tb_残疾人_基本信息.视力残下次随访时间] = "";
                        }
                        if (this.chk残疾情况_精神残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = "未建";
                            row残疾人[tb_残疾人_基本信息.精神残下次随访时间] = "";
                        }
                        //row残疾人[tb_CJR_残疾人基本信息.完整度] = "11";
                        //row残疾人[tb_CJR_残疾人基本信息.缺项] = "16";
                        //jsbinfo.setdJtdabh(tdaJkdaRkxzl.getdJtdabh());
                        //if (chk残疾情况_精神残.Checked)
                        //{
                        //    //_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = string.Format("{0},{1}", "16", "11");
                        //    //_ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病随访表] = "未建";
                        //}
                    }
                    else
                    {
                        if (this.chk残疾情况_听力残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表] = "未建";
                        }
                        else if (this.chk残疾情况_言语残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表] = "未建";
                        }
                        else
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表] = DBNull.Value;
                        }
                        if (this.chk残疾情况_肢体残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.肢体残疾随访表] = "未建";
                        }
                        else
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.肢体残疾随访表] = DBNull.Value;
                        }
                        if (this.chk残疾情况_智力残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.智力残疾随访表] = "未建";
                        }
                        else
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.智力残疾随访表] = DBNull.Value;
                        }
                        if (this.chk残疾情况_视力残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.视力残疾随访表] = "未建";
                        }
                        else
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.视力残疾随访表] = DBNull.Value;
                        }
                        if (this.chk残疾情况_精神残.Checked)
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = "未建";
                        }
                        else
                        {
                            _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.精神疾病信息补充表] = DBNull.Value;
                        }
                    }
                }
                else
                {
                    if (_ds健康档案.Tables[tb_残疾人_基本信息.__TableName].Rows.Count == 1)
                    {
                        _ds健康档案.Tables[tb_残疾人_基本信息.__TableName].Rows[0].Delete();
                    }
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.听力言语残疾随访表] = DBNull.Value;
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.肢体残疾随访表] = DBNull.Value;
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.智力残疾随访表] = DBNull.Value;
                    _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.视力残疾随访表] = DBNull.Value;
                }
                #endregion

                #endregion
                #region 既往病史保存
                if (_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Count > 0)
                {
                    for (int k = 0; k < _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Count; k++)
                    {
                        if (_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k].RowState != DataRowState.Deleted && _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期] != null &&
                            !string.IsNullOrEmpty(_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期].ToString()))
                        {
                            DateTime date;
                            if (DateTime.TryParse(_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期].ToString(), out date))
                            {
                                if (_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.疾病类型].ToString() == "疾病")
                                {
                                    _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期] = date.ToString("yyyy-MM");
                                }
                                else
                                {
                                    _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期] = date.ToString("yyyy-MM-dd");
                                }
                                //_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows[k][tb_健康档案_既往病史.日期] = date.ToString("yyyy-MM-dd");
                            }
                        }

                    }
                }
                #endregion
                #region  家庭情况保存
                 _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.户主姓名] = this.txt户主姓名.Text.Trim();
                 _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.户主身份证号] = this.txt户主身份证号.Text.Trim();
                if (Age >= 65)
                {
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住情况] = Get居住情况();
                }
                else
                {
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.居住情况] = "";
                }
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.家庭人口数] = this.txt家庭人口数.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.家庭结构] = this.txt家庭结构.Text.Trim();
                #endregion

                //_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人或家属签字] = this.textEdit本人或家属签字.Text.Trim();
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.签字时间] = this.dateEdit签字时间.Text.Trim();
                if (b手签更新)
                {
                    using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
                    {
                        this.textEdit本人或家属签字.Image.Save(ms1, System.Drawing.Imaging.ImageFormat.Jpeg);
                        byte[] arr1 = new byte[ms1.Length];
                        ms1.Position = 0;
                        ms1.Read(arr1, 0, (int)ms1.Length);
                        ms1.Close();
                        _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人或家属签字] = Convert.ToBase64String(arr1);
                    }
                    //_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人或家属签字] = this.textEdit本人或家属签字.Text.Trim();
                    _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.签字时间] = _Bll.ServiceDateTime;
                }
                
                _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.个人基本信息表] = string.Format("{0},{1}", i, Get完整度(i));
                _ds健康档案.Tables[tb_健康档案_个人健康特征.__TableName].Rows[0][tb_健康档案_个人健康特征.与户主关系] = ControlsHelper.GetComboxKey(cbo与户主关系);
                _Bll.WriteLog();
                SaveResult result;
                if (_UpdateType == UpdateType.Add)//此用户不再任何的家庭中
                {
                    DataSet ds = _Bll.CreateSaveData(_ds健康档案, _UpdateType);
                    result = _Bll.Save(ds);
                    _frm.ReLoad(this.Name, this._docNo, result.GUID);
                }
                else
                {
                    DataSet ds = _Bll.CreateOnlyOneData(_ds健康档案, _UpdateType);
                    result = _Bll.SaveOneData(ds);
                    //写入变更日志-2017年9月1日15点44分
                    _Bll.WriteLog(_Old健康档案, ds.Tables[tb_健康档案.__TableName]);
                    _frm.ReLoad(this.Name, result.DocNo);
                }
                //_frm.ReLoad(this.Name, this._docNo);

                

            }
        }
        private void chk医疗费用支付方式_其他_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk医疗费用支付方式_其他.Checked)
            {
                this.txt医疗费用支付方式_其他.Enabled = true;
            }
            else
            {
                this.txt医疗费用支付方式_其他.Enabled = false;
                this.txt医疗费用支付方式_其他.Text = "";
            }
            //this.txt医疗费用支付方式_其他.Enabled = this.chk医疗费用支付方式_其他.Checked;
        }
        private void chk过敏史_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt过敏史_其他.Enabled = this.chk过敏史_其他.Checked;
        }
        private void chk残疾情况_其他残疾_CheckedChanged(object sender, EventArgs e)
        {
            this.txt残疾情况_其他残疾.Enabled = this.chk残疾情况_其他残疾.Checked;
        }
        private void cbo暴露史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbo暴露史.Text.Trim();
            if (text == "有")
            {
                this.txt暴露史_毒物.Enabled = true;
                this.txt暴露史_化学品.Enabled = true;
                this.txt暴露史_射线.Enabled = true;
            }
            else
            {
                this.txt暴露史_毒物.Enabled = false;
                this.txt暴露史_化学品.Enabled = false;
                this.txt暴露史_射线.Enabled = false;
            }
        }
        private void cbo家族史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbo家族史.Text.Trim();
            if (text == "有")
            {
                this.chk家族史_恶性肿瘤.Enabled = true;
                this.chk家族史_肝炎.Enabled = true;
                this.chk家族史_高血压.Enabled = true;
                this.chk家族史_冠心病.Enabled = true;
                this.chk家族史_结核病.Enabled = true;
                this.chk家族史_慢性阻塞性肺疾病.Enabled = true;
                this.chk家族史_脑卒中.Enabled = true;
                this.chk家族史_其他.Enabled = true;
                this.chk家族史_糖尿病.Enabled = true;
                this.chk家族史_先天畸形.Enabled = true;
                this.chk家族史_重性精神疾病.Enabled = true;
                this.cbo家族史关系.Enabled = true;

                this.btn家族史_添加.Enabled = true;
            }
            else
            {
                this.chk家族史_恶性肿瘤.Enabled = false;
                this.chk家族史_肝炎.Enabled = false;
                this.chk家族史_高血压.Enabled = false;
                this.chk家族史_冠心病.Enabled = false;
                this.chk家族史_结核病.Enabled = false;
                this.chk家族史_慢性阻塞性肺疾病.Enabled = false;
                this.chk家族史_脑卒中.Enabled = false;
                this.chk家族史_其他.Enabled = false;
                this.chk家族史_糖尿病.Enabled = false;
                this.chk家族史_先天畸形.Enabled = false;
                this.chk家族史_重性精神疾病.Enabled = false;
                this.txt家族史_其他.Enabled = false;
                this.cbo家族史关系.Enabled = false;
                this.btn家族史_添加.Enabled = false;

            }
        }
        private void cbo遗传病史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbo遗传病史.Text.Trim();
            if (text == "有")
            {
                this.txt遗传病史_疾病名称.Enabled = true;
            }
            else
            {
                this.txt遗传病史_疾病名称.Enabled = false;
            }
        }
        private void cbo残疾情况_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbo残疾情况.Text.Trim();
            if (text == "有")
            {
                this.chk残疾情况_精神残.Enabled = true;
                this.chk残疾情况_其他残疾.Enabled = true;
                this.chk残疾情况_视力残.Enabled = true;
                this.chk残疾情况_听力残.Enabled = true;
                this.chk残疾情况_言语残.Enabled = true;
                this.chk残疾情况_肢体残.Enabled = true;
                this.chk残疾情况_智力残.Enabled = true;
                this.txt残疾情况_其他残疾.Enabled = true;
            }
            else
            {
                this.chk残疾情况_精神残.Enabled = false;
                this.chk残疾情况_精神残.Checked = false;
                this.chk残疾情况_视力残.Enabled = false;
                this.chk残疾情况_视力残.Checked = false;
                this.chk残疾情况_听力残.Enabled = false;
                this.chk残疾情况_听力残.Checked = false;
                this.chk残疾情况_言语残.Enabled = false;
                this.chk残疾情况_言语残.Checked = false;
                this.chk残疾情况_肢体残.Enabled = false;
                this.chk残疾情况_肢体残.Checked = false;
                this.chk残疾情况_智力残.Enabled = false;
                this.chk残疾情况_智力残.Checked = false;
                this.txt残疾情况_其他残疾.Enabled = false;
                this.chk残疾情况_其他残疾.Checked = false;
                this.chk残疾情况_其他残疾.Enabled = false;
            }
        }
        private void chk厨房排风设施_无_CheckedChanged(object sender, EventArgs e)
        {
            this.chk厨房排风设施_换气扇.Enabled = this.chk厨房排风设施_油烟机.Enabled = this.chk厨房排风设施_烟囱.Enabled = !chk厨房排风设施_无.Checked;
            if (chk厨房排风设施_无.Checked)
                this.chk厨房排风设施_换气扇.Checked = this.chk厨房排风设施_油烟机.Checked = this.chk厨房排风设施_烟囱.Checked = false;

        }
        private void chk疾病_恶性肿瘤_CheckedChanged(object sender, EventArgs e)
        {
            this.txt疾病_恶性肿瘤.Enabled = this.chk疾病_恶性肿瘤.Checked;
        }
        private void chk家族史_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt家族史_其他.Enabled = this.chk家族史_其他.Checked;
        }
        private void chk疾病_职业病_CheckedChanged(object sender, EventArgs e)
        {
            this.txt疾病_职业病.Enabled = this.chk疾病_职业病.Checked;
        }
        private void chk疾病_其他_CheckedChanged(object sender, EventArgs e)
        {
            this.txt疾病_其他.Enabled = this.chk疾病_其他.Checked;

        }
        private void cbo过敏史_SelectedIndexChanged(object sender, EventArgs e)
        {
            string text = this.cbo过敏史.Text.Trim();
            if (text == "有")
            {
                this.chk过敏史_磺胺.Enabled = true;
                this.chk过敏史_链霉素.Enabled = true;
                this.chk过敏史_青霉素.Enabled = true;
                this.chk过敏史_其他.Enabled = true;
                this.chk过敏史_不详.Enabled = true;
                this.txt过敏史_其他.Enabled = true;
            }
            else
            {
                this.chk过敏史_磺胺.Enabled = false;
                this.chk过敏史_链霉素.Enabled = false;
                this.chk过敏史_青霉素.Enabled = false;
                this.chk过敏史_其他.Enabled = false;
                this.chk过敏史_不详.Enabled = false;
                this.txt过敏史_其他.Enabled = false;
            }
        }
        private void radio疾病_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio疾病.EditValue == null ? "" : this.radio疾病.EditValue.ToString();
            if (string.IsNullOrEmpty(index)) return;
            this.chk疾病_高血压.Enabled = this.chk疾病_恶性肿瘤.Enabled = this.chk疾病_肝炎.Enabled = this.chk疾病_肝炎.Enabled = this.chk疾病_冠心病.Enabled = this.chk疾病_结核病.Enabled = this.chk疾病_慢性阻塞性肺病.Enabled = this.chk疾病_脑卒中.Enabled = this.chk疾病_其他.Enabled = this.chk疾病_其他法定传染病.Enabled = this.chk疾病_糖尿病.Enabled = this.chk疾病_职业病.Enabled = this.chk疾病_重性精神疾病.Enabled = this.chk疾病_恶性肿瘤.Enabled = this.dte疾病_确诊时间.Enabled = this.btn疾病_添加.Enabled = index == "2" ? false : true;
        }
        private void radio手术_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio手术.EditValue == null ? "" : this.radio手术.EditValue.ToString();
            this.txt手术名称.Enabled = this.dte手术时间.Enabled = this.btn手术_添加.Enabled = index == "2" ? false : true;
        }
        private void radio外伤_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio外伤.EditValue.ToString();
            this.txt外伤名称.Enabled = this.dte外伤时间.Enabled = this.btn外伤_添加.Enabled = index == "2" ? false : true;
        }
        private void radio输血_SelectedIndexChanged(object sender, EventArgs e)
        {
            string index = this.radio输血.EditValue.ToString();
            this.txt输血原因.Enabled = this.dte输血时间.Enabled = this.btn输血_添加.Enabled = index == "2" ? false : true;
        }
        private void btn疾病_添加_Click(object sender, EventArgs e)
        {
            Set既往史_疾病();

        }
        private void btn手术_添加_Click(object sender, EventArgs e)
        {
            Set既往史_手术();
        }
        private void btn外伤_添加_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(dte外伤时间.Text.Trim()))
            {
                Msg.ShowInformation("请填写 确认时间！");
                this.dte外伤时间.Focus();
                return;
            }
            DataRow dr = _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
            dr[tb_健康档案_既往病史.疾病名称] = this.txt外伤名称.Text.Trim();
            dr[tb_健康档案_既往病史.D_JBBM] = this.txt外伤名称.Text.Trim();

            dr[tb_健康档案_既往病史.日期] = this.dte外伤时间.DateTime.ToString("yyyy-MM-dd");
            dr[tb_健康档案_既往病史.疾病类型] = "外伤";

            this.txt外伤名称.Text = string.Empty;
            this.dte外伤时间.Text = string.Empty;
            this.gc既往史.RefreshDataSource();
        }
        private void btn输血_添加_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(dte输血时间.Text.Trim()))
            {
                Msg.ShowInformation("请填写 确认时间！");
                this.dte输血时间.Focus();
                return;
            }
            DataRow dr = _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
            dr[tb_健康档案_既往病史.疾病名称] = this.txt输血原因.Text.Trim();
            dr[tb_健康档案_既往病史.D_JBBM] = this.txt输血原因.Text.Trim();

            dr[tb_健康档案_既往病史.日期] = this.dte输血时间.DateTime.ToString("yyyy-MM-dd");
            dr[tb_健康档案_既往病史.疾病类型] = "输血";

            this.txt输血原因.Text = string.Empty;
            this.dte输血时间.Text = string.Empty;
            this.gc既往史.RefreshDataSource();
        }
        #region 地区联动
        void cbo居住地址_街道_EditValueChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            try
            {
                DataView dv县 = new DataView(DataDictCache.Cache.t地区信息);
                dv县.RowFilter = "上级编码=" + cbo居住地址_街道.EditValue;
                DataBinder.BindingLookupEditDataSource(cbo居住地址_村委会, dv县.ToTable(), "地区名称", "地区编码");
            }
            catch
            {
            }

        }

        void cbo居住地址_县_EditValueChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            try
            {
                DataView dv县 = new DataView(DataDictCache.Cache.t地区信息);
                dv县.RowFilter = "上级编码=" + cbo居住地址_县.EditValue;
                DataBinder.BindingLookupEditDataSource(cbo居住地址_街道, dv县.ToTable(), "地区名称", "地区编码");
            }
            catch
            {
            }

        }

        void cbo居住地址_市_EditValueChanged(object sender, EventArgs e)
        {
            //throw new NotImplementedException();                             
            try
            {
                DataView dv县 = new DataView(DataDictCache.Cache.t地区信息);
                dv县.RowFilter = "上级编码=" + cbo居住地址_市.EditValue;
                DataBinder.BindingLookupEditDataSource(cbo居住地址_县, dv县.ToTable(), "地区名称", "地区编码");
            }
            catch
            {
            }

        }

        #endregion
        private void btn家族史_添加_Click(object sender, EventArgs e)
        {
            Set家族病史();
        }
        private void btn重复档案检测_Click(object sender, EventArgs e)
        {
            string sql = string.Empty;
            if (string.IsNullOrEmpty(this.txt姓名.Text.Trim()))
            {
                Msg.Warning("姓名不能为空！");
                this.txt姓名.Focus();
                return;
            }
            else
            {
                sql += " AND 姓名 = '" + this.txt姓名.Text.Trim() + "'";

            }
            if (string.IsNullOrEmpty(this.cbo性别.Text.Trim()) || this.cbo性别.Text.Trim() == "请选择")
            {
                Msg.Warning("性别不能为空！");
                this.cbo性别.Focus();
                return;
            }
            else
            {
                sql += " AND 性别 = '" + ControlsHelper.GetComboxKey(this.cbo性别) + "'";

            }
            if (string.IsNullOrEmpty(this.dte出生日期.Text.Trim()))
            {
                Msg.Warning("出生日期不能为空！");
                this.dte出生日期.Focus();
                return;
            }
            else
            {
                sql += " AND 出生日期 = '" + this.dte出生日期.DateTime.ToString("yyyy-MM-dd") + "'";
            }

            DataTable dt重复档案 = _Bll.GetSummaryByParam(sql);
            if (dt重复档案.Rows.Count == 0)
            {
                Msg.ShowInformation("没有检测到重复档案！");
            }
            else
            {
                frm重复档案检测 frm = new frm重复档案检测(dt重复档案);
                DialogResult result = frm.ShowDialog();
            }
        }

        private void menuItem删除选中项_Click(object sender, EventArgs e)
        {

            DevExpress.XtraGrid.GridControl control = (DevExpress.XtraGrid.GridControl)this.contextMenuStrip1.SourceControl;
            if (control == null) return;
            control.BeginUpdate();
            DevExpress.XtraGrid.Views.Grid.GridView view = control.MainView as DevExpress.XtraGrid.Views.Grid.GridView;
            if (view == null) return;
            view.DeleteSelectedRows();
            control.EndUpdate();


        }
        /// <summary>
        /// 检查身份证号是否已经注册
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt证件编号_Leave(object sender, EventArgs e)
        {

            string id = this.txt证件编号.Text.Trim();
            if (string.IsNullOrEmpty(id)) return;
            //Begin WXF 2018-12-07 | 16:14
            //验证身份证 
            //AnalysisIDCard isid = new AnalysisIDCard(id);
            if ((!Regex.IsMatch(txt证件编号.Text, @"^(^\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$", RegexOptions.IgnoreCase)))
            //if (!isid.info.是否有效)
            //End
            {
                Msg.Warning("请输入正确的身份证号码！\n" );
                this.txt证件编号.Text = "";
                this.txt证件编号.Focus();
                return;

            }
            if (_Bll.CheckNoExists(id))
            {
                DataTable dt = _Bll.GetBusinessByID(id);
                Msg.ShowInformation("此身份证号已经注册，不能再次注册！" + Environment.NewLine +
                    "居住地址为：" + _Bll.Return地区名称(dt.Rows[0][tb_健康档案.区].ToString()) + _Bll.Return地区名称(dt.Rows[0][tb_健康档案.街道].ToString()) + _Bll.Return地区名称(dt.Rows[0][tb_健康档案.居委会].ToString()) + dt.Rows[0][tb_健康档案.居住地址].ToString());
                this.txt证件编号.Text = "";
                this.txt证件编号.Focus();
                return;
            }
            else
            {
                //Begin WXF 2018-12-07 | 16:16
                //调整身份证信息解析方法 
                if (id.Length == 18)
                {
                    string sf = id.Substring(6, 8);
                    string ddd = sf.Substring(0, 4) + "-" + sf.Substring(4, 2) + "-" + sf.Substring(6, 2);
                    this.dte出生日期.Text = ddd;

                    string xingbieindex = id.Substring(16, 1);
                    int index = 0;
                    if (Int32.TryParse(xingbieindex, out index))
                    {
                        if (index % 2 == 1)//奇数，代表男
                        {
                            util.ControlsHelper.SetComboxData("1", cbo性别);
                        }
                        else if (index % 2 == 0)//偶数，代表女
                        {
                            util.ControlsHelper.SetComboxData("2", cbo性别);
                        }
                    }
                }
                //if (isid.info.是否有效)
                //{
                //    this.dte出生日期.Text = isid.info.出生日期.ToString("yyyy-MM-dd");
                //    if ("男".Equals(isid.info.性别))
                //    {
                //        util.ControlsHelper.SetComboxData("1", cbo性别);
                //    }
                //    if ("女".Equals(isid.info.性别))
                //    {
                //        util.ControlsHelper.SetComboxData("2", cbo性别);
                //    }
                //}
                //End

                return;
            }
        }
        private void chk孕产1_CheckedChanged(object sender, EventArgs e)
        {
            yccpd(chk孕产1, "0");
        }
        private void chk孕产2_CheckedChanged(object sender, EventArgs e)
        {
            yccpd(chk孕产2, "1");
        }
        private void chk孕产3_CheckedChanged(object sender, EventArgs e)
        {
            yccpd(chk孕产3, "2");
        }
        private void chk孕产4_CheckedChanged(object sender, EventArgs e)
        {
            yccpd(chk孕产4, "2");
        }
        private void cbo性别_SelectedIndexChanged(object sender, EventArgs e)
        {
            Show孕产情况();
        }
        private void dte出生日期_EditValueChanged(object sender, EventArgs e)
        {
            Show孕产情况();
        }
        #endregion

        #region Private Methods
        private int GetAge(String strBirthday)
        {
            int returnAge = 0;
            if (string.IsNullOrEmpty(strBirthday)) return returnAge;

            String[] strBirthdayArr = strBirthday.Split('-');
            int birthYear = Int32.Parse(strBirthdayArr[0]);
            int birthMonth = Int32.Parse(strBirthdayArr[1]);
            int birthDay = Int32.Parse(strBirthdayArr[2]);

            String date = _Bll.ServiceDateTime;
            int nowYear = Int32.Parse(date.Substring(0, 4));
            int nowMonth = Int32.Parse(date.Substring(5, 2));
            int nowDay = Int32.Parse(date.Substring(8, 2));
            if (nowYear == birthYear)
            {
                returnAge = 0;
            }
            else
            {
                int ageDiff = nowYear - birthYear;
                if (ageDiff > 0)
                {
                    if (nowMonth == birthMonth)
                    {
                        int dayDiff = nowDay - birthDay;
                        if (dayDiff < 0)
                            returnAge = ageDiff - 1;
                        else
                            returnAge = ageDiff;
                    }
                    else
                    {
                        int monthDiff = nowMonth - birthMonth;
                        if (monthDiff < 0)
                            returnAge = ageDiff - 1;
                        else
                            returnAge = ageDiff;
                    }
                }
                else
                {
                    returnAge = -1;
                }
            }
            return returnAge;
        }
        /// <summary>
        /// 绑定主表控件内容
        /// </summary>
        /// <param name="dataSource"></param>
        void DoBindingSummaryEditor(DataSet dataSource)
        {

            if (dataSource == null) return;
            DataTable _tb健康档案 = dataSource.Tables[tb_健康档案.__TableName];
            DataTable _tb健康状态 = dataSource.Tables[tb_健康档案_健康状态.__TableName];
            DataTable _tb既往史 = dataSource.Tables[tb_健康档案_既往病史.__TableName];
            DataTable _tb家族病史 = dataSource.Tables[tb_健康档案_家族病史.__TableName];

            //DataBinder.BindingTextEdit(cbo与户主关系, _tb健康档案, tb_健康档案.户主关系);
            //DataBinder.BindingTextEdit(txt姓名, _tb健康档案, tb_健康档案.姓名);

            if (_UpdateType == UpdateType.AddPeople)
            {
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建时间] = _serverDateTime;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建人] = Loginer.CurrentUser.用户编码;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改人] = Loginer.CurrentUser.用户编码;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改时间] = _serverDateTime;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建机构] = Loginer.CurrentUser.所属机构;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构] = Loginer.CurrentUser.所属机构;
                this.cbo与户主关系.SelectedIndex = 1;

                if (_户主档案 != null)//如果户主档案不为空，则根据户主档案来填写默认的一些基本信息
                {
                    this.cbo常住类型.Text = _户主档案[tb_健康档案.常住类型].ToString();
                    this.cbo民族.Text = _户主档案[tb_健康档案.民族].ToString();
                    this.cbo档案类别.Text = _户主档案[tb_健康档案.档案类别].ToString();
                    //util.ControlsHelper.SetComboxData(_户主档案[tb_健康档案.常住类型].ToString(), cbo常住类型);
                    //util.ControlsHelper.SetComboxData(_户主档案[tb_健康档案.民族].ToString(), cbo民族);
                    util.ControlsHelper.SetComboxData("5", cbo血型);//不详
                    util.ControlsHelper.SetComboxData("3", cboRH阴性);//不详



                    this.cbo居住地址_街道.Text = _户主档案[tb_健康档案.街道].ToString();
                    this.cbo居住地址_村委会.Text = _户主档案[tb_健康档案.居委会].ToString();
                    this.txt居住地址_详细地址.Text = _户主档案[tb_健康档案.居住地址].ToString();
                    //util.ControlsHelper.SetComboxData(_户主档案[tb_健康档案.档案类别].ToString(), cbo档案类别);

                }


            }
            else if (_UpdateType == UpdateType.Modify || _UpdateType == UpdateType.Add)
            {
                DataBinder.BindingTextEdit(cbo居住地址_市, _tb健康档案, tb_健康档案.市);
                DataBinder.BindingTextEdit(cbo居住地址_县, _tb健康档案, tb_健康档案.区);
                DataBinder.BindingTextEdit(cbo居住地址_街道, _tb健康档案, tb_健康档案.街道);
                DataBinder.BindingTextEdit(cbo居住地址_村委会, _tb健康档案, tb_健康档案.居委会);
                if (_tb健康档案.Rows.Count == 1)
                {
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.与户主关系]), cbo与户主关系);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.性别]), cbo性别);
                    this.radio档案状态.EditValue = Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.档案状态]);
                    this.txt姓名.Text = _tb健康档案.Rows[0][tb_健康档案.姓名] == null ? "" : util.DESEncrypt.DES解密(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.姓名]));
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.证件类型]), cbo证件类型);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.劳动强度]), cbo劳动程度);
                    this.chk是否流动人口.Checked = string.IsNullOrEmpty(_tb健康档案.Rows[0][tb_健康档案.是否流动人口].ToString()) ? false : true;
                    this.chk是否贫困人口.Checked = string.IsNullOrEmpty(_tb健康档案.Rows[0][tb_健康档案.是否贫困人口].ToString()) ? false : true;
                    this.chk是否签约.Checked = string.IsNullOrEmpty(_tb健康档案.Rows[0][tb_健康档案.是否签约].ToString()) ? false : true;
                    this.txt证件编号.Text = _tb健康档案.Rows[0][tb_健康档案.身份证号] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.身份证号]);
                    this.dte出生日期.Text = _tb健康档案.Rows[0][tb_健康档案.出生日期] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.出生日期]);
                    this.txt本人电话.Text = _tb健康档案.Rows[0][tb_健康档案.本人电话] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.本人电话]);
                    this.txt工作单位.Text = _tb健康档案.Rows[0][tb_健康档案.工作单位] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.工作单位]);
                    this.txt联系人电话.Text = _tb健康档案.Rows[0][tb_健康档案.联系人电话] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.联系人电话]);
                    this.txt联系人姓名.Text = _tb健康档案.Rows[0][tb_健康档案.联系人姓名] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.联系人姓名]);
                    this.txt档案位置.Text = _tb健康档案.Rows[0][tb_健康档案.档案位置] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.档案位置]);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.常住类型]), cbo常住类型);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.民族]), cbo民族);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.职业]), cbo职业);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.文化程度]), cbo文化程度);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.婚姻状况]), cbo婚姻状况);
                    SetFlowLayoutResult(_tb健康档案.Rows[0][tb_健康档案.医疗费支付类型].ToString(), flow医疗费用支付方式);
                    //this.txt医疗保险号.Text = _tb健康档案.Rows[0][tb_健康档案.医疗保险号] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.医疗保险号]);
                    this.txt医疗费用支付方式_其他.Text = _tb健康档案.Rows[0][tb_健康档案.医疗费用支付类型其他] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.医疗费用支付类型其他]);
                    
                    #region  新版本添加
                    this.textEdit职工医疗保险卡号.Text = _tb健康档案.Rows[0][tb_健康档案.医疗保险号] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.医疗保险号]);
                    this.textEdit居民医疗保险卡号.Text = _tb健康档案.Rows[0][tb_健康档案.新农合号] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.新农合号]);
                    //this.textEdit职工医疗保险卡号.Text = _tb健康档案.Rows[0][tb_健康档案.职工医疗保险卡号] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.职工医疗保险卡号]);
                    //this.textEdit居民医疗保险卡号.Text = _tb健康档案.Rows[0][tb_健康档案.居民医疗保险卡号] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.居民医疗保险卡号]);
                    this.textEdit贫困救助卡号.Text = _tb健康档案.Rows[0][tb_健康档案.贫困救助卡号] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.贫困救助卡号]);
                    //this.txt新农合号.Text = _tb健康档案.Rows[0][tb_健康档案.新农合号] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.新农合号]);
                    this.txt居住地址_详细地址.Text = _tb健康档案.Rows[0][tb_健康档案.居住地址] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.居住地址]);
                    //ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.所属片区]), cbo所属片区);
                    this.txt所属片区.Text = _tb健康档案.Rows[0][tb_健康档案.所属片区] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.所属片区]);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.档案类别]), cbo档案类别);

                    string str与户主关系 = this.cbo与户主关系.Text.Trim();
                    if (!string.IsNullOrEmpty(str与户主关系) && str与户主关系 == "户主")
                    {
                        this.txt户主姓名.Text = util.DESEncrypt.DES解密(_tb健康档案.Rows[0][tb_健康档案.姓名].ToString());
                        this.txt户主身份证号.Text = _tb健康档案.Rows[0][tb_健康档案.身份证号].ToString();
                    }
                    else
                    {
                        this.txt户主姓名.Text = _tb健康档案.Rows[0][tb_健康档案.户主姓名].ToString();
                        this.txt户主身份证号.Text = _tb健康档案.Rows[0][tb_健康档案.户主身份证号].ToString();
                    }
                    this.txt家庭人口数.Text = _tb健康档案.Rows[0][tb_健康档案.家庭人口数].ToString();
                    this.txt家庭结构.Text = _tb健康档案.Rows[0][tb_健康档案.家庭结构].ToString();
                    SetFlowLayoutResult(_tb健康档案.Rows[0][tb_健康档案.居住情况].ToString(), flow居住情况);
                    //this.textEdit本人或家属签字.Text = _tb健康档案.Rows[0][tb_健康档案.本人或家属签字].ToString();

                    string SQ = _tb健康档案.Rows[0][tb_健康档案.本人或家属签字].ToString();
                    if (!string.IsNullOrEmpty(SQ) && SQ.Length > 64)
                    {
                        Byte[] bitmapData = new Byte[SQ.Length];
                        bitmapData = Convert.FromBase64String(SQ);
                        using (System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData))
                        {
                            this.textEdit本人或家属签字.Image = Image.FromStream(streamBitmap);
                            streamBitmap.Close();
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(SQ))
                            SQ = "暂无电子签名";
                        Graphics g = Graphics.FromImage(new Bitmap(1, 1));
                        Font font = new Font("宋体", 9);
                        SizeF sizeF = g.MeasureString(SQ, font); //测量出字体的高度和宽度  
                        Brush brush; //笔刷，颜色  
                        brush = Brushes.Black;
                        PointF pf = new PointF(0, 0);
                        Bitmap img = new Bitmap(Convert.ToInt32(sizeF.Width), Convert.ToInt32(sizeF.Height));
                        g = Graphics.FromImage(img);
                        g.DrawString(SQ, font, brush, pf);
                        this.textEdit本人或家属签字.Image = img;
                    }

                    if (string.IsNullOrEmpty(_tb健康档案.Rows[0][tb_健康档案.本人或家属签字].ToString()))
                    {
                        lbl本人或家属签字.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                    }

                    this.dateEdit签字时间.Text = _tb健康档案.Rows[0][tb_健康档案.签字时间].ToString() == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.签字时间].ToString());

                    this.txt复核备注.Text = _tb健康档案.Rows[0][tb_健康档案.复核备注].ToString();
                    if (string.IsNullOrEmpty(_tb健康档案.Rows[0][tb_健康档案.复核标记].ToString()))
                        this.ch复核标记.Checked = false;
                    else
                        this.ch复核标记.Checked = true;
                    if (_tb健康档案.Rows[0][tb_健康档案.二次复核].Equals("1"))
                        this.ch二次复核.Checked = true;
                    else
                        this.ch二次复核.Checked = false;
                    #endregion

                    SetFlowLayoutResult(_tb健康档案.Rows[0][tb_健康档案.厨房排气设施].ToString(), flow厨房排风设施);
                    SetFlowLayoutResult(_tb健康档案.Rows[0][tb_健康档案.燃料类型].ToString(), flow燃料类型);
                    SetFlowLayoutResult(_tb健康档案.Rows[0][tb_健康档案.饮水].ToString(), flow饮水);
                    SetFlowLayoutResult(_tb健康档案.Rows[0][tb_健康档案.厕所].ToString(), flow厕所);
                    SetFlowLayoutResult(_tb健康档案.Rows[0][tb_健康档案.禽畜栏].ToString(), flow禽畜栏);
                    SetFlowLayoutResult(_tb健康档案.Rows[0][tb_健康档案.外出情况].ToString(), flow外出);
                    this.txt外出地址.Text = _tb健康档案.Rows[0][tb_健康档案.外出地址] == null ? "" : Convert.ToString(_tb健康档案.Rows[0][tb_健康档案.外出地址]);
                    //SetFlowLayoutResult(_tb健康档案.Rows[0][tb_健康档案.怀孕情况].ToString(), flowLayoutPanel1);
                    string huaiyun = _tb健康档案.Rows[0][tb_健康档案.怀孕情况].ToString();
                    if (!string.IsNullOrEmpty(huaiyun))
                    {
                        if (huaiyun == "未孕")
                        {
                            this.chk孕产1.Checked = true;
                        }
                        else if (huaiyun == "已孕未生产")
                        {
                            this.chk孕产2.Checked = true;
                        }
                        else if (huaiyun == "已生产随访期内")
                        {
                            this.chk孕产3.Checked = true;
                        }
                        else if (huaiyun == "已生产随访期外")
                        {
                            this.chk孕产4.Checked = true;
                        }
                    }
                    this.cbo孕次.Text = _tb健康档案.Rows[0][tb_健康档案.孕次].ToString();
                    this.cbo产次.Text = _tb健康档案.Rows[0][tb_健康档案.产次].ToString();
                    this.dte调查时间.Text = _tb健康档案.Rows[0][tb_健康档案.调查时间].ToString();
                }
                if (_tb健康状态.Rows.Count == 1)
                {
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.血型]), cbo血型);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.RH]), cboRH阴性);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.暴露史]), cbo暴露史);
                    this.txt暴露史_化学品.Text = _tb健康状态.Rows[0][tb_健康档案_健康状态.暴露史化学品] == null ? "" : Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.暴露史化学品]);
                    this.txt暴露史_毒物.Text = _tb健康状态.Rows[0][tb_健康档案_健康状态.暴露史毒物] == null ? "" : Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.暴露史毒物]);
                    this.txt暴露史_射线.Text = _tb健康状态.Rows[0][tb_健康档案_健康状态.暴露史射线] == null ? "" : Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.暴露史射线]);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.遗传病史有无]), cbo遗传病史);
                    this.txt遗传病史_疾病名称.Text = _tb健康状态.Rows[0][tb_健康档案_健康状态.遗传病史] == null ? "" : Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.遗传病史]);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.有无残疾]), cbo残疾情况);
                    SetFlowLayoutResult(_tb健康状态.Rows[0][tb_健康档案_健康状态.残疾情况].ToString(), flow残疾情况);
                    this.txt残疾情况_其他残疾.Text = _tb健康状态.Rows[0][tb_健康档案_健康状态.残疾其他] == null ? "" : Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.残疾其他]);
                    ControlsHelper.SetComboxData(Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.过敏史有无]), cbo过敏史);
                    SetFlowLayoutResult(_tb健康状态.Rows[0][tb_健康档案_健康状态.药物过敏史].ToString(), flow过敏史);
                    this.txt过敏史_其他.Text = _tb健康状态.Rows[0][tb_健康档案_健康状态.过敏史其他] == null ? "" : Convert.ToString(_tb健康状态.Rows[0][tb_健康档案_健康状态.过敏史其他]);
                }

                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改人] = Loginer.CurrentUser.用户编码;
                _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改时间] = _serverDateTime;

                cbo暴露史_SelectedIndexChanged(null, null);
                cbo残疾情况_SelectedIndexChanged(null, null);
                cbo过敏史_SelectedIndexChanged(null, null);
                cbo家族史_SelectedIndexChanged(null, null);
                cbo遗传病史_SelectedIndexChanged(null, null);
            }
            if (_tb既往史.Rows.Count > 0)
            {
                for (int i = 0; i < _tb既往史.Rows.Count; i++)
                {
                    string[] jibing = _tb既往史.Rows[i][tb_健康档案_既往病史.疾病名称].ToString().Split(',');
                    string 疾病名称 = string.Empty;
                    for (int j = 0; j < jibing.Length; j++)
                    {
                        if (string.IsNullOrEmpty(jibing[j])) continue;
                        疾病名称 += _Bll.ReturnDis字典显示("jb_gren", jibing[j]) + ", ";
                    }
                    _tb既往史.Rows[i][tb_健康档案_既往病史.D_JBBM] = 疾病名称;
                }
            }
            if (_tb家族病史.Rows.Count > 0)
            {
                for (int i = 0; i < _tb家族病史.Rows.Count; i++)
                {
                    string[] jibing = _tb家族病史.Rows[i][tb_健康档案_家族病史.家族病史].ToString().Split(',');
                    string 疾病名称 = string.Empty;
                    for (int j = 0; j < jibing.Length; j++)
                    {
                        疾病名称 += _Bll.ReturnDis字典显示("jb_gren", jibing[j]) + ", ";
                    }
                    _tb家族病史.Rows[i][tb_健康档案_家族病史.D_JBBM] = _Bll.ReturnDis字典显示("jzbmc", _tb家族病史.Rows[i][tb_健康档案_家族病史.家族病史].ToString());
                }
            }
            this.gc既往史.DataSource = _tb既往史;

            this.gc家族史.DataSource = _tb家族病史;
            if (_UpdateType == UpdateType.Add || _UpdateType == UpdateType.AddPeople)
            {
                this.dte录入时间.Text = _serverDateTime;
                this.dte最近更新时间.Text = _serverDateTime;
                this.txt录入人.Text = Loginer.CurrentUser.AccountName;
                this.txt最近更新人.Text = Loginer.CurrentUser.AccountName;
                this.txt当前所属机构.Text = Loginer.CurrentUser.所属机构名称;
            }
            else if (_UpdateType == UpdateType.Modify)
            {
                this.dte录入时间.Text = _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建时间].ToString();
                this.dte最近更新时间.Text = _ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改时间].ToString();
                this.txt录入人.Text = _bllUser.Return用户名称(_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.创建人].ToString()); //Loginer.CurrentUser.AccountName;
                this.txt最近更新人.Text = _bllUser.Return用户名称(_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.修改人].ToString());
                this.txt当前所属机构.Text = _Bll.Return机构名称(_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构].ToString()); //Loginer.CurrentUser.所属机构名称;
                //this.txt创建机构.Text = _bll机构信息.Return机构名称(_ds健康档案.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构].ToString()); //Loginer.CurrentUser.所属机构名称;
            }


        }

        //Begin WXF 2018-12-07 | 10:48
        //数据质控部分 
        //调整新增保存检查项
        //End
        private bool f保存检查()
        {
            //Begin WXF 2019-01-04 | 10:22
            //如果档案改为非活动,就没有判断各种项目的意义了 
            if (radio档案状态.EditValue.ToString() == "2")
            {
                return true;
            }
            //End

            if (this.cbo与户主关系.Text.Trim() == "请选择")
            {
                Msg.Warning("与户主关系不能为空！");
                this.cbo与户主关系.Focus();
                return false;
            }
            //如果存在户主了，则不能再新建户主
            if (_UpdateType == UpdateType.AddPeople && this.cbo与户主关系.Text.Trim() == "户主")
            {
                Msg.Warning("已存在户主，不能再创建户主信息！");
                this.cbo与户主关系.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.radio档案状态.Text.Trim()))
            {
                Msg.Warning("档案状态不能为空！");
                this.radio档案状态.Focus();
                return false;
            }
            if (txt姓名.Text == "")
            {
                Msg.Warning("姓名不能为空！");
                this.txt姓名.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.cbo性别.Text.Trim()))
            {
                Msg.Warning("性别不能为空！");
                this.cbo性别.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.dte出生日期.Text.Trim()))
            {
                Msg.Warning("出生日期不能为空！");
                this.dte出生日期.Focus();
                return false;
            }
            // wxf Ben
            // 按年推算年龄,故不用GetAge()方法
            int age = 0;
            DateTime birthday = default(DateTime);
            if (DateTime.TryParse(dte出生日期.Text.Trim(), out birthday))
            {
                DateTime ServerTime = DateTime.Parse(new bllCom().GetDateTimeFromDBServer());
                age = ServerTime.Year - birthday.Year;
            }
            else
            {
                Msg.Warning("出生日期格式不正确！");
                this.dte出生日期.Focus();
                return false;
            }
            if (age > 6)
            {
                if (string.IsNullOrEmpty(txt证件编号.Text.Trim()))
                {
                    Msg.Warning("证件编号不能为空！");
                    this.txt证件编号.Focus();
                    return false;
                }
                if (string.IsNullOrEmpty(txt工作单位.Text.Trim()))
                {
                    Msg.Warning("工作单位不能为空！");
                    this.txt工作单位.Focus();
                    return false;
                }
            }
            // wxf End
            if (string.IsNullOrEmpty(this.cbo民族.Text.Trim()))
            {
                Msg.Warning("民族不能为空！");
                this.cbo民族.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbo居住地址_街道.Text.Trim()))
            {
                Msg.Warning("街道/乡镇不能为空！");
                cbo居住地址_街道.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbo居住地址_村委会.Text.Trim()))
            {
                Msg.Warning("村委会不能为空！");
                cbo居住地址_村委会.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txt居住地址_详细地址.Text.Trim()))
            {
                Msg.Warning("详细地址不能为空！");
                txt居住地址_详细地址.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(cbo档案类别.Text.Trim()))
            {
                Msg.Warning("档案类别不能为空！");
                cbo档案类别.Focus();
                return false;
            }
            //判断是否填写本人电话或联系人电话（必填其一）
            if (string.IsNullOrEmpty(this.txt本人电话.Text.Trim()) && string.IsNullOrEmpty(this.txt联系人电话.Text.Trim()))
            {
                Msg.Warning("请填写本人电话或联系人电话！没有请写（无）");
                this.txt本人电话.Focus();
                return false;
            }
            if (this.txt本人电话.Text.Trim() != "无" && (this.txt本人电话.Text.Trim().Length != 7 && this.txt本人电话.Text.Trim().Length != 11))
            {
                Msg.Warning("请填写7位或者11位本人电话！没有请写（无）");
                this.txt本人电话.Focus();
                return false;
            }
            if (this.txt联系人电话.Text.Trim() != "无" && (this.txt联系人电话.Text.Trim().Length != 7 && this.txt联系人电话.Text.Trim().Length != 11))
            {
                Msg.Warning("请填写7位或者11位联系人电话！没有请写（无）");
                this.txt联系人电话.Focus();
                return false;
            }

            // wxf Ben
            if (string.IsNullOrEmpty(this.cbo常住类型.Text.Trim()))
            {
                Msg.Warning("常住类型不能为空！");
                this.cbo常住类型.Focus();
                return false;
            }
            if (this.cbo残疾情况.Text == "有")//存在残疾情况，至少需要选择一个残疾信息
            {
                if (string.IsNullOrEmpty(GetFlowLayoutResult(this.flow残疾情况).ToString()))
                {
                    Msg.Warning("有无残疾必须选择一个！");
                    this.cbo残疾情况.Focus();
                    return false;
                }
            }
            if (this.radio疾病.EditValue.ToString() == "1") //存在疾病情况
            {
                if (_ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Select("疾病类型='疾病'").Length <= 0)
                {
                    Msg.Warning("你选择了存在疾病，请至少选择一个疾病！");
                    this.radio疾病.Focus();
                    return false;
                }
            }
            if (this.cbo家族史.Text == "有") //存在疾病情况
            {
                if (_ds健康档案.Tables[tb_健康档案_家族病史.__TableName].Rows.Count <= 0)
                {
                    Msg.Warning("你选择了存在家族史，请至少选择一个疾病！");
                    this.cbo家族史.Focus();
                    return false;
                }
            }
            if (string.IsNullOrEmpty(txt户主姓名.Text.Trim()))
            {
                Msg.Warning("户主姓名不能为空！");
                txt户主姓名.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txt户主身份证号.Text.Trim()))
            {
                Msg.Warning("户主身份证号不能为空！");
                txt户主身份证号.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txt家庭人口数.Text.Trim()))
            {
                Msg.Warning("家庭人口数不能为空！");
                txt家庭人口数.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txt家庭结构.Text.Trim()))
            {
                Msg.Warning("家庭结构不能为空！");
                txt家庭结构.Focus();
                return false;
            }
            if (age >= 65)
            {
                if (string.IsNullOrEmpty(Get居住情况().ToString()))
                {
                    Msg.Warning("居住情况不能为空！");
                    flow居住情况.Focus();
                    return false;
                }
            }
            if (!string.IsNullOrEmpty(cbo档案类别.Text.Trim()) && cbo档案类别.Text.Equals("农村"))
            {
                if (string.IsNullOrEmpty(Get厨房排气设施().ToString()))
                {
                    Msg.Warning("农村地区厨房排气设施不能为空！");
                    flow厨房排风设施.Focus();
                    return false;
                }
                if (string.IsNullOrEmpty(Get燃料类型().ToString()))
                {
                    Msg.Warning("农村地区燃料类型不能为空！");
                    flow燃料类型.Focus();
                    return false;
                }
                if (string.IsNullOrEmpty(Get饮水().ToString()))
                {
                    Msg.Warning("农村地区饮水不能为空！");
                    flow饮水.Focus();
                    return false;
                }
                if (string.IsNullOrEmpty(Get厕所().ToString()))
                {
                    Msg.Warning("农村地区厕所不能为空！");
                    flow厕所.Focus();
                    return false;
                }
                if (string.IsNullOrEmpty(Get禽畜栏().ToString()))
                {
                    Msg.Warning("农村地区禽畜栏不能为空！");
                    flow禽畜栏.Focus();
                    return false;
                }
            }
            // wxf End
            if (string.IsNullOrEmpty(dte调查时间.Text.Trim()))
            {
                Msg.Warning("调查时间不能为空！");
                dte调查时间.Focus();
                return false;
            }

            #region   考核项判断（考核项必填，不填无法保存）

            //if(string.IsNullOrEmpty(this.txt证件编号.Text.Trim()))
            //{
            //    Msg.Warning("请填写证件编号！");
            //    this.txt证件编号.Focus();
            //    return false;
            //}
            if (string.IsNullOrEmpty(this.txt工作单位.Text.Trim()))
            {
                Msg.Warning("工作单位不能为空！没有请写（无）");
                this.txt工作单位.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.txt联系人姓名.Text.Trim()))
            {
                Msg.Warning("联系人姓名不能为空！没有请写（无）");
                this.txt联系人姓名.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.cbo职业.Text.Trim()))
            {
                Msg.Warning("职业不能为空！");
                this.cbo职业.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.cbo文化程度.Text.Trim()))
            {
                Msg.Warning("文化程度不能为空！");
                this.cbo文化程度.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(this.cbo婚姻状况.Text.Trim()))
            {
                Msg.Warning("婚姻状况不能为空！");
                this.cbo婚姻状况.Focus();
                return false;
            }
            #endregion
            return true;
        }
        #region 获取生活环境数据

        private object Get禽畜栏()
        {
            return GetFlowLayoutResult(flow禽畜栏);
        }



        private object Get厕所()
        {
            return GetFlowLayoutResult(flow厕所);
        }

        private object Get饮水()
        {
            return GetFlowLayoutResult(flow饮水);
        }

        private object Get燃料类型()
        {
            return GetFlowLayoutResult(flow燃料类型);
        }

        private object Get厨房排气设施()
        {
            return GetFlowLayoutResult(flow厨房排风设施);
        }
        #endregion
        private void Set既往史_疾病()
        {

            if (string.IsNullOrEmpty(dte疾病_确诊时间.Text.Trim()))
            {
                Msg.ShowInformation("请填写 确诊时间！");
                this.dte疾病_确诊时间.Focus();
                return;
            }
            /*
                    10001855	jb_gren	疾病	13	NULL	其他	NULL	1
                    10001856	jb_gren	疾病	2		高血压		1
                    10001857	jb_gren	疾病	3		糖尿病		1
                    10001858	jb_gren	疾病	4		冠心病		1
                    10001859	jb_gren	疾病	5		慢性阻塞性肺疾病		1
                    10001860	jb_gren	疾病	6		恶性肿瘤		1
                    10001861	jb_gren	疾病	7		脑卒中		1
                    10001862	jb_gren	疾病	8		重性精神疾病		1
                    10001863	jb_gren	疾病	9		结核病		1
                    10001864	jb_gren	疾病	10		肝炎		1
                    10001865	jb_gren	疾病	11		其他法定传染病		1
                    10001866	jb_gren	疾病	12		职业病		1
             */
            string 疾病名称 = string.Empty;
            string 疾病代码 = string.Empty;
            for (int i = 0; i < flow疾病.Controls.Count; i++)
            {
                if (flow疾病.Controls[i].GetType() == typeof(CheckEdit))//判断是否是复选框
                {
                    CheckEdit chk = (CheckEdit)flow疾病.Controls[i];
                    if (chk == null) continue;
                    if (chk.Tag == null) continue;
                    if (chk.Checked)
                    {
                        疾病名称 += chk.Tag + ",";
                        疾病代码 += chk.Text + ",";
                    }
                }
            }
            //if (!string.IsNullOrEmpty(疾病名称))
            //{
            //    疾病名称 = 疾病名称.Remove(疾病名称.Length - 1);
            //    疾病代码 = 疾病代码.Remove(疾病代码.Length - 1);
            //}

            DataRow dr = _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
            dr[tb_健康档案_既往病史.疾病名称] = 疾病名称;
            dr[tb_健康档案_既往病史.D_JBBM] = 疾病代码;
            dr[tb_健康档案_既往病史.恶性肿瘤] = this.chk疾病_恶性肿瘤.Checked ? this.txt疾病_恶性肿瘤.Text.Trim() : "";
            dr[tb_健康档案_既往病史.职业病其他] = this.chk疾病_职业病.Checked ? this.txt疾病_职业病.Text.Trim() : "";
            dr[tb_健康档案_既往病史.疾病其他] = this.chk疾病_其他.Checked ? this.txt疾病_其他.Text.Trim() : "";
            dr[tb_健康档案_既往病史.日期] = this.dte疾病_确诊时间.Text == "" ? "" : this.dte疾病_确诊时间.DateTime.ToString("yyyy-MM");
            dr[tb_健康档案_既往病史.疾病类型] = "疾病";
            //清空所有的数据
            for (int i = 0; i < flow疾病.Controls.Count; i++)
            {
                if (flow疾病.Controls[i].GetType() == typeof(CheckEdit))//判断是否是复选框
                {
                    CheckEdit chk = (CheckEdit)flow疾病.Controls[i];
                    chk.Checked = false;
                }
                if (flow疾病.Controls[i].GetType() == typeof(TextEdit))//判断是否是复选框
                {
                    TextEdit txt = (TextEdit)flow疾病.Controls[i];
                    txt.Text = string.Empty;
                }
                if (flow疾病.Controls[i].GetType() == typeof(DateEdit))//判断是否是复选框
                {
                    DateEdit dte = (DateEdit)flow疾病.Controls[i];
                    dte.Text = string.Empty;
                }
            }

            this.gc既往史.RefreshDataSource();
        }
        private void Set家族病史()
        {
            /*
             ID	P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC	P_BEIZHU	P_FLAG
                    10002050	jzsxin	家族史	1		高血压		1
                    10002051	jzsxin	家族史	2		糖尿病		1
                    10002052	jzsxin	家族史	3		冠心病		1
                    10002053	jzsxin	家族史	4		慢性阻塞性肺疾病		1
                    10002054	jzsxin	家族史	5		恶性肿瘤		1
                    10002055	jzsxin	家族史	6		脑卒中		1
                    10002056	jzsxin	家族史	7		重性精神疾病		1
                    10002057	jzsxin	家族史	8		结核病		1
                    10002058	jzsxin	家族史	9		肝炎		1
                    10002059	jzsxin	家族史	10		先天畸形		1
                    10002060	jzsxin	家族史	100		其他		1 
             * 
             * 
             P_FUN_CODE	P_FUN_DESC	P_CODE	P_DESC_CODE	P_DESC
                    jzscy	        家族史成员	1		（外）祖父
                    jzscy	        家族史成员	2		（外）祖母
                    jzscy	        家族史成员	3		父亲
                    jzscy	        家族史成员	4		母亲
                    jzscy	        家族史成员	5		兄弟姐妹
             */
            string 家族病史 = string.Empty;
            string 疾病代码 = string.Empty;
            string 家族关系 = string.Empty;
            for (int i = 0; i < flow家族史.Controls.Count; i++)
            {
                if (flow家族史.Controls[i].GetType() == typeof(CheckEdit))//判断是否是复选框
                {
                    CheckEdit chk = (CheckEdit)flow家族史.Controls[i];
                    if (chk == null) continue;
                    if (chk.Tag == null) continue;
                    if (chk.Checked)
                    {
                        家族病史 += chk.Tag + ",";
                        疾病代码 += chk.Text + ",";
                    }
                }
            }
            if (!string.IsNullOrEmpty(家族病史))
            {
                家族病史 = 家族病史.Remove(家族病史.Length - 1);
                疾病代码 = 疾病代码.Remove(疾病代码.Length - 1);
            }

            DataRow dr = _ds健康档案.Tables[tb_健康档案_家族病史.__TableName].Rows.Add();
            dr[tb_健康档案_家族病史.家族病史] = 家族病史;
            dr[tb_健康档案_家族病史.D_JBBM] = 疾病代码;
            dr[tb_健康档案_家族病史.家族关系] = ControlsHelper.GetComboxKey(cbo家族史关系);
            dr[tb_健康档案_家族病史.其他疾病] = this.txt家族史_其他.Text.Trim();
            dr[tb_健康档案_家族病史.家族关系名称] = this.cbo家族史关系.Text.Trim();
            //清空所有的数据
            for (int i = 0; i < flow家族史.Controls.Count; i++)
            {
                if (flow家族史.Controls[i].GetType() == typeof(CheckEdit))//判断是否是复选框
                {
                    CheckEdit chk = (CheckEdit)flow家族史.Controls[i];
                    chk.Checked = false;
                }
                if (flow家族史.Controls[i].GetType() == typeof(TextEdit))//判断是否是复选框
                {
                    TextEdit txt = (TextEdit)flow家族史.Controls[i];
                    txt.Text = string.Empty;
                }
                if (flow疾病.Controls[i].GetType() == typeof(DateEdit))//判断是否是复选框
                {
                    DateEdit dte = (DateEdit)flow家族史.Controls[i];
                    dte.Text = string.Empty;
                }
            }

            this.gc家族史.RefreshDataSource();
        }
        private void Set既往史_手术()
        {
            if (string.IsNullOrEmpty(dte手术时间.Text.Trim()))
            {
                Msg.ShowInformation("请填写 确诊时间！");
                this.dte手术时间.Focus();
                return;
            }
            DataRow dr = _ds健康档案.Tables[tb_健康档案_既往病史.__TableName].Rows.Add();
            dr[tb_健康档案_既往病史.疾病名称] = this.txt手术名称.Text.Trim();
            dr[tb_健康档案_既往病史.D_JBBM] = this.txt手术名称.Text.Trim();
            dr[tb_健康档案_既往病史.日期] = this.dte手术时间.Text == "" ? "" : this.dte手术时间.DateTime.ToString("yyyy-MM-dd");
            dr[tb_健康档案_既往病史.疾病类型] = "手术";

            this.txt手术名称.Text = string.Empty;
            this.dte手术时间.Text = string.Empty;

            this.gc既往史.RefreshDataSource();
        }
        private object Get过敏史()
        {
            return GetFlowLayoutResult(flow过敏史);
        }
        private object Get残疾情况()
        {
            return GetFlowLayoutResult(flow残疾情况);
        }
        private object Get医疗费用支付方式()
        {
            return GetFlowLayoutResult(flow医疗费用支付方式);
        }
        private object Get居住情况()
        {
            return GetFlowLayoutResult(flow居住情况);
        }
        private object Get档案状态()
        {
            return this.radio档案状态.EditValue;
        }
        private int Get完整度(int i)
        {
            int k = Convert.ToInt32((20 - i) * 100 / 20.0);
            return k;
        }
        private int Get缺项()
        {
            int i = 0;
            if (this.txt证件编号.Text.Trim() == "") i++;
            if (this.txt工作单位.Text.Trim() == "") i++;
            if (this.txt本人电话.Text.Trim() == "") i++;
            if (this.txt联系人姓名.Text.Trim() == "") i++;
            if (this.txt联系人电话.Text.Trim() == "") i++;
            if (this.cbo常住类型.Text.Trim() == "") i++;
            if (this.cbo民族.Text.Trim() == "") i++;
            if (this.cbo血型.Text.Trim() == "") i++;
            //if (this.cboRH阴性.Text.Trim() == "") i++;
            if (this.cbo文化程度.Text.Trim() == "") i++;
            if (this.cbo职业.Text.Trim() == "") i++;
            if (Get医疗费用支付方式().ToString() == "") i++;
            if (this.txt居住地址_详细地址.Text.Trim() == "") i++;
            if (this.cbo婚姻状况.Text.Trim() == "") i++;
            //if (Get过敏史().ToString() == "") i++;
            //if (this.txt工作单位.Text.Trim() == "") i++;
            //if (this.txt工作单位.Text.Trim() == "") i++;
            //if (this.txt工作单位.Text.Trim() == "") i++;
            if (this.textEdit本人或家属签字.Text.Trim() == "") i++;
            if (this.dateEdit签字时间.Text.Trim() == "") i++;
            return i;
        }

        /// <summary>
        /// 孕产判断
        /// </summary>
        /// <param name="pram"></param>
        /// <param name="pd"></param>
        private void yccpd(CheckEdit pram, string pd)
        {
            string fyc = this.cbo孕次.Text.Trim();
            string fcc = this.cbo产次.Text.Trim();
            if (pd == "1" && pram.Checked)
            {
                cbo孕次.Enabled = true;
                cbo产次.Enabled = true;
            }
            else if (pd == "2" && pram.Checked)
            {
                if (fcc == "0" || fcc == "")
                {
                    cbo产次.SelectedIndex = 1;
                }
                cbo孕次.Enabled = true;
                cbo产次.Enabled = true;
            }
            else
            {
                cbo孕次.Enabled = false;
                cbo产次.Enabled = false;
            }
        }
        private void Show孕产情况()
        {
            //修改出生日期，会控制  怀孕情况 的隐藏和显示
            if (this.cbo性别.Text.Trim() == "女")
            {
                int age = GetAge(this.dte出生日期.Text);
                if (age >= 15)
                {
                    this.layout孕产情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                }
                else
                {
                    this.layout孕产情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                }
            }
            else
            {
                this.layout孕产情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;

            }
        }
        #endregion

        private void cbo居住地址_村委会_EditValueChanged(object sender, EventArgs e)
        {
            string value = this.cbo居住地址_村委会.Text.Trim();
            if (string.IsNullOrEmpty(this.txt居住地址_详细地址.Text))
                this.txt居住地址_详细地址.Text = value;
        }

        private void cbo性别_EditValueChanged(object sender, EventArgs e)
        {
            Show孕产情况();
        }

        private void gv既往史_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.Column != col日期) return;
            GridView gv = sender as GridView;
            string fieldName = gv.GetRowCellValue(e.RowHandle, gv.Columns["疾病类型"]).ToString();
            switch (fieldName)
            {
                case "疾病":
                    e.RepositoryItem = repositoryItemDateEdit2;
                    break;
                default:
                    e.RepositoryItem = repositoryItemDateEdit1;
                    break;

            }
        }

        private void chk医疗费用支付方式_城镇职工基本医疗保险_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk医疗费用支付方式_城镇职工基本医疗保险.Checked)
            {
                this.textEdit职工医疗保险卡号.Enabled = true;
            }
            else
            {
                this.textEdit职工医疗保险卡号.Enabled = false;
                this.textEdit职工医疗保险卡号.Text = "";
            }
        }

        private void chk医疗费用支付方式_城镇居民基本医疗保险_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk医疗费用支付方式_城镇居民基本医疗保险.Checked)
            {
                this.textEdit居民医疗保险卡号.Enabled = true;
            }
            else
            {
                this.textEdit居民医疗保险卡号.Enabled = false;
                this.textEdit居民医疗保险卡号.Text = "";
            }
        }

        private void chk医疗费用支付方式_贫困救助_CheckedChanged(object sender, EventArgs e)
        {
            if (this.chk医疗费用支付方式_贫困救助.Checked)
            {
                this.textEdit贫困救助卡号.Enabled = true;
            }
            else
            {
                this.textEdit贫困救助卡号.Enabled = false;
                this.textEdit贫困救助卡号.Text = "";
            }
        }

        bool b手签更新 = false;
        FingerPrintHelper.FPForm frm = new FingerPrintHelper.FPForm();
        private void sbtnFingerPrint_Click(object sender, EventArgs e)
        {
            //FingerPrintHelper.FPForm frm = new FingerPrintHelper.FPForm();
            DialogResult result = frm.ShowDialog();
            if(result == DialogResult.OK)
            {
                b手签更新 = true;
                textEdit本人或家属签字.Image = frm.bmp;
            }
        }

    }
}
