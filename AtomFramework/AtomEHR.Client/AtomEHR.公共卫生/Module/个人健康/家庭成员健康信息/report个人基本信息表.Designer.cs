﻿namespace AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息
{
    partial class report个人基本信息表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt居住地址 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.txt签字时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt本人签字 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt居住情况 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt家庭结构 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.txt户主身份证 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt家庭人口数 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt户主姓名 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt禽畜栏 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt厕所 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt燃料类型 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt饮水 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt厨房排风设施 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel185 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel174 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt残疾情况4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel176 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt残疾情况5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel178 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt残疾情况0 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel180 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt残疾情况1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt残疾情况2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel183 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt残疾情况3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt残疾情况其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel172 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel171 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt遗传病史有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt遗传病史疾病名称 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel168 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel167 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt家族史疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel166 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel165 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel140 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史兄弟姐妹5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel142 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史兄弟姐妹99 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel144 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史兄弟姐妹1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel146 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史兄弟姐妹2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史兄弟姐妹3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel149 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史兄弟姐妹4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史兄弟姐妹其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel152 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史子女5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel154 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史子女99 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel156 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史子女1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel158 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史子女2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史子女3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel161 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史子女4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史子女其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel164 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel122 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史父亲5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel124 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史父亲99 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史父亲1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel117 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史父亲2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史父亲3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史父亲4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史父亲其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel128 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史母亲5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel130 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史母亲99 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel132 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史母亲1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel134 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史母亲2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史母亲3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel137 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史母亲4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt家族史母亲其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel127 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel114 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt输血时间1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt输血原因1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel107 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel109 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt输血时间2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt输血原因2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel112 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt输血有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt外伤时间1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt外伤名称1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel97 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt外伤时间2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt外伤名称2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel101 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt外伤有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt手术时间1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt手术名称1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt手术时间2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt手术名称2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt手术有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt疾病其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病6确诊时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病3确诊时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病6 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病5确诊时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病2确诊时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病5 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病4确诊时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病1确诊时间 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病4 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt疾病1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt职业病 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt恶性肿瘤 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt暴露史有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt暴露史1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt暴露史2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt药物过敏史有无 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt药物过敏史1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt药物过敏史2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt药物过敏史99 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt药物过敏史其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt贫困救助卡号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt居民医保卡号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt职工医保卡号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt医疗费用支付方式1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt医疗费用支付方式2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt医疗费用支付方式99 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt医疗费用支付方式其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt婚姻状况 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt职业 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt文化程度 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.txtRH阴性 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt血型 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt常住类型 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt民族 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt少数民族 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt联系人姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt联系人电话 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt本人电话 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt身份证号 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt工作单位 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel_编号1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt性别 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt出生日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号8 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable33,
            this.xrTable31,
            this.xrTable30,
            this.xrTable29,
            this.xrTable28,
            this.xrTable27,
            this.xrTable26,
            this.xrTable25,
            this.xrTable24,
            this.xrTable23,
            this.xrTable22,
            this.xrLabel185,
            this.xrTable21,
            this.xrTable20,
            this.xrTable19,
            this.xrTable18,
            this.xrTable17,
            this.xrLabel114,
            this.xrTable16,
            this.xrTable15,
            this.xrTable14,
            this.xrTable13,
            this.xrLabel45,
            this.xrTable12,
            this.xrTable11,
            this.xrTable10,
            this.xrTable9,
            this.xrTable8,
            this.xrTable7,
            this.xrTable6,
            this.xrTable5,
            this.xrTable4,
            this.xrTable3,
            this.xrTable2,
            this.xrLine1,
            this.xrLabel_编号1,
            this.xrLabel3,
            this.txt姓名,
            this.xrLabel2,
            this.xrTable1,
            this.xrLabel1,
            this.xrLabel_编号2,
            this.xrLabel_编号3,
            this.xrLabel_编号4,
            this.xrLabel_编号5,
            this.xrLabel_编号6,
            this.xrLabel_编号7,
            this.xrLabel_编号8});
            this.Detail.Font = new System.Drawing.Font("宋体", 9F);
            this.Detail.HeightF = 1150F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable33
            // 
            this.xrTable33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable33.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1063.875F);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrTable33.SizeF = new System.Drawing.SizeF(740F, 86.12488F);
            this.xrTable33.StylePriority.UseBorders = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell69});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1.1529831414473684D;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt居住地址,
            this.xrPictureBox1,
            this.txt签字时间,
            this.xrLabel64,
            this.xrLabel63,
            this.xrLabel59,
            this.txt本人签字,
            this.xrLabel57,
            this.xrLabel56});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Weight = 3D;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt居住地址.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 23F);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt居住地址.SizeF = new System.Drawing.SizeF(340F, 47.12476F);
            this.txt居住地址.StylePriority.UseBorders = false;
            this.txt居住地址.StylePriority.UseTextAlignment = false;
            this.txt居住地址.Text = "复核用，默认隐藏";
            this.txt居住地址.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.txt居住地址.Visible = false;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(575F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(92.60004F, 54.41675F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.xrPictureBox1.StylePriority.UseBorders = false;
            // 
            // txt签字时间
            // 
            this.txt签字时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt签字时间.LocationFloat = new DevExpress.Utils.PointFloat(422.5833F, 31.41675F);
            this.txt签字时间.Name = "txt签字时间";
            this.txt签字时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt签字时间.SizeF = new System.Drawing.SizeF(152.2163F, 23F);
            this.txt签字时间.StylePriority.UseBorders = false;
            this.txt签字时间.StylePriority.UseTextAlignment = false;
            this.txt签字时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(364.5835F, 31.4167F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(57.99985F, 23F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "时间：";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel63
            // 
            this.xrLabel63.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(678.6005F, 0.0003178914F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(53.39978F, 23F);
            this.xrLabel63.StylePriority.UseBorders = false;
            this.xrLabel63.Text = "(家属)";
            // 
            // xrLabel59
            // 
            this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(521.9999F, 0.0003662109F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(53.00006F, 23F);
            this.xrLabel59.StylePriority.UseBorders = false;
            this.xrLabel59.StylePriority.UseTextAlignment = false;
            this.xrLabel59.Text = "(本人)/";
            this.xrLabel59.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt本人签字
            // 
            this.txt本人签字.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt本人签字.LocationFloat = new DevExpress.Utils.PointFloat(422.583F, 0.0002543131F);
            this.txt本人签字.Name = "txt本人签字";
            this.txt本人签字.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt本人签字.SizeF = new System.Drawing.SizeF(82.00009F, 22.99988F);
            this.txt本人签字.StylePriority.UseBorders = false;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(364.5833F, 0F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(57.99991F, 23F);
            this.xrLabel57.StylePriority.UseBorders = false;
            this.xrLabel57.StylePriority.UseTextAlignment = false;
            this.xrLabel57.Text = "签字：";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(340F, 23F);
            this.xrLabel56.StylePriority.UseBorders = false;
            this.xrLabel56.Text = "以上内容经核实确认，与居民本人基本信息一致。";
            // 
            // xrTable31
            // 
            this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable31.LocationFloat = new DevExpress.Utils.PointFloat(400.9997F, 891.8751F);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable31.SizeF = new System.Drawing.SizeF(339.0003F, 48F);
            this.xrTable31.StylePriority.UseBorders = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel51,
            this.xrLabel27,
            this.txt居住情况});
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Weight = 3D;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(10F, 23.00006F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(298.4428F, 23F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "3 夫妻二人同住  4 独居 5 计划生育特殊家庭";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(10F, 0.0001220703F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(298.4428F, 22.99994F);
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "1 与成年子女同住  2 与子孙三代(四代)同住 ";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt居住情况
            // 
            this.txt居住情况.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt居住情况.LocationFloat = new DevExpress.Utils.PointFloat(317.7997F, 1F);
            this.txt居住情况.Name = "txt居住情况";
            this.txt居住情况.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt居住情况.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt居住情况.StylePriority.UseBorders = false;
            // 
            // xrTable30
            // 
            this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable30.LocationFloat = new DevExpress.Utils.PointFloat(99.99984F, 891.8751F);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrTable30.SizeF = new System.Drawing.SizeF(300.9998F, 48F);
            this.xrTable30.StylePriority.UseBorders = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.txt家庭结构,
            this.xrTableCell65});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Text = "家庭结构";
            this.xrTableCell61.Weight = 1.0963465367394352D;
            // 
            // txt家庭结构
            // 
            this.txt家庭结构.Name = "txt家庭结构";
            this.txt家庭结构.Weight = 0.89701200791389213D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Text = "居住情况*";
            this.xrTableCell65.Weight = 1.0066414553466727D;
            // 
            // xrTable29
            // 
            this.xrTable29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable29.LocationFloat = new DevExpress.Utils.PointFloat(400.9997F, 866.8751F);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTable29.SizeF = new System.Drawing.SizeF(339.0002F, 25F);
            this.xrTable29.StylePriority.UseBorders = false;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.txt户主身份证,
            this.xrTableCell64,
            this.txt家庭人口数});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // txt户主身份证
            // 
            this.txt户主身份证.Name = "txt户主身份证";
            this.txt户主身份证.Weight = 1.6247798279457684D;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Text = "家庭人口数";
            this.xrTableCell64.Weight = 0.89734270357509183D;
            // 
            // txt家庭人口数
            // 
            this.txt家庭人口数.Name = "txt家庭人口数";
            this.txt家庭人口数.Weight = 0.47787746847913981D;
            // 
            // xrTable28
            // 
            this.xrTable28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable28.LocationFloat = new DevExpress.Utils.PointFloat(100F, 866.8751F);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable28.SizeF = new System.Drawing.SizeF(300.9997F, 25F);
            this.xrTable28.StylePriority.UseBorders = false;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.txt户主姓名,
            this.xrTableCell62});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseBorders = false;
            this.xrTableCell60.Text = "户主姓名";
            this.xrTableCell60.Weight = 1.0999981689453124D;
            // 
            // txt户主姓名
            // 
            this.txt户主姓名.Name = "txt户主姓名";
            this.txt户主姓名.Weight = 0.90000183105468756D;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "身份证号";
            this.xrTableCell62.Weight = 1.0099966430664062D;
            // 
            // xrTable27
            // 
            this.xrTable27.LocationFloat = new DevExpress.Utils.PointFloat(0F, 866.8751F);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTable27.SizeF = new System.Drawing.SizeF(100F, 73F);
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell59});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.StylePriority.UseBorders = false;
            this.xrTableCell59.Text = "家庭情况";
            this.xrTableCell59.Weight = 3D;
            // 
            // xrTable26
            // 
            this.xrTable26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable26.LocationFloat = new DevExpress.Utils.PointFloat(99.99987F, 1039.875F);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable26.SizeF = new System.Drawing.SizeF(640F, 24F);
            this.xrTable26.StylePriority.UseBorders = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell57,
            this.xrTableCell58});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Text = "禽畜栏";
            this.xrTableCell57.Weight = 0.51562498807907109D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel37,
            this.txt禽畜栏});
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Weight = 2.4843750119209291D;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(9.999977F, 0F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(447.6001F, 23F);
            this.xrLabel37.StylePriority.UseBorders = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "1无     2单设     3室内     4室外";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt禽畜栏
            // 
            this.txt禽畜栏.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt禽畜栏.CanGrow = false;
            this.txt禽畜栏.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt禽畜栏.LocationFloat = new DevExpress.Utils.PointFloat(508.7997F, 5F);
            this.txt禽畜栏.Name = "txt禽畜栏";
            this.txt禽畜栏.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt禽畜栏.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt禽畜栏.StylePriority.UseBorders = false;
            this.txt禽畜栏.StylePriority.UseFont = false;
            // 
            // xrTable25
            // 
            this.xrTable25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable25.LocationFloat = new DevExpress.Utils.PointFloat(99.99987F, 1014.875F);
            this.xrTable25.Name = "xrTable25";
            this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTable25.SizeF = new System.Drawing.SizeF(640F, 25F);
            this.xrTable25.StylePriority.UseBorders = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell56});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Text = "厕所";
            this.xrTableCell55.Weight = 0.51562498807907109D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel33,
            this.txt厕所});
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Weight = 2.4843750119209291D;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(9.999977F, 0F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(447.6001F, 23F);
            this.xrLabel33.StylePriority.UseBorders = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "1 卫生厕所  2 一格或二格粪池式  3 马桶  4 露天粪坑  5 简易棚厕";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt厕所
            // 
            this.txt厕所.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt厕所.CanGrow = false;
            this.txt厕所.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt厕所.LocationFloat = new DevExpress.Utils.PointFloat(508.7997F, 2F);
            this.txt厕所.Name = "txt厕所";
            this.txt厕所.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt厕所.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt厕所.StylePriority.UseBorders = false;
            this.txt厕所.StylePriority.UseFont = false;
            // 
            // xrTable24
            // 
            this.xrTable24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable24.LocationFloat = new DevExpress.Utils.PointFloat(99.99984F, 964.8751F);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable24.SizeF = new System.Drawing.SizeF(640F, 25F);
            this.xrTable24.StylePriority.UseBorders = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.xrTableCell54});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Text = "燃料类型";
            this.xrTableCell53.Weight = 0.51562498807907109D;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel30,
            this.txt燃料类型});
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Weight = 2.4843750119209291D;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(9.999977F, 6.103516E-05F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(355.0002F, 23F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "1 液化气  2 煤  3 天然气  4 沼气  5 柴火  6 其他";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt燃料类型
            // 
            this.txt燃料类型.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt燃料类型.CanGrow = false;
            this.txt燃料类型.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt燃料类型.LocationFloat = new DevExpress.Utils.PointFloat(508.7997F, 2F);
            this.txt燃料类型.Name = "txt燃料类型";
            this.txt燃料类型.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt燃料类型.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt燃料类型.StylePriority.UseBorders = false;
            this.txt燃料类型.StylePriority.UseFont = false;
            // 
            // xrTable23
            // 
            this.xrTable23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable23.LocationFloat = new DevExpress.Utils.PointFloat(99.99987F, 989.8751F);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable23.SizeF = new System.Drawing.SizeF(640F, 25F);
            this.xrTable23.StylePriority.UseBorders = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell51,
            this.xrTableCell52});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Text = "饮水";
            this.xrTableCell51.Weight = 0.51562498807907109D;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel32,
            this.txt饮水});
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Weight = 2.4843750119209291D;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 6.103516E-05F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(447.6001F, 22.99994F);
            this.xrLabel32.StylePriority.UseBorders = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "1 自来水  2 经净化过滤的水  3 井水  4 河湖水  5 塘水  6 其他";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt饮水
            // 
            this.txt饮水.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt饮水.CanGrow = false;
            this.txt饮水.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt饮水.LocationFloat = new DevExpress.Utils.PointFloat(508.7997F, 2F);
            this.txt饮水.Name = "txt饮水";
            this.txt饮水.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt饮水.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt饮水.StylePriority.UseBorders = false;
            this.txt饮水.StylePriority.UseFont = false;
            // 
            // xrTable22
            // 
            this.xrTable22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable22.LocationFloat = new DevExpress.Utils.PointFloat(99.99987F, 939.8751F);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.xrTable22.SizeF = new System.Drawing.SizeF(640F, 25F);
            this.xrTable22.StylePriority.UseBorders = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell50});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Text = "厨房排风设施";
            this.xrTableCell49.Weight = 0.51562498807907109D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel29,
            this.txt厨房排风设施});
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Weight = 2.4843750119209291D;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 6.103516E-05F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(280.0001F, 23F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "1 无    2 油烟机    3 换气窗    4 烟囱";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt厨房排风设施
            // 
            this.txt厨房排风设施.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt厨房排风设施.CanGrow = false;
            this.txt厨房排风设施.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt厨房排风设施.LocationFloat = new DevExpress.Utils.PointFloat(508.7997F, 2F);
            this.txt厨房排风设施.Name = "txt厨房排风设施";
            this.txt厨房排风设施.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt厨房排风设施.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt厨房排风设施.StylePriority.UseBorders = false;
            this.txt厨房排风设施.StylePriority.UseFont = false;
            // 
            // xrLabel185
            // 
            this.xrLabel185.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel185.LocationFloat = new DevExpress.Utils.PointFloat(0F, 939.8751F);
            this.xrLabel185.Name = "xrLabel185";
            this.xrLabel185.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel185.SizeF = new System.Drawing.SizeF(100F, 124.0001F);
            this.xrLabel185.StylePriority.UseBorders = false;
            this.xrLabel185.Text = "生活环境*";
            // 
            // xrTable21
            // 
            this.xrTable21.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable21.LocationFloat = new DevExpress.Utils.PointFloat(0F, 817.8751F);
            this.xrTable21.Name = "xrTable21";
            this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable21.SizeF = new System.Drawing.SizeF(740F, 49F);
            this.xrTable21.StylePriority.UseBorders = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell47,
            this.xrTableCell48});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.969499139077951D;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Text = "残疾情况";
            this.xrTableCell47.Weight = 0.40540538478542021D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel174,
            this.txt残疾情况4,
            this.xrLabel176,
            this.txt残疾情况5,
            this.xrLabel178,
            this.txt残疾情况0,
            this.xrLabel180,
            this.txt残疾情况1,
            this.txt残疾情况2,
            this.xrLabel183,
            this.txt残疾情况3,
            this.txt残疾情况其他,
            this.xrLabel172,
            this.xrLabel171});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 2.5945946152145796D;
            // 
            // xrLabel174
            // 
            this.xrLabel174.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel174.CanGrow = false;
            this.xrLabel174.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel174.LocationFloat = new DevExpress.Utils.PointFloat(573.5996F, 28.00001F);
            this.xrLabel174.Name = "xrLabel174";
            this.xrLabel174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel174.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel174.StylePriority.UseBorders = false;
            this.xrLabel174.StylePriority.UseFont = false;
            this.xrLabel174.Text = "/";
            // 
            // txt残疾情况4
            // 
            this.txt残疾情况4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt残疾情况4.CanGrow = false;
            this.txt残疾情况4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt残疾情况4.LocationFloat = new DevExpress.Utils.PointFloat(585.9997F, 28.00001F);
            this.txt残疾情况4.Name = "txt残疾情况4";
            this.txt残疾情况4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt残疾情况4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt残疾情况4.StylePriority.UseBorders = false;
            this.txt残疾情况4.StylePriority.UseFont = false;
            // 
            // xrLabel176
            // 
            this.xrLabel176.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel176.CanGrow = false;
            this.xrLabel176.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel176.LocationFloat = new DevExpress.Utils.PointFloat(606.3998F, 28.00001F);
            this.xrLabel176.Name = "xrLabel176";
            this.xrLabel176.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel176.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel176.StylePriority.UseBorders = false;
            this.xrLabel176.StylePriority.UseFont = false;
            this.xrLabel176.Text = "/";
            // 
            // txt残疾情况5
            // 
            this.txt残疾情况5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt残疾情况5.CanGrow = false;
            this.txt残疾情况5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt残疾情况5.LocationFloat = new DevExpress.Utils.PointFloat(618.7997F, 28.00001F);
            this.txt残疾情况5.Name = "txt残疾情况5";
            this.txt残疾情况5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt残疾情况5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt残疾情况5.StylePriority.UseBorders = false;
            this.txt残疾情况5.StylePriority.UseFont = false;
            // 
            // xrLabel178
            // 
            this.xrLabel178.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel178.CanGrow = false;
            this.xrLabel178.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel178.LocationFloat = new DevExpress.Utils.PointFloat(475.1995F, 28.00001F);
            this.xrLabel178.Name = "xrLabel178";
            this.xrLabel178.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel178.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel178.StylePriority.UseBorders = false;
            this.xrLabel178.StylePriority.UseFont = false;
            this.xrLabel178.Text = "/";
            // 
            // txt残疾情况0
            // 
            this.txt残疾情况0.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt残疾情况0.CanGrow = false;
            this.txt残疾情况0.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt残疾情况0.LocationFloat = new DevExpress.Utils.PointFloat(459.7995F, 28.00001F);
            this.txt残疾情况0.Name = "txt残疾情况0";
            this.txt残疾情况0.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt残疾情况0.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt残疾情况0.StylePriority.UseBorders = false;
            this.txt残疾情况0.StylePriority.UseFont = false;
            // 
            // xrLabel180
            // 
            this.xrLabel180.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel180.CanGrow = false;
            this.xrLabel180.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel180.LocationFloat = new DevExpress.Utils.PointFloat(507.9995F, 28.00001F);
            this.xrLabel180.Name = "xrLabel180";
            this.xrLabel180.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel180.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel180.StylePriority.UseBorders = false;
            this.xrLabel180.StylePriority.UseFont = false;
            this.xrLabel180.Text = "/";
            // 
            // txt残疾情况1
            // 
            this.txt残疾情况1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt残疾情况1.CanGrow = false;
            this.txt残疾情况1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt残疾情况1.LocationFloat = new DevExpress.Utils.PointFloat(487.5996F, 28.00001F);
            this.txt残疾情况1.Name = "txt残疾情况1";
            this.txt残疾情况1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt残疾情况1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt残疾情况1.StylePriority.UseBorders = false;
            this.txt残疾情况1.StylePriority.UseFont = false;
            // 
            // txt残疾情况2
            // 
            this.txt残疾情况2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt残疾情况2.CanGrow = false;
            this.txt残疾情况2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt残疾情况2.LocationFloat = new DevExpress.Utils.PointFloat(520.3995F, 28.00001F);
            this.txt残疾情况2.Name = "txt残疾情况2";
            this.txt残疾情况2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt残疾情况2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt残疾情况2.StylePriority.UseBorders = false;
            this.txt残疾情况2.StylePriority.UseFont = false;
            // 
            // xrLabel183
            // 
            this.xrLabel183.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel183.CanGrow = false;
            this.xrLabel183.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel183.LocationFloat = new DevExpress.Utils.PointFloat(540.7995F, 28.00001F);
            this.xrLabel183.Name = "xrLabel183";
            this.xrLabel183.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel183.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel183.StylePriority.UseBorders = false;
            this.xrLabel183.StylePriority.UseFont = false;
            this.xrLabel183.Text = "/";
            // 
            // txt残疾情况3
            // 
            this.txt残疾情况3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt残疾情况3.CanGrow = false;
            this.txt残疾情况3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt残疾情况3.LocationFloat = new DevExpress.Utils.PointFloat(553.1995F, 28.00001F);
            this.txt残疾情况3.Name = "txt残疾情况3";
            this.txt残疾情况3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt残疾情况3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt残疾情况3.StylePriority.UseBorders = false;
            this.txt残疾情况3.StylePriority.UseFont = false;
            // 
            // txt残疾情况其他
            // 
            this.txt残疾情况其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt残疾情况其他.LocationFloat = new DevExpress.Utils.PointFloat(255.9999F, 23F);
            this.txt残疾情况其他.Name = "txt残疾情况其他";
            this.txt残疾情况其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt残疾情况其他.SizeF = new System.Drawing.SizeF(198.8F, 23F);
            this.txt残疾情况其他.StylePriority.UseBorders = false;
            this.txt残疾情况其他.StylePriority.UseTextAlignment = false;
            this.txt残疾情况其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel172
            // 
            this.xrLabel172.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel172.LocationFloat = new DevExpress.Utils.PointFloat(10.00011F, 23F);
            this.xrLabel172.Name = "xrLabel172";
            this.xrLabel172.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel172.SizeF = new System.Drawing.SizeF(245.9998F, 23F);
            this.xrLabel172.StylePriority.UseBorders = false;
            this.xrLabel172.StylePriority.UseTextAlignment = false;
            this.xrLabel172.Text = "6智力残疾 7精神残疾  8其他残疾";
            this.xrLabel172.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel171
            // 
            this.xrLabel171.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel171.LocationFloat = new DevExpress.Utils.PointFloat(9.183757F, 0F);
            this.xrLabel171.Name = "xrLabel171";
            this.xrLabel171.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel171.SizeF = new System.Drawing.SizeF(460F, 23F);
            this.xrLabel171.StylePriority.UseBorders = false;
            this.xrLabel171.StylePriority.UseTextAlignment = false;
            this.xrLabel171.Text = "1无残疾 2 视力残疾 3听力残疾 4言语残疾 5 肢体残疾";
            this.xrLabel171.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable20
            // 
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 791.8751F);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable20.SizeF = new System.Drawing.SizeF(740F, 26F);
            this.xrTable20.StylePriority.UseBorders = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.8666666666666667D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Text = "遗传病史";
            this.xrTableCell45.Weight = 0.40540538478542021D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt遗传病史有无,
            this.txt遗传病史疾病名称,
            this.xrLabel168,
            this.xrLabel167});
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 2.5945946152145796D;
            // 
            // txt遗传病史有无
            // 
            this.txt遗传病史有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt遗传病史有无.CanGrow = false;
            this.txt遗传病史有无.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt遗传病史有无.LocationFloat = new DevExpress.Utils.PointFloat(617.0001F, 5.000051F);
            this.txt遗传病史有无.Name = "txt遗传病史有无";
            this.txt遗传病史有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt遗传病史有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt遗传病史有无.StylePriority.UseBorders = false;
            this.txt遗传病史有无.StylePriority.UseFont = false;
            // 
            // txt遗传病史疾病名称
            // 
            this.txt遗传病史疾病名称.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt遗传病史疾病名称.LocationFloat = new DevExpress.Utils.PointFloat(165F, 0F);
            this.txt遗传病史疾病名称.Name = "txt遗传病史疾病名称";
            this.txt遗传病史疾病名称.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt遗传病史疾病名称.SizeF = new System.Drawing.SizeF(265F, 23F);
            this.txt遗传病史疾病名称.StylePriority.UseBorders = false;
            this.txt遗传病史疾病名称.StylePriority.UseTextAlignment = false;
            this.txt遗传病史疾病名称.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel168
            // 
            this.xrLabel168.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel168.LocationFloat = new DevExpress.Utils.PointFloat(94.99998F, 6.357829E-05F);
            this.xrLabel168.Name = "xrLabel168";
            this.xrLabel168.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel168.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.xrLabel168.StylePriority.UseBorders = false;
            this.xrLabel168.Text = "疾病名称";
            // 
            // xrLabel167
            // 
            this.xrLabel167.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel167.LocationFloat = new DevExpress.Utils.PointFloat(9.999974F, 3.178914E-05F);
            this.xrLabel167.Name = "xrLabel167";
            this.xrLabel167.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel167.SizeF = new System.Drawing.SizeF(85F, 23F);
            this.xrLabel167.StylePriority.UseBorders = false;
            this.xrLabel167.StylePriority.UseTextAlignment = false;
            this.xrLabel167.Text = "1 无 2 有：";
            this.xrLabel167.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable19
            // 
            this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable19.LocationFloat = new DevExpress.Utils.PointFloat(100F, 741.8751F);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable19.SizeF = new System.Drawing.SizeF(640F, 50F);
            this.xrTable19.StylePriority.UseBorders = false;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt家族史疾病其他,
            this.xrLabel166,
            this.xrLabel165});
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 3D;
            // 
            // txt家族史疾病其他
            // 
            this.txt家族史疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt家族史疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(532.9999F, 23.00012F);
            this.txt家族史疾病其他.Name = "txt家族史疾病其他";
            this.txt家族史疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt家族史疾病其他.SizeF = new System.Drawing.SizeF(85.99994F, 23F);
            this.txt家族史疾病其他.StylePriority.UseBorders = false;
            this.txt家族史疾病其他.StylePriority.UseTextAlignment = false;
            this.txt家族史疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel166
            // 
            this.xrLabel166.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel166.LocationFloat = new DevExpress.Utils.PointFloat(9.999977F, 23.00012F);
            this.xrLabel166.Name = "xrLabel166";
            this.xrLabel166.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel166.SizeF = new System.Drawing.SizeF(522.9999F, 22.99994F);
            this.xrLabel166.StylePriority.UseBorders = false;
            this.xrLabel166.StylePriority.UseTextAlignment = false;
            this.xrLabel166.Text = "7 脑卒中  8 严重精神障碍    9 结核病    10 肝炎    11 先天畸形    12 其他";
            this.xrLabel166.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel165
            // 
            this.xrLabel165.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel165.LocationFloat = new DevExpress.Utils.PointFloat(10.00014F, 6.103516E-05F);
            this.xrLabel165.Name = "xrLabel165";
            this.xrLabel165.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel165.SizeF = new System.Drawing.SizeF(527.9998F, 23.00006F);
            this.xrLabel165.StylePriority.UseBorders = false;
            this.xrLabel165.StylePriority.UseTextAlignment = false;
            this.xrLabel165.Text = "1 无   2 高血压   3 糖尿病   4 冠心病   5 慢性阻塞性肺疾病   6 恶性肿瘤";
            this.xrLabel165.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable18
            // 
            this.xrTable18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(100F, 716.8751F);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable18.SizeF = new System.Drawing.SizeF(640F, 25F);
            this.xrTable18.StylePriority.UseBorders = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell41,
            this.xrTableCell42,
            this.xrTableCell43});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Text = "兄弟姐妹";
            this.xrTableCell41.Weight = 0.32812500596046451D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel140,
            this.txt家族史兄弟姐妹5,
            this.xrLabel142,
            this.txt家族史兄弟姐妹99,
            this.xrLabel144,
            this.txt家族史兄弟姐妹1,
            this.xrLabel146,
            this.txt家族史兄弟姐妹2,
            this.txt家族史兄弟姐妹3,
            this.xrLabel149,
            this.txt家族史兄弟姐妹4,
            this.txt家族史兄弟姐妹其他});
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Weight = 1.1812499701976775D;
            // 
            // xrLabel140
            // 
            this.xrLabel140.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel140.CanGrow = false;
            this.xrLabel140.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel140.LocationFloat = new DevExpress.Utils.PointFloat(122.8F, 2.000046F);
            this.xrLabel140.Name = "xrLabel140";
            this.xrLabel140.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel140.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel140.StylePriority.UseBorders = false;
            this.xrLabel140.StylePriority.UseFont = false;
            this.xrLabel140.Text = "/";
            // 
            // txt家族史兄弟姐妹5
            // 
            this.txt家族史兄弟姐妹5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史兄弟姐妹5.CanGrow = false;
            this.txt家族史兄弟姐妹5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史兄弟姐妹5.LocationFloat = new DevExpress.Utils.PointFloat(135.2001F, 2.000046F);
            this.txt家族史兄弟姐妹5.Name = "txt家族史兄弟姐妹5";
            this.txt家族史兄弟姐妹5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史兄弟姐妹5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史兄弟姐妹5.StylePriority.UseBorders = false;
            this.txt家族史兄弟姐妹5.StylePriority.UseFont = false;
            // 
            // xrLabel142
            // 
            this.xrLabel142.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel142.CanGrow = false;
            this.xrLabel142.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel142.LocationFloat = new DevExpress.Utils.PointFloat(155.6F, 2.000046F);
            this.xrLabel142.Name = "xrLabel142";
            this.xrLabel142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel142.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel142.StylePriority.UseBorders = false;
            this.xrLabel142.StylePriority.UseFont = false;
            this.xrLabel142.Text = "/";
            // 
            // txt家族史兄弟姐妹99
            // 
            this.txt家族史兄弟姐妹99.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史兄弟姐妹99.CanGrow = false;
            this.txt家族史兄弟姐妹99.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史兄弟姐妹99.LocationFloat = new DevExpress.Utils.PointFloat(168.0001F, 2.000046F);
            this.txt家族史兄弟姐妹99.Name = "txt家族史兄弟姐妹99";
            this.txt家族史兄弟姐妹99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史兄弟姐妹99.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史兄弟姐妹99.StylePriority.UseBorders = false;
            this.txt家族史兄弟姐妹99.StylePriority.UseFont = false;
            // 
            // xrLabel144
            // 
            this.xrLabel144.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel144.CanGrow = false;
            this.xrLabel144.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel144.LocationFloat = new DevExpress.Utils.PointFloat(24.39982F, 2.000046F);
            this.xrLabel144.Name = "xrLabel144";
            this.xrLabel144.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel144.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel144.StylePriority.UseBorders = false;
            this.xrLabel144.StylePriority.UseFont = false;
            this.xrLabel144.Text = "/";
            // 
            // txt家族史兄弟姐妹1
            // 
            this.txt家族史兄弟姐妹1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史兄弟姐妹1.CanGrow = false;
            this.txt家族史兄弟姐妹1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史兄弟姐妹1.LocationFloat = new DevExpress.Utils.PointFloat(3.999774F, 2.000046F);
            this.txt家族史兄弟姐妹1.Name = "txt家族史兄弟姐妹1";
            this.txt家族史兄弟姐妹1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史兄弟姐妹1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史兄弟姐妹1.StylePriority.UseBorders = false;
            this.txt家族史兄弟姐妹1.StylePriority.UseFont = false;
            // 
            // xrLabel146
            // 
            this.xrLabel146.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel146.CanGrow = false;
            this.xrLabel146.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel146.LocationFloat = new DevExpress.Utils.PointFloat(57.19992F, 2.000046F);
            this.xrLabel146.Name = "xrLabel146";
            this.xrLabel146.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel146.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel146.StylePriority.UseBorders = false;
            this.xrLabel146.StylePriority.UseFont = false;
            this.xrLabel146.Text = "/";
            // 
            // txt家族史兄弟姐妹2
            // 
            this.txt家族史兄弟姐妹2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史兄弟姐妹2.CanGrow = false;
            this.txt家族史兄弟姐妹2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史兄弟姐妹2.LocationFloat = new DevExpress.Utils.PointFloat(36.79981F, 2.000046F);
            this.txt家族史兄弟姐妹2.Name = "txt家族史兄弟姐妹2";
            this.txt家族史兄弟姐妹2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史兄弟姐妹2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史兄弟姐妹2.StylePriority.UseBorders = false;
            this.txt家族史兄弟姐妹2.StylePriority.UseFont = false;
            // 
            // txt家族史兄弟姐妹3
            // 
            this.txt家族史兄弟姐妹3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史兄弟姐妹3.CanGrow = false;
            this.txt家族史兄弟姐妹3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史兄弟姐妹3.LocationFloat = new DevExpress.Utils.PointFloat(69.59985F, 2.000046F);
            this.txt家族史兄弟姐妹3.Name = "txt家族史兄弟姐妹3";
            this.txt家族史兄弟姐妹3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史兄弟姐妹3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史兄弟姐妹3.StylePriority.UseBorders = false;
            this.txt家族史兄弟姐妹3.StylePriority.UseFont = false;
            // 
            // xrLabel149
            // 
            this.xrLabel149.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel149.CanGrow = false;
            this.xrLabel149.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel149.LocationFloat = new DevExpress.Utils.PointFloat(89.99996F, 2.000046F);
            this.xrLabel149.Name = "xrLabel149";
            this.xrLabel149.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel149.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel149.StylePriority.UseBorders = false;
            this.xrLabel149.StylePriority.UseFont = false;
            this.xrLabel149.Text = "/";
            // 
            // txt家族史兄弟姐妹4
            // 
            this.txt家族史兄弟姐妹4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史兄弟姐妹4.CanGrow = false;
            this.txt家族史兄弟姐妹4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史兄弟姐妹4.LocationFloat = new DevExpress.Utils.PointFloat(102.3999F, 2.000046F);
            this.txt家族史兄弟姐妹4.Name = "txt家族史兄弟姐妹4";
            this.txt家族史兄弟姐妹4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史兄弟姐妹4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史兄弟姐妹4.StylePriority.UseBorders = false;
            this.txt家族史兄弟姐妹4.StylePriority.UseFont = false;
            // 
            // txt家族史兄弟姐妹其他
            // 
            this.txt家族史兄弟姐妹其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt家族史兄弟姐妹其他.LocationFloat = new DevExpress.Utils.PointFloat(190F, 0F);
            this.txt家族史兄弟姐妹其他.Name = "txt家族史兄弟姐妹其他";
            this.txt家族史兄弟姐妹其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史兄弟姐妹其他.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.txt家族史兄弟姐妹其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel152,
            this.txt家族史子女5,
            this.xrLabel154,
            this.txt家族史子女99,
            this.xrLabel156,
            this.txt家族史子女1,
            this.xrLabel158,
            this.txt家族史子女2,
            this.txt家族史子女3,
            this.xrLabel161,
            this.txt家族史子女4,
            this.txt家族史子女其他,
            this.xrLabel164});
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Text = "xrTableCell40";
            this.xrTableCell43.Weight = 1.4906250238418579D;
            // 
            // xrLabel152
            // 
            this.xrLabel152.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel152.CanGrow = false;
            this.xrLabel152.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel152.LocationFloat = new DevExpress.Utils.PointFloat(185.4F, 2.000046F);
            this.xrLabel152.Name = "xrLabel152";
            this.xrLabel152.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel152.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel152.StylePriority.UseBorders = false;
            this.xrLabel152.StylePriority.UseFont = false;
            this.xrLabel152.Text = "/";
            // 
            // txt家族史子女5
            // 
            this.txt家族史子女5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史子女5.CanGrow = false;
            this.txt家族史子女5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史子女5.LocationFloat = new DevExpress.Utils.PointFloat(197.8001F, 2.000046F);
            this.txt家族史子女5.Name = "txt家族史子女5";
            this.txt家族史子女5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史子女5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史子女5.StylePriority.UseBorders = false;
            this.txt家族史子女5.StylePriority.UseFont = false;
            // 
            // xrLabel154
            // 
            this.xrLabel154.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel154.CanGrow = false;
            this.xrLabel154.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel154.LocationFloat = new DevExpress.Utils.PointFloat(218.2002F, 2.000046F);
            this.xrLabel154.Name = "xrLabel154";
            this.xrLabel154.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel154.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel154.StylePriority.UseBorders = false;
            this.xrLabel154.StylePriority.UseFont = false;
            this.xrLabel154.Text = "/";
            // 
            // txt家族史子女99
            // 
            this.txt家族史子女99.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史子女99.CanGrow = false;
            this.txt家族史子女99.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史子女99.LocationFloat = new DevExpress.Utils.PointFloat(230.6001F, 2.000046F);
            this.txt家族史子女99.Name = "txt家族史子女99";
            this.txt家族史子女99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史子女99.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史子女99.StylePriority.UseBorders = false;
            this.txt家族史子女99.StylePriority.UseFont = false;
            // 
            // xrLabel156
            // 
            this.xrLabel156.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel156.CanGrow = false;
            this.xrLabel156.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel156.LocationFloat = new DevExpress.Utils.PointFloat(86.99995F, 2.000046F);
            this.xrLabel156.Name = "xrLabel156";
            this.xrLabel156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel156.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel156.StylePriority.UseBorders = false;
            this.xrLabel156.StylePriority.UseFont = false;
            this.xrLabel156.Text = "/";
            // 
            // txt家族史子女1
            // 
            this.txt家族史子女1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史子女1.CanGrow = false;
            this.txt家族史子女1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史子女1.LocationFloat = new DevExpress.Utils.PointFloat(66.59985F, 2.000046F);
            this.txt家族史子女1.Name = "txt家族史子女1";
            this.txt家族史子女1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史子女1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史子女1.StylePriority.UseBorders = false;
            this.txt家族史子女1.StylePriority.UseFont = false;
            // 
            // xrLabel158
            // 
            this.xrLabel158.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel158.CanGrow = false;
            this.xrLabel158.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel158.LocationFloat = new DevExpress.Utils.PointFloat(119.7999F, 2.000046F);
            this.xrLabel158.Name = "xrLabel158";
            this.xrLabel158.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel158.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel158.StylePriority.UseBorders = false;
            this.xrLabel158.StylePriority.UseFont = false;
            this.xrLabel158.Text = "/";
            // 
            // txt家族史子女2
            // 
            this.txt家族史子女2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史子女2.CanGrow = false;
            this.txt家族史子女2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史子女2.LocationFloat = new DevExpress.Utils.PointFloat(99.39989F, 2.000046F);
            this.txt家族史子女2.Name = "txt家族史子女2";
            this.txt家族史子女2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史子女2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史子女2.StylePriority.UseBorders = false;
            this.txt家族史子女2.StylePriority.UseFont = false;
            // 
            // txt家族史子女3
            // 
            this.txt家族史子女3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史子女3.CanGrow = false;
            this.txt家族史子女3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史子女3.LocationFloat = new DevExpress.Utils.PointFloat(132.1999F, 2.000046F);
            this.txt家族史子女3.Name = "txt家族史子女3";
            this.txt家族史子女3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史子女3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史子女3.StylePriority.UseBorders = false;
            this.txt家族史子女3.StylePriority.UseFont = false;
            // 
            // xrLabel161
            // 
            this.xrLabel161.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel161.CanGrow = false;
            this.xrLabel161.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel161.LocationFloat = new DevExpress.Utils.PointFloat(152.6F, 2.000046F);
            this.xrLabel161.Name = "xrLabel161";
            this.xrLabel161.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel161.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel161.StylePriority.UseBorders = false;
            this.xrLabel161.StylePriority.UseFont = false;
            this.xrLabel161.Text = "/";
            // 
            // txt家族史子女4
            // 
            this.txt家族史子女4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史子女4.CanGrow = false;
            this.txt家族史子女4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史子女4.LocationFloat = new DevExpress.Utils.PointFloat(164.9999F, 2.000046F);
            this.txt家族史子女4.Name = "txt家族史子女4";
            this.txt家族史子女4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史子女4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史子女4.StylePriority.UseBorders = false;
            this.txt家族史子女4.StylePriority.UseFont = false;
            // 
            // txt家族史子女其他
            // 
            this.txt家族史子女其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt家族史子女其他.LocationFloat = new DevExpress.Utils.PointFloat(252.6F, 0.0001271566F);
            this.txt家族史子女其他.Name = "txt家族史子女其他";
            this.txt家族史子女其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史子女其他.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.txt家族史子女其他.StylePriority.UseBorders = false;
            // 
            // xrLabel164
            // 
            this.xrLabel164.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel164.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 0F);
            this.xrLabel164.Name = "xrLabel164";
            this.xrLabel164.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel164.SizeF = new System.Drawing.SizeF(60F, 25F);
            this.xrLabel164.StylePriority.UseBorders = false;
            this.xrLabel164.Text = "子女";
            // 
            // xrTable17
            // 
            this.xrTable17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(100F, 691.8751F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable17.SizeF = new System.Drawing.SizeF(640F, 25F);
            this.xrTable17.StylePriority.UseBorders = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell38,
            this.xrTableCell39,
            this.xrTableCell40});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "父亲";
            this.xrTableCell38.Weight = 0.32812500596046451D;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel122,
            this.txt家族史父亲5,
            this.xrLabel124,
            this.txt家族史父亲99,
            this.xrLabel115,
            this.txt家族史父亲1,
            this.xrLabel117,
            this.txt家族史父亲2,
            this.txt家族史父亲3,
            this.xrLabel120,
            this.txt家族史父亲4,
            this.txt家族史父亲其他});
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Weight = 1.1812499701976775D;
            // 
            // xrLabel122
            // 
            this.xrLabel122.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel122.CanGrow = false;
            this.xrLabel122.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel122.LocationFloat = new DevExpress.Utils.PointFloat(122.8F, 2.000046F);
            this.xrLabel122.Name = "xrLabel122";
            this.xrLabel122.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel122.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel122.StylePriority.UseBorders = false;
            this.xrLabel122.StylePriority.UseFont = false;
            this.xrLabel122.Text = "/";
            // 
            // txt家族史父亲5
            // 
            this.txt家族史父亲5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史父亲5.CanGrow = false;
            this.txt家族史父亲5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史父亲5.LocationFloat = new DevExpress.Utils.PointFloat(135.2001F, 2.000046F);
            this.txt家族史父亲5.Name = "txt家族史父亲5";
            this.txt家族史父亲5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史父亲5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史父亲5.StylePriority.UseBorders = false;
            this.txt家族史父亲5.StylePriority.UseFont = false;
            // 
            // xrLabel124
            // 
            this.xrLabel124.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel124.CanGrow = false;
            this.xrLabel124.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel124.LocationFloat = new DevExpress.Utils.PointFloat(155.6F, 2.000046F);
            this.xrLabel124.Name = "xrLabel124";
            this.xrLabel124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel124.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel124.StylePriority.UseBorders = false;
            this.xrLabel124.StylePriority.UseFont = false;
            this.xrLabel124.Text = "/";
            // 
            // txt家族史父亲99
            // 
            this.txt家族史父亲99.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史父亲99.CanGrow = false;
            this.txt家族史父亲99.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史父亲99.LocationFloat = new DevExpress.Utils.PointFloat(168.0001F, 2.000046F);
            this.txt家族史父亲99.Name = "txt家族史父亲99";
            this.txt家族史父亲99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史父亲99.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史父亲99.StylePriority.UseBorders = false;
            this.txt家族史父亲99.StylePriority.UseFont = false;
            // 
            // xrLabel115
            // 
            this.xrLabel115.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel115.CanGrow = false;
            this.xrLabel115.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(24.39982F, 2.000046F);
            this.xrLabel115.Name = "xrLabel115";
            this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel115.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel115.StylePriority.UseBorders = false;
            this.xrLabel115.StylePriority.UseFont = false;
            this.xrLabel115.Text = "/";
            // 
            // txt家族史父亲1
            // 
            this.txt家族史父亲1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史父亲1.CanGrow = false;
            this.txt家族史父亲1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史父亲1.LocationFloat = new DevExpress.Utils.PointFloat(3.999774F, 2.000046F);
            this.txt家族史父亲1.Name = "txt家族史父亲1";
            this.txt家族史父亲1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史父亲1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史父亲1.StylePriority.UseBorders = false;
            this.txt家族史父亲1.StylePriority.UseFont = false;
            // 
            // xrLabel117
            // 
            this.xrLabel117.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel117.CanGrow = false;
            this.xrLabel117.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel117.LocationFloat = new DevExpress.Utils.PointFloat(57.19992F, 2.000046F);
            this.xrLabel117.Name = "xrLabel117";
            this.xrLabel117.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel117.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel117.StylePriority.UseBorders = false;
            this.xrLabel117.StylePriority.UseFont = false;
            this.xrLabel117.Text = "/";
            // 
            // txt家族史父亲2
            // 
            this.txt家族史父亲2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史父亲2.CanGrow = false;
            this.txt家族史父亲2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史父亲2.LocationFloat = new DevExpress.Utils.PointFloat(36.79981F, 2.000046F);
            this.txt家族史父亲2.Name = "txt家族史父亲2";
            this.txt家族史父亲2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史父亲2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史父亲2.StylePriority.UseBorders = false;
            this.txt家族史父亲2.StylePriority.UseFont = false;
            // 
            // txt家族史父亲3
            // 
            this.txt家族史父亲3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史父亲3.CanGrow = false;
            this.txt家族史父亲3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史父亲3.LocationFloat = new DevExpress.Utils.PointFloat(69.59985F, 2.000046F);
            this.txt家族史父亲3.Name = "txt家族史父亲3";
            this.txt家族史父亲3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史父亲3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史父亲3.StylePriority.UseBorders = false;
            this.txt家族史父亲3.StylePriority.UseFont = false;
            // 
            // xrLabel120
            // 
            this.xrLabel120.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel120.CanGrow = false;
            this.xrLabel120.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(89.99996F, 2.000046F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel120.StylePriority.UseBorders = false;
            this.xrLabel120.StylePriority.UseFont = false;
            this.xrLabel120.Text = "/";
            // 
            // txt家族史父亲4
            // 
            this.txt家族史父亲4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史父亲4.CanGrow = false;
            this.txt家族史父亲4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史父亲4.LocationFloat = new DevExpress.Utils.PointFloat(102.3999F, 2.000046F);
            this.txt家族史父亲4.Name = "txt家族史父亲4";
            this.txt家族史父亲4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史父亲4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史父亲4.StylePriority.UseBorders = false;
            this.txt家族史父亲4.StylePriority.UseFont = false;
            // 
            // txt家族史父亲其他
            // 
            this.txt家族史父亲其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt家族史父亲其他.LocationFloat = new DevExpress.Utils.PointFloat(190F, 0F);
            this.txt家族史父亲其他.Name = "txt家族史父亲其他";
            this.txt家族史父亲其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史父亲其他.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.txt家族史父亲其他.StylePriority.UseBorders = false;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel128,
            this.txt家族史母亲5,
            this.xrLabel130,
            this.txt家族史母亲99,
            this.xrLabel132,
            this.txt家族史母亲1,
            this.xrLabel134,
            this.txt家族史母亲2,
            this.txt家族史母亲3,
            this.xrLabel137,
            this.txt家族史母亲4,
            this.txt家族史母亲其他,
            this.xrLabel127});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Text = "xrTableCell40";
            this.xrTableCell40.Weight = 1.4906250238418579D;
            // 
            // xrLabel128
            // 
            this.xrLabel128.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel128.CanGrow = false;
            this.xrLabel128.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel128.LocationFloat = new DevExpress.Utils.PointFloat(185.4F, 2.000046F);
            this.xrLabel128.Name = "xrLabel128";
            this.xrLabel128.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel128.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel128.StylePriority.UseBorders = false;
            this.xrLabel128.StylePriority.UseFont = false;
            this.xrLabel128.Text = "/";
            // 
            // txt家族史母亲5
            // 
            this.txt家族史母亲5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史母亲5.CanGrow = false;
            this.txt家族史母亲5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史母亲5.LocationFloat = new DevExpress.Utils.PointFloat(197.8001F, 2.000046F);
            this.txt家族史母亲5.Name = "txt家族史母亲5";
            this.txt家族史母亲5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史母亲5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史母亲5.StylePriority.UseBorders = false;
            this.txt家族史母亲5.StylePriority.UseFont = false;
            // 
            // xrLabel130
            // 
            this.xrLabel130.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel130.CanGrow = false;
            this.xrLabel130.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel130.LocationFloat = new DevExpress.Utils.PointFloat(218.2002F, 2.000046F);
            this.xrLabel130.Name = "xrLabel130";
            this.xrLabel130.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel130.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel130.StylePriority.UseBorders = false;
            this.xrLabel130.StylePriority.UseFont = false;
            this.xrLabel130.Text = "/";
            // 
            // txt家族史母亲99
            // 
            this.txt家族史母亲99.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史母亲99.CanGrow = false;
            this.txt家族史母亲99.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史母亲99.LocationFloat = new DevExpress.Utils.PointFloat(230.6001F, 2.000046F);
            this.txt家族史母亲99.Name = "txt家族史母亲99";
            this.txt家族史母亲99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史母亲99.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史母亲99.StylePriority.UseBorders = false;
            this.txt家族史母亲99.StylePriority.UseFont = false;
            // 
            // xrLabel132
            // 
            this.xrLabel132.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel132.CanGrow = false;
            this.xrLabel132.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel132.LocationFloat = new DevExpress.Utils.PointFloat(86.99995F, 2.000046F);
            this.xrLabel132.Name = "xrLabel132";
            this.xrLabel132.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel132.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel132.StylePriority.UseBorders = false;
            this.xrLabel132.StylePriority.UseFont = false;
            this.xrLabel132.Text = "/";
            // 
            // txt家族史母亲1
            // 
            this.txt家族史母亲1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史母亲1.CanGrow = false;
            this.txt家族史母亲1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史母亲1.LocationFloat = new DevExpress.Utils.PointFloat(66.59985F, 2.000046F);
            this.txt家族史母亲1.Name = "txt家族史母亲1";
            this.txt家族史母亲1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史母亲1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史母亲1.StylePriority.UseBorders = false;
            this.txt家族史母亲1.StylePriority.UseFont = false;
            // 
            // xrLabel134
            // 
            this.xrLabel134.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel134.CanGrow = false;
            this.xrLabel134.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel134.LocationFloat = new DevExpress.Utils.PointFloat(119.7999F, 2.000046F);
            this.xrLabel134.Name = "xrLabel134";
            this.xrLabel134.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel134.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel134.StylePriority.UseBorders = false;
            this.xrLabel134.StylePriority.UseFont = false;
            this.xrLabel134.Text = "/";
            // 
            // txt家族史母亲2
            // 
            this.txt家族史母亲2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史母亲2.CanGrow = false;
            this.txt家族史母亲2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史母亲2.LocationFloat = new DevExpress.Utils.PointFloat(99.39989F, 2.000046F);
            this.txt家族史母亲2.Name = "txt家族史母亲2";
            this.txt家族史母亲2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史母亲2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史母亲2.StylePriority.UseBorders = false;
            this.txt家族史母亲2.StylePriority.UseFont = false;
            // 
            // txt家族史母亲3
            // 
            this.txt家族史母亲3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史母亲3.CanGrow = false;
            this.txt家族史母亲3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史母亲3.LocationFloat = new DevExpress.Utils.PointFloat(132.1999F, 2.000046F);
            this.txt家族史母亲3.Name = "txt家族史母亲3";
            this.txt家族史母亲3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史母亲3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史母亲3.StylePriority.UseBorders = false;
            this.txt家族史母亲3.StylePriority.UseFont = false;
            // 
            // xrLabel137
            // 
            this.xrLabel137.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel137.CanGrow = false;
            this.xrLabel137.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel137.LocationFloat = new DevExpress.Utils.PointFloat(152.6F, 2.000046F);
            this.xrLabel137.Name = "xrLabel137";
            this.xrLabel137.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel137.SizeF = new System.Drawing.SizeF(10F, 18F);
            this.xrLabel137.StylePriority.UseBorders = false;
            this.xrLabel137.StylePriority.UseFont = false;
            this.xrLabel137.Text = "/";
            // 
            // txt家族史母亲4
            // 
            this.txt家族史母亲4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt家族史母亲4.CanGrow = false;
            this.txt家族史母亲4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt家族史母亲4.LocationFloat = new DevExpress.Utils.PointFloat(164.9999F, 2.000046F);
            this.txt家族史母亲4.Name = "txt家族史母亲4";
            this.txt家族史母亲4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史母亲4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt家族史母亲4.StylePriority.UseBorders = false;
            this.txt家族史母亲4.StylePriority.UseFont = false;
            // 
            // txt家族史母亲其他
            // 
            this.txt家族史母亲其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt家族史母亲其他.LocationFloat = new DevExpress.Utils.PointFloat(252.6F, 0.0001271566F);
            this.txt家族史母亲其他.Name = "txt家族史母亲其他";
            this.txt家族史母亲其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt家族史母亲其他.SizeF = new System.Drawing.SizeF(55F, 20F);
            this.txt家族史母亲其他.StylePriority.UseBorders = false;
            // 
            // xrLabel127
            // 
            this.xrLabel127.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel127.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 0F);
            this.xrLabel127.Name = "xrLabel127";
            this.xrLabel127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel127.SizeF = new System.Drawing.SizeF(60F, 25F);
            this.xrLabel127.StylePriority.UseBorders = false;
            this.xrLabel127.Text = "母亲";
            // 
            // xrLabel114
            // 
            this.xrLabel114.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel114.LocationFloat = new DevExpress.Utils.PointFloat(3.178914E-05F, 691.8751F);
            this.xrLabel114.Name = "xrLabel114";
            this.xrLabel114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel114.SizeF = new System.Drawing.SizeF(100F, 100F);
            this.xrLabel114.StylePriority.UseBorders = false;
            this.xrLabel114.Text = "家族史";
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(29.99992F, 666.875F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable16.SizeF = new System.Drawing.SizeF(710F, 25F);
            this.xrTable16.StylePriority.UseBorders = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.xrTableCell37});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Text = "输血";
            this.xrTableCell36.Weight = 0.29577469086982833D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel103,
            this.txt输血时间1,
            this.xrLabel105,
            this.txt输血原因1,
            this.xrLabel107,
            this.xrLabel108,
            this.xrLabel109,
            this.txt输血时间2,
            this.txt输血原因2,
            this.xrLabel112,
            this.txt输血有无});
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 2.7042253091301718D;
            // 
            // xrLabel103
            // 
            this.xrLabel103.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel103.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(340.0001F, 4.999987F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel103.StylePriority.UseBorders = false;
            this.xrLabel103.StylePriority.UseFont = false;
            this.xrLabel103.Text = "/";
            // 
            // txt输血时间1
            // 
            this.txt输血时间1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt输血时间1.LocationFloat = new DevExpress.Utils.PointFloat(260.0001F, 1F);
            this.txt输血时间1.Name = "txt输血时间1";
            this.txt输血时间1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt输血时间1.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt输血时间1.StylePriority.UseBorders = false;
            this.txt输血时间1.StylePriority.UseTextAlignment = false;
            this.txt输血时间1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel105
            // 
            this.xrLabel105.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(210.0001F, 9.536743E-05F);
            this.xrLabel105.Name = "xrLabel105";
            this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel105.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel105.StylePriority.UseBorders = false;
            this.xrLabel105.Text = "时间";
            // 
            // txt输血原因1
            // 
            this.txt输血原因1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt输血原因1.LocationFloat = new DevExpress.Utils.PointFloat(130.0001F, 1F);
            this.txt输血原因1.Name = "txt输血原因1";
            this.txt输血原因1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt输血原因1.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt输血原因1.StylePriority.UseBorders = false;
            this.txt输血原因1.StylePriority.UseTextAlignment = false;
            this.txt输血原因1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel107
            // 
            this.xrLabel107.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel107.LocationFloat = new DevExpress.Utils.PointFloat(80.00011F, 3.178914E-05F);
            this.xrLabel107.Name = "xrLabel107";
            this.xrLabel107.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel107.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel107.StylePriority.UseBorders = false;
            this.xrLabel107.Text = "原因1";
            // 
            // xrLabel108
            // 
            this.xrLabel108.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(10.0001F, 0F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.xrLabel108.StylePriority.UseBorders = false;
            this.xrLabel108.StylePriority.UseTextAlignment = false;
            this.xrLabel108.Text = "1 无 2 有";
            this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel109
            // 
            this.xrLabel109.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel109.LocationFloat = new DevExpress.Utils.PointFloat(485F, 0F);
            this.xrLabel109.Name = "xrLabel109";
            this.xrLabel109.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel109.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel109.StylePriority.UseBorders = false;
            this.xrLabel109.Text = "时间";
            // 
            // txt输血时间2
            // 
            this.txt输血时间2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt输血时间2.LocationFloat = new DevExpress.Utils.PointFloat(535.0001F, 1F);
            this.txt输血时间2.Name = "txt输血时间2";
            this.txt输血时间2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt输血时间2.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt输血时间2.StylePriority.UseBorders = false;
            this.txt输血时间2.StylePriority.UseTextAlignment = false;
            this.txt输血时间2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt输血原因2
            // 
            this.txt输血原因2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt输血原因2.LocationFloat = new DevExpress.Utils.PointFloat(405.0001F, 1F);
            this.txt输血原因2.Name = "txt输血原因2";
            this.txt输血原因2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt输血原因2.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt输血原因2.StylePriority.UseBorders = false;
            this.txt输血原因2.StylePriority.UseTextAlignment = false;
            this.txt输血原因2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel112
            // 
            this.xrLabel112.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel112.LocationFloat = new DevExpress.Utils.PointFloat(355.0001F, 0F);
            this.xrLabel112.Name = "xrLabel112";
            this.xrLabel112.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel112.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel112.StylePriority.UseBorders = false;
            this.xrLabel112.Text = "原因2";
            // 
            // txt输血有无
            // 
            this.txt输血有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt输血有无.CanGrow = false;
            this.txt输血有无.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt输血有无.LocationFloat = new DevExpress.Utils.PointFloat(617.0001F, 5.000051F);
            this.txt输血有无.Name = "txt输血有无";
            this.txt输血有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt输血有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt输血有无.StylePriority.UseBorders = false;
            this.txt输血有无.StylePriority.UseFont = false;
            // 
            // xrTable15
            // 
            this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(29.99989F, 641.875F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable15.SizeF = new System.Drawing.SizeF(710F, 25F);
            this.xrTable15.StylePriority.UseBorders = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell35});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Text = "外伤";
            this.xrTableCell34.Weight = 0.29577469086982833D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel92,
            this.txt外伤时间1,
            this.xrLabel94,
            this.txt外伤名称1,
            this.xrLabel96,
            this.xrLabel97,
            this.xrLabel98,
            this.txt外伤时间2,
            this.txt外伤名称2,
            this.xrLabel101,
            this.txt外伤有无});
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Weight = 2.7042253091301718D;
            // 
            // xrLabel92
            // 
            this.xrLabel92.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel92.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(340.0001F, 4.999987F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel92.StylePriority.UseBorders = false;
            this.xrLabel92.StylePriority.UseFont = false;
            this.xrLabel92.Text = "/";
            // 
            // txt外伤时间1
            // 
            this.txt外伤时间1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt外伤时间1.LocationFloat = new DevExpress.Utils.PointFloat(260.0001F, 1F);
            this.txt外伤时间1.Name = "txt外伤时间1";
            this.txt外伤时间1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt外伤时间1.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt外伤时间1.StylePriority.UseBorders = false;
            this.txt外伤时间1.StylePriority.UseTextAlignment = false;
            this.txt外伤时间1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel94
            // 
            this.xrLabel94.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(210.0001F, 9.536743E-05F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel94.StylePriority.UseBorders = false;
            this.xrLabel94.Text = "时间";
            // 
            // txt外伤名称1
            // 
            this.txt外伤名称1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt外伤名称1.LocationFloat = new DevExpress.Utils.PointFloat(130.0002F, 1.000023F);
            this.txt外伤名称1.Name = "txt外伤名称1";
            this.txt外伤名称1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt外伤名称1.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt外伤名称1.StylePriority.UseBorders = false;
            this.txt外伤名称1.StylePriority.UseTextAlignment = false;
            this.txt外伤名称1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel96
            // 
            this.xrLabel96.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(80.00011F, 3.178914E-05F);
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel96.StylePriority.UseBorders = false;
            this.xrLabel96.Text = "名称1";
            // 
            // xrLabel97
            // 
            this.xrLabel97.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel97.LocationFloat = new DevExpress.Utils.PointFloat(10.0001F, 0F);
            this.xrLabel97.Name = "xrLabel97";
            this.xrLabel97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel97.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.xrLabel97.StylePriority.UseBorders = false;
            this.xrLabel97.StylePriority.UseTextAlignment = false;
            this.xrLabel97.Text = "1 无 2 有";
            this.xrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel98
            // 
            this.xrLabel98.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(485F, 0F);
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel98.StylePriority.UseBorders = false;
            this.xrLabel98.Text = "时间";
            // 
            // txt外伤时间2
            // 
            this.txt外伤时间2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt外伤时间2.LocationFloat = new DevExpress.Utils.PointFloat(535.0001F, 1F);
            this.txt外伤时间2.Name = "txt外伤时间2";
            this.txt外伤时间2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt外伤时间2.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt外伤时间2.StylePriority.UseBorders = false;
            this.txt外伤时间2.StylePriority.UseTextAlignment = false;
            this.txt外伤时间2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt外伤名称2
            // 
            this.txt外伤名称2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt外伤名称2.LocationFloat = new DevExpress.Utils.PointFloat(405.0001F, 1F);
            this.txt外伤名称2.Name = "txt外伤名称2";
            this.txt外伤名称2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt外伤名称2.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt外伤名称2.StylePriority.UseBorders = false;
            this.txt外伤名称2.StylePriority.UseTextAlignment = false;
            this.txt外伤名称2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel101
            // 
            this.xrLabel101.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel101.LocationFloat = new DevExpress.Utils.PointFloat(355.0001F, 0F);
            this.xrLabel101.Name = "xrLabel101";
            this.xrLabel101.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel101.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel101.StylePriority.UseBorders = false;
            this.xrLabel101.Text = "名称2";
            // 
            // txt外伤有无
            // 
            this.txt外伤有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt外伤有无.CanGrow = false;
            this.txt外伤有无.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt外伤有无.LocationFloat = new DevExpress.Utils.PointFloat(617.0001F, 5.000051F);
            this.txt外伤有无.Name = "txt外伤有无";
            this.txt外伤有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt外伤有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt外伤有无.StylePriority.UseBorders = false;
            this.txt外伤有无.StylePriority.UseFont = false;
            // 
            // xrTable14
            // 
            this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(29.99989F, 616.875F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable14.SizeF = new System.Drawing.SizeF(710F, 25F);
            this.xrTable14.StylePriority.UseBorders = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Text = "手术";
            this.xrTableCell32.Weight = 0.29577469086982833D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel86,
            this.txt手术时间1,
            this.xrLabel84,
            this.txt手术名称1,
            this.xrLabel76,
            this.xrLabel75,
            this.xrLabel89,
            this.txt手术时间2,
            this.txt手术名称2,
            this.xrLabel91,
            this.txt手术有无});
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Weight = 2.7042253091301718D;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel86.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(340.0001F, 4.999987F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel86.StylePriority.UseBorders = false;
            this.xrLabel86.StylePriority.UseFont = false;
            this.xrLabel86.Text = "/";
            // 
            // txt手术时间1
            // 
            this.txt手术时间1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt手术时间1.LocationFloat = new DevExpress.Utils.PointFloat(260.0001F, 1F);
            this.txt手术时间1.Name = "txt手术时间1";
            this.txt手术时间1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt手术时间1.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt手术时间1.StylePriority.UseBorders = false;
            this.txt手术时间1.StylePriority.UseTextAlignment = false;
            this.txt手术时间1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(210.0001F, 9.536743E-05F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.Text = "时间";
            // 
            // txt手术名称1
            // 
            this.txt手术名称1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt手术名称1.LocationFloat = new DevExpress.Utils.PointFloat(130.0001F, 1.000061F);
            this.txt手术名称1.Name = "txt手术名称1";
            this.txt手术名称1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt手术名称1.SizeF = new System.Drawing.SizeF(79.99998F, 20F);
            this.txt手术名称1.StylePriority.UseBorders = false;
            this.txt手术名称1.StylePriority.UseTextAlignment = false;
            this.txt手术名称1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(80.00011F, 3.178914E-05F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel76.StylePriority.UseBorders = false;
            this.xrLabel76.Text = "名称1";
            // 
            // xrLabel75
            // 
            this.xrLabel75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(10.0001F, 0F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.xrLabel75.StylePriority.UseBorders = false;
            this.xrLabel75.StylePriority.UseTextAlignment = false;
            this.xrLabel75.Text = "1 无 2 有";
            this.xrLabel75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel89
            // 
            this.xrLabel89.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel89.LocationFloat = new DevExpress.Utils.PointFloat(485F, 0F);
            this.xrLabel89.Name = "xrLabel89";
            this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel89.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel89.StylePriority.UseBorders = false;
            this.xrLabel89.Text = "时间";
            // 
            // txt手术时间2
            // 
            this.txt手术时间2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt手术时间2.LocationFloat = new DevExpress.Utils.PointFloat(535.0001F, 1F);
            this.txt手术时间2.Name = "txt手术时间2";
            this.txt手术时间2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt手术时间2.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt手术时间2.StylePriority.UseBorders = false;
            this.txt手术时间2.StylePriority.UseTextAlignment = false;
            this.txt手术时间2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt手术名称2
            // 
            this.txt手术名称2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt手术名称2.LocationFloat = new DevExpress.Utils.PointFloat(405.0001F, 1F);
            this.txt手术名称2.Name = "txt手术名称2";
            this.txt手术名称2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt手术名称2.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.txt手术名称2.StylePriority.UseBorders = false;
            this.txt手术名称2.StylePriority.UseTextAlignment = false;
            this.txt手术名称2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel91
            // 
            this.xrLabel91.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel91.LocationFloat = new DevExpress.Utils.PointFloat(355.0001F, 0F);
            this.xrLabel91.Name = "xrLabel91";
            this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel91.SizeF = new System.Drawing.SizeF(50F, 23F);
            this.xrLabel91.StylePriority.UseBorders = false;
            this.xrLabel91.Text = "名称2";
            // 
            // txt手术有无
            // 
            this.txt手术有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt手术有无.CanGrow = false;
            this.txt手术有无.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt手术有无.LocationFloat = new DevExpress.Utils.PointFloat(617.0001F, 5.000051F);
            this.txt手术有无.Name = "txt手术有无";
            this.txt手术有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt手术有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt手术有无.StylePriority.UseBorders = false;
            this.txt手术有无.StylePriority.UseFont = false;
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(29.99989F, 496.875F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable13.SizeF = new System.Drawing.SizeF(710F, 120F);
            this.xrTable13.StylePriority.UseBorders = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Text = "疾病";
            this.xrTableCell30.Weight = 0.29577462639607166D;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt疾病其他,
            this.xrLabel55,
            this.txt疾病6确诊时间,
            this.txt疾病3确诊时间,
            this.xrLabel79,
            this.xrLabel80,
            this.txt疾病6,
            this.txt疾病3,
            this.xrLabel67,
            this.xrLabel68,
            this.txt疾病5确诊时间,
            this.txt疾病2确诊时间,
            this.xrLabel71,
            this.xrLabel72,
            this.txt疾病5,
            this.txt疾病2,
            this.xrLabel66,
            this.xrLabel65,
            this.txt疾病4确诊时间,
            this.txt疾病1确诊时间,
            this.xrLabel62,
            this.xrLabel61,
            this.txt疾病4,
            this.txt疾病1,
            this.txt职业病,
            this.txt恶性肿瘤,
            this.xrLabel52,
            this.xrLabel46});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 2.7042253736039283D;
            // 
            // txt疾病其他
            // 
            this.txt疾病其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt疾病其他.LocationFloat = new DevExpress.Utils.PointFloat(80.00024F, 46.00012F);
            this.txt疾病其他.Name = "txt疾病其他";
            this.txt疾病其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病其他.SizeF = new System.Drawing.SizeF(155.5999F, 23F);
            this.txt疾病其他.StylePriority.UseBorders = false;
            this.txt疾病其他.StylePriority.UseTextAlignment = false;
            this.txt疾病其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(10.00026F, 46.00003F);
            this.xrLabel55.Multiline = true;
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.xrLabel55.StylePriority.UseBorders = false;
            this.xrLabel55.StylePriority.UseTextAlignment = false;
            this.xrLabel55.Text = "13 其他\r\n";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt疾病6确诊时间
            // 
            this.txt疾病6确诊时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt疾病6确诊时间.LocationFloat = new DevExpress.Utils.PointFloat(535.0001F, 95.99995F);
            this.txt疾病6确诊时间.Name = "txt疾病6确诊时间";
            this.txt疾病6确诊时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病6确诊时间.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.txt疾病6确诊时间.StylePriority.UseBorders = false;
            this.txt疾病6确诊时间.StylePriority.UseTextAlignment = false;
            this.txt疾病6确诊时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt疾病3确诊时间
            // 
            this.txt疾病3确诊时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt疾病3确诊时间.LocationFloat = new DevExpress.Utils.PointFloat(535.0001F, 71.99998F);
            this.txt疾病3确诊时间.Name = "txt疾病3确诊时间";
            this.txt疾病3确诊时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病3确诊时间.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.txt疾病3确诊时间.StylePriority.UseBorders = false;
            this.txt疾病3确诊时间.StylePriority.UseTextAlignment = false;
            this.txt疾病3确诊时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel79
            // 
            this.xrLabel79.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(467.0001F, 73.99998F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(66F, 20F);
            this.xrLabel79.StylePriority.UseBorders = false;
            this.xrLabel79.Text = "确诊时间";
            // 
            // xrLabel80
            // 
            this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(467.0001F, 98.99995F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(66F, 20F);
            this.xrLabel80.StylePriority.UseBorders = false;
            this.xrLabel80.Text = "确诊时间";
            // 
            // txt疾病6
            // 
            this.txt疾病6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt疾病6.CanGrow = false;
            this.txt疾病6.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt疾病6.LocationFloat = new DevExpress.Utils.PointFloat(447.0001F, 98.99994F);
            this.txt疾病6.Name = "txt疾病6";
            this.txt疾病6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病6.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt疾病6.StylePriority.UseBorders = false;
            this.txt疾病6.StylePriority.UseFont = false;
            // 
            // txt疾病3
            // 
            this.txt疾病3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt疾病3.CanGrow = false;
            this.txt疾病3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt疾病3.LocationFloat = new DevExpress.Utils.PointFloat(447.0001F, 74F);
            this.txt疾病3.Name = "txt疾病3";
            this.txt疾病3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病3.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt疾病3.StylePriority.UseBorders = false;
            this.txt疾病3.StylePriority.UseFont = false;
            // 
            // xrLabel67
            // 
            this.xrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel67.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(430F, 99.99997F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel67.StylePriority.UseBorders = false;
            this.xrLabel67.StylePriority.UseFont = false;
            this.xrLabel67.Text = "/";
            // 
            // xrLabel68
            // 
            this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel68.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(430F, 73.00005F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel68.StylePriority.UseBorders = false;
            this.xrLabel68.StylePriority.UseFont = false;
            this.xrLabel68.Text = "/";
            // 
            // txt疾病5确诊时间
            // 
            this.txt疾病5确诊时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt疾病5确诊时间.LocationFloat = new DevExpress.Utils.PointFloat(322F, 95.99998F);
            this.txt疾病5确诊时间.Name = "txt疾病5确诊时间";
            this.txt疾病5确诊时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病5确诊时间.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.txt疾病5确诊时间.StylePriority.UseBorders = false;
            this.txt疾病5确诊时间.StylePriority.UseTextAlignment = false;
            this.txt疾病5确诊时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt疾病2确诊时间
            // 
            this.txt疾病2确诊时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt疾病2确诊时间.LocationFloat = new DevExpress.Utils.PointFloat(322F, 72.99998F);
            this.txt疾病2确诊时间.Name = "txt疾病2确诊时间";
            this.txt疾病2确诊时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病2确诊时间.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.txt疾病2确诊时间.StylePriority.UseBorders = false;
            this.txt疾病2确诊时间.StylePriority.UseTextAlignment = false;
            this.txt疾病2确诊时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel71
            // 
            this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(256F, 73.00002F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(66F, 20F);
            this.xrLabel71.StylePriority.UseBorders = false;
            this.xrLabel71.Text = "确诊时间";
            // 
            // xrLabel72
            // 
            this.xrLabel72.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(256F, 97.99995F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(66F, 20F);
            this.xrLabel72.StylePriority.UseBorders = false;
            this.xrLabel72.Text = "确诊时间";
            // 
            // txt疾病5
            // 
            this.txt疾病5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt疾病5.CanGrow = false;
            this.txt疾病5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt疾病5.LocationFloat = new DevExpress.Utils.PointFloat(236F, 98.99994F);
            this.txt疾病5.Name = "txt疾病5";
            this.txt疾病5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病5.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt疾病5.StylePriority.UseBorders = false;
            this.txt疾病5.StylePriority.UseFont = false;
            // 
            // txt疾病2
            // 
            this.txt疾病2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt疾病2.CanGrow = false;
            this.txt疾病2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt疾病2.LocationFloat = new DevExpress.Utils.PointFloat(236F, 73.00006F);
            this.txt疾病2.Name = "txt疾病2";
            this.txt疾病2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt疾病2.StylePriority.UseBorders = false;
            this.txt疾病2.StylePriority.UseFont = false;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(216.0002F, 99.99997F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel66.StylePriority.UseBorders = false;
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.Text = "/";
            // 
            // xrLabel65
            // 
            this.xrLabel65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel65.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(216.0002F, 75F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel65.StylePriority.UseBorders = false;
            this.xrLabel65.StylePriority.UseFont = false;
            this.xrLabel65.Text = "/";
            // 
            // txt疾病4确诊时间
            // 
            this.txt疾病4确诊时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt疾病4确诊时间.LocationFloat = new DevExpress.Utils.PointFloat(108.0002F, 96.99995F);
            this.txt疾病4确诊时间.Name = "txt疾病4确诊时间";
            this.txt疾病4确诊时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病4确诊时间.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.txt疾病4确诊时间.StylePriority.UseBorders = false;
            this.txt疾病4确诊时间.StylePriority.UseTextAlignment = false;
            this.txt疾病4确诊时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt疾病1确诊时间
            // 
            this.txt疾病1确诊时间.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt疾病1确诊时间.LocationFloat = new DevExpress.Utils.PointFloat(108.0002F, 75F);
            this.txt疾病1确诊时间.Name = "txt疾病1确诊时间";
            this.txt疾病1确诊时间.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病1确诊时间.SizeF = new System.Drawing.SizeF(100F, 20F);
            this.txt疾病1确诊时间.StylePriority.UseBorders = false;
            this.txt疾病1确诊时间.StylePriority.UseTextAlignment = false;
            this.txt疾病1确诊时间.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(41.54179F, 75F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(66F, 20F);
            this.xrLabel62.StylePriority.UseBorders = false;
            this.xrLabel62.Text = "确诊时间";
            // 
            // xrLabel61
            // 
            this.xrLabel61.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(41.54177F, 97.99994F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(66F, 20F);
            this.xrLabel61.StylePriority.UseBorders = false;
            this.xrLabel61.Text = "确诊时间";
            // 
            // txt疾病4
            // 
            this.txt疾病4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt疾病4.CanGrow = false;
            this.txt疾病4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt疾病4.LocationFloat = new DevExpress.Utils.PointFloat(21.54179F, 98F);
            this.txt疾病4.Name = "txt疾病4";
            this.txt疾病4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病4.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt疾病4.StylePriority.UseBorders = false;
            this.txt疾病4.StylePriority.UseFont = false;
            // 
            // txt疾病1
            // 
            this.txt疾病1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt疾病1.CanGrow = false;
            this.txt疾病1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt疾病1.LocationFloat = new DevExpress.Utils.PointFloat(21.54179F, 75F);
            this.txt疾病1.Name = "txt疾病1";
            this.txt疾病1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt疾病1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt疾病1.StylePriority.UseBorders = false;
            this.txt疾病1.StylePriority.UseFont = false;
            // 
            // txt职业病
            // 
            this.txt职业病.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt职业病.LocationFloat = new DevExpress.Utils.PointFloat(520.3996F, 23F);
            this.txt职业病.Name = "txt职业病";
            this.txt职业病.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt职业病.SizeF = new System.Drawing.SizeF(98.60028F, 23F);
            this.txt职业病.StylePriority.UseBorders = false;
            this.txt职业病.StylePriority.UseTextAlignment = false;
            this.txt职业病.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt恶性肿瘤
            // 
            this.txt恶性肿瘤.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt恶性肿瘤.LocationFloat = new DevExpress.Utils.PointFloat(484.6001F, 0F);
            this.txt恶性肿瘤.Name = "txt恶性肿瘤";
            this.txt恶性肿瘤.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt恶性肿瘤.SizeF = new System.Drawing.SizeF(134.3998F, 23F);
            this.txt恶性肿瘤.StylePriority.UseBorders = false;
            this.txt恶性肿瘤.StylePriority.UseTextAlignment = false;
            this.txt恶性肿瘤.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(10.0001F, 23F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(509.9999F, 23F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "7 脑卒中  8 严重精神障碍  9 结核病  10 肝炎  11 其他法定传染病 12 职业病";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(10.00013F, 0F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(474.6F, 23F);
            this.xrLabel46.StylePriority.UseBorders = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "1 无  2 高血压  3 糖尿病  4 冠心病  5 慢性阻塞性肺疾病  6 恶性肿瘤";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(0F, 496.875F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(30F, 195.0001F);
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.Text = "既往史";
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(0F, 470.875F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable12.SizeF = new System.Drawing.SizeF(740F, 26F);
            this.xrTable12.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.8666666666666667D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Text = "暴露史";
            this.xrTableCell28.Weight = 0.40540538478542021D;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel47,
            this.txt暴露史有无,
            this.txt暴露史1,
            this.xrLabel50,
            this.txt暴露史2,
            this.xrLabel53,
            this.xrLabel54});
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 2.5945946152145796D;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(555.9997F, 4.999987F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.Text = "/";
            // 
            // txt暴露史有无
            // 
            this.txt暴露史有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt暴露史有无.CanGrow = false;
            this.txt暴露史有无.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt暴露史有无.LocationFloat = new DevExpress.Utils.PointFloat(537.9998F, 4.999987F);
            this.txt暴露史有无.Name = "txt暴露史有无";
            this.txt暴露史有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt暴露史有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt暴露史有无.StylePriority.UseBorders = false;
            this.txt暴露史有无.StylePriority.UseFont = false;
            // 
            // txt暴露史1
            // 
            this.txt暴露史1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt暴露史1.CanGrow = false;
            this.txt暴露史1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt暴露史1.LocationFloat = new DevExpress.Utils.PointFloat(570.9997F, 4.999987F);
            this.txt暴露史1.Name = "txt暴露史1";
            this.txt暴露史1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt暴露史1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt暴露史1.StylePriority.UseBorders = false;
            this.txt暴露史1.StylePriority.UseFont = false;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel50.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(588.9998F, 4.999987F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel50.StylePriority.UseBorders = false;
            this.xrLabel50.StylePriority.UseFont = false;
            this.xrLabel50.Text = "/";
            // 
            // txt暴露史2
            // 
            this.txt暴露史2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt暴露史2.CanGrow = false;
            this.txt暴露史2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt暴露史2.LocationFloat = new DevExpress.Utils.PointFloat(603.9998F, 4.999987F);
            this.txt暴露史2.Name = "txt暴露史2";
            this.txt暴露史2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt暴露史2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt暴露史2.StylePriority.UseBorders = false;
            this.txt暴露史2.StylePriority.UseFont = false;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(95.00002F, 0F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(209.7998F, 23F);
            this.xrLabel53.StylePriority.UseBorders = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.Text = "2 化学品  3 毒物  4 射线";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(10.00005F, 0F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(85F, 23F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "1 无  有：";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(0F, 443.875F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable11.SizeF = new System.Drawing.SizeF(740F, 27F);
            this.xrTable11.StylePriority.UseBorders = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.xrTableCell27});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.9D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Text = "药物过敏史";
            this.xrTableCell26.Weight = 0.40540538478542021D;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel43,
            this.txt药物过敏史有无,
            this.xrLabel38,
            this.txt药物过敏史1,
            this.txt药物过敏史2,
            this.xrLabel41,
            this.txt药物过敏史99,
            this.txt药物过敏史其他,
            this.xrLabel36,
            this.xrLabel35});
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 2.5945946152145796D;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(522.9998F, 5.000019F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.Text = "/";
            // 
            // txt药物过敏史有无
            // 
            this.txt药物过敏史有无.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt药物过敏史有无.CanGrow = false;
            this.txt药物过敏史有无.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt药物过敏史有无.LocationFloat = new DevExpress.Utils.PointFloat(504.9998F, 5.000003F);
            this.txt药物过敏史有无.Name = "txt药物过敏史有无";
            this.txt药物过敏史有无.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt药物过敏史有无.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt药物过敏史有无.StylePriority.UseBorders = false;
            this.txt药物过敏史有无.StylePriority.UseFont = false;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(555.9997F, 4.999987F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.Text = "/";
            // 
            // txt药物过敏史1
            // 
            this.txt药物过敏史1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt药物过敏史1.CanGrow = false;
            this.txt药物过敏史1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt药物过敏史1.LocationFloat = new DevExpress.Utils.PointFloat(537.9998F, 4.999987F);
            this.txt药物过敏史1.Name = "txt药物过敏史1";
            this.txt药物过敏史1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt药物过敏史1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt药物过敏史1.StylePriority.UseBorders = false;
            this.txt药物过敏史1.StylePriority.UseFont = false;
            // 
            // txt药物过敏史2
            // 
            this.txt药物过敏史2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt药物过敏史2.CanGrow = false;
            this.txt药物过敏史2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt药物过敏史2.LocationFloat = new DevExpress.Utils.PointFloat(570.9997F, 4.999987F);
            this.txt药物过敏史2.Name = "txt药物过敏史2";
            this.txt药物过敏史2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt药物过敏史2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt药物过敏史2.StylePriority.UseBorders = false;
            this.txt药物过敏史2.StylePriority.UseFont = false;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(588.9998F, 4.999987F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel41.StylePriority.UseBorders = false;
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.Text = "/";
            // 
            // txt药物过敏史99
            // 
            this.txt药物过敏史99.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt药物过敏史99.CanGrow = false;
            this.txt药物过敏史99.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt药物过敏史99.LocationFloat = new DevExpress.Utils.PointFloat(603.9998F, 4.999987F);
            this.txt药物过敏史99.Name = "txt药物过敏史99";
            this.txt药物过敏史99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt药物过敏史99.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt药物过敏史99.StylePriority.UseBorders = false;
            this.txt药物过敏史99.StylePriority.UseFont = false;
            // 
            // txt药物过敏史其他
            // 
            this.txt药物过敏史其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt药物过敏史其他.LocationFloat = new DevExpress.Utils.PointFloat(355F, 0F);
            this.txt药物过敏史其他.Name = "txt药物过敏史其他";
            this.txt药物过敏史其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt药物过敏史其他.SizeF = new System.Drawing.SizeF(120F, 23F);
            this.txt药物过敏史其他.StylePriority.UseBorders = false;
            this.txt药物过敏史其他.StylePriority.UseTextAlignment = false;
            this.txt药物过敏史其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(95.00002F, 0F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(260F, 23F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "2 青霉素  3 磺胺  4 链霉素  5 其他";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(10.00005F, 0F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(85F, 23F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "1 无  有：    ";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(0F, 349.875F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(740F, 94F);
            this.xrTable10.StylePriority.UseBorders = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.xrTableCell25});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.97032227497408174D;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel17,
            this.xrLabel23});
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.40540538478542021D;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(80F, 23F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.Text = "医疗费用";
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 23.00002F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(80F, 23F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.Text = "支付方式";
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt贫困救助卡号,
            this.xrLabel49,
            this.xrLabel48,
            this.txt居民医保卡号,
            this.xrLabel44,
            this.xrLabel42,
            this.txt职工医保卡号,
            this.xrLabel40,
            this.xrLabel39,
            this.xrLabel34,
            this.txt医疗费用支付方式1,
            this.txt医疗费用支付方式2,
            this.xrLabel31,
            this.txt医疗费用支付方式99,
            this.txt医疗费用支付方式其他,
            this.xrLabel28});
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 2.5945946152145796D;
            // 
            // txt贫困救助卡号
            // 
            this.txt贫困救助卡号.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt贫困救助卡号.LocationFloat = new DevExpress.Utils.PointFloat(293.3334F, 45.99997F);
            this.txt贫困救助卡号.Name = "txt贫困救助卡号";
            this.txt贫困救助卡号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt贫困救助卡号.SizeF = new System.Drawing.SizeF(161.4666F, 23F);
            this.txt贫困救助卡号.StylePriority.UseBorders = false;
            this.txt贫困救助卡号.StylePriority.UseTextAlignment = false;
            this.txt贫困救助卡号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(210F, 45.99997F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(83.33347F, 23F);
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.StylePriority.UseTextAlignment = false;
            this.xrLabel49.Text = "卡号：";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(7.541664F, 45.99997F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(202.4583F, 23F);
            this.xrLabel48.StylePriority.UseBorders = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.Text = "3 贫困救助";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt居民医保卡号
            // 
            this.txt居民医保卡号.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt居民医保卡号.LocationFloat = new DevExpress.Utils.PointFloat(293.3334F, 23F);
            this.txt居民医保卡号.Name = "txt居民医保卡号";
            this.txt居民医保卡号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt居民医保卡号.SizeF = new System.Drawing.SizeF(161.4666F, 23F);
            this.txt居民医保卡号.StylePriority.UseBorders = false;
            this.txt居民医保卡号.StylePriority.UseTextAlignment = false;
            this.txt居民医保卡号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel44
            // 
            this.xrLabel44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(210.0001F, 23F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(83.33334F, 23F);
            this.xrLabel44.StylePriority.UseBorders = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = "医保卡号：";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(9.183762F, 23F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(200.8163F, 23F);
            this.xrLabel42.StylePriority.UseBorders = false;
            this.xrLabel42.StylePriority.UseTextAlignment = false;
            this.xrLabel42.Text = "2 居民基本医疗保险";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt职工医保卡号
            // 
            this.txt职工医保卡号.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt职工医保卡号.LocationFloat = new DevExpress.Utils.PointFloat(293.3334F, 0F);
            this.txt职工医保卡号.Name = "txt职工医保卡号";
            this.txt职工医保卡号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt职工医保卡号.SizeF = new System.Drawing.SizeF(161.4665F, 23F);
            this.txt职工医保卡号.StylePriority.UseBorders = false;
            this.txt职工医保卡号.StylePriority.UseTextAlignment = false;
            this.txt职工医保卡号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(210.0001F, 0F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(83.33334F, 23F);
            this.xrLabel40.StylePriority.UseBorders = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "医保卡号：";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(9.183762F, 0F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(200.8164F, 23F);
            this.xrLabel39.StylePriority.UseBorders = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "1 城镇或省直职工基本医疗保险";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(555.9997F, 28.00004F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.Text = "/";
            // 
            // txt医疗费用支付方式1
            // 
            this.txt医疗费用支付方式1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt医疗费用支付方式1.CanGrow = false;
            this.txt医疗费用支付方式1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt医疗费用支付方式1.LocationFloat = new DevExpress.Utils.PointFloat(537.9998F, 28.00004F);
            this.txt医疗费用支付方式1.Name = "txt医疗费用支付方式1";
            this.txt医疗费用支付方式1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt医疗费用支付方式1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt医疗费用支付方式1.StylePriority.UseBorders = false;
            this.txt医疗费用支付方式1.StylePriority.UseFont = false;
            // 
            // txt医疗费用支付方式2
            // 
            this.txt医疗费用支付方式2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt医疗费用支付方式2.CanGrow = false;
            this.txt医疗费用支付方式2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt医疗费用支付方式2.LocationFloat = new DevExpress.Utils.PointFloat(570.9997F, 28.00004F);
            this.txt医疗费用支付方式2.Name = "txt医疗费用支付方式2";
            this.txt医疗费用支付方式2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt医疗费用支付方式2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt医疗费用支付方式2.StylePriority.UseBorders = false;
            this.txt医疗费用支付方式2.StylePriority.UseFont = false;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(588.9998F, 28.00004F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(15F, 18F);
            this.xrLabel31.StylePriority.UseBorders = false;
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.Text = "/";
            // 
            // txt医疗费用支付方式99
            // 
            this.txt医疗费用支付方式99.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt医疗费用支付方式99.CanGrow = false;
            this.txt医疗费用支付方式99.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt医疗费用支付方式99.LocationFloat = new DevExpress.Utils.PointFloat(603.9998F, 28.00004F);
            this.txt医疗费用支付方式99.Name = "txt医疗费用支付方式99";
            this.txt医疗费用支付方式99.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt医疗费用支付方式99.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt医疗费用支付方式99.StylePriority.UseBorders = false;
            this.txt医疗费用支付方式99.StylePriority.UseFont = false;
            // 
            // txt医疗费用支付方式其他
            // 
            this.txt医疗费用支付方式其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt医疗费用支付方式其他.LocationFloat = new DevExpress.Utils.PointFloat(305F, 68.99997F);
            this.txt医疗费用支付方式其他.Name = "txt医疗费用支付方式其他";
            this.txt医疗费用支付方式其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt医疗费用支付方式其他.SizeF = new System.Drawing.SizeF(149.8F, 23F);
            this.txt医疗费用支付方式其他.StylePriority.UseBorders = false;
            this.txt医疗费用支付方式其他.StylePriority.UseTextAlignment = false;
            this.txt医疗费用支付方式其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(7.541664F, 68.99997F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(297.4583F, 23F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "4 商业医疗保险  5 全公费  6 全自费  7 其他";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(0F, 322.875F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(740F, 27F);
            this.xrTable9.StylePriority.UseBorders = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell23});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.9D;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "婚姻状况";
            this.xrTableCell22.Weight = 0.40540538478542021D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt婚姻状况,
            this.xrLabel19});
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 2.59459461521458D;
            // 
            // txt婚姻状况
            // 
            this.txt婚姻状况.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt婚姻状况.CanGrow = false;
            this.txt婚姻状况.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt婚姻状况.LocationFloat = new DevExpress.Utils.PointFloat(603.9998F, 7.000001F);
            this.txt婚姻状况.Name = "txt婚姻状况";
            this.txt婚姻状况.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt婚姻状况.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt婚姻状况.StylePriority.UseBorders = false;
            this.txt婚姻状况.StylePriority.UseFont = false;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 2F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(420F, 25F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "1 未婚    2 已婚    3 丧偶    4 离婚    5 未说明的婚姻状况";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 252.875F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(740F, 70F);
            this.xrTable8.StylePriority.UseBorders = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell21});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.93333333333333335D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Text = "职业";
            this.xrTableCell20.Weight = 0.40540538478542021D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt职业,
            this.xrLabel26,
            this.xrLabel24,
            this.xrLabel22});
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Weight = 2.5945946152145796D;
            // 
            // txt职业
            // 
            this.txt职业.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt职业.CanGrow = false;
            this.txt职业.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt职业.LocationFloat = new DevExpress.Utils.PointFloat(603.9998F, 51.00005F);
            this.txt职业.Name = "txt职业";
            this.txt职业.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt职业.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt职业.StylePriority.UseBorders = false;
            this.txt职业.StylePriority.UseFont = false;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 46F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(444.8F, 23F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "及有关人员  6军人  7不便分类的其他从业人员  8无职业";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(9.999985F, 23F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(611.9999F, 23F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "3商业、服务业人员 4 农、林、牧、渔、水利业生产人员  5生产、运输设备操作人员";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 0F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(611.9998F, 23F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "0国家机关、党群组织、企业、事业单位负责人 1专业技术人员 2办事人员和有关人员";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 214.875F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(740F, 38F);
            this.xrTable7.StylePriority.UseBorders = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell19});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Text = "文化程度";
            this.xrTableCell18.Weight = 0.40540538478542021D;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt文化程度,
            this.xrLabel25});
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 2.59459461521458D;
            // 
            // txt文化程度
            // 
            this.txt文化程度.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt文化程度.CanGrow = false;
            this.txt文化程度.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt文化程度.LocationFloat = new DevExpress.Utils.PointFloat(603.9998F, 7.000001F);
            this.txt文化程度.Name = "txt文化程度";
            this.txt文化程度.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt文化程度.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt文化程度.StylePriority.UseBorders = false;
            this.txt文化程度.StylePriority.UseFont = false;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 3.051758E-05F);
            this.xrLabel25.Multiline = true;
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(550F, 37.99997F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "1研究生2大学本科3大学专科和专科学校4中等专业学校5技工学校6高中7初中\r\n8小学9文盲或半文盲";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable6
            // 
            this.xrTable6.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 184.875F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow6});
            this.xrTable6.SizeF = new System.Drawing.SizeF(740F, 30F);
            this.xrTable6.StylePriority.UseBorders = false;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell17});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Text = "血型";
            this.xrTableCell16.Weight = 0.40540538478542021D;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel20,
            this.txtRH阴性,
            this.txt血型,
            this.xrLabel21});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 2.59459461521458D;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(589.5999F, 7F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(14.3999F, 18F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.Text = "/";
            // 
            // txtRH阴性
            // 
            this.txtRH阴性.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txtRH阴性.CanGrow = false;
            this.txtRH阴性.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txtRH阴性.LocationFloat = new DevExpress.Utils.PointFloat(603.9998F, 7.000001F);
            this.txtRH阴性.Name = "txtRH阴性";
            this.txtRH阴性.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtRH阴性.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txtRH阴性.StylePriority.UseBorders = false;
            this.txtRH阴性.StylePriority.UseFont = false;
            // 
            // txt血型
            // 
            this.txt血型.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt血型.CanGrow = false;
            this.txt血型.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt血型.LocationFloat = new DevExpress.Utils.PointFloat(574.6F, 7.000001F);
            this.txt血型.Name = "txt血型";
            this.txt血型.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt血型.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt血型.StylePriority.UseBorders = false;
            this.txt血型.StylePriority.UseFont = false;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(500F, 25F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "1 A型  2 B型  3 O型  4 AB型  5 不详  / RH 阴性： 1 否  2 是  3 不详";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 154.875F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(740F, 30F);
            this.xrTable5.StylePriority.UseBorders = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell15});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Text = "常住类型";
            this.xrTableCell13.Weight = 0.40540538478542021D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt常住类型,
            this.xrLabel15});
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Weight = 1.0726351557551204D;
            // 
            // txt常住类型
            // 
            this.txt常住类型.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt常住类型.CanGrow = false;
            this.txt常住类型.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt常住类型.LocationFloat = new DevExpress.Utils.PointFloat(232F, 4.999987F);
            this.txt常住类型.Name = "txt常住类型";
            this.txt常住类型.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt常住类型.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt常住类型.StylePriority.UseBorders = false;
            this.txt常住类型.StylePriority.UseFont = false;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 0F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(150F, 25F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "1 户籍  2 非户籍";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt民族,
            this.txt少数民族,
            this.xrLabel18,
            this.xrLabel13});
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 1.5219594594594597D;
            // 
            // txt民族
            // 
            this.txt民族.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt民族.CanGrow = false;
            this.txt民族.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt民族.LocationFloat = new DevExpress.Utils.PointFloat(339.4164F, 5.000019F);
            this.txt民族.Name = "txt民族";
            this.txt民族.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt民族.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt民族.StylePriority.UseBorders = false;
            this.txt民族.StylePriority.UseFont = false;
            // 
            // txt少数民族
            // 
            this.txt少数民族.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt少数民族.LocationFloat = new DevExpress.Utils.PointFloat(237.2012F, 0F);
            this.txt少数民族.Name = "txt少数民族";
            this.txt少数民族.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt少数民族.SizeF = new System.Drawing.SizeF(80F, 23F);
            this.txt少数民族.StylePriority.UseBorders = false;
            this.txt少数民族.StylePriority.UseTextAlignment = false;
            this.txt少数民族.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(95.41664F, 0F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(141.7846F, 25F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "1 汉族  99 少数民族";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(85.41663F, 30F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.Text = "民族";
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(364.5833F, 124.8749F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(375.4167F, 30F);
            this.xrTable4.StylePriority.UseBorders = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10,
            this.xrTableCell11,
            this.xrTableCell12});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt联系人姓名});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Weight = 0.69256749980431942D;
            // 
            // txt联系人姓名
            // 
            this.txt联系人姓名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt联系人姓名.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.txt联系人姓名.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2.000014F);
            this.txt联系人姓名.Name = "txt联系人姓名";
            this.txt联系人姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt联系人姓名.SizeF = new System.Drawing.SizeF(80F, 26F);
            this.txt联系人姓名.StylePriority.UseBorders = false;
            this.txt联系人姓名.StylePriority.UseFont = false;
            this.txt联系人姓名.StylePriority.UseTextAlignment = false;
            this.txt联系人姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "联系人电话";
            this.xrTableCell11.Weight = 0.810810816464677D;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt联系人电话});
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Weight = 1.540540595083731D;
            // 
            // txt联系人电话
            // 
            this.txt联系人电话.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt联系人电话.LocationFloat = new DevExpress.Utils.PointFloat(9.44252F, 0F);
            this.txt联系人电话.Name = "txt联系人电话";
            this.txt联系人电话.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt联系人电话.SizeF = new System.Drawing.SizeF(170F, 28F);
            this.txt联系人电话.StylePriority.UseBorders = false;
            this.txt联系人电话.StylePriority.UseTextAlignment = false;
            this.txt联系人电话.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 124.8749F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(364.5833F, 30F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "本人电话";
            this.xrTableCell7.Weight = 0.81081079019082558D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt本人电话});
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Weight = 1.3378379334438888D;
            // 
            // txt本人电话
            // 
            this.txt本人电话.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt本人电话.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.txt本人电话.Name = "txt本人电话";
            this.txt本人电话.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt本人电话.SizeF = new System.Drawing.SizeF(150F, 28F);
            this.txt本人电话.StylePriority.UseBorders = false;
            this.txt本人电话.StylePriority.UseTextAlignment = false;
            this.txt本人电话.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "联系人姓名";
            this.xrTableCell9.Weight = 0.80743243992630753D;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 94.87501F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(740F, 30F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "身份证号";
            this.xrTableCell4.Weight = 0.40540538478542021D;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt身份证号});
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Weight = 1.0726351557551204D;
            // 
            // txt身份证号
            // 
            this.txt身份证号.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt身份证号.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.txt身份证号.Name = "txt身份证号";
            this.txt身份证号.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt身份证号.SizeF = new System.Drawing.SizeF(240F, 28F);
            this.txt身份证号.StylePriority.UseBorders = false;
            this.txt身份证号.StylePriority.UseTextAlignment = false;
            this.txt身份证号.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt工作单位,
            this.xrLabel16});
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 1.5219594594594592D;
            // 
            // txt工作单位
            // 
            this.txt工作单位.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt工作单位.LocationFloat = new DevExpress.Utils.PointFloat(95.41663F, 0F);
            this.txt工作单位.Name = "txt工作单位";
            this.txt工作单位.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt工作单位.SizeF = new System.Drawing.SizeF(269.4424F, 28F);
            this.txt工作单位.StylePriority.UseBorders = false;
            this.txt工作单位.StylePriority.UseTextAlignment = false;
            this.txt工作单位.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(85.41666F, 30F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.Text = "工作单位";
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(574.9999F, 43.87501F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(15F, 18F);
            // 
            // xrLabel_编号1
            // 
            this.xrLabel_编号1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号1.LocationFloat = new DevExpress.Utils.PointFloat(499.9999F, 43.87501F);
            this.xrLabel_编号1.Name = "xrLabel_编号1";
            this.xrLabel_编号1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号1.StylePriority.UseBorders = false;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(434.375F, 41.87501F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "编号：";
            // 
            // txt姓名
            // 
            this.txt姓名.Font = new System.Drawing.Font("宋体", 11F);
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(73.95834F, 41.87501F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(136.0415F, 23F);
            this.txt姓名.StylePriority.UseFont = false;
            this.txt姓名.StylePriority.UseTextAlignment = false;
            this.txt姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("宋体", 11F);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(11.04167F, 41.87501F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(62.91666F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "姓名：";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 64.87501F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(740F, 30F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "性别";
            this.xrTableCell1.Weight = 0.405405415715398D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt性别,
            this.xrLabel12});
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 1.4189188673689561D;
            // 
            // txt性别
            // 
            this.txt性别.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt性别.CanGrow = false;
            this.txt性别.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt性别.LocationFloat = new DevExpress.Utils.PointFloat(321.9999F, 7.000001F);
            this.txt性别.Name = "txt性别";
            this.txt性别.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt性别.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt性别.StylePriority.UseBorders = false;
            this.txt性别.StylePriority.UseFont = false;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(295F, 30F);
            this.xrLabel12.StylePriority.UseBorders = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "0 未知的性别  1 男  2 女  9 未说明的性别";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt出生日期,
            this.xrLabel14});
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 1.175675716915646D;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt出生日期.LocationFloat = new DevExpress.Utils.PointFloat(109.4425F, 0F);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt出生日期.SizeF = new System.Drawing.SizeF(150F, 28F);
            this.txt出生日期.StylePriority.UseBorders = false;
            this.txt出生日期.StylePriority.UseTextAlignment = false;
            this.txt出生日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(100F, 30F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.Text = "出生日期";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(239.5999F, 12F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(290.4001F, 26.125F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "个人基本信息表";
            // 
            // xrLabel_编号2
            // 
            this.xrLabel_编号2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号2.LocationFloat = new DevExpress.Utils.PointFloat(524.9999F, 43.87501F);
            this.xrLabel_编号2.Name = "xrLabel_编号2";
            this.xrLabel_编号2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号2.StylePriority.UseBorders = false;
            // 
            // xrLabel_编号3
            // 
            this.xrLabel_编号3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号3.LocationFloat = new DevExpress.Utils.PointFloat(549.9999F, 43.87501F);
            this.xrLabel_编号3.Name = "xrLabel_编号3";
            this.xrLabel_编号3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号3.StylePriority.UseBorders = false;
            // 
            // xrLabel_编号4
            // 
            this.xrLabel_编号4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号4.LocationFloat = new DevExpress.Utils.PointFloat(599.9999F, 43.87501F);
            this.xrLabel_编号4.Name = "xrLabel_编号4";
            this.xrLabel_编号4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号4.StylePriority.UseBorders = false;
            // 
            // xrLabel_编号5
            // 
            this.xrLabel_编号5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号5.LocationFloat = new DevExpress.Utils.PointFloat(624.9999F, 43.87501F);
            this.xrLabel_编号5.Name = "xrLabel_编号5";
            this.xrLabel_编号5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号5.StylePriority.UseBorders = false;
            // 
            // xrLabel_编号6
            // 
            this.xrLabel_编号6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号6.LocationFloat = new DevExpress.Utils.PointFloat(649.9999F, 43.87501F);
            this.xrLabel_编号6.Name = "xrLabel_编号6";
            this.xrLabel_编号6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号6.StylePriority.UseBorders = false;
            // 
            // xrLabel_编号7
            // 
            this.xrLabel_编号7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号7.LocationFloat = new DevExpress.Utils.PointFloat(674.9999F, 43.87501F);
            this.xrLabel_编号7.Name = "xrLabel_编号7";
            this.xrLabel_编号7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号7.StylePriority.UseBorders = false;
            // 
            // xrLabel_编号8
            // 
            this.xrLabel_编号8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号8.LocationFloat = new DevExpress.Utils.PointFloat(699.9999F, 43.87501F);
            this.xrLabel_编号8.Name = "xrLabel_编号8";
            this.xrLabel_编号8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel_编号8.StylePriority.UseBorders = false;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 1F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 11F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report个人基本信息表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(43, 44, 1, 11);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号8;
        private DevExpress.XtraReports.UI.XRLabel txt性别;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel txt出生日期;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRLabel txt身份证号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel txt工作单位;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRLabel txt联系人电话;
        private DevExpress.XtraReports.UI.XRLabel txt本人电话;
        private DevExpress.XtraReports.UI.XRLabel txt联系人姓名;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRLabel txt常住类型;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel txt民族;
        private DevExpress.XtraReports.UI.XRLabel txt少数民族;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel txtRH阴性;
        private DevExpress.XtraReports.UI.XRLabel txt血型;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRLabel txt文化程度;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel txt职业;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRLabel txt婚姻状况;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel txt医疗费用支付方式1;
        private DevExpress.XtraReports.UI.XRLabel txt医疗费用支付方式2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel txt医疗费用支付方式99;
        private DevExpress.XtraReports.UI.XRLabel txt医疗费用支付方式其他;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel txt药物过敏史其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel txt药物过敏史1;
        private DevExpress.XtraReports.UI.XRLabel txt药物过敏史2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel txt药物过敏史99;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel txt药物过敏史有无;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel txt暴露史有无;
        private DevExpress.XtraReports.UI.XRLabel txt暴露史1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel txt暴露史2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRLabel txt职业病;
        private DevExpress.XtraReports.UI.XRLabel txt恶性肿瘤;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel txt疾病4确诊时间;
        private DevExpress.XtraReports.UI.XRLabel txt疾病1确诊时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel txt疾病4;
        private DevExpress.XtraReports.UI.XRLabel txt疾病1;
        private DevExpress.XtraReports.UI.XRLabel txt疾病6确诊时间;
        private DevExpress.XtraReports.UI.XRLabel txt疾病3确诊时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel txt疾病6;
        private DevExpress.XtraReports.UI.XRLabel txt疾病3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel txt疾病5确诊时间;
        private DevExpress.XtraReports.UI.XRLabel txt疾病2确诊时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRLabel txt疾病5;
        private DevExpress.XtraReports.UI.XRLabel txt疾病2;
        private DevExpress.XtraReports.UI.XRLabel txt疾病其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel txt手术时间1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel txt手术名称1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel xrLabel89;
        private DevExpress.XtraReports.UI.XRLabel txt手术时间2;
        private DevExpress.XtraReports.UI.XRLabel txt手术名称2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel91;
        private DevExpress.XtraReports.UI.XRLabel txt手术有无;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRLabel txt输血时间1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel105;
        private DevExpress.XtraReports.UI.XRLabel txt输血原因1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel107;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRLabel xrLabel109;
        private DevExpress.XtraReports.UI.XRLabel txt输血时间2;
        private DevExpress.XtraReports.UI.XRLabel txt输血原因2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel112;
        private DevExpress.XtraReports.UI.XRLabel txt输血有无;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel txt外伤时间1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel txt外伤名称1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRLabel xrLabel97;
        private DevExpress.XtraReports.UI.XRLabel xrLabel98;
        private DevExpress.XtraReports.UI.XRLabel txt外伤时间2;
        private DevExpress.XtraReports.UI.XRLabel txt外伤名称2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel101;
        private DevExpress.XtraReports.UI.XRLabel txt外伤有无;
        private DevExpress.XtraReports.UI.XRLabel xrLabel114;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel122;
        private DevExpress.XtraReports.UI.XRLabel txt家族史父亲5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel124;
        private DevExpress.XtraReports.UI.XRLabel txt家族史父亲99;
        private DevExpress.XtraReports.UI.XRLabel xrLabel115;
        private DevExpress.XtraReports.UI.XRLabel txt家族史父亲1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel117;
        private DevExpress.XtraReports.UI.XRLabel txt家族史父亲2;
        private DevExpress.XtraReports.UI.XRLabel txt家族史父亲3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel txt家族史父亲4;
        private DevExpress.XtraReports.UI.XRLabel txt家族史父亲其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel128;
        private DevExpress.XtraReports.UI.XRLabel txt家族史母亲5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel130;
        private DevExpress.XtraReports.UI.XRLabel txt家族史母亲99;
        private DevExpress.XtraReports.UI.XRLabel xrLabel132;
        private DevExpress.XtraReports.UI.XRLabel txt家族史母亲1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel134;
        private DevExpress.XtraReports.UI.XRLabel txt家族史母亲2;
        private DevExpress.XtraReports.UI.XRLabel txt家族史母亲3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel137;
        private DevExpress.XtraReports.UI.XRLabel txt家族史母亲4;
        private DevExpress.XtraReports.UI.XRLabel txt家族史母亲其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel127;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRLabel xrLabel140;
        private DevExpress.XtraReports.UI.XRLabel txt家族史兄弟姐妹5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel142;
        private DevExpress.XtraReports.UI.XRLabel txt家族史兄弟姐妹99;
        private DevExpress.XtraReports.UI.XRLabel xrLabel144;
        private DevExpress.XtraReports.UI.XRLabel txt家族史兄弟姐妹1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel146;
        private DevExpress.XtraReports.UI.XRLabel txt家族史兄弟姐妹2;
        private DevExpress.XtraReports.UI.XRLabel txt家族史兄弟姐妹3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel149;
        private DevExpress.XtraReports.UI.XRLabel txt家族史兄弟姐妹4;
        private DevExpress.XtraReports.UI.XRLabel txt家族史兄弟姐妹其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel152;
        private DevExpress.XtraReports.UI.XRLabel txt家族史子女5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel154;
        private DevExpress.XtraReports.UI.XRLabel txt家族史子女99;
        private DevExpress.XtraReports.UI.XRLabel xrLabel156;
        private DevExpress.XtraReports.UI.XRLabel txt家族史子女1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel158;
        private DevExpress.XtraReports.UI.XRLabel txt家族史子女2;
        private DevExpress.XtraReports.UI.XRLabel txt家族史子女3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel161;
        private DevExpress.XtraReports.UI.XRLabel txt家族史子女4;
        private DevExpress.XtraReports.UI.XRLabel txt家族史子女其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel164;
        private DevExpress.XtraReports.UI.XRLabel xrLabel166;
        private DevExpress.XtraReports.UI.XRLabel xrLabel165;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRLabel txt遗传病史有无;
        private DevExpress.XtraReports.UI.XRLabel txt遗传病史疾病名称;
        private DevExpress.XtraReports.UI.XRLabel xrLabel168;
        private DevExpress.XtraReports.UI.XRLabel xrLabel167;
        private DevExpress.XtraReports.UI.XRTable xrTable21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel174;
        private DevExpress.XtraReports.UI.XRLabel txt残疾情况4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel176;
        private DevExpress.XtraReports.UI.XRLabel txt残疾情况5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel178;
        private DevExpress.XtraReports.UI.XRLabel txt残疾情况0;
        private DevExpress.XtraReports.UI.XRLabel xrLabel180;
        private DevExpress.XtraReports.UI.XRLabel txt残疾情况1;
        private DevExpress.XtraReports.UI.XRLabel txt残疾情况2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel183;
        private DevExpress.XtraReports.UI.XRLabel txt残疾情况3;
        private DevExpress.XtraReports.UI.XRLabel txt残疾情况其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel172;
        private DevExpress.XtraReports.UI.XRLabel xrLabel171;
        private DevExpress.XtraReports.UI.XRLabel xrLabel185;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRLabel txt禽畜栏;
        private DevExpress.XtraReports.UI.XRTable xrTable25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRLabel txt厕所;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRLabel txt燃料类型;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRLabel txt饮水;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRLabel txt厨房排风设施;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel txt本人签字;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel txt居住情况;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell txt家庭结构;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell txt户主身份证;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell txt家庭人口数;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell txt户主姓名;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRLabel txt家族史疾病其他;
        private DevExpress.XtraReports.UI.XRLabel txt贫困救助卡号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel txt居民医保卡号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel txt职工医保卡号;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel txt签字时间;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel txt居住地址;
    }
}
