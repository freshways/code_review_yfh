﻿namespace AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息
{
    partial class report居民健康档案封面
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(report居民健康档案封面));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel_编号13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_编号17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_建档日期_日 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_建档日期_月 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_建档日期_年 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_责任医生 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_建档人 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_建档单位 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_居委会 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_街道 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_联系电话 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_现住址 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_户籍地址 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel_姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.pic照片 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pic照片,
            this.xrLabel64,
            this.xrLabel63,
            this.xrLabel_编号5,
            this.xrLabel59,
            this.xrLabel60,
            this.xrLabel_编号6,
            this.xrLabel_编号4,
            this.xrLabel_编号8,
            this.xrLabel54,
            this.xrLabel55,
            this.xrLabel_编号9,
            this.xrLabel_编号7,
            this.xrLine3,
            this.xrLabel51,
            this.xrLabel52,
            this.xrLine2,
            this.xrLabel49,
            this.xrLabel50,
            this.xrLabel48,
            this.xrLabel47,
            this.xrLabel_编号11,
            this.xrLabel43,
            this.xrLabel44,
            this.xrLabel_编号12,
            this.xrLabel_编号10,
            this.xrLabel_编号1,
            this.xrLine1,
            this.xrLabel_编号13,
            this.xrLabel_编号15,
            this.xrLabel37,
            this.xrLabel38,
            this.xrLabel_编号14,
            this.xrLabel_编号3,
            this.xrLabel33,
            this.xrLabel34,
            this.xrLabel_编号2,
            this.xrLabel_编号16,
            this.xrLabel31,
            this.xrLabel29,
            this.xrLabel_编号17,
            this.xrLabel26,
            this.xrLabel_建档日期_日,
            this.xrLabel_建档日期_月,
            this.xrLabel24,
            this.xrLabel23,
            this.xrLabel_建档日期_年,
            this.xrLabel_责任医生,
            this.xrLabel_建档人,
            this.xrLabel_建档单位,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel16,
            this.xrLabel15,
            this.xrLabel_居委会,
            this.xrLabel_街道,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel_联系电话,
            this.xrLabel_现住址,
            this.xrLabel_户籍地址,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel_姓名,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1});
            this.Detail.Font = new System.Drawing.Font("宋体", 9.75F);
            this.Detail.HeightF = 1169F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(47.4582F, 41.25001F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(57.99997F, 25F);
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "编号 ";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            // 
            // xrLabel63
            // 
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(204.4582F, 41.25001F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel_编号5
            // 
            this.xrLabel_编号5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号5.BorderWidth = 2F;
            this.xrLabel_编号5.CanGrow = false;
            this.xrLabel_编号5.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号5.LocationFloat = new DevExpress.Utils.PointFloat(253.4583F, 41.25001F);
            this.xrLabel_编号5.Name = "xrLabel_编号5";
            this.xrLabel_编号5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号5.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号5.StylePriority.UseBorders = false;
            this.xrLabel_编号5.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号5.StylePriority.UseFont = false;
            this.xrLabel_编号5.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel59
            // 
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(241.4583F, 41.25001F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel60
            // 
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(278.4583F, 41.25001F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel_编号6
            // 
            this.xrLabel_编号6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号6.BorderWidth = 2F;
            this.xrLabel_编号6.CanGrow = false;
            this.xrLabel_编号6.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号6.LocationFloat = new DevExpress.Utils.PointFloat(290.4582F, 41.25001F);
            this.xrLabel_编号6.Name = "xrLabel_编号6";
            this.xrLabel_编号6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号6.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号6.StylePriority.UseBorders = false;
            this.xrLabel_编号6.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号6.StylePriority.UseFont = false;
            this.xrLabel_编号6.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号4
            // 
            this.xrLabel_编号4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号4.BorderWidth = 2F;
            this.xrLabel_编号4.CanGrow = false;
            this.xrLabel_编号4.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号4.LocationFloat = new DevExpress.Utils.PointFloat(216.4583F, 41.25001F);
            this.xrLabel_编号4.Name = "xrLabel_编号4";
            this.xrLabel_编号4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号4.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号4.StylePriority.UseBorders = false;
            this.xrLabel_编号4.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号4.StylePriority.UseFont = false;
            this.xrLabel_编号4.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号8
            // 
            this.xrLabel_编号8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号8.BorderWidth = 2F;
            this.xrLabel_编号8.CanGrow = false;
            this.xrLabel_编号8.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号8.LocationFloat = new DevExpress.Utils.PointFloat(385.4583F, 41.25001F);
            this.xrLabel_编号8.Name = "xrLabel_编号8";
            this.xrLabel_编号8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号8.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号8.StylePriority.UseBorders = false;
            this.xrLabel_编号8.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号8.StylePriority.UseFont = false;
            this.xrLabel_编号8.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel54
            // 
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(373.4582F, 41.25001F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel55
            // 
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(410.4583F, 41.25001F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel_编号9
            // 
            this.xrLabel_编号9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号9.BorderWidth = 2F;
            this.xrLabel_编号9.CanGrow = false;
            this.xrLabel_编号9.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号9.LocationFloat = new DevExpress.Utils.PointFloat(422.4583F, 41.25001F);
            this.xrLabel_编号9.Name = "xrLabel_编号9";
            this.xrLabel_编号9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号9.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号9.StylePriority.UseBorders = false;
            this.xrLabel_编号9.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号9.StylePriority.UseFont = false;
            this.xrLabel_编号9.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号7
            // 
            this.xrLabel_编号7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号7.BorderWidth = 2F;
            this.xrLabel_编号7.CanGrow = false;
            this.xrLabel_编号7.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号7.LocationFloat = new DevExpress.Utils.PointFloat(348.4582F, 41.25001F);
            this.xrLabel_编号7.Name = "xrLabel_编号7";
            this.xrLabel_编号7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号7.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号7.StylePriority.UseBorders = false;
            this.xrLabel_编号7.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号7.StylePriority.UseFont = false;
            this.xrLabel_编号7.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(452.4583F, 41.25001F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(23F, 25F);
            // 
            // xrLabel51
            // 
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(475.4583F, 41.25001F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(5F, 25F);
            // 
            // xrLabel52
            // 
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(447.4583F, 41.25001F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(5F, 25F);
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(585.8333F, 41.25001F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(23F, 25F);
            // 
            // xrLabel49
            // 
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(608.8332F, 41.25001F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(5F, 25F);
            // 
            // xrLabel50
            // 
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(580.8332F, 41.25001F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(5F, 25F);
            // 
            // xrLabel48
            // 
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(315.4582F, 41.25001F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(5F, 25F);
            // 
            // xrLabel47
            // 
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(343.4583F, 41.25001F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(5F, 25F);
            // 
            // xrLabel_编号11
            // 
            this.xrLabel_编号11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号11.BorderWidth = 2F;
            this.xrLabel_编号11.CanGrow = false;
            this.xrLabel_编号11.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号11.LocationFloat = new DevExpress.Utils.PointFloat(517.6666F, 41.25001F);
            this.xrLabel_编号11.Name = "xrLabel_编号11";
            this.xrLabel_编号11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号11.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号11.StylePriority.UseBorders = false;
            this.xrLabel_编号11.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号11.StylePriority.UseFont = false;
            this.xrLabel_编号11.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel43
            // 
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(505.6666F, 41.25001F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel44
            // 
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(542.6666F, 41.25001F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel_编号12
            // 
            this.xrLabel_编号12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号12.BorderWidth = 2F;
            this.xrLabel_编号12.CanGrow = false;
            this.xrLabel_编号12.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号12.LocationFloat = new DevExpress.Utils.PointFloat(554.6666F, 41.25001F);
            this.xrLabel_编号12.Name = "xrLabel_编号12";
            this.xrLabel_编号12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号12.SizeF = new System.Drawing.SizeF(26.16663F, 25F);
            this.xrLabel_编号12.StylePriority.UseBorders = false;
            this.xrLabel_编号12.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号12.StylePriority.UseFont = false;
            this.xrLabel_编号12.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号10
            // 
            this.xrLabel_编号10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号10.BorderWidth = 2F;
            this.xrLabel_编号10.CanGrow = false;
            this.xrLabel_编号10.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号10.LocationFloat = new DevExpress.Utils.PointFloat(480.6666F, 41.25001F);
            this.xrLabel_编号10.Name = "xrLabel_编号10";
            this.xrLabel_编号10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号10.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号10.StylePriority.UseBorders = false;
            this.xrLabel_编号10.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号10.StylePriority.UseFont = false;
            this.xrLabel_编号10.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号1
            // 
            this.xrLabel_编号1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号1.BorderWidth = 2F;
            this.xrLabel_编号1.CanGrow = false;
            this.xrLabel_编号1.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号1.LocationFloat = new DevExpress.Utils.PointFloat(105.4583F, 41.25001F);
            this.xrLabel_编号1.Name = "xrLabel_编号1";
            this.xrLabel_编号1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号1.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号1.StylePriority.UseBorders = false;
            this.xrLabel_编号1.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号1.StylePriority.UseFont = false;
            this.xrLabel_编号1.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(320.4582F, 41.25001F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(23F, 25F);
            // 
            // xrLabel_编号13
            // 
            this.xrLabel_编号13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号13.BorderWidth = 2F;
            this.xrLabel_编号13.CanGrow = false;
            this.xrLabel_编号13.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号13.LocationFloat = new DevExpress.Utils.PointFloat(613.8333F, 41.25001F);
            this.xrLabel_编号13.Name = "xrLabel_编号13";
            this.xrLabel_编号13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号13.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号13.StylePriority.UseBorders = false;
            this.xrLabel_编号13.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号13.StylePriority.UseFont = false;
            this.xrLabel_编号13.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号15
            // 
            this.xrLabel_编号15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号15.BorderWidth = 2F;
            this.xrLabel_编号15.CanGrow = false;
            this.xrLabel_编号15.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号15.LocationFloat = new DevExpress.Utils.PointFloat(687.8331F, 41.25001F);
            this.xrLabel_编号15.Name = "xrLabel_编号15";
            this.xrLabel_编号15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号15.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号15.StylePriority.UseBorders = false;
            this.xrLabel_编号15.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号15.StylePriority.UseFont = false;
            this.xrLabel_编号15.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel37
            // 
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(675.8331F, 41.25001F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel38
            // 
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(638.8333F, 41.25001F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel_编号14
            // 
            this.xrLabel_编号14.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号14.BorderWidth = 2F;
            this.xrLabel_编号14.CanGrow = false;
            this.xrLabel_编号14.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号14.LocationFloat = new DevExpress.Utils.PointFloat(650.8332F, 41.25001F);
            this.xrLabel_编号14.Name = "xrLabel_编号14";
            this.xrLabel_编号14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号14.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号14.StylePriority.UseBorders = false;
            this.xrLabel_编号14.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号14.StylePriority.UseFont = false;
            this.xrLabel_编号14.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号3
            // 
            this.xrLabel_编号3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号3.BorderWidth = 2F;
            this.xrLabel_编号3.CanGrow = false;
            this.xrLabel_编号3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号3.LocationFloat = new DevExpress.Utils.PointFloat(179.4582F, 41.25001F);
            this.xrLabel_编号3.Name = "xrLabel_编号3";
            this.xrLabel_编号3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号3.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号3.StylePriority.UseBorders = false;
            this.xrLabel_编号3.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号3.StylePriority.UseFont = false;
            this.xrLabel_编号3.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel33
            // 
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(167.4582F, 41.25001F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel34
            // 
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(130.4582F, 41.25001F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel_编号2
            // 
            this.xrLabel_编号2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号2.BorderWidth = 2F;
            this.xrLabel_编号2.CanGrow = false;
            this.xrLabel_编号2.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号2.LocationFloat = new DevExpress.Utils.PointFloat(142.4582F, 41.25001F);
            this.xrLabel_编号2.Name = "xrLabel_编号2";
            this.xrLabel_编号2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号2.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号2.StylePriority.UseBorders = false;
            this.xrLabel_编号2.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号2.StylePriority.UseFont = false;
            this.xrLabel_编号2.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel_编号16
            // 
            this.xrLabel_编号16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号16.BorderWidth = 2F;
            this.xrLabel_编号16.CanGrow = false;
            this.xrLabel_编号16.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号16.LocationFloat = new DevExpress.Utils.PointFloat(724.8332F, 41.25001F);
            this.xrLabel_编号16.Name = "xrLabel_编号16";
            this.xrLabel_编号16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号16.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号16.StylePriority.UseBorders = false;
            this.xrLabel_编号16.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号16.StylePriority.UseFont = false;
            this.xrLabel_编号16.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel31
            // 
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(712.8331F, 41.25001F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel29
            // 
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(749.8332F, 41.25001F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(12F, 25F);
            // 
            // xrLabel_编号17
            // 
            this.xrLabel_编号17.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel_编号17.BorderWidth = 2F;
            this.xrLabel_编号17.CanGrow = false;
            this.xrLabel_编号17.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel_编号17.LocationFloat = new DevExpress.Utils.PointFloat(761.8333F, 41.25001F);
            this.xrLabel_编号17.Name = "xrLabel_编号17";
            this.xrLabel_编号17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_编号17.SizeF = new System.Drawing.SizeF(25F, 25F);
            this.xrLabel_编号17.StylePriority.UseBorders = false;
            this.xrLabel_编号17.StylePriority.UseBorderWidth = false;
            this.xrLabel_编号17.StylePriority.UseFont = false;
            this.xrLabel_编号17.StylePriority.UseTextAlignment = false;
            this.xrLabel_编号17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(570.8333F, 1020.042F);
            this.xrLabel26.Multiline = true;
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(30.20834F, 39.66669F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "日";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel_建档日期_日
            // 
            this.xrLabel_建档日期_日.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_建档日期_日.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_建档日期_日.LocationFloat = new DevExpress.Utils.PointFloat(514.8333F, 1020.042F);
            this.xrLabel_建档日期_日.Multiline = true;
            this.xrLabel_建档日期_日.Name = "xrLabel_建档日期_日";
            this.xrLabel_建档日期_日.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_建档日期_日.SizeF = new System.Drawing.SizeF(56F, 39.66663F);
            this.xrLabel_建档日期_日.StylePriority.UseBorders = false;
            this.xrLabel_建档日期_日.StylePriority.UseFont = false;
            this.xrLabel_建档日期_日.StylePriority.UseTextAlignment = false;
            this.xrLabel_建档日期_日.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel_建档日期_月
            // 
            this.xrLabel_建档日期_月.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_建档日期_月.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_建档日期_月.LocationFloat = new DevExpress.Utils.PointFloat(428.6249F, 1020.042F);
            this.xrLabel_建档日期_月.Multiline = true;
            this.xrLabel_建档日期_月.Name = "xrLabel_建档日期_月";
            this.xrLabel_建档日期_月.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_建档日期_月.SizeF = new System.Drawing.SizeF(56F, 39.66663F);
            this.xrLabel_建档日期_月.StylePriority.UseBorders = false;
            this.xrLabel_建档日期_月.StylePriority.UseFont = false;
            this.xrLabel_建档日期_月.StylePriority.UseTextAlignment = false;
            this.xrLabel_建档日期_月.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(484.6249F, 1020.042F);
            this.xrLabel24.Multiline = true;
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(30.20834F, 39.66669F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "月";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(398.4166F, 1020.042F);
            this.xrLabel23.Multiline = true;
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(30.20834F, 39.66669F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "年";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel_建档日期_年
            // 
            this.xrLabel_建档日期_年.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_建档日期_年.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_建档日期_年.LocationFloat = new DevExpress.Utils.PointFloat(308.3333F, 1020.042F);
            this.xrLabel_建档日期_年.Multiline = true;
            this.xrLabel_建档日期_年.Name = "xrLabel_建档日期_年";
            this.xrLabel_建档日期_年.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_建档日期_年.SizeF = new System.Drawing.SizeF(90.08328F, 39.66663F);
            this.xrLabel_建档日期_年.StylePriority.UseBorders = false;
            this.xrLabel_建档日期_年.StylePriority.UseFont = false;
            this.xrLabel_建档日期_年.StylePriority.UseTextAlignment = false;
            this.xrLabel_建档日期_年.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel_责任医生
            // 
            this.xrLabel_责任医生.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_责任医生.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_责任医生.LocationFloat = new DevExpress.Utils.PointFloat(308.3333F, 980.3752F);
            this.xrLabel_责任医生.Multiline = true;
            this.xrLabel_责任医生.Name = "xrLabel_责任医生";
            this.xrLabel_责任医生.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_责任医生.SizeF = new System.Drawing.SizeF(292.7083F, 39.66669F);
            this.xrLabel_责任医生.StylePriority.UseBorders = false;
            this.xrLabel_责任医生.StylePriority.UseFont = false;
            this.xrLabel_责任医生.StylePriority.UseTextAlignment = false;
            this.xrLabel_责任医生.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel_建档人
            // 
            this.xrLabel_建档人.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_建档人.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_建档人.LocationFloat = new DevExpress.Utils.PointFloat(308.3333F, 940.7085F);
            this.xrLabel_建档人.Multiline = true;
            this.xrLabel_建档人.Name = "xrLabel_建档人";
            this.xrLabel_建档人.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_建档人.SizeF = new System.Drawing.SizeF(292.7083F, 39.66669F);
            this.xrLabel_建档人.StylePriority.UseBorders = false;
            this.xrLabel_建档人.StylePriority.UseFont = false;
            this.xrLabel_建档人.StylePriority.UseTextAlignment = false;
            this.xrLabel_建档人.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel_建档单位
            // 
            this.xrLabel_建档单位.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_建档单位.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_建档单位.LocationFloat = new DevExpress.Utils.PointFloat(308.3333F, 901.0417F);
            this.xrLabel_建档单位.Multiline = true;
            this.xrLabel_建档单位.Name = "xrLabel_建档单位";
            this.xrLabel_建档单位.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_建档单位.SizeF = new System.Drawing.SizeF(292.7083F, 39.66669F);
            this.xrLabel_建档单位.StylePriority.UseBorders = false;
            this.xrLabel_建档单位.StylePriority.UseFont = false;
            this.xrLabel_建档单位.StylePriority.UseTextAlignment = false;
            this.xrLabel_建档单位.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(200F, 1020.042F);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(108.3333F, 39.66666F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "建档日期:";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(200F, 980.375F);
            this.xrLabel17.Multiline = true;
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(108.3333F, 39.66666F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "责任医生:";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(200F, 940.7084F);
            this.xrLabel16.Multiline = true;
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(108.3333F, 39.66666F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "建 档 人:";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(200F, 901.0417F);
            this.xrLabel15.Multiline = true;
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(108.3333F, 39.66666F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "建档单位:";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel_居委会
            // 
            this.xrLabel_居委会.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_居委会.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_居委会.LocationFloat = new DevExpress.Utils.PointFloat(337.2499F, 704.7083F);
            this.xrLabel_居委会.Multiline = true;
            this.xrLabel_居委会.Name = "xrLabel_居委会";
            this.xrLabel_居委会.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_居委会.SizeF = new System.Drawing.SizeF(311.4583F, 39.66669F);
            this.xrLabel_居委会.StylePriority.UseBorders = false;
            this.xrLabel_居委会.StylePriority.UseFont = false;
            this.xrLabel_居委会.StylePriority.UseTextAlignment = false;
            this.xrLabel_居委会.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel_街道
            // 
            this.xrLabel_街道.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_街道.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_街道.LocationFloat = new DevExpress.Utils.PointFloat(337.2498F, 665.0417F);
            this.xrLabel_街道.Multiline = true;
            this.xrLabel_街道.Name = "xrLabel_街道";
            this.xrLabel_街道.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_街道.SizeF = new System.Drawing.SizeF(311.4583F, 39.66666F);
            this.xrLabel_街道.StylePriority.UseBorders = false;
            this.xrLabel_街道.StylePriority.UseFont = false;
            this.xrLabel_街道.StylePriority.UseTextAlignment = false;
            this.xrLabel_街道.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(142.4582F, 704.7084F);
            this.xrLabel12.Multiline = true;
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(194.7917F, 39.66666F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "村（居）委会名称:";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(142.4582F, 665.0416F);
            this.xrLabel11.Multiline = true;
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(194.7917F, 39.66666F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "乡镇（街道）名称:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel_联系电话
            // 
            this.xrLabel_联系电话.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_联系电话.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_联系电话.LocationFloat = new DevExpress.Utils.PointFloat(250.7915F, 625.375F);
            this.xrLabel_联系电话.Multiline = true;
            this.xrLabel_联系电话.Name = "xrLabel_联系电话";
            this.xrLabel_联系电话.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_联系电话.SizeF = new System.Drawing.SizeF(397.9167F, 39.66666F);
            this.xrLabel_联系电话.StylePriority.UseBorders = false;
            this.xrLabel_联系电话.StylePriority.UseFont = false;
            this.xrLabel_联系电话.StylePriority.UseTextAlignment = false;
            this.xrLabel_联系电话.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel_现住址
            // 
            this.xrLabel_现住址.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_现住址.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_现住址.LocationFloat = new DevExpress.Utils.PointFloat(250.7915F, 546.0417F);
            this.xrLabel_现住址.Multiline = true;
            this.xrLabel_现住址.Name = "xrLabel_现住址";
            this.xrLabel_现住址.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_现住址.SizeF = new System.Drawing.SizeF(397.9167F, 39.66666F);
            this.xrLabel_现住址.StylePriority.UseBorders = false;
            this.xrLabel_现住址.StylePriority.UseFont = false;
            this.xrLabel_现住址.StylePriority.UseTextAlignment = false;
            this.xrLabel_现住址.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel_户籍地址
            // 
            this.xrLabel_户籍地址.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_户籍地址.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_户籍地址.LocationFloat = new DevExpress.Utils.PointFloat(250.7914F, 585.7083F);
            this.xrLabel_户籍地址.Multiline = true;
            this.xrLabel_户籍地址.Name = "xrLabel_户籍地址";
            this.xrLabel_户籍地址.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_户籍地址.SizeF = new System.Drawing.SizeF(397.9167F, 39.66666F);
            this.xrLabel_户籍地址.StylePriority.UseBorders = false;
            this.xrLabel_户籍地址.StylePriority.UseFont = false;
            this.xrLabel_户籍地址.StylePriority.UseTextAlignment = false;
            this.xrLabel_户籍地址.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(142.4581F, 625.375F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(108.3333F, 39.66666F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "联系电话:";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(142.4581F, 585.7083F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(108.3333F, 39.66666F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "户籍地址:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(142.4582F, 546.0417F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(108.3333F, 39.66666F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "现 住 址:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel_姓名
            // 
            this.xrLabel_姓名.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel_姓名.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel_姓名.LocationFloat = new DevExpress.Utils.PointFloat(250.7915F, 506.3751F);
            this.xrLabel_姓名.Multiline = true;
            this.xrLabel_姓名.Name = "xrLabel_姓名";
            this.xrLabel_姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel_姓名.SizeF = new System.Drawing.SizeF(397.9167F, 39.66666F);
            this.xrLabel_姓名.StylePriority.UseBorders = false;
            this.xrLabel_姓名.StylePriority.UseFont = false;
            this.xrLabel_姓名.StylePriority.UseTextAlignment = false;
            this.xrLabel_姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(142.4582F, 506.3751F);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(108.3333F, 39.66666F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "姓    名:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(56.25003F, 260.4584F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(696.88F, 60.5F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "居民健康档案";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("宋体", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(56.25003F, 199.9584F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(696.875F, 60.50003F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "国家基本公共卫生服务项目";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 14F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // pic照片
            // 
            this.pic照片.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.pic照片.Image = ((System.Drawing.Image)(resources.GetObject("pic照片.Image")));
            this.pic照片.LocationFloat = new DevExpress.Utils.PointFloat(320.4582F, 333.4584F);
            this.pic照片.Name = "pic照片";
            this.pic照片.SizeF = new System.Drawing.SizeF(155.0001F, 155F);
            this.pic照片.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.pic照片.StylePriority.UseBorders = false;
            // 
            // report居民健康档案封面
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(12, 14, 0, 14);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_联系电话;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_现住址;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_户籍地址;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_居委会;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_街道;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_建档日期_年;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_责任医生;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_建档人;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_建档单位;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_建档日期_日;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_建档日期_月;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号7;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel_编号1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRPictureBox pic照片;
    }
}
