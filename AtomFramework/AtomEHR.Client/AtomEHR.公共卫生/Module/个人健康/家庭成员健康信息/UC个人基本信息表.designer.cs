﻿namespace AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息
{
    partial class UC个人基本信息表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC个人基本信息表));
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ch二次复核 = new DevExpress.XtraEditors.CheckEdit();
            this.ch复核标记 = new DevExpress.XtraEditors.CheckEdit();
            this.txt复核备注 = new DevExpress.XtraEditors.TextEdit();
            this.sbtnFingerPrint = new DevExpress.XtraEditors.SimpleButton();
            this.dateEdit签字时间 = new DevExpress.XtraEditors.DateEdit();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt户主身份证号 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt家庭结构 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt户主姓名 = new DevExpress.XtraEditors.TextEdit();
            this.flow居住情况 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk与成年子女同住 = new DevExpress.XtraEditors.CheckEdit();
            this.chk与子孙三代同住 = new DevExpress.XtraEditors.CheckEdit();
            this.chk夫妻二人同住 = new DevExpress.XtraEditors.CheckEdit();
            this.chk独居 = new DevExpress.XtraEditors.CheckEdit();
            this.chk计划生育特殊家庭 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt家庭人口数 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.txt档案位置 = new DevExpress.XtraEditors.TextEdit();
            this.txt外出地址 = new DevExpress.XtraEditors.TextEdit();
            this.flow外出 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit县域外流入 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit县域内流出 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit县域外流出 = new DevExpress.XtraEditors.CheckEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk是否流动人口 = new DevExpress.XtraEditors.CheckEdit();
            this.chk是否签约 = new DevExpress.XtraEditors.CheckEdit();
            this.chk是否贫困人口 = new DevExpress.XtraEditors.CheckEdit();
            this.txt证件编号 = new DevExpress.XtraEditors.TextEdit();
            this.cbo证件类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk孕产1 = new DevExpress.XtraEditors.CheckEdit();
            this.chk孕产2 = new DevExpress.XtraEditors.CheckEdit();
            this.chk孕产3 = new DevExpress.XtraEditors.CheckEdit();
            this.chk孕产4 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.cbo孕次 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.cbo产次 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.gc既往史 = new DevExpress.XtraGrid.GridControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuItem删除选中项 = new System.Windows.Forms.ToolStripMenuItem();
            this.gv既往史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col疾病名称 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col日期 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.col疾病名称其他 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col疾病类型 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col疾病其他 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col恶性肿瘤 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col职业病其他 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.flow家族史 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk家族史_高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史_糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史_冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史_慢性阻塞性肺疾病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史_恶性肿瘤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史_脑卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史_重性精神疾病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史_结核病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史_肝炎 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史_先天畸形 = new DevExpress.XtraEditors.CheckEdit();
            this.chk家族史_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt家族史_其他 = new DevExpress.XtraEditors.TextEdit();
            this.cbo家族史关系 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btn家族史_添加 = new DevExpress.XtraEditors.SimpleButton();
            this.gc家族史 = new DevExpress.XtraGrid.GridControl();
            this.gv家族史 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col家族关系 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lkp家族关系 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.col家族病史 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col其他疾病 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col疾病编号 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col家族病史代码 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpcbo家族关系 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbo残疾情况 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo遗传病史 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo暴露史 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo家族史 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.txt居住地址_详细地址 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.cbo居住地址_市 = new DevExpress.XtraEditors.LookUpEdit();
            this.cbo居住地址_县 = new DevExpress.XtraEditors.LookUpEdit();
            this.cbo居住地址_街道 = new DevExpress.XtraEditors.LookUpEdit();
            this.cbo居住地址_村委会 = new DevExpress.XtraEditors.LookUpEdit();
            this.btn重复档案检测 = new DevExpress.XtraEditors.SimpleButton();
            this.cbo档案类别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.radio档案状态 = new DevExpress.XtraEditors.RadioGroup();
            this.flow禽畜栏 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk禽畜栏_单设 = new DevExpress.XtraEditors.CheckEdit();
            this.chk禽畜栏_室内 = new DevExpress.XtraEditors.CheckEdit();
            this.chk禽畜栏_室外 = new DevExpress.XtraEditors.CheckEdit();
            this.chk禽畜栏_无 = new DevExpress.XtraEditors.CheckEdit();
            this.flow厕所 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk厕所_卫生厕所 = new DevExpress.XtraEditors.CheckEdit();
            this.chk厕所_一格或二格粪池式 = new DevExpress.XtraEditors.CheckEdit();
            this.chk厕所_露天粪坑 = new DevExpress.XtraEditors.CheckEdit();
            this.chk厕所_马桶 = new DevExpress.XtraEditors.CheckEdit();
            this.chk厕所_简易棚侧 = new DevExpress.XtraEditors.CheckEdit();
            this.flow饮水 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk饮水_自来水 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮水_经净化过滤的水 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮水_井水 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮水_河湖水 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮水_糖水 = new DevExpress.XtraEditors.CheckEdit();
            this.chk饮水_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.flow燃料类型 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk燃料类型_液化气 = new DevExpress.XtraEditors.CheckEdit();
            this.chk燃料类型_煤 = new DevExpress.XtraEditors.CheckEdit();
            this.chk燃料类型_天然气 = new DevExpress.XtraEditors.CheckEdit();
            this.chk燃料类型_沼气 = new DevExpress.XtraEditors.CheckEdit();
            this.chk燃料类型_柴火 = new DevExpress.XtraEditors.CheckEdit();
            this.chk燃料类型_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.flow厨房排风设施 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk厨房排风设施_无 = new DevExpress.XtraEditors.CheckEdit();
            this.chk厨房排风设施_油烟机 = new DevExpress.XtraEditors.CheckEdit();
            this.chk厨房排风设施_换气扇 = new DevExpress.XtraEditors.CheckEdit();
            this.chk厨房排风设施_烟囱 = new DevExpress.XtraEditors.CheckEdit();
            this.flow残疾情况 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk残疾情况_听力残 = new DevExpress.XtraEditors.CheckEdit();
            this.chk残疾情况_言语残 = new DevExpress.XtraEditors.CheckEdit();
            this.chk残疾情况_肢体残 = new DevExpress.XtraEditors.CheckEdit();
            this.chk残疾情况_智力残 = new DevExpress.XtraEditors.CheckEdit();
            this.chk残疾情况_视力残 = new DevExpress.XtraEditors.CheckEdit();
            this.chk残疾情况_精神残 = new DevExpress.XtraEditors.CheckEdit();
            this.chk残疾情况_其他残疾 = new DevExpress.XtraEditors.CheckEdit();
            this.txt残疾情况_其他残疾 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio输血 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txt输血原因 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.dte输血时间 = new DevExpress.XtraEditors.DateEdit();
            this.btn输血_添加 = new DevExpress.XtraEditors.SimpleButton();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio外伤 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txt外伤名称 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.dte外伤时间 = new DevExpress.XtraEditors.DateEdit();
            this.btn外伤_添加 = new DevExpress.XtraEditors.SimpleButton();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio手术 = new DevExpress.XtraEditors.RadioGroup();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txt手术名称 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.dte手术时间 = new DevExpress.XtraEditors.DateEdit();
            this.btn手术_添加 = new DevExpress.XtraEditors.SimpleButton();
            this.flow疾病 = new System.Windows.Forms.FlowLayoutPanel();
            this.radio疾病 = new DevExpress.XtraEditors.RadioGroup();
            this.chk疾病_高血压 = new DevExpress.XtraEditors.CheckEdit();
            this.chk疾病_糖尿病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk疾病_冠心病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk疾病_慢性阻塞性肺病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk疾病_恶性肿瘤 = new DevExpress.XtraEditors.CheckEdit();
            this.txt疾病_恶性肿瘤 = new DevExpress.XtraEditors.TextEdit();
            this.chk疾病_脑卒中 = new DevExpress.XtraEditors.CheckEdit();
            this.chk疾病_重性精神疾病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk疾病_结核病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk疾病_肝炎 = new DevExpress.XtraEditors.CheckEdit();
            this.chk疾病_其他法定传染病 = new DevExpress.XtraEditors.CheckEdit();
            this.chk疾病_职业病 = new DevExpress.XtraEditors.CheckEdit();
            this.txt疾病_职业病 = new DevExpress.XtraEditors.TextEdit();
            this.chk疾病_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt疾病_其他 = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dte疾病_确诊时间 = new DevExpress.XtraEditors.DateEdit();
            this.btn疾病_添加 = new DevExpress.XtraEditors.SimpleButton();
            this.flow医疗费用支付方式 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk医疗费用支付方式_城镇职工基本医疗保险 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit职工医疗保险卡号 = new DevExpress.XtraEditors.TextEdit();
            this.chk医疗费用支付方式_城镇居民基本医疗保险 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit居民医疗保险卡号 = new DevExpress.XtraEditors.TextEdit();
            this.chk医疗费用支付方式_贫困救助 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit贫困救助卡号 = new DevExpress.XtraEditors.TextEdit();
            this.chk医疗费用支付方式_商业医疗保险 = new DevExpress.XtraEditors.CheckEdit();
            this.chk医疗费用支付方式_全公费 = new DevExpress.XtraEditors.CheckEdit();
            this.chk医疗费用支付方式_全自费 = new DevExpress.XtraEditors.CheckEdit();
            this.chk医疗费用支付方式_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt医疗费用支付方式_其他 = new DevExpress.XtraEditors.TextEdit();
            this.chk医疗费用支付方式_新型农村合作医疗 = new DevExpress.XtraEditors.CheckEdit();
            this.chk医疗费用支付方式_社会医疗保险 = new DevExpress.XtraEditors.CheckEdit();
            this.flow过敏史 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbo过敏史 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.chk过敏史_青霉素 = new DevExpress.XtraEditors.CheckEdit();
            this.chk过敏史_磺胺 = new DevExpress.XtraEditors.CheckEdit();
            this.chk过敏史_不详 = new DevExpress.XtraEditors.CheckEdit();
            this.chk过敏史_链霉素 = new DevExpress.XtraEditors.CheckEdit();
            this.chk过敏史_其他 = new DevExpress.XtraEditors.CheckEdit();
            this.txt过敏史_其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt当前所属机构 = new DevExpress.XtraEditors.TextEdit();
            this.txt最近更新人 = new DevExpress.XtraEditors.TextEdit();
            this.txt录入人 = new DevExpress.XtraEditors.TextEdit();
            this.txt遗传病史_疾病名称 = new DevExpress.XtraEditors.TextEdit();
            this.txt暴露史_射线 = new DevExpress.XtraEditors.TextEdit();
            this.txt暴露史_毒物 = new DevExpress.XtraEditors.TextEdit();
            this.txt暴露史_化学品 = new DevExpress.XtraEditors.TextEdit();
            this.txt新农合号 = new DevExpress.XtraEditors.TextEdit();
            this.txt医疗保险号 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系人电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系人姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt本人电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt工作单位 = new DevExpress.XtraEditors.TextEdit();
            this.dte出生日期 = new DevExpress.XtraEditors.DateEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.cbo性别 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo常住类型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo民族 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo血型 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cboRH阴性 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo文化程度 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo职业 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo劳动程度 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo婚姻状况 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbo与户主关系 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.dte调查时间 = new DevExpress.XtraEditors.DateEdit();
            this.dte录入时间 = new DevExpress.XtraEditors.TextEdit();
            this.dte最近更新时间 = new DevExpress.XtraEditors.TextEdit();
            this.txt所属片区 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit本人或家属签字 = new DevExpress.XtraEditors.PictureEdit();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem5 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem3 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem6 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem57 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem58 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem59 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem60 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem61 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem62 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem50 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem48 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layout孕产情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator4 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem4 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem2 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleSeparator5 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator6 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator7 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator8 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator9 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator10 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem51 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem52 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem7 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlItem53 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem56 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem64 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem54 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem63 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lay居住情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl本人或家属签字 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl签字时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem55 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem65 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem66 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem67 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem43 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem44 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem45 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem46 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem47 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem49 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch二次复核.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch复核标记.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt复核备注.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit签字时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit签字时间.Properties)).BeginInit();
            this.flowLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt户主身份证号.Properties)).BeginInit();
            this.flowLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt家庭结构.Properties)).BeginInit();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt户主姓名.Properties)).BeginInit();
            this.flow居住情况.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk与成年子女同住.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk与子孙三代同住.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk夫妻二人同住.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk独居.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk计划生育特殊家庭.Properties)).BeginInit();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt家庭人口数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案位置.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外出地址.Properties)).BeginInit();
            this.flow外出.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit县域外流入.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit县域内流出.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit县域外流出.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否流动人口.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否签约.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否贫困人口.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt证件编号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo证件类型.Properties)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk孕产1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk孕产2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk孕产3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk孕产4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo孕次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo产次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc既往史)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gv既往史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            this.flow家族史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_慢性阻塞性肺疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_恶性肿瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_脑卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_重性精神疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_结核病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_肝炎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_先天畸形.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家族史_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo家族史关系.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc家族史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv家族史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp家族关系)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpcbo家族关系)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo残疾情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo遗传病史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo暴露史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo家族史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址_详细地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo居住地址_市.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo居住地址_县.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo居住地址_街道.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo居住地址_村委会.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio档案状态.Properties)).BeginInit();
            this.flow禽畜栏.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk禽畜栏_单设.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk禽畜栏_室内.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk禽畜栏_室外.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk禽畜栏_无.Properties)).BeginInit();
            this.flow厕所.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk厕所_卫生厕所.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厕所_一格或二格粪池式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厕所_露天粪坑.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厕所_马桶.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厕所_简易棚侧.Properties)).BeginInit();
            this.flow饮水.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_自来水.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_经净化过滤的水.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_井水.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_河湖水.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_糖水.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_其他.Properties)).BeginInit();
            this.flow燃料类型.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_液化气.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_煤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_天然气.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_沼气.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_柴火.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_其他.Properties)).BeginInit();
            this.flow厨房排风设施.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk厨房排风设施_无.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厨房排风设施_油烟机.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厨房排风设施_换气扇.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厨房排风设施_烟囱.Properties)).BeginInit();
            this.flow残疾情况.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_听力残.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_言语残.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_肢体残.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_智力残.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_视力残.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_精神残.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_其他残疾.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt残疾情况_其他残疾.Properties)).BeginInit();
            this.flowLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio输血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt输血原因.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte输血时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte输血时间.Properties)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio外伤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外伤名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte外伤时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte外伤时间.Properties)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio手术.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt手术名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte手术时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte手术时间.Properties)).BeginInit();
            this.flow疾病.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_高血压.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_糖尿病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_冠心病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_慢性阻塞性肺病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_恶性肿瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt疾病_恶性肿瘤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_脑卒中.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_重性精神疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_结核病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_肝炎.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_其他法定传染病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_职业病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt疾病_职业病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt疾病_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte疾病_确诊时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte疾病_确诊时间.Properties)).BeginInit();
            this.flow医疗费用支付方式.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_城镇职工基本医疗保险.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit职工医疗保险卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_城镇居民基本医疗保险.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居民医疗保险卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_贫困救助.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit贫困救助卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_商业医疗保险.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_全公费.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_全自费.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医疗费用支付方式_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_新型农村合作医疗.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_社会医疗保险.Properties)).BeginInit();
            this.flow过敏史.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo过敏史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk过敏史_青霉素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk过敏史_磺胺.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk过敏史_不详.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk过敏史_链霉素.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk过敏史_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt过敏史_其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入人.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遗传病史_疾病名称.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt暴露史_射线.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt暴露史_毒物.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt暴露史_化学品.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新农合号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医疗保险号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本人电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt工作单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo常住类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo民族.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo血型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRH阴性.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo文化程度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo劳动程度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo婚姻状况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo与户主关系.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt所属片区.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit本人或家属签字.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout孕产情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lay居住情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl本人或家属签字)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl签字时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.flowLayoutPanel15.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(4);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem2.Location = new System.Drawing.Point(362, 197);
            this.emptySpaceItem2.Name = "emptySpaceItem1";
            this.emptySpaceItem2.Size = new System.Drawing.Size(363, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem1";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.ch二次复核);
            this.layoutControl1.Controls.Add(this.ch复核标记);
            this.layoutControl1.Controls.Add(this.txt复核备注);
            this.layoutControl1.Controls.Add(this.sbtnFingerPrint);
            this.layoutControl1.Controls.Add(this.dateEdit签字时间);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel11);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel10);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel9);
            this.layoutControl1.Controls.Add(this.flow居住情况);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel7);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.txt档案位置);
            this.layoutControl1.Controls.Add(this.txt外出地址);
            this.layoutControl1.Controls.Add(this.flow外出);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.txt证件编号);
            this.layoutControl1.Controls.Add(this.cbo证件类型);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel1);
            this.layoutControl1.Controls.Add(this.gc既往史);
            this.layoutControl1.Controls.Add(this.flow家族史);
            this.layoutControl1.Controls.Add(this.gc家族史);
            this.layoutControl1.Controls.Add(this.cbo残疾情况);
            this.layoutControl1.Controls.Add(this.cbo遗传病史);
            this.layoutControl1.Controls.Add(this.cbo暴露史);
            this.layoutControl1.Controls.Add(this.cbo家族史);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.btn重复档案检测);
            this.layoutControl1.Controls.Add(this.cbo档案类别);
            this.layoutControl1.Controls.Add(this.radio档案状态);
            this.layoutControl1.Controls.Add(this.flow禽畜栏);
            this.layoutControl1.Controls.Add(this.flow厕所);
            this.layoutControl1.Controls.Add(this.flow饮水);
            this.layoutControl1.Controls.Add(this.flow燃料类型);
            this.layoutControl1.Controls.Add(this.flow厨房排风设施);
            this.layoutControl1.Controls.Add(this.flow残疾情况);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel6);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel5);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel4);
            this.layoutControl1.Controls.Add(this.flow疾病);
            this.layoutControl1.Controls.Add(this.flow医疗费用支付方式);
            this.layoutControl1.Controls.Add(this.flow过敏史);
            this.layoutControl1.Controls.Add(this.txt当前所属机构);
            this.layoutControl1.Controls.Add(this.txt最近更新人);
            this.layoutControl1.Controls.Add(this.txt录入人);
            this.layoutControl1.Controls.Add(this.txt遗传病史_疾病名称);
            this.layoutControl1.Controls.Add(this.txt暴露史_射线);
            this.layoutControl1.Controls.Add(this.txt暴露史_毒物);
            this.layoutControl1.Controls.Add(this.txt暴露史_化学品);
            this.layoutControl1.Controls.Add(this.txt新农合号);
            this.layoutControl1.Controls.Add(this.txt医疗保险号);
            this.layoutControl1.Controls.Add(this.txt联系人电话);
            this.layoutControl1.Controls.Add(this.txt联系人姓名);
            this.layoutControl1.Controls.Add(this.txt本人电话);
            this.layoutControl1.Controls.Add(this.txt工作单位);
            this.layoutControl1.Controls.Add(this.dte出生日期);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.cbo性别);
            this.layoutControl1.Controls.Add(this.cbo常住类型);
            this.layoutControl1.Controls.Add(this.cbo民族);
            this.layoutControl1.Controls.Add(this.cbo血型);
            this.layoutControl1.Controls.Add(this.cboRH阴性);
            this.layoutControl1.Controls.Add(this.cbo文化程度);
            this.layoutControl1.Controls.Add(this.cbo职业);
            this.layoutControl1.Controls.Add(this.cbo劳动程度);
            this.layoutControl1.Controls.Add(this.cbo婚姻状况);
            this.layoutControl1.Controls.Add(this.cbo与户主关系);
            this.layoutControl1.Controls.Add(this.dte调查时间);
            this.layoutControl1.Controls.Add(this.dte录入时间);
            this.layoutControl1.Controls.Add(this.dte最近更新时间);
            this.layoutControl1.Controls.Add(this.txt所属片区);
            this.layoutControl1.Controls.Add(this.textEdit本人或家属签字);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20});
            this.layoutControl1.Location = new System.Drawing.Point(0, 30);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1133, 277, 470, 502);
            this.layoutControl1.OptionsFocus.AllowFocusGroups = false;
            this.layoutControl1.OptionsFocus.AllowFocusReadonlyEditors = false;
            this.layoutControl1.OptionsFocus.AllowFocusTabbedGroups = false;
            this.layoutControl1.OptionsFocus.EnableAutoTabOrder = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(752, 470);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // ch二次复核
            // 
            this.ch二次复核.Location = new System.Drawing.Point(582, 1405);
            this.ch二次复核.Name = "ch二次复核";
            this.ch二次复核.Properties.Caption = "二次复核";
            this.ch二次复核.Size = new System.Drawing.Size(146, 19);
            this.ch二次复核.StyleController = this.layoutControl1;
            this.ch二次复核.TabIndex = 177;
            // 
            // ch复核标记
            // 
            this.ch复核标记.Location = new System.Drawing.Point(432, 1405);
            this.ch复核标记.Name = "ch复核标记";
            this.ch复核标记.Properties.Caption = "复核标记";
            this.ch复核标记.Size = new System.Drawing.Size(146, 19);
            this.ch复核标记.StyleController = this.layoutControl1;
            this.ch复核标记.TabIndex = 176;
            // 
            // txt复核备注
            // 
            this.txt复核备注.Location = new System.Drawing.Point(537, 1381);
            this.txt复核备注.Name = "txt复核备注";
            this.txt复核备注.Size = new System.Drawing.Size(191, 20);
            this.txt复核备注.StyleController = this.layoutControl1;
            this.txt复核备注.TabIndex = 175;
            // 
            // sbtnFingerPrint
            // 
            this.sbtnFingerPrint.Location = new System.Drawing.Point(279, 1357);
            this.sbtnFingerPrint.Name = "sbtnFingerPrint";
            this.sbtnFingerPrint.Size = new System.Drawing.Size(149, 22);
            this.sbtnFingerPrint.StyleController = this.layoutControl1;
            this.sbtnFingerPrint.TabIndex = 174;
            this.sbtnFingerPrint.Text = "更新指纹";
            this.sbtnFingerPrint.Click += new System.EventHandler(this.sbtnFingerPrint_Click);
            // 
            // dateEdit签字时间
            // 
            this.dateEdit签字时间.EditValue = null;
            this.dateEdit签字时间.Location = new System.Drawing.Point(537, 1357);
            this.dateEdit签字时间.Name = "dateEdit签字时间";
            this.dateEdit签字时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit签字时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit签字时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dateEdit签字时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEdit签字时间.Size = new System.Drawing.Size(191, 20);
            this.dateEdit签字时间.StyleController = this.layoutControl1;
            this.dateEdit签字时间.TabIndex = 173;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.txt户主身份证号);
            this.flowLayoutPanel11.Location = new System.Drawing.Point(312, 1131);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(136, 26);
            this.flowLayoutPanel11.TabIndex = 171;
            // 
            // txt户主身份证号
            // 
            this.txt户主身份证号.Location = new System.Drawing.Point(3, 3);
            this.txt户主身份证号.Name = "txt户主身份证号";
            this.txt户主身份证号.Size = new System.Drawing.Size(133, 20);
            this.txt户主身份证号.TabIndex = 0;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.txt家庭结构);
            this.flowLayoutPanel10.Location = new System.Drawing.Point(647, 1131);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(81, 26);
            this.flowLayoutPanel10.TabIndex = 170;
            // 
            // txt家庭结构
            // 
            this.txt家庭结构.Location = new System.Drawing.Point(3, 3);
            this.txt家庭结构.Name = "txt家庭结构";
            this.txt家庭结构.Size = new System.Drawing.Size(78, 20);
            this.txt家庭结构.TabIndex = 0;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Controls.Add(this.txt户主姓名);
            this.flowLayoutPanel9.Location = new System.Drawing.Point(172, 1131);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(71, 26);
            this.flowLayoutPanel9.TabIndex = 169;
            // 
            // txt户主姓名
            // 
            this.txt户主姓名.Location = new System.Drawing.Point(3, 3);
            this.txt户主姓名.Name = "txt户主姓名";
            this.txt户主姓名.Size = new System.Drawing.Size(67, 20);
            this.txt户主姓名.TabIndex = 0;
            // 
            // flow居住情况
            // 
            this.flow居住情况.Controls.Add(this.chk与成年子女同住);
            this.flow居住情况.Controls.Add(this.chk与子孙三代同住);
            this.flow居住情况.Controls.Add(this.chk夫妻二人同住);
            this.flow居住情况.Controls.Add(this.chk独居);
            this.flow居住情况.Controls.Add(this.chk计划生育特殊家庭);
            this.flow居住情况.Location = new System.Drawing.Point(172, 1161);
            this.flow居住情况.Name = "flow居住情况";
            this.flow居住情况.Size = new System.Drawing.Size(556, 46);
            this.flow居住情况.TabIndex = 168;
            // 
            // chk与成年子女同住
            // 
            this.chk与成年子女同住.Location = new System.Drawing.Point(3, 3);
            this.chk与成年子女同住.Name = "chk与成年子女同住";
            this.chk与成年子女同住.Properties.Caption = "与成年子女同住";
            this.chk与成年子女同住.Size = new System.Drawing.Size(121, 19);
            this.chk与成年子女同住.TabIndex = 0;
            this.chk与成年子女同住.Tag = "1";
            // 
            // chk与子孙三代同住
            // 
            this.chk与子孙三代同住.Location = new System.Drawing.Point(130, 3);
            this.chk与子孙三代同住.Name = "chk与子孙三代同住";
            this.chk与子孙三代同住.Properties.Caption = "与子孙三代（四代）同住";
            this.chk与子孙三代同住.Size = new System.Drawing.Size(163, 19);
            this.chk与子孙三代同住.TabIndex = 1;
            this.chk与子孙三代同住.Tag = "2";
            // 
            // chk夫妻二人同住
            // 
            this.chk夫妻二人同住.Location = new System.Drawing.Point(299, 3);
            this.chk夫妻二人同住.Name = "chk夫妻二人同住";
            this.chk夫妻二人同住.Properties.Caption = "夫妻二人同住";
            this.chk夫妻二人同住.Size = new System.Drawing.Size(118, 19);
            this.chk夫妻二人同住.TabIndex = 2;
            this.chk夫妻二人同住.Tag = "3";
            // 
            // chk独居
            // 
            this.chk独居.Location = new System.Drawing.Point(423, 3);
            this.chk独居.Name = "chk独居";
            this.chk独居.Properties.Caption = "独居";
            this.chk独居.Size = new System.Drawing.Size(75, 19);
            this.chk独居.TabIndex = 3;
            this.chk独居.Tag = "4";
            // 
            // chk计划生育特殊家庭
            // 
            this.chk计划生育特殊家庭.Location = new System.Drawing.Point(3, 28);
            this.chk计划生育特殊家庭.Name = "chk计划生育特殊家庭";
            this.chk计划生育特殊家庭.Properties.Caption = "计划生育特殊家庭";
            this.chk计划生育特殊家庭.Size = new System.Drawing.Size(128, 19);
            this.chk计划生育特殊家庭.TabIndex = 4;
            this.chk计划生育特殊家庭.Tag = "5";
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.txt家庭人口数);
            this.flowLayoutPanel7.Location = new System.Drawing.Point(517, 1131);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(61, 26);
            this.flowLayoutPanel7.TabIndex = 167;
            // 
            // txt家庭人口数
            // 
            this.txt家庭人口数.Location = new System.Drawing.Point(3, 3);
            this.txt家庭人口数.Name = "txt家庭人口数";
            this.txt家庭人口数.Size = new System.Drawing.Size(58, 20);
            this.txt家庭人口数.TabIndex = 0;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Location = new System.Drawing.Point(112, 1131);
            this.flowLayoutPanel3.MinimumSize = new System.Drawing.Size(0, 50);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(0, 76);
            this.flowLayoutPanel3.TabIndex = 166;
            // 
            // txt档案位置
            // 
            this.txt档案位置.Location = new System.Drawing.Point(462, 231);
            this.txt档案位置.Margin = new System.Windows.Forms.Padding(2);
            this.txt档案位置.Name = "txt档案位置";
            this.txt档案位置.Size = new System.Drawing.Size(266, 20);
            this.txt档案位置.StyleController = this.layoutControl1;
            this.txt档案位置.TabIndex = 165;
            // 
            // txt外出地址
            // 
            this.txt外出地址.Location = new System.Drawing.Point(537, 1333);
            this.txt外出地址.Margin = new System.Windows.Forms.Padding(2);
            this.txt外出地址.Name = "txt外出地址";
            this.txt外出地址.Size = new System.Drawing.Size(191, 20);
            this.txt外出地址.StyleController = this.layoutControl1;
            this.txt外出地址.TabIndex = 164;
            // 
            // flow外出
            // 
            this.flow外出.Controls.Add(this.checkEdit县域外流入);
            this.flow外出.Controls.Add(this.checkEdit县域内流出);
            this.flow外出.Controls.Add(this.checkEdit县域外流出);
            this.flow外出.Location = new System.Drawing.Point(114, 1333);
            this.flow外出.Name = "flow外出";
            this.flow外出.Size = new System.Drawing.Size(314, 20);
            this.flow外出.TabIndex = 163;
            // 
            // checkEdit县域外流入
            // 
            this.checkEdit县域外流入.Location = new System.Drawing.Point(0, 0);
            this.checkEdit县域外流入.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit县域外流入.Name = "checkEdit县域外流入";
            this.checkEdit县域外流入.Properties.Caption = "县域外流入";
            this.checkEdit县域外流入.Size = new System.Drawing.Size(91, 19);
            this.checkEdit县域外流入.TabIndex = 1;
            this.checkEdit县域外流入.Tag = "0";
            // 
            // checkEdit县域内流出
            // 
            this.checkEdit县域内流出.Location = new System.Drawing.Point(91, 0);
            this.checkEdit县域内流出.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit县域内流出.Name = "checkEdit县域内流出";
            this.checkEdit县域内流出.Properties.Caption = "县域内流出";
            this.checkEdit县域内流出.Size = new System.Drawing.Size(91, 19);
            this.checkEdit县域内流出.TabIndex = 2;
            this.checkEdit县域内流出.Tag = "1";
            // 
            // checkEdit县域外流出
            // 
            this.checkEdit县域外流出.Location = new System.Drawing.Point(182, 0);
            this.checkEdit县域外流出.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit县域外流出.Name = "checkEdit县域外流出";
            this.checkEdit县域外流出.Properties.Caption = "县域外流出";
            this.checkEdit县域外流出.Size = new System.Drawing.Size(111, 19);
            this.checkEdit县域外流出.TabIndex = 3;
            this.checkEdit县域外流出.Tag = "2";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel2.Controls.Add(this.chk是否流动人口);
            this.flowLayoutPanel2.Controls.Add(this.chk是否签约);
            this.flowLayoutPanel2.Controls.Add(this.chk是否贫困人口);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(114, 105);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(614, 26);
            this.flowLayoutPanel2.TabIndex = 162;
            // 
            // chk是否流动人口
            // 
            this.chk是否流动人口.Location = new System.Drawing.Point(3, 3);
            this.chk是否流动人口.Name = "chk是否流动人口";
            this.chk是否流动人口.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.chk是否流动人口.Properties.Appearance.Options.UseFont = true;
            this.chk是否流动人口.Properties.AutoHeight = false;
            this.chk是否流动人口.Properties.Caption = "是否流动人口";
            this.chk是否流动人口.Size = new System.Drawing.Size(138, 19);
            this.chk是否流动人口.StyleController = this.layoutControl1;
            this.chk是否流动人口.TabIndex = 162;
            // 
            // chk是否签约
            // 
            this.chk是否签约.Location = new System.Drawing.Point(147, 3);
            this.chk是否签约.Name = "chk是否签约";
            this.chk是否签约.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.chk是否签约.Properties.Appearance.Options.UseFont = true;
            this.chk是否签约.Properties.AutoHeight = false;
            this.chk是否签约.Properties.Caption = "是否签约服务";
            this.chk是否签约.Size = new System.Drawing.Size(116, 19);
            this.chk是否签约.TabIndex = 163;
            // 
            // chk是否贫困人口
            // 
            this.chk是否贫困人口.Location = new System.Drawing.Point(269, 3);
            this.chk是否贫困人口.Name = "chk是否贫困人口";
            this.chk是否贫困人口.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.chk是否贫困人口.Properties.Appearance.Options.UseFont = true;
            this.chk是否贫困人口.Properties.AutoHeight = false;
            this.chk是否贫困人口.Properties.Caption = "是否贫困人口";
            this.chk是否贫困人口.Size = new System.Drawing.Size(118, 19);
            this.chk是否贫困人口.TabIndex = 164;
            // 
            // txt证件编号
            // 
            this.txt证件编号.EditValue = "";
            this.txt证件编号.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.txt证件编号.Location = new System.Drawing.Point(207, 79);
            this.txt证件编号.Margin = new System.Windows.Forms.Padding(0);
            this.txt证件编号.Name = "txt证件编号";
            this.txt证件编号.Size = new System.Drawing.Size(146, 20);
            this.txt证件编号.StyleController = this.layoutControl1;
            this.txt证件编号.TabIndex = 8;
            this.txt证件编号.Leave += new System.EventHandler(this.txt证件编号_Leave);
            // 
            // cbo证件类型
            // 
            this.cbo证件类型.EditValue = "身份证";
            this.cbo证件类型.EnterMoveNextControl = true;
            this.cbo证件类型.Location = new System.Drawing.Point(112, 79);
            this.cbo证件类型.Margin = new System.Windows.Forms.Padding(0);
            this.cbo证件类型.Name = "cbo证件类型";
            this.cbo证件类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo证件类型.Properties.Items.AddRange(new object[] {
            "身份证",
            "护照（外籍人士）",
            "军官证"});
            this.cbo证件类型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo证件类型.Size = new System.Drawing.Size(91, 20);
            this.cbo证件类型.StyleController = this.layoutControl1;
            this.cbo证件类型.TabIndex = 7;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel1.Controls.Add(this.chk孕产1);
            this.flowLayoutPanel1.Controls.Add(this.chk孕产2);
            this.flowLayoutPanel1.Controls.Add(this.chk孕产3);
            this.flowLayoutPanel1.Controls.Add(this.chk孕产4);
            this.flowLayoutPanel1.Controls.Add(this.labelControl11);
            this.flowLayoutPanel1.Controls.Add(this.cbo孕次);
            this.flowLayoutPanel1.Controls.Add(this.labelControl12);
            this.flowLayoutPanel1.Controls.Add(this.cbo产次);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(114, 1101);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(614, 26);
            this.flowLayoutPanel1.TabIndex = 118;
            // 
            // chk孕产1
            // 
            this.chk孕产1.EditValue = true;
            this.chk孕产1.Location = new System.Drawing.Point(3, 3);
            this.chk孕产1.Name = "chk孕产1";
            this.chk孕产1.Properties.Caption = "未孕";
            this.chk孕产1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk孕产1.Properties.RadioGroupIndex = 0;
            this.chk孕产1.Size = new System.Drawing.Size(50, 19);
            this.chk孕产1.TabIndex = 119;
            this.chk孕产1.CheckedChanged += new System.EventHandler(this.chk孕产1_CheckedChanged);
            // 
            // chk孕产2
            // 
            this.chk孕产2.Location = new System.Drawing.Point(59, 3);
            this.chk孕产2.Name = "chk孕产2";
            this.chk孕产2.Properties.Caption = "已孕未生产";
            this.chk孕产2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk孕产2.Properties.RadioGroupIndex = 0;
            this.chk孕产2.Size = new System.Drawing.Size(88, 19);
            this.chk孕产2.TabIndex = 120;
            this.chk孕产2.TabStop = false;
            this.chk孕产2.CheckedChanged += new System.EventHandler(this.chk孕产2_CheckedChanged);
            // 
            // chk孕产3
            // 
            this.chk孕产3.Location = new System.Drawing.Point(153, 3);
            this.chk孕产3.Name = "chk孕产3";
            this.chk孕产3.Properties.Caption = "已生产(随访期内)";
            this.chk孕产3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk孕产3.Properties.RadioGroupIndex = 0;
            this.chk孕产3.Size = new System.Drawing.Size(122, 19);
            this.chk孕产3.TabIndex = 121;
            this.chk孕产3.TabStop = false;
            this.chk孕产3.CheckedChanged += new System.EventHandler(this.chk孕产3_CheckedChanged);
            // 
            // chk孕产4
            // 
            this.chk孕产4.Location = new System.Drawing.Point(281, 3);
            this.chk孕产4.Name = "chk孕产4";
            this.chk孕产4.Properties.Caption = "已生产(随访期外)";
            this.chk孕产4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.chk孕产4.Properties.RadioGroupIndex = 0;
            this.chk孕产4.Size = new System.Drawing.Size(124, 19);
            this.chk孕产4.TabIndex = 122;
            this.chk孕产4.TabStop = false;
            this.chk孕产4.CheckedChanged += new System.EventHandler(this.chk孕产4_CheckedChanged);
            // 
            // labelControl11
            // 
            this.labelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl11.Location = new System.Drawing.Point(411, 3);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(24, 21);
            this.labelControl11.TabIndex = 4;
            this.labelControl11.Text = "孕次";
            // 
            // cbo孕次
            // 
            this.cbo孕次.Enabled = false;
            this.cbo孕次.Location = new System.Drawing.Point(441, 3);
            this.cbo孕次.Name = "cbo孕次";
            this.cbo孕次.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo孕次.Properties.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.cbo孕次.Size = new System.Drawing.Size(62, 20);
            this.cbo孕次.TabIndex = 123;
            // 
            // labelControl12
            // 
            this.labelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl12.Location = new System.Drawing.Point(509, 3);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(24, 21);
            this.labelControl12.TabIndex = 5;
            this.labelControl12.Text = "产次";
            // 
            // cbo产次
            // 
            this.cbo产次.Enabled = false;
            this.cbo产次.Location = new System.Drawing.Point(539, 3);
            this.cbo产次.Name = "cbo产次";
            this.cbo产次.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo产次.Properties.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.cbo产次.Size = new System.Drawing.Size(59, 20);
            this.cbo产次.TabIndex = 123;
            // 
            // gc既往史
            // 
            this.gc既往史.ContextMenuStrip = this.contextMenuStrip1;
            this.gc既往史.Location = new System.Drawing.Point(29, 733);
            this.gc既往史.MainView = this.gv既往史;
            this.gc既往史.Name = "gc既往史";
            this.gc既往史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit2});
            this.gc既往史.Size = new System.Drawing.Size(699, 96);
            this.gc既往史.TabIndex = 89;
            this.gc既往史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv既往史});
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItem删除选中项});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(137, 26);
            // 
            // menuItem删除选中项
            // 
            this.menuItem删除选中项.Name = "menuItem删除选中项";
            this.menuItem删除选中项.Size = new System.Drawing.Size(136, 22);
            this.menuItem删除选中项.Text = "删除选中项";
            this.menuItem删除选中项.Click += new System.EventHandler(this.menuItem删除选中项_Click);
            // 
            // gv既往史
            // 
            this.gv既往史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col疾病名称,
            this.col日期,
            this.col疾病名称其他,
            this.col疾病类型,
            this.col疾病其他,
            this.col恶性肿瘤,
            this.col职业病其他});
            this.gv既往史.GridControl = this.gc既往史;
            this.gv既往史.Name = "gv既往史";
            this.gv既往史.OptionsSelection.MultiSelect = true;
            this.gv既往史.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gv既往史.OptionsView.ColumnAutoWidth = false;
            this.gv既往史.OptionsView.EnableAppearanceEvenRow = true;
            this.gv既往史.OptionsView.EnableAppearanceOddRow = true;
            this.gv既往史.OptionsView.ShowGroupPanel = false;
            this.gv既往史.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gv既往史_CustomRowCellEdit);
            // 
            // col疾病名称
            // 
            this.col疾病名称.AppearanceHeader.Options.UseTextOptions = true;
            this.col疾病名称.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col疾病名称.Caption = "疾病名称";
            this.col疾病名称.FieldName = "D_JBBM";
            this.col疾病名称.Name = "col疾病名称";
            this.col疾病名称.OptionsColumn.AllowEdit = false;
            this.col疾病名称.OptionsColumn.ReadOnly = true;
            this.col疾病名称.Visible = true;
            this.col疾病名称.VisibleIndex = 1;
            this.col疾病名称.Width = 80;
            // 
            // col日期
            // 
            this.col日期.AppearanceHeader.Options.UseTextOptions = true;
            this.col日期.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col日期.Caption = "日期";
            this.col日期.ColumnEdit = this.repositoryItemDateEdit1;
            this.col日期.FieldName = "日期";
            this.col日期.Name = "col日期";
            this.col日期.Visible = true;
            this.col日期.VisibleIndex = 2;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "yyyy-MM-dd";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // col疾病名称其他
            // 
            this.col疾病名称其他.AppearanceHeader.Options.UseTextOptions = true;
            this.col疾病名称其他.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col疾病名称其他.Caption = "疾病名称其他";
            this.col疾病名称其他.FieldName = "疾病名称其他";
            this.col疾病名称其他.Name = "col疾病名称其他";
            this.col疾病名称其他.OptionsColumn.AllowEdit = false;
            this.col疾病名称其他.OptionsColumn.ReadOnly = true;
            this.col疾病名称其他.Width = 95;
            // 
            // col疾病类型
            // 
            this.col疾病类型.AppearanceHeader.Options.UseTextOptions = true;
            this.col疾病类型.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col疾病类型.Caption = "疾病类型";
            this.col疾病类型.FieldName = "疾病类型";
            this.col疾病类型.Name = "col疾病类型";
            this.col疾病类型.OptionsColumn.AllowEdit = false;
            this.col疾病类型.OptionsColumn.ReadOnly = true;
            this.col疾病类型.Visible = true;
            this.col疾病类型.VisibleIndex = 4;
            this.col疾病类型.Width = 88;
            // 
            // col疾病其他
            // 
            this.col疾病其他.AppearanceHeader.Options.UseTextOptions = true;
            this.col疾病其他.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col疾病其他.Caption = "疾病其他";
            this.col疾病其他.FieldName = "疾病其他";
            this.col疾病其他.Name = "col疾病其他";
            this.col疾病其他.OptionsColumn.AllowEdit = false;
            this.col疾病其他.OptionsColumn.ReadOnly = true;
            this.col疾病其他.Visible = true;
            this.col疾病其他.VisibleIndex = 5;
            this.col疾病其他.Width = 89;
            // 
            // col恶性肿瘤
            // 
            this.col恶性肿瘤.AppearanceHeader.Options.UseTextOptions = true;
            this.col恶性肿瘤.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col恶性肿瘤.Caption = "恶性肿瘤";
            this.col恶性肿瘤.Name = "col恶性肿瘤";
            this.col恶性肿瘤.OptionsColumn.AllowEdit = false;
            this.col恶性肿瘤.OptionsColumn.ReadOnly = true;
            this.col恶性肿瘤.Visible = true;
            this.col恶性肿瘤.VisibleIndex = 3;
            // 
            // col职业病其他
            // 
            this.col职业病其他.AppearanceHeader.Options.UseTextOptions = true;
            this.col职业病其他.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col职业病其他.Caption = "职业病其他";
            this.col职业病其他.FieldName = "职业病其他";
            this.col职业病其他.Name = "col职业病其他";
            this.col职业病其他.OptionsColumn.AllowEdit = false;
            this.col职业病其他.OptionsColumn.ReadOnly = true;
            this.col职业病其他.Visible = true;
            this.col职业病其他.VisibleIndex = 6;
            this.col职业病其他.Width = 108;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Mask.EditMask = "yyyy-MM";
            this.repositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // flow家族史
            // 
            this.flow家族史.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flow家族史.Controls.Add(this.chk家族史_高血压);
            this.flow家族史.Controls.Add(this.chk家族史_糖尿病);
            this.flow家族史.Controls.Add(this.chk家族史_冠心病);
            this.flow家族史.Controls.Add(this.chk家族史_慢性阻塞性肺疾病);
            this.flow家族史.Controls.Add(this.chk家族史_恶性肿瘤);
            this.flow家族史.Controls.Add(this.chk家族史_脑卒中);
            this.flow家族史.Controls.Add(this.chk家族史_重性精神疾病);
            this.flow家族史.Controls.Add(this.chk家族史_结核病);
            this.flow家族史.Controls.Add(this.chk家族史_肝炎);
            this.flow家族史.Controls.Add(this.chk家族史_先天畸形);
            this.flow家族史.Controls.Add(this.chk家族史_其他);
            this.flow家族史.Controls.Add(this.txt家族史_其他);
            this.flow家族史.Controls.Add(this.cbo家族史关系);
            this.flow家族史.Controls.Add(this.btn家族史_添加);
            this.flow家族史.Location = new System.Drawing.Point(7, 859);
            this.flow家族史.Name = "flow家族史";
            this.flow家族史.Size = new System.Drawing.Size(358, 86);
            this.flow家族史.TabIndex = 87;
            // 
            // chk家族史_高血压
            // 
            this.chk家族史_高血压.Enabled = false;
            this.chk家族史_高血压.Location = new System.Drawing.Point(0, 0);
            this.chk家族史_高血压.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_高血压.Name = "chk家族史_高血压";
            this.chk家族史_高血压.Properties.Caption = "高血压";
            this.chk家族史_高血压.Size = new System.Drawing.Size(64, 19);
            this.chk家族史_高血压.TabIndex = 88;
            this.chk家族史_高血压.Tag = "1";
            // 
            // chk家族史_糖尿病
            // 
            this.chk家族史_糖尿病.Enabled = false;
            this.chk家族史_糖尿病.Location = new System.Drawing.Point(64, 0);
            this.chk家族史_糖尿病.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_糖尿病.Name = "chk家族史_糖尿病";
            this.chk家族史_糖尿病.Properties.Caption = "糖尿病";
            this.chk家族史_糖尿病.Size = new System.Drawing.Size(64, 19);
            this.chk家族史_糖尿病.TabIndex = 89;
            this.chk家族史_糖尿病.Tag = "2";
            // 
            // chk家族史_冠心病
            // 
            this.chk家族史_冠心病.Enabled = false;
            this.chk家族史_冠心病.Location = new System.Drawing.Point(128, 0);
            this.chk家族史_冠心病.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_冠心病.Name = "chk家族史_冠心病";
            this.chk家族史_冠心病.Properties.Caption = "冠心病";
            this.chk家族史_冠心病.Size = new System.Drawing.Size(64, 19);
            this.chk家族史_冠心病.TabIndex = 90;
            this.chk家族史_冠心病.Tag = "3";
            // 
            // chk家族史_慢性阻塞性肺疾病
            // 
            this.chk家族史_慢性阻塞性肺疾病.Enabled = false;
            this.chk家族史_慢性阻塞性肺疾病.Location = new System.Drawing.Point(192, 0);
            this.chk家族史_慢性阻塞性肺疾病.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_慢性阻塞性肺疾病.Name = "chk家族史_慢性阻塞性肺疾病";
            this.chk家族史_慢性阻塞性肺疾病.Properties.Caption = "慢性阻塞性肺疾病";
            this.chk家族史_慢性阻塞性肺疾病.Size = new System.Drawing.Size(122, 19);
            this.chk家族史_慢性阻塞性肺疾病.TabIndex = 91;
            this.chk家族史_慢性阻塞性肺疾病.Tag = "4";
            // 
            // chk家族史_恶性肿瘤
            // 
            this.chk家族史_恶性肿瘤.Enabled = false;
            this.chk家族史_恶性肿瘤.Location = new System.Drawing.Point(0, 19);
            this.chk家族史_恶性肿瘤.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_恶性肿瘤.Name = "chk家族史_恶性肿瘤";
            this.chk家族史_恶性肿瘤.Properties.Caption = "恶性肿瘤";
            this.chk家族史_恶性肿瘤.Size = new System.Drawing.Size(77, 19);
            this.chk家族史_恶性肿瘤.TabIndex = 92;
            this.chk家族史_恶性肿瘤.Tag = "5";
            // 
            // chk家族史_脑卒中
            // 
            this.chk家族史_脑卒中.Enabled = false;
            this.chk家族史_脑卒中.Location = new System.Drawing.Point(77, 19);
            this.chk家族史_脑卒中.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_脑卒中.Name = "chk家族史_脑卒中";
            this.chk家族史_脑卒中.Properties.Caption = "脑卒中";
            this.chk家族史_脑卒中.Size = new System.Drawing.Size(64, 19);
            this.chk家族史_脑卒中.TabIndex = 93;
            this.chk家族史_脑卒中.Tag = "6";
            // 
            // chk家族史_重性精神疾病
            // 
            this.chk家族史_重性精神疾病.Enabled = false;
            this.chk家族史_重性精神疾病.Location = new System.Drawing.Point(141, 19);
            this.chk家族史_重性精神疾病.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_重性精神疾病.Name = "chk家族史_重性精神疾病";
            this.chk家族史_重性精神疾病.Properties.Caption = "严重精神障碍";
            this.chk家族史_重性精神疾病.Size = new System.Drawing.Size(107, 19);
            this.chk家族史_重性精神疾病.TabIndex = 94;
            this.chk家族史_重性精神疾病.Tag = "7";
            // 
            // chk家族史_结核病
            // 
            this.chk家族史_结核病.Enabled = false;
            this.chk家族史_结核病.Location = new System.Drawing.Point(248, 19);
            this.chk家族史_结核病.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_结核病.Name = "chk家族史_结核病";
            this.chk家族史_结核病.Properties.Caption = "结核病";
            this.chk家族史_结核病.Size = new System.Drawing.Size(58, 19);
            this.chk家族史_结核病.TabIndex = 95;
            this.chk家族史_结核病.Tag = "8";
            // 
            // chk家族史_肝炎
            // 
            this.chk家族史_肝炎.Enabled = false;
            this.chk家族史_肝炎.Location = new System.Drawing.Point(306, 19);
            this.chk家族史_肝炎.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_肝炎.Name = "chk家族史_肝炎";
            this.chk家族史_肝炎.Properties.Caption = "肝炎";
            this.chk家族史_肝炎.Size = new System.Drawing.Size(45, 19);
            this.chk家族史_肝炎.TabIndex = 96;
            this.chk家族史_肝炎.Tag = "9";
            // 
            // chk家族史_先天畸形
            // 
            this.chk家族史_先天畸形.Enabled = false;
            this.chk家族史_先天畸形.Location = new System.Drawing.Point(0, 38);
            this.chk家族史_先天畸形.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_先天畸形.Name = "chk家族史_先天畸形";
            this.chk家族史_先天畸形.Properties.Caption = "先天畸形";
            this.chk家族史_先天畸形.Size = new System.Drawing.Size(77, 19);
            this.chk家族史_先天畸形.TabIndex = 97;
            this.chk家族史_先天畸形.Tag = "10";
            // 
            // chk家族史_其他
            // 
            this.chk家族史_其他.Enabled = false;
            this.chk家族史_其他.Location = new System.Drawing.Point(77, 38);
            this.chk家族史_其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk家族史_其他.Name = "chk家族史_其他";
            this.chk家族史_其他.Properties.Caption = "其他";
            this.chk家族史_其他.Size = new System.Drawing.Size(49, 19);
            this.chk家族史_其他.TabIndex = 98;
            this.chk家族史_其他.Tag = "100";
            this.chk家族史_其他.CheckedChanged += new System.EventHandler(this.chk家族史_其他_CheckedChanged);
            // 
            // txt家族史_其他
            // 
            this.txt家族史_其他.Enabled = false;
            this.txt家族史_其他.Location = new System.Drawing.Point(126, 38);
            this.txt家族史_其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt家族史_其他.Name = "txt家族史_其他";
            this.txt家族史_其他.Size = new System.Drawing.Size(86, 20);
            this.txt家族史_其他.TabIndex = 99;
            // 
            // cbo家族史关系
            // 
            this.cbo家族史关系.Enabled = false;
            this.cbo家族史关系.Location = new System.Drawing.Point(215, 38);
            this.cbo家族史关系.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.cbo家族史关系.Name = "cbo家族史关系";
            this.cbo家族史关系.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo家族史关系.Size = new System.Drawing.Size(84, 20);
            this.cbo家族史关系.TabIndex = 100;
            // 
            // btn家族史_添加
            // 
            this.btn家族史_添加.Enabled = false;
            this.btn家族史_添加.Location = new System.Drawing.Point(3, 61);
            this.btn家族史_添加.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.btn家族史_添加.Name = "btn家族史_添加";
            this.btn家族史_添加.Size = new System.Drawing.Size(56, 19);
            this.btn家族史_添加.TabIndex = 101;
            this.btn家族史_添加.Text = "添加";
            this.btn家族史_添加.Click += new System.EventHandler(this.btn家族史_添加_Click);
            // 
            // gc家族史
            // 
            this.gc家族史.ContextMenuStrip = this.contextMenuStrip1;
            this.gc家族史.Location = new System.Drawing.Point(369, 859);
            this.gc家族史.MainView = this.gv家族史;
            this.gc家族史.Name = "gc家族史";
            this.gc家族史.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpcbo家族关系,
            this.lkp家族关系});
            this.gc家族史.Size = new System.Drawing.Size(359, 86);
            this.gc家族史.TabIndex = 87;
            this.gc家族史.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv家族史});
            // 
            // gv家族史
            // 
            this.gv家族史.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.col家族关系,
            this.col家族病史,
            this.col其他疾病,
            this.col疾病编号,
            this.col家族病史代码});
            this.gv家族史.GridControl = this.gc家族史;
            this.gv家族史.Name = "gv家族史";
            this.gv家族史.OptionsSelection.MultiSelect = true;
            this.gv家族史.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gv家族史.OptionsView.ColumnAutoWidth = false;
            this.gv家族史.OptionsView.EnableAppearanceEvenRow = true;
            this.gv家族史.OptionsView.EnableAppearanceOddRow = true;
            this.gv家族史.OptionsView.ShowGroupPanel = false;
            // 
            // col家族关系
            // 
            this.col家族关系.AppearanceHeader.Options.UseTextOptions = true;
            this.col家族关系.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col家族关系.Caption = "家族关系";
            this.col家族关系.ColumnEdit = this.lkp家族关系;
            this.col家族关系.FieldName = "家族关系";
            this.col家族关系.Name = "col家族关系";
            this.col家族关系.Visible = true;
            this.col家族关系.VisibleIndex = 1;
            // 
            // lkp家族关系
            // 
            this.lkp家族关系.AutoHeight = false;
            this.lkp家族关系.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkp家族关系.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("P_DESC", "家族关系")});
            this.lkp家族关系.Name = "lkp家族关系";
            // 
            // col家族病史
            // 
            this.col家族病史.AppearanceHeader.Options.UseTextOptions = true;
            this.col家族病史.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col家族病史.Caption = "家族病史";
            this.col家族病史.FieldName = "D_JBBM";
            this.col家族病史.Name = "col家族病史";
            this.col家族病史.OptionsColumn.AllowEdit = false;
            this.col家族病史.OptionsColumn.ReadOnly = true;
            this.col家族病史.Visible = true;
            this.col家族病史.VisibleIndex = 2;
            this.col家族病史.Width = 98;
            // 
            // col其他疾病
            // 
            this.col其他疾病.AppearanceHeader.Options.UseTextOptions = true;
            this.col其他疾病.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col其他疾病.Caption = "其他疾病";
            this.col其他疾病.FieldName = "其他疾病";
            this.col其他疾病.Name = "col其他疾病";
            this.col其他疾病.OptionsColumn.AllowEdit = false;
            this.col其他疾病.OptionsColumn.ReadOnly = true;
            this.col其他疾病.Visible = true;
            this.col其他疾病.VisibleIndex = 3;
            this.col其他疾病.Width = 89;
            // 
            // col疾病编号
            // 
            this.col疾病编号.Caption = "疾病编号";
            this.col疾病编号.FieldName = "家族病史";
            this.col疾病编号.Name = "col疾病编号";
            // 
            // col家族病史代码
            // 
            this.col家族病史代码.Caption = "gridColumn1";
            this.col家族病史代码.FieldName = "家族关系";
            this.col家族病史代码.Name = "col家族病史代码";
            // 
            // rpcbo家族关系
            // 
            this.rpcbo家族关系.AutoHeight = false;
            this.rpcbo家族关系.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpcbo家族关系.Name = "rpcbo家族关系";
            // 
            // cbo残疾情况
            // 
            this.cbo残疾情况.EditValue = "无";
            this.cbo残疾情况.Location = new System.Drawing.Point(107, 1051);
            this.cbo残疾情况.Name = "cbo残疾情况";
            this.cbo残疾情况.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo残疾情况.Properties.Items.AddRange(new object[] {
            "有",
            "无"});
            this.cbo残疾情况.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo残疾情况.Size = new System.Drawing.Size(50, 20);
            this.cbo残疾情况.StyleController = this.layoutControl1;
            this.cbo残疾情况.TabIndex = 108;
            this.cbo残疾情况.SelectedIndexChanged += new System.EventHandler(this.cbo残疾情况_SelectedIndexChanged);
            // 
            // cbo遗传病史
            // 
            this.cbo遗传病史.EditValue = "无";
            this.cbo遗传病史.Location = new System.Drawing.Point(107, 1001);
            this.cbo遗传病史.Name = "cbo遗传病史";
            this.cbo遗传病史.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo遗传病史.Properties.Items.AddRange(new object[] {
            "有",
            "无"});
            this.cbo遗传病史.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo遗传病史.Size = new System.Drawing.Size(50, 20);
            this.cbo遗传病史.StyleController = this.layoutControl1;
            this.cbo遗传病史.TabIndex = 106;
            this.cbo遗传病史.SelectedIndexChanged += new System.EventHandler(this.cbo遗传病史_SelectedIndexChanged);
            // 
            // cbo暴露史
            // 
            this.cbo暴露史.EditValue = "无";
            this.cbo暴露史.Location = new System.Drawing.Point(107, 951);
            this.cbo暴露史.Name = "cbo暴露史";
            this.cbo暴露史.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo暴露史.Properties.Items.AddRange(new object[] {
            "有",
            "无"});
            this.cbo暴露史.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo暴露史.Size = new System.Drawing.Size(50, 20);
            this.cbo暴露史.StyleController = this.layoutControl1;
            this.cbo暴露史.TabIndex = 102;
            this.cbo暴露史.SelectedIndexChanged += new System.EventHandler(this.cbo暴露史_SelectedIndexChanged);
            // 
            // cbo家族史
            // 
            this.cbo家族史.EditValue = "无";
            this.cbo家族史.Location = new System.Drawing.Point(107, 835);
            this.cbo家族史.Name = "cbo家族史";
            this.cbo家族史.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo家族史.Properties.Items.AddRange(new object[] {
            "有",
            "无"});
            this.cbo家族史.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo家族史.Size = new System.Drawing.Size(50, 20);
            this.cbo家族史.StyleController = this.layoutControl1;
            this.cbo家族史.TabIndex = 86;
            this.cbo家族史.SelectedIndexChanged += new System.EventHandler(this.cbo家族史_SelectedIndexChanged);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl10);
            this.panelControl1.Controls.Add(this.txt居住地址_详细地址);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.cbo居住地址_市);
            this.panelControl1.Controls.Add(this.cbo居住地址_县);
            this.panelControl1.Controls.Add(this.cbo居住地址_街道);
            this.panelControl1.Controls.Add(this.cbo居住地址_村委会);
            this.panelControl1.Location = new System.Drawing.Point(112, 457);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(616, 56);
            this.panelControl1.TabIndex = 36;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.labelControl10.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl10.Location = new System.Drawing.Point(5, 27);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(65, 14);
            this.labelControl10.TabIndex = 7;
            this.labelControl10.Text = "详细地址：";
            // 
            // txt居住地址_详细地址
            // 
            this.txt居住地址_详细地址.Location = new System.Drawing.Point(78, 25);
            this.txt居住地址_详细地址.Name = "txt居住地址_详细地址";
            this.txt居住地址_详细地址.Size = new System.Drawing.Size(297, 20);
            this.txt居住地址_详细地址.TabIndex = 41;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(336, 5);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(53, 14);
            this.labelControl9.TabIndex = 3;
            this.labelControl9.Text = "居/村委会";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(192, 4);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(53, 14);
            this.labelControl8.TabIndex = 2;
            this.labelControl8.Text = "街道/乡镇";
            // 
            // cbo居住地址_市
            // 
            this.cbo居住地址_市.Location = new System.Drawing.Point(4, 3);
            this.cbo居住地址_市.Name = "cbo居住地址_市";
            this.cbo居住地址_市.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo居住地址_市.Properties.NullText = "";
            this.cbo居住地址_市.Properties.PopupSizeable = false;
            this.cbo居住地址_市.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbo居住地址_市.Size = new System.Drawing.Size(86, 20);
            this.cbo居住地址_市.TabIndex = 37;
            // 
            // cbo居住地址_县
            // 
            this.cbo居住地址_县.Location = new System.Drawing.Point(95, 3);
            this.cbo居住地址_县.Name = "cbo居住地址_县";
            this.cbo居住地址_县.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo居住地址_县.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("地区编码", "编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("地区名称", "名称")});
            this.cbo居住地址_县.Properties.NullText = "";
            this.cbo居住地址_县.Properties.PopupSizeable = false;
            this.cbo居住地址_县.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbo居住地址_县.Size = new System.Drawing.Size(86, 20);
            this.cbo居住地址_县.TabIndex = 38;
            // 
            // cbo居住地址_街道
            // 
            this.cbo居住地址_街道.Location = new System.Drawing.Point(246, 3);
            this.cbo居住地址_街道.Name = "cbo居住地址_街道";
            this.cbo居住地址_街道.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo居住地址_街道.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("地区编码", "编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("地区名称", "名称")});
            this.cbo居住地址_街道.Properties.NullText = "";
            this.cbo居住地址_街道.Properties.PopupSizeable = false;
            this.cbo居住地址_街道.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbo居住地址_街道.Size = new System.Drawing.Size(91, 20);
            this.cbo居住地址_街道.TabIndex = 39;
            // 
            // cbo居住地址_村委会
            // 
            this.cbo居住地址_村委会.Location = new System.Drawing.Point(387, 2);
            this.cbo居住地址_村委会.Name = "cbo居住地址_村委会";
            this.cbo居住地址_村委会.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo居住地址_村委会.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("地区编码", "编码"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("地区名称", "名称")});
            this.cbo居住地址_村委会.Properties.NullText = "";
            this.cbo居住地址_村委会.Properties.PopupSizeable = false;
            this.cbo居住地址_村委会.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cbo居住地址_村委会.Size = new System.Drawing.Size(151, 20);
            this.cbo居住地址_村委会.TabIndex = 40;
            this.cbo居住地址_村委会.EditValueChanged += new System.EventHandler(this.cbo居住地址_村委会_EditValueChanged);
            // 
            // btn重复档案检测
            // 
            this.btn重复档案检测.Location = new System.Drawing.Point(604, 79);
            this.btn重复档案检测.Name = "btn重复档案检测";
            this.btn重复档案检测.Size = new System.Drawing.Size(124, 22);
            this.btn重复档案检测.StyleController = this.layoutControl1;
            this.btn重复档案检测.TabIndex = 10;
            this.btn重复档案检测.Text = "重复档案检测";
            this.btn重复档案检测.Click += new System.EventHandler(this.btn重复档案检测_Click);
            // 
            // cbo档案类别
            // 
            this.cbo档案类别.Location = new System.Drawing.Point(637, 517);
            this.cbo档案类别.Name = "cbo档案类别";
            this.cbo档案类别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo档案类别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo档案类别.Size = new System.Drawing.Size(91, 20);
            this.cbo档案类别.StyleController = this.layoutControl1;
            this.cbo档案类别.TabIndex = 43;
            // 
            // radio档案状态
            // 
            this.radio档案状态.EditValue = "1";
            this.radio档案状态.Location = new System.Drawing.Point(462, 31);
            this.radio档案状态.Name = "radio档案状态";
            this.radio档案状态.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "活动"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "非活动")});
            this.radio档案状态.Size = new System.Drawing.Size(141, 20);
            this.radio档案状态.StyleController = this.layoutControl1;
            this.radio档案状态.TabIndex = 3;
            // 
            // flow禽畜栏
            // 
            this.flow禽畜栏.BackColor = System.Drawing.SystemColors.Control;
            this.flow禽畜栏.Controls.Add(this.chk禽畜栏_单设);
            this.flow禽畜栏.Controls.Add(this.chk禽畜栏_室内);
            this.flow禽畜栏.Controls.Add(this.chk禽畜栏_室外);
            this.flow禽畜栏.Controls.Add(this.chk禽畜栏_无);
            this.flow禽畜栏.Location = new System.Drawing.Point(114, 1309);
            this.flow禽畜栏.Name = "flow禽畜栏";
            this.flow禽畜栏.Size = new System.Drawing.Size(614, 20);
            this.flow禽畜栏.TabIndex = 150;
            // 
            // chk禽畜栏_单设
            // 
            this.chk禽畜栏_单设.Location = new System.Drawing.Point(0, 0);
            this.chk禽畜栏_单设.Margin = new System.Windows.Forms.Padding(0);
            this.chk禽畜栏_单设.Name = "chk禽畜栏_单设";
            this.chk禽畜栏_单设.Properties.Caption = "单设";
            this.chk禽畜栏_单设.Size = new System.Drawing.Size(64, 19);
            this.chk禽畜栏_单设.TabIndex = 151;
            this.chk禽畜栏_单设.Tag = "1";
            // 
            // chk禽畜栏_室内
            // 
            this.chk禽畜栏_室内.Location = new System.Drawing.Point(64, 0);
            this.chk禽畜栏_室内.Margin = new System.Windows.Forms.Padding(0);
            this.chk禽畜栏_室内.Name = "chk禽畜栏_室内";
            this.chk禽畜栏_室内.Properties.Caption = "室内";
            this.chk禽畜栏_室内.Size = new System.Drawing.Size(64, 19);
            this.chk禽畜栏_室内.TabIndex = 152;
            this.chk禽畜栏_室内.Tag = "2";
            // 
            // chk禽畜栏_室外
            // 
            this.chk禽畜栏_室外.Location = new System.Drawing.Point(128, 0);
            this.chk禽畜栏_室外.Margin = new System.Windows.Forms.Padding(0);
            this.chk禽畜栏_室外.Name = "chk禽畜栏_室外";
            this.chk禽畜栏_室外.Properties.Caption = "室外";
            this.chk禽畜栏_室外.Size = new System.Drawing.Size(64, 19);
            this.chk禽畜栏_室外.TabIndex = 153;
            this.chk禽畜栏_室外.Tag = "3";
            // 
            // chk禽畜栏_无
            // 
            this.chk禽畜栏_无.Location = new System.Drawing.Point(192, 0);
            this.chk禽畜栏_无.Margin = new System.Windows.Forms.Padding(0);
            this.chk禽畜栏_无.Name = "chk禽畜栏_无";
            this.chk禽畜栏_无.Properties.Caption = "无";
            this.chk禽畜栏_无.Size = new System.Drawing.Size(64, 19);
            this.chk禽畜栏_无.TabIndex = 154;
            this.chk禽畜栏_无.Tag = "4";
            // 
            // flow厕所
            // 
            this.flow厕所.Controls.Add(this.chk厕所_卫生厕所);
            this.flow厕所.Controls.Add(this.chk厕所_一格或二格粪池式);
            this.flow厕所.Controls.Add(this.chk厕所_露天粪坑);
            this.flow厕所.Controls.Add(this.chk厕所_马桶);
            this.flow厕所.Controls.Add(this.chk厕所_简易棚侧);
            this.flow厕所.Location = new System.Drawing.Point(114, 1285);
            this.flow厕所.Name = "flow厕所";
            this.flow厕所.Size = new System.Drawing.Size(614, 20);
            this.flow厕所.TabIndex = 144;
            // 
            // chk厕所_卫生厕所
            // 
            this.chk厕所_卫生厕所.Location = new System.Drawing.Point(0, 0);
            this.chk厕所_卫生厕所.Margin = new System.Windows.Forms.Padding(0);
            this.chk厕所_卫生厕所.Name = "chk厕所_卫生厕所";
            this.chk厕所_卫生厕所.Properties.Caption = "卫生厕所";
            this.chk厕所_卫生厕所.Size = new System.Drawing.Size(81, 19);
            this.chk厕所_卫生厕所.TabIndex = 145;
            this.chk厕所_卫生厕所.Tag = "1";
            // 
            // chk厕所_一格或二格粪池式
            // 
            this.chk厕所_一格或二格粪池式.Location = new System.Drawing.Point(81, 0);
            this.chk厕所_一格或二格粪池式.Margin = new System.Windows.Forms.Padding(0);
            this.chk厕所_一格或二格粪池式.Name = "chk厕所_一格或二格粪池式";
            this.chk厕所_一格或二格粪池式.Properties.Caption = "一格或二格粪池式";
            this.chk厕所_一格或二格粪池式.Size = new System.Drawing.Size(106, 19);
            this.chk厕所_一格或二格粪池式.TabIndex = 146;
            this.chk厕所_一格或二格粪池式.Tag = "2";
            // 
            // chk厕所_露天粪坑
            // 
            this.chk厕所_露天粪坑.Location = new System.Drawing.Point(187, 0);
            this.chk厕所_露天粪坑.Margin = new System.Windows.Forms.Padding(0);
            this.chk厕所_露天粪坑.Name = "chk厕所_露天粪坑";
            this.chk厕所_露天粪坑.Properties.Caption = "露天粪坑";
            this.chk厕所_露天粪坑.Size = new System.Drawing.Size(86, 19);
            this.chk厕所_露天粪坑.TabIndex = 147;
            this.chk厕所_露天粪坑.Tag = "4";
            // 
            // chk厕所_马桶
            // 
            this.chk厕所_马桶.Location = new System.Drawing.Point(273, 0);
            this.chk厕所_马桶.Margin = new System.Windows.Forms.Padding(0);
            this.chk厕所_马桶.Name = "chk厕所_马桶";
            this.chk厕所_马桶.Properties.Caption = "马桶";
            this.chk厕所_马桶.Size = new System.Drawing.Size(64, 19);
            this.chk厕所_马桶.TabIndex = 148;
            this.chk厕所_马桶.Tag = "3";
            // 
            // chk厕所_简易棚侧
            // 
            this.chk厕所_简易棚侧.Location = new System.Drawing.Point(337, 0);
            this.chk厕所_简易棚侧.Margin = new System.Windows.Forms.Padding(0);
            this.chk厕所_简易棚侧.Name = "chk厕所_简易棚侧";
            this.chk厕所_简易棚侧.Properties.Caption = "简易棚厕";
            this.chk厕所_简易棚侧.Size = new System.Drawing.Size(89, 19);
            this.chk厕所_简易棚侧.TabIndex = 149;
            this.chk厕所_简易棚侧.Tag = "5";
            // 
            // flow饮水
            // 
            this.flow饮水.BackColor = System.Drawing.SystemColors.Control;
            this.flow饮水.Controls.Add(this.chk饮水_自来水);
            this.flow饮水.Controls.Add(this.chk饮水_经净化过滤的水);
            this.flow饮水.Controls.Add(this.chk饮水_井水);
            this.flow饮水.Controls.Add(this.chk饮水_河湖水);
            this.flow饮水.Controls.Add(this.chk饮水_糖水);
            this.flow饮水.Controls.Add(this.chk饮水_其他);
            this.flow饮水.Location = new System.Drawing.Point(114, 1261);
            this.flow饮水.Name = "flow饮水";
            this.flow饮水.Size = new System.Drawing.Size(614, 20);
            this.flow饮水.TabIndex = 137;
            // 
            // chk饮水_自来水
            // 
            this.chk饮水_自来水.Location = new System.Drawing.Point(0, 0);
            this.chk饮水_自来水.Margin = new System.Windows.Forms.Padding(0);
            this.chk饮水_自来水.Name = "chk饮水_自来水";
            this.chk饮水_自来水.Properties.Caption = "自来水";
            this.chk饮水_自来水.Size = new System.Drawing.Size(64, 19);
            this.chk饮水_自来水.TabIndex = 138;
            this.chk饮水_自来水.Tag = "1";
            // 
            // chk饮水_经净化过滤的水
            // 
            this.chk饮水_经净化过滤的水.Location = new System.Drawing.Point(64, 0);
            this.chk饮水_经净化过滤的水.Margin = new System.Windows.Forms.Padding(0);
            this.chk饮水_经净化过滤的水.Name = "chk饮水_经净化过滤的水";
            this.chk饮水_经净化过滤的水.Properties.Caption = "经净化过滤的水";
            this.chk饮水_经净化过滤的水.Size = new System.Drawing.Size(106, 19);
            this.chk饮水_经净化过滤的水.TabIndex = 139;
            this.chk饮水_经净化过滤的水.Tag = "2";
            // 
            // chk饮水_井水
            // 
            this.chk饮水_井水.Location = new System.Drawing.Point(170, 0);
            this.chk饮水_井水.Margin = new System.Windows.Forms.Padding(0);
            this.chk饮水_井水.Name = "chk饮水_井水";
            this.chk饮水_井水.Properties.Caption = "井水";
            this.chk饮水_井水.Size = new System.Drawing.Size(63, 19);
            this.chk饮水_井水.TabIndex = 140;
            this.chk饮水_井水.Tag = "3";
            // 
            // chk饮水_河湖水
            // 
            this.chk饮水_河湖水.Location = new System.Drawing.Point(233, 0);
            this.chk饮水_河湖水.Margin = new System.Windows.Forms.Padding(0);
            this.chk饮水_河湖水.Name = "chk饮水_河湖水";
            this.chk饮水_河湖水.Properties.Caption = "河湖水";
            this.chk饮水_河湖水.Size = new System.Drawing.Size(64, 19);
            this.chk饮水_河湖水.TabIndex = 141;
            this.chk饮水_河湖水.Tag = "4";
            // 
            // chk饮水_糖水
            // 
            this.chk饮水_糖水.Location = new System.Drawing.Point(297, 0);
            this.chk饮水_糖水.Margin = new System.Windows.Forms.Padding(0);
            this.chk饮水_糖水.Name = "chk饮水_糖水";
            this.chk饮水_糖水.Properties.Caption = "塘水";
            this.chk饮水_糖水.Size = new System.Drawing.Size(64, 19);
            this.chk饮水_糖水.TabIndex = 142;
            this.chk饮水_糖水.Tag = "5";
            // 
            // chk饮水_其他
            // 
            this.chk饮水_其他.Location = new System.Drawing.Point(361, 0);
            this.chk饮水_其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk饮水_其他.Name = "chk饮水_其他";
            this.chk饮水_其他.Properties.Caption = "其他";
            this.chk饮水_其他.Size = new System.Drawing.Size(64, 19);
            this.chk饮水_其他.TabIndex = 143;
            this.chk饮水_其他.Tag = "6";
            // 
            // flow燃料类型
            // 
            this.flow燃料类型.Controls.Add(this.chk燃料类型_液化气);
            this.flow燃料类型.Controls.Add(this.chk燃料类型_煤);
            this.flow燃料类型.Controls.Add(this.chk燃料类型_天然气);
            this.flow燃料类型.Controls.Add(this.chk燃料类型_沼气);
            this.flow燃料类型.Controls.Add(this.chk燃料类型_柴火);
            this.flow燃料类型.Controls.Add(this.chk燃料类型_其他);
            this.flow燃料类型.Location = new System.Drawing.Point(114, 1237);
            this.flow燃料类型.Name = "flow燃料类型";
            this.flow燃料类型.Size = new System.Drawing.Size(614, 20);
            this.flow燃料类型.TabIndex = 130;
            // 
            // chk燃料类型_液化气
            // 
            this.chk燃料类型_液化气.Location = new System.Drawing.Point(0, 0);
            this.chk燃料类型_液化气.Margin = new System.Windows.Forms.Padding(0);
            this.chk燃料类型_液化气.Name = "chk燃料类型_液化气";
            this.chk燃料类型_液化气.Properties.Caption = "液化气";
            this.chk燃料类型_液化气.Size = new System.Drawing.Size(64, 19);
            this.chk燃料类型_液化气.TabIndex = 131;
            this.chk燃料类型_液化气.Tag = "1";
            // 
            // chk燃料类型_煤
            // 
            this.chk燃料类型_煤.Location = new System.Drawing.Point(64, 0);
            this.chk燃料类型_煤.Margin = new System.Windows.Forms.Padding(0);
            this.chk燃料类型_煤.Name = "chk燃料类型_煤";
            this.chk燃料类型_煤.Properties.Caption = "煤";
            this.chk燃料类型_煤.Size = new System.Drawing.Size(64, 19);
            this.chk燃料类型_煤.TabIndex = 132;
            this.chk燃料类型_煤.Tag = "2";
            // 
            // chk燃料类型_天然气
            // 
            this.chk燃料类型_天然气.Location = new System.Drawing.Point(128, 0);
            this.chk燃料类型_天然气.Margin = new System.Windows.Forms.Padding(0);
            this.chk燃料类型_天然气.Name = "chk燃料类型_天然气";
            this.chk燃料类型_天然气.Properties.Caption = "天然气";
            this.chk燃料类型_天然气.Size = new System.Drawing.Size(64, 19);
            this.chk燃料类型_天然气.TabIndex = 133;
            this.chk燃料类型_天然气.Tag = "3";
            // 
            // chk燃料类型_沼气
            // 
            this.chk燃料类型_沼气.Location = new System.Drawing.Point(192, 0);
            this.chk燃料类型_沼气.Margin = new System.Windows.Forms.Padding(0);
            this.chk燃料类型_沼气.Name = "chk燃料类型_沼气";
            this.chk燃料类型_沼气.Properties.Caption = "沼气";
            this.chk燃料类型_沼气.Size = new System.Drawing.Size(64, 19);
            this.chk燃料类型_沼气.TabIndex = 134;
            this.chk燃料类型_沼气.Tag = "4";
            // 
            // chk燃料类型_柴火
            // 
            this.chk燃料类型_柴火.Location = new System.Drawing.Point(256, 0);
            this.chk燃料类型_柴火.Margin = new System.Windows.Forms.Padding(0);
            this.chk燃料类型_柴火.Name = "chk燃料类型_柴火";
            this.chk燃料类型_柴火.Properties.Caption = "柴火";
            this.chk燃料类型_柴火.Size = new System.Drawing.Size(64, 19);
            this.chk燃料类型_柴火.TabIndex = 135;
            this.chk燃料类型_柴火.Tag = "5";
            // 
            // chk燃料类型_其他
            // 
            this.chk燃料类型_其他.Location = new System.Drawing.Point(320, 0);
            this.chk燃料类型_其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk燃料类型_其他.Name = "chk燃料类型_其他";
            this.chk燃料类型_其他.Properties.Caption = "其他";
            this.chk燃料类型_其他.Size = new System.Drawing.Size(64, 19);
            this.chk燃料类型_其他.TabIndex = 136;
            this.chk燃料类型_其他.Tag = "6";
            // 
            // flow厨房排风设施
            // 
            this.flow厨房排风设施.BackColor = System.Drawing.SystemColors.Control;
            this.flow厨房排风设施.Controls.Add(this.chk厨房排风设施_无);
            this.flow厨房排风设施.Controls.Add(this.chk厨房排风设施_油烟机);
            this.flow厨房排风设施.Controls.Add(this.chk厨房排风设施_换气扇);
            this.flow厨房排风设施.Controls.Add(this.chk厨房排风设施_烟囱);
            this.flow厨房排风设施.Location = new System.Drawing.Point(114, 1213);
            this.flow厨房排风设施.Name = "flow厨房排风设施";
            this.flow厨房排风设施.Size = new System.Drawing.Size(614, 20);
            this.flow厨房排风设施.TabIndex = 125;
            // 
            // chk厨房排风设施_无
            // 
            this.chk厨房排风设施_无.Location = new System.Drawing.Point(0, 0);
            this.chk厨房排风设施_无.Margin = new System.Windows.Forms.Padding(0);
            this.chk厨房排风设施_无.Name = "chk厨房排风设施_无";
            this.chk厨房排风设施_无.Properties.Caption = "无";
            this.chk厨房排风设施_无.Size = new System.Drawing.Size(64, 19);
            this.chk厨房排风设施_无.TabIndex = 126;
            this.chk厨房排风设施_无.Tag = "1";
            this.chk厨房排风设施_无.CheckedChanged += new System.EventHandler(this.chk厨房排风设施_无_CheckedChanged);
            // 
            // chk厨房排风设施_油烟机
            // 
            this.chk厨房排风设施_油烟机.Location = new System.Drawing.Point(64, 0);
            this.chk厨房排风设施_油烟机.Margin = new System.Windows.Forms.Padding(0);
            this.chk厨房排风设施_油烟机.Name = "chk厨房排风设施_油烟机";
            this.chk厨房排风设施_油烟机.Properties.Caption = "油烟机";
            this.chk厨房排风设施_油烟机.Size = new System.Drawing.Size(64, 19);
            this.chk厨房排风设施_油烟机.TabIndex = 127;
            this.chk厨房排风设施_油烟机.Tag = "2";
            // 
            // chk厨房排风设施_换气扇
            // 
            this.chk厨房排风设施_换气扇.Location = new System.Drawing.Point(128, 0);
            this.chk厨房排风设施_换气扇.Margin = new System.Windows.Forms.Padding(0);
            this.chk厨房排风设施_换气扇.Name = "chk厨房排风设施_换气扇";
            this.chk厨房排风设施_换气扇.Properties.Caption = "换气扇";
            this.chk厨房排风设施_换气扇.Size = new System.Drawing.Size(64, 19);
            this.chk厨房排风设施_换气扇.TabIndex = 128;
            this.chk厨房排风设施_换气扇.Tag = "3";
            // 
            // chk厨房排风设施_烟囱
            // 
            this.chk厨房排风设施_烟囱.Location = new System.Drawing.Point(192, 0);
            this.chk厨房排风设施_烟囱.Margin = new System.Windows.Forms.Padding(0);
            this.chk厨房排风设施_烟囱.Name = "chk厨房排风设施_烟囱";
            this.chk厨房排风设施_烟囱.Properties.Caption = "烟囱";
            this.chk厨房排风设施_烟囱.Size = new System.Drawing.Size(64, 19);
            this.chk厨房排风设施_烟囱.TabIndex = 129;
            this.chk厨房排风设施_烟囱.Tag = "4";
            // 
            // flow残疾情况
            // 
            this.flow残疾情况.Controls.Add(this.chk残疾情况_听力残);
            this.flow残疾情况.Controls.Add(this.chk残疾情况_言语残);
            this.flow残疾情况.Controls.Add(this.chk残疾情况_肢体残);
            this.flow残疾情况.Controls.Add(this.chk残疾情况_智力残);
            this.flow残疾情况.Controls.Add(this.chk残疾情况_视力残);
            this.flow残疾情况.Controls.Add(this.chk残疾情况_精神残);
            this.flow残疾情况.Controls.Add(this.chk残疾情况_其他残疾);
            this.flow残疾情况.Controls.Add(this.txt残疾情况_其他残疾);
            this.flow残疾情况.Location = new System.Drawing.Point(7, 1075);
            this.flow残疾情况.Name = "flow残疾情况";
            this.flow残疾情况.Size = new System.Drawing.Size(721, 20);
            this.flow残疾情况.TabIndex = 109;
            // 
            // chk残疾情况_听力残
            // 
            this.chk残疾情况_听力残.Enabled = false;
            this.chk残疾情况_听力残.Location = new System.Drawing.Point(0, 0);
            this.chk残疾情况_听力残.Margin = new System.Windows.Forms.Padding(0);
            this.chk残疾情况_听力残.Name = "chk残疾情况_听力残";
            this.chk残疾情况_听力残.Properties.Caption = "听力残疾";
            this.chk残疾情况_听力残.Size = new System.Drawing.Size(75, 19);
            this.chk残疾情况_听力残.TabIndex = 110;
            this.chk残疾情况_听力残.Tag = "2";
            // 
            // chk残疾情况_言语残
            // 
            this.chk残疾情况_言语残.Enabled = false;
            this.chk残疾情况_言语残.Location = new System.Drawing.Point(75, 0);
            this.chk残疾情况_言语残.Margin = new System.Windows.Forms.Padding(0);
            this.chk残疾情况_言语残.Name = "chk残疾情况_言语残";
            this.chk残疾情况_言语残.Properties.Caption = "言语残疾";
            this.chk残疾情况_言语残.Size = new System.Drawing.Size(75, 19);
            this.chk残疾情况_言语残.TabIndex = 111;
            this.chk残疾情况_言语残.Tag = "3";
            // 
            // chk残疾情况_肢体残
            // 
            this.chk残疾情况_肢体残.Enabled = false;
            this.chk残疾情况_肢体残.Location = new System.Drawing.Point(150, 0);
            this.chk残疾情况_肢体残.Margin = new System.Windows.Forms.Padding(0);
            this.chk残疾情况_肢体残.Name = "chk残疾情况_肢体残";
            this.chk残疾情况_肢体残.Properties.Caption = "肢体残疾";
            this.chk残疾情况_肢体残.Size = new System.Drawing.Size(72, 19);
            this.chk残疾情况_肢体残.TabIndex = 112;
            this.chk残疾情况_肢体残.Tag = "4";
            // 
            // chk残疾情况_智力残
            // 
            this.chk残疾情况_智力残.Enabled = false;
            this.chk残疾情况_智力残.Location = new System.Drawing.Point(222, 0);
            this.chk残疾情况_智力残.Margin = new System.Windows.Forms.Padding(0);
            this.chk残疾情况_智力残.Name = "chk残疾情况_智力残";
            this.chk残疾情况_智力残.Properties.Caption = "智力残疾";
            this.chk残疾情况_智力残.Size = new System.Drawing.Size(74, 19);
            this.chk残疾情况_智力残.TabIndex = 113;
            this.chk残疾情况_智力残.Tag = "5";
            // 
            // chk残疾情况_视力残
            // 
            this.chk残疾情况_视力残.Enabled = false;
            this.chk残疾情况_视力残.Location = new System.Drawing.Point(296, 0);
            this.chk残疾情况_视力残.Margin = new System.Windows.Forms.Padding(0);
            this.chk残疾情况_视力残.Name = "chk残疾情况_视力残";
            this.chk残疾情况_视力残.Properties.Caption = "视力残疾";
            this.chk残疾情况_视力残.Size = new System.Drawing.Size(79, 19);
            this.chk残疾情况_视力残.TabIndex = 114;
            this.chk残疾情况_视力残.Tag = "6";
            // 
            // chk残疾情况_精神残
            // 
            this.chk残疾情况_精神残.Enabled = false;
            this.chk残疾情况_精神残.Location = new System.Drawing.Point(375, 0);
            this.chk残疾情况_精神残.Margin = new System.Windows.Forms.Padding(0);
            this.chk残疾情况_精神残.Name = "chk残疾情况_精神残";
            this.chk残疾情况_精神残.Properties.Caption = "精神残疾";
            this.chk残疾情况_精神残.Size = new System.Drawing.Size(75, 19);
            this.chk残疾情况_精神残.TabIndex = 115;
            this.chk残疾情况_精神残.Tag = "7";
            // 
            // chk残疾情况_其他残疾
            // 
            this.chk残疾情况_其他残疾.Enabled = false;
            this.chk残疾情况_其他残疾.Location = new System.Drawing.Point(450, 0);
            this.chk残疾情况_其他残疾.Margin = new System.Windows.Forms.Padding(0);
            this.chk残疾情况_其他残疾.Name = "chk残疾情况_其他残疾";
            this.chk残疾情况_其他残疾.Properties.Caption = "其他残疾";
            this.chk残疾情况_其他残疾.Size = new System.Drawing.Size(82, 19);
            this.chk残疾情况_其他残疾.TabIndex = 116;
            this.chk残疾情况_其他残疾.Tag = "9";
            this.chk残疾情况_其他残疾.CheckedChanged += new System.EventHandler(this.chk残疾情况_其他残疾_CheckedChanged);
            // 
            // txt残疾情况_其他残疾
            // 
            this.txt残疾情况_其他残疾.Enabled = false;
            this.txt残疾情况_其他残疾.Location = new System.Drawing.Point(532, 0);
            this.txt残疾情况_其他残疾.Margin = new System.Windows.Forms.Padding(0);
            this.txt残疾情况_其他残疾.Name = "txt残疾情况_其他残疾";
            this.txt残疾情况_其他残疾.Size = new System.Drawing.Size(127, 20);
            this.txt残疾情况_其他残疾.TabIndex = 117;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.radio输血);
            this.flowLayoutPanel6.Controls.Add(this.labelControl6);
            this.flowLayoutPanel6.Controls.Add(this.txt输血原因);
            this.flowLayoutPanel6.Controls.Add(this.labelControl7);
            this.flowLayoutPanel6.Controls.Add(this.dte输血时间);
            this.flowLayoutPanel6.Controls.Add(this.btn输血_添加);
            this.flowLayoutPanel6.Location = new System.Drawing.Point(114, 705);
            this.flowLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(614, 24);
            this.flowLayoutPanel6.TabIndex = 81;
            // 
            // radio输血
            // 
            this.radio输血.Location = new System.Drawing.Point(0, 0);
            this.radio输血.Margin = new System.Windows.Forms.Padding(0);
            this.radio输血.Name = "radio输血";
            this.radio输血.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有")});
            this.radio输血.Size = new System.Drawing.Size(86, 25);
            this.radio输血.TabIndex = 82;
            this.radio输血.SelectedIndexChanged += new System.EventHandler(this.radio输血_SelectedIndexChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(86, 0);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(48, 14);
            this.labelControl6.TabIndex = 19;
            this.labelControl6.Text = "输血原因";
            // 
            // txt输血原因
            // 
            this.txt输血原因.Enabled = false;
            this.txt输血原因.Location = new System.Drawing.Point(134, 0);
            this.txt输血原因.Margin = new System.Windows.Forms.Padding(0);
            this.txt输血原因.Name = "txt输血原因";
            this.txt输血原因.Size = new System.Drawing.Size(86, 20);
            this.txt输血原因.TabIndex = 83;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(220, 0);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(48, 14);
            this.labelControl7.TabIndex = 21;
            this.labelControl7.Text = "      时间";
            // 
            // dte输血时间
            // 
            this.dte输血时间.EditValue = null;
            this.dte输血时间.Enabled = false;
            this.dte输血时间.Location = new System.Drawing.Point(268, 0);
            this.dte输血时间.Margin = new System.Windows.Forms.Padding(0);
            this.dte输血时间.Name = "dte输血时间";
            this.dte输血时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte输血时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte输血时间.Properties.Mask.EditMask = "";
            this.dte输血时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.dte输血时间.Size = new System.Drawing.Size(86, 20);
            this.dte输血时间.TabIndex = 84;
            // 
            // btn输血_添加
            // 
            this.btn输血_添加.Enabled = false;
            this.btn输血_添加.Location = new System.Drawing.Point(354, 0);
            this.btn输血_添加.Margin = new System.Windows.Forms.Padding(0);
            this.btn输血_添加.Name = "btn输血_添加";
            this.btn输血_添加.Size = new System.Drawing.Size(52, 20);
            this.btn输血_添加.TabIndex = 85;
            this.btn输血_添加.Text = "添加";
            this.btn输血_添加.Click += new System.EventHandler(this.btn输血_添加_Click);
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BackColor = System.Drawing.SystemColors.Control;
            this.flowLayoutPanel5.Controls.Add(this.radio外伤);
            this.flowLayoutPanel5.Controls.Add(this.labelControl4);
            this.flowLayoutPanel5.Controls.Add(this.txt外伤名称);
            this.flowLayoutPanel5.Controls.Add(this.labelControl5);
            this.flowLayoutPanel5.Controls.Add(this.dte外伤时间);
            this.flowLayoutPanel5.Controls.Add(this.btn外伤_添加);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(114, 675);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(614, 24);
            this.flowLayoutPanel5.TabIndex = 76;
            // 
            // radio外伤
            // 
            this.radio外伤.Location = new System.Drawing.Point(0, 0);
            this.radio外伤.Margin = new System.Windows.Forms.Padding(0);
            this.radio外伤.Name = "radio外伤";
            this.radio外伤.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有")});
            this.radio外伤.Size = new System.Drawing.Size(86, 28);
            this.radio外伤.TabIndex = 77;
            this.radio外伤.SelectedIndexChanged += new System.EventHandler(this.radio外伤_SelectedIndexChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(86, 0);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(48, 14);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "外伤名称";
            // 
            // txt外伤名称
            // 
            this.txt外伤名称.Enabled = false;
            this.txt外伤名称.Location = new System.Drawing.Point(134, 0);
            this.txt外伤名称.Margin = new System.Windows.Forms.Padding(0);
            this.txt外伤名称.Name = "txt外伤名称";
            this.txt外伤名称.Size = new System.Drawing.Size(86, 20);
            this.txt外伤名称.TabIndex = 78;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(220, 0);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(48, 14);
            this.labelControl5.TabIndex = 15;
            this.labelControl5.Text = "      时间";
            // 
            // dte外伤时间
            // 
            this.dte外伤时间.EditValue = null;
            this.dte外伤时间.Enabled = false;
            this.dte外伤时间.Location = new System.Drawing.Point(268, 0);
            this.dte外伤时间.Margin = new System.Windows.Forms.Padding(0);
            this.dte外伤时间.Name = "dte外伤时间";
            this.dte外伤时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte外伤时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte外伤时间.Properties.Mask.EditMask = "";
            this.dte外伤时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.dte外伤时间.Size = new System.Drawing.Size(86, 20);
            this.dte外伤时间.TabIndex = 79;
            // 
            // btn外伤_添加
            // 
            this.btn外伤_添加.Enabled = false;
            this.btn外伤_添加.Location = new System.Drawing.Point(354, 0);
            this.btn外伤_添加.Margin = new System.Windows.Forms.Padding(0);
            this.btn外伤_添加.Name = "btn外伤_添加";
            this.btn外伤_添加.Size = new System.Drawing.Size(52, 20);
            this.btn外伤_添加.TabIndex = 80;
            this.btn外伤_添加.Text = "添加";
            this.btn外伤_添加.Click += new System.EventHandler(this.btn外伤_添加_Click);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.radio手术);
            this.flowLayoutPanel4.Controls.Add(this.labelControl2);
            this.flowLayoutPanel4.Controls.Add(this.txt手术名称);
            this.flowLayoutPanel4.Controls.Add(this.labelControl3);
            this.flowLayoutPanel4.Controls.Add(this.dte手术时间);
            this.flowLayoutPanel4.Controls.Add(this.btn手术_添加);
            this.flowLayoutPanel4.Location = new System.Drawing.Point(114, 645);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(614, 24);
            this.flowLayoutPanel4.TabIndex = 71;
            // 
            // radio手术
            // 
            this.radio手术.Location = new System.Drawing.Point(0, 0);
            this.radio手术.Margin = new System.Windows.Forms.Padding(0);
            this.radio手术.Name = "radio手术";
            this.radio手术.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有")});
            this.radio手术.Size = new System.Drawing.Size(86, 24);
            this.radio手术.TabIndex = 72;
            this.radio手术.SelectedIndexChanged += new System.EventHandler(this.radio手术_SelectedIndexChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(86, 0);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 14);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "手术名称";
            // 
            // txt手术名称
            // 
            this.txt手术名称.Enabled = false;
            this.txt手术名称.Location = new System.Drawing.Point(134, 0);
            this.txt手术名称.Margin = new System.Windows.Forms.Padding(0);
            this.txt手术名称.Name = "txt手术名称";
            this.txt手术名称.Size = new System.Drawing.Size(86, 20);
            this.txt手术名称.TabIndex = 73;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(220, 0);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(48, 14);
            this.labelControl3.TabIndex = 9;
            this.labelControl3.Text = "手术时间";
            // 
            // dte手术时间
            // 
            this.dte手术时间.EditValue = null;
            this.dte手术时间.Enabled = false;
            this.dte手术时间.Location = new System.Drawing.Point(268, 0);
            this.dte手术时间.Margin = new System.Windows.Forms.Padding(0);
            this.dte手术时间.Name = "dte手术时间";
            this.dte手术时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte手术时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte手术时间.Properties.Mask.EditMask = "";
            this.dte手术时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.dte手术时间.Size = new System.Drawing.Size(86, 20);
            this.dte手术时间.TabIndex = 74;
            // 
            // btn手术_添加
            // 
            this.btn手术_添加.Enabled = false;
            this.btn手术_添加.Location = new System.Drawing.Point(354, 0);
            this.btn手术_添加.Margin = new System.Windows.Forms.Padding(0);
            this.btn手术_添加.Name = "btn手术_添加";
            this.btn手术_添加.Size = new System.Drawing.Size(52, 20);
            this.btn手术_添加.TabIndex = 75;
            this.btn手术_添加.Text = "添加";
            this.btn手术_添加.Click += new System.EventHandler(this.btn手术_添加_Click);
            // 
            // flow疾病
            // 
            this.flow疾病.Controls.Add(this.radio疾病);
            this.flow疾病.Controls.Add(this.chk疾病_高血压);
            this.flow疾病.Controls.Add(this.chk疾病_糖尿病);
            this.flow疾病.Controls.Add(this.chk疾病_冠心病);
            this.flow疾病.Controls.Add(this.chk疾病_慢性阻塞性肺病);
            this.flow疾病.Controls.Add(this.chk疾病_恶性肿瘤);
            this.flow疾病.Controls.Add(this.txt疾病_恶性肿瘤);
            this.flow疾病.Controls.Add(this.chk疾病_脑卒中);
            this.flow疾病.Controls.Add(this.chk疾病_重性精神疾病);
            this.flow疾病.Controls.Add(this.chk疾病_结核病);
            this.flow疾病.Controls.Add(this.chk疾病_肝炎);
            this.flow疾病.Controls.Add(this.chk疾病_其他法定传染病);
            this.flow疾病.Controls.Add(this.chk疾病_职业病);
            this.flow疾病.Controls.Add(this.txt疾病_职业病);
            this.flow疾病.Controls.Add(this.chk疾病_其他);
            this.flow疾病.Controls.Add(this.txt疾病_其他);
            this.flow疾病.Controls.Add(this.labelControl1);
            this.flow疾病.Controls.Add(this.dte疾病_确诊时间);
            this.flow疾病.Controls.Add(this.btn疾病_添加);
            this.flow疾病.Location = new System.Drawing.Point(114, 568);
            this.flow疾病.Name = "flow疾病";
            this.flow疾病.Size = new System.Drawing.Size(614, 71);
            this.flow疾病.TabIndex = 52;
            // 
            // radio疾病
            // 
            this.radio疾病.Location = new System.Drawing.Point(0, 0);
            this.radio疾病.Margin = new System.Windows.Forms.Padding(0);
            this.radio疾病.Name = "radio疾病";
            this.radio疾病.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem("2", "无"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem("1", "有")});
            this.radio疾病.Size = new System.Drawing.Size(86, 19);
            this.radio疾病.TabIndex = 53;
            this.radio疾病.SelectedIndexChanged += new System.EventHandler(this.radio疾病_SelectedIndexChanged);
            // 
            // chk疾病_高血压
            // 
            this.chk疾病_高血压.Enabled = false;
            this.chk疾病_高血压.Location = new System.Drawing.Point(86, 0);
            this.chk疾病_高血压.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_高血压.Name = "chk疾病_高血压";
            this.chk疾病_高血压.Properties.Caption = "高血压";
            this.chk疾病_高血压.Size = new System.Drawing.Size(60, 19);
            this.chk疾病_高血压.TabIndex = 54;
            this.chk疾病_高血压.Tag = "2";
            // 
            // chk疾病_糖尿病
            // 
            this.chk疾病_糖尿病.Enabled = false;
            this.chk疾病_糖尿病.Location = new System.Drawing.Point(146, 0);
            this.chk疾病_糖尿病.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_糖尿病.Name = "chk疾病_糖尿病";
            this.chk疾病_糖尿病.Properties.Caption = "糖尿病";
            this.chk疾病_糖尿病.Size = new System.Drawing.Size(57, 19);
            this.chk疾病_糖尿病.TabIndex = 55;
            this.chk疾病_糖尿病.Tag = "3";
            // 
            // chk疾病_冠心病
            // 
            this.chk疾病_冠心病.Enabled = false;
            this.chk疾病_冠心病.Location = new System.Drawing.Point(203, 0);
            this.chk疾病_冠心病.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_冠心病.Name = "chk疾病_冠心病";
            this.chk疾病_冠心病.Properties.Caption = "冠心病";
            this.chk疾病_冠心病.Size = new System.Drawing.Size(65, 19);
            this.chk疾病_冠心病.TabIndex = 56;
            this.chk疾病_冠心病.Tag = "4";
            // 
            // chk疾病_慢性阻塞性肺病
            // 
            this.chk疾病_慢性阻塞性肺病.Enabled = false;
            this.chk疾病_慢性阻塞性肺病.Location = new System.Drawing.Point(268, 0);
            this.chk疾病_慢性阻塞性肺病.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_慢性阻塞性肺病.Name = "chk疾病_慢性阻塞性肺病";
            this.chk疾病_慢性阻塞性肺病.Properties.Caption = "慢性阻塞性肺病";
            this.chk疾病_慢性阻塞性肺病.Size = new System.Drawing.Size(109, 19);
            this.chk疾病_慢性阻塞性肺病.TabIndex = 57;
            this.chk疾病_慢性阻塞性肺病.Tag = "5";
            // 
            // chk疾病_恶性肿瘤
            // 
            this.chk疾病_恶性肿瘤.Enabled = false;
            this.chk疾病_恶性肿瘤.Location = new System.Drawing.Point(377, 0);
            this.chk疾病_恶性肿瘤.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_恶性肿瘤.Name = "chk疾病_恶性肿瘤";
            this.chk疾病_恶性肿瘤.Properties.Caption = "恶性肿瘤";
            this.chk疾病_恶性肿瘤.Size = new System.Drawing.Size(75, 19);
            this.chk疾病_恶性肿瘤.TabIndex = 58;
            this.chk疾病_恶性肿瘤.Tag = "6";
            this.chk疾病_恶性肿瘤.CheckedChanged += new System.EventHandler(this.chk疾病_恶性肿瘤_CheckedChanged);
            // 
            // txt疾病_恶性肿瘤
            // 
            this.txt疾病_恶性肿瘤.Enabled = false;
            this.txt疾病_恶性肿瘤.Location = new System.Drawing.Point(452, 0);
            this.txt疾病_恶性肿瘤.Margin = new System.Windows.Forms.Padding(0);
            this.txt疾病_恶性肿瘤.Name = "txt疾病_恶性肿瘤";
            this.txt疾病_恶性肿瘤.Size = new System.Drawing.Size(86, 20);
            this.txt疾病_恶性肿瘤.TabIndex = 59;
            // 
            // chk疾病_脑卒中
            // 
            this.chk疾病_脑卒中.Enabled = false;
            this.chk疾病_脑卒中.Location = new System.Drawing.Point(538, 0);
            this.chk疾病_脑卒中.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_脑卒中.Name = "chk疾病_脑卒中";
            this.chk疾病_脑卒中.Properties.Caption = "脑卒中";
            this.chk疾病_脑卒中.Size = new System.Drawing.Size(64, 19);
            this.chk疾病_脑卒中.TabIndex = 60;
            this.chk疾病_脑卒中.Tag = "7";
            // 
            // chk疾病_重性精神疾病
            // 
            this.chk疾病_重性精神疾病.Enabled = false;
            this.chk疾病_重性精神疾病.Location = new System.Drawing.Point(0, 20);
            this.chk疾病_重性精神疾病.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_重性精神疾病.Name = "chk疾病_重性精神疾病";
            this.chk疾病_重性精神疾病.Properties.Caption = "严重精神障碍";
            this.chk疾病_重性精神疾病.Size = new System.Drawing.Size(99, 19);
            this.chk疾病_重性精神疾病.TabIndex = 61;
            this.chk疾病_重性精神疾病.Tag = "8";
            // 
            // chk疾病_结核病
            // 
            this.chk疾病_结核病.Enabled = false;
            this.chk疾病_结核病.Location = new System.Drawing.Point(99, 20);
            this.chk疾病_结核病.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_结核病.Name = "chk疾病_结核病";
            this.chk疾病_结核病.Properties.Caption = "结核病";
            this.chk疾病_结核病.Size = new System.Drawing.Size(73, 19);
            this.chk疾病_结核病.TabIndex = 62;
            this.chk疾病_结核病.Tag = "9";
            // 
            // chk疾病_肝炎
            // 
            this.chk疾病_肝炎.Enabled = false;
            this.chk疾病_肝炎.Location = new System.Drawing.Point(172, 20);
            this.chk疾病_肝炎.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_肝炎.Name = "chk疾病_肝炎";
            this.chk疾病_肝炎.Properties.Caption = "肝炎";
            this.chk疾病_肝炎.Size = new System.Drawing.Size(58, 19);
            this.chk疾病_肝炎.TabIndex = 63;
            this.chk疾病_肝炎.Tag = "10";
            // 
            // chk疾病_其他法定传染病
            // 
            this.chk疾病_其他法定传染病.Enabled = false;
            this.chk疾病_其他法定传染病.Location = new System.Drawing.Point(230, 20);
            this.chk疾病_其他法定传染病.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_其他法定传染病.Name = "chk疾病_其他法定传染病";
            this.chk疾病_其他法定传染病.Properties.Caption = "其他法定传染病";
            this.chk疾病_其他法定传染病.Size = new System.Drawing.Size(113, 19);
            this.chk疾病_其他法定传染病.TabIndex = 64;
            this.chk疾病_其他法定传染病.Tag = "11";
            // 
            // chk疾病_职业病
            // 
            this.chk疾病_职业病.Enabled = false;
            this.chk疾病_职业病.Location = new System.Drawing.Point(343, 20);
            this.chk疾病_职业病.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_职业病.Name = "chk疾病_职业病";
            this.chk疾病_职业病.Properties.Caption = "职业病";
            this.chk疾病_职业病.Size = new System.Drawing.Size(74, 19);
            this.chk疾病_职业病.TabIndex = 65;
            this.chk疾病_职业病.Tag = "12";
            this.chk疾病_职业病.CheckedChanged += new System.EventHandler(this.chk疾病_职业病_CheckedChanged);
            // 
            // txt疾病_职业病
            // 
            this.txt疾病_职业病.Enabled = false;
            this.txt疾病_职业病.Location = new System.Drawing.Point(417, 20);
            this.txt疾病_职业病.Margin = new System.Windows.Forms.Padding(0);
            this.txt疾病_职业病.Name = "txt疾病_职业病";
            this.txt疾病_职业病.Size = new System.Drawing.Size(86, 20);
            this.txt疾病_职业病.TabIndex = 66;
            // 
            // chk疾病_其他
            // 
            this.chk疾病_其他.Enabled = false;
            this.chk疾病_其他.Location = new System.Drawing.Point(503, 20);
            this.chk疾病_其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk疾病_其他.Name = "chk疾病_其他";
            this.chk疾病_其他.Properties.Caption = "其他";
            this.chk疾病_其他.Size = new System.Drawing.Size(64, 19);
            this.chk疾病_其他.TabIndex = 67;
            this.chk疾病_其他.Tag = "13";
            this.chk疾病_其他.CheckedChanged += new System.EventHandler(this.chk疾病_其他_CheckedChanged);
            // 
            // txt疾病_其他
            // 
            this.txt疾病_其他.Enabled = false;
            this.txt疾病_其他.Location = new System.Drawing.Point(0, 40);
            this.txt疾病_其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt疾病_其他.Name = "txt疾病_其他";
            this.txt疾病_其他.Size = new System.Drawing.Size(86, 20);
            this.txt疾病_其他.TabIndex = 68;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(86, 43);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(60, 14);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "确诊时间：";
            // 
            // dte疾病_确诊时间
            // 
            this.dte疾病_确诊时间.EditValue = null;
            this.dte疾病_确诊时间.Enabled = false;
            this.dte疾病_确诊时间.Location = new System.Drawing.Point(146, 40);
            this.dte疾病_确诊时间.Margin = new System.Windows.Forms.Padding(0);
            this.dte疾病_确诊时间.Name = "dte疾病_确诊时间";
            this.dte疾病_确诊时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte疾病_确诊时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte疾病_确诊时间.Properties.Mask.EditMask = "yyyy-MM";
            this.dte疾病_确诊时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte疾病_确诊时间.Size = new System.Drawing.Size(86, 20);
            this.dte疾病_确诊时间.TabIndex = 69;
            // 
            // btn疾病_添加
            // 
            this.btn疾病_添加.Enabled = false;
            this.btn疾病_添加.Location = new System.Drawing.Point(235, 40);
            this.btn疾病_添加.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.btn疾病_添加.Name = "btn疾病_添加";
            this.btn疾病_添加.Size = new System.Drawing.Size(66, 22);
            this.btn疾病_添加.TabIndex = 70;
            this.btn疾病_添加.Text = "添加";
            this.btn疾病_添加.Click += new System.EventHandler(this.btn疾病_添加_Click);
            // 
            // flow医疗费用支付方式
            // 
            this.flow医疗费用支付方式.AutoScroll = true;
            this.flow医疗费用支付方式.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.flow医疗费用支付方式.Controls.Add(this.chk医疗费用支付方式_城镇职工基本医疗保险);
            this.flow医疗费用支付方式.Controls.Add(this.textEdit职工医疗保险卡号);
            this.flow医疗费用支付方式.Controls.Add(this.chk医疗费用支付方式_城镇居民基本医疗保险);
            this.flow医疗费用支付方式.Controls.Add(this.textEdit居民医疗保险卡号);
            this.flow医疗费用支付方式.Controls.Add(this.chk医疗费用支付方式_贫困救助);
            this.flow医疗费用支付方式.Controls.Add(this.textEdit贫困救助卡号);
            this.flow医疗费用支付方式.Controls.Add(this.chk医疗费用支付方式_商业医疗保险);
            this.flow医疗费用支付方式.Controls.Add(this.chk医疗费用支付方式_全公费);
            this.flow医疗费用支付方式.Controls.Add(this.chk医疗费用支付方式_全自费);
            this.flow医疗费用支付方式.Controls.Add(this.chk医疗费用支付方式_其他);
            this.flow医疗费用支付方式.Controls.Add(this.txt医疗费用支付方式_其他);
            this.flow医疗费用支付方式.Controls.Add(this.chk医疗费用支付方式_新型农村合作医疗);
            this.flow医疗费用支付方式.Controls.Add(this.chk医疗费用支付方式_社会医疗保险);
            this.flow医疗费用支付方式.Location = new System.Drawing.Point(112, 303);
            this.flow医疗费用支付方式.Margin = new System.Windows.Forms.Padding(0);
            this.flow医疗费用支付方式.Name = "flow医疗费用支付方式";
            this.flow医疗费用支付方式.Size = new System.Drawing.Size(616, 126);
            this.flow医疗费用支付方式.TabIndex = 23;
            this.flow医疗费用支付方式.TabStop = true;
            // 
            // chk医疗费用支付方式_城镇职工基本医疗保险
            // 
            this.chk医疗费用支付方式_城镇职工基本医疗保险.Location = new System.Drawing.Point(5, 6);
            this.chk医疗费用支付方式_城镇职工基本医疗保险.Margin = new System.Windows.Forms.Padding(5, 6, 0, 0);
            this.chk医疗费用支付方式_城镇职工基本医疗保险.Name = "chk医疗费用支付方式_城镇职工基本医疗保险";
            this.chk医疗费用支付方式_城镇职工基本医疗保险.Properties.Caption = "1 城镇或省直职工基本医疗保险    医保卡号:";
            this.chk医疗费用支付方式_城镇职工基本医疗保险.Size = new System.Drawing.Size(270, 19);
            this.chk医疗费用支付方式_城镇职工基本医疗保险.TabIndex = 26;
            this.chk医疗费用支付方式_城镇职工基本医疗保险.Tag = "3";
            this.chk医疗费用支付方式_城镇职工基本医疗保险.CheckedChanged += new System.EventHandler(this.chk医疗费用支付方式_城镇职工基本医疗保险_CheckedChanged);
            // 
            // textEdit职工医疗保险卡号
            // 
            this.textEdit职工医疗保险卡号.Enabled = false;
            this.flow医疗费用支付方式.SetFlowBreak(this.textEdit职工医疗保险卡号, true);
            this.textEdit职工医疗保险卡号.Location = new System.Drawing.Point(275, 7);
            this.textEdit职工医疗保险卡号.Margin = new System.Windows.Forms.Padding(0, 7, 5, 3);
            this.textEdit职工医疗保险卡号.Name = "textEdit职工医疗保险卡号";
            this.textEdit职工医疗保险卡号.Size = new System.Drawing.Size(196, 20);
            this.textEdit职工医疗保险卡号.TabIndex = 34;
            // 
            // chk医疗费用支付方式_城镇居民基本医疗保险
            // 
            this.chk医疗费用支付方式_城镇居民基本医疗保险.Location = new System.Drawing.Point(5, 36);
            this.chk医疗费用支付方式_城镇居民基本医疗保险.Margin = new System.Windows.Forms.Padding(5, 6, 0, 0);
            this.chk医疗费用支付方式_城镇居民基本医疗保险.Name = "chk医疗费用支付方式_城镇居民基本医疗保险";
            this.chk医疗费用支付方式_城镇居民基本医疗保险.Properties.Caption = "2 居民基本医疗保险                   医保卡号:";
            this.chk医疗费用支付方式_城镇居民基本医疗保险.Size = new System.Drawing.Size(270, 19);
            this.chk医疗费用支付方式_城镇居民基本医疗保险.TabIndex = 27;
            this.chk医疗费用支付方式_城镇居民基本医疗保险.Tag = "4";
            this.chk医疗费用支付方式_城镇居民基本医疗保险.CheckedChanged += new System.EventHandler(this.chk医疗费用支付方式_城镇居民基本医疗保险_CheckedChanged);
            // 
            // textEdit居民医疗保险卡号
            // 
            this.textEdit居民医疗保险卡号.Enabled = false;
            this.flow医疗费用支付方式.SetFlowBreak(this.textEdit居民医疗保险卡号, true);
            this.textEdit居民医疗保险卡号.Location = new System.Drawing.Point(275, 37);
            this.textEdit居民医疗保险卡号.Margin = new System.Windows.Forms.Padding(0, 7, 0, 3);
            this.textEdit居民医疗保险卡号.Name = "textEdit居民医疗保险卡号";
            this.textEdit居民医疗保险卡号.Size = new System.Drawing.Size(196, 20);
            this.textEdit居民医疗保险卡号.TabIndex = 35;
            // 
            // chk医疗费用支付方式_贫困救助
            // 
            this.chk医疗费用支付方式_贫困救助.Location = new System.Drawing.Point(5, 66);
            this.chk医疗费用支付方式_贫困救助.Margin = new System.Windows.Forms.Padding(5, 6, 0, 0);
            this.chk医疗费用支付方式_贫困救助.Name = "chk医疗费用支付方式_贫困救助";
            this.chk医疗费用支付方式_贫困救助.Properties.Caption = "3 贫困救助                                卡号:";
            this.chk医疗费用支付方式_贫困救助.Size = new System.Drawing.Size(270, 19);
            this.chk医疗费用支付方式_贫困救助.TabIndex = 31;
            this.chk医疗费用支付方式_贫困救助.Tag = "8";
            this.chk医疗费用支付方式_贫困救助.CheckedChanged += new System.EventHandler(this.chk医疗费用支付方式_贫困救助_CheckedChanged);
            // 
            // textEdit贫困救助卡号
            // 
            this.textEdit贫困救助卡号.Enabled = false;
            this.flow医疗费用支付方式.SetFlowBreak(this.textEdit贫困救助卡号, true);
            this.textEdit贫困救助卡号.Location = new System.Drawing.Point(275, 67);
            this.textEdit贫困救助卡号.Margin = new System.Windows.Forms.Padding(0, 7, 3, 3);
            this.textEdit贫困救助卡号.Name = "textEdit贫困救助卡号";
            this.textEdit贫困救助卡号.Size = new System.Drawing.Size(196, 20);
            this.textEdit贫困救助卡号.TabIndex = 36;
            // 
            // chk医疗费用支付方式_商业医疗保险
            // 
            this.chk医疗费用支付方式_商业医疗保险.Location = new System.Drawing.Point(5, 96);
            this.chk医疗费用支付方式_商业医疗保险.Margin = new System.Windows.Forms.Padding(5, 6, 0, 0);
            this.chk医疗费用支付方式_商业医疗保险.Name = "chk医疗费用支付方式_商业医疗保险";
            this.chk医疗费用支付方式_商业医疗保险.Properties.Caption = "4 商业医疗保险";
            this.chk医疗费用支付方式_商业医疗保险.Size = new System.Drawing.Size(120, 19);
            this.chk医疗费用支付方式_商业医疗保险.TabIndex = 30;
            this.chk医疗费用支付方式_商业医疗保险.Tag = "7";
            // 
            // chk医疗费用支付方式_全公费
            // 
            this.chk医疗费用支付方式_全公费.Location = new System.Drawing.Point(125, 96);
            this.chk医疗费用支付方式_全公费.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.chk医疗费用支付方式_全公费.Name = "chk医疗费用支付方式_全公费";
            this.chk医疗费用支付方式_全公费.Properties.Caption = "5 全公费";
            this.chk医疗费用支付方式_全公费.Size = new System.Drawing.Size(80, 19);
            this.chk医疗费用支付方式_全公费.TabIndex = 25;
            this.chk医疗费用支付方式_全公费.Tag = "2";
            // 
            // chk医疗费用支付方式_全自费
            // 
            this.chk医疗费用支付方式_全自费.Location = new System.Drawing.Point(205, 96);
            this.chk医疗费用支付方式_全自费.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.chk医疗费用支付方式_全自费.Name = "chk医疗费用支付方式_全自费";
            this.chk医疗费用支付方式_全自费.Properties.Caption = "6 全自费";
            this.chk医疗费用支付方式_全自费.Size = new System.Drawing.Size(80, 19);
            this.chk医疗费用支付方式_全自费.TabIndex = 24;
            this.chk医疗费用支付方式_全自费.Tag = "1";
            // 
            // chk医疗费用支付方式_其他
            // 
            this.chk医疗费用支付方式_其他.Location = new System.Drawing.Point(290, 96);
            this.chk医疗费用支付方式_其他.Margin = new System.Windows.Forms.Padding(5, 6, 0, 0);
            this.chk医疗费用支付方式_其他.Name = "chk医疗费用支付方式_其他";
            this.chk医疗费用支付方式_其他.Properties.Caption = "7 其他";
            this.chk医疗费用支付方式_其他.Size = new System.Drawing.Size(64, 19);
            this.chk医疗费用支付方式_其他.TabIndex = 32;
            this.chk医疗费用支付方式_其他.Tag = "99";
            this.chk医疗费用支付方式_其他.CheckedChanged += new System.EventHandler(this.chk医疗费用支付方式_其他_CheckedChanged);
            // 
            // txt医疗费用支付方式_其他
            // 
            this.txt医疗费用支付方式_其他.Enabled = false;
            this.flow医疗费用支付方式.SetFlowBreak(this.txt医疗费用支付方式_其他, true);
            this.txt医疗费用支付方式_其他.Location = new System.Drawing.Point(354, 97);
            this.txt医疗费用支付方式_其他.Margin = new System.Windows.Forms.Padding(0, 7, 3, 0);
            this.txt医疗费用支付方式_其他.Name = "txt医疗费用支付方式_其他";
            this.txt医疗费用支付方式_其他.Size = new System.Drawing.Size(117, 20);
            this.txt医疗费用支付方式_其他.TabIndex = 33;
            // 
            // chk医疗费用支付方式_新型农村合作医疗
            // 
            this.chk医疗费用支付方式_新型农村合作医疗.Location = new System.Drawing.Point(5, 123);
            this.chk医疗费用支付方式_新型农村合作医疗.Margin = new System.Windows.Forms.Padding(5, 6, 0, 0);
            this.chk医疗费用支付方式_新型农村合作医疗.Name = "chk医疗费用支付方式_新型农村合作医疗";
            this.chk医疗费用支付方式_新型农村合作医疗.Properties.Caption = "新型农村合作医疗";
            this.chk医疗费用支付方式_新型农村合作医疗.Size = new System.Drawing.Size(125, 19);
            this.chk医疗费用支付方式_新型农村合作医疗.TabIndex = 28;
            this.chk医疗费用支付方式_新型农村合作医疗.Tag = "5";
            this.chk医疗费用支付方式_新型农村合作医疗.Visible = false;
            // 
            // chk医疗费用支付方式_社会医疗保险
            // 
            this.chk医疗费用支付方式_社会医疗保险.Location = new System.Drawing.Point(130, 123);
            this.chk医疗费用支付方式_社会医疗保险.Margin = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.chk医疗费用支付方式_社会医疗保险.Name = "chk医疗费用支付方式_社会医疗保险";
            this.chk医疗费用支付方式_社会医疗保险.Properties.Caption = "社会医疗保险";
            this.chk医疗费用支付方式_社会医疗保险.Size = new System.Drawing.Size(101, 19);
            this.chk医疗费用支付方式_社会医疗保险.TabIndex = 29;
            this.chk医疗费用支付方式_社会医疗保险.Tag = "6";
            this.chk医疗费用支付方式_社会医疗保险.Visible = false;
            // 
            // flow过敏史
            // 
            this.flow过敏史.Controls.Add(this.cbo过敏史);
            this.flow过敏史.Controls.Add(this.chk过敏史_青霉素);
            this.flow过敏史.Controls.Add(this.chk过敏史_磺胺);
            this.flow过敏史.Controls.Add(this.chk过敏史_不详);
            this.flow过敏史.Controls.Add(this.chk过敏史_链霉素);
            this.flow过敏史.Controls.Add(this.chk过敏史_其他);
            this.flow过敏史.Controls.Add(this.txt过敏史_其他);
            this.flow过敏史.Location = new System.Drawing.Point(112, 542);
            this.flow过敏史.Margin = new System.Windows.Forms.Padding(0);
            this.flow过敏史.Name = "flow过敏史";
            this.flow过敏史.Size = new System.Drawing.Size(616, 20);
            this.flow过敏史.TabIndex = 44;
            // 
            // cbo过敏史
            // 
            this.cbo过敏史.EditValue = "无";
            this.cbo过敏史.Location = new System.Drawing.Point(0, 0);
            this.cbo过敏史.Margin = new System.Windows.Forms.Padding(0);
            this.cbo过敏史.Name = "cbo过敏史";
            this.cbo过敏史.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo过敏史.Properties.Items.AddRange(new object[] {
            "有",
            "无"});
            this.cbo过敏史.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo过敏史.Size = new System.Drawing.Size(57, 20);
            this.cbo过敏史.TabIndex = 45;
            this.cbo过敏史.SelectedIndexChanged += new System.EventHandler(this.cbo过敏史_SelectedIndexChanged);
            // 
            // chk过敏史_青霉素
            // 
            this.chk过敏史_青霉素.Enabled = false;
            this.chk过敏史_青霉素.Location = new System.Drawing.Point(57, 0);
            this.chk过敏史_青霉素.Margin = new System.Windows.Forms.Padding(0);
            this.chk过敏史_青霉素.Name = "chk过敏史_青霉素";
            this.chk过敏史_青霉素.Properties.Caption = "青霉素";
            this.chk过敏史_青霉素.Size = new System.Drawing.Size(60, 19);
            this.chk过敏史_青霉素.TabIndex = 46;
            this.chk过敏史_青霉素.Tag = "2";
            // 
            // chk过敏史_磺胺
            // 
            this.chk过敏史_磺胺.Enabled = false;
            this.chk过敏史_磺胺.Location = new System.Drawing.Point(117, 0);
            this.chk过敏史_磺胺.Margin = new System.Windows.Forms.Padding(0);
            this.chk过敏史_磺胺.Name = "chk过敏史_磺胺";
            this.chk过敏史_磺胺.Properties.Caption = "磺胺";
            this.chk过敏史_磺胺.Size = new System.Drawing.Size(60, 19);
            this.chk过敏史_磺胺.TabIndex = 47;
            this.chk过敏史_磺胺.Tag = "3";
            // 
            // chk过敏史_不详
            // 
            this.chk过敏史_不详.Enabled = false;
            this.chk过敏史_不详.Location = new System.Drawing.Point(177, 0);
            this.chk过敏史_不详.Margin = new System.Windows.Forms.Padding(0);
            this.chk过敏史_不详.Name = "chk过敏史_不详";
            this.chk过敏史_不详.Properties.Caption = "不详";
            this.chk过敏史_不详.Size = new System.Drawing.Size(60, 19);
            this.chk过敏史_不详.TabIndex = 48;
            this.chk过敏史_不详.Tag = "";
            this.chk过敏史_不详.Visible = false;
            // 
            // chk过敏史_链霉素
            // 
            this.chk过敏史_链霉素.Enabled = false;
            this.chk过敏史_链霉素.Location = new System.Drawing.Point(237, 0);
            this.chk过敏史_链霉素.Margin = new System.Windows.Forms.Padding(0);
            this.chk过敏史_链霉素.Name = "chk过敏史_链霉素";
            this.chk过敏史_链霉素.Properties.Caption = "链霉素";
            this.chk过敏史_链霉素.Size = new System.Drawing.Size(60, 19);
            this.chk过敏史_链霉素.TabIndex = 49;
            this.chk过敏史_链霉素.Tag = "4";
            // 
            // chk过敏史_其他
            // 
            this.chk过敏史_其他.Enabled = false;
            this.chk过敏史_其他.Location = new System.Drawing.Point(297, 0);
            this.chk过敏史_其他.Margin = new System.Windows.Forms.Padding(0);
            this.chk过敏史_其他.Name = "chk过敏史_其他";
            this.chk过敏史_其他.Properties.Caption = "其他";
            this.chk过敏史_其他.Size = new System.Drawing.Size(60, 19);
            this.chk过敏史_其他.TabIndex = 50;
            this.chk过敏史_其他.Tag = "7";
            this.chk过敏史_其他.CheckedChanged += new System.EventHandler(this.chk过敏史_其他_CheckedChanged);
            // 
            // txt过敏史_其他
            // 
            this.txt过敏史_其他.Enabled = false;
            this.txt过敏史_其他.Location = new System.Drawing.Point(357, 0);
            this.txt过敏史_其他.Margin = new System.Windows.Forms.Padding(0);
            this.txt过敏史_其他.Name = "txt过敏史_其他";
            this.txt过敏史_其他.Size = new System.Drawing.Size(98, 20);
            this.txt过敏史_其他.TabIndex = 51;
            // 
            // txt当前所属机构
            // 
            this.txt当前所属机构.Location = new System.Drawing.Point(576, 1460);
            this.txt当前所属机构.Name = "txt当前所属机构";
            this.txt当前所属机构.Properties.ReadOnly = true;
            this.txt当前所属机构.Size = new System.Drawing.Size(152, 20);
            this.txt当前所属机构.StyleController = this.layoutControl1;
            this.txt当前所属机构.TabIndex = 160;
            // 
            // txt最近更新人
            // 
            this.txt最近更新人.Location = new System.Drawing.Point(335, 1460);
            this.txt最近更新人.Name = "txt最近更新人";
            this.txt最近更新人.Properties.ReadOnly = true;
            this.txt最近更新人.Size = new System.Drawing.Size(130, 20);
            this.txt最近更新人.StyleController = this.layoutControl1;
            this.txt最近更新人.TabIndex = 159;
            // 
            // txt录入人
            // 
            this.txt录入人.Location = new System.Drawing.Point(114, 1460);
            this.txt录入人.Name = "txt录入人";
            this.txt录入人.Properties.ReadOnly = true;
            this.txt录入人.Size = new System.Drawing.Size(110, 20);
            this.txt录入人.StyleController = this.layoutControl1;
            this.txt录入人.TabIndex = 158;
            // 
            // txt遗传病史_疾病名称
            // 
            this.txt遗传病史_疾病名称.Location = new System.Drawing.Point(114, 1025);
            this.txt遗传病史_疾病名称.Name = "txt遗传病史_疾病名称";
            this.txt遗传病史_疾病名称.Size = new System.Drawing.Size(73, 20);
            this.txt遗传病史_疾病名称.StyleController = this.layoutControl1;
            this.txt遗传病史_疾病名称.TabIndex = 107;
            // 
            // txt暴露史_射线
            // 
            this.txt暴露史_射线.Enabled = false;
            this.txt暴露史_射线.Location = new System.Drawing.Point(452, 975);
            this.txt暴露史_射线.Name = "txt暴露史_射线";
            this.txt暴露史_射线.Size = new System.Drawing.Size(276, 20);
            this.txt暴露史_射线.StyleController = this.layoutControl1;
            this.txt暴露史_射线.TabIndex = 105;
            // 
            // txt暴露史_毒物
            // 
            this.txt暴露史_毒物.Enabled = false;
            this.txt暴露史_毒物.Location = new System.Drawing.Point(283, 975);
            this.txt暴露史_毒物.Name = "txt暴露史_毒物";
            this.txt暴露史_毒物.Size = new System.Drawing.Size(58, 20);
            this.txt暴露史_毒物.StyleController = this.layoutControl1;
            this.txt暴露史_毒物.TabIndex = 104;
            // 
            // txt暴露史_化学品
            // 
            this.txt暴露史_化学品.Enabled = false;
            this.txt暴露史_化学品.Location = new System.Drawing.Point(114, 975);
            this.txt暴露史_化学品.Name = "txt暴露史_化学品";
            this.txt暴露史_化学品.Size = new System.Drawing.Size(58, 20);
            this.txt暴露史_化学品.StyleController = this.layoutControl1;
            this.txt暴露史_化学品.TabIndex = 103;
            // 
            // txt新农合号
            // 
            this.txt新农合号.Location = new System.Drawing.Point(476, 433);
            this.txt新农合号.Name = "txt新农合号";
            this.txt新农合号.Size = new System.Drawing.Size(252, 20);
            this.txt新农合号.StyleController = this.layoutControl1;
            this.txt新农合号.TabIndex = 35;
            // 
            // txt医疗保险号
            // 
            this.txt医疗保险号.Location = new System.Drawing.Point(112, 433);
            this.txt医疗保险号.Name = "txt医疗保险号";
            this.txt医疗保险号.Size = new System.Drawing.Size(253, 20);
            this.txt医疗保险号.StyleController = this.layoutControl1;
            this.txt医疗保险号.TabIndex = 34;
            // 
            // txt联系人电话
            // 
            this.txt联系人电话.Location = new System.Drawing.Point(112, 159);
            this.txt联系人电话.Name = "txt联系人电话";
            this.txt联系人电话.Size = new System.Drawing.Size(241, 20);
            this.txt联系人电话.StyleController = this.layoutControl1;
            this.txt联系人电话.TabIndex = 13;
            // 
            // txt联系人姓名
            // 
            this.txt联系人姓名.Location = new System.Drawing.Point(462, 159);
            this.txt联系人姓名.Name = "txt联系人姓名";
            this.txt联系人姓名.Size = new System.Drawing.Size(266, 20);
            this.txt联系人姓名.StyleController = this.layoutControl1;
            this.txt联系人姓名.TabIndex = 14;
            // 
            // txt本人电话
            // 
            this.txt本人电话.Location = new System.Drawing.Point(112, 135);
            this.txt本人电话.Name = "txt本人电话";
            this.txt本人电话.Size = new System.Drawing.Size(241, 20);
            this.txt本人电话.StyleController = this.layoutControl1;
            this.txt本人电话.TabIndex = 11;
            // 
            // txt工作单位
            // 
            this.txt工作单位.Location = new System.Drawing.Point(462, 135);
            this.txt工作单位.Name = "txt工作单位";
            this.txt工作单位.Size = new System.Drawing.Size(266, 20);
            this.txt工作单位.StyleController = this.layoutControl1;
            this.txt工作单位.TabIndex = 12;
            // 
            // dte出生日期
            // 
            this.dte出生日期.EditValue = null;
            this.dte出生日期.Location = new System.Drawing.Point(462, 79);
            this.dte出生日期.Name = "dte出生日期";
            this.dte出生日期.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生日期.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte出生日期.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte出生日期.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte出生日期.Size = new System.Drawing.Size(138, 20);
            this.dte出生日期.StyleController = this.layoutControl1;
            this.dte出生日期.TabIndex = 9;
            this.dte出生日期.EditValueChanged += new System.EventHandler(this.dte出生日期_EditValueChanged);
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(112, 55);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Size = new System.Drawing.Size(241, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 4;
            // 
            // cbo性别
            // 
            this.cbo性别.Location = new System.Drawing.Point(462, 55);
            this.cbo性别.Name = "cbo性别";
            this.cbo性别.Properties.AllowMouseWheel = false;
            this.cbo性别.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo性别.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo性别.Size = new System.Drawing.Size(141, 20);
            this.cbo性别.StyleController = this.layoutControl1;
            this.cbo性别.TabIndex = 5;
            this.cbo性别.SelectedIndexChanged += new System.EventHandler(this.cbo性别_SelectedIndexChanged);
            this.cbo性别.EditValueChanged += new System.EventHandler(this.cbo性别_EditValueChanged);
            // 
            // cbo常住类型
            // 
            this.cbo常住类型.EditValue = "";
            this.cbo常住类型.Location = new System.Drawing.Point(112, 183);
            this.cbo常住类型.Name = "cbo常住类型";
            this.cbo常住类型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo常住类型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo常住类型.Size = new System.Drawing.Size(241, 20);
            this.cbo常住类型.StyleController = this.layoutControl1;
            this.cbo常住类型.TabIndex = 15;
            // 
            // cbo民族
            // 
            this.cbo民族.EditValue = "";
            this.cbo民族.Location = new System.Drawing.Point(462, 183);
            this.cbo民族.Name = "cbo民族";
            this.cbo民族.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo民族.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo民族.Size = new System.Drawing.Size(91, 20);
            this.cbo民族.StyleController = this.layoutControl1;
            this.cbo民族.TabIndex = 16;
            // 
            // cbo血型
            // 
            this.cbo血型.Location = new System.Drawing.Point(112, 207);
            this.cbo血型.Name = "cbo血型";
            this.cbo血型.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo血型.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo血型.Size = new System.Drawing.Size(241, 20);
            this.cbo血型.StyleController = this.layoutControl1;
            this.cbo血型.TabIndex = 17;
            // 
            // cboRH阴性
            // 
            this.cboRH阴性.Location = new System.Drawing.Point(462, 207);
            this.cboRH阴性.Name = "cboRH阴性";
            this.cboRH阴性.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboRH阴性.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cboRH阴性.Size = new System.Drawing.Size(91, 20);
            this.cboRH阴性.StyleController = this.layoutControl1;
            this.cboRH阴性.TabIndex = 18;
            // 
            // cbo文化程度
            // 
            this.cbo文化程度.Location = new System.Drawing.Point(112, 255);
            this.cbo文化程度.Name = "cbo文化程度";
            this.cbo文化程度.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo文化程度.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo文化程度.Size = new System.Drawing.Size(241, 20);
            this.cbo文化程度.StyleController = this.layoutControl1;
            this.cbo文化程度.TabIndex = 20;
            // 
            // cbo职业
            // 
            this.cbo职业.Location = new System.Drawing.Point(112, 231);
            this.cbo职业.Name = "cbo职业";
            this.cbo职业.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo职业.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo职业.Size = new System.Drawing.Size(241, 20);
            this.cbo职业.StyleController = this.layoutControl1;
            this.cbo职业.TabIndex = 19;
            // 
            // cbo劳动程度
            // 
            this.cbo劳动程度.Location = new System.Drawing.Point(462, 255);
            this.cbo劳动程度.Name = "cbo劳动程度";
            this.cbo劳动程度.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo劳动程度.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo劳动程度.Size = new System.Drawing.Size(266, 20);
            this.cbo劳动程度.StyleController = this.layoutControl1;
            this.cbo劳动程度.TabIndex = 21;
            this.cbo劳动程度.Visible = false;
            // 
            // cbo婚姻状况
            // 
            this.cbo婚姻状况.Location = new System.Drawing.Point(112, 279);
            this.cbo婚姻状况.Name = "cbo婚姻状况";
            this.cbo婚姻状况.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo婚姻状况.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo婚姻状况.Size = new System.Drawing.Size(141, 20);
            this.cbo婚姻状况.StyleController = this.layoutControl1;
            this.cbo婚姻状况.TabIndex = 22;
            // 
            // cbo与户主关系
            // 
            this.cbo与户主关系.EditValue = "户主";
            this.cbo与户主关系.Location = new System.Drawing.Point(112, 31);
            this.cbo与户主关系.Name = "cbo与户主关系";
            this.cbo与户主关系.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo与户主关系.Properties.Items.AddRange(new object[] {
            "户主"});
            this.cbo与户主关系.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbo与户主关系.Size = new System.Drawing.Size(241, 20);
            this.cbo与户主关系.StyleController = this.layoutControl1;
            this.cbo与户主关系.TabIndex = 2;
            // 
            // dte调查时间
            // 
            this.dte调查时间.EditValue = null;
            this.dte调查时间.Location = new System.Drawing.Point(114, 1436);
            this.dte调查时间.Name = "dte调查时间";
            this.dte调查时间.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte调查时间.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dte调查时间.Properties.Mask.EditMask = "yyyy-MM-dd";
            this.dte调查时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte调查时间.Size = new System.Drawing.Size(110, 20);
            this.dte调查时间.StyleController = this.layoutControl1;
            this.dte调查时间.TabIndex = 155;
            // 
            // dte录入时间
            // 
            this.dte录入时间.Location = new System.Drawing.Point(335, 1436);
            this.dte录入时间.Name = "dte录入时间";
            this.dte录入时间.Properties.Mask.EditMask = "yyyy-MM-dd hh:MM:ss";
            this.dte录入时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.dte录入时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte录入时间.Properties.ReadOnly = true;
            this.dte录入时间.Size = new System.Drawing.Size(130, 20);
            this.dte录入时间.StyleController = this.layoutControl1;
            this.dte录入时间.TabIndex = 156;
            // 
            // dte最近更新时间
            // 
            this.dte最近更新时间.Location = new System.Drawing.Point(576, 1436);
            this.dte最近更新时间.Name = "dte最近更新时间";
            this.dte最近更新时间.Properties.Mask.EditMask = "yyyy-MM-dd hh:MM:ss";
            this.dte最近更新时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.dte最近更新时间.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dte最近更新时间.Properties.ReadOnly = true;
            this.dte最近更新时间.Size = new System.Drawing.Size(152, 20);
            this.dte最近更新时间.StyleController = this.layoutControl1;
            this.dte最近更新时间.TabIndex = 157;
            // 
            // txt所属片区
            // 
            this.txt所属片区.Location = new System.Drawing.Point(112, 517);
            this.txt所属片区.Name = "txt所属片区";
            this.txt所属片区.Size = new System.Drawing.Size(416, 20);
            this.txt所属片区.StyleController = this.layoutControl1;
            this.txt所属片区.TabIndex = 42;
            // 
            // textEdit本人或家属签字
            // 
            this.textEdit本人或家属签字.Location = new System.Drawing.Point(114, 1357);
            this.textEdit本人或家属签字.Name = "textEdit本人或家属签字";
            this.textEdit本人或家属签字.Properties.ReadOnly = true;
            this.textEdit本人或家属签字.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.textEdit本人或家属签字.Size = new System.Drawing.Size(161, 67);
            this.textEdit本人或家属签字.StyleController = this.layoutControl1;
            this.textEdit本人或家属签字.TabIndex = 172;
            this.textEdit本人或家属签字.TabStop = true;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem20.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem20.Control = this.cbo劳动程度;
            this.layoutControlItem20.CustomizationFormText = "劳动程度";
            this.layoutControlItem20.Location = new System.Drawing.Point(350, 224);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem20.Text = "劳动程度";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(735, 1487);
            this.layoutControlGroup1.Text = "个人基本信息表";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem22,
            this.layoutControlItem29,
            this.layoutControlItem32,
            this.layoutControlItem31,
            this.simpleLabelItem5,
            this.simpleLabelItem3,
            this.layoutControlItem33,
            this.layoutControlItem34,
            this.layoutControlItem35,
            this.layoutControlItem37,
            this.simpleLabelItem6,
            this.layoutControlItem57,
            this.layoutControlItem58,
            this.layoutControlItem59,
            this.layoutControlItem60,
            this.layoutControlItem61,
            this.layoutControlItem62,
            this.layoutControlItem15,
            this.layoutControlItem6,
            this.layoutControlItem4,
            this.layoutControlItem3,
            this.layoutControlItem5,
            this.layoutControlItem11,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem16,
            this.layoutControlItem14,
            this.layoutControlItem17,
            this.layoutControlItem19,
            this.layoutControlItem18,
            this.layoutControlItem21,
            this.layoutControlItem28,
            this.layoutControlItem23,
            this.layoutControlItem24,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem7,
            this.layoutControlItem9,
            this.layoutControlItem50,
            this.layoutControlItem2,
            this.layoutControlItem8,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.emptySpaceItem7,
            this.layoutControlItem30,
            this.layoutControlItem36,
            this.layoutControlItem38,
            this.layoutControlItem27,
            this.layoutControlItem41,
            this.layoutControlItem48,
            this.emptySpaceItem8,
            this.layout孕产情况,
            this.simpleSeparator1,
            this.simpleSeparator2,
            this.simpleSeparator3,
            this.simpleSeparator4,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.simpleSeparator5,
            this.simpleSeparator6,
            this.simpleSeparator7,
            this.simpleSeparator8,
            this.simpleSeparator9,
            this.simpleSeparator10,
            this.layoutControlItem39,
            this.layoutControlItem40,
            this.layoutControlItem42,
            this.layoutControlItem1,
            this.layoutControlItem51,
            this.layoutControlItem52,
            this.simpleLabelItem7,
            this.layoutControlItem53,
            this.layoutControlItem56,
            this.layoutControlItem64,
            this.layoutControlItem54,
            this.layoutControlItem63,
            this.lay居住情况,
            this.lbl本人或家属签字,
            this.lbl签字时间,
            this.layoutControlItem55,
            this.layoutControlItem65,
            this.layoutControlItem66,
            this.layoutControlItem67});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlGroup2.Size = new System.Drawing.Size(733, 1405);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem22.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem22.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem22.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem22.Control = this.flow疾病;
            this.layoutControlItem22.CustomizationFormText = "疾病";
            this.layoutControlItem22.FillControlToClientArea = false;
            this.layoutControlItem22.Location = new System.Drawing.Point(22, 537);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(143, 75);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(703, 75);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "疾病";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem29.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem29.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem29.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem29.Control = this.flowLayoutPanel4;
            this.layoutControlItem29.CustomizationFormText = "手术";
            this.layoutControlItem29.Location = new System.Drawing.Point(22, 614);
            this.layoutControlItem29.MinSize = new System.Drawing.Size(203, 28);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(703, 28);
            this.layoutControlItem29.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem29.Text = "手术";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem32.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem32.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem32.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem32.Control = this.flowLayoutPanel5;
            this.layoutControlItem32.CustomizationFormText = "外伤";
            this.layoutControlItem32.Location = new System.Drawing.Point(22, 644);
            this.layoutControlItem32.MinSize = new System.Drawing.Size(169, 28);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(703, 28);
            this.layoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem32.Text = "外伤";
            this.layoutControlItem32.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem32.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem32.TextToControlDistance = 5;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem31.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem31.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem31.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem31.Control = this.flowLayoutPanel6;
            this.layoutControlItem31.CustomizationFormText = "输血";
            this.layoutControlItem31.Location = new System.Drawing.Point(22, 674);
            this.layoutControlItem31.MinSize = new System.Drawing.Size(169, 28);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(703, 28);
            this.layoutControlItem31.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem31.Text = "输血";
            this.layoutControlItem31.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem31.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem31.TextToControlDistance = 5;
            // 
            // simpleLabelItem5
            // 
            this.simpleLabelItem5.AllowHotTrack = false;
            this.simpleLabelItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem5.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem5.CustomizationFormText = "家族史";
            this.simpleLabelItem5.Location = new System.Drawing.Point(0, 804);
            this.simpleLabelItem5.MinSize = new System.Drawing.Size(100, 18);
            this.simpleLabelItem5.Name = "simpleLabelItem5";
            this.simpleLabelItem5.Size = new System.Drawing.Size(100, 24);
            this.simpleLabelItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem5.Text = "家族史";
            this.simpleLabelItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem5.TextSize = new System.Drawing.Size(96, 14);
            // 
            // simpleLabelItem3
            // 
            this.simpleLabelItem3.AllowHotTrack = false;
            this.simpleLabelItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem3.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.simpleLabelItem3.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem3.CustomizationFormText = "暴露史";
            this.simpleLabelItem3.Location = new System.Drawing.Point(0, 920);
            this.simpleLabelItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.simpleLabelItem3.MinSize = new System.Drawing.Size(100, 24);
            this.simpleLabelItem3.Name = "simpleLabelItem3";
            this.simpleLabelItem3.Size = new System.Drawing.Size(100, 24);
            this.simpleLabelItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem3.Text = "暴露史";
            this.simpleLabelItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem3.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem33.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem33.Control = this.txt暴露史_化学品;
            this.layoutControlItem33.CustomizationFormText = "化学品";
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 944);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(169, 24);
            this.layoutControlItem33.Text = "化学品：";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem34.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem34.Control = this.txt暴露史_毒物;
            this.layoutControlItem34.CustomizationFormText = "毒物";
            this.layoutControlItem34.Location = new System.Drawing.Point(169, 944);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(169, 24);
            this.layoutControlItem34.Text = "毒物：";
            this.layoutControlItem34.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem35.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem35.Control = this.txt暴露史_射线;
            this.layoutControlItem35.CustomizationFormText = "射线";
            this.layoutControlItem35.Location = new System.Drawing.Point(338, 944);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(387, 24);
            this.layoutControlItem35.Text = "射线：";
            this.layoutControlItem35.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem37.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem37.Control = this.txt遗传病史_疾病名称;
            this.layoutControlItem37.CustomizationFormText = "遗传病史";
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 994);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(184, 24);
            this.layoutControlItem37.Text = "疾病名称：";
            this.layoutControlItem37.TextSize = new System.Drawing.Size(104, 14);
            // 
            // simpleLabelItem6
            // 
            this.simpleLabelItem6.AllowHotTrack = false;
            this.simpleLabelItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem6.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.simpleLabelItem6.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem6.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem6.CustomizationFormText = "遗传病史";
            this.simpleLabelItem6.Location = new System.Drawing.Point(0, 970);
            this.simpleLabelItem6.MaxSize = new System.Drawing.Size(0, 18);
            this.simpleLabelItem6.MinSize = new System.Drawing.Size(100, 18);
            this.simpleLabelItem6.Name = "simpleLabelItem6";
            this.simpleLabelItem6.Size = new System.Drawing.Size(100, 24);
            this.simpleLabelItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem6.Text = "遗传病史";
            this.simpleLabelItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem6.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem57
            // 
            this.layoutControlItem57.Control = this.flow残疾情况;
            this.layoutControlItem57.CustomizationFormText = "layoutControlItem57";
            this.layoutControlItem57.Location = new System.Drawing.Point(0, 1044);
            this.layoutControlItem57.Name = "layoutControlItem57";
            this.layoutControlItem57.Size = new System.Drawing.Size(725, 24);
            this.layoutControlItem57.Text = "layoutControlItem57";
            this.layoutControlItem57.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem57.TextToControlDistance = 0;
            this.layoutControlItem57.TextVisible = false;
            // 
            // layoutControlItem58
            // 
            this.layoutControlItem58.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem58.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem58.Control = this.flow厨房排风设施;
            this.layoutControlItem58.CustomizationFormText = "厨房排风设施";
            this.layoutControlItem58.Location = new System.Drawing.Point(22, 1182);
            this.layoutControlItem58.MinSize = new System.Drawing.Size(209, 24);
            this.layoutControlItem58.Name = "layoutControlItem58";
            this.layoutControlItem58.Size = new System.Drawing.Size(703, 24);
            this.layoutControlItem58.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem58.Text = "厨房排风设施";
            this.layoutControlItem58.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem58.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem58.TextToControlDistance = 5;
            // 
            // layoutControlItem59
            // 
            this.layoutControlItem59.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem59.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem59.Control = this.flow燃料类型;
            this.layoutControlItem59.CustomizationFormText = "燃料类型";
            this.layoutControlItem59.Location = new System.Drawing.Point(22, 1206);
            this.layoutControlItem59.MinSize = new System.Drawing.Size(209, 24);
            this.layoutControlItem59.Name = "layoutControlItem59";
            this.layoutControlItem59.Size = new System.Drawing.Size(703, 24);
            this.layoutControlItem59.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem59.Text = "燃料类型";
            this.layoutControlItem59.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem59.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem59.TextToControlDistance = 5;
            // 
            // layoutControlItem60
            // 
            this.layoutControlItem60.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem60.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem60.Control = this.flow饮水;
            this.layoutControlItem60.CustomizationFormText = "饮水";
            this.layoutControlItem60.Location = new System.Drawing.Point(22, 1230);
            this.layoutControlItem60.MinSize = new System.Drawing.Size(209, 24);
            this.layoutControlItem60.Name = "layoutControlItem60";
            this.layoutControlItem60.Size = new System.Drawing.Size(703, 24);
            this.layoutControlItem60.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem60.Text = "饮水";
            this.layoutControlItem60.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem60.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem60.TextToControlDistance = 5;
            // 
            // layoutControlItem61
            // 
            this.layoutControlItem61.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem61.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem61.Control = this.flow厕所;
            this.layoutControlItem61.CustomizationFormText = "厕所";
            this.layoutControlItem61.Location = new System.Drawing.Point(22, 1254);
            this.layoutControlItem61.MinSize = new System.Drawing.Size(209, 24);
            this.layoutControlItem61.Name = "layoutControlItem61";
            this.layoutControlItem61.Size = new System.Drawing.Size(703, 24);
            this.layoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem61.Text = "厕所";
            this.layoutControlItem61.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem61.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem61.TextToControlDistance = 5;
            // 
            // layoutControlItem62
            // 
            this.layoutControlItem62.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem62.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem62.Control = this.flow禽畜栏;
            this.layoutControlItem62.CustomizationFormText = "禽畜栏";
            this.layoutControlItem62.Location = new System.Drawing.Point(22, 1278);
            this.layoutControlItem62.MinSize = new System.Drawing.Size(209, 24);
            this.layoutControlItem62.Name = "layoutControlItem62";
            this.layoutControlItem62.Size = new System.Drawing.Size(703, 24);
            this.layoutControlItem62.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem62.Text = "禽畜栏";
            this.layoutControlItem62.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem62.TextSize = new System.Drawing.Size(80, 14);
            this.layoutControlItem62.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem15.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem15.Control = this.cbo与户主关系;
            this.layoutControlItem15.CustomizationFormText = "与户主关系";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "与户主关系(*)";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.radio档案状态;
            this.layoutControlItem6.CustomizationFormText = "档案状态";
            this.layoutControlItem6.Location = new System.Drawing.Point(350, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(250, 0);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "档案状态";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem4.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.cbo性别;
            this.layoutControlItem4.CustomizationFormText = "性别";
            this.layoutControlItem4.Location = new System.Drawing.Point(350, 24);
            this.layoutControlItem4.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem4.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem4.Text = "性别";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem3.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem3.Control = this.txt姓名;
            this.layoutControlItem3.CustomizationFormText = "姓名";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(155, 24);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "姓名";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem5.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.dte出生日期;
            this.layoutControlItem5.CustomizationFormText = "出生日期：";
            this.layoutControlItem5.Location = new System.Drawing.Point(350, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(247, 26);
            this.layoutControlItem5.Text = "出生日期";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem11.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt联系人姓名;
            this.layoutControlItem11.CustomizationFormText = "联系人姓名：";
            this.layoutControlItem11.Location = new System.Drawing.Point(350, 128);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem11.Text = "联系人姓名";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem10.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem10.Control = this.txt本人电话;
            this.layoutControlItem10.CustomizationFormText = "本人电话：";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 104);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.Text = "本人电话";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem12.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem12.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem12.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem12.Control = this.txt联系人电话;
            this.layoutControlItem12.CustomizationFormText = "联系人电话：";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 128);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "联系人电话";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem13.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem13.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem13.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem13.Control = this.cbo常住类型;
            this.layoutControlItem13.CustomizationFormText = "常住类型";
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 152);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "常住类型";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem16.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem16.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem16.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem16.Control = this.cbo血型;
            this.layoutControlItem16.CustomizationFormText = "血型";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 176);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "血型";
            this.layoutControlItem16.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem16.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem14.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem14.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem14.Control = this.cbo民族;
            this.layoutControlItem14.CustomizationFormText = "民族";
            this.layoutControlItem14.Location = new System.Drawing.Point(350, 152);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "民族";
            this.layoutControlItem14.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem14.TextToControlDistance = 5;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem17.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem17.Control = this.cboRH阴性;
            this.layoutControlItem17.CustomizationFormText = "RH";
            this.layoutControlItem17.Location = new System.Drawing.Point(350, 176);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(159, 24);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "RH阴性";
            this.layoutControlItem17.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem17.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem17.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem19.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem19.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem19.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem19.Control = this.cbo职业;
            this.layoutControlItem19.CustomizationFormText = "职业";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 200);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(155, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(350, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "职业";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem18.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem18.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem18.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem18.Control = this.cbo文化程度;
            this.layoutControlItem18.CustomizationFormText = "文化程度";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 224);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(350, 24);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(725, 24);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "文化程度";
            this.layoutControlItem18.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem18.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem18.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem21.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem21.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem21.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem21.Control = this.cbo婚姻状况;
            this.layoutControlItem21.CustomizationFormText = "婚姻状况";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 248);
            this.layoutControlItem21.MaxSize = new System.Drawing.Size(250, 24);
            this.layoutControlItem21.MinSize = new System.Drawing.Size(155, 24);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(725, 24);
            this.layoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem21.Text = "婚姻状况";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem28.Control = this.flow医疗费用支付方式;
            this.layoutControlItem28.CustomizationFormText = "医疗费用支付方式";
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 272);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(0, 200);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(725, 130);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(725, 130);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "医疗费用支付方式";
            this.layoutControlItem28.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem28.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem28.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem23.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem23.Control = this.txt医疗保险号;
            this.layoutControlItem23.CustomizationFormText = "医疗保险号";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 402);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(362, 24);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(362, 24);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(362, 24);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "医疗保险号";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem23.TextToControlDistance = 5;
            this.layoutControlItem23.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem24.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem24.Control = this.txt新农合号;
            this.layoutControlItem24.CustomizationFormText = "新农合号";
            this.layoutControlItem24.Location = new System.Drawing.Point(362, 402);
            this.layoutControlItem24.MaxSize = new System.Drawing.Size(363, 24);
            this.layoutControlItem24.MinSize = new System.Drawing.Size(363, 24);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(363, 24);
            this.layoutControlItem24.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem24.Text = "新农合号";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(104, 14);
            this.layoutControlItem24.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem25.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem25.Control = this.txt所属片区;
            this.layoutControlItem25.CustomizationFormText = "所属片区";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 486);
            this.layoutControlItem25.MinSize = new System.Drawing.Size(50, 25);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(525, 25);
            this.layoutControlItem25.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem25.Text = "所属片区";
            this.layoutControlItem25.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem25.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem25.TextToControlDistance = 5;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem26.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem26.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem26.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem26.Control = this.cbo档案类别;
            this.layoutControlItem26.CustomizationFormText = "档案类别";
            this.layoutControlItem26.Location = new System.Drawing.Point(525, 486);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(155, 24);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(200, 25);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "档案类别";
            this.layoutControlItem26.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem26.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem26.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btn重复档案检测;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(597, 48);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(128, 26);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem9.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt工作单位;
            this.layoutControlItem9.CustomizationFormText = "工作单位：";
            this.layoutControlItem9.Location = new System.Drawing.Point(350, 104);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem9.Text = "工作单位";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem50
            // 
            this.layoutControlItem50.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem50.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem50.Control = this.flow过敏史;
            this.layoutControlItem50.CustomizationFormText = "药物过敏史";
            this.layoutControlItem50.Location = new System.Drawing.Point(0, 511);
            this.layoutControlItem50.MinSize = new System.Drawing.Size(205, 24);
            this.layoutControlItem50.Name = "layoutControlItem50";
            this.layoutControlItem50.Size = new System.Drawing.Size(725, 24);
            this.layoutControlItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem50.Text = "药物过敏史";
            this.layoutControlItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem50.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem50.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem2.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.layoutControlItem2.Control = this.panelControl1;
            this.layoutControlItem2.CustomizationFormText = "居住地址";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 426);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(203, 60);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(725, 60);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "居住地址";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.cbo家族史;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(100, 804);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(54, 24);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(154, 804);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(571, 24);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(154, 920);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(571, 24);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(154, 970);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(571, 24);
            this.emptySpaceItem6.Text = "emptySpaceItem6";
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(154, 1020);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(571, 24);
            this.emptySpaceItem7.Text = "emptySpaceItem7";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.cbo暴露史;
            this.layoutControlItem30.CustomizationFormText = "layoutControlItem30";
            this.layoutControlItem30.Location = new System.Drawing.Point(100, 920);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(54, 24);
            this.layoutControlItem30.Text = "layoutControlItem30";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextToControlDistance = 0;
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.cbo遗传病史;
            this.layoutControlItem36.CustomizationFormText = "layoutControlItem36";
            this.layoutControlItem36.Location = new System.Drawing.Point(100, 970);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(54, 24);
            this.layoutControlItem36.Text = "layoutControlItem36";
            this.layoutControlItem36.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem36.TextToControlDistance = 0;
            this.layoutControlItem36.TextVisible = false;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.cbo残疾情况;
            this.layoutControlItem38.CustomizationFormText = "layoutControlItem38";
            this.layoutControlItem38.Location = new System.Drawing.Point(100, 1020);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(54, 24);
            this.layoutControlItem38.Text = "layoutControlItem38";
            this.layoutControlItem38.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem38.TextToControlDistance = 0;
            this.layoutControlItem38.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.gc家族史;
            this.layoutControlItem27.CustomizationFormText = "layoutControlItem27";
            this.layoutControlItem27.Location = new System.Drawing.Point(362, 828);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(104, 90);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(363, 90);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "layoutControlItem27";
            this.layoutControlItem27.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextToControlDistance = 0;
            this.layoutControlItem27.TextVisible = false;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.flow家族史;
            this.layoutControlItem41.CustomizationFormText = "layoutControlItem41";
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 828);
            this.layoutControlItem41.MinSize = new System.Drawing.Size(104, 70);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(362, 90);
            this.layoutControlItem41.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem41.Text = "layoutControlItem41";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem41.TextToControlDistance = 0;
            this.layoutControlItem41.TextVisible = false;
            // 
            // layoutControlItem48
            // 
            this.layoutControlItem48.Control = this.gc既往史;
            this.layoutControlItem48.CustomizationFormText = "layoutControlItem48";
            this.layoutControlItem48.Location = new System.Drawing.Point(22, 702);
            this.layoutControlItem48.MinSize = new System.Drawing.Size(219, 100);
            this.layoutControlItem48.Name = "layoutControlItem48";
            this.layoutControlItem48.Size = new System.Drawing.Size(703, 100);
            this.layoutControlItem48.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem48.Text = "layoutControlItem48";
            this.layoutControlItem48.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem48.TextToControlDistance = 0;
            this.layoutControlItem48.TextVisible = false;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(184, 994);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(541, 24);
            this.emptySpaceItem8.Text = "emptySpaceItem8";
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layout孕产情况
            // 
            this.layout孕产情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layout孕产情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layout孕产情况.Control = this.flowLayoutPanel1;
            this.layout孕产情况.CustomizationFormText = "孕产情况:";
            this.layout孕产情况.Location = new System.Drawing.Point(0, 1070);
            this.layout孕产情况.MinSize = new System.Drawing.Size(179, 30);
            this.layout孕产情况.Name = "layout孕产情况";
            this.layout孕产情况.Size = new System.Drawing.Size(725, 30);
            this.layout孕产情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layout孕产情况.Text = "孕产情况:";
            this.layout孕产情况.TextSize = new System.Drawing.Size(104, 14);
            this.layout孕产情况.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 535);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(725, 2);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.CustomizationFormText = "simpleSeparator2";
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 802);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(725, 2);
            this.simpleSeparator2.Text = "simpleSeparator2";
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator3";
            this.simpleSeparator3.Location = new System.Drawing.Point(0, 918);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(725, 2);
            this.simpleSeparator3.Text = "simpleSeparator3";
            // 
            // simpleSeparator4
            // 
            this.simpleSeparator4.AllowHotTrack = false;
            this.simpleSeparator4.CustomizationFormText = "simpleSeparator4";
            this.simpleSeparator4.Location = new System.Drawing.Point(0, 1180);
            this.simpleSeparator4.Name = "simpleSeparator4";
            this.simpleSeparator4.Size = new System.Drawing.Size(725, 2);
            this.simpleSeparator4.Text = "simpleSeparator4";
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem4});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 1182);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Size = new System.Drawing.Size(22, 120);
            this.layoutControlGroup4.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // simpleLabelItem4
            // 
            this.simpleLabelItem4.AllowHotTrack = false;
            this.simpleLabelItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem4.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem4.CustomizationFormText = "生活环境";
            this.simpleLabelItem4.FillControlToClientArea = false;
            this.simpleLabelItem4.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem4.MinSize = new System.Drawing.Size(20, 18);
            this.simpleLabelItem4.Name = "simpleLabelItem4";
            this.simpleLabelItem4.Size = new System.Drawing.Size(20, 118);
            this.simpleLabelItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem4.Text = "生活环境";
            this.simpleLabelItem4.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 537);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(22, 265);
            this.layoutControlGroup5.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            this.layoutControlGroup5.TextVisible = false;
            // 
            // simpleLabelItem2
            // 
            this.simpleLabelItem2.AllowHotTrack = false;
            this.simpleLabelItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem2.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem2.CustomizationFormText = "既往史";
            this.simpleLabelItem2.FillControlToClientArea = false;
            this.simpleLabelItem2.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem2.MinSize = new System.Drawing.Size(20, 18);
            this.simpleLabelItem2.Name = "simpleLabelItem2";
            this.simpleLabelItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.simpleLabelItem2.Size = new System.Drawing.Size(20, 263);
            this.simpleLabelItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem2.Text = "既往史";
            this.simpleLabelItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem2.TextSize = new System.Drawing.Size(96, 14);
            // 
            // simpleSeparator5
            // 
            this.simpleSeparator5.AllowHotTrack = false;
            this.simpleSeparator5.CustomizationFormText = "simpleSeparator5";
            this.simpleSeparator5.Location = new System.Drawing.Point(22, 612);
            this.simpleSeparator5.Name = "simpleSeparator5";
            this.simpleSeparator5.Size = new System.Drawing.Size(703, 2);
            this.simpleSeparator5.Text = "simpleSeparator5";
            // 
            // simpleSeparator6
            // 
            this.simpleSeparator6.AllowHotTrack = false;
            this.simpleSeparator6.CustomizationFormText = "simpleSeparator6";
            this.simpleSeparator6.Location = new System.Drawing.Point(22, 642);
            this.simpleSeparator6.Name = "simpleSeparator6";
            this.simpleSeparator6.Size = new System.Drawing.Size(703, 2);
            this.simpleSeparator6.Text = "simpleSeparator6";
            // 
            // simpleSeparator7
            // 
            this.simpleSeparator7.AllowHotTrack = false;
            this.simpleSeparator7.CustomizationFormText = "simpleSeparator7";
            this.simpleSeparator7.Location = new System.Drawing.Point(22, 672);
            this.simpleSeparator7.Name = "simpleSeparator7";
            this.simpleSeparator7.Size = new System.Drawing.Size(703, 2);
            this.simpleSeparator7.Text = "simpleSeparator7";
            // 
            // simpleSeparator8
            // 
            this.simpleSeparator8.AllowHotTrack = false;
            this.simpleSeparator8.CustomizationFormText = "simpleSeparator8";
            this.simpleSeparator8.Location = new System.Drawing.Point(0, 968);
            this.simpleSeparator8.Name = "simpleSeparator8";
            this.simpleSeparator8.Size = new System.Drawing.Size(725, 2);
            this.simpleSeparator8.Text = "simpleSeparator8";
            // 
            // simpleSeparator9
            // 
            this.simpleSeparator9.AllowHotTrack = false;
            this.simpleSeparator9.CustomizationFormText = "simpleSeparator9";
            this.simpleSeparator9.Location = new System.Drawing.Point(0, 1018);
            this.simpleSeparator9.Name = "simpleSeparator9";
            this.simpleSeparator9.Size = new System.Drawing.Size(725, 2);
            this.simpleSeparator9.Text = "simpleSeparator9";
            // 
            // simpleSeparator10
            // 
            this.simpleSeparator10.AllowHotTrack = false;
            this.simpleSeparator10.CustomizationFormText = "simpleSeparator10";
            this.simpleSeparator10.Location = new System.Drawing.Point(0, 1068);
            this.simpleSeparator10.Name = "simpleSeparator10";
            this.simpleSeparator10.Size = new System.Drawing.Size(725, 2);
            this.simpleSeparator10.Text = "simpleSeparator10";
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem39.AppearanceItemCaption.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem39.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem39.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem39.Control = this.cbo证件类型;
            this.layoutControlItem39.CustomizationFormText = "证件编号";
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem39.MaxSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem39.MinSize = new System.Drawing.Size(200, 24);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem39.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem39.Text = "证件编号";
            this.layoutControlItem39.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem39.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem39.TextToControlDistance = 5;
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.txt证件编号;
            this.layoutControlItem40.CustomizationFormText = "layoutControlItem40";
            this.layoutControlItem40.Location = new System.Drawing.Point(200, 48);
            this.layoutControlItem40.MaxSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem40.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(150, 26);
            this.layoutControlItem40.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem40.Text = "layoutControlItem40";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem40.TextToControlDistance = 0;
            this.layoutControlItem40.TextVisible = false;
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.flowLayoutPanel2;
            this.layoutControlItem42.CustomizationFormText = "layoutControlItem42";
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem42.MinSize = new System.Drawing.Size(104, 30);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(725, 30);
            this.layoutControlItem42.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem42.Text = "  ";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem1.Control = this.flow外出;
            this.layoutControlItem1.CustomizationFormText = "外出情况:";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 1302);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(425, 24);
            this.layoutControlItem1.Text = "流动人口:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem51
            // 
            this.layoutControlItem51.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem51.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem51.Control = this.txt外出地址;
            this.layoutControlItem51.CustomizationFormText = "外出地址:";
            this.layoutControlItem51.Location = new System.Drawing.Point(425, 1302);
            this.layoutControlItem51.Name = "layoutControlItem51";
            this.layoutControlItem51.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem51.Text = "流动地址:";
            this.layoutControlItem51.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem51.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem51.TextToControlDistance = 5;
            // 
            // layoutControlItem52
            // 
            this.layoutControlItem52.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem52.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem52.Control = this.txt档案位置;
            this.layoutControlItem52.CustomizationFormText = "档案位置";
            this.layoutControlItem52.Location = new System.Drawing.Point(350, 200);
            this.layoutControlItem52.Name = "layoutControlItem52";
            this.layoutControlItem52.Size = new System.Drawing.Size(375, 24);
            this.layoutControlItem52.Text = "档案位置";
            this.layoutControlItem52.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem52.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem52.TextToControlDistance = 5;
            // 
            // simpleLabelItem7
            // 
            this.simpleLabelItem7.AllowHotTrack = false;
            this.simpleLabelItem7.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem7.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.simpleLabelItem7.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem7.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem7.CustomizationFormText = "残疾情况";
            this.simpleLabelItem7.Location = new System.Drawing.Point(0, 1020);
            this.simpleLabelItem7.MaxSize = new System.Drawing.Size(0, 18);
            this.simpleLabelItem7.MinSize = new System.Drawing.Size(100, 18);
            this.simpleLabelItem7.Name = "simpleLabelItem7";
            this.simpleLabelItem7.Size = new System.Drawing.Size(100, 24);
            this.simpleLabelItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem7.Text = "残疾情况";
            this.simpleLabelItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem7.TextSize = new System.Drawing.Size(96, 14);
            // 
            // layoutControlItem53
            // 
            this.layoutControlItem53.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.layoutControlItem53.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseForeColor = true;
            this.layoutControlItem53.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem53.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem53.Control = this.flowLayoutPanel3;
            this.layoutControlItem53.CustomizationFormText = "家庭情况";
            this.layoutControlItem53.Location = new System.Drawing.Point(0, 1100);
            this.layoutControlItem53.MaxSize = new System.Drawing.Size(100, 0);
            this.layoutControlItem53.MinSize = new System.Drawing.Size(100, 54);
            this.layoutControlItem53.Name = "layoutControlItem53";
            this.layoutControlItem53.Size = new System.Drawing.Size(100, 80);
            this.layoutControlItem53.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem53.Text = "家庭情况";
            this.layoutControlItem53.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem53.TextSize = new System.Drawing.Size(100, 14);
            this.layoutControlItem53.TextToControlDistance = 5;
            // 
            // layoutControlItem56
            // 
            this.layoutControlItem56.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem56.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem56.Control = this.flowLayoutPanel9;
            this.layoutControlItem56.CustomizationFormText = "户主姓名";
            this.layoutControlItem56.Location = new System.Drawing.Point(100, 1100);
            this.layoutControlItem56.MinSize = new System.Drawing.Size(140, 30);
            this.layoutControlItem56.Name = "layoutControlItem56";
            this.layoutControlItem56.Size = new System.Drawing.Size(140, 30);
            this.layoutControlItem56.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem56.Text = "户主姓名";
            this.layoutControlItem56.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem56.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem56.TextToControlDistance = 5;
            // 
            // layoutControlItem64
            // 
            this.layoutControlItem64.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem64.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem64.Control = this.flowLayoutPanel11;
            this.layoutControlItem64.CustomizationFormText = "身份证号";
            this.layoutControlItem64.Location = new System.Drawing.Point(240, 1100);
            this.layoutControlItem64.Name = "layoutControlItem64";
            this.layoutControlItem64.Size = new System.Drawing.Size(205, 30);
            this.layoutControlItem64.Text = "身份证号";
            this.layoutControlItem64.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem64.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem64.TextToControlDistance = 5;
            // 
            // layoutControlItem54
            // 
            this.layoutControlItem54.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem54.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem54.Control = this.flowLayoutPanel7;
            this.layoutControlItem54.CustomizationFormText = "家庭人口数";
            this.layoutControlItem54.Location = new System.Drawing.Point(445, 1100);
            this.layoutControlItem54.MinSize = new System.Drawing.Size(130, 24);
            this.layoutControlItem54.Name = "layoutControlItem54";
            this.layoutControlItem54.Size = new System.Drawing.Size(130, 30);
            this.layoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem54.Text = "家庭人口数";
            this.layoutControlItem54.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem54.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem54.TextToControlDistance = 5;
            // 
            // layoutControlItem63
            // 
            this.layoutControlItem63.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem63.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem63.Control = this.flowLayoutPanel10;
            this.layoutControlItem63.CustomizationFormText = "家庭结构";
            this.layoutControlItem63.Location = new System.Drawing.Point(575, 1100);
            this.layoutControlItem63.MinSize = new System.Drawing.Size(150, 24);
            this.layoutControlItem63.Name = "layoutControlItem63";
            this.layoutControlItem63.Size = new System.Drawing.Size(150, 30);
            this.layoutControlItem63.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem63.Text = "家庭结构";
            this.layoutControlItem63.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem63.TextSize = new System.Drawing.Size(60, 20);
            this.layoutControlItem63.TextToControlDistance = 5;
            // 
            // lay居住情况
            // 
            this.lay居住情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lay居住情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lay居住情况.Control = this.flow居住情况;
            this.lay居住情况.CustomizationFormText = "居住情况";
            this.lay居住情况.Location = new System.Drawing.Point(100, 1130);
            this.lay居住情况.MinSize = new System.Drawing.Size(179, 50);
            this.lay居住情况.Name = "lay居住情况";
            this.lay居住情况.Size = new System.Drawing.Size(625, 50);
            this.lay居住情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lay居住情况.Text = "居住情况";
            this.lay居住情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lay居住情况.TextSize = new System.Drawing.Size(60, 20);
            this.lay居住情况.TextToControlDistance = 5;
            // 
            // lbl本人或家属签字
            // 
            this.lbl本人或家属签字.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl本人或家属签字.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl本人或家属签字.AppearanceItemCaption.Options.UseFont = true;
            this.lbl本人或家属签字.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl本人或家属签字.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl本人或家属签字.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl本人或家属签字.Control = this.textEdit本人或家属签字;
            this.lbl本人或家属签字.CustomizationFormText = "本人或家属签字：";
            this.lbl本人或家属签字.Location = new System.Drawing.Point(0, 1326);
            this.lbl本人或家属签字.MinSize = new System.Drawing.Size(131, 60);
            this.lbl本人或家属签字.Name = "lbl本人或家属签字";
            this.lbl本人或家属签字.Size = new System.Drawing.Size(272, 71);
            this.lbl本人或家属签字.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl本人或家属签字.Tag = "check";
            this.lbl本人或家属签字.Text = "本人或家属签字：";
            this.lbl本人或家属签字.TextSize = new System.Drawing.Size(104, 14);
            // 
            // lbl签字时间
            // 
            this.lbl签字时间.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl签字时间.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl签字时间.AppearanceItemCaption.Options.UseFont = true;
            this.lbl签字时间.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl签字时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl签字时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl签字时间.Control = this.dateEdit签字时间;
            this.lbl签字时间.CustomizationFormText = "签字时间：";
            this.lbl签字时间.Location = new System.Drawing.Point(425, 1326);
            this.lbl签字时间.Name = "lbl签字时间";
            this.lbl签字时间.Size = new System.Drawing.Size(300, 24);
            this.lbl签字时间.Tag = "check";
            this.lbl签字时间.Text = "签字时间：";
            this.lbl签字时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl签字时间.TextSize = new System.Drawing.Size(100, 14);
            this.lbl签字时间.TextToControlDistance = 5;
            // 
            // layoutControlItem55
            // 
            this.layoutControlItem55.Control = this.sbtnFingerPrint;
            this.layoutControlItem55.CustomizationFormText = "layoutControlItem55";
            this.layoutControlItem55.Location = new System.Drawing.Point(272, 1326);
            this.layoutControlItem55.Name = "layoutControlItem55";
            this.layoutControlItem55.Size = new System.Drawing.Size(153, 71);
            this.layoutControlItem55.Text = "layoutControlItem55";
            this.layoutControlItem55.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem55.TextToControlDistance = 0;
            this.layoutControlItem55.TextVisible = false;
            // 
            // layoutControlItem65
            // 
            this.layoutControlItem65.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem65.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem65.Control = this.txt复核备注;
            this.layoutControlItem65.CustomizationFormText = "复核备注：";
            this.layoutControlItem65.Location = new System.Drawing.Point(425, 1350);
            this.layoutControlItem65.Name = "layoutControlItem65";
            this.layoutControlItem65.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem65.Text = "复核备注：";
            this.layoutControlItem65.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem65.TextSize = new System.Drawing.Size(100, 20);
            this.layoutControlItem65.TextToControlDistance = 5;
            // 
            // layoutControlItem66
            // 
            this.layoutControlItem66.Control = this.ch复核标记;
            this.layoutControlItem66.CustomizationFormText = "layoutControlItem66";
            this.layoutControlItem66.Location = new System.Drawing.Point(425, 1374);
            this.layoutControlItem66.MaxSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem66.MinSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem66.Name = "layoutControlItem66";
            this.layoutControlItem66.Size = new System.Drawing.Size(150, 23);
            this.layoutControlItem66.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem66.Text = "layoutControlItem66";
            this.layoutControlItem66.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem66.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem66.TextToControlDistance = 0;
            this.layoutControlItem66.TextVisible = false;
            // 
            // layoutControlItem67
            // 
            this.layoutControlItem67.Control = this.ch二次复核;
            this.layoutControlItem67.CustomizationFormText = "layoutControlItem67";
            this.layoutControlItem67.Location = new System.Drawing.Point(575, 1374);
            this.layoutControlItem67.MaxSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem67.MinSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem67.Name = "layoutControlItem67";
            this.layoutControlItem67.Size = new System.Drawing.Size(150, 23);
            this.layoutControlItem67.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem67.Text = "layoutControlItem67";
            this.layoutControlItem67.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem67.TextToControlDistance = 0;
            this.layoutControlItem67.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem43,
            this.layoutControlItem44,
            this.layoutControlItem45,
            this.layoutControlItem46,
            this.layoutControlItem47,
            this.layoutControlItem49});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 1405);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1);
            this.layoutControlGroup3.Size = new System.Drawing.Size(733, 56);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem43
            // 
            this.layoutControlItem43.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem43.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem43.Control = this.dte调查时间;
            this.layoutControlItem43.CustomizationFormText = "调查时间";
            this.layoutControlItem43.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem43.Name = "layoutControlItem43";
            this.layoutControlItem43.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem43.Text = "调查时间";
            this.layoutControlItem43.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem44
            // 
            this.layoutControlItem44.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem44.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem44.Control = this.dte录入时间;
            this.layoutControlItem44.CustomizationFormText = "录入时间";
            this.layoutControlItem44.Location = new System.Drawing.Point(221, 0);
            this.layoutControlItem44.Name = "layoutControlItem44";
            this.layoutControlItem44.Size = new System.Drawing.Size(241, 24);
            this.layoutControlItem44.Text = "录入时间";
            this.layoutControlItem44.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem45
            // 
            this.layoutControlItem45.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem45.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem45.Control = this.dte最近更新时间;
            this.layoutControlItem45.CustomizationFormText = "最近更新时间";
            this.layoutControlItem45.Location = new System.Drawing.Point(462, 0);
            this.layoutControlItem45.Name = "layoutControlItem45";
            this.layoutControlItem45.Size = new System.Drawing.Size(263, 24);
            this.layoutControlItem45.Text = "最近更新时间";
            this.layoutControlItem45.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem46
            // 
            this.layoutControlItem46.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem46.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem46.Control = this.txt录入人;
            this.layoutControlItem46.CustomizationFormText = "录入人";
            this.layoutControlItem46.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem46.Name = "layoutControlItem46";
            this.layoutControlItem46.Size = new System.Drawing.Size(221, 24);
            this.layoutControlItem46.Text = "录入人";
            this.layoutControlItem46.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem47
            // 
            this.layoutControlItem47.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem47.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem47.Control = this.txt最近更新人;
            this.layoutControlItem47.CustomizationFormText = "最近更新人";
            this.layoutControlItem47.Location = new System.Drawing.Point(221, 24);
            this.layoutControlItem47.Name = "layoutControlItem47";
            this.layoutControlItem47.Size = new System.Drawing.Size(241, 24);
            this.layoutControlItem47.Text = "最近更新人";
            this.layoutControlItem47.TextSize = new System.Drawing.Size(104, 14);
            // 
            // layoutControlItem49
            // 
            this.layoutControlItem49.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem49.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem49.Control = this.txt当前所属机构;
            this.layoutControlItem49.CustomizationFormText = "当前所属机构";
            this.layoutControlItem49.Location = new System.Drawing.Point(462, 24);
            this.layoutControlItem49.Name = "layoutControlItem49";
            this.layoutControlItem49.Size = new System.Drawing.Size(263, 24);
            this.layoutControlItem49.Text = "当前所属机构";
            this.layoutControlItem49.TextSize = new System.Drawing.Size(104, 14);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.flowLayoutPanel15);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(752, 30);
            this.panelControl2.TabIndex = 1;
            // 
            // flowLayoutPanel15
            // 
            this.flowLayoutPanel15.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel15.Controls.Add(this.simpleButton2);
            this.flowLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(748, 26);
            this.flowLayoutPanel15.TabIndex = 2;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(3, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(61, 21);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "保存";
            this.simpleButton1.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.Location = new System.Drawing.Point(70, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(97, 21);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "填表说明";
            // 
            // UC个人基本信息表
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl2);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UC个人基本信息表";
            this.Size = new System.Drawing.Size(752, 500);
            this.Load += new System.EventHandler(this.frm个人基本信息表_Load);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch二次复核.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch复核标记.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt复核备注.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit签字时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit签字时间.Properties)).EndInit();
            this.flowLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt户主身份证号.Properties)).EndInit();
            this.flowLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt家庭结构.Properties)).EndInit();
            this.flowLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt户主姓名.Properties)).EndInit();
            this.flow居住情况.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk与成年子女同住.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk与子孙三代同住.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk夫妻二人同住.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk独居.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk计划生育特殊家庭.Properties)).EndInit();
            this.flowLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txt家庭人口数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案位置.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外出地址.Properties)).EndInit();
            this.flow外出.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit县域外流入.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit县域内流出.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit县域外流出.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk是否流动人口.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否签约.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否贫困人口.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt证件编号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo证件类型.Properties)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk孕产1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk孕产2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk孕产3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk孕产4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo孕次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo产次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc既往史)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gv既往史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            this.flow家族史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_慢性阻塞性肺疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_恶性肿瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_脑卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_重性精神疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_结核病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_肝炎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_先天畸形.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk家族史_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家族史_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo家族史关系.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gc家族史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv家族史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkp家族关系)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpcbo家族关系)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo残疾情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo遗传病史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo暴露史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo家族史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址_详细地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo居住地址_市.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo居住地址_县.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo居住地址_街道.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo居住地址_村委会.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo档案类别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radio档案状态.Properties)).EndInit();
            this.flow禽畜栏.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk禽畜栏_单设.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk禽畜栏_室内.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk禽畜栏_室外.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk禽畜栏_无.Properties)).EndInit();
            this.flow厕所.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk厕所_卫生厕所.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厕所_一格或二格粪池式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厕所_露天粪坑.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厕所_马桶.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厕所_简易棚侧.Properties)).EndInit();
            this.flow饮水.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_自来水.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_经净化过滤的水.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_井水.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_河湖水.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_糖水.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk饮水_其他.Properties)).EndInit();
            this.flow燃料类型.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_液化气.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_煤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_天然气.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_沼气.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_柴火.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk燃料类型_其他.Properties)).EndInit();
            this.flow厨房排风设施.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk厨房排风设施_无.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厨房排风设施_油烟机.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厨房排风设施_换气扇.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk厨房排风设施_烟囱.Properties)).EndInit();
            this.flow残疾情况.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_听力残.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_言语残.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_肢体残.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_智力残.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_视力残.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_精神残.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk残疾情况_其他残疾.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt残疾情况_其他残疾.Properties)).EndInit();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio输血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt输血原因.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte输血时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte输血时间.Properties)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio外伤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外伤名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte外伤时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte外伤时间.Properties)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio手术.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt手术名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte手术时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte手术时间.Properties)).EndInit();
            this.flow疾病.ResumeLayout(false);
            this.flow疾病.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radio疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_高血压.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_糖尿病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_冠心病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_慢性阻塞性肺病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_恶性肿瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt疾病_恶性肿瘤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_脑卒中.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_重性精神疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_结核病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_肝炎.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_其他法定传染病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_职业病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt疾病_职业病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt疾病_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte疾病_确诊时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte疾病_确诊时间.Properties)).EndInit();
            this.flow医疗费用支付方式.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_城镇职工基本医疗保险.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit职工医疗保险卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_城镇居民基本医疗保险.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居民医疗保险卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_贫困救助.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit贫困救助卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_商业医疗保险.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_全公费.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_全自费.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医疗费用支付方式_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_新型农村合作医疗.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式_社会医疗保险.Properties)).EndInit();
            this.flow过敏史.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cbo过敏史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk过敏史_青霉素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk过敏史_磺胺.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk过敏史_不详.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk过敏史_链霉素.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk过敏史_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt过敏史_其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入人.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遗传病史_疾病名称.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt暴露史_射线.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt暴露史_毒物.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt暴露史_化学品.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新农合号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医疗保险号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本人电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt工作单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo常住类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo民族.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo血型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboRH阴性.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo文化程度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo劳动程度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo婚姻状况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo与户主关系.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte调查时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte录入时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dte最近更新时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt所属片区.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit本人或家属签字.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layout孕产情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lay居住情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl本人或家属签字)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl签字时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.flowLayoutPanel15.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.DateEdit dte出生日期;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt联系人电话;
        private DevExpress.XtraEditors.TextEdit txt联系人姓名;
        private DevExpress.XtraEditors.TextEdit txt本人电话;
        private DevExpress.XtraEditors.TextEdit txt工作单位;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraEditors.TextEdit txt最近更新人;
        private DevExpress.XtraEditors.TextEdit txt录入人;
        private DevExpress.XtraEditors.TextEdit txt遗传病史_疾病名称;
        private DevExpress.XtraEditors.TextEdit txt暴露史_射线;
        private DevExpress.XtraEditors.TextEdit txt暴露史_毒物;
        private DevExpress.XtraEditors.TextEdit txt暴露史_化学品;
        private DevExpress.XtraEditors.TextEdit txt新农合号;
        private DevExpress.XtraEditors.TextEdit txt医疗保险号;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem43;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem44;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem45;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem46;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem47;
        private DevExpress.XtraEditors.TextEdit txt当前所属机构;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem49;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.ComboBoxEdit cbo性别;
        private DevExpress.XtraEditors.ComboBoxEdit cbo常住类型;
        private DevExpress.XtraEditors.ComboBoxEdit cbo民族;
        private DevExpress.XtraEditors.ComboBoxEdit cbo血型;
        private DevExpress.XtraEditors.ComboBoxEdit cboRH阴性;
        private DevExpress.XtraEditors.ComboBoxEdit cbo文化程度;
        private DevExpress.XtraEditors.ComboBoxEdit cbo职业;
        private DevExpress.XtraEditors.ComboBoxEdit cbo劳动程度;
        private DevExpress.XtraEditors.ComboBoxEdit cbo婚姻状况;
        private System.Windows.Forms.FlowLayoutPanel flow过敏史;
        private DevExpress.XtraEditors.ComboBoxEdit cbo过敏史;
        private DevExpress.XtraEditors.CheckEdit chk过敏史_青霉素;
        private DevExpress.XtraEditors.CheckEdit chk过敏史_磺胺;
        private DevExpress.XtraEditors.CheckEdit chk过敏史_链霉素;
        private DevExpress.XtraEditors.CheckEdit chk过敏史_其他;
        private DevExpress.XtraEditors.TextEdit txt过敏史_其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem50;
        private System.Windows.Forms.FlowLayoutPanel flow医疗费用支付方式;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式_全自费;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式_全公费;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式_城镇职工基本医疗保险;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式_城镇居民基本医疗保险;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式_新型农村合作医疗;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式_社会医疗保险;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式_商业医疗保险;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式_贫困救助;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式_其他;
        private DevExpress.XtraEditors.TextEdit txt医疗费用支付方式_其他;
        private System.Windows.Forms.FlowLayoutPanel flow疾病;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.CheckEdit chk疾病_高血压;
        private DevExpress.XtraEditors.CheckEdit chk疾病_糖尿病;
        private DevExpress.XtraEditors.CheckEdit chk疾病_冠心病;
        private DevExpress.XtraEditors.CheckEdit chk疾病_慢性阻塞性肺病;
        private DevExpress.XtraEditors.CheckEdit chk疾病_恶性肿瘤;
        private DevExpress.XtraEditors.TextEdit txt疾病_恶性肿瘤;
        private DevExpress.XtraEditors.CheckEdit chk疾病_脑卒中;
        private DevExpress.XtraEditors.CheckEdit chk疾病_重性精神疾病;
        private DevExpress.XtraEditors.CheckEdit chk疾病_结核病;
        private DevExpress.XtraEditors.CheckEdit chk疾病_肝炎;
        private DevExpress.XtraEditors.CheckEdit chk疾病_其他法定传染病;
        private DevExpress.XtraEditors.CheckEdit chk疾病_职业病;
        private DevExpress.XtraEditors.TextEdit txt疾病_职业病;
        private DevExpress.XtraEditors.CheckEdit chk疾病_其他;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dte疾病_确诊时间;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.SimpleButton btn手术_添加;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraEditors.SimpleButton btn外伤_添加;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private DevExpress.XtraEditors.SimpleButton btn输血_添加;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem5;
        private DevExpress.XtraEditors.SimpleButton btn家族史_添加;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem6;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem7;
        private System.Windows.Forms.FlowLayoutPanel flow残疾情况;
        private DevExpress.XtraEditors.CheckEdit chk残疾情况_听力残;
        private DevExpress.XtraEditors.CheckEdit chk残疾情况_言语残;
        private DevExpress.XtraEditors.CheckEdit chk残疾情况_肢体残;
        private DevExpress.XtraEditors.CheckEdit chk残疾情况_智力残;
        private DevExpress.XtraEditors.CheckEdit chk残疾情况_视力残;
        private DevExpress.XtraEditors.CheckEdit chk残疾情况_精神残;
        private DevExpress.XtraEditors.CheckEdit chk残疾情况_其他残疾;
        private DevExpress.XtraEditors.TextEdit txt残疾情况_其他残疾;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem57;
        private System.Windows.Forms.FlowLayoutPanel flow禽畜栏;
        private DevExpress.XtraEditors.CheckEdit chk禽畜栏_单设;
        private DevExpress.XtraEditors.CheckEdit chk禽畜栏_室内;
        private DevExpress.XtraEditors.CheckEdit chk禽畜栏_室外;
        private System.Windows.Forms.FlowLayoutPanel flow厕所;
        private DevExpress.XtraEditors.CheckEdit chk厕所_卫生厕所;
        private DevExpress.XtraEditors.CheckEdit chk厕所_一格或二格粪池式;
        private DevExpress.XtraEditors.CheckEdit chk厕所_马桶;
        private DevExpress.XtraEditors.CheckEdit chk厕所_露天粪坑;
        private DevExpress.XtraEditors.CheckEdit chk厕所_简易棚侧;
        private System.Windows.Forms.FlowLayoutPanel flow饮水;
        private DevExpress.XtraEditors.CheckEdit chk饮水_自来水;
        private DevExpress.XtraEditors.CheckEdit chk饮水_经净化过滤的水;
        private DevExpress.XtraEditors.CheckEdit chk饮水_井水;
        private DevExpress.XtraEditors.CheckEdit chk饮水_河湖水;
        private DevExpress.XtraEditors.CheckEdit chk饮水_糖水;
        private DevExpress.XtraEditors.CheckEdit chk饮水_其他;
        private System.Windows.Forms.FlowLayoutPanel flow燃料类型;
        private DevExpress.XtraEditors.CheckEdit chk燃料类型_液化气;
        private DevExpress.XtraEditors.CheckEdit chk燃料类型_煤;
        private DevExpress.XtraEditors.CheckEdit chk燃料类型_天然气;
        private DevExpress.XtraEditors.CheckEdit chk燃料类型_沼气;
        private DevExpress.XtraEditors.CheckEdit chk燃料类型_柴火;
        private DevExpress.XtraEditors.CheckEdit chk燃料类型_其他;
        private System.Windows.Forms.FlowLayoutPanel flow厨房排风设施;
        private DevExpress.XtraEditors.CheckEdit chk厨房排风设施_无;
        private DevExpress.XtraEditors.CheckEdit chk厨房排风设施_油烟机;
        private DevExpress.XtraEditors.CheckEdit chk厨房排风设施_换气扇;
        private DevExpress.XtraEditors.CheckEdit chk厨房排风设施_烟囱;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem58;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem59;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem60;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem61;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem62;
        private DevExpress.XtraEditors.ComboBoxEdit cbo与户主关系;
        private DevExpress.XtraEditors.RadioGroup radio档案状态;
        private DevExpress.XtraEditors.ComboBoxEdit cbo证件类型;
        private DevExpress.XtraEditors.TextEdit txt证件编号;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.ComboBoxEdit cbo档案类别;
        private DevExpress.XtraEditors.SimpleButton btn重复档案检测;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.TextEdit txt居住地址_详细地址;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.ComboBoxEdit cbo家族史;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.ComboBoxEdit cbo残疾情况;
        private DevExpress.XtraEditors.ComboBoxEdit cbo遗传病史;
        private DevExpress.XtraEditors.ComboBoxEdit cbo暴露史;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraEditors.SimpleButton btn疾病_添加;
        private DevExpress.XtraEditors.DateEdit dte调查时间;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LookUpEdit cbo居住地址_市;
        private DevExpress.XtraEditors.LookUpEdit cbo居住地址_县;
        private DevExpress.XtraEditors.LookUpEdit cbo居住地址_街道;
        private DevExpress.XtraEditors.LookUpEdit cbo居住地址_村委会;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        private System.Windows.Forms.FlowLayoutPanel flow家族史;
        private DevExpress.XtraEditors.CheckEdit chk家族史_高血压;
        private DevExpress.XtraEditors.CheckEdit chk家族史_糖尿病;
        private DevExpress.XtraEditors.CheckEdit chk家族史_冠心病;
        private DevExpress.XtraEditors.CheckEdit chk家族史_慢性阻塞性肺疾病;
        private DevExpress.XtraEditors.CheckEdit chk家族史_恶性肿瘤;
        private DevExpress.XtraEditors.CheckEdit chk家族史_脑卒中;
        private DevExpress.XtraEditors.CheckEdit chk家族史_重性精神疾病;
        private DevExpress.XtraEditors.CheckEdit chk家族史_结核病;
        private DevExpress.XtraEditors.CheckEdit chk家族史_肝炎;
        private DevExpress.XtraEditors.CheckEdit chk家族史_先天畸形;
        private DevExpress.XtraEditors.CheckEdit chk家族史_其他;
        private DevExpress.XtraEditors.TextEdit txt家族史_其他;
        private DevExpress.XtraGrid.GridControl gc家族史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv家族史;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraGrid.Columns.GridColumn col家族病史;
        private DevExpress.XtraGrid.Columns.GridColumn col家族关系;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraEditors.CheckEdit chk禽畜栏_无;
        private DevExpress.XtraEditors.CheckEdit chk过敏史_不详;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit txt输血原因;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txt外伤名称;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txt手术名称;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit dte手术时间;
        private DevExpress.XtraEditors.DateEdit dte外伤时间;
        private DevExpress.XtraGrid.GridControl gc既往史;
        private DevExpress.XtraGrid.Views.Grid.GridView gv既往史;
        private DevExpress.XtraGrid.Columns.GridColumn col疾病名称;
        private DevExpress.XtraGrid.Columns.GridColumn col日期;
        private DevExpress.XtraGrid.Columns.GridColumn col疾病名称其他;
        private DevExpress.XtraGrid.Columns.GridColumn col疾病类型;
        private DevExpress.XtraGrid.Columns.GridColumn col疾病其他;
        private DevExpress.XtraGrid.Columns.GridColumn col职业病其他;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem48;
        private DevExpress.XtraGrid.Columns.GridColumn col恶性肿瘤;
        private DevExpress.XtraEditors.TextEdit txt疾病_其他;
        private DevExpress.XtraEditors.DateEdit dte输血时间;
        private DevExpress.XtraEditors.RadioGroup radio手术;
        private DevExpress.XtraEditors.RadioGroup radio输血;
        private DevExpress.XtraEditors.RadioGroup radio外伤;
        private DevExpress.XtraEditors.RadioGroup radio疾病;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuItem删除选中项;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraEditors.ComboBoxEdit cbo家族史关系;
        private DevExpress.XtraGrid.Columns.GridColumn col其他疾病;
        private DevExpress.XtraGrid.Columns.GridColumn col疾病编号;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rpcbo家族关系;
        private DevExpress.XtraGrid.Columns.GridColumn col家族病史代码;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lkp家族关系;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraLayout.LayoutControlItem layout孕产情况;
        private DevExpress.XtraEditors.CheckEdit chk孕产1;
        private DevExpress.XtraEditors.CheckEdit chk孕产2;
        private DevExpress.XtraEditors.CheckEdit chk孕产3;
        private DevExpress.XtraEditors.CheckEdit chk孕产4;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.ComboBoxEdit cbo孕次;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.ComboBoxEdit cbo产次;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator5;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator6;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator7;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator8;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator9;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraEditors.TextEdit dte录入时间;
        private DevExpress.XtraEditors.TextEdit dte最近更新时间;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.CheckEdit chk是否流动人口;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraEditors.CheckEdit chk是否签约;
        private DevExpress.XtraEditors.CheckEdit chk是否贫困人口;
        private DevExpress.XtraEditors.TextEdit txt外出地址;
        private System.Windows.Forms.FlowLayoutPanel flow外出;
        private DevExpress.XtraEditors.CheckEdit checkEdit县域内流出;
        private DevExpress.XtraEditors.CheckEdit checkEdit县域外流出;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem51;
        private DevExpress.XtraEditors.TextEdit txt档案位置;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem52;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private DevExpress.XtraEditors.TextEdit txt户主身份证号;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        private DevExpress.XtraEditors.TextEdit txt家庭结构;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        private DevExpress.XtraEditors.TextEdit txt户主姓名;
        private System.Windows.Forms.FlowLayoutPanel flow居住情况;
        private DevExpress.XtraEditors.CheckEdit chk与成年子女同住;
        private DevExpress.XtraEditors.CheckEdit chk与子孙三代同住;
        private DevExpress.XtraEditors.CheckEdit chk夫妻二人同住;
        private DevExpress.XtraEditors.CheckEdit chk独居;
        private DevExpress.XtraEditors.CheckEdit chk计划生育特殊家庭;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private DevExpress.XtraEditors.TextEdit txt家庭人口数;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem53;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem56;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem64;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem54;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem63;
        private DevExpress.XtraLayout.LayoutControlItem lay居住情况;
        private DevExpress.XtraEditors.TextEdit textEdit职工医疗保险卡号;
        private DevExpress.XtraEditors.TextEdit textEdit居民医疗保险卡号;
        private DevExpress.XtraEditors.DateEdit dateEdit签字时间;
        private DevExpress.XtraLayout.LayoutControlItem lbl本人或家属签字;
        private DevExpress.XtraLayout.LayoutControlItem lbl签字时间;
        private DevExpress.XtraEditors.TextEdit textEdit贫困救助卡号;
        private DevExpress.XtraEditors.CheckEdit checkEdit县域外流入;
        private DevExpress.XtraEditors.TextEdit txt所属片区;
        private DevExpress.XtraEditors.SimpleButton sbtnFingerPrint;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem55;
        private DevExpress.XtraEditors.PictureEdit textEdit本人或家属签字;
        private DevExpress.XtraEditors.CheckEdit ch复核标记;
        private DevExpress.XtraEditors.TextEdit txt复核备注;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem65;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem66;
        private DevExpress.XtraEditors.CheckEdit ch二次复核;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem67;
    }
}