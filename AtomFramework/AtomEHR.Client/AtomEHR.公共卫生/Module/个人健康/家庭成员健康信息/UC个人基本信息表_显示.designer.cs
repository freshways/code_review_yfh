﻿namespace AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息
{
    partial class UC个人基本信息表_显示
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC个人基本信息表_显示));
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btn新建家庭成员档案 = new DevExpress.XtraEditors.SimpleButton();
            this.btn修改 = new DevExpress.XtraEditors.SimpleButton();
            this.btn导出 = new DevExpress.XtraEditors.SimpleButton();
            this.btn查看照片 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.sBut_统合打印 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.ch二次复核 = new DevExpress.XtraEditors.CheckEdit();
            this.btn签约医生 = new DevExpress.XtraEditors.SimpleButton();
            this.ch复核标记 = new DevExpress.XtraEditors.CheckEdit();
            this.txt复核备注 = new DevExpress.XtraEditors.TextEdit();
            this.txt签约医生 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk医疗费用支付方式1 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit职工医疗保险卡号 = new DevExpress.XtraEditors.TextEdit();
            this.chk医疗费用支付方式2 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit居民医疗保险卡号 = new DevExpress.XtraEditors.TextEdit();
            this.chk医疗费用支付方式3 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit贫困救助卡号 = new DevExpress.XtraEditors.TextEdit();
            this.chk医疗费用支付方式4 = new DevExpress.XtraEditors.CheckEdit();
            this.chk医疗费用支付方式5 = new DevExpress.XtraEditors.CheckEdit();
            this.chk医疗费用支付方式6 = new DevExpress.XtraEditors.CheckEdit();
            this.chk医疗费用支付方式7 = new DevExpress.XtraEditors.CheckEdit();
            this.textEdit医疗费用支付方式其他 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住情况 = new DevExpress.XtraEditors.MemoEdit();
            this.txt家庭结构 = new DevExpress.XtraEditors.MemoEdit();
            this.txt家庭人口数 = new DevExpress.XtraEditors.MemoEdit();
            this.txt户主身份证号 = new DevExpress.XtraEditors.MemoEdit();
            this.txt户主姓名 = new DevExpress.XtraEditors.MemoEdit();
            this.txt档案位置 = new DevExpress.XtraEditors.TextEdit();
            this.flow外出 = new System.Windows.Forms.FlowLayoutPanel();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.txt外出地址 = new DevExpress.XtraEditors.TextEdit();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.chk是否流动人口 = new DevExpress.XtraEditors.CheckEdit();
            this.chk是否签约 = new DevExpress.XtraEditors.CheckEdit();
            this.chk是否贫困人口 = new DevExpress.XtraEditors.CheckEdit();
            this.txt输血 = new DevExpress.XtraEditors.MemoEdit();
            this.txt外伤 = new DevExpress.XtraEditors.MemoEdit();
            this.txt手术 = new DevExpress.XtraEditors.MemoEdit();
            this.txt疾病 = new DevExpress.XtraEditors.MemoEdit();
            this.txt孕次 = new DevExpress.XtraEditors.TextEdit();
            this.txt产次 = new DevExpress.XtraEditors.TextEdit();
            this.txt孕产情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt禽畜栏 = new DevExpress.XtraEditors.TextEdit();
            this.txt厕所 = new DevExpress.XtraEditors.TextEdit();
            this.txt饮水 = new DevExpress.XtraEditors.TextEdit();
            this.txt燃料类型 = new DevExpress.XtraEditors.TextEdit();
            this.txt厨房排风设施 = new DevExpress.XtraEditors.TextEdit();
            this.txt残疾情况 = new DevExpress.XtraEditors.TextEdit();
            this.txt遗传病史 = new DevExpress.XtraEditors.TextEdit();
            this.txt毒物 = new DevExpress.XtraEditors.TextEdit();
            this.txt化学品 = new DevExpress.XtraEditors.TextEdit();
            this.txt射线 = new DevExpress.XtraEditors.TextEdit();
            this.txt药物过敏史 = new DevExpress.XtraEditors.TextEdit();
            this.txt居住地址 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案类别 = new DevExpress.XtraEditors.TextEdit();
            this.txt新农合号 = new DevExpress.XtraEditors.TextEdit();
            this.txt所属片区 = new DevExpress.XtraEditors.TextEdit();
            this.txt医疗保险号 = new DevExpress.XtraEditors.TextEdit();
            this.txt医疗费用支付方式 = new DevExpress.XtraEditors.TextEdit();
            this.txt婚姻状况 = new DevExpress.XtraEditors.TextEdit();
            this.txt劳动强度 = new DevExpress.XtraEditors.TextEdit();
            this.txt职业 = new DevExpress.XtraEditors.TextEdit();
            this.txt文化程度 = new DevExpress.XtraEditors.TextEdit();
            this.txtRH = new DevExpress.XtraEditors.TextEdit();
            this.txt血型 = new DevExpress.XtraEditors.TextEdit();
            this.txt与户主关系 = new DevExpress.XtraEditors.TextEdit();
            this.txt民族 = new DevExpress.XtraEditors.TextEdit();
            this.txt常住类型 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系人电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt联系人姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt本人电话 = new DevExpress.XtraEditors.TextEdit();
            this.txt工作单位 = new DevExpress.XtraEditors.TextEdit();
            this.txt证件号码 = new DevExpress.XtraEditors.TextEdit();
            this.txt完整度 = new DevExpress.XtraEditors.TextEdit();
            this.txt缺项 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案状态 = new DevExpress.XtraEditors.TextEdit();
            this.txt出生日期 = new DevExpress.XtraEditors.TextEdit();
            this.txt性别 = new DevExpress.XtraEditors.TextEdit();
            this.txt姓名 = new DevExpress.XtraEditors.TextEdit();
            this.txt档案号 = new DevExpress.XtraEditors.TextEdit();
            this.txt家族史 = new DevExpress.XtraEditors.MemoEdit();
            this.dateEdit签字时间 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit本人或家属签字 = new DevExpress.XtraEditors.PictureEdit();
            this.lbl劳动强度 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl医疗费用支付方式 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl档案号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl姓名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl性别 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl出生日期 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl档案状态 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl缺项 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl完整度 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl证件号码 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl工作单位 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl联系人姓名 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl联系人电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl本人电话 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl常住类型 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl民族 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl与户主关系 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl血型 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lblRH = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl文化程度 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl职业 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl婚姻状况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl医疗保险号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl所属片区 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl新农合号 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl档案类别 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl居住地址 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl医疗支付方式 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem79 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.txt录入时间 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem修改时间 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem89 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.txt最近更新时间 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.txt录入人 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem85 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem91 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.txt当前所属机构 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl本人或家属签字 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl签字时间 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleLabelItem修改人 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.txt最近修改人 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.txt创建机构 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleLabelItem84 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.txt调查时间 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl厨房排风设施 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl燃料类型 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl饮水 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl厕所 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl禽畜栏 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem68 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl药物过敏史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl家族史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl射线 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl化学品 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl毒物 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl遗传病史 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl残疾情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl疾病 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl手术 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl外伤 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl输血 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem50 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.simpleLabelItem57 = new DevExpress.XtraLayout.SimpleLabelItem();
            this.simpleSeparator4 = new DevExpress.XtraLayout.SimpleSeparator();
            this.simpleSeparator5 = new DevExpress.XtraLayout.SimpleSeparator();
            this.group妇女 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lbl孕产情况 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl孕次 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lbl产次 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ch二次复核.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch复核标记.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt复核备注.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约医生.Properties)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit职工医疗保险卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居民医疗保险卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit贫困救助卡号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit医疗费用支付方式其他.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家庭结构.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家庭人口数.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt户主身份证号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt户主姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案位置.Properties)).BeginInit();
            this.flow外出.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外出地址.Properties)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否流动人口.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否签约.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否贫困人口.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt输血.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外伤.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt手术.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt疾病.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产次.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕产情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt禽畜栏.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt厕所.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮水.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt燃料类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt厨房排风设施.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt残疾情况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遗传病史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt毒物.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学品.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt射线.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物过敏史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案类别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新农合号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt所属片区.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医疗保险号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医疗费用支付方式.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt婚姻状况.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt劳动强度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt文化程度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt与户主关系.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt民族.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt常住类型.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本人电话.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt工作单位.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt证件号码.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt完整度.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺项.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案状态.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家族史.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit签字时间.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit本人或家属签字.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl劳动强度)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl医疗费用支付方式)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl档案号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl姓名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl性别)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl出生日期)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl档案状态)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl缺项)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl完整度)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl证件号码)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl工作单位)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl联系人姓名)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl联系人电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl本人电话)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl常住类型)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl民族)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl与户主关系)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血型)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl文化程度)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl婚姻状况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl医疗保险号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl所属片区)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl新农合号)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl档案类别)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl居住地址)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl医疗支付方式)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem79)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem修改时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem89)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem85)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem91)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl本人或家属签字)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl签字时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem修改人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem84)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt调查时间)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl厨房排风设施)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl燃料类型)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮水)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl厕所)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl禽畜栏)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl药物过敏史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家族史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl射线)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl化学品)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl毒物)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl遗传病史)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl残疾情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl疾病)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl手术)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl外伤)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl输血)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group妇女)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl孕产情况)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl孕次)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl产次)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFocusForSave
            // 
            this.txtFocusForSave.Margin = new System.Windows.Forms.Padding(4);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem2.Location = new System.Drawing.Point(362, 197);
            this.emptySpaceItem2.Name = "emptySpaceItem1";
            this.emptySpaceItem2.Size = new System.Drawing.Size(363, 24);
            this.emptySpaceItem2.Text = "emptySpaceItem1";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.flowLayoutPanel1);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(748, 41);
            this.panelControl2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.btn新建家庭成员档案);
            this.flowLayoutPanel1.Controls.Add(this.btn修改);
            this.flowLayoutPanel1.Controls.Add(this.btn导出);
            this.flowLayoutPanel1.Controls.Add(this.btn查看照片);
            this.flowLayoutPanel1.Controls.Add(this.simpleButton1);
            this.flowLayoutPanel1.Controls.Add(this.sBut_统合打印);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(744, 37);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // btn新建家庭成员档案
            // 
            this.btn新建家庭成员档案.Image = ((System.Drawing.Image)(resources.GetObject("btn新建家庭成员档案.Image")));
            this.btn新建家庭成员档案.Location = new System.Drawing.Point(3, 3);
            this.btn新建家庭成员档案.Name = "btn新建家庭成员档案";
            this.btn新建家庭成员档案.Size = new System.Drawing.Size(121, 30);
            this.btn新建家庭成员档案.TabIndex = 0;
            this.btn新建家庭成员档案.Text = "新建家庭成员档案";
            this.btn新建家庭成员档案.Click += new System.EventHandler(this.btn新建家庭成员档案_Click);
            // 
            // btn修改
            // 
            this.btn修改.Image = ((System.Drawing.Image)(resources.GetObject("btn修改.Image")));
            this.btn修改.Location = new System.Drawing.Point(130, 3);
            this.btn修改.Name = "btn修改";
            this.btn修改.Size = new System.Drawing.Size(58, 30);
            this.btn修改.TabIndex = 1;
            this.btn修改.Text = "修改";
            this.btn修改.Click += new System.EventHandler(this.btn修改_Click);
            // 
            // btn导出
            // 
            this.btn导出.Image = ((System.Drawing.Image)(resources.GetObject("btn导出.Image")));
            this.btn导出.Location = new System.Drawing.Point(194, 3);
            this.btn导出.Name = "btn导出";
            this.btn导出.Size = new System.Drawing.Size(50, 30);
            this.btn导出.TabIndex = 2;
            this.btn导出.Text = "导出";
            this.btn导出.Click += new System.EventHandler(this.btn导出_Click);
            // 
            // btn查看照片
            // 
            this.btn查看照片.Image = ((System.Drawing.Image)(resources.GetObject("btn查看照片.Image")));
            this.btn查看照片.Location = new System.Drawing.Point(250, 3);
            this.btn查看照片.Name = "btn查看照片";
            this.btn查看照片.Size = new System.Drawing.Size(100, 30);
            this.btn查看照片.TabIndex = 3;
            this.btn查看照片.Text = "查看照片";
            this.btn查看照片.Click += new System.EventHandler(this.btn查看照片_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(356, 3);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(100, 30);
            this.simpleButton1.TabIndex = 6;
            this.simpleButton1.Text = "计生调查问卷";
            this.simpleButton1.Visible = false;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // sBut_统合打印
            // 
            this.sBut_统合打印.Image = ((System.Drawing.Image)(resources.GetObject("sBut_统合打印.Image")));
            this.sBut_统合打印.Location = new System.Drawing.Point(462, 3);
            this.sBut_统合打印.Name = "sBut_统合打印";
            this.sBut_统合打印.Size = new System.Drawing.Size(86, 30);
            this.sBut_统合打印.TabIndex = 7;
            this.sBut_统合打印.Text = "综合打印";
            this.sBut_统合打印.Click += new System.EventHandler(this.sBut_统合打印_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.ch二次复核);
            this.layoutControl1.Controls.Add(this.btn签约医生);
            this.layoutControl1.Controls.Add(this.ch复核标记);
            this.layoutControl1.Controls.Add(this.txt复核备注);
            this.layoutControl1.Controls.Add(this.txt签约医生);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel3);
            this.layoutControl1.Controls.Add(this.txt居住情况);
            this.layoutControl1.Controls.Add(this.txt家庭结构);
            this.layoutControl1.Controls.Add(this.txt家庭人口数);
            this.layoutControl1.Controls.Add(this.txt户主身份证号);
            this.layoutControl1.Controls.Add(this.txt户主姓名);
            this.layoutControl1.Controls.Add(this.txt档案位置);
            this.layoutControl1.Controls.Add(this.flow外出);
            this.layoutControl1.Controls.Add(this.txt外出地址);
            this.layoutControl1.Controls.Add(this.flowLayoutPanel2);
            this.layoutControl1.Controls.Add(this.txt输血);
            this.layoutControl1.Controls.Add(this.txt外伤);
            this.layoutControl1.Controls.Add(this.txt手术);
            this.layoutControl1.Controls.Add(this.txt疾病);
            this.layoutControl1.Controls.Add(this.txt孕次);
            this.layoutControl1.Controls.Add(this.txt产次);
            this.layoutControl1.Controls.Add(this.txt孕产情况);
            this.layoutControl1.Controls.Add(this.txt禽畜栏);
            this.layoutControl1.Controls.Add(this.txt厕所);
            this.layoutControl1.Controls.Add(this.txt饮水);
            this.layoutControl1.Controls.Add(this.txt燃料类型);
            this.layoutControl1.Controls.Add(this.txt厨房排风设施);
            this.layoutControl1.Controls.Add(this.txt残疾情况);
            this.layoutControl1.Controls.Add(this.txt遗传病史);
            this.layoutControl1.Controls.Add(this.txt毒物);
            this.layoutControl1.Controls.Add(this.txt化学品);
            this.layoutControl1.Controls.Add(this.txt射线);
            this.layoutControl1.Controls.Add(this.txt药物过敏史);
            this.layoutControl1.Controls.Add(this.txt居住地址);
            this.layoutControl1.Controls.Add(this.txt档案类别);
            this.layoutControl1.Controls.Add(this.txt新农合号);
            this.layoutControl1.Controls.Add(this.txt所属片区);
            this.layoutControl1.Controls.Add(this.txt医疗保险号);
            this.layoutControl1.Controls.Add(this.txt医疗费用支付方式);
            this.layoutControl1.Controls.Add(this.txt婚姻状况);
            this.layoutControl1.Controls.Add(this.txt劳动强度);
            this.layoutControl1.Controls.Add(this.txt职业);
            this.layoutControl1.Controls.Add(this.txt文化程度);
            this.layoutControl1.Controls.Add(this.txtRH);
            this.layoutControl1.Controls.Add(this.txt血型);
            this.layoutControl1.Controls.Add(this.txt与户主关系);
            this.layoutControl1.Controls.Add(this.txt民族);
            this.layoutControl1.Controls.Add(this.txt常住类型);
            this.layoutControl1.Controls.Add(this.txt联系人电话);
            this.layoutControl1.Controls.Add(this.txt联系人姓名);
            this.layoutControl1.Controls.Add(this.txt本人电话);
            this.layoutControl1.Controls.Add(this.txt工作单位);
            this.layoutControl1.Controls.Add(this.txt证件号码);
            this.layoutControl1.Controls.Add(this.txt完整度);
            this.layoutControl1.Controls.Add(this.txt缺项);
            this.layoutControl1.Controls.Add(this.txt档案状态);
            this.layoutControl1.Controls.Add(this.txt出生日期);
            this.layoutControl1.Controls.Add(this.txt性别);
            this.layoutControl1.Controls.Add(this.txt姓名);
            this.layoutControl1.Controls.Add(this.txt档案号);
            this.layoutControl1.Controls.Add(this.txt家族史);
            this.layoutControl1.Controls.Add(this.dateEdit签字时间);
            this.layoutControl1.Controls.Add(this.textEdit本人或家属签字);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl劳动强度,
            this.lbl医疗费用支付方式});
            this.layoutControl1.Location = new System.Drawing.Point(0, 41);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(873, 319, 250, 350);
            this.layoutControl1.OptionsView.ItemBorderColor = System.Drawing.Color.DarkRed;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(748, 457);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // ch二次复核
            // 
            this.ch二次复核.Enabled = false;
            this.ch二次复核.Location = new System.Drawing.Point(582, 992);
            this.ch二次复核.Name = "ch二次复核";
            this.ch二次复核.Properties.Caption = "二次复核";
            this.ch二次复核.Size = new System.Drawing.Size(146, 19);
            this.ch二次复核.StyleController = this.layoutControl1;
            this.ch二次复核.TabIndex = 78;
            // 
            // btn签约医生
            // 
            this.btn签约医生.Image = ((System.Drawing.Image)(resources.GetObject("btn签约医生.Image")));
            this.btn签约医生.Location = new System.Drawing.Point(350, 101);
            this.btn签约医生.Name = "btn签约医生";
            this.btn签约医生.Size = new System.Drawing.Size(102, 22);
            this.btn签约医生.StyleController = this.layoutControl1;
            this.btn签约医生.TabIndex = 77;
            this.btn签约医生.Text = "查看签约医生";
            this.btn签约医生.Click += new System.EventHandler(this.btn签约医生_Click);
            // 
            // ch复核标记
            // 
            this.ch复核标记.Enabled = false;
            this.ch复核标记.Location = new System.Drawing.Point(432, 992);
            this.ch复核标记.Name = "ch复核标记";
            this.ch复核标记.Properties.Caption = "  复核标记";
            this.ch复核标记.Size = new System.Drawing.Size(146, 19);
            this.ch复核标记.StyleController = this.layoutControl1;
            this.ch复核标记.TabIndex = 76;
            // 
            // txt复核备注
            // 
            this.txt复核备注.Location = new System.Drawing.Point(549, 968);
            this.txt复核备注.Name = "txt复核备注";
            this.txt复核备注.Properties.ReadOnly = true;
            this.txt复核备注.Size = new System.Drawing.Size(179, 20);
            this.txt复核备注.StyleController = this.layoutControl1;
            this.txt复核备注.TabIndex = 75;
            // 
            // txt签约医生
            // 
            this.txt签约医生.Location = new System.Drawing.Point(118, 101);
            this.txt签约医生.Name = "txt签约医生";
            this.txt签约医生.Size = new System.Drawing.Size(228, 20);
            this.txt签约医生.StyleController = this.layoutControl1;
            this.txt签约医生.TabIndex = 74;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.chk医疗费用支付方式1);
            this.flowLayoutPanel3.Controls.Add(this.textEdit职工医疗保险卡号);
            this.flowLayoutPanel3.Controls.Add(this.chk医疗费用支付方式2);
            this.flowLayoutPanel3.Controls.Add(this.textEdit居民医疗保险卡号);
            this.flowLayoutPanel3.Controls.Add(this.chk医疗费用支付方式3);
            this.flowLayoutPanel3.Controls.Add(this.textEdit贫困救助卡号);
            this.flowLayoutPanel3.Controls.Add(this.chk医疗费用支付方式4);
            this.flowLayoutPanel3.Controls.Add(this.chk医疗费用支付方式5);
            this.flowLayoutPanel3.Controls.Add(this.chk医疗费用支付方式6);
            this.flowLayoutPanel3.Controls.Add(this.chk医疗费用支付方式7);
            this.flowLayoutPanel3.Controls.Add(this.textEdit医疗费用支付方式其他);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(118, 295);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(610, 86);
            this.flowLayoutPanel3.TabIndex = 72;
            // 
            // chk医疗费用支付方式1
            // 
            this.chk医疗费用支付方式1.Location = new System.Drawing.Point(3, 3);
            this.chk医疗费用支付方式1.Name = "chk医疗费用支付方式1";
            this.chk医疗费用支付方式1.Properties.Caption = "城镇或省直职工基本医疗保险";
            this.chk医疗费用支付方式1.Properties.ReadOnly = true;
            this.chk医疗费用支付方式1.Size = new System.Drawing.Size(189, 19);
            this.chk医疗费用支付方式1.TabIndex = 0;
            this.chk医疗费用支付方式1.Tag = "3";
            // 
            // textEdit职工医疗保险卡号
            // 
            this.textEdit职工医疗保险卡号.Location = new System.Drawing.Point(195, 3);
            this.textEdit职工医疗保险卡号.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.textEdit职工医疗保险卡号.Name = "textEdit职工医疗保险卡号";
            this.textEdit职工医疗保险卡号.Properties.ReadOnly = true;
            this.textEdit职工医疗保险卡号.Size = new System.Drawing.Size(123, 20);
            this.textEdit职工医疗保险卡号.TabIndex = 7;
            // 
            // chk医疗费用支付方式2
            // 
            this.chk医疗费用支付方式2.Location = new System.Drawing.Point(321, 3);
            this.chk医疗费用支付方式2.Name = "chk医疗费用支付方式2";
            this.chk医疗费用支付方式2.Properties.Caption = "居民基本医疗保险";
            this.chk医疗费用支付方式2.Properties.ReadOnly = true;
            this.chk医疗费用支付方式2.Size = new System.Drawing.Size(121, 19);
            this.chk医疗费用支付方式2.TabIndex = 1;
            this.chk医疗费用支付方式2.Tag = "4";
            // 
            // textEdit居民医疗保险卡号
            // 
            this.textEdit居民医疗保险卡号.Location = new System.Drawing.Point(445, 3);
            this.textEdit居民医疗保险卡号.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.textEdit居民医疗保险卡号.Name = "textEdit居民医疗保险卡号";
            this.textEdit居民医疗保险卡号.Properties.ReadOnly = true;
            this.textEdit居民医疗保险卡号.Size = new System.Drawing.Size(108, 20);
            this.textEdit居民医疗保险卡号.TabIndex = 8;
            // 
            // chk医疗费用支付方式3
            // 
            this.chk医疗费用支付方式3.Location = new System.Drawing.Point(3, 29);
            this.chk医疗费用支付方式3.Name = "chk医疗费用支付方式3";
            this.chk医疗费用支付方式3.Properties.Caption = "贫困救助";
            this.chk医疗费用支付方式3.Properties.ReadOnly = true;
            this.chk医疗费用支付方式3.Size = new System.Drawing.Size(75, 19);
            this.chk医疗费用支付方式3.TabIndex = 2;
            this.chk医疗费用支付方式3.Tag = "8";
            // 
            // textEdit贫困救助卡号
            // 
            this.textEdit贫困救助卡号.Location = new System.Drawing.Point(81, 29);
            this.textEdit贫困救助卡号.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.textEdit贫困救助卡号.Name = "textEdit贫困救助卡号";
            this.textEdit贫困救助卡号.Properties.ReadOnly = true;
            this.textEdit贫困救助卡号.Size = new System.Drawing.Size(105, 20);
            this.textEdit贫困救助卡号.TabIndex = 9;
            // 
            // chk医疗费用支付方式4
            // 
            this.chk医疗费用支付方式4.Location = new System.Drawing.Point(192, 29);
            this.chk医疗费用支付方式4.Name = "chk医疗费用支付方式4";
            this.chk医疗费用支付方式4.Properties.Caption = "商业医疗保险";
            this.chk医疗费用支付方式4.Properties.ReadOnly = true;
            this.chk医疗费用支付方式4.Size = new System.Drawing.Size(104, 19);
            this.chk医疗费用支付方式4.TabIndex = 3;
            this.chk医疗费用支付方式4.Tag = "7";
            // 
            // chk医疗费用支付方式5
            // 
            this.chk医疗费用支付方式5.Location = new System.Drawing.Point(302, 29);
            this.chk医疗费用支付方式5.Name = "chk医疗费用支付方式5";
            this.chk医疗费用支付方式5.Properties.Caption = "全公费";
            this.chk医疗费用支付方式5.Properties.ReadOnly = true;
            this.chk医疗费用支付方式5.Size = new System.Drawing.Size(97, 19);
            this.chk医疗费用支付方式5.TabIndex = 4;
            this.chk医疗费用支付方式5.Tag = "2";
            // 
            // chk医疗费用支付方式6
            // 
            this.chk医疗费用支付方式6.Location = new System.Drawing.Point(405, 29);
            this.chk医疗费用支付方式6.Name = "chk医疗费用支付方式6";
            this.chk医疗费用支付方式6.Properties.Caption = "全自费";
            this.chk医疗费用支付方式6.Properties.ReadOnly = true;
            this.chk医疗费用支付方式6.Size = new System.Drawing.Size(111, 19);
            this.chk医疗费用支付方式6.TabIndex = 5;
            this.chk医疗费用支付方式6.Tag = "1";
            // 
            // chk医疗费用支付方式7
            // 
            this.chk医疗费用支付方式7.Location = new System.Drawing.Point(522, 29);
            this.chk医疗费用支付方式7.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.chk医疗费用支付方式7.Name = "chk医疗费用支付方式7";
            this.chk医疗费用支付方式7.Properties.Caption = "其他：";
            this.chk医疗费用支付方式7.Properties.ReadOnly = true;
            this.chk医疗费用支付方式7.Size = new System.Drawing.Size(75, 19);
            this.chk医疗费用支付方式7.TabIndex = 6;
            this.chk医疗费用支付方式7.Tag = "99";
            // 
            // textEdit医疗费用支付方式其他
            // 
            this.textEdit医疗费用支付方式其他.Location = new System.Drawing.Point(3, 55);
            this.textEdit医疗费用支付方式其他.Name = "textEdit医疗费用支付方式其他";
            this.textEdit医疗费用支付方式其他.Properties.ReadOnly = true;
            this.textEdit医疗费用支付方式其他.Size = new System.Drawing.Size(102, 20);
            this.textEdit医疗费用支付方式其他.TabIndex = 10;
            // 
            // txt居住情况
            // 
            this.txt居住情况.Location = new System.Drawing.Point(325, 767);
            this.txt居住情况.Name = "txt居住情况";
            this.txt居住情况.Size = new System.Drawing.Size(403, 20);
            this.txt居住情况.StyleController = this.layoutControl1;
            this.txt居住情况.TabIndex = 69;
            this.txt居住情况.UseOptimizedRendering = true;
            // 
            // txt家庭结构
            // 
            this.txt家庭结构.Location = new System.Drawing.Point(118, 767);
            this.txt家庭结构.Name = "txt家庭结构";
            this.txt家庭结构.Size = new System.Drawing.Size(108, 20);
            this.txt家庭结构.StyleController = this.layoutControl1;
            this.txt家庭结构.TabIndex = 68;
            this.txt家庭结构.UseOptimizedRendering = true;
            // 
            // txt家庭人口数
            // 
            this.txt家庭人口数.Location = new System.Drawing.Point(618, 743);
            this.txt家庭人口数.Name = "txt家庭人口数";
            this.txt家庭人口数.Size = new System.Drawing.Size(110, 20);
            this.txt家庭人口数.StyleController = this.layoutControl1;
            this.txt家庭人口数.TabIndex = 67;
            this.txt家庭人口数.UseOptimizedRendering = true;
            // 
            // txt户主身份证号
            // 
            this.txt户主身份证号.Location = new System.Drawing.Point(325, 743);
            this.txt户主身份证号.Name = "txt户主身份证号";
            this.txt户主身份证号.Size = new System.Drawing.Size(194, 20);
            this.txt户主身份证号.StyleController = this.layoutControl1;
            this.txt户主身份证号.TabIndex = 66;
            this.txt户主身份证号.UseOptimizedRendering = true;
            // 
            // txt户主姓名
            // 
            this.txt户主姓名.Location = new System.Drawing.Point(118, 743);
            this.txt户主姓名.Name = "txt户主姓名";
            this.txt户主姓名.Size = new System.Drawing.Size(108, 20);
            this.txt户主姓名.StyleController = this.layoutControl1;
            this.txt户主姓名.TabIndex = 65;
            this.txt户主姓名.UseOptimizedRendering = true;
            // 
            // txt档案位置
            // 
            this.txt档案位置.Location = new System.Drawing.Point(487, 75);
            this.txt档案位置.Margin = new System.Windows.Forms.Padding(2);
            this.txt档案位置.Name = "txt档案位置";
            this.txt档案位置.Size = new System.Drawing.Size(241, 20);
            this.txt档案位置.StyleController = this.layoutControl1;
            this.txt档案位置.TabIndex = 57;
            // 
            // flow外出
            // 
            this.flow外出.Controls.Add(this.checkEdit3);
            this.flow外出.Controls.Add(this.checkEdit1);
            this.flow外出.Controls.Add(this.checkEdit2);
            this.flow外出.Location = new System.Drawing.Point(98, 918);
            this.flow外出.Margin = new System.Windows.Forms.Padding(2);
            this.flow外出.Name = "flow外出";
            this.flow外出.Size = new System.Drawing.Size(369, 20);
            this.flow外出.TabIndex = 56;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Enabled = false;
            this.checkEdit3.Location = new System.Drawing.Point(0, 0);
            this.checkEdit3.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "县域外流入";
            this.checkEdit3.Properties.RadioGroupIndex = 0;
            this.checkEdit3.Size = new System.Drawing.Size(91, 19);
            this.checkEdit3.TabIndex = 4;
            this.checkEdit3.TabStop = false;
            this.checkEdit3.Tag = "0";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Enabled = false;
            this.checkEdit1.Location = new System.Drawing.Point(91, 0);
            this.checkEdit1.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "县域内流出";
            this.checkEdit1.Size = new System.Drawing.Size(92, 19);
            this.checkEdit1.TabIndex = 0;
            this.checkEdit1.Tag = "1";
            // 
            // checkEdit2
            // 
            this.checkEdit2.Enabled = false;
            this.checkEdit2.Location = new System.Drawing.Point(183, 0);
            this.checkEdit2.Margin = new System.Windows.Forms.Padding(0);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "县域外流出";
            this.checkEdit2.Size = new System.Drawing.Size(92, 19);
            this.checkEdit2.TabIndex = 0;
            this.checkEdit2.Tag = "2";
            // 
            // txt外出地址
            // 
            this.txt外出地址.Location = new System.Drawing.Point(588, 918);
            this.txt外出地址.Margin = new System.Windows.Forms.Padding(2);
            this.txt外出地址.Name = "txt外出地址";
            this.txt外出地址.Size = new System.Drawing.Size(140, 20);
            this.txt外出地址.StyleController = this.layoutControl1;
            this.txt外出地址.TabIndex = 55;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.flowLayoutPanel2.Controls.Add(this.chk是否流动人口);
            this.flowLayoutPanel2.Controls.Add(this.chk是否签约);
            this.flowLayoutPanel2.Controls.Add(this.chk是否贫困人口);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(436, 49);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(292, 22);
            this.flowLayoutPanel2.TabIndex = 54;
            // 
            // chk是否流动人口
            // 
            this.chk是否流动人口.Location = new System.Drawing.Point(0, 0);
            this.chk是否流动人口.Margin = new System.Windows.Forms.Padding(0);
            this.chk是否流动人口.Name = "chk是否流动人口";
            this.chk是否流动人口.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.chk是否流动人口.Properties.Appearance.Options.UseFont = true;
            this.chk是否流动人口.Properties.AutoHeight = false;
            this.chk是否流动人口.Properties.Caption = "是否流动人口";
            this.chk是否流动人口.Properties.ReadOnly = true;
            this.chk是否流动人口.Size = new System.Drawing.Size(102, 19);
            this.chk是否流动人口.TabIndex = 0;
            // 
            // chk是否签约
            // 
            this.chk是否签约.Location = new System.Drawing.Point(102, 0);
            this.chk是否签约.Margin = new System.Windows.Forms.Padding(0);
            this.chk是否签约.Name = "chk是否签约";
            this.chk是否签约.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.chk是否签约.Properties.Appearance.Options.UseFont = true;
            this.chk是否签约.Properties.AutoHeight = false;
            this.chk是否签约.Properties.Caption = "是否签约服务";
            this.chk是否签约.Properties.ReadOnly = true;
            this.chk是否签约.Size = new System.Drawing.Size(107, 19);
            this.chk是否签约.TabIndex = 1;
            // 
            // chk是否贫困人口
            // 
            this.chk是否贫困人口.Location = new System.Drawing.Point(0, 19);
            this.chk是否贫困人口.Margin = new System.Windows.Forms.Padding(0);
            this.chk是否贫困人口.Name = "chk是否贫困人口";
            this.chk是否贫困人口.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.chk是否贫困人口.Properties.Appearance.Options.UseFont = true;
            this.chk是否贫困人口.Properties.AutoHeight = false;
            this.chk是否贫困人口.Properties.Caption = "是否贫困人口";
            this.chk是否贫困人口.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.chk是否贫困人口.Properties.ReadOnly = true;
            this.chk是否贫困人口.Size = new System.Drawing.Size(102, 19);
            this.chk是否贫困人口.TabIndex = 2;
            // 
            // txt输血
            // 
            this.txt输血.Location = new System.Drawing.Point(112, 555);
            this.txt输血.Name = "txt输血";
            this.txt输血.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt输血.Properties.Appearance.Options.UseBackColor = true;
            this.txt输血.Properties.ReadOnly = true;
            this.txt输血.Size = new System.Drawing.Size(616, 20);
            this.txt输血.StyleController = this.layoutControl1;
            this.txt输血.TabIndex = 53;
            this.txt输血.UseOptimizedRendering = true;
            // 
            // txt外伤
            // 
            this.txt外伤.Location = new System.Drawing.Point(112, 531);
            this.txt外伤.Name = "txt外伤";
            this.txt外伤.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt外伤.Properties.Appearance.Options.UseBackColor = true;
            this.txt外伤.Properties.ReadOnly = true;
            this.txt外伤.Size = new System.Drawing.Size(616, 20);
            this.txt外伤.StyleController = this.layoutControl1;
            this.txt外伤.TabIndex = 52;
            this.txt外伤.UseOptimizedRendering = true;
            // 
            // txt手术
            // 
            this.txt手术.Location = new System.Drawing.Point(112, 507);
            this.txt手术.Name = "txt手术";
            this.txt手术.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt手术.Properties.Appearance.Options.UseBackColor = true;
            this.txt手术.Properties.ReadOnly = true;
            this.txt手术.Size = new System.Drawing.Size(616, 20);
            this.txt手术.StyleController = this.layoutControl1;
            this.txt手术.TabIndex = 51;
            this.txt手术.UseOptimizedRendering = true;
            // 
            // txt疾病
            // 
            this.txt疾病.Location = new System.Drawing.Point(112, 483);
            this.txt疾病.Name = "txt疾病";
            this.txt疾病.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt疾病.Properties.Appearance.Options.UseBackColor = true;
            this.txt疾病.Properties.ReadOnly = true;
            this.txt疾病.Size = new System.Drawing.Size(616, 20);
            this.txt疾病.StyleController = this.layoutControl1;
            this.txt疾病.TabIndex = 50;
            this.txt疾病.UseOptimizedRendering = true;
            // 
            // txt孕次
            // 
            this.txt孕次.Location = new System.Drawing.Point(372, 697);
            this.txt孕次.Name = "txt孕次";
            this.txt孕次.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt孕次.Properties.Appearance.Options.UseBackColor = true;
            this.txt孕次.Properties.ReadOnly = true;
            this.txt孕次.Size = new System.Drawing.Size(156, 20);
            this.txt孕次.StyleController = this.layoutControl1;
            this.txt孕次.TabIndex = 49;
            // 
            // txt产次
            // 
            this.txt产次.Location = new System.Drawing.Point(617, 697);
            this.txt产次.Name = "txt产次";
            this.txt产次.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt产次.Properties.Appearance.Options.UseBackColor = true;
            this.txt产次.Properties.ReadOnly = true;
            this.txt产次.Size = new System.Drawing.Size(111, 20);
            this.txt产次.StyleController = this.layoutControl1;
            this.txt产次.TabIndex = 48;
            // 
            // txt孕产情况
            // 
            this.txt孕产情况.Location = new System.Drawing.Point(118, 697);
            this.txt孕产情况.Name = "txt孕产情况";
            this.txt孕产情况.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt孕产情况.Properties.Appearance.Options.UseBackColor = true;
            this.txt孕产情况.Properties.ReadOnly = true;
            this.txt孕产情况.Size = new System.Drawing.Size(165, 20);
            this.txt孕产情况.StyleController = this.layoutControl1;
            this.txt孕产情况.TabIndex = 47;
            // 
            // txt禽畜栏
            // 
            this.txt禽畜栏.Location = new System.Drawing.Point(120, 894);
            this.txt禽畜栏.Name = "txt禽畜栏";
            this.txt禽畜栏.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt禽畜栏.Properties.Appearance.Options.UseBackColor = true;
            this.txt禽畜栏.Properties.ReadOnly = true;
            this.txt禽畜栏.Size = new System.Drawing.Size(608, 20);
            this.txt禽畜栏.StyleController = this.layoutControl1;
            this.txt禽畜栏.TabIndex = 46;
            // 
            // txt厕所
            // 
            this.txt厕所.Location = new System.Drawing.Point(120, 870);
            this.txt厕所.Name = "txt厕所";
            this.txt厕所.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt厕所.Properties.Appearance.Options.UseBackColor = true;
            this.txt厕所.Properties.ReadOnly = true;
            this.txt厕所.Size = new System.Drawing.Size(608, 20);
            this.txt厕所.StyleController = this.layoutControl1;
            this.txt厕所.TabIndex = 45;
            // 
            // txt饮水
            // 
            this.txt饮水.Location = new System.Drawing.Point(120, 846);
            this.txt饮水.Name = "txt饮水";
            this.txt饮水.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt饮水.Properties.Appearance.Options.UseBackColor = true;
            this.txt饮水.Properties.ReadOnly = true;
            this.txt饮水.Size = new System.Drawing.Size(608, 20);
            this.txt饮水.StyleController = this.layoutControl1;
            this.txt饮水.TabIndex = 44;
            // 
            // txt燃料类型
            // 
            this.txt燃料类型.Location = new System.Drawing.Point(120, 822);
            this.txt燃料类型.Name = "txt燃料类型";
            this.txt燃料类型.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt燃料类型.Properties.Appearance.Options.UseBackColor = true;
            this.txt燃料类型.Properties.ReadOnly = true;
            this.txt燃料类型.Size = new System.Drawing.Size(608, 20);
            this.txt燃料类型.StyleController = this.layoutControl1;
            this.txt燃料类型.TabIndex = 43;
            // 
            // txt厨房排风设施
            // 
            this.txt厨房排风设施.Location = new System.Drawing.Point(120, 798);
            this.txt厨房排风设施.Name = "txt厨房排风设施";
            this.txt厨房排风设施.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt厨房排风设施.Properties.Appearance.Options.UseBackColor = true;
            this.txt厨房排风设施.Properties.ReadOnly = true;
            this.txt厨房排风设施.Size = new System.Drawing.Size(608, 20);
            this.txt厨房排风设施.StyleController = this.layoutControl1;
            this.txt厨房排风设施.TabIndex = 42;
            // 
            // txt残疾情况
            // 
            this.txt残疾情况.Location = new System.Drawing.Point(118, 673);
            this.txt残疾情况.Name = "txt残疾情况";
            this.txt残疾情况.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt残疾情况.Properties.Appearance.Options.UseBackColor = true;
            this.txt残疾情况.Properties.ReadOnly = true;
            this.txt残疾情况.Size = new System.Drawing.Size(610, 20);
            this.txt残疾情况.StyleController = this.layoutControl1;
            this.txt残疾情况.TabIndex = 41;
            // 
            // txt遗传病史
            // 
            this.txt遗传病史.Location = new System.Drawing.Point(118, 649);
            this.txt遗传病史.Name = "txt遗传病史";
            this.txt遗传病史.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt遗传病史.Properties.Appearance.Options.UseBackColor = true;
            this.txt遗传病史.Properties.ReadOnly = true;
            this.txt遗传病史.Size = new System.Drawing.Size(610, 20);
            this.txt遗传病史.StyleController = this.layoutControl1;
            this.txt遗传病史.TabIndex = 40;
            // 
            // txt毒物
            // 
            this.txt毒物.Location = new System.Drawing.Point(317, 625);
            this.txt毒物.Name = "txt毒物";
            this.txt毒物.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt毒物.Properties.Appearance.Options.UseBackColor = true;
            this.txt毒物.Properties.ReadOnly = true;
            this.txt毒物.Size = new System.Drawing.Size(118, 20);
            this.txt毒物.StyleController = this.layoutControl1;
            this.txt毒物.TabIndex = 39;
            // 
            // txt化学品
            // 
            this.txt化学品.Location = new System.Drawing.Point(118, 625);
            this.txt化学品.Name = "txt化学品";
            this.txt化学品.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt化学品.Properties.Appearance.Options.UseBackColor = true;
            this.txt化学品.Properties.ReadOnly = true;
            this.txt化学品.Size = new System.Drawing.Size(110, 20);
            this.txt化学品.StyleController = this.layoutControl1;
            this.txt化学品.TabIndex = 38;
            // 
            // txt射线
            // 
            this.txt射线.Location = new System.Drawing.Point(524, 625);
            this.txt射线.Name = "txt射线";
            this.txt射线.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt射线.Properties.Appearance.Options.UseBackColor = true;
            this.txt射线.Properties.ReadOnly = true;
            this.txt射线.Size = new System.Drawing.Size(204, 20);
            this.txt射线.StyleController = this.layoutControl1;
            this.txt射线.TabIndex = 37;
            // 
            // txt药物过敏史
            // 
            this.txt药物过敏史.Location = new System.Drawing.Point(118, 457);
            this.txt药物过敏史.Name = "txt药物过敏史";
            this.txt药物过敏史.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt药物过敏史.Properties.Appearance.Options.UseBackColor = true;
            this.txt药物过敏史.Properties.ReadOnly = true;
            this.txt药物过敏史.Size = new System.Drawing.Size(610, 20);
            this.txt药物过敏史.StyleController = this.layoutControl1;
            this.txt药物过敏史.TabIndex = 35;
            // 
            // txt居住地址
            // 
            this.txt居住地址.Location = new System.Drawing.Point(118, 433);
            this.txt居住地址.Name = "txt居住地址";
            this.txt居住地址.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt居住地址.Properties.Appearance.Options.UseBackColor = true;
            this.txt居住地址.Properties.ReadOnly = true;
            this.txt居住地址.Size = new System.Drawing.Size(610, 20);
            this.txt居住地址.StyleController = this.layoutControl1;
            this.txt居住地址.TabIndex = 34;
            // 
            // txt档案类别
            // 
            this.txt档案类别.Location = new System.Drawing.Point(397, 409);
            this.txt档案类别.Name = "txt档案类别";
            this.txt档案类别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt档案类别.Properties.Appearance.Options.UseBackColor = true;
            this.txt档案类别.Properties.ReadOnly = true;
            this.txt档案类别.Size = new System.Drawing.Size(331, 20);
            this.txt档案类别.StyleController = this.layoutControl1;
            this.txt档案类别.TabIndex = 33;
            // 
            // txt新农合号
            // 
            this.txt新农合号.Location = new System.Drawing.Point(397, 385);
            this.txt新农合号.Name = "txt新农合号";
            this.txt新农合号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt新农合号.Properties.Appearance.Options.UseBackColor = true;
            this.txt新农合号.Properties.ReadOnly = true;
            this.txt新农合号.Size = new System.Drawing.Size(331, 20);
            this.txt新农合号.StyleController = this.layoutControl1;
            this.txt新农合号.TabIndex = 32;
            // 
            // txt所属片区
            // 
            this.txt所属片区.EditValue = "";
            this.txt所属片区.Location = new System.Drawing.Point(118, 409);
            this.txt所属片区.Name = "txt所属片区";
            this.txt所属片区.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt所属片区.Properties.Appearance.Options.UseBackColor = true;
            this.txt所属片区.Properties.ReadOnly = true;
            this.txt所属片区.Size = new System.Drawing.Size(170, 20);
            this.txt所属片区.StyleController = this.layoutControl1;
            this.txt所属片区.TabIndex = 31;
            // 
            // txt医疗保险号
            // 
            this.txt医疗保险号.Location = new System.Drawing.Point(118, 385);
            this.txt医疗保险号.Name = "txt医疗保险号";
            this.txt医疗保险号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt医疗保险号.Properties.Appearance.Options.UseBackColor = true;
            this.txt医疗保险号.Properties.ReadOnly = true;
            this.txt医疗保险号.Size = new System.Drawing.Size(170, 20);
            this.txt医疗保险号.StyleController = this.layoutControl1;
            this.txt医疗保险号.TabIndex = 30;
            // 
            // txt医疗费用支付方式
            // 
            this.txt医疗费用支付方式.Location = new System.Drawing.Point(118, 359);
            this.txt医疗费用支付方式.Name = "txt医疗费用支付方式";
            this.txt医疗费用支付方式.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt医疗费用支付方式.Properties.Appearance.Options.UseBackColor = true;
            this.txt医疗费用支付方式.Properties.ReadOnly = true;
            this.txt医疗费用支付方式.Size = new System.Drawing.Size(610, 20);
            this.txt医疗费用支付方式.StyleController = this.layoutControl1;
            this.txt医疗费用支付方式.TabIndex = 29;
            // 
            // txt婚姻状况
            // 
            this.txt婚姻状况.Location = new System.Drawing.Point(118, 271);
            this.txt婚姻状况.Name = "txt婚姻状况";
            this.txt婚姻状况.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt婚姻状况.Properties.Appearance.Options.UseBackColor = true;
            this.txt婚姻状况.Properties.ReadOnly = true;
            this.txt婚姻状况.Size = new System.Drawing.Size(610, 20);
            this.txt婚姻状况.StyleController = this.layoutControl1;
            this.txt婚姻状况.TabIndex = 28;
            // 
            // txt劳动强度
            // 
            this.txt劳动强度.Location = new System.Drawing.Point(118, 245);
            this.txt劳动强度.Name = "txt劳动强度";
            this.txt劳动强度.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt劳动强度.Properties.Appearance.Options.UseBackColor = true;
            this.txt劳动强度.Properties.ReadOnly = true;
            this.txt劳动强度.Size = new System.Drawing.Size(610, 20);
            this.txt劳动强度.StyleController = this.layoutControl1;
            this.txt劳动强度.TabIndex = 27;
            this.txt劳动强度.Visible = false;
            // 
            // txt职业
            // 
            this.txt职业.Location = new System.Drawing.Point(118, 247);
            this.txt职业.Name = "txt职业";
            this.txt职业.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt职业.Properties.Appearance.Options.UseBackColor = true;
            this.txt职业.Properties.ReadOnly = true;
            this.txt职业.Size = new System.Drawing.Size(610, 20);
            this.txt职业.StyleController = this.layoutControl1;
            this.txt职业.TabIndex = 26;
            // 
            // txt文化程度
            // 
            this.txt文化程度.Location = new System.Drawing.Point(118, 223);
            this.txt文化程度.Name = "txt文化程度";
            this.txt文化程度.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt文化程度.Properties.Appearance.Options.UseBackColor = true;
            this.txt文化程度.Properties.ReadOnly = true;
            this.txt文化程度.Size = new System.Drawing.Size(610, 20);
            this.txt文化程度.StyleController = this.layoutControl1;
            this.txt文化程度.TabIndex = 25;
            // 
            // txtRH
            // 
            this.txtRH.Location = new System.Drawing.Point(369, 199);
            this.txtRH.Name = "txtRH";
            this.txtRH.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txtRH.Properties.Appearance.Options.UseBackColor = true;
            this.txtRH.Properties.ReadOnly = true;
            this.txtRH.Size = new System.Drawing.Size(359, 20);
            this.txtRH.StyleController = this.layoutControl1;
            this.txtRH.TabIndex = 24;
            // 
            // txt血型
            // 
            this.txt血型.Location = new System.Drawing.Point(118, 199);
            this.txt血型.Name = "txt血型";
            this.txt血型.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt血型.Properties.Appearance.Options.UseBackColor = true;
            this.txt血型.Properties.ReadOnly = true;
            this.txt血型.Size = new System.Drawing.Size(142, 20);
            this.txt血型.StyleController = this.layoutControl1;
            this.txt血型.TabIndex = 23;
            // 
            // txt与户主关系
            // 
            this.txt与户主关系.Location = new System.Drawing.Point(560, 175);
            this.txt与户主关系.Name = "txt与户主关系";
            this.txt与户主关系.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt与户主关系.Properties.Appearance.Options.UseBackColor = true;
            this.txt与户主关系.Properties.ReadOnly = true;
            this.txt与户主关系.Size = new System.Drawing.Size(168, 20);
            this.txt与户主关系.StyleController = this.layoutControl1;
            this.txt与户主关系.TabIndex = 22;
            // 
            // txt民族
            // 
            this.txt民族.Location = new System.Drawing.Point(369, 175);
            this.txt民族.Name = "txt民族";
            this.txt民族.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt民族.Properties.Appearance.Options.UseBackColor = true;
            this.txt民族.Properties.ReadOnly = true;
            this.txt民族.Size = new System.Drawing.Size(82, 20);
            this.txt民族.StyleController = this.layoutControl1;
            this.txt民族.TabIndex = 21;
            // 
            // txt常住类型
            // 
            this.txt常住类型.Location = new System.Drawing.Point(118, 175);
            this.txt常住类型.Name = "txt常住类型";
            this.txt常住类型.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt常住类型.Properties.Appearance.Options.UseBackColor = true;
            this.txt常住类型.Properties.ReadOnly = true;
            this.txt常住类型.Size = new System.Drawing.Size(142, 20);
            this.txt常住类型.StyleController = this.layoutControl1;
            this.txt常住类型.TabIndex = 20;
            // 
            // txt联系人电话
            // 
            this.txt联系人电话.Location = new System.Drawing.Point(560, 151);
            this.txt联系人电话.Name = "txt联系人电话";
            this.txt联系人电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系人电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系人电话.Properties.ReadOnly = true;
            this.txt联系人电话.Size = new System.Drawing.Size(168, 20);
            this.txt联系人电话.StyleController = this.layoutControl1;
            this.txt联系人电话.TabIndex = 19;
            // 
            // txt联系人姓名
            // 
            this.txt联系人姓名.Location = new System.Drawing.Point(369, 151);
            this.txt联系人姓名.Name = "txt联系人姓名";
            this.txt联系人姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt联系人姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt联系人姓名.Properties.ReadOnly = true;
            this.txt联系人姓名.Size = new System.Drawing.Size(82, 20);
            this.txt联系人姓名.StyleController = this.layoutControl1;
            this.txt联系人姓名.TabIndex = 18;
            // 
            // txt本人电话
            // 
            this.txt本人电话.Location = new System.Drawing.Point(118, 151);
            this.txt本人电话.Name = "txt本人电话";
            this.txt本人电话.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt本人电话.Properties.Appearance.Options.UseBackColor = true;
            this.txt本人电话.Properties.ReadOnly = true;
            this.txt本人电话.Size = new System.Drawing.Size(142, 20);
            this.txt本人电话.StyleController = this.layoutControl1;
            this.txt本人电话.TabIndex = 17;
            // 
            // txt工作单位
            // 
            this.txt工作单位.Location = new System.Drawing.Point(369, 127);
            this.txt工作单位.Name = "txt工作单位";
            this.txt工作单位.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt工作单位.Properties.Appearance.Options.UseBackColor = true;
            this.txt工作单位.Properties.ReadOnly = true;
            this.txt工作单位.Size = new System.Drawing.Size(359, 20);
            this.txt工作单位.StyleController = this.layoutControl1;
            this.txt工作单位.TabIndex = 16;
            // 
            // txt证件号码
            // 
            this.txt证件号码.Location = new System.Drawing.Point(118, 127);
            this.txt证件号码.Name = "txt证件号码";
            this.txt证件号码.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt证件号码.Properties.Appearance.Options.UseBackColor = true;
            this.txt证件号码.Properties.ReadOnly = true;
            this.txt证件号码.Size = new System.Drawing.Size(142, 20);
            this.txt证件号码.StyleController = this.layoutControl1;
            this.txt证件号码.TabIndex = 15;
            // 
            // txt完整度
            // 
            this.txt完整度.Location = new System.Drawing.Point(346, 75);
            this.txt完整度.Name = "txt完整度";
            this.txt完整度.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt完整度.Properties.Appearance.Options.UseBackColor = true;
            this.txt完整度.Properties.ReadOnly = true;
            this.txt完整度.Size = new System.Drawing.Size(86, 20);
            this.txt完整度.StyleController = this.layoutControl1;
            this.txt完整度.TabIndex = 14;
            // 
            // txt缺项
            // 
            this.txt缺项.Location = new System.Drawing.Point(108, 75);
            this.txt缺项.Name = "txt缺项";
            this.txt缺项.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt缺项.Properties.Appearance.Options.UseBackColor = true;
            this.txt缺项.Properties.ReadOnly = true;
            this.txt缺项.Size = new System.Drawing.Size(129, 20);
            this.txt缺项.StyleController = this.layoutControl1;
            this.txt缺项.TabIndex = 13;
            // 
            // txt档案状态
            // 
            this.txt档案状态.Location = new System.Drawing.Point(346, 49);
            this.txt档案状态.Name = "txt档案状态";
            this.txt档案状态.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt档案状态.Properties.Appearance.Options.UseBackColor = true;
            this.txt档案状态.Properties.ReadOnly = true;
            this.txt档案状态.Size = new System.Drawing.Size(86, 20);
            this.txt档案状态.StyleController = this.layoutControl1;
            this.txt档案状态.TabIndex = 12;
            // 
            // txt出生日期
            // 
            this.txt出生日期.Location = new System.Drawing.Point(108, 49);
            this.txt出生日期.Name = "txt出生日期";
            this.txt出生日期.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt出生日期.Properties.Appearance.Options.UseBackColor = true;
            this.txt出生日期.Properties.ReadOnly = true;
            this.txt出生日期.Size = new System.Drawing.Size(129, 20);
            this.txt出生日期.StyleController = this.layoutControl1;
            this.txt出生日期.TabIndex = 11;
            // 
            // txt性别
            // 
            this.txt性别.Location = new System.Drawing.Point(577, 25);
            this.txt性别.Name = "txt性别";
            this.txt性别.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt性别.Properties.Appearance.Options.UseBackColor = true;
            this.txt性别.Properties.ReadOnly = true;
            this.txt性别.Size = new System.Drawing.Size(151, 20);
            this.txt性别.StyleController = this.layoutControl1;
            this.txt性别.TabIndex = 10;
            // 
            // txt姓名
            // 
            this.txt姓名.Location = new System.Drawing.Point(346, 25);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt姓名.Properties.Appearance.Options.UseBackColor = true;
            this.txt姓名.Properties.ReadOnly = true;
            this.txt姓名.Size = new System.Drawing.Size(122, 20);
            this.txt姓名.StyleController = this.layoutControl1;
            this.txt姓名.TabIndex = 9;
            // 
            // txt档案号
            // 
            this.txt档案号.Location = new System.Drawing.Point(108, 25);
            this.txt档案号.Name = "txt档案号";
            this.txt档案号.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt档案号.Properties.Appearance.Options.UseBackColor = true;
            this.txt档案号.Properties.ReadOnly = true;
            this.txt档案号.Size = new System.Drawing.Size(129, 20);
            this.txt档案号.StyleController = this.layoutControl1;
            this.txt档案号.TabIndex = 8;
            // 
            // txt家族史
            // 
            this.txt家族史.Location = new System.Drawing.Point(118, 581);
            this.txt家族史.Name = "txt家族史";
            this.txt家族史.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.txt家族史.Properties.Appearance.Options.UseBackColor = true;
            this.txt家族史.Properties.ReadOnly = true;
            this.txt家族史.Size = new System.Drawing.Size(610, 20);
            this.txt家族史.StyleController = this.layoutControl1;
            this.txt家族史.TabIndex = 36;
            this.txt家族史.UseOptimizedRendering = true;
            // 
            // dateEdit签字时间
            // 
            this.dateEdit签字时间.Location = new System.Drawing.Point(549, 944);
            this.dateEdit签字时间.Name = "dateEdit签字时间";
            this.dateEdit签字时间.Properties.Mask.EditMask = "d";
            this.dateEdit签字时间.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.dateEdit签字时间.Properties.ReadOnly = true;
            this.dateEdit签字时间.Size = new System.Drawing.Size(179, 20);
            this.dateEdit签字时间.StyleController = this.layoutControl1;
            this.dateEdit签字时间.TabIndex = 71;
            // 
            // textEdit本人或家属签字
            // 
            this.textEdit本人或家属签字.Location = new System.Drawing.Point(120, 944);
            this.textEdit本人或家属签字.Name = "textEdit本人或家属签字";
            this.textEdit本人或家属签字.Properties.ReadOnly = true;
            this.textEdit本人或家属签字.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom;
            this.textEdit本人或家属签字.Size = new System.Drawing.Size(308, 67);
            this.textEdit本人或家属签字.StyleController = this.layoutControl1;
            this.textEdit本人或家属签字.TabIndex = 70;
            this.textEdit本人或家属签字.TabStop = true;
            // 
            // lbl劳动强度
            // 
            this.lbl劳动强度.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl劳动强度.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl劳动强度.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl劳动强度.Control = this.txt劳动强度;
            this.lbl劳动强度.CustomizationFormText = "劳动强度:";
            this.lbl劳动强度.Location = new System.Drawing.Point(0, 144);
            this.lbl劳动强度.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl劳动强度.MinSize = new System.Drawing.Size(168, 24);
            this.lbl劳动强度.Name = "lbl劳动强度";
            this.lbl劳动强度.Size = new System.Drawing.Size(729, 24);
            this.lbl劳动强度.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl劳动强度.Text = "劳动强度:";
            this.lbl劳动强度.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl劳动强度.TextSize = new System.Drawing.Size(110, 20);
            this.lbl劳动强度.TextToControlDistance = 5;
            // 
            // lbl医疗费用支付方式
            // 
            this.lbl医疗费用支付方式.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl医疗费用支付方式.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl医疗费用支付方式.AppearanceItemCaption.Options.UseFont = true;
            this.lbl医疗费用支付方式.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl医疗费用支付方式.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl医疗费用支付方式.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl医疗费用支付方式.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl医疗费用支付方式.Control = this.txt医疗费用支付方式;
            this.lbl医疗费用支付方式.CustomizationFormText = "医疗费用支付方式";
            this.lbl医疗费用支付方式.Location = new System.Drawing.Point(0, 258);
            this.lbl医疗费用支付方式.MaxSize = new System.Drawing.Size(0, 40);
            this.lbl医疗费用支付方式.MinSize = new System.Drawing.Size(168, 24);
            this.lbl医疗费用支付方式.Name = "lbl医疗费用支付方式";
            this.lbl医疗费用支付方式.Size = new System.Drawing.Size(729, 24);
            this.lbl医疗费用支付方式.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl医疗费用支付方式.Text = "医疗费用支付方式";
            this.lbl医疗费用支付方式.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl医疗费用支付方式.TextSize = new System.Drawing.Size(110, 20);
            this.lbl医疗费用支付方式.TextToControlDistance = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.AppearanceGroup.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "个人基本信息表";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.simpleSeparator1,
            this.simpleSeparator2,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup5,
            this.layoutControlGroup6,
            this.group妇女});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(731, 1080);
            this.layoutControlGroup1.Text = "个人基本信息表";
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl档案号,
            this.lbl姓名,
            this.lbl性别,
            this.lbl出生日期,
            this.lbl档案状态,
            this.lbl缺项,
            this.lbl完整度,
            this.layoutControlItem1,
            this.layoutControlItem4});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Size = new System.Drawing.Size(729, 74);
            this.layoutControlGroup2.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            // 
            // lbl档案号
            // 
            this.lbl档案号.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl档案号.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl档案号.Control = this.txt档案号;
            this.lbl档案号.CustomizationFormText = "档案号：";
            this.lbl档案号.Location = new System.Drawing.Point(0, 0);
            this.lbl档案号.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl档案号.MinSize = new System.Drawing.Size(109, 24);
            this.lbl档案号.Name = "lbl档案号";
            this.lbl档案号.Size = new System.Drawing.Size(238, 24);
            this.lbl档案号.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl档案号.Text = "档案号：";
            this.lbl档案号.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl档案号.TextSize = new System.Drawing.Size(100, 20);
            this.lbl档案号.TextToControlDistance = 5;
            // 
            // lbl姓名
            // 
            this.lbl姓名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl姓名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl姓名.Control = this.txt姓名;
            this.lbl姓名.CustomizationFormText = "姓名：";
            this.lbl姓名.Location = new System.Drawing.Point(238, 0);
            this.lbl姓名.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl姓名.MinSize = new System.Drawing.Size(109, 24);
            this.lbl姓名.Name = "lbl姓名";
            this.lbl姓名.Size = new System.Drawing.Size(231, 24);
            this.lbl姓名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl姓名.Text = "姓名：";
            this.lbl姓名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl姓名.TextSize = new System.Drawing.Size(100, 20);
            this.lbl姓名.TextToControlDistance = 5;
            // 
            // lbl性别
            // 
            this.lbl性别.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl性别.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl性别.Control = this.txt性别;
            this.lbl性别.CustomizationFormText = "性别：";
            this.lbl性别.Location = new System.Drawing.Point(469, 0);
            this.lbl性别.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl性别.MinSize = new System.Drawing.Size(109, 24);
            this.lbl性别.Name = "lbl性别";
            this.lbl性别.Size = new System.Drawing.Size(260, 24);
            this.lbl性别.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl性别.Text = "性别：";
            this.lbl性别.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl性别.TextSize = new System.Drawing.Size(100, 20);
            this.lbl性别.TextToControlDistance = 5;
            // 
            // lbl出生日期
            // 
            this.lbl出生日期.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl出生日期.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl出生日期.Control = this.txt出生日期;
            this.lbl出生日期.CustomizationFormText = "出生日期：";
            this.lbl出生日期.Location = new System.Drawing.Point(0, 24);
            this.lbl出生日期.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl出生日期.MinSize = new System.Drawing.Size(109, 24);
            this.lbl出生日期.Name = "lbl出生日期";
            this.lbl出生日期.Size = new System.Drawing.Size(238, 26);
            this.lbl出生日期.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl出生日期.Text = "出生日期：";
            this.lbl出生日期.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl出生日期.TextSize = new System.Drawing.Size(100, 20);
            this.lbl出生日期.TextToControlDistance = 5;
            // 
            // lbl档案状态
            // 
            this.lbl档案状态.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl档案状态.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl档案状态.Control = this.txt档案状态;
            this.lbl档案状态.CustomizationFormText = "档案状态：";
            this.lbl档案状态.Location = new System.Drawing.Point(238, 24);
            this.lbl档案状态.MaxSize = new System.Drawing.Size(200, 24);
            this.lbl档案状态.MinSize = new System.Drawing.Size(100, 24);
            this.lbl档案状态.Name = "lbl档案状态";
            this.lbl档案状态.Size = new System.Drawing.Size(195, 26);
            this.lbl档案状态.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl档案状态.Text = "档案状态：";
            this.lbl档案状态.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl档案状态.TextSize = new System.Drawing.Size(100, 20);
            this.lbl档案状态.TextToControlDistance = 5;
            // 
            // lbl缺项
            // 
            this.lbl缺项.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl缺项.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl缺项.Control = this.txt缺项;
            this.lbl缺项.CustomizationFormText = "缺项：";
            this.lbl缺项.Location = new System.Drawing.Point(0, 50);
            this.lbl缺项.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl缺项.MinSize = new System.Drawing.Size(109, 24);
            this.lbl缺项.Name = "lbl缺项";
            this.lbl缺项.Size = new System.Drawing.Size(238, 24);
            this.lbl缺项.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl缺项.Text = "缺项：";
            this.lbl缺项.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl缺项.TextSize = new System.Drawing.Size(100, 20);
            this.lbl缺项.TextToControlDistance = 5;
            // 
            // lbl完整度
            // 
            this.lbl完整度.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl完整度.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl完整度.Control = this.txt完整度;
            this.lbl完整度.CustomizationFormText = "layoutControlItem11";
            this.lbl完整度.Location = new System.Drawing.Point(238, 50);
            this.lbl完整度.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl完整度.MinSize = new System.Drawing.Size(194, 24);
            this.lbl完整度.Name = "lbl完整度";
            this.lbl完整度.Size = new System.Drawing.Size(195, 24);
            this.lbl完整度.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl完整度.Text = "完整度：";
            this.lbl完整度.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl完整度.TextSize = new System.Drawing.Size(100, 14);
            this.lbl完整度.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.flowLayoutPanel2;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(433, 24);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(104, 26);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(296, 26);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem4.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem4.Control = this.txt档案位置;
            this.layoutControlItem4.CustomizationFormText = "档案位置";
            this.layoutControlItem4.Location = new System.Drawing.Point(433, 50);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(296, 24);
            this.layoutControlItem4.Text = "档案位置";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(48, 14);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 74);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(729, 2);
            this.simpleSeparator1.Text = "simpleSeparator1";
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.CustomizationFormText = "simpleSeparator2";
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 696);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(729, 2);
            this.simpleSeparator2.Text = "simpleSeparator2";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl证件号码,
            this.lbl工作单位,
            this.lbl联系人姓名,
            this.lbl联系人电话,
            this.lbl本人电话,
            this.lbl常住类型,
            this.lbl民族,
            this.lbl与户主关系,
            this.lbl血型,
            this.lblRH,
            this.lbl文化程度,
            this.lbl职业,
            this.lbl婚姻状况,
            this.lbl医疗保险号,
            this.lbl所属片区,
            this.lbl新农合号,
            this.lbl档案类别,
            this.lbl居住地址,
            this.lbl医疗支付方式,
            this.layoutControlItem11,
            this.layoutControlItem13,
            this.emptySpaceItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 76);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(729, 356);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            // 
            // lbl证件号码
            // 
            this.lbl证件号码.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl证件号码.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl证件号码.AppearanceItemCaption.Options.UseFont = true;
            this.lbl证件号码.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl证件号码.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl证件号码.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl证件号码.Control = this.txt证件号码;
            this.lbl证件号码.CustomizationFormText = "证件号码";
            this.lbl证件号码.Location = new System.Drawing.Point(0, 26);
            this.lbl证件号码.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl证件号码.MinSize = new System.Drawing.Size(168, 24);
            this.lbl证件号码.Name = "lbl证件号码";
            this.lbl证件号码.Size = new System.Drawing.Size(261, 24);
            this.lbl证件号码.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl证件号码.Text = "证件号码";
            this.lbl证件号码.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl证件号码.TextSize = new System.Drawing.Size(110, 20);
            this.lbl证件号码.TextToControlDistance = 5;
            // 
            // lbl工作单位
            // 
            this.lbl工作单位.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl工作单位.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl工作单位.AppearanceItemCaption.Options.UseFont = true;
            this.lbl工作单位.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl工作单位.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl工作单位.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl工作单位.Control = this.txt工作单位;
            this.lbl工作单位.CustomizationFormText = "工作单位";
            this.lbl工作单位.Location = new System.Drawing.Point(261, 26);
            this.lbl工作单位.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl工作单位.MinSize = new System.Drawing.Size(168, 24);
            this.lbl工作单位.Name = "lbl工作单位";
            this.lbl工作单位.Size = new System.Drawing.Size(468, 24);
            this.lbl工作单位.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl工作单位.Text = "工作单位";
            this.lbl工作单位.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl工作单位.TextSize = new System.Drawing.Size(100, 20);
            this.lbl工作单位.TextToControlDistance = 5;
            // 
            // lbl联系人姓名
            // 
            this.lbl联系人姓名.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl联系人姓名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl联系人姓名.AppearanceItemCaption.Options.UseFont = true;
            this.lbl联系人姓名.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl联系人姓名.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl联系人姓名.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl联系人姓名.Control = this.txt联系人姓名;
            this.lbl联系人姓名.CustomizationFormText = "联系人姓名";
            this.lbl联系人姓名.Location = new System.Drawing.Point(261, 50);
            this.lbl联系人姓名.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl联系人姓名.MinSize = new System.Drawing.Size(168, 24);
            this.lbl联系人姓名.Name = "lbl联系人姓名";
            this.lbl联系人姓名.Size = new System.Drawing.Size(191, 24);
            this.lbl联系人姓名.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl联系人姓名.Text = "联系人姓名";
            this.lbl联系人姓名.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl联系人姓名.TextSize = new System.Drawing.Size(100, 20);
            this.lbl联系人姓名.TextToControlDistance = 5;
            // 
            // lbl联系人电话
            // 
            this.lbl联系人电话.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl联系人电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl联系人电话.AppearanceItemCaption.Options.UseFont = true;
            this.lbl联系人电话.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl联系人电话.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl联系人电话.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl联系人电话.Control = this.txt联系人电话;
            this.lbl联系人电话.CustomizationFormText = "联系人电话";
            this.lbl联系人电话.Location = new System.Drawing.Point(452, 50);
            this.lbl联系人电话.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl联系人电话.MinSize = new System.Drawing.Size(168, 24);
            this.lbl联系人电话.Name = "lbl联系人电话";
            this.lbl联系人电话.Size = new System.Drawing.Size(277, 24);
            this.lbl联系人电话.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl联系人电话.Text = "联系人电话";
            this.lbl联系人电话.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl联系人电话.TextSize = new System.Drawing.Size(100, 20);
            this.lbl联系人电话.TextToControlDistance = 5;
            // 
            // lbl本人电话
            // 
            this.lbl本人电话.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl本人电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl本人电话.AppearanceItemCaption.Options.UseFont = true;
            this.lbl本人电话.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl本人电话.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl本人电话.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl本人电话.Control = this.txt本人电话;
            this.lbl本人电话.CustomizationFormText = "本人电话";
            this.lbl本人电话.Location = new System.Drawing.Point(0, 50);
            this.lbl本人电话.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl本人电话.MinSize = new System.Drawing.Size(168, 24);
            this.lbl本人电话.Name = "lbl本人电话";
            this.lbl本人电话.Size = new System.Drawing.Size(261, 24);
            this.lbl本人电话.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl本人电话.Text = "本人电话";
            this.lbl本人电话.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl本人电话.TextSize = new System.Drawing.Size(110, 20);
            this.lbl本人电话.TextToControlDistance = 5;
            // 
            // lbl常住类型
            // 
            this.lbl常住类型.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl常住类型.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl常住类型.AppearanceItemCaption.Options.UseFont = true;
            this.lbl常住类型.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl常住类型.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl常住类型.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl常住类型.Control = this.txt常住类型;
            this.lbl常住类型.CustomizationFormText = "常住类型";
            this.lbl常住类型.Location = new System.Drawing.Point(0, 74);
            this.lbl常住类型.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl常住类型.MinSize = new System.Drawing.Size(168, 24);
            this.lbl常住类型.Name = "lbl常住类型";
            this.lbl常住类型.Size = new System.Drawing.Size(261, 24);
            this.lbl常住类型.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl常住类型.Text = "常住类型";
            this.lbl常住类型.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl常住类型.TextSize = new System.Drawing.Size(110, 20);
            this.lbl常住类型.TextToControlDistance = 5;
            // 
            // lbl民族
            // 
            this.lbl民族.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl民族.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl民族.AppearanceItemCaption.Options.UseFont = true;
            this.lbl民族.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl民族.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl民族.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl民族.Control = this.txt民族;
            this.lbl民族.CustomizationFormText = "民族";
            this.lbl民族.Location = new System.Drawing.Point(261, 74);
            this.lbl民族.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl民族.MinSize = new System.Drawing.Size(168, 24);
            this.lbl民族.Name = "lbl民族";
            this.lbl民族.Size = new System.Drawing.Size(191, 24);
            this.lbl民族.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl民族.Text = "民族";
            this.lbl民族.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl民族.TextSize = new System.Drawing.Size(100, 20);
            this.lbl民族.TextToControlDistance = 5;
            // 
            // lbl与户主关系
            // 
            this.lbl与户主关系.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl与户主关系.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl与户主关系.Control = this.txt与户主关系;
            this.lbl与户主关系.CustomizationFormText = "与户主关系:";
            this.lbl与户主关系.Location = new System.Drawing.Point(452, 74);
            this.lbl与户主关系.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl与户主关系.MinSize = new System.Drawing.Size(168, 24);
            this.lbl与户主关系.Name = "lbl与户主关系";
            this.lbl与户主关系.Size = new System.Drawing.Size(277, 24);
            this.lbl与户主关系.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl与户主关系.Text = "与户主关系:";
            this.lbl与户主关系.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl与户主关系.TextSize = new System.Drawing.Size(100, 20);
            this.lbl与户主关系.TextToControlDistance = 5;
            // 
            // lbl血型
            // 
            this.lbl血型.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl血型.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl血型.AppearanceItemCaption.Options.UseFont = true;
            this.lbl血型.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl血型.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl血型.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl血型.Control = this.txt血型;
            this.lbl血型.CustomizationFormText = "血型";
            this.lbl血型.Location = new System.Drawing.Point(0, 98);
            this.lbl血型.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl血型.MinSize = new System.Drawing.Size(168, 24);
            this.lbl血型.Name = "lbl血型";
            this.lbl血型.Size = new System.Drawing.Size(261, 24);
            this.lbl血型.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl血型.Text = "血型";
            this.lbl血型.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl血型.TextSize = new System.Drawing.Size(110, 20);
            this.lbl血型.TextToControlDistance = 5;
            // 
            // lblRH
            // 
            this.lblRH.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblRH.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lblRH.AppearanceItemCaption.Options.UseFont = true;
            this.lblRH.AppearanceItemCaption.Options.UseForeColor = true;
            this.lblRH.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lblRH.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblRH.Control = this.txtRH;
            this.lblRH.CustomizationFormText = "RH";
            this.lblRH.Location = new System.Drawing.Point(261, 98);
            this.lblRH.MaxSize = new System.Drawing.Size(0, 24);
            this.lblRH.MinSize = new System.Drawing.Size(168, 24);
            this.lblRH.Name = "lblRH";
            this.lblRH.Size = new System.Drawing.Size(468, 24);
            this.lblRH.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lblRH.Text = "RH";
            this.lblRH.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lblRH.TextSize = new System.Drawing.Size(100, 20);
            this.lblRH.TextToControlDistance = 5;
            // 
            // lbl文化程度
            // 
            this.lbl文化程度.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl文化程度.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl文化程度.AppearanceItemCaption.Options.UseFont = true;
            this.lbl文化程度.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl文化程度.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl文化程度.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl文化程度.Control = this.txt文化程度;
            this.lbl文化程度.CustomizationFormText = "文化程度";
            this.lbl文化程度.Location = new System.Drawing.Point(0, 122);
            this.lbl文化程度.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl文化程度.MinSize = new System.Drawing.Size(168, 24);
            this.lbl文化程度.Name = "lbl文化程度";
            this.lbl文化程度.Size = new System.Drawing.Size(729, 24);
            this.lbl文化程度.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl文化程度.Text = "文化程度";
            this.lbl文化程度.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl文化程度.TextSize = new System.Drawing.Size(110, 20);
            this.lbl文化程度.TextToControlDistance = 5;
            // 
            // lbl职业
            // 
            this.lbl职业.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl职业.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl职业.AppearanceItemCaption.Options.UseFont = true;
            this.lbl职业.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl职业.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl职业.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl职业.Control = this.txt职业;
            this.lbl职业.CustomizationFormText = "职业";
            this.lbl职业.Location = new System.Drawing.Point(0, 146);
            this.lbl职业.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl职业.MinSize = new System.Drawing.Size(168, 24);
            this.lbl职业.Name = "lbl职业";
            this.lbl职业.Size = new System.Drawing.Size(729, 24);
            this.lbl职业.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl职业.Text = "职业";
            this.lbl职业.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl职业.TextSize = new System.Drawing.Size(110, 20);
            this.lbl职业.TextToControlDistance = 5;
            // 
            // lbl婚姻状况
            // 
            this.lbl婚姻状况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl婚姻状况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl婚姻状况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl婚姻状况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl婚姻状况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl婚姻状况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl婚姻状况.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl婚姻状况.Control = this.txt婚姻状况;
            this.lbl婚姻状况.CustomizationFormText = "婚姻状况";
            this.lbl婚姻状况.Location = new System.Drawing.Point(0, 170);
            this.lbl婚姻状况.MaxSize = new System.Drawing.Size(0, 40);
            this.lbl婚姻状况.MinSize = new System.Drawing.Size(168, 24);
            this.lbl婚姻状况.Name = "lbl婚姻状况";
            this.lbl婚姻状况.Size = new System.Drawing.Size(729, 24);
            this.lbl婚姻状况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl婚姻状况.Text = "婚姻状况";
            this.lbl婚姻状况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl婚姻状况.TextSize = new System.Drawing.Size(110, 20);
            this.lbl婚姻状况.TextToControlDistance = 5;
            // 
            // lbl医疗保险号
            // 
            this.lbl医疗保险号.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl医疗保险号.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl医疗保险号.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl医疗保险号.Control = this.txt医疗保险号;
            this.lbl医疗保险号.CustomizationFormText = "医疗保险号:";
            this.lbl医疗保险号.Location = new System.Drawing.Point(0, 284);
            this.lbl医疗保险号.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl医疗保险号.MinSize = new System.Drawing.Size(168, 24);
            this.lbl医疗保险号.Name = "lbl医疗保险号";
            this.lbl医疗保险号.Size = new System.Drawing.Size(289, 24);
            this.lbl医疗保险号.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl医疗保险号.Text = "医疗保险号:";
            this.lbl医疗保险号.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl医疗保险号.TextSize = new System.Drawing.Size(110, 20);
            this.lbl医疗保险号.TextToControlDistance = 5;
            this.lbl医疗保险号.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl所属片区
            // 
            this.lbl所属片区.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl所属片区.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl所属片区.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl所属片区.Control = this.txt所属片区;
            this.lbl所属片区.CustomizationFormText = "所属片区:";
            this.lbl所属片区.Location = new System.Drawing.Point(0, 308);
            this.lbl所属片区.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl所属片区.MinSize = new System.Drawing.Size(168, 24);
            this.lbl所属片区.Name = "lbl所属片区";
            this.lbl所属片区.Size = new System.Drawing.Size(289, 24);
            this.lbl所属片区.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl所属片区.Text = "所属片区:";
            this.lbl所属片区.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl所属片区.TextSize = new System.Drawing.Size(110, 14);
            this.lbl所属片区.TextToControlDistance = 5;
            // 
            // lbl新农合号
            // 
            this.lbl新农合号.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl新农合号.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl新农合号.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl新农合号.Control = this.txt新农合号;
            this.lbl新农合号.CustomizationFormText = "新农合号:";
            this.lbl新农合号.Location = new System.Drawing.Point(289, 284);
            this.lbl新农合号.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl新农合号.MinSize = new System.Drawing.Size(168, 24);
            this.lbl新农合号.Name = "lbl新农合号";
            this.lbl新农合号.Size = new System.Drawing.Size(440, 24);
            this.lbl新农合号.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl新农合号.Text = "新农合号:";
            this.lbl新农合号.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl新农合号.TextSize = new System.Drawing.Size(100, 20);
            this.lbl新农合号.TextToControlDistance = 5;
            this.lbl新农合号.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // lbl档案类别
            // 
            this.lbl档案类别.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl档案类别.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl档案类别.AppearanceItemCaption.Options.UseFont = true;
            this.lbl档案类别.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl档案类别.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl档案类别.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl档案类别.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl档案类别.Control = this.txt档案类别;
            this.lbl档案类别.CustomizationFormText = "档案类别:";
            this.lbl档案类别.Location = new System.Drawing.Point(289, 308);
            this.lbl档案类别.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl档案类别.MinSize = new System.Drawing.Size(168, 24);
            this.lbl档案类别.Name = "lbl档案类别";
            this.lbl档案类别.Size = new System.Drawing.Size(440, 24);
            this.lbl档案类别.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl档案类别.Text = "档案类别:";
            this.lbl档案类别.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl档案类别.TextSize = new System.Drawing.Size(100, 20);
            this.lbl档案类别.TextToControlDistance = 5;
            // 
            // lbl居住地址
            // 
            this.lbl居住地址.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl居住地址.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl居住地址.AppearanceItemCaption.Options.UseFont = true;
            this.lbl居住地址.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl居住地址.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl居住地址.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl居住地址.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbl居住地址.Control = this.txt居住地址;
            this.lbl居住地址.CustomizationFormText = "居住地址:";
            this.lbl居住地址.Location = new System.Drawing.Point(0, 332);
            this.lbl居住地址.MaxSize = new System.Drawing.Size(0, 40);
            this.lbl居住地址.MinSize = new System.Drawing.Size(168, 24);
            this.lbl居住地址.Name = "lbl居住地址";
            this.lbl居住地址.Size = new System.Drawing.Size(729, 24);
            this.lbl居住地址.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl居住地址.Text = "居住地址:";
            this.lbl居住地址.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl居住地址.TextSize = new System.Drawing.Size(110, 20);
            this.lbl居住地址.TextToControlDistance = 5;
            // 
            // lbl医疗支付方式
            // 
            this.lbl医疗支付方式.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl医疗支付方式.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl医疗支付方式.AppearanceItemCaption.Options.UseFont = true;
            this.lbl医疗支付方式.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl医疗支付方式.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl医疗支付方式.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl医疗支付方式.Control = this.flowLayoutPanel3;
            this.lbl医疗支付方式.CustomizationFormText = "医疗费用支付方式";
            this.lbl医疗支付方式.Location = new System.Drawing.Point(0, 194);
            this.lbl医疗支付方式.MinSize = new System.Drawing.Size(211, 90);
            this.lbl医疗支付方式.Name = "lbl医疗支付方式";
            this.lbl医疗支付方式.Size = new System.Drawing.Size(729, 90);
            this.lbl医疗支付方式.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl医疗支付方式.Text = "医疗费用支付方式";
            this.lbl医疗支付方式.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl医疗支付方式.TextSize = new System.Drawing.Size(110, 14);
            this.lbl医疗支付方式.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem11.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem11.Control = this.txt签约医生;
            this.layoutControlItem11.CustomizationFormText = "签约医生";
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(347, 26);
            this.layoutControlItem11.Text = "签约医生";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.btn签约医生;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem13";
            this.layoutControlItem13.Location = new System.Drawing.Point(347, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(106, 26);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(106, 26);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(106, 26);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem13";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(453, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(276, 26);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "layoutControlGroup4";
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem79,
            this.txt录入时间,
            this.simpleLabelItem修改时间,
            this.simpleLabelItem89,
            this.txt最近更新时间,
            this.txt录入人,
            this.simpleLabelItem85,
            this.simpleLabelItem91,
            this.txt当前所属机构,
            this.simpleSeparator3,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.lbl本人或家属签字,
            this.lbl签字时间,
            this.layoutControlItem10,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.simpleLabelItem修改人,
            this.txt最近修改人,
            this.txt创建机构,
            this.simpleLabelItem84,
            this.txt调查时间});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 893);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(729, 163);
            this.layoutControlGroup4.Text = "layoutControlGroup4";
            // 
            // simpleLabelItem79
            // 
            this.simpleLabelItem79.AllowHotTrack = false;
            this.simpleLabelItem79.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem79.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem79.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem79.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem79.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem79.CustomizationFormText = "调查时间:";
            this.simpleLabelItem79.Location = new System.Drawing.Point(0, 97);
            this.simpleLabelItem79.MaxSize = new System.Drawing.Size(0, 22);
            this.simpleLabelItem79.MinSize = new System.Drawing.Size(120, 22);
            this.simpleLabelItem79.Name = "simpleLabelItem79";
            this.simpleLabelItem79.Size = new System.Drawing.Size(121, 22);
            this.simpleLabelItem79.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem79.Text = "调查时间:";
            this.simpleLabelItem79.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem79.TextSize = new System.Drawing.Size(50, 20);
            // 
            // txt录入时间
            // 
            this.txt录入时间.AllowHotTrack = false;
            this.txt录入时间.CustomizationFormText = "Labellbl录入时间";
            this.txt录入时间.Location = new System.Drawing.Point(348, 97);
            this.txt录入时间.Name = "txt录入时间";
            this.txt录入时间.Size = new System.Drawing.Size(164, 22);
            this.txt录入时间.Text = " ";
            this.txt录入时间.TextSize = new System.Drawing.Size(48, 14);
            // 
            // simpleLabelItem修改时间
            // 
            this.simpleLabelItem修改时间.AllowHotTrack = false;
            this.simpleLabelItem修改时间.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem修改时间.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem修改时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem修改时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem修改时间.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem修改时间.CustomizationFormText = "最近更新时间:";
            this.simpleLabelItem修改时间.Location = new System.Drawing.Point(512, 97);
            this.simpleLabelItem修改时间.MaxSize = new System.Drawing.Size(0, 18);
            this.simpleLabelItem修改时间.MinSize = new System.Drawing.Size(100, 18);
            this.simpleLabelItem修改时间.Name = "simpleLabelItem修改时间";
            this.simpleLabelItem修改时间.Size = new System.Drawing.Size(100, 22);
            this.simpleLabelItem修改时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem修改时间.Text = "最近更新时间:";
            this.simpleLabelItem修改时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem修改时间.TextSize = new System.Drawing.Size(50, 20);
            // 
            // simpleLabelItem89
            // 
            this.simpleLabelItem89.AllowHotTrack = false;
            this.simpleLabelItem89.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem89.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem89.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem89.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem89.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem89.CustomizationFormText = "创建机构:";
            this.simpleLabelItem89.Location = new System.Drawing.Point(248, 119);
            this.simpleLabelItem89.MaxSize = new System.Drawing.Size(0, 18);
            this.simpleLabelItem89.MinSize = new System.Drawing.Size(100, 18);
            this.simpleLabelItem89.Name = "simpleLabelItem89";
            this.simpleLabelItem89.Size = new System.Drawing.Size(100, 22);
            this.simpleLabelItem89.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem89.Text = "创建机构:";
            this.simpleLabelItem89.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem89.TextSize = new System.Drawing.Size(50, 20);
            // 
            // txt最近更新时间
            // 
            this.txt最近更新时间.AllowHotTrack = false;
            this.txt最近更新时间.CustomizationFormText = "Labellbl最近更新时间";
            this.txt最近更新时间.Location = new System.Drawing.Point(612, 97);
            this.txt最近更新时间.Name = "txt最近更新时间";
            this.txt最近更新时间.Size = new System.Drawing.Size(117, 22);
            this.txt最近更新时间.Text = " ";
            this.txt最近更新时间.TextSize = new System.Drawing.Size(48, 14);
            // 
            // txt录入人
            // 
            this.txt录入人.AllowHotTrack = false;
            this.txt录入人.CustomizationFormText = "Labellbl录入人";
            this.txt录入人.Location = new System.Drawing.Point(121, 119);
            this.txt录入人.Name = "txt录入人";
            this.txt录入人.Size = new System.Drawing.Size(127, 22);
            this.txt录入人.Text = " ";
            this.txt录入人.TextSize = new System.Drawing.Size(48, 14);
            // 
            // simpleLabelItem85
            // 
            this.simpleLabelItem85.AllowHotTrack = false;
            this.simpleLabelItem85.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem85.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem85.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem85.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem85.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem85.CustomizationFormText = "录入人:";
            this.simpleLabelItem85.Location = new System.Drawing.Point(0, 119);
            this.simpleLabelItem85.MaxSize = new System.Drawing.Size(0, 22);
            this.simpleLabelItem85.MinSize = new System.Drawing.Size(120, 22);
            this.simpleLabelItem85.Name = "simpleLabelItem85";
            this.simpleLabelItem85.Size = new System.Drawing.Size(121, 22);
            this.simpleLabelItem85.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem85.Text = "录入人:";
            this.simpleLabelItem85.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem85.TextSize = new System.Drawing.Size(50, 20);
            // 
            // simpleLabelItem91
            // 
            this.simpleLabelItem91.AllowHotTrack = false;
            this.simpleLabelItem91.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem91.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem91.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem91.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem91.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem91.CustomizationFormText = "当前所属机构:";
            this.simpleLabelItem91.Location = new System.Drawing.Point(0, 141);
            this.simpleLabelItem91.MinSize = new System.Drawing.Size(120, 22);
            this.simpleLabelItem91.Name = "simpleLabelItem91";
            this.simpleLabelItem91.Size = new System.Drawing.Size(121, 22);
            this.simpleLabelItem91.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem91.Text = "当前所属机构:";
            this.simpleLabelItem91.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem91.TextSize = new System.Drawing.Size(120, 20);
            // 
            // txt当前所属机构
            // 
            this.txt当前所属机构.AllowHotTrack = false;
            this.txt当前所属机构.CustomizationFormText = "Labellbl当前所属机构";
            this.txt当前所属机构.Location = new System.Drawing.Point(121, 141);
            this.txt当前所属机构.Name = "txt当前所属机构";
            this.txt当前所属机构.Size = new System.Drawing.Size(608, 22);
            this.txt当前所属机构.Text = " ";
            this.txt当前所属机构.TextSize = new System.Drawing.Size(48, 14);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator3";
            this.simpleSeparator3.Location = new System.Drawing.Point(0, 24);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(729, 2);
            this.simpleSeparator3.Text = "simpleSeparator3";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem3.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem3.Control = this.flow外出;
            this.layoutControlItem3.CustomizationFormText = "外出情况：";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(468, 24);
            this.layoutControlItem3.Text = "流动人口：";
            this.layoutControlItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem3.TextToControlDistance = 5;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem2.Control = this.txt外出地址;
            this.layoutControlItem2.CustomizationFormText = "外出地址：";
            this.layoutControlItem2.Location = new System.Drawing.Point(468, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(261, 24);
            this.layoutControlItem2.Text = "外出地址：";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(112, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            // 
            // lbl本人或家属签字
            // 
            this.lbl本人或家属签字.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl本人或家属签字.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl本人或家属签字.AppearanceItemCaption.Options.UseFont = true;
            this.lbl本人或家属签字.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl本人或家属签字.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl本人或家属签字.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl本人或家属签字.Control = this.textEdit本人或家属签字;
            this.lbl本人或家属签字.CustomizationFormText = "本人或家属签字：";
            this.lbl本人或家属签字.Location = new System.Drawing.Point(0, 26);
            this.lbl本人或家属签字.MinSize = new System.Drawing.Size(141, 71);
            this.lbl本人或家属签字.Name = "lbl本人或家属签字";
            this.lbl本人或家属签字.Size = new System.Drawing.Size(429, 71);
            this.lbl本人或家属签字.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl本人或家属签字.Tag = "check";
            this.lbl本人或家属签字.Text = "本人或家属签字：";
            this.lbl本人或家属签字.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl本人或家属签字.TextSize = new System.Drawing.Size(112, 14);
            this.lbl本人或家属签字.TextToControlDistance = 5;
            // 
            // lbl签字时间
            // 
            this.lbl签字时间.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl签字时间.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl签字时间.AppearanceItemCaption.Options.UseFont = true;
            this.lbl签字时间.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl签字时间.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl签字时间.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl签字时间.Control = this.dateEdit签字时间;
            this.lbl签字时间.CustomizationFormText = "签字时间：";
            this.lbl签字时间.Location = new System.Drawing.Point(429, 26);
            this.lbl签字时间.Name = "lbl签字时间";
            this.lbl签字时间.Size = new System.Drawing.Size(300, 24);
            this.lbl签字时间.Tag = "check";
            this.lbl签字时间.Text = "签字时间：";
            this.lbl签字时间.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl签字时间.TextSize = new System.Drawing.Size(112, 14);
            this.lbl签字时间.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem10.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem10.Control = this.txt复核备注;
            this.layoutControlItem10.CustomizationFormText = "复核备注：";
            this.layoutControlItem10.Location = new System.Drawing.Point(429, 50);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(300, 24);
            this.layoutControlItem10.Text = "复核备注：";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(112, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.ch复核标记;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(429, 74);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(150, 23);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.ch二次复核;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(579, 74);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(150, 23);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(150, 23);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // simpleLabelItem修改人
            // 
            this.simpleLabelItem修改人.AllowHotTrack = false;
            this.simpleLabelItem修改人.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem修改人.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem修改人.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem修改人.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem修改人.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem修改人.CustomizationFormText = "最近修改人:";
            this.simpleLabelItem修改人.Location = new System.Drawing.Point(512, 119);
            this.simpleLabelItem修改人.MaxSize = new System.Drawing.Size(0, 18);
            this.simpleLabelItem修改人.MinSize = new System.Drawing.Size(100, 18);
            this.simpleLabelItem修改人.Name = "simpleLabelItem修改人";
            this.simpleLabelItem修改人.Size = new System.Drawing.Size(100, 22);
            this.simpleLabelItem修改人.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem修改人.Text = "最近修改人:";
            this.simpleLabelItem修改人.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem修改人.TextSize = new System.Drawing.Size(50, 20);
            // 
            // txt最近修改人
            // 
            this.txt最近修改人.AllowHotTrack = false;
            this.txt最近修改人.CustomizationFormText = "Labellbl最近修改人";
            this.txt最近修改人.Location = new System.Drawing.Point(612, 119);
            this.txt最近修改人.Name = "txt最近修改人";
            this.txt最近修改人.Size = new System.Drawing.Size(117, 22);
            this.txt最近修改人.Text = " ";
            this.txt最近修改人.TextSize = new System.Drawing.Size(48, 14);
            // 
            // txt创建机构
            // 
            this.txt创建机构.AllowHotTrack = false;
            this.txt创建机构.CustomizationFormText = "Labellbl创建机构";
            this.txt创建机构.Location = new System.Drawing.Point(348, 119);
            this.txt创建机构.Name = "txt创建机构";
            this.txt创建机构.Size = new System.Drawing.Size(164, 22);
            this.txt创建机构.Text = " ";
            this.txt创建机构.TextSize = new System.Drawing.Size(48, 14);
            // 
            // simpleLabelItem84
            // 
            this.simpleLabelItem84.AllowHotTrack = false;
            this.simpleLabelItem84.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem84.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem84.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem84.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem84.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem84.CustomizationFormText = "录入时间:";
            this.simpleLabelItem84.Location = new System.Drawing.Point(248, 97);
            this.simpleLabelItem84.MaxSize = new System.Drawing.Size(100, 18);
            this.simpleLabelItem84.MinSize = new System.Drawing.Size(100, 18);
            this.simpleLabelItem84.Name = "simpleLabelItem84";
            this.simpleLabelItem84.Size = new System.Drawing.Size(100, 22);
            this.simpleLabelItem84.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem84.Text = "录入时间:";
            this.simpleLabelItem84.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem84.TextSize = new System.Drawing.Size(50, 20);
            // 
            // txt调查时间
            // 
            this.txt调查时间.AllowHotTrack = false;
            this.txt调查时间.CustomizationFormText = "Labellbl调查时间";
            this.txt调查时间.Location = new System.Drawing.Point(121, 97);
            this.txt调查时间.MinSize = new System.Drawing.Size(127, 22);
            this.txt调查时间.Name = "txt调查时间";
            this.txt调查时间.Size = new System.Drawing.Size(127, 22);
            this.txt调查时间.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.txt调查时间.Text = " ";
            this.txt调查时间.TextSize = new System.Drawing.Size(48, 14);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup5";
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl厨房排风设施,
            this.lbl燃料类型,
            this.lbl饮水,
            this.lbl厕所,
            this.lbl禽畜栏,
            this.layoutControlGroup9,
            this.emptySpaceItem1,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem3,
            this.emptySpaceItem4});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 698);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(729, 195);
            this.layoutControlGroup5.Text = "layoutControlGroup5";
            // 
            // lbl厨房排风设施
            // 
            this.lbl厨房排风设施.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl厨房排风设施.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl厨房排风设施.Control = this.txt厨房排风设施;
            this.lbl厨房排风设施.CustomizationFormText = "厨房排风设施：";
            this.lbl厨房排风设施.Location = new System.Drawing.Point(22, 75);
            this.lbl厨房排风设施.Name = "lbl厨房排风设施";
            this.lbl厨房排风设施.Size = new System.Drawing.Size(707, 24);
            this.lbl厨房排风设施.Text = "厨房排风设施：";
            this.lbl厨房排风设施.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl厨房排风设施.TextSize = new System.Drawing.Size(90, 20);
            this.lbl厨房排风设施.TextToControlDistance = 5;
            // 
            // lbl燃料类型
            // 
            this.lbl燃料类型.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl燃料类型.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl燃料类型.Control = this.txt燃料类型;
            this.lbl燃料类型.CustomizationFormText = "燃料类型：";
            this.lbl燃料类型.Location = new System.Drawing.Point(22, 99);
            this.lbl燃料类型.Name = "lbl燃料类型";
            this.lbl燃料类型.Size = new System.Drawing.Size(707, 24);
            this.lbl燃料类型.Text = "燃料类型：";
            this.lbl燃料类型.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl燃料类型.TextSize = new System.Drawing.Size(90, 20);
            this.lbl燃料类型.TextToControlDistance = 5;
            // 
            // lbl饮水
            // 
            this.lbl饮水.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl饮水.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl饮水.Control = this.txt饮水;
            this.lbl饮水.CustomizationFormText = "饮水：";
            this.lbl饮水.Location = new System.Drawing.Point(22, 123);
            this.lbl饮水.Name = "lbl饮水";
            this.lbl饮水.Size = new System.Drawing.Size(707, 24);
            this.lbl饮水.Text = "饮水：";
            this.lbl饮水.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl饮水.TextSize = new System.Drawing.Size(90, 20);
            this.lbl饮水.TextToControlDistance = 5;
            // 
            // lbl厕所
            // 
            this.lbl厕所.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl厕所.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl厕所.Control = this.txt厕所;
            this.lbl厕所.CustomizationFormText = "厕所：";
            this.lbl厕所.Location = new System.Drawing.Point(22, 147);
            this.lbl厕所.Name = "lbl厕所";
            this.lbl厕所.Size = new System.Drawing.Size(707, 24);
            this.lbl厕所.Text = "厕所：";
            this.lbl厕所.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl厕所.TextSize = new System.Drawing.Size(90, 20);
            this.lbl厕所.TextToControlDistance = 5;
            // 
            // lbl禽畜栏
            // 
            this.lbl禽畜栏.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl禽畜栏.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl禽畜栏.Control = this.txt禽畜栏;
            this.lbl禽畜栏.CustomizationFormText = "禽畜栏：";
            this.lbl禽畜栏.Location = new System.Drawing.Point(22, 171);
            this.lbl禽畜栏.Name = "lbl禽畜栏";
            this.lbl禽畜栏.Size = new System.Drawing.Size(707, 24);
            this.lbl禽畜栏.Text = "禽畜栏：";
            this.lbl禽畜栏.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl禽畜栏.TextSize = new System.Drawing.Size(90, 20);
            this.lbl禽畜栏.TextToControlDistance = 5;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "layoutControlGroup9";
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem68});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 75);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup9.Size = new System.Drawing.Size(22, 120);
            this.layoutControlGroup9.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup9.Text = "layoutControlGroup9";
            this.layoutControlGroup9.TextVisible = false;
            // 
            // simpleLabelItem68
            // 
            this.simpleLabelItem68.AllowHotTrack = false;
            this.simpleLabelItem68.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem68.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem68.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem68.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem68.CustomizationFormText = "生活环境";
            this.simpleLabelItem68.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem68.MaxSize = new System.Drawing.Size(0, 18);
            this.simpleLabelItem68.MinSize = new System.Drawing.Size(20, 18);
            this.simpleLabelItem68.Name = "simpleLabelItem68";
            this.simpleLabelItem68.Size = new System.Drawing.Size(20, 118);
            this.simpleLabelItem68.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem68.Text = "生活环境";
            this.simpleLabelItem68.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem68.TextSize = new System.Drawing.Size(50, 20);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.emptySpaceItem1.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emptySpaceItem1.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseBackColor = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseFont = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseForeColor = true;
            this.emptySpaceItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.emptySpaceItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.emptySpaceItem1.CustomizationFormText = "家庭情况";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 20);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(729, 20);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "家庭情况";
            this.emptySpaceItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 20);
            this.emptySpaceItem1.TextVisible = true;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem5.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem5.Control = this.txt户主姓名;
            this.layoutControlItem5.CustomizationFormText = "户主姓名：";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 20);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(227, 24);
            this.layoutControlItem5.Text = "户主姓名：";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem6.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem6.Control = this.txt户主身份证号;
            this.layoutControlItem6.CustomizationFormText = "户主身份证号：";
            this.layoutControlItem6.Location = new System.Drawing.Point(227, 20);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(293, 24);
            this.layoutControlItem6.Text = "户主身份证号：";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem7.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem7.Control = this.txt家庭人口数;
            this.layoutControlItem7.CustomizationFormText = "家庭人口数：";
            this.layoutControlItem7.Location = new System.Drawing.Point(520, 20);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(209, 24);
            this.layoutControlItem7.Text = "家庭人口数：";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem8.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem8.Control = this.txt家庭结构;
            this.layoutControlItem8.CustomizationFormText = "家庭结构：";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 44);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(227, 24);
            this.layoutControlItem8.Text = "家庭结构：";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(110, 20);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem9.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlItem9.Control = this.txt居住情况;
            this.layoutControlItem9.CustomizationFormText = "居住情况*：";
            this.layoutControlItem9.Location = new System.Drawing.Point(227, 44);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(502, 24);
            this.layoutControlItem9.Text = "居住情况*：";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(90, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 68);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 2);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(729, 2);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 70);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 5);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(729, 5);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6";
            this.layoutControlGroup6.GroupBordersVisible = false;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl药物过敏史,
            this.lbl家族史,
            this.lbl射线,
            this.lbl化学品,
            this.lbl毒物,
            this.lbl遗传病史,
            this.lbl残疾情况,
            this.lbl疾病,
            this.lbl手术,
            this.lbl外伤,
            this.lbl输血,
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.simpleSeparator4,
            this.simpleSeparator5});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 432);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(729, 240);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            // 
            // lbl药物过敏史
            // 
            this.lbl药物过敏史.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl药物过敏史.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl药物过敏史.AppearanceItemCaption.Options.UseFont = true;
            this.lbl药物过敏史.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl药物过敏史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl药物过敏史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl药物过敏史.Control = this.txt药物过敏史;
            this.lbl药物过敏史.CustomizationFormText = "药物过敏史";
            this.lbl药物过敏史.Location = new System.Drawing.Point(0, 0);
            this.lbl药物过敏史.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl药物过敏史.MinSize = new System.Drawing.Size(109, 24);
            this.lbl药物过敏史.Name = "lbl药物过敏史";
            this.lbl药物过敏史.Size = new System.Drawing.Size(729, 24);
            this.lbl药物过敏史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl药物过敏史.Text = "药物过敏史";
            this.lbl药物过敏史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl药物过敏史.TextSize = new System.Drawing.Size(110, 20);
            this.lbl药物过敏史.TextToControlDistance = 5;
            // 
            // lbl家族史
            // 
            this.lbl家族史.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl家族史.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl家族史.AppearanceItemCaption.Options.UseFont = true;
            this.lbl家族史.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl家族史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl家族史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl家族史.Control = this.txt家族史;
            this.lbl家族史.CustomizationFormText = "家族史";
            this.lbl家族史.Location = new System.Drawing.Point(0, 124);
            this.lbl家族史.MinSize = new System.Drawing.Size(129, 24);
            this.lbl家族史.Name = "lbl家族史";
            this.lbl家族史.Size = new System.Drawing.Size(729, 24);
            this.lbl家族史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl家族史.Text = "家族史";
            this.lbl家族史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl家族史.TextSize = new System.Drawing.Size(110, 20);
            this.lbl家族史.TextToControlDistance = 5;
            // 
            // lbl射线
            // 
            this.lbl射线.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl射线.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl射线.Control = this.txt射线;
            this.lbl射线.CustomizationFormText = "射线：";
            this.lbl射线.Location = new System.Drawing.Point(436, 168);
            this.lbl射线.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl射线.MinSize = new System.Drawing.Size(109, 24);
            this.lbl射线.Name = "lbl射线";
            this.lbl射线.Size = new System.Drawing.Size(293, 24);
            this.lbl射线.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl射线.Text = "射线：";
            this.lbl射线.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl射线.TextSize = new System.Drawing.Size(80, 20);
            this.lbl射线.TextToControlDistance = 5;
            // 
            // lbl化学品
            // 
            this.lbl化学品.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl化学品.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl化学品.Control = this.txt化学品;
            this.lbl化学品.CustomizationFormText = "化学品:";
            this.lbl化学品.Location = new System.Drawing.Point(0, 168);
            this.lbl化学品.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl化学品.MinSize = new System.Drawing.Size(109, 24);
            this.lbl化学品.Name = "lbl化学品";
            this.lbl化学品.Size = new System.Drawing.Size(229, 24);
            this.lbl化学品.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl化学品.Text = "化学品:";
            this.lbl化学品.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl化学品.TextSize = new System.Drawing.Size(110, 20);
            this.lbl化学品.TextToControlDistance = 5;
            // 
            // lbl毒物
            // 
            this.lbl毒物.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl毒物.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl毒物.Control = this.txt毒物;
            this.lbl毒物.CustomizationFormText = "毒物";
            this.lbl毒物.Location = new System.Drawing.Point(229, 168);
            this.lbl毒物.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl毒物.MinSize = new System.Drawing.Size(109, 24);
            this.lbl毒物.Name = "lbl毒物";
            this.lbl毒物.Size = new System.Drawing.Size(207, 24);
            this.lbl毒物.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl毒物.Text = "毒物";
            this.lbl毒物.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl毒物.TextSize = new System.Drawing.Size(80, 20);
            this.lbl毒物.TextToControlDistance = 5;
            // 
            // lbl遗传病史
            // 
            this.lbl遗传病史.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl遗传病史.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl遗传病史.AppearanceItemCaption.Options.UseFont = true;
            this.lbl遗传病史.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl遗传病史.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl遗传病史.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl遗传病史.Control = this.txt遗传病史;
            this.lbl遗传病史.CustomizationFormText = "遗传病史";
            this.lbl遗传病史.Location = new System.Drawing.Point(0, 192);
            this.lbl遗传病史.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl遗传病史.MinSize = new System.Drawing.Size(109, 24);
            this.lbl遗传病史.Name = "lbl遗传病史";
            this.lbl遗传病史.Size = new System.Drawing.Size(729, 24);
            this.lbl遗传病史.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl遗传病史.Text = "遗传病史";
            this.lbl遗传病史.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl遗传病史.TextSize = new System.Drawing.Size(110, 20);
            this.lbl遗传病史.TextToControlDistance = 5;
            // 
            // lbl残疾情况
            // 
            this.lbl残疾情况.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lbl残疾情况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.lbl残疾情况.AppearanceItemCaption.Options.UseFont = true;
            this.lbl残疾情况.AppearanceItemCaption.Options.UseForeColor = true;
            this.lbl残疾情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl残疾情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl残疾情况.Control = this.txt残疾情况;
            this.lbl残疾情况.CustomizationFormText = "残疾情况";
            this.lbl残疾情况.Location = new System.Drawing.Point(0, 216);
            this.lbl残疾情况.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl残疾情况.MinSize = new System.Drawing.Size(109, 24);
            this.lbl残疾情况.Name = "lbl残疾情况";
            this.lbl残疾情况.Size = new System.Drawing.Size(729, 24);
            this.lbl残疾情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl残疾情况.Text = "残疾情况";
            this.lbl残疾情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl残疾情况.TextSize = new System.Drawing.Size(110, 20);
            this.lbl残疾情况.TextToControlDistance = 5;
            // 
            // lbl疾病
            // 
            this.lbl疾病.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl疾病.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl疾病.Control = this.txt疾病;
            this.lbl疾病.CustomizationFormText = "疾病：";
            this.lbl疾病.Location = new System.Drawing.Point(14, 26);
            this.lbl疾病.MinSize = new System.Drawing.Size(133, 24);
            this.lbl疾病.Name = "lbl疾病";
            this.lbl疾病.Size = new System.Drawing.Size(715, 24);
            this.lbl疾病.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl疾病.Text = "疾病：";
            this.lbl疾病.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl疾病.TextSize = new System.Drawing.Size(90, 20);
            this.lbl疾病.TextToControlDistance = 5;
            // 
            // lbl手术
            // 
            this.lbl手术.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl手术.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl手术.Control = this.txt手术;
            this.lbl手术.CustomizationFormText = "手术：";
            this.lbl手术.Location = new System.Drawing.Point(14, 50);
            this.lbl手术.MinSize = new System.Drawing.Size(133, 24);
            this.lbl手术.Name = "lbl手术";
            this.lbl手术.Size = new System.Drawing.Size(715, 24);
            this.lbl手术.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl手术.Text = "手术：";
            this.lbl手术.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl手术.TextSize = new System.Drawing.Size(90, 20);
            this.lbl手术.TextToControlDistance = 5;
            // 
            // lbl外伤
            // 
            this.lbl外伤.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl外伤.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl外伤.Control = this.txt外伤;
            this.lbl外伤.CustomizationFormText = "外伤：";
            this.lbl外伤.Location = new System.Drawing.Point(14, 74);
            this.lbl外伤.MinSize = new System.Drawing.Size(133, 24);
            this.lbl外伤.Name = "lbl外伤";
            this.lbl外伤.Size = new System.Drawing.Size(715, 24);
            this.lbl外伤.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl外伤.Text = "外伤：";
            this.lbl外伤.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl外伤.TextSize = new System.Drawing.Size(90, 20);
            this.lbl外伤.TextToControlDistance = 5;
            // 
            // lbl输血
            // 
            this.lbl输血.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl输血.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl输血.Control = this.txt输血;
            this.lbl输血.CustomizationFormText = "输血：";
            this.lbl输血.Location = new System.Drawing.Point(14, 98);
            this.lbl输血.MinSize = new System.Drawing.Size(133, 24);
            this.lbl输血.Name = "lbl输血";
            this.lbl输血.Size = new System.Drawing.Size(715, 24);
            this.lbl输血.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl输血.Text = "输血：";
            this.lbl输血.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl输血.TextSize = new System.Drawing.Size(90, 20);
            this.lbl输血.TextToControlDistance = 5;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "layoutControlGroup7";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem50});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 26);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Size = new System.Drawing.Size(14, 96);
            this.layoutControlGroup7.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup7.Text = "layoutControlGroup7";
            this.layoutControlGroup7.TextVisible = false;
            // 
            // simpleLabelItem50
            // 
            this.simpleLabelItem50.AllowHotTrack = false;
            this.simpleLabelItem50.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem50.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem50.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.simpleLabelItem50.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem50.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem50.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem50.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem50.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem50.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.simpleLabelItem50.CustomizationFormText = "既往史";
            this.simpleLabelItem50.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem50.MinSize = new System.Drawing.Size(10, 18);
            this.simpleLabelItem50.Name = "simpleLabelItem50";
            this.simpleLabelItem50.Size = new System.Drawing.Size(12, 94);
            this.simpleLabelItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem50.Text = "既往史";
            this.simpleLabelItem50.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem50.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "layoutControlGroup8";
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.simpleLabelItem57});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 148);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup8.Size = new System.Drawing.Size(729, 20);
            this.layoutControlGroup8.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup8.Text = "layoutControlGroup8";
            this.layoutControlGroup8.TextVisible = false;
            // 
            // simpleLabelItem57
            // 
            this.simpleLabelItem57.AllowHotTrack = false;
            this.simpleLabelItem57.AppearanceItemCaption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.simpleLabelItem57.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.simpleLabelItem57.AppearanceItemCaption.ForeColor = System.Drawing.Color.Blue;
            this.simpleLabelItem57.AppearanceItemCaption.Options.UseBackColor = true;
            this.simpleLabelItem57.AppearanceItemCaption.Options.UseFont = true;
            this.simpleLabelItem57.AppearanceItemCaption.Options.UseForeColor = true;
            this.simpleLabelItem57.AppearanceItemCaption.Options.UseTextOptions = true;
            this.simpleLabelItem57.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.simpleLabelItem57.CustomizationFormText = "暴露史";
            this.simpleLabelItem57.Location = new System.Drawing.Point(0, 0);
            this.simpleLabelItem57.MinSize = new System.Drawing.Size(120, 18);
            this.simpleLabelItem57.Name = "simpleLabelItem57";
            this.simpleLabelItem57.Size = new System.Drawing.Size(727, 18);
            this.simpleLabelItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.simpleLabelItem57.Text = "暴露史";
            this.simpleLabelItem57.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.simpleLabelItem57.TextSize = new System.Drawing.Size(50, 20);
            // 
            // simpleSeparator4
            // 
            this.simpleSeparator4.AllowHotTrack = false;
            this.simpleSeparator4.CustomizationFormText = "simpleSeparator4";
            this.simpleSeparator4.Location = new System.Drawing.Point(0, 24);
            this.simpleSeparator4.Name = "simpleSeparator4";
            this.simpleSeparator4.Size = new System.Drawing.Size(729, 2);
            this.simpleSeparator4.Text = "simpleSeparator4";
            // 
            // simpleSeparator5
            // 
            this.simpleSeparator5.AllowHotTrack = false;
            this.simpleSeparator5.CustomizationFormText = "simpleSeparator5";
            this.simpleSeparator5.Location = new System.Drawing.Point(0, 122);
            this.simpleSeparator5.Name = "simpleSeparator5";
            this.simpleSeparator5.Size = new System.Drawing.Size(729, 2);
            this.simpleSeparator5.Text = "simpleSeparator5";
            // 
            // group妇女
            // 
            this.group妇女.CustomizationFormText = "layoutControlGroup7";
            this.group妇女.GroupBordersVisible = false;
            this.group妇女.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lbl孕产情况,
            this.lbl孕次,
            this.lbl产次});
            this.group妇女.Location = new System.Drawing.Point(0, 672);
            this.group妇女.Name = "group妇女";
            this.group妇女.Size = new System.Drawing.Size(729, 24);
            this.group妇女.Text = "group妇女";
            // 
            // lbl孕产情况
            // 
            this.lbl孕产情况.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl孕产情况.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl孕产情况.Control = this.txt孕产情况;
            this.lbl孕产情况.CustomizationFormText = "孕产情况:";
            this.lbl孕产情况.Location = new System.Drawing.Point(0, 0);
            this.lbl孕产情况.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl孕产情况.MinSize = new System.Drawing.Size(173, 24);
            this.lbl孕产情况.Name = "lbl孕产情况";
            this.lbl孕产情况.Size = new System.Drawing.Size(284, 24);
            this.lbl孕产情况.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl孕产情况.Text = "孕产情况:";
            this.lbl孕产情况.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl孕产情况.TextSize = new System.Drawing.Size(110, 14);
            this.lbl孕产情况.TextToControlDistance = 5;
            // 
            // lbl孕次
            // 
            this.lbl孕次.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl孕次.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl孕次.Control = this.txt孕次;
            this.lbl孕次.CustomizationFormText = "孕次：";
            this.lbl孕次.Location = new System.Drawing.Point(284, 0);
            this.lbl孕次.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl孕次.MinSize = new System.Drawing.Size(173, 24);
            this.lbl孕次.Name = "lbl孕次";
            this.lbl孕次.Size = new System.Drawing.Size(245, 24);
            this.lbl孕次.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl孕次.Text = "孕次：";
            this.lbl孕次.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl孕次.TextSize = new System.Drawing.Size(80, 20);
            this.lbl孕次.TextToControlDistance = 5;
            // 
            // lbl产次
            // 
            this.lbl产次.AppearanceItemCaption.Options.UseTextOptions = true;
            this.lbl产次.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl产次.Control = this.txt产次;
            this.lbl产次.CustomizationFormText = "产次：";
            this.lbl产次.Location = new System.Drawing.Point(529, 0);
            this.lbl产次.MaxSize = new System.Drawing.Size(0, 24);
            this.lbl产次.MinSize = new System.Drawing.Size(173, 24);
            this.lbl产次.Name = "lbl产次";
            this.lbl产次.Size = new System.Drawing.Size(200, 24);
            this.lbl产次.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lbl产次.Text = "产次：";
            this.lbl产次.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.lbl产次.TextSize = new System.Drawing.Size(80, 20);
            this.lbl产次.TextToControlDistance = 5;
            // 
            // UC个人基本信息表_显示
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.panelControl2);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UC个人基本信息表_显示";
            this.Size = new System.Drawing.Size(748, 498);
            this.Load += new System.EventHandler(this.UC个人基本信息表_显示_Load);
            this.Controls.SetChildIndex(this.panelControl2, 0);
            this.Controls.SetChildIndex(this.layoutControl1, 0);
            this.Controls.SetChildIndex(this.txtFocusForSave, 0);
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ch二次复核.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ch复核标记.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt复核备注.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt签约医生.Properties)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit职工医疗保险卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit居民医疗保险卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit贫困救助卡号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk医疗费用支付方式7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit医疗费用支付方式其他.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家庭结构.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家庭人口数.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt户主身份证号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt户主姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案位置.Properties)).EndInit();
            this.flow外出.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外出地址.Properties)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chk是否流动人口.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否签约.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk是否贫困人口.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt输血.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt外伤.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt手术.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt疾病.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt产次.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt孕产情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt禽畜栏.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt厕所.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt饮水.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt燃料类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt厨房排风设施.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt残疾情况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt遗传病史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt毒物.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt化学品.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt射线.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt药物过敏史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt居住地址.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案类别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt新农合号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt所属片区.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医疗保险号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt医疗费用支付方式.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt婚姻状况.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt劳动强度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt职业.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt文化程度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt血型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt与户主关系.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt民族.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt常住类型.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt联系人姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt本人电话.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt工作单位.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt证件号码.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt完整度.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt缺项.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案状态.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt出生日期.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt性别.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt姓名.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt档案号.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt家族史.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit签字时间.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit本人或家属签字.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl劳动强度)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl医疗费用支付方式)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl档案号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl姓名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl性别)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl出生日期)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl档案状态)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl缺项)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl完整度)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl证件号码)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl工作单位)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl联系人姓名)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl联系人电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl本人电话)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl常住类型)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl民族)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl与户主关系)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl血型)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl文化程度)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl职业)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl婚姻状况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl医疗保险号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl所属片区)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl新农合号)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl档案类别)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl居住地址)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl医疗支付方式)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem79)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem修改时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem89)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近更新时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt录入人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem85)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem91)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt当前所属机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl本人或家属签字)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl签字时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem修改人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt最近修改人)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt创建机构)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem84)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txt调查时间)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl厨房排风设施)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl燃料类型)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl饮水)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl厕所)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl禽畜栏)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl药物过敏史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl家族史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl射线)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl化学品)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl毒物)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl遗传病史)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl残疾情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl疾病)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl手术)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl外伤)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl输血)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleLabelItem57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group妇女)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl孕产情况)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl孕次)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lbl产次)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btn修改;
        private DevExpress.XtraEditors.SimpleButton btn新建家庭成员档案;
        private DevExpress.XtraEditors.SimpleButton btn查看照片;
        private DevExpress.XtraEditors.SimpleButton btn导出;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem50;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem57;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem68;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem79;
        private DevExpress.XtraLayout.SimpleLabelItem txt最近更新时间;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem修改时间;
        private DevExpress.XtraLayout.SimpleLabelItem txt录入时间;
        private DevExpress.XtraLayout.SimpleLabelItem txt调查时间;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem84;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem85;
        private DevExpress.XtraLayout.SimpleLabelItem txt录入人;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem修改人;
        private DevExpress.XtraLayout.SimpleLabelItem txt最近修改人;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem89;
        private DevExpress.XtraLayout.SimpleLabelItem txt创建机构;
        private DevExpress.XtraLayout.SimpleLabelItem simpleLabelItem91;
        private DevExpress.XtraLayout.SimpleLabelItem txt当前所属机构;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraEditors.TextEdit txt性别;
        private DevExpress.XtraEditors.TextEdit txt姓名;
        private DevExpress.XtraEditors.TextEdit txt档案号;
        private DevExpress.XtraLayout.LayoutControlItem lbl档案号;
        private DevExpress.XtraLayout.LayoutControlItem lbl姓名;
        private DevExpress.XtraLayout.LayoutControlItem lbl性别;
        private DevExpress.XtraEditors.TextEdit txt完整度;
        private DevExpress.XtraEditors.TextEdit txt缺项;
        private DevExpress.XtraEditors.TextEdit txt档案状态;
        private DevExpress.XtraEditors.TextEdit txt出生日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl出生日期;
        private DevExpress.XtraLayout.LayoutControlItem lbl档案状态;
        private DevExpress.XtraLayout.LayoutControlItem lbl缺项;
        private DevExpress.XtraLayout.LayoutControlItem lbl完整度;
        private DevExpress.XtraEditors.TextEdit txt与户主关系;
        private DevExpress.XtraEditors.TextEdit txt民族;
        private DevExpress.XtraEditors.TextEdit txt常住类型;
        private DevExpress.XtraEditors.TextEdit txt联系人电话;
        private DevExpress.XtraEditors.TextEdit txt联系人姓名;
        private DevExpress.XtraEditors.TextEdit txt本人电话;
        private DevExpress.XtraEditors.TextEdit txt工作单位;
        private DevExpress.XtraEditors.TextEdit txt证件号码;
        private DevExpress.XtraLayout.LayoutControlItem lbl证件号码;
        private DevExpress.XtraLayout.LayoutControlItem lbl工作单位;
        private DevExpress.XtraLayout.LayoutControlItem lbl联系人姓名;
        private DevExpress.XtraLayout.LayoutControlItem lbl联系人电话;
        private DevExpress.XtraLayout.LayoutControlItem lbl本人电话;
        private DevExpress.XtraLayout.LayoutControlItem lbl常住类型;
        private DevExpress.XtraLayout.LayoutControlItem lbl民族;
        private DevExpress.XtraLayout.LayoutControlItem lbl与户主关系;
        private DevExpress.XtraEditors.TextEdit txt居住地址;
        private DevExpress.XtraEditors.TextEdit txt档案类别;
        private DevExpress.XtraEditors.TextEdit txt新农合号;
        private DevExpress.XtraEditors.TextEdit txt所属片区;
        private DevExpress.XtraEditors.TextEdit txt医疗保险号;
        private DevExpress.XtraEditors.TextEdit txt医疗费用支付方式;
        private DevExpress.XtraEditors.TextEdit txt婚姻状况;
        private DevExpress.XtraEditors.TextEdit txt劳动强度;
        private DevExpress.XtraEditors.TextEdit txt职业;
        private DevExpress.XtraEditors.TextEdit txt文化程度;
        private DevExpress.XtraEditors.TextEdit txtRH;
        private DevExpress.XtraEditors.TextEdit txt血型;
        private DevExpress.XtraLayout.LayoutControlItem lbl血型;
        private DevExpress.XtraLayout.LayoutControlItem lblRH;
        private DevExpress.XtraLayout.LayoutControlItem lbl文化程度;
        private DevExpress.XtraLayout.LayoutControlItem lbl职业;
        private DevExpress.XtraLayout.LayoutControlItem lbl劳动强度;
        private DevExpress.XtraLayout.LayoutControlItem lbl婚姻状况;
        private DevExpress.XtraLayout.LayoutControlItem lbl医疗费用支付方式;
        private DevExpress.XtraLayout.LayoutControlItem lbl医疗保险号;
        private DevExpress.XtraLayout.LayoutControlItem lbl所属片区;
        private DevExpress.XtraLayout.LayoutControlItem lbl新农合号;
        private DevExpress.XtraLayout.LayoutControlItem lbl档案类别;
        private DevExpress.XtraLayout.LayoutControlItem lbl居住地址;
        private DevExpress.XtraEditors.TextEdit txt禽畜栏;
        private DevExpress.XtraEditors.TextEdit txt厕所;
        private DevExpress.XtraEditors.TextEdit txt饮水;
        private DevExpress.XtraEditors.TextEdit txt燃料类型;
        private DevExpress.XtraEditors.TextEdit txt厨房排风设施;
        private DevExpress.XtraEditors.TextEdit txt残疾情况;
        private DevExpress.XtraEditors.TextEdit txt遗传病史;
        private DevExpress.XtraEditors.TextEdit txt毒物;
        private DevExpress.XtraEditors.TextEdit txt化学品;
        private DevExpress.XtraEditors.TextEdit txt射线;
        private DevExpress.XtraEditors.TextEdit txt药物过敏史;
        private DevExpress.XtraLayout.LayoutControlItem lbl厨房排风设施;
        private DevExpress.XtraLayout.LayoutControlItem lbl燃料类型;
        private DevExpress.XtraLayout.LayoutControlItem lbl饮水;
        private DevExpress.XtraLayout.LayoutControlItem lbl厕所;
        private DevExpress.XtraLayout.LayoutControlItem lbl禽畜栏;
        private DevExpress.XtraLayout.LayoutControlItem lbl药物过敏史;
        private DevExpress.XtraLayout.LayoutControlItem lbl家族史;
        private DevExpress.XtraLayout.LayoutControlItem lbl射线;
        private DevExpress.XtraLayout.LayoutControlItem lbl化学品;
        private DevExpress.XtraLayout.LayoutControlItem lbl毒物;
        private DevExpress.XtraLayout.LayoutControlItem lbl遗传病史;
        private DevExpress.XtraLayout.LayoutControlItem lbl残疾情况;
        private DevExpress.XtraEditors.TextEdit txt孕次;
        private DevExpress.XtraEditors.TextEdit txt产次;
        private DevExpress.XtraEditors.TextEdit txt孕产情况;
        private DevExpress.XtraLayout.LayoutControlItem lbl孕产情况;
        private DevExpress.XtraLayout.LayoutControlItem lbl产次;
        private DevExpress.XtraLayout.LayoutControlItem lbl孕次;
        private DevExpress.XtraLayout.LayoutControlGroup group妇女;
        private DevExpress.XtraEditors.MemoEdit txt输血;
        private DevExpress.XtraEditors.MemoEdit txt外伤;
        private DevExpress.XtraEditors.MemoEdit txt手术;
        private DevExpress.XtraEditors.MemoEdit txt疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl疾病;
        private DevExpress.XtraLayout.LayoutControlItem lbl手术;
        private DevExpress.XtraLayout.LayoutControlItem lbl外伤;
        private DevExpress.XtraLayout.LayoutControlItem lbl输血;
        private DevExpress.XtraEditors.MemoEdit txt家族史;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator4;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private DevExpress.XtraEditors.CheckEdit chk是否流动人口;
        private DevExpress.XtraEditors.CheckEdit chk是否签约;
        private DevExpress.XtraEditors.CheckEdit chk是否贫困人口;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit txt外出地址;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.Windows.Forms.FlowLayoutPanel flow外出;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.TextEdit txt档案位置;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.MemoEdit txt居住情况;
        private DevExpress.XtraEditors.MemoEdit txt家庭结构;
        private DevExpress.XtraEditors.MemoEdit txt家庭人口数;
        private DevExpress.XtraEditors.MemoEdit txt户主身份证号;
        private DevExpress.XtraEditors.MemoEdit txt户主姓名;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem lbl本人或家属签字;
        private DevExpress.XtraLayout.LayoutControlItem lbl签字时间;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式1;
        private DevExpress.XtraEditors.TextEdit textEdit职工医疗保险卡号;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式2;
        private DevExpress.XtraEditors.TextEdit textEdit居民医疗保险卡号;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式3;
        private DevExpress.XtraEditors.TextEdit textEdit贫困救助卡号;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式4;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式5;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式6;
        private DevExpress.XtraEditors.CheckEdit chk医疗费用支付方式7;
        private DevExpress.XtraEditors.TextEdit textEdit医疗费用支付方式其他;
        private DevExpress.XtraLayout.LayoutControlItem lbl医疗支付方式;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.TextEdit dateEdit签字时间;
        private DevExpress.XtraEditors.TextEdit txt签约医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.PictureEdit textEdit本人或家属签字;
        private DevExpress.XtraEditors.CheckEdit ch复核标记;
        private DevExpress.XtraEditors.TextEdit txt复核备注;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraEditors.SimpleButton btn签约医生;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.CheckEdit ch二次复核;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.SimpleButton sBut_统合打印;
    }
}