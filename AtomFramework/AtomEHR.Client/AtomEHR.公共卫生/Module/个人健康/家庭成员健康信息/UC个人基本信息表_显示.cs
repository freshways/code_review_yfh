﻿using AtomEHR.公共卫生.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AtomEHR.Business;
using AtomEHR.Models;
using AtomEHR.Library;
using AtomEHR.Common;
using DevExpress.XtraReports.UI;
using AtomEHR.公共卫生.ReportFrm;

namespace AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息
{
    public partial class UC个人基本信息表_显示 : UserControlBase
    {
        public frm个人健康 frm = null;
        bll健康档案 _Bll = new bll健康档案();
        DataSet _ds;
        string docNo;
        string familyNo;
        public UC个人基本信息表_显示()
        {
            InitializeComponent();
        }

        public UC个人基本信息表_显示(Form parentForm)
        {
            InitializeComponent();
            frm = (frm个人健康)parentForm;
            docNo = frm._docNo;
            familyNo = frm._familyDocNo;

            _ds = _Bll.GetOneUserByKeyForShow(docNo, true);
            //frm.LoadFamilyTree(_ds.Tables[tb_健康档案.__TableName]);
            //DoBindingDataSource(_ds);
            if (string.IsNullOrEmpty(familyNo))//不存在家庭档案编号，说明此人不属于任何家庭
            {
                this.btn新建家庭成员档案.Visible = false;
            }
           
        }

        BackgroundWorker bgInvoke = new BackgroundWorker();
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private void UC个人基本信息表_显示_Load(object sender, EventArgs e)
        {
            bgInvoke.DoWork += bgInvoke_DoWork;
            bgInvoke.RunWorkerCompleted += bgInvoke_RunWorkerCompleted;
            int isallow = DataDictCache.Cache.IsAllow延时加载数据();
            if (isallow > 0 && !Loginer.CurrentUser.IsSubAdmin())
            {
                if (!bgInvoke.IsBusy)
                    bgInvoke.RunWorkerAsync(isallow);
                //初始化timer
                timer.Interval = (1000 * 3); //3秒执行一次
                timer.Tick += timer_Tick;
                this.txt最近更新时间.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.txt最近修改人.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.simpleLabelItem修改时间.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                this.simpleLabelItem修改人.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
            else
            {
                DoBindingDataSource(_ds);
            }
        }

        #region 延时加载
        void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                _ds = _Bll.GetOneUserByKeyForShow(docNo, true);
                this.txt本人电话.Text = _ds.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.本人电话].ToString();
            }
            catch (Exception)
            {

            }
        }
        void bgInvoke_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm();
                DoBindingDataSource(_ds);

                //timer.Start();
                //this.OnLoad(e);
            }
            catch (Exception)
            {

            }
        }

        void bgInvoke_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int iSS = Convert.ToInt32(e.Argument);

                DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(typeof(WaitForm1));

                for (int i = 0; i < iSS; i++)
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }
            catch (Exception)
            {

            }
        }

        #endregion

        private void DoBindingDataSource(DataSet _ds)
        {
            DataTable table健康档案 = _ds.Tables[tb_健康档案.__TableName];
            DataTable table健康状态 = _ds.Tables[tb_健康档案_健康状态.__TableName];
            DataTable table既往病史 = _ds.Tables[tb_健康档案_既往病史.__TableName];
            DataTable table家族病史 = _ds.Tables[tb_健康档案_家族病史.__TableName];

            if (table健康档案.Rows.Count == 1)
            {
                this.txt档案号.Text = table健康档案.Rows[0][tb_健康档案.__KeyName].ToString();
                this.txt姓名.Text = util.DESEncrypt.DES解密(table健康档案.Rows[0][tb_健康档案.姓名].ToString());
                this.txt性别.Text = table健康档案.Rows[0][tb_健康档案.性别].ToString();
                if (this.txt性别.Text == "男")
                    this.group妇女.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                else
                {
                    this.group妇女.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    this.txt孕产情况.Text = table健康档案.Rows[0][tb_健康档案.怀孕情况].ToString();
                    this.txt孕次.Text = table健康档案.Rows[0][tb_健康档案.孕次].ToString();
                    this.txt产次.Text = table健康档案.Rows[0][tb_健康档案.产次].ToString();
                }
                this.txt出生日期.Text = table健康档案.Rows[0][tb_健康档案.出生日期].ToString();
                this.txt档案状态.Text = table健康档案.Rows[0][tb_健康档案.档案状态].ToString();
                this.txt缺项.Text = table健康档案.Rows[0][tb_健康档案.缺项].ToString();
                this.txt完整度.Text = table健康档案.Rows[0][tb_健康档案.完整度].ToString() + "%";
                this.txt档案位置.Text = table健康档案.Rows[0][tb_健康档案.档案位置].ToString();

                this.txt证件号码.Text = table健康档案.Rows[0][tb_健康档案.证件类型] + ":" + table健康档案.Rows[0][tb_健康档案.身份证号];
                //str_身份证号 = table健康档案.Rows[0][tb_健康档案.身份证号].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.身份证号].ToString()))
                {
                    lbl证件号码.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txt本人电话.Text = table健康档案.Rows[0][tb_健康档案.本人电话] == null ? "" : Convert.ToString(table健康档案.Rows[0][tb_健康档案.本人电话]);
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.本人电话].ToString()))
                {
                    lbl本人电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txt联系人姓名.Text = table健康档案.Rows[0][tb_健康档案.联系人姓名] == null ? "" : table健康档案.Rows[0][tb_健康档案.联系人姓名].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.联系人姓名].ToString()))
                {
                    lbl联系人姓名.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txt联系人电话.Text = table健康档案.Rows[0][tb_健康档案.联系人电话] == null ? "" : table健康档案.Rows[0][tb_健康档案.联系人电话].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.联系人电话].ToString()))
                {
                    lbl联系人电话.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txt常住类型.Text = table健康档案.Rows[0][tb_健康档案.常住类型] == null ? "" : table健康档案.Rows[0][tb_健康档案.常住类型].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.常住类型].ToString()))
                {
                    lbl常住类型.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txt民族.Text = table健康档案.Rows[0][tb_健康档案.民族] == null ? "" : table健康档案.Rows[0][tb_健康档案.民族].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.民族].ToString()))
                {
                    lbl民族.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txt与户主关系.Text = table健康档案.Rows[0][tb_健康档案.与户主关系] == null ? "" : table健康档案.Rows[0][tb_健康档案.与户主关系].ToString();

                this.txt职业.Text = table健康档案.Rows[0][tb_健康档案.职业] == null ? "" : table健康档案.Rows[0][tb_健康档案.职业].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.职业].ToString()))
                {
                    lbl职业.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txt文化程度.Text = table健康档案.Rows[0][tb_健康档案.文化程度] as string == null ? "" : table健康档案.Rows[0][tb_健康档案.文化程度].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.文化程度].ToString()))
                {
                    lbl文化程度.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txt劳动强度.Text = table健康档案.Rows[0][tb_健康档案.劳动强度] as string == null ? "" : table健康档案.Rows[0][tb_健康档案.劳动强度].ToString();
                this.txt婚姻状况.Text = table健康档案.Rows[0][tb_健康档案.婚姻状况] as string == null ? "" : table健康档案.Rows[0][tb_健康档案.婚姻状况].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.婚姻状况].ToString()))
                {
                    lbl婚姻状况.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.chk是否流动人口.Checked = string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.是否流动人口].ToString()) ? false : true;
                this.chk是否贫困人口.Checked = string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.是否贫困人口].ToString()) ? false : true;
                this.chk是否签约.Checked = string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.是否签约].ToString()) ? false : true;

                //this.txt医疗费用支付方式.Text = table健康档案.Rows[0][tb_健康档案.医疗费支付类型] == null ? "" : table健康档案.Rows[0][tb_健康档案.医疗费支付类型].ToString();
                //if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.医疗费支付类型].ToString()))
                //{
                //    lbl医疗费用支付方式.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                //}
                string str医疗费用支付方式 = table健康档案.Rows[0][tb_健康档案.医疗费支付类型] == null ? "" : table健康档案.Rows[0][tb_健康档案.医疗费支付类型].ToString();
                char[] c分隔符 = { ',' };
                string[] strs医疗费用支付方式 = str医疗费用支付方式.Split(c分隔符);
                for (int index = 0; strs医疗费用支付方式.Length > index;index++ )
                {
                    switch (strs医疗费用支付方式[index])
                    {
                        case "城镇或省直职工基本医疗保险":
                            this.chk医疗费用支付方式1.Checked = true;
                            this.textEdit职工医疗保险卡号.Text = table健康档案.Rows[0][tb_健康档案.医疗保险号].ToString();
                            break;
                        case "居民基本医疗保险":
                            this.chk医疗费用支付方式2.Checked = true;
                            this.textEdit居民医疗保险卡号.Text = table健康档案.Rows[0][tb_健康档案.新农合号].ToString();
                            break;
                        case "贫困救助":
                            this.chk医疗费用支付方式3.Checked = true;
                            this.textEdit贫困救助卡号.Text = table健康档案.Rows[0][tb_健康档案.贫困救助卡号].ToString();
                            break;
                        case "商业医疗保险":
                            this.chk医疗费用支付方式4.Checked = true;
                            break;
                        case "全公费":
                            this.chk医疗费用支付方式5.Checked = true;
                            break;
                        case "全自费":
                            this.chk医疗费用支付方式6.Checked = true;
                            break;
                        case "其他":
                            this.chk医疗费用支付方式7.Checked = true;
                            this.textEdit医疗费用支付方式其他.Text = table健康档案.Rows[0][tb_健康档案.医疗费用支付类型其他].ToString();
                            break;
                        default:
                            break;
                    }

                }
                if (string.IsNullOrWhiteSpace(str医疗费用支付方式))
                {
                    lbl医疗支付方式.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }

                this.txt医疗保险号.Text = table健康档案.Rows[0][tb_健康档案.医疗保险号] == null ? "" : table健康档案.Rows[0][tb_健康档案.医疗保险号].ToString();
                this.txt新农合号.Text = table健康档案.Rows[0][tb_健康档案.新农合号] == null ? "" : table健康档案.Rows[0][tb_健康档案.新农合号].ToString();
                this.txt所属片区.Text = table健康档案.Rows[0][tb_健康档案.所属片区] == null ? "" : table健康档案.Rows[0][tb_健康档案.所属片区].ToString();
                this.txt档案类别.Text = table健康档案.Rows[0][tb_健康档案.档案类别] == null ? "" : table健康档案.Rows[0][tb_健康档案.档案类别].ToString();
                this.txt居住地址.Text = table健康档案.Rows[0][tb_健康档案.市].ToString() + table健康档案.Rows[0][tb_健康档案.区].ToString() + table健康档案.Rows[0][tb_健康档案.街道].ToString() + table健康档案.Rows[0][tb_健康档案.居委会].ToString() + table健康档案.Rows[0][tb_健康档案.居住地址].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.居住地址].ToString()))
                {
                    lbl居住地址.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txt工作单位.Text = table健康档案.Rows[0][tb_健康档案.工作单位] == null ? "" : table健康档案.Rows[0][tb_健康档案.工作单位].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.工作单位].ToString()))
                {
                    lbl工作单位.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }

                this.txt厨房排风设施.Text = table健康档案.Rows[0][tb_健康档案.厨房排气设施] as string == null ? "无" : table健康档案.Rows[0][tb_健康档案.厨房排气设施].ToString();
                this.txt燃料类型.Text = table健康档案.Rows[0][tb_健康档案.燃料类型] as string == null ? "无" : table健康档案.Rows[0][tb_健康档案.燃料类型].ToString();
                this.txt饮水.Text = table健康档案.Rows[0][tb_健康档案.饮水] as string == null ? "无" : table健康档案.Rows[0][tb_健康档案.饮水].ToString();
                this.txt禽畜栏.Text = table健康档案.Rows[0][tb_健康档案.禽畜栏] as string == null ? "无" : table健康档案.Rows[0][tb_健康档案.禽畜栏].ToString();
                this.txt厕所.Text = table健康档案.Rows[0][tb_健康档案.厕所] as string == null ? "无" : table健康档案.Rows[0][tb_健康档案.厕所].ToString();
                this.txt外出地址.Text = table健康档案.Rows[0][tb_健康档案.外出地址] as string == null ? "" : table健康档案.Rows[0][tb_健康档案.外出地址].ToString();
                SetFlowLayoutResult(table健康档案.Rows[0][tb_健康档案.外出情况].ToString(), flow外出);

                string str与户主关系 = this.txt与户主关系.Text.Trim();
                if (!string.IsNullOrEmpty(str与户主关系) && str与户主关系=="户主")
                {
                    this.txt户主姓名.Text = util.DESEncrypt.DES解密(table健康档案.Rows[0][tb_健康档案.姓名].ToString());
                    this.txt户主身份证号.Text = table健康档案.Rows[0][tb_健康档案.身份证号].ToString();
                }
                else
                {
                    this.txt户主姓名.Text = util.DESEncrypt.DES解密(table健康档案.Rows[0][tb_健康档案.户主姓名].ToString());//table健康档案.Rows[0][tb_健康档案.户主姓名].ToString();
                    this.txt户主身份证号.Text = table健康档案.Rows[0][tb_健康档案.户主身份证号].ToString();
                }
                this.txt家庭人口数.Text = table健康档案.Rows[0][tb_健康档案.家庭人口数].ToString();
                this.txt家庭结构.Text = table健康档案.Rows[0][tb_健康档案.家庭结构].ToString();
                this.txt居住情况.Text = table健康档案.Rows[0][tb_健康档案.居住情况] == null ? "" : table健康档案.Rows[0][tb_健康档案.居住情况].ToString();
                
                //this.textEdit本人或家属签字.Text = table健康档案.Rows[0][tb_健康档案.本人或家属签字].ToString();
                string SQ = table健康档案.Rows[0][tb_健康档案.本人或家属签字].ToString();

                try
                {//手签图片转换，异常显示无签名
                    Byte[] bitmapData = new Byte[SQ.Length];

                    bitmapData = Convert.FromBase64String(SQ);

                    System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);

                    this.textEdit本人或家属签字.Image = Image.FromStream(streamBitmap);
                }
                catch
                {
                    if (!string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.复核备注].ToString()))
                        SQ = table健康档案.Rows[0][tb_健康档案.复核备注].ToString();
                    if (string.IsNullOrEmpty(SQ))
                        SQ = "暂无电子签名";
                    Graphics g = Graphics.FromImage(new Bitmap(1, 1));
                    Font font = new Font("宋体", 9);
                    SizeF sizeF = g.MeasureString(SQ, font); //测量出字体的高度和宽度  
                    Brush brush; //笔刷，颜色  
                    brush = Brushes.Black;
                    PointF pf = new PointF(0, 0);
                    Bitmap img = new Bitmap(Convert.ToInt32(sizeF.Width), Convert.ToInt32(sizeF.Height));
                    g = Graphics.FromImage(img);
                    g.DrawString(SQ, font, brush, pf);
                    this.textEdit本人或家属签字.Image = img;
                }                
                
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.本人或家属签字].ToString()) &&
                    string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.复核备注].ToString()))
                {
                    lbl本人或家属签字.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.dateEdit签字时间.Text = table健康档案.Rows[0][tb_健康档案.签字时间].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.签字时间].ToString()))
                {
                    lbl签字时间.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txt复核备注.Text = table健康档案.Rows[0][tb_健康档案.复核备注].ToString();
                if (string.IsNullOrEmpty(table健康档案.Rows[0][tb_健康档案.复核标记].ToString()))
                    this.ch复核标记.Checked = false;
                else
                    this.ch复核标记.Checked = true;
                if (table健康档案.Rows[0][tb_健康档案.二次复核].Equals("1"))
                    this.ch二次复核.Checked = true;
                else
                    this.ch二次复核.Checked = false;

                this.txt调查时间.Text = table健康档案.Rows[0][tb_健康档案.调查时间].ToString();
                this.txt创建机构.Text = _Bll.Return机构名称(table健康档案.Rows[0][tb_健康档案.创建机构].ToString());
                this.txt当前所属机构.Text = _Bll.Return机构名称(table健康档案.Rows[0][tb_健康档案.所属机构].ToString());
                this.txt最近修改人.Text = _Bll.Return用户名称(table健康档案.Rows[0][tb_健康档案.修改人].ToString());
                this.txt最近更新时间.Text = table健康档案.Rows[0][tb_健康档案.修改时间].ToString();
                this.txt录入时间.Text = table健康档案.Rows[0][tb_健康档案.创建时间].ToString();
                this.txt录入人.Text = _Bll.Return用户名称(table健康档案.Rows[0][tb_健康档案.创建人].ToString());

            }

            if (table健康状态.Rows.Count == 1)
            {
                this.txt血型.Text = table健康状态.Rows[0][tb_健康档案_健康状态.血型] == null ? "" : table健康状态.Rows[0][tb_健康档案_健康状态.血型].ToString();
                if (string.IsNullOrEmpty(table健康状态.Rows[0][tb_健康档案_健康状态.血型].ToString()))
                {
                    lbl血型.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }
                this.txtRH.Text = table健康状态.Rows[0][tb_健康档案_健康状态.RH] == null ? "" : table健康状态.Rows[0][tb_健康档案_健康状态.RH].ToString();
                if (string.IsNullOrEmpty(table健康状态.Rows[0][tb_健康档案_健康状态.RH].ToString()))
                {
                    lblRH.AppearanceItemCaption.ForeColor = System.Drawing.Color.Red;
                }


                this.txt化学品.Text = table健康状态.Rows[0][tb_健康档案_健康状态.暴露史] as string == "2" ? "无" : table健康状态.Rows[0][tb_健康档案_健康状态.暴露史化学品].ToString();
                this.txt毒物.Text = table健康状态.Rows[0][tb_健康档案_健康状态.暴露史] as string == "2" ? "无" : table健康状态.Rows[0][tb_健康档案_健康状态.暴露史毒物].ToString();
                this.txt射线.Text = table健康状态.Rows[0][tb_健康档案_健康状态.暴露史] as string == "2" ? "无" : table健康状态.Rows[0][tb_健康档案_健康状态.暴露史射线].ToString();
                this.txt遗传病史.Text = table健康状态.Rows[0][tb_健康档案_健康状态.遗传病史有无] as string == "2" ? "无" : table健康状态.Rows[0][tb_健康档案_健康状态.遗传病史].ToString();
                this.txt残疾情况.Text = (table健康状态.Rows[0][tb_健康档案_健康状态.残疾情况] as string == null ? "无" : (table健康状态.Rows[0][tb_健康档案_健康状态.残疾情况].ToString())
                    + (table健康状态.Rows[0][tb_健康档案_健康状态.残疾其他] == "" ? "" : table健康状态.Rows[0][tb_健康档案_健康状态.残疾其他].ToString()));
                this.txt药物过敏史.Text = (table健康状态.Rows[0][tb_健康档案_健康状态.过敏史有无] as string == "2" ? "无" : table健康状态.Rows[0][tb_健康档案_健康状态.药物过敏史].ToString())
                     + (table健康状态.Rows[0][tb_健康档案_健康状态.过敏史其他] == null ? "" : table健康状态.Rows[0][tb_健康档案_健康状态.过敏史其他].ToString());

            }

            if (table既往病史.Rows.Count > 0)
            {
                DataRow[] rows疾病 = table既往病史.Select("疾病类型='疾病'");//获取为疾病类型的所有数据行
                if (rows疾病.Length > 0)
                {
                    int count = rows疾病.Length;
                    int length = 0;
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < rows疾病.Length; i++)
                    {
                        DataRow item = rows疾病[i];
                        string 疾病名称 = item[tb_健康档案_既往病史.疾病名称].ToString();
                        if (疾病名称.IndexOf(',') != -1)//存在多个
                        {
                            string[] itemList = 疾病名称.Split(',');
                            //count += itemList.Length - 1;
                            for (int j = 0; j < itemList.Length; j++)
                            {
                                if (string.IsNullOrEmpty(itemList[j])) continue;
                                string _疾病名称 = _Bll.ReturnDis字典显示("jb_gren", itemList[j]);
                                length++;
                                if (_疾病名称 == "其他")//其他
                                {
                                    if (j == itemList.Length - 1)
                                        builder.Append(_疾病名称 + ":" + item[tb_健康档案_既往病史.疾病其他].ToString().PadRight(10) + "确诊时间：" + item[tb_健康档案_既往病史.日期].ToString());
                                    else
                                        builder.AppendLine(_疾病名称 + ":" + item[tb_健康档案_既往病史.疾病其他].ToString().PadRight(10) + "确诊时间：" + item[tb_健康档案_既往病史.日期].ToString());
                                }
                                else
                                {
                                    if (j == itemList.Length - 1)
                                        builder.Append(_疾病名称.PadRight(10) + "确诊时间：" + item[tb_健康档案_既往病史.日期].ToString());
                                    else
                                        builder.AppendLine(_疾病名称.PadRight(10) + "确诊时间：" + item[tb_健康档案_既往病史.日期].ToString());
                                }
                            }
                        }
                        else//只有一条
                        {
                            string _疾病名称 = _Bll.ReturnDis字典显示("jb_gren", 疾病名称);
                            length++;
                            if (_疾病名称 == "其他")
                            {
                                if (i == rows疾病.Length - 1)
                                    builder.Append(_疾病名称 + ":" + item[tb_健康档案_既往病史.疾病其他].ToString().PadRight(10) + "确诊时间：" + item[tb_健康档案_既往病史.日期].ToString());
                                else
                                    builder.AppendLine(_疾病名称 + ":" + item[tb_健康档案_既往病史.疾病其他].ToString().PadRight(10) + "确诊时间：" + item[tb_健康档案_既往病史.日期].ToString());
                            }
                            else
                            {
                                if (i == rows疾病.Length - 1)
                                    builder.Append(_疾病名称.PadRight(10) + "确诊时间：" + item[tb_健康档案_既往病史.日期].ToString());
                                else
                                    builder.AppendLine(_疾病名称.PadRight(10) + "确诊时间：" + item[tb_健康档案_既往病史.日期].ToString());
                            }
                        }
                    }
                    this.txt疾病.Text = builder.ToString();
                    this.lbl疾病.MinSize = new System.Drawing.Size(109, 24 * length);
                    this.lbl疾病.Height = 24 * length;
                }
                else
                {
                    this.txt疾病.Text = "无";
                }

                DataRow[] rows手术 = table既往病史.Select("疾病类型='手术'");//获取为疾病类型的所有数据行
                if (rows手术.Length > 0)
                {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < rows手术.Length; i++)
                    {
                        DataRow item = rows手术[i];
                        string 疾病名称 = item[tb_健康档案_既往病史.疾病名称].ToString();
                        if (i == rows手术.Length - 1)
                            builder.Append(疾病名称.PadRight(10) + "确诊日期：" + item[tb_健康档案_既往病史.日期].ToString());
                        else
                            builder.AppendLine(疾病名称.PadRight(10) + "确诊日期：" + item[tb_健康档案_既往病史.日期].ToString());
                    }
                    this.txt手术.Text = builder.ToString();
                }
                else
                {
                    this.txt手术.Text = "无";
                }

                DataRow[] rows外伤 = table既往病史.Select("疾病类型='外伤'");//获取为疾病类型的所有数据行
                if (rows外伤.Length > 0)
                {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < rows外伤.Length; i++)
                    {
                        DataRow item = rows外伤[i];
                        string 疾病名称 = item[tb_健康档案_既往病史.疾病名称].ToString();
                        if (i == rows外伤.Length - 1)
                            builder.Append(疾病名称.PadRight(10) + "确诊日期：" + item[tb_健康档案_既往病史.日期].ToString());
                        else
                            builder.AppendLine(疾病名称.PadRight(10) + "确诊日期：" + item[tb_健康档案_既往病史.日期].ToString());
                    }
                    this.txt外伤.Text = builder.ToString();
                }
                else
                {
                    this.txt外伤.Text = "无";
                }

                DataRow[] rows输血 = table既往病史.Select("疾病类型='输血'");//获取为疾病类型的所有数据行
                if (rows输血.Length > 0)
                {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < rows输血.Length; i++)
                    {
                        DataRow item = rows输血[i];
                        string 疾病名称 = item[tb_健康档案_既往病史.疾病名称].ToString();
                        if (i == rows输血.Length - 1)
                            builder.Append(疾病名称.PadRight(10) + "确诊日期：" + item[tb_健康档案_既往病史.日期].ToString());
                        else
                            builder.AppendLine(疾病名称.PadRight(10) + "确诊日期：" + item[tb_健康档案_既往病史.日期].ToString());
                    }
                    this.txt输血.Text = builder.ToString();
                }
                else
                {
                    this.txt输血.Text = "无";
                }
            }
            else
            {
                this.txt输血.Text = "无";
                this.txt手术.Text = "无";
                this.txt外伤.Text = "无";
                this.txt疾病.Text = "无";
            }


            if (table家族病史.Rows.Count > 0)
            {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < table家族病史.Rows.Count; i++)
                {
                    string[] 疾病名称 = table家族病史.Rows[i][tb_健康档案_家族病史.家族病史].ToString().Split(',');
                    string 家族关系 = _Bll.ReturnDis字典显示("jzscy", table家族病史.Rows[i][tb_健康档案_家族病史.家族关系].ToString());
                    string _疾病名称 = string.Empty;
                    for (int j = 0; j < 疾病名称.Length; j++)
                    {
                        string a = _Bll.ReturnDis字典显示("jzsxin", 疾病名称[j]);
                        if (a == "其他")
                        {
                            _疾病名称 += table家族病史.Rows[i][tb_健康档案_家族病史.其他疾病] + ", ";
                        }
                        else
                        {
                            _疾病名称 += a + ", ";
                        }
                    }
                    if (i == table家族病史.Rows.Count - 1)//最后一个
                        builder.Append("关系：" + 家族关系.PadRight(10) + _疾病名称);
                    else
                        builder.AppendLine("关系：" + 家族关系.PadRight(10) + _疾病名称);
                }
                this.txt家族史.Text = builder.ToString();
                this.lbl家族史.MinSize = new Size(this.lbl家族史.Width, this.lbl家族史.MinSize.Height * table家族病史.Rows.Count);

            }
            else
            {
                this.txt家族史.Text = "无";
            }
        }
        private void btn新建家庭成员档案_Click(object sender, EventArgs e)
        {
            UC个人基本信息表 control = new UC个人基本信息表(frm, UpdateType.AddPeople);
            ShowControl(control, DockStyle.Fill);
        }

        private void btn修改_Click(object sender, EventArgs e)
        {
            // changed 删除孕产妇基本信息，由个人基本信息表代替，把个人基本信息表的编辑权限放给妇幼人员，不再是使用孕产妇所在卫生室的账号编辑。 ▽
            //if (canModifyBy同一机构(_ds.Tables[tb_健康档案.__TableName].Rows[0][tb_健康体检.所属机构].ToString()))     //old code
            string str档案创建机构 = _ds.Tables[tb_健康档案.__TableName].Rows[0][tb_健康档案.所属机构].ToString();
            if (canModifyBy同一机构(str档案创建机构)
                || Check是否下属机构(str档案创建机构))
            // changed 删除孕产妇基本信息，由个人基本信息表代替，把个人基本信息表的编辑权限放给妇幼人员，不再是使用孕产妇所在卫生室的账号编辑。 △
            {
                UC个人基本信息表 control;
                //2018年3月16日 yufh 修改，此处针对没有家庭档案的档案进行修复，没有家庭档案对其新增会造成后续的若干问题，比如基础表重复生成
                //if (string.IsNullOrEmpty(familyNo))//没有家庭档案编号的时候，需要在修改页面添加 一个家庭档案
                //{
                //    control = new UC个人基本信息表(frm, UpdateType.Add);
                //}
                //else//不需要添加家庭档案
                //{
                //    control = new UC个人基本信息表(frm, UpdateType.Modify);
                //}
                control = new UC个人基本信息表(frm, UpdateType.Modify);
                ShowControl(control, DockStyle.Fill);
            }
            else
            {
                Msg.Warning("对不起，您只能对本机构的业务数据进行此操作！");
            }
        }

        private void btn导出_Click(object sender, EventArgs e)
        {
            string _docNo = this.txt档案号.Text.Trim();
            if (!string.IsNullOrEmpty(_docNo))
            {
                report个人基本信息表 report = new report个人基本信息表(_docNo);
                ReportPrintTool tool = new ReportPrintTool(report);
                tool.ShowPreviewDialog();
            }
            else
            {
                Msg.ShowInformation("此人不存在个人档案编号，请确认！");
            }

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var idcard = "";
            if (!string.IsNullOrEmpty(this.txt证件号码.Text))
            {
                string[] sfzh = this.txt证件号码.Text.Split(':');
                if (sfzh.Length < 1)
                    return;
                idcard = sfzh[1];
            }
            string url = @"http://192.168.10.118:86/jihuashengyu/5second.aspx?IDcard=" + idcard;

            System.Diagnostics.Process.Start("IExplore.exe", url);
        }
        /// <summary>
        /// 家庭医生签约单独查询，2018年3月13日 118服务器崩溃导致变慢调整。 by ：yufh
        /// </summary>
        string S_身份证号 = string.Empty;
        private void btn签约医生_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(S_身份证号))
            {
                string doctorID = _Bll.GetQYYS(S_身份证号);
                if (!string.IsNullOrEmpty(doctorID))
                    txt签约医生.Text = _Bll.GetQYYSName(doctorID);
            }
        }

        private void btn查看照片_Click(object sender, EventArgs e)
        {
            try
            {
                string filename = this.txt档案号.Text ;
                // 服务器IP
                string host = "192.168.10.118";
                // 服务器端口
                string port = "83";
                // 获取用户信息接口
                string sUrl = string.Format("http://{0}:{1}/WebApi/api/AtomEHR/Get_PIC", host, port);
                string sMessage = "";
                string sEntity = "{\"cno\":\"" + filename + "\",\"ctype\":\"GRDA\"}";

                string sReq = WebApiMethod.RequestJsonPostMethod(sUrl, sEntity, out sMessage);

                byte[] pic = Convert.FromBase64String(sReq.Replace("\"", ""));
                if (pic.Length > 0)
                {
                    System.IO.MemoryStream m = new System.IO.MemoryStream(pic);

                    AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息.Frm随访照片查看 frm = new AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息.Frm随访照片查看(Image.FromStream(m));
                    frm.ShowDialog();
                }
                else
                { //二次判断
                    
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        //Begin WXF 2018-11-03 | 23:27
        //整合打印功能  
        private void sBut_统合打印_Click(object sender, EventArgs e)
        {
            frm_ReportALL repo = new frm_ReportALL(docNo);
            repo.ShowDialog();
        }
        //End
											 


    }
}
