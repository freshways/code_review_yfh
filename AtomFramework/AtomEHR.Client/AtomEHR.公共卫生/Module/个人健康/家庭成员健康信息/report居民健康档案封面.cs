﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using AtomEHR.Business;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Business.Security;
using AtomEHR.Common;

namespace AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息
{
    public partial class report居民健康档案封面 : DevExpress.XtraReports.UI.XtraReport
    {
        public report居民健康档案封面()
        {
            InitializeComponent();
        }

        public report居民健康档案封面(string str_recordNum)
        {
            InitializeComponent();
            BindingData(str_recordNum);
        }

        bllUser _bllUser = new bllUser();
        private void BindingData(string str_recordNum)
        {
            try
            {
                bll健康档案 bll_健康档案 = new bll健康档案();
                DataSet ds_健康档案 = bll_健康档案.Get_健康档案封面(str_recordNum);

                DataRow dr_健康档案 = ds_健康档案.Tables[tb_健康档案.__TableName].Rows[0];

                //责任医生不再从体检表获取，改为创建人
                if (!string.IsNullOrEmpty(dr_健康档案[tb_健康档案.创建人].ToString()))
                {
                    xrLabel_责任医生.Text = _bllUser.Return用户名称(dr_健康档案[tb_健康档案.创建人].ToString());
                }
                else
                {//如果创建人修改人都是空的则取体检表
                    if (ds_健康档案.Tables[tb_健康体检.__TableName].Rows.Count > 0)
                    {
                        DataRow dr_健康体检 = ds_健康档案.Tables[tb_健康体检.__TableName].Rows[0];
                        xrLabel_责任医生.Text = dr_健康体检[tb_健康体检.FIELD2].ToString();
                    }
                }
                //替换中间的0，
                string str_个人档案编号 = dr_健康档案[tb_健康档案.个人档案编号].ToString().Remove(11,1);
                char[] char_编号 = str_个人档案编号.ToCharArray();
                for (int i = 0; i < char_编号.Length; i++)
                {
                    string xrName = "xrLabel_编号" + (i + 1);
                    XRLabel xrl = (XRLabel)FindControl(xrName, false);
                    xrl.Text = char_编号[i].ToString();
                }

                xrLabel_姓名.Text = dr_健康档案[tb_健康档案.姓名].ToString();
                xrLabel_现住址.Text = dr_健康档案[tb_健康档案.居住地址].ToString();
                xrLabel_户籍地址.Text = dr_健康档案[tb_健康档案.市].ToString() + dr_健康档案[tb_健康档案.区].ToString() + dr_健康档案[tb_健康档案.街道].ToString() + dr_健康档案[tb_健康档案.居委会].ToString();
                xrLabel_联系电话.Text = dr_健康档案[tb_健康档案.本人电话].ToString();
                xrLabel_街道.Text = dr_健康档案[tb_健康档案.街道].ToString();
                xrLabel_居委会.Text = dr_健康档案[tb_健康档案.居委会].ToString();
                xrLabel_建档单位.Text = dr_健康档案[tb_健康档案.创建机构].ToString();
                xrLabel_建档人.Text = dr_健康档案[tb_健康档案.创建人].ToString();

                string str_建档日期 = dr_健康档案[tb_健康档案.创建时间].ToString().Substring(0, 10);
                DateTime dtDate;
                if (!string.IsNullOrWhiteSpace(str_建档日期) && DateTime.TryParse(str_建档日期, out dtDate))
                {
                    //string[] strs_建档日期 = str_建档日期.Split('-');
                    xrLabel_建档日期_年.Text = dtDate.Year.ToString();//strs_建档日期[0];
                    xrLabel_建档日期_月.Text = dtDate.Month.ToString();//strs_建档日期[1];
                    xrLabel_建档日期_日.Text = dtDate.Day.ToString();//strs_建档日期[2];
                }
                else
                { 
                    //默认当前日期
                    dtDate = DateTime.Now;
                    xrLabel_建档日期_年.Text = dtDate.Year.ToString();//strs_建档日期[0];
                    xrLabel_建档日期_月.Text = dtDate.Month.ToString();//strs_建档日期[1];
                    xrLabel_建档日期_日.Text = dtDate.Day.ToString();//strs_建档日期[2];
                }

                DataTable dt_图像采集 = ds_健康档案.Tables["tb_健康体检_图像采集"]; 
                if (dt_图像采集 != null && dt_图像采集.Rows.Count >= 1)
                {
                    var img = dt_图像采集.Rows[0]["图像"];
                    if (img != null)
                    {
                        System.Drawing.Image img健康评价 = (Image)ZipTools.DecompressionObject((byte[])img);
                        this.pic照片.Image = img健康评价;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
