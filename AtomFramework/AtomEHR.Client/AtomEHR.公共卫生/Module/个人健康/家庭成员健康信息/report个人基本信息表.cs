﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using AtomEHR.Models;
using AtomEHR.Business;

namespace AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息
{
    public partial class report个人基本信息表 : DevExpress.XtraReports.UI.XtraReport
    {
        #region Fields
        DataSet _ds个人信息;
        string docNo;
        bll健康档案 _bll个人 = new bll健康档案();
        #endregion
        public report个人基本信息表()
        {
            InitializeComponent();
        }

        public report个人基本信息表(string _docNo,string _居住地址="")
        {
            InitializeComponent();
            this.docNo = _docNo;
            _ds个人信息 = _bll个人.GetInfoByXinxi(_docNo, true);
            DoBindingDataSource(_ds个人信息);
            if (!string.IsNullOrEmpty(_居住地址))
            {
                this.txt居住地址.Visible = true;
                this.txt居住地址.Text = "居住地址：" + _居住地址;
            }
            //拆解个人档案编号后8位
            char[] char_编号 = _docNo.Substring(_docNo.Length - 8, 8).ToCharArray();
            for (int i = 0; i < char_编号.Length; i++)
            {
                string xrName = "xrLabel_编号" + (i + 1);
                XRLabel xrl = (XRLabel)FindControl(xrName, false);
                if(xrl!=null)
                    xrl.Text = char_编号[i].ToString();
            }
        }

        private void DoBindingDataSource(DataSet _ds个人信息)
        {
            DataTable dt健康档案 = _ds个人信息.Tables[Models.tb_健康档案.__TableName];
            if (dt健康档案 == null || dt健康档案.Rows.Count == 0) return;

            DataTable dt健康状态 = _ds个人信息.Tables[Models.tb_健康档案_健康状态.__TableName];

            DataTable dt既往病史 = _ds个人信息.Tables[Models.tb_健康档案_既往病史.__TableName];

            DataTable dt家族病史 = _ds个人信息.Tables[Models.tb_健康档案_家族病史.__TableName];

            this.txt姓名.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.姓名].ToString());
            this.txt性别.Text = dt健康档案.Rows[0][tb_健康档案.性别].ToString();
            this.txt出生日期.Text = dt健康档案.Rows[0][tb_健康档案.出生日期].ToString();
            this.txt身份证号.Text = dt健康档案.Rows[0][tb_健康档案.身份证号].ToString();
            this.txt工作单位.Text = dt健康档案.Rows[0][tb_健康档案.工作单位].ToString();
            this.txt本人电话.Text = dt健康档案.Rows[0][tb_健康档案.本人电话].ToString();
            this.txt联系人姓名.Text = dt健康档案.Rows[0][tb_健康档案.联系人姓名].ToString();
            this.txt联系人电话.Text = dt健康档案.Rows[0][tb_健康档案.联系人电话].ToString();
            this.txt常住类型.Text = dt健康档案.Rows[0][tb_健康档案.常住类型].ToString() == "6" ? "2" : "1";
            this.txt民族.Text = dt健康档案.Rows[0][tb_健康档案.民族].ToString();
            //this.txt少数民族.Text = util.DESEncrypt.DES解密(dt健康档案.Rows[0][tb_健康档案.民族].ToString());
            if (dt健康状态 != null && dt健康状态.Rows.Count > 0)
            {
                this.txt血型.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.血型].ToString();
            }
            //author：WXF datetime 2018年4月8日 15:57:04
            if (this.txt血型.Text == "2")
            {
                this.txt血型.Text = "4";
            }
            else if (this.txt血型.Text == "3")
            {
                this.txt血型.Text = "2";
            }
            else if (this.txt血型.Text == "4")
            {
                this.txt血型.Text = "3";
            }
            else
            {

            }
            if (dt健康状态 != null && dt健康状态.Rows.Count > 0)
            {
                this.txtRH阴性.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.RH].ToString();
            }
            //文化程度
            BindWHCD(dt健康档案);
            //职业
            BindZY(dt健康档案);
            //根据数据库中婚姻状况为 选项数字*10，所以强转后/10就获得页面 选项数字
            this.txt婚姻状况.Text = string.IsNullOrEmpty(dt健康档案.Rows[0][tb_健康档案.婚姻状况].ToString()) ? "" : (Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.婚姻状况]) / 10).ToString();
            //5.未说明的婚姻状况
            if (this.txt婚姻状况.Text == "9")
            {
                this.txt婚姻状况.Text = ((Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.婚姻状况]) / 10) - 4).ToString();
            }
            //医疗费用支付方式
            BindYLZF(dt健康档案);

            //判断是否有健康状态
            if (dt健康状态 != null && dt健康状态.Rows.Count > 0)
            {
                //药物过敏史
                BindYWGMS(dt健康状态);
                //暴露史
                BindBLS(dt健康状态);
                //遗传病史
                BindYCBS(dt健康状态);
                //残疾情况
                BindCJQK(dt健康状态);
                this.txt残疾情况其他.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.残疾其他].ToString();
            }
            //判断是否有既往病史
            if (dt既往病史 != null && dt既往病史.Rows.Count > 0)
            {
                int index1 = 1;//确定赋值控件的index
                int index2 = 1;//确定赋值控件的index
                int index3 = 1;//确定赋值控件的index
                int index4 = 1;//确定赋值控件的index
                #region 绑定疾病

                DataRow[] datarows疾病 = dt既往病史.Select("疾病类型='疾病'");
                if (datarows疾病.Length <= 0) this.txt疾病1.Text = "1";                
                for (int i = 0; i < datarows疾病.Length; i++)
                {
                    string 疾病 = datarows疾病[i][tb_健康档案_既往病史.疾病名称].ToString();
                    string[] 疾病s = 疾病.Split(',');
                    if (疾病s.Length == 1) // 一行中只有一个疾病名称
                    {
                        index1 = BindJJ(index1, datarows疾病[i]);
                    }
                    else//一行中存在多个疾病名称
                    {
                        for (int j = 0; j < 疾病s.Length; j++)
                        {
                            index1 = BindJJ(index1, datarows疾病[i]);
                        }
                    }
                }

                #endregion
                #region 绑定手术

                DataRow[] datarows手术 = dt既往病史.Select("疾病类型='手术'");
                if (datarows手术.Length <= 0) this.txt手术有无.Text = "1";
                for (int i = 0; i < datarows手术.Length; i++)
                {
                    string 疾病 = datarows手术[i][tb_健康档案_既往病史.疾病名称].ToString();
                    string[] 疾病s = 疾病.Split(',');
                    if (疾病s.Length == 1) // 一行中只有一个疾病名称
                    {
                        index2 = BindSS(index2, datarows手术[i]);
                    }
                    else//一行中存在多个疾病名称
                    {
                        for (int j = 0; j < 疾病s.Length; j++)
                        {
                            index2 = BindSS(index2, datarows手术[i]);
                        }
                    }
                }

                #endregion
                #region 绑定疾病

                DataRow[] datarows外伤 = dt既往病史.Select("疾病类型='外伤'");
                if (datarows外伤.Length <= 0) this.txt外伤有无.Text = "1";
                for (int i = 0; i < datarows外伤.Length; i++)
                {
                    string 疾病 = datarows外伤[i][tb_健康档案_既往病史.疾病名称].ToString();
                    string[] 疾病s = 疾病.Split(',');
                    if (疾病s.Length == 1) // 一行中只有一个疾病名称
                    {
                        index3 = BindWS(index3, datarows外伤[i]);
                    }
                    else//一行中存在多个疾病名称
                    {
                        for (int j = 0; j < 疾病s.Length; j++)
                        {
                            index3 = BindWS(index3, datarows外伤[i]);
                        }
                    }
                }

                #endregion
                #region 绑定输血

                DataRow[] datarows输血 = dt既往病史.Select("疾病类型='输血'");
                if (datarows输血.Length <= 0) this.txt输血有无.Text = "1";
                for (int i = 0; i < datarows输血.Length; i++)
                {
                    string 疾病 = datarows输血[i][tb_健康档案_既往病史.疾病名称].ToString();
                    string[] 疾病s = 疾病.Split(',');
                    if (疾病s.Length == 1) // 一行中只有一个疾病名称
                    {
                        index4 = BindSX(index4, datarows输血[i]);
                    }
                    else//一行中存在多个疾病名称
                    {
                        for (int j = 0; j < 疾病s.Length; j++)
                        {
                            index4 = BindSX(index4, datarows输血[i]);
                        }
                    }
                }

                #endregion
            }

            //判断是否有家族史
            if (dt家族病史 != null && dt家族病史.Rows.Count > 0)
            {
                string 家族关系 = dt家族病史.Rows[0][tb_健康档案_家族病史.家族关系].ToString();
                //家族史父亲
                if (家族关系 == "3")
                {
                    BindJZSFQ(dt家族病史);
                }
                //家族史母亲
                if (家族关系 == "4")
                {
                    BindJZSMQ(dt家族病史);
                }
                //家族史兄弟姐妹
                if (家族关系 == "5")
                {
                    BindJZSXDJM(dt家族病史);
                }
                //家族史子女
                if (家族关系 == "6")
                {
                    BindJZSZN(dt家族病史);
                }
            }

            this.txt厨房排风设施.Text = dt健康档案.Rows[0][tb_健康档案.厨房排气设施].ToString();
            this.txt燃料类型.Text = dt健康档案.Rows[0][tb_健康档案.燃料类型].ToString();
            this.txt饮水.Text = dt健康档案.Rows[0][tb_健康档案.饮水].ToString();
            this.txt厕所.Text = dt健康档案.Rows[0][tb_健康档案.厕所].ToString();
            this.txt禽畜栏.Text = dt健康档案.Rows[0][tb_健康档案.禽畜栏].ToString();

            #region  新版本添加
            this.txt职工医保卡号.Text = dt健康档案.Rows[0][tb_健康档案.医疗保险号].ToString();
            this.txt居民医保卡号.Text = dt健康档案.Rows[0][tb_健康档案.新农合号].ToString();
            this.txt贫困救助卡号.Text = dt健康档案.Rows[0][tb_健康档案.贫困救助卡号].ToString();
            this.txt户主姓名.Text = dt健康档案.Rows[0][tb_健康档案.户主姓名].ToString();
            this.txt户主身份证.Text = dt健康档案.Rows[0][tb_健康档案.户主身份证号].ToString();
            this.txt家庭人口数.Text = dt健康档案.Rows[0][tb_健康档案.家庭人口数].ToString();
            this.txt家庭结构.Text = dt健康档案.Rows[0][tb_健康档案.家庭结构].ToString();
            this.txt居住情况.Text = dt健康档案.Rows[0][tb_健康档案.居住情况].ToString();
            string str本人或家属签字 = dt健康档案.Rows[0][tb_健康档案.本人或家属签字].ToString();
            if (!string.IsNullOrEmpty(str本人或家属签字) && str本人或家属签字 == this.txt姓名.Text)
            {
                this.txt本人签字.Text = this.txt姓名.Text;
            }
            else
            {
                //this.txt家属签字.Text = str本人或家属签字;
                if (!string.IsNullOrEmpty(str本人或家属签字) && str本人或家属签字.Length > 64)
                {
                    Byte[] bitmapData = new Byte[str本人或家属签字.Length];

                    bitmapData = Convert.FromBase64String(str本人或家属签字);

                    System.IO.MemoryStream streamBitmap = new System.IO.MemoryStream(bitmapData);

                    this.xrPictureBox1.Image = Image.FromStream(streamBitmap);
                }
                else
                {
                    if (!string.IsNullOrEmpty(dt健康档案.Rows[0][tb_健康档案.复核备注].ToString()))
                        str本人或家属签字 = dt健康档案.Rows[0][tb_健康档案.复核备注].ToString();
                    if (string.IsNullOrEmpty(str本人或家属签字))
                        str本人或家属签字 = " ";
                    Graphics g = Graphics.FromImage(new Bitmap(1, 1));
                    Font font = new Font("宋体", 9);
                    SizeF sizeF = g.MeasureString(str本人或家属签字, font); //测量出字体的高度和宽度  
                    Brush brush; //笔刷，颜色  
                    brush = Brushes.Black;
                    PointF pf = new PointF(0, 0);
                    Bitmap img = new Bitmap(Convert.ToInt32(sizeF.Width), Convert.ToInt32(sizeF.Height));
                    g = Graphics.FromImage(img);
                    g.DrawString(str本人或家属签字, font, brush, pf);
                    this.xrPictureBox1.Image = img;
                }
            }
            this.txt签字时间.Text = dt健康档案.Rows[0][tb_健康档案.签字时间].ToString();
            #endregion

        }

        //遗传病史
        private void BindYCBS(DataTable dt健康状态)
        {
            this.txt遗传病史有无.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.遗传病史有无].ToString();
            if (this.txt遗传病史有无.Text == "1")
            {
                this.txt遗传病史有无.Text = "2";
            }
            else
            {
                this.txt遗传病史有无.Text = "1";
            }
            this.txt遗传病史疾病名称.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.遗传病史].ToString();
        }
        //家族史子女
        private void BindJZSZN(DataTable dt家族病史)
        {
            string 家族史子女 = dt家族病史.Rows[0][tb_健康档案_家族病史.家族病史].ToString();
            if (!string.IsNullOrEmpty(家族史子女))
            {
                string[] a = 家族史子女.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt家族史子女" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = "12";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
            this.txt家族史子女其他.Text = dt家族病史.Rows[0][tb_健康档案_家族病史.其他疾病].ToString();
        }
        //家族史兄弟姐妹
        private void BindJZSXDJM(DataTable dt家族病史)
        {
            string 家族史兄弟姐妹 = dt家族病史.Rows[0][tb_健康档案_家族病史.家族病史].ToString();
            if (!string.IsNullOrEmpty(家族史兄弟姐妹))
            {
                string[] a = 家族史兄弟姐妹.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt家族史兄弟姐妹 " + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = "12";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
            this.txt家族史兄弟姐妹其他.Text = dt家族病史.Rows[0][tb_健康档案_家族病史.其他疾病].ToString();
        }
        //家族史母亲
        private void BindJZSMQ(DataTable dt家族病史)
        {
            string 家族史母亲 = dt家族病史.Rows[0][tb_健康档案_家族病史.家族病史].ToString();
            if (!string.IsNullOrEmpty(家族史母亲))
            {
                string[] a = 家族史母亲.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt家族史母亲" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = "12";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
            this.txt家族史母亲其他.Text = dt家族病史.Rows[0][tb_健康档案_家族病史.其他疾病].ToString();
        }
        //家族史父亲
        private void BindJZSFQ(DataTable dt家族病史)
        {
            string 家族史父亲 = dt家族病史.Rows[0][tb_健康档案_家族病史.家族病史].ToString();
            if (!string.IsNullOrEmpty(家族史父亲))
            {
                string[] a = 家族史父亲.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt家族史父亲" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "100") lbl.Text = "12";
                    else lbl.Text = (Convert.ToInt32(a[i]) + 1).ToString();
                }
            }
            this.txt家族史父亲其他.Text = dt家族病史.Rows[0][tb_健康档案_家族病史.其他疾病].ToString();
        }

        //残疾情况
        private void BindCJQK(DataTable dt健康状态)
        {
            string s有无残疾 = dt健康状态.Rows[0][tb_健康档案_健康状态.有无残疾].ToString();
            if (s有无残疾 == "2")
            {
                this.txt残疾情况0.Text = "1";
            }
            else
            {
                string 残疾情况 = dt健康状态.Rows[0][tb_健康档案_健康状态.残疾情况].ToString();
                if (!string.IsNullOrEmpty(残疾情况))
                {
                    string[] a = 残疾情况.Split(',');
                    for (int i = 0; i < a.Length; i++)
                    {
                        if (string.IsNullOrEmpty(a[i])) continue;
                        string strName = "txt残疾情况" + (i);
                        XRLabel lbl = (XRLabel)FindControl(strName, false);
                        if (lbl == null) continue;
                        if (a[i] == "9") lbl.Text = "8";
                        else lbl.Text = a[i];
                    }
                }
            }
        }
        //暴露史
        private void BindBLS(DataTable dt健康状态)
        {
            this.txt暴露史有无.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.暴露史].ToString();
            if (this.txt暴露史有无.Text == "2")
            {
                this.txt暴露史有无.Text = "1";
            }
            this.txt暴露史1.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.暴露史化学品].ToString();
            if (this.txt暴露史2.Text == "4")
            {
                this.txt暴露史2.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.暴露史毒物].ToString();
            }
            else if (this.txt暴露史2.Text == "5")
            {
                this.txt暴露史2.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.暴露史射线].ToString();
            }
        }
        //药物过敏史
        private void BindYWGMS(DataTable dt健康状态)
        {
            this.txt药物过敏史有无.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.过敏史有无].ToString();
            if (this.txt药物过敏史有无.Text == "2")
            {
                this.txt药物过敏史有无.Text = "1";
            }
            string 药物过敏史 = dt健康状态.Rows[0][tb_健康档案_健康状态.药物过敏史].ToString();
            if (!string.IsNullOrEmpty(药物过敏史))
            {
                string[] a = 药物过敏史.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt药物过敏史" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (a[i] == "99") lbl.Text = "5";
                    else lbl.Text = a[i];
                }
            }
            this.txt药物过敏史其他.Text = dt健康状态.Rows[0][tb_健康档案_健康状态.过敏史其他].ToString();
        }
        //医疗费用支付方式
        private void BindYLZF(DataTable dt健康档案)
        {
            string 医疗费用支付方式 = dt健康档案.Rows[0][tb_健康档案.医疗费支付类型].ToString();
            if (!string.IsNullOrEmpty(医疗费用支付方式))
            {
                string[] a = 医疗费用支付方式.Split(',');
                for (int i = 0; i < a.Length; i++)
                {
                    if (string.IsNullOrEmpty(a[i])) continue;
                    string strName = "txt医疗费用支付方式" + (i + 1);
                    XRLabel lbl = (XRLabel)FindControl(strName, false);
                    if (lbl == null) continue;
                    switch(a[i])
                    {
                        case "1":
                            lbl.Text = "6";//全自费
                            break;
                        case "2":
                            lbl.Text = "5"; //全公费
                            break;
                        case "3":
                            lbl.Text = "1";//城镇或省直职工基本医疗保险
                            break;
                        case "4":
                            lbl.Text = "2";  //居民基本医疗保险
                            break;
                        case "7":
                            lbl.Text = "4";//商业医疗保险
                            break;
                        case "8":
                            lbl.Text = "3"; //贫困救助
                            break;
                        case "99":
                            lbl.Text = "7"; //其他
                            break;
                        default:
                            break;
                    }
                    #region    （未用）
                    //switch (a[i])
                    //{
                    //    case "1":
                    //        lbl.Text = "7"; //全自费
                    //        break;
                    //    case "2":
                    //        lbl.Text = "6"; //全公费
                    //        break;
                    //    case "3":
                    //        lbl.Text = "1"; //城镇或省直职工基本医疗保险
                    //        break;
                    //    case "4":
                    //        lbl.Text = "2"; //居民基本医疗保险
                    //        break;
                    //    case "5":
                    //        lbl.Text = "3";  //新型农村合作医疗
                    //        break;
                    //    case "7":
                    //        lbl.Text = "5";//商业医疗保险
                    //        break;
                    //    case "8":
                    //        lbl.Text = "4"; //贫困救助
                    //        break;
                    //    case "99":
                    //        lbl.Text = "8"; //其他
                    //        break;
                    //    default:
                    //        break;
                    //}
                    #endregion
                }
            }
            this.txt医疗费用支付方式其他.Text = dt健康档案.Rows[0][tb_健康档案.医疗费用支付类型其他].ToString();
        }
        //职业
        private void BindZY(DataTable dt健康档案)
        {
            //由于数据库中职业顺序与“个人信息表”中顺序不同需要调整位置
            this.txt职业.Text = dt健康档案.Rows[0][tb_健康档案.职业].ToString();
            //switch (this.txt职业.Text)
            //{ //2018年11月22日 yfh 按照2017办对应
            //    case "1":
            //        this.txt职业.Text = "4"; //农、林、牧、渔、水利业生产人员
            //        break;
            //    case "2":
            //        this.txt职业.Text = "5"; //生产、运输设备操作人员及有关人员
            //        break;
            //    case "3":
            //        this.txt职业.Text = "1"; //专业技术人员
            //        break;
            //    case "4":
            //        this.txt职业.Text = "2"; //办事人员和有关人员
            //        break;
            //    case "5":
            //        this.txt职业.Text = "3"; //商业、服务业人员
            //        break;
            //    case "6":
            //        this.txt职业.Text = "0"; //国家机关、党群组织、企业、事业单位负责人
            //        break;
            //    case "12":
            //        this.txt职业.Text = "6"; //军人
            //        break;
            //    case "99":
            //        this.txt职业.Text = "7"; //不便分类的其他从业人员
            //        break;
            //    default:
            //        this.txt职业.Text = "8"; //无职业
            //        break;
            //}
        }
        //文化程度
        private void BindWHCD(DataTable dt健康档案)
        {
            //根据数据库中文化程度为 选项数字*10，所以强转后/10就获得页面 选项数字
            this.txt文化程度.Text = string.IsNullOrEmpty(dt健康档案.Rows[0][tb_健康档案.文化程度].ToString()) ? "" : (Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.文化程度]) / 10).ToString();
            //1.文盲及半文盲:90
            //if (this.txt文化程度.Text == "9")
            //{
            //    this.txt文化程度.Text = ((Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.文化程度]) / 10) - 8).ToString();
            //}
            ////2.小学:80
            //if (this.txt文化程度.Text == "8")
            //{
            //    this.txt文化程度.Text = ((Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.文化程度]) / 10) - 6).ToString();
            //}
            ////3.初中:70
            //if (this.txt文化程度.Text == "7")
            //{
            //    this.txt文化程度.Text = ((Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.文化程度]) / 10) - 4).ToString();
            //}
            ////4.高中/技校/中专:60/50/40
            //if (this.txt文化程度.Text == "6")
            //{
            //    this.txt文化程度.Text = ((Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.文化程度]) / 10) - 2).ToString();
            //}
            //if (this.txt文化程度.Text == "5")
            //{
            //    this.txt文化程度.Text = ((Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.文化程度]) / 10) - 1).ToString();
            //}
            ////5.大学专科及以上:30/20/10
            //if (this.txt文化程度.Text == "3")
            //{
            //    this.txt文化程度.Text = ((Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.文化程度]) / 10) + 2).ToString();
            //}
            //if (this.txt文化程度.Text == "2")
            //{
            //    this.txt文化程度.Text = ((Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.文化程度]) / 10) + 3).ToString();
            //}
            //if (this.txt文化程度.Text == "1")
            //{
            //    this.txt文化程度.Text = ((Convert.ToInt32(dt健康档案.Rows[0][tb_健康档案.文化程度]) / 10) + 4).ToString();
            //}
            ////6.不详:99
            //if (this.txt文化程度.Text == "13")
            //{
            //    this.txt文化程度.Text = "6";
            //}
        }

        //输血
        private int BindSX(int index, DataRow dataRow)
        {
            string str疾病 = "txt输血原因" + index;
            string str疾病确诊时间 = "txt输血时间" + index;
            XRLabel lbl疾病 = (XRLabel)FindControl(str疾病, false);
            XRLabel lbl疾病确诊时间 = (XRLabel)FindControl(str疾病确诊时间, false);
            lbl疾病.Text = dataRow[tb_健康档案_既往病史.疾病名称].ToString();
            lbl疾病确诊时间.Text = dataRow[tb_健康档案_既往病史.日期].ToString();
            index++;
            return index;
        }
        //外伤
        private int BindWS(int index, DataRow dataRow)
        {
            string str疾病 = "txt外伤名称" + index;
            string str疾病确诊时间 = "txt外伤时间" + index;
            XRLabel lbl疾病 = (XRLabel)FindControl(str疾病, false);
            XRLabel lbl疾病确诊时间 = (XRLabel)FindControl(str疾病确诊时间, false);
            lbl疾病.Text = dataRow[tb_健康档案_既往病史.疾病名称].ToString();
            lbl疾病确诊时间.Text = dataRow[tb_健康档案_既往病史.日期].ToString();
            index++;
            return index;
        }
        //手术
        private int BindSS(int index, DataRow dataRow)
        {
            string str疾病 = "txt手术名称" + index;
            string str疾病确诊时间 = "txt手术时间" + index;
            XRLabel lbl疾病 = (XRLabel)FindControl(str疾病, false);
            XRLabel lbl疾病确诊时间 = (XRLabel)FindControl(str疾病确诊时间, false);
            lbl疾病.Text = dataRow[tb_健康档案_既往病史.疾病名称].ToString();
            lbl疾病确诊时间.Text = dataRow[tb_健康档案_既往病史.日期].ToString();
            index++;
            return index;
        }
        //疾病
        private int BindJJ(int index, DataRow row疾病)
        {
            string str疾病 = "txt疾病" + index;
            string str疾病确诊时间 = "txt疾病" + index + "确诊时间";
            XRLabel lbl疾病 = (XRLabel)FindControl(str疾病, false);
            XRLabel lbl疾病确诊时间 = (XRLabel)FindControl(str疾病确诊时间, false);
            lbl疾病.Text = row疾病[tb_健康档案_既往病史.疾病名称].ToString();
            lbl疾病确诊时间.Text = row疾病[tb_健康档案_既往病史.日期].ToString();
            if (str疾病 == "6")//恶性肿瘤
            {
                this.txt恶性肿瘤.Text = row疾病[tb_健康档案_既往病史.恶性肿瘤].ToString();
            }
            if (str疾病 == "12")//职业病
            {
                this.txt职业病.Text = row疾病[tb_健康档案_既往病史.职业病其他].ToString();
            }
            if (str疾病 == "13")//其他
            {
                this.txt疾病其他.Text = row疾病[tb_健康档案_既往病史.疾病其他].ToString();
            }
            index++;
            return index;
        }

    }
}
