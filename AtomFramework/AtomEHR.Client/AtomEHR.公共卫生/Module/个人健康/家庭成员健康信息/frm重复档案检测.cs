﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AtomEHR.公共卫生.Module.个人健康.家庭成员健康信息
{
    public partial class frm重复档案检测 : DevExpress.XtraEditors.XtraForm
    {

        public frm重复档案检测()
        {
            InitializeComponent();
        }
        public frm重复档案检测(DataTable dt)
        {
            InitializeComponent();
            this.gc个人健康档案.DataSource = dt;
            this.gv个人健康档案.BestFitColumns();
        }

        private void repositoryItemHyperLinkEdit1_Click(object sender, EventArgs e)
        {
            DataRow row = this.gv个人健康档案.GetFocusedDataRow();
            if (row == null) return;
            string 家庭档案编号 = row["家庭档案编号"] as string;
            string 个人档案编号 = row["个人档案编号"] as string;
            //_BLL.GetBusinessByKey(个人档案编号, true);//下载一个空业务单据            
            //_BLL.NewBusiness(); //增加一条主表记录
            //frm个人健康 frm = new frm个人健康(false, this.Name, 家庭档案编号, 个人档案编号, null);
            //frm.Show();
        }
    }
}
