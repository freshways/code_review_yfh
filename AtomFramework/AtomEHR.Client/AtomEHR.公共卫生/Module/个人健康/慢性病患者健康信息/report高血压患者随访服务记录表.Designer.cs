﻿namespace AtomEHR.公共卫生.Module.个人健康.慢性病患者健康信息
{
    partial class report高血压患者随访服务记录表
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访症状6 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访症状7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访症状5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访症状4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访症状3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访症状2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访症状1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访症状8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访症状6 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访症状7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访症状5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访症状4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel59 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访症状3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访症状2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访症状1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访症状8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访症状其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访症状其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel100 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访症状6 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访症状7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访症状5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访症状4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访症状3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel75 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访症状2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel77 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访症状1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访症状8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访症状6 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访症状7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访症状5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel86 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访症状4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel88 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访症状3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel90 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访症状2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访症状1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel95 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访症状8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable62 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访医生签名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访医生签名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable63 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访医生签名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访医生签名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable60 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访下次随访日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访下次随访日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable61 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访下次随访日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访下次随访日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable58 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable59 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访转诊机构 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable56 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable57 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访转诊原因 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel366 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable54 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel342 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel343 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel344 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel345 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel346 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel347 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel348 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel349 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel350 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel351 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel352 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel353 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable55 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel354 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel355 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel356 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访其他药物用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel358 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel359 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel360 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel361 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel362 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel363 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel364 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel365 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable52 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访其他药物 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访其他药物 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable53 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访其他药物 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访其他药物 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable48 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访药物名称3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访药物名称3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable49 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访药物名称3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访药物名称3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable51 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访药物3用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访药物3用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable50 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访药物3用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访药物3用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable44 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访药物名称2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访药物名称2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable45 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访药物名称2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访药物名称2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable47 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访药物2用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访药物2用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable46 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访药物2用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访药物2用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable42 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访药物1用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访药物1用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable43 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访药物1用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访药物1用法 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable40 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell96 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访药物名称1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访药物名称1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable41 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访药物名称1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访药物名称1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel253 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable38 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访分类 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel241 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel242 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访分类 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel247 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel248 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable39 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访分类 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel244 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel249 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访分类 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel251 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel252 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable36 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访药物不良反应其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访药物不良反应 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访药物不良反应其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访药物不良反应 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable37 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访药物不良反应其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访药物不良反应 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel234 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访药物不良反应其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访药物不良反应 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable34 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访服药依从性 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel222 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访服药依从性 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable35 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访服药依从性 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访服药依从性 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable32 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访辅助检查 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访辅助检查 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable33 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访辅助检查 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访辅助检查 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable30 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访遵医行为 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel210 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访遵医行为 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel212 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable31 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访遵医行为 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访遵医行为 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel216 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable28 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访心理调整 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel201 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访心理调整 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel202 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable29 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访心理调整 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel203 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访心理调整 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel204 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable26 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel58 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访摄盐情况1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访摄盐情况2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel194 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel189 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访摄盐情况1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访摄盐情况2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable27 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访摄盐情况1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访摄盐情况2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访摄盐情况1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访摄盐情况2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable24 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel173 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel174 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel169 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访运动次数1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel159 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访运动时间1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访运动时间2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访运动次数2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访运动次数2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访运动次数1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访运动时间1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访运动时间2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable25 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访运动次数2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访运动次数1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访运动时间1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访运动时间2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访运动次数2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访运动次数1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访运动时间1 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访运动时间2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable22 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访日饮酒量2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访日饮酒量1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel147 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访日饮酒量2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访日饮酒量1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel150 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable23 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访日饮酒量2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访日饮酒量1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel153 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访日饮酒量2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访日饮酒量1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel156 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable20 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访日吸烟量2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访日吸烟量1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel135 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访日吸烟量2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访日吸烟量1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel138 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable21 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访日吸烟量2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访日吸烟量1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel141 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访日吸烟量2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访日吸烟量1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel144 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel132 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable18 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访体征其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访体征其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable19 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访体征其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访体征其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访心率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访心率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访心率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访心率 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访体质指数2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel56 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访体质指数1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访体质指数2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访体质指数1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访体质指数2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访体质指数1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访体质指数2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访体质指数1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访体重2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访体重1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访体重2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访体重1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访体重2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访体重1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访体重2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访体重1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访血压2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访血压1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel113 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访血压2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访血压1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访血压2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第1次随访血压1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel108 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访血压2 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访血压1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel110 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第3次随访方式 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第4次随访方式 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访方式 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt第2次随访方式 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.txt姓名 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第1次随访日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第2次随访日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访日期 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第3次随访症状其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.txt第4次随访症状其他 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable64 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel384 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel385 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel386 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel387 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel388 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel389 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel390 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel391 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel392 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5,
            this.xrTable7,
            this.xrTable8,
            this.xrTable62,
            this.xrTable63,
            this.xrTable60,
            this.xrTable61,
            this.xrTable58,
            this.xrTable59,
            this.xrTable56,
            this.xrTable57,
            this.xrLabel366,
            this.xrTable54,
            this.xrTable55,
            this.xrTable52,
            this.xrTable53,
            this.xrTable48,
            this.xrTable49,
            this.xrTable51,
            this.xrTable50,
            this.xrTable44,
            this.xrTable45,
            this.xrTable47,
            this.xrTable46,
            this.xrTable42,
            this.xrTable43,
            this.xrTable40,
            this.xrTable41,
            this.xrLabel253,
            this.xrTable38,
            this.xrTable39,
            this.xrTable36,
            this.xrTable37,
            this.xrTable34,
            this.xrTable35,
            this.xrTable32,
            this.xrTable33,
            this.xrTable30,
            this.xrTable31,
            this.xrTable28,
            this.xrTable29,
            this.xrTable26,
            this.xrTable27,
            this.xrTable24,
            this.xrTable25,
            this.xrTable22,
            this.xrTable23,
            this.xrTable20,
            this.xrTable21,
            this.xrLabel132,
            this.xrTable18,
            this.xrTable19,
            this.xrTable16,
            this.xrTable17,
            this.xrTable14,
            this.xrTable15,
            this.xrTable12,
            this.xrTable13,
            this.xrTable11,
            this.xrTable10,
            this.xrLabel103,
            this.xrTable3,
            this.xrTable4,
            this.xrLine1,
            this.xrLabel5,
            this.txt姓名,
            this.xrLabel2,
            this.xrLabel1,
            this.xrTable1,
            this.xrLabel4,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrTable2,
            this.xrTable9,
            this.xrLabel27,
            this.xrTable64});
            this.Detail.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.Detail.HeightF = 1131.25F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable5
            // 
            this.xrTable5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(445.9998F, 137.9167F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow5});
            this.xrTable5.SizeF = new System.Drawing.SizeF(330F, 40.00003F);
            this.xrTable5.StylePriority.UseBorders = false;
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell11,
            this.xrTableCell12});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1D;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访症状6,
            this.txt第3次随访症状7,
            this.xrLabel45,
            this.txt第3次随访症状5,
            this.xrLabel47,
            this.txt第3次随访症状4,
            this.xrLabel49,
            this.txt第3次随访症状3,
            this.xrLabel43,
            this.txt第3次随访症状2,
            this.xrLabel41,
            this.txt第3次随访症状1,
            this.xrLabel25,
            this.xrLabel55,
            this.txt第3次随访症状8});
            this.xrTableCell11.Font = new System.Drawing.Font("仿宋", 8.25F);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.StylePriority.UseFont = false;
            this.xrTableCell11.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访症状6
            // 
            this.txt第3次随访症状6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访症状6.CanGrow = false;
            this.txt第3次随访症状6.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访症状6.LocationFloat = new DevExpress.Utils.PointFloat(105.8572F, 7.499969F);
            this.txt第3次随访症状6.Name = "txt第3次随访症状6";
            this.txt第3次随访症状6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访症状6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访症状6.StylePriority.UseBorders = false;
            this.txt第3次随访症状6.StylePriority.UseFont = false;
            // 
            // txt第3次随访症状7
            // 
            this.txt第3次随访症状7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访症状7.CanGrow = false;
            this.txt第3次随访症状7.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访症状7.LocationFloat = new DevExpress.Utils.PointFloat(126.4286F, 7.500032F);
            this.txt第3次随访症状7.Name = "txt第3次随访症状7";
            this.txt第3次随访症状7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访症状7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访症状7.StylePriority.UseBorders = false;
            this.txt第3次随访症状7.StylePriority.UseFont = false;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel45.CanGrow = false;
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(141.7143F, 7.500032F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel45.StylePriority.UseBorders = false;
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.Text = "/";
            // 
            // txt第3次随访症状5
            // 
            this.txt第3次随访症状5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访症状5.CanGrow = false;
            this.txt第3次随访症状5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访症状5.LocationFloat = new DevExpress.Utils.PointFloat(85.28573F, 7.500032F);
            this.txt第3次随访症状5.Name = "txt第3次随访症状5";
            this.txt第3次随访症状5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访症状5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访症状5.StylePriority.UseBorders = false;
            this.txt第3次随访症状5.StylePriority.UseFont = false;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel47.CanGrow = false;
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(100.5714F, 7.500032F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel47.StylePriority.UseBorders = false;
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.Text = "/";
            // 
            // txt第3次随访症状4
            // 
            this.txt第3次随访症状4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访症状4.CanGrow = false;
            this.txt第3次随访症状4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访症状4.LocationFloat = new DevExpress.Utils.PointFloat(64.71429F, 7.5F);
            this.txt第3次随访症状4.Name = "txt第3次随访症状4";
            this.txt第3次随访症状4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访症状4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访症状4.StylePriority.UseBorders = false;
            this.txt第3次随访症状4.StylePriority.UseFont = false;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel49.CanGrow = false;
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(80.00001F, 7.5F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel49.StylePriority.UseBorders = false;
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.Text = "/";
            // 
            // txt第3次随访症状3
            // 
            this.txt第3次随访症状3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访症状3.CanGrow = false;
            this.txt第3次随访症状3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访症状3.LocationFloat = new DevExpress.Utils.PointFloat(44.14285F, 7.500032F);
            this.txt第3次随访症状3.Name = "txt第3次随访症状3";
            this.txt第3次随访症状3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访症状3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访症状3.StylePriority.UseBorders = false;
            this.txt第3次随访症状3.StylePriority.UseFont = false;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel43.CanGrow = false;
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(59.42856F, 7.500032F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel43.StylePriority.UseBorders = false;
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.Text = "/";
            // 
            // txt第3次随访症状2
            // 
            this.txt第3次随访症状2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访症状2.CanGrow = false;
            this.txt第3次随访症状2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访症状2.LocationFloat = new DevExpress.Utils.PointFloat(23.57141F, 7.499969F);
            this.txt第3次随访症状2.Name = "txt第3次随访症状2";
            this.txt第3次随访症状2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访症状2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访症状2.StylePriority.UseBorders = false;
            this.txt第3次随访症状2.StylePriority.UseFont = false;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel41.CanGrow = false;
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(38.85713F, 7.500032F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel41.StylePriority.UseBorders = false;
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.Text = "/";
            // 
            // txt第3次随访症状1
            // 
            this.txt第3次随访症状1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访症状1.CanGrow = false;
            this.txt第3次随访症状1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访症状1.LocationFloat = new DevExpress.Utils.PointFloat(2.999977F, 7.499969F);
            this.txt第3次随访症状1.Name = "txt第3次随访症状1";
            this.txt第3次随访症状1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访症状1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访症状1.StylePriority.UseBorders = false;
            this.txt第3次随访症状1.StylePriority.UseFont = false;
            // 
            // xrLabel25
            // 
            this.xrLabel25.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel25.CanGrow = false;
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(18.2857F, 7.5F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel25.StylePriority.UseBorders = false;
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.Text = "/";
            // 
            // xrLabel55
            // 
            this.xrLabel55.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel55.CanGrow = false;
            this.xrLabel55.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(121.1429F, 7.500032F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel55.StylePriority.UseBorders = false;
            this.xrLabel55.StylePriority.UseFont = false;
            this.xrLabel55.Text = "/";
            // 
            // txt第3次随访症状8
            // 
            this.txt第3次随访症状8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访症状8.CanGrow = false;
            this.txt第3次随访症状8.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访症状8.LocationFloat = new DevExpress.Utils.PointFloat(147F, 7.500032F);
            this.txt第3次随访症状8.Name = "txt第3次随访症状8";
            this.txt第3次随访症状8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访症状8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访症状8.StylePriority.UseBorders = false;
            this.txt第3次随访症状8.StylePriority.UseFont = false;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访症状6,
            this.txt第4次随访症状7,
            this.xrLabel53,
            this.txt第4次随访症状5,
            this.xrLabel57,
            this.txt第4次随访症状4,
            this.xrLabel59,
            this.txt第4次随访症状3,
            this.xrLabel61,
            this.txt第4次随访症状2,
            this.xrLabel63,
            this.txt第4次随访症状1,
            this.xrLabel65,
            this.xrLabel66,
            this.txt第4次随访症状8});
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访症状6
            // 
            this.txt第4次随访症状6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访症状6.CanGrow = false;
            this.txt第4次随访症状6.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访症状6.LocationFloat = new DevExpress.Utils.PointFloat(105.8572F, 7.499969F);
            this.txt第4次随访症状6.Name = "txt第4次随访症状6";
            this.txt第4次随访症状6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访症状6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访症状6.StylePriority.UseBorders = false;
            this.txt第4次随访症状6.StylePriority.UseFont = false;
            // 
            // txt第4次随访症状7
            // 
            this.txt第4次随访症状7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访症状7.CanGrow = false;
            this.txt第4次随访症状7.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访症状7.LocationFloat = new DevExpress.Utils.PointFloat(126.4286F, 7.500032F);
            this.txt第4次随访症状7.Name = "txt第4次随访症状7";
            this.txt第4次随访症状7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访症状7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访症状7.StylePriority.UseBorders = false;
            this.txt第4次随访症状7.StylePriority.UseFont = false;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel53.CanGrow = false;
            this.xrLabel53.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(141.7143F, 7.500032F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel53.StylePriority.UseBorders = false;
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.Text = "/";
            // 
            // txt第4次随访症状5
            // 
            this.txt第4次随访症状5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访症状5.CanGrow = false;
            this.txt第4次随访症状5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访症状5.LocationFloat = new DevExpress.Utils.PointFloat(85.28573F, 7.500032F);
            this.txt第4次随访症状5.Name = "txt第4次随访症状5";
            this.txt第4次随访症状5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访症状5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访症状5.StylePriority.UseBorders = false;
            this.txt第4次随访症状5.StylePriority.UseFont = false;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel57.CanGrow = false;
            this.xrLabel57.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(100.5714F, 7.500032F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel57.StylePriority.UseBorders = false;
            this.xrLabel57.StylePriority.UseFont = false;
            this.xrLabel57.Text = "/";
            // 
            // txt第4次随访症状4
            // 
            this.txt第4次随访症状4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访症状4.CanGrow = false;
            this.txt第4次随访症状4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访症状4.LocationFloat = new DevExpress.Utils.PointFloat(64.71429F, 7.5F);
            this.txt第4次随访症状4.Name = "txt第4次随访症状4";
            this.txt第4次随访症状4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访症状4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访症状4.StylePriority.UseBorders = false;
            this.txt第4次随访症状4.StylePriority.UseFont = false;
            // 
            // xrLabel59
            // 
            this.xrLabel59.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel59.CanGrow = false;
            this.xrLabel59.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel59.LocationFloat = new DevExpress.Utils.PointFloat(80.00001F, 7.5F);
            this.xrLabel59.Name = "xrLabel59";
            this.xrLabel59.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel59.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel59.StylePriority.UseBorders = false;
            this.xrLabel59.StylePriority.UseFont = false;
            this.xrLabel59.Text = "/";
            // 
            // txt第4次随访症状3
            // 
            this.txt第4次随访症状3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访症状3.CanGrow = false;
            this.txt第4次随访症状3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访症状3.LocationFloat = new DevExpress.Utils.PointFloat(44.14285F, 7.500032F);
            this.txt第4次随访症状3.Name = "txt第4次随访症状3";
            this.txt第4次随访症状3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访症状3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访症状3.StylePriority.UseBorders = false;
            this.txt第4次随访症状3.StylePriority.UseFont = false;
            // 
            // xrLabel61
            // 
            this.xrLabel61.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel61.CanGrow = false;
            this.xrLabel61.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(59.42856F, 7.500032F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel61.StylePriority.UseBorders = false;
            this.xrLabel61.StylePriority.UseFont = false;
            this.xrLabel61.Text = "/";
            // 
            // txt第4次随访症状2
            // 
            this.txt第4次随访症状2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访症状2.CanGrow = false;
            this.txt第4次随访症状2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访症状2.LocationFloat = new DevExpress.Utils.PointFloat(23.57141F, 7.499969F);
            this.txt第4次随访症状2.Name = "txt第4次随访症状2";
            this.txt第4次随访症状2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访症状2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访症状2.StylePriority.UseBorders = false;
            this.txt第4次随访症状2.StylePriority.UseFont = false;
            // 
            // xrLabel63
            // 
            this.xrLabel63.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel63.CanGrow = false;
            this.xrLabel63.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(38.85713F, 7.500032F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel63.StylePriority.UseBorders = false;
            this.xrLabel63.StylePriority.UseFont = false;
            this.xrLabel63.Text = "/";
            // 
            // txt第4次随访症状1
            // 
            this.txt第4次随访症状1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访症状1.CanGrow = false;
            this.txt第4次随访症状1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访症状1.LocationFloat = new DevExpress.Utils.PointFloat(2.999977F, 7.499969F);
            this.txt第4次随访症状1.Name = "txt第4次随访症状1";
            this.txt第4次随访症状1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访症状1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访症状1.StylePriority.UseBorders = false;
            this.txt第4次随访症状1.StylePriority.UseFont = false;
            // 
            // xrLabel65
            // 
            this.xrLabel65.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel65.CanGrow = false;
            this.xrLabel65.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(18.2857F, 7.5F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel65.StylePriority.UseBorders = false;
            this.xrLabel65.StylePriority.UseFont = false;
            this.xrLabel65.Text = "/";
            // 
            // xrLabel66
            // 
            this.xrLabel66.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel66.CanGrow = false;
            this.xrLabel66.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(121.1429F, 7.500032F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel66.StylePriority.UseBorders = false;
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.Text = "/";
            // 
            // txt第4次随访症状8
            // 
            this.txt第4次随访症状8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访症状8.CanGrow = false;
            this.txt第4次随访症状8.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访症状8.LocationFloat = new DevExpress.Utils.PointFloat(147F, 7.500032F);
            this.txt第4次随访症状8.Name = "txt第4次随访症状8";
            this.txt第4次随访症状8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访症状8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访症状8.StylePriority.UseBorders = false;
            this.txt第4次随访症状8.StylePriority.UseFont = false;
            // 
            // xrTable7
            // 
            this.xrTable7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(116.0004F, 177.9167F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow7});
            this.xrTable7.SizeF = new System.Drawing.SizeF(330F, 140F);
            this.xrTable7.StylePriority.UseBorders = false;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.xrTableCell17});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1D;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访症状其他,
            this.xrLabel38});
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Weight = 1.5D;
            // 
            // txt第1次随访症状其他
            // 
            this.txt第1次随访症状其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访症状其他.LocationFloat = new DevExpress.Utils.PointFloat(4.999893F, 25.00002F);
            this.txt第1次随访症状其他.Name = "txt第1次随访症状其他";
            this.txt第1次随访症状其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访症状其他.SizeF = new System.Drawing.SizeF(150F, 105F);
            this.txt第1次随访症状其他.StylePriority.UseBorders = false;
            this.txt第1次随访症状其他.StylePriority.UseTextAlignment = false;
            this.txt第1次随访症状其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(2.999973F, 1.589457E-05F);
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(50F, 25F);
            this.xrLabel38.StylePriority.UseBorders = false;
            this.xrLabel38.Text = "其他：";
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访症状其他,
            this.xrLabel100});
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 1.5D;
            // 
            // txt第2次随访症状其他
            // 
            this.txt第2次随访症状其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访症状其他.LocationFloat = new DevExpress.Utils.PointFloat(4.999969F, 25.00002F);
            this.txt第2次随访症状其他.Name = "txt第2次随访症状其他";
            this.txt第2次随访症状其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访症状其他.SizeF = new System.Drawing.SizeF(150F, 105F);
            this.txt第2次随访症状其他.StylePriority.UseBorders = false;
            this.txt第2次随访症状其他.StylePriority.UseTextAlignment = false;
            this.txt第2次随访症状其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel100
            // 
            this.xrLabel100.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel100.LocationFloat = new DevExpress.Utils.PointFloat(3.000005F, 0F);
            this.xrLabel100.Name = "xrLabel100";
            this.xrLabel100.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel100.SizeF = new System.Drawing.SizeF(50F, 25F);
            this.xrLabel100.StylePriority.UseBorders = false;
            this.xrLabel100.Text = "其他：";
            // 
            // xrTable8
            // 
            this.xrTable8.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable8.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(116.0003F, 137.9167F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable8.SizeF = new System.Drawing.SizeF(330F, 40.00003F);
            this.xrTable8.StylePriority.UseBorders = false;
            this.xrTable8.StylePriority.UseFont = false;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell15});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1D;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访症状6,
            this.txt第1次随访症状7,
            this.xrLabel69,
            this.txt第1次随访症状5,
            this.xrLabel71,
            this.txt第1次随访症状4,
            this.xrLabel73,
            this.txt第1次随访症状3,
            this.xrLabel75,
            this.txt第1次随访症状2,
            this.xrLabel77,
            this.txt第1次随访症状1,
            this.xrLabel79,
            this.xrLabel80,
            this.txt第1次随访症状8});
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Weight = 1.5000000257749815D;
            // 
            // txt第1次随访症状6
            // 
            this.txt第1次随访症状6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访症状6.CanGrow = false;
            this.txt第1次随访症状6.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访症状6.LocationFloat = new DevExpress.Utils.PointFloat(105.8572F, 7.499969F);
            this.txt第1次随访症状6.Name = "txt第1次随访症状6";
            this.txt第1次随访症状6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访症状6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访症状6.StylePriority.UseBorders = false;
            this.txt第1次随访症状6.StylePriority.UseFont = false;
            // 
            // txt第1次随访症状7
            // 
            this.txt第1次随访症状7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访症状7.CanGrow = false;
            this.txt第1次随访症状7.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访症状7.LocationFloat = new DevExpress.Utils.PointFloat(126.4286F, 7.500032F);
            this.txt第1次随访症状7.Name = "txt第1次随访症状7";
            this.txt第1次随访症状7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访症状7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访症状7.StylePriority.UseBorders = false;
            this.txt第1次随访症状7.StylePriority.UseFont = false;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel69.CanGrow = false;
            this.xrLabel69.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(141.7143F, 7.500032F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel69.StylePriority.UseBorders = false;
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.Text = "/";
            // 
            // txt第1次随访症状5
            // 
            this.txt第1次随访症状5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访症状5.CanGrow = false;
            this.txt第1次随访症状5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访症状5.LocationFloat = new DevExpress.Utils.PointFloat(85.28573F, 7.500032F);
            this.txt第1次随访症状5.Name = "txt第1次随访症状5";
            this.txt第1次随访症状5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访症状5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访症状5.StylePriority.UseBorders = false;
            this.txt第1次随访症状5.StylePriority.UseFont = false;
            // 
            // xrLabel71
            // 
            this.xrLabel71.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel71.CanGrow = false;
            this.xrLabel71.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(100.5714F, 7.500032F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel71.StylePriority.UseBorders = false;
            this.xrLabel71.StylePriority.UseFont = false;
            this.xrLabel71.Text = "/";
            // 
            // txt第1次随访症状4
            // 
            this.txt第1次随访症状4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访症状4.CanGrow = false;
            this.txt第1次随访症状4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访症状4.LocationFloat = new DevExpress.Utils.PointFloat(64.71429F, 7.5F);
            this.txt第1次随访症状4.Name = "txt第1次随访症状4";
            this.txt第1次随访症状4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访症状4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访症状4.StylePriority.UseBorders = false;
            this.txt第1次随访症状4.StylePriority.UseFont = false;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel73.CanGrow = false;
            this.xrLabel73.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(80.00001F, 7.5F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel73.StylePriority.UseBorders = false;
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.Text = "/";
            // 
            // txt第1次随访症状3
            // 
            this.txt第1次随访症状3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访症状3.CanGrow = false;
            this.txt第1次随访症状3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访症状3.LocationFloat = new DevExpress.Utils.PointFloat(44.14285F, 7.500032F);
            this.txt第1次随访症状3.Name = "txt第1次随访症状3";
            this.txt第1次随访症状3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访症状3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访症状3.StylePriority.UseBorders = false;
            this.txt第1次随访症状3.StylePriority.UseFont = false;
            // 
            // xrLabel75
            // 
            this.xrLabel75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel75.CanGrow = false;
            this.xrLabel75.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel75.LocationFloat = new DevExpress.Utils.PointFloat(59.42856F, 7.500032F);
            this.xrLabel75.Name = "xrLabel75";
            this.xrLabel75.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel75.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel75.StylePriority.UseBorders = false;
            this.xrLabel75.StylePriority.UseFont = false;
            this.xrLabel75.Text = "/";
            // 
            // txt第1次随访症状2
            // 
            this.txt第1次随访症状2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访症状2.CanGrow = false;
            this.txt第1次随访症状2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访症状2.LocationFloat = new DevExpress.Utils.PointFloat(23.57141F, 7.499969F);
            this.txt第1次随访症状2.Name = "txt第1次随访症状2";
            this.txt第1次随访症状2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访症状2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访症状2.StylePriority.UseBorders = false;
            this.txt第1次随访症状2.StylePriority.UseFont = false;
            // 
            // xrLabel77
            // 
            this.xrLabel77.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel77.CanGrow = false;
            this.xrLabel77.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel77.LocationFloat = new DevExpress.Utils.PointFloat(38.85713F, 7.500032F);
            this.xrLabel77.Name = "xrLabel77";
            this.xrLabel77.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel77.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel77.StylePriority.UseBorders = false;
            this.xrLabel77.StylePriority.UseFont = false;
            this.xrLabel77.Text = "/";
            // 
            // txt第1次随访症状1
            // 
            this.txt第1次随访症状1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访症状1.CanGrow = false;
            this.txt第1次随访症状1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访症状1.LocationFloat = new DevExpress.Utils.PointFloat(2.999977F, 7.499969F);
            this.txt第1次随访症状1.Name = "txt第1次随访症状1";
            this.txt第1次随访症状1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访症状1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访症状1.StylePriority.UseBorders = false;
            this.txt第1次随访症状1.StylePriority.UseFont = false;
            // 
            // xrLabel79
            // 
            this.xrLabel79.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel79.CanGrow = false;
            this.xrLabel79.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(18.2857F, 7.5F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel79.StylePriority.UseBorders = false;
            this.xrLabel79.StylePriority.UseFont = false;
            this.xrLabel79.Text = "/";
            // 
            // xrLabel80
            // 
            this.xrLabel80.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel80.CanGrow = false;
            this.xrLabel80.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(121.1429F, 7.500032F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel80.StylePriority.UseBorders = false;
            this.xrLabel80.StylePriority.UseFont = false;
            this.xrLabel80.Text = "/";
            // 
            // txt第1次随访症状8
            // 
            this.txt第1次随访症状8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访症状8.CanGrow = false;
            this.txt第1次随访症状8.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访症状8.LocationFloat = new DevExpress.Utils.PointFloat(147F, 7.500032F);
            this.txt第1次随访症状8.Name = "txt第1次随访症状8";
            this.txt第1次随访症状8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访症状8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访症状8.StylePriority.UseBorders = false;
            this.txt第1次随访症状8.StylePriority.UseFont = false;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.CanGrow = false;
            this.xrTableCell15.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访症状6,
            this.txt第2次随访症状7,
            this.xrLabel84,
            this.txt第2次随访症状5,
            this.xrLabel86,
            this.txt第2次随访症状4,
            this.xrLabel88,
            this.txt第2次随访症状3,
            this.xrLabel90,
            this.txt第2次随访症状2,
            this.xrLabel92,
            this.txt第2次随访症状1,
            this.xrLabel94,
            this.xrLabel95,
            this.txt第2次随访症状8});
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 1.4999999742250185D;
            // 
            // txt第2次随访症状6
            // 
            this.txt第2次随访症状6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访症状6.CanGrow = false;
            this.txt第2次随访症状6.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访症状6.LocationFloat = new DevExpress.Utils.PointFloat(105.8572F, 7.499969F);
            this.txt第2次随访症状6.Name = "txt第2次随访症状6";
            this.txt第2次随访症状6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访症状6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访症状6.StylePriority.UseBorders = false;
            this.txt第2次随访症状6.StylePriority.UseFont = false;
            // 
            // txt第2次随访症状7
            // 
            this.txt第2次随访症状7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访症状7.CanGrow = false;
            this.txt第2次随访症状7.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访症状7.LocationFloat = new DevExpress.Utils.PointFloat(126.4286F, 7.500032F);
            this.txt第2次随访症状7.Name = "txt第2次随访症状7";
            this.txt第2次随访症状7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访症状7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访症状7.StylePriority.UseBorders = false;
            this.txt第2次随访症状7.StylePriority.UseFont = false;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel84.CanGrow = false;
            this.xrLabel84.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(141.7143F, 7.500032F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel84.StylePriority.UseBorders = false;
            this.xrLabel84.StylePriority.UseFont = false;
            this.xrLabel84.Text = "/";
            // 
            // txt第2次随访症状5
            // 
            this.txt第2次随访症状5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访症状5.CanGrow = false;
            this.txt第2次随访症状5.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访症状5.LocationFloat = new DevExpress.Utils.PointFloat(85.28573F, 7.500032F);
            this.txt第2次随访症状5.Name = "txt第2次随访症状5";
            this.txt第2次随访症状5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访症状5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访症状5.StylePriority.UseBorders = false;
            this.txt第2次随访症状5.StylePriority.UseFont = false;
            // 
            // xrLabel86
            // 
            this.xrLabel86.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel86.CanGrow = false;
            this.xrLabel86.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel86.LocationFloat = new DevExpress.Utils.PointFloat(100.5714F, 7.500032F);
            this.xrLabel86.Name = "xrLabel86";
            this.xrLabel86.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel86.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel86.StylePriority.UseBorders = false;
            this.xrLabel86.StylePriority.UseFont = false;
            this.xrLabel86.Text = "/";
            // 
            // txt第2次随访症状4
            // 
            this.txt第2次随访症状4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访症状4.CanGrow = false;
            this.txt第2次随访症状4.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访症状4.LocationFloat = new DevExpress.Utils.PointFloat(64.71429F, 7.5F);
            this.txt第2次随访症状4.Name = "txt第2次随访症状4";
            this.txt第2次随访症状4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访症状4.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访症状4.StylePriority.UseBorders = false;
            this.txt第2次随访症状4.StylePriority.UseFont = false;
            // 
            // xrLabel88
            // 
            this.xrLabel88.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel88.CanGrow = false;
            this.xrLabel88.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel88.LocationFloat = new DevExpress.Utils.PointFloat(80.00001F, 7.5F);
            this.xrLabel88.Name = "xrLabel88";
            this.xrLabel88.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel88.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel88.StylePriority.UseBorders = false;
            this.xrLabel88.StylePriority.UseFont = false;
            this.xrLabel88.Text = "/";
            // 
            // txt第2次随访症状3
            // 
            this.txt第2次随访症状3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访症状3.CanGrow = false;
            this.txt第2次随访症状3.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访症状3.LocationFloat = new DevExpress.Utils.PointFloat(44.14285F, 7.500032F);
            this.txt第2次随访症状3.Name = "txt第2次随访症状3";
            this.txt第2次随访症状3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访症状3.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访症状3.StylePriority.UseBorders = false;
            this.txt第2次随访症状3.StylePriority.UseFont = false;
            // 
            // xrLabel90
            // 
            this.xrLabel90.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel90.CanGrow = false;
            this.xrLabel90.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel90.LocationFloat = new DevExpress.Utils.PointFloat(59.42856F, 7.500032F);
            this.xrLabel90.Name = "xrLabel90";
            this.xrLabel90.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel90.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel90.StylePriority.UseBorders = false;
            this.xrLabel90.StylePriority.UseFont = false;
            this.xrLabel90.Text = "/";
            // 
            // txt第2次随访症状2
            // 
            this.txt第2次随访症状2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访症状2.CanGrow = false;
            this.txt第2次随访症状2.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访症状2.LocationFloat = new DevExpress.Utils.PointFloat(23.57141F, 7.499969F);
            this.txt第2次随访症状2.Name = "txt第2次随访症状2";
            this.txt第2次随访症状2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访症状2.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访症状2.StylePriority.UseBorders = false;
            this.txt第2次随访症状2.StylePriority.UseFont = false;
            // 
            // xrLabel92
            // 
            this.xrLabel92.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel92.CanGrow = false;
            this.xrLabel92.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(38.85713F, 7.500032F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel92.StylePriority.UseBorders = false;
            this.xrLabel92.StylePriority.UseFont = false;
            this.xrLabel92.Text = "/";
            // 
            // txt第2次随访症状1
            // 
            this.txt第2次随访症状1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访症状1.CanGrow = false;
            this.txt第2次随访症状1.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访症状1.LocationFloat = new DevExpress.Utils.PointFloat(2.999977F, 7.499969F);
            this.txt第2次随访症状1.Name = "txt第2次随访症状1";
            this.txt第2次随访症状1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访症状1.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访症状1.StylePriority.UseBorders = false;
            this.txt第2次随访症状1.StylePriority.UseFont = false;
            // 
            // xrLabel94
            // 
            this.xrLabel94.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel94.CanGrow = false;
            this.xrLabel94.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(18.2857F, 7.5F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel94.StylePriority.UseBorders = false;
            this.xrLabel94.StylePriority.UseFont = false;
            this.xrLabel94.Text = "/";
            // 
            // xrLabel95
            // 
            this.xrLabel95.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel95.CanGrow = false;
            this.xrLabel95.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.xrLabel95.LocationFloat = new DevExpress.Utils.PointFloat(121.1429F, 7.500032F);
            this.xrLabel95.Name = "xrLabel95";
            this.xrLabel95.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel95.SizeF = new System.Drawing.SizeF(5F, 15F);
            this.xrLabel95.StylePriority.UseBorders = false;
            this.xrLabel95.StylePriority.UseFont = false;
            this.xrLabel95.Text = "/";
            // 
            // txt第2次随访症状8
            // 
            this.txt第2次随访症状8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访症状8.CanGrow = false;
            this.txt第2次随访症状8.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访症状8.LocationFloat = new DevExpress.Utils.PointFloat(147F, 7.500032F);
            this.txt第2次随访症状8.Name = "txt第2次随访症状8";
            this.txt第2次随访症状8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访症状8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访症状8.StylePriority.UseBorders = false;
            this.txt第2次随访症状8.StylePriority.UseFont = false;
            // 
            // xrTable62
            // 
            this.xrTable62.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable62.LocationFloat = new DevExpress.Utils.PointFloat(446.0003F, 1057.917F);
            this.xrTable62.Name = "xrTable62";
            this.xrTable62.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow62});
            this.xrTable62.SizeF = new System.Drawing.SizeF(330F, 30F);
            this.xrTable62.StylePriority.UseBorders = false;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell150,
            this.xrTableCell151});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 1D;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访医生签名});
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访医生签名
            // 
            this.txt第3次随访医生签名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访医生签名.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访医生签名.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 1.999982F);
            this.txt第3次随访医生签名.Name = "txt第3次随访医生签名";
            this.txt第3次随访医生签名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访医生签名.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第3次随访医生签名.StylePriority.UseBorders = false;
            this.txt第3次随访医生签名.StylePriority.UseFont = false;
            this.txt第3次随访医生签名.StylePriority.UseTextAlignment = false;
            this.txt第3次随访医生签名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访医生签名});
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访医生签名
            // 
            this.txt第4次随访医生签名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访医生签名.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访医生签名.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 1.999982F);
            this.txt第4次随访医生签名.Name = "txt第4次随访医生签名";
            this.txt第4次随访医生签名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访医生签名.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第4次随访医生签名.StylePriority.UseBorders = false;
            this.txt第4次随访医生签名.StylePriority.UseFont = false;
            this.txt第4次随访医生签名.StylePriority.UseTextAlignment = false;
            this.txt第4次随访医生签名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable63
            // 
            this.xrTable63.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable63.LocationFloat = new DevExpress.Utils.PointFloat(0.0003496806F, 1057.917F);
            this.xrTable63.Name = "xrTable63";
            this.xrTable63.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow63});
            this.xrTable63.SizeF = new System.Drawing.SizeF(446F, 30F);
            this.xrTable63.StylePriority.UseBorders = false;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell152,
            this.xrTableCell153,
            this.xrTableCell154});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 1D;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.StylePriority.UseFont = false;
            this.xrTableCell152.Text = "随访医生签名";
            this.xrTableCell152.Weight = 0.78378369460019082D;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访医生签名});
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Weight = 1.1148647592047134D;
            // 
            // txt第1次随访医生签名
            // 
            this.txt第1次随访医生签名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访医生签名.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访医生签名.LocationFloat = new DevExpress.Utils.PointFloat(8.000088F, 1.999982F);
            this.txt第1次随访医生签名.Name = "txt第1次随访医生签名";
            this.txt第1次随访医生签名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt第1次随访医生签名.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第1次随访医生签名.StylePriority.UseBorders = false;
            this.txt第1次随访医生签名.StylePriority.UseFont = false;
            this.txt第1次随访医生签名.StylePriority.UseTextAlignment = false;
            this.txt第1次随访医生签名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访医生签名});
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Weight = 1.1148647220543551D;
            // 
            // txt第2次随访医生签名
            // 
            this.txt第2次随访医生签名.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访医生签名.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访医生签名.LocationFloat = new DevExpress.Utils.PointFloat(6.000074F, 1.999982F);
            this.txt第2次随访医生签名.Name = "txt第2次随访医生签名";
            this.txt第2次随访医生签名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访医生签名.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第2次随访医生签名.StylePriority.UseBorders = false;
            this.txt第2次随访医生签名.StylePriority.UseFont = false;
            this.txt第2次随访医生签名.StylePriority.UseTextAlignment = false;
            this.txt第2次随访医生签名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable60
            // 
            this.xrTable60.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable60.LocationFloat = new DevExpress.Utils.PointFloat(445.9998F, 1027.917F);
            this.xrTable60.Name = "xrTable60";
            this.xrTable60.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow60});
            this.xrTable60.SizeF = new System.Drawing.SizeF(330F, 30F);
            this.xrTable60.StylePriority.UseBorders = false;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell145,
            this.xrTableCell146});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 1D;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访下次随访日期});
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访下次随访日期
            // 
            this.txt第3次随访下次随访日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访下次随访日期.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访下次随访日期.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 1.999982F);
            this.txt第3次随访下次随访日期.Name = "txt第3次随访下次随访日期";
            this.txt第3次随访下次随访日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访下次随访日期.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第3次随访下次随访日期.StylePriority.UseBorders = false;
            this.txt第3次随访下次随访日期.StylePriority.UseFont = false;
            this.txt第3次随访下次随访日期.StylePriority.UseTextAlignment = false;
            this.txt第3次随访下次随访日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访下次随访日期});
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访下次随访日期
            // 
            this.txt第4次随访下次随访日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访下次随访日期.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访下次随访日期.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 1.999982F);
            this.txt第4次随访下次随访日期.Name = "txt第4次随访下次随访日期";
            this.txt第4次随访下次随访日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访下次随访日期.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第4次随访下次随访日期.StylePriority.UseBorders = false;
            this.txt第4次随访下次随访日期.StylePriority.UseFont = false;
            this.txt第4次随访下次随访日期.StylePriority.UseTextAlignment = false;
            this.txt第4次随访下次随访日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable61
            // 
            this.xrTable61.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable61.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1027.917F);
            this.xrTable61.Name = "xrTable61";
            this.xrTable61.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow61});
            this.xrTable61.SizeF = new System.Drawing.SizeF(446F, 30F);
            this.xrTable61.StylePriority.UseBorders = false;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell147,
            this.xrTableCell148,
            this.xrTableCell149});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.Weight = 1D;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseFont = false;
            this.xrTableCell147.Text = "下次随访日期";
            this.xrTableCell147.Weight = 0.78378369460019082D;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访下次随访日期});
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 1.1148647592047134D;
            // 
            // txt第1次随访下次随访日期
            // 
            this.txt第1次随访下次随访日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访下次随访日期.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访下次随访日期.LocationFloat = new DevExpress.Utils.PointFloat(8.000088F, 1.999982F);
            this.txt第1次随访下次随访日期.Name = "txt第1次随访下次随访日期";
            this.txt第1次随访下次随访日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt第1次随访下次随访日期.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第1次随访下次随访日期.StylePriority.UseBorders = false;
            this.txt第1次随访下次随访日期.StylePriority.UseFont = false;
            this.txt第1次随访下次随访日期.StylePriority.UseTextAlignment = false;
            this.txt第1次随访下次随访日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访下次随访日期});
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Weight = 1.1148647220543551D;
            // 
            // txt第2次随访下次随访日期
            // 
            this.txt第2次随访下次随访日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访下次随访日期.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访下次随访日期.LocationFloat = new DevExpress.Utils.PointFloat(6.000074F, 1.999982F);
            this.txt第2次随访下次随访日期.Name = "txt第2次随访下次随访日期";
            this.txt第2次随访下次随访日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访下次随访日期.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第2次随访下次随访日期.StylePriority.UseBorders = false;
            this.txt第2次随访下次随访日期.StylePriority.UseFont = false;
            this.txt第2次随访下次随访日期.StylePriority.UseTextAlignment = false;
            this.txt第2次随访下次随访日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable58
            // 
            this.xrTable58.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable58.LocationFloat = new DevExpress.Utils.PointFloat(20.00027F, 1002.917F);
            this.xrTable58.Name = "xrTable58";
            this.xrTable58.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow58});
            this.xrTable58.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable58.StylePriority.UseBorders = false;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell140,
            this.xrTableCell141,
            this.xrTableCell142});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 1D;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Text = "机构及科别";
            this.xrTableCell140.Weight = 0.96000045776367182D;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访转诊机构});
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Weight = 1.649999978150547D;
            // 
            // txt第1次随访转诊机构
            // 
            this.txt第1次随访转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访转诊机构.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(4.999725F, 2.000061F);
            this.txt第1次随访转诊机构.Name = "txt第1次随访转诊机构";
            this.txt第1次随访转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访转诊机构.SizeF = new System.Drawing.SizeF(155.2499F, 22.99994F);
            this.txt第1次随访转诊机构.StylePriority.UseBorders = false;
            this.txt第1次随访转诊机构.StylePriority.UseFont = false;
            this.txt第1次随访转诊机构.StylePriority.UseTextAlignment = false;
            this.txt第1次随访转诊机构.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访转诊机构});
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访转诊机构
            // 
            this.txt第2次随访转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访转诊机构.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(7.499947F, 1.999939F);
            this.txt第2次随访转诊机构.Name = "txt第2次随访转诊机构";
            this.txt第2次随访转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访转诊机构.SizeF = new System.Drawing.SizeF(154.1246F, 22.99994F);
            this.txt第2次随访转诊机构.StylePriority.UseBorders = false;
            this.txt第2次随访转诊机构.StylePriority.UseFont = false;
            this.txt第2次随访转诊机构.StylePriority.UseTextAlignment = false;
            this.txt第2次随访转诊机构.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable59
            // 
            this.xrTable59.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable59.LocationFloat = new DevExpress.Utils.PointFloat(446.0004F, 1002.917F);
            this.xrTable59.Name = "xrTable59";
            this.xrTable59.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow59});
            this.xrTable59.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable59.StylePriority.UseBorders = false;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell143,
            this.xrTableCell144});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 1D;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访转诊机构});
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访转诊机构
            // 
            this.txt第3次随访转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访转诊机构.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(7.499939F, 0F);
            this.txt第3次随访转诊机构.Name = "txt第3次随访转诊机构";
            this.txt第3次随访转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访转诊机构.SizeF = new System.Drawing.SizeF(154.1246F, 23F);
            this.txt第3次随访转诊机构.StylePriority.UseBorders = false;
            this.txt第3次随访转诊机构.StylePriority.UseFont = false;
            this.txt第3次随访转诊机构.StylePriority.UseTextAlignment = false;
            this.txt第3次随访转诊机构.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访转诊机构});
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访转诊机构
            // 
            this.txt第4次随访转诊机构.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访转诊机构.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访转诊机构.LocationFloat = new DevExpress.Utils.PointFloat(7.499878F, 0F);
            this.txt第4次随访转诊机构.Name = "txt第4次随访转诊机构";
            this.txt第4次随访转诊机构.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访转诊机构.SizeF = new System.Drawing.SizeF(152.7496F, 23F);
            this.txt第4次随访转诊机构.StylePriority.UseBorders = false;
            this.txt第4次随访转诊机构.StylePriority.UseFont = false;
            this.txt第4次随访转诊机构.StylePriority.UseTextAlignment = false;
            this.txt第4次随访转诊机构.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable56
            // 
            this.xrTable56.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable56.LocationFloat = new DevExpress.Utils.PointFloat(20.00023F, 977.9171F);
            this.xrTable56.Name = "xrTable56";
            this.xrTable56.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow56});
            this.xrTable56.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable56.StylePriority.UseBorders = false;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell135,
            this.xrTableCell136,
            this.xrTableCell137});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 1D;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Text = "原因";
            this.xrTableCell135.Weight = 0.96000045776367182D;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访转诊原因});
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Weight = 1.649999978150547D;
            // 
            // txt第1次随访转诊原因
            // 
            this.txt第1次随访转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访转诊原因.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(4.999727F, 2.000061F);
            this.txt第1次随访转诊原因.Name = "txt第1次随访转诊原因";
            this.txt第1次随访转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访转诊原因.SizeF = new System.Drawing.SizeF(157F, 22.99994F);
            this.txt第1次随访转诊原因.StylePriority.UseBorders = false;
            this.txt第1次随访转诊原因.StylePriority.UseFont = false;
            this.txt第1次随访转诊原因.StylePriority.UseTextAlignment = false;
            this.txt第1次随访转诊原因.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访转诊原因});
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访转诊原因
            // 
            this.txt第2次随访转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访转诊原因.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(7.499918F, 1.999939F);
            this.txt第2次随访转诊原因.Name = "txt第2次随访转诊原因";
            this.txt第2次随访转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访转诊原因.SizeF = new System.Drawing.SizeF(152.7497F, 23F);
            this.txt第2次随访转诊原因.StylePriority.UseBorders = false;
            this.txt第2次随访转诊原因.StylePriority.UseFont = false;
            this.txt第2次随访转诊原因.StylePriority.UseTextAlignment = false;
            this.txt第2次随访转诊原因.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable57
            // 
            this.xrTable57.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable57.LocationFloat = new DevExpress.Utils.PointFloat(446.0003F, 977.9171F);
            this.xrTable57.Name = "xrTable57";
            this.xrTable57.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow57});
            this.xrTable57.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable57.StylePriority.UseBorders = false;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell138,
            this.xrTableCell139});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 1D;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访转诊原因});
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访转诊原因
            // 
            this.txt第3次随访转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访转诊原因.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(7.499908F, 0F);
            this.txt第3次随访转诊原因.Name = "txt第3次随访转诊原因";
            this.txt第3次随访转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访转诊原因.SizeF = new System.Drawing.SizeF(154.9998F, 23F);
            this.txt第3次随访转诊原因.StylePriority.UseBorders = false;
            this.txt第3次随访转诊原因.StylePriority.UseFont = false;
            this.txt第3次随访转诊原因.StylePriority.UseTextAlignment = false;
            this.txt第3次随访转诊原因.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访转诊原因});
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访转诊原因
            // 
            this.txt第4次随访转诊原因.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访转诊原因.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访转诊原因.LocationFloat = new DevExpress.Utils.PointFloat(7.499908F, 0F);
            this.txt第4次随访转诊原因.Name = "txt第4次随访转诊原因";
            this.txt第4次随访转诊原因.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访转诊原因.SizeF = new System.Drawing.SizeF(154.1246F, 23F);
            this.txt第4次随访转诊原因.StylePriority.UseBorders = false;
            this.txt第4次随访转诊原因.StylePriority.UseFont = false;
            this.txt第4次随访转诊原因.StylePriority.UseTextAlignment = false;
            this.txt第4次随访转诊原因.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel366
            // 
            this.xrLabel366.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel366.LocationFloat = new DevExpress.Utils.PointFloat(0F, 977.9171F);
            this.xrLabel366.Name = "xrLabel366";
            this.xrLabel366.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel366.SizeF = new System.Drawing.SizeF(20.00006F, 50F);
            this.xrLabel366.StylePriority.UseBorders = false;
            this.xrLabel366.Text = "转诊";
            // 
            // xrTable54
            // 
            this.xrTable54.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable54.LocationFloat = new DevExpress.Utils.PointFloat(446.0003F, 952.9171F);
            this.xrTable54.Name = "xrTable54";
            this.xrTable54.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow54});
            this.xrTable54.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable54.StylePriority.UseBorders = false;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell130,
            this.xrTableCell131});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 1D;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel342,
            this.xrLabel343,
            this.xrLabel344,
            this.xrLabel345,
            this.xrLabel346,
            this.xrLabel347});
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Weight = 1.5000000257749815D;
            // 
            // xrLabel342
            // 
            this.xrLabel342.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel342.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrLabel342.LocationFloat = new DevExpress.Utils.PointFloat(114.9819F, 0F);
            this.xrLabel342.Name = "xrLabel342";
            this.xrLabel342.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel342.SizeF = new System.Drawing.SizeF(26.64319F, 23F);
            this.xrLabel342.StylePriority.UseBorders = false;
            this.xrLabel342.StylePriority.UseFont = false;
            this.xrLabel342.StylePriority.UseTextAlignment = false;
            this.xrLabel342.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel343
            // 
            this.xrLabel343.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel343.LocationFloat = new DevExpress.Utils.PointFloat(82.98186F, 3.210703E-05F);
            this.xrLabel343.Name = "xrLabel343";
            this.xrLabel343.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel343.SizeF = new System.Drawing.SizeF(32F, 23F);
            this.xrLabel343.StylePriority.UseBorders = false;
            this.xrLabel343.StylePriority.UseTextAlignment = false;
            this.xrLabel343.Text = "每次";
            this.xrLabel343.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel344
            // 
            this.xrLabel344.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel344.LocationFloat = new DevExpress.Utils.PointFloat(141.6251F, 3.210703E-05F);
            this.xrLabel344.Name = "xrLabel344";
            this.xrLabel344.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel344.SizeF = new System.Drawing.SizeF(20F, 25F);
            this.xrLabel344.StylePriority.UseBorders = false;
            this.xrLabel344.StylePriority.UseTextAlignment = false;
            this.xrLabel344.Text = "mg";
            this.xrLabel344.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel345
            // 
            this.xrLabel345.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel345.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrLabel345.LocationFloat = new DevExpress.Utils.PointFloat(35.37495F, 0F);
            this.xrLabel345.Name = "xrLabel345";
            this.xrLabel345.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel345.SizeF = new System.Drawing.SizeF(26.64319F, 23F);
            this.xrLabel345.StylePriority.UseBorders = false;
            this.xrLabel345.StylePriority.UseFont = false;
            this.xrLabel345.StylePriority.UseTextAlignment = false;
            this.xrLabel345.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel346
            // 
            this.xrLabel346.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel346.LocationFloat = new DevExpress.Utils.PointFloat(3.374954F, 3.178902E-07F);
            this.xrLabel346.Name = "xrLabel346";
            this.xrLabel346.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel346.SizeF = new System.Drawing.SizeF(32F, 23F);
            this.xrLabel346.StylePriority.UseBorders = false;
            this.xrLabel346.StylePriority.UseTextAlignment = false;
            this.xrLabel346.Text = "每日";
            this.xrLabel346.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel347
            // 
            this.xrLabel347.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel347.LocationFloat = new DevExpress.Utils.PointFloat(62.01814F, 3.178902E-07F);
            this.xrLabel347.Name = "xrLabel347";
            this.xrLabel347.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel347.SizeF = new System.Drawing.SizeF(20F, 25F);
            this.xrLabel347.StylePriority.UseBorders = false;
            this.xrLabel347.StylePriority.UseTextAlignment = false;
            this.xrLabel347.Text = "次";
            this.xrLabel347.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel348,
            this.xrLabel349,
            this.xrLabel350,
            this.xrLabel351,
            this.xrLabel352,
            this.xrLabel353});
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Weight = 1.4999999742250185D;
            // 
            // xrLabel348
            // 
            this.xrLabel348.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel348.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrLabel348.LocationFloat = new DevExpress.Utils.PointFloat(114.9819F, 0F);
            this.xrLabel348.Name = "xrLabel348";
            this.xrLabel348.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel348.SizeF = new System.Drawing.SizeF(26.64319F, 23F);
            this.xrLabel348.StylePriority.UseBorders = false;
            this.xrLabel348.StylePriority.UseFont = false;
            this.xrLabel348.StylePriority.UseTextAlignment = false;
            this.xrLabel348.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel349
            // 
            this.xrLabel349.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel349.LocationFloat = new DevExpress.Utils.PointFloat(82.98186F, 3.210703E-05F);
            this.xrLabel349.Name = "xrLabel349";
            this.xrLabel349.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel349.SizeF = new System.Drawing.SizeF(32F, 23F);
            this.xrLabel349.StylePriority.UseBorders = false;
            this.xrLabel349.StylePriority.UseTextAlignment = false;
            this.xrLabel349.Text = "每次";
            this.xrLabel349.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel350
            // 
            this.xrLabel350.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel350.LocationFloat = new DevExpress.Utils.PointFloat(141.6251F, 3.210703E-05F);
            this.xrLabel350.Name = "xrLabel350";
            this.xrLabel350.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel350.SizeF = new System.Drawing.SizeF(20F, 25F);
            this.xrLabel350.StylePriority.UseBorders = false;
            this.xrLabel350.StylePriority.UseTextAlignment = false;
            this.xrLabel350.Text = "mg";
            this.xrLabel350.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel351
            // 
            this.xrLabel351.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel351.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrLabel351.LocationFloat = new DevExpress.Utils.PointFloat(35.37495F, 0F);
            this.xrLabel351.Name = "xrLabel351";
            this.xrLabel351.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel351.SizeF = new System.Drawing.SizeF(26.64319F, 23F);
            this.xrLabel351.StylePriority.UseBorders = false;
            this.xrLabel351.StylePriority.UseFont = false;
            this.xrLabel351.StylePriority.UseTextAlignment = false;
            this.xrLabel351.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel352
            // 
            this.xrLabel352.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel352.LocationFloat = new DevExpress.Utils.PointFloat(3.374954F, 3.178902E-07F);
            this.xrLabel352.Name = "xrLabel352";
            this.xrLabel352.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel352.SizeF = new System.Drawing.SizeF(32F, 23F);
            this.xrLabel352.StylePriority.UseBorders = false;
            this.xrLabel352.StylePriority.UseTextAlignment = false;
            this.xrLabel352.Text = "每日";
            this.xrLabel352.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel353
            // 
            this.xrLabel353.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel353.LocationFloat = new DevExpress.Utils.PointFloat(62.01814F, 3.178902E-07F);
            this.xrLabel353.Name = "xrLabel353";
            this.xrLabel353.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel353.SizeF = new System.Drawing.SizeF(20F, 25F);
            this.xrLabel353.StylePriority.UseBorders = false;
            this.xrLabel353.StylePriority.UseTextAlignment = false;
            this.xrLabel353.Text = "次";
            this.xrLabel353.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable55
            // 
            this.xrTable55.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable55.LocationFloat = new DevExpress.Utils.PointFloat(20.00023F, 952.9171F);
            this.xrTable55.Name = "xrTable55";
            this.xrTable55.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow55});
            this.xrTable55.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable55.StylePriority.UseBorders = false;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell132,
            this.xrTableCell133,
            this.xrTableCell134});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 1D;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Text = "用法用量";
            this.xrTableCell132.Weight = 0.96000045776367182D;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel354,
            this.xrLabel355,
            this.xrLabel356,
            this.txt第1次随访其他药物用法,
            this.xrLabel358,
            this.xrLabel359});
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Weight = 1.649999978150547D;
            // 
            // xrLabel354
            // 
            this.xrLabel354.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel354.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrLabel354.LocationFloat = new DevExpress.Utils.PointFloat(116.3568F, 0F);
            this.xrLabel354.Name = "xrLabel354";
            this.xrLabel354.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel354.SizeF = new System.Drawing.SizeF(26.64319F, 23F);
            this.xrLabel354.StylePriority.UseBorders = false;
            this.xrLabel354.StylePriority.UseFont = false;
            this.xrLabel354.StylePriority.UseTextAlignment = false;
            this.xrLabel354.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel355
            // 
            this.xrLabel355.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel355.LocationFloat = new DevExpress.Utils.PointFloat(84.35678F, 6.357829E-05F);
            this.xrLabel355.Name = "xrLabel355";
            this.xrLabel355.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel355.SizeF = new System.Drawing.SizeF(32F, 23F);
            this.xrLabel355.StylePriority.UseBorders = false;
            this.xrLabel355.StylePriority.UseTextAlignment = false;
            this.xrLabel355.Text = "每次";
            this.xrLabel355.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel356
            // 
            this.xrLabel356.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel356.LocationFloat = new DevExpress.Utils.PointFloat(143F, 6.357829E-05F);
            this.xrLabel356.Name = "xrLabel356";
            this.xrLabel356.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel356.SizeF = new System.Drawing.SizeF(20F, 25F);
            this.xrLabel356.StylePriority.UseBorders = false;
            this.xrLabel356.StylePriority.UseTextAlignment = false;
            this.xrLabel356.Text = "mg";
            this.xrLabel356.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第1次随访其他药物用法
            // 
            this.txt第1次随访其他药物用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访其他药物用法.Font = new System.Drawing.Font("仿宋", 10F);
            this.txt第1次随访其他药物用法.LocationFloat = new DevExpress.Utils.PointFloat(36.74987F, 0F);
            this.txt第1次随访其他药物用法.Name = "txt第1次随访其他药物用法";
            this.txt第1次随访其他药物用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访其他药物用法.SizeF = new System.Drawing.SizeF(26.64319F, 23F);
            this.txt第1次随访其他药物用法.StylePriority.UseBorders = false;
            this.txt第1次随访其他药物用法.StylePriority.UseFont = false;
            this.txt第1次随访其他药物用法.StylePriority.UseTextAlignment = false;
            this.txt第1次随访其他药物用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel358
            // 
            this.xrLabel358.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel358.LocationFloat = new DevExpress.Utils.PointFloat(4.74987F, 3.178914E-05F);
            this.xrLabel358.Name = "xrLabel358";
            this.xrLabel358.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel358.SizeF = new System.Drawing.SizeF(32F, 23F);
            this.xrLabel358.StylePriority.UseBorders = false;
            this.xrLabel358.StylePriority.UseTextAlignment = false;
            this.xrLabel358.Text = "每日";
            this.xrLabel358.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel359
            // 
            this.xrLabel359.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel359.LocationFloat = new DevExpress.Utils.PointFloat(63.39305F, 3.178914E-05F);
            this.xrLabel359.Name = "xrLabel359";
            this.xrLabel359.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel359.SizeF = new System.Drawing.SizeF(20F, 25F);
            this.xrLabel359.StylePriority.UseBorders = false;
            this.xrLabel359.StylePriority.UseTextAlignment = false;
            this.xrLabel359.Text = "次";
            this.xrLabel359.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel360,
            this.xrLabel361,
            this.xrLabel362,
            this.xrLabel363,
            this.xrLabel364,
            this.xrLabel365});
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Weight = 1.6500000218494533D;
            // 
            // xrLabel360
            // 
            this.xrLabel360.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel360.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrLabel360.LocationFloat = new DevExpress.Utils.PointFloat(114.9819F, 0F);
            this.xrLabel360.Name = "xrLabel360";
            this.xrLabel360.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel360.SizeF = new System.Drawing.SizeF(26.64319F, 23F);
            this.xrLabel360.StylePriority.UseBorders = false;
            this.xrLabel360.StylePriority.UseFont = false;
            this.xrLabel360.StylePriority.UseTextAlignment = false;
            this.xrLabel360.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel361
            // 
            this.xrLabel361.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel361.LocationFloat = new DevExpress.Utils.PointFloat(82.98186F, 3.210703E-05F);
            this.xrLabel361.Name = "xrLabel361";
            this.xrLabel361.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel361.SizeF = new System.Drawing.SizeF(32F, 23F);
            this.xrLabel361.StylePriority.UseBorders = false;
            this.xrLabel361.StylePriority.UseTextAlignment = false;
            this.xrLabel361.Text = "每次";
            this.xrLabel361.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel362
            // 
            this.xrLabel362.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel362.LocationFloat = new DevExpress.Utils.PointFloat(141.6251F, 3.210703E-05F);
            this.xrLabel362.Name = "xrLabel362";
            this.xrLabel362.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel362.SizeF = new System.Drawing.SizeF(20F, 25F);
            this.xrLabel362.StylePriority.UseBorders = false;
            this.xrLabel362.StylePriority.UseTextAlignment = false;
            this.xrLabel362.Text = "mg";
            this.xrLabel362.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel363
            // 
            this.xrLabel363.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrLabel363.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrLabel363.LocationFloat = new DevExpress.Utils.PointFloat(35.37495F, 0F);
            this.xrLabel363.Name = "xrLabel363";
            this.xrLabel363.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel363.SizeF = new System.Drawing.SizeF(26.64319F, 23F);
            this.xrLabel363.StylePriority.UseBorders = false;
            this.xrLabel363.StylePriority.UseFont = false;
            this.xrLabel363.StylePriority.UseTextAlignment = false;
            this.xrLabel363.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel364
            // 
            this.xrLabel364.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel364.LocationFloat = new DevExpress.Utils.PointFloat(3.374954F, 3.178902E-07F);
            this.xrLabel364.Name = "xrLabel364";
            this.xrLabel364.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel364.SizeF = new System.Drawing.SizeF(32F, 23F);
            this.xrLabel364.StylePriority.UseBorders = false;
            this.xrLabel364.StylePriority.UseTextAlignment = false;
            this.xrLabel364.Text = "每日";
            this.xrLabel364.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel365
            // 
            this.xrLabel365.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrLabel365.LocationFloat = new DevExpress.Utils.PointFloat(62.01814F, 3.178902E-07F);
            this.xrLabel365.Name = "xrLabel365";
            this.xrLabel365.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel365.SizeF = new System.Drawing.SizeF(20F, 25F);
            this.xrLabel365.StylePriority.UseBorders = false;
            this.xrLabel365.StylePriority.UseTextAlignment = false;
            this.xrLabel365.Text = "次";
            this.xrLabel365.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable52
            // 
            this.xrTable52.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable52.LocationFloat = new DevExpress.Utils.PointFloat(19.99976F, 927.9171F);
            this.xrTable52.Name = "xrTable52";
            this.xrTable52.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow52});
            this.xrTable52.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable52.StylePriority.UseBorders = false;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.xrTableCell126,
            this.xrTableCell127});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 1D;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Text = "其他药物";
            this.xrTableCell125.Weight = 0.96000045776367182D;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访其他药物});
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Weight = 1.649999978150547D;
            // 
            // txt第1次随访其他药物
            // 
            this.txt第1次随访其他药物.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访其他药物.LocationFloat = new DevExpress.Utils.PointFloat(4.999729F, 2.000061F);
            this.txt第1次随访其他药物.Name = "txt第1次随访其他药物";
            this.txt第1次随访其他药物.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访其他药物.SizeF = new System.Drawing.SizeF(158.0008F, 22.99994F);
            this.txt第1次随访其他药物.StylePriority.UseBorders = false;
            this.txt第1次随访其他药物.StylePriority.UseTextAlignment = false;
            this.txt第1次随访其他药物.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访其他药物});
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访其他药物
            // 
            this.txt第2次随访其他药物.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访其他药物.LocationFloat = new DevExpress.Utils.PointFloat(7.499935F, 1.999939F);
            this.txt第2次随访其他药物.Name = "txt第2次随访其他药物";
            this.txt第2次随访其他药物.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访其他药物.SizeF = new System.Drawing.SizeF(152.7502F, 23F);
            this.txt第2次随访其他药物.StylePriority.UseBorders = false;
            this.txt第2次随访其他药物.StylePriority.UseTextAlignment = false;
            this.txt第2次随访其他药物.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable53
            // 
            this.xrTable53.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable53.LocationFloat = new DevExpress.Utils.PointFloat(445.9998F, 927.9171F);
            this.xrTable53.Name = "xrTable53";
            this.xrTable53.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow53});
            this.xrTable53.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable53.StylePriority.UseBorders = false;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell128,
            this.xrTableCell129});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 1D;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访其他药物});
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访其他药物
            // 
            this.txt第3次随访其他药物.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访其他药物.LocationFloat = new DevExpress.Utils.PointFloat(7.499939F, 0F);
            this.txt第3次随访其他药物.Name = "txt第3次随访其他药物";
            this.txt第3次随访其他药物.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访其他药物.SizeF = new System.Drawing.SizeF(150.0003F, 23F);
            this.txt第3次随访其他药物.StylePriority.UseBorders = false;
            this.txt第3次随访其他药物.StylePriority.UseTextAlignment = false;
            this.txt第3次随访其他药物.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访其他药物});
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访其他药物
            // 
            this.txt第4次随访其他药物.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访其他药物.LocationFloat = new DevExpress.Utils.PointFloat(7.499908F, 0F);
            this.txt第4次随访其他药物.Name = "txt第4次随访其他药物";
            this.txt第4次随访其他药物.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访其他药物.SizeF = new System.Drawing.SizeF(150.679F, 23F);
            this.txt第4次随访其他药物.StylePriority.UseBorders = false;
            this.txt第4次随访其他药物.StylePriority.UseTextAlignment = false;
            this.txt第4次随访其他药物.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable48
            // 
            this.xrTable48.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable48.LocationFloat = new DevExpress.Utils.PointFloat(446.0003F, 877.9169F);
            this.xrTable48.Name = "xrTable48";
            this.xrTable48.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow48});
            this.xrTable48.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable48.StylePriority.UseBorders = false;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell115,
            this.xrTableCell116});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.Weight = 1D;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访药物名称3});
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访药物名称3
            // 
            this.txt第3次随访药物名称3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访药物名称3.LocationFloat = new DevExpress.Utils.PointFloat(7.499939F, 0F);
            this.txt第3次随访药物名称3.Name = "txt第3次随访药物名称3";
            this.txt第3次随访药物名称3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访药物名称3.SizeF = new System.Drawing.SizeF(149.9997F, 23F);
            this.txt第3次随访药物名称3.StylePriority.UseBorders = false;
            this.txt第3次随访药物名称3.StylePriority.UseTextAlignment = false;
            this.txt第3次随访药物名称3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访药物名称3});
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访药物名称3
            // 
            this.txt第4次随访药物名称3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访药物名称3.LocationFloat = new DevExpress.Utils.PointFloat(7.499878F, 0F);
            this.txt第4次随访药物名称3.Name = "txt第4次随访药物名称3";
            this.txt第4次随访药物名称3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访药物名称3.SizeF = new System.Drawing.SizeF(149.9998F, 23F);
            this.txt第4次随访药物名称3.StylePriority.UseBorders = false;
            this.txt第4次随访药物名称3.StylePriority.UseTextAlignment = false;
            this.txt第4次随访药物名称3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable49
            // 
            this.xrTable49.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable49.LocationFloat = new DevExpress.Utils.PointFloat(20.00023F, 877.9169F);
            this.xrTable49.Name = "xrTable49";
            this.xrTable49.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow49});
            this.xrTable49.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable49.StylePriority.UseBorders = false;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell119});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 1D;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Text = "药物名称3";
            this.xrTableCell117.Weight = 0.96000045776367182D;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访药物名称3});
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Weight = 1.649999978150547D;
            // 
            // txt第1次随访药物名称3
            // 
            this.txt第1次随访药物名称3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访药物名称3.LocationFloat = new DevExpress.Utils.PointFloat(4.999723F, 2F);
            this.txt第1次随访药物名称3.Name = "txt第1次随访药物名称3";
            this.txt第1次随访药物名称3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访药物名称3.SizeF = new System.Drawing.SizeF(155.2499F, 23F);
            this.txt第1次随访药物名称3.StylePriority.UseBorders = false;
            this.txt第1次随访药物名称3.StylePriority.UseTextAlignment = false;
            this.txt第1次随访药物名称3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访药物名称3});
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访药物名称3
            // 
            this.txt第2次随访药物名称3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访药物名称3.LocationFloat = new DevExpress.Utils.PointFloat(7.499952F, 1.999939F);
            this.txt第2次随访药物名称3.Name = "txt第2次随访药物名称3";
            this.txt第2次随访药物名称3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访药物名称3.SizeF = new System.Drawing.SizeF(152.7496F, 23F);
            this.txt第2次随访药物名称3.StylePriority.UseBorders = false;
            this.txt第2次随访药物名称3.StylePriority.UseTextAlignment = false;
            this.txt第2次随访药物名称3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable51
            // 
            this.xrTable51.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable51.LocationFloat = new DevExpress.Utils.PointFloat(20.00023F, 902.9169F);
            this.xrTable51.Name = "xrTable51";
            this.xrTable51.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow51});
            this.xrTable51.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable51.StylePriority.UseBorders = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell122,
            this.xrTableCell123,
            this.xrTableCell124});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 1D;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "用法用量";
            this.xrTableCell122.Weight = 0.96000045776367182D;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访药物3用法});
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Weight = 1.649999978150547D;
            // 
            // txt第1次随访药物3用法
            // 
            this.txt第1次随访药物3用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访药物3用法.LocationFloat = new DevExpress.Utils.PointFloat(5.249487F, 0F);
            this.txt第1次随访药物3用法.Name = "txt第1次随访药物3用法";
            this.txt第1次随访药物3用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访药物3用法.SizeF = new System.Drawing.SizeF(152.7503F, 23F);
            this.txt第1次随访药物3用法.StylePriority.UseBorders = false;
            this.txt第1次随访药物3用法.StylePriority.UseTextAlignment = false;
            this.txt第1次随访药物3用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访药物3用法});
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访药物3用法
            // 
            this.txt第2次随访药物3用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次随访药物3用法.LocationFloat = new DevExpress.Utils.PointFloat(6.499708F, 0F);
            this.txt第2次随访药物3用法.Name = "txt第2次随访药物3用法";
            this.txt第2次随访药物3用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访药物3用法.SizeF = new System.Drawing.SizeF(155.0002F, 23F);
            this.txt第2次随访药物3用法.StylePriority.UseBorders = false;
            this.txt第2次随访药物3用法.StylePriority.UseTextAlignment = false;
            this.txt第2次随访药物3用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable50
            // 
            this.xrTable50.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable50.LocationFloat = new DevExpress.Utils.PointFloat(446.0003F, 902.9169F);
            this.xrTable50.Name = "xrTable50";
            this.xrTable50.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow50});
            this.xrTable50.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable50.StylePriority.UseBorders = false;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell120,
            this.xrTableCell121});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 1D;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访药物3用法});
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访药物3用法
            // 
            this.txt第3次随访药物3用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次随访药物3用法.LocationFloat = new DevExpress.Utils.PointFloat(6.999268F, 0F);
            this.txt第3次随访药物3用法.Name = "txt第3次随访药物3用法";
            this.txt第3次随访药物3用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访药物3用法.SizeF = new System.Drawing.SizeF(150.5005F, 23F);
            this.txt第3次随访药物3用法.StylePriority.UseBorders = false;
            this.txt第3次随访药物3用法.StylePriority.UseTextAlignment = false;
            this.txt第3次随访药物3用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访药物3用法});
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访药物3用法
            // 
            this.txt第4次随访药物3用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次随访药物3用法.LocationFloat = new DevExpress.Utils.PointFloat(7.749756F, 0F);
            this.txt第4次随访药物3用法.Name = "txt第4次随访药物3用法";
            this.txt第4次随访药物3用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访药物3用法.SizeF = new System.Drawing.SizeF(150.4286F, 23F);
            this.txt第4次随访药物3用法.StylePriority.UseBorders = false;
            this.txt第4次随访药物3用法.StylePriority.UseTextAlignment = false;
            this.txt第4次随访药物3用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable44
            // 
            this.xrTable44.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable44.LocationFloat = new DevExpress.Utils.PointFloat(445.9998F, 827.9169F);
            this.xrTable44.Name = "xrTable44";
            this.xrTable44.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow44});
            this.xrTable44.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable44.StylePriority.UseBorders = false;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell106});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1D;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访药物名称2});
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访药物名称2
            // 
            this.txt第3次随访药物名称2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访药物名称2.LocationFloat = new DevExpress.Utils.PointFloat(7.499939F, 0F);
            this.txt第3次随访药物名称2.Name = "txt第3次随访药物名称2";
            this.txt第3次随访药物名称2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访药物名称2.SizeF = new System.Drawing.SizeF(150.0003F, 23F);
            this.txt第3次随访药物名称2.StylePriority.UseBorders = false;
            this.txt第3次随访药物名称2.StylePriority.UseTextAlignment = false;
            this.txt第3次随访药物名称2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访药物名称2});
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访药物名称2
            // 
            this.txt第4次随访药物名称2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访药物名称2.LocationFloat = new DevExpress.Utils.PointFloat(7.499908F, 0F);
            this.txt第4次随访药物名称2.Name = "txt第4次随访药物名称2";
            this.txt第4次随访药物名称2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访药物名称2.SizeF = new System.Drawing.SizeF(150.679F, 23F);
            this.txt第4次随访药物名称2.StylePriority.UseBorders = false;
            this.txt第4次随访药物名称2.StylePriority.UseTextAlignment = false;
            this.txt第4次随访药物名称2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable45
            // 
            this.xrTable45.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable45.LocationFloat = new DevExpress.Utils.PointFloat(19.99979F, 827.9169F);
            this.xrTable45.Name = "xrTable45";
            this.xrTable45.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow45});
            this.xrTable45.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable45.StylePriority.UseBorders = false;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell109});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1D;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Text = "药物名称2";
            this.xrTableCell107.Weight = 0.96000045776367182D;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访药物名称2});
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Weight = 1.649999978150547D;
            // 
            // txt第1次随访药物名称2
            // 
            this.txt第1次随访药物名称2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访药物名称2.LocationFloat = new DevExpress.Utils.PointFloat(4.999729F, 2F);
            this.txt第1次随访药物名称2.Name = "txt第1次随访药物名称2";
            this.txt第1次随访药物名称2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访药物名称2.SizeF = new System.Drawing.SizeF(155.2505F, 23F);
            this.txt第1次随访药物名称2.StylePriority.UseBorders = false;
            this.txt第1次随访药物名称2.StylePriority.UseTextAlignment = false;
            this.txt第1次随访药物名称2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访药物名称2});
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访药物名称2
            // 
            this.txt第2次随访药物名称2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访药物名称2.LocationFloat = new DevExpress.Utils.PointFloat(7.499935F, 1.999939F);
            this.txt第2次随访药物名称2.Name = "txt第2次随访药物名称2";
            this.txt第2次随访药物名称2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访药物名称2.SizeF = new System.Drawing.SizeF(152.7501F, 23F);
            this.txt第2次随访药物名称2.StylePriority.UseBorders = false;
            this.txt第2次随访药物名称2.StylePriority.UseTextAlignment = false;
            this.txt第2次随访药物名称2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable47
            // 
            this.xrTable47.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable47.LocationFloat = new DevExpress.Utils.PointFloat(19.99979F, 852.9169F);
            this.xrTable47.Name = "xrTable47";
            this.xrTable47.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow47});
            this.xrTable47.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable47.StylePriority.UseBorders = false;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.xrTableCell113,
            this.xrTableCell114});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 1D;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Text = "用法用量";
            this.xrTableCell112.Weight = 0.96000045776367182D;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访药物2用法});
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 1.649999978150547D;
            // 
            // txt第1次随访药物2用法
            // 
            this.txt第1次随访药物2用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访药物2用法.LocationFloat = new DevExpress.Utils.PointFloat(5.249928F, 0F);
            this.txt第1次随访药物2用法.Name = "txt第1次随访药物2用法";
            this.txt第1次随访药物2用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访药物2用法.SizeF = new System.Drawing.SizeF(155.0002F, 23F);
            this.txt第1次随访药物2用法.StylePriority.UseBorders = false;
            this.txt第1次随访药物2用法.StylePriority.UseTextAlignment = false;
            this.txt第1次随访药物2用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访药物2用法});
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访药物2用法
            // 
            this.txt第2次随访药物2用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次随访药物2用法.LocationFloat = new DevExpress.Utils.PointFloat(6.500149F, 0F);
            this.txt第2次随访药物2用法.Name = "txt第2次随访药物2用法";
            this.txt第2次随访药物2用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访药物2用法.SizeF = new System.Drawing.SizeF(155.0002F, 23F);
            this.txt第2次随访药物2用法.StylePriority.UseBorders = false;
            this.txt第2次随访药物2用法.StylePriority.UseTextAlignment = false;
            this.txt第2次随访药物2用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable46
            // 
            this.xrTable46.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable46.LocationFloat = new DevExpress.Utils.PointFloat(445.9998F, 852.9169F);
            this.xrTable46.Name = "xrTable46";
            this.xrTable46.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow46});
            this.xrTable46.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable46.StylePriority.UseBorders = false;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell110,
            this.xrTableCell111});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1D;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访药物2用法});
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访药物2用法
            // 
            this.txt第3次随访药物2用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次随访药物2用法.LocationFloat = new DevExpress.Utils.PointFloat(8.017975F, 0F);
            this.txt第3次随访药物2用法.Name = "txt第3次随访药物2用法";
            this.txt第3次随访药物2用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访药物2用法.SizeF = new System.Drawing.SizeF(150.1607F, 23F);
            this.txt第3次随访药物2用法.StylePriority.UseBorders = false;
            this.txt第3次随访药物2用法.StylePriority.UseTextAlignment = false;
            this.txt第3次随访药物2用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访药物2用法});
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访药物2用法
            // 
            this.txt第4次随访药物2用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次随访药物2用法.LocationFloat = new DevExpress.Utils.PointFloat(6.000031F, 0F);
            this.txt第4次随访药物2用法.Name = "txt第4次随访药物2用法";
            this.txt第4次随访药物2用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访药物2用法.SizeF = new System.Drawing.SizeF(152.1786F, 23F);
            this.txt第4次随访药物2用法.StylePriority.UseBorders = false;
            this.txt第4次随访药物2用法.StylePriority.UseTextAlignment = false;
            this.txt第4次随访药物2用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable42
            // 
            this.xrTable42.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable42.LocationFloat = new DevExpress.Utils.PointFloat(20.00014F, 802.9169F);
            this.xrTable42.Name = "xrTable42";
            this.xrTable42.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42});
            this.xrTable42.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable42.StylePriority.UseBorders = false;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 1D;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Text = "用法用量";
            this.xrTableCell100.Weight = 0.96000045776367182D;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访药物1用法});
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 1.649999978150547D;
            // 
            // txt第1次随访药物1用法
            // 
            this.txt第1次随访药物1用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访药物1用法.LocationFloat = new DevExpress.Utils.PointFloat(6.642614F, 0F);
            this.txt第1次随访药物1用法.Name = "txt第1次随访药物1用法";
            this.txt第1次随访药物1用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访药物1用法.SizeF = new System.Drawing.SizeF(151.3575F, 23F);
            this.txt第1次随访药物1用法.StylePriority.UseBorders = false;
            this.txt第1次随访药物1用法.StylePriority.UseTextAlignment = false;
            this.txt第1次随访药物1用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访药物1用法});
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访药物1用法
            // 
            this.txt第2次随访药物1用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次随访药物1用法.LocationFloat = new DevExpress.Utils.PointFloat(6.4998F, 0F);
            this.txt第2次随访药物1用法.Name = "txt第2次随访药物1用法";
            this.txt第2次随访药物1用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访药物1用法.SizeF = new System.Drawing.SizeF(154.5002F, 23F);
            this.txt第2次随访药物1用法.StylePriority.UseBorders = false;
            this.txt第2次随访药物1用法.StylePriority.UseTextAlignment = false;
            this.txt第2次随访药物1用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable43
            // 
            this.xrTable43.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable43.LocationFloat = new DevExpress.Utils.PointFloat(446.0002F, 802.9169F);
            this.xrTable43.Name = "xrTable43";
            this.xrTable43.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow43});
            this.xrTable43.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable43.StylePriority.UseBorders = false;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.xrTableCell104});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1D;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访药物1用法});
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访药物1用法
            // 
            this.txt第3次随访药物1用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次随访药物1用法.LocationFloat = new DevExpress.Utils.PointFloat(8.017548F, 0F);
            this.txt第3次随访药物1用法.Name = "txt第3次随访药物1用法";
            this.txt第3次随访药物1用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访药物1用法.SizeF = new System.Drawing.SizeF(149.4822F, 23F);
            this.txt第3次随访药物1用法.StylePriority.UseBorders = false;
            this.txt第3次随访药物1用法.StylePriority.UseTextAlignment = false;
            this.txt第3次随访药物1用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访药物1用法});
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访药物1用法
            // 
            this.txt第4次随访药物1用法.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次随访药物1用法.LocationFloat = new DevExpress.Utils.PointFloat(5.999542F, 0F);
            this.txt第4次随访药物1用法.Name = "txt第4次随访药物1用法";
            this.txt第4次随访药物1用法.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访药物1用法.SizeF = new System.Drawing.SizeF(152.1788F, 23F);
            this.txt第4次随访药物1用法.StylePriority.UseBorders = false;
            this.txt第4次随访药物1用法.StylePriority.UseTextAlignment = false;
            this.txt第4次随访药物1用法.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable40
            // 
            this.xrTable40.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable40.LocationFloat = new DevExpress.Utils.PointFloat(20.00014F, 777.9169F);
            this.xrTable40.Name = "xrTable40";
            this.xrTable40.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40});
            this.xrTable40.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable40.StylePriority.UseBorders = false;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell95,
            this.xrTableCell96,
            this.xrTableCell97});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1D;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Text = "药物名称1";
            this.xrTableCell95.Weight = 0.96000045776367182D;
            // 
            // xrTableCell96
            // 
            this.xrTableCell96.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访药物名称1});
            this.xrTableCell96.Name = "xrTableCell96";
            this.xrTableCell96.Weight = 1.649999978150547D;
            // 
            // txt第1次随访药物名称1
            // 
            this.txt第1次随访药物名称1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访药物名称1.LocationFloat = new DevExpress.Utils.PointFloat(4.999731F, 2F);
            this.txt第1次随访药物名称1.Name = "txt第1次随访药物名称1";
            this.txt第1次随访药物名称1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访药物名称1.SizeF = new System.Drawing.SizeF(155.2501F, 23F);
            this.txt第1次随访药物名称1.StylePriority.UseBorders = false;
            this.txt第1次随访药物名称1.StylePriority.UseTextAlignment = false;
            this.txt第1次随访药物名称1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访药物名称1});
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访药物名称1
            // 
            this.txt第2次随访药物名称1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访药物名称1.LocationFloat = new DevExpress.Utils.PointFloat(7.499952F, 1.999939F);
            this.txt第2次随访药物名称1.Name = "txt第2次随访药物名称1";
            this.txt第2次随访药物名称1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访药物名称1.SizeF = new System.Drawing.SizeF(152.7498F, 23F);
            this.txt第2次随访药物名称1.StylePriority.UseBorders = false;
            this.txt第2次随访药物名称1.StylePriority.UseTextAlignment = false;
            this.txt第2次随访药物名称1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable41
            // 
            this.xrTable41.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable41.LocationFloat = new DevExpress.Utils.PointFloat(446.0002F, 777.9169F);
            this.xrTable41.Name = "xrTable41";
            this.xrTable41.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41});
            this.xrTable41.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable41.StylePriority.UseBorders = false;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell98,
            this.xrTableCell99});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1D;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访药物名称1});
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访药物名称1
            // 
            this.txt第3次随访药物名称1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访药物名称1.LocationFloat = new DevExpress.Utils.PointFloat(7.499908F, 0F);
            this.txt第3次随访药物名称1.Name = "txt第3次随访药物名称1";
            this.txt第3次随访药物名称1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访药物名称1.SizeF = new System.Drawing.SizeF(152.1247F, 23F);
            this.txt第3次随访药物名称1.StylePriority.UseBorders = false;
            this.txt第3次随访药物名称1.StylePriority.UseTextAlignment = false;
            this.txt第3次随访药物名称1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访药物名称1});
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访药物名称1
            // 
            this.txt第4次随访药物名称1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访药物名称1.LocationFloat = new DevExpress.Utils.PointFloat(7.499908F, 0F);
            this.txt第4次随访药物名称1.Name = "txt第4次随访药物名称1";
            this.txt第4次随访药物名称1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访药物名称1.SizeF = new System.Drawing.SizeF(150.6786F, 23F);
            this.txt第4次随访药物名称1.StylePriority.UseBorders = false;
            this.txt第4次随访药物名称1.StylePriority.UseTextAlignment = false;
            this.txt第4次随访药物名称1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel253
            // 
            this.xrLabel253.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel253.LocationFloat = new DevExpress.Utils.PointFloat(0F, 777.9167F);
            this.xrLabel253.Name = "xrLabel253";
            this.xrLabel253.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel253.SizeF = new System.Drawing.SizeF(20F, 200.0004F);
            this.xrLabel253.StylePriority.UseBorders = false;
            this.xrLabel253.StylePriority.UseTextAlignment = false;
            this.xrLabel253.Text = "用药情况";
            this.xrLabel253.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable38
            // 
            this.xrTable38.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable38.LocationFloat = new DevExpress.Utils.PointFloat(0F, 727.9167F);
            this.xrTable38.Name = "xrTable38";
            this.xrTable38.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable38.SizeF = new System.Drawing.SizeF(446F, 50F);
            this.xrTable38.StylePriority.UseBorders = false;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell90,
            this.xrTableCell91,
            this.xrTableCell92});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1D;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.StylePriority.UseFont = false;
            this.xrTableCell90.Text = "此次随访分类";
            this.xrTableCell90.Weight = 0.78378369460019082D;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访分类,
            this.xrLabel241,
            this.xrLabel242});
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Weight = 1.1148647592047134D;
            // 
            // txt第1次随访分类
            // 
            this.txt第1次随访分类.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访分类.CanGrow = false;
            this.txt第1次随访分类.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访分类.LocationFloat = new DevExpress.Utils.PointFloat(147F, 24.99987F);
            this.txt第1次随访分类.Name = "txt第1次随访分类";
            this.txt第1次随访分类.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访分类.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访分类.StylePriority.UseBorders = false;
            this.txt第1次随访分类.StylePriority.UseFont = false;
            // 
            // xrLabel241
            // 
            this.xrLabel241.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel241.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel241.LocationFloat = new DevExpress.Utils.PointFloat(5.249771F, 5F);
            this.xrLabel241.Name = "xrLabel241";
            this.xrLabel241.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel241.SizeF = new System.Drawing.SizeF(152.7505F, 20.00006F);
            this.xrLabel241.StylePriority.UseBorders = false;
            this.xrLabel241.StylePriority.UseFont = false;
            this.xrLabel241.StylePriority.UseTextAlignment = false;
            this.xrLabel241.Text = "1控制满意 2控制不满意 ";
            this.xrLabel241.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel242
            // 
            this.xrLabel242.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel242.LocationFloat = new DevExpress.Utils.PointFloat(4.750053F, 24.99998F);
            this.xrLabel242.Name = "xrLabel242";
            this.xrLabel242.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel242.SizeF = new System.Drawing.SizeF(135.4999F, 20F);
            this.xrLabel242.StylePriority.UseBorders = false;
            this.xrLabel242.StylePriority.UseTextAlignment = false;
            this.xrLabel242.Text = "3不良反应 4并发症 ";
            this.xrLabel242.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访分类,
            this.xrLabel247,
            this.xrLabel248});
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 1.1148647220543551D;
            // 
            // txt第2次随访分类
            // 
            this.txt第2次随访分类.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访分类.CanGrow = false;
            this.txt第2次随访分类.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访分类.LocationFloat = new DevExpress.Utils.PointFloat(146.0002F, 25F);
            this.txt第2次随访分类.Name = "txt第2次随访分类";
            this.txt第2次随访分类.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访分类.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访分类.StylePriority.UseBorders = false;
            this.txt第2次随访分类.StylePriority.UseFont = false;
            // 
            // xrLabel247
            // 
            this.xrLabel247.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel247.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel247.LocationFloat = new DevExpress.Utils.PointFloat(5.249756F, 5F);
            this.xrLabel247.Name = "xrLabel247";
            this.xrLabel247.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel247.SizeF = new System.Drawing.SizeF(155.0002F, 20.00006F);
            this.xrLabel247.StylePriority.UseBorders = false;
            this.xrLabel247.StylePriority.UseFont = false;
            this.xrLabel247.StylePriority.UseTextAlignment = false;
            this.xrLabel247.Text = "1控制满意 2控制不满意 ";
            this.xrLabel247.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel248
            // 
            this.xrLabel248.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel248.LocationFloat = new DevExpress.Utils.PointFloat(4.750053F, 24.99998F);
            this.xrLabel248.Name = "xrLabel248";
            this.xrLabel248.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel248.SizeF = new System.Drawing.SizeF(135.4999F, 20F);
            this.xrLabel248.StylePriority.UseBorders = false;
            this.xrLabel248.StylePriority.UseTextAlignment = false;
            this.xrLabel248.Text = "3不良反应 4并发症 ";
            this.xrLabel248.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable39
            // 
            this.xrTable39.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable39.LocationFloat = new DevExpress.Utils.PointFloat(445.9998F, 727.9167F);
            this.xrTable39.Name = "xrTable39";
            this.xrTable39.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39});
            this.xrTable39.SizeF = new System.Drawing.SizeF(330F, 50.00006F);
            this.xrTable39.StylePriority.UseBorders = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1D;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访分类,
            this.xrLabel244,
            this.xrLabel249});
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访分类
            // 
            this.txt第3次随访分类.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访分类.CanGrow = false;
            this.txt第3次随访分类.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访分类.LocationFloat = new DevExpress.Utils.PointFloat(147.7505F, 24.99994F);
            this.txt第3次随访分类.Name = "txt第3次随访分类";
            this.txt第3次随访分类.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访分类.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访分类.StylePriority.UseBorders = false;
            this.txt第3次随访分类.StylePriority.UseFont = false;
            // 
            // xrLabel244
            // 
            this.xrLabel244.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel244.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel244.LocationFloat = new DevExpress.Utils.PointFloat(5.249786F, 5F);
            this.xrLabel244.Name = "xrLabel244";
            this.xrLabel244.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel244.SizeF = new System.Drawing.SizeF(157.2504F, 20.00006F);
            this.xrLabel244.StylePriority.UseBorders = false;
            this.xrLabel244.StylePriority.UseFont = false;
            this.xrLabel244.StylePriority.UseTextAlignment = false;
            this.xrLabel244.Text = "1控制满意 2控制不满意 ";
            this.xrLabel244.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel249
            // 
            this.xrLabel249.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel249.LocationFloat = new DevExpress.Utils.PointFloat(4.750061F, 25.00002F);
            this.xrLabel249.Name = "xrLabel249";
            this.xrLabel249.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel249.SizeF = new System.Drawing.SizeF(135.4999F, 20F);
            this.xrLabel249.StylePriority.UseBorders = false;
            this.xrLabel249.StylePriority.UseTextAlignment = false;
            this.xrLabel249.Text = "3不良反应 4并发症 ";
            this.xrLabel249.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访分类,
            this.xrLabel251,
            this.xrLabel252});
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访分类
            // 
            this.txt第4次随访分类.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访分类.CanGrow = false;
            this.txt第4次随访分类.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访分类.LocationFloat = new DevExpress.Utils.PointFloat(146.1788F, 24.99994F);
            this.txt第4次随访分类.Name = "txt第4次随访分类";
            this.txt第4次随访分类.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访分类.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访分类.StylePriority.UseBorders = false;
            this.txt第4次随访分类.StylePriority.UseFont = false;
            // 
            // xrLabel251
            // 
            this.xrLabel251.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel251.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel251.LocationFloat = new DevExpress.Utils.PointFloat(5.249786F, 5F);
            this.xrLabel251.Name = "xrLabel251";
            this.xrLabel251.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel251.SizeF = new System.Drawing.SizeF(152.9291F, 20.00006F);
            this.xrLabel251.StylePriority.UseBorders = false;
            this.xrLabel251.StylePriority.UseFont = false;
            this.xrLabel251.StylePriority.UseTextAlignment = false;
            this.xrLabel251.Text = "1控制满意 2控制不满意 ";
            this.xrLabel251.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel252
            // 
            this.xrLabel252.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel252.LocationFloat = new DevExpress.Utils.PointFloat(4.750061F, 25.00002F);
            this.xrLabel252.Name = "xrLabel252";
            this.xrLabel252.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel252.SizeF = new System.Drawing.SizeF(135.4999F, 20F);
            this.xrLabel252.StylePriority.UseBorders = false;
            this.xrLabel252.StylePriority.UseTextAlignment = false;
            this.xrLabel252.Text = "3不良反应 4并发症 ";
            this.xrLabel252.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable36
            // 
            this.xrTable36.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable36.LocationFloat = new DevExpress.Utils.PointFloat(445.9999F, 697.9167F);
            this.xrTable36.Name = "xrTable36";
            this.xrTable36.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable36.SizeF = new System.Drawing.SizeF(330F, 30F);
            this.xrTable36.StylePriority.UseBorders = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.xrTableCell86});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 1D;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel67,
            this.txt第3次随访药物不良反应其他,
            this.txt第3次随访药物不良反应});
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 1.5000000257749815D;
            // 
            // xrLabel67
            // 
            this.xrLabel67.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(6.821314F, 5.000019F);
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(56.75053F, 23F);
            this.xrLabel67.StylePriority.UseBorders = false;
            this.xrLabel67.StylePriority.UseTextAlignment = false;
            this.xrLabel67.Text = "1无 2有";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次随访药物不良反应其他
            // 
            this.txt第3次随访药物不良反应其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次随访药物不良反应其他.LocationFloat = new DevExpress.Utils.PointFloat(68.17864F, 3.500008F);
            this.txt第3次随访药物不良反应其他.Name = "txt第3次随访药物不良反应其他";
            this.txt第3次随访药物不良反应其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访药物不良反应其他.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第3次随访药物不良反应其他.StylePriority.UseBorders = false;
            this.txt第3次随访药物不良反应其他.StylePriority.UseTextAlignment = false;
            this.txt第3次随访药物不良反应其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次随访药物不良反应
            // 
            this.txt第3次随访药物不良反应.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访药物不良反应.CanGrow = false;
            this.txt第3次随访药物不良反应.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访药物不良反应.LocationFloat = new DevExpress.Utils.PointFloat(147.7504F, 4.999987F);
            this.txt第3次随访药物不良反应.Name = "txt第3次随访药物不良反应";
            this.txt第3次随访药物不良反应.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访药物不良反应.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访药物不良反应.StylePriority.UseBorders = false;
            this.txt第3次随访药物不良反应.StylePriority.UseFont = false;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel68,
            this.txt第4次随访药物不良反应其他,
            this.txt第4次随访药物不良反应});
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 1.4999999742250185D;
            // 
            // xrLabel68
            // 
            this.xrLabel68.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(7.677905F, 3.499985F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(56.75053F, 23F);
            this.xrLabel68.StylePriority.UseBorders = false;
            this.xrLabel68.StylePriority.UseTextAlignment = false;
            this.xrLabel68.Text = "1无 2有";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第4次随访药物不良反应其他
            // 
            this.txt第4次随访药物不良反应其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次随访药物不良反应其他.LocationFloat = new DevExpress.Utils.PointFloat(68.17864F, 3.500008F);
            this.txt第4次随访药物不良反应其他.Name = "txt第4次随访药物不良反应其他";
            this.txt第4次随访药物不良反应其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访药物不良反应其他.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第4次随访药物不良反应其他.StylePriority.UseBorders = false;
            this.txt第4次随访药物不良反应其他.StylePriority.UseTextAlignment = false;
            this.txt第4次随访药物不良反应其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第4次随访药物不良反应
            // 
            this.txt第4次随访药物不良反应.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访药物不良反应.CanGrow = false;
            this.txt第4次随访药物不良反应.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访药物不良反应.LocationFloat = new DevExpress.Utils.PointFloat(146.1788F, 4.999987F);
            this.txt第4次随访药物不良反应.Name = "txt第4次随访药物不良反应";
            this.txt第4次随访药物不良反应.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访药物不良反应.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访药物不良反应.StylePriority.UseBorders = false;
            this.txt第4次随访药物不良反应.StylePriority.UseFont = false;
            // 
            // xrTable37
            // 
            this.xrTable37.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable37.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 697.9167F);
            this.xrTable37.Name = "xrTable37";
            this.xrTable37.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37});
            this.xrTable37.SizeF = new System.Drawing.SizeF(446F, 30F);
            this.xrTable37.StylePriority.UseBorders = false;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell89});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1D;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseFont = false;
            this.xrTableCell87.Text = "药物不良反应";
            this.xrTableCell87.Weight = 0.78378369460019082D;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访药物不良反应其他,
            this.txt第1次随访药物不良反应,
            this.xrLabel234});
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Weight = 1.1148647592047134D;
            // 
            // txt第1次随访药物不良反应其他
            // 
            this.txt第1次随访药物不良反应其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访药物不良反应其他.LocationFloat = new DevExpress.Utils.PointFloat(68.00003F, 3.500016F);
            this.txt第1次随访药物不良反应其他.Name = "txt第1次随访药物不良反应其他";
            this.txt第1次随访药物不良反应其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访药物不良反应其他.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第1次随访药物不良反应其他.StylePriority.UseBorders = false;
            this.txt第1次随访药物不良反应其他.StylePriority.UseTextAlignment = false;
            this.txt第1次随访药物不良反应其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第1次随访药物不良反应
            // 
            this.txt第1次随访药物不良反应.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访药物不良反应.CanGrow = false;
            this.txt第1次随访药物不良反应.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访药物不良反应.LocationFloat = new DevExpress.Utils.PointFloat(146.9999F, 4.999987F);
            this.txt第1次随访药物不良反应.Name = "txt第1次随访药物不良反应";
            this.txt第1次随访药物不良反应.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访药物不良反应.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访药物不良反应.StylePriority.UseBorders = false;
            this.txt第1次随访药物不良反应.StylePriority.UseFont = false;
            // 
            // xrLabel234
            // 
            this.xrLabel234.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel234.LocationFloat = new DevExpress.Utils.PointFloat(6.642744F, 3.5F);
            this.xrLabel234.Name = "xrLabel234";
            this.xrLabel234.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel234.SizeF = new System.Drawing.SizeF(56.75053F, 23F);
            this.xrLabel234.StylePriority.UseBorders = false;
            this.xrLabel234.StylePriority.UseTextAlignment = false;
            this.xrLabel234.Text = "1无 2有";
            this.xrLabel234.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel64,
            this.txt第2次随访药物不良反应其他,
            this.txt第2次随访药物不良反应});
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 1.1148647220543551D;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(5.999967F, 3.5F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(58.4288F, 23F);
            this.xrLabel64.StylePriority.UseBorders = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "1无 2有";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次随访药物不良反应其他
            // 
            this.txt第2次随访药物不良反应其他.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次随访药物不良反应其他.LocationFloat = new DevExpress.Utils.PointFloat(67.49973F, 3.500016F);
            this.txt第2次随访药物不良反应其他.Name = "txt第2次随访药物不良反应其他";
            this.txt第2次随访药物不良反应其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访药物不良反应其他.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第2次随访药物不良反应其他.StylePriority.UseBorders = false;
            this.txt第2次随访药物不良反应其他.StylePriority.UseTextAlignment = false;
            this.txt第2次随访药物不良反应其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次随访药物不良反应
            // 
            this.txt第2次随访药物不良反应.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访药物不良反应.CanGrow = false;
            this.txt第2次随访药物不良反应.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访药物不良反应.LocationFloat = new DevExpress.Utils.PointFloat(145.2498F, 4.999987F);
            this.txt第2次随访药物不良反应.Name = "txt第2次随访药物不良反应";
            this.txt第2次随访药物不良反应.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访药物不良反应.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访药物不良反应.StylePriority.UseBorders = false;
            this.txt第2次随访药物不良反应.StylePriority.UseFont = false;
            // 
            // xrTable34
            // 
            this.xrTable34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable34.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 667.9167F);
            this.xrTable34.Name = "xrTable34";
            this.xrTable34.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow34});
            this.xrTable34.SizeF = new System.Drawing.SizeF(446F, 30F);
            this.xrTable34.StylePriority.UseBorders = false;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1D;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.StylePriority.UseFont = false;
            this.xrTableCell80.Text = "服药依从性";
            this.xrTableCell80.Weight = 0.78378369460019082D;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访服药依从性,
            this.xrLabel222});
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Weight = 1.1148647592047134D;
            // 
            // txt第1次随访服药依从性
            // 
            this.txt第1次随访服药依从性.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访服药依从性.CanGrow = false;
            this.txt第1次随访服药依从性.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访服药依从性.LocationFloat = new DevExpress.Utils.PointFloat(147.0003F, 4.999987F);
            this.txt第1次随访服药依从性.Name = "txt第1次随访服药依从性";
            this.txt第1次随访服药依从性.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访服药依从性.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访服药依从性.StylePriority.UseBorders = false;
            this.txt第1次随访服药依从性.StylePriority.UseFont = false;
            // 
            // xrLabel222
            // 
            this.xrLabel222.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel222.LocationFloat = new DevExpress.Utils.PointFloat(6.642744F, 3.5F);
            this.xrLabel222.Name = "xrLabel222";
            this.xrLabel222.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel222.SizeF = new System.Drawing.SizeF(140.0718F, 23F);
            this.xrLabel222.StylePriority.UseBorders = false;
            this.xrLabel222.StylePriority.UseTextAlignment = false;
            this.xrLabel222.Text = "1规律 2间断 3不服药";
            this.xrLabel222.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel35,
            this.txt第2次随访服药依从性});
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Weight = 1.1148647220543551D;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(4.749998F, 3.499985F);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(140.0718F, 23F);
            this.xrLabel35.StylePriority.UseBorders = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "1规律 2间断 3不服药";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次随访服药依从性
            // 
            this.txt第2次随访服药依从性.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访服药依从性.CanGrow = false;
            this.txt第2次随访服药依从性.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访服药依从性.LocationFloat = new DevExpress.Utils.PointFloat(146.0001F, 4.999987F);
            this.txt第2次随访服药依从性.Name = "txt第2次随访服药依从性";
            this.txt第2次随访服药依从性.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访服药依从性.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访服药依从性.StylePriority.UseBorders = false;
            this.txt第2次随访服药依从性.StylePriority.UseFont = false;
            // 
            // xrTable35
            // 
            this.xrTable35.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable35.LocationFloat = new DevExpress.Utils.PointFloat(446F, 667.9167F);
            this.xrTable35.Name = "xrTable35";
            this.xrTable35.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35});
            this.xrTable35.SizeF = new System.Drawing.SizeF(330F, 30F);
            this.xrTable35.StylePriority.UseBorders = false;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.xrTableCell84});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1D;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel50,
            this.txt第3次随访服药依从性});
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 1.5000000257749815D;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(6.821187F, 3.499985F);
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(140.0718F, 23F);
            this.xrLabel50.StylePriority.UseBorders = false;
            this.xrLabel50.StylePriority.UseTextAlignment = false;
            this.xrLabel50.Text = "1规律 2间断 3不服药";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次随访服药依从性
            // 
            this.txt第3次随访服药依从性.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访服药依从性.CanGrow = false;
            this.txt第3次随访服药依从性.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访服药依从性.LocationFloat = new DevExpress.Utils.PointFloat(147.6248F, 4.500016F);
            this.txt第3次随访服药依从性.Name = "txt第3次随访服药依从性";
            this.txt第3次随访服药依从性.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访服药依从性.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访服药依从性.StylePriority.UseBorders = false;
            this.txt第3次随访服药依从性.StylePriority.UseFont = false;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel54,
            this.txt第4次随访服药依从性});
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Weight = 1.4999999742250185D;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(5.999819F, 3.500016F);
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(140.0718F, 23F);
            this.xrLabel54.StylePriority.UseBorders = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "1规律 2间断 3不服药";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第4次随访服药依从性
            // 
            this.txt第4次随访服药依从性.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访服药依从性.CanGrow = false;
            this.txt第4次随访服药依从性.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访服药依从性.LocationFloat = new DevExpress.Utils.PointFloat(146.1786F, 5.000019F);
            this.txt第4次随访服药依从性.Name = "txt第4次随访服药依从性";
            this.txt第4次随访服药依从性.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访服药依从性.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访服药依从性.StylePriority.UseBorders = false;
            this.txt第4次随访服药依从性.StylePriority.UseFont = false;
            // 
            // xrTable32
            // 
            this.xrTable32.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable32.LocationFloat = new DevExpress.Utils.PointFloat(445.9999F, 617.9167F);
            this.xrTable32.Name = "xrTable32";
            this.xrTable32.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow32});
            this.xrTable32.SizeF = new System.Drawing.SizeF(330F, 49.99994F);
            this.xrTable32.StylePriority.UseBorders = false;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell75,
            this.xrTableCell76});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1D;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访辅助检查});
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访辅助检查
            // 
            this.txt第3次随访辅助检查.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访辅助检查.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访辅助检查.LocationFloat = new DevExpress.Utils.PointFloat(6F, 2F);
            this.txt第3次随访辅助检查.Name = "txt第3次随访辅助检查";
            this.txt第3次随访辅助检查.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访辅助检查.SizeF = new System.Drawing.SizeF(150F, 42.99994F);
            this.txt第3次随访辅助检查.StylePriority.UseBorders = false;
            this.txt第3次随访辅助检查.StylePriority.UseFont = false;
            this.txt第3次随访辅助检查.StylePriority.UseTextAlignment = false;
            this.txt第3次随访辅助检查.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访辅助检查});
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访辅助检查
            // 
            this.txt第4次随访辅助检查.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访辅助检查.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访辅助检查.LocationFloat = new DevExpress.Utils.PointFloat(6.000031F, 2F);
            this.txt第4次随访辅助检查.Name = "txt第4次随访辅助检查";
            this.txt第4次随访辅助检查.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访辅助检查.SizeF = new System.Drawing.SizeF(150F, 42.99994F);
            this.txt第4次随访辅助检查.StylePriority.UseBorders = false;
            this.txt第4次随访辅助检查.StylePriority.UseFont = false;
            this.txt第4次随访辅助检查.StylePriority.UseTextAlignment = false;
            this.txt第4次随访辅助检查.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable33
            // 
            this.xrTable33.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable33.LocationFloat = new DevExpress.Utils.PointFloat(0F, 617.9167F);
            this.xrTable33.Name = "xrTable33";
            this.xrTable33.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33});
            this.xrTable33.SizeF = new System.Drawing.SizeF(446F, 50F);
            this.xrTable33.StylePriority.UseBorders = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell77,
            this.xrTableCell78,
            this.xrTableCell79});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1D;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.StylePriority.UseFont = false;
            this.xrTableCell77.Text = "辅助检查";
            this.xrTableCell77.Weight = 0.78378369460019082D;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访辅助检查});
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 1.1148647592047134D;
            // 
            // txt第1次随访辅助检查
            // 
            this.txt第1次随访辅助检查.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访辅助检查.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访辅助检查.LocationFloat = new DevExpress.Utils.PointFloat(2.999908F, 0F);
            this.txt第1次随访辅助检查.Name = "txt第1次随访辅助检查";
            this.txt第1次随访辅助检查.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt第1次随访辅助检查.SizeF = new System.Drawing.SizeF(150F, 45F);
            this.txt第1次随访辅助检查.StylePriority.UseBorders = false;
            this.txt第1次随访辅助检查.StylePriority.UseFont = false;
            this.txt第1次随访辅助检查.StylePriority.UseTextAlignment = false;
            this.txt第1次随访辅助检查.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访辅助检查});
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Weight = 1.1148647220543551D;
            // 
            // txt第2次随访辅助检查
            // 
            this.txt第2次随访辅助检查.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访辅助检查.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访辅助检查.LocationFloat = new DevExpress.Utils.PointFloat(6.000061F, 2F);
            this.txt第2次随访辅助检查.Name = "txt第2次随访辅助检查";
            this.txt第2次随访辅助检查.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访辅助检查.SizeF = new System.Drawing.SizeF(150F, 43F);
            this.txt第2次随访辅助检查.StylePriority.UseBorders = false;
            this.txt第2次随访辅助检查.StylePriority.UseFont = false;
            this.txt第2次随访辅助检查.StylePriority.UseTextAlignment = false;
            this.txt第2次随访辅助检查.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable30
            // 
            this.xrTable30.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable30.LocationFloat = new DevExpress.Utils.PointFloat(445.9999F, 592.9166F);
            this.xrTable30.Name = "xrTable30";
            this.xrTable30.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30});
            this.xrTable30.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable30.StylePriority.UseBorders = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell70,
            this.xrTableCell71});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 1D;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访遵医行为,
            this.xrLabel210});
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访遵医行为
            // 
            this.txt第3次随访遵医行为.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访遵医行为.CanGrow = false;
            this.txt第3次随访遵医行为.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访遵医行为.LocationFloat = new DevExpress.Utils.PointFloat(146.625F, 2.000109F);
            this.txt第3次随访遵医行为.Name = "txt第3次随访遵医行为";
            this.txt第3次随访遵医行为.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访遵医行为.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访遵医行为.StylePriority.UseBorders = false;
            this.txt第3次随访遵医行为.StylePriority.UseFont = false;
            // 
            // xrLabel210
            // 
            this.xrLabel210.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel210.LocationFloat = new DevExpress.Utils.PointFloat(7.499695F, 0F);
            this.xrLabel210.Name = "xrLabel210";
            this.xrLabel210.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel210.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.xrLabel210.StylePriority.UseBorders = false;
            this.xrLabel210.StylePriority.UseTextAlignment = false;
            this.xrLabel210.Text = "1良好  2一般  3差";
            this.xrLabel210.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访遵医行为,
            this.xrLabel212});
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访遵医行为
            // 
            this.txt第4次随访遵医行为.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访遵医行为.CanGrow = false;
            this.txt第4次随访遵医行为.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访遵医行为.LocationFloat = new DevExpress.Utils.PointFloat(146.1788F, 1.999919F);
            this.txt第4次随访遵医行为.Name = "txt第4次随访遵医行为";
            this.txt第4次随访遵医行为.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访遵医行为.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访遵医行为.StylePriority.UseBorders = false;
            this.txt第4次随访遵医行为.StylePriority.UseFont = false;
            // 
            // xrLabel212
            // 
            this.xrLabel212.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel212.LocationFloat = new DevExpress.Utils.PointFloat(4.957962F, 2.000141F);
            this.xrLabel212.Name = "xrLabel212";
            this.xrLabel212.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel212.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.xrLabel212.StylePriority.UseBorders = false;
            this.xrLabel212.StylePriority.UseTextAlignment = false;
            this.xrLabel212.Text = "1良好  2一般  3差";
            this.xrLabel212.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable31
            // 
            this.xrTable31.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable31.LocationFloat = new DevExpress.Utils.PointFloat(19.99989F, 592.9167F);
            this.xrTable31.Name = "xrTable31";
            this.xrTable31.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31});
            this.xrTable31.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable31.StylePriority.UseBorders = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell72,
            this.xrTableCell73,
            this.xrTableCell74});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1D;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Text = "遵医行为";
            this.xrTableCell72.Weight = 0.96000045776367182D;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel62,
            this.txt第1次随访遵医行为});
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Weight = 1.649999978150547D;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(5.249834F, 0F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.xrLabel62.StylePriority.UseBorders = false;
            this.xrLabel62.StylePriority.UseTextAlignment = false;
            this.xrLabel62.Text = "1良好  2一般  3差";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第1次随访遵医行为
            // 
            this.txt第1次随访遵医行为.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访遵医行为.CanGrow = false;
            this.txt第1次随访遵医行为.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访遵医行为.LocationFloat = new DevExpress.Utils.PointFloat(147F, 2F);
            this.txt第1次随访遵医行为.Name = "txt第1次随访遵医行为";
            this.txt第1次随访遵医行为.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访遵医行为.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访遵医行为.StylePriority.UseBorders = false;
            this.txt第1次随访遵医行为.StylePriority.UseFont = false;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访遵医行为,
            this.xrLabel216});
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访遵医行为
            // 
            this.txt第2次随访遵医行为.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访遵医行为.CanGrow = false;
            this.txt第2次随访遵医行为.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访遵医行为.LocationFloat = new DevExpress.Utils.PointFloat(146.0002F, 1.999855F);
            this.txt第2次随访遵医行为.Name = "txt第2次随访遵医行为";
            this.txt第2次随访遵医行为.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访遵医行为.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访遵医行为.StylePriority.UseBorders = false;
            this.txt第2次随访遵医行为.StylePriority.UseFont = false;
            // 
            // xrLabel216
            // 
            this.xrLabel216.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel216.LocationFloat = new DevExpress.Utils.PointFloat(7.499886F, 0F);
            this.xrLabel216.Name = "xrLabel216";
            this.xrLabel216.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel216.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.xrLabel216.StylePriority.UseBorders = false;
            this.xrLabel216.StylePriority.UseTextAlignment = false;
            this.xrLabel216.Text = "1良好  2一般  3差";
            this.xrLabel216.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable28
            // 
            this.xrTable28.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable28.LocationFloat = new DevExpress.Utils.PointFloat(20.00014F, 567.9167F);
            this.xrTable28.Name = "xrTable28";
            this.xrTable28.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28});
            this.xrTable28.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable28.StylePriority.UseBorders = false;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.xrTableCell66,
            this.xrTableCell67});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1D;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Text = "心理调整";
            this.xrTableCell65.Weight = 0.96000045776367182D;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访心理调整,
            this.xrLabel201});
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Weight = 1.649999978150547D;
            // 
            // txt第1次随访心理调整
            // 
            this.txt第1次随访心理调整.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访心理调整.CanGrow = false;
            this.txt第1次随访心理调整.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访心理调整.LocationFloat = new DevExpress.Utils.PointFloat(147.0002F, 2.000046F);
            this.txt第1次随访心理调整.Name = "txt第1次随访心理调整";
            this.txt第1次随访心理调整.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访心理调整.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访心理调整.StylePriority.UseBorders = false;
            this.txt第1次随访心理调整.StylePriority.UseFont = false;
            // 
            // xrLabel201
            // 
            this.xrLabel201.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel201.LocationFloat = new DevExpress.Utils.PointFloat(4.99951F, 1.999887F);
            this.xrLabel201.Name = "xrLabel201";
            this.xrLabel201.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel201.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.xrLabel201.StylePriority.UseBorders = false;
            this.xrLabel201.StylePriority.UseTextAlignment = false;
            this.xrLabel201.Text = "1良好  2一般  3差";
            this.xrLabel201.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访心理调整,
            this.xrLabel202});
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访心理调整
            // 
            this.txt第2次随访心理调整.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访心理调整.CanGrow = false;
            this.txt第2次随访心理调整.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访心理调整.LocationFloat = new DevExpress.Utils.PointFloat(146F, 2.000077F);
            this.txt第2次随访心理调整.Name = "txt第2次随访心理调整";
            this.txt第2次随访心理调整.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访心理调整.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访心理调整.StylePriority.UseBorders = false;
            this.txt第2次随访心理调整.StylePriority.UseFont = false;
            // 
            // xrLabel202
            // 
            this.xrLabel202.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel202.LocationFloat = new DevExpress.Utils.PointFloat(7.499886F, 0F);
            this.xrLabel202.Name = "xrLabel202";
            this.xrLabel202.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel202.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.xrLabel202.StylePriority.UseBorders = false;
            this.xrLabel202.StylePriority.UseTextAlignment = false;
            this.xrLabel202.Text = "1良好  2一般  3差";
            this.xrLabel202.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable29
            // 
            this.xrTable29.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable29.LocationFloat = new DevExpress.Utils.PointFloat(446.0002F, 567.9166F);
            this.xrTable29.Name = "xrTable29";
            this.xrTable29.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow29});
            this.xrTable29.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable29.StylePriority.UseBorders = false;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.xrTableCell69});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1D;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访心理调整,
            this.xrLabel203});
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访心理调整
            // 
            this.txt第3次随访心理调整.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访心理调整.CanGrow = false;
            this.txt第3次随访心理调整.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访心理调整.LocationFloat = new DevExpress.Utils.PointFloat(146.6247F, 1.999919F);
            this.txt第3次随访心理调整.Name = "txt第3次随访心理调整";
            this.txt第3次随访心理调整.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访心理调整.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访心理调整.StylePriority.UseBorders = false;
            this.txt第3次随访心理调整.StylePriority.UseFont = false;
            // 
            // xrLabel203
            // 
            this.xrLabel203.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel203.LocationFloat = new DevExpress.Utils.PointFloat(7.499695F, 0F);
            this.xrLabel203.Name = "xrLabel203";
            this.xrLabel203.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel203.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.xrLabel203.StylePriority.UseBorders = false;
            this.xrLabel203.StylePriority.UseTextAlignment = false;
            this.xrLabel203.Text = "1良好  2一般  3差";
            this.xrLabel203.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访心理调整,
            this.xrLabel204});
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访心理调整
            // 
            this.txt第4次随访心理调整.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访心理调整.CanGrow = false;
            this.txt第4次随访心理调整.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访心理调整.LocationFloat = new DevExpress.Utils.PointFloat(146.1785F, 2.000063F);
            this.txt第4次随访心理调整.Name = "txt第4次随访心理调整";
            this.txt第4次随访心理调整.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访心理调整.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访心理调整.StylePriority.UseBorders = false;
            this.txt第4次随访心理调整.StylePriority.UseFont = false;
            // 
            // xrLabel204
            // 
            this.xrLabel204.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel204.LocationFloat = new DevExpress.Utils.PointFloat(4.957962F, 2.000141F);
            this.xrLabel204.Name = "xrLabel204";
            this.xrLabel204.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel204.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.xrLabel204.StylePriority.UseBorders = false;
            this.xrLabel204.StylePriority.UseTextAlignment = false;
            this.xrLabel204.Text = "1良好  2一般  3差";
            this.xrLabel204.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable26
            // 
            this.xrTable26.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable26.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrTable26.LocationFloat = new DevExpress.Utils.PointFloat(19.99989F, 542.9167F);
            this.xrTable26.Name = "xrTable26";
            this.xrTable26.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable26.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable26.StylePriority.UseBorders = false;
            this.xrTable26.StylePriority.UseFont = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.xrTableCell61,
            this.xrTableCell62});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1D;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.StylePriority.UseFont = false;
            this.xrTableCell60.Text = "摄盐情况(咸淡)";
            this.xrTableCell60.Weight = 0.96000045776367182D;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel58,
            this.txt第1次随访摄盐情况1,
            this.txt第1次随访摄盐情况2,
            this.xrLabel194,
            this.xrLabel189});
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 1.649999978150547D;
            // 
            // xrLabel58
            // 
            this.xrLabel58.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel58.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel58.LocationFloat = new DevExpress.Utils.PointFloat(77.50006F, 0F);
            this.xrLabel58.Name = "xrLabel58";
            this.xrLabel58.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel58.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel58.StylePriority.UseBorders = false;
            this.xrLabel58.StylePriority.UseFont = false;
            this.xrLabel58.StylePriority.UseTextAlignment = false;
            this.xrLabel58.Text = "/";
            this.xrLabel58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第1次随访摄盐情况1
            // 
            this.txt第1次随访摄盐情况1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访摄盐情况1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访摄盐情况1.LocationFloat = new DevExpress.Utils.PointFloat(2.500065F, 0.9999728F);
            this.txt第1次随访摄盐情况1.Name = "txt第1次随访摄盐情况1";
            this.txt第1次随访摄盐情况1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访摄盐情况1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt第1次随访摄盐情况1.StylePriority.UseBorders = false;
            this.txt第1次随访摄盐情况1.StylePriority.UseFont = false;
            this.txt第1次随访摄盐情况1.StylePriority.UseTextAlignment = false;
            this.txt第1次随访摄盐情况1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第1次随访摄盐情况2
            // 
            this.txt第1次随访摄盐情况2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访摄盐情况2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访摄盐情况2.LocationFloat = new DevExpress.Utils.PointFloat(87.9999F, 1.000069F);
            this.txt第1次随访摄盐情况2.Name = "txt第1次随访摄盐情况2";
            this.txt第1次随访摄盐情况2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访摄盐情况2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt第1次随访摄盐情况2.StylePriority.UseBorders = false;
            this.txt第1次随访摄盐情况2.StylePriority.UseFont = false;
            this.txt第1次随访摄盐情况2.StylePriority.UseTextAlignment = false;
            this.txt第1次随访摄盐情况2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel194
            // 
            this.xrLabel194.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel194.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel194.LocationFloat = new DevExpress.Utils.PointFloat(107.9999F, 6.357829E-05F);
            this.xrLabel194.Name = "xrLabel194";
            this.xrLabel194.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel194.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel194.StylePriority.UseBorders = false;
            this.xrLabel194.StylePriority.UseFont = false;
            this.xrLabel194.StylePriority.UseTextAlignment = false;
            this.xrLabel194.Text = "轻/中/重";
            this.xrLabel194.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel189
            // 
            this.xrLabel189.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel189.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel189.LocationFloat = new DevExpress.Utils.PointFloat(22.50056F, 1.000061F);
            this.xrLabel189.Name = "xrLabel189";
            this.xrLabel189.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel189.SizeF = new System.Drawing.SizeF(54.99951F, 23F);
            this.xrLabel189.StylePriority.UseBorders = false;
            this.xrLabel189.StylePriority.UseFont = false;
            this.xrLabel189.StylePriority.UseTextAlignment = false;
            this.xrLabel189.Text = "轻/中/重";
            this.xrLabel189.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel15,
            this.txt第2次随访摄盐情况1,
            this.xrLabel28,
            this.txt第2次随访摄盐情况2,
            this.xrLabel31});
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Weight = 1.6500000218494533D;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(77.74992F, 1.000023F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel15.StylePriority.UseBorders = false;
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "/";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次随访摄盐情况1
            // 
            this.txt第2次随访摄盐情况1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次随访摄盐情况1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访摄盐情况1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 1.000061F);
            this.txt第2次随访摄盐情况1.Name = "txt第2次随访摄盐情况1";
            this.txt第2次随访摄盐情况1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访摄盐情况1.SizeF = new System.Drawing.SizeF(22.25009F, 20F);
            this.txt第2次随访摄盐情况1.StylePriority.UseBorders = false;
            this.txt第2次随访摄盐情况1.StylePriority.UseFont = false;
            this.txt第2次随访摄盐情况1.StylePriority.UseTextAlignment = false;
            this.txt第2次随访摄盐情况1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel28.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(22.25011F, 0.9999657F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel28.StylePriority.UseBorders = false;
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "轻/中/重";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次随访摄盐情况2
            // 
            this.txt第2次随访摄盐情况2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次随访摄盐情况2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访摄盐情况2.LocationFloat = new DevExpress.Utils.PointFloat(87.74989F, 1.000061F);
            this.txt第2次随访摄盐情况2.Name = "txt第2次随访摄盐情况2";
            this.txt第2次随访摄盐情况2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访摄盐情况2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt第2次随访摄盐情况2.StylePriority.UseBorders = false;
            this.txt第2次随访摄盐情况2.StylePriority.UseFont = false;
            this.txt第2次随访摄盐情况2.StylePriority.UseTextAlignment = false;
            this.txt第2次随访摄盐情况2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel31.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(107.7499F, 1.000029F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel31.StylePriority.UseBorders = false;
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "轻/中/重";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable27
            // 
            this.xrTable27.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable27.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrTable27.LocationFloat = new DevExpress.Utils.PointFloat(446F, 542.9167F);
            this.xrTable27.Name = "xrTable27";
            this.xrTable27.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27});
            this.xrTable27.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable27.StylePriority.UseBorders = false;
            this.xrTable27.StylePriority.UseFont = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell64});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1D;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel17,
            this.txt第3次随访摄盐情况1,
            this.xrLabel37,
            this.txt第3次随访摄盐情况2,
            this.xrLabel40});
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 1.5000000257749815D;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(77.74989F, 0F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel17.StylePriority.UseBorders = false;
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "/";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次随访摄盐情况1
            // 
            this.txt第3次随访摄盐情况1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次随访摄盐情况1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访摄盐情况1.LocationFloat = new DevExpress.Utils.PointFloat(2.250076F, 1.000061F);
            this.txt第3次随访摄盐情况1.Name = "txt第3次随访摄盐情况1";
            this.txt第3次随访摄盐情况1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访摄盐情况1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt第3次随访摄盐情况1.StylePriority.UseBorders = false;
            this.txt第3次随访摄盐情况1.StylePriority.UseFont = false;
            this.txt第3次随访摄盐情况1.StylePriority.UseTextAlignment = false;
            this.txt第3次随访摄盐情况1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel37.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(22.2501F, 0.9999657F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel37.StylePriority.UseBorders = false;
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "轻/中/重";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次随访摄盐情况2
            // 
            this.txt第3次随访摄盐情况2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次随访摄盐情况2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访摄盐情况2.LocationFloat = new DevExpress.Utils.PointFloat(87.74991F, 1.000034F);
            this.txt第3次随访摄盐情况2.Name = "txt第3次随访摄盐情况2";
            this.txt第3次随访摄盐情况2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访摄盐情况2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt第3次随访摄盐情况2.StylePriority.UseBorders = false;
            this.txt第3次随访摄盐情况2.StylePriority.UseFont = false;
            this.txt第3次随访摄盐情况2.StylePriority.UseTextAlignment = false;
            this.txt第3次随访摄盐情况2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel40.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(107.7499F, 1.000029F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel40.StylePriority.UseBorders = false;
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "轻/中/重";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel20,
            this.txt第4次随访摄盐情况1,
            this.xrLabel44,
            this.txt第4次随访摄盐情况2,
            this.xrLabel48});
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Weight = 1.4999999742250185D;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(77.74989F, 0F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel20.StylePriority.UseBorders = false;
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "/";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第4次随访摄盐情况1
            // 
            this.txt第4次随访摄盐情况1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次随访摄盐情况1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访摄盐情况1.LocationFloat = new DevExpress.Utils.PointFloat(2.250076F, 0.999939F);
            this.txt第4次随访摄盐情况1.Name = "txt第4次随访摄盐情况1";
            this.txt第4次随访摄盐情况1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访摄盐情况1.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt第4次随访摄盐情况1.StylePriority.UseBorders = false;
            this.txt第4次随访摄盐情况1.StylePriority.UseFont = false;
            this.txt第4次随访摄盐情况1.StylePriority.UseTextAlignment = false;
            this.txt第4次随访摄盐情况1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel44
            // 
            this.xrLabel44.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel44.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(22.2501F, 0.9999657F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel44.StylePriority.UseBorders = false;
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = "轻/中/重";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次随访摄盐情况2
            // 
            this.txt第4次随访摄盐情况2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次随访摄盐情况2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访摄盐情况2.LocationFloat = new DevExpress.Utils.PointFloat(87.74991F, 0.9999456F);
            this.txt第4次随访摄盐情况2.Name = "txt第4次随访摄盐情况2";
            this.txt第4次随访摄盐情况2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访摄盐情况2.SizeF = new System.Drawing.SizeF(20F, 20F);
            this.txt第4次随访摄盐情况2.StylePriority.UseBorders = false;
            this.txt第4次随访摄盐情况2.StylePriority.UseFont = false;
            this.txt第4次随访摄盐情况2.StylePriority.UseTextAlignment = false;
            this.txt第4次随访摄盐情况2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel48.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(107.7499F, 1.000029F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(55F, 23F);
            this.xrLabel48.StylePriority.UseBorders = false;
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.Text = "轻/中/重";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable24
            // 
            this.xrTable24.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable24.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrTable24.LocationFloat = new DevExpress.Utils.PointFloat(20.00011F, 492.9167F);
            this.xrTable24.Name = "xrTable24";
            this.xrTable24.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24});
            this.xrTable24.SizeF = new System.Drawing.SizeF(426.0001F, 50F);
            this.xrTable24.StylePriority.UseBorders = false;
            this.xrTable24.StylePriority.UseFont = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell56,
            this.xrTableCell57});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 1D;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.StylePriority.UseFont = false;
            this.xrTableCell55.Text = "运动";
            this.xrTableCell55.Weight = 0.96000045776367182D;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel173,
            this.xrLabel174,
            this.xrLabel169,
            this.txt第1次随访运动次数1,
            this.xrLabel159,
            this.txt第1次随访运动时间1,
            this.txt第1次随访运动时间2,
            this.txt第1次随访运动次数2});
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Weight = 1.649999978150547D;
            // 
            // xrLabel173
            // 
            this.xrLabel173.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel173.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel173.LocationFloat = new DevExpress.Utils.PointFloat(32.49987F, 25.5F);
            this.xrLabel173.Name = "xrLabel173";
            this.xrLabel173.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel173.SizeF = new System.Drawing.SizeF(36F, 20F);
            this.xrLabel173.StylePriority.UseBorders = false;
            this.xrLabel173.StylePriority.UseFont = false;
            this.xrLabel173.StylePriority.UseTextAlignment = false;
            this.xrLabel173.Text = "次/周";
            this.xrLabel173.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel174
            // 
            this.xrLabel174.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel174.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel174.LocationFloat = new DevExpress.Utils.PointFloat(113.0001F, 24.49996F);
            this.xrLabel174.Name = "xrLabel174";
            this.xrLabel174.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel174.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrLabel174.StylePriority.UseBorders = false;
            this.xrLabel174.StylePriority.UseFont = false;
            this.xrLabel174.StylePriority.UseTextAlignment = false;
            this.xrLabel174.Text = "分钟/次";
            this.xrLabel174.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel169
            // 
            this.xrLabel169.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel169.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel169.LocationFloat = new DevExpress.Utils.PointFloat(32.49987F, 5.500061F);
            this.xrLabel169.Name = "xrLabel169";
            this.xrLabel169.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel169.SizeF = new System.Drawing.SizeF(36F, 19.99994F);
            this.xrLabel169.StylePriority.UseBorders = false;
            this.xrLabel169.StylePriority.UseFont = false;
            this.xrLabel169.StylePriority.UseTextAlignment = false;
            this.xrLabel169.Text = "次/周";
            this.xrLabel169.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第1次随访运动次数1
            // 
            this.txt第1次随访运动次数1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访运动次数1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访运动次数1.LocationFloat = new DevExpress.Utils.PointFloat(2.500067F, 4.500015F);
            this.txt第1次随访运动次数1.Name = "txt第1次随访运动次数1";
            this.txt第1次随访运动次数1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访运动次数1.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt第1次随访运动次数1.StylePriority.UseBorders = false;
            this.txt第1次随访运动次数1.StylePriority.UseFont = false;
            this.txt第1次随访运动次数1.StylePriority.UseTextAlignment = false;
            this.txt第1次随访运动次数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel159
            // 
            this.xrLabel159.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel159.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel159.LocationFloat = new DevExpress.Utils.PointFloat(113.0001F, 4.500008F);
            this.xrLabel159.Name = "xrLabel159";
            this.xrLabel159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel159.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrLabel159.StylePriority.UseBorders = false;
            this.xrLabel159.StylePriority.UseFont = false;
            this.xrLabel159.StylePriority.UseTextAlignment = false;
            this.xrLabel159.Text = "分钟/次";
            this.xrLabel159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第1次随访运动时间1
            // 
            this.txt第1次随访运动时间1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访运动时间1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访运动时间1.LocationFloat = new DevExpress.Utils.PointFloat(68.49989F, 5.500061F);
            this.txt第1次随访运动时间1.Name = "txt第1次随访运动时间1";
            this.txt第1次随访运动时间1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访运动时间1.SizeF = new System.Drawing.SizeF(43.99997F, 19.99994F);
            this.txt第1次随访运动时间1.StylePriority.UseBorders = false;
            this.txt第1次随访运动时间1.StylePriority.UseFont = false;
            this.txt第1次随访运动时间1.StylePriority.UseTextAlignment = false;
            this.txt第1次随访运动时间1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第1次随访运动时间2
            // 
            this.txt第1次随访运动时间2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访运动时间2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访运动时间2.LocationFloat = new DevExpress.Utils.PointFloat(68.49989F, 25.5F);
            this.txt第1次随访运动时间2.Name = "txt第1次随访运动时间2";
            this.txt第1次随访运动时间2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访运动时间2.SizeF = new System.Drawing.SizeF(43.99998F, 20F);
            this.txt第1次随访运动时间2.StylePriority.UseBorders = false;
            this.txt第1次随访运动时间2.StylePriority.UseFont = false;
            this.txt第1次随访运动时间2.StylePriority.UseTextAlignment = false;
            this.txt第1次随访运动时间2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第1次随访运动次数2
            // 
            this.txt第1次随访运动次数2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第1次随访运动次数2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访运动次数2.LocationFloat = new DevExpress.Utils.PointFloat(2.499851F, 25.49998F);
            this.txt第1次随访运动次数2.Name = "txt第1次随访运动次数2";
            this.txt第1次随访运动次数2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访运动次数2.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt第1次随访运动次数2.StylePriority.UseBorders = false;
            this.txt第1次随访运动次数2.StylePriority.UseFont = false;
            this.txt第1次随访运动次数2.StylePriority.UseTextAlignment = false;
            this.txt第1次随访运动次数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访运动次数2,
            this.xrLabel3,
            this.xrLabel13,
            this.xrLabel14,
            this.txt第2次随访运动次数1,
            this.xrLabel16,
            this.txt第2次随访运动时间1,
            this.txt第2次随访运动时间2});
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访运动次数2
            // 
            this.txt第2次随访运动次数2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次随访运动次数2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访运动次数2.LocationFloat = new DevExpress.Utils.PointFloat(0.000222524F, 24.49996F);
            this.txt第2次随访运动次数2.Name = "txt第2次随访运动次数2";
            this.txt第2次随访运动次数2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访运动次数2.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt第2次随访运动次数2.StylePriority.UseBorders = false;
            this.txt第2次随访运动次数2.StylePriority.UseFont = false;
            this.txt第2次随访运动次数2.StylePriority.UseTextAlignment = false;
            this.txt第2次随访运动次数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel3.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(32.24977F, 25.49999F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(36F, 20F);
            this.xrLabel3.StylePriority.UseBorders = false;
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "次/周";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel13.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(112.75F, 24.49996F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrLabel13.StylePriority.UseBorders = false;
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "分钟/次";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel14.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(32.24977F, 5.500061F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(36F, 19.99994F);
            this.xrLabel14.StylePriority.UseBorders = false;
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "次/周";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次随访运动次数1
            // 
            this.txt第2次随访运动次数1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次随访运动次数1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访运动次数1.LocationFloat = new DevExpress.Utils.PointFloat(2.249969F, 4.500015F);
            this.txt第2次随访运动次数1.Name = "txt第2次随访运动次数1";
            this.txt第2次随访运动次数1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访运动次数1.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt第2次随访运动次数1.StylePriority.UseBorders = false;
            this.txt第2次随访运动次数1.StylePriority.UseFont = false;
            this.txt第2次随访运动次数1.StylePriority.UseTextAlignment = false;
            this.txt第2次随访运动次数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel16.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(112.75F, 4.500008F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrLabel16.StylePriority.UseBorders = false;
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "分钟/次";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次随访运动时间1
            // 
            this.txt第2次随访运动时间1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次随访运动时间1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访运动时间1.LocationFloat = new DevExpress.Utils.PointFloat(68.24977F, 5.500061F);
            this.txt第2次随访运动时间1.Name = "txt第2次随访运动时间1";
            this.txt第2次随访运动时间1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访运动时间1.SizeF = new System.Drawing.SizeF(44F, 19.99994F);
            this.txt第2次随访运动时间1.StylePriority.UseBorders = false;
            this.txt第2次随访运动时间1.StylePriority.UseFont = false;
            this.txt第2次随访运动时间1.StylePriority.UseTextAlignment = false;
            this.txt第2次随访运动时间1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次随访运动时间2
            // 
            this.txt第2次随访运动时间2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第2次随访运动时间2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访运动时间2.LocationFloat = new DevExpress.Utils.PointFloat(68.24977F, 25.5F);
            this.txt第2次随访运动时间2.Name = "txt第2次随访运动时间2";
            this.txt第2次随访运动时间2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访运动时间2.SizeF = new System.Drawing.SizeF(44F, 20F);
            this.txt第2次随访运动时间2.StylePriority.UseBorders = false;
            this.txt第2次随访运动时间2.StylePriority.UseFont = false;
            this.txt第2次随访运动时间2.StylePriority.UseTextAlignment = false;
            this.txt第2次随访运动时间2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable25
            // 
            this.xrTable25.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable25.Font = new System.Drawing.Font("仿宋", 10F);
            this.xrTable25.LocationFloat = new DevExpress.Utils.PointFloat(446.0002F, 492.9167F);
            this.xrTable25.Name = "xrTable25";
            this.xrTable25.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow25});
            this.xrTable25.SizeF = new System.Drawing.SizeF(330F, 50F);
            this.xrTable25.StylePriority.UseBorders = false;
            this.xrTable25.StylePriority.UseFont = false;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.xrTableCell59});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1D;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访运动次数2,
            this.xrLabel21,
            this.xrLabel23,
            this.xrLabel26,
            this.txt第3次随访运动次数1,
            this.xrLabel29,
            this.txt第3次随访运动时间1,
            this.txt第3次随访运动时间2});
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访运动次数2
            // 
            this.txt第3次随访运动次数2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次随访运动次数2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访运动次数2.LocationFloat = new DevExpress.Utils.PointFloat(2.249781F, 25.49998F);
            this.txt第3次随访运动次数2.Name = "txt第3次随访运动次数2";
            this.txt第3次随访运动次数2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访运动次数2.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt第3次随访运动次数2.StylePriority.UseBorders = false;
            this.txt第3次随访运动次数2.StylePriority.UseFont = false;
            this.txt第3次随访运动次数2.StylePriority.UseTextAlignment = false;
            this.txt第3次随访运动次数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel21.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(32.24976F, 25.49999F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(36F, 20F);
            this.xrLabel21.StylePriority.UseBorders = false;
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "次/周";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel23.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(112.75F, 24.49996F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrLabel23.StylePriority.UseBorders = false;
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "分钟/次";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel26.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(32.24976F, 5.500038F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(36F, 20F);
            this.xrLabel26.StylePriority.UseBorders = false;
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "次/周";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次随访运动次数1
            // 
            this.txt第3次随访运动次数1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次随访运动次数1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访运动次数1.LocationFloat = new DevExpress.Utils.PointFloat(2.249954F, 4.500015F);
            this.txt第3次随访运动次数1.Name = "txt第3次随访运动次数1";
            this.txt第3次随访运动次数1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访运动次数1.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt第3次随访运动次数1.StylePriority.UseBorders = false;
            this.txt第3次随访运动次数1.StylePriority.UseFont = false;
            this.txt第3次随访运动次数1.StylePriority.UseTextAlignment = false;
            this.txt第3次随访运动次数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel29.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(112.75F, 4.500008F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrLabel29.StylePriority.UseBorders = false;
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "分钟/次";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次随访运动时间1
            // 
            this.txt第3次随访运动时间1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次随访运动时间1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访运动时间1.LocationFloat = new DevExpress.Utils.PointFloat(68.24985F, 5.500061F);
            this.txt第3次随访运动时间1.Name = "txt第3次随访运动时间1";
            this.txt第3次随访运动时间1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访运动时间1.SizeF = new System.Drawing.SizeF(43.99988F, 19.99994F);
            this.txt第3次随访运动时间1.StylePriority.UseBorders = false;
            this.txt第3次随访运动时间1.StylePriority.UseFont = false;
            this.txt第3次随访运动时间1.StylePriority.UseTextAlignment = false;
            this.txt第3次随访运动时间1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次随访运动时间2
            // 
            this.txt第3次随访运动时间2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第3次随访运动时间2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访运动时间2.LocationFloat = new DevExpress.Utils.PointFloat(68.24991F, 25.5F);
            this.txt第3次随访运动时间2.Name = "txt第3次随访运动时间2";
            this.txt第3次随访运动时间2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访运动时间2.SizeF = new System.Drawing.SizeF(43.99982F, 20F);
            this.txt第3次随访运动时间2.StylePriority.UseBorders = false;
            this.txt第3次随访运动时间2.StylePriority.UseFont = false;
            this.txt第3次随访运动时间2.StylePriority.UseTextAlignment = false;
            this.txt第3次随访运动时间2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访运动次数2,
            this.xrLabel32,
            this.xrLabel33,
            this.xrLabel34,
            this.txt第4次随访运动次数1,
            this.xrLabel36,
            this.txt第4次随访运动时间1,
            this.txt第4次随访运动时间2});
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访运动次数2
            // 
            this.txt第4次随访运动次数2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次随访运动次数2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访运动次数2.LocationFloat = new DevExpress.Utils.PointFloat(2.249718F, 25.50001F);
            this.txt第4次随访运动次数2.Name = "txt第4次随访运动次数2";
            this.txt第4次随访运动次数2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访运动次数2.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt第4次随访运动次数2.StylePriority.UseBorders = false;
            this.txt第4次随访运动次数2.StylePriority.UseFont = false;
            this.txt第4次随访运动次数2.StylePriority.UseTextAlignment = false;
            this.txt第4次随访运动次数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel32.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(32.24976F, 25.49999F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(36F, 20F);
            this.xrLabel32.StylePriority.UseBorders = false;
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "次/周";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel33.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(112.75F, 24.49996F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrLabel33.StylePriority.UseBorders = false;
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "分钟/次";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel34
            // 
            this.xrLabel34.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel34.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(32.24976F, 5.500038F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(36F, 20F);
            this.xrLabel34.StylePriority.UseBorders = false;
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "次/周";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次随访运动次数1
            // 
            this.txt第4次随访运动次数1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次随访运动次数1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访运动次数1.LocationFloat = new DevExpress.Utils.PointFloat(2.249954F, 4.500015F);
            this.txt第4次随访运动次数1.Name = "txt第4次随访运动次数1";
            this.txt第4次随访运动次数1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访运动次数1.SizeF = new System.Drawing.SizeF(30F, 20F);
            this.txt第4次随访运动次数1.StylePriority.UseBorders = false;
            this.txt第4次随访运动次数1.StylePriority.UseFont = false;
            this.txt第4次随访运动次数1.StylePriority.UseTextAlignment = false;
            this.txt第4次随访运动次数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel36.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold);
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(112.75F, 4.500008F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(50F, 20F);
            this.xrLabel36.StylePriority.UseBorders = false;
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "分钟/次";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第4次随访运动时间1
            // 
            this.txt第4次随访运动时间1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次随访运动时间1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访运动时间1.LocationFloat = new DevExpress.Utils.PointFloat(68.24979F, 5.500061F);
            this.txt第4次随访运动时间1.Name = "txt第4次随访运动时间1";
            this.txt第4次随访运动时间1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访运动时间1.SizeF = new System.Drawing.SizeF(44F, 19.99994F);
            this.txt第4次随访运动时间1.StylePriority.UseBorders = false;
            this.txt第4次随访运动时间1.StylePriority.UseFont = false;
            this.txt第4次随访运动时间1.StylePriority.UseTextAlignment = false;
            this.txt第4次随访运动时间1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次随访运动时间2
            // 
            this.txt第4次随访运动时间2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.txt第4次随访运动时间2.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访运动时间2.LocationFloat = new DevExpress.Utils.PointFloat(68.24979F, 25.5F);
            this.txt第4次随访运动时间2.Name = "txt第4次随访运动时间2";
            this.txt第4次随访运动时间2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访运动时间2.SizeF = new System.Drawing.SizeF(43.99994F, 20F);
            this.txt第4次随访运动时间2.StylePriority.UseBorders = false;
            this.txt第4次随访运动时间2.StylePriority.UseFont = false;
            this.txt第4次随访运动时间2.StylePriority.UseTextAlignment = false;
            this.txt第4次随访运动时间2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable22
            // 
            this.xrTable22.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable22.LocationFloat = new DevExpress.Utils.PointFloat(19.99989F, 467.9167F);
            this.xrTable22.Name = "xrTable22";
            this.xrTable22.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow22});
            this.xrTable22.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable22.StylePriority.UseBorders = false;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.xrTableCell51,
            this.xrTableCell52});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 1D;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "日饮酒量(两)";
            this.xrTableCell50.Weight = 0.96000045776367182D;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访日饮酒量2,
            this.txt第1次随访日饮酒量1,
            this.xrLabel147});
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 1.649999978150547D;
            // 
            // txt第1次随访日饮酒量2
            // 
            this.txt第1次随访日饮酒量2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访日饮酒量2.LocationFloat = new DevExpress.Utils.PointFloat(84.99979F, 0F);
            this.txt第1次随访日饮酒量2.Name = "txt第1次随访日饮酒量2";
            this.txt第1次随访日饮酒量2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访日饮酒量2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第1次随访日饮酒量2.StylePriority.UseBorders = false;
            this.txt第1次随访日饮酒量2.StylePriority.UseTextAlignment = false;
            this.txt第1次随访日饮酒量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第1次随访日饮酒量1
            // 
            this.txt第1次随访日饮酒量1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访日饮酒量1.LocationFloat = new DevExpress.Utils.PointFloat(4.999765F, 0F);
            this.txt第1次随访日饮酒量1.Name = "txt第1次随访日饮酒量1";
            this.txt第1次随访日饮酒量1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访日饮酒量1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第1次随访日饮酒量1.StylePriority.UseBorders = false;
            this.txt第1次随访日饮酒量1.StylePriority.UseTextAlignment = false;
            this.txt第1次随访日饮酒量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel147
            // 
            this.xrLabel147.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel147.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel147.LocationFloat = new DevExpress.Utils.PointFloat(74.99978F, 0F);
            this.xrLabel147.Name = "xrLabel147";
            this.xrLabel147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel147.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel147.StylePriority.UseBorders = false;
            this.xrLabel147.StylePriority.UseFont = false;
            this.xrLabel147.StylePriority.UseTextAlignment = false;
            this.xrLabel147.Text = "/";
            this.xrLabel147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访日饮酒量2,
            this.txt第2次随访日饮酒量1,
            this.xrLabel150});
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访日饮酒量2
            // 
            this.txt第2次随访日饮酒量2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访日饮酒量2.LocationFloat = new DevExpress.Utils.PointFloat(87.50002F, 1F);
            this.txt第2次随访日饮酒量2.Name = "txt第2次随访日饮酒量2";
            this.txt第2次随访日饮酒量2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访日饮酒量2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第2次随访日饮酒量2.StylePriority.UseBorders = false;
            this.txt第2次随访日饮酒量2.StylePriority.UseTextAlignment = false;
            this.txt第2次随访日饮酒量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次随访日饮酒量1
            // 
            this.txt第2次随访日饮酒量1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访日饮酒量1.LocationFloat = new DevExpress.Utils.PointFloat(7.499992F, 1F);
            this.txt第2次随访日饮酒量1.Name = "txt第2次随访日饮酒量1";
            this.txt第2次随访日饮酒量1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访日饮酒量1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第2次随访日饮酒量1.StylePriority.UseBorders = false;
            this.txt第2次随访日饮酒量1.StylePriority.UseTextAlignment = false;
            this.txt第2次随访日饮酒量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel150
            // 
            this.xrLabel150.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel150.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel150.LocationFloat = new DevExpress.Utils.PointFloat(77.50001F, 1F);
            this.xrLabel150.Name = "xrLabel150";
            this.xrLabel150.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel150.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel150.StylePriority.UseBorders = false;
            this.xrLabel150.StylePriority.UseFont = false;
            this.xrLabel150.StylePriority.UseTextAlignment = false;
            this.xrLabel150.Text = "/";
            this.xrLabel150.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable23
            // 
            this.xrTable23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable23.LocationFloat = new DevExpress.Utils.PointFloat(445.9999F, 467.9167F);
            this.xrTable23.Name = "xrTable23";
            this.xrTable23.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable23.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable23.StylePriority.UseBorders = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell53,
            this.xrTableCell54});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1D;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访日饮酒量2,
            this.txt第3次随访日饮酒量1,
            this.xrLabel153});
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访日饮酒量2
            // 
            this.txt第3次随访日饮酒量2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访日饮酒量2.LocationFloat = new DevExpress.Utils.PointFloat(87.50002F, 1F);
            this.txt第3次随访日饮酒量2.Name = "txt第3次随访日饮酒量2";
            this.txt第3次随访日饮酒量2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访日饮酒量2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第3次随访日饮酒量2.StylePriority.UseBorders = false;
            this.txt第3次随访日饮酒量2.StylePriority.UseTextAlignment = false;
            this.txt第3次随访日饮酒量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次随访日饮酒量1
            // 
            this.txt第3次随访日饮酒量1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访日饮酒量1.LocationFloat = new DevExpress.Utils.PointFloat(7.499992F, 1F);
            this.txt第3次随访日饮酒量1.Name = "txt第3次随访日饮酒量1";
            this.txt第3次随访日饮酒量1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访日饮酒量1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第3次随访日饮酒量1.StylePriority.UseBorders = false;
            this.txt第3次随访日饮酒量1.StylePriority.UseTextAlignment = false;
            this.txt第3次随访日饮酒量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel153
            // 
            this.xrLabel153.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel153.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel153.LocationFloat = new DevExpress.Utils.PointFloat(77.50001F, 1F);
            this.xrLabel153.Name = "xrLabel153";
            this.xrLabel153.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel153.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel153.StylePriority.UseBorders = false;
            this.xrLabel153.StylePriority.UseFont = false;
            this.xrLabel153.StylePriority.UseTextAlignment = false;
            this.xrLabel153.Text = "/";
            this.xrLabel153.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访日饮酒量2,
            this.txt第4次随访日饮酒量1,
            this.xrLabel156});
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访日饮酒量2
            // 
            this.txt第4次随访日饮酒量2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访日饮酒量2.LocationFloat = new DevExpress.Utils.PointFloat(87.50002F, 1F);
            this.txt第4次随访日饮酒量2.Name = "txt第4次随访日饮酒量2";
            this.txt第4次随访日饮酒量2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访日饮酒量2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第4次随访日饮酒量2.StylePriority.UseBorders = false;
            this.txt第4次随访日饮酒量2.StylePriority.UseTextAlignment = false;
            this.txt第4次随访日饮酒量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次随访日饮酒量1
            // 
            this.txt第4次随访日饮酒量1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访日饮酒量1.LocationFloat = new DevExpress.Utils.PointFloat(7.499992F, 1F);
            this.txt第4次随访日饮酒量1.Name = "txt第4次随访日饮酒量1";
            this.txt第4次随访日饮酒量1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访日饮酒量1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第4次随访日饮酒量1.StylePriority.UseBorders = false;
            this.txt第4次随访日饮酒量1.StylePriority.UseTextAlignment = false;
            this.txt第4次随访日饮酒量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel156
            // 
            this.xrLabel156.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel156.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel156.LocationFloat = new DevExpress.Utils.PointFloat(77.50001F, 1F);
            this.xrLabel156.Name = "xrLabel156";
            this.xrLabel156.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel156.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel156.StylePriority.UseBorders = false;
            this.xrLabel156.StylePriority.UseFont = false;
            this.xrLabel156.StylePriority.UseTextAlignment = false;
            this.xrLabel156.Text = "/";
            this.xrLabel156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable20
            // 
            this.xrTable20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable20.LocationFloat = new DevExpress.Utils.PointFloat(20.00008F, 442.9167F);
            this.xrTable20.Name = "xrTable20";
            this.xrTable20.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow20});
            this.xrTable20.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable20.StylePriority.UseBorders = false;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 1D;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Text = "日吸烟量(支)";
            this.xrTableCell45.Weight = 0.96000045776367182D;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访日吸烟量2,
            this.txt第1次随访日吸烟量1,
            this.xrLabel135});
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Weight = 1.649999978150547D;
            // 
            // txt第1次随访日吸烟量2
            // 
            this.txt第1次随访日吸烟量2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访日吸烟量2.LocationFloat = new DevExpress.Utils.PointFloat(84.99979F, 0F);
            this.txt第1次随访日吸烟量2.Name = "txt第1次随访日吸烟量2";
            this.txt第1次随访日吸烟量2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访日吸烟量2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第1次随访日吸烟量2.StylePriority.UseBorders = false;
            this.txt第1次随访日吸烟量2.StylePriority.UseTextAlignment = false;
            this.txt第1次随访日吸烟量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第1次随访日吸烟量1
            // 
            this.txt第1次随访日吸烟量1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访日吸烟量1.LocationFloat = new DevExpress.Utils.PointFloat(4.999765F, 0F);
            this.txt第1次随访日吸烟量1.Name = "txt第1次随访日吸烟量1";
            this.txt第1次随访日吸烟量1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访日吸烟量1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第1次随访日吸烟量1.StylePriority.UseBorders = false;
            this.txt第1次随访日吸烟量1.StylePriority.UseTextAlignment = false;
            this.txt第1次随访日吸烟量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel135
            // 
            this.xrLabel135.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel135.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel135.LocationFloat = new DevExpress.Utils.PointFloat(74.99978F, 0F);
            this.xrLabel135.Name = "xrLabel135";
            this.xrLabel135.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel135.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel135.StylePriority.UseBorders = false;
            this.xrLabel135.StylePriority.UseFont = false;
            this.xrLabel135.StylePriority.UseTextAlignment = false;
            this.xrLabel135.Text = "/";
            this.xrLabel135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访日吸烟量2,
            this.txt第2次随访日吸烟量1,
            this.xrLabel138});
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访日吸烟量2
            // 
            this.txt第2次随访日吸烟量2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访日吸烟量2.LocationFloat = new DevExpress.Utils.PointFloat(87.50002F, 1F);
            this.txt第2次随访日吸烟量2.Name = "txt第2次随访日吸烟量2";
            this.txt第2次随访日吸烟量2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访日吸烟量2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第2次随访日吸烟量2.StylePriority.UseBorders = false;
            this.txt第2次随访日吸烟量2.StylePriority.UseTextAlignment = false;
            this.txt第2次随访日吸烟量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次随访日吸烟量1
            // 
            this.txt第2次随访日吸烟量1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访日吸烟量1.LocationFloat = new DevExpress.Utils.PointFloat(7.499992F, 1F);
            this.txt第2次随访日吸烟量1.Name = "txt第2次随访日吸烟量1";
            this.txt第2次随访日吸烟量1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访日吸烟量1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第2次随访日吸烟量1.StylePriority.UseBorders = false;
            this.txt第2次随访日吸烟量1.StylePriority.UseTextAlignment = false;
            this.txt第2次随访日吸烟量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel138
            // 
            this.xrLabel138.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel138.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel138.LocationFloat = new DevExpress.Utils.PointFloat(77.50001F, 1F);
            this.xrLabel138.Name = "xrLabel138";
            this.xrLabel138.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel138.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel138.StylePriority.UseBorders = false;
            this.xrLabel138.StylePriority.UseFont = false;
            this.xrLabel138.StylePriority.UseTextAlignment = false;
            this.xrLabel138.Text = "/";
            this.xrLabel138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable21
            // 
            this.xrTable21.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable21.LocationFloat = new DevExpress.Utils.PointFloat(446.0001F, 442.9167F);
            this.xrTable21.Name = "xrTable21";
            this.xrTable21.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow21});
            this.xrTable21.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable21.StylePriority.UseBorders = false;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell49});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1D;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访日吸烟量2,
            this.txt第3次随访日吸烟量1,
            this.xrLabel141});
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访日吸烟量2
            // 
            this.txt第3次随访日吸烟量2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访日吸烟量2.LocationFloat = new DevExpress.Utils.PointFloat(87.50002F, 1F);
            this.txt第3次随访日吸烟量2.Name = "txt第3次随访日吸烟量2";
            this.txt第3次随访日吸烟量2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访日吸烟量2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第3次随访日吸烟量2.StylePriority.UseBorders = false;
            this.txt第3次随访日吸烟量2.StylePriority.UseTextAlignment = false;
            this.txt第3次随访日吸烟量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次随访日吸烟量1
            // 
            this.txt第3次随访日吸烟量1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访日吸烟量1.LocationFloat = new DevExpress.Utils.PointFloat(7.499992F, 1F);
            this.txt第3次随访日吸烟量1.Name = "txt第3次随访日吸烟量1";
            this.txt第3次随访日吸烟量1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访日吸烟量1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第3次随访日吸烟量1.StylePriority.UseBorders = false;
            this.txt第3次随访日吸烟量1.StylePriority.UseTextAlignment = false;
            this.txt第3次随访日吸烟量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel141
            // 
            this.xrLabel141.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel141.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel141.LocationFloat = new DevExpress.Utils.PointFloat(77.50001F, 1F);
            this.xrLabel141.Name = "xrLabel141";
            this.xrLabel141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel141.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel141.StylePriority.UseBorders = false;
            this.xrLabel141.StylePriority.UseFont = false;
            this.xrLabel141.StylePriority.UseTextAlignment = false;
            this.xrLabel141.Text = "/";
            this.xrLabel141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访日吸烟量2,
            this.txt第4次随访日吸烟量1,
            this.xrLabel144});
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访日吸烟量2
            // 
            this.txt第4次随访日吸烟量2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访日吸烟量2.LocationFloat = new DevExpress.Utils.PointFloat(87.50002F, 1F);
            this.txt第4次随访日吸烟量2.Name = "txt第4次随访日吸烟量2";
            this.txt第4次随访日吸烟量2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访日吸烟量2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第4次随访日吸烟量2.StylePriority.UseBorders = false;
            this.txt第4次随访日吸烟量2.StylePriority.UseTextAlignment = false;
            this.txt第4次随访日吸烟量2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次随访日吸烟量1
            // 
            this.txt第4次随访日吸烟量1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访日吸烟量1.LocationFloat = new DevExpress.Utils.PointFloat(7.499992F, 1F);
            this.txt第4次随访日吸烟量1.Name = "txt第4次随访日吸烟量1";
            this.txt第4次随访日吸烟量1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访日吸烟量1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第4次随访日吸烟量1.StylePriority.UseBorders = false;
            this.txt第4次随访日吸烟量1.StylePriority.UseTextAlignment = false;
            this.txt第4次随访日吸烟量1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel144
            // 
            this.xrLabel144.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel144.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel144.LocationFloat = new DevExpress.Utils.PointFloat(77.50001F, 1F);
            this.xrLabel144.Name = "xrLabel144";
            this.xrLabel144.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel144.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel144.StylePriority.UseBorders = false;
            this.xrLabel144.StylePriority.UseFont = false;
            this.xrLabel144.StylePriority.UseTextAlignment = false;
            this.xrLabel144.Text = "/";
            this.xrLabel144.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel132
            // 
            this.xrLabel132.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel132.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 442.9167F);
            this.xrLabel132.Name = "xrLabel132";
            this.xrLabel132.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel132.SizeF = new System.Drawing.SizeF(20F, 174.9999F);
            this.xrLabel132.StylePriority.UseBorders = false;
            this.xrLabel132.StylePriority.UseTextAlignment = false;
            this.xrLabel132.Text = "生活方式指导";
            this.xrLabel132.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable18
            // 
            this.xrTable18.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable18.LocationFloat = new DevExpress.Utils.PointFloat(446.0002F, 417.9167F);
            this.xrTable18.Name = "xrTable18";
            this.xrTable18.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow18});
            this.xrTable18.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable18.StylePriority.UseBorders = false;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell40,
            this.xrTableCell41});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1D;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访体征其他});
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访体征其他
            // 
            this.txt第3次随访体征其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访体征其他.LocationFloat = new DevExpress.Utils.PointFloat(7.499949F, 0F);
            this.txt第3次随访体征其他.Name = "txt第3次随访体征其他";
            this.txt第3次随访体征其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访体征其他.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.txt第3次随访体征其他.StylePriority.UseBorders = false;
            this.txt第3次随访体征其他.StylePriority.UseTextAlignment = false;
            this.txt第3次随访体征其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访体征其他});
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访体征其他
            // 
            this.txt第4次随访体征其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访体征其他.LocationFloat = new DevExpress.Utils.PointFloat(7.499886F, 0F);
            this.txt第4次随访体征其他.Name = "txt第4次随访体征其他";
            this.txt第4次随访体征其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访体征其他.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.txt第4次随访体征其他.StylePriority.UseBorders = false;
            this.txt第4次随访体征其他.StylePriority.UseTextAlignment = false;
            this.txt第4次随访体征其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable19
            // 
            this.xrTable19.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable19.LocationFloat = new DevExpress.Utils.PointFloat(20.00008F, 417.9167F);
            this.xrTable19.Name = "xrTable19";
            this.xrTable19.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19});
            this.xrTable19.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable19.StylePriority.UseBorders = false;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell42,
            this.xrTableCell43,
            this.xrTableCell44});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 1D;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "其他";
            this.xrTableCell42.Weight = 0.96000045776367182D;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访体征其他});
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Weight = 1.649999978150547D;
            // 
            // txt第1次随访体征其他
            // 
            this.txt第1次随访体征其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访体征其他.LocationFloat = new DevExpress.Utils.PointFloat(4.749918F, 0F);
            this.txt第1次随访体征其他.Name = "txt第1次随访体征其他";
            this.txt第1次随访体征其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访体征其他.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.txt第1次随访体征其他.StylePriority.UseBorders = false;
            this.txt第1次随访体征其他.StylePriority.UseTextAlignment = false;
            this.txt第1次随访体征其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访体征其他});
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访体征其他
            // 
            this.txt第2次随访体征其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访体征其他.LocationFloat = new DevExpress.Utils.PointFloat(7.499949F, 1.99995F);
            this.txt第2次随访体征其他.Name = "txt第2次随访体征其他";
            this.txt第2次随访体征其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访体征其他.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.txt第2次随访体征其他.StylePriority.UseBorders = false;
            this.txt第2次随访体征其他.StylePriority.UseTextAlignment = false;
            this.txt第2次随访体征其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable16
            // 
            this.xrTable16.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(446.0002F, 392.9167F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow16});
            this.xrTable16.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable16.StylePriority.UseBorders = false;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell35,
            this.xrTableCell36});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1D;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访心率});
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访心率
            // 
            this.txt第3次随访心率.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访心率.LocationFloat = new DevExpress.Utils.PointFloat(7.499949F, 0F);
            this.txt第3次随访心率.Name = "txt第3次随访心率";
            this.txt第3次随访心率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访心率.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.txt第3次随访心率.StylePriority.UseBorders = false;
            this.txt第3次随访心率.StylePriority.UseTextAlignment = false;
            this.txt第3次随访心率.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访心率});
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访心率
            // 
            this.txt第4次随访心率.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访心率.LocationFloat = new DevExpress.Utils.PointFloat(7.499886F, 0F);
            this.txt第4次随访心率.Name = "txt第4次随访心率";
            this.txt第4次随访心率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访心率.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.txt第4次随访心率.StylePriority.UseBorders = false;
            this.txt第4次随访心率.StylePriority.UseTextAlignment = false;
            this.txt第4次随访心率.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable17
            // 
            this.xrTable17.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(20.00008F, 392.9167F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow17});
            this.xrTable17.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable17.StylePriority.UseBorders = false;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1D;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Text = "心率";
            this.xrTableCell37.Weight = 0.96000045776367182D;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访心率});
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Weight = 1.649999978150547D;
            // 
            // txt第1次随访心率
            // 
            this.txt第1次随访心率.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访心率.LocationFloat = new DevExpress.Utils.PointFloat(4.999733F, 1.999982F);
            this.txt第1次随访心率.Name = "txt第1次随访心率";
            this.txt第1次随访心率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访心率.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.txt第1次随访心率.StylePriority.UseBorders = false;
            this.txt第1次随访心率.StylePriority.UseTextAlignment = false;
            this.txt第1次随访心率.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访心率});
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访心率
            // 
            this.txt第2次随访心率.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访心率.LocationFloat = new DevExpress.Utils.PointFloat(7.499949F, 1.99995F);
            this.txt第2次随访心率.Name = "txt第2次随访心率";
            this.txt第2次随访心率.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访心率.SizeF = new System.Drawing.SizeF(130F, 23F);
            this.txt第2次随访心率.StylePriority.UseBorders = false;
            this.txt第2次随访心率.StylePriority.UseTextAlignment = false;
            this.txt第2次随访心率.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable14
            // 
            this.xrTable14.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(446.0001F, 367.9167F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow14});
            this.xrTable14.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable14.StylePriority.UseBorders = false;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.xrTableCell31});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1D;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访体质指数2,
            this.xrLabel56,
            this.txt第3次随访体质指数1});
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访体质指数2
            // 
            this.txt第3次随访体质指数2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访体质指数2.LocationFloat = new DevExpress.Utils.PointFloat(88.17838F, 1.999919F);
            this.txt第3次随访体质指数2.Name = "txt第3次随访体质指数2";
            this.txt第3次随访体质指数2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访体质指数2.SizeF = new System.Drawing.SizeF(69.99985F, 23F);
            this.txt第3次随访体质指数2.StylePriority.UseBorders = false;
            this.txt第3次随访体质指数2.StylePriority.UseTextAlignment = false;
            this.txt第3次随访体质指数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel56
            // 
            this.xrLabel56.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel56.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel56.LocationFloat = new DevExpress.Utils.PointFloat(77.89205F, 1.999919F);
            this.xrLabel56.Name = "xrLabel56";
            this.xrLabel56.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel56.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel56.StylePriority.UseBorders = false;
            this.xrLabel56.StylePriority.UseFont = false;
            this.xrLabel56.StylePriority.UseTextAlignment = false;
            this.xrLabel56.Text = "/";
            this.xrLabel56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次随访体质指数1
            // 
            this.txt第3次随访体质指数1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访体质指数1.LocationFloat = new DevExpress.Utils.PointFloat(7.499908F, 0F);
            this.txt第3次随访体质指数1.Name = "txt第3次随访体质指数1";
            this.txt第3次随访体质指数1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访体质指数1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第3次随访体质指数1.StylePriority.UseBorders = false;
            this.txt第3次随访体质指数1.StylePriority.UseTextAlignment = false;
            this.txt第3次随访体质指数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访体质指数2,
            this.xrLabel60,
            this.txt第4次随访体质指数1});
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访体质指数2
            // 
            this.txt第4次随访体质指数2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访体质指数2.LocationFloat = new DevExpress.Utils.PointFloat(88.1787F, 1.999887F);
            this.txt第4次随访体质指数2.Name = "txt第4次随访体质指数2";
            this.txt第4次随访体质指数2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访体质指数2.SizeF = new System.Drawing.SizeF(69.99985F, 23F);
            this.txt第4次随访体质指数2.StylePriority.UseBorders = false;
            this.txt第4次随访体质指数2.StylePriority.UseTextAlignment = false;
            this.txt第4次随访体质指数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel60.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(77.89236F, 1.999887F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel60.StylePriority.UseBorders = false;
            this.xrLabel60.StylePriority.UseFont = false;
            this.xrLabel60.StylePriority.UseTextAlignment = false;
            this.xrLabel60.Text = "/";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第4次随访体质指数1
            // 
            this.txt第4次随访体质指数1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访体质指数1.LocationFloat = new DevExpress.Utils.PointFloat(7.499908F, 0F);
            this.txt第4次随访体质指数1.Name = "txt第4次随访体质指数1";
            this.txt第4次随访体质指数1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访体质指数1.SizeF = new System.Drawing.SizeF(69.75012F, 23F);
            this.txt第4次随访体质指数1.StylePriority.UseBorders = false;
            this.txt第4次随访体质指数1.StylePriority.UseTextAlignment = false;
            this.txt第4次随访体质指数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable15
            // 
            this.xrTable15.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(20.00004F, 367.9167F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow15});
            this.xrTable15.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable15.StylePriority.UseBorders = false;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell34});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1D;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Text = "体质指数";
            this.xrTableCell32.Weight = 0.96000045776367182D;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访体质指数2,
            this.xrLabel42,
            this.txt第1次随访体质指数1});
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Weight = 1.649999978150547D;
            // 
            // txt第1次随访体质指数2
            // 
            this.txt第1次随访体质指数2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访体质指数2.LocationFloat = new DevExpress.Utils.PointFloat(85.28598F, 2.000014F);
            this.txt第1次随访体质指数2.Name = "txt第1次随访体质指数2";
            this.txt第1次随访体质指数2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访体质指数2.SizeF = new System.Drawing.SizeF(69.99985F, 23F);
            this.txt第1次随访体质指数2.StylePriority.UseBorders = false;
            this.txt第1次随访体质指数2.StylePriority.UseTextAlignment = false;
            this.txt第1次随访体质指数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(74.99962F, 2.000014F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel42.StylePriority.UseBorders = false;
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.StylePriority.UseTextAlignment = false;
            this.xrLabel42.Text = "/";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第1次随访体质指数1
            // 
            this.txt第1次随访体质指数1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访体质指数1.LocationFloat = new DevExpress.Utils.PointFloat(4.999723F, 1.999969F);
            this.txt第1次随访体质指数1.Name = "txt第1次随访体质指数1";
            this.txt第1次随访体质指数1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访体质指数1.SizeF = new System.Drawing.SizeF(69.99982F, 23F);
            this.txt第1次随访体质指数1.StylePriority.UseBorders = false;
            this.txt第1次随访体质指数1.StylePriority.UseTextAlignment = false;
            this.txt第1次随访体质指数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访体质指数2,
            this.xrLabel52,
            this.txt第2次随访体质指数1});
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访体质指数2
            // 
            this.txt第2次随访体质指数2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访体质指数2.LocationFloat = new DevExpress.Utils.PointFloat(90.24995F, 2.000014F);
            this.txt第2次随访体质指数2.Name = "txt第2次随访体质指数2";
            this.txt第2次随访体质指数2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访体质指数2.SizeF = new System.Drawing.SizeF(69.99985F, 23F);
            this.txt第2次随访体质指数2.StylePriority.UseBorders = false;
            this.txt第2次随访体质指数2.StylePriority.UseTextAlignment = false;
            this.txt第2次随访体质指数2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel52.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(79.96359F, 2.000014F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel52.StylePriority.UseBorders = false;
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "/";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次随访体质指数1
            // 
            this.txt第2次随访体质指数1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访体质指数1.LocationFloat = new DevExpress.Utils.PointFloat(7.499929F, 1.999939F);
            this.txt第2次随访体质指数1.Name = "txt第2次随访体质指数1";
            this.txt第2次随访体质指数1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访体质指数1.SizeF = new System.Drawing.SizeF(72.21457F, 23F);
            this.txt第2次随访体质指数1.StylePriority.UseBorders = false;
            this.txt第2次随访体质指数1.StylePriority.UseTextAlignment = false;
            this.txt第2次随访体质指数1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable12
            // 
            this.xrTable12.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(20.00004F, 342.9167F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow12});
            this.xrTable12.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable12.StylePriority.UseBorders = false;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1D;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Text = "体重(kg)";
            this.xrTableCell25.Weight = 0.96000045776367182D;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访体重2,
            this.xrLabel30,
            this.txt第1次随访体重1});
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Weight = 1.649999978150547D;
            // 
            // txt第1次随访体重2
            // 
            this.txt第1次随访体重2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访体重2.LocationFloat = new DevExpress.Utils.PointFloat(85.28595F, 0F);
            this.txt第1次随访体重2.Name = "txt第1次随访体重2";
            this.txt第1次随访体重2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访体重2.SizeF = new System.Drawing.SizeF(69.99985F, 23F);
            this.txt第1次随访体重2.StylePriority.UseBorders = false;
            this.txt第1次随访体重2.StylePriority.UseTextAlignment = false;
            this.txt第1次随访体重2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(74.99959F, 0F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel30.StylePriority.UseBorders = false;
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "/";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第1次随访体重1
            // 
            this.txt第1次随访体重1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访体重1.LocationFloat = new DevExpress.Utils.PointFloat(4.999723F, 1.999969F);
            this.txt第1次随访体重1.Name = "txt第1次随访体重1";
            this.txt第1次随访体重1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访体重1.SizeF = new System.Drawing.SizeF(69.99985F, 23F);
            this.txt第1次随访体重1.StylePriority.UseBorders = false;
            this.txt第1次随访体重1.StylePriority.UseTextAlignment = false;
            this.txt第1次随访体重1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访体重2,
            this.xrLabel39,
            this.txt第2次随访体重1});
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访体重2
            // 
            this.txt第2次随访体重2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访体重2.LocationFloat = new DevExpress.Utils.PointFloat(88.1787F, 0F);
            this.txt第2次随访体重2.Name = "txt第2次随访体重2";
            this.txt第2次随访体重2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访体重2.SizeF = new System.Drawing.SizeF(69.99985F, 23F);
            this.txt第2次随访体重2.StylePriority.UseBorders = false;
            this.txt第2次随访体重2.StylePriority.UseTextAlignment = false;
            this.txt第2次随访体重2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(77.89233F, 0F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel39.StylePriority.UseBorders = false;
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "/";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次随访体重1
            // 
            this.txt第2次随访体重1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访体重1.LocationFloat = new DevExpress.Utils.PointFloat(7.499929F, 1.999939F);
            this.txt第2次随访体重1.Name = "txt第2次随访体重1";
            this.txt第2次随访体重1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访体重1.SizeF = new System.Drawing.SizeF(70.00012F, 23F);
            this.txt第2次随访体重1.StylePriority.UseBorders = false;
            this.txt第2次随访体重1.StylePriority.UseTextAlignment = false;
            this.txt第2次随访体重1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable13
            // 
            this.xrTable13.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(446.0001F, 342.9167F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13});
            this.xrTable13.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable13.StylePriority.UseBorders = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell28,
            this.xrTableCell29});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1D;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访体重2,
            this.xrLabel46,
            this.txt第3次随访体重1});
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访体重2
            // 
            this.txt第3次随访体重2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访体重2.LocationFloat = new DevExpress.Utils.PointFloat(89.62492F, 1.999919F);
            this.txt第3次随访体重2.Name = "txt第3次随访体重2";
            this.txt第3次随访体重2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访体重2.SizeF = new System.Drawing.SizeF(69.99985F, 23F);
            this.txt第3次随访体重2.StylePriority.UseBorders = false;
            this.txt第3次随访体重2.StylePriority.UseTextAlignment = false;
            this.txt第3次随访体重2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(79.33858F, 1.999919F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel46.StylePriority.UseBorders = false;
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "/";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次随访体重1
            // 
            this.txt第3次随访体重1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访体重1.LocationFloat = new DevExpress.Utils.PointFloat(7.499908F, 0F);
            this.txt第3次随访体重1.Name = "txt第3次随访体重1";
            this.txt第3次随访体重1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访体重1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第3次随访体重1.StylePriority.UseBorders = false;
            this.txt第3次随访体重1.StylePriority.UseTextAlignment = false;
            this.txt第3次随访体重1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访体重2,
            this.xrLabel51,
            this.txt第4次随访体重1});
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访体重2
            // 
            this.txt第4次随访体重2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访体重2.LocationFloat = new DevExpress.Utils.PointFloat(88.1787F, 1.999887F);
            this.txt第4次随访体重2.Name = "txt第4次随访体重2";
            this.txt第4次随访体重2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访体重2.SizeF = new System.Drawing.SizeF(69.99985F, 23F);
            this.txt第4次随访体重2.StylePriority.UseBorders = false;
            this.txt第4次随访体重2.StylePriority.UseTextAlignment = false;
            this.txt第4次随访体重2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(77.89236F, 1.999887F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel51.StylePriority.UseBorders = false;
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "/";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第4次随访体重1
            // 
            this.txt第4次随访体重1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访体重1.LocationFloat = new DevExpress.Utils.PointFloat(7.499908F, 0F);
            this.txt第4次随访体重1.Name = "txt第4次随访体重1";
            this.txt第4次随访体重1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访体重1.SizeF = new System.Drawing.SizeF(69.75012F, 23F);
            this.txt第4次随访体重1.StylePriority.UseBorders = false;
            this.txt第4次随访体重1.StylePriority.UseTextAlignment = false;
            this.txt第4次随访体重1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable11
            // 
            this.xrTable11.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(446F, 317.9167F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow11});
            this.xrTable11.SizeF = new System.Drawing.SizeF(330F, 25F);
            this.xrTable11.StylePriority.UseBorders = false;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.xrTableCell24});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1D;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访血压2,
            this.txt第3次随访血压1,
            this.xrLabel113});
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访血压2
            // 
            this.txt第3次随访血压2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访血压2.LocationFloat = new DevExpress.Utils.PointFloat(87.50002F, 1F);
            this.txt第3次随访血压2.Name = "txt第3次随访血压2";
            this.txt第3次随访血压2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访血压2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第3次随访血压2.StylePriority.UseBorders = false;
            this.txt第3次随访血压2.StylePriority.UseTextAlignment = false;
            this.txt第3次随访血压2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第3次随访血压1
            // 
            this.txt第3次随访血压1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访血压1.LocationFloat = new DevExpress.Utils.PointFloat(7.499992F, 1F);
            this.txt第3次随访血压1.Name = "txt第3次随访血压1";
            this.txt第3次随访血压1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访血压1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第3次随访血压1.StylePriority.UseBorders = false;
            this.txt第3次随访血压1.StylePriority.UseTextAlignment = false;
            this.txt第3次随访血压1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel113
            // 
            this.xrLabel113.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel113.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel113.LocationFloat = new DevExpress.Utils.PointFloat(77.50001F, 1F);
            this.xrLabel113.Name = "xrLabel113";
            this.xrLabel113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel113.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel113.StylePriority.UseBorders = false;
            this.xrLabel113.StylePriority.UseFont = false;
            this.xrLabel113.StylePriority.UseTextAlignment = false;
            this.xrLabel113.Text = "/";
            this.xrLabel113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访血压2,
            this.txt第4次随访血压1,
            this.xrLabel116});
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访血压2
            // 
            this.txt第4次随访血压2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访血压2.LocationFloat = new DevExpress.Utils.PointFloat(87.50002F, 1F);
            this.txt第4次随访血压2.Name = "txt第4次随访血压2";
            this.txt第4次随访血压2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访血压2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第4次随访血压2.StylePriority.UseBorders = false;
            this.txt第4次随访血压2.StylePriority.UseTextAlignment = false;
            this.txt第4次随访血压2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第4次随访血压1
            // 
            this.txt第4次随访血压1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访血压1.LocationFloat = new DevExpress.Utils.PointFloat(7.499992F, 1F);
            this.txt第4次随访血压1.Name = "txt第4次随访血压1";
            this.txt第4次随访血压1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访血压1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第4次随访血压1.StylePriority.UseBorders = false;
            this.txt第4次随访血压1.StylePriority.UseTextAlignment = false;
            this.txt第4次随访血压1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel116
            // 
            this.xrLabel116.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel116.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(77.50001F, 1F);
            this.xrLabel116.Name = "xrLabel116";
            this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel116.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel116.StylePriority.UseBorders = false;
            this.xrLabel116.StylePriority.UseFont = false;
            this.xrLabel116.StylePriority.UseTextAlignment = false;
            this.xrLabel116.Text = "/";
            this.xrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable10
            // 
            this.xrTable10.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(20.00001F, 317.9167F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10});
            this.xrTable10.SizeF = new System.Drawing.SizeF(426.0001F, 25F);
            this.xrTable10.StylePriority.UseBorders = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell21,
            this.xrTableCell22});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1D;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.StylePriority.UseBorders = false;
            this.xrTableCell20.Text = "血压(mmHg)";
            this.xrTableCell20.Weight = 0.96000045776367182D;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访血压2,
            this.txt第1次随访血压1,
            this.xrLabel108});
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Weight = 1.649999978150547D;
            // 
            // txt第1次随访血压2
            // 
            this.txt第1次随访血压2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访血压2.LocationFloat = new DevExpress.Utils.PointFloat(84.99979F, 0F);
            this.txt第1次随访血压2.Name = "txt第1次随访血压2";
            this.txt第1次随访血压2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访血压2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第1次随访血压2.StylePriority.UseBorders = false;
            this.txt第1次随访血压2.StylePriority.UseTextAlignment = false;
            this.txt第1次随访血压2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第1次随访血压1
            // 
            this.txt第1次随访血压1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访血压1.LocationFloat = new DevExpress.Utils.PointFloat(4.999765F, 0F);
            this.txt第1次随访血压1.Name = "txt第1次随访血压1";
            this.txt第1次随访血压1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访血压1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第1次随访血压1.StylePriority.UseBorders = false;
            this.txt第1次随访血压1.StylePriority.UseTextAlignment = false;
            this.txt第1次随访血压1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel108
            // 
            this.xrLabel108.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel108.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel108.LocationFloat = new DevExpress.Utils.PointFloat(74.99978F, 0F);
            this.xrLabel108.Name = "xrLabel108";
            this.xrLabel108.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel108.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel108.StylePriority.UseBorders = false;
            this.xrLabel108.StylePriority.UseFont = false;
            this.xrLabel108.StylePriority.UseTextAlignment = false;
            this.xrLabel108.Text = "/";
            this.xrLabel108.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访血压2,
            this.txt第2次随访血压1,
            this.xrLabel110});
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Weight = 1.6500000218494533D;
            // 
            // txt第2次随访血压2
            // 
            this.txt第2次随访血压2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访血压2.LocationFloat = new DevExpress.Utils.PointFloat(87.50002F, 1F);
            this.txt第2次随访血压2.Name = "txt第2次随访血压2";
            this.txt第2次随访血压2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访血压2.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第2次随访血压2.StylePriority.UseBorders = false;
            this.txt第2次随访血压2.StylePriority.UseTextAlignment = false;
            this.txt第2次随访血压2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // txt第2次随访血压1
            // 
            this.txt第2次随访血压1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访血压1.LocationFloat = new DevExpress.Utils.PointFloat(7.499992F, 1F);
            this.txt第2次随访血压1.Name = "txt第2次随访血压1";
            this.txt第2次随访血压1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访血压1.SizeF = new System.Drawing.SizeF(70F, 23F);
            this.txt第2次随访血压1.StylePriority.UseBorders = false;
            this.txt第2次随访血压1.StylePriority.UseTextAlignment = false;
            this.txt第2次随访血压1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel110
            // 
            this.xrLabel110.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel110.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel110.LocationFloat = new DevExpress.Utils.PointFloat(77.50001F, 1F);
            this.xrLabel110.Name = "xrLabel110";
            this.xrLabel110.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel110.SizeF = new System.Drawing.SizeF(10F, 23F);
            this.xrLabel110.StylePriority.UseBorders = false;
            this.xrLabel110.StylePriority.UseFont = false;
            this.xrLabel110.StylePriority.UseTextAlignment = false;
            this.xrLabel110.Text = "/";
            this.xrLabel110.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel103
            // 
            this.xrLabel103.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(0F, 317.9167F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(20F, 125F);
            this.xrLabel103.StylePriority.UseBorders = false;
            this.xrLabel103.StylePriority.UseTextAlignment = false;
            this.xrLabel103.Text = "体征";
            this.xrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(446F, 107.9167F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(330F, 30F);
            this.xrTable3.StylePriority.UseBorders = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell7});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1D;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel22,
            this.txt第3次随访方式});
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 1.5000000257749815D;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(6.000265F, 0F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(130F, 25F);
            this.xrLabel22.StylePriority.UseBorders = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "1门诊 2家庭 3电话";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第3次随访方式
            // 
            this.txt第3次随访方式.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第3次随访方式.CanGrow = false;
            this.txt第3次随访方式.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访方式.LocationFloat = new DevExpress.Utils.PointFloat(143.1784F, 4.499994F);
            this.txt第3次随访方式.Name = "txt第3次随访方式";
            this.txt第3次随访方式.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访方式.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第3次随访方式.StylePriority.UseBorders = false;
            this.txt第3次随访方式.StylePriority.UseFont = false;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel24,
            this.txt第4次随访方式});
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 1.4999999742250185D;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(6.821314F, 0F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(130F, 25F);
            this.xrLabel24.StylePriority.UseBorders = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "1门诊 2家庭 3电话";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第4次随访方式
            // 
            this.txt第4次随访方式.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第4次随访方式.CanGrow = false;
            this.txt第4次随访方式.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访方式.LocationFloat = new DevExpress.Utils.PointFloat(143.1787F, 4.499994F);
            this.txt第4次随访方式.Name = "txt第4次随访方式";
            this.txt第4次随访方式.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访方式.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第4次随访方式.StylePriority.UseBorders = false;
            this.txt第4次随访方式.StylePriority.UseFont = false;
            // 
            // xrTable4
            // 
            this.xrTable4.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 107.9167F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable4.SizeF = new System.Drawing.SizeF(446F, 30F);
            this.xrTable4.StylePriority.UseBorders = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1D;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "随访方式";
            this.xrTableCell8.Weight = 0.78378369460019082D;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访方式,
            this.xrLabel19});
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 1.1148647592047134D;
            // 
            // txt第1次随访方式
            // 
            this.txt第1次随访方式.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第1次随访方式.CanGrow = false;
            this.txt第1次随访方式.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访方式.LocationFloat = new DevExpress.Utils.PointFloat(143.0002F, 4.499994F);
            this.txt第1次随访方式.Name = "txt第1次随访方式";
            this.txt第1次随访方式.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第1次随访方式.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第1次随访方式.StylePriority.UseBorders = false;
            this.txt第1次随访方式.StylePriority.UseFont = false;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(8.000043F, 1.525879E-05F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(130F, 25F);
            this.xrLabel19.StylePriority.UseBorders = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "1门诊 2家庭 3电话";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel18,
            this.txt第2次随访方式});
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Weight = 1.1148647220543551D;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(4.749998F, 0F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(130F, 25F);
            this.xrLabel18.StylePriority.UseBorders = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "1门诊 2家庭 3电话";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txt第2次随访方式
            // 
            this.txt第2次随访方式.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.txt第2次随访方式.CanGrow = false;
            this.txt第2次随访方式.Font = new System.Drawing.Font("仿宋", 8F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访方式.LocationFloat = new DevExpress.Utils.PointFloat(143.1786F, 4.499994F);
            this.txt第2次随访方式.Name = "txt第2次随访方式";
            this.txt第2次随访方式.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访方式.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.txt第2次随访方式.StylePriority.UseBorders = false;
            this.txt第2次随访方式.StylePriority.UseFont = false;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(631.625F, 56.91666F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(15F, 18F);
            // 
            // xrLabel5
            // 
            this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(570.625F, 56.91666F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel5.StylePriority.UseBorders = false;
            // 
            // txt姓名
            // 
            this.txt姓名.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.txt姓名.LocationFloat = new DevExpress.Utils.PointFloat(78.00002F, 53.91666F);
            this.txt姓名.Name = "txt姓名";
            this.txt姓名.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt姓名.SizeF = new System.Drawing.SizeF(80F, 23F);
            this.txt姓名.StylePriority.UseFont = false;
            this.txt姓名.StylePriority.UseTextAlignment = false;
            this.txt姓名.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(18.00001F, 53.91666F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "姓名：";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(235.2083F, 26.00001F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(283.1251F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.Text = "高血压患者随访服务记录表";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 77.91666F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(446F, 30F);
            this.xrTable1.StylePriority.UseBorders = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1D;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseFont = false;
            this.xrTableCell1.Text = "随访日期";
            this.xrTableCell1.Weight = 0.78378369460019082D;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第1次随访日期});
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 1.1148647592047134D;
            // 
            // txt第1次随访日期
            // 
            this.txt第1次随访日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第1次随访日期.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第1次随访日期.LocationFloat = new DevExpress.Utils.PointFloat(8.000088F, 1.999982F);
            this.txt第1次随访日期.Name = "txt第1次随访日期";
            this.txt第1次随访日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96F);
            this.txt第1次随访日期.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第1次随访日期.StylePriority.UseBorders = false;
            this.txt第1次随访日期.StylePriority.UseFont = false;
            this.txt第1次随访日期.StylePriority.UseTextAlignment = false;
            this.txt第1次随访日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第2次随访日期});
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Weight = 1.1148647220543551D;
            // 
            // txt第2次随访日期
            // 
            this.txt第2次随访日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第2次随访日期.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第2次随访日期.LocationFloat = new DevExpress.Utils.PointFloat(6.000074F, 1.999982F);
            this.txt第2次随访日期.Name = "txt第2次随访日期";
            this.txt第2次随访日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第2次随访日期.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第2次随访日期.StylePriority.UseBorders = false;
            this.txt第2次随访日期.StylePriority.UseFont = false;
            this.txt第2次随访日期.StylePriority.UseTextAlignment = false;
            this.txt第2次随访日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("仿宋", 12F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(510.625F, 53.91666F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(60F, 23F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "编号：";
            // 
            // xrLabel12
            // 
            this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(590.9584F, 56.91666F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel12.StylePriority.UseBorders = false;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(611.2916F, 56.91666F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel11.StylePriority.UseBorders = false;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(669.2916F, 56.91666F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel10.StylePriority.UseBorders = false;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(709.9583F, 56.91666F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel9.StylePriority.UseBorders = false;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(730.2915F, 56.91666F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel8.StylePriority.UseBorders = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(648.9583F, 56.91666F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel7.StylePriority.UseBorders = false;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(689.6249F, 56.91666F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(15F, 15F);
            this.xrLabel6.StylePriority.UseBorders = false;
            // 
            // xrTable2
            // 
            this.xrTable2.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(446F, 77.91666F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(330F, 30F);
            this.xrTable2.StylePriority.UseBorders = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1D;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访日期});
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 1.5000000257749815D;
            // 
            // txt第3次随访日期
            // 
            this.txt第3次随访日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访日期.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第3次随访日期.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 1.999982F);
            this.txt第3次随访日期.Name = "txt第3次随访日期";
            this.txt第3次随访日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访日期.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第3次随访日期.StylePriority.UseBorders = false;
            this.txt第3次随访日期.StylePriority.UseFont = false;
            this.txt第3次随访日期.StylePriority.UseTextAlignment = false;
            this.txt第3次随访日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访日期});
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Weight = 1.4999999742250185D;
            // 
            // txt第4次随访日期
            // 
            this.txt第4次随访日期.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访日期.Font = new System.Drawing.Font("仿宋", 10F, System.Drawing.FontStyle.Bold);
            this.txt第4次随访日期.LocationFloat = new DevExpress.Utils.PointFloat(6.00001F, 1.999982F);
            this.txt第4次随访日期.Name = "txt第4次随访日期";
            this.txt第4次随访日期.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访日期.SizeF = new System.Drawing.SizeF(130F, 28F);
            this.txt第4次随访日期.StylePriority.UseBorders = false;
            this.txt第4次随访日期.StylePriority.UseFont = false;
            this.txt第4次随访日期.StylePriority.UseTextAlignment = false;
            this.txt第4次随访日期.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable9
            // 
            this.xrTable9.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(446.0004F, 177.9168F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable9.SizeF = new System.Drawing.SizeF(329.9999F, 139.9999F);
            this.xrTable9.StylePriority.UseBorders = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell18,
            this.xrTableCell19});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1D;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第3次随访症状其他,
            this.xrLabel98});
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Weight = 1.5D;
            // 
            // txt第3次随访症状其他
            // 
            this.txt第3次随访症状其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第3次随访症状其他.LocationFloat = new DevExpress.Utils.PointFloat(4.999878F, 25.00003F);
            this.txt第3次随访症状其他.Name = "txt第3次随访症状其他";
            this.txt第3次随访症状其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第3次随访症状其他.SizeF = new System.Drawing.SizeF(150F, 115F);
            this.txt第3次随访症状其他.StylePriority.UseBorders = false;
            this.txt第3次随访症状其他.StylePriority.UseTextAlignment = false;
            this.txt第3次随访症状其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel98
            // 
            this.xrLabel98.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(2.999973F, 1.589457E-05F);
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(50F, 25F);
            this.xrLabel98.StylePriority.UseBorders = false;
            this.xrLabel98.Text = "其他：";
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txt第4次随访症状其他,
            this.xrLabel102});
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 1.5D;
            // 
            // txt第4次随访症状其他
            // 
            this.txt第4次随访症状其他.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.txt第4次随访症状其他.LocationFloat = new DevExpress.Utils.PointFloat(4.958221F, 24.99997F);
            this.txt第4次随访症状其他.Name = "txt第4次随访症状其他";
            this.txt第4次随访症状其他.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txt第4次随访症状其他.SizeF = new System.Drawing.SizeF(150F, 115F);
            this.txt第4次随访症状其他.StylePriority.UseBorders = false;
            this.txt第4次随访症状其他.StylePriority.UseTextAlignment = false;
            this.txt第4次随访症状其他.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel102
            // 
            this.xrLabel102.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(2.958361F, 0F);
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(50F, 25F);
            this.xrLabel102.StylePriority.UseBorders = false;
            this.xrLabel102.Text = "其他：";
            // 
            // xrLabel27
            // 
            this.xrLabel27.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(0.0003496806F, 137.9167F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(20.00006F, 180F);
            this.xrLabel27.StylePriority.UseBorders = false;
            this.xrLabel27.Text = "症状";
            // 
            // xrTable64
            // 
            this.xrTable64.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable64.LocationFloat = new DevExpress.Utils.PointFloat(19.99976F, 137.9167F);
            this.xrTable64.Name = "xrTable64";
            this.xrTable64.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow64});
            this.xrTable64.SizeF = new System.Drawing.SizeF(96.00001F, 180F);
            this.xrTable64.StylePriority.UseBorders = false;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 1D;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel384,
            this.xrLabel385,
            this.xrLabel386,
            this.xrLabel387,
            this.xrLabel388,
            this.xrLabel389,
            this.xrLabel390,
            this.xrLabel391,
            this.xrLabel392});
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Weight = 3D;
            // 
            // xrLabel384
            // 
            this.xrLabel384.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel384.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.629395E-06F);
            this.xrLabel384.Name = "xrLabel384";
            this.xrLabel384.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel384.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.xrLabel384.StylePriority.UseBorders = false;
            this.xrLabel384.StylePriority.UseTextAlignment = false;
            this.xrLabel384.Text = "1 无症状";
            this.xrLabel384.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel385
            // 
            this.xrLabel385.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel385.LocationFloat = new DevExpress.Utils.PointFloat(0F, 20.00004F);
            this.xrLabel385.Name = "xrLabel385";
            this.xrLabel385.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel385.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.xrLabel385.StylePriority.UseBorders = false;
            this.xrLabel385.StylePriority.UseTextAlignment = false;
            this.xrLabel385.Text = "2 头痛头晕";
            this.xrLabel385.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel386
            // 
            this.xrLabel386.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel386.LocationFloat = new DevExpress.Utils.PointFloat(0F, 40.00007F);
            this.xrLabel386.Name = "xrLabel386";
            this.xrLabel386.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel386.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.xrLabel386.StylePriority.UseBorders = false;
            this.xrLabel386.StylePriority.UseTextAlignment = false;
            this.xrLabel386.Text = "3 恶心呕吐";
            this.xrLabel386.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel387
            // 
            this.xrLabel387.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel387.LocationFloat = new DevExpress.Utils.PointFloat(0F, 60.00007F);
            this.xrLabel387.Name = "xrLabel387";
            this.xrLabel387.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel387.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.xrLabel387.StylePriority.UseBorders = false;
            this.xrLabel387.StylePriority.UseTextAlignment = false;
            this.xrLabel387.Text = "4 眼花耳鸣";
            this.xrLabel387.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel388
            // 
            this.xrLabel388.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel388.LocationFloat = new DevExpress.Utils.PointFloat(0F, 80.00004F);
            this.xrLabel388.Name = "xrLabel388";
            this.xrLabel388.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel388.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.xrLabel388.StylePriority.UseBorders = false;
            this.xrLabel388.StylePriority.UseTextAlignment = false;
            this.xrLabel388.Text = "5 呼吸困难";
            this.xrLabel388.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel389
            // 
            this.xrLabel389.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel389.LocationFloat = new DevExpress.Utils.PointFloat(0F, 100F);
            this.xrLabel389.Name = "xrLabel389";
            this.xrLabel389.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel389.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.xrLabel389.StylePriority.UseBorders = false;
            this.xrLabel389.StylePriority.UseTextAlignment = false;
            this.xrLabel389.Text = "6 心悸胸闷";
            this.xrLabel389.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel390
            // 
            this.xrLabel390.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel390.Font = new System.Drawing.Font("仿宋", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrLabel390.LocationFloat = new DevExpress.Utils.PointFloat(0F, 120F);
            this.xrLabel390.Name = "xrLabel390";
            this.xrLabel390.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel390.SizeF = new System.Drawing.SizeF(95.99998F, 20F);
            this.xrLabel390.StylePriority.UseBorders = false;
            this.xrLabel390.StylePriority.UseFont = false;
            this.xrLabel390.StylePriority.UseTextAlignment = false;
            this.xrLabel390.Text = "7鼻子出血不止";
            this.xrLabel390.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel391
            // 
            this.xrLabel391.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel391.LocationFloat = new DevExpress.Utils.PointFloat(0F, 140F);
            this.xrLabel391.Name = "xrLabel391";
            this.xrLabel391.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel391.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.xrLabel391.StylePriority.UseBorders = false;
            this.xrLabel391.StylePriority.UseTextAlignment = false;
            this.xrLabel391.Text = "8 四肢发麻";
            this.xrLabel391.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel392
            // 
            this.xrLabel392.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel392.LocationFloat = new DevExpress.Utils.PointFloat(0F, 160F);
            this.xrLabel392.Name = "xrLabel392";
            this.xrLabel392.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel392.SizeF = new System.Drawing.SizeF(80F, 20F);
            this.xrLabel392.StylePriority.UseBorders = false;
            this.xrLabel392.StylePriority.UseTextAlignment = false;
            this.xrLabel392.Text = "9 下肢水肿";
            this.xrLabel392.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 0F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 11F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // report高血压患者随访服务记录表
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Font = new System.Drawing.Font("宋体", 9F);
            this.Margins = new System.Drawing.Printing.Margins(24, 27, 0, 11);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "13.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRLabel txt姓名;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访日期;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访日期;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访日期;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访日期;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访方式;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访方式;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访方式;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访方式;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访体重1;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访血压1;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访血压2;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访血压1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel113;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访血压2;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访血压1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel116;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访血压2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel108;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访血压2;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访血压1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel110;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访体重1;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访体重1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访体重1;
        private DevExpress.XtraReports.UI.XRTable xrTable18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访体征其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访体征其他;
        private DevExpress.XtraReports.UI.XRTable xrTable19;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访体征其他;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访体征其他;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访心率;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访心率;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访心率;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访心率;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访体质指数1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访体质指数1;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访体质指数1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访体质指数1;
        private DevExpress.XtraReports.UI.XRTable xrTable24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTable xrTable25;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTable xrTable22;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访日饮酒量2;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访日饮酒量1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访日饮酒量2;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访日饮酒量1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel150;
        private DevExpress.XtraReports.UI.XRTable xrTable23;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访日饮酒量2;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访日饮酒量1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访日饮酒量2;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访日饮酒量1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel156;
        private DevExpress.XtraReports.UI.XRTable xrTable20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访日吸烟量2;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访日吸烟量1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访日吸烟量2;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访日吸烟量1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel138;
        private DevExpress.XtraReports.UI.XRTable xrTable21;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访日吸烟量2;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访日吸烟量1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel141;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访日吸烟量2;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访日吸烟量1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel144;
        private DevExpress.XtraReports.UI.XRLabel xrLabel132;
        private DevExpress.XtraReports.UI.XRLabel xrLabel173;
        private DevExpress.XtraReports.UI.XRLabel xrLabel174;
        private DevExpress.XtraReports.UI.XRLabel xrLabel169;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访运动次数1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel159;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访运动时间1;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访运动时间2;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访运动次数2;
        private DevExpress.XtraReports.UI.XRTable xrTable26;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRLabel xrLabel189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTable xrTable27;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel194;
        private DevExpress.XtraReports.UI.XRTable xrTable28;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTable xrTable29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel201;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访心理调整;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访心理调整;
        private DevExpress.XtraReports.UI.XRLabel xrLabel202;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访心理调整;
        private DevExpress.XtraReports.UI.XRLabel xrLabel203;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访心理调整;
        private DevExpress.XtraReports.UI.XRLabel xrLabel204;
        private DevExpress.XtraReports.UI.XRTable xrTable30;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访遵医行为;
        private DevExpress.XtraReports.UI.XRLabel xrLabel210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访遵医行为;
        private DevExpress.XtraReports.UI.XRLabel xrLabel212;
        private DevExpress.XtraReports.UI.XRTable xrTable31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访遵医行为;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访遵医行为;
        private DevExpress.XtraReports.UI.XRLabel xrLabel216;
        private DevExpress.XtraReports.UI.XRTable xrTable32;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访辅助检查;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访辅助检查;
        private DevExpress.XtraReports.UI.XRTable xrTable33;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访辅助检查;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访辅助检查;
        private DevExpress.XtraReports.UI.XRTable xrTable34;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访服药依从性;
        private DevExpress.XtraReports.UI.XRLabel xrLabel222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTable xrTable35;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访服药依从性;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访服药依从性;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访服药依从性;
        private DevExpress.XtraReports.UI.XRTable xrTable36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTable xrTable37;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访药物不良反应其他;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访药物不良反应;
        private DevExpress.XtraReports.UI.XRLabel xrLabel234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访药物不良反应其他;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访药物不良反应;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访药物不良反应其他;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访药物不良反应;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访药物不良反应其他;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访药物不良反应;
        private DevExpress.XtraReports.UI.XRTable xrTable38;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTable xrTable39;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访分类;
        private DevExpress.XtraReports.UI.XRLabel xrLabel247;
        private DevExpress.XtraReports.UI.XRLabel xrLabel248;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访分类;
        private DevExpress.XtraReports.UI.XRLabel xrLabel244;
        private DevExpress.XtraReports.UI.XRLabel xrLabel249;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访分类;
        private DevExpress.XtraReports.UI.XRLabel xrLabel251;
        private DevExpress.XtraReports.UI.XRLabel xrLabel252;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访分类;
        private DevExpress.XtraReports.UI.XRLabel xrLabel241;
        private DevExpress.XtraReports.UI.XRLabel xrLabel242;
        private DevExpress.XtraReports.UI.XRTable xrTable42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTable xrTable43;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTable xrTable40;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell96;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访药物名称1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访药物名称1;
        private DevExpress.XtraReports.UI.XRTable xrTable41;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访药物名称1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访药物名称1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel253;
        private DevExpress.XtraReports.UI.XRTable xrTable48;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访药物名称3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访药物名称3;
        private DevExpress.XtraReports.UI.XRTable xrTable49;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访药物名称3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访药物名称3;
        private DevExpress.XtraReports.UI.XRTable xrTable51;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访药物3用法;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTable xrTable50;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTable xrTable44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访药物名称2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访药物名称2;
        private DevExpress.XtraReports.UI.XRTable xrTable45;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访药物名称2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访药物名称2;
        private DevExpress.XtraReports.UI.XRTable xrTable47;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访药物2用法;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTable xrTable46;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTable xrTable54;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRLabel xrLabel342;
        private DevExpress.XtraReports.UI.XRLabel xrLabel343;
        private DevExpress.XtraReports.UI.XRLabel xrLabel344;
        private DevExpress.XtraReports.UI.XRLabel xrLabel345;
        private DevExpress.XtraReports.UI.XRLabel xrLabel346;
        private DevExpress.XtraReports.UI.XRLabel xrLabel347;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRLabel xrLabel348;
        private DevExpress.XtraReports.UI.XRLabel xrLabel349;
        private DevExpress.XtraReports.UI.XRLabel xrLabel350;
        private DevExpress.XtraReports.UI.XRLabel xrLabel351;
        private DevExpress.XtraReports.UI.XRLabel xrLabel352;
        private DevExpress.XtraReports.UI.XRLabel xrLabel353;
        private DevExpress.XtraReports.UI.XRTable xrTable55;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRLabel xrLabel354;
        private DevExpress.XtraReports.UI.XRLabel xrLabel355;
        private DevExpress.XtraReports.UI.XRLabel xrLabel356;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访其他药物用法;
        private DevExpress.XtraReports.UI.XRLabel xrLabel358;
        private DevExpress.XtraReports.UI.XRLabel xrLabel359;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRLabel xrLabel360;
        private DevExpress.XtraReports.UI.XRLabel xrLabel361;
        private DevExpress.XtraReports.UI.XRLabel xrLabel362;
        private DevExpress.XtraReports.UI.XRLabel xrLabel363;
        private DevExpress.XtraReports.UI.XRLabel xrLabel364;
        private DevExpress.XtraReports.UI.XRLabel xrLabel365;
        private DevExpress.XtraReports.UI.XRTable xrTable52;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访其他药物;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访其他药物;
        private DevExpress.XtraReports.UI.XRTable xrTable53;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访其他药物;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访其他药物;
        private DevExpress.XtraReports.UI.XRLabel xrLabel366;
        private DevExpress.XtraReports.UI.XRTable xrTable56;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访转诊原因;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访转诊原因;
        private DevExpress.XtraReports.UI.XRTable xrTable57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访转诊原因;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访转诊原因;
        private DevExpress.XtraReports.UI.XRTable xrTable58;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访转诊机构;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访转诊机构;
        private DevExpress.XtraReports.UI.XRTable xrTable59;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访转诊机构;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访转诊机构;
        private DevExpress.XtraReports.UI.XRTable xrTable62;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访医生签名;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访医生签名;
        private DevExpress.XtraReports.UI.XRTable xrTable63;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访医生签名;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访医生签名;
        private DevExpress.XtraReports.UI.XRTable xrTable60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访下次随访日期;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访下次随访日期;
        private DevExpress.XtraReports.UI.XRTable xrTable61;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访下次随访日期;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访下次随访日期;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访症状6;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访症状7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访症状5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访症状4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访症状3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访症状2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访症状1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访症状8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访症状6;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访症状7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访症状5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访症状4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel59;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访症状3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访症状2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访症状1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访症状8;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访症状其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访症状其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel100;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访症状6;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访症状7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访症状5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访症状4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访症状3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel75;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访症状2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel77;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访症状1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访症状8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访症状6;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访症状7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访症状5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel86;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访症状4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel88;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访症状3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel90;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访症状2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访症状1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel xrLabel95;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访症状8;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访症状其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访症状其他;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRTable xrTable64;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRLabel xrLabel384;
        private DevExpress.XtraReports.UI.XRLabel xrLabel385;
        private DevExpress.XtraReports.UI.XRLabel xrLabel386;
        private DevExpress.XtraReports.UI.XRLabel xrLabel387;
        private DevExpress.XtraReports.UI.XRLabel xrLabel388;
        private DevExpress.XtraReports.UI.XRLabel xrLabel389;
        private DevExpress.XtraReports.UI.XRLabel xrLabel390;
        private DevExpress.XtraReports.UI.XRLabel xrLabel391;
        private DevExpress.XtraReports.UI.XRLabel xrLabel392;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访运动次数2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访运动次数1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访运动时间1;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访运动时间2;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访运动次数2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访运动次数1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访运动时间1;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访运动时间2;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访运动次数2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访运动次数1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访运动时间1;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访运动时间2;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访摄盐情况1;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访摄盐情况2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访摄盐情况1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访摄盐情况2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访摄盐情况1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访摄盐情况2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访摄盐情况1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访摄盐情况2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访体重2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访体重2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访体重2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访体重2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访体质指数2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel56;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访体质指数2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访体质指数2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访体质指数2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel txt第1次随访药物1用法;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel58;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访药物3用法;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访药物2用法;
        private DevExpress.XtraReports.UI.XRLabel txt第2次随访药物1用法;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访药物3用法;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访药物3用法;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访药物2用法;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访药物2用法;
        private DevExpress.XtraReports.UI.XRLabel txt第3次随访药物1用法;
        private DevExpress.XtraReports.UI.XRLabel txt第4次随访药物1用法;
    }
}
